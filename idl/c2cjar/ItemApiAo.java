/*     */ package com.paipai.c2c.api.item.ao.idl;
/*     */ 
/*     */ import com.paipai.lang.MultiMap;
/*     */ import com.paipai.lang.uint16_t;
/*     */ import com.paipai.lang.uint32_t;
/*     */ import com.paipai.lang.uint8_t;
/*     */ import com.paipai.util.annotation.ApiProtocol;
/*     */ import com.paipai.util.annotation.Field;
/*     */ import com.paipai.util.annotation.HeadApiProtocol;
/*     */ import com.paipai.util.annotation.Member;
/*     */ import java.util.Map;
/*     */ import java.util.Vector;
/*     */ 
/*     */ @HeadApiProtocol(cPlusNamespace="c2cent::ao::item", needInit=true)
/*     */ public class ItemApiAo
/*     */ {
/*     */   @Member(desc="商品拍卖信息", cPlusNamespace="c2cent::po::item", isNeedUFlag=true)
/*     */   class ApiAuction
/*     */   {
/*     */ 
/*     */     @Field(desc="商品ID")
/* 496 */     String itemId = "";
/*     */ 
/*     */     @Field(desc="商品库存ID")
/* 499 */     String stockId = "";
/*     */ 
/*     */     @Field(desc="卖家QQ")
/*     */     uint32_t sellerUin;
/*     */ 
/*     */     @Field(desc="类型")
/*     */     uint32_t type;
/*     */ 
/*     */     @Field(desc="加价幅度")
/*     */     uint32_t priceAddStep;
/*     */ 
/*     */     @Field(desc="加价方式")
/*     */     uint32_t priceAddType;
/*     */ 
/*     */     @Field(desc="抵押金生效金额")
/*     */     uint32_t plegeStart;
/*     */ 
/*     */     @Field(desc="抵押金")
/*     */     uint32_t plegeMoney;
/*     */     uint8_t itemId_u;
/*     */     uint8_t sellerUin_u;
/*     */     uint8_t stockId_u;
/*     */     uint8_t type_u;
/*     */     uint8_t priceAddStep_u;
/*     */     uint8_t priceAddType_u;
/*     */     uint8_t plegeStart_u;
/*     */     uint8_t plegeMoney_u;
/*     */ 
/*     */     ApiAuction()
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   @Member(desc="批量处理商品返回po", cPlusNamespace="c2cent::po::item", isNeedUFlag=false)
/*     */   class ApiBatchItemRet
/*     */   {
/*     */ 
/*     */     @Field(desc="版本号", defaultValue="1")
/*     */     uint8_t version;
/*     */ 
/*     */     @Field(desc="商品id")
/*     */     String itemid;
/*     */ 
/*     */     @Field(desc="返回值")
/*     */     int retcode;
/*     */ 
/*     */     @Field(desc="保留字")
/*     */     String reserve;
/*     */ 
/*     */     ApiBatchItemRet()
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   @ApiProtocol(cmdid="0x55071808L", desc="批量设置商品折扣")
/*     */   class ApiBatchUpdateDiscount
/*     */   {
/*     */     ApiBatchUpdateDiscount()
/*     */     {
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55071808L", desc="批量设置商品折扣请求类")
/*     */     class Req
/*     */     {
/*     */ 
/*     */       @Field(desc="机器码")
/*     */       String machineKey;
/*     */ 
/*     */       @Field(desc="来源,必填")
/*     */       String source;
/*     */ 
/*     */       @Field(desc="场景id,预留")
/*     */       uint32_t scene;
/*     */ 
/*     */       @Field(desc="商品折扣Po列表,注意里面的折扣 9折用9000表示依次类推")
/*     */       Vector<ItemApiAo.ApiItemDiscount> itemDiscount;
/*     */ 
/*     */       @Field(desc="请求保留字")
/*     */       String inReserve;
/*     */ 
/*     */       Req()
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55078808L", desc="批量设置商品折扣返回类")
/*     */     class Resp
/*     */     {
/*     */ 
/*     */       @Field(desc="返回结果")
/*     */       Vector<ItemApiAo.ApiBatchItemRet> batchItemRetList;
/*     */ 
/*     */       @Field(desc="错误信息")
/*     */       String errmsg;
/*     */ 
/*     */       @Field(desc="返回保留字")
/*     */       String reserve;
/*     */ 
/*     */       Resp()
/*     */       {
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   @ApiProtocol(cmdid="0x55071809L", desc="批量设置商品自定义分类")
/*     */   class ApiBatchUpdateShopCategory
/*     */   {
/*     */     ApiBatchUpdateShopCategory()
/*     */     {
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55071809L", desc="批量设置商品自定义分类请求类")
/*     */     class Req
/*     */     {
/*     */ 
/*     */       @Field(desc="机器码")
/*     */       String machineKey;
/*     */ 
/*     */       @Field(desc="来源,必填")
/*     */       String source;
/*     */ 
/*     */       @Field(desc="场景id,预留")
/*     */       uint32_t scene;
/*     */ 
/*     */       @Field(desc="商品自定义分类Po列表")
/*     */       Vector<ItemApiAo.ApiItemShopCategory> itemShopCategory;
/*     */ 
/*     */       @Field(desc="请求保留字")
/*     */       String inReserve;
/*     */ 
/*     */       Req()
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55078809L", desc="批量设置商品自定义分类返回类")
/*     */     class Resp
/*     */     {
/*     */ 
/*     */       @Field(desc="返回结果")
/*     */       Vector<ItemApiAo.ApiBatchItemRet> batchItemRetList;
/*     */ 
/*     */       @Field(desc="错误信息")
/*     */       String errmsg;
/*     */ 
/*     */       @Field(desc="返回保留字")
/*     */       String reserve;
/*     */ 
/*     */       Resp()
/*     */       {
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   @ApiProtocol(cmdid="0x55071804L", desc="下架商品")
/*     */   class ApiDownItem
/*     */   {
/*     */     ApiDownItem()
/*     */     {
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55071804L", desc="下架商品请求类")
/*     */     class Req
/*     */     {
/*     */ 
/*     */       @Field(desc="机器码")
/*     */       String machineKey;
/*     */ 
/*     */       @Field(desc="来源,必填")
/*     */       String source;
/*     */ 
/*     */       @Field(desc="场景id")
/*     */       uint32_t scene;
/*     */ 
/*     */       @Field(desc="商品id列表")
/*     */       Vector<String> itemIdList;
/*     */ 
/*     */       @Field(desc="请求保留字")
/*     */       String inReserve;
/*     */ 
/*     */       Req()
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55078804L", desc="下架商品返回类")
/*     */     class Resp
/*     */     {
/*     */ 
/*     */       @Field(desc="返回结果")
/*     */       Vector<ItemApiAo.ApiBatchItemRet> batchItemRetList;
/*     */ 
/*     */       @Field(desc="错误信息")
/*     */       String errmsg;
/*     */ 
/*     */       @Field(desc="返回保留字")
/*     */       String reserve;
/*     */ 
/*     */       Resp()
/*     */       {
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   @ApiProtocol(cmdid="0x55061801L", desc="查找商品")
/*     */   class ApiFindItemList
/*     */   {
/*     */     ApiFindItemList()
/*     */     {
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55061801L", desc="查找商品请求类")
/*     */     class Req
/*     */     {
/*     */ 
/*     */       @Field(desc="机器码")
/*     */       String machineKey;
/*     */ 
/*     */       @Field(desc="来源,不能为空")
/*     */       String source;
/*     */ 
/*     */       @Field(desc="场景id")
/*     */       uint32_t scene;
/*     */ 
/*     */       @Field(desc="过滤器", cPlusNamespace="itemVFilter|c2cent::po::item")
/*     */       ItemApiAo.ApiItemFilter itemVFilter;
/*     */ 
/*     */       @Field(desc="请求保留字")
/*     */       String inReserve;
/*     */ 
/*     */       Req()
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55068801L", desc="查找商品返回类")
/*     */     class Resp
/*     */     {
/*     */ 
/*     */       @Field(desc="商品PO列表", cPlusNamespace="itemPo|c2cent::po::item")
/*     */       Vector<ItemApiAo.ApiItem> apiItemList;
/*     */ 
/*     */       @Field(desc="总长度")
/*     */       uint32_t totalSize;
/*     */ 
/*     */       @Field(desc="错误信息,用于debug")
/*     */       String errmsg;
/*     */ 
/*     */       @Field(desc="返回保留字")
/*     */       String reserve;
/*     */ 
/*     */       Resp()
/*     */       {
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   @ApiProtocol(cmdid="0x55061807L", desc="获取单个商品信息")
/*     */   class ApiGetItem
/*     */   {
/*     */     ApiGetItem()
/*     */     {
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55061807L", desc="获取单个商品信息请求类")
/*     */     class Req
/*     */     {
/*     */ 
/*     */       @Field(desc="机器码")
/*     */       String machineKey;
/*     */ 
/*     */       @Field(desc="来源,必填")
/*     */       String source;
/*     */ 
/*     */       @Field(desc="场景id")
/*     */       uint32_t scene;
/*     */ 
/*     */       @Field(desc="商品id")
/*     */       String itemId;
/*     */ 
/*     */       @Field(desc="请求保留字")
/*     */       String inReserve;
/*     */ 
/*     */       Req()
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55068807L", desc="获取单个商品信息返回类")
/*     */     class Resp
/*     */     {
/*     */ 
/*     */       @Field(desc="商品PO", cPlusNamespace="apiItem|c2cent::po::item")
/*     */       ItemApiAo.ApiItem apiItem;
/*     */ 
/*     */       @Field(desc="错误信息")
/*     */       String errmsg;
/*     */ 
/*     */       @Field(desc="返回保留字")
/*     */       String reserve;
/*     */ 
/*     */       Resp()
/*     */       {
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   @ApiProtocol(cmdid="0x55061808L", desc="获取商品列表")
/*     */   class ApiGetItemList
/*     */   {
/*     */     ApiGetItemList()
/*     */     {
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55061808L", desc="获取商品列表请求类")
/*     */     class Req
/*     */     {
/*     */ 
/*     */       @Field(desc="机器码")
/*     */       String machineKey;
/*     */ 
/*     */       @Field(desc="来源,必填")
/*     */       String source;
/*     */ 
/*     */       @Field(desc="场景id")
/*     */       uint32_t scene;
/*     */ 
/*     */       @Field(desc="商品id列表")
/*     */       Vector<String> itemIdList;
/*     */ 
/*     */       @Field(desc="请求保留字")
/*     */       String inReserve;
/*     */ 
/*     */       Req()
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55068808L", desc="获取商品列表返回类")
/*     */     class Resp
/*     */     {
/*     */ 
/*     */       @Field(desc="商品PO列表", cPlusNamespace="apiItemList|c2cent::po::item")
/*     */       Vector<ItemApiAo.ApiItem> apiItemList;
/*     */ 
/*     */       @Field(desc="错误信息")
/*     */       String errmsg;
/*     */ 
/*     */       @Field(desc="返回保留字")
/*     */       String reserve;
/*     */ 
/*     */       Resp()
/*     */       {
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   @ApiProtocol(cmdid="0x55061802L", desc="通过商家编码列表(stockid)和用户QQ找出商品列表")
/*     */   class ApiGetItemListFromStockIdList
/*     */   {
/*     */     ApiGetItemListFromStockIdList()
/*     */     {
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55061802L", desc="通过商家编码列表(stockid)和用户QQ找出商品列表请求类")
/*     */     class Req
/*     */     {
/*     */ 
/*     */       @Field(desc="机器码")
/*     */       String machineKey;
/*     */ 
/*     */       @Field(desc="来源")
/*     */       String source;
/*     */ 
/*     */       @Field(desc="场景id")
/*     */       uint32_t scene;
/*     */ 
/*     */       @Field(desc="用户QQ")
/*     */       uint32_t uin;
/*     */ 
/*     */       @Field(desc="控制位,用来控制是否需要库存信息和附属信息")
/*     */       uint32_t controlbit;
/*     */ 
/*     */       @Field(desc="商家编码列表")
/*     */       Vector<String> stockIdList;
/*     */ 
/*     */       @Field(desc="请求保留字")
/*     */       String inReserve;
/*     */ 
/*     */       Req()
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55068802L", desc="通过商家编码列表(stockid)和用户QQ找出商品列表返回类")
/*     */     class Resp
/*     */     {
/*     */ 
/*     */       @Field(desc="商品PO列表,Key是商家编码")
/*     */       Map<String, ItemApiAo.ApiItem> itemMap;
/*     */ 
/*     */       @Field(desc="错误信息,用于debug")
/*     */       String errmsg;
/*     */ 
/*     */       @Field(desc="返回保留字")
/*     */       String reserve;
/*     */ 
/*     */       Resp()
/*     */       {
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   @Member(desc="商品Po", cPlusNamespace="c2cent::po::item", isNeedUFlag=true)
/*     */   public class ApiItem
/*     */   {
/*     */ 
/*     */     @Field(desc="版本", defaultValue="20100601")
/*     */     uint32_t version;
/*     */ 
/*     */     @Field(desc="快照版本号")
/*     */     uint32_t snapVersion;
/*     */ 
/*     */     @Field(desc="卖家Uin")
/*     */     uint32_t qQUin;
/*     */ 
/*     */     @Field(desc="品类(类目)ID")
/*     */     uint32_t leafClassId;
/*     */ 
/*     */     @Field(desc="城市")
/*     */     uint32_t city;
/*     */ 
/*     */     @Field(desc="省份")
/*     */     uint32_t province;
/*     */ 
/*     */     @Field(desc="国家")
/*     */     uint32_t country;
/*     */ 
/*     */     @Field(desc="商品单价")
/*     */     uint32_t price;
/*     */ 
/*     */     @Field(desc="商品数量")
/*     */     uint32_t num;
/*     */ 
/*     */     @Field(desc="商品新旧程度 值见{@link com.paipai.c2c.item.constants.ItemConstants}")
/*     */     uint32_t newType;
/*     */ 
/*     */     @Field(desc="出售方式 值见{@link com.paipai.c2c.item.constants.ItemConstants}")
/*     */     uint32_t dealType;
/*     */ 
/*     */     @Field(desc="是否支持财付通")
/*     */     uint32_t supportPayAgency;
/*     */ 
/*     */     @Field(desc="店铺推荐")
/*     */     uint32_t isRecommend;
/*     */ 
/*     */     @Field(desc="商品状态 值见{@link com.paipai.c2c.item.constants.ItemConstants}")
/*     */     uint32_t state;
/*     */ 
/*     */     @Field(desc="商品品牌id")
/*     */     uint32_t brand;
/*     */ 
/*     */     @Field(desc="商品规格id")
/*     */     uint32_t spec;
/*     */ 
/*     */     @Field(desc="商品生产商id")
/*     */     uint32_t producer;
/*     */ 
/*     */     @Field(desc="有效时间")
/*     */     uint32_t validTime;
/*     */ 
/*     */     @Field(desc="运费承担方式 值见{@link com.paipai.c2c.item.constants.ItemConstants#TRANSPORT_SELLER_PAY}等")
/*     */     uint32_t transportPriceType;
/*     */ 
/*     */     @Field(desc="商品发货方式 值见{@link com.paipai.c2c.item.constants.ItemConstants#SEND_TYPE_MONEY}等")
/*     */     uint32_t sendType;
/*     */ 
/*     */     @Field(desc="平邮价格")
/*     */     uint32_t normalMailPrice;
/*     */ 
/*     */     @Field(desc="快递价格")
/*     */     uint32_t expressMailPrice;
/*     */ 
/*     */     @Field(desc="商品限制购买数量,0表示不限制")
/*     */     uint32_t buyLimit;
/*     */ 
/*     */     @Field(desc="商品详情页面颜色主题")
/*     */     uint32_t customStyleType;
/*     */ 
/*     */     @Field(desc="商品详情文件大小")
/*     */     uint32_t detailSize;
/*     */ 
/*     */     @Field(desc="产品ID")
/*     */     uint32_t productId;
/*     */ 
/*     */     @Field(desc="商品重量 预留字段")
/*     */     uint32_t weight;
/*     */ 
/*     */     @Field(desc="上级分销商相关,没有使用")
/*     */     uint32_t uperUin;
/*     */ 
/*     */     @Field(desc="上级分销商相关,没有使用")
/*     */     uint32_t uperItemId;
/*     */ 
/*     */     @Field(desc="重新上架次数")
/*     */     uint32_t reloadCnt;
/*     */ 
/*     */     @Field(desc="店铺自定义分类")
/* 125 */     String shopClassId = "";
/*     */ 
/*     */     @Field(desc="商品详情文件名")
/* 128 */     String descFilePos = "";
/*     */ 
/*     */     @Field(desc="商品ID")
/* 131 */     String itemId = "";
/*     */ 
/*     */     @Field(desc="商品快照ID")
/* 134 */     String snapId = "";
/*     */ 
/*     */     @Field(desc="卖家昵称")
/* 137 */     String qQNick = "";
/*     */ 
/*     */     @Field(desc="卖家店铺ID")
/* 140 */     String shopId = "";
/*     */ 
/*     */     @Field(desc="商品名称")
/* 143 */     String title = "";
/*     */ 
/*     */     @Field(desc="商品所有属性,使用方法{@link com.paipai.c2c.item.constants.ItemConstants}")
/*     */     Map<uint16_t, uint8_t> property;
/*     */ 
/*     */     @Field(desc="推荐搭配商品ID列表,多个以|分隔")
/* 158 */     String relatedItems = "";
/*     */ 
/*     */     @Field(desc="类目属性串")
/* 161 */     String attrText = "";
/*     */ 
/*     */     @Field(desc="商品图片列表")
/* 164 */     Vector<String> logo = new Vector();
/*     */ 
/*     */     @Field(desc="商品扩展表字段 Key见{@link com.paipai.c2c.item.constants.ItemConstants}")
/* 167 */     MultiMap<String, String> extMap = new MultiMap();
/*     */ 
/*     */     @Field(desc="统计信息，如售出数量等")
/* 170 */     ItemApiAo.ApiItemStat stat = new ItemApiAo.ApiItemStat(ItemApiAo.this);
/*     */ 
/*     */     @Field(desc="库存相关")
/* 173 */     Vector<ItemApiAo.ApiStock> stock = new Vector();
/*     */ 
/*     */     @Field(desc="拍卖相关")
/* 182 */     ItemApiAo.ApiAuction auction = new ItemApiAo.ApiAuction(ItemApiAo.this);
/*     */ 
/*     */     @Field(desc="EMS价格")
/*     */     uint32_t emsMailPrice;
/*     */ 
/*     */     @Field(desc="商品被编辑且主图被修改的时间")
/*     */     uint32_t resetTime;
/*     */ 
/*     */     @Field(desc="当期销售量")
/*     */     uint32_t resetPayedNum;
/*     */ 
/*     */     @Field(desc="商家编码localcode")
/*     */     String stockId;
/*     */ 
/*     */     @Field(desc="保留字")
/* 197 */     String reserve = "";
/*     */     uint8_t version_u;
/*     */     uint8_t snapVersion_u;
/*     */     uint8_t qQUin_u;
/*     */     uint8_t leafClassId_u;
/*     */     uint8_t shopClassId_u;
/*     */     uint8_t city_u;
/*     */     uint8_t province_u;
/*     */     uint8_t country_u;
/*     */     uint8_t price_u;
/*     */     uint8_t num_u;
/*     */     uint8_t newType_u;
/*     */     uint8_t dealType_u;
/*     */     uint8_t supportPayAgency_u;
/*     */     uint8_t isRecommend_u;
/*     */     uint8_t state_u;
/*     */     uint8_t brand_u;
/*     */     uint8_t spec_u;
/*     */     uint8_t producer_u;
/*     */     uint8_t validTime_u;
/*     */     uint8_t transportPriceType_u;
/*     */     uint8_t sendType_u;
/*     */     uint8_t normalMailPrice_u;
/*     */     uint8_t expressMailPrice_u;
/*     */     uint8_t buyLimit_u;
/*     */     uint8_t customStyleType_u;
/*     */     uint8_t detailSize_u;
/*     */     uint8_t productId_u;
/*     */     uint8_t weight_u;
/*     */     uint8_t uperUin_u;
/*     */     uint8_t uperItemId_u;
/*     */     uint8_t reloadCnt_u;
/*     */     uint8_t descFilePos_u;
/*     */     uint8_t itemId_u;
/*     */     uint8_t snapId_u;
/*     */     uint8_t qQNick_u;
/*     */     uint8_t shopId_u;
/*     */     uint8_t title_u;
/*     */     uint8_t property_u;
/*     */     uint8_t relatedItems_u;
/*     */     uint8_t attrText_u;
/*     */     uint8_t logo_u;
/*     */     uint8_t extMap_u;
/*     */     uint8_t stat_u;
/*     */     uint8_t stock_u;
/*     */     uint8_t auction_u;
/*     */ 
/*     */     @Field(desc="")
/*     */     uint8_t emsMailPrice_u;
/*     */ 
/*     */     @Field(desc="")
/*     */     uint8_t resetTime_u;
/*     */ 
/*     */     @Field(desc="")
/*     */     uint8_t resetPayedNum_u;
/*     */ 
/*     */     @Field(desc="")
/*     */     uint8_t stockId_u;
/*     */     uint8_t reserve_u;
/*     */ 
/*     */     public ApiItem()
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   @Member(desc="商品折扣Po", cPlusNamespace="c2cent::po::item", isNeedUFlag=true)
/*     */   class ApiItemDiscount
/*     */   {
/*     */ 
/*     */     @Field(desc="版本", defaultValue="20100810")
/*     */     uint32_t version;
/*     */ 
/*     */     @Field(desc="商品Id")
/*     */     String itemId;
/*     */ 
/*     */     @Field(desc="商品折扣,彩钻多个折扣用|分隔")
/*     */     String discount;
/*     */ 
/*     */     @Field(desc="保留字段")
/*     */     String reserve;
/*     */     uint8_t version_u;
/*     */     uint8_t itemId_u;
/*     */     uint8_t discount_u;
/*     */     uint8_t reserve_u;
/*     */ 
/*     */     ApiItemDiscount()
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   @Member(desc="过滤器，详细使用见{@link com.paipai.c2c.item.constants.CVItemFilterConstants}", cPlusNamespace="c2cent::po::item", isNeedUFlag=false)
/*     */   public class ApiItemFilter
/*     */   {
/*     */ 
/*     */     @Field(desc="版本", defaultValue="1")
/*     */     uint8_t version;
/*     */ 
/*     */     @Field(desc="卖家QQ")
/*     */     uint32_t sellerUin;
/*     */ 
/*     */     @Field(desc="每页大小,最多50")
/*     */     uint32_t pageSize;
/*     */ 
/*     */     @Field(desc="页号,从0开始")
/*     */     uint32_t startIndex;
/*     */ 
/*     */     @Field(desc="排序方式,使用说明见{@link com.paipai.c2c.item.constants.CVItemFilterConstants}")
/*     */     uint32_t orderType;
/*     */ 
/*     */     @Field(desc="过滤map，使用说明见{@link com.paipai.c2c.item.constants.CVItemFilterConstants}")
/*     */     Map<String, String> filterMap;
/*     */ 
/*     */     @Field(desc="过滤map属性,用于是否需要免登录操作等特殊需求")
/*     */     uint32_t property;
/*     */ 
/*     */     public ApiItemFilter()
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   @Member(desc="商品自定义分类Po", cPlusNamespace="c2cent::po::item", isNeedUFlag=true)
/*     */   class ApiItemShopCategory
/*     */   {
/*     */ 
/*     */     @Field(desc="版本", defaultValue="20100810")
/*     */     uint32_t version;
/*     */ 
/*     */     @Field(desc="商品Id")
/*     */     String itemId;
/*     */ 
/*     */     @Field(desc="商品自定义分类")
/*     */     String shopClassId;
/*     */ 
/*     */     @Field(desc="保留字段")
/*     */     String reserve;
/*     */     uint8_t version_u;
/*     */     uint8_t itemId_u;
/*     */     uint8_t shopClassId_u;
/*     */     uint8_t reserve_u;
/*     */ 
/*     */     ApiItemShopCategory()
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   @Member(desc="商品统计信息", cPlusNamespace="c2cent::po::item", isNeedUFlag=true)
/*     */   class ApiItemStat
/*     */   {
/*     */ 
/*     */     @Field(desc="商品第一次发布时间")
/*     */     uint32_t addTime;
/*     */ 
/*     */     @Field(desc="商品编辑时间")
/*     */     uint32_t editTime;
/*     */ 
/*     */     @Field(desc="商品最后发布时间")
/*     */     uint32_t uploadTime;
/*     */ 
/*     */     @Field(desc="商品最后下架时间")
/*     */     uint32_t downloadTime;
/*     */ 
/*     */     @Field(desc="商品好评数")
/*     */     uint32_t goodRateNum;
/*     */ 
/*     */     @Field(desc="付款数")
/*     */     uint32_t payNum;
/*     */ 
/*     */     @Field(desc="全部付款数")
/*     */     uint32_t totalPayNum;
/*     */ 
/*     */     @Field(desc="付款次数")
/*     */     uint32_t payCount;
/*     */ 
/*     */     @Field(desc="总的付款次数")
/*     */     uint32_t totalPayCount;
/*     */ 
/*     */     @Field(desc="最后付款时间")
/*     */     uint32_t lastPayTime;
/*     */ 
/*     */     @Field(desc="下单数")
/*     */     uint32_t buyNum;
/*     */ 
/*     */     @Field(desc="下单总数")
/*     */     uint32_t toalBuyNum;
/*     */ 
/*     */     @Field(desc="下单次数")
/*     */     uint32_t buyCount;
/*     */ 
/*     */     @Field(desc="总的下单次数")
/*     */     uint32_t totalBuyCount;
/*     */ 
/*     */     @Field(desc="最后下单时间")
/*     */     uint32_t lastBuyTime;
/*     */ 
/*     */     @Field(desc="发布时ip地址")
/*     */     uint32_t addIp;
/*     */ 
/*     */     @Field(desc="编辑时ip地址")
/*     */     uint32_t editIp;
/*     */ 
/*     */     @Field(desc="浏览次数")
/*     */     uint32_t visitCount;
/*     */ 
/*     */     @Field(desc="市场价")
/*     */     uint32_t marketPrice;
/*     */ 
/*     */     @Field(desc="最后编辑时间")
/*     */     uint32_t lastModified;
/*     */ 
/*     */     @Field(desc="发布来源")
/* 377 */     String addSource = "";
/*     */ 
/*     */     @Field(desc="编辑来源")
/* 380 */     String editSource = "";
/*     */     uint8_t addTime_u;
/*     */     uint8_t editTime_u;
/*     */     uint8_t uploadTime_u;
/*     */     uint8_t downloadTime_u;
/*     */     uint8_t goodRateNum_u;
/*     */     uint8_t payNum_u;
/*     */     uint8_t totalPayNum_u;
/*     */     uint8_t payCount_u;
/*     */     uint8_t totalPayCount_u;
/*     */     uint8_t lastPayTime_u;
/*     */     uint8_t buyNum_u;
/*     */     uint8_t toalBuyNum_u;
/*     */     uint8_t buyCount_u;
/*     */     uint8_t totalBuyCount_u;
/*     */     uint8_t lastBuyTime_u;
/*     */     uint8_t addIp_u;
/*     */     uint8_t editIp_u;
/*     */     uint8_t visitCount_u;
/*     */     uint8_t marketPrice_u;
/*     */     uint8_t lastModified_u;
/*     */     uint8_t addSource_u;
/*     */     uint8_t editSource_u;
/*     */ 
/*     */     ApiItemStat()
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   @ApiProtocol(cmdid="0x55071805L", desc="设置店铺推荐商品")
/*     */   class ApiSetRecommendItem
/*     */   {
/*     */     ApiSetRecommendItem()
/*     */     {
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55071805L", desc="设置店铺推荐商品请求类")
/*     */     class Req
/*     */     {
/*     */ 
/*     */       @Field(desc="机器码")
/*     */       String machineKey;
/*     */ 
/*     */       @Field(desc="来源,必填")
/*     */       String source;
/*     */ 
/*     */       @Field(desc="场景id")
/*     */       uint32_t scene;
/*     */ 
/*     */       @Field(desc="商品id列表")
/*     */       Vector<String> itemIdList;
/*     */ 
/*     */       @Field(desc="推荐类型,1表示店主推荐")
/*     */       uint32_t recommendBit;
/*     */ 
/*     */       @Field(desc="请求保留字")
/*     */       String inReserve;
/*     */ 
/*     */       Req()
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55078805L", desc="设置店铺推荐商品返回类")
/*     */     class Resp
/*     */     {
/*     */ 
/*     */       @Field(desc="返回结果")
/*     */       Vector<ItemApiAo.ApiBatchItemRet> batchItemRetList;
/*     */ 
/*     */       @Field(desc="错误信息")
/*     */       String errmsg;
/*     */ 
/*     */       @Field(desc="返回保留字")
/*     */       String reserve;
/*     */ 
/*     */       Resp()
/*     */       {
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   @Member(desc="商品库存信息", cPlusNamespace="c2cent::po::item", isNeedUFlag=true)
/*     */   class ApiStock
/*     */   {
/*     */ 
/*     */     @Field(desc="商品ID")
/* 429 */     String itemId = "";
/*     */ 
/*     */     @Field(desc="发布来源")
/* 432 */     String attr = "";
/*     */ 
/*     */     @Field(desc="描述信息")
/* 435 */     String desc = "";
/*     */ 
/*     */     @Field(desc="库存ID")
/* 438 */     String stockId = "";
/*     */ 
/*     */     @Field(desc="库存hash值")
/*     */     uint32_t attrHash;
/*     */ 
/*     */     @Field(desc="库存属性")
/*     */     uint32_t property;
/*     */ 
/*     */     @Field(desc="卖家Uin")
/*     */     uint32_t sellerUin;
/*     */ 
/*     */     @Field(desc="售出数量")
/*     */     uint32_t soldNum;
/*     */ 
/*     */     @Field(desc="单价,单位：分")
/*     */     uint32_t price;
/*     */ 
/*     */     @Field(desc="库存数量")
/*     */     uint32_t num;
/*     */ 
/*     */     @Field(desc="发布时间")
/*     */     uint32_t addTime;
/*     */ 
/*     */     @Field(desc="最后编辑时间")
/*     */     uint32_t lastModifyTime;
/*     */     uint32_t index;
/*     */     uint8_t itemId_u;
/*     */     uint8_t attr_u;
/*     */     uint8_t desc_u;
/*     */     uint8_t stockId_u;
/*     */     uint8_t attrHash_u;
/*     */     uint8_t property_u;
/*     */     uint8_t sellerUin_u;
/*     */     uint8_t soldNum_u;
/*     */     uint8_t price_u;
/*     */     uint8_t num_u;
/*     */     uint8_t addTime_u;
/*     */     uint8_t lastModifyTime_u;
/*     */     uint8_t index_u;
/*     */ 
/*     */     ApiStock()
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   @ApiProtocol(cmdid="0x55071807L", desc="取消所有店铺推荐商品")
/*     */   class ApiUnSetAllRecommendItem
/*     */   {
/*     */     ApiUnSetAllRecommendItem()
/*     */     {
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55071807L", desc="取消所有店铺推荐商品请求类")
/*     */     class Req
/*     */     {
/*     */ 
/*     */       @Field(desc="机器码")
/*     */       String machineKey;
/*     */ 
/*     */       @Field(desc="来源,必填")
/*     */       String source;
/*     */ 
/*     */       @Field(desc="场景id,预留")
/*     */       uint32_t scene;
/*     */ 
/*     */       @Field(desc="推荐类型,1表示店主推荐")
/*     */       uint32_t recommendBit;
/*     */ 
/*     */       @Field(desc="请求保留字")
/*     */       String inReserve;
/*     */ 
/*     */       Req()
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55078807L", desc="取消所有店铺推荐商品返回类")
/*     */     class Resp
/*     */     {
/*     */ 
/*     */       @Field(desc="错误信息")
/*     */       String errmsg;
/*     */ 
/*     */       @Field(desc="返回保留字")
/*     */       String reserve;
/*     */ 
/*     */       Resp()
/*     */       {
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   @ApiProtocol(cmdid="0x55071806L", desc="取消店铺推荐商品")
/*     */   class ApiUnSetRecommendItem
/*     */   {
/*     */     ApiUnSetRecommendItem()
/*     */     {
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55071806L", desc="取消店铺推荐商品请求类")
/*     */     class Req
/*     */     {
/*     */ 
/*     */       @Field(desc="机器码")
/*     */       String machineKey;
/*     */ 
/*     */       @Field(desc="来源,必填")
/*     */       String source;
/*     */ 
/*     */       @Field(desc="场景id,预留")
/*     */       uint32_t scene;
/*     */ 
/*     */       @Field(desc="商品id列表")
/*     */       Vector<String> itemIdList;
/*     */ 
/*     */       @Field(desc="推荐类型,1表示店主推荐")
/*     */       uint32_t recommendBit;
/*     */ 
/*     */       @Field(desc="请求保留字")
/*     */       String inReserve;
/*     */ 
/*     */       Req()
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55078806L", desc="取消店铺推荐商品返回类")
/*     */     class Resp
/*     */     {
/*     */ 
/*     */       @Field(desc="返回结果")
/*     */       Vector<ItemApiAo.ApiBatchItemRet> batchItemRetList;
/*     */ 
/*     */       @Field(desc="错误信息")
/*     */       String errmsg;
/*     */ 
/*     */       @Field(desc="返回保留字")
/*     */       String reserve;
/*     */ 
/*     */       Resp()
/*     */       {
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   @ApiProtocol(cmdid="0x55071803L", desc="上架商品")
/*     */   class ApiUpItem
/*     */   {
/*     */     ApiUpItem()
/*     */     {
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55071803L", desc="上架商品请求类")
/*     */     class Req
/*     */     {
/*     */ 
/*     */       @Field(desc="机器码")
/*     */       String machineKey;
/*     */ 
/*     */       @Field(desc="来源,必填")
/*     */       String source;
/*     */ 
/*     */       @Field(desc="场景id")
/*     */       uint32_t scene;
/*     */ 
/*     */       @Field(desc="商品id列表")
/*     */       Vector<String> itemIdList;
/*     */ 
/*     */       @Field(desc="请求保留字")
/*     */       String inReserve;
/*     */ 
/*     */       Req()
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55078803L", desc="上架商品返回类")
/*     */     class Resp
/*     */     {
/*     */ 
/*     */       @Field(desc="返回结果")
/*     */       Vector<ItemApiAo.ApiBatchItemRet> batchItemRetList;
/*     */ 
/*     */       @Field(desc="错误信息")
/*     */       String errmsg;
/*     */ 
/*     */       @Field(desc="返回保留字")
/*     */       String reserve;
/*     */ 
/*     */       Resp()
/*     */       {
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   @ApiProtocol(cmdid="0x55071802L", desc="编辑商品")
/*     */   class ApiUpdateItem
/*     */   {
/*     */     ApiUpdateItem()
/*     */     {
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55071802L", desc="编辑商品请求类")
/*     */     class Req
/*     */     {
/*     */ 
/*     */       @Field(desc="机器码")
/*     */       String machineKey;
/*     */ 
/*     */       @Field(desc="来源,必填")
/*     */       String source;
/*     */ 
/*     */       @Field(desc="场景id")
/*     */       uint32_t scene;
/*     */ 
/*     */       @Field(desc="商品Po", cPlusNamespace="itemPo|c2cent::po::item")
/*     */       ItemApiAo.ApiItem itemPo;
/*     */ 
/*     */       @Field(desc="请求保留字")
/*     */       String inReserve;
/*     */ 
/*     */       Req()
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55078802L", desc="编辑商品返回类")
/*     */     class Resp
/*     */     {
/*     */ 
/*     */       @Field(desc="原始商品PO", cPlusNamespace="itemPo|c2cent::po::item")
/*     */       ItemApiAo.ApiItem itemPoOrg;
/*     */ 
/*     */       @Field(desc="新商品PO", cPlusNamespace="itemPo|c2cent::po::item")
/*     */       ItemApiAo.ApiItem itemPoNew;
/*     */ 
/*     */       @Field(desc="快照商品PO", cPlusNamespace="itemPo|c2cent::po::item")
/*     */       ItemApiAo.ApiItem itemPoSnap;
/*     */ 
/*     */       @Field(desc="错误信息")
/*     */       String errmsg;
/*     */ 
/*     */       @Field(desc="返回保留字")
/*     */       String reserve;
/*     */ 
/*     */       Resp()
/*     */       {
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   @ApiProtocol(cmdid="0x55071801L", desc="发布商品")
/*     */   class ApiUploadItem
/*     */   {
/*     */     ApiUploadItem()
/*     */     {
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55071801L", desc="发布商品请求类")
/*     */     class Req
/*     */     {
/*     */ 
/*     */       @Field(desc="机器码")
/*     */       String machineKey;
/*     */ 
/*     */       @Field(desc="来源,不能为空")
/*     */       String source;
/*     */ 
/*     */       @Field(desc="场景id")
/*     */       uint32_t scene;
/*     */ 
/*     */       @Field(desc="商品Po", cPlusNamespace="itemPo|c2cent::po::item")
/*     */       ItemApiAo.ApiItem itemPo;
/*     */ 
/*     */       @Field(desc="请求保留字")
/*     */       String inReserve;
/*     */ 
/*     */       Req()
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x55078801L", desc="发布商品返回类")
/*     */     class Resp
/*     */     {
/*     */ 
/*     */       @Field(desc="新商品PO", cPlusNamespace="itemPo|c2cent::po::item")
/*     */       ItemApiAo.ApiItem itemPoNew;
/*     */ 
/*     */       @Field(desc="错误信息")
/*     */       String errmsg;
/*     */ 
/*     */       @Field(desc="返回保留字")
/*     */       String reserve;
/*     */ 
/*     */       Resp()
/*     */       {
/*     */       }
/*     */     }
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.item.ao.idl.ItemApiAo
 * JD-Core Version:    0.6.2
 */