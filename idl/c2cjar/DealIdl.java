package com.paipai.c2c.api.deal.ao.idl;

import com.paipai.lang.uint32_t;
import com.paipai.lang.uint64_t;
import com.paipai.lang.uint8_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;
import java.util.Vector;

@HeadApiProtocol(cPlusNamespace="c2cent::ao::dealidl")
public class DealIdl
{
  @Member(cPlusNamespace="c2cent::po::dealidl", desc="支付信息", isSetClassSize=true)
  class CAccountInfo
  {

    @Field(desc="订单id")
    uint64_t DealId;

    @Field(desc="支付单编号")
    uint64_t PayId;

    @Field(desc="")
    uint32_t Version;

    @Field(desc="订单生成时间")
    uint32_t DealCreateTime;

    @Field(desc="订单类型")
    uint32_t DealType;

    @Field(desc="卖家号")
    uint32_t SellerUin;

    @Field(desc="买家好")
    uint32_t BuyerUin;

    @Field(desc="商品总价")
    uint32_t SumFee;

    @Field(desc="运费")
    uint32_t MailFee;

    @Field(desc="退款:买家退款金额")
    uint32_t BuyerRecvRefund;

    @Field(desc="退款:卖家实收金额")
    uint32_t SellerRecvRefund;

    @Field(desc="打款完成时间")
    uint32_t RecvfeeTime;

    @Field(desc="打款返回时间")
    uint32_t RecvfeeReturnTime;

    @Field(desc="支付现金")
    uint32_t PayFeeCash;

    @Field(desc="支付现金卷")
    uint32_t PayFeeTicket;

    @Field(desc="支付其它金额")
    uint32_t PayFeeEtc;

    @Field(desc="支付手续费")
    uint32_t PayHandleFee;

    @Field(desc="支付折扣卷")
    uint32_t PayFeeVfee;

    @Field(desc="支付使用积分")
    uint32_t PayPoint;

    @Field(desc="支付完成时间")
    uint32_t PayTime;

    @Field(desc="支付返回时间")
    uint32_t PayReturnTime;

    @Field(desc="最后更新时间")
    uint32_t LastUpdateTime;

    @Field(desc="银行类型")
    uint32_t BankType;

    @Field(desc="支付单状态")
    uint32_t PayState;

    @Field(desc="商家名称")
    String SellerName;

    @Field(desc="买家名称")
    String BuyerName;

    @Field(desc="商品标题列表")
    String ItemTitleList;

    @Field(desc="订单支付ID")
    String DealCftPayid;

    @Field(desc="")
    String Reserve;

    CAccountInfo()
    {
    }
  }

  @Member(cPlusNamespace="c2cent::po::dealidl", desc="退款信息", isSetClassSize=true)
  class CActionLog
  {

    @Field(desc="订单操作者类别")
    uint8_t OperatorType;

    @Field(desc="操作类型")
    uint8_t OperationType;

    @Field(desc="操作前订单状态\t")
    uint8_t FromState;

    @Field(desc="操作后订单状态")
    uint8_t ToState;

    @Field(desc="订单流水编号")
    uint64_t DealLogId;

    @Field(desc="订单编号")
    uint64_t DealId;

    @Field(desc="子订单编号")
    uint64_t TradeId;

    @Field(desc="\t版本号\t")
    uint32_t Version;

    @Field(desc="订单生成时间(备用)")
    uint32_t DealCreateTime;

    @Field(desc="操作时间")
    uint32_t OperateTime;

    @Field(desc="动作描述")
    String OperationDesc;

    @Field(desc="用户IP")
    String UserIp;

    @Field(desc="订单状态变更备注")
    String UserRemark;

    @Field(desc="机器码")
    String UserMachineKey;

    CActionLog()
    {
    }
  }

  @Member(cPlusNamespace="c2cent::po::dealidl", desc="大订单info", isSetClassSize=true)
  class CDealInfo
  {

    @Field(desc="承担运费方式,1卖家承担运费，2买家承担运费，3无需运费，0x0a支持运费模板的边界值")
    uint8_t WhoPayShippingfee;

    @Field(desc="订单类型, 所有类型 1--一口价 SELL_TYPE_BIN 2--单件拍卖，3--b2c订单")
    uint8_t DealType;

    @Field(desc="订单状态")
    uint8_t DealState;

    @Field(desc="preState")
    uint8_t PreDealState;

    @Field(desc="支付方式 0未定义 1支付中介 2未使用财付通付款(货到付款) 3分期付款 4移动积分")
    uint8_t DealPayType;

    @Field(desc="id")
    uint64_t DealId;

    @Field(desc="订单序列")
    uint64_t GenTimestamp;

    @Field(desc="物流id")
    uint64_t WuliuId;

    @Field(desc="商品ID签名")
    uint64_t ItemlistMd5;

    @Field(desc="订单支付ID")
    uint64_t PayId;

    @Field(desc="折扣优惠金额")
    int CouponFee;

    @Field(desc="版本号")
    uint32_t Version;

    @Field(desc="卖家号")
    uint32_t SellerUin;

    @Field(desc="买家号")
    uint32_t BuyerUin;

    @Field(desc="邮费")
    uint32_t DealPayFeeShipping;

    @Field(desc="")
    uint32_t PreDealPayFeeTotal;

    @Field(desc="费用合计")
    uint32_t DealPayFeeTotal;

    @Field(desc="订单生成时间")
    uint32_t DealCreateTime;

    @Field(desc="客服CRM")
    uint32_t SellerCrm;

    @Field(desc="邮递类型 1快递，2平邮，3EMS")
    uint32_t MailType;

    @Field(desc="退款:买家退款金额")
    uint32_t BuyerRecvRefund;

    @Field(desc="退款:卖家实收金额")
    uint32_t SellerRecvRefund;

    @Field(desc="评价状态")
    uint32_t DealRateState;

    @Field(desc="lastmodify")
    uint32_t LastUpdateTime;

    @Field(desc="现金支付金额")
    uint32_t DealPayFeeCash;

    @Field(desc="财付券支付金额")
    uint32_t DealPayFeeTicket;

    @Field(desc="积分支付金额")
    uint32_t DealPayFeeScore;

    @Field(desc="支付完成时间")
    uint32_t PayTime;

    @Field(desc="支付返回时间")
    uint32_t PayReturnTime;

    @Field(desc="卖家发货时间")
    uint32_t SellerConsignmentTime;

    @Field(desc="订单结束时间")
    uint32_t DealEndTime;

    @Field(desc="打款完成时间")
    uint32_t RecvfeeTime;

    @Field(desc="打款返回时间")
    uint32_t RecvfeeReturnTime;

    @Field(desc="备注类型")
    uint32_t DealNoteType;

    @Field(desc="退款状态")
    uint32_t DealRefundState;

    @Field(desc="大单属性位")
    uint32_t Propertymask;

    @Field(desc="")
    uint32_t DealRecvScore;

    @Field(desc="")
    uint32_t DataVersion;

    @Field(desc="")
    uint32_t BuyerDataVersion;

    @Field(desc="卖家昵称")
    String SellerName;

    @Field(desc="买家昵称")
    String BuyerName;

    @Field(desc="订单描述")
    String DealDesc;

    @Field(desc="收货人姓名")
    String ReceiveName;

    @Field(desc="地址")
    String ReceiveAddr;

    @Field(desc="邮编")
    String ReceivePostcode;

    @Field(desc="电话")
    String ReceiveTel;

    @Field(desc="手机")
    String strReceiveMobile;

    @Field(desc="发票抬头")
    String DealInvoiceTitle;

    @Field(desc="发票内容")
    String DealInvoiceContent;

    @Field(desc="运费合计说明")
    String ShippingfeeCalc;

    @Field(desc="商品标题列表")
    String ItemTitleList;

    @Field(desc="促销信息")
    String ComboInfo;

    @Field(desc="收货地址_地址编码")
    String ReceiveAddrCode;

    @Field(desc="订单来源信息")
    String Referer;

    @Field(desc="订单支付ID")
    String DealCftPayid;

    @Field(desc="订单备注")
    String DealNote;

    @Field(desc="卖家昵称")
    String SellerNick;

    @Field(desc="买家昵称")
    String BuyerNick;

    @Field(desc="string订单id")
    String DisDealId;

    @Field(desc="购买留言")
    String BuyerBuyRemark;

    @Field(desc="")
    String ImportId;

    @Field(desc="子单列表")
    Vector<DealIdl.CTradoInfo> TradeList;

    @Field(desc="支付单列表")
    Vector<DealIdl.CAccountInfo> AccountList;

    @Field(desc="物流信息")
    DealIdl.CShippingInfo ShippingInfo;
    Vector<DealIdl.CActionLog> ActionLogList;

    CDealInfo()
    {
    }
  }

  @Member(cPlusNamespace="c2cent::po::dealidl", desc="订单列表请求filter", isSetClassSize=true, isNeedUFlag=true)
  class CDealListReqPo
  {

    @Field(desc="卖家号")
    uint32_t SellerUin;
    uint8_t SellerUin_u;

    @Field(desc="买家号")
    uint32_t BuyerUin;
    uint8_t BuyerUin_u;

    @Field(desc="需要拉取的订单信息类型，请参照文档设置对应值，不要过多拉取无用信息，影响大家的性能")
    uint32_t InfoType;
    uint8_t InfoType_u;

    @Field(desc="排序类型 0升序 1降序（订单生成时间）")
    uint32_t TimeOrder;
    uint8_t TimeOrder_u;

    @Field(desc="起始时间")
    uint32_t TimeStart;
    uint8_t TimeStart_u;

    @Field(desc="结束时间")
    uint32_t TimeEnd;
    uint8_t TimeEnd_u;

    @Field(desc="页码，第一页请填 1")
    uint32_t PageIndex;
    uint8_t PageIndex_u;

    @Field(desc="每页数量，请填写1-20, 默认为20")
    uint32_t PageSize;
    uint8_t PageSize_u;

    @Field(desc="订单状态")
    uint32_t DealState;
    uint8_t DealState_u;

    @Field(desc="商品id")
    String ItemId;
    uint8_t ItemId_u;

    @Field(desc="历史订单标记 0 false 1 true")
    uint32_t HistoryFlag;
    uint8_t HistoryFlag_u;

    CDealListReqPo()
    {
    }
  }

  @Member(cPlusNamespace="c2cent::po::dealidl", desc="订单列表请求filter", isSetClassSize=true, isNeedUFlag=true)
  class CDealListReqPoNew
  {

    @Field(desc="版本号", defaultValue="1")
    uint32_t Version;
    uint8_t Version_u;

    @Field(desc="卖家号")
    uint32_t SellerUin;
    uint8_t SellerUin_u;

    @Field(desc="买家号")
    uint32_t BuyerUin;
    uint8_t BuyerUin_u;

    @Field(desc="收货人")
    String RecvName;
    uint8_t RecvName_u;

    @Field(desc="需要拉取的订单信息类型，请参照文档设置对应值，不要过多拉取无用信息，影响大家的性能")
    uint32_t InfoType;
    uint8_t InfoType_u;

    @Field(desc="时间类型")
    uint32_t TimeType;
    uint8_t TimeType_u;

    @Field(desc="排序类型 0升序 1降序")
    uint32_t TimeOrder;
    uint8_t TimeOrder_u;

    @Field(desc="备注类型1-5")
    uint32_t NoteType;
    uint8_t NoteType_u;

    @Field(desc="起始时间")
    uint32_t TimeStart;
    uint8_t TimeStart_u;

    @Field(desc="结束时间")
    uint32_t TimeEnd;
    uint8_t TimeEnd_u;

    @Field(desc="页码，第一页请填 1")
    uint32_t PageIndex;
    uint8_t PageIndex_u;

    @Field(desc="每页数量，请填写1-20, 默认为20")
    uint32_t PageSize;
    uint8_t PageSize_u;

    @Field(desc="订单状态")
    uint32_t DealState;
    uint8_t DealState_u;

    @Field(desc="评价状态101-103")
    uint32_t RateState;
    uint8_t RateState_u;

    @Field(desc="商品名称")
    String ItemTitle;
    uint8_t ItemTitle_u;

    @Field(desc="商品id")
    String ItemId;
    uint8_t ItemId_u;

    @Field(desc="历史订单标记 0 false 1 true")
    uint32_t HistoryFlag;
    uint8_t HistoryFlag_u;

    CDealListReqPoNew()
    {
    }
  }

  @Member(cPlusNamespace="c2cent::po::dealidl", desc="退款信息", isSetClassSize=true)
  class CRefundInfo
  {

    @Field(desc="子订单编号")
    uint64_t TradeId;

    @Field(desc="订单编号")
    uint64_t DealId;

    @Field(desc="退款协议ID")
    uint64_t TradeRefundId;

    @Field(desc="退款申请时间")
    uint32_t RefundReqTime;

    @Field(desc="状态")
    uint32_t RefundState;

    @Field(desc="预状态")
    uint32_t PreRefundState;

    @Field(desc="货物状态（买家选的“是否收到货”,0:没有收到货，1.已经收到货）")
    uint32_t RefundItemState;

    @Field(desc="是否需要退货，1需要退货，0不需要退货，")
    uint32_t RefundReqitemFlag;

    @Field(desc="退还买家数量")
    uint32_t RefundToBuyerNum;

    @Field(desc="申请退还买家金额")
    uint32_t RefundToBuyer;

    @Field(desc="支付给卖家金额")
    uint32_t RefundToSeller;

    @Field(desc="退款原因类型")
    uint32_t RefundReasonType;

    @Field(desc="卖家同意退货时间")
    uint32_t SellerAgreeGivebackTime;

    @Field(desc="买家发送退货时间")
    uint32_t BuyerConsignmentTime;

    @Field(desc="退款结束时间")
    uint32_t RefundEndTime;

    @Field(desc="版本号")
    uint32_t Version;

    @Field(desc="")
    uint32_t LastUpdateTime;

    @Field(desc="子单属性位")
    uint32_t TradePropertymask;

    @Field(desc="卖家号")
    uint32_t SellerUin;

    @Field(desc="订单创建时间")
    uint32_t DealCreateTime;

    @Field(desc="卖家拒绝时间")
    uint32_t SellerRefuseTime;

    @Field(desc="超时标志")
    uint32_t TimeoutItemFlag;

    @Field(desc="退款原因描述")
    String RefundReasonDesc;

    @Field(desc="买家发送退货物流信息")
    String BuyerConsignmentWuliu;

    @Field(desc="买家发送退货描述")
    String BuyerConsignmentDesc;

    @Field(desc="卖家退货地址")
    String SellerRefundAddr;

    @Field(desc="卖家同意退款附言")
    String SellerAgreeMsg;

    @Field(desc="卖家同意退货附言")
    String SellerAgreeItemMsg;

    @Field(desc="子订单超时商品标识")
    String Reserve;

    CRefundInfo()
    {
    }
  }

  @Member(cPlusNamespace="c2cent::po::dealidl", desc="物流信息", isSetClassSize=true)
  class CSendGoods
  {

    @Field(desc="收货时间")
    uint32_t RecvDays;

    @Field(desc="发货时间")
    uint32_t SendTime;

    @Field(desc="版本")
    uint32_t Version;

    @Field(desc="cardinfo")
    Vector<String> vecCardInfo;

    @Field(desc="发货描述")
    String WuliuDesc;

    @Field(desc="物流公司")
    String WuliuCompany;

    @Field(desc="物流id")
    String WuliuCode;

    @Field(desc="取货地")
    String GetItemAddr;

    @Field(desc="物流方式")
    String WuliuType;

    @Field(desc="保留字段")
    String Reserve;

    CSendGoods()
    {
    }
  }

  @Member(cPlusNamespace="c2cent::po::dealidl", desc="物流信息", isSetClassSize=true)
  class CShippingInfo
  {

    @Field(desc="订单编号")
    uint64_t DealId;

    @Field(desc="物流单生成时间")
    uint32_t WuliuGenTime;

    @Field(desc="物流状态")
    uint32_t WuliuState;

    @Field(desc="到货时间")
    uint32_t RecvTime;

    @Field(desc="预计到货时间")
    uint32_t ExpectArrivalTime;

    @Field(desc="间")
    uint32_t SendTime;

    @Field(desc="")
    uint32_t Version;

    @Field(desc="最后更新时间")
    uint32_t LastUpdateTime;

    @Field(desc="物流公司类型")
    uint32_t WuliuCompanyId;

    @Field(desc="收货地址_姓名")
    String ReceiveName;

    @Field(desc="收货地址_地址")
    String ReceiveAddr;

    @Field(desc="收货地址_地址编码")
    String ReceiveAddrCode;

    @Field(desc="收货地址_邮编")
    String ReceivePostcode;

    @Field(desc="收货地息_电话")
    String ReceiveTel;

    @Field(desc="收货地址_手机")
    String ReceiveMobile;

    @Field(desc="物流方式")
    String WuliuType;

    @Field(desc="物流公司")
    String WuliuCompany;

    @Field(desc="物流运单号")
    String WuliuCode;

    @Field(desc="取货地")
    String GetItemAddr;

    @Field(desc="物流描述")
    String WuliuDesc;

    @Field(desc="发货目的地")
    String SendToAddr;

    CShippingInfo()
    {
    }
  }

  @Member(cPlusNamespace="c2cent::po::dealidl", desc="改价", isSetClassSize=true)
  class CTradePrice
  {

    @Field(desc="子单id")
    uint64_t TradeId;

    @Field(desc="子单价格")
    int Price;

    @Field(desc="保留字段")
    String Reserve;

    CTradePrice()
    {
    }
  }

  @Member(cPlusNamespace="c2cent::po::dealidl", desc="子订单info", isSetClassSize=true)
  class CTradoInfo
  {

    @Field(desc="退款单信息表")
    Vector<DealIdl.CRefundInfo> RefundList;

    @Field(desc="订单商品状态")
    uint8_t DealItemState;

    @Field(desc="支付方式")
    uint8_t DealPayType;

    @Field(desc="订单状态")
    uint8_t DealState;

    @Field(desc="是否自动发货商品")
    uint8_t IsAutoSendItem;

    @Field(desc="商品快照版本")
    uint8_t ItemSnapversion;

    @Field(desc="商品类型")
    uint8_t ItemType;

    @Field(desc="订单号")
    uint64_t DealId;

    @Field(desc="商品id")
    uint64_t ItemId;

    @Field(desc="商品库存编号")
    uint64_t ItemStockId;

    @Field(desc="运费模板编号")
    uint64_t ShippingfeeTemplateId;

    @Field(desc="子单号")
    uint64_t TradeId;

    @Field(desc="退款协议ID")
    uint64_t TradeRefundId;

    @Field(desc="买家号")
    uint32_t BuyerUin;

    @Field(desc="关闭原因")
    uint32_t CloseReason;

    @Field(desc="关闭时间")
    uint32_t CloseTime;

    @Field(desc="生成时间")
    uint32_t DealCreateTime;

    @Field(desc="购买数量")
    uint32_t DealItemCount;

    @Field(desc="实际销售数目")
    uint32_t DealItemNum;

    @Field(desc="积分")
    uint32_t DealItemScore;

    @Field(desc="退款投诉状态")
    uint32_t DealRefundState;

    @Field(desc="卖家延迟收货时间")
    uint32_t DelayRecvtime;

    @Field(desc="商品类目")
    uint32_t ItemClass;

    @Field(desc="商品成本价")
    uint32_t ItemCostPrice;

    @Field(desc="商品原价")
    uint32_t ItemOriginalPrice;

    @Field(desc="商品价格")
    uint32_t ItemPrice;

    @Field(desc="商品重置时间")
    uint32_t ItemResetTime;

    @Field(desc="商品运费")
    uint32_t ItemShippingFee;

    @Field(desc="最后更新时间")
    uint32_t LastUpdateTime;

    @Field(desc="卖家标记缺货时间")
    uint32_t MarkNostockTime;

    @Field(desc="支付完成时间")
    uint32_t PayTime;

    @Field(desc="")
    uint32_t PreTradeState;

    @Field(desc="打款返回时间")
    uint32_t RecvfeeReturnTime;

    @Field(desc="打款完成时间")
    uint32_t RecvfeeTime;

    @Field(desc="麦基发货时间")
    uint32_t SellerConsignmentTime;

    @Field(desc="卖家uin")
    uint32_t SellerUin;

    @Field(desc="子单属性位")
    uint32_t TradePropertymask;

    @Field(desc="子单退款状态")
    uint32_t TradeRefundState;

    @Field(desc="子单状态")
    uint32_t TradeState;

    @Field(desc="子单类型")
    uint32_t TradeType;

    @Field(desc="超时商品标识")
    uint32_t TimeoutItemFlag;

    @Field(desc="")
    uint32_t Version;

    @Field(desc="折扣（红包）金额")
    uint32_t DiscountFee;

    @Field(desc="买家姓名")
    String BuyerName;

    @Field(desc="买家留言")
    String BuyerRemark;

    @Field(desc="关闭原因描述")
    String CloseReasonDesc;

    @Field(desc="商品id")
    String DisItemId;

    @Field(desc="子单id")
    String DisTradeId;

    @Field(desc="扩展信息")
    String ExtInfo;

    @Field(desc="商品标配说明")
    String ItemAccessoryDesc;

    @Field(desc="商品销售属性选项组合值")
    String ItemAttrOptionValue;

    @Field(desc="商品图片主图")
    String ItemLogo;

    @Field(desc="商品名称")
    String ItemName;

    @Field(desc="商品促销说明")
    String ItemPromotionDesc;

    @Field(desc="套餐信息")
    String PackageInfo;

    @Field(desc="产品编码")
    String ProductCode;

    @Field(desc="送送所需天数")
    String ReceiveDays;

    @Field(desc="商家名称")
    String SellerName;

    @Field(desc="卖家备注")
    String SellerRemark;

    @Field(desc="运费模板说明")
    String ShippingfeeDesc;

    @Field(desc="")
    String Reserve;

    @Field(desc="商品本地编码")
    String ItemLocalCode;

    @Field(desc="库存本地编码")
    String StockLocalCode;

    @Field(desc="商品价格调整优惠")
    int ItemAdjustPrice;

    CTradoInfo()
    {
    }
  }

  @ApiProtocol(cmdid="0x26301804L", desc="取消订单")
  class CloseDeal
  {
    CloseDeal()
    {
    }

    @ApiProtocol(cmdid="0x26301804L", desc="取消订单请求")
    class Req
    {

      @Field(desc="调用者==>请设置为源文件名")
      String Source;

      @Field(desc="订单id")
      String DealId;

      @Field(desc="场景id")
      uint32_t SceneId;

      @Field(desc="关闭原因")
      uint32_t CloseReason;

      @Field(desc="子单列表")
      Vector<uint64_t> oTradeIdList;

      @Field(desc="用户MachineKey")
      String MachineKey;

      @Field(desc="保留输入字段")
      String ReserveIn;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x26308804L", desc="取消订单返回")
    class Resp
    {

      @Field(desc="保留输出字段")
      String ReserveOut;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x26301809L", desc="确认收货")
  class ConfirmRecv
  {
    ConfirmRecv()
    {
    }

    @ApiProtocol(cmdid="0x26301809L", desc="确认收货")
    class Req
    {

      @Field(desc="调用者==>请设置为源文件名")
      String Source;

      @Field(desc="订单id")
      String DealId;

      @Field(desc="子单列表")
      Vector<uint64_t> TradeIdList;

      @Field(desc="token")
      String Token;

      @Field(desc="drawid")
      String DrawId;

      @Field(desc="用户MachineKey")
      String MachineKey;

      @Field(desc="保留输入字段")
      String ReserveIn;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x26308809L", desc="确认收货")
    class Resp
    {

      @Field(desc="保留输出字段")
      String ReserveOut;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x26301805L", desc=" 延迟收货时间")
  class DelayRecvTime
  {
    DelayRecvTime()
    {
    }

    @ApiProtocol(cmdid="0x26301805L", desc="延迟收货时间请求")
    class Req
    {

      @Field(desc="调用者==>请设置为源文件名")
      String Source;

      @Field(desc="订单id")
      String DealId;

      @Field(desc="场景id")
      uint32_t SceneId;

      @Field(desc="延迟天数")
      uint32_t Delaydays;

      @Field(desc="用户MachineKey")
      String MachineKey;

      @Field(desc="保留输入字段")
      String ReserveIn;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x26308805L", desc="延迟收货时间返回")
    class Resp
    {

      @Field(desc="保留输出字段")
      String ReserveOut;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x2630180aL", desc="获取订单信息")
  class GetDealInfo
  {
    GetDealInfo()
    {
    }

    @ApiProtocol(cmdid="0x2630180aL", desc="获取订单信息")
    class Req
    {

      @Field(desc="调用者==>请设置为源文件名")
      String Source;

      @Field(desc="订单id")
      String DealId;

      @Field(desc="infotype")
      uint32_t InfoType;

      @Field(desc="ishistory")
      boolean Ishistory;

      @Field(desc="用户MachineKey")
      String MachineKey;

      @Field(desc="保留输入字段")
      String ReserveIn;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x2630880aL", desc="获取订单信息")
    class Resp
    {

      @Field(desc="保留输出字段")
      String ReserveOut;

      @Field(desc="返回订单信息")
      DealIdl.CDealInfo oDealInfo;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x26301801L", desc="获取订单信息列表")
  class GetDealInfoList
  {
    GetDealInfoList()
    {
    }

    @ApiProtocol(cmdid="0x26301801L", desc="获取订单信息列表")
    class Req
    {

      @Field(desc="调用者==>请设置为源文件名")
      String Source;

      @Field(desc="大订单列表")
      Vector<String> oDealIdList;

      @Field(desc="请求filter")
      DealIdl.CDealListReqPo oFilter;

      @Field(desc="用户MachineKey")
      String MachineKey;

      @Field(desc="保留输入字段")
      String ReserveIn;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x26308801L", desc="做评返回")
    class Resp
    {

      @Field(desc="订单列表")
      Vector<DealIdl.CDealInfo> oDealInfoList;

      @Field(desc="符合条件总数")
      uint32_t TotalNum;

      @Field(desc="保留输出字段")
      String ReserveOut;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x2630180bL", desc="获取订单信息列表new")
  class GetDealInfoList2
  {
    GetDealInfoList2()
    {
    }

    @ApiProtocol(cmdid="0x2630180bL", desc="获取订单信息列表new请求")
    class Req
    {

      @Field(desc="调用者==>请设置为源文件名")
      String Source;

      @Field(desc="大订单列表")
      Vector<String> oDealIdList;

      @Field(desc="请求filter")
      DealIdl.CDealListReqPoNew oFilter;

      @Field(desc="用户MachineKey")
      String MachineKey;

      @Field(desc="保留输入字段")
      String ReserveIn;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x2630880bL", desc="获取订单信息列表new返回")
    class Resp
    {

      @Field(desc="订单列表")
      Vector<DealIdl.CDealInfo> oDealInfoList;

      @Field(desc="符合条件总数")
      uint32_t TotalNum;

      @Field(desc="保留输出字段")
      String ReserveOut;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x26301806L", desc="标记发货")
  class MarkShipping
  {
    MarkShipping()
    {
    }

    @ApiProtocol(cmdid="0x26301806L", desc="标记发货")
    class Req
    {

      @Field(desc="调用者==>请设置为源文件名")
      String Source;

      @Field(desc="订单id")
      String DealId;

      @Field(desc="场景id")
      uint32_t SceneId;

      @Field(desc="物流信息")
      DealIdl.CSendGoods oSendGoodsInfo;

      @Field(desc="用户MachineKey")
      String MachineKey;

      @Field(desc="保留输入字段")
      String ReserveIn;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x26308806L", desc="做评返回")
    class Resp
    {

      @Field(desc="保留输出字段")
      String ReserveOut;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x26301802L", desc="标记缺货")
  class MarkStockOut
  {
    MarkStockOut()
    {
    }

    @ApiProtocol(cmdid="0x26301802L", desc="标记缺货请求")
    class Req
    {

      @Field(desc="调用者==>请设置为源文件名")
      String Source;

      @Field(desc="订单id")
      String DealId;

      @Field(desc="子单列表")
      Vector<uint64_t> oTradeIdList;

      @Field(desc="卖家号")
      uint32_t SceneId;

      @Field(desc="用户MachineKey")
      String MachineKey;

      @Field(desc="保留输入字段")
      String ReserveIn;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x26308802L", desc="标记缺货返回")
    class Resp
    {

      @Field(desc="保留输出字段")
      String ReserveOut;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x26301803L", desc="配货中..")
  class PrepareGoods
  {
    PrepareGoods()
    {
    }

    @ApiProtocol(cmdid="0x26301803L", desc="配货中请求")
    class Req
    {

      @Field(desc="调用者==>请设置为源文件名")
      String Source;

      @Field(desc="订单id")
      String DealId;

      @Field(desc="场景id")
      uint32_t SceneId;

      @Field(desc="用户MachineKey")
      String MachineKey;

      @Field(desc="保留输入字段")
      String ReserveIn;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x26308803L", desc="配货中返回")
    class Resp
    {

      @Field(desc="保留输出字段")
      String ReserveOut;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x26301808L", desc="卖家改价")
  class SellerModifyPrice
  {
    SellerModifyPrice()
    {
    }

    @ApiProtocol(cmdid="0x26301808L", desc="卖家改价")
    class Req
    {

      @Field(desc="调用者==>请设置为源文件名")
      String Source;

      @Field(desc="订单id")
      String DealId;

      @Field(desc="场景id")
      uint32_t SceneId;

      @Field(desc="邮费")
      uint32_t ShippingFee;

      @Field(desc="子单信息")
      Vector<DealIdl.CTradePrice> vecTradeList;

      @Field(desc="用户MachineKey")
      String MachineKey;

      @Field(desc="保留输入字段")
      String ReserveIn;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x26308808L", desc="卖家改价")
    class Resp
    {

      @Field(desc="保留输出字段")
      String ReserveOut;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x2630180cL", desc="系统获取订单信息")
  class SysGetDealInfo
  {
    SysGetDealInfo()
    {
    }

    @ApiProtocol(cmdid="0x2630180cL", desc="系统获取订单信息")
    class Req
    {

      @Field(desc="调用者==>请设置为源文件名")
      String Source;

      @Field(desc="订单id")
      String DealId;

      @Field(desc="infotype")
      uint32_t InfoType;

      @Field(desc="history")
      boolean IsHistory;

      @Field(desc="用户MachineKey")
      String MachineKey;

      @Field(desc="保留输入字段")
      String ReserveIn;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x2630880cL", desc="获取订单信息")
    class Resp
    {

      @Field(desc="保留输出字段")
      String ReserveOut;

      @Field(desc="返回订单信息")
      DealIdl.CDealInfo oDealInfo;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x26301807L", desc="更新交易备注")
  class UpdateDealNote
  {
    UpdateDealNote()
    {
    }

    @ApiProtocol(cmdid="0x26301807L", desc="更新交易备注请求")
    class Req
    {

      @Field(desc="调用者==>请设置为源文件名")
      String Source;

      @Field(desc="订单id")
      String DealId;

      @Field(desc="场景id")
      uint32_t SceneId;

      @Field(desc="备注type")
      uint32_t NoteType;

      @Field(desc="备注内容")
      String Content;

      @Field(desc="用户MachineKey")
      String MachineKey;

      @Field(desc="保留输入字段")
      String ReserveIn;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x26308807L", desc="更新交易备注返回")
    class Resp
    {

      @Field(desc="保留输出字段")
      String ReserveOut;

      Resp()
      {
      }
    }
  }
}

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.deal.ao.idl.DealIdl
 * JD-Core Version:    0.6.2
 */