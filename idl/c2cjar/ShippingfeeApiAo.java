package com.paipai.c2c.api.shippingfee.ao.idl;

import com.paipai.lang.uint32_t;
import com.paipai.lang.uint8_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;
import java.util.Vector;

@HeadApiProtocol(cPlusNamespace="c2cent::ao::shippingfee", needInit=true)
public class ShippingfeeApiAo
{
  @ApiProtocol(cmdid="0x22091803L", desc="根据运费模板计算运费")
  class ApiCalcShipTemplateFee
  {
    ApiCalcShipTemplateFee()
    {
    }

    @ApiProtocol(cmdid="0x22098803L", desc="根据运费模板计算运费")
    class Resp
    {

      @Field(desc="计算运费相关信息")
      ShippingfeeApiAo.ApiShipInfoList apiShipInfoListOut;

      @Field(desc="错误信息，用于debug")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x22091803L", desc="根据运费模板计算运费")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="场景id,预留")
      uint32_t scene;

      @Field(desc="收货地址id")
      uint32_t recvRegId;

      @Field(desc="运费模板所属卖家QQ号(需要和操作者QQ一致)")
      uint32_t uin;

      @Field(desc="计算运费相关信息")
      ShippingfeeApiAo.ApiShipInfoList apiShipInfoListIn;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x22091802L", desc="通过ID获取单个运费模板")
  class ApiGetShippingfeeById
  {
    ApiGetShippingfeeById()
    {
    }

    @ApiProtocol(cmdid="0x22098802L", desc="通过ID获取单个运费模板返回类")
    class Resp
    {

      @Field(desc="运费模板Po")
      ShippingfeeApiAo.ApiShippingfee apiShippingfee;

      @Field(desc="错误信息，用于debug")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x22091802L", desc="通过ID获取单个运费模板请求类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="场景id,预留")
      uint32_t scene;

      @Field(desc="运费模板所属卖家QQ号(需要和操作者QQ一直)")
      uint32_t uin;

      @Field(desc="运费模板Id")
      uint8_t innerId;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x22091801L", desc="获取运费模板列表")
  class ApiGetShippingfeeList
  {
    ApiGetShippingfeeList()
    {
    }

    @ApiProtocol(cmdid="0x22098801L", desc="获取运费模板列表返回类")
    class Resp
    {

      @Field(desc="运费模板Po列表")
      Vector<ShippingfeeApiAo.ApiShippingfee> apiShippingfeeList;

      @Field(desc="总数")
      uint32_t totalCount;

      @Field(desc="错误信息，用于debug")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x22091801L", desc="获取运费模板列表请求类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="场景id,预留")
      uint32_t scene;

      @Field(desc="卖家QQ号")
      uint32_t uin;

      @Field(desc="每页大小,最多50")
      uint32_t pageSize;

      @Field(desc="页号,从0开始")
      uint32_t startIndex;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }
  }

  @Member(desc="运费模板相关信息list", cPlusNamespace="c2cent::po::shippingfee", isNeedUFlag=true)
  class ApiShipInfoList
  {

    @Field(desc="版本", defaultValue="0")
    uint32_t version;

    @Field(desc="运费模板相关信息vector")
    Vector<ShippingfeeApiAo.ApiShipInfo> vecShipInfo;
    uint8_t version_u;
    uint8_t vecShipInfo_u;

    ApiShipInfoList()
    {
    }
  }

  @Member(desc="运费模板相关信息", cPlusNamespace="c2cent::po::shippingfee", isNeedUFlag=true)
  class ApiShipInfo
  {

    @Field(desc="版本", defaultValue="0")
    uint32_t version;

    @Field(desc="运费模板id")
    uint32_t shipTptId;

    @Field(desc="商品所在地id")
    uint32_t cmdyRegId;

    @Field(desc="商品数量")
    uint32_t cmdyNum;

    @Field(desc="商品重量")
    uint32_t cmdyWeight;

    @Field(desc="平邮费用")
    uint32_t normalFee;

    @Field(desc="快递费用")
    uint32_t expressFee;

    @Field(desc="EMS费用")
    uint32_t emsFee;

    @Field(desc="货到付款手续费")
    uint32_t codFee;

    @Field(desc="运费类型属性")
    uint32_t property;

    @Field(desc="保留字段1")
    uint32_t reserve1;

    @Field(desc="保留字段2")
    uint32_t reserve2;

    @Field(desc="保留string")
    String reserve;
    uint8_t version_u;
    uint8_t shipTptId_u;
    uint8_t cmdyRegId_u;
    uint8_t cmdyNum_u;
    uint8_t cmdyWeight_u;
    uint8_t normalFee_u;
    uint8_t expressFee_u;
    uint8_t emsFee_u;
    uint8_t codFee_u;
    uint8_t property_u;
    uint8_t reserve1_u;
    uint8_t reserve2_u;
    uint8_t reserve_u;

    ApiShipInfo()
    {
    }
  }

  @Member(desc="运费模板Po", cPlusNamespace="c2cent::po::shippingfee", isNeedUFlag=true)
  class ApiShippingfee
  {

    @Field(desc="版本", defaultValue="0")
    uint32_t version;

    @Field(desc="自增id")
    uint32_t id;

    @Field(desc="运费模板id")
    uint32_t innerId;

    @Field(desc="属性")
    uint32_t property;

    @Field(desc="名称")
    String name;

    @Field(desc="描述信息")
    String desc;

    @Field(desc="创建时间")
    uint32_t createTime;

    @Field(desc="最后编辑时间")
    uint32_t lastModifyTime;

    @Field(desc="具体运费规则")
    Vector<ShippingfeeApiAo.ApiShippingfeeRule> vecRule;
    uint8_t version_u;
    uint8_t id_u;
    uint8_t innerId_u;
    uint8_t property_u;
    uint8_t name_u;
    uint8_t desc_u;
    uint8_t createTime_u;
    uint8_t lastModifyTime_u;
    uint8_t vecRule_u;

    ApiShippingfee()
    {
    }
  }

  @Member(desc="运费模板规则", cPlusNamespace="c2cent::po::shippingfee", isNeedUFlag=true)
  class ApiShippingfeeRule
  {

    @Field(desc="版本", defaultValue="1")
    uint32_t version;

    @Field(desc="目的地 C2C为到省份 B2C为到市")
    Vector<uint32_t> vecDest;

    @Field(desc="类型 按件或按重量")
    uint8_t type;

    @Field(desc="属性")
    uint32_t property;

    @Field(desc="运送达到时间")
    uint32_t arriveDays;

    @Field(desc="货到付款手续费")
    uint32_t onArriFee;

    @Field(desc="平邮初始重量或件数")
    uint32_t normInitHvyorCount;

    @Field(desc="平邮初始价格")
    uint32_t normInitShipFee;

    @Field(desc="平邮超过的重量或件数")
    uint32_t normOverHvyorCount;

    @Field(desc="平邮超过重量或件数时需要的额外增加的费用")
    uint32_t normOverShipFee;

    @Field(desc="快递初始重量或件数")
    uint32_t expInitHvyorCount;

    @Field(desc="快递初始价格")
    uint32_t expInitShipFee;

    @Field(desc="快递超过的重量或件数")
    uint32_t expOverHvyorCount;

    @Field(desc="快递超过重量或件数时需要的额外增加的费用")
    uint32_t expOverShipFee;

    @Field(desc="EMS初始重量或件数")
    uint32_t emsInitHvyorCount;

    @Field(desc="EMS初始价格")
    uint32_t emsInitShipFee;

    @Field(desc="EMS超过的重量或件数")
    uint32_t emsOverHvyorCount;

    @Field(desc="EMS超过重量或件数时需要的额外增加的费用")
    uint32_t emsOverShipFee;
    uint8_t version_u;
    uint8_t vecDest_u;
    uint8_t type_u;
    uint8_t property_u;
    uint8_t arriveDays_u;
    uint8_t onArriFee_u;
    uint8_t normInitHvyorCount_u;
    uint8_t normInitShipFee_u;
    uint8_t normOverHvyorCount_u;
    uint8_t normOverShipFee_u;
    uint8_t expInitHvyorCount_u;
    uint8_t expInitShipFee_u;
    uint8_t expOverHvyorCount_u;
    uint8_t expOverShipFee_u;
    uint8_t emsInitHvyorCount_u;
    uint8_t emsInitShipFee_u;
    uint8_t emsOverHvyorCount_u;
    uint8_t emsOverShipFee_u;

    ApiShippingfeeRule()
    {
    }
  }
}

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.shippingfee.ao.idl.ShippingfeeApiAo
 * JD-Core Version:    0.6.2
 */