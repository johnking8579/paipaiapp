package com.paipai.c2c.api.item.ao.idl;

import com.paipai.c2c.api.util.VBossChecker;
import com.paipai.lang.uint32_t;
import com.paipai.lang.uint8_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;
import java.util.Vector;

@HeadApiProtocol(cPlusNamespace="c2cent::ao::item", needInit=true)
public class ItemBossAo
{
  @ApiProtocol(cmdid="0x55041806L", desc="Boss设置执行历史商品关联spu")
  class AddItemInfoForSpu
  {
    AddItemInfoForSpu()
    {
    }

    @ApiProtocol(cmdid="0x55041806L", desc="Boss设置执行历史商品关联spu请求类")
    class Req
    {

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="Boss合法性检查", cPlusNamespace="VBossChecker|c2cent::po::boss", header="VBossChecker|c2cent/po/boss/boss_vcheck_po.h")
      VBossChecker bossChecker;

      @Field(desc="Spu相关信息po", cPlusNamespace="ApiItem4Spu|c2cent::po::item", header="ApiItem4Spu|c2cent/po/item/apiitem4spu_po.h")
      Vector<ItemBossAo.ApiItem4Spu> ApiItem4Spu;

      @Field(desc="是否覆盖已打spuid历史商品的标识")
      uint8_t coverSpuFlag;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x55048806L", desc="Boss设置执行历史商品关联spu返回类")
    class Resp
    {

      @Field(desc="错误信息,调试使用")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x55041805L", desc="Boss删除商品")
  class ApiBossDeleteItem
  {
    ApiBossDeleteItem()
    {
    }

    @ApiProtocol(cmdid="0x55041805L", desc="Boss删除商品请求类")
    class Req
    {

      @Field(desc="机器码,注意操作者qq不能为0")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="Boss合法性检查", cPlusNamespace="VBossChecker|c2cent::po::boss", header="VBossChecker|c2cent/po/boss/boss_vcheck_po.h")
      VBossChecker bossChecker;

      @Field(desc="商品ID")
      String itemId;

      @Field(desc="商品的最后修改时间")
      uint32_t time;

      @Field(desc="删除原因ID")
      uint32_t reasonId;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x55048805L", desc="Boss删除商品返回类")
    class Resp
    {

      @Field(desc="错误信息,调试使用")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x55041804L", desc="Boss下架商品")
  class ApiBossDownItem
  {
    ApiBossDownItem()
    {
    }

    @ApiProtocol(cmdid="0x55041804L", desc="Boss下架商品请求类")
    class Req
    {

      @Field(desc="机器码,注意操作者qq不能为0")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="Boss合法性检查", cPlusNamespace="VBossChecker|c2cent::po::boss", header="VBossChecker|c2cent/po/boss/boss_vcheck_po.h")
      VBossChecker bossChecker;

      @Field(desc="商品ID")
      String itemId;

      @Field(desc="商品的最后修改时间")
      uint32_t time;

      @Field(desc="下架原因ID")
      uint32_t reasonId;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x55048804L", desc="Boss下架商品返回类")
    class Resp
    {

      @Field(desc="错误信息,调试使用")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x55041801L", desc="Boss查找商品")
  class ApiBossFindItemList
  {
    ApiBossFindItemList()
    {
    }

    @ApiProtocol(cmdid="0x55041801L", desc="Boss查找商品请求类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="Boss合法性检查", cPlusNamespace="VBossChecker|c2cent::po::boss", header="VBossChecker|c2cent/po/boss/boss_vcheck_po.h")
      VBossChecker bossChecker;

      @Field(desc="场景id,建议申请一下")
      uint32_t scene;

      @Field(desc="过滤器,具体使用见CVItemFilterConstants.java", cPlusNamespace="ApiItemFilter|c2cent::po::item", header="ApiItemFilter|c2cent/po/item/apiitemfilter_po.h")
      ItemApiAo.ApiItemFilter filter;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x55048801L", desc="Boss查找商品返回类")
    class Resp
    {

      @Field(desc="商品PO列表")
      Vector<ItemApiAo.ApiItem> itemList;

      @Field(desc="总数")
      uint32_t totalCount;

      @Field(desc="错误信息,调试使用")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x55041813L", desc="获取单个商品信息")
  class ApiBossGetItem
  {
    ApiBossGetItem()
    {
    }

    @ApiProtocol(cmdid="0x55041813L", desc="获取单个商品信息请求类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源,必填")
      String source;

      @Field(desc="Boss合法性检查", cPlusNamespace="VBossChecker|c2cent::po::boss", header="VBossChecker|c2cent/po/boss/boss_vcheck_po.h")
      VBossChecker bossChecker;

      @Field(desc="场景id")
      uint32_t scene;

      @Field(desc="商品id")
      String itemId;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x55048813L", desc="获取单个商品信息返回类")
    class Resp
    {

      @Field(desc="商品PO", cPlusNamespace="apiItem|c2cent::po::item")
      ItemApiAo.ApiItem apiItem;

      @Field(desc="错误信息")
      String errmsg;

      @Field(desc="返回保留字")
      String reserve;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x55041814L", desc="设置商品属性通用接口，目前可设置的属性只有6|24|16|17|26|30|28|14|32|34|35|36|38|39|40|43|44，每次只能设置一个属性位")
  class ApiBossSetProperty
  {
    ApiBossSetProperty()
    {
    }

    @ApiProtocol(cmdid="0x55041814L", desc="设置商品属性通用接口")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源,必填")
      String source;

      @Field(desc="Boss合法性检查", cPlusNamespace="VBossChecker|c2cent::po::boss", header="VBossChecker|c2cent/po/boss/boss_vcheck_po.h")
      VBossChecker bossChecker;

      @Field(desc="场景id")
      uint32_t scene;

      @Field(desc="商品列表")
      Vector<ItemApiAo.ApiItem> apiItemList;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x55048814L", desc="设置商品属性通用接口返回类")
    class Resp
    {

      @Field(desc="返回结果列表", cPlusNamespace="ApiBatchItemRet|c2cent::po::item", header="ApiBatchItemRet|c2cent/po/item/apibatchitemret_po.h")
      Vector<ItemApiAo.ApiBatchItemRet> batchItemRetList;

      @Field(desc="错误信息")
      String errmsg;

      @Field(desc="返回保留字")
      String reserve;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x55041803L", desc="Boss上架商品")
  class ApiBossUpItem
  {
    ApiBossUpItem()
    {
    }

    @ApiProtocol(cmdid="0x55041803L", desc="Boss上架商品请求类")
    class Req
    {

      @Field(desc="机器码,注意操作者qq不能为0")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="Boss合法性检查", cPlusNamespace="VBossChecker|c2cent::po::boss", header="VBossChecker|c2cent/po/boss/boss_vcheck_po.h")
      VBossChecker bossChecker;

      @Field(desc="商品ID")
      String itemId;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x55048803L", desc="Boss上架商品返回类")
    class Resp
    {

      @Field(desc="错误信息,调试使用")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x55041802L", desc="Boss编辑商品")
  class ApiBossUpdateItem
  {
    ApiBossUpdateItem()
    {
    }

    @ApiProtocol(cmdid="0x55041802L", desc="Boss编辑商品请求类")
    class Req
    {

      @Field(desc="机器码,注意操作者qq不能为0")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="Boss合法性检查", cPlusNamespace="VBossChecker|c2cent::po::boss", header="VBossChecker|c2cent/po/boss/boss_vcheck_po.h")
      VBossChecker bossChecker;

      @Field(desc="场景id,建议申请一下")
      uint32_t scene;

      @Field(desc="商品Po", cPlusNamespace="ApiItem|c2cent::po::item", header="ApiItem|c2cent/po/item/apiitem_po.h")
      ItemApiAo.ApiItem apiItem;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x55048802L", desc="Boss编辑商品返回类")
    class Resp
    {

      @Field(desc="原始商品PO", cPlusNamespace="ApiItem|c2cent::po::item", header="ApiItem|c2cent/po/item/apiitem_po.h")
      ItemApiAo.ApiItem itemOrg;

      @Field(desc="新商品Po", cPlusNamespace="ApiItem|c2cent::po::item", header="ApiItem|c2cent/po/item/apiitem_po.h")
      ItemApiAo.ApiItem itemNew;

      @Field(desc="快照商品Po", cPlusNamespace="ApiItem|c2cent::po::item", header="ApiItem|c2cent/po/item/apiitem_po.h")
      ItemApiAo.ApiItem itemSnap;

      @Field(desc="错误信息,调试使用")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }
  }

  @Member(desc="ApiItem4Spu Po", cPlusNamespace="c2cent::po::item", isNeedUFlag=true)
  class ApiItem4Spu
  {
    uint32_t leafClassId;
    uint32_t propertyKeyId;
    uint32_t propertyValueId;
    uint32_t spuId;
    uint8_t leafClassId_u;
    uint8_t propertyKeyId_u;
    uint8_t propertyValueId_u;
    uint8_t spuId_u;

    ApiItem4Spu()
    {
    }
  }

  @ApiProtocol(cmdid="0x55041809L", desc="BOSS促销活动创建")
  class C2CCreatePromotionActive
  {
    C2CCreatePromotionActive()
    {
    }

    @ApiProtocol(cmdid="0x55041809L", desc="BOSS促销活动创建类")
    class Req
    {

      @Field(desc="机器码,注意操作者qq不能为0")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="Boss合法性检查", cPlusNamespace="VBossChecker|c2cent::po::boss", header="VBossChecker|c2cent/po/boss/boss_vcheck_po.h")
      VBossChecker bossChecker;

      @Field(desc="用户QQ号码")
      uint32_t dwUin;

      @Field(desc="促销活动ID")
      uint32_t dwActiveId;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x55048809L", desc="BOSS促销活动创建")
    class Resp
    {

      @Field(desc="错误信息,调试使用")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x55041811L", desc="BOSS促销活动结束")
  class C2CEndPromotionActive
  {
    C2CEndPromotionActive()
    {
    }

    @ApiProtocol(cmdid="0x55041811L", desc="BOSS促销活动结束类")
    class Req
    {

      @Field(desc="机器码,注意操作者qq不能为0")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="Boss合法性检查", cPlusNamespace="VBossChecker|c2cent::po::boss", header="VBossChecker|c2cent/po/boss/boss_vcheck_po.h")
      VBossChecker bossChecker;

      @Field(desc="用户QQ号码")
      uint32_t dwUin;

      @Field(desc="促销活动ID")
      uint32_t dwActiveId;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x55048811L", desc="BOSS促销活动结束")
    class Resp
    {

      @Field(desc="错误信息,调试使用")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x55041810L", desc="BOSS促销活动开始")
  class C2CStartPromotionActive
  {
    C2CStartPromotionActive()
    {
    }

    @ApiProtocol(cmdid="0x55041810L", desc="BOSS促销活动开始类")
    class Req
    {

      @Field(desc="机器码,注意操作者qq不能为0")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="Boss合法性检查", cPlusNamespace="VBossChecker|c2cent::po::boss", header="VBossChecker|c2cent/po/boss/boss_vcheck_po.h")
      VBossChecker bossChecker;

      @Field(desc="用户QQ号码")
      uint32_t dwUin;

      @Field(desc="促销活动ID")
      uint32_t dwActiveId;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x55048810L", desc="BOSS促销活动开始")
    class Resp
    {

      @Field(desc="错误信息,调试使用")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x55041807L", desc="Boss查询历史商品关联spu运行状态")
  class GetSpuSetStatus
  {
    GetSpuSetStatus()
    {
    }

    @ApiProtocol(cmdid="0x55041807L", desc="Boss查询历史商品关联spu运行状态请求类")
    class Req
    {

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="Boss合法性检查", cPlusNamespace="VBossChecker|c2cent::po::boss", header="VBossChecker|c2cent/po/boss/boss_vcheck_po.h")
      VBossChecker bossChecker;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x55048807L", desc="Boss查询历史商品关联spu运行状态返回类")
    class Resp
    {

      @Field(desc="历史商品关联spu运行状态信息")
      String spuSetStatus;

      @Field(desc="是否执行完毕的标识")
      uint8_t endFlag;

      @Field(desc="错误信息,调试使用")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x55041808L", desc="BOSS设置和取消AUCTION2商品属性")
  class SetAuction2Property
  {
    SetAuction2Property()
    {
    }

    @ApiProtocol(cmdid="0x55041808L", desc="BOSS设置和取消AUCTION2商品属性请求类")
    class Req
    {

      @Field(desc="机器码,注意操作者qq不能为0")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="Boss合法性检查", cPlusNamespace="VBossChecker|c2cent::po::boss", header="VBossChecker|c2cent/po/boss/boss_vcheck_po.h")
      VBossChecker bossChecker;

      @Field(desc="商品ID")
      String itemId;

      @Field(desc="设置和取消操作Flag。1:设置;0:取消")
      uint8_t opFlag;

      @Field(desc="活动类型。0:今日特价;3:试吃试用;4:折扣专享;5:疯狂抢车;10: 会员欢乐购;11:冰点价;等等")
      uint32_t auctionType;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x55048808L", desc="BOSS设置和取消AUCTION2商品属性返回类")
    class Resp
    {

      @Field(desc="错误信息,调试使用")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x55041812L", desc="设置商品的当期销售量")
  class SetSaleCount
  {
    SetSaleCount()
    {
    }

    @ApiProtocol(cmdid="0x55041812L", desc="设置商品的当期销售量")
    class Req
    {

      @Field(desc="机器码,注意操作者qq不能为0")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="Boss合法性检查", cPlusNamespace="VBossChecker|c2cent::po::boss", header="VBossChecker|c2cent/po/boss/boss_vcheck_po.h")
      VBossChecker bossChecker;

      @Field(desc="商品ID")
      String itemId;

      @Field(desc="设置当期销售量。保留字段，现在只能恢复当期销售量。1:恢复当期销售量;0:清空当期销售量")
      uint8_t opFlag;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x55048812L", desc="设置商品的当期销售量")
    class Resp
    {

      @Field(desc="错误信息,调试使用")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }
  }
}

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.item.ao.idl.ItemBossAo
 * JD-Core Version:    0.6.2
 */