package com.paipai.c2c.api.dealmsg.ao.idl;

import com.paipai.lang.uint32_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;
import java.util.Vector;

@HeadApiProtocol(cPlusNamespace="c2cent::ao::dealmsgidl")
public class DealmsgIdl
{
  @ApiProtocol(cmdid="0x91201802L", desc="获取单个订单的留言列表")
  class GetDealMsg
  {
    GetDealMsg()
    {
    }

    @ApiProtocol(cmdid="0x91208802L", desc="获取单个订单的留言列表--返回")
    class Resp
    {

      @Field(desc="返回po")
      Vector<DealmsgIdl.CDealMsgPo> RespList;

      @Field(desc="留言总数")
      uint32_t TotalNum;

      @Field(desc="错误信息")
      String ErrMsg;

      @Field(desc="保留输出字段")
      String ReserveOut;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x91201802L", desc="获取单个订单的留言列表--请求")
    class Req
    {

      @Field(desc="调用者==>请设置为源文件名")
      String Source;

      @Field(desc="订单id")
      String DealId;

      @Field(desc="页码")
      uint32_t PageIndex;

      @Field(desc="每页数")
      uint32_t PageSize;

      @Field(desc="用户MachineKey")
      String MachineKey;

      @Field(desc="保留输入字段")
      String ReserveIn;

      Req()
      {
      }
    }
  }

  @Member(cPlusNamespace="c2cent::po::dealmsgidl", desc="订单列表请求filter", isSetClassSize=true, isNeedUFlag=true)
  class CDealMsgPo
  {

    @Field(desc="留言id")
    uint32_t MsgID;

    @Field(desc="订单id")
    String DealId;

    @Field(desc="留言者uin")
    uint32_t QuestionerUin;

    @Field(desc="留言者nick")
    String QuestionerNick;

    @Field(desc="被留言者uin")
    uint32_t ReplierUin;

    @Field(desc="被留言者nick")
    String ReplierNick;

    @Field(desc="内容")
    String Content;

    @Field(desc="留言时间")
    uint32_t QuestionTime;

    CDealMsgPo()
    {
    }
  }

  @ApiProtocol(cmdid="0x91201801L", desc="订单留言接口")
  class LeaveMsg
  {
    LeaveMsg()
    {
    }

    @ApiProtocol(cmdid="0x91208801L", desc="订单留言接口--返回")
    class Resp
    {

      @Field(desc="错误信息")
      String ErrMsg;

      @Field(desc="保留输出字段")
      String ReserveOut;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x91201801L", desc="订单留言接口--请求")
    class Req
    {

      @Field(desc="调用者==>请设置为源文件名")
      String Source;

      @Field(desc="订单id")
      String DealId;

      @Field(desc="留言内容")
      String MsgContent;

      @Field(desc="用户MachineKey")
      String MachineKey;

      @Field(desc="保留输入字段")
      String ReserveIn;

      Req()
      {
      }
    }
  }
}

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.dealmsg.ao.idl.DealmsgIdl
 * JD-Core Version:    0.6.2
 */