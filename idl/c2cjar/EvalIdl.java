package com.paipai.c2c.api.eval.ao.idl;

import com.paipai.lang.uint32_t;
import com.paipai.lang.uint8_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;
import java.util.Vector;

/**
 * 从c2c.jar编译
 * @author JingYing
 * @date 2015年1月13日
 */
@HeadApiProtocol(cPlusNamespace="c2cent::ao::evalidl")
public class EvalIdl
{
  @Member(cPlusNamespace="c2cent::po::evalidl", desc="商品dsr", isSetClassSize=true)
  class ComdyDsr
  {

    @Field(desc="当期的好评数(付款时间 -90天~当天)C")
    uint32_t goodcount;

    @Field(desc="当期的中评数(-90天~当天)C")
    uint32_t normalcount;

    @Field(desc=" 当期的差评数(-90天~当天)C")
    uint32_t badcount;

    @Field(desc="最近6个月DSR1好评数 B")
    uint32_t dsr1good;

    @Field(desc="最近6个月DSR1差评数 B")
    uint32_t dsr1bad;

    @Field(desc="最近6个月DSR2好评数 B")
    uint32_t dsr2good;

    @Field(desc="最近6个月DSR2差评数 B")
    uint32_t dsr2bad;

    @Field(desc="最近6个月DSR3好评数 B")
    uint32_t dsr3good;

    @Field(desc="最近6个月DSR3差评数 B")
    uint32_t dsr3bad;

    @Field(desc="用户的有效DSR1总分数 C")
    uint32_t dsr1total;

    @Field(desc="用户的有效DSR2总分数 C")
    uint32_t dsr2total;

    @Field(desc="用户的有效DSR3总分数 C")
    uint32_t dsr3total;

    @Field(desc="用户的有效DSR打分个数 C")
    uint32_t dsrtotal;

    ComdyDsr()
    {
    }
  }

  @Member(cPlusNamespace="c2cent::po::evalidl", desc="做评结果", isSetClassSize=true)
  class EvalRet
  {

    @Field(desc="订单id-大单")
    String dealid;

    @Field(desc="结果-0表示成功  1表示失败")
    uint32_t retcode;

    EvalRet()
    {
    }
  }

  @Member(cPlusNamespace="c2cent::po::evalidl", desc="评价统计po", isSetClassSize=true)
  class CEvalStatPo
  {

    @Field(desc="卖家号")
    uint32_t UserUin;

    @Field(desc="做为买家好评总数")
    uint32_t BuyerGoodAll;

    @Field(desc="做为买家所有中评")
    uint32_t BuyerNormalAll;

    @Field(desc="做为买家所有差评")
    uint32_t BuyerBadAll;

    @Field(desc="卖家时对应的评价数")
    uint32_t SellerGoodAll;

    @Field(desc="卖家时对应的评价数")
    uint32_t SellerNormalAll;

    @Field(desc="卖家时对应的评价数")
    uint32_t SellerBadAll;

    @Field(desc="卖家信用总数")
    int SellerCreditAll;

    @Field(desc="实物信用")
    int SellerCreditNormal;

    @Field(desc="虚拟信用")
    int SellerCreditVirtual;

    @Field(desc="买家信用总数")
    int BuyerCreditAll;

    @Field(desc="买家实物信用")
    int BuyerCreditNormal;

    @Field(desc="买家虚拟信用")
    int BuyerCreditVirtual;

    @Field(desc="b评价总数")
    int dwEvalNums;

    @Field(desc="b评价总分数")
    int dwSellerLevelTotal;

    CEvalStatPo()
    {
    }
  }

  @Member(cPlusNamespace="c2cent::po::evalidl", desc="评价统计po", isSetClassSize=true)
  class CEvalListReqPo
  {

    @Field(desc="订单id")
    String dealid;

    @Field(desc="卖家QQ号-")
    uint32_t selleruin;

    @Field(desc="商品id--")
    String comdyid;

    @Field(desc="页码")
    uint32_t pageindex;

    @Field(desc="每页数量")
    uint32_t pagesize;

    @Field(desc="当期时间")
    uint32_t userresettime;

    @Field(desc="是否历史库")
    uint32_t ishistory;

    @Field(desc="是否需要回复")
    uint32_t needreply;

    @Field(desc="请求者身份")
    uint32_t role;

    @Field(desc="评价等级")
    uint32_t level;

    @Field(desc="是否为收到的评价")
    uint32_t isrecv;

    @Field(desc="时间类型")
    uint32_t timetype;

    @Field(desc="内容标记")
    uint32_t content;

    CEvalListReqPo()
    {
    }
  }

  @Member(cPlusNamespace="c2cent::po::evalidl", desc="评价po", isSetClassSize=true)
  class CEvalReplyPo
  {

    @Field(desc="回复者QQ号")
    uint32_t Uin;

    @Field(desc="订单id-int型")
    uint32_t FDealId;

    @Field(desc="创建时间")
    uint32_t CreateTime;

    @Field(desc="订单id-str型")
    String DealId;

    @Field(desc="内容")
    String Content;

    @Field(desc="id")
    uint32_t Id;

    CEvalReplyPo()
    {
    }
  }

  @Member(cPlusNamespace="c2cent::po::evalidl", desc="评价po", isSetClassSize=true)
  class CEvalRecordPo
  {

    @Field(desc="版本号")
    uint32_t Version;

    @Field(desc="子订单id-int型")
    uint32_t FDealId;

    @Field(desc="子订单id-str型")
    String DealId;

    @Field(desc="商品id")
    String CommodityId;

    @Field(desc="商品logo")
    String CommodityLogo;

    @Field(desc="商品名称")
    String CommodityTitle;

    @Field(desc="快照号")
    String SnapId;

    @Field(desc="买家号")
    uint32_t BuyerUin;

    @Field(desc="卖家号")
    uint32_t SellerUin;

    @Field(desc="卖家nick")
    String SellerNickName;

    @Field(desc="买家nick")
    String BuyerNickName;

    @Field(desc="买家做评时间")
    uint32_t BuyerEvalTime;

    @Field(desc="卖家做评时间")
    uint32_t SellerEvalTime;

    @Field(desc="买家评价内容")
    String BuyerExplain;

    @Field(desc="卖家评价内容")
    String SellerExplain;

    @Field(desc="买家评价原因")
    Vector<uint32_t> BuyerReason;

    @Field(desc="卖家评价原因")
    Vector<uint32_t> SellerReason;

    @Field(desc="买家得分")
    int BuyerScore;

    @Field(desc="卖家得分")
    int SellerScore;

    @Field(desc="实际交易总额，单位分")
    uint32_t DealPayment;

    @Field(desc="系统计分标记")
    uint32_t ScoreFlag;

    @Field(desc="系统计分时间")
    uint32_t ScoreTime;

    @Field(desc="商品价格")
    uint32_t Price;

    @Field(desc="评价创建时间")
    uint32_t EvalCreateTime;

    @Field(desc="订单支付类型")
    uint32_t paytype;

    @Field(desc="订单结束时间")
    uint32_t DealFinishTime;

    @Field(desc="订单支付时间")
    uint32_t DealPayTime;

    @Field(desc="买家评价级别1：差评，2：中评，3：好评")
    uint8_t BuyerEvalLevel;

    @Field(desc="卖家评价级别1：差评，2：中评，3：好评")
    uint8_t SellerEvalLevel;

    @Field(desc="买家记录是否被删除,0不删除1删除")
    uint8_t BuyerEvalDelFlag;

    @Field(desc="卖家记录是否被删除，0不删除1删除")
    uint8_t SellerEvalDelFlag;

    @Field(desc="对方是否为系统做评")
    uint8_t PeerSysEval;

    @Field(desc="自己是否为系统做评")
    uint8_t SelfSysEval;

    @Field(desc="评价回复")
    Vector<EvalIdl.CEvalReplyPo> ReplyMsg;

    @Field(desc="可以回复次数")
    uint32_t NumCanReply;

    @Field(desc="子订单对应的大订单号")
    String FtradeId;

    @Field(desc="发货速度")
    uint32_t Dsr1;

    @Field(desc="商品质量")
    uint32_t Dsr2;

    @Field(desc="服务态度")
    uint32_t Dsr3;

    @Field(desc="dsr做评时间")
    uint32_t DsrEvalTime;

    @Field(desc="0表示有效，1为无效")
    uint32_t DsrFlag;

    CEvalRecordPo()
    {
    }
  }

  @ApiProtocol(cmdid="0x23151805L", desc="买家做评")
  class BuyerMakeEval
  {
    BuyerMakeEval()
    {
    }

    @ApiProtocol(cmdid="0x23158805L", desc="做评返回")
    class Resp
    {

      @Field(desc="保留输出字段")
      String reserveOut;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x23151805L", desc="买家做评请求")
    class Req
    {

      @Field(desc="调用者")
      String source;

      @Field(desc="待做评列表，大订单列表")
      String dealid;

      @Field(desc="评价等级")
      uint32_t evallevel;

      @Field(desc="评价内容")
      String evalcontent;

      @Field(desc="用户IP")
      String userIP;

      @Field(desc="用户MachineKey")
      String userMachineKey;

      @Field(desc="保留输入字段")
      String reserveIn;

      Req()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x23151804L", desc="获取商品的dsr")
  class GetComdyDsr
  {
    GetComdyDsr()
    {
    }

    @ApiProtocol(cmdid="0x23158804L", desc="获取商品的dsr-返回")
    class Resp
    {

      @Field(desc="商品dsrpo")
      EvalIdl.ComdyDsr comdydsr;

      @Field(desc="保留输出字段")
      String reserveOut;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x23151804L", desc="获取商品的dsr-请求")
    class Req
    {

      @Field(desc="调用者==>请设置为源文件名")
      String source;

      @Field(desc="商品id--必填")
      String comdyid;

      @Field(desc="用户IP")
      String userIP;

      @Field(desc="用户MachineKey")
      String userMachineKey;

      @Field(desc="保留输入字段")
      String reserveIn;

      Req()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x23151803L", desc="获取统计信息")
  class GetEvalStat
  {
    GetEvalStat()
    {
    }

    @ApiProtocol(cmdid="0x23158803L", desc="用户统计信息-返回")
    class Resp
    {

      @Field(desc="统计信息po")
      EvalIdl.CEvalStatPo statinfo;

      @Field(desc="保留输出字段")
      String reserveOut;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x23151803L", desc="用户统计信息-请求")
    class Req
    {

      @Field(desc="调用者==>请设置为源文件名")
      String source;

      @Field(desc="卖家号--必填")
      uint32_t selleruin;

      @Field(desc="用户IP")
      String userIP;

      @Field(desc="用户MachineKey")
      String userMachineKey;

      @Field(desc="保留输入字段")
      String reserveIn;

      Req()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x23151802L", desc="获取评价列表--支持3种方式(dealid,comdyid,selleruin,优先级依次下降)")
  class GetEvalList
  {
    GetEvalList()
    {
    }

    @ApiProtocol(cmdid="0x23158802L", desc="根据商品id获取评价-返回")
    class Resp
    {

      @Field(desc="评价信息poList")
      Vector<EvalIdl.CEvalRecordPo> evalInfolist;

      @Field(desc="总数")
      uint32_t totalnum;

      @Field(desc="保留输出字段")
      String reserveOut;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x23151802L", desc="获取评价列表-请求，按商品id查时，需要提供当期时间")
    class Req
    {

      @Field(desc="调用者==>请设置为源文件名")
      String source;

      @Field(desc="请求过滤器")
      EvalIdl.CEvalListReqPo ofilter;

      @Field(desc="用户IP")
      String userIP;

      @Field(desc="用户MachineKey")
      String userMachineKey;

      @Field(desc="保留输入字段")
      String reserveIn;

      Req()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x23151801L", desc="做评接口")
  class MakeEvalList
  {
    MakeEvalList()
    {
    }

    @ApiProtocol(cmdid="0x23158801L", desc="做评返回")
    class Resp
    {

      @Field(desc="订单id")
      Vector<EvalIdl.EvalRet> RetList;

      @Field(desc="保留输出字段")
      String reserveOut;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x23151801L", desc="做评请求")
    class Req
    {

      @Field(desc="调用者==>请设置为源文件名")
      String source;

      @Field(desc="待做评列表，大订单列表")
      Vector<String> dealidlist;

      @Field(desc="评价等级")
      uint32_t evallevel;

      @Field(desc="评价内容")
      String evalcontent;

      @Field(desc="用户IP")
      String userIP;

      @Field(desc="用户MachineKey")
      String userMachineKey;

      @Field(desc="保留输入字段")
      String reserveIn;

      Req()
      {
      }
    }
  }
}

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.eval.ao.idl.EvalIdl
 * JD-Core Version:    0.6.2
 */