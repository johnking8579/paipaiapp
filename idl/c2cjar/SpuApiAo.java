package com.paipai.c2c.api.spu.ao.idl;

import com.paipai.c2c.api.item.ao.idl.ItemApiAo.ApiItem;
import com.paipai.c2c.api.shop.ao.idl.ShopApiAo.ApiShopInfo;
import com.paipai.c2c.api.user.ao.idl.UserApiAo.APIUserProfile;
import com.paipai.lang.uint32_t;
import com.paipai.lang.uint64_t;
import com.paipai.lang.uint8_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;
import java.util.Set;
import java.util.Vector;

@HeadApiProtocol(cPlusNamespace="c2cent::ao::spu", needInit=true)
public class SpuApiAo
{
  @ApiProtocol(cmdid="0x54001801L", desc="查找Spu商品")
  class ApiFindSpuList
  {
    ApiFindSpuList()
    {
    }

    @ApiProtocol(cmdid="0x54008801L", desc="查找Spu商品返回类")
    class Resp
    {

      @Field(desc="Spu维度相关信息")
      SpuApiAo.ApiSpuDimension spuDimension;

      @Field(desc="错误信息,用于debug")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x54001801L", desc="查找Spu商品请求类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="预留")
      uint32_t scene;

      @Field(desc="Spu查找过滤器")
      SpuApiAo.ApiSpuFilter spuFilter;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }
  }

  @Member(desc="Spu维度", cPlusNamespace="c2cent::po::spu", isNeedUFlag=true)
  class ApiSpuDimension
  {

    @Field(desc="版本", defaultValue="20100805")
    uint32_t version;

    @Field(desc="商品总数")
    uint32_t totalItemNum;

    @Field(desc="用户总数")
    uint32_t totalUserNum;

    @Field(desc="总库存数")
    uint32_t totalStockNum;

    @Field(desc="最低价")
    uint32_t minPrice;

    @Field(desc="最高价")
    uint32_t maxPrice;

    @Field(desc="Spu单元列表")
    Vector<SpuApiAo.ApiSpuCell> spuCellList;

    @Field(desc="保留字")
    String reserve;
    uint8_t version_u;
    uint8_t totalItemNum_u;
    uint8_t totalUserNum_u;
    uint8_t totalStockNum_u;
    uint8_t minPrice_u;
    uint8_t maxPrice_u;
    uint8_t spuCellList_u;
    uint8_t reserve_u;

    ApiSpuDimension()
    {
    }
  }

  @Member(desc="Spu单元", cPlusNamespace="c2cent::po::spu", isNeedUFlag=true)
  class ApiSpuCell
  {

    @Field(desc="版本", defaultValue="20100805")
    uint32_t version;

    @Field(desc="商品po")
    ItemApiAo.ApiItem itemPo;

    @Field(desc="店铺po")
    ShopApiAo.ApiShopInfo shopPo;

    @Field(desc="用户po")
    UserApiAo.APIUserProfile userPo;
    uint8_t version_u;
    uint8_t itemPo_u;
    uint8_t shopPo_u;
    uint8_t userPo_u;

    ApiSpuCell()
    {
    }
  }

  @Member(desc="Spu查找过滤器", cPlusNamespace="c2cent::po::spu", isNeedUFlag=true)
  class ApiSpuFilter
  {

    @Field(desc="版本号", defaultValue="20100805")
    uint32_t version;

    @Field(desc="spuid,必填")
    uint64_t spuId;

    @Field(desc="卖家uin,全量查询可以不填")
    uint32_t sellerUin;

    @Field(desc="库存属性1,可选")
    String stockAttr1;

    @Field(desc="库存属性2,可选")
    String stockAttr2;

    @Field(desc="库存属性3,可选")
    String stockAttr3;

    @Field(desc="页号,从0开始,必填")
    uint32_t page;

    @Field(desc="每页大小,最大40,必填")
    uint32_t pageSize;

    @Field(desc="排序方式，请参考spu常量类,必填")
    uint32_t order;

    @Field(desc="1表示库存属性完整,0表示不完整 ,必填")
    uint32_t isStockComplete;

    @Field(desc="卖家类型,必填（具体设置见SpuConstants.java）")
    Set<uint32_t> sellerType;

    @Field(desc="商品状态,必填 具体设置见SpuConstants.java")
    uint32_t state;

    @Field(desc="属性")
    Set<uint32_t> property;

    @Field(desc="新旧类型")
    uint32_t newtype;

    @Field(desc="省份,可选")
    uint32_t province;

    @Field(desc="城市,可选")
    uint32_t city;

    @Field(desc="保留字")
    String reserve;
    uint8_t version_u;
    uint8_t spuId_u;
    uint8_t sellerUin_u;
    uint8_t stockAttr1_u;
    uint8_t stockAttr2_u;
    uint8_t stockAttr3_u;
    uint8_t page_u;
    uint8_t pageSize_u;
    uint8_t order_u;
    uint8_t isStockComplete_u;
    uint8_t sellerType_u;
    uint8_t state_u;
    uint8_t property_u;
    uint8_t newtype_u;
    uint8_t province_u;
    uint8_t city_u;
    uint8_t reserve_u;

    ApiSpuFilter()
    {
    }
  }
}

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.spu.ao.idl.SpuApiAo
 * JD-Core Version:    0.6.2
 */