package com.paipai.c2c.api.dimension.ao.idl;

import com.paipai.lang.MultiMap;
import com.paipai.lang.uint32_t;
import com.paipai.lang.uint8_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;
import java.util.Vector;

@HeadApiProtocol(cPlusNamespace="c2cent::ao::dimension", needInit=true)
public class DimensionApiAo
{
  @ApiProtocol(cmdid="0x90221809L", desc="删除尺码对照表")
  class ApiDeleteSizeTable
  {
    ApiDeleteSizeTable()
    {
    }

    @ApiProtocol(cmdid="0x90228809L", desc="删除尺码对照表返回类")
    class Resp
    {

      @Field(desc="错误信息，用于debug")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x90221809L", desc="删除尺码对照表请求类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="卖家QQ")
      uint32_t uin;

      @Field(desc="尺码对照表id")
      uint32_t sizeTableId;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x90221808L", desc="更新尺码对照表")
  class ApiUpdateSizeTable
  {
    ApiUpdateSizeTable()
    {
    }

    @ApiProtocol(cmdid="0x90228808L", desc="更新尺码对照表返回类")
    class Resp
    {

      @Field(desc="错误信息，用于debug")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x90221808L", desc="更新尺码对照表请求类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="卖家QQ")
      uint32_t uin;

      @Field(desc="尺码对照表")
      DimensionApiAo.ApiSizeTableInfo sizeTableInfo;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x90221807L", desc="增加尺码对照表")
  class ApiAddSizeTable
  {
    ApiAddSizeTable()
    {
    }

    @ApiProtocol(cmdid="0x90228807L", desc="增加尺码对照表返回类")
    class Resp
    {

      @Field(desc="尺码对照表id")
      uint32_t sizeTableId;

      @Field(desc="错误信息，用于debug")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x90221807L", desc="增加尺码对照表请求类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="卖家QQ")
      uint32_t uin;

      @Field(desc="尺码对照表")
      DimensionApiAo.ApiSizeTableInfo sizeTableInfo;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x90221806L", desc="获取尺码对照表(无需登录)")
  class ApiGetSizeTable
  {
    ApiGetSizeTable()
    {
    }

    @ApiProtocol(cmdid="0x90228806L", desc="获取尺码对照表返回类")
    class Resp
    {

      @Field(desc="尺码对照表Po")
      DimensionApiAo.ApiSizeTableInfo sizeTemplate;

      @Field(desc="错误信息，用于debug")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x90221806L", desc="获取尺码对照表请求类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="卖家QQ")
      uint32_t uin;

      @Field(desc="尺码对照表id")
      uint32_t sizeTableId;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x90221805L", desc="获取尺码对照表列表")
  class ApiGetSizeTableList
  {
    ApiGetSizeTableList()
    {
    }

    @ApiProtocol(cmdid="0x90228805L", desc="获取尺码对照表列表返回类")
    class Resp
    {

      @Field(desc="总数")
      uint32_t totalCount;

      @Field(desc="尺码对照表列表")
      Vector<DimensionApiAo.ApiSizeTableInfo> sizeTemplateList;

      @Field(desc="错误信息，用于debug")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x90221805L", desc="获取尺码对照表列表请求类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="卖家QQ")
      uint32_t uin;

      @Field(desc="类目id(如果需要获取全部尺码对照表，则值为0)")
      uint32_t classid;

      @Field(desc="页码 从0开始")
      uint32_t startIndex;

      @Field(desc="每页个数（注：最多40）")
      uint32_t pageSize;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x90221804L", desc="获取尺码对照表数量")
  class ApiGetSizeTableCount
  {
    ApiGetSizeTableCount()
    {
    }

    @ApiProtocol(cmdid="0x90228804L", desc="获取尺码对照表数量返回类")
    class Resp
    {

      @Field(desc="总数")
      uint32_t totalCount;

      @Field(desc="错误信息，用于debug")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x90221804L", desc="获取尺码对照表数量请求类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="卖家QQ号")
      uint32_t uin;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }
  }

  @Member(desc="<h1>尺码对照表Po</h1><br/>这里主要描述一下尺码对照表ruleList的格式<br/>格式应该严格遵循：<br/>情况1:尺码#cm^高度#cm^宽度#cm|L^xxx/180^85/100|M^160/xxx^70/85<br/>情况2:尺码#^高度#cm^宽度#cm|L^xxx/180^85/100|M^160/xxx^70/85<br/>", cPlusNamespace="c2cent::po::dimension", isNeedUFlag=true)
  class ApiSizeTableInfo
  {

    @Field(desc="版本", defaultValue="0")
    uint32_t version;

    @Field(desc="QQ号 (预留,暂时不用)")
    uint32_t uin;

    @Field(desc="尺码对照表id")
    uint32_t sizeTableId;

    @Field(desc="类目id (预留,暂时不用)")
    uint32_t leafClassId;

    @Field(desc="属性 (预留,暂时不用)")
    uint32_t property;

    @Field(desc="创建时间")
    uint32_t createTime;

    @Field(desc="最后更新时间")
    uint32_t lastModifyTime;

    @Field(desc="最后使用时间")
    uint32_t lastUseTime;

    @Field(desc="使用次数 (预留,暂时不用)")
    uint32_t useTimes;

    @Field(desc="模板id")
    uint32_t templateId;

    @Field(desc="模板名")
    String templateName;

    @Field(desc="尺码对照表名称")
    String name;

    @Field(desc="尺码对照表描述信息")
    String desc;

    @Field(desc="规则,见类描述信息")
    String ruleList;

    @Field(desc="示意图url")
    String pictureURL;

    @Field(desc="类目id(多个 用;分隔)")
    String classId;

    @Field(desc="预留 (预留,暂时不用)")
    MultiMap<String, String> ext;

    @Field(desc="保留字段")
    String reserve;
    uint8_t version_u;
    uint8_t uin_u;
    uint8_t sizeTableId_u;
    uint8_t leafClassId_u;
    uint8_t property_u;
    uint8_t createTime_u;
    uint8_t lastModifyTime_u;
    uint8_t lastUseTime_u;
    uint8_t useTimes_u;
    uint8_t templateId_u;
    uint8_t templateName_u;
    uint8_t name_u;
    uint8_t desc_u;
    uint8_t ruleList_u;
    uint8_t pictureURL_u;
    uint8_t classId_u;
    uint8_t ext_u;
    uint8_t reserve_u;

    ApiSizeTableInfo()
    {
    }
  }
}

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.dimension.ao.idl.DimensionApiAo
 * JD-Core Version:    0.6.2
 */