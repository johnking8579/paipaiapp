package com.paipai.c2c.api.shop.ao.idl;

import com.paipai.lang.uint32_t;
import com.paipai.lang.uint8_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;
import java.util.Vector;

@HeadApiProtocol(cPlusNamespace="c2cent::ao::shopcategory", needInit=true)
public class ShopCategoryApiAo
{
  @ApiProtocol(cmdid="0x20031803L", desc="增加自定义分类")
  class ApiAddShopCategory
  {
    ApiAddShopCategory()
    {
    }

    @ApiProtocol(cmdid="0x20031803L", desc="增加自定义分类请求类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源,必填")
      String source;

      @Field(desc="场景id,预留")
      uint32_t scene;

      @Field(desc="店主Uin")
      uint32_t uin;

      @Field(desc="自定义分类")
      ShopCategoryApiAo.ApiShopCategory apiShopCategory;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x20038803L", desc="增加自定义分类返回类")
    class Resp
    {

      @Field(desc="错误信息,用于debug")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x20031804L", desc="删除自定义分类")
  class ApiDeleteShopCategory
  {
    ApiDeleteShopCategory()
    {
    }

    @ApiProtocol(cmdid="0x20031804L", desc="删除自定义分类请求类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源,必填")
      String source;

      @Field(desc="场景id,预留")
      uint32_t scene;

      @Field(desc="店主Uin")
      uint32_t uin;

      @Field(desc="自定义分类id")
      String categoryId;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x20038804L", desc="删除自定义分类返回类")
    class Resp
    {

      @Field(desc="错误信息,用于debug")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x20031801L", desc="获取店铺自定义分类")
  class ApiGetShopCategoryListByUin
  {
    ApiGetShopCategoryListByUin()
    {
    }

    @ApiProtocol(cmdid="0x20031801L", desc="获取店铺自定义分类请求类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源,必填")
      String source;

      @Field(desc="场景id,预留")
      uint32_t scene;

      @Field(desc="店主Uin")
      uint32_t uin;

      @Field(desc="每页大小,最多20")
      uint32_t pageSize;

      @Field(desc="页号,从0开始")
      uint32_t startIndex;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x20038801L", desc="获取店铺自定义分类返回类")
    class Resp
    {

      @Field(desc="店铺自定义分类列表", cPlusNamespace="ApiShopActionInfoPo|c2cent::po::shopcategory")
      Vector<ShopCategoryApiAo.ApiShopCategory> apiShopCategoryList;

      @Field(desc="总数")
      uint32_t totalSize;

      @Field(desc="错误信息,用于debug")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }
  }

  @Member(desc="店铺自定义分类信息po", cPlusNamespace="c2cent::po::shopcategory", isNeedUFlag=true)
  class ApiShopCategory
  {

    @Field(desc="版本号", defaultValue="1")
    uint32_t version;

    @Field(desc="自定义分类id")
    String categoryId;

    @Field(desc="自定义分类名称")
    String desc;

    @Field(desc="图片url")
    String picUrl;

    @Field(desc="父自定义分类id,如果是一级分类，则为空")
    String parentId;

    @Field(desc="店铺id")
    uint32_t shopId;

    @Field(desc="序号")
    uint32_t sequence;

    @Field(desc="该类别的商品数")
    uint32_t commNum;
    uint8_t version_u;
    uint8_t categoryId_u;
    uint8_t shopId_u;
    uint8_t sequence_u;
    uint8_t desc_u;
    uint8_t commNum_u;
    uint8_t picUrl_u;
    uint8_t parentId_u;

    ApiShopCategory()
    {
    }
  }

  @ApiProtocol(cmdid="0x20031802L", desc="修改店铺自定义分类")
  class ApiUpdateShopCategory
  {
    ApiUpdateShopCategory()
    {
    }

    @ApiProtocol(cmdid="0x20031802L", desc="修改店铺自定义分类类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源,必填")
      String source;

      @Field(desc="场景id,预留")
      uint32_t scene;

      @Field(desc="店主Uin")
      uint32_t uin;

      @Field(desc="新自定义分类")
      ShopCategoryApiAo.ApiShopCategory apiShopCategory;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x20038802L", desc="修改店铺自定义分类返回类")
    class Resp
    {

      @Field(desc="错误信息,用于debug")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }
  }
}

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.shop.ao.idl.ShopCategoryApiAo
 * JD-Core Version:    0.6.2
 */