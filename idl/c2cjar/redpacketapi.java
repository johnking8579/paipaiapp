package com.paipai.c2c.api.redpacket.ao.idl;

import com.paipai.lang.uint32_t;
import com.paipai.lang.uint64_t;
import com.paipai.lang.uint8_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;
import java.util.Set;
import java.util.Vector;

@HeadApiProtocol(cPlusNamespace="c2cent::ao::redpacketapi")
public class redpacketapi
{
  @ApiProtocol(cmdid="0x91111801L", desc="获取红包列表")
  class GetRedPacketList
  {
    GetRedPacketList()
    {
    }

    @ApiProtocol(cmdid="0x91111801L", desc="获取红包列表")
    class Req
    {

      @Field(desc="调用者==>请设置为源文件名")
      String Source;

      @Field(desc="场景id")
      uint32_t SceneId;

      @Field(desc="请求filter")
      redpacketapi.RedPacketFilter oFilter;

      @Field(desc="用户MachineKey")
      String MachineKey;

      @Field(desc="保留输入字段")
      String ReserveIn;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x91118801L", desc="获取红包列表返回")
    class Resp
    {

      @Field(desc="订单列表")
      Vector<redpacketapi.RedPacket> RedPacketList;

      @Field(desc="符合条件总数")
      uint32_t TotalNum;

      @Field(desc="错误信息")
      String ErrMsg;

      @Field(desc="保留输出字段")
      String ReserveOut;

      Resp()
      {
      }
    }
  }

  @Member(cPlusNamespace="c2cent::po::redpacketapi", desc="红包列表", isSetClassSize=true, isNeedUFlag=true, version=0)
  class RedPacket
  {

    @Field(desc="版本号")
    uint32_t Version;
    uint8_t Version_u;

    @Field(desc="红包id")
    uint32_t PacketId;
    uint8_t PacketId_u;

    @Field(desc="红包名称")
    String PacketName;
    uint8_t PacketName_u;

    @Field(desc="红包类型,红包(1)，店铺代金券(2)")
    uint32_t Type;
    uint8_t Type_u;

    @Field(desc="红包批次id")
    uint64_t PacketStockId;
    uint8_t PacketStockId_u;

    @Field(desc="所属者")
    uint32_t OwnerUin;
    uint8_t OwnerUin_u;

    @Field(desc="面值")
    uint32_t PacketPrice;
    uint8_t PacketPrice_u;

    @Field(desc="红包标识")
    uint32_t PacketFlag;
    uint8_t PacketFlag_u;

    @Field(desc="对应卖家号")
    uint32_t SellerUin;
    uint8_t SellerUin_u;

    @Field(desc="起始时间")
    uint32_t BeginTime;
    uint8_t BeginTime_u;

    @Field(desc="结束时间")
    uint32_t EndTime;
    uint8_t EndTime_u;

    @Field(desc="领取ip")
    String Ip;
    uint8_t Ip_u;

    @Field(desc="领取时间")
    uint32_t RecvTime;
    uint8_t RecvTime_u;

    @Field(desc="使用时间")
    uint32_t UseTime;
    uint8_t UseTime_u;

    @Field(desc="红包状态,待使用(100),使用中--绑定和冻结(101)，已使用(102)")
    uint32_t State;
    uint8_t State_u;

    @Field(desc="绑定的订单id")
    String DealCode;
    uint8_t DealCode_u;

    @Field(desc="关联url")
    String RelaUrl;
    uint8_t RelaUrl_u;

    @Field(desc="图片url")
    String ImageUrl;
    uint8_t ImageUrl_u;

    @Field(desc="实际抵扣金额")
    uint32_t ActualPrice;
    uint8_t ActualPrice_u;

    @Field(desc="兑现编号")
    uint64_t WithdrawId;
    uint8_t WithdrawId_u;

    @Field(desc="兑现时间")
    uint32_t WithdrawTime;
    uint8_t WithdrawTime_u;

    @Field(desc="适用店铺")
    Set<uint32_t> ApplicableScope;
    uint8_t ApplicableScope_u;

    RedPacket()
    {
    }
  }

  @Member(cPlusNamespace="c2cent::po::redpacketapi", desc="请求filter", isSetClassSize=true, isNeedUFlag=true, version=0)
  class RedPacketFilter
  {

    @Field(desc="版本号")
    uint32_t Version;
    uint8_t Version_u;

    @Field(desc="红包名称")
    String PacketName;
    uint8_t PacketName_u;

    @Field(desc="红包批次id")
    uint64_t PacketStockId;
    uint8_t PacketStockId_u;

    @Field(desc="所属者")
    uint32_t OwnerUin;
    uint8_t OwnerUin_u;

    @Field(desc="对应卖家号")
    uint32_t SellerUin;
    uint8_t SellerUin_u;

    @Field(desc="红包状态,待使用(100),使用中--绑定和冻结(101)，已使用(102),过期(2)")
    uint32_t State;
    uint8_t State_u;

    @Field(desc="绑定的订单id")
    String DealCode;
    uint8_t DealCode_u;

    @Field(desc="兑现编号")
    uint64_t WithdrawId;
    uint8_t WithdrawId_u;

    @Field(desc="最大面值")
    uint32_t MaxFaceValue;
    uint8_t MaxFaceValue_u;

    @Field(desc="适用范围")
    Set<uint32_t> ApplicableScope;
    uint8_t ApplicableScope_u;

    @Field(desc="红包id列表")
    Vector<uint64_t> PacketIdList;
    uint8_t PacketIdList_u;

    @Field(desc="红包类型,红包(1)，店铺代金券(2)")
    uint32_t Type;
    uint8_t Type_u;

    @Field(desc="拉取信息类型，基本信息(0x01),批次(0x02),兑现信息(0x04),详细信息(0x08),代金券购买(0x1)")
    uint32_t Info;
    uint8_t Info_u;

    RedPacketFilter()
    {
    }
  }
}

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.redpacket.ao.idl.redpacketapi
 * JD-Core Version:    0.6.2
 */