/*    */ package com.paipai.c2c.api.shop.ao.idl;
/*    */ 
/*    */ import com.paipai.c2c.api.util.VBossChecker;
/*    */ import com.paipai.lang.uint32_t;
/*    */ import com.paipai.lang.uint64_t;
/*    */ import com.paipai.lang.uint8_t;
/*    */ import com.paipai.util.annotation.ApiProtocol;
/*    */ import com.paipai.util.annotation.Field;
/*    */ import com.paipai.util.annotation.HeadApiProtocol;
/*    */ import com.paipai.util.annotation.Member;
/*    */ import java.util.Map;
/*    */ 
/*    */ @HeadApiProtocol(cPlusNamespace="c2cent::ao::shopboss", needInit=true, needReset=true)
/*    */ public class ShopBossApiAo
/*    */ {
/*    */   @Member(desc="店铺模块审核PO", cPlusNamespace="c2cent::po::shop", isNeedUFlag=true)
/*    */   public class ApiShopAuditInfo
/*    */   {
/*    */ 
/*    */     @Field(desc="版本号", defaultValue="20101222")
/*    */     uint32_t poVersion;
/*    */ 
/*    */     @Field(desc="店铺id，即qq号")
/*    */     uint32_t shopID;
/*    */ 
/*    */     @Field(desc="所有模块审核通过标记，1: 所有模块审核通过，0:通过auditfsid的map里面的信息来进行审核")
/*    */     uint32_t allPassed;
/*    */ 
/*    */     @Field(desc="审核信息，Map->first: 模块标识 Map->second: 模块审核状态[0:审核通过;1:审核不通过]")
/*    */     Map<uint64_t, uint32_t> auditfsid;
/*    */ 
/*    */     @Field(desc="预留字段")
/* 54 */     String reserve = "";
/*    */     uint8_t poVersion_u;
/*    */     uint8_t shopID_u;
/*    */     uint8_t allPassed_u;
/*    */     uint8_t auditfsid_u;
/*    */     uint8_t reserve_u;
/*    */ 
/*    */     public ApiShopAuditInfo()
/*    */     {
/*    */     }
/*    */   }
/*    */ 
/*    */   @ApiProtocol(cmdid="0x30191801L", desc="更新店铺模块审核信息")
/*    */   class ApiUpdateShopAuditInfo
/*    */   {
/*    */     ApiUpdateShopAuditInfo()
/*    */     {
/*    */     }
/*    */ 
/*    */     @ApiProtocol(cmdid="0x30191801L", desc="更新店铺模块审核信息请求类")
/*    */     class Req
/*    */     {
/*    */ 
/*    */       @Field(desc="机器码")
/*    */       String machineKey;
/*    */ 
/*    */       @Field(desc="来源,必填")
/*    */       String source;
/*    */ 
/*    */       @Field(desc="Boss合法性检查", cPlusNamespace="VBossChecker|c2cent::po::boss", header="VBossChecker|c2cent/po/boss/boss_vcheck_po.h")
/*    */       VBossChecker bossChecker;
/*    */ 
/*    */       @Field(desc="场景id")
/*    */       uint32_t scene;
/*    */ 
/*    */       @Field(desc="店铺已审核信息", cPlusNamespace="ApiShopAuditInfo|c2cent::po::shop")
/*    */       ShopBossApiAo.ApiShopAuditInfo shopauditinfo;
/*    */ 
/*    */       @Field(desc="请求保留字")
/*    */       String inReserve;
/*    */ 
/*    */       Req()
/*    */       {
/*    */       }
/*    */     }
/*    */ 
/*    */     @ApiProtocol(cmdid="0x30198801L", desc="更新店铺模块审核信息返回类")
/*    */     class Resp
/*    */     {
/*    */ 
/*    */       @Field(desc="错误信息,用于debug")
/*    */       String errmsg;
/*    */ 
/*    */       @Field(desc="返回保留字")
/*    */       String outReserve;
/*    */ 
/*    */       Resp()
/*    */       {
/*    */       }
/*    */     }
/*    */   }
/*    */ 
/*    */   @Member(desc="店铺模块变更消息PO", cPlusNamespace="c2cent::po::shop", isNeedUFlag=true)
/*    */   public class ShopModuleAlter
/*    */   {
/*    */ 
/*    */     @Field(desc="版本号", defaultValue="20101227")
/*    */     uint32_t poVersion;
/*    */ 
/*    */     @Field(desc="店铺id，即qq号")
/*    */     uint32_t shopID;
/*    */ 
/*    */     @Field(desc="预留字段")
/* 31 */     String reserve = "";
/*    */     uint8_t poVersion_u;
/*    */     uint8_t shopID_u;
/*    */     uint8_t reserve_u;
/*    */ 
/*    */     public ShopModuleAlter()
/*    */     {
/*    */     }
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.shop.ao.idl.ShopBossApiAo
 * JD-Core Version:    0.6.2
 */