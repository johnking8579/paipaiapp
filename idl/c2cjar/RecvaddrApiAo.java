package com.paipai.c2c.api.recvadr.ao.idl;

import com.paipai.lang.uint32_t;
import com.paipai.lang.uint8_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;
import java.util.Vector;

@HeadApiProtocol(cPlusNamespace="c2cent::ao::recvaddr", needInit=true)
public class RecvaddrApiAo
{
  @ApiProtocol(cmdid="0x22121802L", desc="获取收货地址列表")
  class ApiGetRecvaddrList
  {
    ApiGetRecvaddrList()
    {
    }

    @ApiProtocol(cmdid="0x22128802L", desc="获取收货地址列表返回类")
    class Resp
    {

      @Field(desc="收货地址Po列表")
      Vector<RecvaddrApiAo.ApiRecvaddr> vecApiRecvaddr;

      @Field(desc="总数")
      uint32_t totalCount;

      @Field(desc="错误信息，用于debug")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x22121802L", desc="获取收货地址列表请求类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="场景id,预留")
      uint32_t scene;

      @Field(desc="收货地址所属卖家QQ")
      uint32_t uin;

      @Field(desc="收货地址类型")
      uint32_t addrType;

      @Field(desc="每页大小 (预留,目前没用到)")
      uint32_t pageSize;

      @Field(desc="页号 (预留,目前没用到)")
      uint32_t startIndex;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x22121801L", desc="获取单个收货地址")
  class ApiGetRecvaddr
  {
    ApiGetRecvaddr()
    {
    }

    @ApiProtocol(cmdid="0x22128801L", desc="获取单个收货地址返回类")
    class Resp
    {

      @Field(desc="收货地址Po")
      RecvaddrApiAo.ApiRecvaddr apiRecvaddr;

      @Field(desc="错误信息，用于debug")
      String errmsg;

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x22121801L", desc="获取单个收货地址请求类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源,不能为空")
      String source;

      @Field(desc="场景id,预留")
      uint32_t scene;

      @Field(desc="收货地址id")
      uint32_t addrId;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }
  }

  @Member(desc="<h1>收货地址Po</h1><br/>", cPlusNamespace="c2cent::po::recvaddr", isNeedUFlag=true)
  class ApiRecvaddr
  {

    @Field(desc="版本", defaultValue="1")
    uint32_t version;

    @Field(desc="收货地址ID)")
    uint32_t addrId;

    @Field(desc="QQ号码")
    uint32_t uin;

    @Field(desc="地址ID")
    uint32_t regionId;

    @Field(desc="收获地址类型")
    uint32_t addrType;

    @Field(desc="最近一次使用时间")
    uint32_t lastUsedTime;

    @Field(desc="最近修改时间")
    uint32_t lastModifyTime;

    @Field(desc="使用次数")
    uint32_t usedCount;

    @Field(desc="接收者的姓名")
    String recvName;

    @Field(desc="邮编")
    String postCode;

    @Field(desc="详细收货地址")
    String recvAddress;

    @Field(desc="电话")
    String recvPhone;

    @Field(desc="手机")
    String recvMobile;
    uint8_t version_u;
    uint8_t addrId_u;
    uint8_t uin_u;
    uint8_t regionId_u;
    uint8_t addrType_u;
    uint8_t lastUsedTime_u;
    uint8_t lastModifyTime_u;
    uint8_t usedCount_u;
    uint8_t recvName_u;
    uint8_t postCode_u;
    uint8_t recvAddress_u;
    uint8_t recvPhone_u;
    uint8_t recvMobile_u;

    ApiRecvaddr()
    {
    }
  }
}

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.recvadr.ao.idl.RecvaddrApiAo
 * JD-Core Version:    0.6.2
 */