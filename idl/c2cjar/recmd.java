package com.paipai.c2c.api.recmd.ao.idl;

import com.paipai.lang.uint32_t;
import com.paipai.lang.uint8_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;
import java.util.Vector;

@HeadApiProtocol(cPlusNamespace="c2cent::ao::recmd")
public class recmd
{
  @Member(cPlusNamespace="c2cent::po::recmd", desc="关联推荐po", isNeedUFlag=true, isSetClassSize=true, version=0)
  class CRecmdResp
  {

    @Field(desc="版本号")
    uint32_t Version;

    @Field(desc="卖家号")
    uint32_t SellerUin;

    @Field(desc="商品单价")
    uint32_t Price;

    @Field(desc="商品属性")
    uint32_t Property;

    @Field(desc="商品id")
    String ComdyId;

    @Field(desc="商品名称")
    String Title;

    @Field(desc="图片url")
    String LogoUrl;

    @Field(desc="保留字段")
    String Reserve;
    uint8_t Version_u;
    uint8_t SellerUin_u;
    uint8_t Price_u;
    uint8_t Property_u;
    uint8_t ComdyId_u;
    uint8_t Title_u;
    uint8_t LogoUrl_u;
    uint8_t Reserve_u;

    CRecmdResp()
    {
    }
  }

  @ApiProtocol(cmdid="0x91101801L", desc="购买推荐")
  class BuyComdyRecmd
  {
    BuyComdyRecmd()
    {
    }

    @ApiProtocol(cmdid="0x91108801L", desc="购买返回")
    class Resp
    {

      @Field(desc="推荐商品列表")
      Vector<recmd.CRecmdResp> oData;

      @Field(desc="保留输出字段")
      String reserveOut;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x91101801L", desc="购买请求")
    class Req
    {

      @Field(desc="请求者qq uin")
      uint32_t Uin;

      @Field(desc="type")
      uint32_t Type;

      @Field(desc="itemid")
      String ItemId;

      @Field(desc="用户MachineKey")
      String userMachineKey;

      @Field(desc="调用者==>请设置为源文件名")
      String source;

      @Field(desc="保留输入字段")
      String reserveIn;

      Req()
      {
      }
    }
  }
}

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.recmd.ao.idl.recmd
 * JD-Core Version:    0.6.2
 */