package source;

import java.util.Map;
import java.util.Vector;

import com.paipai.lang.uint8_t;
import com.paipai.lang.uint16_t;
import com.paipai.lang.uint32_t;
import com.paipai.lang.uint64_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;


/**
 * ao_verifycode
 * @author stonenie
 *
 */
@HeadApiProtocol(cPlusNamespace = "c2cent::ao::verifycode", needInit = true)
public class VerifyCodeAo {
					
	@ApiProtocol(cmdid = "0x10551801L", desc = "judge if has passed the mobile verify process")
	class JudgeVerifyCode {
		@ApiProtocol(cmdid = "0x10551801L", desc = "judge if has passed the mobile verify process Req")
		class Req {						
			@Field(desc = "macheine key")
			String machineKey;

			@Field(desc = "source")
			String source;		

			@Field(desc = "uin")
			uint32_t uin;

			@Field(desc = "mobile number")
			String mobile;
						
			@Field(desc = "verify code, set null if have no value")
			String verifyCode;	
			
			@Field(desc = "reserve req")
			String inReserve;																		
		}

		@ApiProtocol(cmdid = "0x10558801L", desc = "judge if has passed the mobile verify process Resp")
		class Resp {
			@Field(desc = "verify result, 0-not define, 1-not pass, 2-passed")
			uint8_t verifyResult;			
			
			@Field(desc = "error message")
			String errMsg;
									
			@Field(desc = "reserve resp")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0x10551802L", desc = "create verify code and send short message")
	class CreateVerifyCodeAndSendSms {
		@ApiProtocol(cmdid = "0x10551802L", desc = "create verify code and send short message req")
		class Req {						
			@Field(desc = "machine key")
			String machineKey;

			@Field(desc = "source")
			String source;

			@Field(desc = "scene id, reserved")
			uint32_t sceneId;
			
			@Field(desc = "uin")
			uint32_t uin;

			@Field(desc = "mobile number")
			String mobile;
						
			@Field(desc = "reserve req")
			String inReserve;														
		}

		@ApiProtocol(cmdid = "0x10558802L", desc = "create verify code and send short message resp")
		class Resp {
			@Field(desc = "error message")
			String errMsg;
									
			@Field(desc = "reserve resp")
			String outReserve;
		}
	}
		
}
