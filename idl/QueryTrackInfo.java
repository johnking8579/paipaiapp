package qqdeliver.ao;


import java.util.Vector;

import com.paipai.lang.uint32_t;
import com.paipai.lang.uint64_t;
import com.paipai.lang.uint8_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;

/**
 * QQ速递的查件服务定义
 * @author bingbingzi
 *
 */
@HeadApiProtocol(cPlusNamespace = "qqdeliver::ao", needInit=true)
public class QueryTrackInfo {
	
	/**
	 * 该接口为速递查件提供的查询宽接口
	 */
	@ApiProtocol(cmdid = "0x1fb31809L", desc = "查物流跟踪信息")
	class RetrieveTrackInfo {
		@ApiProtocol(cmdid = "0x1fb31809L", desc = "查物流跟踪信息 req")
		class Req {
			@Field(desc = "用户帐号,选填，40字符内")
			String UserId;
			@Field(desc = "快递运单号，必填")
			String DeliverId;
			@Field(desc = "快递公司编码,必填")
			String CompanyCode;
			@Field(desc = "速递订单Id，选填")
			uint64_t SudiOrderId;
			@Field(desc = "调用者来源，必填。新调用方请联系我们分配id号" +
					"：用于区分不同平台数据来源0 - all全部平台适用1 - QQ网购2 -拍拍3 -速递4 -微信5 - soso6 - QQ团购7 - 高朋8 - QQ腾爱")
			uint8_t Source;
			@Field(desc = "调用方提供的业务唯一性识别码。选填，" +
					"比如网购查询，可使用网购订单id做标识码； 如果前端页面查询，可使用页面唯一用户标识uid")
			String BizId;
			
			@Field(desc = "发货地址，选填，统计使用")
			SimpleAddressBo	SenderAddr;
			@Field(desc = "收货地址，选填，统计使用")
			SimpleAddressBo	ReceiverAddr;
		}
		@ApiProtocol(cmdid = "0x1fb38809L", desc = "查物流跟踪信息 resp")
		class Resp {
			@Field(desc = "物流跟踪信息数据")
			TrackInfoBo TraceInfoDo;
			@Field(desc = "用于存储debug信息")
			String RetMsg;
		}
	}
	

	
	@ApiProtocol(cmdid = "0x1fb31802L", desc = "获取查件记录列表 req")
	class GetQueryHistoryList {
		@ApiProtocol(cmdid = "0x1fb31802L", desc = "获取查件记录列表 req")
		class Req {
			@Field(desc = "用户帐号,选填，40字符内")
			String UserId;
			@Field(desc = "page index")
			uint32_t PageIndex;
			@Field(desc = "page size")
			uint32_t PageSize;
			@Field(desc = "调用者来源")
			uint8_t Source;
		}
		@ApiProtocol(cmdid = "0x1fb38802L", desc = "获取查件记录列表 resp")
		class Resp {
			@Field(desc = "查件记录列表", cPlusNamespace="QueryHistoryDo|qqdeliver::dao::ddo", header = "QueryHistoryDo|qqdeliver/dao/ddo/queryhistorydo_dao.h")
			Vector<QueryHistoryDo> QueryHistoryDoList;
			@Field(desc = "记录总数，用于前端分页")
			uint32_t TotalSize;
			@Field(desc = "用于存储debug信息")
			String RetMsg;
		}
	}

	@ApiProtocol(cmdid = "0x1fb31803L", desc = "删除一条查件记录")
	class DelQueryHistory {
		@ApiProtocol(cmdid = "0x1fb31803L", desc = "删除一条查件记录 req")
		class Req {
			@Field(desc = "用户帐号,选填，40字符内")
			String UserId;
			@Field(desc = "快递运单号，必填")
			String DeliverId;
			@Field(desc = "快递公司编码,必填")
			String CompanyCode;
			@Field(desc = "调用者来源")
			uint8_t Source;
		}
		@ApiProtocol(cmdid = "0x1fb38804L", desc = "获取查件记录列表 resp")
		class Resp {
			@Field(desc = "用于存储debug信息")
			String RetMsg;
		}
	}

	@Member(cPlusNamespace="qqdeliver::ao::bo", desc = "物流跟踪信息表DO", isNeedUFlag = true)
	public class TrackInfoBo{
		@Field(desc = "版本号")
		uint32_t	Version;
		
		@Field(desc = "记录ID")
		uint64_t	Id;
		@Field(desc = "运单号")
		String		DeliverId;
		@Field(desc = "用户帐号,选填，40字符内")
		String UserId;
		@Field(desc = "物流公司编码")
		String CompanyCode;;
		@Field(desc = "更新物流跟踪信息次数")
		uint32_t	Count;

		@Field(desc = "记录创建时间")
		uint32_t	CreateTime;
		@Field(desc = "揽件时间")
		uint32_t	PickupTime;
		@Field(desc = "派件时间")
		uint32_t	DeliveryTime;
		@Field(desc = "签收时间")
		uint32_t	SignTime;
		@Field(desc = "物流跟踪信息更新时间")
		uint32_t	UpdateTime;
		@Field(desc = "备注或失败信息")
		String		Remark;
		
		@Field(desc = "QQ速递订单ID")
		uint64_t	SudiOrderId;
		@Field(desc = "运单号绑定状态 ")
		uint32_t	Bind;
		
		@Field(desc = "调用者来源")
		uint8_t Source;
		@Field(desc = "调用者系统中业务Id")
		String BizId;
		
		@Field(desc = "发货地址")
		SimpleAddressBo	SenderAddr;
		@Field(desc = "收货地址")
		SimpleAddressBo	ReceiverAddr;		
		
		@Field(desc = "结构化包裹路由详情")
		Vector<TrackStep> Steps;
		@Field(desc = "字符串形式包裹路由详情")
		String	StepRemark;
		
		uint8_t Version_u;
		uint8_t	Id_u;
		uint8_t	DeliverId_u;
		uint8_t	Uin_u;
		uint8_t CompanyCode_u;
		uint8_t	Count_u;
		uint8_t	CreateTime_u;
		uint8_t	PickupTime_u;
		uint8_t	DeliveryTime_u;
		uint8_t	SignTime_u;
		uint8_t	UpdateTime_u;
		uint8_t	Remark_u;
		uint8_t	SudiOrderId_u;
		uint8_t	Bind_u;
		uint8_t Source_u;
		uint8_t BizId_u;
		uint8_t SenderAddr_u;
		uint8_t ReceiverAddr_u;
		uint8_t Steps_u;

	}
	@Member(cPlusNamespace="qqdeliver::ao::bo", desc = "简单地址Bo", isNeedUFlag = true)
	public class SimpleAddressBo{
		@Field(desc = "版本号")
		uint32_t	Version;
		
		@Field(desc = "发货人Id")
		String	UserId;
		@Field(desc = "发货地址")
		String	Address;
		@Field(desc = "发货地代码")
		uint32_t CityCode;
		
		uint8_t Version_u;
		uint8_t	UserId_u;
		uint8_t	Address_u;
		uint8_t	CityCode_u;
	}
	@Member(cPlusNamespace="qqdeliver::ao::bo", desc = "包裹跟踪详情步骤", isNeedUFlag = true)
	public class TrackStep{
		@Field(desc = "版本号")
		uint32_t	Version;
		
		@Field(desc = "时间")
		String	EventTime;
		@Field(desc = "step描述")
		String	Desc;
		
		uint8_t Version_u;
		uint8_t	EventTime_u;
		uint8_t	Desc_u;
	}
	
	@Member(cPlusNamespace="qqdeliver::dao::ddo", desc = "物流跟踪信息表DO")
	public class TraceInfoDo {
		@Field(desc = "id")
		uint64_t Id;
		@Field(desc = "运单号")
		String DeliverId;
		@Field(desc = "用户帐号,选填，40字符内")
		String UserId;
		@Field(desc = "物流公司编码")
		String CompanyCode;;
		@Field(desc = "更新次数")
		uint32_t Count;
		@Field(desc = "物流跟踪信息")
		String Trace_info;
		@Field(desc = "创建时间")
		String Createtime;
		@Field(desc = "更新时间")
		String Updatetime;
		@Field(desc = "速递单号")
		uint64_t Listid;
		@Field(desc = "是否绑定速递单号和运单号")
		uint32_t Bind;
		@Field(desc = "备注或出错信息")
		String Remark;
		@Field(desc = "保留字段1")
		String Standby1;
	}
	    
	@Member(cPlusNamespace="qqdeliver::dao::ddo", desc = "查件记录DO")
	public class QueryHistoryDo {
		@Field(desc = "id")
		uint64_t Id;
		@Field(desc = "运单号")
		String Deliver_id;
		@Field(desc = "用户帐号,选填，40字符内")
		String UserId;
		@Field(desc = "物流公司id")
		String CompanyCode;
		@Field(desc = "创建时间")
		String Createtime;
		@Field(desc = "更新时间")
		String Updatetime;
		@Field(desc = "查件次数")
		uint32_t Count;
		@Field(desc = "保留字段1")
		String Standby1;
	}

}
