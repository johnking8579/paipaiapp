package com.paipai.idl;

import java.util.List;
import java.util.Map;

import com.paipai.lang.uint32_t;
import com.paipai.lang.uint64_t;
import com.paipai.lang.uint16_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;

@HeadApiProtocol(cPlusNamespace = "c2cent::dao::statserv", needInit = true, needLib = false)
public class StatServDao {
	/**
	 * 获取留言的所有统计信息
	 * 
	 * @author jeanqiang
	 * 
	 * @date Aug 06, 2010
	 * @version 1.0
	 */
	@ApiProtocol(cmdid = "0x10501801L", desc = "获取留言的所有统计信息")
	class GetMsgBrdStatInfoByUin {
		/**
		 * 通过QQ号获取留言统计信息之请求
		 * 
		 * @author jeanqiang
		 * 
		 * @date Aug 06, 2010
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x10501801L", desc = "通过QQ号获取留言统计信息之请求")
		class Req {
			@Field(desc = "用户QQ号码")
			uint32_t Uin;
		}
		
		/**
		 * 通过QQ号获取留言统计信息之回复
		 * 
		 * @author jeanqiang
		 * 
		 * @date Aug 06, 2010
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x10508801L", desc = "通过QQ号获取留言统计信息之回复")
		class Resp {
			@Field(desc = "业务统计信息")
			Map<uint32_t,Integer> StatData;
		}
	}
	
	/**
	 * 获取订单的所有统计信息
	 * 
	 * @author jeanqiang
	 * 
	 * @date Sep 09, 2010
	 * @version 1.0
	 */
	@ApiProtocol(cmdid = "0x10511801L", desc = "获取订单的所有统计信息")
	class GetDealStatInfoByUin {
		/**
		 * 通过QQ号获取定单统计信息之请求
		 * 
		 * @author jeanqiang
		 * 
		 * @date Sep 09, 2010
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x10511801L", desc = "通过QQ号获取定单统计信息之请求")
		class Req {
			@Field(desc = "用户QQ号码")
			uint32_t Uin;
		}
		
		/**
		 * 通过QQ号获取定单统计信息之回复
		 * 
		 * @author jeanqiang
		 * 
		 * @date Sep 09, 2010
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x10518801L", desc = "通过QQ号获取定单统计信息之回复")
		class Resp {
			@Field(desc = "业务统计信息")
			Map<uint32_t,Integer> StatData;
		}
	}
	
	/**
	 * 获取订单的所有统计信息
	 * 
	 * @author jeanqiang
	 * 
	 * @date Sep 09, 2010
	 * @version 1.0
	 */
	@ApiProtocol(cmdid = "0x10511811L", desc = "获取订单的所有统计信息")
	class GetDealStatInfoByComdyId {
		/**
		 * 通过商品ID获取定单统计信息之请求
		 * 
		 * @author jeanqiang
		 * 
		 * @date Sep 09, 2010
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x10511811L", desc = "通过商品ID获取定单统计信息之请求")
		class Req {
			@Field(desc = "商品ID")
			uint64_t comdyId;
		}
		
		/**
		 * 通过商品ID获取定单统计信息之回复
		 * 
		 * @author jeanqiang
		 * 
		 * @date Sep 09, 2010
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x10518811L", desc = "通过商品ID获取定单统计信息之回复")
		class Resp {
			@Field(desc = "业务统计信息")
			Map<uint32_t,Integer> StatData;
		}
	}
	
	/**
	 * 设置订单的统计项
	 * 
	 * @author jeanqiang
	 * 
	 * @date Sep 09, 2010
	 * @version 1.0
	 */
	@ApiProtocol(cmdid = "0x10511812L", desc = "设置订单的统计项")
	class SetDealStatInfoByComdyId {
		/**
		 * 通过商品ID和统计类型来设置统计数值之请求
		 * 
		 * @author jeanqiang
		 * 
		 * @date Apr 20, 2011
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x10511812L", desc = "通过商品ID和统计类型来设置统计数值之请求")
		class Req {
			@Field(desc = "商品ID")
			uint64_t comdyId;
			@Field(desc = "要设置统计类型")
			Map<uint32_t,Integer> StatData;
		}
		
		/**
		 * 通过商品ID和统计类型来设置统计数值之回复
		 * 
		 * @author jeanqiang
		 * 
		 * @data Apr 20,2011
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x10518812L", desc = "通过商品ID和统计类型来设置统计数值之回复",export=false)
		class Resp {
			@Field(desc = "保留字段，无用字段")
			String reserver;
		}
	}
	
	
	/**
	 * 批量获取订单的所有统计信息
	 * 
	 * @author wendyhu
	 * 
	 * @date May 04, 2011
	 * @version 1.0
	 */
	@ApiProtocol(cmdid = "0x10511813L", desc = "批量获取订单的所有统计信息,只适合数据全部在一个set")
	class GetBatchDealStatInfoByUin {
		/**
		 * 批量获取订单的所有统计信息之请求
		 * 
		 * @author wendyhu
		 * 
		 * @date May 04, 2011
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x10511813L", desc = "批量获取订单的所有统计信息请求")
		class Req {
			@Field(desc = "qq号")
			List<uint32_t> uins;
		}
		
		/**
		 * 批量获取订单的所有统计信息之回复
		 * 
		 * @author wendyhu
		 * 
		 * @date May 04, 2011
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x10518813L", desc = "批量获取订单的所有统计信息之回复")
		class Resp {
			@Field(desc = "业务统计信息")
			Map<uint32_t,Map<uint32_t,Integer>> StatDatas;
		}
	}
	
	
	/**
	 * 批量获取订单的所有统计信息
	 * 
	 * @author wendyhu
	 * 
	 * @date May 04, 2011
	 * @version 1.0
	 */
	@ApiProtocol(cmdid = "0x10511814L", desc = "批量获取订单的所有统计信息,只适合数据全部在一个set")
	class GetBatchDealStatInfoByComdyId {
		/**
		 * 批量获取订单的所有统计信息之请求
		 * 
		 * @author wendyhu
		 * 
		 * @date May 04, 2011
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x10511814L", desc = "批量获取订单的所有统计信息请求")
		class Req {
			@Field(desc = "商品ID")
			List<uint64_t> comdyIds;
		}
		
		/**
		 * 批量获取订单的所有统计信息之回复
		 * 
		 * @author wendyhu
		 * 
		 * @date May 04, 2011
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x10518814L", desc = "批量获取订单的所有统计信息之回复")
		class Resp {
			@Field(desc = "业务统计信息")
			Map<uint64_t,Map<uint32_t,Integer>> StatDatas;
		}
	}




	   /**
		 * 获取新统一平台订单的所有统计信息
		 * 
		 * @author maxwellwang
		 * 
		 * @date Sep 09, 2013
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x10511815L", desc = "获取新统一平台订单的所有统计信息")
		class GetUnifyingPlatformDealStatInfoByUin {
			/**
			 * 通过QQ号获取新统一平台定单统计信息之请求
			 * 
			 * @author maxwellwang
			 * 
			 * @date Sep 09, 2013
			 * @version 1.0
			 */
			@ApiProtocol(cmdid = "0x10511815L", desc = "通过QQ号获取新统一平台定单统计信息之请求")
			class Req {
				@Field(desc = "用户QQ号码")
				uint32_t Uin;
			}
			
			/**
			 * 通过QQ号获取新统一平台定单统计信息之回复
			 * 
			 * @author maxwellwang
			 * 
			 * @date Sep 09, 2013
			 * @version 1.0
			 */
			@ApiProtocol(cmdid = "0x10518815L", desc = "通过QQ号获取新统一平台定单统计信息之回复")
			class Resp {
				@Field(desc = "业务统计信息")
				Map<uint32_t,Integer> StatData;
			}
		}
		
		/**
		 * 获取新统一平台订单的所有统计信息
		 * 
		 * @author maxwellwang
		 * 
		 * @date Sep 09, 2013
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x10511816L", desc = "获取新统一平台订单的所有统计信息")
		class GetUnifyingPlatformDealStatInfoByComdyId {
			/**
			 * 通过商品ID获取新统一平台定单统计信息之请求
			 * 
			 * @author maxwellwang
			 * 
			 * @date Sep 09, 2013
			 * @version 1.0
			 */
			@ApiProtocol(cmdid = "0x10511816L", desc = "通过商品ID获取新统一平台定单统计信息之请求")
			class Req {
				@Field(desc = "商品ID")
				uint64_t comdyId;
			}
			
			/**
			 * 通过商品ID获取新统一平台定单统计信息之回复
			 * 
			 * @author maxwellwang
			 * 
			 * @date Sep 09, 2013
			 * @version 1.0
			 */
			@ApiProtocol(cmdid = "0x10518816L", desc = "通过商品ID获取新统一平台定单统计信息之回复")
			class Resp {
				@Field(desc = "业务统计信息")
				Map<uint32_t,Integer> StatData;
			}
		}
		
		/**
		 * 设置新统一平台订单的统计项
		 * 
		 * @author maxwellwang
		 * 
		 * @date Sep 02, 2013
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x10511817L", desc = "设置新统一平台订单的统计项")
		class SetUnifyingPlatformDealStatInfoByComdyId {
			/**
			 * 通过商品ID和统计类型来设置统计数值之请求
			 * 
			 * @author maxwellwang
			 * 
			 * @date Sep 02, 2013
			 * @version 1.0
			 */
			@ApiProtocol(cmdid = "0x10511817L", desc = "通过商品ID和统计类型来设置统计数值之请求")
			class Req {
				@Field(desc = "商品ID")
				uint64_t comdyId;
				@Field(desc = "要设置统计类型")
				Map<uint32_t,Integer> StatData;
			}
			
			/**
			 * 通过商品ID和统计类型来设置统计数值之回复
			 * 
			 * @author maxwellwang
			 * 
			 * @data Sep 02,2013
			 * @version 1.0
			 */
			@ApiProtocol(cmdid = "0x10518817L", desc = "通过商品ID和统计类型来设置统计数值之回复",export=false)
			class Resp {
				@Field(desc = "保留字段，无用字段")
				String reserver;
			}
		}
		
		
		/**
		 * 批量获取新统一平台订单的所有统计信息
		 * 
		 * @author maxwellwang
		 * 
		 * @date Sep 02, 2013
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x10511818L", desc = "批量获取新统一平台订单的所有统计信息,只适合数据全部在一个set")
		class GetBatchUnifyingPlatformDealStatInfoByUin {
			/**
			 * 批量获取新统计平台订单的所有统计信息之请求
			 * 
			 * @author maxwellwang
			 * 
			 * @date Sep 02, 2013
			 * @version 1.0
			 */
			@ApiProtocol(cmdid = "0x10511818L", desc = "批量获取新统一平台订单的所有统计信息请求")
			class Req {
				@Field(desc = "qq号")
				List<uint32_t> uins;
			}
			
			/**
			 * 批量获取新统一平台订单的所有统计信息之回复
			 * 
		     * @author maxwellwang
			 * 
			 * @date Sep 02, 2013
			 * @version 1.0
			 */
			@ApiProtocol(cmdid = "0x10518818L", desc = "批量获取新统一平台订单的所有统计信息之回复")
			class Resp {
				@Field(desc = "业务统计信息")
				Map<uint32_t,Map<uint32_t,Integer>> StatDatas;
			}
		}
		
		
		/**
		 * 批量获取新统一平台订单的所有统计信息
		 * 
		 * @author maxwellwang
		 * 
		 * @date Sep 02, 2013
		 * @version 1.0

		 */
		@ApiProtocol(cmdid = "0x10511819L", desc = "批量获取新统一平台订单的所有统计信息,只适合数据全部在一个set")
		class GetBatchUnifyingPlatformDealStatInfoByComdyId {
			/**
			 * 批量获取新统一平台订单的所有统计信息之请求
			 * 
			 * @author maxwellwang
			 * 
			 * @date Sep 02, 2013
			 * @version 1.0
			 */
			@ApiProtocol(cmdid = "0x10511819L", desc = "批量获取新统一平台订单的所有统计信息请求")
			class Req {
				@Field(desc = "商品ID")
				List<uint64_t> comdyIds;
			}
			
			/**
			 * 批量获取新统一平台订单的所有统计信息之回复
			 * 
			 * @author maxwellwang
			 * 
			 * @date Sep 02, 2013
			 * @version 1.0

			 */
			@ApiProtocol(cmdid = "0x10518819L", desc = "批量获取新统一平台订单的所有统计信息之回复")
			class Resp {
				@Field(desc = "业务统计信息")
				Map<uint64_t,Map<uint32_t,Integer>> StatDatas;
			}
		}



	
	/**
	 * 获取商品的所有统计信息
	 * 
	 * @author wendyhu
	 * 
	 * @date Jun 17, 2011
	 * @version 1.0
	 */
	@ApiProtocol(cmdid = "0x10521801L", desc = "获取商品的所有统计信息")
	class GetCommodityStatInfoByUin {
		/**
		 * 获取商品的所有统计信息
		 * 
		 * @author wendyhu
		 * 
		 * @date Jun 17, 2011
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x10521801L", desc = "通过key获取商品统计信息之请求")
		class Req {
			@Field(desc = "key")
			uint64_t key;
		}
		
		/**
		 * 获取商品的所有统计信息
		 * 
		 * @author wendyhu
		 * 
		 * @date Jun 17, 2011
		 * @version 1.0
		 */
		@ApiProtocol(cmdid = "0x10528801L", desc = "通过key获取商品统计信息之回复")
		class Resp {
			@Field(desc = "业务统计信息")
			Map<uint32_t,Integer> StatData;
		}
	}
	
//	Jingying say: 此处IDL生成失败
//	/**
//	 * 获取商品的所有统计信息
//	 * 
//	 * @author  jetwang 补上
//	 * 
//	 * @date Jun 17, 2011
//	 * @version 1.0
//	 */
//	@ApiProtocol(cmdid = "0x10521802L", desc = "获取商品的所有统计信息")
//	class GetCommodityStatInfoByUinWithResp {
//		/**
//		 * 获取商品的所有统计信息
//		 * 
//		 * @author  jetwang 补上
//		 * 
//		 * @date Jun 17, 2011
//		 * @version 1.0
//		 */
//		@ApiProtocol(cmdid = "0x10521802L", desc = "通过key获取商品统计信息之请求")
//		class Req {
//			@Field(desc = "key")
//			uint64_t key;
//		}
//		
//		/**
//		 * 获取商品的所有统计信息
//		 * 
//		 * @author jetwang 补上
//		 * 
//		 * @date Jun 24, 2012
//		 * @version 1.0
//		 */
//		@ApiProtocol(cmdid = "0x10528802L", desc = "通过key获取商品统计信息之回复")
//		class Resp {
//			@Field(desc = "业务统计信息")
//			Map<uint32_t,Integer> StatData;
//			
//			@Field(desc = "返回key 供异步调用")
//			uint64_t respKey;
//		}
//	}

	/**
	 * 获取收藏的统计信息
	 *
	 * @author wileywang
	 *
	 * @date Aug 15, 2011
	 * @version 1.0
	 */
	 @ApiProtocol(cmdid = "0x10531801L", desc = "根据商品id或者店铺id获取该商品的收藏统计数")
	 class GetFavoritesById
	 {
		 @ApiProtocol(cmdid = "0x10531801L", desc = "根据商品id或者店铺id获取该商品的收藏统计数的请求参数")
		 class Req
		 {
			 @Field(desc = "keyId")
		     uint64_t keyId;
		 }

		 @ApiProtocol(cmdid = "0x10538801L", desc = "根据商品id获取该商品的收藏统计数的回复参数")
		 class Resp
		 {
			 @Field(desc = "业务统计信息")
			 uint64_t requestKeyId;
			 Map<uint32_t, Integer> statData;
		 }
	 }
	 
	/**
	 * 修改统计server的值，此接口可以加几，减几操作
	 *
	 * @author anthonywei
	 *
	 * @date Aug 15, 2011
	 * @version 1.0
	 */
	 @ApiProtocol(cmdid = "0x10541801L", desc = "根据统计server的key，修改具体的值，有加，减，具体的值在value中指定")
	 class UpdateStatCommTmem
	 {
		 @ApiProtocol(cmdid = "0x10541801L", desc = "据统计server的key，修改具体的值，有加，减，具体的值在value中指定")
		 class Req
		 {
			 @Field(desc = "keyId")
		     uint64_t keyId;

			 @Field(desc = "areaType")
			 uint16_t areaType;	 

			 @Field(desc = "statType")
			 uint32_t statType;

			 @Field(desc = "value")
			 int value;	 
		 }

		 @ApiProtocol(cmdid = "0x10548801L", desc = "据统计server的key，修改具体的值，有加，减，具体的值在value中指定")
		 class Resp
		 {
			 @Field(desc = "返回最新的统计server的值")
			 uint32_t newValue;
		 }
	 }
	
	 /**
	 * 设置统计server的值，此接口将统计server的值设置为固定的
	 *
	 * @author anthonywei
	 *
	 * @date Aug 15, 2011
	 * @version 1.0
	 */
	 @ApiProtocol(cmdid = "0x10541802L", desc = "根据统计server的key，设置统计server的具体值")
	 class SetStatCommTmem
	 {
		 @ApiProtocol(cmdid = "0x10541802L", desc = "据统计server的key，设置统计server的具体值")
		 class Req
		 {
			 @Field(desc = "keyId")
		     uint64_t keyId;

			 @Field(desc = "areaType")
			 uint16_t areaType;	 

			 @Field(desc = "statType")
			 uint32_t statType;

			 @Field(desc = "value")
			 uint32_t value;	 
		 }

		 @ApiProtocol(cmdid = "0x10548802L", desc = "据统计server的key，设置统计server的具体值")
		 class Resp
		 {
			 @Field(desc = "返回最新的统计server的值")
			 uint32_t newValue;
		 }
	 }

	 /**
	 * 设置统计server的值，此接口在初始化的时候，将统计server的值设置为固定的
	 *
	 * @author anthonywei
	 *
	 * @date Aug 15, 2011
	 * @version 1.0
	 */
	 @ApiProtocol(cmdid = "0x10541803L", desc = "根据统计server的key，初始化统计server的具体值")
	 class InitStatCommTmem
	 {
		 @ApiProtocol(cmdid = "0x10541803L", desc = "据统计server的key，初始化统计server的具体值")
		 class Req
		 {
			 @Field(desc = "keyId")
			 uint64_t keyId;

			 @Field(desc = "areaType")
			 uint16_t areaType;	 

			 @Field(desc = "statType")
			 uint32_t statType;

			 @Field(desc = "value")
			 uint32_t value;	 
		 }

		 @ApiProtocol(cmdid = "0x10548803L", desc = "据统计server的key，初始化统计server的具体值")
		 class Resp
		 {
			 @Field(desc = "返回最新的统计server的值")
			 uint32_t newValue;
		 }
	 }

	 /**
	 * 反转Tmem的当前使用的key
	 *
	 * @author anthonywei
	 *
	 * @date Aug 15, 2011
	 * @version 1.0
	 */
	 @ApiProtocol(cmdid = "0x10541804L", desc = "反转Tmem的当前使用的key")
	 class ReverseTmemKey
	 {
		 @ApiProtocol(cmdid = "0x10541804L", desc = "反转Tmem的当前使用的key")
		 class Req
		 {
			 @Field(desc = "领域类型")
			 uint16_t areaType;
			 
			 @Field(desc = "同步时间")
			 uint32_t initTime;
			 
			 @Field(desc = "模得大小")
			 uint32_t modSize;
		 }

		 @ApiProtocol(cmdid = "0x10548804L", desc = "反转Tmem的当前使用的key")
		 class Resp
		 {
			 @Field(desc = "ErrMsg")
			 String errMsg;
		 }
	 }

 
	/**
	 * 修改统计server的值，此接口可以加几，减几操作，初始化的时候，更新msg消息接口
	 *
	 * @author anthonywei
	 *
	 * @date Aug 15, 2011
	 * @version 1.0
	 */
	 @ApiProtocol(cmdid = "0x10541805L", desc = "初始化的时候，更新msg消息接口")
	 class InitUpdateStatCommTmem
	 {
		 @ApiProtocol(cmdid = "0x10541805L", desc = "初始化的时候，更新msg消息接口")
		 class Req
		 {
			 @Field(desc = "keyId")
			 uint64_t keyId;

			 @Field(desc = "areaType")
			 uint16_t areaType;	 

			 @Field(desc = "statType")
			 uint32_t statType;

			 @Field(desc = "value")
			 int value;	 
		 }

		 @ApiProtocol(cmdid = "0x10548805L", desc = "初始化的时候，更新msg消息接口")
		 class Resp
		 {
			 @Field(desc = "返回最新的统计server的值")
			 uint32_t newValue;
		 }
	 } 
}

