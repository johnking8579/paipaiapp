package com.qq.qqbuy.thirdparty.idl.refund;

import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.qq.qqbuy.thirdparty.idl.refund.RefundPoV1.EventParamBasePo;
import com.qq.qqbuy.thirdparty.idl.refund.RefundPoV1.EventParamsRefundPo;


@HeadApiProtocol(cPlusNamespace = "c2cent::ao::refund", needInit = true) 
public class RefundAo
{
	 @ApiProtocol(cmdid = "0x876d1801L", desc = "买家要求退款")
	 class BuyerRefund{
		 @ApiProtocol(cmdid = "0x876d1801L", desc = "买家要求退款")
		 class Req{
			 @Field(desc = "代码文件名")
			 String Source;
		  
			 @Field(desc = "基本事件需要的信息")
			 EventParamBasePo Basestu;  
			 
			 @Field(desc = "标记发货事件参数需要的信息")
			 EventParamsRefundPo Refundstu;
		 }
		 @ApiProtocol(cmdid = "0x876d8801L", desc = "买家要求退款")
		 class Resp{
			 @Field(desc = "返回保留字")
			 String outReserve;
		 	
		 }
	 }
}




