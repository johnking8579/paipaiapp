package com.qq.qqbuy.thirdparty.idl.refund;

import com.paipai.lang.uint64_t;
import com.paipai.lang.uint32_t;
import com.paipai.lang.uint8_t;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;
import java.util.Vector;


@HeadApiProtocol(cPlusNamespace = "c2cent::po::refund", needInit = true) 
public class RefundPoV1
{
        @Member(desc = "基本事件参数Po", cPlusNamespace = "c2cent::po::refund", isNeedUFlag = false, isNeedTempVersion=true)
        public class EventParamBasePo{
                @Field(desc = "版本号", defaultValue="1"  )
                uint32_t version;
                
                @Field(desc = "大订单号")
                String DealId;
                
                @Field(desc = "事件号")
                uint32_t EventId;
                
                @Field(desc = "源文件文件名")
                String Source;
                
                @Field(desc = "买家账号")
                uint32_t BuyerUin;
                
                @Field(desc = "卖家账号")
                uint32_t SellerUin;
                
                @Field(desc = "子订单号,全额退款传空值即可")
                String TradeId;
                
                @Field(desc = "客户端IP")
                String ClientIP;
                
                @Field(desc = "机器码")
                String MachineKey;
                
       }
       @Member(desc = "标记发货事件参数Po", cPlusNamespace = "c2cent::po::refund", isNeedUFlag = false, isNeedTempVersion=true)
       public class EventParamsRefundPo{
               @Field(desc = "版本号", defaultValue="1"  )
               uint32_t version;
                
               @Field(desc = "是否收到货")
               uint32_t isRecvItem;
                
               @Field(desc = "是否请求退货")
               uint32_t isReqitem;
                
               @Field(desc = "退货原因类型")
               uint32_t RefundReasonType;
                
               @Field(desc = "退货申请详细说明")
               String RefundApplyMsg;
                
               @Field(desc = "退款金额，单位是分，四舍五入")
               uint32_t RefundToBuyer;
       }
}
                
