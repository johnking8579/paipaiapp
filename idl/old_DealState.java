package com.qq.qqbuy.dealopr.po;
public enum DealState
{
  DS_UNKNOWN(0L, "系统处理中,未知状态"), 
  DS_WAIT_BUYER_PAY(1L, "等待买家付款"), 
  DS_WAIT_SELLER_DELIVERY(2L, "买家已付款,等待卖家发货"), 
  DS_WAIT_BUYER_RECEIVE(3L, "商家已发货，等待买家收货"), 
  DS_DEAL_END_NORMAL(4L, "交易成功"), 
  DS_DEAL_CANCELLED(5L, "订单取消"), 
  DS_SYSTEM_HALT(6L, "系统暂停订单"), 
  DS_SYSTEM_PAYING(7L, "系统打款中"), 
  DS_DEAL_REFUNDING(8L, "退款处理中"), 

  DS_DEAL_SHIPPING_PREPARE(9L, "卖家配货中"),
  DS_DEAL_TAKECHEAP_TUAN_GOING(10L, "已付款,待成团"),
  DS_REFUND_START(20L, "开始退款流程"), 
  DS_REFUND_WAIT_BUYER_DELIVERY(22L, "等待买家发送退货"), 
  DS_REFUND_WAIT_MODIFY_AFTER_SHIP(23L, "等待买家修改退货申请(发送退货后)"), 
  DS_REFUND_WAIT_MODIFY(24L, "卖家拒绝，等待买家修改退款协议"), //等待买家修改退款申请
  DS_REFUND_WAIT_SELLER_RECEIVE(25L, "等待卖家确认收到退货"), 
  DS_REFUND_WAIT_SELLER_AGREE(26L, "等待卖家同意退款"), 
  DS_REFUND_OK(27L, "退款成功"), 
  DS_REFUND_CANCEL(29L, "退款取消"), 
  DS_REFUND_ALL_WAIT_SELLER_AGREE(31L, "等待卖家同意取消包裹"), //等待卖家同意全额退款
  DS_REFUND_ALL_CANCEL(32L, "全额退款取消"), 
  DS_REFUND_ALL_OK(33L, "全额退款成功"), 
  DS_CLOSED(-1L, "订单已关闭"), 

  STATE_COD_START(40L, "货到付款开始"), 
  STATE_COD_WAIT_SHIP(41L, "货到付款等待发货"), 
  STATE_COD_SHIP_OK(42L, "货到付款已发货"), 
  STATE_COD_SIGN(43L, "货到付款已签收"), 
  STATE_COD_REFUSE(44L, "货到付款拒签"), 
  STATE_COD_SUCESS(45L, "货到付款成功(已打款)"), 
  STATE_COD_CANCEL(46L, "货到付款取消(关闭or 拒签后关闭)"), 

  STATE_FQFK_START(50L, "等待商家确认分期付款"), 
  STATE_FQFK_CANCEL(51L, "未通过商家审核分期付款"), 

  DS_DEAL_END_REFUND(-1L, "订单退款结束"), 

  DS_SYSTEM_DELIVERING(-1L, "系统发货中"), 

  DS_SYSTEM_REFUNDING(-1L, "系统退款中"), 

  DS_TIMEOUT_BUYER_RECEIVE(-1L, "等待买家确认收货超时"), 

  DS_TIMEOUT_SELLER_RECEIVE(-1L, "等待卖家确认收货超时"), 

  DS_TIMEOUT_SELLER_PASS_RETURN(-1L, "等待卖家响应买家退货请求超时"), 

  DS_TIMEOUT_SELLER_PASS_REFUND_ALL(-1L, "等待卖家确认全额退款超时");

  private final String desc;
  private final long code;

  public String toString()
  {
    return name();
  }

  private DealState(long code, String desc)
  {
    this.code = code;
    this.desc = desc;
  }

  public String getDesc() {
    return this.desc;
  }

  public long getCode() {
    return this.code;
  }

  public static DealState getDealStateByCode(long code) {
    for (DealState ds : values()) {
      if (ds.code == code)
        return ds;
    }
    return DS_UNKNOWN;
  }

  public static DealState valueOfIgnoreCase(String state) {
    try {
      return valueOf(state.toUpperCase());
    } catch (Exception e) {
    }
    return DS_UNKNOWN;
  }
}
