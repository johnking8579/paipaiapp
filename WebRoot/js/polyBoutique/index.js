define("kit/login", ['require','exports','module'],function(require, exports, module) {
    var checkLogin = function() {
        return (($.getCookie("wg_skey") && $.getCookie("wg_uin") || $.getCookie("p_skey") && $.getCookie("p_uin") || $.getCookie("skey") && $.getCookie("uin")) ? true : false);
    };

    var checkAndRedirect = function(rurl) {
        if (!checkLogin()) {
            gotoLogin(rurl);
        }
    };

    var gotoLogin = function(rurl) {
        var isWX = window.WeixinJSBridge;
        if (!isWX) {
            var ua = navigator.userAgent.toLowerCase();
            isWX = ua.match(/micromessenger/) ? true : false;
        }
        if (isWX) {
            window.location.href = 'http://b.paipai.com/mlogin/tws64/m/wxv2/Login?appid=1&rurl=' + encodeURIComponent(rurl||window.location.href);
        } else {
            window.location.href = 'http://b.paipai.com/mlogin/tws64/m/h5v1/cpLogin?rurl=' + encodeURIComponent(rurl||window.location.href) + '&sid=' + $.getCookie('sid') + '&uk=' + $.getCookie('uk');
        }
    };

    module.exports = {
        checkAndRedirect: checkAndRedirect,
        checkLogin: checkLogin,
        gotoLogin: gotoLogin
    };
});
define("kit/follow", ['require','exports','module','kit/login'],function(require, exports, module) {
    var login = require("kit/login");
    var curtarget;
    var cgis = {
        'addfavoriteshop': 'http://my.paipai.com/cgi-bin/favorite_new/AddFavShop',
        'delfavoriteshop': 'http://my.paipai.com/cgi-bin/favorite_new/DelFavShopList',
        'getfollowcount': 'http://opr.paipai.com/item/statshow?type=1',
        'checkfollow':'http://my.paipai.com/favorite/CheckFavShopList'
    }


    var renderFollowCount = function(items) {
        $.ajax({
            'url': cgis.getfollowcount,
            type: 'GET',
            dataType: 'jsonp',
            data: {
                item: items.join(',')
            },
            success: function(data) {
                if (data && data.errCode == 0) {
                    var data = data.data;
                    for (var i = 0; i < items.length; i++) {
                        var finalCount = data[items[i]].shop.shopfav;
                        if (finalCount > 9999) {
                            finalCount = "9999+";
                        }
                        $('a[shopid="' + items[i] + '"]').html("<br>"+finalCount).data('followcount', data[items[i]].shop.shopfav);
                    }
                }
            }
        });

        $.ajax({
            'url': cgis.checkfollow,
            type: 'GET',
            dataType: 'jsonp',
            data: {
                shopid: items.join("|")
            },
            success: function(data) {
                if (data && data.errcode == 0) {
                    var t = data.checkResult;
                    for (var n in t)
                        t[n].isFavorited == 1 && ($('a[shopid="' + n + '"]').addClass("mod_bar_shop_fs_on"), $('a[shopid="' + n + '"]').data("followed", !0))
                }
            }
        });
    };

    var addFavoriteShop = function(event) {
        event.preventDefault();
        var target = $(event.target);
        if (target.attr('shopid') === null && target.data('followcount') === undefined) {
            return;
        }
        if (!target.data('following')) {
            target.data('following', true);
            if (!target.data('followed')) {
                $.ajax({
                    'url': cgis.addfavoriteshop,
                    type: 'GET',
                    dataType: 'jsonp',
                    'data': {
                        'shopId': target.attr('shopid')
                    },
                    success: function(data) {
                        if (data) {
                            if (data.errCode == 0) {
                                var curNumber = target.data('followcount');
                                target.data('followcount', Number(curNumber) + 1)
                                var finalCount = Number(curNumber) + 1 > 9999 ? "9999+" : Number(curNumber) + 1;
                                target.html("<br>"+finalCount);
                                target.data('followed', true);
                                target.addClass('mod_bar_shop_fs_on');
                            } else if (data.errCode == 13) {
                                login.gotoLogin();
                            }
                        }
                        target.data('following', false);
                    }
                });
            } else {
                curtarget = $(event.target);
                $.ajax({
                    'url': cgis.delfavoriteshop,
                    type: 'GET',
                    dataType: 'jsonp',
                    'data': {
                        'shopId': target.attr('shopid')
                    }
                });
            }
        }
    };

    window.DelFavShopListCallBack = function(data) {
        if (data && data.errCode == 0) {
            var curNumber = curtarget.data('followcount');
            curtarget.data('followcount', Number(curNumber) - 1)
            var finalCount = Number(curNumber) - 1 > 9999 ? "9999+" : Number(curNumber) - 1;
            curtarget.html("<br>"+finalCount);
            curtarget.data('followed', false);
            curtarget.removeClass('mod_bar_shop_fs_on');
        }
        curtarget.data('following', false);
    }
    var bindDOM = function() {
        $('#main_content').delegate('a[action-type="follow"]', 'touchend', addFavoriteShop);
    };
    var init = function() {
        bindDOM();
    };

    module.exports = {
        init: init,
        renderFollowCount: renderFollowCount

    }
});
define("kit/shareapi", ['require','exports','module'],function(require, exports, module) {
    var browser="",config;
    var doc=document;
    var check=function(){
        if(mqq.iOS||mqq.android)
        {
            return browser="qq";
        }
        else
        {
            var ua = window.navigator.userAgent.toLowerCase();
            return ua.match(/micromessenger/i) == 'micromessenger'?browser="wechat":"";
        }
    };

    var base=function(){
        if(browser=="qq")
        {
            mqq.ui.setOnShareHandler(function(type){
                hidesharetip();
                var itemUrl;
                if(type==0||type==1){
                    itemUrl=addPtag("");
                }else if(type==2){
                    itemUrl=addPtag("20408.1.3");
                }else{
                    itemUrl=addPtag("20408.1.4");
                }
                mqq.ui.shareMessage({
                    'title':type==0?config.qq_title:(type==1?config.qzone_title:(type==2?config.wechat_title:config.wechat_time_title)),
                    'desc':type==0?config.qq_desc:(type==1?config.qzone_desc:config.wechat_desc),
                    'share_type':type,
                    'share_url':config.url,
                    'image_url':config.img,
                    'back':true,
                    'shareElement':'news',
                    'flash_url':'',
                    'puin':'',
                    'appid':'',
                    'uinType':''
                },function(){});
            });
        }
        else if(browser=="wechat")
        {
            var btn=doc.getElementsByName("shareBtn");
            var btnVal=["发送给朋友","分享到朋友圈","分享到腾讯微博","发送邮件"];
            for(var i=0;i<btn.length;i++)
            {
                btn[i].value=btnVal[i];
            }
            var sendMessage=function(){
                WeixinJSBridge.on('menu:share:timeline', function(argv){
                    wechatInvoke('shareTimeline');
                });
                WeixinJSBridge.on('menu:share:appmessage', function(argv){
                    wechatInvoke('sendAppMessage');
                });
                WeixinJSBridge.on('menu:share:weibo', function(argv){
                    wechatInvoke('shareWeibo');
                });
                WeixinJSBridge.on('menu:share:email', function(argv){
                    wechatInvoke('sendEmail');
                });
            };

            if (typeof WeixinJSBridge == "object" && typeof WeixinJSBridge.invoke == "function") {
                sendMessage();
            } else {
                if (doc.addEventListener) {
                    doc.addEventListener("WeixinJSBridgeReady", sendMessage, false);
                } else if (doc.attachEvent) {
                    doc.attachEvent("WeixinJSBridgeReady", sendMessage);
                    doc.attachEvent("onWeixinJSBridgeReady", sendMessage);
                }
            }
        }
        else
        {
            hideshare();
        }

    };

    var begin=function(event){
        var obj=event.target;
        var shareType=obj.getAttribute("share");
        if(browser=="qq")
        {
            mqq.ui.shareMessage({
                'title':config.qq_title,
                'desc':config.qq_desc,
                'share_type':shareType,
                'share_url':config.url,
                'image_url':config.img,
                'back':true,
                'shareElement':'news',
                'flash_url':'',
                'puin':'',
                'appid':'',
                'uinType':''
            },function(){});
        }
        else if(browser=="wechat")
        {
            var shareMessage=function(){
                var action=["sendAppMessage","shareTimeline","shareWeibo","sendEmail"];
                wechatInvoke(action[shareType]);
            };
            if (typeof WeixinJSBridge == "object" && typeof WeixinJSBridge.invoke == "function") {
                shareMessage();
            } else {
                if (doc.addEventListener) {
                    doc.addEventListener("WeixinJSBridgeReady", shareMessage, false);
                } else if (doc.attachEvent) {
                    doc.attachEvent("WeixinJSBridgeReady", shareMessage);
                    doc.attachEvent("onWeixinJSBridgeReady", shareMessage);
                }
            }
        }
    };

    var wechatInvoke=function(action)
    {
        hidesharetip();
        if(action=="shareWeibo")
        {
            var itemUrl=addPtag("");
            WeixinJSBridge.invoke(action,{
                "url":itemUrl,
                "content":config.wechat_title,
                "type":"link"
            }, function(res){});
        }
        else if(action=="sendEmail")
        {
            var itemUrl=addPtag("");
            WeixinJSBridge.invoke(action,{
                "title":config.wechat_title,
                "content":itemUrl
            }, function(res){});
        }
        else if(action=="shareTimeline")
        {
            var itemUrl=addPtag("20408.1.4");
            WeixinJSBridge.invoke(action, {
                img_url:config.img,
                img_width: "240",
                img_height: "240",
                link: itemUrl,
                desc: config.wechat_desc,
                title: config.wechat_time_title
            }, function (res) {});
        }
        else
        {
            var itemUrl=addPtag("20408.1.3");
            WeixinJSBridge.invoke(action, {
                img_url:config.img,
                img_width: "240",
                img_height: "240",
                link: itemUrl,
                desc: config.wechat_desc,
                title: config.wechat_title
            }, function (res) {});
        }
    }

    var addPtag=function(pNum){
        var itemUrl=config.url;
        if(pNum){
            if(itemUrl.indexOf("?")!=-1){
                itemUrl=itemUrl.split('?');
                itemUrl=itemUrl.splice(0,1)+"?ptag="+pNum+"&"+itemUrl.join("&");
            }else if(itemUrl.indexOf("#")!=-1){
                itemUrl=itemUrl.split('#');
                itemUrl=itemUrl.splice(0,1)+"?ptag="+pNum+"#"+itemUrl.join("#");
            }else{
                itemUrl+="?ptag="+pNum;
            }
        }
        return itemUrl;
    }

    var hidesharetip=function(){
        var obj=doc.getElementById("shareTipBox");
        if(obj)
        {
            obj.style.display="none";
        }
    };

    var hideshare=function(){
        var obj=doc.getElementById("shareBox");
        if(obj)
        {
            obj.style.display="none";
        }
    };

    var formatData=function(){
        config.qq_title=config.qq_title||config.title;
        config.qzone_title=config.qzone_title||config.qq_title;
        config.wechat_title=config.wechat_title||config.title;
        config.wechat_time_title=config.wechat_time_title||config.wechat_title;
    };

    module.exports.shareApi=function(configVal){
        config=configVal;
        formatData();
        var _doc=doc.getElementsByTagName('head')[0];
        var script=doc.createElement('script');
        script.setAttribute('type','text/javascript');
        script.setAttribute('src','http://static.paipaiimg.com/js/weidian/js/kit/qqapi.custom.js?_bid=152');
        _doc.appendChild(script);
        script.onload=script.onreadystatechange=function(){
            if(!this.readyState||this.readyState=='loaded'||this.readyState=='complete'){
                check();
                base();
                var obj=doc.getElementById("shareBox");
                if(obj)
                {
                    var objs=obj.getElementsByTagName("input");
                    for(var i=0;i<objs.length;i++)
                    {
                        if (doc.addEventListener) {
                            objs[i].addEventListener("touchstart",begin);
                        } else if (doc.attachEvent) {
                            objs[i].attachEvent("touchstart",begin);
                        }
                    }
                }
            }
            else
            {
                hideshare();
            }
            script.onload=script.onreadystatechange=null;
        }
    };
});


define("section/getIndex", ['require','exports','module','kit/login','kit/follow','kit/shareapi'],function(require, exports, module) {
    var boutData,jendtime,systemTime,sysResTime,scrollTimer,windowHeight = $(window).height(),SCROLLOFFSET = 5000,clone,pclone
        login=require("kit/login"),
        follow = require("kit/follow"),
        share=require("kit/shareapi"),
    //touchRoll=require("section/touchRoll"),
        map=['35431','35415','35425','35427','35428','35429','35430'],
        cgis={
            groupUrl:"http://b.paipai.com/groupservice/grouplist",
            boutiqueUrlBase:"http://opr.paipai.com/mart/activeshow",
            flashBuyBase:"http://www.paipai.com/sinclude/xml/tjw/tjw2014/tjw9/",
            flashBuyDetail:"http://act.paipai.com/flashbuy/new/active/getActiveInfoParamJson?jsonlist="
        },
        config={
            "url": window.location.href,
            "qq_title": "拍拍微店-精选",
            "qzone_title": "拍拍微店-精选",
            "wechat_title": "拍拍微店-精选",
            "wechat_time_title": "拍拍微店-精选，精挑细选，聚惠狂欢，等你来！",
            "wechat_desc": "精挑细选，聚惠狂欢，等你来！",
            "qq_desc": "精挑细选，聚惠狂欢，等你来！",
            "qzone_desc":"精挑细选，聚惠狂欢，等你来！",
            "img": "http://static.paipaiimg.com/fd/wd/h5/base/img/share_jx.png"
        };

    //超时处理
    var setTimeoutInfo=function(a,b,c){
        console.log(a,"test");
        console.log(b,"test");
        console.log(c,"test");
    };

    var getTimeNow=function(){
        //systemTime=new Date($.ajax({url:"http://"+window.location.hostname+"/favicon.ico?t="+Math.random(), async: false}).getResponseHeader("Date")||"");
        var timstamp = new Date().valueOf();
        systemTime = new Date($.ajax({url:"?t="+timstamp,async: false}).getResponseHeader("Date"));
        changeEndTime();
    };

    /*    //获取团团降信息
     var getGroupInfo=function(){
     //touchRoll.resize();
     $.ajax({
     url: cgis.groupUrl,
     type: 'GET',
     dataType: 'jsonp',
     timeout:15000,
     error:setTimeoutInfo,
     success: function(data) {
     if(data.data.length>5){
     data.data.length=5;
     }
     var html=$("#template_tuan").template({
     'data':data.data
     });
     $("#template_tuan").replaceWith(html);
     //touchRoll.init();
     }
     });
     };*/

    //获取聚精品信息
    var getBoutique=function(actionByTab){
        var timeS=systemTime.getTime();
        var itemDate=systemTime;
        if(systemTime.getHours()<1){
            itemDate=new Date(systemTime-1000*60*60*24);
        }
        $.ajax({
            url: cgis.boutiqueUrlBase+"?ids="+map[itemDate.getDay()]+"&pcs=1&sort=1",
            type: 'GET',
            dataType: 'jsonp',
            jsonpCallback:'activeshow_callback',
            timeout:15000,
            error:setTimeoutInfo,
            success: function(data) {
                boutData=data.data[0].vecAreas;
                formatBoutique(timeS,true,actionByTab);
            }
        });
        setBoutiqueTitle();
    };

    var formatBoutique=function(timeS,isFirst,actionByTab){
        var needNew=true;
        if(!isFirst){
            timeS-=1000*60*60*2;
            if($("#boutiqueBox li[class*='"+timeS+"']")[0]){
                needNew=false;
            }
        }
        if(needNew){
            var itemDate=systemTime;
            if(systemTime.getHours()<1){
                itemDate=new Date(systemTime-1000*60*60*24);
            }
            var istr="?ids="+map[itemDate.getDay()]+"&pcs=1&sort=1&ctl=",clsName;
            $("#loadingProduct1").show();

            for(var i=0;i<boutData.length;i++){

                var bdBeginTime=boutData[i]["dwBeginTime"]*1000,bdEndTime=boutData[i]["dwEndTime"]*1000;
                if(bdBeginTime<=timeS&&bdEndTime>=timeS){
                    var tData=getResTime(bdBeginTime,bdEndTime);
                    if(tData.hours<2||tData.hours==5){
                        istr+=boutData[i]["dwPoolId"]+":100,";
                        clsName=bdBeginTime;
                    }
                }
            }
            $.ajax({
                url: cgis.boutiqueUrlBase+istr,
                type: 'GET',
                dataType: 'jsonp',
                jsonpCallback:'callback',
                error:setTimeoutInfo,
                success: function(data) {
                    setBoutique(data.data[0].vecAreas[0].vecTabs,clsName,actionByTab);
                }
            });
        }else{
            $("#boutiqueBox li").hide();
            $("#boutiqueBox li[class*='"+timeS+"']").show();
            document.getElementById("select_tab").scrollIntoView();
        }
        if(!isFirst){
            var tData=getResTime(systemTime.getTime(),parseInt(timeS)+1000*60*60*2);
            if((tData.hours>=0&&tData.hours<2)||tData.hours==5){
                $("#ju_act_title").html("距结束：<b id='resTimeBox'></b>");
            }else{
                $("#ju_act_title").html("未开始");
            }
            var dd=new Date(parseInt(timeS));
            var st = padLeft(dd.getHours())=="05"?"01":padLeft(dd.getHours());
            $("#ju_act_time").html(st+":"+padLeft(dd.getMinutes())+" 场");
            $("#ju_act_box").data("state",timeS.toString());
        }
    };

    //显示聚精品信息
    var setBoutique=function(data,clsName,actionByTab){
        var ihtml="",shopID=[];
        if(data){
            for(var i=0;i<data.length;i++)
            {

                shopID.push(data[i]["ddwSellerUin"]);
                ihtml+="<li class='"+clsName+(data[i]["dwStock"]==0?" over":"")+"'>";
                ihtml+="<a class='info_wrap_click' href='http://b.paipai.com/itemweb/item?ptag=20408.2.5&scence=101&ic="+data[i]["strCommodityId"]+"'><div class='url'>";
                if(i<4){
                    ihtml+="<img src='"+data[i]["mapCustomData"]["uploadPicUrl3"]+"'>";
                }else{
                    ihtml+="<img src='http://static.paipaiimg.com/fd/wd/h5/base/img/1x1.gif'  data-original='"+data[i]["mapCustomData"]["uploadPicUrl3"]+"'>";
                }
                ihtml+="<i class='icon_miao'></i> </div><div class='info_wrap'>";
                ihtml+="<div class='pfn'><h3 class='fn'>"+data[i]["mapExt"]["recmdRegName"]+"</h3>";
                //ihtml+="<a class='product_list_like' action-type='follow' shopid='"+data[i]["ddwSellerUin"]+"' name='"+data[i]["ddwSellerUin"]+"'><span></span></a>";
                ihtml+="<div class='price_wrap'>";
                ihtml+=data[i]["dwPrice"]==0?"":("<div class='price_wrap_fir'><i class='discount'>"+parseFloat(data[i]["dwActivePrice"]/data[i]["dwPrice"]*10).toFixed(1)+"折</i>");
                ihtml+="<div class='bottom_info'>";
                if(data[i]["dwStock"]==0){
                    ihtml+="<span class='btn_shop'>进入店铺</span>";
                }
                else{
                    ihtml+="<span class='remain_num'>仅剩<em>"+data[i]["dwStock"]+"</em>件</span>";
                }
                ihtml+="</div></div>";
                ihtml+="<div class='price_wrapdiv'><span class='price'>&yen;"+parseFloat(data[i]["dwActivePrice"]/100).toFixed(2)+"</span>";
                ihtml+="<del class='old_price'>&yen;"+parseFloat(data[i]["dwPrice"]/100).toFixed(2)+"</del></div>";
                ihtml+="</div></div></div><i class='icon_state'></i><div class='product_list_bar'>";
                ihtml+="<a class='product_list_bar_tt' href='http://wd.paipai.com/mshop/"+data[i]["ddwSellerUin"]+"'><span>"+data[i]["strShopName"]+"</span>";
                ihtml+="<span class='indp'>进入店铺</span><i></i></a>";
                //ihtml+="<a class='product_list_like' action-type='follow' shopid='"+data[i]["ddwSellerUin"]+"' name='"+data[i]["ddwSellerUin"]+"'><span></span></a></div></li>";
            }

            $("#boutiqueBox").append(ihtml);
            //follow.renderFollowCount(shopID);
        }
        $("#boutiqueBox li").hide();
        $("#boutiqueBox li[class*='"+clsName+"']").show();
        $("#loadingProduct1").hide();
        $("#ju_act_box").data("state",clsName.toString());
        if(actionByTab){
            document.getElementById("select_tab").scrollIntoView();
        }
        if(document.getElementById("g_temp_box2")) {
            $("#g_temp_box2").remove();
        }
        $("img").lazyload();
    };

    var setBoutiqueTitle=function(){
        var data=new Array(),nowState,resTime,stime=['01','07','09','11','13','15','17','19','21','23','01'];
        var timeNow=systemTime;
        for(var i=0;i<stime.length-1;i++){
            var tHour=timeNow.getHours();
            data[i]={};
            data[i]["AreaStartTimeHM"]=stime[i]+":00";
            data[i]["AreaEndTimeHM"]=stime[i+1]+":00";
            data[i]["status"]=2;
            if(i!=stime.length-2){
                data[i]["AreaEndTime"]=new Date(timeNow.getFullYear()+"/"+(timeNow.getMonth()+1)+"/"+timeNow.getDate()+" "+data[i]["AreaEndTimeHM"]).getTime();
            }else{
                if(tHour<=23&&tHour>1){
                    var timeNow2=new Date(timeNow.getTime()+1000*24*60*60);
                    data[i]["AreaEndTime"]=new Date(timeNow2.getFullYear()+"/"+(timeNow2.getMonth()+1)+"/"+timeNow2.getDate()+" "+data[i]["AreaEndTimeHM"]).getTime();
                }else{
                    data[i]["AreaEndTime"]=new Date(timeNow.getFullYear()+"/"+(timeNow.getMonth()+1)+"/"+timeNow.getDate()+" "+data[i]["AreaEndTimeHM"]).getTime();
                }
            }
            if(stime[i]<=tHour&&stime[i+1]>tHour){
                data[i]["status"]=1;
                nowState=stime[i]+":00 场";
                jendtime=data[i]["AreaEndTime"];
            }else if((tHour<=24&&tHour>=23)||(tHour<1&&tHour>=0)){
                if(i==stime.length-2){
                    data[i]["status"]=1;
                    nowState=stime[i]+":00 场";
                    jendtime=data[i]["AreaEndTime"];
                }
            }else if(stime[i]>timeNow.getHours()){
                data[i]["status"]=0;
            }
        }
        var html=$("#template_ju").template({
            "data":data,
            "nowState":nowState
        });
        $("#template_ju").replaceWith(html);
        clone =  $("a[class*='jx_ju_today']");
        pclone = $("a[class*='jx_ju_today']").prev();
        $("a[class*='jx_ju_today']").remove();
    };

    //获取闪购信息
    var getFlashBuy=function(type,actionByTab){
        if(!document.getElementById("glist_"+type)){
            $("#loadingProduct2").show();
            var flashBuyType={
                "todaySale":"tjw49877194708",
                "women":"tjw39804314700",
                "children":"tjw49036654704",
                "men":"tjw49405104706",
                "life":"tjw49885884703",
                "outdoor":"tjw49404884705"
            };
            $.ajax({
                url: cgis.flashBuyBase+flashBuyType[type]+".js?t="+new Date().valueOf(),
                type: 'GET',
                dataType: 'jsonp',
                jsonpCallback:flashBuyType[type],
                error:setTimeoutInfo,
                success: function(data) {
                    getFlashBuyDetail(data.data.adList,type,actionByTab);
                }
            });
        }else{
            $(".sg_glist").hide();
            $("#glist_"+type).show();
            document.getElementById("select_tab").scrollIntoView();
        }
        config.url="http://"+window.location.host+window.location.pathname+"#"+type+"_T";
        setShare();
    };

    //通过闪购信息获取活动时间等信息
    var getFlashBuyDetail=function(dlist,type,actionByTab){
        var showTime=systemTime;
        var visibTimeS=new Date(showTime.getTime()-1000*60*60*24*5);
        var visibTimeS=visibTimeS.getFullYear()+""+(padLeft(visibTimeS.getMonth()+1))+""+padLeft(visibTimeS.getDate());
        var visibTimeE=showTime.getFullYear()+""+(padLeft(showTime.getMonth()+1))+""+padLeft(showTime.getDate());
        var arr="[",itemList=[];
        var ihtml="<ul class='sg_glist' id='glist_"+type+"'>";
        dlist.sort(function(a,b){
            return a.recmdRegName<b.recmdRegName?1:-1;
        });
        for(var i=0,j=0;i<dlist.length;i++){
            var temp=dlist[i]["qq"]||"";
            var itemDate=dlist[i]["recmdRegName"];
            if(itemDate>visibTimeS&&itemDate<=visibTimeE&&temp.indexOf('|')!=-1){
                var seedNum=dlist[i]["userCredit"]||"";
                temp=temp.split('|');
                seedNum=seedNum.trim()==""?((temp[0]*temp[1])%150+150):seedNum.trim();
                if(!isNaN(seedNum)){
                    arr+='{'+'"activeId":'+(temp[0]||"")+',"poolId":"'+(temp[1]||"")+'","seedNum":"'+seedNum+'"},';
                    dlist[i]["activeId"]=temp[0]||"";
                    dlist[i]["poolId"]=temp[1]||"";
                    itemList.push(dlist[i]);
                    var itemIDPre=type+"_"+(itemList.length-1)+"_";
                    ihtml+="<li><a class='sg_g' href=http://m.paipai.com/3/detail.shtml?ptag=20408.2.8&classId="+type+"&actId="+dlist[i]["activeId"]+"&areaId="+dlist[i]["poolId"]+"><div class='sg_g_img'>";
                    if(j<3){
                        ihtml+="<img id="+itemIDPre+"img"+" src='"+dlist[i]["image"]+"' alt='"+dlist[i]["recmdReason"]+"'> </div>";
                        j++;
                    }else{
                        ihtml+="<img id="+itemIDPre+"img"+" src='http://static.paipaiimg.com/fd/wd/h5/base/img/1x1.gif' data-original='"+dlist[i]["image"]+"' alt='"+dlist[i]["recmdReason"]+"'> </div>";
                    }
                    ihtml+="<div class='sg_g_info'>";
                    ihtml+="<p class='sg_g_tit'>"+dlist[i]["recmdReason"]+"</p>";
                    ihtml+="<div class='sg_g_bar'><p class='sg_g_row'><span class='sg_g_price fl'>";
                    ihtml+="<span id="+itemIDPre+"arealowestPrice"+" class='no_load_data'></span>起</span><span class='sg_g_sale fr'>已售<span id="+itemIDPre+"areasalesVolume"+" class='no_load_data'></span>件</span></p>";
                    ihtml+="<p class='sg_g_time no_load_data' id="+itemIDPre+"rdata"+"></p>";
                    ihtml+="</div></div></a></li>";
                }
            }
        }
        arr=arr.substring(0,arr.length-1)+"]";
        $.ajax({
            url: cgis.flashBuyDetail+arr,
            type: 'GET',
            dataType: 'jsonp',
            jsonpCallback:'callback',
            error:setTimeoutInfo,
            success: function(data) {
                setFlashBuy(itemList,data.data,type);
            }
        });
        ihtml+="</ul>";
        $("#glist_box").append(ihtml);
        if(document.getElementById("g_temp_box")) {
            $("#g_temp_box").remove();
        }
        $(".sg_glist").hide();
        $("#glist_"+type).show();
        $("#loadingProduct2").hide();
        if(actionByTab){
            document.getElementById("select_tab").scrollIntoView();
        }
        $("img").lazyload();
    };

    var setFlashBuy=function(dlist,data,type){
        var showTime=systemTime;
        for(var j=0;j<dlist.length;j++){
            for(var i=0;i<data.length;i++){
                if(dlist[j]["activeId"]==data[i]["activeId"]&&dlist[j]["poolId"]==data[i]["poolId"]){
                    var rdata;
                    if(data[i]["endTime"]<=0){
                        var itemEndTime=dlist[j]["recmdRegName"];
                        itemEndTime=new Date(itemEndTime.substr(0,4)+"-"+itemEndTime.substr(4,2)+"-"+itemEndTime.substr(6,2)+" "+"10:00:00")/1000;
                        data[i]["endTime"]=itemEndTime;
                    }else{
                        data[i]["endTime"]=parseInt(data[i]["endTime"])+60*60*10;
                    }
                    if(data[i]["beginTime"]<=0){
                        var itemBeginTime=dlist[j]["recmdRegName"];
                        itemBeginTime=new Date(itemBeginTime.substr(0,4)+"-"+itemBeginTime.substr(4,2)+"-"+itemBeginTime.substr(6,2)+" "+"10:00:00")/1000;
                        data[i]["beginTime"]=itemBeginTime;
                    }else{
                        data[i]["beginTime"]=parseInt(data[i]["beginTime"])+60*60*10;
                    }
                    rdata=getResTime(showTime,data[i]["endTime"]*1000);
                    if(parseInt(rdata.days)>0||parseInt(rdata.hours)>0||parseInt(rdata.minutes)>0||parseInt(rdata.seconds)>0){
                        var isToday=(showTime-data[i]["beginTime"]*1000)/(1000*60*60*24),itemIDPre=type+"_"+j+"_";
                        if(isToday>=0&&isToday<1){
                            $(itemIDPre+"img").before("<span class='sg_g_tag'>今日上新</span>");
                        }
                        var price="?"+parseFloat(data[i]["arealowestPrice"]/100).toFixed(2),dHtml="<span>"+rdata.days+"</span>天<span>"+rdata.hours+"</span>时<span>"+rdata.minutes+"</span>分";
                        $("#"+itemIDPre+"arealowestPrice").html(price);
                        $("#"+itemIDPre+"areasalesVolume").html(data[i]["areasalesVolume"]);
                        $("#"+itemIDPre+"rdata").html(dHtml);
                        break;
                    }
                }
            }
        }
    };

    var changeTab=function(dom,actionByTab){
        if(!dom.hasClass("on")){
            $("#select_tab .on").removeClass("on");
            dom.addClass("on");
            if(dom.index()==1){
                var hash=window.location.hash.replace("#","").replace("_T","");
                var flashBuyMap=["todaySale","women","children","men","life","outdoor"];
                if($.inArray(hash,flashBuyMap)==-1){
                    hash=$("#sg_lk_box .on").attr("name");
                }
                if(!hash||hash==""){
                    hash="todaySale";
                }
                if(hash.indexOf("?")!=-1){ //处理手Q分享后带的参数
                    hash=hash.split('?')[0];
                }
                hash=hash.replace("=",""); //处理微信下的错误
                getFlashBuy(hash,actionByTab);
                $("#sg_lk_box .on").removeClass("on");
                $("#sg_lk_box a[name='"+hash+"']").addClass("on");
                asyncReport("20408.2.6");
            }else{
                if(document.getElementById("template_ju")){
                    getBoutique(true);
                }
                config.url="http://"+window.location.host+window.location.pathname;
                setShare();
                document.getElementById("select_tab").scrollIntoView();
                asyncReport("20408.2.3");
            }
            $(".jx_selector").hide().eq(dom.index()).show();
        }
        checkWidth();
    };

    //获取剩余时间
    var getResTime=function(stime,etime){
        var rdata={},seconds=(etime-stime)/1000;
        var minutes = Math.floor(seconds / 60);
        var hours = Math.floor(minutes / 60);
        var days = Math.floor(hours / 24);
        rdata.days=padLeft(days);
        rdata.hours = padLeft(hours % 24);
        rdata.minutes = padLeft(minutes % 60);
        rdata.seconds = padLeft(Math.floor(seconds % 60));
        return rdata;
    };

    var padLeft=function(str) {
        if (str.toString().length >= 2)
            return str;
        else
            return ("0" + str);
    };

    var tabListResize=function(){
        var wval=$('.jx_ju_tab_item').width();
        if(wval!=0){
            $('.jx_ju_tab_item').height(wval);

        }
    };

    var checkWidth=function(){
        $("#sg_lk_box").removeClass("jx_sg_lk_hide");
        var hval=$("#sg_lk_box").height();
        if(hval>28){
            $("#list_contol").show();
            $("#sg_lk_box").addClass("jx_sg_lk_hide");
        }else{
            $("#list_contol").hide();
        }
    };

    var changItemIcon=function(){
        var obj=$("#ju_tab_box").find("i");
        if(obj.hasClass("jx_ju_act_show")){
            obj.removeClass("jx_ju_act_show");
        }else{
            obj.addClass("jx_ju_act_show");
        }
    };

    var changeEndTime=function(){
        if(jendtime){
            if(!sysResTime){
                sysResTime=(jendtime - systemTime.getTime())/1000;
            }
            var seconds = sysResTime;
            var minutes = Math.floor(seconds / 60);
            var hours = Math.floor(minutes / 60);
            hours = hours % 24;
            minutes = minutes % 60;
            seconds = Math.floor(seconds % 60);
            if (jendtime >= systemTime.getTime()) {
                if(document.getElementById("resTimeBox")){
                    $("#resTimeBox").html(padLeft(hours)+":"+padLeft(minutes)+":"+padLeft(seconds));
                }
            }
            sysResTime--;
        }
        systemTime=new Date(systemTime.getTime()+1000);
        setTimeout(changeEndTime, 1000);
    };

    var setShare=function(){
        share.shareApi(config);
    };

    var bind=function(){
        $("#select_tab").delegate("a","tap",function(){
            changeTab($(this),true);
        });
        $("#ju_tab_box").delegate("div[id='ju_act_box']","tap",function(){
            $("#ju_other_box").toggle();
            document.getElementById("select_tab").scrollIntoView();
            changItemIcon();
            tabListResize();
        }).delegate("a[class*='jx_ju_notstart'],a[class*='jx_ju_today']","click",function(event){
                event.preventDefault();
                if(pclone.length==0){
                    $("#jxTabList").prepend(clone);
                }else{
                    pclone.after(clone);
                }
                clone = $(this);
                pclone = $(this).prev();
                $(this).remove();
                formatBoutique($(this).attr("timestamp"),false,true);
                changItemIcon();
                $("#ju_other_box").toggle();
                asyncReport("20408.2.4");
            });
        $("#sg_lk_box").delegate("a","tap",function(){
            $("#sg_lk_box .on").removeClass("on");
            getFlashBuy($(this)[0].name,true);
            $(this).addClass("on");
            $("#s_gou_Tab")[0].href="#"+$(this)[0].href.split("#")[1];
            asyncReport("20408.2.7");
        }).delegate("i","tap",function(){
                var obj=$("#sg_lk_box");
                if(obj.hasClass("jx_sg_lk_hide")){
                    obj.removeClass("jx_sg_lk_hide")
                }
                else{
                    obj.addClass("jx_sg_lk_hide");
                }
            });
        window.onresize =function(){
            //touchRoll.resize();
            tabListResize();
            checkWidth();
        };
    };

    var isFlashBuyHash=function(){
        var hash=window.location.hash.replace("#","").replace("_T","");
        if(hash.indexOf("?")!=-1){ //处理手Q分享后带的参数
            hash=hash.split('?')[0];
        }
        hash=hash.replace("=",""); //处理微信下的错误
        var flashBuyMap=["todaySale","women","children","men","life","outdoor"];
        if($.inArray(hash,flashBuyMap)!=-1){
            changeTab($("#s_gou_Tab"));
            return true;
        }
        return false;
    };

    //主动上报ptag  ------start
    var asyncReport=function(ptag){
        setTimeout(function(){
            $countRd(ptag);
        },30);
    }

    var $countRd=function(rd,random){
        var arrRd=rd.split("."),
            rand = random||100;

        var jsrdUrl="http://service.paipai.com/cgi-bin/ping?u=http://jsrd.paipai.com&fu=http://jsrd.paipai.com%3FPTAG%3D"+rd+"&resolution=1024*768";
        //添加用户行为支持
        jsrdUrl+="&fpageId="+arrRd[0]+"&fdomainId="+arrRd[1]+"&flinkId="+arrRd[2];
        if(/paipai.com|buy.qq.com|wanggou.com/.test(document.domain)){
            //用于uv统计,参数u，fu，resolution无意义但不能为空
            if(Math.random()<=rand/100){
                $report(jsrdUrl);
            }
        }else{
            //外站点击统计
            $report($makeRd(rd));
            //$report(jsrdUrl);
        }
    }
    var $loadUrl=function(o) {
        o.element = o.element || 'script';
        var el = document.createElement(o.element);
        el.charset = o.charset || 'utf-8';
        o.onBeforeSend&&o.onBeforeSend(el);//发请求之前调用的函数
        el.onload = el.onreadystatechange = function() {
            if(/loaded|complete/i.test(this.readyState) || navigator.userAgent.toLowerCase().indexOf("msie") == -1) {
                o.onLoad&&o.onLoad();//
                clear();
            }
        };
        el.onerror = function(){
            clear();
        };
        el.src = o.url;
        document.getElementsByTagName('head')[0].appendChild(el);

        var clear=function(){
            if(!el){return ;}
            el.onload = el.onreadystatechange = el.onerror = null;
            el.parentNode&&(el.parentNode.removeChild(el));
            el = null;
        }
    };
    var $makeRd=function(rd,url){
        var url=url||'http://www.paipai.com/rd.html',
            arrRd=rd.split(".");
        return "http://service.paipai.com/cgi-bin/go?pageId="+arrRd[0]+"&domainId="+arrRd[1]+"&linkId="+arrRd[2]+ "&url=" + escape(url);
    };
    var $report=function(url){
        $loadUrl({
            'url':url+((url.indexOf('?')==-1)?'?':'&')+Math.random(),'element':'img'
        });
    }
    //主动上报ptag  ------end

    module.exports.init=function(){
        //login.checkAndRedirect();
        getTimeNow();
        //getGroupInfo();
        if(!isFlashBuyHash()){
            getBoutique();
        }
        setShare();
        bind();
        follow.init();
    };
});
require(["section/getIndex"],function(index){
    index.init();
});
define("index", function(){});

