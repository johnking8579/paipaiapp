(function() {
      var gid="",uin="",state="",doc=document,nowtime="",isSuccess=false,xhr,dataBuyNum="";
      var curHref=window.location.href;
      var initHash=window.location.hash;
      var randomYaohe=['帮你团！你是请我吃饭还是以身相许？','参团不带我？辣怎么行！','矮油，这货不错，快来一起买！',
                       '帮你一把，怎么报答我？自觉联系我。','这么给力的，你还上哪儿找切。','我来审查你的口味！',
                       '啧啧，团长都剁手了，赶紧帮一把。','价钱合适量又足，谢谢团长！','搬板凳，排排坐，跟着团长拍便宜。',
                       '一直潜水，今日得以表白: 团长，么么哒！','这个也团啊？好吧，坚定跟着团长走！'];
      //获取uin
      function getQQ(){
          var uin=getCookie("wg_uin") || getCookie("p_uin") || getCookie("uin");
          return uin;
      }
      //从url获取团ID
      var getGID=function(){
          var url=location.search;
          if(url.indexOf("?")!=-1)
          {
              url=url.substr(1);
              url = url.split("&");
              for(var i=0;i<url.length;i++)
              {
                  var urlVal=url[i].split("=");
                  if(urlVal[0]=="gid")
                  {
                      return gid=urlVal[1];
                  } 
              }
          }

      }();
      //s sku功能
      var ajax_getSkuInfo =function(commodityId,oData){
          xhr=$.ajax({
            type: 'GET',
            url: 'http://b.paipai.com/groupservice/createGroup?ic='+commodityId+'&querysku=1',
            dataType: 'jsonp',
            success: function(data){
              if(data.ret =="0"){
                if(data.StockList_StockNum==1){//进入有可能是multisku=2,即无法判断
                  setLocalItem(oData,0);
                  if(getQQ() && parseInt(getQQ().replace(/^o/,""),10)%2){
                    window.location.href=localStorage.tuan_buyurl+"&speak="+encodeURI(randomYaohe[Math.floor(Math.random()*(11-0)+0)])+(oData.isVTuan?"&isdav=1":"");
                   }else{
                    window.location.href="tuan_speak.html?_wv=1"+(oData.isVTuan?"&isdav=1":"");
                   }
                }else{
                  initSku(data);
                  $('#sku_buyBtn').on('click',function(){
                    if(checkSku()){
                      setLocalItem(oData,1);
                      window.location.hash =initHash;
                      if(getQQ() && parseInt(getQQ().replace(/^o/,""),10)%2){
                      window.location.href=localStorage.tuan_buyurl+"&speak="+encodeURI(randomYaohe[Math.floor(Math.random()*(11-0)+0)])+(oData.isVTuan?"&isdav=1":"");
                     }else{
                      window.location.href="tuan_speak.html?_wv=1"+(oData.isVTuan?"&isdav=1":"");
                     }
                    }
                  });
                  window.location.hash ="#sku"+Math.random();
                  window.onhashchange =function(){
                    if(window.location.hash !=initHash){
                      $('#sku_dom').show();
                      $('.fixopt,.support,.pp,.topStateWrap,.step,.mod_footer').hide();
                    }else{
                      $('#sku_dom').hide();
                      $('.fixopt,.support,.pp,.topStateWrap,.step,.mod_footer').show();
                    }
                  };
                }
              }else{ 
                alert("系统繁忙，去看看别的～");
                window.location.href="tuan_list.html?_wv=1";
              }
            },
            error: function(xhr, type){ 
              alert("系统繁忙，去看看别的～");
              window.location.href="tuan_list.html?_wv=1";
            }
          });
        };
        
        //初始化sku组件
        var initSku =function(__obj){
          window.cmdtySku =new propSelector({
            domId: 'propertyDiv',           //存放多个sku的容器div的id
            activeClass: 'sku_option_selected',   //选择相应sku属性时候添加的类class
            disableClass: 'sku_option_disabled',  //未选择相应sku属性时候添加的类class
            propGroupClass: 'sku_attr',     //存放单个sku中所有属性值的容器div的class
            stocklist : __obj.stocklist,    //存放sku信息的数组
            stockData : __obj.StockList_StockString,  //所有sku对应的字符串信息
            onSkuEmpty:function(){},
            onSelected:function(propData){
              window.skuChoose = propData;
            }
          });
          window.skuChoose = window.cmdtySku.getSelectedProp();
        };
        //检测sku是否选中
        var checkSku =function(){
          var propData = window.skuChoose, emptyProp = [];
          for (var key in propData.propMap) {
            if (!propData.propMap[key]) { //没有选中的
              emptyProp.push("“" + key + "”");
            }
          }
          if (emptyProp.length > 0) {
            $('.ftips_cnt').text('请选择:'+emptyProp.join('/'));
            $('.ftips').show();
            window.setTimeout(function(){
              $('.ftips').hide();
            },700);
            return false;
          }
          return true;
        };
        //e sku功能
        //本地存储
        function setLocalItem(data,multiP){
              var url,hrefVal="&tuan=1&itemid="+data["itemID"]+"&uin="+uin+"&groupnum="+data["ptotal"]+"&gid="+gid+(data.isVTuan?"&isdav=1":"");
              if(multiP){
                var propData = window.skuChoose, skuPro=encodeURI(propData.prop.join('|'));
                url = 'http://m.paipai.com/m2/p/cart/order/s_confirm.shtml?ptag=20400.3.2'+hrefVal+'&commlist=' +[commodityId, skuPro, dataBuyNum, propData.skuid].join(',')+'&_wv=1';
                localStorage.tuan_sku=propData.prop.length==1?propData.prop[0]:propData.prop.join('|') || "";
              }else{
                var sku=data["SKU"].trim().split("|"),skuID=sku.splice(0,1)||"",skuAttr=encodeURI(sku.join("|")||"");
                url="http://m.paipai.com/m2/p/cart/order/s_confirm.shtml?ptag=20400.3.2&_wv=1"+hrefVal+"&commlist="+data["itemID"]+","+skuAttr+","+data["buy_num"]+","+skuID;
                localStorage.tuan_sku=sku.join("|")||"";
              }
              localStorage.tuan_buyurl=url;
              localStorage.tuan_role=data.isVTuan?3:1;
              localStorage.tuan_productInfo=data.img+"|"+data.title+"|"+data.ptotal+"|"+data.saleprice;
          }
        //跳转到吆喝页
        function gotoYaohe(data){
          if(data.multiSku!=0){
            ajax_getSkuInfo(data.itemID,data);
          }else{
            setLocalItem(data,0);
            if(getQQ() && parseInt(getQQ().replace(/^o/,""),10)%2){
                    window.location.href=localStorage.tuan_buyurl+"&speak="+encodeURI(randomYaohe[Math.floor(Math.random()*(11-0)+0)])+(data.isVTuan?"&isdav=1":"");
                   }else{
                    window.location.href="tuan_speak.html?_wv=1"+(data.isVTuan?"&isdav=1":"");
                   }
          }
        }
      //分享动作提示
      var setShareTipsForm=function(){
          $countRd("20400.3.1");
          $("#shareTipBox").show();
      };
      var hideShareTipsForm=function(event){
          event.preventDefault();
          $("#shareTipBox").hide();
      };
      //昵称decode
       var decodeNick=function(nick){
          try{
            return decodeURIComponent(nick).replace(/\+/g," ");
          }catch(e){
            return nick;
          }
        };
      function isWx(){
          var isWX = window.WeixinJSBridge;
          if(!isWX){
               var ua = navigator.userAgent.toLowerCase();
               isWX = ua.match(/micromessenger/)?true : false;
          }
          return isWX;
      }
      function isMQQUserAgent() {
          if (/qq\/([\d\.]+)*/i.test(navigator.userAgent)) {
              return true;
          }
          return false;
      }
      //微信头像使用小头像
      function smallImg(url){
        if(url && url.match(/^http:\/\/wx\.qlogo\.cn/)!=null){
          url=url.replace(/\/0$/,"/46");
        }
        if(!url){
          url="http://static.paipaiimg.com/fd/qqpai/tuan/img/avatar_3_64.png";
        }
        return url;
      }
      //判断是否已经登录，
      var check=function(){
          plogin()?"":plogin(window.location.href);
          uin=getCookie("wg_uin")||getCookie("p_uin")||getCookie("uin");
          uin=uin?parseInt(uin.replace("o",""),10):"";
      }
      //补齐工具
      var padLeft=function(str) {
          if (str.toString().length >= 2)
              return str;
          else
              return ("0" + str);
      }
      //计时器
      var changeEndTime=function(){
                  var endtime=window.endtime;
                  var restime = endtime - nowtime;
                  var seconds = restime / 1000;
                  var minutes = Math.floor(seconds / 60);
                  var hours = Math.floor(minutes / 60);
                  hours = hours % 24;
                  minutes = minutes % 60;
                  seconds = Math.floor(seconds % 60); 
                  if (endtime >= nowtime) {
                      $("#ti_time_hour").html(padLeft(hours));
                      $("#ti_time_min").html(padLeft(minutes));
                      $("#ti_time_sec").html(padLeft(seconds));
                      nowtime += 1000;
                      setTimeout(changeEndTime, 1000);
                  }
      };

      var setCookie=function(name, value, expires, path, domain, secure) {
        //写入COOKIES
        var exp = new Date(), expires = arguments[2] || null, path = arguments[3] || "/", domain = arguments[4] || null, secure = arguments[5] || false;
        expires ? exp.setMinutes(exp.getMinutes() + parseInt(expires)) : "";
        document.cookie = name + '=' + escape(value) + ( expires ? ';expires=' + exp.toGMTString() : '') + ( path ? ';path=' + path : '') + ( domain ? ';domain=' + domain : '') + ( secure ? ';secure' : '');
      }

      var getCookie=function(name) {
          var reg = new RegExp("(^| )" + name + "(?:=([^;]*))?(;|$)"),
              val = doc.cookie.match(reg);
          return val ? (val[2] ? unescape(val[2]) : "") : null;
      }
      var getQuery=function(name, url) {
          var u = arguments[1] || window.location.search,
              reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"),
              r = u.substr(u.indexOf("\?") + 1).match(reg);
          return r != null ? r[2] : "";
      }

      var plogin=function (rurl){
        var ua = navigator.userAgent.toLowerCase();
        var isWX = window.WeixinJSBridge;
        var isMQQ = false;
        if(!isWX){isWX = ua.match(/micromessenger/)?true : false;}
        if(/qq\/([\d\.]+)*/.test(ua)){isMQQ = true;}
        if(!rurl){
          if(isMQQ){
            return (getCookie("skey") && getCookie("uin"))?true:false;
          }else{
            return (getCookie("wg_skey") && getCookie("wg_uin") || getCookie("p_skey") && getCookie("p_uin") || getCookie("skey") && getCookie("uin"))?true:false;
          }
        }
        if(isWX){
          window.location.href = 'http://b.paipai.com/mlogin/tws64/m/wxv2/Login?appid=1&rurl=' + encodeURIComponent(rurl);
        }else if(isMQQ){
          if(mqq && mqq.data){
            mqq.data.getUserInfo(function(o){
              window.location.href = rurl;
            });
          }
        }else{
          window.location.href = 'http://b.paipai.com/mlogin/tws64/m/h5v1/cpLogin?rurl=' + encodeURIComponent(rurl) + '&sid=' + getCookie('sid') + '&uk=' + getCookie('uk');
        }
      }
      //S 主动上报
      var $countRd=function(rd,random){
        var arrRd=rd.split("."),
          rand = random||100;

        var jsrdUrl="http://service.paipai.com/cgi-bin/ping?u=http://jsrd.paipai.com&fu=http://jsrd.paipai.com%3FPTAG%3D"+rd+"&resolution=1024*768";
        //添加用户行为支持
        jsrdUrl+="&fpageId="+arrRd[0]+"&fdomainId="+arrRd[1]+"&flinkId="+arrRd[2];
        if(/paipai.com|buy.qq.com|wanggou.com/.test(document.domain)){
          //用于uv统计,参数u，fu，resolution无意义但不能为空
          if(Math.random()<=rand/100){
             $report(jsrdUrl);
          }
        }else{
          //外站点击统计
          $report($makeRd(rd));
          //$report(jsrdUrl);
        }
      }
      var $loadUrl=function(o) {
          o.element = o.element || 'script';
        var el = document.createElement(o.element);
        el.charset = o.charset || 'utf-8';
        o.onBeforeSend&&o.onBeforeSend(el);//发请求之前调用的函数
        el.onload = el.onreadystatechange = function() {
          if(/loaded|complete/i.test(this.readyState) || navigator.userAgent.toLowerCase().indexOf("msie") == -1) {
                    o.onLoad&&o.onLoad();//
                          clear();
          }
        };
        el.onerror = function(){
          clear();
        };
        el.src = o.url;
        document.getElementsByTagName('head')[0].appendChild(el);
        
        var clear=function(){
          if(!el){return ;}
          el.onload = el.onreadystatechange = el.onerror = null;
          el.parentNode&&(el.parentNode.removeChild(el));
          el = null;
        }
      };
      var $makeRd=function(rd,url){
        var url=url||'http://www.paipai.com/rd.html',
          arrRd=rd.split(".");
        return "http://service.paipai.com/cgi-bin/go?pageId="+arrRd[0]+"&domainId="+arrRd[1]+"&linkId="+arrRd[2]+ "&url=" + escape(url);
      };
      var $report=function(url){
        $loadUrl({
          'url':url+((url.indexOf('?')==-1)?'?':'&')+Math.random(),'element':'img'
        });
      }
      //E 主动上报
      //主动终止ajax请求
      var setTimeoutInfo=function(a,b,c)
      {
             if(xhr){
                xhr.abort();
                xhr=null;
             }
             $(".H5_con").hide();
             $("#sorryBox").show();
             footFoucsPPY(3);
      };
      //遍历工具--适用于数组和对象
      function eachProp(obj, func) {
          var prop;
          for (prop in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                  if (func(obj[prop], prop)) {
                      break;
                  }
              }
          }
      }
      //浏览器主动分享工具
      function shareContent(data){
        var contentPool=["大神开场了，快快跟随冒泡踩场吧","达人难得有空吆喝聚友，一起来聚聚","开嗓吆喝，喊你凑桌一起玩一玩","开摊吆喝了，速来围观Ta的品位吧"];
        var ranContent=contentPool[Math.floor(Math.random()*(4-0)+0)];
        var shareContent=data.mname.replace(/\s/g,"")+" "+ranContent+"！"+data.title+"仅售"+data.saleprice+"元";
        paipai_h5_share.init({
            hideQQ: false,
            hideQzone: false,
            hideWX: false,
            hideWXFriend: false,
            hideTqq: false,
            qq: {
                title: (data.isVTuan?"超级团长":"")+data["mname"]+"发起了团购：",
                desc: "【"+data["ptotal"]+"人成团"+data["saleprice"]+"元】"+(data["title"].length>16?(data["title"].substring(0,16)+"..."):data["title"])+"，跟我组团抢，超划算，信我没错！",
                share_url: curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),
                ptag: "12475.18.1",
                image_url: data["img"]
            },
            qzone: {
                title: (data.isVTuan?"超级团长":"")+data["mname"]+"发起了团购：",
                desc: "【"+data["ptotal"]+"人成团"+data["saleprice"]+"元】"+(data["title"].length>16?(data["title"].substring(0,16)+"..."):data["title"])+"，跟我组团抢，超划算，信我没错！",
                share_url: curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),
                ptag: "12475.18.2",
                image_url: data["img"]
            },
            weixin: {
                title: (data.isVTuan?"超级团长":"")+data["mname"]+"发起了团购：",
                desc: shareContent,
                share_url: curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),
                ptag: "12475.18.3",
                image_url: data["img"]
            },
            weixinFriend: {
                title: (data.isVTuan?"超级团长":"")+data["mname"]+"发起了团购：",
                share_url: curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),
                desc: "",
                ptag: "12475.18.4",
                image_url: data["img"]
            },
            tqq: {
                title: (data.isVTuan?"超级团长":"")+data["mname"]+"发起了团购：",
                desc: shareContent,
                share_url: curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),
                ptag: "12475.18.5",
                image_url: data["img"]
            }
        });
    }
    //网易分享组件
    function wyShare(options){
      if(navigator.userAgent.indexOf("NewsApp")<0){
        return;
      }
      var my = arguments.callee,
      $section = $("#__newsapp_container"),
      wyScriptUrl = "http://img3.cache.netease.com/utf8/3g/libs/NewsAppClient.js?"+(new Date().getTime()),
      tpl = [
          '<div id="__newsapp_sharetext">{text}{linkUrl}</div>',
          '<div id="__newsapp_sharephotourl">{photoUrl}</div>',
          '<div id="__newsapp_sharewxtitle">{title}</div>',     
          '<div id="__newsapp_sharewxurl">{linkUrl}</div>',
          '<div id="__newsapp_sharewxtext">{text}</div>',
          '<div id="__newsapp_sharewxthumburl">{photoUrl}</div>'
        ].join(""),
      defaultOptions = {
        title : "标题",
        linkUrl   : "",
        photoUrl : "",
        text     : "",
        success : function(){},
        failed  : function(){} 
      };
      $.extend(defaultOptions,options);
      var newTpl = tpl.replace(/\{([^\}]+)\}/g,function(all,$1){
          return defaultOptions[$1];
        }); 
      $section.html(newTpl);
      if(my.isCreate){
        options.isFire
        &&NewsAppClient.share({
          success : defaultOptions.success,
          failed  : defaultOptions.failed
        });

      }else{
        $('<script>')
          .appendTo(document.body)
          .bind("load",function(){
            if(!window["NewsAppClient"]){
              return;
            }
            my.isCreate = true;
            
            options.isFire
            &&NewsAppClient.share({
              success : defaultOptions.success,
              failed  : defaultOptions.failed
            });
          })
          .bind("error",function(){
          })
          .attr("src",wyScriptUrl);
      }
    }
      //事件捆绑
      function bindEvent(data){
        $("#bottomBtn1").click(function(){
          if(isMQQUserAgent() || isWx()){
            setShareTipsForm();
            $("#shareTipBox").click(function(e){
              hideShareTipsForm(e);
            });
          }else if(navigator.userAgent.indexOf("NewsApp")>=0){
            wyShare({
                title    : "【拍拍-拍便宜】",
                linkUrl  : curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),
                photoUrl : data.img,
                text     : data.mname+"开摊吆喝了，速来围观Ta的品位吧！"+data.title,
                isFire   : true
              });
          }else{
            //shareContent(data);
              var ua = navigator.userAgent.toLowerCase();
              if(ua.indexOf("paipaiapp")>=0){
                  var shareinfo = {
                      title: (data.isVTuan?"超级团长":"")+data["mname"]+"发起了团购：",
                      desc: "【"+data["ptotal"]+"人成团"+data["saleprice"]+"元】"+(data["title"].length>16?(data["title"].substring(0,16)+"..."):data["title"])+"，跟我组团抢，超划算，信我没错！",
                      share_url: curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),
                      image_url: data["img"]
                  }
                  location.href="paipai://sharepaicheap?1=1&title="+encodeURI(shareinfo.title)+"&desc="+encodeURI(shareinfo.desc)+"&share_url="+shareinfo.share_url+"&image_url="+shareinfo.image_url;
              }else{
                  shareContent(data);
              }
          }
          return false;
        });
        $("#bottomBtn2").click(function(event){
          event.preventDefault();
          gotoYaohe(data);
          return false;
        });
      }
      //S observer工具
      //observer
      function ObserverList(){
        this.observerList = [];
      }
       
      ObserverList.prototype.add = function( obj ){
        return this.observerList.push( obj );
      };
       
      ObserverList.prototype.count = function(){
        return this.observerList.length;
      };
       
      ObserverList.prototype.get = function( index ){
        if( index > -1 && index < this.observerList.length ){
          return this.observerList[ index ];
        }
      };
      function Observer(){
        this.update = function(){
          // ...
        };
      }
      //subject
      var domObserverList=new ObserverList();
      domObserverList.notify=function(data){
        var observerLen=domObserverList.count();
        for(var i=0; i < observerLen; i++){
          domObserverList.get(i).update(data);
        }
      }
      //制造observer
      var observerObj={
        "topState":null,//顶部状态
        "tuanDetail":null,//团信息模块
        "headerImg":null,//头像模块
        "tuanState1":null,//团状态描述1：还有多少人
        "tuanState2":null,//团状态描述2：倒计时或发货提示语
        "yaoheList":null,//吆喝列表
        "seller":null,//卖家信息
        "hotTuan":null,//热门团
        "bottom":null,//公共底部
        "button":null//按钮
      };
      for(var i in observerObj){
        observerObj[i]=new Observer();
        domObserverList.add(observerObj[i]);
      }
      observerObj.topState.update=function(data){
        //ubstate:1当前用户是挤爆团成员 0当前用户非挤爆团成员
        if((data.tstate=="sales" && data.ustate=="succ") || (data.tstate=="succ" && data.ubstate==1)){
          $(".topStateWrap").addClass("tips_succ");
          $(".tips_tit").html(data["ismaster"]?"开团成功":data.ubstate==1?"成功挤爆该团":"参团成功");
          $(".tips_extra b").html(data["shopName"]?data["shopName"]:"");
          if(data.isVTuan && data["ismaster"]){$(".tips_extra").html("赶快带领你的粉丝发现更多惊喜吧！")};
        }else if(data.tstate=="sales" && data.ustate=="noin"){
          $(".topStateWrap").addClass("tips_notice tips_notice2");
          $(".tips_tit").html(data.mname+(data.isVTuan?"的超级团":"开摊吆喝")+"，快来围观TA的品位");
          $(".tips_txt").attr("class","tips_opt").html('<a href="http://app.paipai.com/takeCheap/tuan_list.html?_wv=1" class="tips_btn1">品味太差，我去组织吆喝<span class="tips_bar"></span>看更多</a><a href="#" class="tips_btn2" id="goShareBtn">'+(data.isVTuan?"晒晒土豪的尊贵超级团":"品味还行，帮忙聚下人气")+'<span class="tips_bar"></span>去分享</a>');
          $("#goShareBtn").click(function(){
            if(isMQQUserAgent() || isWx()){
              setShareTipsForm();
              $("#shareTipBox").click(function(e){
                hideShareTipsForm(e);
              });
            }else if(navigator.userAgent.indexOf("NewsApp")>=0){
              wyShare({
                  title    : "【拍拍-拍便宜】",
                  linkUrl  : curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),
                  photoUrl : data.img,
                  text     : data.mname+"开摊吆喝了，速来围观Ta的品位吧！"+data.title,
                  isFire   : true
                });
            }else{
              //shareContent(data);
                var ua = navigator.userAgent.toLowerCase();
                if(ua.indexOf("paipaiapp")>=0){
                    var shareinfo = {
                        title: (data.isVTuan?"超级团长":"")+data["mname"]+"发起了团购：",
                        desc: "【"+data["ptotal"]+"人成团"+data["saleprice"]+"元】"+(data["title"].length>16?(data["title"].substring(0,16)+"..."):data["title"])+"，跟我组团抢，超划算，信我没错！",
                        share_url: curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),
                        image_url: data["img"]
                    }
                    location.href="paipai://sharepaicheap?1=1&title="+encodeURI(shareinfo.title)+"&desc="+encodeURI(shareinfo.desc)+"&share_url="+shareinfo.share_url+"&image_url="+shareinfo.image_url;
                }else{
                    shareContent(data);
                }
            }
            pointReport("ptag",points.ptag[2],"comodity="+localStorage["comodity"]);
            return false;
          });
        }else if(data.tstate=="succ"){
          if(data.ustate=="succ"){
            $(".topStateWrap").addClass("tips_succ tips_succ2");
            $(".tips_tit").html("团购成功");
            $(".tips_txt").html("我们会尽快为您发货，请耐心等待");
          }else if(data.tbstate!="true" && data.ustate=="noin"){
            $(".topStateWrap").addClass("tips_notice tips_notice2");
            $(".tips_tit").html(data.isVTuan?"此超级团长魅力太大，挤不进团":(data.mname+"开团如此火爆，换火箭都没挤上团"));
            $(".tips_txt").attr("class","tips_opt").html('<a href="http://app.paipai.com/takeCheap/tuan_detail.html?_wv=1&comodityId='+data.itemID+'" class="tips_btn2">向大神学习，'+(data.isVTuan?"开团升级":"去开团页超越Ta")+'</a>');
          }else{
            $(".topStateWrap").addClass("tips_notice tips_notice2");
            $(".tips_tit").html(data.mname+(data.isVTuan?"的超级团也太抢手了，凑热闹来晚了":"开团如此火爆，坐飞机都没有追上"));
            $(".tips_txt").attr("class","tips_opt").html("<a ptag='20400.9.1' class='tips_btn2' id='goJibao' href='http://app.paipai.com/takeCheap/tuan_jibaoresult.html?_wv=1&isvtuan="+data.isVTuan+"&ptotal="+data.ptotal+"&dataBuyNum="+dataBuyNum+"&uin="+uin+"&comodityId="+commodityId+"&gid="+gid+"'>"+(data.isVTuan?"求让我进次超级团":"求再给我一次机会")+"，挤爆TA</a>");
            $("#goJibao").on("click",function(){
              setLocalItem(data,0);
            });
          }
        }else{
          if(data.ustate=="succ"){
            $(".topStateWrap").addClass("tips_err");
            $(".tips_tit").html("组团失败，退款中");
            $(".tips_txt").html("<p>时间到了，小伙伴不给力，组团失败了。</p><p>您的已付款项将在3-5天内退至您的支付账户，请留意相关信息</p>");
          }else{
            $(".topStateWrap").addClass("tips_notice");
            $(".tips_tit").html("您还没有参加这个团");
            $(".tips_txt").html("");
          }
        }
      };
      observerObj.tuanDetail.update=function(data){
        if(data.tstate=="succ"){
          $(".tm").addClass("tm_succ");
        }else if(data.tstate=="failed"){
          $(".tm").addClass("tm_err");
        }
        $(".goItemPage").attr("href","http://b.paipai.com/itemweb/item?scence=101&ic="+data.itemID+"&tuanflag=1&_wv=1");
        $(".tuanDetailWrap img").attr("src",data.img?data.img:"");
        $(".tuanDetailWrap .td_name").html(data.title?data.title:"拍拍商品");
        $(".tuanTotal").html(data.ptotal);
        $(".td_mprice b").html(data.saleprice);
        if(data.tstate=="sales"){
          xhr=$.get('http://b.paipai.com/groupservice/groupplayers?productId='+data.itemID,function(json){
            if(json && json.ret==0){
              $(".td_num").html(data.tstate=="sales"?("已参团："+json.data.groupplayers+"人"):"");
              localStorage.tuan_num=json.data.groupplayers;
            }
          },'jsonp');
        }
        /*var sku=data["SKU"].trim().split("|"),skuID=sku.splice(0,1)||"",skuAttr=sku.join("|")||"";
        if(skuAttr){
          $(".td_attr span").html(skuAttr);
        }else{
          $(".td_attr").hide();
        }*/
        $(".td_attr").hide();
      };
      observerObj.headerImg.update=function(data){
        if(data.userlist==null){return false;}
        var userlistArr=data.userlist.slice(1),ulLen=userlistArr.length,memberData='[';
        var withDotted=false,promoteUserId,promoteUserType;
        if(data.promoteprice != 0){
                eachProp(userlistArr,function(ele){
                  if(ele["pprcieuser"]){
                    promoteUserId=ele["userid"];
                    promoteUserType=ele["usertype"];
                    return true;
                  }
                });
              };
        var newHeaderImg=function(){};
        //decorator pattern
        newHeaderImg.decorators={
          bigVTuan:{
            begin:function(){
              if(data.ptotal>6 || ulLen>5){
                $(".pp_users").addClass("pp_users2");
              }
              if(data.ptotal>9 || ulLen>8){withDotted=true;}//大于8人团或挤爆后超过8人
              $(".pp_users").addClass(".pp_users2").before('<div class="pp_vipuser"><img src="'+smallImg(data.mimg)+'" alt=""><span class="pp_vipuser_tit">团长</span></div>');
              if(data.promoteprice == 0 && data.tstate=="sales"){
                  $(".pp_vtips").html("还有机会获得超值幸运价！").show();
                }
              this.prepareImgNum();
            },
            prepareImgNum:function(){
              if(ulLen==0){
                this.callbackFun({list:[]});
                return false;
              }
              if(ulLen>8){
                this.prepareData(6);
              }else{
                if(data.ptotal>9){
                  this.prepareData(ulLen<=6?ulLen:6);
                }else{
                  this.prepareData(ulLen);
                }
              }
            },
            prepareData:function(n){
              eachProp(userlistArr,function(ele,index){
                  memberData+='{"userid":"'+ele["userid"]+'",'+'"usertype":'+ele["usertype"]+'},';
                  if(index>=(n-1)){return true;}
                });
                if(ulLen>8 && ulLen>=data.ptotal-1){
                  memberData+='{"userid":"'+userlistArr[userlistArr.length-1]["userid"]+'",'+'"usertype":'+userlistArr[userlistArr.length-1]["usertype"]+'},';
                }
                memberData=memberData.substring(0,memberData.length-1);
                memberData+=']';
              this.requestImg();
            },
            callbackFun:function(json){
              var item=json.list,itemLen=item.length,promoteTagged=false;
              for(var i=0;i<(withDotted?7:(data.ptotal<(ulLen+1)?ulLen:data.ptotal-1));i++){
                if(withDotted && i==6){
                  $(".pp_users").append('<span class="pp_users_item pp_users_dot"><i></i>');
                }
                if(item[i]){
                    if(promoteUserId != undefined && item[i]["userid"]==promoteUserId && !promoteTagged){
                      promoteTagged=true;
                      var proName=item[i]["name"]?decodeNick(item[i]["name"]):"拍拍用户";
                      proName=proName.replace(/\s/g,"");
                      if(proName.length>4){
                        proName=proName.substring(0,3)+"...";
                      }
                      $(".pp_vtips").html("恭喜"+proName+"获得"+(parseFloat(data["promoteprice"])/100).toFixed(2)+"元幸运价购买资格！").show();
                      $(".pp_users").append('<a href="#" class="pp_users_item pp_users_normal pp_users_sale"><img src="'+smallImg(item[i]["imgurl"])+'" alt=""><span>优惠</span></a>');
                    }else{
                      $(".pp_users").append('<a href="#" class="pp_users_item pp_users_normal"><img src="'+smallImg(item[i]["imgurl"])+'" alt=""></a>');
                    }
                  }else{
                    $(".pp_users").append('<span class="pp_users_item pp_users_blank"><img src="http://static.paipaiimg.com/fd/qqpai/tuan/img/avatar_4_64.png" alt=""></span>');
                  };
              };
              if(!promoteTagged && promoteUserId){
                var extraData='[{"userid":"'+promoteUserId+'",'+'"usertype":'+promoteUserType+'}]';
                xhr=$.get("http://b.paipai.com/userinfo/getusernickandhead?userlist="+extraData,function(json){
                  if(json.retCode==13){
                    plogin(window.location.href);
                  }else{
                    var proName=json.list[0]["name"]?decodeNick(json.list[0]["name"]):"拍拍用户";
                    proName=proName.replace(/\s/g,"");
                      if(proName.length>4){
                        proName=proName.substring(0,3)+"...";
                      }
                    $(".pp_vtips").html("恭喜"+proName+"获得"+(parseFloat(data["promoteprice"])/100).toFixed(2)+"元幸运价购买资格！").show();
                  }
                },'jsonp');
              }
            }
          }
        };
        newHeaderImg.prototype.begin=function(){
            if(data.ptotal>5 || ulLen>4){
              $(".pp_users").addClass("pp_users2");
            }
            if(data.ptotal>8 || ulLen>7){withDotted=true;}//大于8人团或挤爆后超过8人
            $(".pp_users").append('<a href="#" class="pp_users_item pp_users_normal"><img src="'+smallImg(data.mimg)+'" alt=""></a>');
            this.prepareImgNum();
          };
        newHeaderImg.prototype.prepareImgNum=function(){
            if(ulLen==0){
              this.callbackFun({list:[]});
              return false;
            }
            if(ulLen>7){
              this.prepareData(5);
            }else{
              if(data.ptotal>8){
                this.prepareData(ulLen<=5?ulLen:5);
              }else{
                this.prepareData(ulLen);
              }
            }
          };
        newHeaderImg.prototype.prepareData=function(n){
          eachProp(userlistArr,function(ele,index){
              memberData+='{"userid":"'+ele["userid"]+'",'+'"usertype":'+ele["usertype"]+'},';
              if(index>=(n-1)){return true;}
            });
            if(ulLen>7 && ulLen>=data.ptotal-1){
              memberData+='{"userid":"'+userlistArr[userlistArr.length-1]["userid"]+'",'+'"usertype":'+userlistArr[userlistArr.length-1]["usertype"]+'},';
            }
            memberData=memberData.substring(0,memberData.length-1);
            memberData+=']';
          this.requestImg();
        };
        newHeaderImg.prototype.requestImg=function(){
            var that=this;
            xhr=$.ajax({
                          url: 'http://b.paipai.com/userinfo/getusernickandhead?userlist='+memberData,
                          type: 'GET',
                          dataType: 'jsonp',
                          jsonpCallback:'getuserinfo'+new Date().valueOf(),
                          error:function(a,b,c){
                          },
                          success: function(json){
                             if(json.retCode==13){
                               plogin(window.location.href);
                             }else{
                                that.callbackFun(json);
                             }
                          }
                        });
          };
        newHeaderImg.prototype.callbackFun=function(json){
              var item=json.list,itemLen=item.length;
              for(var i=0;i<(withDotted?6:(data.ptotal<(ulLen+1)?ulLen:data.ptotal-1));i++){
                if(withDotted && i==5){
                  $(".pp_users").append('<span class="pp_users_item pp_users_dot"><i></i>');
                }
                if(item[i]){
                  $(".pp_users").append('<a href="#" class="pp_users_item pp_users_normal"><img src="'+smallImg(item[i]["imgurl"])+'" alt=""></a>');
                }else{
                  $(".pp_users").append('<span class="pp_users_item pp_users_blank"><img src="http://static.paipaiimg.com/fd/qqpai/tuan/img/avatar_4_64.png" alt=""></span>');
                }
              }
          };
        newHeaderImg.prototype.decorate=function(str){
            var decorator=this.constructor.decorators[str];//get decorators and ship
            for(var i in decorator){
              this[i]=decorator[i];
            };
          };
        var headerImgObj=new newHeaderImg();
        if(data.isVTuan){
          $("body").addClass("vip");
          headerImgObj.decorate("bigVTuan");
        };
        headerImgObj.begin();
      };
      observerObj.tuanState1.update=function(data){
        if(data.tstate=="sales"){
          var pLeft=data.ptotal-data.pnum,propagation="";
          if(data.isVTuan){
            propagation=pLeft>=3?("至尊无敌超级团，差<b>"+pLeft+"</b>人，抢个位"):(pLeft==2?("仅剩<b>"+pLeft+"</b>名额，想搭车的赶紧"):"最后<b>1</b>席，超级团谁来压轴？");
          }else{
            propagation=pLeft>=3?("跪求了，还差<b>"+pLeft+"</b>人，援个手"):(pLeft==2?("等哭了，还差<b>"+pLeft+"</b>人，拜托了"):"急疯了，还差<b>1</b>人，给跪了");
          }
          $(".pp_tips").html(propagation);
        }else if(data.tstate=="succ"){
          $(".pp_tips").html(data.userlist.length>data.ptotal?"OMG，团被挤爆了！欢迎新同学~~":(data.isVTuan?"银河系无敌超级团，完美收官！":"感谢各位仗义救急！妥妥儿的"));
        }else{
          $(".pp_tips").html("小伙伴不给力哇，下次要攒人品啦");
        }
      };
      observerObj.tuanState2.update=function(data){
        if(data.tstate=="sales"){
          nowtime=new Date((data.servertime*1000)||(new Date())).getTime();
          window.endtime=data["endtime"]*1000;
          changeEndTime();
        }else if(data.tstate=="failed"){
          $(".pp_state_txt").html("团购失败，给大家退款中").show();
          $(".pp_time").hide();
        }else{
          $(".pp_state_txt").show();
          $(".pp_time").hide();
        }
      };
      observerObj.yaoheList.update=function(data){
        showYaoheMod(data.userlist,data.tstate,{"totalTNum":data.ptotal,"haveNum":data.pnum});
      };
      observerObj.seller.update=function(data){
        if(data["shopName"]){
          $("#itemShopName").html(data["shopName"]);
          doc.getElementById("shopLink").href="http://wd.paipai.com/mshop/"+data["shopId"];
          sessionStorage["shopId"]=data["shopId"];
        }else{
          $(".support").hide();
        }
        //微信下显示
        if(!isWx() || !data.shopwxid){
          $(".support_btn").hide();
        }else{
          $(".support_btn").click(function(){
            $("#shopwxid").val(data.shopwxid);
            $(".mod_wxa").show();
            return false;
          });
          $(".mod_wxa_close").click(function(){$(".mod_wxa").hide();});
        }
      };
      observerObj.hotTuan.update=function(data){
        if(data.tstate=="failed" || (data.tbstate=="bao" && data.ustate=="noin")){
          hotTuanInto("#hotTuanWrap");
        }
      };
      observerObj.bottom.update=function(data){
        switch(data.tstate){
          case "sales":
            footFoucsPPY(2);
            break;
          case "failed":
          case "succ"://迭代点
            footFoucsPPY(3);
            break;
        }
      };
      observerObj.button.update=function(data){
        $(".fixopt").show();
        if(data.tstate=="sales" && data["canbuy"]>0){
          $("#bottomBtn"+(data.ustate=="succ"?1:2)).show();
          bindEvent(data);
        }else{
          $("#bottomBtn3").html("看全部商品，我也开个团").attr("href","http://app.paipai.com/takeCheap/tuan_list.html?_wv=1");
          $("#bottomBtn3").show();
        }
      };
      //E observer工具
      //根据用户的uin和团gID获取团信息
      var getGrounpInfo=function(){
              xhr=$.ajax({
                      url: 'http://b.paipai.com/tuaninfo/GetDetail?tid='+gid+'&uid='+uin,
                      type: 'GET',
                      dataType: 'jsonp',
                      jsonpCallback:'GetDetail',
                      success: function(data) {
                          isSuccess=true;
                          $("#loadingBox").hide();
                          if(!data["errCode"])
                          {
                              window.commodityId=data["itemID"];
                              window._itemInfo = {
                                "commodity":{
                                   "C_ID":data.itemID,
                                   "C_Uin":data.shopId,
                                   "C_ClassID":""
                                }
                                };
                              $("#eccReportJs").attr("src","http://static.gtimg.com/js/version/2014/09/ecc.cloud.ppmobilereport.20140905.js?t=201409091150");
                              dataBuyNum=data["buy_num"];
                              data["saleprice"]=(parseFloat(data["saleprice"])/100).toFixed(2);
                              data["marketprice"]=(parseFloat(data["marketprice"])/100).toFixed(2);
                              data["mname"]=data["mname"]==""?"你好友":decodeNick(data["mname"]);
                              data["img"]=data["img"].replace(".0.jpg",".0.120x120.jpg");//换小尺寸图URL进行分享展示透传
                              //向observer通告变更信息
                              domObserverList.notify(data);
                              if(isMQQUserAgent() || isWx()){
                                var contentPool=["大神开场了，快快跟随冒泡踩场吧","达人难得有空吆喝聚友，一起来聚聚","开嗓吆喝，喊你凑桌一起玩一玩","开摊吆喝了，速来围观Ta的品位吧"];
                                var ranContent=contentPool[Math.floor(Math.random()*(4-0)+0)];
                                var shareContent=(data.isVTuan?"超级团长":"")+data.mname.replace(/\s/g,"")+" "+ranContent+"！"+data.title+"仅售"+data.saleprice+"元";
                                shareApi({
                                  "url": curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),
                                  "qq_title": (data.isVTuan?"超级团长":"")+data["mname"]+"发起了团购：",
                                  "qzone_title": (data.isVTuan?"超级团长":"")+data["mname"]+"发起了团购：",
                                  "qq_desc": "【"+data["ptotal"]+"人成团"+data["saleprice"]+"元】"+(data["title"].length>16?(data["title"].substring(0,16)+"..."):data["title"])+"，跟我组团抢，超划算，信我没错！",
                                  "qzone_desc": "【"+data["ptotal"]+"人成团"+data["saleprice"]+"元】"+(data["title"].length>16?(data["title"].substring(0,16)+"..."):data["title"])+"，跟我组团抢，超划算，信我没错！",
                                  "wechat_title": (data.isVTuan?"超级团长":"")+data["mname"]+"发起了团购：",
                                  "wechat_time_title": shareContent,
                                  "wechat_desc": shareContent,
                                  "img": data["img"],
                                  "ptag":{ 
                                           w2w:"20400.5.3", //微信分享至微信好友
                                           w2t:"20400.5.4", //微信分享至朋友圈
                                           q2q:"20400.5.1", //手Q分享至QQ好友或群
                                           q2z:"20400.5.2", //手Q分享至QQ空间
                                           q2w:"20400.5.3", //手Q分享至微信好友
                                           q2t:"20400.5.4"  //手Q分享至朋友圈
                                         }
                                });
                              };
                              if(navigator.userAgent.indexOf("NewsApp")>=0){
                                wyShare({
                                  title    : "【拍拍-拍便宜】",
                                  linkUrl  : curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),
                                  photoUrl : data.img,
                                  text     : data.mname+"开摊吆喝了，速来围观Ta的品位吧！"+data.title
                                });
                              };
                          }else if(data["errCode"]==13){
                                plogin(window.location.href);
                          }else if(data["errCode"]==20020){
                            setTimeoutInfo();
                          }else{
                            $("#sorryWord").html("<p>对不起，系统加载繁忙~~~您可以选择：<p><p><a onclick='window.location.reload()'>1、刷新试试？</a></p><p><a href='http://app.paipai.com/takeCheap/tuan_list.html?_wv=1'>2、去看看别的→</a></p>");
                             setTimeoutInfo();
                          }
                      }
              });
              window.timer=new Date().getTime();
              var timeoutWork=function(){
                 if(!isSuccess){
                       var tt=new Date().getTime()-window.timer;
                       if(tt<15000){
                            setTimeout(timeoutWork,15000-tt);
                       }else{
                         setTimeoutInfo(); 
                       }
                  }
              };
              setTimeout(timeoutWork,15000);
      };
    /**
     * 埋点开始
     * */
    var  url_param = {};
    var points = {
        upPv:"http://app.paipai.com/api/bi/upPv.xhtml",
        ptag:["20381.59.1","20381.59.2","20381.59.3","20381.59.4","20381.59.5","20381.59.6","20381.59.7"]
    }
    var getParams = function(){
        var str = sessionStorage["pointParam"];
        if(str!=null&&str!=""){
            var strs = str.split("&");
            for (var i = 0; i < strs.length; i++) {
                url_param[strs[i].split("=")[0]] = decodeURI(strs[i].split("=")[1]);
            }
        }
    }
    var getPoint = function(pv_ptag,ptag,click_p){
        var refer = document.referrer;
        var param = "";
        var page_name = location.origin+location.pathname;
        var launchType=sessionStorage["ppy_tuan_result_launchType"]?"back":"new";
        sessionStorage["ppy_tuan_result_launchType"]="1";
        var devicetime =  (new Date()).valueOf();
        var  isHttp = "",
            duration = "",
            pageParam = "",
            rawPvid = url_param["mk"]+"|"+devicetime+"|"+Math.floor(Math.random()*10),
            dap="";
        if(sessionStorage["ppy_ptag"]&&sessionStorage["ppy_ptag"]!=""&&sessionStorage["ppy_ptag"]!=null){
            pageParam = "ptag="+sessionStorage["ppy_ptag"]+"&launchType="+launchType;
        }else{
            pageParam = "launchType="+launchType;
        }
        if(pv_ptag=="ptag"){
            sessionStorage["ppy_ptag"]=ptag;
        }
        refer=pv_ptag=="pv"?document.referrer:"";
        ptag = pv_ptag=="pv"?"":ptag;
        click_p = pv_ptag=="pv"?"":click_p;
        param= {
            content:devicetime+"|||"+page_name+"|||"+isHttp+"|||"+duration+"|||"+pageParam+"|||"+ptag+"|||"+click_p+"|||"+refer+"|||"+rawPvid+"|||"+dap
        };
        for(var key in url_param){
            param[key] = url_param[key];
        }
        $.ajax({
            url: points.upPv,
            type: 'GET',
            data :param,
            dataType: 'json',
            timeout:15000,
            success: function(data) {
            }
        });
    };
    var pointReport = function(pv_ptag,ptag,click_p){
        getPoint(pv_ptag,ptag,click_p);
    };
    /**
     * 埋点结束
     * */
      var Init=function(){
           //check();
           if(gid!="")
           {
              getGrounpInfo();
           }
           else
           {
              //重定向去列表页
           }
            getParams();
            pointReport("pv","","");
            $("#bottomBtn2").on("click",function(){
                pointReport("ptag",points.ptag[0],"comodity="+localStorage["comodity"]);
            })
            $(".goItemPage.td_lk").on("click",function(){
                pointReport("ptag",points.ptag[1],"comodity="+localStorage["comodity"]);
            })
        // var nname= e.target.nodeName.toLowerCase();
            $(".tips_txt").on("click",function(e){
                var nname= e.target.nodeName.toLowerCase();
                if(nname=="a"){
                    pointReport("ptag",points.ptag[3],"comodity="+localStorage["comodity"]+"&type=全部");
                }
            })
            $("#shopLink").on("click",function(e){
                    pointReport("ptag",points.ptag[4],"comodity="+localStorage["comodity"]+"&shop="+sessionStorage["shopId"]);
            })
            $(".step_more").on("click",function(e){
                pointReport("ptag",points.ptag[5],"comodity="+localStorage["comodity"]);
            })

            $("#bottomBtn1").on("click",function(){
                pointReport("ptag",points.ptag[6],"comodity="+localStorage["comodity"]);
            })
      }();

}());