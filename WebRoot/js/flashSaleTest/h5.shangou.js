$(function($) {
	shangouh5.init();
});
var shangouh5 = {
	init : function(){
		var classIdNav = $('.sg_nav_list').attr('id');
		shangouh5.getNavHtml(classIdNav);
		if(document.body.clientWidth > 385){
			$('.sg_nav_trigger').hide();
		}
		if(classIdNav == 'recommend' || classIdNav == 'sniping'){
			$('.sg_nav').toggleClass('sg_nav_on');
		}
		var clickEvent = 'ontouchstart' in window ? 'touchstart' : 'click';	
		$('.sg_nav_trigger').on(clickEvent,function(e){
			e.preventDefault();
			$('.sg_nav').toggleClass('sg_nav_on');
		})
		this.currentDateTime = commonDate.currentDate();
		this.currentTime = commonDate.getTime(this.currentDateTime)/1000;
		var dateTitle = this.currentDateTime;
		//矫正10小时误差
		var dateTitle10 = this.currentDateTime.split(' ');
		var date10 = dateTitle10[1].split(':');
		this.currentTime10 = this.currentTime;
		if(date10[0] < 10){
			this.currentTime10 = this.currentTime - 86400;
			dateTitle = commonDate.getDate('Y-m-d H:i:s', this.currentTime10);
		}
		
		dateTitle = dateTitle.split(' ');
		dateTitle = dateTitle[0].replace(/-/g, '');
		this.date = dateTitle;
		
		this.isApp = '';
		if(common.getUrlParam('app') != null){
			this.isApp = '&app='+common.getUrlParam('app');
			$('.mod_nav').hide();
		}
		//判断是否是手Q引用
		this.is_wv = '';
		if(common.getUrlParam('_wv') != null){
			this.is_wv = '&_wv='+common.getUrlParam('_wv');
		}
	
		//判断是否拍便宜引用
		this.is_src = '';
		if(common.getUrlParam('src') != null){
			this.is_src = '&src='+common.getUrlParam('src');
			$("body").addClass("nav_fixed");
		}else{
			$('#nav_jujingpin').show();
		}
		
		if(common.getUrlParam('app') == null && common.getUrlParam('_wv') == null){
			shangouh5.getTopBannerData();
		}
		shangouh5.getQQWXData();
	},
	activityApi : {
		'index' : 'http://www.paipai.com/sinclude/xml/tjw/tjw2014/tjw9/tjw49877194708.js?t='+Math.floor((new Date()).getTime()/(5*60*1000)),	//今日特卖 - 今日主打
		'women' : 'http://www.paipai.com/sinclude/xml/tjw/tjw2014/tjw9/tjw39804314700.js?t='+Math.floor((new Date()).getTime()/(5*60*1000)),	//女人tjw289240974735.js
		'kids' : 'http://www.paipai.com/sinclude/xml/tjw/tjw2014/tjw9/tjw49036654704.js?t='+Math.floor((new Date()).getTime()/(5*60*1000)),	//儿童tjw39804314700
		'men' : 'http://www.paipai.com/sinclude/xml/tjw/tjw2014/tjw9/tjw49405104706.js?t='+Math.floor((new Date()).getTime()/(5*60*1000)),	//男人
		'discovery' : 'http://www.paipai.com/sinclude/xml/tjw/tjw2014/tjw9/tjw49885884703.js?t='+Math.floor((new Date()).getTime()/(5*60*1000)),	//生活家居
		'sports' : 'http://www.paipai.com/sinclude/xml/tjw/tjw2014/tjw9/tjw49404884705.js?t='+Math.floor((new Date()).getTime()/(5*60*1000)),	//运动户外
		'hot' : 'http://www.paipai.com/sinclude/xml/tjw/tjw2014/tjw12/tjw99286274864.js?t='+Math.floor((new Date()).getTime()/(5*60*1000)),	//hot
		"more" : 'http://www.paipai.com/sinclude/xml/tjw/tjw2014/tjw9/tjw59010184723.js?t='+Math.floor((new Date()).getTime()/(5*60*1000)),	//最后疯抢
		"fineshop" : 'http://www.paipai.com/sinclude/xml/tjw/tjw2014/tjw9/tjw59066114722.js?t='+Math.floor((new Date()).getTime()/(5*60*1000)),	//优店推荐
		
		'topBanner' : 'http://static.paipaiimg.com/js/data/ppms.page16840.js?t='+Math.floor((new Date()).getTime()/(5*60*1000)),	//头部banner
		'QQWX' : 'http://static.paipaiimg.com/js/data/ppms.page16911.js?t='+Math.floor((new Date()).getTime()/(5*60*1000)),	//微信QQ

		'flashbuy' : 'http://act.paipai.com/flashbuy/active/getActiveInfoParamJson',	//COSS
		'flashbuyNew' : 'http://act.paipai.com/flashbuy/new/active/getActiveInfoParamJson',	//COSS 新接口
		'activity' : 'http://express.paipai.com/tws/martcpc/martcpcshow?actid={#actid#}&callback={#callbackFunction#}&areaOn=1&pcs={#areaid#}:{#count#}',	//活动基本信息
		'product' : 'http://express.paipai.com/tws/martcpc/martcpcshow?actid={#actid#}&callback={#callbackFunction#}&areaOn=1&pcs={#areaid#}:{#count#}',	//活动包含产
		'productNew' : 'http://opr.paipai.com/mart/activeshow',
		'productCDN' : 'http://static.paipaiimg.com/oprcdn/mart/act_{#actid#}.js?t='+Math.floor((new Date()).getTime()/(5*60*1000)),
		'processCount' : 'http://static.paipaiimg.com/act_cms/shangou/shangou_ypms_count.js?t='+Math.floor((new Date()).getTime()/(5*60*1000)),
		'sniping' : 'http://static.paipaiimg.com/act_cms/shangou/shangou_ypms_sniping.js?t='+Math.floor((new Date()).getTime()/(5*60*1000)),
		'indexSniping' : 'http://static.paipaiimg.com/act_cms/shangou/shangou_ypms_sniping_column.js?t='+Math.floor((new Date()).getTime()/(5*60*1000)),
		'recommend' : 'http://static.paipaiimg.com/act_cms/shangou/shangou_ypms_recommend.js?t='+Math.floor((new Date()).getTime()/(5*60*1000)),
		'buyers' : 'http://static.paipaiimg.com/js/data/ppms.page16945.js?t='+Math.floor((new Date()).getTime()/(5*60*1000)),	//微信QQ
		//'processCount' : 'http://www.paipai.com/3/new/js/shangou_ypms_count.js',
		//'sniping' : 'http://www.paipai.com/3/new/js/shangou_ypms_sniping.js',
		//'recommend' : 'http://www.paipai.com/3/new/js/shangou_ypms_recommend.js'
	},
	
	getStartTime : function(startTime){
		return parseInt(startTime)+60*60*10;
	},
	
	getEndTime : function(endTime){
		return parseInt(endTime)+60*60*10;
	},
	
	getSeedNum : function(actId, areaId){
		return (actId * areaId) % 150 + 150;
	},
	getTimeStamp : function(date){
		return commonDate.getTime(date.substr(0,4)+"-"+date.substr(4,2)+"-"+date.substr(6,2)+" "+"10:00:00")/1000;
	},
	
	getHistoryHref : function(){
		if(history.length > 1){
			history.go(-1);
		}else{
			location.href='http://m.paipai.com';
		}
		//var url = (document.referrer == '') ? 'http://m.paipai.com' : document.referrer;
		//location.href = url;
	},
	
	getNavHtml : function(classId){
		var html = '<li {#index#}>\
				<a href="index.html?ptag=20399.1.6{#isApp#}{#isDate#}{#is_wv#}{#is_mqq#}">今日特卖</a>\
			</li>\
			<li {#women#}>\
				<a href="women.shtml?ptag=20399.1.1{#isApp#}{#isDate#}{#is_wv#}{#is_mqq#}">女人</a>\
			</li>\
			<li {#kids#}>\
				<a href="kids.shtml?ptag=20399.1.2{#isApp#}{#isDate#}{#is_wv#}{#is_mqq#}">儿童</a>\
			</li>\
			<li {#men#}>\
				<a href="men.shtml?ptag=20399.1.3{#isApp#}{#isDate#}{#is_wv#}{#is_mqq#}">生活</a>\
			</li>\
			<li {#discovery#}>\
				<a href="discovery.shtml?ptag=20499.8.1{#isApp#}{#isDate#}{#is_wv#}{#is_mqq#}">新奇特</a>\
			</li>\
            <li {#recommend1#}>\
				<a href="recommend.shtml?ptag=20499.8.25{#isApp#}{#isDate#}{#is_wv#}{#is_mqq#}">买手推荐</a>\
			</li>\
            <li {#sniping1#}>\
				<a href="sniping.shtml?ptag=20499.8.22{#isApp#}{#isDate#}{#is_wv#}{#is_mqq#}">优品秒杀</a>\
			</li>';
		if(classId == 'hot'){
			html += '<li {#hot#}>\
				<a href="hot.shtml?ptag=20499.2.4{#isApp#}{#isDate#}{#is_wv#}{#is_mqq#}">红包专场</a>\
			</li>';
		}
		var isApp = isDate = is_wv = is_mqq = '';
		if(common.getUrlParam('app') != null){
			var t = '&app=';
			isApp = t + common.getUrlParam('app');
		}
		if(common.getUrlParam('date') != null){
			var t = '&date=';
			isDate = t + common.getUrlParam('date');
		}
		if(common.getUrlParam('_wv') != null){
			var t = '&_wv=';
			is_wv = t + common.getUrlParam('_wv');
		}
		if(common.getUrlParam('src') != null){
			var t = '&src=';
			is_mqq = t + common.getUrlParam('src');
		}
		var index = women = kids = men = discovery = recommend1 = sniping1 = hot = ''; 
		switch(classId){
			case "index":
				index = 'class="on"';
				break;
			case "women":
				women = 'class="on"';
				break;
			case "kids":
				kids = 'class="on"';
				break;
			case "men":
				men = 'class="on"';
				break;
			case "discovery":
				discovery = 'class="on"';
				break;
			case "recommend":
				recommend1 = 'class="on"';
				break;	
			case "sniping":
				sniping1 = 'class="on"';
				break;	
			case "hot":
				hot = 'class="on"';
				break;				
		}
		html = html.replace(new RegExp("\{#index#\}","g"), index);
		html = html.replace(new RegExp("\{#women#\}","g"), women);
		html = html.replace(new RegExp("\{#kids#\}","g"), kids);
		html = html.replace(new RegExp("\{#men#\}","g"), men);
		html = html.replace(new RegExp("\{#discovery#\}","g"), discovery);
		html = html.replace(new RegExp("\{#recommend1#\}","g"), recommend1);
		html = html.replace(new RegExp("\{#sniping1#\}","g"), sniping1);
		html = html.replace(new RegExp("\{#hot#\}","g"), hot);
		html = html.replace(new RegExp("\{#isApp#\}","g"), isApp);
		html = html.replace(new RegExp("\{#isDate#\}","g"), isDate);
		html = html.replace(new RegExp("\{#is_wv#\}","g"), is_wv);
		html = html.replace(new RegExp("\{#is_mqq#\}","g"), is_mqq);
		$('.sg_nav_list').html(html);
	},
	//获取头部广告
	getTopBannerData : function(index){
		var apiUrl = this.activityApi['topBanner'];
		common.getApiData('h5', apiUrl, 'showPageData16840');
	},
	//获取微信QQ信息
	getQQWXData : function(index){
		var apiUrl = this.activityApi['QQWX'];
		common.getApiData('h5', apiUrl, 'showPageData16911');
	},
	//获取今日上新总数
	getProcessCountData : function(){
		var apiUrl = this.activityApi['processCount'];
		common.getApiData('h5', apiUrl, 'shangou_ypms_count');
	},
	getActivityOldData : function(data, time){
		var newData = new Array();
		var dateOld1 = time;//今天
		var dateOld2 = time - 86400;//前一天
		var dateOld3 = time - 86400 * 2;//前二天
		var dateOld4 = time - 86400 * 3;//前三天
		var dateOld5 = time - 86400 * 4;//前四天
		
		var dateTitleOld1 = commonDate.getDate('Ymd', dateOld1);//今天
		var dateTitleOld2 = commonDate.getDate('Ymd', dateOld2);//前一天
		var dateTitleOld3 = commonDate.getDate('Ymd', dateOld3);//前二天
		var dateTitleOld4 = commonDate.getDate('Ymd', dateOld4);//前三天
		var dateTitleOld5 = commonDate.getDate('Ymd', dateOld5);//前四天
		for(var i=0;i<data.length;i++){
			var item = data[i];
			if(item == undefined || typeof item != "object"){
				continue;
			}
			if(item.recmdRegName == dateTitleOld1 || item.recmdRegName == dateTitleOld2 || item.recmdRegName == dateTitleOld3 || item.recmdRegName == dateTitleOld4 || item.recmdRegName == dateTitleOld5){
				newData.push(item);
			}
		}
		return this.sort(newData);
	},
	getActivityDayData : function(data, time){
		var newData = new Array();
		var newData1 = new Array();
		var dateOld1 = time;//今天
		var dateOld2 = time - 86400;//前一天
		var dateOld3 = time - 86400 * 2;//前二天
		var dateOld4 = time - 86400 * 3;//前三天
		var dateOld5 = time - 86400 * 4;//前四天
		
		var dateTitleOld1 = commonDate.getDate('Ymd', dateOld1);//今天
		var dateTitleOld2 = commonDate.getDate('Ymd', dateOld2);//前一天
		var dateTitleOld3 = commonDate.getDate('Ymd', dateOld3);//前二天
		var dateTitleOld4 = commonDate.getDate('Ymd', dateOld4);//前三天
		var dateTitleOld5 = commonDate.getDate('Ymd', dateOld5);//前四天
		for(var i=0;i<data.length;i++){
			var item = data[i];
			if(item == undefined || typeof item != "object"){
				continue;
			}
			if(item.recmdRegName == dateTitleOld1){
				newData.push(item);
			}else if(item.recmdRegName == dateTitleOld2 || item.recmdRegName == dateTitleOld3 || item.recmdRegName == dateTitleOld4 || item.recmdRegName == dateTitleOld5){
				newData1.push(item);
			}
		}
		return [newData, newData1];
	},
	sort : function (data){
		var newData = new Array();
		for(var i=0;i<data.length;i++){
			if(data[i] == undefined || typeof data[i] != "object"){
				continue;
			}else{
				newData.push(data[i]);
			}
		}
		if(newData.length>1){
			for(var i=0;i<newData.length;i++){
				for(var j=newData.length-1;j>0;j--){
					if(parseInt(newData[j].recmdRegName) > parseInt(newData[j-1].recmdRegName)){
						var tmp = newData[j];
						newData[j] = newData[j-1];
						newData[j-1] = tmp;
					}
				}
			}
		}
		return newData;
	},
	sortEndTime : function (data){
		if(data.length>1){
			for(var i=0;i<data.length;i++){
				for(var j=data.length-1;j>0;j--){
					if(parseInt(data[j].endTime) > parseInt(data[j-1].endTime)){
						var tmp = data[j];
						data[j] = data[j-1];
						data[j-1] = tmp;
					}
				}
			}
		}
		return data;
	},
	sordSeedNumFuc : function (data){
		if(data.length>1){
			for(var i=0;i<data.length;i++){
				for(var j=data.length-1;j>0;j--){
					if(parseInt(data[j].sordSeedNum) > parseInt(data[j-1].sordSeedNum)){
						var tmp = data[j];
						data[j] = data[j-1];
						data[j-1] = tmp;
					}
				}
			}
		}
		return data;
	},
	getFilterRepeatDate : function(data){
		var newData = new Array();
		for(var i=0;i<data.length;i++){
			var state = 0;
			for(var j=i;j<data.length;j++){
				if(i != j && data[i].qq == data[j].qq && data[i].id == data[j].id){
					state = 1;
					break;
				}
			}
			if(state == 0){
				newData.push(data[i]);
			}
		}
		return newData;
	},
	timerFormat : function(currentTime, startTime, endTime){
		var title = '';
		if(currentTime<startTime){
			title = '<span>即将开始</span>';
		}else if(currentTime>=endTime){
			title = '<span>活动已结束</span>';
		}else{
			var differenceTime = endTime - currentTime;
			var differenceTimeTitle = commonDate.getDifferTimeTitle(differenceTime);
			title = '<span>'+differenceTimeTitle.D+'</span>天<span>'+differenceTimeTitle.H+'</span>时<span>'+differenceTimeTitle.I+'</span>分<span>'+differenceTimeTitle.S+'</span>秒';
		}
		return title;
	},
	getProductHtml : function(item, ptag, html){
		var activityPrice = (item["dwActivePrice"] == '') ? item["dwPrice"] : item["dwActivePrice"];
		activityPrice = (activityPrice/100).toFixed(2);
		var newPrice = item["dwMarketPrice"];
		newPrice = (newPrice/100).toFixed(2);
		var proUrl = '';
		var ptagTitle = (ptag != '') ? '&ptag='+ptag+'' : '';
		var appTitle = (common.getUrlParam('app') != null) ? '&app='+common.getUrlParam('app')+'' : '';
		proUrl = 'http://b.paipai.com/itemweb/item?scence=101&ic='+item["strCommodityId"] + appTitle + ptagTitle;
		if(item["vecExtData"] == null){
			var imgUrl = (item["mapCustomData"]["uploadPicUrl1"] == '') ? item["strPic"] : item["mapCustomData"]["uploadPicUrl1"];
		}else{
			var imgUrl = (item["vecExtData"][0]["strValue"] == '') ? (item["mapCustomData"]["uploadPicUrl1"] == '') ? item["strPic"] : item["mapCustomData"]["uploadPicUrl1"] : item["vecExtData"][0]["strValue"];
		}
		if(item["vecExtData"].length>=6){
			if(item["vecExtData"][5]["strValue"] != ''){
				imgUrl = item["vecExtData"][5]["strValue"];
			}	
		}
		html = html.replace(new RegExp("\{#imgUrl#\}","g"), imgUrl);
		html = html.replace(new RegExp("\{#proName#\}","g"), item["strTitle"]);
		html = html.replace(new RegExp("\{#proUrl#\}","g"), proUrl);
		html = html.replace(new RegExp("\{#activityPrice#\}","g"), activityPrice);
		html = html.replace(new RegExp("\{#newPrice#\}","g"), newPrice);
		return html;
	}
}
function showPageData16840(data){
	if(data != undefined && typeof data == "object"){
		var item = data.data;
		var html = barHtml = '';
		var htmlConfig = '<li><a href="{#bannerUrl#}"><img src="{#bannerImg#}"></a></li>';
		var j = 0;
		for(var i=0;i<item.length;i++){
			var startTime = commonDate.getTime(item[i].startTime)/1000;
			var endTime = commonDate.getTime(item[i].endTime)/1000;
			if(shangouh5.currentTime >= startTime && shangouh5.currentTime < endTime){
				if(j<5){
					var bannerHtml = htmlConfig;
					bannerHtml = bannerHtml.replace(new RegExp("\{#bannerUrl#\}","g"), item[i].url);
					bannerHtml = bannerHtml.replace(new RegExp("\{#bannerImg#\}","g"), item[i].imgUrl);
					html += bannerHtml;
					var onHtml = (j==0)?'class="on"':'';
					barHtml += '<li '+onHtml+'></li>';
					j++;
				}
			}
		}
		if(html != ''){
			$('.sliderBanner_pic_list').html(html);
			$('.sliderBanner_bar').html(barHtml);
			$('#sliderBanner').show();
			$('#sliderBanner').sliderBanner();
			//$('.mod_banner').html(html);
//				$('body').children().eq(0).before(html);
		}
	}	
}
function showPageData16911(data){
	if(data != undefined && typeof data == "object"){
		var random = Math.floor(Math.random() * data.data.length);
		var item = data.data[random];
		if(item == undefined){
			item = data.data[0];
		}
		var url = (item['shareUrl'] == '') ? window.location.href : item['shareUrl'] ;
		shareApi({
			"url" : url,
			"title" : item['shareTitle'],
			"wechat_desc" : item['shareWechatDesc'],
			"qq_desc" : item['shareQQDesc'],
			"img" : item['imgUrl'],
			"qzone_title" : item['shareQzoneTitle'],
			"qzone_desc" : item['shareQzoneDesc']
		});	
	}
}
function shangou_ypms_count(data){
	var classId = $('.sg_nav_list').attr('id');
	if(data!=undefined){
		var num = 0;
		switch(classId){
			case 'women':
				num = data.data.women;
				break;
			case 'men':
				num = data.data.men;
				break;
			case 'kids':
				num = data.data.kids;
				break;
			case 'discovery':
				num = data.data.discovery;
				break;
			default:
				num = parseInt(data.data.women) + parseInt(data.data.men) + parseInt(data.data.kids) + parseInt(data.data.discovery);
				break;
		}
		$('#shangou_ypms_count').text(num);
	}
}
$.fn.sliderBanner=function(){
	var oPlay = $(this);
	var aBtn = $(this).find('.sliderBanner_bar').children();
	var oUl = $(this).find('.sliderBanner_banner_ctn').children();
	var aLi = $(this).find('.sliderBanner_pic_list').children();
	var iNow = 0;
	var timer = null;
	
	aLi.each(function(index, element) {
        $(this).width($('#sliderBanner').width());
    });
	
	oUl.css('width', aLi[0].offsetWidth*aLi.length+'px');

	aBtn.click(function(){
		iNow=$(this).index();
		tab();
	});
	
	var touch_start={},touch_now={},touch_end={},diff={};
	$(this).off().on('touchstart',function(e){
		clearInterval(timer);
		touch_start.x = e.touches[0].pageX||e.pageX;
		touch_start.y = e.touches[0].pageY||e.pageY;
	}).on('touchmove',function(e){
		touch_now.x = e.touches[0].pageX||e.pageX;
		touch_now.y = e.touches[0].pageY||e.pageY;
		diff.x = touch_now.x - touch_start.x;
		diff.y = touch_now.y - touch_start.y;
		if(Math.abs(diff.x)>20){var s=true;}
		if(s){e.preventDefault();}
	}).on('touchend',function(e){
		if(Math.abs(diff.y)<100){
			if(diff.x>100){
				previous();
			}else if(diff.x<-100){
				next();
			}
		}
		timer=setInterval(next, 5000);
	});

	function tab(){
		aBtn.removeClass('on');
		$(aBtn[iNow]).addClass('on');
		oUl.animate({left:-iNow*aLi.width()});
	}

	function next(){
		iNow++;
		if(iNow==aLi.length){
			iNow=0;
		}
		tab();
	}
	
	function previous(){
		iNow--;
		if(iNow<0){
			iNow=aLi.length-1;
		}
		tab();
	}
	

	timer=setInterval(next, 5000);

	oPlay.mouseover(function(){
		clearInterval(timer);
	});
	oPlay.mouseout(function(){
		timer=setInterval(next, 5000);
	});
};