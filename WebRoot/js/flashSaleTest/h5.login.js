function setCookie(name, value, expires, path, domain, secure) {
	var exp = new Date(), expires = arguments[2] || null, path = arguments[3] || "/", domain = arguments[4] || null, secure = arguments[5] || false;
	expires ? exp.setMinutes(exp.getMinutes() + parseInt(expires)) : "";
	document.cookie = name + '=' + escape(value) + ( expires ? ';expires=' + exp.toGMTString() : '') + ( path ? ';path=' + path : '') + ( domain ? ';domain=' + domain : '') + ( secure ? ';secure' : '');
}
function getCookie(name) {
	var reg = new RegExp("(^| )" + name + "(?:=([^;]*))?(;|$)"),
		val = document.cookie.match(reg);
	return val ? (val[2] ? unescape(val[2]) : "") : null;
}
function delCookie(name, path, domain, secure) {
	var value = getCookie(name);
	if(value != null) {
		var exp = new Date();
		exp.setMinutes(exp.getMinutes() - 1000);
		path = path || "/";
		document.cookie = name + '=;expires=' + exp.toGMTString() + ( path ? ';path=' + path : '') + ( domain ? ';domain=' + domain : '') + ( secure ? ';secure' : '');
	}
}
function getQuery(name, url) {
	var u = arguments[1] || window.location.search,
		reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"),
		r = u.substr(u.indexOf("\?") + 1).match(reg);
	return r != null ? r[2] : "";
}
function plogin(rurl){
	function getCookie(name) {
		var reg = new RegExp("(^| )" + name + "(?:=([^;]*))?(;|$)"),
			val = document.cookie.match(reg);
		return val ? (val[2] ? unescape(val[2]) : "") : null;
	}
	var isWX = window.WeixinJSBridge;
	if(!isWX){
		 var ua = navigator.userAgent.toLowerCase();
		 isWX = ua.match(/micromessenger/)?true : false;
	}
	if(!rurl){return (getCookie("wg_skey") && getCookie("wg_uin") || getCookie("p_skey") && getCookie("p_uin") || getCookie("skey") && getCookie("uin"))?true:false;}
	if (isWX) {
		window.location.href = 'http://party.wanggou.com/tws64/m/wxv3/Login?appid=1&rurl=' + encodeURIComponent(location.href);
	} else {
		window.location.href = 'http://b.paipai.com/mlogin/tws64/m/h5v1/cpLogin?rurl=' + encodeURIComponent(location.href) + '&sid=' + getCookie('sid') + '&uk=' + getCookie('uk');
	}
}
var uin=getCookie("wg_uin")||getCookie("uin");
var s_txt=$("#search")[0];
var s_form=$("#searchForm")[0];
if(plogin()){
if(common.getUrlParam('_wv') == null){
	$("#login,#loginBottom").html("��������");
}
$("#login,#loginBottom").attr("href","http://www.paipai.com/m2/my/index.shtml?src=mqq&_wv=1");
$("#register").hide();
}
$('body').on('tap', function (e) {
	var target = e.srcElement || e.target,em = target,i = 1;
	while (!em.id && i <= 3) {
		em = em.parentNode;i++;
	}
	if (!em.id) return;
	switch (em.id) {
		case 'login':
		case 'loginBottom':
			if(!plogin()){
				plogin("http://www.paipai.com/m2/index.html");
			}
		break;
		default:
            break;
	}
});