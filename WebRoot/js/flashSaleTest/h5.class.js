$(function ($) {
    shangouh5.getProcessCountData();
    activity.init();
    fineshop.init();
});
//女人
function tjw39804314700(data) {
    activity.getActivityTjwData(data);
    setTimeout(function () {
        activity.getActivityHtml('', false)
    }, 1000);
}
//儿童
function tjw49036654704(data) {
    activity.getActivityTjwData(data);
    setTimeout(function () {
        activity.getActivityHtml('', false)
    }, 1000);
}
//男人
function tjw49405104706(data) {
    activity.getActivityTjwData(data);
    setTimeout(function () {
        activity.getActivityHtml('', false)
    }, 1000);
}
//生活家居
function tjw49885884703(data) {
    activity.getActivityTjwData(data);
    setTimeout(function () {
        activity.getActivityHtml('', false)
    }, 1000);
}
//运动户外
function tjw49404884705(data) {
    activity.getActivityTjwData(data);
    setTimeout(function () {
        activity.getActivityHtml('', false)
    }, 1000);
}

var activity = {
    config: {
        'html': '<a class="sg_g" href="{#url#}">\
							<div class="sg_g_img">{#subScriptIco#}\
								<img src="{#image#}" alt="{#shopName#}">\
								<p class="sg_g_desc">{#recmdReason#}</p>\
							</div>\
							<p class="sg_g_tit">{#shopName#}</p>\
							<p class="sg_g_info">\
								<span class="sg_g_sale"><em class="sg_g_sale_price"><i>&yen;</i>{#price#}</em>起，已售<em class="sg_g_sale_num">{#sold#}</em>件</span>\
								<span class="sg_g_time sg_g_time_FloorOne" startTime="{#startTime#}" endTime="{#endTime#}">{#timerFormatTitle#}</span>\
							</p>\
						</a>',
        'ptag': {
            'women': '20399.2.1',	//女人
            'kids': '20399.3.1',	//儿童
            'men': '20399.4.1',	//男人
            'discovery': '20499.8.11'	//新奇特
        }
    },
    init: function () {
        this.currentTime = shangouh5.currentTime;
        this.currentTime10 = shangouh5.currentTime;
        this.isExecuteState = false;
        this.classId = $('.sg_nav_list').attr('id');
        this.getActivityData(this.classId);
    },
    getActivityData: function (index) {
        var apiUrl = shangouh5.activityApi[index];
        common.getApiData('h5', apiUrl, 'tjw');
    },
    getActivityTjwData: function (data) {
        //通过ppms获取推荐位数据
        var data = common.getData('js', data);
        if (data != undefined && typeof data == "object") {
            var tjwDataArray = new Array();
            this.tjwData = new Array();
            var ptag = '';
            //获取最近5天的
            var newData = shangouh5.getActivityOldData(data, this.currentTime10);
            //获取今日主打
            for (var i = 0; i < newData.length; i++) {
                var item = newData[i];
                if (item != undefined && typeof item == "object") {
                    if (item.recmdRegName == this.date) {
                        tjwDataArray.push(item);
                    }
                }
            }
            //获取剩余
            for (var i = 0; i < newData.length; i++) {
                var item = newData[i];
                if (item != undefined && typeof item == "object") {
                    if (item.recmdRegName != this.date && item.recmdRegName != '') {
                        tjwDataArray.push(item);
                    }
                }
            }
            tjwDataArray = shangouh5.getFilterRepeatDate(tjwDataArray);
            ptag = this.config.ptag[this.classId];

            for (var i = 0; i < tjwDataArray.length; i++) {
                var item = tjwDataArray[i];
                var zImage = item.image;
                var actId = item.id;
                var areaId = item.qq;

                var subScriptIco = '';
                if (item.favNum != '') {
                    //subScriptIco = '<span class="sg_g_tag">'+item.favNum+'</span>';
                }
                var urlConfig = (item.oldPrice == '' ) ? "detail.shtml?ptag=" + ptag + "&classId=" + this.classId + "&actId=" + actId + "&areaId=" + areaId + shangouh5.isApp + shangouh5.is_wv : item.oldPrice;
                //推荐位数据存放到数组中
                this.tjwData.push({
                    'image': item.image,
                    'zImage': zImage,
                    'shopName': item.shopName,
                    'recmdReason': item.recmdReason,
                    'userCredit': item.userCredit,
                    'favNum': item.favNum,
                    'remdRegName': item.recmdRegName,
                    'subScriptIco': subScriptIco,
                    'actId': actId,
                    'areaId': areaId,
                    'classId': this.classId,
                    'url': urlConfig
                })
            }
            this.getActivityCossData(this.classId);
        }
    },
    getActivityCossData: function () {
        if (this.tjwData != undefined && typeof this.tjwData == "object") {
            var jsonList = '[';
            for (var i = 0; i < this.tjwData.length; i++) {
                var item = this.tjwData[i];
                if (i != 0) {
                    jsonList += ','
                }
                var seedNum = (item.userCredit == undefined || item.userCredit == '') ? shangouh5.getSeedNum(item.actId, item.areaId) : (isNaN(item.userCredit)) ? shangouh5.getSeedNum(item.actId, item.areaId) : item.userCredit;
                jsonList += '{"activeId":"' + item.actId + '","poolId":"' + item.areaId + '","seedNum":"' + seedNum + '"}';
            }
            jsonList += ']';

            //通过coss获取数据
            $.ajax({
                type: 'GET',
                contentType: 'application/json',
                url: shangouh5.activityApi['flashbuyNew'],
                dataType: 'jsonp',
                jsonpCallback: 'activity.getActivityTrueHtml',
                data: "jsonlist=" + jsonList
            });
        }
    },
    getActivityTrueHtml: function (data) {
        this.getActivityHtml(data, true);
    },
    getActivityHtml: function (data, isTimeOut) {
        if (this.isExecuteState) {
            return true;
        }
        var html = '';
        var newData = new Array();
        var cossData = data.data;
        for (var i = 0; i < this.tjwData.length; i++) {
            if (isTimeOut) {
                for (var j = 0; j < cossData.length; j++) {
                    var getActivityHtmlAreaId = (cossData[j].poolId == undefined) ? cossData[j].areaId : cossData[j].poolId;
                    if (this.tjwData[i].actId == cossData[j].activeId && this.tjwData[i].areaId == getActivityHtmlAreaId) {
                        var cossItem = cossData[j];
                        this.tjwData[i]['startTime'] = cossData[j]['beginTime'];
                        this.tjwData[i]['endTime'] = cossData[j]['endTime'];
                        this.tjwData[i]['sold'] = (cossData[j]['areasalesVolume'] != 0) ? cossData[j]['areasalesVolume'] : cossItem.seedNum * 5;
                        this.tjwData[i]['price'] = cossData[j]['arealowestPrice'];
                        newData.push(this.tjwData[i]);
                        break;
                    }
                }
            } else {
                this.tjwData[i]['startTime'] = '';
                this.tjwData[i]['endTime'] = '';
                this.tjwData[i]['sold'] = shangouh5.getSeedNum(this.tjwData[i].actId, this.tjwData[i].areaId) * 10;
                this.tjwData[i]['price'] = shangouh5.getSeedNum(this.tjwData[i].actId, this.tjwData[i].areaId) * 50;
                newData.push(this.tjwData[i]);
            }
        }
        //只有分类页需要进行结束时间排序
        newData = shangouh5.sortEndTime(newData);
        for (var i = 0; i < newData.length; i++) {
            var basicData = newData[i];
            var price = sold = startTime = endTime = 0;
            if (basicData.sold != undefined) {
                sold = basicData.sold;
            }
            if (basicData.price != 'undefined') {
                price = basicData.price;
                price = (price / 100).toFixed(2);
            }
            if (basicData.startTime == '' || basicData.endTime == '' || basicData.startTime == '0' || basicData.endTime == '0') {
                startTime = shangouh5.getTimeStamp(basicData.remdRegName);
                endTime = startTime + 86400 * 5;
            } else {
                startTime = basicData.startTime;
                startTime = shangouh5.getStartTime(startTime);

                endTime = basicData.endTime;
                endTime = shangouh5.getEndTime(endTime);
            }
            if (this.currentTime >= startTime && this.currentTime < endTime) {
                var timerFormatTitle = shangouh5.timerFormat(this.currentTime, startTime, endTime);
                var forHtml = this.config.html;
                forHtml = forHtml.replace(new RegExp("\{#sold#\}", "g"), sold);
                forHtml = forHtml.replace(new RegExp("\{#price#\}", "g"), price);
                forHtml = forHtml.replace(new RegExp("\{#startTime#\}", "g"), startTime);
                forHtml = forHtml.replace(new RegExp("\{#endTime#\}", "g"), endTime);
                if (basicData.url != "" && basicData.url != null) {
                    if ((basicData.url).indexOf("?") > 0) {
                        basicData.url = basicData.url + "&app=1";
                    } else {
                        basicData.url = basicData.url + "?app=1";
                    }
                }
                forHtml = forHtml.replace(new RegExp("\{#url#\}", "g"), basicData.url);
                forHtml = forHtml.replace(new RegExp("\{#subScriptIco#\}", "g"), basicData.subScriptIco);
                forHtml = forHtml.replace(new RegExp("\{#image#\}", "g"), basicData.zImage);
                forHtml = forHtml.replace(new RegExp("\{#shopName#\}", "g"), basicData.shopName);
                forHtml = forHtml.replace(new RegExp("\{#recmdReason#\}", "g"), basicData.recmdReason);
                forHtml = forHtml.replace(new RegExp("\{#timerFormatTitle#\}", "g"), timerFormatTitle);
                html += forHtml;
            }
        }
        $('.sg_glist').eq(0).html(html);
        this.isExecuteState = true;
        var timer = setInterval('activity.timerCountDown()', 1000);
    },
    timerCountDown: function () {
        var This = this;
        this.currentTime = this.currentTime + 1;
        $('.sg_g_time_FloorOne').each(function (index, element) {
            var startTime = $(this).attr('startTime');
            var endTime = $(this).attr('endTime');
            $(this).html(shangouh5.timerFormat(This.currentTime, startTime, endTime));
        });
    }
}

//运动户外
function tjw59066114722(data) {
    fineshop.getActivityHtml(data);
}
var fineshop = {
    config: {
        'html': '<a href="{#url#}"><img class="mod_banner_img" src="{#image#}" alt=""></a>',
        'ptag': {
            'women': '20499.8.13',	//女人
            'kids': '20499.8.14',	//儿童
            'men': '20499.8.15',	//男人
            'discovery': '20499.8.16'	//特色街
        }
    },
    init: function () {
        this.classId = activity.classId;
        this.getActivityData();
    },
    getActivityData: function (index) {
        var apiUrl = shangouh5.activityApi['fineshop'];
        common.getApiData('h5', apiUrl, 'tjw');
    },
    getActivityHtml: function (data) {
        var html = '';
        var newData = common.getData('js', data);
        var ptag = this.config.ptag[this.classId];
        for (var i = 0; i < newData.length; i++) {
            var item = newData[i];
            if (item.nick == this.classId) {
                var forHtml = this.config.html;
                var urlTo = common.getUrlTo(item.recmdReason);
                var ptagTitle = (ptag != '') ? urlTo + 'ptag=' + ptag + '' : '';
                forHtml = forHtml.replace(new RegExp("\{#url#\}", "g"), item.recmdReason + ptagTitle);
                forHtml = forHtml.replace(new RegExp("\{#image#\}", "g"), item.image);
                html += forHtml;
            }
        }
        if (html != '') {
            $('.sg_shop_list').html(html);
            $('.sg_shop').show();
        }
    }
}	