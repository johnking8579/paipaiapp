$(function ($) {
    shangouh5.getProcessCountData();
    activity.init();
    lastActivity.init('index');
});
//今日特卖
function tjw49877194708(data) {
    activity.getActivityTjwData(data, 'index');
    setTimeout(function () {
        activity.getActivityHtml('', 'index', false)
    }, 1000);
}
//女人
function tjw39804314700(data) {
    activity.getActivityTjwData(data, 'women');
    setTimeout(function () {
        activity.getActivityHtml('', 'women', false)
    }, 1000);
}
//儿童
function tjw49036654704(data) {
    activity.getActivityTjwData(data, 'kids');
    setTimeout(function () {
        activity.getActivityHtml('', 'kids', false)
    }, 1000);
}
//男人
function tjw49405104706(data) {
    activity.getActivityTjwData(data, 'men');
    setTimeout(function () {
        activity.getActivityHtml('', 'men', false)
    }, 1000);
}
//生活家居
function tjw49885884703(data) {
    activity.getActivityTjwData(data, 'discovery');
    setTimeout(function () {
        activity.getActivityHtml('', 'discovery', false)
    }, 1000);
}
//运动户外
function tjw49404884705(data) {
    activity.getActivityTjwData(data, 'sports');
    setTimeout(function () {
        activity.getActivityHtml('', 'sports', false)
    }, 1000);
}
var activity = {
    isExecuteState: {
        'index': false,
        'women': false,	//女人
        'kids': false,	//儿童
        'men': false,	//男人
        'discovery': false,	//生活家居
        'sports': false	//运动户外
    },
    config: {
        'html': '<a class="sg_g" href="{#url#}">\
							<div class="sg_g_img">{#subScriptIco#}\
								<img src="{#image#}" alt="{#shopName#}">\
								<p class="sg_g_desc">{#recmdReason#}</p>\
							</div>\
							<p class="sg_g_tit">{#shopName#}</p>\
							<p class="sg_g_info">\
								<span class="sg_g_sale"><em class="sg_g_sale_price"><i>&yen;</i>{#price#}</em>起，已售<em class="sg_g_sale_num">{#sold#}</em>件</span>\
								<span class="sg_g_time sg_g_time_FloorOne" startTime="{#startTime#}" endTime="{#endTime#}">{#timerFormatTitle#}</span>\
							</p>\
						</a>',
        'ptag': {
            'index': '20399.1.6',	//今日特卖
            'women': '20399.1.1',	//女人
            'kids': '20399.1.2',	//儿童
            'men': '20399.1.3',	//男人
            'discovery': '20499.8.1',	//生活家居
            'sports': '20399.1.5',	//运动户外
        }
    },
    init: function () {
        this.timerCountDownCount = 5;//合适执行倒计时
        this.timerCountDownCountI = 0;
        this.currentTime = shangouh5.currentTime;
        this.currentTime10 = shangouh5.currentTime;
        this.date = shangouh5.date;
        this.tjwData = {
            'index': new Array(),
            'women': new Array(),
            'kids': new Array(),
            'men': new Array(),
            'discovery': new Array(),
            'sports': new Array()
        }
        this.getActivityData('index');
        this.getActivityData('women');
        this.getActivityData('kids');
        this.getActivityData('men');
        this.getActivityData('discovery');
        //this.getActivityData('sports');
    },
    getActivityData: function (index) {
        var apiUrl = shangouh5.activityApi[index];
        common.getApiData('h5', apiUrl, 'tjw');
    },
    getActivityTjwData: function (data, classId) {
        //通过ppms获取推荐位数据
        var data = common.getData('js', data);
        if (data != undefined && typeof data == "object") {
            var tjwDataArray = tjwDataArrayOld = new Array();
            var ptag = this.config.ptag[classId];
            //今日特卖--首页
            if (classId == 'index') {
                var newData = shangouh5.getActivityOldData(data, this.currentTime10);
                for (var i = 0; i < newData.length; i++) {
                    if (i < 5) {
                        tjwDataArray.push(newData[i]);
                    } else {
                        break;
                    }
                }
            } else {
                var newData = shangouh5.getActivityDayData(data, this.currentTime10);
                var tjwDataArrayPush = newData[1];
                for (var j = 1; j < newData[0].length; j++) {
                    tjwDataArray.push(newData[0][j]);
                }
                for (var i = 0; i < tjwDataArrayPush.length; i++) {
                    var actId = tjwDataArrayPush[i].id;
                    var areaId = tjwDataArrayPush[i].qq;
                    var sordSeedNum = (tjwDataArrayPush[i].userCredit == undefined || tjwDataArrayPush[i].userCredit == '') ? shangouh5.getSeedNum(actId, areaId) : (isNaN(tjwDataArrayPush[i].userCredit)) ? shangouh5.getSeedNum(actId, areaId) : tjwDataArrayPush[i].userCredit;
                    tjwDataArrayPush[i].sordSeedNum = sordSeedNum;
                }
                tjwDataArrayPush = shangouh5.sordSeedNumFuc(tjwDataArrayPush);
                var t = 9 - (j - 1);
                for (var i = 0; i < tjwDataArrayPush.length; i++) {
                    if (i < t) {
                        tjwDataArray.push(tjwDataArrayPush[i]);
                    }
                }
            }
            for (var i = 0; i < tjwDataArray.length; i++) {
                var item = tjwDataArray[i];
                var zImage = item.image;
                var actAreaId = item.qq;
                var actId = item.id;
                var areaId = item.qq;

                var subScriptIco = '';
                if (item.favNum != '') {
                    //subScriptIco = '<span class="sg_g_tag">'+item.favNum+'</span>';
                }
                var urlConfig = (item.oldPrice == '' ) ? "detail.shtml?ptag=" + ptag + "&classId=" + classId + "&actId=" + actId + "&areaId=" + areaId + shangouh5.isApp + shangouh5.is_wv : item.oldPrice;
                //推荐位数据存放到数组中
                this.tjwData[classId].push({
                    'image': item.image,
                    'zImage': zImage,
                    'shopName': item.shopName,
                    'recmdReason': item.recmdReason,
                    'userCredit': item.userCredit,
                    'favNum': item.favNum,
                    'remdRegName': item.recmdRegName,
                    'subScriptIco': subScriptIco,
                    'actId': actId,
                    'areaId': areaId,
                    'classId': classId,
                    'url': urlConfig
                })
            }
            this.getActivityCossData(classId);
        }
    },
    getActivityCossData: function (classId) {
        if (this.tjwData[classId] != undefined && typeof this.tjwData[classId] == "object") {
            var jsonList = '[';
            for (var i = 0; i < this.tjwData[classId].length; i++) {
                var item = this.tjwData[classId][i];
                if (i != 0) {
                    jsonList += ','
                }
                var seedNum = (item.userCredit == undefined || item.userCredit == '') ? shangouh5.getSeedNum(item.actId, item.areaId) : (isNaN(item.userCredit)) ? shangouh5.getSeedNum(item.actId, item.areaId) : item.userCredit;
                jsonList += '{"activeId":"' + item.actId + '","poolId":"' + item.areaId + '","seedNum":"' + seedNum + '"}';
            }
            jsonList += ']';

            var callbackName = '';
            switch (classId) {
                case 'index':
                    callbackName = 'activity.getActivityIndexHtml';
                    break;
                case 'women':
                    callbackName = 'activity.getActivityWomenHtml';
                    break;
                case 'kids':
                    callbackName = 'activity.getActivityKidsHtml';
                    break;
                case 'men':
                    callbackName = 'activity.getActivityMenHtml';
                    break;
                case 'discovery':
                    callbackName = 'activity.getActivityDiscoveryHtml';
                    break;
                case 'sports':
                    callbackName = 'activity.getActivitySportsHtml';
                    break;
            }

            //通过coss获取数据
            $.ajax({
                type: 'GET',
                contentType: 'application/json',
                url: shangouh5.activityApi['flashbuyNew'],
                dataType: 'jsonp',
                jsonpCallback: callbackName,
                data: "jsonlist=" + jsonList
            });
        }
    },
    getActivityIndexHtml: function (data) {
        this.getActivityHtml(data, 'index', true);
    },

    getActivityWomenHtml: function (data) {
        this.getActivityHtml(data, 'women', true);
    },

    getActivityKidsHtml: function (data) {
        this.getActivityHtml(data, 'kids', true);
    },

    getActivityMenHtml: function (data) {
        this.getActivityHtml(data, 'men', true);
    },

    getActivityDiscoveryHtml: function (data) {
        this.getActivityHtml(data, 'discovery', true);
    },

    getActivitySportsHtml: function (data) {
        this.getActivityHtml(data, 'sports', true);
    },
    getActivityHtml: function (data, classId, isTimeOut) {
        if (this.isExecuteState[classId]) {
            return true;
        }
        var html = '';
        var newData = new Array();
        for (var i = 0; i < this.tjwData[classId].length; i++) {
            if (isTimeOut) {
                var cossData = data.data;
                for (var j = 0; j < cossData.length; j++) {
                    var getActivityHtmlAreaId = (cossData[j].poolId == undefined) ? cossData[j].areaId : cossData[j].poolId;
                    if (this.tjwData[classId][i].actId == cossData[j].activeId && this.tjwData[classId][i].areaId == getActivityHtmlAreaId) {
                        var cossItem = cossData[j];
                        this.tjwData[classId][i]['startTime'] = cossData[j]['beginTime'];
                        this.tjwData[classId][i]['endTime'] = cossData[j]['endTime'];
                        this.tjwData[classId][i]['sold'] = (cossData[j]['areasalesVolume'] != 0) ? cossData[j]['areasalesVolume'] : cossItem.seedNum * 5;
                        this.tjwData[classId][i]['price'] = cossData[j]['arealowestPrice'];
                        newData.push(this.tjwData[classId][i]);
                        break;
                    }
                }
            } else {
                this.tjwData[classId][i]['startTime'] = '';
                this.tjwData[classId][i]['endTime'] = '';
                this.tjwData[classId][i]['sold'] = shangouh5.getSeedNum(this.tjwData[classId][i].actId, this.tjwData[classId][i].areaId) * 10;
                this.tjwData[classId][i]['price'] = shangouh5.getSeedNum(this.tjwData[classId][i].actId, this.tjwData[classId][i].areaId) * 50;
                newData.push(this.tjwData[classId][i]);
            }
        }
        for (var i = 0; i < newData.length; i++) {
            var basicData = newData[i];
            var price = sold = startTime = endTime = 0;
            if (basicData.sold != undefined) {
                sold = basicData.sold;
            }
            if (basicData.price != 'undefined') {
                price = basicData.price;
                price = (price / 100).toFixed(2);
            }
            if (basicData.startTime == '' || basicData.endTime == '' || basicData.startTime == '0' || basicData.endTime == '0') {
                startTime = shangouh5.getTimeStamp(basicData.remdRegName);
                endTime = startTime + 86400 * 5;
            } else {
                startTime = basicData.startTime;
                startTime = shangouh5.getStartTime(startTime);

                endTime = basicData.endTime;
                endTime = shangouh5.getEndTime(endTime);
            }
            if (this.currentTime >= startTime && this.currentTime < endTime) {
                var timerFormatTitle = shangouh5.timerFormat(this.currentTime, startTime, endTime);
                var forHtml = this.config.html;
                forHtml = forHtml.replace(new RegExp("\{#sold#\}", "g"), sold);
                forHtml = forHtml.replace(new RegExp("\{#price#\}", "g"), price);
                forHtml = forHtml.replace(new RegExp("\{#startTime#\}", "g"), startTime);
                forHtml = forHtml.replace(new RegExp("\{#endTime#\}", "g"), endTime);
                if (basicData.url != "" && basicData.url != null) {
                    if ((basicData.url).indexOf("?") > 0) {
                        basicData.url = basicData.url + "&app=1";
                    } else {
                        basicData.url = basicData.url + "?app=1";
                    }
                }
                forHtml = forHtml.replace(new RegExp("\{#url#\}", "g"), basicData.url);
                forHtml = forHtml.replace(new RegExp("\{#subScriptIco#\}", "g"), basicData.subScriptIco);
                forHtml = forHtml.replace(new RegExp("\{#image#\}", "g"), basicData.zImage);
                forHtml = forHtml.replace(new RegExp("\{#shopName#\}", "g"), basicData.shopName);
                forHtml = forHtml.replace(new RegExp("\{#recmdReason#\}", "g"), basicData.recmdReason);
                forHtml = forHtml.replace(new RegExp("\{#timerFormatTitle#\}", "g"), timerFormatTitle);
                html += forHtml;
            }
        }
        $("#" + classId + "Html").html(html);
        this.isExecuteState[classId] = true;
        this.timerCountDownCountI++;
        if (this.timerCountDownCountI == this.timerCountDownCount) {
            var timer = setInterval('activity.timerCountDown()', 1000);
        }
    },
    timerCountDown: function () {
        var This = this;
        this.currentTime = this.currentTime + 1;
        $('.sg_g_time_FloorOne').each(function (index, element) {
            var startTime = $(this).attr('startTime');
            var endTime = $(this).attr('endTime');
            $(this).html(shangouh5.timerFormat(This.currentTime, startTime, endTime));
        });
    }
}

//买手推荐
$(function ($) {
    recommends.init();
});
function recommend(data) {
    recommends.getActivityHtml(data);
}

var recommends = {
    config: {
        'html': '<li style=" width:{#liWidth#}px;">\
                  					<a class="sg_slider_item" href="{#proUrl#}">\
                      				<div class="sg_slider_img">\
                          				<img src="{#imgUrl#}" alt="{#proName#}">\
                      				</div>\
                      				<div class="sg_slider_text">\
                         				<p class="sg_slider_tit">{#proName#}</p>\
                         				<div class="sg_slider_rec buyerClass" data-BuyerId="{#buyerId#}">\
                            				<div class="sg_slider_rec_tit">\
                               					<img src="http://img.qian-duan-she-ji.us/32x32" alt="买手">\
                                				<span>cyin说：</span> \
                            				</div>\
                            				<p class="sg_slider_rec_ctn">{#buyerTitle#}</p>\
                         				</div>\
                         				<p class="sg_slider_price"><i>&yen;</i>{#activityPrice#}</p>\
                         				<p class="sg_slider_mprice">原价：<i>&yen;</i>{#newPrice#}</p>\
                      				</div>\
                  					</a>\
                				</li>',
        'ptag': '20499.8.'
    },
    init: function () {
        this.getActivityData();
    },
    getActivityData: function () {
        var apiUrl = shangouh5.activityApi['recommend'];
        common.getApiData('h5', apiUrl, 'recommend');
    },
    getActivityHtml: function (data) {
        var html = '';
        var newData = data.data;
        var ptag = this.config.ptag;
        var barOnConfig = '<li class="on"></li>';
        var barConfig = '<li></li>';
        var barHtml = '';
        var liWidth = $('.sg_box').width();
        for (var i = 0; i < newData.length; i++) {
            var t = i + 6;
            var ptag = ptag + t;
            var forHtml = shangouh5.getProductHtml(newData[i], ptag, this.config.html);
            forHtml = forHtml.replace(new RegExp("\{#liWidth#\}", "g"), liWidth);
            forHtml = forHtml.replace(new RegExp("\{#buyerId#\}", "g"), (newData[i].vecExtData[2].strValue == '') ? 7 : newData[i].vecExtData[2].strValue);
            forHtml = forHtml.replace(new RegExp("\{#buyerTitle#\}", "g"), newData[i].vecExtData[3].strValue);
            html += forHtml;
            barHtml += (i == 0) ? barOnConfig : barConfig;
            if (i == 3) {
                break;
            }
        }
        $('.pic_list').html(html);
        $('.bar').html(barHtml);
        buyers.init(replaceHtml);
    }
}

function showPageData16945(data) {
    buyers.getActivityHtml(data);
}
var buyers = {
    init: function (func) {
        this.func = func;
        this.buyersConfig = new Array();
        this.getActivityData();
    },
    getActivityData: function () {
        var apiUrl = shangouh5.activityApi['buyers'];
        common.getApiData('h5', apiUrl, 'showPageData16945');
    },
    getActivityHtml: function (data) {
        var html = '';
        var newData = data.data;
        for (var i = 0; i < newData.length; i++) {
            var _array = {};
            _array.buyerId = newData[i].uin;
            _array.buyerName = newData[i].userNick;
            _array.buyerIco = newData[i].userImg;
            this.buyersConfig.push(_array);
        }
        this.func();
    }
}
function replaceHtml() {
    $('.buyerClass').each(function (index, element) {
        var buyerId = parseInt($(this).attr('data-BuyerId'));
        for (var i = 0; buyers.buyersConfig.length; i++) {
            if (buyerId == parseInt(buyers.buyersConfig[i].buyerId)) {
                $(this).find('img').attr('src', buyers.buyersConfig[i].buyerIco);
                $(this).find('span').html(buyers.buyersConfig[i].buyerName + '说：');
                break;
            }
        }
    });
    $('.sg_slider').slider();
}
$.fn.slider = function () {
    var oPlay = $(this);
    var aBtn = $(this).find('.bar').children();
    var oUl = $(this).find('.sg_slider_ctn').children();
    var aLi = $(this).find('.pic_list').children();
    var iNow = 0;
    var timer = null;

    oUl.css('width', aLi[0].offsetWidth * aLi.length + 'px');

    aBtn.click(function () {
        iNow = $(this).index();
        tab();
    });

    var touch_start = {}, touch_now = {}, touch_end = {}, diff = {};
    $(this).off().on('touchstart', function (e) {
        clearInterval(timer);
        touch_start.x = e.touches[0].pageX || e.pageX;
        touch_start.y = e.touches[0].pageY || e.pageY;
    }).on('touchmove', function (e) {
        touch_now.x = e.touches[0].pageX || e.pageX;
        touch_now.y = e.touches[0].pageY || e.pageY;
        diff.x = touch_now.x - touch_start.x;
        diff.y = touch_now.y - touch_start.y;
        if (Math.abs(diff.x) > 20) {
            var s = true;
        }
        if (s) {
            e.preventDefault();
        }
    }).on('touchend', function (e) {
        if (Math.abs(diff.y) < 100) {
            if (diff.x > 100) {
                previous();
            } else if (diff.x < -100) {
                next();
            }
        }
        timer = setInterval(next, 5000);
    });

    function tab() {
        aBtn.removeClass('on');
        $(aBtn[iNow]).addClass('on');
        oUl.animate({left: -iNow * aLi.width()});
    }

    function next() {
        iNow++;
        if (iNow == aLi.length) {
            iNow = 0;
        }
        tab();
    }

    function previous() {
        iNow--;
        if (iNow < 0) {
            iNow = aLi.length - 1;
        }
        tab();
    }


    timer = setInterval(next, 5000);

    oPlay.mouseover(function () {
        clearInterval(timer);
    });
    oPlay.mouseout(function () {
        timer = setInterval(next, 5000);
    });
};