$(function($) {
	activityInfo.init();
	product.init();
	lastActivity.init(activityInfo.classId);
});
//获取活动详细信息
function tjw49877194708(data){
	activityInfo.getActivityHtml(data);
}
function tjw39804314700(data){
	activityInfo.getActivityHtml(data);
}
function tjw49036654704(data){
	activityInfo.getActivityHtml(data);
}
function tjw49405104706(data){
	activityInfo.getActivityHtml(data);
}
function tjw49885884703(data){
	activityInfo.getActivityHtml(data);
}
function tjw49404884705(data){
	activityInfo.getActivityHtml(data);
}
function tjw99286274864(data){
	activityInfo.getActivityHtml(data);
}
var activityInfo = {
	config : {
		'html' : '<div class="act_g_img">{#imgHtml#}</div>\
				<div class="act_g_txt">\
					<div class="act_g_hd">\
						<span class="act_g_tit">{#activityName#}</span>\
					</div>\
					<div class="act_g_rec">\
						<p class="act_g_rec_tit">买手推荐：</p>\
						<p class="act_g_rec_ctn">{#activitDesc#}</p>\
					</div>\
					<div class="act_g_info">\
						<span class="act_g_sale">已售<em class="act_g_sale_num">{#activityCount#}</em>件</span>\
						<span class="sg_g_time activity_g_time" startTime="{#activitStartTime#}" endTime="{#activitEndTime#}">{#timerFormatTitle#}</span>\
					</div>\
				</div>'
	},
	init : function(){
		this.classId = common.getUrlParam('classId');
		this.actId = common.getUrlParam('actId');
		this.areaId = common.getUrlParam('areaId');
		this.poolId = this.areaId;
		this.currentTime = shangouh5.currentTime;
		this.getActivityData();
		this.timerCountDown();
		var timer = setInterval('activityInfo.timerCountDown()', 1000);
	},
	getActivityData : function(){
		var apiUrl = shangouh5.activityApi[this.classId];
		common.getApiData('h5', apiUrl, 'activityInfo');
	},
	//设置HTML功能函数区域
	getActivityHtml : function(data){
		var that = this;
		var data = common.getData('js', data);
		var item = {'img200x200':'', 'shopName':'', 'recmdReason':'', 'recmdRegName':'', 'visitCount':''};
		for(var i=0;i<data.length;i++){
			if(this.actId == data[i].id && this.areaId == data[i].qq){
				item.img200x200 = data[i].image;
				item.shopName = data[i].shopName;
				item.recmdReason = data[i].recmdReason;
				item.recmdRegName = data[i].recmdRegName;
				item.visitCount = data[i].visitCount;
				break;
			}
		}

		var activityHtml = this.config.html;
		if(data != undefined && typeof data == "object"){
			var getActivityHtmlUrlApi = shangouh5.activityApi['flashbuyNew'];
			var getActivityHtmlData = 'jsonlist=[{"activeId":"'+this.actId+'","poolId":"'+this.areaId+'","seedNum":"'+shangouh5.getSeedNum(this.actId, this.areaId)+'"}]';
			//通过coss获取数据
			$.ajax({
				type : 'GET',
				async: true,
				contentType : 'application/json',
				url : getActivityHtmlUrlApi,
				dataType : 'jsonp',
				data : getActivityHtmlData,
				success : function(val){
					var cossItem = val.data[0];
					var activityPrice = cossItem.arealowestPrice;
					activityPrice = (activityPrice/100).toFixed(2);
					var activityCount = (cossItem.areasalesVolume!=0) ? cossItem.areasalesVolume : cossItem.seedNum*5;
					var activitStartTime =  shangouh5.getStartTime(cossItem.beginTime);
					var activitEndTime =  shangouh5.getEndTime(cossItem.endTime);
					if(!activitStartTime || !activitEndTime){
						activitStartTime = shangouh5.getTimeStamp(item.recmdRegName);
						activitEndTime = activitStartTime + 86400 * 5;
					}
					var imgUrl = item.img200x200;
					var activityName = item.shopName;
					var activitDesc = (item.recmdReason=='')?'&nbsp;':item.recmdReason;
					var timerFormatTitle = shangouh5.timerFormat(that.currentTime, activitStartTime, activitEndTime);
					
					if(item.visitCount!=''){
						var imgHtml = '<a href="'+item.visitCount+'" title="'+item.activityName+'"><img src="'+imgUrl+'" alt="'+activityName+'"></a>';
					}else{
						var imgHtml = '<img src="'+imgUrl+'" alt="'+activityName+'">';
					}
					
					activityHtml = activityHtml.replace(new RegExp("\{#imgHtml#\}","g"), imgHtml);
					activityHtml = activityHtml.replace(new RegExp("\{#activityName#\}","g"), activityName);
					activityHtml = activityHtml.replace(new RegExp("\{#activityPrice#\}","g"), activityPrice);
					activityHtml = activityHtml.replace(new RegExp("\{#activityCount#\}","g"), activityCount);
					activityHtml = activityHtml.replace(new RegExp("\{#activitStartTime#\}","g"), activitStartTime);
					activityHtml = activityHtml.replace(new RegExp("\{#activitEndTime#\}","g"), activitEndTime);
					activityHtml = activityHtml.replace(new RegExp("\{#timerFormatTitle#\}","g"), timerFormatTitle);
					activityHtml = activityHtml.replace(new RegExp("\{#activitDesc#\}","g"), activitDesc);
					$(".act_g").html(activityHtml);
				}
			});		
		}
	},
	timerCountDown : function(){
		this.currentTime = this.currentTime + 1;
		var startTime = $('.sg_g_time').attr('startTime');
		var endTime = $('.sg_g_time').attr('endTime');
		$('.activity_g_time').html(shangouh5.timerFormat(this.currentTime, startTime, endTime));
	}
}


var product = {
	config : {
		'floorOneHtml' : '<a class="sg_g4" href="{#proUrl#}">\
							<div class="sg_g4_img">\
								<img src="{#imgUrl#}" alt="{#proName#}">\
							</div>\
							<div class="sg_g4_info">\
								<p class="sg_g4_tit">{#proName#}</p>\
								<span class="sg_g4_price"><i>￥</i>{#activityPrice#}</span>\
								<span class="sg_g4_price sg_g4_mprice"><i>￥</i><del>{#newPrice#}</del></span>\
							</div>\
						</a>',
		'floorTwoHtml' : '<li style=" width:{#liWidth#}px;">\
                  					<a class="sg_slider_item" href="{#proUrl#}">\
                      				<div class="sg_slider_img">\
                          				<img src="{#imgUrl#}" alt="{#proName#}">\
                      				</div>\
                      				<div class="sg_slider_text">\
                         				<p class="sg_slider_tit">{#proName#}</p>\
                         				<div class="sg_slider_rec buyerClass" data-BuyerId="{#buyerId#}">\
                            				<div class="sg_slider_rec_tit">\
                               					<img src="http://img.qian-duan-she-ji.us/32x32" alt="买手">\
                                				<span>cyin说：</span> \
                            				</div>\
                            				<p class="sg_slider_rec_ctn">{#buyerTitle#}</p>\
                         				</div>\
                         				<p class="sg_slider_price"><i>&yen;</i>{#activityPrice#}</p>\
                         				<p class="sg_slider_mprice">原价：<i>&yen;</i>{#newPrice#}</p>\
                      				</div>\
                  					</a>\
                				</li>',
		'floorThreeHtml' : '<a class="sg_g4" href="{#proUrl#}">\
							<div class="sg_g4_img">\
								<img src="{#imgUrl#}" alt="{#proName#}">\
							</div>\
							<div class="sg_g4_info">\
								<p class="sg_g4_tit">{#proName#}</p>\
								<span class="sg_g4_price"><i>&yen;</i>{#activityPrice#}</span>\
								<span class="sg_g4_price sg_g4_mprice"><i>&yen;</i><del>{#newPrice#}</del></span>\
							</div>\
						</a>',
		'floorFourHtml' : '<a class="sg_g4" href="{#proUrl#}">\
							<div class="sg_g4_img">\
								<img src="{#imgUrl#}" alt="{#proName#}">\
							</div>\
							<div class="sg_g4_info">\
								<p class="sg_g4_tit">{#proName#}</p>\
								<span class="sg_g4_price"><i>&yen;</i>{#activityPrice#}</span>\
								<span class="sg_g4_price sg_g4_mprice"><i>&yen;</i><del>{#newPrice#}</del></span>\
							</div>\
						</a>',
		'floorOnePtag' : '20499.8.17',
		'floorTwoPtag' : '20499.8.18',
		'floorThreePtag' : '20499.8.19',
		'floorFourPtag' : '20499.8.20'
	},
	init : function(){
		this.actionCDN = false;
		this.actId = activityInfo.actId;
		this.poolId = activityInfo.poolId;
		this.getCdnProductData();
	},
	getCdnProductData : function(){
		var apiUrl = shangouh5.activityApi['productCDN'];
		apiUrl = apiUrl.replace(new RegExp("\{#actid#\}","g"), this.actId);
		var that = this;
		$.ajax({
			type : 'GET',
			async: false,
			contentType : 'application/json',
			url : apiUrl,
			dataType : 'jsonp',
			jsonpCallback : 'activeshow_'+this.actId+'_cb',
			data : '',
			success: function(data){
				that.actionCDN = true;
				that.setFloorData(data);
			}
		});
		setTimeout(function(){
			if(!this.actionCDN){
				that.getCossProductData();
			}
		}, 1000);
	},
	getCossProductData : function(){
		var apiUrl = shangouh5.activityApi['productNew'];
		common.getApiDataH5(apiUrl, 'setFloorData', '?sort=1&ids='+this.actId+'&ctl='+this.poolId+':128');
	},
	setFloorData : function(data){
		var vecTabs = common.vecTabsSort(data.data[0].vecAreas[0].vecTabs);
		this.floorOne = [];
		this.floorTwo = [];
		this.floorThree = [];
		this.floorFour = [];
		for(var i=0,length=vecTabs.length;i<length;i++){
			if(vecTabs[i].vecExtData == null){
				this.floorFour.push(vecTabs[i]);
			}else{
				if(vecTabs[i].vecExtData[1] != undefined){
					switch(vecTabs[i].vecExtData[1].strValue){
						case '1':
							this.floorOne.push(vecTabs[i]);
							break;
						case '2':
							this.floorTwo.push(vecTabs[i]);
							break;
						case '3':
							this.floorThree.push(vecTabs[i]);
							break;
						default:
							this.floorFour.push(vecTabs[i]);
							break;
					}
				}else{
					this.floorFour.push(vecTabs[i]);
				}
			}
		}
		this.replaceFloorOneHtml();
		this.replaceFloorTwoHtml();
		this.replaceFloorThreeHtml();
		this.replaceFloorFourHtml();
	},
	//替换商品列表html
	replaceFloorOneHtml : function(vecTabs){
		var html = '';
		var proHtml = this.config.floorOneHtml;
		var ptag = this.config.floorOnePtag;
		if(this.floorOne.length != 0){
			for(var i=0;i<this.floorOne.length;i++){
				html += shangouh5.getProductHtml(this.floorOne[i], ptag, proHtml);
			}
			$('#floorOne').show();
			$('#floorOne .sg_glist4').html(html);
		}
	},
	replaceFloorTwoHtml : function(vecTabs){
		var html = '';
		var proHtml = this.config.floorTwoHtml;
		var ptag = this.config.floorTwoPtag;
		var barOnConfig = '<li class="on"></li>';
		var barConfig = '<li></li>';
		var barHtml = '';
		var liWidth = $('.act_g').width();
		if(this.floorTwo.length != 0){
			for(var i=0;i<this.floorTwo.length;i++){
				var forHtml = shangouh5.getProductHtml(this.floorTwo[i], ptag, proHtml);
				forHtml = forHtml.replace(new RegExp("\{#liWidth#\}","g"), liWidth);
				forHtml = forHtml.replace(new RegExp("\{#buyerId#\}","g"), this.floorTwo[i].vecExtData[2].strValue);
				forHtml = forHtml.replace(new RegExp("\{#buyerTitle#\}","g"), this.floorTwo[i].vecExtData[3].strValue);
				html += forHtml;
				barHtml += (i==0)?barOnConfig:barConfig;
				if(i==3){
					break;
				}
			}
			$('.pic_list').html(html);
			$('.bar').html(barHtml);
			$('#floorTwo').show();
			buyers.init(replaceHtml);
		}
	},
	replaceFloorThreeHtml : function(vecTabs){
		var html = '';
		var proHtml = this.config.floorThreeHtml;
		var ptag = this.config.floorThreePtag;
		if(this.floorThree.length != 0){
			for(var i=0;i<this.floorThree.length;i++){
				html += shangouh5.getProductHtml(this.floorThree[i], ptag, proHtml);
			}
			$('#floorThree').show();
			$('#floorThree .sg_glist4').html(html);
		}
	},
	replaceFloorFourHtml : function(vecTabs){
		var html = '';
		var proHtml = this.config.floorFourHtml;
		var ptag = this.config.floorFourPtag;
		if(this.floorFour.length != 0){
			for(var i=0;i<this.floorFour.length;i++){
				html += shangouh5.getProductHtml(this.floorFour[i], ptag, proHtml);
			}
			$('#floorFour').show();
			$('#floorFour .sg_glist4').html(html);
		}
	}
}
function setFloorData(data){
	product.setFloorData(data);
}



function showPageData16945(data){
	buyers.getActivityHtml(data);
}
var buyers = {
	init : function(func){
		this.func = func;
		this.buyersConfig = new Array();
		this.getActivityData();
	},
	getActivityData : function(){
		var apiUrl = shangouh5.activityApi['buyers'];
		common.getApiData('h5', apiUrl, 'showPageData16945');
	},
	getActivityHtml : function(data){
		var html = '';
		var newData = data.data;
		for(var i=0;i<newData.length;i++){
			var _array = {};
			_array.buyerId = newData[i].uin;
			_array.buyerName = newData[i].userNick;
			_array.buyerIco = newData[i].userImg;
			this.buyersConfig.push(_array);
		}
		this.func();
	}
}
function replaceHtml(){
	$('.buyerClass').each(function(index, element) {
		var buyerId = parseInt($(this).attr('data-BuyerId'));
		for(var i=0;buyers.buyersConfig.length;i++){
			if(buyerId == parseInt(buyers.buyersConfig[i].buyerId)){
				$(this).find('img').attr('src', buyers.buyersConfig[i].buyerIco);
				$(this).find('span').html(buyers.buyersConfig[i].buyerName + '说：');
				break;
			}
		}
	});
	$('.sg_slider').slider();
}
$.fn.slider=function(){
	var oPlay = $(this);
	var aBtn = $(this).find('.bar').children();
	var oUl = $(this).find('.sg_slider_ctn').children();
	var aLi = $(this).find('.pic_list').children();
	var iNow = 0;
	var timer = null;

	oUl.css('width', aLi[0].offsetWidth*aLi.length+'px');

	aBtn.click(function(){
		iNow=$(this).index();
		tab();
	});
	
	var touch_start={},touch_now={},touch_end={},diff={};
	$(this).off().on('touchstart',function(e){
		clearInterval(timer);
		touch_start.x = e.touches[0].pageX||e.pageX;
		touch_start.y = e.touches[0].pageY||e.pageY;
	}).on('touchmove',function(e){
		touch_now.x = e.touches[0].pageX||e.pageX;
		touch_now.y = e.touches[0].pageY||e.pageY;
		diff.x = touch_now.x - touch_start.x;
		diff.y = touch_now.y - touch_start.y;
		if(Math.abs(diff.x)>20){var s=true;}
		if(s){e.preventDefault();}
	}).on('touchend',function(e){
		if(Math.abs(diff.y)<100){
			if(diff.x>100){
				previous();
			}else if(diff.x<-100){
				next();
			}
		}
		timer=setInterval(next, 5000);
	});

	function tab(){
		aBtn.removeClass('on');
		$(aBtn[iNow]).addClass('on');
		oUl.animate({left:-iNow*aLi.width()});
	}

	function next(){
		iNow++;
		if(iNow==aLi.length){
			iNow=0;
		}
		tab();
	}
	
	function previous(){
		iNow--;
		if(iNow<0){
			iNow=aLi.length-1;
		}
		tab();
	}
	

	timer=setInterval(next, 5000);

	oPlay.mouseover(function(){
		clearInterval(timer);
	});
	oPlay.mouseout(function(){
		timer=setInterval(next, 5000);
	});
};