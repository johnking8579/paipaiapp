window.onload = function(){
	//判断是否显示导航
	if(common.getUrlParam('app') != null){
		$('.mod_bar').show();
		$('body').attr('class', 'bar_fixed');
		$('.mod_nav_my').attr('id', 'loginbak');
	}else if(common.getUrlParam('_wv') != null){
		$('.mod_bar_login').attr('id', 'loginbak');
		$('.mod_nav').show();
	}else{
		$('.mod_bar').show();
		$('.mod_bar_crumb').show();
		$('.mod_bar_login').show();
		$('.mod_nav_my').attr('id', 'loginbak');
	}
	flashSale.init();
	common.setShareInfo();
	var timer = setInterval('flashSale.timerCountDown()', 1000);
}

function tjw49877194708(data){
	flashSale.getActivityHtml(data);
}
function tjw39804314700(data){
	flashSale.getActivityHtml(data);
}
//测试
function tjw59010184723(data){
	flashSale.getActivityHtml(data);
}
function tjw49036654704(data){
	flashSale.getActivityHtml(data);
}
function tjw49405104706(data){
	flashSale.getActivityHtml(data);
}
function tjw49885884703(data){
	flashSale.getActivityHtml(data);
}
function tjw49404884705(data){
	flashSale.getActivityHtml(data);
}
function getActivityProductHtmlNew(data){
	flashSale.getActivityProductHtmlNew(data);
}

var flashSale = {
	//功能函数
	//页面操作功能函数
	init : function(){
		this.actionCDN = false;
		this.actId = common.getUrlParam('actId');
		this.areaId = common.getUrlParam('areaId');
		this.poolId = this.areaId;
		this.classId = common.getUrlParam('classId');
		shangouh5.getNavHtml(this.classId);
		this.currentTime = commonDate.getTime(commonDate.currentDate())/1000;
		this.getActivityData();
		
	},
	timerCountDown : function(){
		var This = this;
		this.currentTime = this.currentTime + 1;
		var startTime = $('.sg_g_time').attr('startTime');
		var endTime = $('.sg_g_time').attr('endTime');
		$('.sg_g_time').html(This.timerFormat(this.currentTime, startTime, endTime));
	},
	timerFormat : function(currentTime, startTime, endTime){
		var title = '';
		if(currentTime<startTime){
			title = '<span>即将开始</span>';
		}else if(currentTime>=endTime){
			if(typeof timer != 'undefined'){
				clearInterval(timer);
			}
			title = '<span>活动已结束</span>';
		}else{
			var differenceTime = endTime - currentTime;
			var differenceTimeTitle = commonDate.getDifferTimeTitle(differenceTime);
			title = '<span>'+differenceTimeTitle.D+'</span>天<span>'+differenceTimeTitle.H+'</span>时<span>'+differenceTimeTitle.I+'</span>分';
		}
		return title;
	},
	//获取数据功能函数区域
	getActivityData : function(index){
		var apiUrl = common.activityApi[this.classId];
		common.getApiData('h5', apiUrl, 'flashSale.getActivityHtml');
	},
	
	getActivityProductDataNew : function(){
		var apiUrl = common.activityApi['productCDN'];
		apiUrl = apiUrl.replace(new RegExp("\{#actid#\}","g"), this.actId);
		var that = this;
		$.ajax({
			type : 'GET',
			async: false,
			contentType : 'application/json',
			url : apiUrl,
			dataType : 'jsonp',
			jsonpCallback : 'activeshow_'+this.actId+'_cb',
			data : '',
			success: function(data){
				that.getActivityProductHtmlCDN(data);
			}
		});
		setTimeout(function(){
			if(!this.actionCDN){
				that.getCossProductData();
			}
		}, 1000);
	},
	getCossProductData : function(){
		var apiUrl = common.activityApi['productNew'];
		common.getApiDataH5(apiUrl, 'getActivityProductHtmlNew', '?sort=1&ids='+this.actId+'&ctl='+this.poolId+':128');
	},
	//获取时间戳，date格式为20140926
	getTimeStamp : function(date){
		return commonDate.getTime(date.substr(0,4)+"-"+date.substr(4,2)+"-"+date.substr(6,2)+" "+"10:00:00")/1000;
	},
	//设置HTML功能函数区域
	getActivityHtml : function(data){
		var This = this;
		var data = common.getData('js', data);
		var item = {'img200x200':'', 'shopName':'', 'recmdReason':'', 'recmdRegName':''};
		for(var i=0;i<data.length;i++){
            var t = this.actId+'|'+this.areaId;
            var dataActId = dataAreaId = 0;
            if(data[i].qq.indexOf("|") > 0 || data[i].qq.indexOf("_") > 0){
                if(data[i].qq.indexOf("_") > 0){
                    var actAreaArray = data[i].qq.split('_');
                }else{
                    var actAreaArray = data[i].qq.split('|');
                }
                if(actAreaArray.length>0){
                    dataActId = actAreaArray[0];
                    dataAreaId = (actAreaArray[1] == undefined)?0:actAreaArray[1];
                }
            }else{
                dataActId = data[i].id;
                dataAreaId = data[i].qq;
            }
            if(this.actId == dataActId && this.areaId == dataAreaId){
                item.img200x200 = data[i].image;
                item.shopName = data[i].shopName;
                item.recmdReason = data[i].recmdReason;
                item.recmdRegName = data[i].recmdRegName;
                break;
            }
		}

		var activityHtml = this.getConfigHtml('activityHtml');
		if(data != undefined && typeof data == "object"){
			var getActivityHtmlUrlApi = common.activityApi['flashbuyNew'];
			var getActivityHtmlData = 'jsonlist=[{"activeId":"'+this.actId+'","poolId":"'+this.areaId+'","seedNum":"'+shangouh5.getSeedNum(this.actId, this.areaId)+'"}]';
			//通过coss获取数据
			$.ajax({
				type : 'GET',
				async: true,
				contentType : 'application/json',
				url : getActivityHtmlUrlApi,
				dataType : 'jsonp',
				data : getActivityHtmlData,
				success : function(val){
					var cossItem = val.data[0];
					var activityPrice = cossItem.arealowestPrice;
					activityPrice = (activityPrice/100).toFixed(2);
					var activityCount = (cossItem.areasalesVolume!=0) ? cossItem.areasalesVolume : cossItem.seedNum*5;
					var activitStartTime =  shangouh5.getStartTime(cossItem.beginTime);
					var activitEndTime =  shangouh5.getEndTime(cossItem.endTime);
					if(!activitStartTime || !activitEndTime){
						activitStartTime = This.getTimeStamp(item.recmdRegName);
						activitEndTime = activitStartTime + 86400 * 5;
					}
					var imgUrl = item.img200x200;
					var activityName = item.shopName;
					var activitDesc = (item.recmdReason=='')?'&nbsp;':item.recmdReason;
					var timerFormatTitle = This.timerFormat(This.currentTime, activitStartTime, activitEndTime);
					
					activityHtml = activityHtml.replace(new RegExp("\{#imgUrl#\}","g"), imgUrl);
					activityHtml = activityHtml.replace(new RegExp("\{#activityName#\}","g"), activityName);
					activityHtml = activityHtml.replace(new RegExp("\{#activityPrice#\}","g"), activityPrice);
					activityHtml = activityHtml.replace(new RegExp("\{#activityCount#\}","g"), activityCount);
					activityHtml = activityHtml.replace(new RegExp("\{#activitStartTime#\}","g"), activitStartTime);
					activityHtml = activityHtml.replace(new RegExp("\{#activitEndTime#\}","g"), activitEndTime);
					activityHtml = activityHtml.replace(new RegExp("\{#timerFormatTitle#\}","g"), timerFormatTitle);
					activityHtml = activityHtml.replace(new RegExp("\{#activitDesc#\}","g"), activitDesc);
					$("#actHtml").html(activityHtml);
				}
			});		
		}
		this.getActivityProductDataNew();
	},
	
	getActivityProductHtmlNew : function(data){
		var data = common.getData('cossNew', data);
		data = this.vecTabsSort(data.vecAreas[0].vecTabs);
		var html = '';
		var proHtml = this.getConfigHtml('proHtml');
		if(data != undefined && typeof data == "object"){
			var ptag = shangouh5.ptagConfig[this.classId][2];
			for(var i=0;i<data.length;i++){
				var forHtml = proHtml;
				var item = data[i];
				var activityPrice = (item["dwActivePrice"] == '') ? item["dwPrice"] : item["dwActivePrice"];
				activityPrice = (activityPrice/100).toFixed(2);
				var newPrice = item["dwMarketPrice"];
				newPrice = (newPrice/100).toFixed(2);
				var proUrl = '';
				if(common.getUrlParam('app') != null){
					proUrl = 'qqbuyjump:/ppitem?ptag='+ptag+'&itemId='+item["strCommodityId"];
				}else{
					proUrl = 'http://b.paipai.com/itemweb/item?ptag='+ptag+'&scence=101&ic='+item["strCommodityId"];
				}
				var imgUrl = (item["mapCustomData"]["uploadPicUrl1"] == '') ? item["strPic"] : item["mapCustomData"]["uploadPicUrl1"];
				forHtml = forHtml.replace(new RegExp("\{#imgUrl#\}","g"), imgUrl);
				forHtml = forHtml.replace(new RegExp("\{#proName#\}","g"), item["strTitle"]);
				forHtml = forHtml.replace(new RegExp("\{#proUrl#\}","g"), proUrl);
				forHtml = forHtml.replace(new RegExp("\{#activityPrice#\}","g"), activityPrice);
				forHtml = forHtml.replace(new RegExp("\{#newPrice#\}","g"), newPrice);
				forHtml = forHtml.replace(new RegExp("\{#soldCount#\}","g"), 100);
				html += forHtml;
			}
		}
		$("#proHtml").html(html);
	},
	
	getActivityProductHtmlCDN : function(data){
		var html = '';
		var proHtml = this.getConfigHtml('proHtml');
		if(data != undefined && typeof data == "object"){
			var data = data.data.vecAreas;
			for(var i=0;i<data.length;i++){
				if(this.poolId == data[i].dwPoolId){
					var data = data[i];
				}
			}
			var data = this.vecTabsSort(data.vecTabs);
			var ptag = shangouh5.ptagConfig[this.classId][2];
			for(var i=0;i<data.length;i++){
				var forHtml = proHtml;
				var item = data[i];
				var activityPrice = (item["dwActivePrice"] == '') ? item["dwPrice"] : item["dwActivePrice"];
				activityPrice = (activityPrice/100).toFixed(2);
				var newPrice = item["dwMarketPrice"];
				newPrice = (newPrice/100).toFixed(2);
				var proUrl = '';
				if(common.getUrlParam('app') != null){
					proUrl = 'qqbuyjump:/ppitem?ptag='+ptag+'&itemId='+item["strCommodityId"];
				}else{
					proUrl = 'http://b.paipai.com/itemweb/item?ptag='+ptag+'&scence=101&ic='+item["strCommodityId"];
				}
				var imgUrl = (item["mapCustomData"]["uploadPicUrl1"] == '') ? item["strPic"] : item["mapCustomData"]["uploadPicUrl1"];
				forHtml = forHtml.replace(new RegExp("\{#imgUrl#\}","g"), imgUrl);
				forHtml = forHtml.replace(new RegExp("\{#proName#\}","g"), item["strTitle"]);
				forHtml = forHtml.replace(new RegExp("\{#proUrl#\}","g"), proUrl);
				forHtml = forHtml.replace(new RegExp("\{#activityPrice#\}","g"), activityPrice);
				forHtml = forHtml.replace(new RegExp("\{#newPrice#\}","g"), newPrice);
				forHtml = forHtml.replace(new RegExp("\{#soldCount#\}","g"), 100);
				html += forHtml;
			}
		}else{
			this.getCossProductData();
		}
		$("#proHtml").html(html);
	},
	vecTabsSort : function(vecTabs){
		vecTabs = vecTabs.sort(function(a,b){
			return a.dwOrderNum - b.dwOrderNum;
		});
		var temp = 0;
		for(;temp< vecTabs.length;temp++)
		{
			if(vecTabs[temp]["dwOrderNum"]!=0){
				break;
			}
		}
		return vecTabs.concat(vecTabs.splice(0,temp));
	},
	
	
	//配置HTML代码区域
	getConfigHtml : function(index){
		var html = {
			'activityHtml' : '<div class="sg_detail_g sg_g">\
				<div class="sg_g_img">\
					<img src="{#imgUrl#}" alt="{#activityName#}">\
				</div>\
				<div class="sg_g_info">\
						<p class="sg_g_tit">{#activityName#}<i class="sg_g_new"></i></p>\
						<p class="sg_g_price"><span>&yen;{#activityPrice#}</span>起</p>\
						<p class="sg_g_sale">已售<span>{#activityCount#}</span>件</p>\
						<p class="sg_g_time" startTime="{#activitStartTime#}" endTime="{#activitEndTime#}">{#timerFormatTitle#}</p>\
					</div>\
			</div>\
			<div class="sg_detail_rec">\
				<p class="sg_detail_rec_form">买手推荐：</p>\
				<p class="sg_detail_rec_reason">{#activitDesc#}</p>\
			</div>'
			,
			'proHtml' : '<li>\
				<a class="sg_g" href="{#proUrl#}">\
					<div class="sg_g_img">\
						<img src="{#imgUrl#}" alt="{#proName#}">\
					</div>\
					<div class="sg_g_info">\
						<p class="sg_g_name">{#proName#}</p>\
						<p class="sg_g_price">\
							<span>&yen;{#activityPrice#}</span>\
							<del>&yen;{#newPrice#}</del>\
						</p>\
					</div>\
				</a>\
			</li>'
		}
		return html[index];
	}
}