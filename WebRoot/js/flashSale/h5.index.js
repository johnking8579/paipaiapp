$(document).ready(function () {
	flashSale.init();
	common.setShareInfo();
});

var flashSale = {
	isExecuteState : {
		'women' : false,	//女人
		'children' : false,	//儿童
		'men' : false,	//男人
		'life' : false,	//生活家居
		'outdoor' : false	//运动户外
	},
	//功能函数
	//页面操作功能函数
	init : function(){
		this.isPoolId = true;//记录是否为商品池ID true是 false否
		//判断是否显示导航和是否app引用
		this.isApp = '';
		if(common.getUrlParam('app') != null){
			$('.mod_nav_my').attr('id', 'loginbak');
			$('.mod_bar').hide();
			$('body').children().eq(1).attr('class', 'mod_page sg sg_fixed');
			this.isApp = '&app='+common.getUrlParam('app');
		}
		//判断是否是手Q引用
		this.is_wv = '';
		if(common.getUrlParam('_wv') != null){
			$('.mod_bar_login').attr('id', 'loginbak');
			$('.mod_bar').hide();
			$('.mod_nav').show();
			this.is_wv = '&_wv='+common.getUrlParam('_wv');
		}
		
		shangouh5.getNavHtml();
		//获取当前时间 //1411315199 1410796700
		var currentDateTime = commonDate.currentDate();
		this.currentTime = commonDate.getTime(currentDateTime)/1000;
		var dateTitle = currentDateTime;
		//矫正10小时误差
		dateTitle10 = dateTitle.split(' ');
		var date10 = dateTitle10[1].split(':');
		this.currentTime10 = this.currentTime;
		if(date10[0] < 10){
			this.currentTime10 = this.currentTime - 86400;
			dateTitle = commonDate.getDate('Y-m-d H:i:s', this.currentTime10);
		}
		
		dateTitle = dateTitle.split(' ');
		dateTitle = dateTitle[0].replace(/-/g, '');
		this.date = dateTitle;
		this.tjwData = {
			'todaySale' : new Array(),
			'women' : new Array(),
			'children' : new Array(),
			'men' : new Array(),
			'life' : new Array(),
			'outdoor' : new Array()
		}
		this.classId = $('.sg_nav_list').attr('id');
		//默认打开导航
    	if(this.classId == 'outdoor'){
			$('.sg_nav').toggleClass('sg_nav_on');
		}
		if(document.body.clientWidth > 385){
			$('.sg_nav_trigger').hide();
		}
		shangouh5.getNavHtml(this.classId);
		if(this.classId == 'todaySale'){
			this.getActivityData('todaySale');
			this.getActivityData('women');
			this.getActivityData('children');
			this.getActivityData('men');
			this.getActivityData('life');
			this.getActivityData('outdoor');
		}else{
			this.getActivityData(this.classId);
		}
	},
	timerCountDown : function(){
		var This = this;
		this.currentTime = this.currentTime + 1;
		$('.sg_g_time').each(function(index, element) {
			var startTime = $(this).attr('startTime');
			var endTime = $(this).attr('endTime');
			$(this).html(This.timerFormat(This.currentTime, startTime, endTime));		
        });
	},
	timerFormat : function(currentTime, startTime, endTime){
		var title = '';
		if(currentTime<startTime){
			title = '<span>即将开始</span>';
		}else if(currentTime>=endTime){
			location.reload(true);
			title = '<span>00</span>天<span>00</span>时<span>00</span>分<span>00</span>秒';
			//title = '<span>活动已结束</span>';
		}else{
			var differenceTime = endTime - currentTime;
			var differenceTimeTitle = commonDate.getDifferTimeTitle(differenceTime);
			title = '<span>'+differenceTimeTitle.D+'</span>天<span>'+differenceTimeTitle.H+'</span>时<span>'+differenceTimeTitle.I+'</span>分';
		}
		return title;
	},
	//获取数据功能函数区域
	getActivityData : function(index){
		var apiUrl = common.activityApi[index];
		common.getApiData('h5', apiUrl, 'flashSale.getActivityHtml');
	},
	
	//设置HTML功能函数区域
	getActivityTjwData : function(data, classId){
		//通过ppms获取推荐位数据
		var data = common.getData('js', data);
		if(data != undefined && typeof data == "object"){
			var tjwDataArray = tjwDataArrayOld = new Array();
			var ptag = '';
			if(this.classId == 'todaySale'){
				ptag = shangouh5.ptagConfig[classId][0];
				//今日特卖--首页
				if(classId == 'todaySale'){
					var t = 5;
				}else if(classId == 'women' || classId == 'children' || classId == 'men'){
					var t = 6;
				}else{
					var t = 3;
				}
				var newData = this.getActivityOldData(data);
				for(var i=0;i<newData.length;i++){
					if(i < t){
						tjwDataArray.push(newData[i]);
					}else{
						break;
					}
				}
			}else{
				//分类页
				//获取今日主打
				for(var i=0;i<data.length;i++){
					var item = data[i];
					if(item != undefined && typeof item == "object"){
						if(item.recmdRegName == this.date){
							tjwDataArray.push(item);
						}
					}
				}
				//获取剩余
				for(var i=0;i<data.length;i++){
					var item = data[i];
					if(item != undefined && typeof item == "object"){
						if(item.recmdRegName != this.date && item.recmdRegName != ''){
							tjwDataArray.push(item);
						}
					}
				}
				tjwDataArray = this.getFilterRepeatDate(tjwDataArray);
				ptag = shangouh5.ptagConfig[classId][1];
			}

			for(var i=0;i<tjwDataArray.length;i++){
				var item = tjwDataArray[i];
				/*var actAdSentence = item.adSentence;
				if(actAdSentence != undefined){
					var actAdSentenceArray = actAdSentence.split('|');
					if(actAdSentenceArray.length>0){
						zImage = actAdSentenceArray[0]; //获取今日主打图片
					}
				}*/
				var zImage = item.image;

				var actAreaId = item.qq;



			/*	if(actAreaId != undefined){
					if(actAreaId.indexOf("_") > 0){
						var actAreaArray = actAreaId.split('_');
						this.isPoolId = false;
					}else{
						var actAreaArray = actAreaId.split('|');
						this.isPoolId = true;
					}
					if(actAreaArray.length>0){
						actId = actAreaArray[0];
						areaId = (actAreaArray[1] == undefined)?0:actAreaArray[1];
					}
				}*/
                if(item.qq.indexOf("|") > 0 || item.qq.indexOf("_") > 0){
                    if(actAreaId.indexOf("_") > 0){
                        var actAreaArray = actAreaId.split('_');
                        this.isPoolId = false;
                    }else{
                        var actAreaArray = actAreaId.split('|');
                        this.isPoolId = true;
                    }
                    if(actAreaArray.length>0){
                        actId = actAreaArray[0];
                        areaId = (actAreaArray[1] == undefined)?0:actAreaArray[1];
                    }
                }else{
                    actId = item.id;
                    areaId = item.qq;
                }

				var subScriptIco = '';
				if(item.favNum != ''){
					//subScriptIco = '<span class="sg_g_tag">'+item.favNum+'</span>';
				}
				//推荐位数据存放到数组中
				this.tjwData[classId].push({
					'image' : item.image,
					'zImage' : zImage,
					'shopName' : item.shopName,
					'recmdReason' : item.recmdReason,
					'userCredit' : item.userCredit,
					'favNum' : item.favNum,
					'remdRegName' : item.recmdRegName,
					'subScriptIco' : subScriptIco,
					'actId' : actId,
					'areaId' : areaId,
					'classId' : classId,
					'url' : "detail.html?ptag="+ptag+"&classId="+classId+"&actId="+actId+"&areaId="+areaId+this.isApp+this.is_wv
				})
			}

			if(this.isPoolId){
				this.getActivityCossDataNew(classId);
			}else{
				this.getActivityCossData(classId);
			}
		}
	},
	
	getFilterRepeatDate : function(data){
		var newData = new Array();
		for(var i=0;i<data.length;i++){
			var state = 0;
			for(var j=i;j<data.length;j++){
				if(i != j && data[i].qq == data[j].qq){
					state = 1;
					break;
				}
			}
			if(state == 0){
				newData.push(data[i]);
			}
		}
		return newData;
	},
	
	getActivityOldData : function(data){
		var newData = new Array();
		var dateOld1 = this.currentTime10;//今天
		var dateOld2 = this.currentTime10 - 86400;//前一天
		var dateOld3 = this.currentTime10 - 86400 * 2;//前二天
		var dateOld4 = this.currentTime10 - 86400 * 3;//前三天
		var dateOld5 = this.currentTime10 - 86400 * 4;//前四天
		
		var dateTitleOld1 = commonDate.getDate('Ymd', dateOld1);//今天
		var dateTitleOld2 = commonDate.getDate('Ymd', dateOld2);//前一天
		var dateTitleOld3 = commonDate.getDate('Ymd', dateOld3);//前二天
		var dateTitleOld4 = commonDate.getDate('Ymd', dateOld4);//前三天
		var dateTitleOld5 = commonDate.getDate('Ymd', dateOld5);//前四天
		for(var i=0;i<data.length;i++){
			var item = data[i];
			if(item == undefined || typeof item != "object"){
				continue;
			}
			if(item.recmdRegName == dateTitleOld1 || item.recmdRegName == dateTitleOld2 || item.recmdRegName == dateTitleOld3 || item.recmdRegName == dateTitleOld4 || item.recmdRegName == dateTitleOld5){
				newData.push(item);
			}
		}
		return this.sort(newData);
	},
	
	sort : function (data){
		var newData = new Array();
		for(var i=0;i<data.length;i++){
			if(data[i] == undefined || typeof data[i] != "object"){
				continue;
			}else{
				newData.push(data[i]);
			}
		}
		if(newData.length>1){
			for(var i=0;i<newData.length;i++){
				for(var j=newData.length-1;j>0;j--){
					if(parseInt(newData[j].recmdRegName) > parseInt(newData[j-1].recmdRegName)){
						var tmp = newData[j];
						newData[j] = newData[j-1];
						newData[j-1] = tmp;
					}
				}
			}
		}
		return newData;
	},
	
	sortEndTime : function (data){
		if(data.length>1){
			for(var i=0;i<data.length;i++){
				for(var j=data.length-1;j>0;j--){
					if(parseInt(data[j].endTime) > parseInt(data[j-1].endTime)){
						var tmp = data[j];
						data[j] = data[j-1];
						data[j-1] = tmp;
					}
				}
			}
		}
		return data;
	},
	
	getActivityCossData : function(classId){
		if(this.tjwData[classId] != undefined && typeof this.tjwData[classId] == "object"){
			var jsonList = '[';
			for(var i=0;i<this.tjwData[classId].length;i++){
				var item = this.tjwData[classId][i];
				if(i!=0){jsonList += ','}
				var seedNum = (item.userCredit == undefined || item.userCredit == '')?shangouh5.getSeedNum(item.actId, item.areaId):(isNaN(item.userCredit))?shangouh5.getSeedNum(item.actId, item.areaId):item.userCredit;
				jsonList += '{"activeId":"'+item.actId+'","areaId":"'+item.areaId+'","seedNum":"'+seedNum+'"}';
			}
			jsonList += ']';
			
			var callbackName = '';
			switch(classId){
				case 'todaySale':
					callbackName = 'flashSale.getActivityTodaySaleHtml';
					break;
				case 'women':
					callbackName = 'flashSale.getActivityWomenHtml';
					break;
				case 'children':
					callbackName = 'flashSale.getActivityChildrenHtml';
					break;
				case 'men':
					callbackName = 'flashSale.getActivityMenHtml';
					break;
				case 'life':
					callbackName = 'flashSale.getActivityLifeHtml';
					break;
				case 'outdoor':
					callbackName = 'flashSale.getActivityOutdoorHtml';
					break;
			}

			//通过coss获取数据
			$.ajax({
				type : 'GET',
				async: false,
				contentType : 'application/json',
				url : common.activityApi['flashbuy'],
				dataType : 'jsonp',
				jsonpCallback : callbackName,
				data : "jsonlist=" + jsonList
			});
		}
	},
	
	getActivityCossDataNew : function(classId){
		if(this.tjwData[classId] != undefined && typeof this.tjwData[classId] == "object"){
			var jsonList = '[';
			for(var i=0;i<this.tjwData[classId].length;i++){
				var item = this.tjwData[classId][i];
				if(i!=0){jsonList += ','}
				var seedNum = (item.userCredit == undefined || item.userCredit == '')?shangouh5.getSeedNum(item.actId, item.areaId):(isNaN(item.userCredit))?shangouh5.getSeedNum(item.actId, item.areaId):item.userCredit;
				jsonList += '{"activeId":"'+item.actId+'","poolId":"'+item.areaId+'","seedNum":"'+seedNum+'"}';
			}
			jsonList += ']';

			var callbackName = '';
			switch(classId){
				case 'todaySale':
					callbackName = 'flashSale.getActivityTodaySaleHtml';
					break;
				case 'women':
					callbackName = 'flashSale.getActivityWomenHtml';
					break;
				case 'children':
					callbackName = 'flashSale.getActivityChildrenHtml';
					break;
				case 'men':
					callbackName = 'flashSale.getActivityMenHtml';
					break;
				case 'life':
					callbackName = 'flashSale.getActivityLifeHtml';
					break;
				case 'outdoor':
					callbackName = 'flashSale.getActivityOutdoorHtml';
					break;
			}

			//通过coss获取数据
			$.ajax({
				type : 'GET',
				contentType : 'application/json',
				url : common.activityApi['flashbuyNew'],
				dataType : 'jsonp',
				jsonpCallback : callbackName,
				data : "jsonlist=" + jsonList
			});
		}
	},

	getActivityTodaySaleHtml : function(data){
		this.getActivityHtml(data, 'todaySale', true);
	},
	
	getActivityWomenHtml : function(data){
		this.getActivityHtml(data, 'women', true);
	},
	
	getActivityChildrenHtml : function(data){
		this.getActivityHtml(data, 'children', true);
	},
	
	getActivityMenHtml : function(data){
		this.getActivityHtml(data, 'men', true);
	},
	
	getActivityLifeHtml : function(data){
		this.getActivityHtml(data, 'life', true);
	},
	
	getActivityOutdoorHtml : function(data){
		this.getActivityHtml(data, 'outdoor', true);
	},
	
	//获取时间戳，date格式为20140926
	getTimeStamp : function(date){
		return commonDate.getTime(date.substr(0,4)+"-"+date.substr(4,2)+"-"+date.substr(6,2)+" "+"10:00:00")/1000;
	},

	getActivityHtml : function(data, classId, isTimeOut){
		if(flashSale.isExecuteState[classId]){
			return true;
		}
		var html = '';
		var newData = new Array();
		for(var i=0;i<this.tjwData[classId].length;i++){
			if(isTimeOut){
				var cossData = data.data;
				for(var j=0;j<cossData.length;j++){
					var getActivityHtmlAreaId = (cossData[j].poolId == undefined)?cossData[j].areaId:cossData[j].poolId;
					if(this.tjwData[classId][i].actId == cossData[j].activeId && this.tjwData[classId][i].areaId == getActivityHtmlAreaId){
						var cossItem = cossData[j];
						this.tjwData[classId][i]['startTime'] = cossData[j]['beginTime'];
						this.tjwData[classId][i]['endTime'] = cossData[j]['endTime'];
						this.tjwData[classId][i]['sold'] = (cossData[j]['areasalesVolume']!=0) ? cossData[j]['areasalesVolume'] : cossItem.seedNum*5;
						this.tjwData[classId][i]['price'] = cossData[j]['arealowestPrice'];
						newData.push(this.tjwData[classId][i]);
						break;
					}
				}
			}else{
				this.tjwData[classId][i]['startTime'] = '';
				this.tjwData[classId][i]['endTime'] = '';
				this.tjwData[classId][i]['sold'] = shangouh5.getSeedNum(this.tjwData[classId][i].actId, this.tjwData[classId][i].areaId) * 10;
				this.tjwData[classId][i]['price'] = shangouh5.getSeedNum(this.tjwData[classId][i].actId, this.tjwData[classId][i].areaId) * 50;
				newData.push(this.tjwData[classId][i]);
			}
		}
		//只有分类页需要进行结束时间排序
		if(this.classId != 'todaySale'){
			newData = this.sortEndTime(newData);
		}
		for(var i=0;i<newData.length;i++){
			var basicData = newData[i];
			var price = sold = startTime = endTime = 0;
			if(basicData.sold != undefined){
				sold = basicData.sold;
			}
			if(basicData.price != 'undefined'){
				price = basicData.price;
				price = (price/100).toFixed(2);
			}
			if(basicData.startTime == '' || basicData.endTime == '' || basicData.startTime == '0' || basicData.endTime == '0'){
				startTime = this.getTimeStamp(basicData.remdRegName);
				endTime = startTime + 86400 * 5;
			}else{
				startTime = basicData.startTime;
				startTime = shangouh5.getStartTime(startTime);
				
				endTime = basicData.endTime;
				endTime = shangouh5.getEndTime(endTime);
			}
			if(this.currentTime>=startTime && this.currentTime<endTime){
				var timerFormatTitle = this.timerFormat(this.currentTime, startTime, endTime);
				var forHtml = this.getConfigHtml('activityHtml');
				forHtml = forHtml.replace(new RegExp("\{#sold#\}","g"), sold);
				forHtml = forHtml.replace(new RegExp("\{#price#\}","g"), price);
				forHtml = forHtml.replace(new RegExp("\{#startTime#\}","g"), startTime);
				forHtml = forHtml.replace(new RegExp("\{#endTime#\}","g"), endTime);
				forHtml = forHtml.replace(new RegExp("\{#url#\}","g"), basicData.url);
				forHtml = forHtml.replace(new RegExp("\{#subScriptIco#\}","g"), basicData.subScriptIco);
				forHtml = forHtml.replace(new RegExp("\{#image#\}","g"), basicData.zImage);
				forHtml = forHtml.replace(new RegExp("\{#shopName#\}","g"), basicData.shopName);
				forHtml = forHtml.replace(new RegExp("\{#recmdReason#\}","g"), basicData.recmdReason);
				forHtml = forHtml.replace(new RegExp("\{#timerFormatTitle#\}","g"), timerFormatTitle);
				html += forHtml;
			}	
		}
		$(".sg_loading").hide();
		$("#"+classId+"Html").html(html);
		flashSale.isExecuteState[classId] = true;
		var timer = setInterval('flashSale.timerCountDown()', 1000);
	},
	
	//配置HTML代码区域
	getConfigHtml : function(index){
		var html = {
			'activityHtml' : '<li>\
					<a class="sg_g" href="{#url#}">\
						<div class="sg_g_img">\
							{#subScriptIco#}\
							<img src="{#image#}" alt="{#shopName#}">\
							<p class="sg_g_desc">{#recmdReason#}</p>\
						</div>\
						<div class="sg_g_info">\
							<p class="sg_g_tit">{#shopName#}</p>\
							<p class="sg_g_row">\
								<span class="sg_g_price fl"><span>&yen;{#price#}</span>起</span>\
								<span class="sg_g_sale fr">已售<span>{#sold#}</span>件</span>\
							</p>\
							<p class="sg_g_time" startTime="{#startTime#}" endTime="{#endTime#}">\
								{#timerFormatTitle#}\
							</p>\
						</div>\
					</a>\
				</li>'
		}
		return html[index];
	}
}

//今日特卖
function tjw49877194708(data){
	//flashSale.getActivityTjwData(data, 'todaySale');
    setTimeout(function(){flashSale.getActivityHtml('', 'todaySale', false)}, 1000);
}
//女人
function tjw39804314700(data){
	flashSale.getActivityTjwData(data, 'women');
	setTimeout(function(){flashSale.getActivityHtml('', 'women', false)}, 1000);
}
//测试
function tjw59010184723(data){
	flashSale.getActivityTjwData(data, 'women');
	setTimeout(function(){flashSale.getActivityHtml('', 'women', false)}, 1000);
}
//儿童
function tjw49036654704(data){
	flashSale.getActivityTjwData(data, 'children');
	setTimeout(function(){flashSale.getActivityHtml('', 'children', false)}, 1000);
}
//男人
function tjw49405104706(data){
	flashSale.getActivityTjwData(data, 'men');
	setTimeout(function(){flashSale.getActivityHtml('', 'men', false)}, 1000);
}
//生活家居
function tjw49885884703(data){
	flashSale.getActivityTjwData(data, 'life');
	setTimeout(function(){flashSale.getActivityHtml('', 'life', false)}, 1000);
}
//运动户外
function tjw49404884705(data){
	flashSale.getActivityTjwData(data, 'outdoor');
	setTimeout(function(){flashSale.getActivityHtml('', 'outdoor', false)}, 1000);
}



var clickEvent = 'ontouchstart' in window ? 'touchstart' : 'click';	
$('.sg_nav_trigger').on(clickEvent,function(e){
	e.preventDefault();
	$('.sg_nav').toggleClass('sg_nav_on');
})