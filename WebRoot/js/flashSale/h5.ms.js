$(function($) {
	snipings.init();
});
function sniping(data){alert(1);
	snipings.getActivityHtml(data);alert(1);
}

var snipings = {
	config : {
		'html' : '<a class="sg_g2" href="{#proUrl#}">\
							<div class="sg_g2_info">\
								<p class="sg_g2_name">{#proName#}</p>\
								<p class="sg_g2_price"><i>&yen;</i><span>{#activityPrice#}</span></p>\
								<del class="sg_g2_mprice"><i>&yen;</i><span>{#newPrice#}</span></del>\
							</div>\
							<img src="{#imgUrl#}" alt="{#proName#}" class="sg_g2_img">\
						</a>',
		'ptag' : '20399.1.1'
	},
	init : function(){
		this.getActivityData();
	},
	getActivityData : function(index){
		var apiUrl = shangouh5.activityApi['sniping'];
		common.getApiData('h5', apiUrl, 'sniping');
	},
	getActivityHtml : function(data){
		var html = '';
		var newData = data.data;
		var ptag = this.config.ptag;
		for(var i=0;i<newData.length;i++){
			var forHtml = this.config.html;
			var item = newData[i];
			var activityPrice = (item["dwActivePrice"] == '') ? item["dwPrice"] : item["dwActivePrice"];
			activityPrice = (activityPrice/100).toFixed(2);
			var newPrice = item["dwMarketPrice"];
			newPrice = (newPrice/100).toFixed(2);
			var proUrl = '';
			var ptagTitle = (ptag != '') ? '&ptag='+ptag+'' : '';
			var appTitle = (common.getUrlParam('app') != null) ? '&app='+common.getUrlParam('app')+'' : '';
			proUrl = 'http://b.paipai.com/itemweb/item?scence=101&ic='+item["strCommodityId"] + appTitle + ptagTitle;
			if(item["vecExtData"] == null){
				var imgUrl = (item["mapCustomData"]["uploadPicUrl1"] == '') ? item["strPic"] : item["mapCustomData"]["uploadPicUrl1"];
			}else{
				var imgUrl = (item["vecExtData"][0]["strValue"] == '') ? (item["mapCustomData"]["uploadPicUrl1"] == '') ? item["strPic"] : item["mapCustomData"]["uploadPicUrl1"] : item["vecExtData"][0]["strValue"];
			}
			forHtml = forHtml.replace(new RegExp("\{#imgUrl#\}","g"), imgUrl);
			forHtml = forHtml.replace(new RegExp("\{#proName#\}","g"), item["strTitle"]);
			forHtml = forHtml.replace(new RegExp("\{#proUrl#\}","g"), proUrl);
			forHtml = forHtml.replace(new RegExp("\{#activityPrice#\}","g"), activityPrice);
			forHtml = forHtml.replace(new RegExp("\{#newPrice#\}","g"), newPrice);
			html += forHtml;
		}
		$('.sg_glist2').html(html);
	}
}