(function() {
      var gid="",uin="",state="",doc=document,nowtime="",isSuccess=false,xhr,dataBuyNum="";
      var curHref=window.location.href;
      var initHash=window.location.hash;
      var randomYaohe=['帮你团！你是请我吃饭还是以身相许？','参团不带我？辣怎么行！','矮油，这货不错，快来一起买！',
                       '帮你一把，怎么报答我？自觉联系我。','这么给力的，你还上哪儿找切。','我来审查你的口味！',
                       '啧啧，团长都剁手了，赶紧帮一把。','价钱合适量又足，谢谢团长！','搬板凳，排排坐，跟着团长拍便宜。',
                       '一直潜水，今日得以表白: 团长，么么哒！','这个也团啊？好吧，坚定跟着团长走！'];
      var vRandomYaohe =['别误会，我只是奔着价格来的。','难得看见超级团，当然不能错过。','没想到啊，您老竟然是超级团长。',
                       '我身边竟然藏着超级团长。','以后都靠你开团了，团长威武。','你是我的小呀小苹果，怎么跟你团都不嫌多。',
                       '我什么都愿意，什么都愿意为你！','说，给你捧场能有啥好处？','超级团就是牛，教教我怎么当大V吧。',
                       '说好一起剁手的呢，我也是醉了。','与其仰望，不如加入。','相信团长的品味，团购必须跟上！'];
      //获取uin
      function getQQ(){
          var uin=getCookie("wg_uin") || getCookie("p_uin") || getCookie("uin");
          return uin;
      }
      function urlAddUin(url,uin){
        if(!uin){
          return url;
        }else{
          return url.replace(/&suin=[^]+?\b/,"")+"&suin="+uin;
        }
      }
      function getCustomTuanInfo(){
        if(typeof threepapasConf == 'undefined'){
          return false;
        }else{
          threepapasConf.pop();
          if(threepapasConf.length>0){
            var curTuanId=getQuery("gid"),flag=false;
            eachProp(threepapasConf,function(ele){
              if(ele.tuanId==curTuanId){
                flag=ele;
                return true;//结束遍历
              }
            });
            return flag;
          }else{
            return false;
          }
        } 
      }
      //从url获取团ID
      var getGID=function(){
          var url=location.search;
          if(url.indexOf("?")!=-1)
          {
              url=url.substr(1);
              url = url.split("&");
              for(var i=0;i<url.length;i++)
              {
                  var urlVal=url[i].split("=");
                  if(urlVal[0]=="gid")
                  {
                      return gid=urlVal[1];
                  } 
              }
          }

      }();
      //s sku功能
      var ajax_getSkuInfo =function(commodityId,oData){
          xhr=$.ajax({
            type: 'GET',
            url: 'http://b.paipai.com/groupservice/createGroup?ic='+commodityId+'&querysku=1',
            dataType: 'jsonp',
            success: function(data){
              if(data.ret =="0"){
                if(data.StockList_StockNum==1){//进入有可能是multisku=2,即无法判断
                  setLocalItem(oData,0);
                  if(false){
                    if(oData.isVTuan){
                      window.location.href=localStorage.tuan_buyurl+"&speak="+encodeURI(vRandomYaohe[Math.floor(Math.random()*(12-0)+0)])+"&isdav=1";
                    }else{
                      window.location.href=localStorage.tuan_buyurl+"&speak="+encodeURI(randomYaohe[Math.floor(Math.random()*(11-0)+0)]);
                    }
                   }else{
                    window.location.href="tuan_speak.html?_wv=1"+(oData.isVTuan?"&isdav=1":"");
                   }
                }else{
                  initSku(data);
                  $('#sku_buyBtn').on('click',function(){
                    if(checkSku()){
                      setLocalItem(oData,1);
                      window.location.hash =initHash;
                      if(false){
                        if(oData.isVTuan){
                          window.location.href=localStorage.tuan_buyurl+"&speak="+encodeURI(vRandomYaohe[Math.floor(Math.random()*(12-0)+0)])+"&isdav=1";
                        }else{
                          window.location.href=localStorage.tuan_buyurl+"&speak="+encodeURI(randomYaohe[Math.floor(Math.random()*(11-0)+0)]);
                        }
                       }else{
                        window.location.href="tuan_speak.html?_wv=1"+(oData.isVTuan?"&isdav=1":"");
                       }
                    }
                  });
                  window.location.hash ="#sku"+Math.random();
                  window.onhashchange =function(){
                    if(window.location.hash !=initHash){
                      $('#sku_dom').show();
                      $('.fixopt,.support,.pp,.topStateWrap,.step,.mod_footer').hide();
                    }else{
                      $('#sku_dom').hide();
                      $('.fixopt,.support,.pp,.topStateWrap,.step,.mod_footer').show();
                    }
                  };
                }
              }else{ 
                alert("系统繁忙，去看看别的～");
                window.location.href="tuan_list.html?_wv=1";
              }
            },
            error: function(xhr, type){ 
              alert("系统繁忙，去看看别的～");
              window.location.href="tuan_list.html?_wv=1";
            }
          });
        };
        
        //初始化sku组件
        var initSku =function(__obj){
          window.cmdtySku =new propSelector({
            domId: 'propertyDiv',           //存放多个sku的容器div的id
            activeClass: 'sku_option_selected',   //选择相应sku属性时候添加的类class
            disableClass: 'sku_option_disabled',  //未选择相应sku属性时候添加的类class
            propGroupClass: 'sku_attr',     //存放单个sku中所有属性值的容器div的class
            stocklist : __obj.stocklist,    //存放sku信息的数组
            stockData : __obj.StockList_StockString,  //所有sku对应的字符串信息
            onSkuEmpty:function(){},
            onSelected:function(propData){
              window.skuChoose = propData;
            }
          });
          window.skuChoose = window.cmdtySku.getSelectedProp();
        };
        //检测sku是否选中
        var checkSku =function(){
          var propData = window.skuChoose, emptyProp = [];
          for (var key in propData.propMap) {
            if (!propData.propMap[key]) { //没有选中的
              emptyProp.push("“" + key + "”");
            }
          }
          if (emptyProp.length > 0) {
            $('.ftips_cnt').text('请选择:'+emptyProp.join('/'));
            $('.ftips').show();
            window.setTimeout(function(){
              $('.ftips').hide();
            },700);
            return false;
          }
          return true;
        };
        //e sku功能
        //本地存储
        function setLocalItem(data,multiP){
              var url,hrefVal="&tuan=1&itemid="+data["itemID"]+"&uin="+uin+"&groupnum="+data["ptotal"]+"&gid="+gid+(data.isVTuan?"&isdav=1":"");
              if(multiP){
                var propData = window.skuChoose, skuPro=encodeURI(propData.prop.join('|'));
                url = 'http://m.paipai.com/m2/p/cart/order/s_confirm.shtml?_wv=1'+hrefVal+'&commlist=' +[commodityId, skuPro, dataBuyNum, propData.skuid].join(',');
                localStorage.tuan_sku=propData.prop.length==1?propData.prop[0]:propData.prop.join('|') || "";
              }else{
                var sku=data["SKU"].trim().split("|"),skuID=sku.splice(0,1)||"",skuAttr=encodeURI(sku.join("|")||"");
                url="http://m.paipai.com/m2/p/cart/order/s_confirm.shtml?_wv=1"+hrefVal+"&commlist="+data["itemID"]+","+skuAttr+","+data["buy_num"]+","+skuID;
                localStorage.tuan_sku=sku.join("|")||"";
              }
              localStorage.tuan_buyurl=url;
              localStorage.tuan_role=data.isVTuan?3:1;
              localStorage.tuan_productInfo=data.img+"|"+data.title+"|"+data.ptotal+"|"+data.saleprice;
          }
        //跳转到吆喝页
        function gotoYaohe(data){
          if(data.multiSku!=0){
            ajax_getSkuInfo(data.itemID,data);
          }else{
            setLocalItem(data,0);
            if(false){//灰度条件：getQQ() && parseInt(getQQ().replace(/^o/,""),10)%2
                    if(data.isVTuan){
                      window.location.href=localStorage.tuan_buyurl+"&speak="+encodeURI(vRandomYaohe[Math.floor(Math.random()*(12-0)+0)])+"&isdav=1";
                    }else{
                      window.location.href=localStorage.tuan_buyurl+"&speak="+encodeURI(randomYaohe[Math.floor(Math.random()*(11-0)+0)]);
                    }
                   }else{
                    window.location.href="tuan_speak.html?_wv=1"+(data.isVTuan?"&isdav=1":"");
                   }
          }
        }
      //分享动作提示
      var setShareTipsForm=function(){
          $("#shareTipBox").show();
      };
      var hideShareTipsForm=function(event){
          event.preventDefault();
          $("#shareTipBox").hide();
      };
      //昵称decode
       var decodeNick=function(nick){
          try{
            return decodeURIComponent(nick).replace(/\+/g," ");
          }catch(e){
            return nick;
          }
        };
      function isWx(){
          var isWX = window.WeixinJSBridge;
          if(!isWX){
               var ua = navigator.userAgent.toLowerCase();
               isWX = ua.match(/micromessenger/)?true : false;
          }
          return isWX;
      }
      function isMQQUserAgent() {
          if (/qq\/([\d\.]+)*/i.test(navigator.userAgent)) {
              return true;
          }
          return false;
      }
      //头像统一：微信132尺寸，手Q100尺寸
      function unifiedImg(url){
        if(url && url.match(/^http:\/\/wx\.qlogo\.cn/)!=null){
          url=url.replace(/\/0$/,"/132");
        }
        if(url && url.match(/^http:\/\/q\d{1,3}\.qlogo\.cn/)!=null){
          url=url.replace(/&s=\d{1,4}\b/,"&s=100");
        }
        if(!url){
          url="http://static.paipaiimg.com/fd/qqpai/tuan/img/avatar_3_64.png";
        }
        return url;
      }
      //判断是否已经登录，
      var check=function(){
          plogin()?"":plogin(window.location.href);
          uin=getCookie("wg_uin")||getCookie("p_uin")||getCookie("uin");
          uin=uin?parseInt(uin.replace("o",""),10):"";
      }
      //补齐工具
      var padLeft=function(str) {
          if (str.toString().length >= 2)
              return str;
          else
              return ("0" + str);
      }
      //计时器
      var changeEndTime=function(){
                  var endtime=window.endtime;
                  var restime = endtime - nowtime;
                  var seconds = restime / 1000;
                  var minutes = Math.floor(seconds / 60);
                  var hours = Math.floor(minutes / 60);
                  var days = Math.floor(hours / 24);
                  days = days % 365;
                  hours = hours % 24;
                  minutes = minutes % 60;
                  seconds = Math.floor(seconds % 60); 
                  if (endtime >= nowtime) {
                      $("#ti_time_hour").html(padLeft(hours));
                      $("#ti_time_min").html(padLeft(minutes));
                      $("#ti_time_sec").html(padLeft(seconds));
                      //超过1天的团
                      if((endtime - starttime)>86400000){
                        if($("#ti_time_day").length>0){
                          $("#ti_time_day").html(days);
                        }else{
                          $("#ti_time_hour").before('<span id="ti_time_day">'+days+'</span>天');
                        }
                      }
                      nowtime += 1000;
                      setTimeout(changeEndTime, 1000);
                  }
      };

      var setCookie=function(name, value, expires, path, domain, secure) {
        //写入COOKIES
        var exp = new Date(), expires = arguments[2] || null, path = arguments[3] || "/", domain = arguments[4] || null, secure = arguments[5] || false;
        expires ? exp.setMinutes(exp.getMinutes() + parseInt(expires)) : "";
        document.cookie = name + '=' + escape(value) + ( expires ? ';expires=' + exp.toGMTString() : '') + ( path ? ';path=' + path : '') + ( domain ? ';domain=' + domain : '') + ( secure ? ';secure' : '');
      }

      var getCookie=function(name) {
          var reg = new RegExp("(^| )" + name + "(?:=([^;]*))?(;|$)"),
              val = doc.cookie.match(reg);
          return val ? (val[2] ? unescape(val[2]) : "") : null;
      }
      var getQuery=function(name, url) {
          var u = arguments[1] || window.location.search,
              reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"),
              r = u.substr(u.indexOf("\?") + 1).match(reg);
          return r != null ? r[2] : "";
      }

      var plogin=function (rurl){
        var ua = navigator.userAgent.toLowerCase();
        var isWX = window.WeixinJSBridge;
        var isMQQ = false;
        if(!isWX){isWX = ua.match(/micromessenger/)?true : false;}
        if(/qq\/([\d\.]+)*/.test(ua)){isMQQ = true;}
        if(!rurl){
          if(isMQQ){
            return (getCookie("skey") && getCookie("uin"))?true:false;
          }else{
            return (getCookie("wg_skey") && getCookie("wg_uin") || getCookie("p_skey") && getCookie("p_uin") || getCookie("skey") && getCookie("uin"))?true:false;
          }
        }
        if(isWX){
          window.location.href = 'http://b.paipai.com/mlogin/tws64/m/wxv2/Login?appid=1&rurl=' + encodeURIComponent(rurl);
        }else if(isMQQ){
          if(mqq && mqq.data){
            mqq.data.getUserInfo(function(o){
              window.location.href = rurl;
            });
          }
        }else{
          window.location.href = 'http://b.paipai.com/mlogin/tws64/m/h5v1/cpLogin?rurl=' + encodeURIComponent(rurl) + '&sid=' + getCookie('sid') + '&uk=' + getCookie('uk');
        }
      }
      var $loadUrl=function(o) {
          o.element = o.element || 'script';
        var el = document.createElement(o.element);
        el.charset = o.charset || 'utf-8';
        o.onBeforeSend&&o.onBeforeSend(el);//发请求之前调用的函数
        el.onload = el.onreadystatechange = function() {
          if(/loaded|complete/i.test(this.readyState) || navigator.userAgent.toLowerCase().indexOf("msie") == -1) {
                    o.onLoad&&o.onLoad();//
                          clear();
          }
        };
        el.onerror = function(){
          clear();
        };
        el.src = o.url;
        document.getElementsByTagName('head')[0].appendChild(el);
        
        var clear=function(){
          if(!el){return ;}
          el.onload = el.onreadystatechange = el.onerror = null;
          el.parentNode&&(el.parentNode.removeChild(el));
          el = null;
        }
      };
      var $makeRd=function(rd,url){
        var url=url||'http://www.paipai.com/rd.html',
          arrRd=rd.split(".");
        return "http://service.paipai.com/cgi-bin/go?pageId="+arrRd[0]+"&domainId="+arrRd[1]+"&linkId="+arrRd[2]+ "&url=" + escape(url);
      };
      var $report=function(url){
        $loadUrl({
          'url':url+((url.indexOf('?')==-1)?'?':'&')+Math.random(),'element':'img'
        });
      }
      //E 主动上报
      //主动终止ajax请求
      var setTimeoutInfo=function(a,b,c)
      {
             if(xhr){
                xhr.abort();
                xhr=null;
             }
             $(".H5_con").hide();
             $("#sorryBox").show();
             footFoucsPPY(3);
      };
      //遍历工具--适用于数组和对象
      function eachProp(obj, func) {
          var prop;
          for (prop in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                  if (func(obj[prop], prop)) {
                      break;
                  }
              }
          }
      }
        /**数组专用遍历工具--反向遍历---requirejs
       * Helper function for iterating over an array backwards. If the func
       * returns a true value, it will break out of the loop.
       */
      function eachReverse(ary, func) {
          if (ary) {
              var i;
              for (i = ary.length - 1; i > -1; i -= 1) {
                  if (ary[i] && func(ary[i], i, ary)) {
                      break;
                  }
              }
          }
      }
      function initShareContentPPMS(data){
        var shareContentConf;
        if(data.ustate=="noin"){//路人
          shareContentConf = typeof shareContentOuterConf !=="undefined" && shareContentOuterConf;
        }else if(data.ubstate){//挤爆者
          shareContentConf = typeof shareContentJibaoConf !=="undefined" && shareContentJibaoConf;
        }else if(data.isVTuan){
          if(data.ismaster){//超级团长
            shareContentConf = typeof shareContentVMasterConf !=="undefined" && shareContentVMasterConf;
          }else{//超级团员
            shareContentConf = typeof shareContentVYuanConf !=="undefined" && shareContentVYuanConf;
          }
        }else{
          if(data.ismaster){//普通团长
            shareContentConf = typeof shareContentTuanMasterConf !=="undefined" && shareContentTuanMasterConf;
          }else{//普通团员
            shareContentConf = typeof shareContentTuanYuanConf !=="undefined" && shareContentTuanYuanConf;
          }
        }
        return shareContentConf;
        /*
        var shareContentConf=initShareContent(data);
        var contentPool= shareContentConf != false && shareContentConf.slice(0,shareContentConf.length-1);
        contentPool=(contentPool && contentPool.length>0)?contentPool:["大神开场了，快快跟随冒泡踩场吧","达人难得有空吆喝聚友，一起来聚聚","开嗓吆喝，喊你凑桌一起玩一玩","开摊吆喝了，速来围观Ta的品位吧"];
        var ranContent=contentPool[Math.floor(Math.random()*(4-0)+0)];
        var shareContent=data.mname.replace(/\s/g,"")+" "+ranContent+"！"+data.title+"仅售"+data.saleprice+"元";
        */
      }
      function initShareContent(data){
        var contentPool,masterName=(data.isVTuan?"超级团长":"")+data.mname.replace(/\s/g,"")+" ",
            price=data.saleprice,
            shortTitle=(data["title"].length>16?(data["title"].substring(0,16)+"..."):data["title"]);
        if(data.tstate=="sales"){
          if(data.upstate){//优惠价
            contentPool=["最低价被我抢到！只剩超级团了，不抢就贵了哦~","参加超级团中奖了，羡慕嫉妒恨吧~","价格惊爆天，大家都快来抢啊！"];
          }else if(data.isVTuan){//超级团
            if(data.ismaster){
              contentPool=[masterName+"求粉给好处，"+shortTitle+",竟然"+price+"元！",masterName+"就是任性！"+shortTitle+",竟然"+price+"元！",masterName+"发年终奖啦，"+shortTitle+",竟然"+price+"元！"];
            }else if(data.ustate=="succ"){
              contentPool=["百年一遇超级团，必然得加入啊~","竟然撞上了超级团，团完这个就去买彩票！","超级团长威武，我进来沾个光。"];
            }else{
              contentPool=["百年一遇的超级团，第一时间转给大家~","据说看到超级团的，都超级幸运哦~","人品好如我，看到超级团自然要分享。"];
            }
          }else{//普通团
            if(data.ismaster){
              contentPool=[masterName+"是会省钱还是真败家？"+shortTitle+",仅售"+price+"元！",masterName+"邀你一起任性！"+shortTitle+",仅售"+price+"元！",masterName+"团长发福利，"+shortTitle+",仅售"+price+"元！"];
            }else if(data.ustate=="succ"){
              contentPool=["今朝一起抢货，明夕相约剁手!","赚便宜我最在行了，哈哈~","有缘千里来成团，感谢有缘团长!"];
            }else{
              contentPool=["传说，帮人转发此团，胜造七级浮屠。","不帮你转发，愧对多年情分啊~","这次纯转发，下回帮你团~"];
            }
          }
        }else if(data.tstate=="succ"){
          if(data.upstate){//优惠价
            contentPool=["抽到优惠价，真呀么真高兴~","从来没中过奖的我，今天真是太神奇了~","肯定是我人品太好，才能抽到优惠价！"];
          }else if(data.isVTuan){//超级团
            if(data.ustate=="noin"){
              contentPool=["世上最遥远的距离，就是你团上，我迟到~","竟然错过，难道我就是普通团的命么~","发愤图强，从此立志当超级团长~"];
            }else{
              contentPool=["超级团就是牛气，便宜太多啦！","团结就是力量，团结就是便宜！","只要功夫深，超级团成真！"];
            }
          }else{//普通团
            if(data.ustate=="noin"){
              contentPool=["低估团长的实力了，竟然连我都没团上~","紧赶慢赶还是来晚了，我还是自己开团去吧~","一张自拍的时间就错过了，下回一定记住教训~"];
            }else{
              contentPool=["嗓子没白嚷破，成功拍到便宜啦！","团购成功，喜大普奔！","是我魅力太大么，这么快就团上了~"];
            }
          }
        }else{
          if(data.ustate=="noin"){
            contentPool=["哈哈，幸好我没进这个团~","让你们不叫上我，看，失败了吧~","这个团太懒啦，我若开团，绝对爆满！"];
          }else if(data.isVTuan){
            contentPool=["心塞，百年一遇的超级团就这样错过了~","屡败屡团！下次再有超级团记得叫上我~","一定是我品味太高，曲高和寡，才没人一起团~"];
          }else{
            contentPool=["竟然没团成功，大家都不爱我了么？","万万没想到，我也团购失败的一天~","没人能阻止我败家！下次一定团成功！"];
          }
        }
        if(!data.ismaster || data.tstate!="sales"){
          for(var i=0,poolLen=contentPool.length;i<poolLen;i++){
            contentPool[i]+=shortTitle+",仅售"+price+"元！";
          }
        }
        return contentPool[Math.floor(Math.random()*(3-0)+0)];
      }
      //浏览器主动分享工具
      function shareContent(data){
        var shareContent=initShareContent(data);
        var sangebabaShareTitle=["首发直降1000块！","凑够2人省2000！","大家省才是真的省！"],
            sangebabaShareDesc=["快来跟我一起抢儿童专用空气净化器！","快来一起买儿童专用净化器！怒省1000元！从今天起，做一个不再败家的剁手党！","凑够2人省2000！约吗！"],
            sangebabaShareTtitle=["首发直降1000块，快来跟我一起抢儿童专用空气净化器！","凑够2人省2000，快来一起买儿童专用净化器！怒省1000元！从今天起，做一个不再败家的剁手党！","大家省才是真的省，凑够2人省2000！约吗！"];
        var randomNum=Math.floor(Math.random()*(3-0)+0);
        if(data["itemID"]=="39E9C197000000000401000045AA3913"){
            paipai_h5_share.init({
              hideQQ: false,
              hideQzone: false,
              hideWX: false,
              hideWXFriend: false,
              hideTqq: false,
              qq: {
                  title: sangebabaShareTitle[randomNum],
                  desc: sangebabaShareDesc[randomNum],
                  share_url: urlAddUin(curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),getQQ()),
                  ptag: "12475.18.1",
                  image_url: data["img"]
              },
              qzone: {
                  title: sangebabaShareTitle[randomNum],
                  desc: sangebabaShareDesc[randomNum],
                  share_url: urlAddUin(curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),getQQ()),
                  ptag: "12475.18.2",
                  image_url: data["img"]
              },
              weixin: {
                  title: sangebabaShareTitle[randomNum],
                  desc: sangebabaShareTtitle[randomNum],
                  share_url: urlAddUin(curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),getQQ()),
                  ptag: "12475.18.3",
                  image_url: data["img"]
              },
              weixinFriend: {
                  title: sangebabaShareTitle[randomNum],
                  share_url: urlAddUin(curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),getQQ()),
                  desc: "",
                  ptag: "12475.18.4",
                  image_url: data["img"]
              },
              tqq: {
                  title: sangebabaShareTitle[randomNum],
                  desc: sangebabaShareTtitle[randomNum],
                  share_url: urlAddUin(curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),getQQ()),
                  ptag: "12475.18.5",
                  image_url: data["img"]
              }
          });
        }else{
          paipai_h5_share.init({
              hideQQ: false,
              hideQzone: false,
              hideWX: false,
              hideWXFriend: false,
              hideTqq: false,
              qq: {
                  title: (data.isVTuan?"超级团长":"")+data["mname"]+"发起了团购：",
                  desc: "【"+data["ptotal"]+"人成团"+data["saleprice"]+"元】"+(data["title"].length>16?(data["title"].substring(0,16)+"..."):data["title"])+"，跟我组团抢，超划算，信我没错！",
                  share_url: urlAddUin(curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),getQQ()),
                  ptag: "12475.18.1",
                  image_url: data["img"]
              },
              qzone: {
                  title: (data.isVTuan?"超级团长":"")+data["mname"]+"发起了团购：",
                  desc: "【"+data["ptotal"]+"人成团"+data["saleprice"]+"元】"+(data["title"].length>16?(data["title"].substring(0,16)+"..."):data["title"])+"，跟我组团抢，超划算，信我没错！",
                  share_url: urlAddUin(curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),getQQ()),
                  ptag: "12475.18.2",
                  image_url: data["img"]
              },
              weixin: {
                  title: (data.isVTuan?"超级团长":"")+data["mname"]+"发起了团购：",
                  desc: shareContent,
                  share_url: urlAddUin(curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),getQQ()),
                  ptag: "12475.18.3",
                  image_url: data["img"]
              },
              weixinFriend: {
                  title: (data.isVTuan?"超级团长":"")+data["mname"]+"发起了团购：",
                  share_url: urlAddUin(curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),getQQ()),
                  desc: "",
                  ptag: "12475.18.4",
                  image_url: data["img"]
              },
              tqq: {
                  title: (data.isVTuan?"超级团长":"")+data["mname"]+"发起了团购：",
                  desc: shareContent,
                  share_url: urlAddUin(curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),getQQ()),
                  ptag: "12475.18.5",
                  image_url: data["img"]
              }
          });
        }
    }
    //网易分享组件
    function wyShare(options){
      if(navigator.userAgent.indexOf("NewsApp")<0){
        return;
      }
      var my = arguments.callee,
      $section = $("#__newsapp_container"),
      wyScriptUrl = "http://img3.cache.netease.com/utf8/3g/libs/NewsAppClient.js?"+(new Date().getTime()),
      tpl = [
          '<div id="__newsapp_sharetext">{text}{linkUrl}</div>',
          '<div id="__newsapp_sharephotourl">{photoUrl}</div>',
          '<div id="__newsapp_sharewxtitle">{title}</div>',     
          '<div id="__newsapp_sharewxurl">{linkUrl}</div>',
          '<div id="__newsapp_sharewxtext">{text}</div>',
          '<div id="__newsapp_sharewxthumburl">{photoUrl}</div>'
        ].join(""),
      defaultOptions = {
        title : "标题",
        linkUrl   : "",
        photoUrl : "",
        text     : "",
        success : function(){},
        failed  : function(){} 
      };
      $.extend(defaultOptions,options);
      var newTpl = tpl.replace(/\{([^\}]+)\}/g,function(all,$1){
          return defaultOptions[$1];
        }); 
      $section.html(newTpl);
      if(my.isCreate){
        options.isFire
        &&NewsAppClient.share({
          success : defaultOptions.success,
          failed  : defaultOptions.failed
        });

      }else{
        $('<script>')
          .appendTo(document.body)
          .bind("load",function(){
            if(!window["NewsAppClient"]){
              return;
            }
            my.isCreate = true;
            
            options.isFire
            &&NewsAppClient.share({
              success : defaultOptions.success,
              failed  : defaultOptions.failed
            });
          })
          .bind("error",function(){
          })
          .attr("src",wyScriptUrl);
      }
    }
      //事件捆绑
      function bottomBtnEvent(data){
        $("#bottomBtn1,#bottomBtn6").click(function(){
          if(isMQQUserAgent() || isWx()){
            setShareTipsForm();
            $("#shareTipBox").click(function(e){
              hideShareTipsForm(e);
            });
          }else if(navigator.userAgent.indexOf("NewsApp")>=0){
            wyShare({
                title    : "【拍拍-拍便宜】",
                linkUrl  : urlAddUin(curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),getQQ()),
                photoUrl : data.img,
                text     : data.mname+"开摊吆喝了，速来围观Ta的品位吧！"+data.title,
                isFire   : true
              });
          }else{
            //shareContent(data);
              var ua = navigator.userAgent.toLowerCase();
              if(ua.indexOf("paipaiapp")>=0){
                  var shareinfo = {
                      title: (data.isVTuan?"超级团长":"")+data["mname"]+"发起了团购：",
                      desc: "【"+data["ptotal"]+"人成团"+data["saleprice"]+"元】"+(data["title"].length>16?(data["title"].substring(0,16)+"..."):data["title"])+"，跟我组团抢，超划算，信我没错！",
                      share_url: curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),
                      image_url: data["img"]
                  }
                  location.href="paipai://sharepaicheap?1=1&title="+encodeURI(shareinfo.title)+"&desc="+encodeURI(shareinfo.desc)+"&share_url="+shareinfo.share_url+"&image_url="+shareinfo.image_url;
              }else{
                  shareContent(data);
              }
          }
          try{$countRDV2("20400.3.1");}catch(e){};
          return false;
        });
        $("#bottomBtn2,#bottomBtn7").click(function(event){
          event.preventDefault();
          try{$countRDV2("20400.3.2");}catch(e){};
          gotoYaohe(data);
          return false;
        });

      }
      //S observer工具
      //observer
      function ObserverList(){
        this.observerList = [];
      }
       
      ObserverList.prototype.add = function( obj ){
        return this.observerList.push( obj );
      };
       
      ObserverList.prototype.count = function(){
        return this.observerList.length;
      };
       
      ObserverList.prototype.get = function( index ){
        if( index > -1 && index < this.observerList.length ){
          return this.observerList[ index ];
        }
      };
      function Observer(){
        this.update = function(){
          // ...
        };
      }
      //subject
      var domObserverList=new ObserverList();
      domObserverList.notify=function(data){
        var observerLen=domObserverList.count();
        for(var i=0; i < observerLen; i++){
          domObserverList.get(i).update(data);
        }
      }
      //制造observer
      var observerObj={
        "topState":null,//顶部状态
        "tuanDetail":null,//团信息模块
        "headerImg":null,//头像模块
        "tuanState1":null,//团状态描述1：还有多少人
        "tuanState2":null,//团状态描述2：倒计时或发货提示语
        "yaoheList":null,//吆喝列表
        "seller":null,//卖家信息
        "hotTuan":null,//热门团
        "bottom":null,//公共底部
        "button":null,//按钮
        "hongbao":null,//红包
        "cps":null//cps返佣
      };
      for(var i in observerObj){
        observerObj[i]=new Observer();
        domObserverList.add(observerObj[i]);
      }
      //注意：页面的每一个模块改变，可以通过在主回调domObserverList.notify之前，更改observer集来实现。
      observerObj.topState.update=function(data){
        //ubstate:1当前用户是挤爆团成员 0当前用户非挤爆团成员
        if((data.tstate=="sales" && data.ustate=="succ") || (data.tstate=="succ" && data.ubstate==1)){
          $(".topStateWrap").addClass("tips_succ");
          $(".tips_tit").html(data["ismaster"]?"开团成功":data.ubstate==1?"成功挤爆该团":"参团成功");
          if(!data.ismaster){$(".tips_kc").html("参团");};
          $(".tips_extra b").html(data["shopName"]?data["shopName"]:"");
          if(data.isVTuan && data["ismaster"]){$(".tips_extra").html('<a href="#" class="tips_btn2" id="goShareBtn">赶快带领你的粉丝发现更多惊喜吧！</a>')};
          if(data.upstate){$(".tips_extra").html('<a href="#" class="tips_btn2" id="goShareBtn">超级优惠价都被你抢到了，快去分享吧！</a>')}
        }else if(data.tstate=="sales" && data.ustate=="noin"){
          $(".topStateWrap").hide();
          $("#tMasterImg").attr("src",data.mimg?data.mimg:"http://static.paipaiimg.com/fd/qqpai/tuan/img/avatar_4_64.png");
          $(".bi_name").html(data.mname.replace(/\s/g,"")+" 发起的团购");
          $(".bi").show();
        }else if(data.tstate=="succ"){
          if(data.ustate=="succ"){
            $(".topStateWrap").addClass("tips_succ tips_succ2");
            $(".tips_tit").html("团购成功");
            $(".tips_txt").html((data.isFXTuan && data.ismaster)?"":"我们会尽快为您发货，请耐心等待");
          }else if(data.tbstate!="true" && data.ustate=="noin"){//团挤爆满额
            $(".topStateWrap").addClass("tips_notice tips_notice2");
            $(".tips_tit").html(data.isVTuan?"此超级团长魅力太大，挤不进团":(data.mname+"开团如此火爆，换火箭都没挤上团"));
            $(".tips_txt").attr("class","tips_opt").html('<a href="tuan_detail.html?_wv=1&comodityId='+data.itemID+'" class="tips_btn2">向大神学习，'+(data.isVTuan?"开团升级":"去开团页超越Ta")+'</a>');
          }else{
            $(".topStateWrap").addClass("tips_notice tips_notice2");
            $(".tips_tit").html(data.mname+(data.isVTuan?((1*data.ptotal)<data.userlist.length?"的超级团也太抢手了，凑热闹来晚了":"的超级团必须火爆，做飞机都没有追上"):"开团如此火爆，坐飞机都没有追上"));
            $(".tips_txt").attr("class","tips_opt").html("<a class='tips_btn2' id='goJibao' href='tuan_jibaoresult.html?_wv=1&ptag=20400.9.1&isvtuan="+data.isVTuan+"&ptotal="+data.ptotal+"&dataBuyNum="+dataBuyNum+"&uin="+uin+"&comodityId="+commodityId+"&gid="+gid+"'>"+(data.isVTuan?"求让我进次超级团":"求再给我一次机会")+"，挤爆TA</a>");
            $("#goJibao").on("click",function(){
              setLocalItem(data,0);
            });
          }
        }else{
          if(data.ustate=="succ"){
            $(".topStateWrap").addClass("tips_err");
            if(data.iscod || (data.isFXTuan && data.ismaster)){
              $(".tips_tit").html("组团失败");
              $(".tips_txt").html("<p>时间到了，小伙伴不给力，组团失败了。</p><p>您的订单已关闭</p>");
            }else{
              $(".tips_tit").html("组团失败，退款中");
              $(".tips_txt").html("<p>时间到了，小伙伴不给力，组团失败了。</p><p>您的已付款项将在3-5天内退至您的支付账户，请留意相关信息</p>");
            }
          }else{
            $(".topStateWrap").addClass("tips_notice");
            $(".tips_tit").html("您还没有参加这个团");
            $(".tips_txt").html("");
          }
        }
        //register in the scope of requesting data
        $("body").on("click","#goShareBtn",function(){
            if(isMQQUserAgent() || isWx()){
              setShareTipsForm();
              $("#shareTipBox").click(function(e){
                hideShareTipsForm(e);
              });
            }else if(navigator.userAgent.indexOf("NewsApp")>=0){
              wyShare({
                  title    : "【拍拍-拍便宜】",
                  linkUrl  : urlAddUin(curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),getQQ()),
                  photoUrl : data.img,
                  text     : data.mname+"开摊吆喝了，速来围观Ta的品位吧！"+data.title,
                  isFire   : true
                });
            }else{
                //shareContent(data);
                var ua = navigator.userAgent.toLowerCase();
                if(ua.indexOf("paipaiapp")>=0){
                    var shareinfo = {
                        title: (data.isVTuan?"超级团长":"")+data["mname"]+"发起了团购：",
                        desc: "【"+data["ptotal"]+"人成团"+data["saleprice"]+"元】"+(data["title"].length>16?(data["title"].substring(0,16)+"..."):data["title"])+"，跟我组团抢，超划算，信我没错！",
                        share_url: curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),
                        image_url: data["img"]
                    }
                    location.href="paipai://sharepaicheap?1=1&title="+encodeURI(shareinfo.title)+"&desc="+encodeURI(shareinfo.desc)+"&share_url="+shareinfo.share_url+"&image_url="+shareinfo.image_url;
                }else{
                    shareContent(data);
                }
            }
            try{$countRDV2("20400.3.1");}catch(e){};
            return false;
          });
      };
      observerObj.tuanDetail.update=function(data){
        var hbStr="",packetIdStr="";
        if(data.tstate=="succ"){
          $(".tm").addClass("tm_succ");
        }else if(data.tstate=="failed"){
          $(".tm").addClass("tm_err");
        }
        $(".goItemPage").attr("href","http://b.paipai.com/itemweb/item?scence=101&ic="+data.itemID+"&tuanflag=1&_wv=1");
        $(".tuanDetailWrap img").attr("src",data.img?data.img:"");
        $(".tuanDetailWrap .td_name").html(data.title?data.title:"拍拍商品");
        $(".tuanTotal").html(data.ptotal);
        $(".td_mprice b").html(data.saleprice);
        if(data.tstate=="succ"){
          if(data.hongbao!=""){
            packetIdStr=data.hongbao.replace(/:[^]*/,"");
            $(".hbWrapper").show().attr("href","http://m.paipai.com/promote/card_packet.html?type=3&packetId="+packetIdStr).html('<div class="hb"><span class="hb_ico"><i></i></span><span class="hb_txt">已成团，快抢拍拍红包，全商品抵用，手慢无！</span></div>');
          }else{
            $(".hb_txt").html("红包已被抢光啦！下次要再快点哦~");
            $(".hb").show();
          }
        }else{
          $(".hb_txt").html("团购成功后，团成员可获得拍拍红包，手慢无！");
          $(".hb").show();
        }
        if(data.tstate=="sales"){
          xhr=$.get('http://b.paipai.com/groupservice/groupplayers?productId='+data.itemID,function(json){
            if(json && json.ret==0){
              $(".td_num").html(data.tstate=="sales"?("累计参团："+json.data.groupplayers+"人"):"");
              localStorage.tuan_num=json.data.groupplayers;
            }
          },'jsonp');
        }
        /*var sku=data["SKU"].trim().split("|"),skuID=sku.splice(0,1)||"",skuAttr=sku.join("|")||"";
        if(skuAttr){
          $(".td_attr span").html(skuAttr);
        }else{
          $(".td_attr").hide();
        }*/
        $(".td_attr").hide();
      };
      observerObj.headerImg.update=function(data){
        if(data.userlist==null){return false;}
        var userlistArr=data.userlist.slice(1),ulLen=userlistArr.length,memberData='[';
        var withDotted=false,promoteUserId,promoteUserType;
        var customTuan=getCustomTuanInfo();
        if(data.promoteprice != 0){
                eachReverse(userlistArr,function(ele){
                  if(ele["pprcieuser"]){//反向遍历，获取到最新优惠用户
                    promoteUserId=ele["userid"];
                    promoteUserType=ele["usertype"];
                    return true;
                  }
                });
              };
        var newHeaderImg=function(){};
        //decorator pattern
        newHeaderImg.decorators={
          bigVTuan:{
            begin:function(){
              $("body").addClass("vip");
              if(data.ptotal>6 || ulLen>5){
                $(".pp_users").addClass("pp_users2");
              }
              if(data.ptotal>9 || ulLen>8){withDotted=true;}//大于8人团或挤爆后超过8人
              $(".pp_users").addClass(".pp_users2").before('<div class="pp_vipuser"><img src="'+(customTuan?customTuan.tuanMasterImg:unifiedImg(data.mimg))+'" alt=""><span class="pp_vipuser_tit">团长</span></div>');
              if(data.promoteprice == 0 && data.tstate=="sales"){
                  $(".pp_vtips").html("欢迎来到超级团，无限惊喜等你参团！").show();
                }
              this.prepareImgNum();
            },
            prepareImgNum:function(){
              if(ulLen==0){
                this.callbackFun({list:[]});
                return false;
              }
              if(ulLen>8){
                this.prepareData(6);
              }else{
                if(data.ptotal>9){
                  this.prepareData(ulLen<=6?ulLen:6);
                }else{
                  this.prepareData(ulLen);
                }
              }
            },
            prepareData:function(n){
              eachProp(userlistArr,function(ele,index){
                  memberData+='{"userid":"'+ele["userid"]+'",'+'"usertype":'+ele["usertype"]+'},';
                  if(index>=(n-1)){return true;}
                });
                if(ulLen>8 && ulLen>=data.ptotal-1){
                  memberData+='{"userid":"'+userlistArr[userlistArr.length-1]["userid"]+'",'+'"usertype":'+userlistArr[userlistArr.length-1]["usertype"]+'},';
                }
                memberData=memberData.substring(0,memberData.length-1);
                memberData+=']';
              this.requestImg();
            },
            callbackFun:function(json){
              var item=json.list,itemLen=item.length,promoteTagged=false;
              for(var i=0;i<(withDotted?7:(data.ptotal<(ulLen+1)?ulLen:data.ptotal-1));i++){
                if(withDotted && i==6){
                  $(".pp_users").append('<span class="pp_users_item pp_users_dot"><i></i>');
                }
                if(item[i]){
                    if(promoteUserId != undefined && item[i]["userid"]==promoteUserId && !promoteTagged){
                      promoteTagged=true;
                      var proName=item[i]["name"]?decodeNick(item[i]["name"]):"拍拍用户";
                      proName=proName.replace(/\s/g,"");
                      if(proName.length>4){
                        proName=proName.substring(0,3)+"...";
                      }
                      $(".pp_vtips").html(proName+"喜得"+(parseFloat(data["promoteprice"])/100).toFixed(2)+"元幸运价购买，快抢吧！").show();
                      $(".pp_users").append('<a href="#" class="pp_users_item pp_users_normal pp_users_sale"><img src="'+unifiedImg(item[i]["imgurl"])+'" alt=""><span>优惠</span></a>');
                    }else{
                      $(".pp_users").append('<a href="#" class="pp_users_item pp_users_normal"><img src="'+unifiedImg(item[i]["imgurl"])+'" alt=""></a>');
                    }
                  }else{
                    $(".pp_users").append('<span class="pp_users_item pp_users_blank"><img src="http://static.paipaiimg.com/fd/qqpai/tuan/img/avatar_4_64.png" alt=""></span>');
                  };
              };
              if(!promoteTagged && promoteUserId){
                var extraData='[{"userid":"'+promoteUserId+'",'+'"usertype":'+promoteUserType+'}]';
                xhr=$.get("http://b.paipai.com/userinfo/getusernickandhead?userlist="+extraData,function(json){
                  if(json.retCode==13){
                    plogin(window.location.href);
                  }else{
                    var proName=json.list[0]["name"]?decodeNick(json.list[0]["name"]):"拍拍用户";
                    proName=proName.replace(/\s/g,"");
                      if(proName.length>4){
                        proName=proName.substring(0,3)+"...";
                      }
                    $(".pp_vtips").html(proName+"喜得"+(parseFloat(data["promoteprice"])/100).toFixed(2)+"元幸运价购买，快抢吧！").show();
                  }
                },'jsonp');
              }
            }
          },
          wanRenTuan:{}//此装饰见下
        };
        //临时项目：三个爸爸继承万人团-扩展装饰模式start
        //代码容错忽略点：项目结束时，参团人数（除团长）大于7个。
        newHeaderImg.decorators.wanRenTuan=Object.create(newHeaderImg.decorators.bigVTuan);
        if(data.tstate=="succ" && data.userlist.length<data.ptotal){
          newHeaderImg.decorators.wanRenTuan.prepareData=function(n){
            eachProp(userlistArr,function(ele,index){
                  memberData+='{"userid":"'+ele["userid"]+'",'+'"usertype":'+ele["usertype"]+'},';
                  if(index>=(n-1)){return true;}
                });
            memberData+='{"userid":"'+userlistArr[userlistArr.length-1]["userid"]+'",'+'"usertype":'+userlistArr[userlistArr.length-1]["usertype"]+'},';
            memberData=memberData.substring(0,memberData.length-1);
            memberData+=']';
            this.requestImg();
          };
        };
        //临时项目：三个爸爸继承万人团-扩展装饰模式end
        newHeaderImg.prototype.begin=function(){
            if(data.ptotal>5 || ulLen>4){
              $(".pp_users").addClass("pp_users2");
            }
            if(data.ptotal>8 || ulLen>7){withDotted=true;}//大于8人团或挤爆后超过8人
            $(".pp_users").append('<a href="#" class="pp_users_item pp_users_normal"><img src="'+unifiedImg(data.mimg)+'" alt=""></a>');
            this.prepareImgNum();
          };
        newHeaderImg.prototype.prepareImgNum=function(){
            if(ulLen==0){
              this.callbackFun({list:[]});
              return false;
            }
            if(ulLen>7){
              this.prepareData(5);
            }else{
              if(data.ptotal>8){
                this.prepareData(ulLen<=5?ulLen:5);
              }else{
                this.prepareData(ulLen);
              }
            }
          };
        newHeaderImg.prototype.prepareData=function(n){
          eachProp(userlistArr,function(ele,index){
              memberData+='{"userid":"'+ele["userid"]+'",'+'"usertype":'+ele["usertype"]+'},';
              if(index>=(n-1)){return true;}
            });
            if(ulLen>7 && ulLen>=data.ptotal-1){
              memberData+='{"userid":"'+userlistArr[userlistArr.length-1]["userid"]+'",'+'"usertype":'+userlistArr[userlistArr.length-1]["usertype"]+'},';
            }
            memberData=memberData.substring(0,memberData.length-1);
            memberData+=']';
          this.requestImg();
        };
        newHeaderImg.prototype.requestImg=function(){
            var that=this;
            xhr=$.ajax({
                          url: 'http://b.paipai.com/userinfo/getusernickandhead?userlist='+memberData,
                          type: 'GET',
                          dataType: 'jsonp',
                          jsonpCallback:'getuserinfo'+new Date().valueOf(),
                          error:function(a,b,c){
                          },
                          success: function(json){
                             if(json.retCode==13){
                               plogin(window.location.href);
                             }else{
                                that.callbackFun(json);
                             }
                          }
                        });
          };
        newHeaderImg.prototype.callbackFun=function(json){
              var item=json.list,itemLen=item.length;
              for(var i=0;i<(withDotted?6:(data.ptotal<(ulLen+1)?ulLen:data.ptotal-1));i++){
                if(withDotted && i==5){
                  $(".pp_users").append('<span class="pp_users_item pp_users_dot"><i></i>');
                }
                if(item[i]){
                  $(".pp_users").append('<a href="#" class="pp_users_item pp_users_normal"><img src="'+unifiedImg(item[i]["imgurl"])+'" alt=""></a>');
                }else{
                  $(".pp_users").append('<span class="pp_users_item pp_users_blank"><img src="http://static.paipaiimg.com/fd/qqpai/tuan/img/avatar_4_64.png" alt=""></span>');
                }
              }
          };
        newHeaderImg.prototype.decorate=function(str){
            var decorator=this.constructor.decorators[str];//get decorators and ship
            for(var i in decorator){
              this[i]=decorator[i];
            };
          };
        var headerImgObj=new newHeaderImg();
        if(customTuan || data.isWanRenTuan){
          headerImgObj.decorate("wanRenTuan");
        }else if(data.isVTuan){
          headerImgObj.decorate("bigVTuan");
        };
        headerImgObj.begin();
      };
      observerObj.tuanState1.update=function(data){
        if(data.tstate=="sales"){
          var pLeft=data.ptotal-data.pnum,propagation="";
          if(data.isVTuan){
            propagation=pLeft>=3?("至尊无敌超级团，差<b>"+pLeft+"</b>人，抢个位"):(pLeft==2?("仅剩<b>"+pLeft+"</b>名额，想搭车的赶紧"):"最后<b>1</b>席，超级团谁来压轴？");
          }else{
            propagation=pLeft>=3?("还差<b>"+pLeft+"</b>人，盼你如同盼春天"):(pLeft==2?("还差<b>"+pLeft+"</b>人，盼你如南方人盼暖气~"):"还差<b>1</b>人，是你、是你就是你！");
          }
          $(".pp_tips").html(propagation);
        }else if(data.tstate=="succ"){
          if(data.upstate){//优惠用户
            $(".pp_tips").html("感谢各位仗义救急！妥妥儿的");
          }else if(data.tbstate=="bao"){//挤爆满额
            $(".pp_tips").html("此团已严重超载，没赶趟的开新团去吧~");
          }else if(data.userlist.length>data.ptotal){//已有挤爆，但未满额
            $(".pp_tips").html("此团已满座，愿意挤上来的自备小凳~");
          }else{//默认状态
            $(".pp_tips").html(data.isVTuan?"银河系无敌超级团，完美收官！":"对于诸位大侠的相助，团长感激涕零");
          }
        }else{
          $(".pp_tips").html(data.isVTuan?"失败告诉我一个道理：一人不齐，满团皆输":"分享不努力，失败徒伤悲");
        }
      };
      observerObj.tuanState2.update=function(data){
        if(data.tstate=="sales"){
          nowtime=new Date((data.servertime*1000)||(new Date())).getTime();
          window.endtime=data["endtime"]*1000;
          window.starttime=data["createtime"]*1000;
          changeEndTime();
        }else if(data.tstate=="failed"){
          $(".pp_state_txt").html("团购失败，"+(data.iscod?"您的订单已关闭":"给大家退款中")).show();
          $(".pp_time").hide();
        }else{
          $(".pp_state_txt").show();
          $(".pp_time").hide();
        }
      };
      observerObj.yaoheList.update=function(data){
        var customTuan=getCustomTuanInfo();
        showYaoheMod(data.userlist,data.tstate,{"totalTNum":data.ptotal,"haveNum":data.pnum},customTuan,data.isWanRenTuan?true:false);
      };
      observerObj.seller.update=function(data){
        if(data["shopName"]){
          $("#itemShopName").html(data["shopName"]);
          doc.getElementById("shopLink").href="http://wd.paipai.com/mshop/"+data["shopId"];
        }else{
          $(".support").hide();
        }
        //微信下显示
        if(!isWx() || !data.shopwxid){
          $(".support_btn").hide();
        }else{
          $(".support_btn").click(function(){
            $("#shopwxid").val(data.shopwxid);
            $(".mod_wxa").show();
            return false;
          });
          $(".mod_wxa_close").click(function(){$(".mod_wxa").hide();});
        }
      };
      observerObj.hotTuan.update=function(data){
        if(data.tstate=="failed" || (data.tbstate=="bao" && data.ustate=="noin")){
          hotTuanInto("#hotTuanWrap");
        }
      };
      observerObj.bottom.update=function(data){
        switch(data.tstate){
          case "sales":
            footFoucsPPY(2);
            break;
          case "failed":
          case "succ"://迭代点
            footFoucsPPY(3);
            break;
        }
      };
      observerObj.button.update=function(data){
        $(".fixopt").show();
        if(data.tstate=="sales" && data["canbuy"]>0){
          if(data.ustate=="succ"){
            $("#bottomBtn1").show();
          }else{
            $(".fixopt_item").hide();
            $(".fixopt_item1").show();
          }
          bottomBtnEvent(data);
        }else{
          $("#bottomBtn3").html(data["canbuy"]<=0?"补货中，看全部商品":"看全部商品，我也开个团").attr("href","tuan_list.html?_wv=1&ptag=20400.9.3");
          $("#bottomBtn3").show();
        }
      };
      observerObj.hongbao.update=function(data){
        if(data.hongbao==""){//没有红包或已抢光
          return;//return when the user has taken "hongbao".
        }
        var packetIdStr=data.hongbao.replace(/:[^]*/,"");
        $.ajax({
          url:"http://b.paipai.com/tuaninfo/CheckPackageKeyByUin?packagekey="+packetIdStr,
          type:"GET",
          dataType:"jsonp",
          jsonpCallback:"CheckPackageKeyByUin",
          success:function(json){
            if(json && json.errCode==0){
              if(!json.isget){//未领过红包
                if(data.ustate=="noin"){
                  if(data.tstate=="succ"){
                    switch(data.tbstate){
                      case "true":
                          $(".cardtips_txt").html("此团实在太火爆，已成团。<br>快来抢个团长红包或挤爆该团吧！");
                          $(".cardtips_btn").attr("href","http://m.paipai.com/promote/card_packet.html?type=3&packetId="+packetIdStr);
                          $(".topStateWrap,.fixopt_item").hide();
                          $(".cardtips").show();
                          $(".fixopt_item2").show();
                          $("#bottomBtn9").attr("href","tuan_jibaoresult.html?_wv=1&isvtuan="+data.isVTuan+"&ptotal="+data.ptotal+"&dataBuyNum="+dataBuyNum+"&uin="+uin+"&comodityId="+commodityId+"&gid="+gid);
                          $("#bottomBtn9").on("click",function(){
                            setLocalItem(data,0);
                          });
                        break;
                      default:
                          $(".cardtips_btn").attr("href","http://m.paipai.com/promote/card_packet.html?type=3&packetId="+packetIdStr);
                          $(".topStateWrap").hide();
                          if(data.tbstate=="false"){$(".cardtips_txt_pan").remove()};
                          $(".cardtips").show();
                        break;
                    }
                  }
                }else{
                  if(data.tstate=="succ"){
                    $(".ctips_btn").attr("href","http://m.paipai.com/promote/card_packet.html?type=3&packetId="+packetIdStr);
                    $(".ctips,#mod_maskID").css("z-index",901).show();
                    $(".ctips_close").click(function(){
                      $(".ctips,#mod_maskID").hide();
                      return false;
                    });
                  }
                }
              }else{//已领过红包
                if(data.ustate=="noin" && data.tstate=="succ" && data.tbstate=="true"){
                  $(".topStateWrap,.cardtips_opt,.fixopt_item").hide();
                  $(".cardtips_txt").html("来晚了一步！该团已成团<br>很想要吗？那来挤爆团吧！");
                  $(".cardtips").show();
                  $(".fixopt_item2").show();
                  $("#bottomBtn9").attr("href","tuan_jibaoresult.html?_wv=1&isvtuan="+data.isVTuan+"&ptotal="+data.ptotal+"&dataBuyNum="+dataBuyNum+"&uin="+uin+"&comodityId="+commodityId+"&gid="+gid);
                  $("#bottomBtn9").on("click",function(){
                    setLocalItem(data,0);
                  });
                }
              }
            }
          }
        });
      }
      observerObj.cps.update=function(data){
        if(data.isCPSTUser && data.isCPSItem){
          function reportCPS(){
            $.ajax({
                    url:"http://b.paipai.com/userinfo/gencpsurl?tuid="+data.leaderUin+"&itemid="+data.itemID,
                    type:"GET",
                    dataType:"jsonp",
                    jsonpCallback:"gencpsurl",
                    success:function(json){
                      if(json && json.ret==0){
                        document.createElement("img").src="http://re.paipai.com/tws/etgcl/click?fu="+encodeURIComponent("http://auction1.paipai.com/"+data.itemID+".3000")+"&pps="+json.cpsurllist[0]["pps"]+"&jflag=1";
                      }
                    }
                  });
          }
          if(isMQQUserAgent()){
            var QQ_uin = getCookie("uin").replace(/^o(0*)/,'');
            $.ajax({
              url:"http://wd.paipai.com/wxd/user/getuniformlogin",
              type:"GET",
              dataType:"jsonp",
              data:{
                type:0,
                uin:QQ_uin,
                skey:getCookie("skey"),
                nickname:getCookie("nickname") || QQ_uin
              },
              success:function(wgJson){
                if(wgJson && wgJson.ret =='0'){
                  setCookie('wg_uin',wgJson.wg_uin,"","","paipai.com");
                  setCookie('wg_skey',wgJson.wg_skey,"","","paipai.com");
                  reportCPS();
                }
              }
            });
          }else{
            reportCPS();
          }
        }
      }
      //E observer工具
      //根据用户的uin和团gID获取团信息
      var getGrounpInfo=function(){
              xhr=$.ajax({
                      url: 'http://b.paipai.com/tuaninfo/GetDetail?tid='+gid+'&uid='+uin,
                      type: 'GET',
                      dataType: 'jsonp',
                      jsonpCallback:'GetDetail',
                      success: function(data) {
                          isSuccess=true;
                          $("#loadingBox").hide();
                          if(!data["errCode"])
                          {
                              window.commodityId=data["itemID"];
                              window._itemInfo = {
                                "commodity":{
                                   "C_ID":data.itemID,
                                   "C_Uin":data.shopId,
                                   "C_ClassID":""
                                }
                                };
                              $("#eccReportJs").attr("src","http://static.gtimg.com/js/version/2014/09/ecc.cloud.ppmobilereport.20140905.js?t=201409091150");
                              dataBuyNum=data["buy_num"];
                              data["saleprice"]=(parseFloat(data["saleprice"])/100).toFixed(2);
                              data["marketprice"]=(parseFloat(data["marketprice"])/100).toFixed(2);
                              data["mname"]=data["mname"]==""?"你好友":decodeNick(data["mname"]);
                              data["img"]=data["img"].replace(".0.jpg",".0.120x120.jpg");//换小尺寸图URL进行分享展示透传
                              if(data["userlist"][0]){data["userlist"][0]["userpaytime"]=data["createtime"];}
                              //向observer通告变更信息
                              domObserverList.notify(data);
                              if(isMQQUserAgent() || isWx()){
                                var shareContent= initShareContent(data);
                                if(data["itemID"]=="39E9C197000000000401000045AA3913"){//三个爸爸
                                  var sangebabaShareTitle=["首发直降1000块！","凑够2人省2000！","大家省才是真的省！"],
                                      sangebabaShareDesc=["快来跟我一起抢儿童专用空气净化器！","快来一起买儿童专用净化器！怒省1000元！从今天起，做一个不再败家的剁手党！","凑够2人省2000！约吗！"],
                                      sangebabaShareTtitle=["首发直降1000块，快来跟我一起抢儿童专用空气净化器！","凑够2人省2000，快来一起买儿童专用净化器！怒省1000元！从今天起，做一个不再败家的剁手党！","大家省才是真的省，凑够2人省2000！约吗！"];
                                  var randomNum=Math.floor(Math.random()*(3-0)+0);
                                  shareApi({
                                    "url": urlAddUin(curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),getQQ()),
                                    "qq_title": sangebabaShareTitle[randomNum],
                                    "qzone_title": sangebabaShareTitle[randomNum],
                                    "qq_desc": sangebabaShareDesc[randomNum],
                                    "qzone_desc": sangebabaShareDesc[randomNum],
                                    "wechat_title": sangebabaShareTitle[randomNum],
                                    "wechat_desc": sangebabaShareTtitle[randomNum],
                                    "wechat_time_title": sangebabaShareTtitle[randomNum],
                                    "img": data["img"],
                                    "ptag":{ 
                                             w2w:"20400.5.3", //微信分享至微信好友
                                             w2t:"20400.5.4", //微信分享至朋友圈
                                             q2q:"20400.5.1", //手Q分享至QQ好友或群
                                             q2z:"20400.5.2", //手Q分享至QQ空间
                                             q2w:"20400.5.3", //手Q分享至微信好友
                                             q2t:"20400.5.4"  //手Q分享至朋友圈
                                           }
                                  });
                                }else{
                                  shareApi({
                                    "url": urlAddUin(curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),getQQ()),
                                    "qq_title": (data.isVTuan?"超级团长":"")+data["mname"]+"发起了团购：",
                                    "qzone_title": (data.isVTuan?"超级团长":"")+data["mname"]+"发起了团购：",
                                    "qq_desc": "【"+data["ptotal"]+"人成团"+data["saleprice"]+"元】"+(data["title"].length>16?(data["title"].substring(0,16)+"..."):data["title"])+"，跟我组团抢，超划算，信我没错！",
                                    "qzone_desc": "【"+data["ptotal"]+"人成团"+data["saleprice"]+"元】"+(data["title"].length>16?(data["title"].substring(0,16)+"..."):data["title"])+"，跟我组团抢，超划算，信我没错！",
                                    "wechat_title": (data.isVTuan?"超级团长":"")+data["mname"]+"发起了团购：",
                                    "wechat_desc": shareContent,
                                    "wechat_time_title": shareContent,
                                    "img": data["img"],
                                    "ptag":{ 
                                             w2w:"20400.5.3", //微信分享至微信好友
                                             w2t:"20400.5.4", //微信分享至朋友圈
                                             q2q:"20400.5.1", //手Q分享至QQ好友或群
                                             q2z:"20400.5.2", //手Q分享至QQ空间
                                             q2w:"20400.5.3", //手Q分享至微信好友
                                             q2t:"20400.5.4"  //手Q分享至朋友圈
                                           }
                                  });
                                }
                              };
                              if(navigator.userAgent.indexOf("NewsApp")>=0){
                                wyShare({
                                  title    : "【拍拍-拍便宜】",
                                  linkUrl  : urlAddUin(curHref.replace(/paysource=buyone&/g,"").replace(/paysource=buyone/g,""),getQQ()),
                                  photoUrl : data.img,
                                  text     : data.mname+"开摊吆喝了，速来围观Ta的品位吧！"+data.title
                                });
                              };
                          }else if(data["errCode"]==13){
                                plogin(window.location.href);
                          }else if(data["errCode"]==20020){
                            setTimeoutInfo();
                          }else{
                            $("#sorryWord").html("<p>对不起，系统加载繁忙~~~您可以选择：<p><p><a onclick='window.location.reload()'>1、刷新试试？</a></p><p><a href='tuan_list.html?_wv=1'>2、去看看别的→</a></p>");
                             setTimeoutInfo();
                          }
                      }
              });
              window.timer=new Date().getTime();
              var timeoutWork=function(){
                 if(!isSuccess){
                       var tt=new Date().getTime()-window.timer;
                       if(tt<15000){
                            setTimeout(timeoutWork,15000-tt);
                       }else{
                         setTimeoutInfo(); 
                       }
                  }
              };
              setTimeout(timeoutWork,15000);
      };
      var Init=function(){
           //check();
           if(gid!="")
           {
              getGrounpInfo();
           }
           else
           {
              //重定向去列表页
           }
      }();

}());