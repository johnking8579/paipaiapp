/**
 * Created by liupengfei1 on 2015/1/22.
 */

(function(){

    function PointReport(point){
        this.urlParam= {};
        this.reportData= {};
        this.getParams();
        this.upPv= point.upPv;
        this.pv_ptag= point.pv_ptag;
        this.launchType= point.launchType;
        this.ptagType= point.ptagType;
        this.isHttp= point.isHttp;
        this.duration= point.duration;
        this.dap= point.dap;
        this.clickParam= point.clickParam;
        this.getPoint();
        this.reportAjax();
    }

    PointReport.prototype.getParams= function(){
        var param= "";
        if(sessionStorage["pointParam"]&&sessionStorage["pointParam"]!=""&&sessionStorage["pointParam"]!=null){
            param= sessionStorage["pointParam"];
        }else{
            var url= location.href;
            if (url.indexOf("appToken") != -1) {
                param= url.substr(url.indexOf("appToken") );
                sessionStorage["pointParam"]= param;
            }
        }
        var params= param.split("&");
        for (var i = 0; i < params.length; i++) {
            this.urlParam[params[i].split("=")[0]] = decodeURI(params[i].split("=")[1]);
        }
    }

    PointReport.prototype.getPoint= function(){
        var refer= this.pv_ptag=="pv"?document.referrer:"",
            page_name= location.origin+location.pathname,
            launchType= sessionStorage[this.launchType]?"back":"new",
            devicetime= (new Date()).valueOf(),
            isHttp= this.isHttp,
            duration= this.duration,
            pageParam= "",
            rawPvid= this.urlParam["mk"]+"|"+devicetime+"|"+Math.floor(Math.random()*10),
            dap= this.dap,
            ptag = this.pv_ptag=="pv"?"":ptag,
            clickParam = this.pv_ptag=="pv"?"":this.clickParam;

        if(this.pv_ptag=="ptag"){
            sessionStorage[this.ptagType]=this.ptag;
        }else{
            sessionStorage[this.launchType]= "1";
        }
        if(sessionStorage[this.ptagType]&&sessionStorage[this.ptagType]!=""&&sessionStorage[this.ptagType]!=null){
            pageParam = "ptag="+sessionStorage[this.ptagType]+"&launchType="+launchType;
        }else{
            pageParam = "launchType="+launchType;
        }

        this.reportData= {
            content:devicetime+"|||"+page_name+"|||"+isHttp+"|||"+duration+"|||"+pageParam+"|||"+ptag+"|||"+clickParam+"|||"+refer+"|||"+rawPvid+"|||"+dap
        };
        for(var key in this.urlParam){
            this.reportData[key] = this.urlParam[key];
        }

    }

    PointReport.prototype.reportAjax= function(){
        $.ajax({
            url: this.upPv,
            type: 'GET',
            data :this.reportData,
            dataType: 'json',
            timeout:15000,
            success: function(data) {
            }
        });
    }

    PointReport.report = function(point){
        return new PointReport(point);
    }

   /* PointReport.report({
        upPv: "http://app.paipai.com/api/bi/upPv.xhtml",
        pv_ptag: "pv",
        launchType: "launchType",
        ptagType: "ptagType",
        isHttp: "isHttp",
        duration: "duration",
        dap: "dap",
        clickParam: "clickParam"
    });*/
})()


