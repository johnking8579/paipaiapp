<%@ page contentType="text/html; charset=utf-8" language="java"%>
<%@ include file="/include/include.jsp"%>

<c:set var="pn" value="${param.pn}" scope="request"/>
<c:set var="ps" value="${param.ps}" scope="request"/>
<c:if test="${empty totalCount or totalCount eq 0}">
<c:set var="totalCount" value="${param.totalCount}" scope="request"/>
</c:if>

<c:if test="${empty pn or pn eq 0}">
<c:set var="pn" value="1" scope="request"/>
</c:if>
<c:if test="${empty ps or ps eq 0 }">
<c:set var="ps" value="5" scope="request"/>
</c:if>

<%
int[] pageSizes = {5,10,20,40}; // 普通数组，JSTL直接使用JSP赋值表达式来取
 %>
<!-- 总记录数超过了10才显示页大小切换的模块 -->
<c:if test="${totalCount ge 10}">
<div class="qb_page">每页显示:&#160;
<c:forEach items='<%=pageSizes%>' var='pageSizeSwitch' varStatus='status'>
<c:if test="${totalCount ge pageSizeSwitch*1}">
<c:if test="${pageSizeSwitch eq ps}">${pageSizeSwitch}</c:if>
<c:if test="${pageSizeSwitch ne ps}">
<a href="${param.url}&amp;ps=${pageSizeSwitch}">${pageSizeSwitch}</a>
</c:if>
<!-- 仅当总记录数大于下一阶段分页大小才展示| -->
<c:if test="${not status.last and totalCount ge pageSizeSwitch*2}">
<span class="qb_c_gray">&#160;|&#160;</span>
</c:if>
</c:if>
</c:forEach>
</div>
</c:if>
   
