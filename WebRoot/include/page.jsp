<%@ page contentType="text/html; charset=utf-8" language="java"%>
<%@ include file="/include/include.jsp"%>
<%--禁止浏览器cache本页面--%>
<% 
response.setHeader("Pragma","no-cache"); 
response.setHeader("Cache-Control","no-cache"); 
response.setDateHeader("Expires", 0); 
%>
<c:set var="pn" value="${param.pn}"></c:set>
<c:if test="${empty pn or pn <= 0}">
	<c:set var="pn" value="1"></c:set>
</c:if>

<c:set var="ps" value="${param.ps}" scope="request"></c:set>
<c:if test="${empty ps or ps <= 0}">
	<c:set var="ps" value="5" scope="request"></c:set>
</c:if>

<c:set var="totalCount" value="${param.totalCount}" scope="request"></c:set>
<c:if test="${empty totalCount or totalCount <= 0}">
	<c:set var="totalCount" value="0" scope="request"></c:set>
</c:if>
<!-- 设置总页码 -->
<%
int totalCount=request.getParameter("totalCount")==null?0:Integer.parseInt(request.getParameter("totalCount"));
int pageSize=request.getParameter("ps")==null?5:Integer.parseInt(request.getParameter("ps"));
int pageCount=totalCount%pageSize==0?totalCount/pageSize:totalCount/pageSize+1;
 %>
<!-- 总页数超过2页才分页 -->
<c:if test="${totalCount gt ps*1}">
<div class="qb_page">
<!-- 首页 -->
<c:if test="${pn eq 1}">
<a href="${param.url}&amp;ps=${ps}&amp;pn=${pn+1}">下页</a>
<c:if test="${totalCount gt ps*2}">
<a href="${param.url}&amp;ps=${ps}&amp;pn=<%=pageCount%>">末页</a>
</c:if>
</c:if>
<!-- 末页 -->
<c:if test="${(pn)*(ps) ge totalCount}">
<a href="${param.url}&amp;ps=${ps}&amp;pn=${pn-1}">上页</a>
<c:if test="${totalCount gt ps*2}">
<a href="${param.url}&amp;ps=${ps}&amp;pn=1">首页</a>
</c:if>
</c:if>
<!-- 中间页 -->
<c:if test="${pn gt 1 and (pn)*(ps) lt totalCount and totalCount gt ps*2}">
<a href="${param.url}&amp;ps=${ps}&amp;pn=${pn+1}">下页</a>
<a href="${param.url}&amp;ps=${ps}&amp;pn=${pn-1}">上页</a>
<a href="${param.url}&amp;ps=${ps}&amp;pn=1">首页</a>
<a href="${param.url}&amp;ps=${ps}&amp;pn=<%=pageCount%>">末页</a>
</c:if>
<span class="gray">第${pn}/<%=pageCount%>页</span>
</div>
</c:if>
