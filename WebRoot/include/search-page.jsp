<%@ page contentType="text/html; charset=utf-8" language="java"%>
<%@ include file="/include/include.jsp"%>
<%--禁止浏览器cache本页面--%>
<% 
response.setHeader("Pragma","no-cache"); 
response.setHeader("Cache-Control","no-cache"); 
response.setDateHeader("Expires", 0); 
request.setAttribute("urlPref", basePath);
int totalCount=request.getParameter("totalCount")==null?0:Integer.parseInt(request.getParameter("totalCount"));
int pageSize=request.getParameter("ps")==null?5:Integer.parseInt(request.getParameter("ps"));
int pageCount=totalCount%pageSize==0?totalCount/pageSize:totalCount/pageSize+1;
request.setAttribute("pageCount", pageCount);
%>
<c:set var="pn" value="${param.pn}"></c:set>
<c:if test="${empty pn or pn <= 0}">
	<c:set var="pn" value="1"></c:set>
</c:if>

<c:set var="ps" value="${param.ps}" scope="request"></c:set>
<c:if test="${empty ps or ps <= 0}">
	<c:set var="ps" value="5" scope="request"></c:set>
</c:if>

<c:set var="totalCount" value="${param.totalCount}" scope="request"></c:set>
<c:if test="${empty totalCount or totalCount <= 0}">
	<c:set var="totalCount" value="0" scope="request"></c:set>
</c:if>

<c:if test="${pageCount gt 100}">
	<c:set var="pageCount" value="100"></c:set>
</c:if>

<c:if test="${pageCount gt 1}">
<c:choose>
<c:when test="${pageCount eq 2}">
<div class="searchpage">
<p>
<c:if test="${pn eq 1}">
<a href="${urlPref}${param.url}&amp;ps=${ps}&amp;pn=2">下页</a>
</c:if>
<c:if test="${pn eq 2}">
<a href="${urlPref}${param.url}&amp;ps=${ps}&amp;pn=1">上页</a>
</c:if>
</p>
</div>
</c:when>

<c:when test="${pageCount ge 3 and pageCount le 7}">
<div class="searchpage">
<p>
<c:if test="${pn ge 1 and pn lt 7}">
<a href="${urlPref}${param.url}&amp;ps=${ps}&amp;pn=${pn + 1}">下页</a>
</c:if>
<c:if test="${pn gt 1 and pn le 7}">
<a href="${urlPref}${param.url}&amp;ps=${ps}&amp;pn=${pn - 1}">上页</a>
<a href="${urlPref}${param.url}&amp;ps=${ps}&amp;pn=1">首页</a>
</c:if>
</p>
</div>
<div class="searchpage">
<p>
<c:forEach begin="1" end="${pageCount}" step="1" varStatus="sindex">
<c:choose>
<c:when test="${pn eq sindex.index}">
<span class="gray">[${sindex.index}]&nbsp;</span>
</c:when>
<c:otherwise>
<a href="${urlPref}${param.url}&amp;ps=${ps}&amp;pn=${sindex.index}">${sindex.index}&nbsp;</a>
</c:otherwise>
</c:choose>
</c:forEach>
</p>
</div>
</c:when>

<c:otherwise>
<div class="searchpage">
<p>
<c:if test="${pn ge 1 and pn lt pageCount*1}">
<a href="${urlPref}${param.url}&amp;ps=${ps}&amp;pn=${pn + 1}">下页</a>
</c:if>
<c:if test="${pn gt 1 and pn le pageCount*1}">
<a href="${urlPref}${param.url}&amp;ps=${ps}&amp;pn=${pn - 1}">上页</a>
<a href="${urlPref}${param.url}&amp;ps=${ps}&amp;pn=1">首页</a>
</c:if>
</p>
</div>
<div class="searchpage">
<p>
<c:set var="indexStart" value="1"></c:set>
<c:set var="indexStop" value="7"></c:set>
<c:if test="${pn gt 3 and pn lt 98}">
	<c:set var="indexStart" value="${pn - 3}"></c:set>
	<c:set var="indexStop" value="${pn + 3}"></c:set>
	<c:if test="${indexStop gt pageCount}">
	<c:set var="indexStop" value="${pageCount}"></c:set>
	<c:choose>
		<c:when test="${pageCount gt 7}">
			<c:set var="indexStart" value="${pageCount - 6}"></c:set>
		</c:when>
		<c:otherwise>
			<c:set var="indexStart" value="1"></c:set>
		</c:otherwise>
	</c:choose>
</c:if>
</c:if>
<c:if test="${pn ge 98}">
	<c:set var="indexStart" value="94"></c:set>
	<c:set var="indexStop" value="100"></c:set>
</c:if>
<c:forEach begin="${indexStart}" end="${indexStop}" step="1" varStatus="sindex1">
<c:choose>
<c:when test="${pn eq sindex1.index}">
<span class="gray">[${sindex1.index}]&nbsp;</span>
</c:when>
<c:otherwise>
<a href="${urlPref}${param.url}&amp;ps=${ps}&amp;pn=${sindex1.index}">${sindex1.index}&nbsp;</a>
</c:otherwise>
</c:choose>
</c:forEach>
</p>
</div>
</c:otherwise>
</c:choose>
</c:if>