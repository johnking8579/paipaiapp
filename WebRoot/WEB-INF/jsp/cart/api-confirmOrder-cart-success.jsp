<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
{ "errCode":"${result.errCode}", "retCode":"${result.retCode}",
"msg":"${result.msg}", "dtag":"${result.dtag}", "data":{
"totalPrice":${confirmVo.totalPrice}, "normalPackages":[
<c:forEach items='${confirmVo.normalPackages}' var='package'
	varStatus='i'>
			 	{
		 		    "sellerUin": "${package.sellerUin}",
   					"totalPrice": "${package.totalPrice}",
					"shopName":"${package.shopName}",
					"shopType":"${package.shopType}",
					"promotionRules":[<c:forEach items='${package.promotionRules}'
		var='promo' varStatus='j'>
		{
		"ruleId":"${promo.ruleId}",
		"limitNum":"${promo.limitNum}",
		"limitPrice":"${promo.limitPrice}",		
		"freeMoney":"${promo.freeMoney}",
		"discount":"${promo.discount}",
		"desc":"${promo.desc}",
		"barterMoney":"${promo.barterMoney}",
		"present":"${promo.present}",
		"freeCarriage":"${promo.freeCarriage}"}
		<c:if test='${j.last != true}'>,</c:if>
	</c:forEach>],
   					"shipCalcInfos":[<c:forEach items='${package.shipCalcInfos}'
		var='shipCalcVo' varStatus='j'>
		{
		"mailType":"${shipCalcVo.mailType}",
		"payType":"${shipCalcVo.payType}",
		"name":"${shipCalcVo.name}",
		"fee":"${shipCalcVo.fee}"}
		<c:if test='${j.last != true}'>,</c:if>
	</c:forEach>],
	"items":[<c:forEach items='${package.items}' var='it' varStatus='j'>
	{"itemTitle":"${it.itemTitle}",
	"itemCode":"${it.itemCode}",
	"itemAttr":"${it.itemAttr}",
	"buyNum":"${it.buyNum}",
	"redPacketValue":"${it.redPacketValue}",
	"mostLowPrice":{"priceValue":"${it.mostLowPrice.priceValue}","priceType":"${it.mostLowPrice.priceType}","priceTypeDesc":"${it.mostLowPrice.priceTypeDesc}"},
	"mainLogoUrl":"${it.mainLogoUrl}",
	"totalPrice":"${it.totalPrice}"}
		<c:if test='${j.last != true}'>,</c:if>
	</c:forEach>]
			 	}
			 	<c:if test='${i.last != true}'>,</c:if>
</c:forEach>
], "redPackages":[
<c:forEach items='${confirmVo.redPackages}' var='red' varStatus='j'>
	{"packetId":"${red.packetId}",
	"validShop":[<c:forEach items='${red.validShop}'
		var='shop' varStatus='k'>
			"${shop}"
		<c:if test='${k.last != true}'>,</c:if>
	</c:forEach>],
	"value":"${red.value}",
	"low":"${red.low}",
	"name":"${red.name}"}
		<c:if test='${j.last != true}'>,</c:if>
</c:forEach>
] } }
