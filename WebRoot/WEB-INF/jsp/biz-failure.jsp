<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/include/include.jsp" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ include file="/include/head.jsp" %>
<title>出现错误</title>
<style type="text/css">
</style>
</head>
  
  <body>
    <%@ include file="/include/logo.jsp" %>
    <div class="warn">
    	<p><img class="img1" alt="出现错误" src="<%=wimgPath%>/icon1_warn.png" />&nbsp;亲,你试图访问的页面暂时无法打开,${result.msg},错误码:${result.errCode}</p>
    </div>
    <div class="div3">
    	您再看看别的?<a href="<%=basePath%>/w/template.xhtml?tid=index&amp;sid=${sid}&amp;${baseParam}">去逛逛</a><br/>
    </div>
  	<%@ include file="/include/foot.jsp" %>
  </body>
</html>