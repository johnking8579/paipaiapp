<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
{
  "errCode":${result.errCode},
  "retCode":${result.retCode},
  "msg":"${result.msg}",
  "dtag":"${result.dtag}",
  "data":{
  	"funProperty":"0000", 
    "user" : {
        "colorDiamondLevel" : ${colorDiamondInfo.colorDiamondLevel},
        "colorDiamondState" : ${colorDiamondInfo.colorDiamondState},
        "level" : ${level}
    }
  }
 }