<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
{
  "errCode":${result.errCode},
  "retCode":${result.retCode},
  "msg":"${result.msg}",
  "data":{
        "totalNum": "${searchPo.totalNum }",
        "keyType": "${searchPo.keyType }",
        "keyWords": "${keyWords}",
        "shopList": ${searchPo.shopList},
        "class": [
        <c:forEach items='${searchPo.classes}' var='clazz' varStatus='sClazz'>
        {
            "id": "${clazz.id }",
            "count": "${clazz.count }"
        }
        <c:if test="${sClazz.last != true}">,</c:if>
        </c:forEach>
        ],
        "paths": [
        <c:forEach items='${searchPo.paths}' var='path' varStatus='sPath'>
        {
            "pathId": "${path.pathId }",
            "pathName": "${path.pathName }",
            "itemId": "${path.itemId }",
            "itemName": "${path.itemName }",
            "path": "${path.path }"
        }
        <c:if test="${sPath.last != true}">,</c:if>
        </c:forEach>
        ],
        "cluster": [
        <c:forEach items='${searchPo.classCluster.clusterList}' var='cluster' varStatus='sCluster'>
        {
            "clusterId": "${cluster.clusterId }",
            "clusterName": "${cluster.clusterName }",
            "clusterCount": "${cluster.clusterCount }",
            "path": "${cluster.path }"
        }
        <c:if test="${sCluster.last != true}">,</c:if>
        </c:forEach>   
        ],
        "properties": [
        <c:forEach items='${searchPo.attrClusterSetList}' var='property' varStatus='sProperty'>
        {
            "name": "${property.setName }",
            "options": [
            <c:forEach items='${property.clusterList}' var='pcluster' varStatus='spCluster'>
            {
                 "clusterId": "${pcluster.clusterId }",
                 "clusterName": "${pcluster.clusterName }",
                 "clusterCount": "${pcluster.clusterCount }",
                 "path": "${pcluster.path }"
            }
            <c:if test="${spCluster.last != true}">,</c:if>
            </c:forEach>   
            ]
        }
        <c:if test="${sProperty.last != true}">,</c:if>
        </c:forEach>
        ],
        "items": [
        <c:forEach items='${searchPo.items}' var='item' varStatus='sItem'>
        {
            "mainImg": "${item.mainImg }",
            "mainImg60": "${item.mainImg60 }",
            "mainImg80": "${item.mainImg80 }",
            "mainImg120": "${item.mainImg120 }",
            "mainImg200": "${item.mainImg200 }",
            "mainImg300": "${item.mainImg300 }",
            "title": "${item.title }",
            "link": "${item.link }",
            "takeCheapLink": "${item.takeCheapLink}",
            "itemCode": "${item.itemCode}",
            "sellerUin": "${item.sellerUin}",
            "sellerNickName": "${item.sellerNickName}", 
            "sellerCredit": "${item.sellerCredit}",
            "sellerLevel": "${item.sellerLevel}",
            "mailPrice": "${item.mailPrice}",
            "expressPrice": "${item.expressPrice}",
            "emsPrice": "${item.emsPrice}",
            "price": "${item.price}",
            "activePriceType": "${item.activePriceType}",
            "marketPrice": "${item.marketPrice}",
            "shopAddr": "${item.shopAddr}",
            "soldNum": "${item.soldNum}",
            "favNum": "${item.favNum}",
            "userValidStatus": "${item.userValidStatus}",
            "legend1Status": "${item.legend1Status}",
            "legend2Status": "${item.legend2Status}",
            "legend3Status": "${item.legend3Status}",
            "legend4Status": "${item.legend4Status}",
            "codStatus": "${item.codStatus}",
            "redPacketStatus": "${item.redPacketStatus}",
            "autoShipStatus": "${item.autoShipStatus}",
            "validClothStatus": "${item.validClothStatus}",
            "qqVipPrice": "${item.qqVipPrice}",
            "propertyString": "${item.propertyString}",
            "commProperty": "${item.commProperty}",
            "promotion": ${item.promotion},
            "isSupportColorDiamond": "${item.supportColorDiamond}",
            "redPacket": "${item.redPacket}",
            "qqShop": "${item.commPrpertyQQShop }",
            "freeMail": "${item.freeMail}",
            "personalizedTypeTag":"${item.personalizedTypeTag}",
            "istakeCheap":"${item.istakeCheap}",
            "isFlashSale":"${item.isFlashSale}"
        }
        <c:if test="${sItem.last != true}">,</c:if>
        </c:forEach>
        ]
  }
}
