<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${!empty callback}">try{${callback}(</c:if>{
  "errCode":"${result.errCode}",
  "retCode":"${result.retCode}",
  "msg":"${result.msg}",
  "dtag":"${result.dtag}"
}<c:if test="${!empty callback}">);}catch(e){}</c:if>