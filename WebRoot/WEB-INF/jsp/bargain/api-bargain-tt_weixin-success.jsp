<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<title>腾讯新闻</title>
<style type="text/css">
<!--

body {
    background-color: #f7f2ed;
}

.main {
       min-width: 280px;
       margin-left:8px;
       margin-right: 8px;
       font-family: "黑体";
}
.text {
/*    font-size: 17px;*/
    TEXT-INDENT: 2em;
    line-height: 24px;
    text-align: justify;
}
.title {
/*    FONT-SIZE: 16pt;*/
    font-weight: bold;
    margin-top: 30px;
    margin-bottom: 10px;
}
.src {
/*    FONT-SIZE: 8pt;*/
    font-weight: bold;
    color: #aaaaaa;
}

.huodong {
    text-decoration:none;
    TEXT-INDENT:2em;
    line-height:24px;
    text-align:justify;
}

#huodong a:visited {
    color:black;
}
#huodong a:link {
    color:black;
}
#huodong a:hover {
    color:black;
}
#huodong a:active {
    color:black;
}
.fontSize1 .title { font-size:20px; }
.fontSize1 .text {  font-size:14px; }
.fontSize1 .src  { font-size:11px; }

.fontSize2 .title { font-size:22px; }
.fontSize2 .text {  font-size:16px; }
.fontSize2 .src  { font-size:13px; }

.fontSize3 .title { font-size:24px; }
.fontSize3 .text {  font-size:18px; }
.fontSize3 .src { font-size:15px; }

.fontSize4 .title { font-size:26px; }
.fontSize4 .text {  font-size:20px; }
.fontSize4 .src  { font-size:16px; }

.moreOperator a { text-decoration:none;color:#385487; }

.moreOperator .share{ border-top:1px solid #ddd;display:none }

.moreOperator .share a{ display:block;border:1px solid #ccc;border-radius:4px;margin:20px 0;border-bottom-style:solid;background:#f8f7f1;color:#000; }

.moreOperator .share a span{ display:block;padding:10px 10px;border-radius:4px;text-align:center;border-top:1px solid #eee;border-bottom:1px solid #eae9e3;font-weight:bold; }

.moreOperator .share a:hover,
.moreOperator .share a:active { background:#efedea; }
@media only screen and (-webkit-min-device-pixel-ratio: 2) {    
}
.client_new_tips{display:block;text-decoration:none;text-align:center;background:url(http://mat1.gtimg.com/www/apps/images/newsdownload_both.png.png) no-repeat -20px 0;background-size:320px auto;height:94px;padding-top:9px;color:#000;width:280px;margin:0 auto; visibility:hidden}
.client_new_tips span{display:block;margin-top:12px;padding:0;}

.client_open_tips{display:block;text-decoration:none;text-align:center;background:url(http://mat1.gtimg.com/www/apps/images/newsdownload_both.png.png) no-repeat -20px -103px;background-size:320px auto;height:94px;padding-top:9px;color:#000;width:280px;margin:0 auto; visibility:hidden}
.client_open_tips span{display:block;margin-top:12px;padding:0;}

.showMore:active{background-color:#f7f2ed;}
#showMore.touchstart {background:-moz-linear-gradient(left, rgb(255, 255, 255) 0, rgb(220, 231, 244) 5%, rgb(220, 231, 244) 93%, rgb(255, 255, 255) 100%);background:-webkit-linear-gradient(left, #fff 0%, #DCE7F4 5%, #DCE7F4 93%, #fff 100%);background:-webkit-gradient(linear,left top,right bottom, color-stop(0, #fff), color-stop(0.05, #DCE7F4),color-stop(0.93, #DCE7F4),color-stop(1, #fff));}
#showMore .showMore{display:block;cursor:pointer;text-align:left;width:148px;height:32px;line-height:32px;text-indent:38px;margin:0 auto;text-decoration:none;color:#6899DA;text-shadow:2px 0px 0px #fff;outline:none;white-space:nowrap;position:relative;overflow:hidden;padding:10px 30px;background:url(http://mat1.gtimg.com/www/apps/images/more_icon2.png) no-repeat 45px 17px;-moz-background-size:18px 19px;-webkit-background-size:18px 19px;-o-background-size:18px 19px;-ms-background-size:18px 19px;background-size:18px 19px;background:url(http://mat1.gtimg.com/www/apps/images/more_icon3.png) no-repeat 15px 9px\9;*background:url(http://mat1.gtimg.com/www/apps/images/more_icon3.png) no-repeat 15px 9px;*line-height:16px;*padding:10px 30px 10px 0;line-height:16px\9;padding:10px 30px 10px 0\9;}
.client_new1_tips{display:block;text-decoration:none;text-align:center;background:url(http://mat1.gtimg.com/www/apps/images/newsdownload06.png) no-repeat 0 0;background-size:100% auto;height:59px;padding-top:7px;color:#000;width:280px;margin:0 auto;text-indent:59px;font-size:14px;}
-->
.preLoad{position:relative; display:block; margin:1em auto; overflow:hidden; border:1px solid #dedada; background:#ede9e6;}
.preLoad div.tip{ position:absolute; z-index:11; bottom:5px; right:7px; color:#B7B7B7; font-style:italic; font-size: 14px;
text-shadow: 1px 1px 1px rgba(255,255,255,.8); -webkit-text-shadow: 1px 1px 1px rgba(255,255,255,.8);}
.preLoad div.img{ position:absolute; z-index:12;}
</style>
</head>
<body>
<div id="content" class="main fontSize2">
<p class="title" align="left"><a href="qqbuy://article_9500?nm=NEW2012103000075117&chlid=news_news_top&type=0">打开我们的app</a></p>
<p class="title" align="left"><a href="qqnews://article_9527?nm=NEW2012103000075117&chlid=news_news_top&type=0">打开新闻app</a></p>
<div class="moreOperator">
<div class="share" id="share">
</div>
</div>
</div>
</body>
</html>

