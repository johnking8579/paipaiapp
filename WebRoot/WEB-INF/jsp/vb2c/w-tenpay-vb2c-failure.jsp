<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/include/include.jsp" %>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta name="author" content="tencent_winson" />
<%@ include file="/include/head.jsp"%>
<script type="text/javascript" ></script>
<title>充值中心</title>
<style type="text/css">
	<q:css name="pp_base,vb2c,pp_template" type="wap2"/>
</style>
</head>
  
<body>
<%@ include file="/include/logo.jsp" %> 

<div class="payretbox">
<div class="payfailBox">
<p><img class="pic" src="<%=wimgPath%>/icon1_warn.png"" alt="支付失败" />付款失败!</p>
<c:choose>
<c:when test="${attachPo.type eq 1}">
<div class="commonbar">
<p>手机号码:&nbsp;${attachPo.mobile }</p>
<p>话费面额:&nbsp;${attachPo.amount}元</p>
</div>
</c:when>
<c:when test="${attachPo.type eq 2}">
<div class="commonbar">
<p>充值的游戏:&nbsp;${attachPo.tGameName }</p>
<p>面额:&nbsp;${attachPo.account}</p>
<p>类型:&nbsp;${attachPo.chargeType}</p>
<p>分区:&nbsp;${attachPo.tSectionName }</p>
<p>服务器:&nbsp;${attachPo.tServerName }</p>
<p>游戏帐号:&nbsp;${attachPo.account }</p>
<p>数量:&nbsp;${attachPo.buyCount }</p>
</div>
</c:when>
<c:when test="${attachPo.type eq 3}">
<div class="commonbar">
<p>充值服务:&nbsp;${attachPo.qqServiceDisName }</p>
<p>充值数量:&nbsp;${attachPo.buyCount }</p>
</div>
</c:when>
</c:choose>
</div>
</div>

<div class="commonbox">
<p><a href="<%=basePath %>/w/vb2c/buyMobile.xhtml?sid=${sid}&amp;${baseParam}">返回充值中心</a></p>
</div>  
<div class="emptybar"></div>

<%@ include file="/include/foot.jsp" %>
</body>
</html>

