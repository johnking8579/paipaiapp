<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/include/include.jsp" %>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta name="author" content="tencent_winson" />
<%@ include file="/include/head.jsp"%>
<script type="text/javascript" ></script>
<title>充值中心</title>
<style type="text/css">
	<q:css name="pp_base,vb2c,pp_template" type="wap2"/>
</style>
</head>
  
<body>
<%@ include file="/include/logo.jsp" %> 
    
<!-- 导航区1 -->
<div class="tabbox">
<p class="tab">
<a href="<%=basePath%>/w/vb2c/buyMobile.xhtml?sid=${sid}&amp;${baseParam}" title="-话费">话费&nbsp;</a>
<a href="<%=basePath%>/w/vb2c/buyGame.xhtml?sid=${sid}&amp;${baseParam}" title="-网游">网游&nbsp;</a>
<span class="cur">QQ服务&nbsp;</span>
</p>
</div>

<div class="vb2cbox">
<c:if test="${fn:length(qqServices) gt 0}">
<div class="commonbar">
<p>
<c:forEach items="${qqServices}" var="qqService" varStatus="sQQService">
<c:choose><c:when test="${qqService.gameId eq 2623}"><a href="<%=basePath%>/w/vb2c/detailQQService.xhtml?gid=${qqService.gameId}&amount=30&amp;sid=${sid}&amp;${baseParam}">${qqService.name }<c:if test="${sQQService.last != true}">.&nbsp;</c:if></a></c:when>
<c:otherwise><a href="<%=basePath%>/w/vb2c/detailQQService.xhtml?gid=${qqService.gameId}&amount=${qqService.amount}&amp;sid=${sid}&amp;${baseParam}">${qqService.name }<c:if test="${sQQService.last != true}">.&nbsp;</c:if></a></c:otherwise></c:choose>
</c:forEach>
</p>
</div>
</c:if>
<div class="emptybar"></div>
</div>

<c:if test="${gcfa eq 19111 or gcfa eq 19112 or gcfa eq 19113}">
<div><q:advertpos advertPosId="8" basePath="<%=basePath%>" sid="${sid}" rid="${rid}" baseParam="${baseParam4ad}"/></div>
</c:if>

<div><q:advertpos advertPosId="9" basePath="<%=basePath%>" sid="${sid}" rid="${rid}" baseParam="${baseParam4ad}"/></div>

<div class="commonbox">
<p><strong class="red">9.3折</strong>起,&nbsp;1分钟内自动到账</p>
</div>

<div class="commonbox">
<p class="gray">支持财付通余额和手机网银支付.&nbsp;如果您已在电脑上注册了财付通,&nbsp;请用电脑登录www.tenpay.com绑定手机,&nbsp;并开通手机支付功能.&nbsp;</p>
</div>

<div class="commonbox">
<p><a href="<%=basePath%>/w/template.xhtml?tid=help_virtual&amp;sid=${sid}&amp;${baseParam}">充值帮助</a></p>
<hr/>
</div>

<div class="commonbox">
<p>
<a href="<%=basePath%>/w/template.xhtml?tid=index&amp;sid=${sid}&amp;${baseParam}">首页</a>&nbsp;&gt;&nbsp;充值中心
</p>
</div>
<div class="emptybar"></div>

<%@ include file="/include/foot.jsp" %>
</body>
</html>

