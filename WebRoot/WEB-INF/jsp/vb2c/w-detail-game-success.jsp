<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/include/include.jsp" %>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta name="author" content="tencent_winson" />
<%@ include file="/include/head.jsp"%>
<script type="text/javascript" ></script>
<title>充值中心</title>
<style type="text/css">
	<q:css name="pp_base,vb2c,pp_template" type="wap2"/>
</style>

</head>
  
<body>
<%@ include file="/include/logo.jsp" %> 
    
<!-- 导航区1 -->
<div class="tabbox">
<p class="tab">
<a href="<%=basePath%>/w/vb2c/buyMobile.xhtml?sid=${sid}&amp;${baseParam}" title="-话费">话费&nbsp;</a>
<span class="cur">网游&nbsp;</span>
<a href="<%=basePath%>/w/vb2c/buyQQService.xhtml?sid=${sid}&amp;${baseParam}" title="-QQ服务">QQ服务&nbsp;</a>
</p>
</div>

<div class="commonbox">
<c:if test="${result.errCode ne 0}">
<!-- 错误信息区域 -->
<div class="errbox">
<p class="errinfo"><img class="pic" alt="出现错误" src="<%=wimgPath%>/icon1_warn.png" />${result.msg}</p>
</div>

</c:if>
<c:if test="${i eq 1}">
<form id="detailGameForm1" method="get" action="<%=basePath%>/w/vb2c/detailGame.xhtml">
<p>充值的游戏:&nbsp;${gameDetailPo.gameInfo.currGame.itemName}</p>
<p>面额:&nbsp;<select class="gamecombo" name="amount"><c:forEach items="${gameDetailPo.options}" var="option" varStatus="sOption">
<c:choose><c:when test="${amount eq option.code}"><option value="${option.code}" selected="selected">${option.name}</option></c:when>
<c:otherwise><option value="${option.code}">${option.name}</option></c:otherwise></c:choose></c:forEach></select></p>
<p class="commonbar">
<%@ include file="/include/baseparam.jsp" %>
<input name="id" type="hidden" value="${id}" />
<input name="i" type="hidden" value="${gameDetailPo.nextIndex }" />
<input name="oi" type="hidden" value="${i}" />
<input class="formCommitBtn" type="submit" id="nextBtn" value="下一步" />
<c:choose><c:when test="${fn:length(cname) gt 0}"><a href="<%=basePath%>/w/vb2c/listGame.xhtml?cname=${cname}&amp;sid=${sid}&amp;${baseParam}">返回上一步</a></c:when>
<c:otherwise><a href="<%=basePath%>/w/vb2c/buyGame.xhtml?id=${id}&amp;sid=${sid}&amp;${baseParam}">返回上一步</a></c:otherwise></c:choose>
</p>
</form>
</c:if>

<c:if test="${i eq 2}">
<form id="detailGameForm2" method="get" action="<%=basePath%>/w/vb2c/detailGame.xhtml">
<p>充值的游戏:&nbsp;${gameDetailPo.gameInfo.currGame.itemName}</p>
<p>充值类型:&nbsp;<select class="gamecombo" name="ct"><c:forEach items="${gameDetailPo.options}" var="option" varStatus="sOption">
<c:choose><c:when test="${ct eq option.code}"><option value="${option.code}" selected="selected">${option.name}</option></c:when>
<c:otherwise><option value="${option.code}">${option.name}</option></c:otherwise></c:choose></c:forEach></select></p>
<p class="commonbar">
<input name="id" type="hidden" value="${id}" />
<input name="i" type="hidden" value="${gameDetailPo.nextIndex }" />
<input name="oi" type="hidden" value="${i}" />
<input name="amount" type="hidden" value="${amount}"/>
<%@ include file="/include/baseparam.jsp" %>
<input class="formCommitBtn" type="submit" id="nextBtn" value="下一步" />
<a href="<%=basePath%>/w/vb2c/detailGame.xhtml?id=${id}&amp;i=${gameDetailPo.preIndex}&amp;oi=${i}&amp;amount=${amount}&amp;sid=${sid}&amp;${baseParam}">返回上一步</a>
</p>
</form>
</c:if>

<c:if test="${i eq 3}">
<form id="detailGameForm3" method="get" action="<%=basePath%>/w/vb2c/detailGame.xhtml">
<p>充值的游戏:&nbsp;${gameDetailPo.gameInfo.currGame.itemName}</p>
<p>分区:&nbsp;<select class="gamecombo" name="sc"><c:forEach items="${gameDetailPo.options}" var="option" varStatus="sOption">
<c:choose><c:when test="${sc eq option.code}"><option value="${option.code}" selected="selected">${option.name}</option></c:when>
<c:otherwise><option value="${option.code}">${option.name}</option></c:otherwise></c:choose></c:forEach></select></p>
<p class="commonbar">
<input name="id" type="hidden" value="${id}" />
<input name="i" type="hidden" value="${gameDetailPo.nextIndex }" />
<input name="oi" type="hidden" value="${i}" />
<input name="amount" type="hidden" value="${amount}"/>
<input name="ct" type="hidden" value="${gameDetailPo.encodeChargeType}"/>
<%@ include file="/include/baseparam.jsp" %>
<input class="formCommitBtn" type="submit" id="nextBtn" value="下一步" />
<a href="<%=basePath%>/w/vb2c/detailGame.xhtml?id=${id}&amp;i=${gameDetailPo.preIndex}&amp;oi=${i }&amp;amount=${amount}&amp;ct=${gameDetailPo.encodeChargeType}&amp;sid=${sid}&amp;${baseParam}">返回上一步</a>
</p>
</form>
</c:if>

<c:if test="${i eq 4}">
<form id="detailGameForm4" method="get" action="<%=basePath%>/w/vb2c/detailGame.xhtml">
<p>充值的游戏:&nbsp;${gameDetailPo.gameInfo.currGame.itemName}</p>
<p>服务器:&nbsp;<select class="gamecombo" name="svc"><c:forEach items="${gameDetailPo.options}" var="option" varStatus="sOption">
<c:choose><c:when test="${svc eq option.code}"><option value="${option.code}" selected="selected">${option.name}</option></c:when>
<c:otherwise><option value="${option.code}">${option.name}</option></c:otherwise></c:choose></c:forEach></select></p>
<p class="commonbar">
<input name="id" type="hidden" value="${id}" />
<input name="i" type="hidden" value="${gameDetailPo.nextIndex }" />
<input name="oi" type="hidden" value="${i}" />
<input name="amount" type="hidden" value="${amount}"/>
<input name="ct" type="hidden" value="${gameDetailPo.encodeChargeType}"/>
<input name="sc" type="hidden" value="${sc}"/>
<%@ include file="/include/baseparam.jsp" %>
<input class="formCommitBtn" type="submit" id="nextBtn" value="下一步" />
<a href="<%=basePath%>/w/vb2c/detailGame.xhtml?id=${id}&amp;i=${gameDetailPo.preIndex}&amp;oi=${i }&amp;amount=${amount}&amp;ct=${gameDetailPo.encodeChargeType}&amp;sc=${sc}&amp;sid=${sid}&amp;${baseParam}">返回上一步</a>
</p>
</form>
</c:if>

<c:if test="${i eq 5}">
<form id="detailGameForm5" method="get" action="<%=basePath%>/w/vb2c/detailGame.xhtml">
<p>充值的游戏:&nbsp;${gameDetailPo.gameInfo.currGame.itemName}</p>
<p>数量:&nbsp;<select class="gamecombo" name="bc"><c:forEach items="${gameDetailPo.options}" var="option" varStatus="sOption">
<c:choose><c:when test="${bc eq option.code}"><option value="${option.code}" selected="selected">${option.name}</option></c:when>
<c:otherwise><option value="${option.code}">${option.name}</option></c:otherwise></c:choose></c:forEach></select></p>
<c:if test="${id eq 2220}">
<p>战网通行证:&nbsp;</p>
<p><input class="gamecombo" name="wp" type="text" value="${wp }"/></p>
<p>确认通行证:&nbsp;</p>
<p><input class="gamecombo" name="wp1" type="text" value="${wp1 }"/></p>
</c:if>
<p>游戏帐号:&nbsp;</p>
<p><input class="gamecombo" name="ac" type="text" value="${ac}" /></p>
<p>确认游戏帐号:&nbsp;</p>
<p><input class="gamecombo" name="ac1" type="text" value="${ac1}" /></p>
<p class="commonbar">
<input name="id" type="hidden" value="${id}" />
<input name="i" type="hidden" value="${gameDetailPo.nextIndex }" />
<input name="oi" type="hidden" value="${i}" />
<input name="amount" type="hidden" value="${amount}"/>
<input name="ct" type="hidden" value="${gameDetailPo.encodeChargeType}"/>
<input name="sc" type="hidden" value="${sc}"/>
<input name="svc" type="hidden" value="${svc}" />
<%@ include file="/include/baseparam.jsp" %>
<input class="formCommitBtn" type="submit" id="nextBtn" value="下一步" />
<c:choose><c:when test="${fn:length(sc) gt 0}"><a href="<%=basePath%>/w/vb2c/detailGame.xhtml?id=${id}&amp;i=${gameDetailPo.preIndex}&amp;oi=${i }&amp;amount=${amount}&amp;ct=${gameDetailPo.encodeChargeType}&amp;sc=${sc}&amp;svc=${svc}&amp;sid=${sid}&amp;${baseParam}">返回上一步</a></c:when>
<c:otherwise><a href="<%=basePath%>/w/vb2c/detailGame.xhtml?id=${id}&amp;i=${gameDetailPo.preIndex}&amp;oi=${i }&amp;amount=${amount}&amp;ct=${gameDetailPo.encodeChargeType}&amp;sid=${sid}&amp;${baseParam}">返回上一步</a></c:otherwise></c:choose>
</p>
</form>
</c:if>

<c:if test="${i eq 6}">
<form id="detailGameForm6" method="get" action="<%=basePath%>/w/vb2c/detailGame.xhtml">
<p>充值的游戏:&nbsp;${gameDetailPo.gameInfo.currGame.itemName}</p>
<p>面额:&nbsp;${amount}元</p>
<p>类型:&nbsp;${ct}</p>
<c:if test="${fn:length(gameDetailPo.gameInfo.currSectionName) gt 0}"><p>分区:&nbsp;${gameDetailPo.gameInfo.currSectionName}</p></c:if>
<c:if test="${fn:length(gameDetailPo.gameInfo.currServerName) gt 0}"><p>服务器:&nbsp;${gameDetailPo.gameInfo.currServerName}</p></c:if>
<c:if test="${id eq 2220}">
<p>战网通行证:&nbsp;${wp }</p>
<p><input name="wp" type="hidden" value="${gameDetailPo.encodewp}"/>
<input name="wp1" type="hidden" value="${gameDetailPo.encodewp1}"/>
</p>
</c:if>
<p>游戏帐号:&nbsp;${ac}</p>
<p>数量:&nbsp;${bc}</p>
<p>价格:&nbsp;<span class="orangeprice">￥${gameDetailPo.totalFee}</span></p>
<p class="commonbar">
<input name="id" type="hidden" value="${id}" />
<input name="i" type="hidden" value="${gameDetailPo.nextIndex }" />
<input name="oi" type="hidden" value="${i}" />
<input name="amount" type="hidden" value="${amount}"/>
<input name="ct" type="hidden" value="${gameDetailPo.encodeChargeType}"/>
<input name="sc" type="hidden" value="${sc}"/>
<input name="svc" type="hidden" value="${svc}" />
<input name="bc" type="hidden" value="${bc}" />
<input name="ac" type="hidden" value="${gameDetailPo.encodeAccount}" />
<input name="ac1" type="hidden" value="${gameDetailPo.encodeConfirmAccount}" />
<%@ include file="/include/baseparam.jsp" %>
<c:choose><c:when test="${fn:length(gcfa) gt 0}"><input name="s" type="hidden" value="4_10_103_${gcfa }_2" /></c:when><c:otherwise><input name="s" type="hidden" value="4_10_103_0_2" /></c:otherwise></c:choose>
<input class="formCommitBtn" type="submit" id="nextBtn" value="立即充值" />
<a href="<%=basePath%>/w/vb2c/detailGame.xhtml?id=${id}&amp;i=${gameDetailPo.preIndex}&amp;oi=${i }&amp;amount=${amount}&amp;ct=${gameDetailPo.encodeChargeType}&amp;sc=${sc}&amp;svc=${svc}&amp;bc=${bc}&amp;ac=${gameDetailPo.encodeAccount}&amp;ac1=${gameDetailPo.encodeConfirmAccount}&amp;wp=${gameDetailPo.encodewp}&amp;wp1=${gameDetailPo.encodewp1}&amp;sid=${sid}&amp;${baseParam}">返回上一步</a>
</p>
</form>
</c:if>
</div>

<div class="commonbox">
<p><strong class="red">最便宜</strong>的网游充值,&nbsp;1-5分钟到账</p>
</div>

<div class="commonbox">
<p><a href="<%=basePath%>/w/template.xhtml?tid=help_games&amp;sid=${sid}&amp;${baseParam}">网游充值帮助</a></p>
<hr/>
</div>

<div class="commonbox">
<p>
<a href="<%=basePath%>/w/template.xhtml?tid=index&amp;sid=${sid}&amp;${baseParam}">首页</a>&nbsp;&gt;&nbsp;充值中心
</p>
</div>
<div class="emptybar"></div>

<%@ include file="/include/foot.jsp" %>
</body>
</html>

