<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/include/include.jsp" %>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta name="author" content="tencent_winson" />
<%@ include file="/include/head.jsp"%>
<script type="text/javascript" ></script>
<title>充值中心</title>
<style type="text/css">
<q:css name="pp_base,vb2c,pp_template" type="wap2"/>
</style>

</head>
  
<body>
<%@ include file="/include/logo.jsp" %> 
    
<!-- 导航区1 -->
<div class="tabbox">
<p class="tab">
<a href="<%=basePath%>/w/vb2c/buyMobile.xhtml?sid=${sid}&amp;${baseParam}" title="-话费">话费&nbsp;</a>
<span class="cur">网游&nbsp;</span>
<a href="<%=basePath%>/w/vb2c/buyQQService.xhtml?sid=${sid}&amp;${baseParam}" title="-QQ服务">QQ服务&nbsp;</a>
</p>
</div>

<div class="vb2cbox">
<c:if test="${fn:length(hotgames) gt 0}">
<div class="commonbar">
<p>热门游戏:&nbsp;</p>
<p>
<c:forEach items="${hotgames}" var="hotgame" varStatus="sHotgame">
<a href="<%=basePath%>/w/vb2c/detailGame.xhtml?id=${hotgame.id}&amp;sid=${sid}&amp;${baseParam}">${hotgame.itemName }&nbsp;</a>
</c:forEach>
</p>
</div>
</c:if>
<div class="commonbar">
<p>按照拼音首字母选择:&nbsp;</p>
<p>
<c:forEach items="${categories}" var="category" varStatus="sCategory">
<a href="<%=basePath%>/w/vb2c/listGame.xhtml?cname=${category}&amp;sid=${sid}&amp;${baseParam}">${category}<c:if test="${sCategory.last != true}">.&nbsp;</c:if></a>
</c:forEach>
</p>
</div>
<div class="emptybar"></div>
</div>

<c:if test="${gcfa eq 19111 or gcfa eq 19112 or gcfa eq 19113}">
<div><q:advertpos advertPosId="7" basePath="<%=basePath%>" sid="${sid}" rid="${rid}" baseParam="${baseParam4ad}"/></div>
</c:if>

<div><q:advertpos advertPosId="9" basePath="<%=basePath%>" sid="${sid}" rid="${rid}" baseParam="${baseParam4ad}"/></div>

<div class="commonbox">
<p><strong class="red">最便宜</strong>的网游充值,&nbsp;1-5分钟到账</p>
</div>

<div class="commonbox">
<p class="gray">支持财付通余额和手机网银支付.&nbsp;如果您已在电脑上注册了财付通,&nbsp;请用电脑登录www.tenpay.com绑定手机,&nbsp;并开通手机支付功能.&nbsp;</p>
</div>

<div class="commonbox">
<p><a href="<%=basePath%>/w/template.xhtml?tid=help_games&amp;sid=${sid}&amp;${baseParam}">网游充值帮助</a></p>
<hr/>
</div>

<div class="commonbox">
<p>
<a href="<%=basePath%>/w/template.xhtml?tid=index&amp;sid=${sid}&amp;${baseParam}">首页</a>&nbsp;&gt;&nbsp;充值中心
</p>
</div>
<div class="emptybar"></div>

<%@ include file="/include/foot.jsp" %>
</body>
</html>

