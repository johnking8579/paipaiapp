package com.qq.ptlogin2;

public class WtloginApi {
    public native int Init();
    public native int VerifySig(int appid, long uin, int userIP, byte[] sig, String serverAddr, int timeout);
    public native int GetSigKey(int appid, long uin, int userIP, byte[] sig, String serverAddr, int timeout, int reqKeyType);

    public native int GetReqPkg(int appid,long uin, int UserIP, int cmd, byte[] reqKey, int reqKeyType, int rspKeyType);
    public native int ParseRspPkg(byte[] rspPacket);

    public static byte[] hex2byte(String str) { 
        if (str == null)
            return null;
        str = str.trim();
        int len = str.length();
        if (len == 0 || len % 2 == 1)
            return null;
        byte[] b = new byte[len / 2];
        try {
            for (int i = 0; i < str.length(); i += 2) {
                b[i / 2] = (byte) Integer.decode("0x" + str.substring(i, i + 2)).intValue();
            }
            return b;
        } catch (Exception e) {
            return null;
        }
    }


    int get_verify_result()
    {
        return verifyResult;
    }

    byte[] get_GTKey()
    {
        return GTKey;
    }

    byte[] get_VKey()
    {
        return VKey;
    }

    String get_errmsg()
    {
        return errMsg;
    }

    protected int verifyResult;
    protected byte[] GTKey;
    protected byte[] VKey;
    protected byte[] reqPacket;
    protected String errMsg;
}


