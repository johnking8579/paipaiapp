package com.qq.ptlogin2;

import com.qq.qqbuy.common.Log;

public class CmlbAccess {
    public native int Init(int appid, boolean is_get_conf_sync);
    public native String GetSvrAddr();

    private CmlbAccess(){}
    
    private static CmlbAccess cmlbAccess=null;
    
    public static synchronized  CmlbAccess  getCmlbAccess(int cmlbId){
    	if(cmlbAccess ==null){
    		cmlbAccess=new CmlbAccess();
    		int ret=cmlbAccess.Init(cmlbId,false);
    		if (ret != 0)
			{
				Log.run.error("CMLB init failed: "+ret);
			    return null;
			}
    	}
    	return cmlbAccess;
    }
     
    public static void main(String[] args) {
        CmlbAccess sample = new CmlbAccess();
        int ret = sample.Init(5258, false);
        System.out.println( ret );
        System.out.println( sample.GetSvrAddr() );
    }

    protected byte[] cmlbobj;
}
