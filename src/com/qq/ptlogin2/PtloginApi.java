package com.qq.ptlogin2;

import com.qq.qqbuy.common.Log;

public class PtloginApi {
    //初始化, 成功时返回0， 失败时返回非0
    public native int Init();
    
    /**  SessVerify 函数功能： 验证 skey 或 p_skey
    输入参数：
    appid        必填   业务的appid, 请务必正确填写
    uin          必填   用户的QQ号码
    skey         必填   要验证的skey 或 p_skey
    userip       必填   用户的IP，请务必正确填写
    domain_id    必填   对于skey 验证请求，请填写为0
                        对于p_skey验证请求，请填写隔离域的ID， 必须正确填写 
    verify_flag  必填   控制skey验证的标识, 需要设置某些位
                        (1)只验证不续期:   设置第2位
                        (2)可获取登录时间: 设置第3位
                        (3)可获取登录IP:   设置第5位
    serverAddr   必填   Ptlogin的后台服务地址，如"127.0.0.1:18890"格式， 请使用CmlbAccess类获取
    timeout      必填   请求超时
    输出：
    On Success:   返回0, 表示请求后台成功。调用get_result()函数可以查看 skey/p_skey验证是否通过
                  0 == get_result()   验证没通过，具体错误原因可调用get_errmsg()查看
                  1 == get_result()   验证通过了！！！
    On Failure:   返回-1, 表示请求后台失败（可能是后台超时，参数错误等原因），具体错误原因可调用get_errmsg()查看
    */
    public native int SessVerify(int appid, long uin, String skey, int userIP, int domainId, long verify_flag, String serverAddr, int timeout);
    
    /**  SessVerifyLowLogin 函数功能： 验证 lskey 或 p_lskey
    输入参数：
    appid        必填   业务的appid, 请务必正确填写
    uin          必填   用户的QQ号码
    skey         必填   要验证的lskey 或 p_lskey
    userip       必填   用户的IP，请务必正确填写
    domain_id    必填   对于lskey 验证请求，请填写为0
                        对于p_lskey验证请求，请填写隔离域的ID， 必须正确填写 
    verify_flag  必填   控制skey验证的标识, 需要设置某些位
                        (1)只验证不续期:   设置第2位
                        (2)可获取登录时间: 设置第3位
                        (3)可获取登录IP:   设置第5位
    serverAddr   必填   Ptlogin的后台服务地址，如"127.0.0.1:18890"格式， 请使用CmlbAccess类获取
    timeout      必填   请求超时
    输出：
    On Success:   返回0, 表示请求后台成功。调用get_result()函数可以查看 lskey/p_lskey验证是否通过
                  0 == get_result()   验证没通过，具体错误原因可调用get_errmsg()查看
                  1 == get_result()   验证通过了！！！
    On Failure:   返回-1, 表示请求后台失败（可能是后台超时，参数错误等原因），具体错误原因可调用get_errmsg()查看
    */
    public native int SessVerifyLowLogin(int appid, long uin, String lskey, int userIP, int domainId, long flag, String serverAddr, int timeout);

    private PtloginApi(){}
    
    private static PtloginApi ptlogin=null;
    
    public static synchronized  PtloginApi  getPtloginApi(){
    	if(ptlogin ==null){
    		ptlogin=new PtloginApi();
    		int ret=ptlogin.Init();
    		if (ret != 0)
			{
				Log.run.error("Api init failed:"+ ret);
			    return null;
			}
    	}
    	return ptlogin;
    }
    public  int get_result()
    {
        return result;
    }

   public String get_errmsg()
    {
        return errMsg;
    }

    protected int result;
    protected byte[] reqPacket;
    protected String errMsg;
}


