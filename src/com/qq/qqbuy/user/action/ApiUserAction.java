package com.qq.qqbuy.user.action;

import java.util.Vector;

import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.action.PaipaiApiBaseAction;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.user.biz.UserBiz;
import com.qq.qqbuy.user.po.ColorDiamondInfo;
import com.qq.qqbuy.user.po.GetUserTagsRespPo;
import com.qq.qqbuy.user.po.SetUserTagRespPo;

public class ApiUserAction extends PaipaiApiBaseAction {

	private ColorDiamondInfo colorDiamondInfo;
	private String jsonStr;
	private String tagIds;

	private UserBiz biz;
	private int level = 0;

	public String getUserInfo() {
		long qq = 0;
		try {
			if (checkLogin()) {
				qq = getQq();
				if (qq > 1000) {
					colorDiamondInfo = biz.getColorDiamondLevel(qq);
					level = biz.getSuperQQLevel(qq);
				}
			}
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg4BExp(e, "QQ=" + qq);
			return RST_API_FAILURE;
		} catch (Throwable e) {
			setErrcodeAndMsg4Throwable(e, "QQ=" + qq);
			return RST_API_FAILURE;
		} finally {
			result.setDtag(dtag);
		}
	}
	
	
	/**
	 * 设置兴趣标签
	 * @param  tagIds  以,分割的标签id
	 * @author lijianji
	 * @return jsonStr
	 * */
	public String setUserTag(){
		jsonStr = "{}";
		
		if (checkLoginAndGetSkey()) {
			if(!StringUtil.isBlank(tagIds)){
				JsonConfig jsoncfg = new JsonConfig();
				String[] excludes = { "size", "empty" };
				jsoncfg.setExcludes(excludes);
				String[] ids=tagIds.split(",");
				
				Vector<String> _tagids=new Vector<String>();
				for(String id:ids){
					_tagids.add(id);
				}
				
				SetUserTagRespPo ret=biz.setUserTag(getWid(),getMk(),_tagids);
				
				if (ret.errCode == 0)
					jsonStr = JSONSerializer.toJSON(ret, jsoncfg)
							.toString();
				else 
					setErrCodeAndMsg((int) ret.errCode, ret.msg);
			}else{
				
				setErrCodeAndMsg(ErrConstant.ERRCODE_INVALID_PARAMETER,
                        "参数校验不合法");
			}
			
        }else{
        	setErrCodeAndMsg(ErrConstant.ERRCODE_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
        }
		return SUCCESS;
	}
	
	/**
	 * 获取兴趣标签
	 * */
	public String getUserTag(){
		jsonStr = "{}";
		
		if (checkLoginAndGetSkey()) {
			JsonConfig jsoncfg = new JsonConfig();
			String[] excludes = { "size", "empty" };
			jsoncfg.setExcludes(excludes);
			GetUserTagsRespPo ret=null;
			try {
				ret=biz.getUserTag(getWid(),getMk());
			} catch (Exception e) {
				Log.run.debug("ApiInterestTagsAction#getUserTag Exception:"+e.getMessage());
				e.printStackTrace();
			}
			
			
			if (ret.errCode == 0)
				jsonStr = JSONSerializer.toJSON(ret, jsoncfg)
						.toString();
			else 
				setErrCodeAndMsg((int) ret.errCode, ret.msg);
        }else{
        	setErrCodeAndMsg(ErrConstant.ERRCODE_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
        }
		return SUCCESS;
	}
	
	
	
	
	/**
	 * 测试服务器迁移到京东  标签的服务
	 * @return
	 */
	public String getUserTagTest(){
		jsonStr = "{}";
		
		if (checkLoginAndGetSkey()) {
			JsonConfig jsoncfg = new JsonConfig();
			String[] excludes = { "size", "empty" };
			jsoncfg.setExcludes(excludes);
			GetUserTagsRespPo ret=null;
			try {
				Log.run.info(getWid());
				ret=biz.getUserTag(getWid(),getMk());
			} catch (Exception e) {
				Log.run.error(e.getMessage(), e);
			}
			
			Log.run.info(ret.errCode);
			if (ret.errCode == 0)
				jsonStr = JSONSerializer.toJSON(ret, jsoncfg)
						.toString();
			else 
				setErrCodeAndMsg((int) ret.errCode, ret.msg);
			Log.run.info(jsonStr);
        }else{
        	setErrCodeAndMsg(ErrConstant.ERRCODE_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
        	Log.run.error("鉴权失败");
        }
		return SUCCESS;
	}
	
	
	
	
	
	
	

	public String getJsonStr() {
		return jsonStr;
	}


	public void setJsonStr(String jsonStr) {
		this.jsonStr = jsonStr;
	}


	public String getTagIds() {
		return tagIds;
	}


	public void setTagIds(String tagIds) {
		this.tagIds = tagIds;
	}

	public ColorDiamondInfo getColorDiamondInfo() {
		return colorDiamondInfo;
	}

	public void setColorDiamondInfo(ColorDiamondInfo colorDiamondInfo) {
		this.colorDiamondInfo = colorDiamondInfo;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public UserBiz getBiz() {
		return biz;
	}

	public void setBiz(UserBiz biz) {
		this.biz = biz;
	}

}
