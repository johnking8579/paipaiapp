/**   
 * @Title: UserBiz.java 
 * @Package com.qq.qqbuy.user.biz 
 * @Description: TODO
 * @author Wendyhu wendyhu@tencent.com  
 * @date 2012-5-30 下午7:36:53 
 * @version V1.0   
 */
package com.qq.qqbuy.user.biz;

import java.util.Vector;

import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.user.po.ColorDiamondInfo;
import com.qq.qqbuy.user.po.GetUserTagsRespPo;
import com.qq.qqbuy.user.po.SetUserTagRespPo;
import com.qq.qqbuy.user.po.User;


/** 
 * @ClassName: UserBiz 
 * @Description: 用户接口
 * @author wendyhu wendyhu@tencent.com
 * @date 2012-5-30 下午7:36:53 
 *  
 */
public interface UserBiz
{
    
    /**
     * 
     * @Title: snapUp 
     * @Description: 获取超Q等级
     * @param uin  qq号码
     * @return  
     *        int    返回超Q等级 ，0 代表没有
     *
     * @throws
     */
    public int getSuperQQLevel(long uin) throws BusinessException;
    /**
     * 
     * @Title: snapUp 
     * @Description: 获取彩钻等级
     * @param uin  qq号码
     * @return  
     *        int    返回超Q等级 ，0 代表没有
     *
     * @throws
     */
    public ColorDiamondInfo getColorDiamondLevel(long uin) throws BusinessException;
    
    /**
     * 调用openAPI获取用户详细信息
     * @param uin QQ
     * @return
     */
//    GetUserDetailInfoResponse getUserDetail(long uin)throws  BusinessException;
	User getUserDetailInfo(long uin) throws BusinessException;
	
	/**
	 * 设置用户兴趣标签
	 * 
	 * */
		SetUserTagRespPo setUserTag(Long wid,String mk,Vector<String> tagIds);
		
	/**
	 * 获取用户兴趣标签
	 * */	
		GetUserTagsRespPo getUserTag(Long wid,String mk);

}
