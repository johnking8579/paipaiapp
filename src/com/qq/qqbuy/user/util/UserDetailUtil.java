package com.qq.qqbuy.user.util;

import com.paipai.util.string.StringUtil;
import com.qq.qqbuy.user.po.User;

public class UserDetailUtil {
	/**
	 * 获取卖家的售前电话
	 * @param userDetail
	 * @return
	 */
//	public static String getSellerTelephone(GetUserDetailInfoResponse userDetail ) {		
//		String telephone = "";
//		try {
//			if (userDetail.mobileBefore != null
//					&& userDetail.mobileBefore.length() > 0) {
//				telephone = userDetail.mobileBefore;
//			} else if (userDetail.telephoneBefore != null
//					&& userDetail.telephoneBefore.length() > 0) {
//				telephone = userDetail.telephoneBefore;
//			}
//		} catch (Exception e) {
//			telephone = "";
//		}
//
//		return telephone;
//	}
	/**
	 * 获取卖家的售前电话
	 * @param userDetail
	 * @return
	 */
	public static String getSellerTelephone(User userDetail ) {		
		String telephone = "";
		try {
			if (StringUtil.isNotEmpty(userDetail.getTelephoneBefore())) {
				telephone = userDetail.getTelephoneBefore();
			} else if (StringUtil.isNotEmpty(userDetail.getTelephoneAfter())) {
				telephone = userDetail.getTelephoneAfter();
			}
		} catch (Exception e) {
			telephone = "";
		}

		return telephone;
	}
	
}
