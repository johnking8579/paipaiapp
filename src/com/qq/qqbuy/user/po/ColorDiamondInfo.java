package com.qq.qqbuy.user.po;

public class ColorDiamondInfo {

	/**
	 * 彩钻等级
	 */
	private int colorDiamondLevel;
	
	/**
	 * 彩钻状态，0:彩钻熄灭状态;1:彩钻点亮状态;2:彩钻未激活，即非彩钻用户态
	 */
	private int colorDiamondState;

	
	public int getColorDiamondLevel() {
		return colorDiamondLevel;
	}

	public void setColorDiamondLevel(int colorDiamondLevel) {
		this.colorDiamondLevel = colorDiamondLevel;
	}

	public int getColorDiamondState() {
		return colorDiamondState;
	}

	public void setColorDiamondState(int colorDiamondState) {
		this.colorDiamondState = colorDiamondState;
	}
	
}
