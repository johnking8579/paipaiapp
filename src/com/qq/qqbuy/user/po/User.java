package com.qq.qqbuy.user.po;

/**
 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
 */
public class User {
	private long uin;
	private String nickName;
	private short sex;
	private String email;
	private String telephone;
	private String mobileBefore;
	private String mobileAfter;
	private String telephoneBefore;
	private String telephoneAfter;
	private long sellerCredit;
	private long buyerCredit;
	private long loginLevel;
	private long authMask;
	private String properties;
	private int city;
	private String sproperties;

	// TODO matrixshi 店铺类型，0：拍拍；1：网购
	private int shopType;

	public long getUin() {
		return uin;
	}

	public void setUin(long uin) {
		this.uin = uin;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public short getSex() {
		return sex;
	}

	public void setSex(short sex) {
		this.sex = sex;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getMobileBefore() {
		return mobileBefore;
	}

	public void setMobileBefore(String mobileBefore) {
		this.mobileBefore = mobileBefore;
	}

	public String getMobileAfter() {
		return mobileAfter;
	}

	public void setMobileAfter(String mobileAfter) {
		this.mobileAfter = mobileAfter;
	}

	public String getTelephoneBefore() {
		return telephoneBefore;
	}

	public void setTelephoneBefore(String telephoneBefore) {
		this.telephoneBefore = telephoneBefore;
	}

	public String getTelephoneAfter() {
		return telephoneAfter;
	}

	public void setTelephoneAfter(String telephoneAfter) {
		this.telephoneAfter = telephoneAfter;
	}

	public long getSellerCredit() {
		return sellerCredit;
	}

	public void setSellerCredit(long sellerCredit) {
		this.sellerCredit = sellerCredit;
	}

	public long getBuyerCredit() {
		return buyerCredit;
	}

	public void setBuyerCredit(long buyerCredit) {
		this.buyerCredit = buyerCredit;
	}

	public long getLoginLevel() {
		return loginLevel;
	}

	public void setLoginLevel(long loginLevel) {
		this.loginLevel = loginLevel;
	}

	public long getAuthMask() {
		return authMask;
	}

	public void setAuthMask(long authMask) {
		this.authMask = authMask;
	}

	public String getProperties() {
		return properties;
	}

	public void setProperties(String properties) {
		this.properties = properties;
	}

	public boolean isGuaranteeCompensation() {
		return sproperties != null ? "1".equals("" + sproperties.charAt(8 + 28)) : false;
	}

	public int getCity() {
		return city;
	}

	public void setCity(int city) {
		this.city = city;
	}

	public String getSproperties() {
		return sproperties;
	}

	public void setSproperties(String sproperties) {
		this.sproperties = sproperties;
	}

	public int getShopType() {
		return shopType;
	}

	public void setShopType(int shopType) {
		this.shopType = shopType;
	}

}
