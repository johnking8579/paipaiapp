package com.qq.qqbuy.user.po;

import java.util.Vector;

import com.paipai.util.io.ByteStream;
import com.qq.qqbuy.common.po.CommonPo;
import com.qq.qqbuy.thirdparty.idl.interestTags.protocol.MyPaipaiExt;
import com.qq.qqbuy.thirdparty.idl.interestTags.protocol.MyPaipaiTag;

public class GetUserTagsRespPo extends CommonPo {


	/**
	 * 标签信息
	 *
	 * 版本 >= 0
	 */
	 private Vector<Long> tagIds = new Vector<Long>();

	public Vector<Long> getTagIds() {
		return tagIds;
	}

	public void setTagIds(Long tagId) {
		this.tagIds.add(tagId);
	}



	
}
