package com.qq.qqbuy.recvaddr.action;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.paipai.util.string.StringUtil;
import com.qq.qqbuy.common.action.PaipaiApiBaseAction;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.AntiXssHelper;
import com.qq.qqbuy.common.util.CoderUtil;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.common.util.region.City;
import com.qq.qqbuy.common.util.region.Province;
import com.qq.qqbuy.common.util.region.RegionManager;
import com.qq.qqbuy.common.util.region.RegionManagerV2;
import com.qq.qqbuy.recvaddr.biz.RecvAddrBiz;
import com.qq.qqbuy.recvaddr.po.ReceiverAddress;
import com.qq.qqbuy.recvaddr.po.RecvAddr;

public class ApiRecvAddrAction extends PaipaiApiBaseAction
{

    /**
     * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)
     */
    private static final long serialVersionUID = 1L;
    // 新增/修改收货地址时
    private int pvid = -1; // 省ID
    private int ctid = -1; // 城市ID
    private long addrId = -1; // 地址ID
    private long regionId = 0; // 区ID
    private String name; // 姓名
    private String address; // 地址
    private String mobile; // 手机
    private String phone; // 电话
    private String postcode; // 邮编
    private String callback ;

    private RecvAddrBiz recvAddrBiz;

    private String datastr;

    // 新建收获地址时候的省信息
    private Province province = new Province();

    // 新建收货地址的选择的城市信息
    private City city = new City();
    
    //	接口版本号
    private int ver = 2;
    
    //	客户端缓存的收货地址版本号
    private int rver = 0;
    
    //	所有收货地址信息，在list接口有参数rver且< 2时返回
    private String regionStr;
    

    public String list()
    {
        long start = System.currentTimeMillis();
        try
        {
        	if (!StringUtils.isEmpty(callback)) {
        		callback = CoderUtil.decodeXssFilter(AntiXssHelper.htmlAttributeEncode(callback));
        	}
            // 鉴权
            boolean checkResult = this.checkLoginAndGetSkey();
            if (!checkResult)
            {
                throw BusinessException
                        .createInstance(BusinessErrorType.NOT_LOGIN);
            }

            RecvAddr recvAddrs = recvAddrBiz
                    .listRecvAddr(getWid(), false, false, ver, getSk());
            
            List<ReceiverAddress> addressList = recvAddrs.getAddressList();
            for (ReceiverAddress receiverAddress : addressList) {
				String MobileNo = receiverAddress.getMobile();
				if(StringUtil.isNotBlank(MobileNo) && MobileNo.trim().length() == 11){
					receiverAddress.setMobile(StringUtil.substring(MobileNo, 0, 3) + "****" + StringUtil.substring(MobileNo, 7, 11));
				}
			}
           /* if(rver != 0 && rver < 2)
            //	client有版本小于2的缓存区域编码，需要下发最新的区域编码信息
            {
            	//	当地址编码有更新时启用
            	// 	ArrayList<Province> allProvinces = RegionManagerV2.getProvinceList();
            	// 	regionStr = RegionFormatter.toString(2, allProvinces);
            }*/
            
            if (recvAddrs != null)
            {
                datastr = obj2Json(recvAddrs);
            }
            return RST_SUCCESS;
        } catch (BusinessException e)
        {
            setErrCodeAndMsg4BExp(e, "wid=" + getWid() + ",ver="+ver);
            //	修改返回msg，用于客户端显示
            setResultMsg(e);
            return RST_API_FAILURE;
        } catch (Throwable e)
        {
            setErrcodeAndMsg4Throwable(e, "wid=" + getWid() + ",ver="+ver);
            //	修改返回msg，用于客户端显示
            setResultMsg(e);
            return RST_API_FAILURE;
        }  
    }
    
    public String listV2()
    {
    	
        try
        {   
        	if (!StringUtils.isEmpty(callback)) {
        		callback = CoderUtil.decodeXssFilter(AntiXssHelper.htmlAttributeEncode(callback));
        	}
        	// 鉴权
            /*boolean checkResult = this.checkLoginAndGetSkey();
            if (!checkResult)
            {
                throw BusinessException
                        .createInstance(BusinessErrorType.NOT_LOGIN);
            }

            RecvAddr recvAddrs = recvAddrBiz
                    .listRecvAddrV2(getWid(), false, false, ver, getSk());
            
            List<ReceiverAddress> addressList = recvAddrs.getAddressList();
            for (ReceiverAddress receiverAddress : addressList) {
				String MobileNo = receiverAddress.getMobile();
				if(StringUtil.isNotBlank(MobileNo) && MobileNo.trim().length() == 11){
					receiverAddress.setMobile(StringUtil.substring(MobileNo, 0, 3) + "****" + StringUtil.substring(MobileNo, 7, 11));
				}
			}*/
        	RecvAddr recvAddrs = new RecvAddr();
            if (recvAddrs != null)
            {
                datastr = obj2Json(recvAddrs);
            }
            return RST_SUCCESS;
        } catch (BusinessException e)
        {
            setErrCodeAndMsg4BExp(e, "wid=" + getWid() + ",ver="+ver);
            //	修改返回msg，用于客户端显示
            setResultMsg(e);
            return RST_API_FAILURE;
        } catch (Throwable e)
        {
            setErrcodeAndMsg4Throwable(e, "wid=" + getWid() + ",ver="+ver);
            //	修改返回msg，用于客户端显示
            setResultMsg(e);
            return RST_API_FAILURE;
        }  
    }
    
    public String modifyDefaultRecvAddr()
    {
        try
        {   
        	if (!StringUtils.isEmpty(callback)) {
        		callback = CoderUtil.decodeXssFilter(AntiXssHelper.htmlAttributeEncode(callback));
        	}
        	// 鉴权
            /*boolean checkResult = this.checkLoginAndGetSkey();
            if (!checkResult)
            {
                throw BusinessException
                        .createInstance(BusinessErrorType.NOT_LOGIN);
            }

            RecvAddr recvTest = recvAddrBiz
                    .modifyDefaultRecvAddr(getWid(), getSk(), addrId);
            
            RecvAddr recvAddrs = recvAddrBiz
                    .listRecvAddrV2(getWid(), false, false, ver, getSk());
            
            List<ReceiverAddress> addressList = recvAddrs.getAddressList();
            for (ReceiverAddress receiverAddress : addressList) {
				String MobileNo = receiverAddress.getMobile();
				if(StringUtil.isNotBlank(MobileNo) && MobileNo.trim().length() == 11){
					receiverAddress.setMobile(StringUtil.substring(MobileNo, 0, 3) + "****" + StringUtil.substring(MobileNo, 7, 11));
				}
			}*/
        	RecvAddr recvAddrs = new RecvAddr();
            if (recvAddrs != null)
            {
                datastr = obj2Json(recvAddrs);
            }
            return RST_SUCCESS;
        } catch (BusinessException e)
        {
            setErrCodeAndMsg4BExp(e, "wid=" + getWid() + ",ver="+ver);
            //	修改返回msg，用于客户端显示
            setResultMsg(e);
            return RST_API_FAILURE;
        } catch (Throwable e)
        {
            setErrcodeAndMsg4Throwable(e, "wid=" + getWid() + ",ver="+ver);
            //	修改返回msg，用于客户端显示
            setResultMsg(e);
            return RST_API_FAILURE;
        }  
    }
    
    /**
     * 设置一个友好的错误提示信息
     * @param e
     */
    private void setResultMsg(BusinessException e){
        this.result.setMsg(e.getErrMsg());
    }
    
    /**
     * 设置一个有好的错误提示信息
     * @param e
     */
    private void setResultMsg(Throwable e){
        this.result.setMsg("抱歉，操作失败啦！错误码"+ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
    }

    public String add()
    {
        try
        {
        	if (!StringUtils.isEmpty(callback)) {
        		callback = CoderUtil.decodeXssFilter(AntiXssHelper.htmlAttributeEncode(callback));
        	}
            // 鉴权
            boolean checkResult = this.checkLoginAndGetSkey();
            if (!checkResult)
            {
                throw BusinessException
                        .createInstance(BusinessErrorType.NOT_LOGIN);
            }

            // 参数校验
            if (pvid < 0 || ctid < 0)// 省ID和市ID为必传递
            {
                throw BusinessException.createInstance(
                        BusinessErrorType.PARAM_ERROR, "省市信息不能为空!");
            }
            if(StringUtil.isNotBlank(mobile)){//验证手机号正确性
            	Pattern p = Pattern.compile("^((13[0-9])|(15[0-9])|(17[0-9])|(18[0-9]))\\d{8}$");
            	Matcher matcher = p.matcher(mobile);
            	if(!matcher.matches()){
            		throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR);
            	}
            }

            
            //	根据不同的版本选用不同的方法获取省市信息
            province = getProvince(pvid, ver);
            city = getCity(ctid, ver);

            // 如果城市存在区/县信息，必须填写regionId
            if (city.getAreaList() != null && city.getAreaList().size() > 0)
            {
                if (regionId <= 0)
                {
                    throw BusinessException.createInstance(
                            BusinessErrorType.PARAM_ERROR, "请选择区域!");
                }
            } else
            // 无区城市，将市ID设置为区ID
            {
                regionId = ctid;
            }

            validateRecvAddrInput();
            
            RecvAddr recvAddrs = recvAddrBiz
                    .listRecvAddr(getWid(), false, false, ver, getSk());
            
            if (recvAddrs.getAddressList().size() >=10) {
            	throw BusinessException
                .createInstance(BusinessErrorType.ADDR_ADD_LIMIT);
            }
            

            recvAddrBiz.addRecvAddr(getWid(), regionId, name, address, mobile,
                    phone, postcode,getSk());

            datastr = "success";
            return RST_SUCCESS;
        } catch (BusinessException e)
        {
            setErrCodeAndMsg4BExp(e, "wid=" + getWid() + " regionId=" + regionId
                    + " name=" + name + " address=" + address + " mobile="
                    + mobile + " phone=" + phone + " postcode=" + postcode+ ",ver="+ver);
            //	修改返回msg，用于客户端显示
            setResultMsg(e);
            return RST_API_FAILURE;
        } catch (Throwable e)
        {
            setErrcodeAndMsg4Throwable(e, "wid=" + getWid() + " regionId="
                    + regionId + " name=" + name + " address=" + address
                    + " mobile=" + mobile + " phone=" + phone + " postcode="
                    + postcode+ ",ver="+ver);
            //	修改返回msg，用于客户端显示
            setResultMsg(e);
            return RST_API_FAILURE;
        }  
    }

    public String update()
    {
        long start = System.currentTimeMillis();
        try
        {

            // 鉴权
            boolean checkResult = this.checkLoginAndGetSkey();
            if (!checkResult)
            {
                throw BusinessException
                        .createInstance(BusinessErrorType.NOT_LOGIN);
            }

            if (ctid<0) {
            	ctid = (int)regionId ;
            }
            // 参数校验
            if (pvid < 0 || ctid < 0 || addrId < 0)// 省ID和市ID和addrId为必传递
            {
                throw BusinessException.createInstance(
                        BusinessErrorType.PARAM_ERROR, "省市信息不能为空");
            }
            if(StringUtil.isNotBlank(mobile)){//验证手机号正确性
            	Pattern p = Pattern.compile("^((13[0-9])|(15[0-9])|(17[0-9])|(18[0-9]))\\d{8}$");
            	Matcher matcher = p.matcher(mobile);
            	if(!matcher.matches()){
            		throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR,"手机号码格式不正确");
            	}
            }
            
            // 	根据不同的版本选用不同的方法获取省市信息
            province = getProvince(pvid, ver);
            city = getCity(ctid, ver);

            // 如果城市存在区/县信息，必须填写regionId
            if (city.getAreaList() != null && city.getAreaList().size() > 0)
            {
                if (regionId <= 0)
                {
                    throw BusinessException.createInstance(
                            BusinessErrorType.PARAM_ERROR, "请选择区域!");
                }
            } else
            // 无区城市，将市ID设置为区ID
            {
                regionId = ctid;
            }

            validateRecvAddrInput();
            recvAddrBiz.modifyRecvAddr(getWid(), addrId, regionId, name,
                    address, mobile, phone, postcode,getSk());

            datastr = "success";
            return RST_SUCCESS;
        } catch (BusinessException e)
        {
            setErrCodeAndMsg4BExp(e, "wid=" + getWid() + " addrId=" + addrId
                    + " regionId=" + regionId + " name=" + name + " address="
                    + address + " mobile=" + mobile + " phone=" + phone
                    + " postcode=" + postcode+ ",ver="+ver);
            //	修改返回msg，用于客户端显示
            setResultMsg(e);
            return RST_API_FAILURE;
        } catch (Throwable e)
        {
            setErrcodeAndMsg4Throwable(e, "wid=" + getWid() + " addrId=" + addrId
                    + " regionId=" + regionId + " name=" + name + " address="
                    + address + " mobile=" + mobile + " phone=" + phone
                    + " postcode=" + postcode+ ",ver="+ver);
            //	修改返回msg，用于客户端显示
            setResultMsg(e);
            return RST_API_FAILURE;
        }  
    }

    public String del()
    {
        long start = System.currentTimeMillis();
        try
        {

            // 鉴权
            boolean checkResult = this.checkLoginAndGetSkey();
            if (!checkResult)
            {
                throw BusinessException
                        .createInstance(BusinessErrorType.NOT_LOGIN);
            }

            // 参数校验
            if (addrId <= 0)// 省ID和市ID和addrId为必传递
            {
                throw BusinessException.createInstance(
                        BusinessErrorType.PARAM_ERROR, "省市信息不能为空");
            }

            // 执行删除
            recvAddrBiz.deleteRecvAddr(getWid(), addrId,getSk());

            datastr = "success";
            return RST_SUCCESS;
        } catch (BusinessException e)
        {
            setErrCodeAndMsg4BExp(e, "wid=" + getWid() + " addrId=" + addrId);
            //	修改返回msg，用于客户端显示
            setResultMsg(e);
            return RST_API_FAILURE;
        } catch (Throwable e)
        {
            setErrcodeAndMsg4Throwable(e, "wid=" + getWid() + " addrId=" + addrId);
            //	修改返回msg，用于客户端显示
            setResultMsg(e);
            return RST_API_FAILURE;
        } 
    }

    // 参数校验工具
    protected void validateRecvAddrInput() throws BusinessException
    {
        try
        {
            if (StringUtil.isEmpty(name))
            {
                throw BusinessException.createInstance(
                        BusinessErrorType.PARAM_ERROR, "收货人姓名为空，请重新输入");
            }
            if (name.length() > 10)
            {
                throw BusinessException.createInstance(
                        BusinessErrorType.PARAM_ERROR, "收货人姓名必须小于10字符!");
            }

            if (StringUtil.isEmpty(address))
            {
                throw BusinessException.createInstance(
                        BusinessErrorType.PARAM_ERROR, "收货地址为空，请重新输入");
            }
            if (address.length() > 50)
            {
                throw BusinessException.createInstance(
                        BusinessErrorType.PARAM_ERROR, "收货地址必须小于50字符!");
            }
            if (StringUtil.isEmpty(mobile))
            {
                throw BusinessException.createInstance(
                        BusinessErrorType.PARAM_ERROR, "手机号码为空,请重新输入");
            }
            if (!Util.isMobileNO(mobile))
            {
                throw BusinessException.createInstance(
                        BusinessErrorType.PARAM_ERROR, "手机号码不正确，请重新输入");
            }
            if (StringUtil.isNotEmpty(postcode)
                    && (postcode.length() != 6 || !StringUtil
                            .isNumeric(postcode)))
            {
                throw BusinessException.createInstance(
                        BusinessErrorType.PARAM_ERROR, "邮编不正确，请重新输入");
            }
            if (StringUtil.isEmpty(postcode))
            {
                postcode = "000000";
            }
            if (!validateFixedPhone(phone))
            {
                throw BusinessException.createInstance(
                        BusinessErrorType.PARAM_ERROR, "电话号码不正确，请重新输入");
            }

        } finally
        {
        }
    }

    /**
     * 参考网购侧JS修改 验证固定电话中字符串的合法性 固定电话：区号为数字，3-4位，电话号码为数字，长度3-9位，分机：数字，1-5位
     * 
     * @return true 合法，false 不合法
     */
    private boolean validateFixedPhone(String p)
    {
        if (StringUtil.isEmpty(p))
        {
            return true;
        }
        int sz = p.length();
        if (sz < 5) // 长度不能小于5
        {
            return false;
        }
        // 必须为数字或-
        for (int i = 0; i < sz; i++)
        {
            if (!Character.isDigit(p.charAt(i)) && p.charAt(i) != '-')
                return false;
        }

        return true;
    }
    
    /**
     * 根据版本和ID查找对应的省信息
     * @param regionId
     * @param ver
     * @return
     */
    private Province getProvince(int regionId, int ver){
    	if(ver != 2){
        	return	RegionManager.getProvinceById(pvid); // 省信息
        }else{
        	return	RegionManagerV2.getProvinceById(pvid); // 省信息
        }
    }
    
    /**
     * 根据版本和ID查找对应的市信息
     * @param cityId
     * @param ver
     * @return
     */
    private City getCity(int cityId, int ver){
    	if(ver != 2){
        	return	RegionManager.getCityById(ctid); // 市信息
        }else{
        	return	RegionManagerV2.getCityById(ctid); // 市信息
        }
    }

    public int getPvid()
    {
        return pvid;
    }

    public void setPvid(int pvid)
    {
        this.pvid = pvid;
    }

    public int getCtid()
    {
        return ctid;
    }

    public void setCtid(int ctid)
    {
        this.ctid = ctid;
    }

    public long getAddrId()
    {
        return addrId;
    }

    public void setAddrId(long addrId)
    {
        this.addrId = addrId;
    }

    public long getRegionId()
    {
        return regionId;
    }

    public void setRegionId(long regionId)
    {
        this.regionId = regionId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getMobile()
    {
        return mobile;
    }

    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getPostcode()
    {
        return postcode;
    }

    public void setPostcode(String value)
    {

        // modify by wendyhu
        if (StringUtil.isNotEmpty(value))
        {
            long posValue = StringUtil.toLong(value.trim(), 0);
            this.postcode = String.format("%06d", posValue);
        }
    }

    public RecvAddrBiz getRecvAddrBiz()
    {
        return recvAddrBiz;
    }

    public void setRecvAddrBiz(RecvAddrBiz recvAddrBiz)
    {
        this.recvAddrBiz = recvAddrBiz;
    }

    public String getDatastr()
    {
        return datastr;
    }

    public void setDatastr(String datastr)
    {
        this.datastr = datastr;
    }

    public Province getProvince()
    {
        return province;
    }

    public void setProvince(Province province)
    {
        this.province = province;
    }

    public City getCity()
    {
        return city;
    }

    public void setCity(City city)
    {
        this.city = city;
    }

	public int getVer() {
		return ver;
	}

	public void setVer(int ver) {
		this.ver = ver;
	}

	public int getRver() {
		return rver;
	}

	public void setRver(int rver) {
		this.rver = rver;
	}

	public String getRegionStr() {
		return regionStr;
	}

	public void setRegionStr(String regionStr) {
		this.regionStr = regionStr;
	}

	public String getCallback() {
		return callback;
	}

	public void setCallback(String callback) {
		this.callback = callback;
	}
	
}
