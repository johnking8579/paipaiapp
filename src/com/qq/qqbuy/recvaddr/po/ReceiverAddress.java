package com.qq.qqbuy.recvaddr.po;

public class ReceiverAddress{
	/**
	 * addressId:收货地址编号
	 */
	public long addressId;
	
	/**
	 * regionId:收货地区编号
	 */
	public long regionId;
	
	/**
	 * name:收货人姓名
	 */
	public String name=null;
	
	/**
	 * 省
	 */
	public String province=null;
	
	/**
	 * 市
	 */
	public String city=null;
	
	/**
	 * 区
	 */
	public String district=null;
	
	/**
	 * address:收货地址
	 */
	public String address=null;
	
	/**
	 * postcode:邮编
	 */
	public String postcode;
	
	
	/**
	 * phone:电话
	 */
	public String phone=null;
	
	/**
	 * mobile:手机
	 */
	public String mobile=null;
	
	/**
	 * usedCount:使用次数
	 */
	public long usedCount;
	
	/**
	 * lastUsedTime:该地址最后一次使用时间
	 */
	public String lastUsedTime=null;
	
	/**
	 * lastModifyTime:该地址最近一次修改时间
	 */
	public String lastModifyTime=null;

	public long getAddressId() {
		return addressId;
	}

	public void setAddressId(long addressId) {
		this.addressId = addressId;
	}

	public long getRegionId() {
		return regionId;
	}

	public void setRegionId(long regionId) {
		this.regionId = regionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public long getUsedCount() {
		return usedCount;
	}

	public void setUsedCount(long usedCount) {
		this.usedCount = usedCount;
	}

	public String getLastUsedTime() {
		return lastUsedTime;
	}

	public void setLastUsedTime(String lastUsedTime) {
		this.lastUsedTime = lastUsedTime;
	}

	public String getLastModifyTime() {
		return lastModifyTime;
	}

	public void setLastModifyTime(String lastModifyTime) {
		this.lastModifyTime = lastModifyTime;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}



	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	@Override
	public String toString() {
		return "ReceiverAddress [address=" + address + ", addressId="
				+ addressId + ", city=" + city + ", district=" + district
				+ ", lastModifyTime=" + lastModifyTime + ", lastUsedTime="
				+ lastUsedTime + ", mobile=" + mobile + ", name=" + name
				+ ", phone=" + phone + ", postcode=" + postcode
				+ ", province=" + province + ", regionId=" + regionId
				+ ", usedCount=" + usedCount + "]";
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public ReceiverAddress(long addressId, long regionId, String name,
			String province, String city, String district, String address,
			String postcode, String phone, String mobile, long usedCount,
			String lastUsedTime, String lastModifyTime) {
		super();
		this.addressId = addressId;
		//香港、澳门特殊处理（理应由深研提供底层服务，目前由我们特殊处理，我们新增地址给的3300/3200，返回变成了33/22）
		if (regionId == 33) {
			this.regionId = 3300;
		} else if (regionId == 32) {
			this.regionId = 3200 ;
		} else {
			this.regionId = regionId;
		}
		this.name = name;
		this.province = province;
		this.city = city;
		this.district = district;
		this.address = address;
		this.postcode = postcode;
		this.phone = phone;
		this.mobile = mobile;
		this.usedCount = usedCount;
		this.lastUsedTime = lastUsedTime;
		this.lastModifyTime = lastModifyTime;
	}

	public ReceiverAddress() {
	}
	
	
}
