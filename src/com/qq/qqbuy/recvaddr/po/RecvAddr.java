package com.qq.qqbuy.recvaddr.po;

import java.util.ArrayList;
import java.util.List;




public class RecvAddr
{

	private List<ReceiverAddress> addressList = new ArrayList<ReceiverAddress>();

	public List<ReceiverAddress> getAddressList() 
	{
		return addressList;
	}

	public void setAddressList(List<ReceiverAddress> addressList) {
		this.addressList = addressList;
	}
}