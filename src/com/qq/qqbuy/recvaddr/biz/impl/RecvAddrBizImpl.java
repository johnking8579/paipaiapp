package com.qq.qqbuy.recvaddr.biz.impl;

import java.util.List;

import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.recvaddr.biz.RecvAddrBiz;
import com.qq.qqbuy.recvaddr.po.ReceiverAddress;
import com.qq.qqbuy.recvaddr.po.RecvAddr;
import com.qq.qqbuy.recvaddr.util.AddressUtil;
import com.qq.qqbuy.thirdparty.idl.IDLResult;
import com.qq.qqbuy.thirdparty.idl.addr.RecvAddrClient;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiAddRecvaddrListReq;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiAddRecvaddrListResp;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiDeleteRecvaddrListReq;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiDeleteRecvaddrListResp;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiGetRecvaddrListReq;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiGetRecvaddrListResp;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiGetRecvaddrReq;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiGetRecvaddrResp;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiModifyRecvaddrListReq;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiModifyRecvaddrListResp;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiRecvaddr;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.GetRecvAddrList2Req;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.GetRecvAddrList2Resp;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ModifyDefaultRecvAddrReq;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ModifyDefaultRecvAddrResp;




public class RecvAddrBizImpl implements RecvAddrBiz {

	@Override
	public RecvAddr listRecvAddr(long qq, boolean needMask, boolean needName,String sk) throws BusinessException 
	{
	  
		RecvAddr addr = new RecvAddr();
		
		RecvAddrClient addrClient = new RecvAddrClient();
		ApiGetRecvaddrListReq req = new ApiGetRecvaddrListReq();
		req.setSource("mqgo");
		
		req.setUin(qq);

		IDLResult<ApiGetRecvaddrListResp> result = addrClient.getRecvaddrList(req, qq, sk);

		List<ReceiverAddress> list = AddressUtil.idlAddr2PoAddr(result.getResult().getVecApiRecvaddr());
		
		if(list == null || list.size() == 0)
		{
			return addr;
		}
		
		//添加省、市, 加马赛克
	    for (ReceiverAddress address : list) 
	    {
	    	String showAddress = AddressUtil.address2String(address, needMask, needName);
	    	address.setAddress(showAddress);
	    }
		
		addr.setAddressList(list);
		
		return addr;
	}
	
	@Override
	public RecvAddr listRecvAddr(long qq, boolean needMask, boolean needName,
			int regionVersion,String sk) throws BusinessException {
		if(regionVersion < 2){
			return	listRecvAddr(qq, needMask, needName,sk);
		}else if(regionVersion == 2){
			RecvAddr addr = new RecvAddr();
			try
			{
				RecvAddrClient addrClient = new RecvAddrClient();
				ApiGetRecvaddrListReq req = new ApiGetRecvaddrListReq();
				req.setSource("mqgo");
				req.setUin(qq);
				IDLResult<ApiGetRecvaddrListResp> result = addrClient.getRecvaddrList(req, qq, sk);
				
				List<ReceiverAddress> list = AddressUtil.idlAddr2PoAddr(result.getResult().getVecApiRecvaddr());
				
				if(list == null || list.size() == 0)
				{
					return addr;
				}
				
				//添加省、市, 加马赛克
			    for (ReceiverAddress address : list) 
			    {
			    	String showAddress = AddressUtil.address2StringV2(address, needMask, needName);
			    	address.setAddress(showAddress);
			    }
				
				addr.setAddressList(list);
				
				return addr;
			}
			catch(Exception e)
			{
				throw BusinessException.createInstance(BusinessErrorType.CALL_OPENAPI_BIZ_FAILED,e);
			}
		}else{
			return new RecvAddr();
		}
	}
	
	@Override
	public RecvAddr listRecvAddrV2(long qq, boolean needMask, boolean needName,
		int regionVersion,String sk) throws BusinessException {		
		RecvAddr addr = new RecvAddr();
		try
		{
			RecvAddrClient addrClient = new RecvAddrClient();
			GetRecvAddrList2Req req = new GetRecvAddrList2Req();
			req.setstrSource("mqgo");
			req.setdwUin(qq);
			GetRecvAddrList2Resp result = addrClient.getRecvaddrListV2(req, qq, sk);
			
			return addr;
		}
		catch(Exception e)
		{
			throw BusinessException.createInstance(BusinessErrorType.CALL_OPENAPI_BIZ_FAILED,e);
		}		
	}
	
	@Override
	public RecvAddr modifyDefaultRecvAddr(long qq, String sk, long addrId) throws BusinessException {		
		RecvAddr addr = new RecvAddr();
		try
		{
			RecvAddrClient addrClient = new RecvAddrClient();
			ModifyDefaultRecvAddrReq req = new ModifyDefaultRecvAddrReq();
			req.setstrSource("mqgo");
			req.setdwUin(qq);
			req.setdwAddrId(addrId) ;
			
			ModifyDefaultRecvAddrResp result = addrClient.modifyDefaultRecvAddr(req, qq, sk);
			
			return addr;
		}
		catch(Exception e)
		{
			throw BusinessException.createInstance(BusinessErrorType.CALL_OPENAPI_BIZ_FAILED,e);
		}		
	}

	@Override
	public boolean addRecvAddr(long buyerUin,
			long regionId, String name, String address, String mobile,
			String phone, String postcode,String sk) throws BusinessException {
		
		RecvAddrClient addrClient = new RecvAddrClient();
		ApiAddRecvaddrListReq req = new ApiAddRecvaddrListReq();
		req.setSource("mqgo");
		
		ApiRecvaddr addr = new ApiRecvaddr();
		addr.setUin(buyerUin);
		addr.setRegionId(regionId);
		addr.setRecvName(name);
		addr.setRecvAddress(address);
		addr.setRecvMobile(mobile);
		addr.setRecvPhone(phone);
		addr.setPostCode(postcode);
		req.getVecApiRecvaddr().add(addr);
		
		IDLResult<ApiAddRecvaddrListResp> resp = addrClient.addRecvaddrList(req, buyerUin, sk);
		return resp.isSucceed();

	}

	@Override
	public boolean modifyRecvAddr(long buyerUin,
			long addressId, long regionId, String name, String address,
			String mobile, String phone, String postcode,String sk) throws BusinessException 
	{

		try 
		{	
    		RecvAddrClient addrClient = new RecvAddrClient();
    		ApiModifyRecvaddrListReq req = new ApiModifyRecvaddrListReq();
    		req.setSource("mqgo");
    		
    		ApiRecvaddr addr = new ApiRecvaddr();
    		addr.setAddrId(addressId);
			addr.setUin(buyerUin);
			addr.setRegionId(regionId);
			addr.setRecvName(name);
			addr.setRecvAddress(address);
			addr.setRecvMobile(mobile);
			addr.setRecvPhone(phone);
			addr.setPostCode(postcode);
    		req.getVecApiRecvaddr().add(addr);
    		
    		IDLResult<ApiModifyRecvaddrListResp> resp = addrClient.modifyRecvaddrList(req, buyerUin, sk);
    		return resp.isSucceed();
		}
       catch (Exception e) 
       {
    	   throw BusinessException.createInstance(BusinessErrorType.CALL_OPENAPI_BIZ_FAILED, e);
       }
	}
	
//	@Override
//	public ReceiverAddress getRecvAddr(long buyerUin,long addressId,String sid,String lskey) throws BusinessException 
//	{
//
//		RecvAddrClient addrClient = new RecvAddrClient();
//		ApiGetRecvaddrReq req = new ApiGetRecvaddrReq();
//		req.setSource("mqgo");
//		req.setAddrId(addressId);
//		IDLResult<ApiGetRecvaddrResp> resp = addrClient.getRecvaddr(req, buyerUin, addrClient.getSkey(buyerUin, sid, lskey));
//		
//		ReceiverAddress addr = AddressUtil.idlAddr2PoAddr(resp.getResult().getApiRecvaddr());
//		return addr;
//	}
	
	@Override
	public boolean deleteRecvAddr(long buyerUin,long addressId,String sk) throws BusinessException 
	{
		RecvAddrClient addrClient = new RecvAddrClient();
		ApiDeleteRecvaddrListReq req = new ApiDeleteRecvaddrListReq();
		req.setSource("mqgo");
		
		ApiRecvaddr addr = new ApiRecvaddr();
		addr.setAddrId(addressId);
		addr.setUin(buyerUin);
		req.getVecApiRecvaddr().add(addr);
		
		IDLResult<ApiDeleteRecvaddrListResp> resp = addrClient.deleteRecvaddrList(req, buyerUin,sk);
		return resp.isSucceed();
	}
	
	@Override
	public long getRecvAddrCount(long qq,String sk) throws BusinessException 
	{
		RecvAddrClient addrClient = new RecvAddrClient();
		ApiGetRecvaddrListReq req = new ApiGetRecvaddrListReq();
		req.setSource("mqgo");
		req.setUin(qq);
		
		IDLResult<ApiGetRecvaddrListResp> result = addrClient.getRecvaddrList(req, qq, sk);
		return result.getResult().getTotalCount();
	}

}

