package com.qq.qqbuy.cmdy.biz.impl;

import java.util.Map;
import java.util.Vector;

import com.qq.qqbuy.cmdy.biz.CmdyBiz;
import com.qq.qqbuy.cmdy.po.CmdyAddVo;
import com.qq.qqbuy.cmdy.po.CmdyItem;
import com.qq.qqbuy.cmdy.po.CmdyItemListPo;
import com.qq.qqbuy.cmdy.po.CmdyListVo;
import com.qq.qqbuy.cmdy.po.CmdyMakeOrderPo;
import com.qq.qqbuy.cmdy.util.CmdyConverter;
import com.qq.qqbuy.cmdy.util.CmdyUtil;
import com.qq.qqbuy.cmdy.util.CmdyValidate;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.SpringHelper;
import com.qq.qqbuy.common.constant.BizConstant;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.deal.po.*;
import com.qq.qqbuy.deal.util.DealConstant;
import com.qq.qqbuy.deal.util.DealUtil;
import com.qq.qqbuy.dealpromote.biz.DealPromoteBiz;
import com.qq.qqbuy.dealpromote.po.DealFavorVo;
import com.qq.qqbuy.dealpromote.po.DealPromotePo;
import com.qq.qqbuy.dealpromote.po.FavorExVo;
import com.qq.qqbuy.dealpromote.po.ItemPromoteResultVo;
import com.qq.qqbuy.dealpromote.po.TradeFavorVo;
import com.qq.qqbuy.dealpromote.util.DealPromoteUtil;
import com.qq.qqbuy.item.util.DispFormater;
import com.qq.qqbuy.recvaddr.biz.RecvAddrBiz;
import com.qq.qqbuy.recvaddr.po.ReceiverAddress;
import com.qq.qqbuy.recvaddr.po.RecvAddr;
import com.qq.qqbuy.shippingFee.biz.SFBiz;
import com.qq.qqbuy.shippingFee.po.CodShipPo;
import com.qq.qqbuy.thirdparty.idl.cmdy.CmdyClient;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.AddCmdy2CartResp;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.ConfirmOrderResp;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.MakeOrderResp;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.ModifyCmdyNumInCartResp;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.OrderView;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.RmvCmdyFromCartResp;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.ViewCartResp;
import com.qq.qqbuy.thirdparty.idl.pay.recommend.PayRecommendClient;
import com.qq.qqbuy.thirdparty.idl.pay.recommend.protocol.GetPayRecommendListResp;
import com.qq.qqbuy.thirdparty.idl.pay.recommend.protocol.PayRecommend;


/**
 * @author winsonwu
 * @Created 2013-2-18 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class CmdyBizImpl implements CmdyBiz {

	public CmdyAddVo add(long buyerUin, String itemCode, String itemAttr,
			long buyNum, boolean addForce, long secenid) throws BusinessException {
		CmdyValidate.validateCmdyAdd(buyerUin, itemCode, itemAttr, buyNum);

		AddCmdy2CartResp resp = CmdyClient.addCmdy2Cart(buyerUin, itemCode,
				itemAttr, buyNum, addForce, secenid);
		if (resp == null || resp.getResult() != 0) {
			if (resp.getResult() == 33) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_ADD_FAIL_LIMIT,
						"商品数量超出最大范围");
			} else if (resp.getResult() == 34) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_ADD_FAIL_FRE, "用户操作频率操作qq号限制");
			} else if (resp.getResult() == 35) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_ADD_FAIL_IP, "用户操作频率操作ip限制");
			} else if(resp.getResult() == 196){
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_ADD_FAIL_DW, "此商品已下架，您不能购买");
			}else {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_ADD_FAIL, "商品加入购物车失败");
			}
		}

		CmdyAddVo vo = new CmdyAddVo();
		vo.setTotalFee(resp.getTotalFee());
		vo.setDisTotalFee(DispFormater.priceDispFormater(resp.getTotalFee()));
		vo.setTotalNum(resp.getTotalNum());

		return vo;
	}

	public CmdyListVo list(long buyerUin) throws BusinessException {

		ViewCartResp resp = CmdyClient.queryCmdy(buyerUin, true);
		if (resp == null || resp.getResult() != 0) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_CMDY_LIST_FAIL, "查询购物车列表失败");
		}

		CmdyListVo vo = new CmdyListVo();
		vo.setNormalItems(CmdyConverter.convert(CmdyUtil.getDealViewList(resp), buyerUin));
		vo.setProblemItems(CmdyConverter.convert(CmdyUtil
				.getProblemDealViewList(resp), buyerUin));
		vo.cacalNormalTotalPriceAfterNormalItems();
		return vo;
	}

	public void modifyItemNum(long buyerUin, String itemList)
			throws BusinessException {

		CmdyItemListPo po = CmdyItemListPo.disSerialize(itemList);
		if (po == null) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_INVALID_PARAMETER, "待修改的商品列表参数错误");
		}
		for (CmdyItem ci : po.getCmdyItems()) {
			ModifyCmdyNumInCartResp resp = CmdyClient.modifyCmdyNum(buyerUin,
					ci.getItemCode(), ci.getItemAttr(), ci.getBuyNum());
			if (resp == null || resp.getResult() != 0) {
				
				if (resp != null && resp.getResult() == 33) {
					throw BusinessException.createInstance(
							ErrConstant.ERRCODE_CMDY_ADD_FAIL_LIMIT,
							"商品数量超出最大范围");
				} else {
					throw BusinessException
					.createInstance(
							ErrConstant.ERRCODE_CMDY_MODIFYNUM_FAIL,
							"修改购物车中商品数量失败");
				}
				
			}
		}

	}

	public void remove(long buyerUin, int payType, String itemList)
			throws BusinessException {
		CmdyItemListPo po = CmdyItemListPo.disSerialize(itemList);
		if (po == null) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_INVALID_PARAMETER, "商品参数错误");
		}
		po.setPayType(payType);
		CmdyValidate.validateCmdyRemove(buyerUin, po);

		RmvCmdyFromCartResp resp = CmdyClient.rmvCmdy4Cart(buyerUin, po);
		Log.run.info("ApiCartAction#remove" + " remove over buyerUin："
				+ buyerUin + " payType:" + payType + " itemList:" + itemList);
		if (resp == null || resp.getResult() != 0) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_CMDY_REMOVE_FAIL, "删除购物车中商品失败");
		}
	}

	public int cartNum(long buyerUin) throws BusinessException {

		ViewCartResp resp = CmdyClient.queryCmdy(buyerUin, true);
		if (resp == null || resp.getResult() != 0) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_CMDY_LIST_FAIL, "查询购物车列表失败");
		}

		return CmdyConverter.getNum(CmdyUtil.getDealViewList(resp))
				+ CmdyConverter.getNum(CmdyUtil.getProblemDealViewList(resp));
	}

	@Override
	public ConfirmOrderVo confirmOrderCmdy(ConfirmOrderPo req, String sk) throws BusinessException {
		CmdyValidate.validateCmdyConfirmOrder(req);
		CmdyItemListPo po = CmdyItemListPo.disSerialize(req.getItemList());
		if (po == null) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_INVALID_PARAMETER, "待分单的商品列表参数错误");
		}
		po.setPayType(req.getPayType());

		// 查询收货地址列表
		RecvAddrBiz addrBiz = (RecvAddrBiz)SpringHelper.getBean("recvAddrBiz");
		RecvAddr addrList = addrBiz.listRecvAddr(req.getBuyerUin(), false, false, sk);
		if (addrList == null || addrList.getAddressList() == null
				|| addrList.getAddressList().size() <= 0) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_DEAL_ADDRLIST_EMPTY, "查询用户收货地址列表为空");
		}
		ReceiverAddress defaultAddr = DealUtil.getDefaultAddr(
				addrList.getAddressList(), req.getAdid());
		if (defaultAddr == null) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_DEAL_FINDADDR_FAIL,
					"查找不到收货地址" + req.getAdid());
		}

		ConfirmOrderResp resp = CmdyClient.cmdyConfirmOrder(req.getBuyerUin(),
				req.getSk(), po, defaultAddr.getRegionId());
		if (resp == null || resp.getResult() != 0) {
			if (resp.getResult() == 110) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_ADD_FAIL, "部分商品无法下单, 请稍后再试.");
			} else if (resp.getResult() == 201) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_ADD_FAIL, "太多未结束支付订单.");
			} else if (resp.getResult() == 196) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_ADD_FAIL, "买卖家相同.");
			} else {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_ADD_FAIL, "下单失败, 请稍后再试.");
			}
		}

		ConfirmOrderVo vo = new ConfirmOrderVo();

		if (req.getVer() == 2) {
			vo.setRedPackages(CmdyUtil.convert(resp,
					DealConstant.REDPACKET_AND_COUPON));
		} else {
			vo.setRedPackages(CmdyUtil.convert(resp, DealConstant.ONLY_REDPACKET));
		}
		vo.setRecv(addrList);
		vo.setDefaultAddr(defaultAddr);
		Vector<ConfirmPackageVo> vcp = CmdyConverter.convertPackage(
				CmdyUtil.getDealViewList(resp, po), req.getPayType());
		vo.setNormalPackages(vcp);
		vo.setProblemPackages(CmdyConverter.convertPackage(
				CmdyUtil.getProblemDealViewList(resp), req.getPayType()));
		vo.lastCalculateTotalPrice();
		return vo;
	}

//	private RecvAddr listRecvAddr(long uin, String sid, String lskey)
//			throws BusinessException {
//		RecvAddrClient addrClient = IDLClientProxy.getIDLClient(RecvAddrClient.class);
//		ApiGetRecvaddrListReq req = IDLClientProxy.getIDLPo(ApiGetRecvaddrListReq.class);
//		
//		IDLResult<ApiGetRecvaddrListResp> result = addrClient.getRecvaddrList(req, uin, IDLClientProxy.getIDLClient(UserInfoClient.class).loginWithSid(uin, sid).getKey());
//		
//		List<ReceiverAddress> list = AddressUtil.idlAddr2PoAddr(result.getResult().getVecApiRecvaddr());
//		RecvAddr addrList = new RecvAddr();
//		addrList.setAddressList(list);
//		return addrList;
//	}

	@Override
	public Vector<MakeOrderVo> makeOrder(MakeOrderPo req)
			throws BusinessException {
		if (req == null || (req.validate().size() > 0)) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_INVALID_PARAMETER, "请求参数有问题");
		}

		// 调购物车下单接口
		CmdyMakeOrderPo po = CmdyMakeOrderPo.deSerialize(req);

		po.setRecvAddrId(req.getAddressid());
		MakeOrderResp cmdyMakeOrderRsp = CmdyClient.cmdyMakeOrder(
				req.getBuyerUin(), req.getSkey(), po);
		Log.run.info("cmdyMakeOrderRsp.getResult()----------------------"+cmdyMakeOrderRsp!=null?cmdyMakeOrderRsp.getResult():"");
		if (cmdyMakeOrderRsp == null || cmdyMakeOrderRsp.getResult() != 0) {
			if (cmdyMakeOrderRsp != null && cmdyMakeOrderRsp.getResult() == 14) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_MAKEORDER_FAIL_PARAM,
						"购物车下单失败");
			} else if (cmdyMakeOrderRsp != null
					&& cmdyMakeOrderRsp.getResult() == 196) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_ADD_FAIL_DW, "此商品已下架，您不能购买");
			} else if (cmdyMakeOrderRsp != null
					&& cmdyMakeOrderRsp.getResult() == 33) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_MAKEORDER_FAIL_OVER_LIMIT,
						"商品数量超出最大范围");
			} else{
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_MAKEORDER_FAIL, "购物车下单失败");
			}
		}
		Vector<OrderView> deals = cmdyMakeOrderRsp.getResponse()
				.getOrderViewInfo();
		if (deals == null) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_CMDY_MAKEORDER_FAIL, "购物车下单返回的数据不正确");
		}
		Vector<MakeOrderVo> vos = new Vector<MakeOrderVo>();

		for (OrderView ov : deals) {
			MakeOrderVo vo = new MakeOrderVo();
			vo.setTotalFee(ov.getTotalFee());
			vo.setDealCode(ov.getDealId());
			vo.setSellerUin(ov.getShopViewInfo().getSellerUin());
			vos.add(vo);
		}
		return vos;
		// // 下单后的异常需要捕获，并将之前在拍拍的订单进行关闭
		// try {
		// MakeOrderVo vo = new MakeOrderVo();
		// vo.setDealCode(dealCode);
		// vo.setPayType(req.getPayType());
		// vo.setSellerUin(req.getSellerUin());
		//
		// if (DealConstant.PAYTYPE_OLP == req.getPayType()) {
		// // 生成去支付的跳转url
		// ret = payBiz.getPayToken(req.getBuyerUin(), req.getSellerUin(),
		// dealCode, payDesc, source, null, callbackUrl,
		// callbackUrl, clientTenPayUrl);
		// }
		//
		// return vo;
		// } catch (Exception e) {
		// // 捕获到异常，将订单设置为关闭
		// // cancelDealNoExp(dealCode, req.getSellerUin());
		// throw BusinessException.createInstance(
		// ErrConstant.ERRCODE_DEAL_AFTER_ORDERDEAL_FAIL,
		// "下单后业务逻辑处理出现异常", e);
		// }

	}
	
	
	
	@Override
	public Vector<OrderView> multiMakeOrder(MakeOrderPo req)
			throws BusinessException {
		if (req == null || (req.validate().size() > 0)) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_INVALID_PARAMETER, "请求参数有问题");
		}

		// 调购物车下单接口
		CmdyMakeOrderPo po = CmdyMakeOrderPo.deSerialize(req);

		po.setRecvAddrId(req.getAddressid());
		MakeOrderResp cmdyMakeOrderRsp = CmdyClient.cmdyMakeOrder(
				req.getBuyerUin(), req.getSkey(), po);
		Log.run.debug(cmdyMakeOrderRsp);
		Log.run.debug(cmdyMakeOrderRsp.getResult());
		
		Log.run.info("cmdyMakeOrderRsp.getResult()----------------------" + cmdyMakeOrderRsp!=null?cmdyMakeOrderRsp.getResult():"");
		if (cmdyMakeOrderRsp == null || cmdyMakeOrderRsp.getResult() != 0) {
			if (cmdyMakeOrderRsp != null && cmdyMakeOrderRsp.getResult() == 14) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_MAKEORDER_FAIL_PARAM,
						"购物车下单失败");
			} else if (cmdyMakeOrderRsp != null
					&& cmdyMakeOrderRsp.getResult() == 196) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_ADD_FAIL_DW, "此商品已下架，您不能购买");
			} else if (cmdyMakeOrderRsp != null
					&& cmdyMakeOrderRsp.getResult() == 33) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_MAKEORDER_FAIL_OVER_LIMIT,
						"商品数量超出最大范围");
			} else{
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_MAKEORDER_FAIL, "购物车下单失败");
			}
		}
		Vector<OrderView> deals = cmdyMakeOrderRsp.getResponse()
				.getOrderViewInfo();
		if (deals == null) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_CMDY_MAKEORDER_FAIL, "购物车下单返回的数据不正确");
		}
		return deals;
	}
	
	/********************************BETA测试数据*********************************
	 * http://candeladiao-pc.tencent.com:8080/webapp_paipai/api/cart/confirmOrderV2.xhtml?
	 * mk=1-4E3BAACBC6BB&ptag=2.0.0.1031.2&itemList=61F90295000000000401000000012514$%E9%9E%8B%E7%A0%81:28|%E9%A2%9C%E8%89%B2%E5%88%86%E7%B1%BB:%E5%B7%A7%E5%85%8B%E5%8A%9B%E8%89%B2$1~
	 * &adid=23&buildVer=201309041000&payType=0&appVer=2.3.0&ver=2&q_g_t=u_8h5fb_2nf8fjk@qq.com&pgid=1031&dtag=ios&
	 * uk=dWluPTI4OTMwMzcyMyZsc2tleT0wMDAzMDAwMDI5YTAyMzkyODkwOWJhNmRkNzI3MzEwMzkxZjU2YmZmZGExYTg0ZmQxNWM0NDI1OTQ3MTRmYTMwNTQ4MTVmMDM1OTU1OGY2NDVjOTgzNzIyJnR5cGU9MQ==
	 */

	@Override
	public DealFavorVo confirmOrderCmdyV2(ConfirmOrderPo req, long uin, String sk) throws BusinessException {
		
		CmdyValidate.validateCmdyConfirmOrder(req);
		
		DealFavorVo favorVo = new DealFavorVo();
		
		CmdyItemListPo po = CmdyItemListPo.disSerialize(req.getItemList());
		if (po == null) {
			favorVo.setErrorCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
			favorVo.setErrorMsg("待分单的商品列表参数错误");
			
			return favorVo;
		}
		po.setPayType(req.getPayType());

		// 查询收货地址列表
		RecvAddrBiz addrBiz = (RecvAddrBiz)SpringHelper.getBean("recvAddrBiz");
		RecvAddr addrList = addrBiz.listRecvAddr(req.getBuyerUin(), false, false, sk);
		if (addrList == null || addrList.getAddressList() == null
				|| addrList.getAddressList().size() <= 0) {
			
			favorVo.setErrorCode(ErrConstant.ERRCODE_DEAL_ADDRLIST_EMPTY);
			favorVo.setErrorMsg("查询用户收货地址列表为空");
			
			return favorVo;
		}
		ReceiverAddress defaultAddr = DealUtil.getDefaultAddr(
				addrList.getAddressList(), req.getAdid());
		if (defaultAddr == null) {
			favorVo.setErrorCode(ErrConstant.ERRCODE_DEAL_FINDADDR_FAIL);
			favorVo.setErrorMsg("查找不到收货地址" + req.getAdid());

			return favorVo;
		}

		ConfirmOrderResp coResp = CmdyClient.cmdyConfirmOrderV2(req.getBuyerUin(),
				req.getSk(), po, defaultAddr.getRegionId());
		if (coResp == null || coResp.getResult() != 0) {
			if (coResp.getResult() == 110) {
				favorVo.setErrorCode(ErrConstant.ERRCODE_DEAL_FINDADDR_FAIL);
				favorVo.setErrorMsg("部分商品无法下单, 请稍后再试.");

				return favorVo;
			} else if (coResp.getResult() == 201) {
				favorVo.setErrorCode(ErrConstant.ERRCODE_CMDY_ADD_FAIL);
				favorVo.setErrorMsg("太多未结束支付订单.");

				return favorVo;
			} else if (coResp.getResult() == 196) {
				favorVo.setErrorCode(ErrConstant.ERRCODE_CMDY_ADD_FAIL);
				favorVo.setErrorMsg("买卖家相同.");

				return favorVo;
			} else {
				favorVo.setErrorCode(ErrConstant.ERRCODE_CMDY_ADD_FAIL);
				favorVo.setErrorMsg("批价失败, 请稍后再试.");

				return favorVo;
			}
		}

		ConfirmOrderVo vo = new ConfirmOrderVo();

		vo.setRedPackages(CmdyUtil.convert(coResp,
					DealConstant.REDPACKET_AND_COUPON));
		vo.setRecv(addrList);
		vo.setDefaultAddr(defaultAddr);
		Vector<ConfirmPackageVo> vcp = CmdyConverter.convertPackageV2(
				CmdyUtil.getDealViewList(coResp, po), req.getPayType(), BizConstant.NORMAL_DEAL_ID_START);
		vo.setNormalPackages(vcp);
		vo.setProblemPackages(CmdyConverter.convertPackageV2(
				CmdyUtil.getProblemDealViewList(coResp), req.getPayType(), BizConstant.PROBLEM_DEAL_ID_START));
//		vo.lastCalculateTotalPrice();
		// 批价
		DealPromoteBiz dealPromoteBiz = (DealPromoteBiz)SpringHelper.getBean("dealPtBiz");
		DealPromotePo dealPo = null;
		try {
			// 1.分单结果转换成批价参数
			dealPo = DealPromoteUtil.convert(uin, req.getPayType(), vo,req.getUseRedPackage());
		} catch (Exception e) {
			favorVo.setErrorCode(ErrConstant.ERRCODE_PROMOTE_OTHER_ERROR);
			favorVo.setErrorMsg("分单输出数据转批价输入数据失败.");

			return favorVo;
		}
		
		if (dealPo != null) {
			// 2.批价
			dealPo.setReqSource(BizConstant.DEAL_PROMOTE_FROM_FIRST_ORDER_CONFIRM);
			favorVo = dealPromoteBiz.queryPromote(dealPo, vo, req.getSk(), req.getMk());
			
			// 3.使用了多个红包需重新批价
			if (favorVo.getQueryPromoteAgain()) {
				dealPo = DealPromoteUtil.convert(uin, req.getPayType(), favorVo,req.getUseRedPackage());
				dealPo.setReqSource(BizConstant.DEAL_PROMOTE_FROM_ORDER_CONFIRM);
				favorVo = dealPromoteBiz.queryPromote(dealPo, vo, req.getSk(), req.getMk());
			}
		}
		//add by wanghao 新增异常商品列表
		Vector<TradeFavorVo> problemDealList = CmdyConverter.convertBadCommodityList(CmdyUtil.getProblemDealViewList(coResp));
		favorVo.setProblemDealList(problemDealList);
		if (favorVo.getDealList().size() == 0 && favorVo.getProblemDealList().size() > 0) {
			boolean wxSupportFlag = true ;			
			for(TradeFavorVo tradeFavorVo:favorVo.getProblemDealList()) {
				wxSupportFlag = wxSupportFlag&&(tradeFavorVo.getIsSupportWXPay()==1) ;
			}
			if (wxSupportFlag) {
				favorVo.setIsSupportWXPay(1) ;
			} else {
				favorVo.setIsSupportWXPay(0) ;
			}					
		}		
		//add by wanghao 
		return favorVo;
	}
	
	
	
	/**
	 * 获取推荐支付方式列表
	 *
	 * @param mk           设备码
	 * @param wid          买家qq
	 * @param userType     用户类型  用户类型：1=QQ qq账号及绑定了qq账号的微信账号(login_type=wx切uin<39亿); 2=WX wx账号非绑定qq(login_type=wx切uin>39亿); 3=JD JD账号
	 * @param dealTotalFee 订单总价
	 * @param itemCodes    订单商品id串,用半角逗号分隔
	 * @param sellerIds    订单商品卖家qq号串,用半角逗号分隔(顺序跟itemCodes一一对应)
	 * @param categorys    订单的商品类目,用半角逗号分隔(顺序跟itemCodes一一对应)
	 * @param supportcods  是否支持cod	串, 1=支持	2=不支持 ,用半角逗号分隔，例如1,2,1(顺序跟itemCodes一一对应)
	 * @return
	 */
	@Override
	public Vector<PayRecommend> getPayRecommends(String mk, long wid, int userType, long dealTotalFee, String itemCodes, String sellerIds, String categorys, String supportcods) {
		System.out.println("DealPromoteBizImpl.getPayRecommends()");
		Log.run.debug("wid:"+wid+";userType:"+userType+";dealTotalFee："+dealTotalFee+";itemCodes:"+itemCodes+";sellerIds:"+sellerIds+";categorys:"+categorys+";:");
		
		Vector<PayRecommend> payRecommends = new Vector<PayRecommend>();
		GetPayRecommendListResp recommendListResp = PayRecommendClient.initPayTypeInfo(mk, wid, userType, dealTotalFee, itemCodes, sellerIds, categorys, supportcods);
		if (recommendListResp != null && recommendListResp.getPayRecommendList()!= null && !recommendListResp.getPayRecommendList().isEmpty()){
			payRecommends = recommendListResp.getPayRecommendList();
		}
		return payRecommends;
	}
	
	
	
	/**
	 * 购物车下单业务方法
	 */
	@Override
	public DealFavorVo confirmOrderCmdyV3(ConfirmOrderPo req, long uin, String sk, int scene, String dealStrs, RecvAddr addrList, ReceiverAddress defaultAddr) throws BusinessException {
	
		DealPromoteBiz dealPromoteBiz = (DealPromoteBiz)SpringHelper.getBean("dealPtBiz");
		SFBiz sfBiz = (SFBiz)SpringHelper.getBean("sfBiz");
		DealFavorVo favorVo = new DealFavorVo();
		Vector<ReceiverAddress> addressList = new Vector<ReceiverAddress>();
		addressList.add(defaultAddr);
		//分单参数校验
		CmdyValidate.validateCmdyConfirmOrder(req);
		CmdyItemListPo po = CmdyItemListPo.disSerialize(req.getItemList());
		if (po == null) {
			favorVo.setErrorCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
			favorVo.setErrorMsg("待分单的商品列表参数错误");
			return favorVo;
		}
		po.setPayType(req.getPayType());
		//分单
		ConfirmOrderResp coResp = CmdyClient.cmdyConfirmOrderV2(req.getBuyerUin(),
				req.getSk(), po, defaultAddr.getRegionId());
		if (coResp == null || coResp.getResult() != 0) {
			if (coResp.getResult() == 110) {
				favorVo.setErrorCode(ErrConstant.ERRCODE_DEAL_FINDADDR_FAIL);
				favorVo.setErrorMsg("部分商品无法下单, 请稍后再试.");
				return favorVo;
			} else if (coResp.getResult() == 201) {
				favorVo.setErrorCode(ErrConstant.ERRCODE_CMDY_ADD_FAIL);
				favorVo.setErrorMsg("太多未结束支付订单.");
				return favorVo;
			} else if (coResp.getResult() == 196) {
				favorVo.setErrorCode(ErrConstant.ERRCODE_CMDY_ADD_FAIL);
				favorVo.setErrorMsg("买卖家相同.");
				return favorVo;
			} else {
				favorVo.setErrorCode(ErrConstant.ERRCODE_CMDY_ADD_FAIL);
				favorVo.setErrorMsg("批价失败, 请稍后再试.");
				return favorVo;
			}
		}
		ConfirmOrderVo vo = new ConfirmOrderVo();
		vo.setRedPackages(CmdyUtil.convert(coResp,
					DealConstant.REDPACKET_AND_COUPON));
		vo.setRecv(addrList);
		vo.setDefaultAddr(defaultAddr);
		Vector<ConfirmPackageVo> vcp = CmdyConverter.convertPackageV2(
				CmdyUtil.getDealViewList(coResp, po), req.getPayType(), BizConstant.NORMAL_DEAL_ID_START);
		vo.setNormalPackages(vcp);
		vo.setProblemPackages(CmdyConverter.convertPackageV2(
				CmdyUtil.getProblemDealViewList(coResp), req.getPayType(), BizConstant.PROBLEM_DEAL_ID_START));
//		vo.lastCalculateTotalPrice();
		DealPromotePo dealPo = null;
		//切换优惠活动进行批价
		if(scene == DealConstant.DEAL_SCENE_CHANGEPROMOTIONS){
			dealPo = DealPromotePo.createInstance(uin, dealStrs, req.getVer(),req.getUseRedPackage());
			if (dealPo.getErrorCode() == 0) {
				dealPo.setReqSource(BizConstant.DEAL_PROMOTE_FROM_ORDER_CONFIRM);
				favorVo = dealPromoteBiz.queryPromote(dealPo, vo, req.getSk(), req.getMk());// "@UVdu8Mcoc",// getMk());//
			}else{
				favorVo.setErrorCode(dealPo.getErrorCode());
				favorVo.setErrorMsg(dealPo.getErrorMsg());
				return favorVo;
			}
		}else{//初始化场景、切换收货地址场景和切换支付方式场景都要先分单再批价
			// 批价
			try {
				// 1.分单结果转换成批价参数
				dealPo = DealPromoteUtil.convert(uin, req.getPayType(), vo,req.getUseRedPackage());
			} catch (Exception e) {
				favorVo.setErrorCode(ErrConstant.ERRCODE_PROMOTE_OTHER_ERROR);
				favorVo.setErrorMsg("分单输出数据转批价输入数据失败.");
				return favorVo;
			}
			//分单之后正常商品列表不为空时进行批价
			if (dealPo != null && !dealPo.getDealPromoteList().isEmpty()) {
				// 2.批价
				dealPo.setReqSource(BizConstant.DEAL_PROMOTE_FROM_FIRST_ORDER_CONFIRM);
				favorVo = dealPromoteBiz.queryPromote(dealPo, vo, req.getSk(), req.getMk());
				// 3.使用了多个红包需重新批价
				if (favorVo.getQueryPromoteAgain()) {
					dealPo = DealPromoteUtil.convert(uin, req.getPayType(), favorVo,req.getUseRedPackage());
					dealPo.setReqSource(BizConstant.DEAL_PROMOTE_FROM_ORDER_CONFIRM);
					favorVo = dealPromoteBiz.queryPromote(dealPo, vo, req.getSk(), req.getMk());
				}
			}
			//add by wanghao 新增异常商品列表
			Vector<TradeFavorVo> problemDealList = CmdyConverter.convertBadCommodityList(CmdyUtil.getProblemDealViewList(coResp));
			favorVo.setProblemDealList(problemDealList);
		}
		//初始化场景返回默认收货地址
		if(scene == DealConstant.DEAL_SCENE_INITIALIZE){
			favorVo.setAddressList(addressList);
		}
		//货到付款时根据批价结果计算货到付款服务费、运费和优惠费
		Vector<CodShipPo> sftplDetail = new Vector<CodShipPo>();
		if(req.getPayType() == 1){
			long totalPrice = favorVo.getTotalPrice();
			long totalCodFee = 0;
			long totalFavFee = 0;
			long totalMailFee = 0;
			if(!favorVo.getDealList().isEmpty()){
				Vector<TradeFavorVo> dealList = favorVo.getDealList();
				for(TradeFavorVo deal : dealList){
					Vector<ItemPromoteResultVo> promoteResults = deal.getItemPromoteResult();
					long dealTotalFee = deal.getTradeFee();
					int isMailFree = 0;
					Vector<FavorExVo> shopPromotions = deal.getShopPromotions();
					if(!shopPromotions.isEmpty()){
						for(FavorExVo shopPromotion : shopPromotions){
							long checkStat = shopPromotion.getCheckStat();
							//当前促销活动选中并且促销活动为包邮活动
							if(checkStat > 0 && shopPromotion.getPostStat() > 0){
								isMailFree = 1;
								break;
							}
						}
					}
					for(ItemPromoteResultVo promoteResult : promoteResults){
						String ci = promoteResult.getItemId();
						long buyNum = promoteResult.getNum();
						CodShipPo codShipPo = null;
						try {
							codShipPo = sfBiz.calcCodShipfeeWithCgi(defaultAddr.getAddressId(), ci, dealTotalFee, buyNum, uin, sk, isMailFree);
						} catch (BusinessException e) {
							favorVo.setErrorCode(e.getErrCode());
							favorVo.setErrorMsg(e.getErrMsg());
							return favorVo;
						}
						deal.setTradeFee(codShipPo.getFinalFee());
						sftplDetail.add(codShipPo);
						totalCodFee += codShipPo.getCodCountFee();
						totalMailFee += codShipPo.getFee();
						totalFavFee += codShipPo.getFavFee();
					}
				}
			}
			//货到付款购物车最终货款
			totalPrice += (totalCodFee + totalMailFee - totalFavFee);
			favorVo.setTotalPrice(totalPrice);
			favorVo.setTotalCodFee(totalCodFee);
			favorVo.setTotalMailFee(totalMailFee);
			favorVo.setTotalFavFee(totalFavFee);
			favorVo.setSftplDetail(sftplDetail);
		} else {//在线支付订单结果计算邮费信息
			long totalPrice = favorVo.getTotalPrice();
			long totalMailFee = 0;
			if(!favorVo.getDealList().isEmpty()){
				Vector<TradeFavorVo> dealList = favorVo.getDealList();
				for (TradeFavorVo deal : dealList) {
					boolean isMailFree = false;
					Vector<FavorExVo> shopPromotions = deal.getShopPromotions();
					if(!shopPromotions.isEmpty()){
						for(FavorExVo shopPromotion : shopPromotions){
							long checkStat = shopPromotion.getCheckStat();
							//当前促销活动选中并且促销活动为包邮活动
							if(checkStat > 0 && shopPromotion.getPostStat() > 0){
								isMailFree = true;
								break;
							}
						}
					}
					if (scene == DealConstant.DEAL_SCENE_CHANGEPROMOTIONS){//在线支付切换优惠活动传参带有邮费信息
						if (!isMailFree && dealPo != null && !dealPo.getMailInfos().isEmpty()){
							Map<Long, ShipCalcVo> mailInfos = dealPo.getMailInfos();
							ShipCalcVo shipCalcVo = mailInfos.get(deal.getDealId());
							if (null != shipCalcVo){
								long fee = shipCalcVo.getFee();
								totalMailFee += fee;
								deal.setTradeFee(deal.getTradeFee()+fee);
							}
						}
					}else{//其他场景在线支付初始化邮费信息
						Vector<ShipCalcVo> shipCalcInfos = deal.getShipCalcInfos();
						if (!shipCalcInfos.isEmpty() && !isMailFree){
							long fee = shipCalcInfos.get(0).getFee();
							totalMailFee += fee;
							deal.setTradeFee(deal.getTradeFee()+fee);
						}
					}
				}
			}
			totalPrice += totalMailFee;
			favorVo.setTotalPrice(totalPrice);
		}
		//判断订单是否支持微信支付
		if (favorVo.getDealList().size() == 0 && favorVo.getProblemDealList().size() > 0) {
			boolean wxSupportFlag = true ;			
			for(TradeFavorVo tradeFavorVo:favorVo.getProblemDealList()) {
				wxSupportFlag = wxSupportFlag&&(tradeFavorVo.getIsSupportWXPay()==1) ;
			}
			if (wxSupportFlag) {
				favorVo.setIsSupportWXPay(1) ;
			} else {
				favorVo.setIsSupportWXPay(0) ;
			}					
		}		
		return favorVo;
	}

	/**
	 * 该方法用于京东收银台下单
	 */
	public Vector<MakeOrderVo> makeOrderByJd(MakeOrderPo req,
			ReceiverAddress defaultAddr) throws BusinessException {
		if (req == null || (req.validate().size() > 0)) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_INVALID_PARAMETER, "请求参数有问题");
		}

		// 调购物车下单接口
		CmdyMakeOrderPo po = CmdyMakeOrderPo.deSerialize(req);

		po.setRecvAddrId(req.getAddressid());
		MakeOrderResp cmdyMakeOrderRsp = CmdyClient.cmdyMakeOrderByJd(
				req.getBuyerUin(), req.getSkey(), po,defaultAddr);
		Log.run.info("cmdyMakeOrderRsp.getResult()----------------------"+cmdyMakeOrderRsp!=null?cmdyMakeOrderRsp.getResult():"");
		if (cmdyMakeOrderRsp == null || cmdyMakeOrderRsp.getResult() != 0) {
			if (cmdyMakeOrderRsp != null && cmdyMakeOrderRsp.getResult() == 14) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_MAKEORDER_FAIL_PARAM,
						"购物车下单失败");
			} else if (cmdyMakeOrderRsp != null
					&& cmdyMakeOrderRsp.getResult() == 196) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_ADD_FAIL_DW, "此商品已下架，您不能购买");
			} else if (cmdyMakeOrderRsp != null
					&& cmdyMakeOrderRsp.getResult() == 33) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_MAKEORDER_FAIL_OVER_LIMIT,
						"商品数量超出最大范围");
			} else{
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_MAKEORDER_FAIL, "购物车下单失败");
			}
		}
		Vector<OrderView> deals = cmdyMakeOrderRsp.getResponse()
				.getOrderViewInfo();
		if (deals == null) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_CMDY_MAKEORDER_FAIL, "购物车下单返回的数据不正确");
		}
		Vector<MakeOrderVo> vos = new Vector<MakeOrderVo>();

		for (OrderView ov : deals) {
			MakeOrderVo vo = new MakeOrderVo();
			vo.setTotalFee(ov.getTotalFee());
			vo.setDealCode(ov.getDealId());
			vo.setSellerUin(ov.getShopViewInfo().getSellerUin());
			vos.add(vo);
		}
		return vos;

	}
	
	
	
	/**、
	 * 多店铺下单--京东收银台
	 * @param req
	 * @param defaultAddr
	 * @return
	 * @throws BusinessException
	 */
	public Vector<OrderView> multiMakeOrderByJd(MakeOrderPo req,
			ReceiverAddress defaultAddr) throws BusinessException {
		if (req == null || (req.validate().size() > 0)) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_INVALID_PARAMETER, "请求参数有问题");
		}

		// 调购物车下单接口
		CmdyMakeOrderPo po = CmdyMakeOrderPo.deSerialize(req);

		po.setRecvAddrId(req.getAddressid());
		MakeOrderResp cmdyMakeOrderRsp = CmdyClient.cmdyMakeOrderByJd(
				req.getBuyerUin(), req.getSkey(), po,defaultAddr);
		Log.run.info("cmdyMakeOrderRsp.getResult()----------------------"+cmdyMakeOrderRsp!=null?cmdyMakeOrderRsp.getResult():"");
		if (cmdyMakeOrderRsp == null || cmdyMakeOrderRsp.getResult() != 0) {
			if (cmdyMakeOrderRsp != null && cmdyMakeOrderRsp.getResult() == 14) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_MAKEORDER_FAIL_PARAM,
						"购物车下单失败");
			} else if (cmdyMakeOrderRsp != null
					&& cmdyMakeOrderRsp.getResult() == 196) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_ADD_FAIL_DW, "此商品已下架，您不能购买");
			} else if (cmdyMakeOrderRsp != null
					&& cmdyMakeOrderRsp.getResult() == 33) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_MAKEORDER_FAIL_OVER_LIMIT,
						"商品数量超出最大范围");
			} else{
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CMDY_MAKEORDER_FAIL, "购物车下单失败");
			}
		}
		Vector<OrderView> deals = cmdyMakeOrderRsp.getResponse()
				.getOrderViewInfo();
		if (deals == null) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_CMDY_MAKEORDER_FAIL, "购物车下单返回的数据不正确");
		}
		return deals;

	}
}
