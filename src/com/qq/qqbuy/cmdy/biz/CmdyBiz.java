package com.qq.qqbuy.cmdy.biz;

import java.util.Vector;

import com.qq.qqbuy.cmdy.po.CmdyAddVo;
import com.qq.qqbuy.cmdy.po.CmdyListVo;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.deal.po.ConfirmOrderPo;
import com.qq.qqbuy.deal.po.ConfirmOrderVo;
import com.qq.qqbuy.deal.po.MakeOrderPo;
import com.qq.qqbuy.deal.po.MakeOrderVo;
import com.qq.qqbuy.dealpromote.po.DealFavorVo;
import com.qq.qqbuy.recvaddr.po.ReceiverAddress;
import com.qq.qqbuy.recvaddr.po.RecvAddr;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.OrderView;
import com.qq.qqbuy.thirdparty.idl.pay.recommend.protocol.PayRecommend;

/**
 * @author winsonwu
 * @Created 2013-2-18 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public interface CmdyBiz {

	public CmdyAddVo add(long buyerUin, String itemCode, String itemAttr, long buyNum, boolean addForce, long secenid)
			throws BusinessException;

	public CmdyListVo list(long buyerUin) throws BusinessException;

	public void modifyItemNum(long buyerUin, String itemList) throws BusinessException;

	public void remove(long buyerUin, int payType, String itemList) throws BusinessException;

	public int cartNum(long buyerUin) throws BusinessException;

	public ConfirmOrderVo confirmOrderCmdy(ConfirmOrderPo req, String sk) throws BusinessException;

	public Vector<MakeOrderVo> makeOrder(MakeOrderPo req) throws BusinessException;
	
	/**
	 * 购物车多店铺下单
	 * @param req
	 * @return
	 * @throws BusinessException
	 */
	public Vector<OrderView> multiMakeOrder(MakeOrderPo req) throws BusinessException;

	public DealFavorVo confirmOrderCmdyV2(ConfirmOrderPo req, long uin, String sk)
			throws BusinessException;

	public DealFavorVo confirmOrderCmdyV3(ConfirmOrderPo po, long uin,
			String sk, int scene, String dealStrs, RecvAddr addrList, ReceiverAddress defaultAddr)throws BusinessException;
	/**
	 * 京东收银台下单
	
	 * @Title: makeOrderByJd 
	
	 * @Description: TODO(这里用一句话描述这个方法的作用) 
	
	 * @param @param req
	 * @param @param defaultAddr
	 * @param @return
	 * @param @throws BusinessException    设定文件 
	
	 * @return Vector<MakeOrderVo>    返回类型 
	
	 * @throws
	 */
	public Vector<MakeOrderVo> makeOrderByJd(MakeOrderPo req,ReceiverAddress defaultAddr)throws BusinessException;
	
	/**
	 * 多店铺下单--京东收银台
	 * @param req
	 * @param defaultAddr
	 * @return
	 * @throws BusinessException
	 */
	public Vector<OrderView> multiMakeOrderByJd(MakeOrderPo req,ReceiverAddress defaultAddr)throws BusinessException;
	
	
	
	/**
	 * 获取推荐支付方式列表
	 * @param mk 设备码
	 * @param wid 买家qq
	 * @param userType 用户类型  用户类型：1=QQ qq账号及绑定了qq账号的微信账号(login_type=wx切uin<39亿); 2=WX wx账号非绑定qq(login_type=wx切uin>39亿); 3=JD JD账号
	 * @param dealTotalFee 订单总价
	 * @param itemCodes 订单商品id串,用半角逗号分隔
	 * @param sellerIds 订单商品卖家qq号串,用半角逗号分隔(顺序跟itemCodes一一对应)
	 * @param categorys 订单的商品类目,用半角逗号分隔(顺序跟itemCodes一一对应)
	 * @param supportcods 是否支持cod	串, 1=支持	2=不支持 ,用半角逗号分隔，例如1,2,1(顺序跟itemCodes一一对应)
	 * @return
	 */
	public Vector<PayRecommend> getPayRecommends(String mk,long wid,int userType,long dealTotalFee,String itemCodes,String sellerIds,String categorys,String supportcods);
}
