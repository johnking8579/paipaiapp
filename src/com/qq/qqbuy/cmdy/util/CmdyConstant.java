package com.qq.qqbuy.cmdy.util;

/**
 * @author winsonwu
 * @Created 2013-2-4 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public interface CmdyConstant {

	// 购物车下单接口ExtInfo中标注来源微购的key和value
	public static final String CMDY_MAKEORDER_EXTINFO_WEIGOU_KEY = "role";
//	public static final String CMDY_MAKEORDER_EXTINFO_WEIGOU_VALUE = "mobileLife";
	public static final String CMDY_MAKEORDER_EXTINFO_WEIGOU_VALUE_SCENEID = "appOrder";
	public static final String CMDY_MAKEORDER_EXTINFO_WEIGOU_VALUE_PAIPAI = "mobileApp";
	
	public static final String CMDY_MAKEORDER_EXTINFO_WEIGOU_UUID = "mobileapp.uuid" ;

	// 卖家属性：OrderSellerPropMask
	public static final int ORDER_USER_QQVIP_SELLER = 1; // 卖家是QQ会员官方店
	public static final int ORDER_USER_XIAOBAO_SELLER = 2; // 卖家是消保卖家
	public static final int ORDER_USER_B2C_SELLER = 4; // 卖家是B2C卖家
	public static final int ORDER_USER_SELLER_HAS_PROMOTION = 8; // 卖家有参加自主店铺营销
	public static final int ORDER_USER_SELLER_OLD_PROMOTION = 16; // 促销参加旧的促销活动
	public static final int ORDER_USER_SELLER_HAS_SHOPVIP = 32; // 卖家有参加店铺vip
	public static final int ORDER_USER_COD_SELLER = 64; // 卖家支持货到付款
	public static final int ORDER_USER_GREENVIP_SELLER = 0x80; // 卖家是拍拍绿钻卖家

}
