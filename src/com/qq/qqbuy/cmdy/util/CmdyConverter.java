package com.qq.qqbuy.cmdy.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;

import net.sf.json.JSONObject;

import com.paipai.lang.uint16_t;
import com.qq.qqbuy.cmdy.po.CmdyItemVo;
import com.qq.qqbuy.cmdy.po.MutiPriceVo;
import com.qq.qqbuy.cmdy.po.PromotionRuleVo;
import com.qq.qqbuy.cmdy.po.ShopVo;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.PaiPaiConfig;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.deal.po.ConfirmPackageVo;
import com.qq.qqbuy.deal.po.ShipCalcVo;
import com.qq.qqbuy.deal.util.DealConstant;
import com.qq.qqbuy.deal.util.DealUtil;
import com.qq.qqbuy.dealpromote.po.ItemPromoteResultVo;
import com.qq.qqbuy.dealpromote.po.TradeFavorVo;
import com.qq.qqbuy.item.constant.TransportType;
import com.qq.qqbuy.item.po.ItemBO;
import com.qq.qqbuy.item.po.ItemStockBo;
import com.qq.qqbuy.item.util.DispFormater;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.CommodityView;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.DealViewData;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.MultiPrice;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.PromotionRule;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.ShipFeeInfo;
import com.qq.qqbuy.thirdparty.idl.item.price.ItemPriceClient;
import com.qq.qqbuy.thirdparty.idl.item.price.protocol.CommodityPrice;
import com.qq.qqbuy.thirdparty.idl.item.price.protocol.GetCommodityListPriceResp;
import com.qq.qqbuy.thirdparty.idl.item.price.protocol.ScenePrice;
import com.qq.qqbuy.thirdparty.idl.item.price.protocol.SkuPrice;
import com.qq.qqbuy.thirdparty.idl.userinfo.ApiUserClient;
import com.qq.qqbuy.thirdparty.idl.userinfo.UserInfoConstants;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.apiuser.APIUserProfile;

/**
 * @author winsonwu
 * @Created 2013-2-19 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class CmdyConverter {
	private static int PROMOTIONRULE_LIMIT_TYPE_CMDY_FEE = 0;
//	private static int PROMOTION_PROP_FREE_MONEY = 1;
	private static int PROMOTION_PROP_DERATE = 2;
	private static int PROMOTION_PROP_PRESENT = 4;
	private static int PROMOTION_PROP_BARTER = 8;
	private static int PROMOTION_PROP_FREECARRIAGE = 16;
	
	protected static final String WEIDIAN_PRICE_HOST = "party.paipai.com" ;

	//微店商品价格接口
	protected static final String WEIDIAN_PRICE_URL = "http://party.paipai.com/cgi-bin/v2/wxprice_favor_query";
	
	// 商品在促销中
	private static int ORDER_CMDY_ONPROMOTION = 1;

	public static Vector<ConfirmPackageVo> convertPackage(
			Vector<DealViewData> dealList, int payType) {
		Vector<ConfirmPackageVo> ret = new Vector<ConfirmPackageVo>();
		if (dealList != null && dealList.size() > 0) {
			for (DealViewData deal : dealList) {
				// 根据卖家进行过滤
				ConfirmPackageVo vo = new ConfirmPackageVo();

				// 运费选择项
				ShipFeeInfo ship = deal.getShipfeeInfo();
				Vector<ShipCalcVo> shipOpts = CmdyUtil.genShipCalcInfoList(
						ship, payType);
				vo.setShipCalcInfos(shipOpts);
				Vector<PromotionRule> vpr = deal.getPromotionactive()
						.getRuleList();
				List<PromotionRuleVo> promotionRules = new ArrayList<PromotionRuleVo>();
				for (PromotionRule pr : vpr) {
					PromotionRuleVo prv = new PromotionRuleVo();
					prv.setRuleId(pr.getRuleId());
					if (PROMOTIONRULE_LIMIT_TYPE_CMDY_FEE == pr.getLimitType()) {
						prv.setLimitNum(0);
						prv.setLimitPrice(pr.getLowLimit());
					} else {
						prv.setLimitPrice(0);
						prv.setLimitNum((int) pr.getLowLimit());
					}
					if (pr.getFreeMoney() != 0) {
						prv.setFreeMoney(pr.getFreeMoney());
						prv.setDiscount(0);
					} else {
						prv.setFreeMoney(0);
						prv.setDiscount(pr.getFreeRebate());
					}
					if ((PROMOTION_PROP_BARTER & pr.getPromotionMask()) != 0) {
						prv.setBarterMoney(pr.getBarterMoney());
					}
					if ((PROMOTION_PROP_FREECARRIAGE & pr.getPromotionMask()) != 0) {
						prv.setFreeCarriage(true);
					} else {
						prv.setFreeCarriage(false);
					}

					if ((PROMOTION_PROP_PRESENT & pr.getPromotionMask()) != 0) {
						prv.setPresent(true);
					} else {
						prv.setPresent(false);
					}
					prv.setDesc(pr.getDesc());
					if (!(payType == 1 && prv.isFreeCarriage())) {
						promotionRules.add(prv);
					}

				}
				vo.setPromotionRules(promotionRules);
				String itemCode = "";
				Vector<CommodityView> commdities = deal.getCmdyViewList();
				if (commdities != null && commdities.size() > 0) {
					for (CommodityView commdity : commdities) {
						CmdyItemVo item = new CmdyItemVo();
						item.setBuyNum(commdity.getCommodityNumber());
						itemCode = commdity.getCommodityId();
						item.setItemCode(commdity.getCommodityId());
						// item.setPrice(commdity.getNormalPrice());
						item.setDisPrice(DispFormater
								.priceDispFormater(commdity.getNormalPrice()));
						item.setRedPacketValue(commdity.getRedPacketValue());
						item.setItemAttr(commdity.getCommodityStockAttribute());
						item.setItemTitle(commdity.getCommodityTitle());
						item.setMainLogoUrl(DealUtil.PP_IMG_URL_PREF
								+ commdity.getCommodityLogo());
						Vector<MultiPrice> multis = commdity
								.getMultiPriceInfo();
						List<MutiPriceVo> mpvos = new ArrayList<MutiPriceVo>();
						long mostPrice = 10000000000000000L;
						MutiPriceVo mostLowPriceVo = new MutiPriceVo();
						for (MultiPrice mp : multis) {
							MutiPriceVo mutiPriceVo = new MutiPriceVo();
							mutiPriceVo.setPriceType(mp.getParticularType());
							mutiPriceVo.setPriceValue(mp.getParticularPrice());
							mpvos.add(mutiPriceVo);
							if (mp.getParticularPrice() < mostPrice) {
								mostPrice = mp.getParticularPrice();
								mostLowPriceVo = mutiPriceVo;
							}
						}
						if (mpvos.size() == 0) {
							MutiPriceVo mutiPriceVo = new MutiPriceVo();
							mutiPriceVo.setPriceType(0);
							mutiPriceVo
									.setPriceValue(commdity.getNormalPrice());
							mutiPriceVo.setPriceTypeDesc("原价");
							mostLowPriceVo = mutiPriceVo;
							mpvos.add(mutiPriceVo);
						}
						item.setMutiPrices(mpvos);
						item.setMostLowPrice(mostLowPriceVo);
						item.setPrice(mostLowPriceVo.getPriceValue());
						// item.setShopName(deal.getShopview().getShopName());
						// item.setSellerUin(deal.getShopview().getSellerUin());

						vo.getItems().add(item);
					}
					vo.setSellerUin(deal.getShopview().getSellerUin());
					vo.setShopName(deal.getShopview().getShopName());
					vo.setShopType((int) deal.getShopview().getShopProperty());
				}
				
				if (payType == 1 && shipOpts != null && shipOpts.size() > 0) {
					ShipCalcVo shipCalcVo = shipOpts.get(0);
					if (shipCalcVo != null && (shipCalcVo.getMailType() == DealConstant.MAIL_TYPE_COD_ZJS || 
							shipCalcVo.getMailType() == DealConstant.MAIL_TYPE_COD_SF)) {
						long pkgCmdyTotalFee = vo.lastCalculateTotalPrice();
						long pkgDealTotalFee = CmdyUtil.getCodFee(itemCode, vo.getSellerUin(), pkgCmdyTotalFee + shipCalcVo.getFee());
						shipCalcVo.setFee(pkgDealTotalFee - pkgCmdyTotalFee);
						shipCalcVo.setName("货到付款: ￥" + DispFormater.priceDispFormater(shipCalcVo.getFee()) + "元");
					}
				}
				ret.add(vo);
			}
		}
		return ret;
	}
	
	public static Vector<ConfirmPackageVo> convertPackageV2(
			Vector<DealViewData> dealList, int payType, int startId) {
		Vector<ConfirmPackageVo> ret = new Vector<ConfirmPackageVo>();
		if (dealList != null && dealList.size() > 0) {
			for (int i = 0; i < dealList.size(); i++) {
				DealViewData deal = dealList.get(i);
				
				// 根据卖家进行过滤
				ConfirmPackageVo vo = new ConfirmPackageVo();
				vo.setDealId(startId + i);

				// 运费选择项
				ShipFeeInfo ship = deal.getShipfeeInfo();
				Vector<ShipCalcVo> shipOpts = CmdyUtil.genShipCalcInfoList(
						ship, payType);
				vo.setShipCalcInfos(shipOpts);
				Vector<PromotionRule> vpr = deal.getPromotionactive()
						.getRuleList();
				List<PromotionRuleVo> promotionRules = new ArrayList<PromotionRuleVo>();
				for (PromotionRule pr : vpr) {
					PromotionRuleVo prv = new PromotionRuleVo();
					prv.setRuleId(pr.getRuleId());
					if (PROMOTIONRULE_LIMIT_TYPE_CMDY_FEE == pr.getLimitType()) {
						prv.setLimitNum(0);
						prv.setLimitPrice(pr.getLowLimit());
					} else {
						prv.setLimitPrice(0);
						prv.setLimitNum((int) pr.getLowLimit());
					}
					if (pr.getFreeMoney() != 0) {
						prv.setFreeMoney(pr.getFreeMoney());
						prv.setDiscount(0);
					} else {
						prv.setFreeMoney(0);
						prv.setDiscount(pr.getFreeRebate());
					}
					if ((PROMOTION_PROP_BARTER & pr.getPromotionMask()) != 0) {
						prv.setBarterMoney(pr.getBarterMoney());
					}
					if ((PROMOTION_PROP_FREECARRIAGE & pr.getPromotionMask()) != 0) {
						prv.setFreeCarriage(true);
					} else {
						prv.setFreeCarriage(false);
					}

					if ((PROMOTION_PROP_PRESENT & pr.getPromotionMask()) != 0) {
						prv.setPresent(true);
					} else {
						prv.setPresent(false);
					}
					prv.setDesc(pr.getDesc());
					if (!(payType == 1 && prv.isFreeCarriage())) {
						promotionRules.add(prv);
					}

				}
				vo.setPromotionRules(promotionRules);
				
				int promotion = 1;
//				String itemCode = "";
				Vector<CommodityView> commdities = deal.getCmdyViewList();
				if (commdities != null && commdities.size() > 0) {
					for (CommodityView commdity : commdities) {
						CmdyItemVo item = new CmdyItemVo();
						
						// 商品不参加促销
						if ((commdity.getOrderMask() & ORDER_CMDY_ONPROMOTION) == 0){
							promotion = 0;
						}
						
						item.setSceneId(commdity.getSceneId());
						item.setBuyNum(commdity.getCommodityNumber());
//						itemCode = commdity.getCommodityId();
						item.setItemCode(commdity.getCommodityId());
						// item.setPrice(commdity.getNormalPrice());
						item.setDisPrice(DispFormater
								.priceDispFormater(commdity.getNormalPrice()));
						item.setRedPacketValue(commdity.getRedPacketValue());
						item.setItemAttr(commdity.getCommodityStockAttribute());
						item.setItemTitle(commdity.getCommodityTitle());
						item.setMainLogoUrl(DealUtil.PP_IMG_URL_PREF
								+ commdity.getCommodityLogo());
						Vector<MultiPrice> multis = commdity
								.getMultiPriceInfo();
						List<MutiPriceVo> mpvos = new ArrayList<MutiPriceVo>();
						long mostPrice = 10000000000000000L;
						MutiPriceVo mostLowPriceVo = new MutiPriceVo();
						for (MultiPrice mp : multis) {
							MutiPriceVo mutiPriceVo = new MutiPriceVo();
							mutiPriceVo.setPriceType(mp.getParticularType());
							mutiPriceVo.setPriceValue(mp.getParticularPrice());
							mpvos.add(mutiPriceVo);
							if (mp.getParticularPrice() < mostPrice) {
								mostPrice = mp.getParticularPrice();
								mostLowPriceVo = mutiPriceVo;
							}
						}
						if (mpvos.size() == 0) {
							MutiPriceVo mutiPriceVo = new MutiPriceVo();
							mutiPriceVo.setPriceType(0);
							mutiPriceVo
									.setPriceValue(commdity.getNormalPrice());
							mutiPriceVo.setPriceTypeDesc("原价");
							mostLowPriceVo = mutiPriceVo;
							mpvos.add(mutiPriceVo);
						}
						item.setMutiPrices(mpvos);
						item.setMostLowPrice(mostLowPriceVo);
						item.setPrice(mostLowPriceVo.getPriceValue());
						// item.setShopName(deal.getShopview().getShopName());
						// item.setSellerUin(deal.getShopview().getSellerUin());

						vo.getItems().add(item);
					}
					vo.setSellerUin(deal.getShopview().getSellerUin());
					vo.setShopName(deal.getShopview().getShopName());
					vo.setShopType((int) deal.getShopview().getShopProperty());
					
					vo.setPromotion(promotion);
				}
				
//				if (payType == 1 && shipOpts != null && shipOpts.size() > 0) {
//					ShipCalcVo shipCalcVo = shipOpts.get(0);
//					if (shipCalcVo != null && (shipCalcVo.getMailType() == DealConstant.MAIL_TYPE_COD_ZJS || 
//							shipCalcVo.getMailType() == DealConstant.MAIL_TYPE_COD_SF)) {
//						long pkgCmdyTotalFee = vo.lastCalculateTotalPrice();
//						long pkgDealTotalFee = CmdyUtil.getCodFee(itemCode, vo.getSellerUin(), pkgCmdyTotalFee + shipCalcVo.getFee());
//						shipCalcVo.setFee(pkgDealTotalFee - pkgCmdyTotalFee);
//						shipCalcVo.setName("货到付款: ￥" + DispFormater.priceDispFormater(shipCalcVo.getFee()) + "元");
//					}
//				}
				ret.add(vo);
			}
		}
		return ret;
	}

	public static Vector<ShopVo> convert(Vector<DealViewData> dealList, long wid) {
		
		Vector<ShopVo> ret = new Vector<ShopVo>();
		if (dealList != null && dealList.size() > 0) {
			for (DealViewData deal : dealList) {
				ShopVo shopVo = new ShopVo();
				Long sellerUin = deal.getShopview().getSellerUin() ;
				Vector<CommodityView> commdities = deal.getCmdyViewList();
				List<CmdyItemVo> items = new ArrayList<CmdyItemVo>();
				if (commdities != null && commdities.size() > 0) {
					for (CommodityView commdity : commdities) {
						CmdyItemVo item = new CmdyItemVo();
						item.setBuyNum(commdity.getCommodityNumber());
						item.setItemCode(commdity.getCommodityId());
						item.setPrice(commdity.getNormalPrice());
						item.setNormalPrice(commdity.getNormalPrice());
						item.setDisPrice(DispFormater
								.priceDispFormater(commdity.getNormalPrice()));
						item.setItemAttr(commdity.getCommodityStockAttribute());
						item.setItemTitle(commdity.getCommodityTitle());
						String imgUrl80x80 = DealUtil.PP_IMG_URL_PREF + commdity.getCommodityLogo();
						//返回图片url 80x80-->120x120
						item.setMainLogoUrl(DealUtil.convert80x80To120x120Img(imgUrl80x80));
						item.setMaxNum(commdity.getCommodityMaxNumber());
						item.setSupportCod((commdity.getOrderMask() & 0x40000) > 0);
						List<MutiPriceVo> mpvos = new ArrayList<MutiPriceVo>();
						Vector<MultiPrice> multis = commdity
								.getMultiPriceInfo();
						long mostPrice = 10000000000000000L;
						
						MutiPriceVo mostLowPriceVo = new MutiPriceVo();
						
						/*
						 * 如果有场景价，则不再计算其他价格，直接显示场景价;
						 * 刘本龙2015-5-11
						 */
						int sceneId = 0;//.....................................
						String itemsceneId = commdity.getMapCmdyReserves().get("itemsceneId");
						if(StringUtils.isNotBlank(itemsceneId)){
							sceneId = Integer.parseInt(itemsceneId);
						}
						
						item.setSceneId(sceneId);
						
						if(sceneId != 0){
							Vector<String> idList = new Vector<String>();
							idList.add(commdity.getCommodityId()) ;
							GetCommodityListPriceResp resp = ItemPriceClient.getItemPrice(sceneId, 1, wid, sellerUin, idList);
							for(CommodityPrice commodityPrice: resp.getCommodityPriceList()) {
								for (SkuPrice skuPrice: commodityPrice.getSkuPriceList()) {
									if(commdity.getCommodityStockAttribute().equals(skuPrice.getDesc())){//根据sku过滤
//										Vector<ScenePrice> vector = skuPrice.getScenePriceList();
//										for (ScenePrice scenePrice : vector) {
//											if(scenePrice.getId() == sceneId){//找到sceneId对应的价格
//												MutiPriceVo mutiPriceVo = new MutiPriceVo();
////												mutiPriceVo.setPriceType(mp.getParticularType());
//												mutiPriceVo.setPriceValue(scenePrice.getPrice());
//												mpvos.add(mutiPriceVo);
//												mostLowPriceVo = mutiPriceVo;
//											}
//										}
										
										ScenePrice recommendedScenePrice = skuPrice.getRecommendedScenePrice();
										if(recommendedScenePrice.getId() == sceneId){//找到sceneId对应的价格
											MutiPriceVo mutiPriceVo = new MutiPriceVo();
											mutiPriceVo.setPriceValue(recommendedScenePrice.getPrice());
											mpvos.add(mutiPriceVo);
											mostLowPriceVo = mutiPriceVo;
										}
									}
								}
							}
						}else{
							for (MultiPrice mp : multis) {//记录返回值中的最低价
								MutiPriceVo mutiPriceVo = new MutiPriceVo();
								mutiPriceVo.setPriceType(mp.getParticularType());
								mutiPriceVo.setPriceValue(mp.getParticularPrice());
								mpvos.add(mutiPriceVo);
								if (mp.getParticularPrice() < mostPrice) {
									mostPrice = mp.getParticularPrice();
									mostLowPriceVo = mutiPriceVo;
								}
							}
							if (mpvos.size() == 0) {
								MutiPriceVo mutiPriceVo = new MutiPriceVo();
								mutiPriceVo.setPriceType(0);
								mutiPriceVo
										.setPriceValue(commdity.getNormalPrice());
								mutiPriceVo.setPriceTypeDesc("原价");
								mostLowPriceVo = mutiPriceVo;
								mpvos.add(mutiPriceVo);
							}
							
							
							
							Long resultPrice = mostLowPriceVo.getPriceValue() ;
							Long wdPrice = wxPrice(sellerUin, item.getItemCode(), resultPrice) ;//计算微店价
							if (wdPrice < resultPrice) {
								mostLowPriceVo.setPriceValue(wdPrice) ;
								mostLowPriceVo.setPriceType(105L);
								mostLowPriceVo.setPriceTypeDesc("微店价") ;
							}
						}
						
						item.setMutiPrices(mpvos);
						
						item.setMostLowPrice(mostLowPriceVo);
						item.setPrice(mostLowPriceVo.getPriceValue());
						items.add(item);
					}
					shopVo.setItems(items);
					shopVo.setSellerUin(sellerUin);
					shopVo.setShopName(deal.getShopview().getShopName());
					shopVo.setShopType((int) deal.getShopview()
							.getShopProperty());
				}
				ret.add(shopVo);
			}
		}
		return ret;
	}

	public static int getNum(Vector<DealViewData> dealList) {
		int num = 0;
		if (dealList != null && dealList.size() > 0) {
			for (DealViewData deal : dealList) {
				// 根据卖家进行过滤
				Vector<CommodityView> commdities = deal.getCmdyViewList();
				num += commdities.size();
				// if (commdities != null && commdities.size() > 0) {
				// for (CommodityView commdity : commdities) {
				// num += commdity.getCommodityNumber();
				// }
				// }
			}
		}
		return num;
	}

	/**
	 * 根据IDL接口返回数据转化为异常商品列表
	 * @param problemDealViewList
	 * @return
	 */
	public static Vector<TradeFavorVo> convertBadCommodityList(
			Vector<DealViewData> problemDealViewList) {
		Vector<TradeFavorVo> badCommodityList = new Vector<TradeFavorVo>();
		if(null != problemDealViewList && !problemDealViewList.isEmpty()){
			for(DealViewData data : problemDealViewList){
				TradeFavorVo favorVo = new TradeFavorVo();
				favorVo.setErrType(data.getResult());
				favorVo.setErrMsg(ErrorMsg.valueOf(data.getResult()).getErrorMeg());
				favorVo.setShopName(data.getShopview().getShopName());
				favorVo.setSellerUin(data.getShopview().getSellerUin());
				Vector<CommodityView> list = data.getCmdyViewList();
				Vector<ItemPromoteResultVo> itemList = new Vector<ItemPromoteResultVo>();
				for(CommodityView commodity : list){
					ItemPromoteResultVo item = new ItemPromoteResultVo();
					item.setItemId(commodity.getCommodityId());
					item.setPrice(commodity.getNormalPrice());
					item.setItemName(commodity.getCommodityTitle());
					item.setNum(commodity.getCommodityNumber());
					item.setMainLogo("http://image.paipai.com/"+commodity.getCommodityLogo());
					itemList.add(item);
				}
				
				APIUserProfile profile = ApiUserClient.getUserSimpleInfo(favorVo.getSellerUin());
				int isSupportWXPay = 0;
				if (profile != null) {
					int sellerPropForWeiXin = 0 ;
					int sellerPropForQQGou = 0 ;
					uint16_t keyForWeiXin = new uint16_t(UserInfoConstants.NEW_USER_PROP_BOS_WEIXIN_PAY);
					uint16_t keyForQQGou = new uint16_t(UserInfoConstants.NEW_USER_PROP_BOS_MEMBER_PRIVILEGE);
					
					if (profile.getProp().get(keyForWeiXin) != null) {
						sellerPropForWeiXin = profile.getProp().get(keyForWeiXin).intValue();
					}
					if (profile.getProp().get(keyForQQGou) != null) {
						sellerPropForQQGou = profile.getProp().get(keyForQQGou).intValue();
					}
					if (sellerPropForQQGou == 0 && sellerPropForWeiXin == 1) {
						isSupportWXPay = 1 ;
					} else {
						isSupportWXPay = 0 ;
					}
				}
				favorVo.setIsSupportWXPay(isSupportWXPay) ;				
				favorVo.setItemPromoteResult(itemList);
				badCommodityList.add(favorVo);
			}
		}
		return badCommodityList;
	}
	
	/**
	 * 購物車結算返回異常商品，對應異常信息，來自c2c_errno_define.h
	 * @author wanghao5
	 *
	 */
	enum ErrorMsg{
		
		ERR_APP_ORDER_NORMAL_SHIPTMP_PROPERTY(0x1002,"收货地址不在商品的送货地址列表中"),
		ERR_APP_ORDER_RECV_ADDR_NOT_SUPPORT(0x6D,"收货地址不在商品的送货地址列表中"),
		ERR_APP_ADPTOR_RESOURCE_NOT_EXIST(0x1000,"商品已下架"),
		ERR_DB_COMMODITY_NOT_EXIST(0xfe,"商品已下架");  
		
		private final long errorCode;
		private final String errorMeg;

		private ErrorMsg(int errorCode, String errorMeg) {
			this.errorCode = errorCode;
			this.errorMeg = errorMeg;
		}

		public static ErrorMsg valueOf(long errorCode) {
			for (ErrorMsg type : values()) {
				if (type.getErrorCode() == errorCode) {
					return type;
				}
			}
			return ERR_APP_ORDER_NORMAL_SHIPTMP_PROPERTY;
		}

		public long getErrorCode() {
			return errorCode;
		}

		public String getErrorMeg() {
			return errorMeg;
		}
	}
	public static void main(String[] args) {
		Long price = wxPrice(1275000334L, "0EF6FE4B00000000000000000BEC393A", 100L);
		System.out.println(price);
	}
	
	public static Long wxPrice(Long sellerUin, String ic, Long oldPrice) {
		Log.run.debug("CmdyConverter.wxPrice()::sellerUin:"+sellerUin+";ic:"+ ic+";oldPrice:"+oldPrice);
		// TODO Auto-generated method stub
		String resultStr = "";
		StringBuffer getUrl = new StringBuffer();
		getUrl.append(WEIDIAN_PRICE_URL).append("?type=0&abletype=15&uin=").append(sellerUin).append("&itemid=").append(ic);
		Long resultPrice = oldPrice ;
		try {
			resultStr = HttpUtil.get(getUrl.toString().replace(WEIDIAN_PRICE_HOST, PaiPaiConfig.getPaipaiCommonIp()),WEIDIAN_PRICE_HOST, 10000, 15000, "gbk", false,false,"",null);
			if (!StringUtil.isEmpty(resultStr) ) {				
				String result = resultStr.substring(resultStr.indexOf("wxPriceQueryCallBack(") + 21, resultStr.indexOf("})") + 1);
				JSONObject resultJson = JSONObject.fromObject(result);	
				if(null != resultJson && !resultJson.isNullObject()){
					int wdpricestate = resultJson.getInt("wdpricestate");
					int wdpricechange = resultJson.getInt("wdpricechange");
					int wdpricevalue = resultJson.getInt("wdpricevalue");
					if(wdpricestate == 0 && wdpricevalue > 0){//微店折扣生效
						resultPrice = (oldPrice*wdpricevalue)/1000;
						if(wdpricechange == 1){//1、抹掉分
							resultPrice = resultPrice /10 * 10 ;
							/*if(resultPrice >= 10){
								int jue = getNumberAtLast(resultPrice,2);
								resultPrice = resultPrice - jue*10;
							}*/
						}else if(wdpricechange == 2){//2、抹掉角
							/*int fen = getNumberAtLast(resultPrice,1);
							resultPrice = resultPrice - fen;*/
							resultPrice = resultPrice /100 * 100 ;
						}
						resultPrice = (resultPrice <= 0L) ? 1L:resultPrice ;						
					}
				}
			}
		} catch (Exception ex) {
//			Log.run.info("handlePrice error " + ex.toString() + ",resultStr=" + resultStr) ;
		}	
		return resultPrice ;
	}
	
	/**
	 * 获取整数倒数第几位的数字
	 * @param number
	 * @param index
	 * @return
	 */
	public static int getNumberAtLast(long number, int index) {  
		for (; index > 1; index--) {  
			number /= 10;  
		}  
		return (int) (number % 10);  
	}
}
