package com.qq.qqbuy.cmdy.util;

import java.util.Vector;

import com.paipai.lang.uint32_t;
import com.qq.qqbuy.cmdy.po.CmdyItem;
import com.qq.qqbuy.cmdy.po.CmdyItemListPo;
import com.qq.qqbuy.cmdy.po.RedPackageVo;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.C2CDefineConstant;
import com.qq.qqbuy.deal.po.ShipCalcVo;
import com.qq.qqbuy.deal.util.DealConstant;
import com.qq.qqbuy.dealpromote.po.DealFavorVo;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.CartRedPacketPo;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.CommodityView;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.ConfirmOrderResp;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.DealViewData;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.DealViewDataList;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.MultiPrice;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.PromotionActive;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.PromotionRule;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.ShipFeeInfo;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.ShopView;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.ViewCartResp;
import com.qq.qqbuy.thirdparty.idl.codFee.CODFeeClient;
import com.qq.qqbuy.thirdparty.idl.codFee.protocol.CODFee;
import com.qq.qqbuy.thirdparty.idl.codFee.protocol.GetTotalFeeResp;
import com.qq.qqbuy.thirdparty.idl.item.ItemClient;
import com.qq.qqbuy.thirdparty.idl.item.protocol.FetchItemInfoResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ItemBase;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ItemPo_v2;
import com.qq.qqbuy.thirdparty.idl.shippingFeeV2.ShippingfeeV2Client;
import com.qq.qqbuy.thirdparty.idl.shippingFeeV2.protocol.GetTemplateDetailNoLoginResp;
import com.qq.qqbuy.thirdparty.idl.shippingFeeV2.protocol.TemplateDetailInfo;
import com.qq.qqbuy.thirdparty.idl.shippingFeeV2.protocol.TemplateSimpleInfo;

/**
 * @author winsonwu
 * @Created 2013-2-6 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class CmdyUtil implements DealConstant, C2CDefineConstant {
	private static int PROMOTION_PROP_NORMAL = 1;
	private static int PROMOTION_PROP_EXPRESS = 2;
	private static int PROMOTION_PROP_EMS = 4;
	
	private static int COD_FEE_ERROR = 2000;
	private static int COD_FEE_TOO_HIGH01_ERROR = 2081; // 揽收金额 超出上限
	private static int COD_FEE_TOO_HIGH02_ERROR = 2082; // 货到付款代收金额过高
	
	public static Vector<ShipCalcVo> genShipCalcInfoList(ShipFeeInfo shipInfo,
			int paytype) {
		Vector<ShipCalcVo> ret = new Vector<ShipCalcVo>();
		if (shipInfo != null) {

			if (TRANSPORT_BUYER_PAY == shipInfo.getWhoPayShipFee()) { // 买家支持运费，才计算相应选项
				long normalFee = shipInfo.getNormalShipFee();
				long expressFee = shipInfo.getExpressFee();
				long emsFee = shipInfo.getEmsFee();
				// TODO winson，货到付款需要计算货到付款的金额，此时需要使用shipMask参数来判断是否支持货到付款
				if (paytype == 0) {
					
					if ((PROMOTION_PROP_NORMAL & shipInfo.getShipMask()) != 0) {
						ret.add(ShipCalcVo.getShipCalcInfo(
								SHIPCALC_TYPE_NORMAL, normalFee));
					}
					if ((PROMOTION_PROP_EXPRESS & shipInfo.getShipMask()) != 0) {
						ret.add(ShipCalcVo.getShipCalcInfo(
								SHIPCALC_TYPE_EXPRESS, expressFee));
					}
					if ((PROMOTION_PROP_EMS & shipInfo.getShipMask()) != 0) {
						ret.add(ShipCalcVo.getShipCalcInfo(SHIPCALC_TYPE_EMS,
								emsFee));
					}
				} else {
					if ((COD_SHIP_ZJS & shipInfo.getShipMask()) == COD_SHIP_ZJS) {
						ret.add(ShipCalcVo.getShipCalcInfo(
								SHIPCALC_TYPE_COD_ZJS, normalFee));
					}
					if ((COD_SHIP_SF & shipInfo.getShipMask()) == COD_SHIP_SF) {
						ret.add(ShipCalcVo.getShipCalcInfo(
								SHIPCALC_TYPE_COD_SF, normalFee));
					}
					if(((1 << (COD_SHIP_YS - 1)) & shipInfo.getShipMask()) != 0){
						ret.add(ShipCalcVo.getShipCalcInfo(
								SHIPCALC_TYPE_COD_YS, normalFee));
					}
					if(((1 << (COD_SHIP_YT - 1)) & shipInfo.getShipMask()) != 0){
						ret.add(ShipCalcVo.getShipCalcInfo(
								SHIPCALC_TYPE_COD_YT, normalFee));
					}
				}
			}
			if (ret.size() <= 0) {
				ret.add(ShipCalcVo.getShipCalcInfo(SHIPCALC_TYPE_FREE, 0));
			}
		}
		return ret;
	}

	public static boolean mapSellerUin(DealViewData data, long sellerUin) {
		if (sellerUin == 0) {
			// 不进行过滤匹配
			return true;
		}
		if (data != null && data.getShopview() != null
				&& data.getShopview().getSellerUin() == sellerUin) {
			return true;
		}
		return false;
	}

	public static Vector<DealViewData> getDealViewList(ViewCartResp resp) {
		if (resp != null && resp.getDealViewLst() != null) {
			return resp.getDealViewLst().getDealViewDataLst();
		}
		return null;
	}

	/**
	 * 添加场景价
	 * 刘本龙20150512
	 * @param resp
	 * @return
	 */
	public static Vector<DealViewData> getDealViewList(ConfirmOrderResp resp, CmdyItemListPo po) {
		if (resp != null && resp.getDealViewLst() != null) {
			Vector<DealViewData> dealViewDataLst = resp.getDealViewLst().getDealViewDataLst();
			
			Vector<CmdyItem> cmdyItems = po.getCmdyItems();
			
			for (DealViewData dealViewData : dealViewDataLst) {
				Vector<CommodityView> cmdyViewList = dealViewData.getCmdyViewList();
				for (CommodityView commodityView : cmdyViewList) {
					
					for (CmdyItem cmdyItem : cmdyItems) {
						if(cmdyItem.getItemCode().equals(commodityView.getCommodityId()) && 
								cmdyItem.getItemAttr().equals(commodityView.getCommodityStockAttribute())){
							commodityView.setSceneId(cmdyItem.getSceneId());
						}
					}
				}
			}
			
			return dealViewDataLst;
		}
		return null;
	}

	public static Vector<DealViewData> getProblemDealViewList(ViewCartResp resp) {
		if (resp != null && resp.getProblemDealViewLst() != null) {
			return resp.getProblemDealViewLst().getDealViewDataLst();
		}
		return null;
	}

	public static Vector<DealViewData> getProblemDealViewList(
			ConfirmOrderResp resp) {
		if (resp != null && resp.getProblemDealViewLst() != null) {
			return resp.getProblemDealViewLst().getDealViewDataLst();
		}
		return new Vector<DealViewData>();
	}

	public static PromotionRule getFirstPromotionRule(DealViewData dealData) {
		if (dealData != null) {
			PromotionActive promotionActive = dealData.getPromotionactive();
			if (promotionActive != null) {
				Vector<PromotionRule> ruleList = promotionActive.getRuleList();
				if (ruleList != null && ruleList.size() > 0) {
					return ruleList.get(0);
				}
			}
		}
		return null;
	}

	public static MultiPrice getFirstPrice(CommodityView item) {
		if (item != null) {
			Vector<MultiPrice> multiPrices = item.getMultiPriceInfo();
			if (multiPrices != null && multiPrices.size() > 0) {
				return multiPrices.get(0);
			}
		}
		return null;
	}

	public static DealViewData getFirstConfirmDealInfo(ConfirmOrderResp resp) {
		if (resp != null) {
			DealViewDataList dealDataList = resp.getDealViewLst();
			if (dealDataList != null) {
				Vector<DealViewData> datas = dealDataList.getDealViewDataLst();
				if (datas != null && datas.size() > 0) {
					return datas.get(0);
				}
			}
		}
		return null;
	}

	public static Vector<DealViewData> getConfirmDealInfos(ConfirmOrderResp resp) {
		if (resp != null) {
			DealViewDataList dealDataList = resp.getDealViewLst();
			if (dealDataList != null) {
				Vector<DealViewData> datas = dealDataList.getDealViewDataLst();
				return datas;
			}
		}
		return null;
	}

	// 过滤出该卖家的分单数据
	public static Vector<DealViewData> filterConfirmDealInfoBySellerUin(
			ConfirmOrderResp resp, long sellerUin) {
		Vector<DealViewData> sellerDealDatas = new Vector<DealViewData>();
		if (resp != null) {
			DealViewDataList dealDataList = resp.getDealViewLst();
			if (dealDataList != null) {
				Vector<DealViewData> datas = dealDataList.getDealViewDataLst();
				if (datas != null && datas.size() > 0) {
					for (DealViewData data : datas) {
						ShopView shopView = data.getShopview();
						if (shopView != null
								&& sellerUin == shopView.getSellerUin()) {
							sellerDealDatas.add(data);
						}
					}
				}
			}
		}
		return sellerDealDatas;
	}

	// 1:会员,2:红钻,3:绿钻,4-9:彩钻1-6级,10:套餐价,11-14:店铺vip1-4,15:促销特价,16:促销团购,17:NBA会员,18:快捷支付价
	public static String getPriceTypeDesc(int value) {
		switch (value) {
		case 1:
			return "会员";
		case 2:
			return "红钻";
		case 3:
			return "绿钻";
		case 4:
			return "彩钻1级";
		case 5:
			return "彩钻2级";
		case 6:
			return "彩钻3级";
		case 7:
			return "彩钻4级";
		case 8:
			return "彩钻5级";
		case 9:
			return "彩钻6级";
		case 10:
			return "套餐价";
		case 11:
			return "VIP会员";
		case 12:
			return "黄金VIP会员";
		case 13:
			return "白金VIP会员";
		case 14:
			return "钻石VIP会员";
		case 15:
			return "促销特价";
		case 16:
			return "店铺vip6级";
		case 17:
			return "促销团购";
		case 18:
			return "NBA会员";
		default:
			return "快捷支付价";
		}
	}

	public static Vector<RedPackageVo> convert(ConfirmOrderResp resp, int type) {
		Vector<RedPackageVo> reds = new Vector<RedPackageVo>();
		for (CartRedPacketPo po : resp.getBuyerInfo().getRedPacketList()
				.getRedPacketPoList()) {
			if ((type == ONLY_REDPACKET && po.getFlag() == 0)
					|| (type == REDPACKET_AND_COUPON && (po.getFlag() == 0 || po
							.getFlag() == 1))) {
				RedPackageVo rpv = new RedPackageVo();
				rpv.setEndTime(po.getEndTime());
				rpv.setFlag(po.getFlag());
				rpv.setLow(po.getQuota());
				rpv.setName(po.getName());
				rpv.setPacketId(po.getPacketId());
				rpv.setValue(po.getValue());
				Vector<Long> validShop = new Vector<Long>();
				for (uint32_t shopId : po.getValidShop()) {
					validShop.add(shopId.longValue());
				}
				rpv.setValidShop(validShop);
				if (validShop.size() == 0 && !rpv.getName().contains("红包")) {
					continue;
				}
				reds.add(rpv);
			}

		}
		return reds;
	}

	/**
	 * 
	 * @Title: getCodFee
	 * 
	 * @Description: 计算货到付款服务费
	 * @param @param itemCode
	 * @param @param sellerUin
	 * @param @return 设定文件
	 * @return long 返回类型
	 * @throws
	 */
	public static long getCodFee(String itemCode, long sellerUin,
			long dealTotalFee) {
		ItemPo_v2 itemPo = null;
		// 商品基本信息，包含商品的基本信息(商品名称，所在地，最低价格，可使用的红包等)
		ItemBase oItemBase = null;
		long property = 0;
		FetchItemInfoResp resp = ItemClient.fetchItemInfo(itemCode);
		if (resp != null && resp.getResult() == 0
				&& (itemPo = resp.getItemPo()) != null
				&& (oItemBase = itemPo.getOItemBase()) != null) {
			long codShipTempId = oItemBase.getCodShipTempId();
			GetTemplateDetailNoLoginResp shipResp = ShippingfeeV2Client
					.getShippingTemplateDetail(sellerUin, (short) codShipTempId);
			if (shipResp != null && shipResp.getResult() == 0) {
				TemplateDetailInfo detail = null;
				TemplateSimpleInfo simple = null;
				if ((detail = shipResp.getDetailInfo()) != null
						&& (simple = detail.getSimple()) != null) {
					property = simple.getProperty();
				}
			}
		}
		int sellerPayCod = 0; // 0买家承担, 1卖家承担
		long shiftProperty = property >> 30;
		if (shiftProperty == 1) {
			sellerPayCod = COD_BUYER_FEE;
		} else if (shiftProperty == 2) {
			sellerPayCod = COD_SELLER_FEE;
		}
		int innerCompanyId = 0;
		if ((property & COD_SHIP_ZJS) == COD_SHIP_ZJS) {
			innerCompanyId = COD_SHIP_ZJS;
		} else if ((property & COD_SHIP_SF) == COD_SHIP_SF) {
			innerCompanyId = COD_SHIP_SF;
		}
		GetTotalFeeResp codFeeResp = CODFeeClient.getCodFee(innerCompanyId,
				sellerPayCod, dealTotalFee);
		CODFee codFee = null;
		if (codFeeResp != null && codFeeResp.getResult() == 0
				&& (codFee = codFeeResp.getRspData()) != null) {
			if (sellerPayCod == COD_BUYER_FEE) {
				dealTotalFee += codFee.getRealTotalFee();
			} else if (sellerPayCod == COD_SELLER_FEE) {
				dealTotalFee -= codFee.getDealAdjustFee();
			}
		}

		return dealTotalFee;
	}
	
	public static long getCodFee2(String itemCode, long sellerUin,
			long dealTotalFee, DealFavorVo favorVo) {
		ItemPo_v2 itemPo = null;
		// 商品基本信息，包含商品的基本信息(商品名称，所在地，最低价格，可使用的红包等)
		ItemBase oItemBase = null;
		long property = 0;
		FetchItemInfoResp resp = ItemClient.fetchItemInfo(itemCode);
		if (resp != null && resp.getResult() == 0
				&& (itemPo = resp.getItemPo()) != null
				&& (oItemBase = itemPo.getOItemBase()) != null) {
			long codShipTempId = oItemBase.getCodShipTempId();
			GetTemplateDetailNoLoginResp shipResp = ShippingfeeV2Client
					.getShippingTemplateDetail(sellerUin, (short) codShipTempId);
			if (shipResp != null && shipResp.getResult() == 0) {
				TemplateDetailInfo detail = null;
				TemplateSimpleInfo simple = null;
				if ((detail = shipResp.getDetailInfo()) != null
						&& (simple = detail.getSimple()) != null) {
					property = simple.getProperty();
				}
			}
		}
		int sellerPayCod = 0; // 0买家承担, 1卖家承担
		long shiftProperty = property >> 30;
		if (shiftProperty == 1) {
			sellerPayCod = COD_BUYER_FEE;
		} else if (shiftProperty == 2) {
			sellerPayCod = COD_SELLER_FEE;
		}
		int innerCompanyId = 0;
		if ((property & COD_SHIP_ZJS) == COD_SHIP_ZJS) {
			innerCompanyId = COD_SHIP_ZJS;
		} else if ((property & COD_SHIP_SF) == COD_SHIP_SF) {
			innerCompanyId = COD_SHIP_SF;
		} else if (((1 << (COD_SHIP_YS - 1)) & property) != 0) {
			innerCompanyId = COD_SHIP_YS;
		} else if (((1 << (COD_SHIP_YT - 1)) & property) != 0) {
			innerCompanyId = COD_SHIP_YT;
		}
		Log.run.info("cart codFee : innerCompanyId= "+innerCompanyId+" --sellerPayCod:"+sellerPayCod
				+"--dealTotalFee:"+dealTotalFee);
		// 计算服务费
		GetTotalFeeResp codFeeResp = CODFeeClient.getCodFee(innerCompanyId,
				sellerPayCod, dealTotalFee);
		CODFee codFee = null;
		if (codFeeResp == null) {
			favorVo.setErrorCode(COD_FEE_ERROR);
			favorVo.setErrorMsg("货到付款运费异常");
			
			return 0;
		}
		
		if (codFeeResp.getResult() == 0
				&& (codFee = codFeeResp.getRspData()) != null) {
			if (sellerPayCod == COD_BUYER_FEE) {
				dealTotalFee += codFee.getRealTotalFee();
			} else if (sellerPayCod == COD_SELLER_FEE) {
				dealTotalFee -= codFee.getDealAdjustFee();
			}
		} else if (codFeeResp.getResult() == COD_FEE_TOO_HIGH01_ERROR) {
			favorVo.setErrorCode(COD_FEE_TOO_HIGH01_ERROR);
			favorVo.setErrorMsg("货到付款揽收金额超过上限");
		} else if (codFeeResp.getResult() == COD_FEE_TOO_HIGH02_ERROR) {
			favorVo.setErrorCode(COD_FEE_TOO_HIGH02_ERROR);
			favorVo.setErrorMsg("货到付款代收金额过高");
		} else {
			favorVo.setErrorCode((int)codFeeResp.getResult());
			favorVo.setErrorMsg(codFeeResp.getErrMsg());
		}

		return dealTotalFee;
	}

}
