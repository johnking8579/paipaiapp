package com.qq.qqbuy.cmdy.util;

import com.qq.qqbuy.cmdy.po.CmdyItemListPo;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.deal.po.ConfirmOrderPo;
import com.qq.qqbuy.deal.po.MakeOrderPo;

/**
 * @author winsonwu
 * @Created 2013-2-18
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class CmdyValidate  {

	public static void validateCmdyAdd(long buyerUin, String itemCode, String itemAttr, long buyNum) throws BusinessException {
		// TODO winson
	}
	
	public static void validateCmdyList(long buyerUin, long sellerUin) throws BusinessException {
		// TODO winson
	}
	
	public static void validateCmdyModifyNum(long buyerUin, String itemCode, String itemAttr, int buyNum) throws BusinessException {
		// TODO winson
	}
	
	public static void validateCmdyRemove(long buyerUin, CmdyItemListPo po) throws BusinessException {
		// TODO winson
	}
	
	public static void validateCmdyConfirmOrder(ConfirmOrderPo req) throws BusinessException {
		// 检查参数
		if (req == null || (req.validate().size() > 0 || req.getVer() > 2)) {
			throw BusinessException.createInstance(ErrConstant.ERRCODE_INVALID_PARAMETER, "购物车分单请求参数有问题");
		}
	}
	
	public static void validateCmdyMakeOrder(MakeOrderPo po) throws BusinessException {
		// 校验参数
		if (po == null || (po.validate().size() > 0)) {
			throw BusinessException.createInstance(ErrConstant.ERRCODE_INVALID_PARAMETER, "购物车下单请求参数有问题");
		}
	}
	
}
