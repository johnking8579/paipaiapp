package com.qq.qqbuy.cmdy.po;

/**
 * @author winsonwu
 * @Created 2013-2-18
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class CmdyAddVo {
	
	private long totalNum;
	
	private long totalFee;
	
	private String disTotalFee;

	public long getTotalNum() {
		return totalNum;
	}

	public void setTotalNum(long totalNum) {
		this.totalNum = totalNum;
	}

	public long getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(long totalFee) {
		this.totalFee = totalFee;
	}

	public String getDisTotalFee() {
		return disTotalFee;
	}

	public void setDisTotalFee(String disTotalFee) {
		this.disTotalFee = disTotalFee;
	}
	
}
