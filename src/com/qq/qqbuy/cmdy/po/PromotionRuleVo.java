package com.qq.qqbuy.cmdy.po;

public class PromotionRuleVo {
	private long ruleId;
	private long freeMoney;
	private long discount;
	private boolean freeCarriage;
	private long barterMoney;
	private boolean present;
	private String desc;
	private int limitNum;
	private long limitPrice;

	public boolean isPresent() {
		return present;
	}

	public void setPresent(boolean present) {
		this.present = present;
	}

	public long getRuleId() {
		return ruleId;
	}

	public void setRuleId(long ruleId) {
		this.ruleId = ruleId;
	}

	public long getFreeMoney() {
		return freeMoney;
	}

	public void setFreeMoney(long freeMoney) {
		this.freeMoney = freeMoney;
	}

	public long getDiscount() {
		return discount;
	}

	public void setDiscount(long discount) {
		this.discount = discount;
	}

	public long getBarterMoney() {
		return barterMoney;
	}

	public void setBarterMoney(long barterMoney) {
		this.barterMoney = barterMoney;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public boolean isFreeCarriage() {
		return freeCarriage;
	}

	public void setFreeCarriage(boolean freeCarriage) {
		this.freeCarriage = freeCarriage;
	}

	public int getLimitNum() {
		return limitNum;
	}

	public void setLimitNum(int limitNum) {
		this.limitNum = limitNum;
	}

	public long getLimitPrice() {
		return limitPrice;
	}

	public void setLimitPrice(long limitPrice) {
		this.limitPrice = limitPrice;
	}

}
