package com.qq.qqbuy.cmdy.po;

import com.qq.qqbuy.cmdy.util.CmdyUtil;

public class MutiPriceVo {
	private long priceValue;
	private long priceType;// 1:会员,2:红钻,3:绿钻,4-9:彩钻1-6级,10:套餐价,11-14:店铺vip1-4,15:促销特价,16:促销团购,17:NBA会员,18:快捷支付价
	private String priceTypeDesc;


	public long getPriceType() {
		return priceType;
	}

	public void setPriceType(long priceType) {
		this.priceType = priceType;
		this.priceTypeDesc = CmdyUtil.getPriceTypeDesc((int) priceType);
	}

	public long getPriceValue() {
		return priceValue;
	}

	public void setPriceValue(long priceValue) {
		this.priceValue = priceValue;
	}

	public String getPriceTypeDesc() {
		return priceTypeDesc;
	}

	public void setPriceTypeDesc(String priceTypeDesc) {
		this.priceTypeDesc = priceTypeDesc;
	}

}
