package com.qq.qqbuy.cmdy.po;

import java.util.Vector;

import org.apache.commons.lang.StringUtils;

import com.paipai.lang.MultiMap;
import com.qq.qqbuy.cmdy.util.CmdyConstant;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.deal.po.MakeOrderPo;
import com.qq.qqbuy.recvaddr.po.ReceiverAddress;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.MakeOrderRequest;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.OrderInformation;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.ReceiveAddress;

/**
 * 购物车下单请求数据封装，对应拍拍IDL接口的MakeOrderRequest对象
 * 
 * @author winsonwu
 * @Created 2013-2-4 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class CmdyMakeOrderPo implements CmdyConstant {

	/********************************************* 收货地址信息 ***********************************/
	// 收货地址ID
	private long recvAddrId;
	
	private String mk ;

	private Vector<CmdyOrder> cmdyOrders = new Vector<CmdyOrder>();

	/********************************************* 对象转换区 **************************************/
	public static CmdyMakeOrderPo deSerialize(MakeOrderPo req) {
		// "~TestClient测试dealNote--winson~0~TestClient测试invoiceTitle--winson~~0~1~1141379069~~~1",
		// "FD0F084400000000007D3A7D0001C4DA--1-0-"};
		// buyerUin recvAddrId
		// comboId~dealNote~dealType~invoiceTitle~invoiceType~promotionRuleId~sellerProperty~sellerUin~specialFee~specialFeeType~transportType
		// itemCode-itemAttr-buyNum-priceType-redPacketId
		// itemCode1-itemAttr1-buyNum1-priceType1-redPacketId1 ......

		// makeOrderStr定义格式
		// Version1.0
		// dealType~promotionRuleId~sellerUin~transportType~itemCode$itemAttr-buyNum-priceType~item1$itemAttr1$buyNum1$priceType1
		// payType~promotionRuleId~sellerUin~transportType~itemCode$itemAttr$buyNum$priceType$redPackageId~item1$itemAttr1$buyNum1$priceType1$redPackageId~msg
	    // Version2.0
	    // dealType~promotionRuleId~sellerUin~transportType~itemCode$itemAttr-buyNum-priceType~item1$itemAttr1$buyNum1$priceType1
        // payType~promotionRuleId~sellerUin~transportType~itemCode$itemAttr$buyNum$priceType$redPackageId~item1$itemAttr1$buyNum1$priceType1$redPackageId~msg~couponId
	    String[] makeOrderStrs = req.getOrderStrList();
		CmdyMakeOrderPo po = new CmdyMakeOrderPo();
		if (makeOrderStrs != null && makeOrderStrs.length > 0) {
			po.setMk(req.getMk());
			for (String makeOrderStr : makeOrderStrs) {
				String[] strs = makeOrderStr.split("~",-1);
				CmdyOrder order = new CmdyOrder();
				order.setDealType(StringUtil.toInt(strs[0], 0));
				order.setPromotionRuleId(StringUtil.toInt(strs[1], 0));
				order.setSellerUin(StringUtil.toLong(strs[2], -1));
				order.setTransportType(StringUtil.toInt(strs[3], 0));
				
				// add by candela 支持代金券
				int itemStartPos = 4;
				if (req.getVer() >= 3) {
					order.setOnlineShoppingCoupons(StringUtil.toInt(strs[4], 0));
					itemStartPos = 5;
				}
				// end
				// add by wanghao 支持平台积分红包
				if(req.getVer() >= 4){
					order.setRedBagScore(StringUtil.toLong(strs[5],0));
					itemStartPos = 6;
				}
				//end
				// order.setDealNote(strs[4]);
				// order.setComboId(strs[0]);
				// order.setDealNote(strs[1]);
				// order.setInvoiceTitle(strs[3]);
				// order.setInvoiceType(StringUtil.toInt(strs[4], 0));
				// order.setSellerProperty(StringUtil.toInt(strs[6], 0));
				// order.setSpecialFee(StringUtil.toInt(strs[8], 0));
				// order.setSpecialFeeType(StringUtil.toInt(strs[9], 0));

				// 商品列表结束
				int endPos = strs.length - 1;
				// ver=2时候多了优惠券信息
				if (req.getVer() >= 2){
				    endPos = strs.length - 2;
				}
				for (int i = itemStartPos; i < endPos; i++) {
					String itemStr = strs[i];
					String[] itemStrs = itemStr.split("\\$", -1);
					CmdyItem item = new CmdyItem();
					item.setItemCode(itemStrs[0]);
					item.setItemAttr(itemStrs[1]);
					item.setBuyNum(StringUtil.toInt(itemStrs[2], 1));
					item.setPriceType(StringUtil.toInt(itemStrs[3], 0));
					item.setRedPacketId(StringUtil.toInt(itemStrs[4], 0));
					order.getCmdyItems().add(item);
				}
				
				order.setDealNote(strs[endPos]);
				if(StringUtils.isEmpty(order.getDealNote())){
				    order.setDealNote(" ");
				}
				// ver=2时候多了优惠券信息
				if (req.getVer() >= 2){
				    order.setCouponId(StringUtil.toLong(strs[strs.length - 1], 0));
                }
				po.getCmdyOrders().add(order);
			}
		}

		return po;
	}

	public MakeOrderRequest toIDLMakeOrderRequest() {
		MakeOrderRequest request = new MakeOrderRequest();

		// request.setVersion(2); // TODO winson, 此处我们不应该设置version,需服务端解决此问题

		// 微购来源
		MultiMap<String, String> extInfos = new MultiMap<String, String>();
		 extInfos.put(CMDY_MAKEORDER_EXTINFO_WEIGOU_KEY,
				 CMDY_MAKEORDER_EXTINFO_WEIGOU_VALUE_SCENEID);
		 extInfos.put(CMDY_MAKEORDER_EXTINFO_WEIGOU_UUID, getMk());
		 
		request.setExtInfo(extInfos);

		request.setSequenceId(System.currentTimeMillis() / 1000); // 下单时间戳

		// 收货地址
		ReceiveAddress recv = new ReceiveAddress();
		recv.setAddressId(recvAddrId);
		request.setRecvAddr(recv);

		// 下单信息
		if (cmdyOrders != null && cmdyOrders.size() > 0) {
			Vector<OrderInformation> orderInfos = new Vector<OrderInformation>();
			for (CmdyOrder cmdyOrder : cmdyOrders) {
				orderInfos.add(cmdyOrder.toIDLOrderInformation());
			}
			request.setOrderInfo(orderInfos);
		}
		return request;
	}
	/**
	 * 
	
	 * @Title: toIDLMakeOrderRequest 
	
	 * @Description: 京东收银台的下间前准备
	
	 * @param @param defaultAddr
	 * @param @return    设定文件 
	
	 * @return MakeOrderRequest    返回类型 
	
	 * @throws
	 */
	public MakeOrderRequest toIDLMakeOrderRequest(ReceiverAddress defaultAddr) {

		MakeOrderRequest request = new MakeOrderRequest();

		// request.setVersion(2); // TODO winson, 此处我们不应该设置version,需服务端解决此问题

		// 微购来源
		MultiMap<String, String> extInfos = new MultiMap<String, String>();
		 extInfos.put(CMDY_MAKEORDER_EXTINFO_WEIGOU_KEY,
				 CMDY_MAKEORDER_EXTINFO_WEIGOU_VALUE_SCENEID);
		 extInfos.put(CMDY_MAKEORDER_EXTINFO_WEIGOU_UUID, getMk());
		 
		request.setExtInfo(extInfos);

		request.setSequenceId(System.currentTimeMillis() / 1000); // 下单时间戳

		// 收货地址
		ReceiveAddress recv = new ReceiveAddress();
		recv.setAddressId(recvAddrId);
		recv.setAddress(defaultAddr.getAddress());
		recv.setName(defaultAddr.getName());
		recv.setRegionId(defaultAddr.getRegionId());
		recv.setMobile(defaultAddr.getMobile());
		recv.setPhone(defaultAddr.getPhone());
		recv.setPostCode(defaultAddr.getPostcode());
		recv.setAddressCode(String.valueOf(defaultAddr.getAddressId()));
		request.setRecvAddr(recv);

		// 下单信息
		if (cmdyOrders != null && cmdyOrders.size() > 0) {
			Vector<OrderInformation> orderInfos = new Vector<OrderInformation>();
			for (CmdyOrder cmdyOrder : cmdyOrders) {
				orderInfos.add(cmdyOrder.toIDLOrderInformation());
			}
			request.setOrderInfo(orderInfos);
		}
		return request;
	
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CmdyMakeOrderPo [cmdyOrders=");
		builder.append(cmdyOrders);
		builder.append(", recvAddrId=");
		builder.append(recvAddrId);
		builder.append("]");
		return builder.toString();
	}

	public long getRecvAddrId() {
		return recvAddrId;
	}

	public void setRecvAddrId(long recvAddrId) {
		this.recvAddrId = recvAddrId;
	}

	public Vector<CmdyOrder> getCmdyOrders() {
		return cmdyOrders;
	}

	public void setCmdyOrders(Vector<CmdyOrder> cmdyOrders) {
		this.cmdyOrders = cmdyOrders;
	}

	public String getMk() {
		return mk;
	}

	public void setMk(String mk) {
		this.mk = mk;
	}

	public static void main(String[] args) {
//		String orderStrList = "0~0~1275000334~0~0~0EF6FE4B00000000040100003707AD2C$尺码:M$1$0$0$app.paipai.com/home.htm?ptag=20381.12.5&launchType=back|||app.paipai.com/myOrder.htm?ptag=20381.28.2&launchType=new&orderCate=三个月内的订单|||app.paipai.com/orderDetail.htm?ptag=20381.29.1&launchType=new&orderID=1275000334-20141215-1333453699|||app.paipai.com/product.htm?ptag=20381.29.1&launchType=new&sku=0EF6FE4B00000000040100003707AD2C&leaf=243708|||app.paipai.com/confirmOrder.htm?ptag=20381.23.8&launchType=new|||app.paipai.com/payResult.htm?ptag=20381.42.5&launchType=new&orderID=1275000334-20141215-1333523952~1275000334|||app.paipai.com/product.htm?ptag=20381.42.5&launchType=back&sku=0EF6FE4B00000000040100003707AD2C&leaf=243708$~~0";
//		String dealStrs = "0~15880532~795019790~1~0~0E0A632F000000000401000043BBE871$自定义项:自定义1$1$0$0~0E0A632F00000000040100003F692E2F$颜色:橙色|鞋码:41$1$0$0~~0,0~0~1275000334~0~0~0EF6FE4B00000000040100003707AD2C$尺码:M$1$0$0~~0,0~0~1275000334~0~0~0EF6FE4B00000000040100003707AD23$尺码:S$12$0$0~~0";
		String dealStrs = "0~15880532~795019790~1~0~0E0A632F000000000401000043BBE871$自定义项:自定义1$1$0$0$app.paipai.com/home.htm?ptag=20381.23.7&launchType=back|||app.paipai.com/product.htm?ptag=20381.28.7&launchType=new&sku=0E0A632F000000000401000043BBE871&leaf=27843$~0E0A632F00000000040100003F692E2F$颜色:橙色|鞋码:41$1$0$0$app.paipai.com/home.htm?ptag=20381.12.5&launchType=back|||app.paipai.com/product.htm?ptag=20381.28.7&launchType=new&sku=0E0A632F00000000040100003F692E2F&leaf=6021$~0,0~0~1275000334~0~0~0EF6FE4B00000000040100003707AD2C$尺码:M$1$0$0$app.paipai.com/home.htm?ptag=20381.12.5&launchType=back|||app.paipai.com/myOrder.htm?ptag=20381.28.2&launchType=new&orderCate=三个月内的订单|||app.paipai.com/orderDetail.htm?ptag=20381.29.1&launchType=new&orderID=1275000334-20141215-1333453699|||app.paipai.com/product.htm?ptag=20381.29.1&launchType=new&sku=0EF6FE4B00000000040100003707AD2C&leaf=243708|||app.paipai.com/confirmOrder.htm?ptag=20381.23.8&launchType=new|||app.paipai.com/payResult.htm?ptag=20381.42.5&launchType=new&orderID=1275000334-20141215-1333523952~1275000334|||app.paipai.com/product.htm?ptag=20381.42.5&launchType=back&sku=0EF6FE4B00000000040100003707AD2C&leaf=243708$~0,0~0~1275000334~0~0~0EF6FE4B00000000040100003707AD23$尺码:S$12$0$0$app.paipai.com/cart.htm?ptag=20381.41.2&launchType=back|||app.paipai.com/product.htm?ptag=20381.41.3&launchType=new&sku=0EF6FE4B00000000040100003707AD23&leaf=243708|||app.paipai.com/confirmOrder.htm?ptag=20381.23.8&launchType=new|||app.paipai.com/payResult.htm?ptag=20381.42.5&launchType=new&orderID=1275000334-20141215-1333523477~1275000334|||app.paipai.com/myOrder.htm?ptag=20381.42.5&launchType=new&orderCate=三个月内的订单|||app.paipai.com/orderDetail.htm?ptag=20381.29.1&launchType=new&orderID=1275000334-20141215-1333552526|||app.paipai.com/myOrder.htm?ptag=20381.44.3&launchType=back&orderCate=三个月内的订单|||app.paipai.com/orderDetail.htm?ptag=20381.29.1&launchType=new&orderID=1275000334-20141215-1333523477|||app.paipai.com/myOrder.htm?ptag=20381.44.3&launchType=back&orderCate=三个月内的订单|||app.paipai.com/product.htm?ptag=20381.44.3&launchType=back&sku=0EF6FE4B00000000040100003707AD23&leaf=243708|||app.paipai.com/confirmOrder.htm?ptag=20381.44.3&launchType=new|||app.paipai.com/product.htm?ptag=20381.44.3&launchType=back&sku=0EF6FE4B00000000040100003707AD23&leaf=243708$~0";
		String[] deals = dealStrs.split(",");
		MakeOrderPo po = new MakeOrderPo();
		po.setVer(3);
		po.setOrderStrList(deals);
		CmdyMakeOrderPo orderPo = deSerialize(po);
		System.out.println(orderPo.toString());
	}
}
