package com.qq.qqbuy.cmdy.po;

import java.util.Vector;

import org.apache.commons.lang.StringUtils;

import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.CartItemData;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.CartItemDataList;

/**
 * 购物车商品列表列表，对应IDL的CartItemDataList数据结构
 * 
 * @author winsonwu
 * @Created 2013-2-4 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class CmdyItemListPo {

	// 支付方式
	// 0、在线支付
	// 1、货到付款
	private int payType;

	// 商品列表
	private Vector<CmdyItem> cmdyItems = new Vector<CmdyItem>();

	/********************************************* 对象转换区 **************************************/
	// itemCode$itemAttr$buyNum~itemCode1$itemAttr1$buyNum1~ ......
	public static CmdyItemListPo disSerialize(String content) {
		if (StringUtil.isNotEmpty(content)) {
			String[] strs = content.split("~");
			if (strs != null && strs.length >= 1) {
				CmdyItemListPo po = new CmdyItemListPo();
				for (int i = 0; i < strs.length; i++) {
					String str = strs[i];
					String[] itemStrs = str.split("\\$");
					if (itemStrs.length<2) {
						return null ;
					}
					CmdyItem item = new CmdyItem();
					item.setItemCode(itemStrs[0]);
					item.setItemAttr(itemStrs[1]);
					item.setBuyNum(StringUtil.toInt(itemStrs[2], 1));
					if(itemStrs.length == 4){
						item.setSceneId(Long.parseLong(StringUtils.isBlank(itemStrs[3]) ? "0" : itemStrs[3]));
					}
					po.getCmdyItems().add(item);
				}
				return po;
			}
		}
		return null;
	}

	public CartItemDataList toIDLCartItemDataList() {
		CartItemDataList dataList = new CartItemDataList();

		dataList.setPayType(payType);

		if (cmdyItems != null && cmdyItems.size() > 0) {
			Vector<CartItemData> datas = new Vector<CartItemData>();
			for (CmdyItem cmdyItem : cmdyItems) {
				datas.add(cmdyItem.toIDLCartItemData());
			}
			dataList.setDataList(datas);
		}

		return dataList;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CmdyItemListPo [cmdyItems=");
		builder.append(cmdyItems);
		builder.append(", payType=");
		builder.append(payType);
		builder.append("]");
		return builder.toString();
	}

	public int getPayType() {
		return payType;
	}

	public void setPayType(int payType) {
		this.payType = payType;
	}

	public Vector<CmdyItem> getCmdyItems() {
		return cmdyItems;
	}

	public void setCmdyItems(Vector<CmdyItem> cmdyItems) {
		this.cmdyItems = cmdyItems;
	}

	public static void main(String[] args) {
		String s = "CD47F63200000000040100001638B073$尺码:42|货号:91248006-2白/浅灰色$1";
		CmdyItemListPo po = disSerialize(s);
	}
}
