package com.qq.qqbuy.cmdy.po;

/**
 * 
 * @ClassName: DealInfo 
 * @Description: 订单信息 
 * @author wendyhu 
 * @date 2013-3-19 下午02:50:19
 */
public class DealInfo
{
    /**
     * 订单id
     */
    private String dealCode = "";
    
    
    /**
     * 卖家uin
     */
    private String sellerUin = "";


    public String getDealCode()
    {
        return dealCode;
    }

    public void setDealCode(String dealCode)
    {
        this.dealCode = dealCode;
    }


    public String getSellerUin()
    {
        return sellerUin;
    }


    public void setSellerUin(String sellerUin)
    {
        this.sellerUin = sellerUin;
    }
    
    

}
