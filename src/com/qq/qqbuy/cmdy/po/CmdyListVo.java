package com.qq.qqbuy.cmdy.po;

import java.util.Vector;

import com.qq.qqbuy.item.util.DispFormater;

/**
 * @author winsonwu
 * @Created 2013-2-18 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class CmdyListVo {

	private long normalTotalPrice;

	private String disNormalTotalPrice;

	// 正常的商品列表
	private Vector<ShopVo> normalItems = new Vector<ShopVo>();

	// 出问题的商品列表
	private Vector<ShopVo> problemItems = new Vector<ShopVo>();

	public void cacalNormalTotalPriceAfterNormalItems() {
		if (normalItems != null && normalItems.size() > 0) {
			long total = 0;
			for (ShopVo shop : normalItems) {
				for (CmdyItemVo item : shop.getItems()) {
					total += item.lastCalculateTotalPrice();
				}
			}
			normalTotalPrice = total;
			disNormalTotalPrice = DispFormater
					.priceDispFormater(normalTotalPrice);
		}
	}

	public Vector<ShopVo> getNormalItems() {
		return normalItems;
	}

	public void setNormalItems(Vector<ShopVo> normalItems) {
		this.normalItems = normalItems;
	}

	public Vector<ShopVo> getProblemItems() {
		return problemItems;
	}

	public void setProblemItems(Vector<ShopVo> problemItems) {
		this.problemItems = problemItems;
	}

	public long getNormalTotalPrice() {
		return normalTotalPrice;
	}

	public void setNormalTotalPrice(long normalTotalPrice) {
		this.normalTotalPrice = normalTotalPrice;
	}

	public String getDisNormalTotalPrice() {
		return disNormalTotalPrice;
	}

	public void setDisNormalTotalPrice(String disNormalTotalPrice) {
		this.disNormalTotalPrice = disNormalTotalPrice;
	}

}
