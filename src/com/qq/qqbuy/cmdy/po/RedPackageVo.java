package com.qq.qqbuy.cmdy.po;

import java.util.Vector;

public class RedPackageVo {
	private long uin;

	/**
	 * 红包编号
	 * 
	 */
	private long packetId;

	/**
	 * 红包类型标识（优惠券，包邮卡还是红包）
	 * 
	 * 版本 >= 0
	 */
	private long flag;

	/**
	 * 面值
	 * 
	 */
	private long value;

	/**
	 * 最低消费
	 * 
	 */
	private long low;

	/**
	 * 到期时间
	 * 
	 */
	private long endTime;

	private Vector<Long> validShop;

	/**
	 * 红包名称
	 * 
	 */
	private String name = new String();

	public long getUin() {
		return uin;
	}

	public void setUin(long uin) {
		this.uin = uin;
	}

	public long getPacketId() {
		return packetId;
	}

	public void setPacketId(long packetId) {
		this.packetId = packetId;
	}

	public long getFlag() {
		return flag;
	}

	public void setFlag(long flag) {
		this.flag = flag;
	}

	public long getValue() {
		return value;
	}

	public void setValue(long value) {
		this.value = value;
	}

	public long getLow() {
		return low;
	}

	public void setLow(long low) {
		this.low = low;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Vector<Long> getValidShop() {
		return validShop;
	}

	public void setValidShop(Vector<Long> validShop) {
		this.validShop = validShop;
	}

}
