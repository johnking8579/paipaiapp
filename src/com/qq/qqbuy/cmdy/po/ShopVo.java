package com.qq.qqbuy.cmdy.po;

import java.util.List;

public class ShopVo
{
    private long sellerUin;
    private String shopName;
    private int shopType;
    private List<CmdyItemVo> items;

    public long getSellerUin()
    {
        return sellerUin;
    }

    public void setSellerUin(long sellerUin)
    {
        this.sellerUin = sellerUin;
    }

    public String getShopName()
    {
        return shopName;
    }

    public void setShopName(String shopNameValue)
    {
        if (shopNameValue != null)
            this.shopName = shopNameValue.replaceAll("\t|\n|\r", "");
    }

    public int getShopType()
    {
        return shopType;
    }

    public void setShopType(int shopType)
    {
        this.shopType = shopType;
    }

    public List<CmdyItemVo> getItems()
    {
        return items;
    }

    public void setItems(List<CmdyItemVo> items)
    {
        this.items = items;
    }

}
