package com.qq.qqbuy.cmdy.po;

import java.util.List;

import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.CartItemData;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.CommodityInformation;

/**
 * @author winsonwu
 * @Created 2013-2-4 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class CmdyItem {

	// 商品id
	private String itemCode;
	// 库存属性
	private String itemAttr;
	// 商品数量
	private int buyNum;
	// 价格类型
	private int priceType;
	// 红包id
	private int redPacketId;
	// 场景ID
	private long sceneId = 0;

	private List<MutiPriceVo> mutiPrices;

	private List<PromotionRuleVo> promotionRules;

	/********************************************* 对象转换区 **************************************/
	public CommodityInformation toIDLCommodityInformation() {
		CommodityInformation info = new CommodityInformation();
		info.setCommodityId(itemCode);
		info.setCommodityNumber(buyNum);
		info.setPriceType(priceType);
		info.setRedPacketId(redPacketId);
		info.setStockAttribute(itemAttr);

		return info;
	}

	public CartItemData toIDLCartItemData() {
		CartItemData item = new CartItemData();
		item.setAttr(itemAttr);
		item.setItemId(itemCode);
		item.setNum(buyNum);

		return item;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CmdyItem [buyNum=");
		builder.append(buyNum);
		builder.append(", itemAttr=");
		builder.append(itemAttr);
		builder.append(", itemCode=");
		builder.append(itemCode);
		builder.append(", priceType=");
		builder.append(priceType);
		builder.append(", redPacketId=");
		builder.append(redPacketId);
		builder.append("]");
		return builder.toString();
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemAttr() {
		return itemAttr;
	}

	public void setItemAttr(String itemAttr) {
		this.itemAttr = itemAttr;
	}

	public int getBuyNum() {
		return buyNum;
	}

	public void setBuyNum(int buyNum) {
		this.buyNum = buyNum;
	}

	public int getPriceType() {
		return priceType;
	}

	public void setPriceType(int priceType) {
		this.priceType = priceType;
	}

	public int getRedPacketId() {
		return redPacketId;
	}

	public void setRedPacketId(int redPacketId) {
		this.redPacketId = redPacketId;
	}

	public List<MutiPriceVo> getMutiPrices() {
		return mutiPrices;
	}

	public void setMutiPrices(List<MutiPriceVo> mutiPrices) {
		this.mutiPrices = mutiPrices;
	}

	public List<PromotionRuleVo> getPromotionRules() {
		return promotionRules;
	}

	public void setPromotionRules(List<PromotionRuleVo> promotionRules) {
		this.promotionRules = promotionRules;
	}

	public long getSceneId() {
		return sceneId;
	}

	public void setSceneId(long sceneId) {
		this.sceneId = sceneId;
	}

}
