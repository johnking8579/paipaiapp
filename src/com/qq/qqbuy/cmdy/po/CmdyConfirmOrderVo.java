package com.qq.qqbuy.cmdy.po;

import java.util.Vector;

import com.qq.qqbuy.deal.po.ShipCalcVo;
import com.qq.qqbuy.recvaddr.po.ReceiverAddress;
import com.qq.qqbuy.recvaddr.po.RecvAddr;


/**
 * @author winsonwu
 * @Created 2013-2-19 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class CmdyConfirmOrderVo {

	// 收货地址列表
	private RecvAddr recv;

	// 默认收货地址
	private ReceiverAddress defaultAddr;

	// 分单后的商品列表
	private Vector<CmdyItemVo> items = new Vector<CmdyItemVo>();

	// 运送方式选择列表
	private ShipCalcVo shipVo;

	public RecvAddr getRecv() {
		return recv;
	}

	public void setRecv(RecvAddr recv) {
		this.recv = recv;
	}

	public ReceiverAddress getDefaultAddr() {
		return defaultAddr;
	}

	public void setDefaultAddr(ReceiverAddress defaultAddr) {
		this.defaultAddr = defaultAddr;
	}

	public Vector<CmdyItemVo> getItems() {
		return items;
	}

	public void setItems(Vector<CmdyItemVo> items) {
		this.items = items;
	}

	public ShipCalcVo getShipVo() {
		return shipVo;
	}

	public void setShipVo(ShipCalcVo shipVo) {
		this.shipVo = shipVo;
	}

}
