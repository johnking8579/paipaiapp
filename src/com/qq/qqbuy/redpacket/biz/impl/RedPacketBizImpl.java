package com.qq.qqbuy.redpacket.biz.impl;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import net.sf.json.JSONObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.paipai.lang.uint32_t;
import com.qq.qqbuy.common.Page;
import com.qq.qqbuy.common.SpringHelper;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.DateUtils;
import com.qq.qqbuy.login.dao.LoginToDayDao;
import com.qq.qqbuy.login.po.LoginToDay;
import com.qq.qqbuy.redpacket.RedPacketState;
import com.qq.qqbuy.redpacket.biz.RedPacketBiz;
import com.qq.qqbuy.redpacket.dao.RedpacketSendDao;
import com.qq.qqbuy.redpacket.po.ActiveResultPo;
import com.qq.qqbuy.redpacket.po.RedPacketPo;
import com.qq.qqbuy.redpacket.po.RedpacketSendPo;
import com.qq.qqbuy.redpacket.util.RedpacketConstants;
import com.qq.qqbuy.shop.biz.ShopBiz;
import com.qq.qqbuy.thirdparty.idl.redpacket.RedPacketClient;
import com.qq.qqbuy.thirdparty.idl.redpacket.protocol.GetRedPacketListResp;
import com.qq.qqbuy.thirdparty.idl.redpacket.protocol.RedPacket;
import com.qq.qqbuy.thirdparty.idl.redpacket.protocol.SendRedPacketResp;


public class RedPacketBizImpl implements RedPacketBiz {

	private LoginToDayDao loginToDayDao ;
	
	private RedpacketSendDao redpacketSendDao ;
	
	private ExecutorService threadPool = Executors.newFixedThreadPool(2);
	
	private static Logger log = LogManager.getLogger("redpacketActive");
	
	private static Logger sendlog = LogManager.getLogger("send");
	
	private static Logger budanlog = LogManager.getLogger("budan");
	
    @Override
    public Map<Long, Long> count(long uin, String mk,String sk) throws BusinessException {
        return count(uin, mk, 1,sk);
    }

    @Override
    public Page<RedPacketPo> query(int state, long uin, String mk,String sk) throws BusinessException {
        return query(state, uin, mk, 1,sk);
    }

    @Override
    public Map<Long, Long> count(long uin, String mk, int type,String sk) throws BusinessException {
        Map<Long, Long> result = new HashMap<Long, Long>();
        
        RedPacketClient rkgClient = new RedPacketClient();
        for (RedPacketState state : RedPacketState.values()) {
        	GetRedPacketListResp resp = rkgClient.getRedPacketList(state.getState(), uin, type,sk);
            if (resp == null || resp.getResult() != 0) {
                throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "红包列表IDL调用失败");
            }
            result.put((long) state.getState(), resp.getTotalNum());
        }

        return result;
    }

	@Override
    public Page<RedPacketPo> query(int state, long uin, String mk, int type,String sk) throws BusinessException {
    	RedPacketClient rkgClient = new RedPacketClient();
    	Page<RedPacketPo> page = new Page<RedPacketPo>();
    	
    	if(state == -1){//全部查询
	    	GetRedPacketListResp resp = rkgClient.getRedPacketList(100, uin,type, sk);//100待使用  --默认
	    	GetRedPacketListResp resp101 = rkgClient.getRedPacketList(101, uin,type, sk);//101绑定或冻结（使用中）
	    	GetRedPacketListResp resp102 = rkgClient.getRedPacketList(102, uin,type, sk);//102已使用
	    	GetRedPacketListResp resp2 = rkgClient.getRedPacketList(2, uin,type, sk);//2过期
    	
	    	if (resp == null || resp.getResult() != 0 || 
	    			resp101 == null || resp101.getResult() != 0 || 
					resp102 == null || resp102.getResult() != 0 || 
					resp2 == null || resp2.getResult() != 0) {
	              throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "红包列表IDL调用失败");
	        }
	 
	        page.setRowCount(resp.getTotalNum() + 
	        		resp101.getTotalNum() + 
	        		resp102.getTotalNum() + 
	        		resp2.getTotalNum());
	        ShopBiz shopbiz = (ShopBiz)SpringHelper.getBean("shopBiz");
	        
	        
	        //待使用的做个处理，得到即将过期的list
//	        状态排序：即将过期 + 待使用 + 已绑定 + 已使用 + 已过期
//
//	        即将过期的判断条件，优惠券的过期时间 - 当前时间    取得的值小于三天，示例代码如下：
//	        (((coupons.get(i).endTime - new Date().getTime())/(24*3600*1000.0))) < 3
	        
	        /*
	         * 即将过期
	         */
	        GetRedPacketListResp respGQ = new GetRedPacketListResp();
	        
	        Vector<RedPacket> redPacketList = resp.getRedPacketList();
	        
	        Iterator<RedPacket> iterator = redPacketList.iterator();
	        while(iterator.hasNext()){
	        	RedPacket redPacket = iterator.next();
	        	
	        	if(((redPacket.getEndTime()*1000 - new Date().getTime())/(24*3600*1000.0)) < 3){
					respGQ.getRedPacketList().add(redPacket);
					iterator.remove();
				}
	        }
	        
	        
	        try {
	        	adapterResult(page, respGQ, shopbiz,103);
	            adapterResult(page, resp, shopbiz,100);
	            adapterResult(page, resp101, shopbiz,101);
	            adapterResult(page, resp102, shopbiz,102);
	            adapterResult(page, resp2, shopbiz,2);
	        } catch (Exception e) {
	            throw BusinessException.createInstance(BusinessErrorType.UNKNOWN_ERROR, e);
	        }
    	}else{
    		GetRedPacketListResp resp = rkgClient.getRedPacketList(state, uin,type, sk);
        	if (resp == null || resp.getResult() != 0) {
                  throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "红包列表IDL调用失败");
            }
     
            page.setRowCount(resp.getTotalNum());
            ShopBiz shopbiz = (ShopBiz)SpringHelper.getBean("shopBiz");
            try {
            	adapterResult(page, resp, shopbiz,state);
            } catch (Exception e) {
                throw BusinessException.createInstance(BusinessErrorType.UNKNOWN_ERROR, e);
            }
    	}
        return page;
    }
	

	private void adapterResult(Page<RedPacketPo> page,
			GetRedPacketListResp resp, ShopBiz shopbiz,int state) {
		
		Vector<RedPacket> redPacketList = resp.getRedPacketList();
		Collections.sort(redPacketList);//根据根据结束时间排序。越早要过期的放前面   越晚要过期的放后面
		
		for (RedPacket redpacket : redPacketList) {
		    RedPacketPo po = new RedPacketPo();
		    po.setActualPrice(redpacket.getActualPrice());
		    po.setDealCode(redpacket.getDealCode());
		    po.setImageUrl(redpacket.getImageUrl());
		    po.setIp(redpacket.getIp());
		    po.setMinimum(redpacket.getMinimum());
		    po.setOwnerUin(redpacket.getOwnerUin());
		    po.setPacketFlag(redpacket.getPacketFlag());
		    po.setPacketId(redpacket.getPacketId());
		    po.setPacketName(redpacket.getPacketName());
		    po.setPacketPrice(redpacket.getPacketPrice());
		    po.setPacketStockId(redpacket.getPacketStockId());
		    po.setRelaUrl(redpacket.getRelaUrl());
		    po.setSellerUin(redpacket.getSellerUin());
		    po.setState(state);
		    po.setType(redpacket.getType());
		    po.setWithdrawId(redpacket.getWithdrawId());
		    
		    po.setBeginTime(redpacket.getBeginTime() * 1000);
		    po.setEndTime(redpacket.getEndTime() * 1000);
		    po.setRecvTime(redpacket.getRecvTime() * 1000);
		    po.setUseTime(redpacket.getUseTime() * 1000);
		    po.setWithdrawTime(redpacket.getWithdrawTime() * 1000);
//                if(StringUtils.isNotEmpty(redpacket.getApplicableScope())){
//                    String shops = StringUtils.substringBetween(redpacket.getApplicableScopeList(), "[", "]");
//                    for (String seller : shops.split(",")){
//                        po.getApplicableScope().add(new uint32_t(StringUtil.toLong(seller, 0)));
//                        po.getApplicableShopName().add(shopbiz.getShopName(StringUtil.toLong(seller, 0), null));
//                    }
//                }
		    
		    //这个是使用店铺的id;没有直接的卖家uin
		    for (uint32_t seller : redpacket.getApplicableScope()) {
		    	 po.getApplicableScope().add(seller);
		         po.getApplicableShopName().add(shopbiz.getShopName(seller.getValue(), null));
		    }
		    page.getElements().add(po);
		}
	}

	@Override
	public void sendRedpacketForThread(String beforeTimeStr, String endTimeStr, String activeId,
			String redpacketId, int totalNum, int sendNum,String activeDesc, String redpacketDesc,List<Long> widList, int justWid) throws Exception {
		threadPool.execute(new SendRedpacketThread(beforeTimeStr, endTimeStr, activeId,
				redpacketId, totalNum, sendNum,activeDesc, redpacketDesc, widList, justWid));
	}
	@Override
	public ActiveResultPo sendRedpacket(String beforeTimeStr, String endTimeStr, String activeId,
			String redpacketId, int totalNum, int sendNum,String activeDesc, String redpacketDesc,List<Long> widList, int justWid) throws Exception {
		// TODO Auto-generated method stub
		ActiveResultPo activeResultPo = new ActiveResultPo() ;
		Long beforeTime = 0l  ;
		Long endTime = 0l  ;
		if ("-1".equals(beforeTimeStr)) {
			beforeTime = -1L ;
		} else {
			beforeTime = DateUtils.praseStrToDate(beforeTimeStr).getTime() ;
		}
		endTime = DateUtils.praseStrToDate(endTimeStr).getTime() ;		
		List<LoginToDay> loginToDayList = loginToDayDao.findByinsertTime(beforeTime, endTime) ;
		for (LoginToDay loginToDay: loginToDayList) {			
			Long wid = loginToDay.getWid() ;
			Long loginTime = loginToDay.getInsertTime() ;
			String vk = loginToDay.getExt1() ;
			String ip = loginToDay.getLoginIp() ;
			if ( widList.size() > 0 && !widList.contains(wid)) {
				continue ;
			}
			
			boolean flag = (justWid == 0 )? (redpacketSendDao.findById(wid, activeId, redpacketId) == null):(redpacketSendDao.findById(wid) == null) ;
			if (flag) {				
				int usedNum = redpacketSendDao.countSendNum(activeId, redpacketId) ;
				log.info("the redpacket used log! activeRedpacketNum="
						+ totalNum + ",usedNum=" + usedNum + ",wid=" + wid ) ;
				if (usedNum >= (totalNum - 10)) {					
					activeResultPo.getErrWid().add(wid) ;
					break ;
				}
				Date date = new Date() ;
				RedpacketSendPo redpacketSendPo = new RedpacketSendPo() ;
				redpacketSendPo.setActiveDesc(activeDesc) ;
				redpacketSendPo.setActiveId(activeId) ;
				redpacketSendPo.setErrCode("0") ;
				redpacketSendPo.setInsertTime(date.getTime()) ;
				redpacketSendPo.setLoginTime(loginTime) ;
				redpacketSendPo.setRedpacketDesc(redpacketDesc) ;
				redpacketSendPo.setRedpacketId(redpacketId) ;
				redpacketSendPo.setSendNum(sendNum) ;
				redpacketSendPo.setStatus(0) ;
				redpacketSendPo.setUpdateTime(date.getTime()) ;
				redpacketSendPo.setWid(wid) ;
				redpacketSendPo.setExt1(vk) ;
				redpacketSendPo.setExt2(ip) ;
				int insertResult = redpacketSendDao.insertRedpacket(redpacketSendPo) ;
				log.info("insert log. wid=" + wid + ",activeId=" + activeId + ", redpacketId=" + redpacketId + ",totalNum=" + totalNum+ ",sendNum=" + sendNum + ",insertResult="+insertResult + ",vk=" + vk + ",ip=" + ip) ;
				Long errCode = -1l ;
				int status = -1 ;
				try {
					SendRedPacketResp sendRedPacketResp = RedPacketClient.sendRedPacket(wid, Long.valueOf(redpacketId), sendNum, vk, ip) ;
					if (sendRedPacketResp == null) {
						errCode = -1l ;
						status = -1 ;
					} else {
						errCode = sendRedPacketResp.getResult() ;
						if (0 == errCode) {
							status = 1 ;
						}
					}
				} catch (Exception ex) {
					log.error(ex.toString()) ;
					errCode = -1L;
					status = -1 ;
				} finally {
					log.info("send log. wid=" + wid + ",activeId=" + activeId + ", redpacketId=" + redpacketId + ",totalNum=" + totalNum+ ",sendNum=" + sendNum + ",vk=" + vk + ",ip=" + ip + ",result=" + errCode) ;
					redpacketSendPo.setErrCode(errCode.toString()) ;
					redpacketSendPo.setStatus(status) ;
					redpacketSendPo.setUpdateTime(new Date().getTime()) ;
					int updateResult = redpacketSendDao.updateRedpacketStatus(redpacketSendPo) ;
					log.info("send log. wid=" + wid + ",activeId=" + activeId + ", redpacketId=" + redpacketId + ",totalNum=" + totalNum+ ",sendNum=" + sendNum + ",vk=" + vk + ",ip=" + ip + ",result=" + errCode + ",updateResult=" + updateResult) ;

					if (1 == status) {
						activeResultPo.getSuccessWid().add(wid) ;
					} else {
						activeResultPo.getErrWid().add(wid) ;
					}					
				}
			}			
		}
		return activeResultPo;
	}
	
	@Override
	public void buDanForRedpacketForThread(String redpacketId, String activeId, int time) throws Exception {
		threadPool.execute(new BuDanRedpacketThread(redpacketId, activeId, time));
	}

	@Override
	public ActiveResultPo buDanForRedpacket(String redpacketId, String activeId, int time)
			throws Exception {
		// TODO Auto-generated method stub
		ActiveResultPo activeResultPo = new ActiveResultPo() ;
		Long beforeTime = new Date().getTime() - time*1000 ;		
		List<RedpacketSendPo> redpacketSendList = redpacketSendDao.findListForBuDan(activeId, redpacketId, beforeTime) ;
		for(RedpacketSendPo redpacketSendPo: redpacketSendList) {
			log.info("budan start.wid=" + redpacketSendPo.getWid() + ",redpacketId="+redpacketSendPo.getRedpacketId() + ",redpacketNum=" + redpacketSendPo.getSendNum() + ",activeId=" + redpacketSendPo.getActiveId() + ",vk=" + redpacketSendPo.getExt1() + ",ip=" + redpacketSendPo.getExt2() + ",errCode=" + redpacketSendPo.getErrCode() ) ;
			/*if ("852649".equals(redpacketSendPo.getErrCode()) ) {
				continue ;//恶意用户不再发券
			}*/ //放到SQL里直接过滤
			Long errCode = -1l ;
			int status = -1 ;			 
			try {
				SendRedPacketResp sendRedPacketResp = RedPacketClient.sendRedPacket(redpacketSendPo.getWid(), 
						Long.valueOf(redpacketSendPo.getRedpacketId()), redpacketSendPo.getSendNum(),redpacketSendPo.getExt1(),redpacketSendPo.getExt2()) ;
				if (sendRedPacketResp == null) {
					errCode = -1l ;
					status = -1 ;
				} else {
					errCode = sendRedPacketResp.getResult() ;
					if (0 == errCode) {
						status = 1 ;
					}
				}
			} catch (Exception ex) {
				errCode = -1L;
				status = -1 ;
			} finally {
				log.info("budan end.result="+ errCode + ",wid=" + redpacketSendPo.getWid() 
						+ ",redpacketId="+redpacketSendPo.getRedpacketId() + ",redpacketNum=" + redpacketSendPo.getSendNum() + ",activeId=" + redpacketSendPo.getActiveId()
						+ ",vk=" + redpacketSendPo.getExt1() + ",ip=" + redpacketSendPo.getExt2() ) ;
				redpacketSendPo.setErrCode(errCode.toString()) ;
				redpacketSendPo.setStatus(status) ;
				redpacketSendPo.setUpdateTime(new Date().getTime()) ;
				int updateResult = redpacketSendDao.updateRedpacketStatus(redpacketSendPo) ;
				log.info("budan end.result="+ errCode + ",wid=" + redpacketSendPo.getWid() 
						+ ",redpacketId="+redpacketSendPo.getRedpacketId() + ",redpacketNum=" + redpacketSendPo.getSendNum() + ",activeId=" + redpacketSendPo.getActiveId() + ",updateResult=" + updateResult
						+ ",vk=" + redpacketSendPo.getExt1() + ",ip=" + redpacketSendPo.getExt2() ) ;
				
				if (1 == status) {
					activeResultPo.getSuccessWid().add(redpacketSendPo.getWid()) ;
				} else {
					activeResultPo.getErrWid().add(redpacketSendPo.getWid()) ;
				}					
			}
		}
		
		return activeResultPo;
	}
	
	
	@Override
	public int isSend(long wid, String activeId, String redpacketId, String activeStatus, int justWid) throws Exception {
		// TODO Auto-generated method stub		
	
		RedpacketSendPo redpacketSendPo = null ;
		if (justWid == 1) {
			redpacketSendPo = redpacketSendDao.findById(wid) ;
		} else {
			redpacketSendPo = redpacketSendDao.findById(wid, activeId, redpacketId) ;
		}
		if (redpacketSendPo == null) {
			return RedpacketConstants.NO_SEND;
		} 
		int status = redpacketSendPo.getStatus() ;
		if (status == 0) {
			return RedpacketConstants.SEND_SENDING ;
		} 
		if (status == 1) {
			return RedpacketConstants.SEND_SUCCESS ;
		}
		if (status == -1) {
			return RedpacketConstants.SEND_ERROR ;
		}
					
		return RedpacketConstants.NO_SEND;
	}
	

	public LoginToDayDao getLoginToDayDao() {
		return loginToDayDao;
	}

	public void setLoginToDayDao(LoginToDayDao loginToDayDao) {
		this.loginToDayDao = loginToDayDao;
	}

	public RedpacketSendDao getRedpacketSendDao() {
		return redpacketSendDao;
	}

	public void setRedpacketSendDao(RedpacketSendDao redpacketSendDao) {
		this.redpacketSendDao = redpacketSendDao;
	}
	
	class BuDanRedpacketThread extends Thread {
		String redpacketId;
		String activeId;
		int time;
		public BuDanRedpacketThread(String redpacketId, String activeId, int time){
			this.redpacketId = redpacketId ; 
			this.activeId = activeId;
			this.time = time;
		}
		
		public void run() {
			// TODO Auto-generated method stub
			budanlog.info("start: redpacketId=" + redpacketId + ",activeId=" + activeId + ",time=" + time) ;
			try {				
				ActiveResultPo activeResultPo = buDanForRedpacket(redpacketId, activeId, time) ;
				JSONObject jstr = JSONObject.fromObject(activeResultPo) ;
				budanlog.info("result = " + jstr.toString()) ;				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				budanlog.info("errMSG = " + e.toString()) ;
			} 
			budanlog.info("end: redpacketId=" + redpacketId + ",activeId=" + activeId + ",time=" + time) ;
		}
		
	}
	
	class SendRedpacketThread extends Thread {
		String beforeTimeStr;
		String endTimeStr;
		String activeId;		
		String redpacketId;
		int totalNum;
		int sendNum;
		String activeDesc; 
		String redpacketDesc;
		List<Long> widList; 
		int justWid;
		public SendRedpacketThread (String beforeTimeStr, String endTimeStr, String activeId, 
	    		String redpacketId, int totalNum, int sendNum,String activeDesc, String redpacketDesc,List<Long> widList, int justWid){
			this.beforeTimeStr = beforeTimeStr;
			this.endTimeStr = endTimeStr;
			this.activeId = activeId;		
			this.redpacketId = redpacketId;
			this.totalNum = totalNum;
			this.sendNum = sendNum;
			this.activeDesc = activeDesc; 
			this.redpacketDesc = redpacketDesc;
			this.widList = widList; 
			this.justWid = justWid;
		}
		
		public void run() {
			// TODO Auto-generated method stub
			sendlog.info("begin:beforeTimeStr=" + beforeTimeStr + ", endTimeStr=" + endTimeStr + 
					", activeId=" + activeId + ", redpacketId=" + redpacketId + ", totalNum=" 
					+ totalNum + ", sendNum = " + sendNum + ",activeDesc=" + activeDesc 
					+ ", redpacketDesc=" + redpacketDesc + ",widList=" + widList + ",justWid=" + justWid) ;
			try {				
				ActiveResultPo activeResultPo = sendRedpacket(beforeTimeStr, endTimeStr, activeId, 
     		    		redpacketId, totalNum, sendNum,activeDesc, redpacketDesc,widList,justWid) ;
				JSONObject jstr = JSONObject.fromObject(activeResultPo) ;
				sendlog.info("result = " + jstr.toString()) ;				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				sendlog.info("errMSG = " + e.toString()) ;
			} 
			sendlog.info("end:beforeTimeStr=" + beforeTimeStr + ", endTimeStr=" + endTimeStr + 
					", activeId=" + activeId + ", redpacketId=" + redpacketId + ", totalNum=" 
					+ totalNum + ", sendNum = " + sendNum + ",activeDesc=" + activeDesc 
					+ ", redpacketDesc=" + redpacketDesc + ",widList=" + widList + ",justWid=" + justWid) ;
		}
		
	}
	

//    @Override
//    public Page<RedPacketPo> query(int state, long uin, String mk, int type,String sk) throws BusinessException {
//    	RedPacketClient rkgClient = IDLClientProxy.getIDLClient(RedPacketClient.class);
//    	GetRedPacketListResp resp = rkgClient.getRedPacketList(state, uin,type, sk);
//    	if (resp == null || resp.getResult() != 0) {
//              throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "红包列表IDL调用失败");
//        }
// 
//        Page<RedPacketPo> page = new Page<RedPacketPo>();
//        page.setRowCount(resp.getTotalNum());
//        ShopBiz shopbiz = (ShopBiz)SpringHelper.getBean("shopBiz");
//        try {
//            for (RedPacket redpacket : resp.getRedPacketList()) {
//                RedPacketPo po = new RedPacketPo();
//                po.setActualPrice(redpacket.getActualPrice());
//                po.setDealCode(redpacket.getDealCode());
//                po.setImageUrl(redpacket.getImageUrl());
//                po.setIp(redpacket.getIp());
//                po.setMinimum(redpacket.getMinimum());
//                po.setOwnerUin(redpacket.getOwnerUin());
//                po.setPacketFlag(redpacket.getPacketFlag());
//                po.setPacketId(redpacket.getPacketId());
//                po.setPacketName(redpacket.getPacketName());
//                po.setPacketPrice(redpacket.getPacketPrice());
//                po.setPacketStockId(redpacket.getPacketStockId());
//                po.setRelaUrl(redpacket.getRelaUrl());
//                po.setSellerUin(redpacket.getSellerUin());
//                po.setState(redpacket.getState());
//                po.setType(redpacket.getType());
//                po.setWithdrawId(redpacket.getWithdrawId());
//                
//                po.setBeginTime(redpacket.getBeginTime() * 1000);
//                po.setEndTime(redpacket.getEndTime() * 1000);
//                po.setRecvTime(redpacket.getRecvTime() * 1000);
//                po.setUseTime(redpacket.getUseTime() * 1000);
//                po.setWithdrawTime(redpacket.getWithdrawTime() * 1000);
////                if(StringUtils.isNotEmpty(redpacket.getApplicableScope())){
////                    String shops = StringUtils.substringBetween(redpacket.getApplicableScopeList(), "[", "]");
////                    for (String seller : shops.split(",")){
////                        po.getApplicableScope().add(new uint32_t(StringUtil.toLong(seller, 0)));
////                        po.getApplicableShopName().add(shopbiz.getShopName(StringUtil.toLong(seller, 0), null));
////                    }
////                }
//                for (uint32_t seller : redpacket.getApplicableScope()) {
//                	 po.getApplicableScope().add(seller);
//                     po.getApplicableShopName().add(shopbiz.getShopName(seller.getValue(), null));
//                }
//                page.getElements().add(po);
//            }
//        } catch (Exception e) {
//            throw BusinessException.createInstance(BusinessErrorType.UNKNOWN_ERROR, e);
//        }
//        return page;
//    }

	
}
