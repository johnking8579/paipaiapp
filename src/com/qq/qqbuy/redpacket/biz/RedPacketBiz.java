package com.qq.qqbuy.redpacket.biz;

import java.util.List;
import java.util.Map;

import com.qq.qqbuy.common.Page;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.redpacket.po.ActiveResultPo;
import com.qq.qqbuy.redpacket.po.RedPacketPo;

public interface RedPacketBiz {

    public Map<Long, Long> count(long uin, String mk,String sk) throws BusinessException; 
    
    public Page<RedPacketPo> query(int state, long uin, String mk,String sk) throws BusinessException; 
    
    /**
     * 
     * @param uin
     * @param mk
     * @param type 1为红包 2为店铺优惠券
     * @return
     * @throws BusinessException
     */
    public Map<Long, Long> count(long uin, String mk , int type,String sk) throws BusinessException; 
    
    /**
     * 
     * @param state  状态,待使用(100),使用中--绑定和冻结(101)，已使用(102),过期(2)
     * @param uin
     * @param mk
     * @param type 1为红包 2为店铺优惠券4平台代金券
     * @return
     * @throws BusinessException
     */
    public Page<RedPacketPo> query(int state, long uin, String mk, int type,String sk) throws BusinessException; 
    
    
    public void sendRedpacketForThread(String beforeTimeStr, String endTimeStr, String activeId, 
    		String redpacketId, int totalNum, int sendNum,String activeDesc, String redpacketDesc,List<Long> widList, int justWid) throws Exception;
    /**
     * 
     * @param beforeTime 开始时间，-1或是yyyyMMddHHmmss
     * @param endTime 结束时间yyyyMMddHHmmss
     * @param activeId 活动ID
     * @param redpacketId 代金券批次号
     * @param num 每个用户发放的数目
     * @param justWid 是否发过就不发（0:wid,activeId,redpacketId都对应，才不发；  1：只有WID发过，就不发）
     * @return
     * @throws BusinessException
     */
    public ActiveResultPo sendRedpacket(String beforeTimeStr, String endTimeStr, String activeId, 
    		String redpacketId, int totalNum, int sendNum,String activeDesc, String redpacketDesc,List<Long> widList, int justWid) throws Exception;
    /**
     * 
     * @param redpacketId 代金券批次号
     * @param actinveId 活动ID
     * @param time 时间（单位秒），处理update为time之前的异常单据
     * @return
     * @throws BusinessException
     */
    public ActiveResultPo buDanForRedpacket(String redpacketId, String activeId,int time) throws Exception ;
    
    public void buDanForRedpacketForThread(String redpacketId, String activeId,int time) throws Exception ;

    /**
	 * 用户是否发放过代金券
	 * @param wid
	 * @return
	 * @throws Exception 1：未发放  0：发放成功  -1：发放中  -2：发放失败，补单程序会进行补单操作 -3：当前无活动 
	 */
	public int isSend(long wid, String activeId, String redpacketId, String activeStatus, int justWid) throws Exception ;
}
