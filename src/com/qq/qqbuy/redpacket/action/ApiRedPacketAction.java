package com.qq.qqbuy.redpacket.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import net.sf.json.JsonConfig;

import org.apache.commons.lang3.StringUtils;

import com.qq.qqbuy.common.ConfigHelper;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.Page;
import com.qq.qqbuy.common.action.PaipaiApiBaseAction;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.StrMD5;
import com.qq.qqbuy.redpacket.biz.RedPacketBiz;
import com.qq.qqbuy.redpacket.po.ActiveResultPo;
import com.qq.qqbuy.redpacket.po.RedPacketPo;
import com.qq.qqbuy.redpacket.util.RedpacketConstants;

public class ApiRedPacketAction extends PaipaiApiBaseAction {

	private static final long serialVersionUID = 1L;
	
	private String beforeTimeStr;
	private String endTimeStr;

	// 红包状态,待使用(100),使用中--绑定和冻结(101)，已使用(102),过期(2)
    private int state = -1;

    private RedPacketBiz redPacketBiz;
    
    private String wids ;

    private String datastr;
    
    private int type = 1;
    
    private String time ;
    
    private String validateKey ;
    
    private int isSendCode ;
    
    public String query() {
//        long start = System.currentTimeMillis();
        try {

            Page<RedPacketPo> redPackets;
            // 鉴权
            boolean checkResult = this.checkLoginAndGetSkey();
            if (!checkResult) {
                throw BusinessException.createInstance(BusinessErrorType.NOT_LOGIN);
            }

            if (!(state == 2 || state == 100 || state == 101 || state == 102) && state != -1) {
            	state=100;
            }
            if (type == 1 || type == 2 || type == 4){//1.红包、2.优惠券、4.代金券
            	redPackets = redPacketBiz.query(state, getQq(), getMk(), type,getSk());
            }else{
            	redPackets = redPacketBiz.query(state, getQq(), getMk(),4, getSk());
            }
            if (redPackets != null) {
                JsonConfig config = new JsonConfig();
                String[] excludes = { "size", "empty", "pageNo", "pageSize", "pageTotal" };
                config.setExcludes(excludes);
                datastr = obj2Json(redPackets, config);
            }
            return RST_SUCCESS;
        } catch (BusinessException e) {
            setErrCodeAndMsg4BExp(e, "QQ=" + getQq() + " state=" + state);
            return RST_API_FAILURE;
        } catch (Throwable e) {
            setErrcodeAndMsg4Throwable(e, "QQ=" + getQq() + " state=" + state);
            return RST_API_FAILURE;
        } finally {
            result.setDtag(dtag);
        }
    }
    
    public String isSend() {
//        long start = System.currentTimeMillis();
        isSendCode = RedpacketConstants.NO_SEND ;
        try {
            boolean checkResult = this.checkLoginAndGetSkey();
            if (!checkResult) {
                throw BusinessException.createInstance(BusinessErrorType.NOT_LOGIN);
            }
            String activeId = ConfigHelper.getStringByKey("activeId", "");
    		String redpacketId = ConfigHelper.getStringByKey("redpacketId", "");
    		String activeStatus = ConfigHelper.getStringByKey("activeStatus", "");
    		int justWid = ConfigHelper.getIntByKey("justWid", 0);
    		Log.run.info("isSend start! activeId=" + activeId + ",redpacketId=" + redpacketId + ",activeStatus=" + activeStatus) ;  
    		if (StringUtils.isEmpty(activeId) || StringUtils.isEmpty(redpacketId) || 
    				StringUtils.isEmpty(activeStatus) || !activeStatus.equals("0")) {
    			isSendCode = RedpacketConstants.SEND_NO_ACTIVE ;
    		} else {
    			isSendCode = redPacketBiz.isSend(getWid(), activeId, redpacketId, activeStatus, justWid) ;            
    		}
    		Log.run.info("isSend end! errCode=" + isSendCode + ", activeId=" + activeId + ",redpacketId=" + redpacketId + ",activeStatus=" + activeStatus) ;    		
            return RST_SUCCESS;
        } catch (BusinessException e) {
            setErrCodeAndMsg4BExp(e, "QQ=" + getQq());
            return RST_API_FAILURE;
        } catch (Throwable e) {
            setErrcodeAndMsg4Throwable(e, "QQ=" + getQq());
            return RST_API_FAILURE;
        } finally {
            result.setDtag(dtag);
        }
    }

    public String buDanForRedpacket() {
        try {
        	if (StringUtils.isEmpty(time) || StringUtils.isEmpty(validateKey)) {
        		throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR);
        	} 
        	if (!new StrMD5(time + RedpacketConstants.KEY_FOR_BUDAN).getResult().equals(validateKey)) {
        		throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR);
        	}
        	try {
        		Long timel = Long.parseLong(time) ;
        		if (new Date().getTime() - timel > 60000 || new Date().getTime() - timel < -60000) {
        			throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR);
        		}
        	} catch (Exception ex) {
        		throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR);
        	}
            String activeId = ConfigHelper.getStringByKey("activeId", "");
    		String redpacketId = ConfigHelper.getStringByKey("redpacketId", "");
     		String activeStatus = ConfigHelper.getStringByKey("activeStatus", "");
     		if (StringUtils.isEmpty(activeStatus) || StringUtils.isEmpty(activeId) || StringUtils.isEmpty(redpacketId) || !activeStatus.equals("0")) {
     			datastr = "{}" ;
     		} else {
     			redPacketBiz.buDanForRedpacketForThread(redpacketId, activeId, RedpacketConstants.BUDAN_BEFORE_TIME) ;
     			datastr = "{}";
     		}
            return RST_SUCCESS;
        } catch (BusinessException e) {
            setErrCodeAndMsg4BExp(e, "");
            return RST_API_FAILURE;
        } catch (Throwable e) {
        	e.printStackTrace() ;
            setErrcodeAndMsg4Throwable(e, "");
            return RST_API_FAILURE;
        } finally {
            result.setDtag(dtag);
        }
    }
    
    public String sendRedpacket() {
        try {
        	if (StringUtils.isEmpty(beforeTimeStr) || StringUtils.isEmpty(endTimeStr) ||StringUtils.isEmpty(time) || StringUtils.isEmpty(validateKey)) {
        		throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR);
        	} 
        	if (!new StrMD5(beforeTimeStr + endTimeStr + time + RedpacketConstants.KEY_FOR_SEND).getResult().equals(validateKey)) {
        		throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR);
        	}
        	try {
        		Long timel = Long.parseLong(time) ;
        		if (new Date().getTime() - timel > 60000 || new Date().getTime() - timel < -60000) {
        			throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR);
        		}
        	} catch (Exception ex) {
        		throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR);
        	}
            String activeId = ConfigHelper.getStringByKey("activeId", "");
    		String activeDesc = ConfigHelper.getStringByKey("activeDesc", "");
     		int redpacketTotalNum = ConfigHelper.getIntByKey("redpacketTotalNum", 0);
     		int redpacketSendNum = ConfigHelper.getIntByKey("redpacketSendNum", 1);
    		String redpacketId = ConfigHelper.getStringByKey("redpacketId", "");
    		String redpacketDesc = ConfigHelper.getStringByKey("redpacketDesc", "");
     		String activeStatus = ConfigHelper.getStringByKey("activeStatus", "");
     		int justWid = ConfigHelper.getIntByKey("justWid", 0);
     		if (redpacketSendNum > 10) {
     			throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR);
     		}
     		if (StringUtils.isEmpty(activeStatus) || StringUtils.isEmpty(activeId) || StringUtils.isEmpty(redpacketId) || !activeStatus.equals("0")) {
     			datastr = "{}" ;
     		} else {
     			List<Long> widList = new ArrayList<Long>() ;
     			if (!StringUtils.isEmpty(wids) && !wids.equals("null")) {
     				String[] widArr = wids.split("\\|") ;
         			for(String widStr:widArr) {
         				widList.add(Long.parseLong(widStr)) ;
         			}
     			}     			
     			redPacketBiz.sendRedpacketForThread(beforeTimeStr, endTimeStr, activeId, 
     		    		redpacketId, redpacketTotalNum, redpacketSendNum,activeDesc, redpacketDesc,widList,justWid) ;
     			datastr = "{}";
     		}
            return RST_SUCCESS;
        } catch (BusinessException e) {
            setErrCodeAndMsg4BExp(e, "");
            return RST_API_FAILURE;
        } catch (Throwable e) {
            setErrcodeAndMsg4Throwable(e, "");
            e.printStackTrace() ;
            return RST_API_FAILURE;
        } finally {
            result.setDtag(dtag);
        }
    }
    
    public String count() {
        long start = System.currentTimeMillis();
        try {

            // 鉴权
            boolean checkResult = this.checkLoginAndGetSkey();
            if (!checkResult) {
                throw BusinessException.createInstance(BusinessErrorType.NOT_LOGIN);
            }
            Map<Long, Long> counts;
            if (type == 1 || type == 2  || type == 4){ //1.红包、2.优惠券、4.代金券
            	counts = redPacketBiz.count(getQq(), getMk(), type, getSk());
            }else{
            	counts = redPacketBiz.count(getQq(), getMk(), 4, getSk());
            }
            if (counts != null) {
                datastr = obj2Json(counts);
            }

            return RST_SUCCESS;
        } catch (BusinessException e) {
            setErrCodeAndMsg4BExp(e, "QQ=" + getQq());
            return RST_API_FAILURE;
        } catch (Throwable e) {
            setErrcodeAndMsg4Throwable(e, "QQ=" + getQq());
            return RST_API_FAILURE;
        } finally {
            result.setDtag(dtag);
        }
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public RedPacketBiz getRedPacketBiz() {
        return redPacketBiz;
    }

    public void setRedPacketBiz(RedPacketBiz redPacketBiz) {
        this.redPacketBiz = redPacketBiz;
    }

    public String getDatastr() {
        return datastr;
    }

    public void setDatastr(String datastr) {
        this.datastr = datastr;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

	public String getBeforeTimeStr() {
		return beforeTimeStr;
	}

	public void setBeforeTimeStr(String beforeTimeStr) {
		this.beforeTimeStr = beforeTimeStr;
	}

	public String getEndTimeStr() {
		return endTimeStr;
	}

	public void setEndTimeStr(String endTimeStr) {
		this.endTimeStr = endTimeStr;
	}

	

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getWids() {
		return wids;
	}

	public void setWids(String wids) {
		this.wids = wids;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getValidateKey() {
		return validateKey;
	}

	public void setValidateKey(String validateKey) {
		this.validateKey = validateKey;
	}

	public int getIsSendCode() {
		return isSendCode;
	}

	public void setIsSendCode(int isSendCode) {
		this.isSendCode = isSendCode;
	}

    
}
