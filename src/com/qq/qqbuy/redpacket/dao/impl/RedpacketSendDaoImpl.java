package com.qq.qqbuy.redpacket.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.qq.qqbuy.common.ConfigHelper;
import com.qq.qqbuy.common.constant.CommonConstants;
import com.qq.qqbuy.login.dao.LoginToDayDao;
import com.qq.qqbuy.login.po.LoginToDay;
import com.qq.qqbuy.redpacket.dao.RedpacketSendDao;
import com.qq.qqbuy.redpacket.po.RedpacketSendPo;

public class RedpacketSendDaoImpl implements RedpacketSendDao {

	private JdbcTemplate jdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	
	@Override
	public RedpacketSendPo findById(long wid, String activeId,
			String redpacketId) throws Exception {
		// TODO Auto-generated method stub
		List<RedpacketSendPo> list = jdbcTemplate.query(
				"select * from redpacketSendLog where wid= ? and activeId=? and redpacketId = ?",
				new Object[] { wid, activeId, redpacketId}, new RedpacketSendPoRowMapper());
		if (list != null && list.size() > 0) {
			return list.get(0) ;
		} else {
			return null ;
		}
	}
	
	@Override
	public RedpacketSendPo findById(long wid) throws Exception {
		// TODO Auto-generated method stub
		List<RedpacketSendPo> list = jdbcTemplate.query(
				"select * from redpacketSendLog where wid= ? ",
				new Object[] { wid}, new RedpacketSendPoRowMapper());
		if (list != null && list.size() > 0) {
			return list.get(0) ;
		} else {
			return null ;
		}
	}

	@Override
	public List<RedpacketSendPo> findListForBuDan(String activeId,
			String redpacketId, Long beforeTime) throws Exception {
		// TODO Auto-generated method stub
		List<RedpacketSendPo> list = jdbcTemplate.query(
				"select * from redpacketSendLog where activeId=? and redpacketId = ? and updateTime < ? and status != 1 and errCode != '852649'",
				new Object[] { activeId, redpacketId, beforeTime}, new RedpacketSendPoRowMapper());
		return list ;
	}

	@Override
	public int insertRedpacket(RedpacketSendPo redpacketSendPo) throws Exception {
		// TODO Auto-generated method stub
		return jdbcTemplate
		.update("insert into redpacketSendLog (wid,activeId,redpacketId,loginTime,sendNum,status,errCode"
				+ ",insertTime,updateTime,redpacketDesc,activeDesc,ext1,ext2,ext3) values ("
				+ "?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
				new Object[] { redpacketSendPo.getWid(),
						redpacketSendPo.getActiveId(),
						redpacketSendPo.getRedpacketId(),
						redpacketSendPo.getLoginTime(),
						redpacketSendPo.getSendNum(),
						redpacketSendPo.getStatus(),
						redpacketSendPo.getErrCode(),
						redpacketSendPo.getInsertTime(),
						redpacketSendPo.getUpdateTime(),
						redpacketSendPo.getRedpacketDesc(),
						redpacketSendPo.getActiveDesc(),
						redpacketSendPo.getExt1(),
						redpacketSendPo.getExt2(),
						redpacketSendPo.getExt3()});
	}

	@Override
	public int updateRedpacketStatus(RedpacketSendPo redpacketSendPo)
			throws Exception {
		// TODO Auto-generated method stub
		return jdbcTemplate
		.update("update redpacketSendLog set status = ?, errCode = ?, updateTime = ? where wid=? and activeId = ? and redpacketId=? ",
				new Object[] {
				redpacketSendPo.getStatus(),
				redpacketSendPo.getErrCode(),
				redpacketSendPo.getUpdateTime(),
				redpacketSendPo.getWid(),
				redpacketSendPo.getActiveId(),
				redpacketSendPo.getRedpacketId() });
	}
	
	class RedpacketSendPoRowMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int index) throws SQLException {
			RedpacketSendPo redpacketSend = new RedpacketSendPo();
			redpacketSend.setActiveDesc(rs.getString("activeDesc"));
			redpacketSend.setActiveId(rs.getString("activeId")) ;
			redpacketSend.setErrCode(rs.getString("errCode")) ;
			redpacketSend.setExt1(rs.getString("ext1"));
			redpacketSend.setExt2(rs.getString("ext2"));
			redpacketSend.setExt3(rs.getString("ext3"));
			redpacketSend.setInsertTime(rs.getLong("insertTime")) ;
			redpacketSend.setLoginTime(rs.getLong("loginTime")) ;
			redpacketSend.setRedpacketDesc(rs.getString("redpacketDesc"));
			redpacketSend.setRedpacketId(rs.getString("redpacketId"));
			redpacketSend.setSendNum(rs.getInt("sendNum")) ;
			redpacketSend.setStatus(rs.getInt("status")) ;
			redpacketSend.setUpdateTime(rs.getLong("updateTime")) ;
			redpacketSend.setWid(rs.getLong("wid")) ;
			
			return redpacketSend;
		}
	}

	@Override
	public int countSendNum(String activeId, String redpacketId)
			throws Exception {
		return jdbcTemplate.queryForInt("select count(sendNum) from redpacketSendLog where activeId=? and redpacketId = ?",
				new Object[] { activeId, redpacketId}) ;
	}


	

}
