package com.qq.qqbuy.redpacket.po;

public class RedpacketSendPo {

	private long wid ;
	private String activeId ;
	private String redpacketId ;
	private long loginTime ;
	private int sendNum ;
	private int status ;
	private String errCode ;
	private long insertTime ;
	private long updateTime ;
	private String redpacketDesc ;
	private String activeDesc ;
	private String ext1 ;
	private String ext2 ;
	private String ext3 ;
	public long getWid() {
		return wid;
	}
	public void setWid(long wid) {
		this.wid = wid;
	}
	public String getActiveId() {
		return activeId;
	}
	public void setActiveId(String activeId) {
		this.activeId = activeId;
	}
	
	public String getRedpacketId() {
		return redpacketId;
	}
	public void setRedpacketId(String redpacketId) {
		this.redpacketId = redpacketId;
	}
	public long getLoginTime() {
		return loginTime;
	}
	public void setLoginTime(long loginTime) {
		this.loginTime = loginTime;
	}
	public int getSendNum() {
		return sendNum;
	}
	public void setSendNum(int sendNum) {
		this.sendNum = sendNum;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getErrCode() {
		return errCode;
	}
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
	public long getInsertTime() {
		return insertTime;
	}
	public void setInsertTime(long insertTime) {
		this.insertTime = insertTime;
	}
	public long getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(long updateTime) {
		this.updateTime = updateTime;
	}
	public String getExt1() {
		return ext1;
	}
	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}
	public String getExt2() {
		return ext2;
	}
	public void setExt2(String ext2) {
		this.ext2 = ext2;
	}
	public String getExt3() {
		return ext3;
	}
	public void setExt3(String ext3) {
		this.ext3 = ext3;
	}
	public String getRedpacketDesc() {
		return redpacketDesc;
	}
	public void setRedpacketDesc(String redpacketDesc) {
		this.redpacketDesc = redpacketDesc;
	}
	public String getActiveDesc() {
		return activeDesc;
	}
	public void setActiveDesc(String activeDesc) {
		this.activeDesc = activeDesc;
	}
	
	
}
