package com.qq.qqbuy.redpacket.po;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.paipai.lang.uint32_t;

public class RedPacketPo {

	/**
	 * 红包编号
	 */
    private long packetId;
    /**
     * 红包名称
     */
    private String packetName = new String();
    /**
     *  红包类型（红包、代金券、店铺优惠券、包邮卡）
     */
    private long type;
    /**
     * 红包批次
     */
    private long packetStockId;
    /**
     * 所有者QQ
     */
    private long ownerUin;
    /**
     * 红包面值
     */
    private long packetPrice;
    /**
     * 红包标识
     */
    private long packetFlag;
    /**
     * 订单卖家QQ
     */
    private long sellerUin;
    /**
     * 生效日期
     */
    private long beginTime;
    /**
     *  失效日期
     */
    private long endTime;
    /**
     * ip地址
     */
    private String ip = new String();
    /**
     * 领取时间
     */
    private long recvTime;
    /**
     * 使用时间
     */
    private long useTime;
    /**
     * 红包状态
     */
    private long state;
    /**
     * 关联订单编号
     */
    private String dealCode = new String();
    /**
     * 关联地址
     */
    private String relaUrl = new String();
    /**
     * 图片地址
     */
    private String imageUrl = new String();
    /**
     * 实际抵扣金额
     */
    private long actualPrice;
    /**
     * 兑现编号
     */
    private long withdrawId;
    /**
     * 兑现时间
     */
    private long withdrawTime;
    /**
     * 最低消费
     */
    private long minimum;
    /**
     * 适用店铺
     */
    private Set<String> applicableShopName = new HashSet<String>();
    
    /**
     * 适用范围
     */
    private Set<uint32_t> applicableScope = new HashSet<uint32_t>();
    
    public long getMinimum() {
		return minimum;
	}

	public void setMinimum(long minimum) {
		this.minimum = minimum;
	}

    public long getPacketId() {
        return packetId;
    }

    public void setPacketId(long packetId) {
        this.packetId = packetId;
    }

    public String getPacketName() {
        return packetName;
    }

    public void setPacketName(String packetName) {
        this.packetName = packetName;
    }

    public long getType() {
        return type;
    }

    public void setType(long type) {
        this.type = type;
    }

    public long getPacketStockId() {
        return packetStockId;
    }

    public void setPacketStockId(long packetStockId) {
        this.packetStockId = packetStockId;
    }

    public long getOwnerUin() {
        return ownerUin;
    }

    public void setOwnerUin(long ownerUin) {
        this.ownerUin = ownerUin;
    }

    public long getPacketPrice() {
        return packetPrice;
    }

    public void setPacketPrice(long packetPrice) {
        this.packetPrice = packetPrice;
    }

    public long getPacketFlag() {
        return packetFlag;
    }

    public void setPacketFlag(long packetFlag) {
        this.packetFlag = packetFlag;
    }

    public long getSellerUin() {
        return sellerUin;
    }

    public void setSellerUin(long sellerUin) {
        this.sellerUin = sellerUin;
    }

    public long getBeginTime() {
        return beginTime;
    }
    
    public Date getBeginDate() {
    	return new Date(this.getEndTime());
    }

    public void setBeginTime(long beginTime) {
        this.beginTime = beginTime;
    }

    public long getEndTime() {
        return endTime;
    }
    
    public Date getEndDate() {
    	return new Date(this.getEndTime());
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public long getRecvTime() {
        return recvTime;
    }

    public void setRecvTime(long recvTime) {
        this.recvTime = recvTime;
    }

    public long getUseTime() {
        return useTime;
    }

    public void setUseTime(long useTime) {
        this.useTime = useTime;
    }

    public long getState() {
        return state;
    }

    public void setState(long state) {
        this.state = state;
    }

    public String getDealCode() {
        return dealCode;
    }

    public void setDealCode(String dealCode) {
        this.dealCode = dealCode;
    }

    public String getRelaUrl() {
        return relaUrl;
    }

    public void setRelaUrl(String relaUrl) {
        this.relaUrl = relaUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public long getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(long actualPrice) {
        this.actualPrice = actualPrice;
    }

    public long getWithdrawId() {
        return withdrawId;
    }

    public void setWithdrawId(long withdrawId) {
        this.withdrawId = withdrawId;
    }

    public long getWithdrawTime() {
        return withdrawTime;
    }

    public void setWithdrawTime(long withdrawTime) {
        this.withdrawTime = withdrawTime;
    }

    public Set<uint32_t> getApplicableScope() {
        return applicableScope;
    }

    public void setApplicableScope(Set<uint32_t> applicableScope) {
        this.applicableScope = applicableScope;
    }

    public Set<String> getApplicableShopName() {
        return applicableShopName;
    }

    public void setApplicableShopName(Set<String> applicableShopName) {
        this.applicableShopName = applicableShopName;
    }
    
    

}
