package com.qq.qqbuy.redpacket.po;

import java.util.ArrayList;
import java.util.List;

public class ActiveResultPo {

	private List<Long> successWid = new ArrayList<Long>() ;
	private List<Long> errWid = new ArrayList<Long>() ;
	
	
	public List<Long> getSuccessWid() {
		return successWid;
	}
	public void setSuccessWid(List<Long> successWid) {
		this.successWid = successWid;
	}
	public List<Long> getErrWid() {
		return errWid;
	}
	public void setErrWid(List<Long> errWid) {
		this.errWid = errWid;
	}
	
	
	
}
