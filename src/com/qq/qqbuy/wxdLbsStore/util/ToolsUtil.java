package com.qq.qqbuy.wxdLbsStore.util;

import com.qq.qqbuy.thirdparty.idl.wxdLbsStore.protocol.LbsStoreInfoAllPo;
import com.qq.qqbuy.thirdparty.idl.wxdLbsStore.protocol.QueryStoreInfoByNearResp;
import com.qq.qqbuy.wxdLbsStore.po.WxdItemPo;
import com.qq.qqbuy.wxdLbsStore.po.WxdListPo;

public class ToolsUtil {

	public static final String mapQQUrl = "http://apis.map.qq.com/ws/geocoder/v1/?get_poi=0&";
	public static final String mapQQKey = "ETABZ-E6R34-LF4UQ-XPMOY-QNL73-62BOS"; // 腾讯地图开放接口密钥
	public static final String mapQQCoordTypeForGPS = "1"; // GPS定位（目前IOS使用）
	public static final String mapQQCoordTypeForBaiDu = "3"; // ANDROID定位（目前android使用）
	public static final long rat = 1000000L ;
	public static final double ratD = 1000000.0 ;
	private static final double EARTH_RADIUS = 6378137;
	private static double rad(double d) {
		return d * Math.PI / 180.0;
	}

	/**
	 * 根据两点间经纬度坐标（double值），计算两点间距离，单位为米
	 * 
	 * @param lng1
	 * @param lat1
	 * @param lng2
	 * @param lat2
	 * @return
	 */
	public static double GetDistance(double lng1, double lat1, double lng2,
			double lat2) {
		double radLat1 = rad(lat1);
		double radLat2 = rad(lat2);
		double a = radLat1 - radLat2;
		double b = rad(lng1) - rad(lng2);
		double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2)
				+ Math.cos(radLat1) * Math.cos(radLat2)
				* Math.pow(Math.sin(b / 2), 2)));
		s = s * EARTH_RADIUS;
		s = Math.round(s * 10000) / 10000;
		return s;
	}

	public static void main(String[] args) {
		System.out.println((long)GetDistance(117.177975,39.144951,0,0));
	}
	/**
	 * @param lbsStoreInfoAllPo
	 * @return
	 */
	public static WxdItemPo converToObject(LbsStoreInfoAllPo lbsStoreInfoAllPo) {
		WxdItemPo wxdItemPo = new WxdItemPo();
		wxdItemPo.setAddress1(lbsStoreInfoAllPo.getAddress1());
		wxdItemPo.setAddress2(lbsStoreInfoAllPo.getAddress2());
		wxdItemPo.setAddress3(lbsStoreInfoAllPo.getAddress3());
		wxdItemPo.setAddress4(lbsStoreInfoAllPo.getAddress4());
		wxdItemPo.setAddress5(lbsStoreInfoAllPo.getAddress5());
		wxdItemPo.setAddress2Id(lbsStoreInfoAllPo.getAddress2Id());
		wxdItemPo.setAddress3Id(lbsStoreInfoAllPo.getAddress3Id());
		wxdItemPo.setAddress4Id(lbsStoreInfoAllPo.getAddress4Id());
		wxdItemPo.setAddress5Id(lbsStoreInfoAllPo.getAddress5Id());
		wxdItemPo.setDetailAddress(lbsStoreInfoAllPo.getDetailAddress());
		wxdItemPo.setFlatitude(lbsStoreInfoAllPo.getFlatitude()/ratD) ;
		wxdItemPo.setFlongitude(lbsStoreInfoAllPo.getFlongitude()/ratD) ;
		wxdItemPo.setIntroduction(lbsStoreInfoAllPo.getIntroduction()) ;
		wxdItemPo.setOpenTime(lbsStoreInfoAllPo.getOpenTime()) ;
		wxdItemPo.setProductcategory(lbsStoreInfoAllPo.getProductcategory()) ;
		wxdItemPo.setRenPrice(lbsStoreInfoAllPo.getRenPrice()) ;
		wxdItemPo.setSpecService(lbsStoreInfoAllPo.getSpecService()) ;
		wxdItemPo.setStoreId(lbsStoreInfoAllPo.getStoreId()) ;
		wxdItemPo.setStoreName(lbsStoreInfoAllPo.getStoreName()) ;
		wxdItemPo.setTelphone(lbsStoreInfoAllPo.getTelphone()) ;
		wxdItemPo.setUin(lbsStoreInfoAllPo.getUin()) ;
		return wxdItemPo ;

	}

}
