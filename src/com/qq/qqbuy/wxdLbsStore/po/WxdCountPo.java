package com.qq.qqbuy.wxdLbsStore.po;

public class WxdCountPo {

	/**
	 * 卖家QQ
	 */
	private long uin ;
	/**
	 * 店铺名称
	 */
	private String storeName ;
	/**
	 * QQ号对应的门店数量
	 */
	private long storeCount ;
	/**
	 * 店铺logo
	 */
	private String storeLogo ;
	
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public long getStoreCount() {
		return storeCount;
	}
	public void setStoreCount(long storeCount) {
		this.storeCount = storeCount;
	}
	public String getStoreLogo() {
		return storeLogo;
	}
	public void setStoreLogo(String storeLogo) {
		this.storeLogo = storeLogo;
	}
	public long getUin() {
		return uin;
	}
	public void setUin(long uin) {
		this.uin = uin;
	}
	
	
}
