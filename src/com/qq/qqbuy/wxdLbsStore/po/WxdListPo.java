package com.qq.qqbuy.wxdLbsStore.po;

import java.util.ArrayList;
import java.util.List;

public class WxdListPo {

	/**
	 * 满足条件总数
	 */
	private long totalNum ;
	/**
	 * 分页查询到的list信息
	 */
	private List<WxdItemPo> wxdList = new ArrayList<WxdItemPo>() ;
	
	public long getTotalNum() {
		return totalNum;
	}
	public void setTotalNum(long totalNum) {
		this.totalNum = totalNum;
	}
	public List<WxdItemPo> getWxdList() {
		return wxdList;
	}
	public void setWxdList(List<WxdItemPo> wxdList) {
		this.wxdList = wxdList;
	}
	
}
