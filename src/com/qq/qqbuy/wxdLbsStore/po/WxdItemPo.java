package com.qq.qqbuy.wxdLbsStore.po;

public class WxdItemPo {

	/**
	 * 用户qq
	 * 
	 * 版本 >= 0
	 */
	private long uin;

	/**
	 * 门店id
	 * 
	 * 版本 >= 0
	 */
	private String storeId = new String();

	/**
	 * 门店地址（国）
	 * 
	 * 版本 >= 0
	 */
	private String address1 = new String();

	/**
	 * 门店地址（省）
	 * 
	 * 版本 >= 0
	 */
	private String address2 = new String();

	/**
	 * 门店地址（省id）
	 * 
	 * 版本 >= 0
	 */
	private long address2Id;

	/**
	 * 门店地址（市）
	 * 
	 * 版本 >= 0
	 */
	private String address3 = new String();

	/**
	 * 门店地址（市id）
	 * 
	 * 版本 >= 0
	 */
	private long address3Id;

	/**
	 * 门店地址（区）
	 * 
	 * 版本 >= 0
	 */
	private String address4 = new String();

	/**
	 * 门店地址（区id）
	 * 
	 * 版本 >= 0
	 */
	private long address4Id;

	/**
	 * 门店地址（街道）
	 * 
	 * 版本 >= 0
	 */
	private String address5 = new String();

	/**
	 * 门店地址（街道id）
	 * 
	 * 版本 >= 0
	 */
	private long address5Id;

	/**
	 * 门店详细地址
	 * 
	 * 版本 >= 0
	 */
	private String detailAddress = new String();

	/**
	 * 门店坐标（纬）
	 * 
	 * 版本 >= 0
	 */
	private double flatitude;

	/**
	 * 门店坐标（经）
	 * 
	 * 版本 >= 0
	 */
	private double flongitude;

	/**
	 * 门店名称
	 * 
	 * 版本 >= 0
	 */
	private String storeName = new String();

	/**
	 * 主营类目
	 * 
	 * 版本 >= 0
	 */
	private String productcategory = new String();

	/**
	 * 联系电话
	 * 
	 * 版本 >= 0
	 */
	private String telphone = new String();

	/**
	 * 人均价格
	 * 
	 * 版本 >= 0
	 */
	private long renPrice;

	/**
	 * 营业时间
	 * 
	 * 版本 >= 0
	 */
	private String openTime = new String();

	/**
	 * 卖家推荐信息
	 * 
	 * 版本 >= 0
	 */
	private String recommend = new String();

	/**
	 * 特色服务
	 * 
	 * 版本 >= 0
	 */
	private String specService = new String();

	/**
	 * 简介
	 * 
	 * 版本 >= 0
	 */
	private String introduction = new String();
	/**
	 * 门店LOGO
	 */
	private String storeLogo = new String();

	/**
	 * 卖家信用等级（具体数值）
	 */
	private int sellerCredit;
	/**
	 * 卖家信用等级（等级）
	 */
	private String sellerCreditLevel;
	/**
	 * 门店到用户的距离(米)（仅查询附近店铺有效）
	 */
	private Long distance;

	public long getUin() {
		return uin;
	}

	public void setUin(long uin) {
		this.uin = uin;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public long getAddress2Id() {
		return address2Id;
	}

	public void setAddress2Id(long address2Id) {
		this.address2Id = address2Id;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public long getAddress3Id() {
		return address3Id;
	}

	public void setAddress3Id(long address3Id) {
		this.address3Id = address3Id;
	}

	public String getAddress4() {
		return address4;
	}

	public void setAddress4(String address4) {
		this.address4 = address4;
	}

	public long getAddress4Id() {
		return address4Id;
	}

	public void setAddress4Id(long address4Id) {
		this.address4Id = address4Id;
	}

	public String getAddress5() {
		return address5;
	}

	public void setAddress5(String address5) {
		this.address5 = address5;
	}

	public String getDetailAddress() {
		return detailAddress;
	}

	public void setDetailAddress(String detailAddress) {
		this.detailAddress = detailAddress;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getProductcategory() {
		return productcategory;
	}

	public void setProductcategory(String productcategory) {
		this.productcategory = productcategory;
	}

	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	public long getRenPrice() {
		return renPrice;
	}

	public void setRenPrice(long renPrice) {
		this.renPrice = renPrice;
	}

	public String getOpenTime() {
		return openTime;
	}

	public void setOpenTime(String openTime) {
		this.openTime = openTime;
	}

	public String getRecommend() {
		return recommend;
	}

	public void setRecommend(String recommend) {
		this.recommend = recommend;
	}

	public String getSpecService() {
		return specService;
	}

	public void setSpecService(String specService) {
		this.specService = specService;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getStoreLogo() {
		return storeLogo;
	}

	public void setStoreLogo(String storeLogo) {
		this.storeLogo = storeLogo;
	}

	public int getSellerCredit() {
		return sellerCredit;
	}

	public void setSellerCredit(int sellerCredit) {
		this.sellerCredit = sellerCredit;
	}

	public String getSellerCreditLevel() {
		return sellerCreditLevel;
	}

	public void setSellerCreditLevel(String sellerCreditLevel) {
		this.sellerCreditLevel = sellerCreditLevel;
	}

	public long getAddress5Id() {
		return address5Id;
	}

	public void setAddress5Id(long address5Id) {
		this.address5Id = address5Id;
	}

	public double getFlatitude() {
		return flatitude;
	}

	public void setFlatitude(double flatitude) {
		this.flatitude = flatitude;
	}

	public double getFlongitude() {
		return flongitude;
	}

	public void setFlongitude(double flongitude) {
		this.flongitude = flongitude;
	}

	public Long getDistance() {
		return distance;
	}

	public void setDistance(Long distance) {
		this.distance = distance;
	}

}
