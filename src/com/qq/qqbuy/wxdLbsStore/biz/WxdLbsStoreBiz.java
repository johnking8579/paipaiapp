package com.qq.qqbuy.wxdLbsStore.biz;

import java.util.Collections;
import java.util.Comparator;

import org.apache.commons.lang3.StringUtils;

import net.sf.json.JSONObject;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.PpConstants;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.item.util.SellerCreditUtil;
import com.qq.qqbuy.thirdparty.idl.shop.ShopClient;
import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiShopInfo;
import com.qq.qqbuy.thirdparty.idl.userinfo.ApiUserClient;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.apiuser.APIUserProfile;
import com.qq.qqbuy.thirdparty.idl.wxdLbsStore.WxdLbsStoreClient;
import com.qq.qqbuy.thirdparty.idl.wxdLbsStore.protocol.QueryStoreInfoByAdressReq;
import com.qq.qqbuy.thirdparty.idl.wxdLbsStore.protocol.QueryStoreInfoByAdressResp;
import com.qq.qqbuy.thirdparty.idl.wxdLbsStore.protocol.QueryStoreInfoByStoreIdResp;
import com.qq.qqbuy.wxdLbsStore.po.WxdCountPo;
import com.qq.qqbuy.wxdLbsStore.po.WxdItemPo;
import com.qq.qqbuy.wxdLbsStore.po.WxdListPo;
import com.qq.qqbuy.wxdLbsStore.util.ToolsUtil;

/**
 * 
 * @author zhaohuayu
 * 
 */
public class WxdLbsStoreBiz {

	protected final int readTimeout = 15000;// 读超时时间
	protected final int connectTimeout = 10000;// 连接超时时间
	protected final String encoding = "UTF-8";// 回复的字符编码
	protected final boolean isNeedProxy = true;// 是否走代理
	private ShopClient shopClient = new ShopClient();

	/**
	 * 根据QQ号获得门店的列表信息
	 * @param uin
	 * @param mk
	 * @return
	 * @throws BusinessException
	 * @throws Exception
	 */
	public WxdListPo queryStoreListByUin(
			long uin, String mk, long startPage,
			long pageNum,String longitudeStr,
			String latitudeStr) throws BusinessException, Exception{		
		WxdListPo listPo = new WxdListPo();
		Short qqType = 0 ;
		QueryStoreInfoByStoreIdResp resp = WxdLbsStoreClient.QueryStoreInfoByStoreId(String.valueOf(uin), qqType, mk, startPage, pageNum) ;
		if (resp == null || resp.getResult() != 0) {
			throw BusinessException
					.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL,
							"call QueryStoreCountByUin  error, resp == null or resp.getResult != 0 ");
		}		
		if (resp.getTotal() > 0) {
			double longitudeD = Double.parseDouble(longitudeStr);
			double latitudeD = Double.parseDouble(latitudeStr);
			APIUserProfile profile = ApiUserClient
					.getUserSimpleInfo(uin);
			int sellerCredit = profile.getSellerCredit();		
			ApiShopInfo apiShopInfo = shopClient.getShopInfo(uin);			
			listPo.setTotalNum(resp.getTotal());
			
			for (int i = 0; i < resp.getStoreInfo().size(); i++) {
				if (resp.getStoreInfo().get(i).getState() == 0) {
					try {
						WxdItemPo itemPo = ToolsUtil.converToObject(resp
								.getStoreInfo().get(i));						
						itemPo.setSellerCredit(sellerCredit);
						itemPo.setSellerCreditLevel(SellerCreditUtil
								.getSellerCreditLevel(sellerCredit));
						itemPo.setDistance((long)ToolsUtil.GetDistance(longitudeD,
								latitudeD, itemPo.getFlongitude(),
								itemPo.getFlatitude()));						
						itemPo.setStoreLogo("http://img"
								+ (apiShopInfo.getShopID() % 8) + ".paipaiimg.com/"
								+ apiShopInfo.getMainLogoName());
						listPo.getWxdList().add(itemPo);
					} catch (Exception ex) {
						ex.printStackTrace() ;						
					}					
				}
			}
			Collections.sort(listPo.getWxdList(), new Comparator<WxdItemPo>() {
	            public int compare(WxdItemPo arg0, WxdItemPo arg1) {
	                return arg0.getDistance().compareTo(arg1.getDistance());
	            }
	        });			
			
		}		
		
		return listPo ;
	}
	
	/**
	 * 根据QQ号获得门店的统计信息
	 * @param uin
	 * @param mk
	 * @return
	 * @throws BusinessException
	 * @throws Exception
	 */
	public WxdCountPo queryStoreCountByUin(
			long uin, String mk) throws BusinessException, Exception{
		Short qqType = 0 ;
		QueryStoreInfoByStoreIdResp resp = WxdLbsStoreClient.QueryStoreInfoByStoreId(String.valueOf(uin), qqType, mk, 0, 1) ;
		if (resp == null || resp.getResult() != 0) {
			throw BusinessException
					.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL,
							"call QueryStoreCountByUin  error, resp == null or resp.getResult != 0 ");
		}
		WxdCountPo po = new WxdCountPo() ;		
		po.setStoreCount(resp.getTotal()) ;
		ApiShopInfo apiShopInfo = shopClient.getShopInfo(uin);
		po.setStoreLogo("http://img"
				+ (apiShopInfo.getShopID() % 8) + ".paipaiimg.com/"
				+ apiShopInfo.getMainLogoName());
		po.setStoreName(apiShopInfo.getShopName()) ;
		po.setUin(uin) ;
		return po ;
	}
	/**
	 * 根据地址查门店。如果传递了当前位置经纬度，则会计算出到门店的距离。单位为“米”
	 * 
	 * @param longitudeStr
	 * @param latitudeStr
	 * @param mk
	 * @param address3Id
	 * @param startPage
	 * @param pageNum
	 * @return
	 * @throws BusinessException
	 * @throws Exception
	 */
	public WxdListPo queryStoreInfoByAdress(String longitudeStr,
			String latitudeStr, String mk, long address4Id, long startPage,
			long pageNum) throws BusinessException, Exception {
		WxdListPo listPo = new WxdListPo();
		
		QueryStoreInfoByAdressReq req = new QueryStoreInfoByAdressReq();
		req.setMachineKey(mk);
		req.setSource(PpConstants.PP_SOURCE);
		req.setSceneId(0l);
		req.getFilter().setAddressLevel(3);
		req.getFilter().setAddress3Id(address4Id);// 分表需要，市ID必传。这里给区ID，服务端进行自动转换
		req.getFilter().setAddress4Id(address4Id);
		req.getFilter().setStartPage(startPage);
		req.getFilter().setPageNum(pageNum);
		QueryStoreInfoByAdressResp resp = WxdLbsStoreClient
				.QueryStoreInfoByAdress(req);
		if (resp == null || resp.getResult() != 0) {
			throw BusinessException
					.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL,
							"call QueryStoreInfoByAdress  error, resp == null or resp.getResult != 0 ");
		}
		if (resp.getTotal() > 0) {
			listPo.setTotalNum(resp.getTotal());
			for (int i = 0; i < resp.getStoreInfo().size(); i++) {
				if (resp.getStoreInfo().get(i).getState() == 0) {
					try {
						WxdItemPo itemPo = ToolsUtil.converToObject(resp
								.getStoreInfo().get(i));
						APIUserProfile profile = ApiUserClient
								.getUserSimpleInfo(itemPo.getUin());
						int sellerCredit = profile.getSellerCredit();
						itemPo.setSellerCredit(sellerCredit);
						itemPo.setSellerCreditLevel(SellerCreditUtil
								.getSellerCreditLevel(sellerCredit));
						if (!StringUtils.isEmpty(longitudeStr)
								&& !StringUtils.isEmpty(latitudeStr)) {
							double longitudeD = Double.parseDouble(longitudeStr);
							double latitudeD = Double.parseDouble(latitudeStr);
							itemPo.setDistance((long)ToolsUtil.GetDistance(longitudeD,
									latitudeD, itemPo.getFlongitude(),
									itemPo.getFlatitude()));
						}
						ApiShopInfo apiShopInfo = shopClient.getShopInfo(itemPo
								.getUin());
						itemPo.setStoreLogo("http://img"
								+ (apiShopInfo.getShopID() % 8) + ".paipaiimg.com/"
								+ apiShopInfo.getMainLogoName());
						listPo.getWxdList().add(itemPo);
					} catch (Exception ex) {
						ex.printStackTrace() ;						
					}					
				}
			}
		}
		return listPo;
	}

	/**
	 * 查询附近的门店
	 * 
	 * @param longitudeStr
	 * @param latitudeStr
	 * @param mk
	 * @param startPage
	 * @param pageNum
	 * @param mt
	 * @throws BusinessException
	 * @throws Exception
	 */
	public WxdListPo queryStoreInfoByNear(String longitudeStr,
			String latitudeStr, String mk, long startPage, long pageNum,
			String mt, long category) throws BusinessException, Exception {
		WxdListPo listPo = new WxdListPo();
		StringBuffer getUrl = new StringBuffer();
		String coordType = "3";
		if ("ios".equalsIgnoreCase(mt)) {
			coordType = "1";
		} else {
			coordType = "3";
		}
		getUrl.append(ToolsUtil.mapQQUrl).append("location=")
				.append(latitudeStr).append(",").append(longitudeStr)
				.append("&key=").append(ToolsUtil.mapQQKey)
				.append("&coord_type=").append(coordType);
		String geocoderResult = HttpUtil.get(getUrl.toString(), connectTimeout,
				readTimeout, encoding, isNeedProxy);
		JSONObject geocoderObject = JSONObject.fromObject(geocoderResult);
		if (geocoderObject.getInt("status") != 0) {
			Log.run.warn("queryStoreInfoByNear:geocoderResult=" + geocoderResult) ;
			throw BusinessException.createInstance(
					BusinessErrorType.HTTP_REQUEST_FAILED,
					"无法获取到坐标信息");
		}
		JSONObject adInfo = geocoderObject.getJSONObject("result")
				.getJSONObject("ad_info");
		if (!adInfo.containsKey("adcode")) {
			Log.run.warn("无法获取到市区ID信息");
			return listPo;
		}
		String adcodeStr = adInfo.getString("adcode");
		long adcode = Long.parseLong(adcodeStr);
		double longitudeD = Double.parseDouble(longitudeStr);
		double latitudeD = Double.parseDouble(latitudeStr);
		
		QueryStoreInfoByAdressReq req = new QueryStoreInfoByAdressReq();
		req.setMachineKey(mk);
		req.setSource(PpConstants.PP_SOURCE);
		req.setSceneId(0l);
		req.getFilter().setAddressLevel(2);
		if (category > 0) {
			req.getFilter().setCategory(category);
		}
		req.getFilter().setAddress3Id(adcode);// 分表需要，市ID必传。这里给区ID，服务端进行自动转换
		//req.getFilter().setAddress4Id(adcode);
		req.getFilter().setStartPage(startPage);
		req.getFilter().setPageNum(pageNum);
		QueryStoreInfoByAdressResp resp = WxdLbsStoreClient
				.QueryStoreInfoByAdress(req);		
		if (resp == null || resp.getResult() != 0) {
			throw BusinessException
					.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL,
							"无法获取附近门店信息 ");
		}
		if (resp.getTotal() > 0) {
			listPo.setTotalNum(resp.getTotal());
			for (int i = 0; i < resp.getStoreInfo().size(); i++) {
				if (resp.getStoreInfo().get(i).getState() == 0) {
					try {
						WxdItemPo itemPo = ToolsUtil.converToObject(resp
								.getStoreInfo().get(i));
						APIUserProfile profile = ApiUserClient
								.getUserSimpleInfo(itemPo.getUin());
						int sellerCredit = profile.getSellerCredit();
						itemPo.setSellerCredit(sellerCredit);
						itemPo.setSellerCreditLevel(SellerCreditUtil
								.getSellerCreditLevel(sellerCredit));
						itemPo.setDistance((long)ToolsUtil.GetDistance(longitudeD,
								latitudeD, itemPo.getFlongitude(),
								itemPo.getFlatitude()));
						ApiShopInfo apiShopInfo = shopClient.getShopInfo(itemPo
								.getUin());
						itemPo.setStoreLogo("http://img"
								+ (apiShopInfo.getShopID() % 8) + ".paipaiimg.com/"
								+ apiShopInfo.getMainLogoName());
						listPo.getWxdList().add(itemPo);
					} catch (Exception ex) {
						ex.printStackTrace() ;						
					}					
				}
			}
			Collections.sort(listPo.getWxdList(), new Comparator<WxdItemPo>() {
	            public int compare(WxdItemPo arg0, WxdItemPo arg1) {
	                return arg0.getDistance().compareTo(arg1.getDistance());
	            }
	        });			
			
		}
		return listPo;
	}
}
