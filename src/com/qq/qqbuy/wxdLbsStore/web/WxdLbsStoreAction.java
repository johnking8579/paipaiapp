package com.qq.qqbuy.wxdLbsStore.web;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.GsonBuilder;
import com.qq.qqbuy.common.JsonOutput;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.action.AuthRequiredAction;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.wxdLbsStore.biz.WxdLbsStoreBiz;
import com.qq.qqbuy.wxdLbsStore.po.WxdCountPo;
import com.qq.qqbuy.wxdLbsStore.po.WxdListPo;

public class WxdLbsStoreAction extends AuthRequiredAction {
	static Logger log = LogManager.getLogger(WxdLbsStoreAction.class);
	WxdLbsStoreBiz wxdLbsStoreBiz;
	JsonOutput out = new JsonOutput();
	long startPage;
	long pageNum;
	long addressId;
	long category = -1 ;
	String lat ;
	String lon ;
	String uinList ;
	String uin ;
	int distance;// 1：计算门店的距离 其他：不计算

	
	/**
	 * 通过QQ查询门店的列表详情
	 * @return
	 * @throws Exception
	 */
	public String findStoreListByUin() throws Exception {
		try {
			if (StringUtils.isEmpty(uin) || StringUtils.isEmpty(this.getMk())) {
				throw BusinessException.createInstance(
						BusinessErrorType.PARAM_ERROR, "参数校验失败！");
			}
			if (StringUtils.isEmpty(this.getLat())
					|| StringUtils.isEmpty(this.getLon())
					|| "0".equals(this.getLat()) // ios未正确获取经纬度
					|| "0".equals(this.getLon()) // ios未正确获取经纬度
					|| "4.9E-324".equals(this.getLat()) // android未正确获取经纬度
					|| "4.9E-324".equals(this.getLon())) {// android未正确获取经纬度
				throw BusinessException.createInstance(
						BusinessErrorType.PARAM_ERROR, "无法获取到坐标信息");
			}
			if (startPage < 0) {
				startPage = 0;
			}
			if (pageNum <= 0) {
				pageNum = 10;
			}
			Long uinL = Long.parseLong(uin) ;			
			WxdListPo listPo = wxdLbsStoreBiz.queryStoreListByUin(uinL, this.getMk(), startPage, pageNum, this.getLon(), this.getLat());
			out.setData(new GsonBuilder().serializeNulls().create()
					.toJsonTree(listPo));
		} catch (BusinessException be) {
			out.setErrCode(be.getErrCode());
			out.setMsg(be.getErrMsg());
		} catch (Exception ex) {
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
			out.setMsg("系统异常");
			ex.printStackTrace();
		}
		return doPrint(out.toJsonStr());
	}
	/**
	 * 通过QQ查询门店的统计信息
	 * @return
	 * @throws Exception
	 */
	public String findStoreCountByUin() throws Exception {
		try {
			if (StringUtils.isEmpty(uinList) || StringUtils.isEmpty(this.getMk())) {
				throw BusinessException.createInstance(
						BusinessErrorType.PARAM_ERROR, "参数校验失败！");
			}
			String[] uinStrArr = uinList.split("-") ;
			Set<Long> uinSet = new HashSet<Long>() ;
			for (String uinStr : uinStrArr) {
				try {
					uinSet.add(Long.parseLong(uinStr)) ;
				} catch (Exception ex) {
					Log.run.warn("findStoreCountByUin error[parseLong],uinStr=" + uinStr, ex);
				}
			}
			if (uinSet.size() <= 0) {
				throw BusinessException.createInstance(
						BusinessErrorType.PARAM_ERROR, "无法正确获取到UIN信息");
			}
			List<WxdCountPo> resultList = new ArrayList<WxdCountPo> () ;
			for(Long uin: uinSet) {
				if (uin <= 0) {
					continue ;
				}
				try {
					WxdCountPo countPo = wxdLbsStoreBiz.queryStoreCountByUin(uin, this.getMk());
					resultList.add(countPo) ;
				} catch (Exception ex) {
					Log.run.warn("findStoreCountByUin error[queryStoreCountByUin],uin=" + uin, ex);
					ex.printStackTrace() ;
				}
			}
			out.setData(new GsonBuilder().serializeNulls().create()
					.toJsonTree(resultList));
		} catch (BusinessException be) {
			out.setErrCode(be.getErrCode());
			out.setMsg(be.getErrMsg());
		} catch (Exception ex) {
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
			out.setMsg("异常" + ex.toString());
			ex.printStackTrace();
		}
		return doPrint(out.toJsonStr());
	}
	/**
	 * 查询附近的门店信息，兼容通过类别查找
	 * @return
	 * @throws Exception
	 */
	public String findStoreByNear() throws Exception {
		try {
			if (StringUtils.isEmpty(this.getLat())
					|| StringUtils.isEmpty(this.getLon())
					|| "0".equals(this.getLat()) // ios未正确获取经纬度
					|| "0".equals(this.getLon()) // ios未正确获取经纬度
					|| "4.9E-324".equals(this.getLat()) // android未正确获取经纬度
					|| "4.9E-324".equals(this.getLon())) {// android未正确获取经纬度
				throw BusinessException.createInstance(
						BusinessErrorType.PARAM_ERROR, "无法获取到坐标信息");
			}
			if (startPage < 0) {
				startPage = 0;
			}
			if (pageNum <= 0) {
				pageNum = 10;
			}
			//为了排序，应徐总要求，写死为取前40个，按距离排序
			startPage = 0 ;
			pageNum = 40 ; 
			WxdListPo listPo = wxdLbsStoreBiz.queryStoreInfoByNear(
					this.getLon(), this.getLat(), this.getMk(),
					startPage, pageNum, this.getMt(), this.category);
			out.setData(new GsonBuilder().serializeNulls().create()
					.toJsonTree(listPo));

		} catch (BusinessException be) {
			out.setErrCode(be.getErrCode());
			out.setMsg(be.getErrMsg());
		} catch (Exception ex) {
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
			out.setMsg("异常" + ex.toString());
			ex.printStackTrace();
		}
		return doPrint(out.toJsonStr());
	}

	/**
	 * 根据市区ID查找
	 * 
	 * @return
	 * @throws Exception
	 */
	public String findStoreByAdress() throws Exception {
		try {
			if (addressId == 0L || StringUtils.isEmpty(this.getMk())) {
				throw BusinessException.createInstance(
						BusinessErrorType.PARAM_ERROR, "参数校验失败！");
			}
			if (startPage < 0) {
				startPage = 0;
			}
			if (pageNum <= 0) {
				pageNum = 10;
			}
			String longitudeStr = "";
			String latitudeStr = "";
			if (distance == 1) {
				if (StringUtils.isEmpty(this.getLat())
						|| StringUtils.isEmpty(this.getLon())
						|| "0".equals(this.getLat()) // ios未正确获取经纬度
						|| "0".equals(this.getLon()) // ios未正确获取经纬度
						|| "4.9E-324".equals(this.getLat()) // android未正确获取经纬度
						|| "4.9E-324".equals(this.getLon())) {// android未正确获取经纬度
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "无法正确获取到坐标信息");
				}
				longitudeStr = this.getLon();
				latitudeStr = this.getLat();
			}
			WxdListPo listPo = wxdLbsStoreBiz.queryStoreInfoByAdress(
					longitudeStr, latitudeStr, this.getMt(), addressId,
					startPage, pageNum);
			out.setData(new GsonBuilder().serializeNulls().create()
					.toJsonTree(listPo));

		} catch (BusinessException be) {
			out.setErrCode(be.getErrCode());
			out.setMsg(be.getErrMsg());
		} catch (Exception ex) {
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
			out.setMsg("异常" + ex.toString());
			ex.printStackTrace();
		}
		return doPrint(out.toJsonStr());
	}

	public WxdLbsStoreBiz getWxdLbsStoreBiz() {
		return wxdLbsStoreBiz;
	}

	public void setWxdLbsStoreBiz(WxdLbsStoreBiz wxdLbsStoreBiz) {
		this.wxdLbsStoreBiz = wxdLbsStoreBiz;
	}

	public JsonOutput getOut() {
		return out;
	}

	public void setOut(JsonOutput out) {
		this.out = out;
	}

	public long getStartPage() {
		return startPage;
	}

	public void setStartPage(long startPage) {
		this.startPage = startPage;
	}

	public long getPageNum() {
		return pageNum;
	}

	public void setPageNum(long pageNum) {
		this.pageNum = pageNum;
	}

	public long getAddressId() {
		return addressId;
	}

	public void setAddressId(long addressId) {
		this.addressId = addressId;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public long getCategory() {
		return category;
	}

	public void setCategory(long category) {
		this.category = category;
	}
	public String getUinList() {
		return uinList;
	}
	public void setUinList(String uinList) {
		this.uinList = uinList;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	public String getUin() {
		return uin;
	}
	public void setUin(String uin) {
		this.uin = uin;
	}

	
}
