package com.qq.qqbuy.shop.biz;

import java.util.List;
import java.util.Map;

import com.qq.qqbuy.common.Pager;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.shop.po.ShopCategoryItem;
import com.qq.qqbuy.shop.po.ShopInfo;
import com.qq.qqbuy.shop.po.ShopItems;
import com.qq.qqbuy.shop.po.ShopSimpleItem;
import com.qq.qqbuy.thirdparty.http.search.po.ShopItem;

/**
 * 
 * @ClassName: ShopBiz
 * 
 * @Description: 店铺相关的biz
 * 
 * @author wendyhu
 * @date 2012-12-13 下午02:55:03
 */
public interface ShopBiz
{
    /**
     * @Title: getShopInfo
     * @Description: 获取店铺相关信息
     * 
     * @param sellerUin
     *            店铺号
     * @param enablePromotion
     *            是否展示店铺促销信息
     * @param enableConcern
     *            是否展示店铺关注信息
     * @return 设定文件
     * @return ShopInfo 返回类型
     * @throws
     */
    public ShopInfo getShopInfo(long sellerUin) ;

    /**
     * 
     * @Title: getShopName
     * @Description: 获取店铺名称，
     * 
     * @param sellerUin
     *            店铺号
     * @param machineKey 机器信息           
     * @return 设定文件
     * @return ShopInfo 返回类型
     * @throws
     */
    public String getShopName(long sellerUin,String machineKey);
    
    /**
     * 
     * @Title: getRecommedComdyList
     * @Description: 获取店铺的推荐商品列表
     * @param shopID
     *            店铺号
     * @param machineKey
     *            机器信息
     * @return 设定文件
     * @return List<ShopSimpleItem> 返回类型
     * @throws
     */
    public List<ShopSimpleItem> getRecommedComdyList(long shopID,
            String machineKey);
    
    
    /**
     * 
     * @Title: getShopCategoryListByUin 
     * @Description:  获取店铺的分类信息
     * @param shopID 店铺号
     * @param machineKey 机器信息
     * @return    设定文件 
     * @return ShopCategoryItem    返回类型 
     * @throws
     */
    public List<ShopCategoryItem> findShopCategory(long shopID, String machineKey);
    
    /**
     * 
     * @Title: getShopComdyList 
     * 
     * @Description: 获取商品列表信息
     * 
     * @param shopID 卖家号
     * @param machineKey 机器信息
     * @param startIndex 起始页 起始值0
     * @param pageSize 每页的大小
     * @param orderType 排序方式  使用说明见{@link com.paipai.c2c.item.constants.CVItemFilterConstants}
     * @param filterMap 过滤map，使用说明见{@link com.paipai.c2c.item.constants.CVItemFilterConstants}
     * @return    设定文件 
     * @return List<ShopSimpleItem>    返回类型 
     * @throws
     */
    public ShopItems getShopComdyList(
            long shopID, String machineKey, long startIndex, long pageSize, long orderType, Map<String,String> filterMap);
    
    
    /**
     * 判断微店是否已装修
     * @param uin
     * @return
     */
    public boolean isDecorated(long uin);

    
	/**
	 * 从CGI接口中搜索店内商品, 价格为折扣价. !!! 该接口速度慢, 不要批量调用
	 * @param shopId
	 * @param categoryId 店内类目ID, 无值传null
	 * @param keyword 搜索关键字， 无值传null
	 * @param order 见ShopItemSearchParam.ORDERSTYLE_*， 注意该值与手机端传的otype不同，要使用OrderStyleAdapter转换. 无值传null
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	Pager<ShopItem> findShopItemCgi(long shopId, String categoryId, String keyword, Integer order, int pageNo, int pageSize);

	/**
	 * 拼接店铺图片. 缓存
	 * @param shopId
	 * @return
	 */
	List<String> joinPicAndCache(long shopId);

	/**
	 * 获取店铺"新品"数量, 缓存
	 * @param shopId
	 * @param mk
	 * @return
	 * @deprecated 需要全量查询并推算出新品数量,速度慢,不推荐查询这种数据
	 */
	int getNewItemCountAndCache(long shopId, String mk);
}
