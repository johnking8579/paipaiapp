package com.qq.qqbuy.shop.biz.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.Pager;
import com.qq.qqbuy.common.client.cache.CacheCallback;
import com.qq.qqbuy.common.client.cache.CacheClient;
import com.qq.qqbuy.common.client.cache.CacheKey;
import com.qq.qqbuy.common.exception.ExternalInterfaceException;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.common.util.ImageUrlUtil;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.shop.biz.ShopBiz;
import com.qq.qqbuy.shop.dao.ShopDao;
import com.qq.qqbuy.shop.po.ShopCategoryItem;
import com.qq.qqbuy.shop.po.ShopInfo;
import com.qq.qqbuy.shop.po.ShopItems;
import com.qq.qqbuy.shop.po.ShopSimpleItem;
import com.qq.qqbuy.thirdparty.http.search.SearchClient;
import com.qq.qqbuy.thirdparty.http.search.ShopItemSearchParam;
import com.qq.qqbuy.thirdparty.http.search.po.ShopItem;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ApiItem;
import com.qq.qqbuy.thirdparty.idl.item.protocol.eval.EvalStat;
import com.qq.qqbuy.thirdparty.idl.shop.CVItemFilterConstants;
import com.qq.qqbuy.thirdparty.idl.shop.ShopClient;
import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiGetRecommedComdyListResp;
import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiGetShopComdyListResp;
import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiShopCategory;
import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiShopInfo;
import com.qq.qqbuy.user.po.User;
import com.qq.qqbuy.user.util.UserDetailUtil;
import com.qq.qqbuy.user.util.UserHelper;

/**
 * 
 * @ClassName: ShopBizImpl
 * 
 * @Description: 店铺相关接口
 * 
 * @author wendyhu
 * 
 * @date 2013-3-8 下午08:12:38
 */
public class ShopBizImpl implements ShopBiz {
	
	protected static final String WEIDIAN_DECORATED_URL = "http://wd.paipai.com/DecorationFlag/GetDecorationFlag";
	private static Logger log = LogManager.getLogger();
	private ShopDao shopDao;
	private SearchClient searchClient = new SearchClient();
	private ShopClient shopClient = new ShopClient();
	private CacheClient cacheClient;
	/**
	 * 
	 * @Title: transferApiItem2ShopSimpleItem
	 * @Description: 转换apiitem 到我们输入的店铺商品元素
	 * @param apiItem
	 * @return 设定文件
	 * @return ShopSimpleItem 返回类型
	 * @throws
	 */
	private ShopSimpleItem transferApiItem2ShopSimpleItem(ApiItem apiItem) {
		ShopSimpleItem item = new ShopSimpleItem();
		item.setDealType(apiItem.getDealType());
		item.setExpressMailPrice(apiItem.getExpressMailPrice());
		Vector<String> image = apiItem.getLogo();
		if (image != null && image.size() > 0) {
			// 300X300图
			item.setImage(ImageUrlUtil.genImageUrl(
					ImageUrlUtil.genImageUrlFromLogo(apiItem.getQQUin(), image.firstElement()),
					ImageUrlUtil.DEFAULTFormat));
			// 200X200图
			item.setImage200(ImageUrlUtil.genImageUrl(
					ImageUrlUtil.genImageUrlFromLogo(apiItem.getQQUin(), image.firstElement()),
					ImageUrlUtil.DEFAULTFormat200));
			// 120X120图
			item.setImage120(ImageUrlUtil.genImageUrl(
					ImageUrlUtil.genImageUrlFromLogo(apiItem.getQQUin(), image.firstElement()),
					ImageUrlUtil.DEFAULTFormat120));
			// 手拍80X80图
			item.setImage80(ImageUrlUtil.genImageUrl(
					ImageUrlUtil.genImageUrlFromLogo(apiItem.getQQUin(), image.firstElement()),
					ImageUrlUtil.DEFAULTFormat80));
		} else {
			// 300X300图
			item.setImage(ImageUrlUtil.genImageUrl(
					ImageUrlUtil.genImageUrlFromItem(apiItem.getQQUin(), apiItem.getItemId()),
					ImageUrlUtil.DEFAULTFormat));
			// 200X200图
			item.setImage200(ImageUrlUtil.genImageUrl(
					ImageUrlUtil.genImageUrlFromItem(apiItem.getQQUin(), apiItem.getItemId()),
					ImageUrlUtil.DEFAULTFormat200));
			// 120X120图
			item.setImage120(ImageUrlUtil.genImageUrl(
					ImageUrlUtil.genImageUrlFromItem(apiItem.getQQUin(), apiItem.getItemId()),
					ImageUrlUtil.DEFAULTFormat120));
			// 手拍80X80图
			item.setImage80(ImageUrlUtil.genImageUrl(
					ImageUrlUtil.genImageUrlFromItem(apiItem.getQQUin(), apiItem.getItemId()),
					ImageUrlUtil.DEFAULTFormat80));
		}
		item.setIsRecommend(apiItem.getIsRecommend());
		item.setItemId(apiItem.getItemId());
		item.setNewType(apiItem.getNewType());
		item.setNormalMailPrice(apiItem.getNormalMailPrice());
		item.setNum(apiItem.getNum());
		item.setPrice(apiItem.getPrice());//这里返回的是商品原价，--联系桂文斌
		item.setSendType(apiItem.getSendType());
		item.setState(apiItem.getState());
		item.setTitle(apiItem.getTitle());
		item.setTransportPriceType(apiItem.getTransportPriceType());
		if (apiItem.getStat() != null)
			item.setSoldNum(apiItem.getStat().getTotalPayNum());
		if (apiItem.getProperty() != null) {
			item.setProperties(apiItem.getProperty());
		}
		return item;
	}

	@Override
	public List<ShopSimpleItem> getRecommedComdyList(long shopID, String machineKey) {
		List<ShopSimpleItem> ret = new ArrayList<ShopSimpleItem>();
		ApiGetRecommedComdyListResp resp = ShopClient.getRecommedComdyList(shopID, machineKey, 0);
		Vector<ApiItem> cmdys = null;
		if (resp != null && (cmdys = resp.getRecommedComdyInfoList()) != null && !cmdys.isEmpty()) {
			for (ApiItem apiItem : cmdys) {
				ret.add(transferApiItem2ShopSimpleItem(apiItem));
			}
		}
		return ret;
	}

	@Override
	public List<ShopCategoryItem> findShopCategory(long shopID, String machineKey) {
		List<ShopCategoryItem> ret = new ArrayList<ShopCategoryItem>();
		Vector<ApiShopCategory> categorys = ShopClient.findShopCategory(shopID, machineKey);
		Map<Long, ShopCategoryItem> tmpMap = new HashMap<Long, ShopCategoryItem>();
		if (categorys != null) {
			for (ApiShopCategory item : categorys) {
				long parentId = StringUtil.toInt(item.getParentId(), 0);
				long categoryId = StringUtil.toInt(item.getCategoryId(), 0);
				ShopCategoryItem shopCategory = new ShopCategoryItem();
				shopCategory.setCategoryId(categoryId);
				shopCategory.setParentId(parentId);
				shopCategory.setDesc(item.getDesc());
				shopCategory.setPicUrl(item.getPicUrl());
				/**
				 * 当没有父结点时，就生成一个父亲结点 有的话就加入到父结点的子结点里面
				 */
				if (parentId == 0) {
					tmpMap.put(categoryId, shopCategory);
					ret.add(shopCategory);
				} else {
					ShopCategoryItem tmp = tmpMap.get(parentId);
					List<ShopCategoryItem> tmpList = null;
					if (tmp != null && (tmpList = tmp.getChilds()) != null) {
						tmpList.add(shopCategory);
					}
				}

			}
		}
		return ret;
	}

	/**
	 * @deprecated 返回的价格为原价,需要二次计算折扣价.返回的销量为总销量.高并发下接口响应速度慢
	 */
	@Override
	public ShopItems getShopComdyList(long shopID, String machineKey, long startIndex, long pageSize, long orderType,
			Map<String, String> filterMap) {
		ShopItems ret = new ShopItems();
		List<ShopSimpleItem> rets = new ArrayList<ShopSimpleItem>();
		ApiGetShopComdyListResp resp = ShopClient.getShopComdyList(shopID, machineKey, 0, startIndex, pageSize,
				orderType, filterMap);
		
		Vector<ApiItem> items = null;
		if (resp != null && resp.result == 0 && (items = resp.getItemList()) != null && !items.isEmpty()) {
			ret.setFindNum(resp.getFoundItemNum());
			ret.setTotalNum(resp.getTotalItemNum());
			for (ApiItem apiItem : items) {
				rets.add(transferApiItem2ShopSimpleItem(apiItem));
			}
			ret.setItems(rets);
		}
		return ret;
	}
	
	@Override
	public Pager<ShopItem> findShopItemCgi(long shopId, String categoryId, String keyword, Integer order, int pageNo, int pageSize)	{
		ShopItemSearchParam par = new ShopItemSearchParam();
		par.setShopId(shopId+"");
		par.setPageNum(pageNo);
		par.setPageSize(pageSize);
		if(categoryId != null)	par.setCategoryId(categoryId);
		if(keyword != null)		par.setKeyword(keyword);
		if(order != null)		par.setOrderStyle(order);
		return searchClient.searchShopItem(par);
	}

	@Override
	public String getShopName(long sellerUin, String machineKey) {
		return shopClient.getShopInfo(sellerUin).getShopName();
	}

	@Override
	public ShopInfo getShopInfo(long sellerUin) {
		ApiShopInfo apiShopInfo = shopClient.getShopInfo(sellerUin);
		ShopInfo ret = new ShopInfo();
		ret.setMainBusiness(apiShopInfo.getMainArea());
		ret.setSellerUin(apiShopInfo.getShopID() + "");
		ret.setShopName(apiShopInfo.getShopName());
		String regDateStr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(
								new Date(apiShopInfo.getRegTime() * 1000));
		if (!StringUtil.isBlank(regDateStr)) {
			ret.setRegTime(regDateStr);
		}
		ret.setLogo("http://img" + (apiShopInfo.getShopID() % 8) + ".paipaiimg.com/" + apiShopInfo.getMainLogoName());
		ret.setItemCountOnSale((int) apiShopInfo.getCommCount());

		// 查评价信息
		
		try {
			EvalStat stat = shopClient.getEval(sellerUin);
			ret.setBadEval((int) stat.getSellerBadAll());
			ret.setGoodEval((int) stat.getSellerGoodAll());
			ret.setNormalEval((int) stat.getSellerNormalAll());

			long totalEval = stat.getDsrTotal() != 0 ? stat.getDsrTotal() : 1;
			long goodDescriptionMatch = stat.getDsr1Total();
			long attitudeOfService = stat.getDsr2Total();
			long speedOfDelivery = stat.getDsr3Total();

			// 计算评分
			// int totalEval = stat.dsrTotal != 0 ? stat.dsrTotal : 1;
			ret.setTotalEval((int) (Double.parseDouble(String.format("%.1f",
					(goodDescriptionMatch + attitudeOfService + speedOfDelivery) * 1.0 / (3 * totalEval))) * 10));
			ret.setGoodDescriptionMatch((int) (Double.parseDouble(String.format("%.1f", goodDescriptionMatch * 1.0
					/ totalEval)) * 10));
			ret.setAttitudeOfService((int) (Double.parseDouble(String.format("%.1f", attitudeOfService * 1.0
					/ totalEval)) * 10));
			ret.setSpeedOfDelivery((int) (Double.parseDouble(String.format("%.1f", speedOfDelivery * 1.0 / totalEval)) * 10));
			int evalNum = ret.getGoodEval() + ret.getNormalEval() + ret.getBadEval();
			if (totalEval > 0 && ret.getGoodEval() > 0) {
				ret.setGoodEvalRate(String.format("%.2f", 100.0 * ret.getGoodEval() / (evalNum)) + "%");
			} else {
				ret.setGoodEvalRate("0.00%");
			}
		} catch (ExternalInterfaceException e) {
			log.error(e.getMessage(), e);
		}

		// 查促销信息
//		 ret.setConcernNum(res.concernNum);
//		 ret.setPromotions(res.promotions);

		// 获取店铺电话
		try {
			User user = UserHelper.getUserInfo(sellerUin);
			if (user != null) {
				ret.setMobileNo(UserDetailUtil.getSellerTelephone(user));
				ret.setSellerCredit(user.getSellerCredit());
				ret.setSellerLevelCount((int) user.getSellerCredit());
				ret.setSellerLocation(user.getCity());
				ret.setProperty(user.getSproperties());
				try {
					ret.setGuaranteeCompensation(user.isGuaranteeCompensation() ? 1 : 0);
				} catch (Exception e) {
					ret.setGuaranteeCompensation(0);
				}
				ret.setShopType(user.getShopType());
			}
		} catch (ExternalInterfaceException e) {
			log.error(e.getMessage(), e);
		} 

		return ret;
	}

	/**
	 * 判断微店是否已经装修
	 */
	@Override
	public boolean isDecorated(long uin) {
		boolean flag = false;
		String resultStr = "";
		StringBuffer getUrl = new StringBuffer();
		getUrl.append(WEIDIAN_DECORATED_URL).append("?Uin=").append(uin);
		try {
			resultStr = HttpUtil.get(getUrl.toString(),null, 10000, 15000, "gbk", true,false,"",null);
			if (!StringUtil.isEmpty(resultStr) ) {	
				ObjectMapper jsonMapper = new ObjectMapper();
				JsonNode rootNode = jsonMapper.readValue(resultStr, JsonNode.class);
				int decorate = rootNode.path("DecorationFlag").getIntValue();
				flag = (decorate == 1);
			}
		} catch (Exception ex) {
			Log.run.info("handlePrice error " + ex.toString() + ",resultStr=" + resultStr) ;
		}
		return flag;
	}
	
	/**
	 * 查询店铺的新品数. 使用redis缓存
	 * @param shopId
	 * @return
	 */
	@Override
	public int getNewItemCountAndCache(final long shopId, final String mk)	{
		return (Integer)cacheClient.getAndSetNx(CacheKey.店铺新品数量 + shopId, 3600*24, new CacheCallback() {
			
			@Override
			public Object execute(CacheClient cacheClient) {
				Map<String, String> filter = new HashMap<String, String>();
				Calendar c = Calendar.getInstance();
				c.add(Calendar.DAY_OF_YEAR, -7);
				filter.put(CVItemFilterConstants.ITEM_FILTER_KEY_ADDTIME_START, c.getTimeInMillis()/1000 + "");
				ShopItems shopItems = getShopComdyList(shopId, mk, 0, Integer.MAX_VALUE, 
										CVItemFilterConstants.C2C_WW_COMDY_FIND_ORDERY_BY_ADDTIME_DESC, filter);
				return shopItems.getItems().size();
			}
		});
	}
	
	/**
	 * 将店铺的部分商品拼接成一组图片. 使用redis缓存
	 * @param shopId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<String> joinPicAndCache(final long shopId)	{
		return (List<String>)cacheClient.getAndSetNx(
				CacheKey.店铺组合图片 + shopId, 3600*24, new CacheCallback(){
			@Override
			public Object execute(CacheClient cacheClient) {
				List<ShopItem> items = findShopItemCgi(shopId, null, null, ShopItemSearchParam.ORDERSTYLE_最新发布降序, 
										1, Util.random(3,5)).getElements();
				List<String> pics = new ArrayList<String>();
				for(int i=0 ; i<items.size(); i++)	{
					if (i == 0) {
						pics.add(items.get(i).getImgLL());
					} else {
						pics.add(items.get(i).getImg160());
					}
				}
				return pics;
			}
		});
	}


	public void setShopDao(ShopDao shopDao) {
		this.shopDao = shopDao;
	}

	public void setCacheClient(CacheClient cacheClient) {
		this.cacheClient = cacheClient;
	}
}
