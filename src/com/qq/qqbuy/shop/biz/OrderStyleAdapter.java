package com.qq.qqbuy.shop.biz;

import com.qq.qqbuy.thirdparty.http.search.ShopItemSearchParam;

public class OrderStyleAdapter {
	
	/**
	 * 把原otype适配成ShopItemSearchParam的orderstyle
	 * 原otype
		2,// 发布时间正序(新的在前)
		3,// 发布时间逆序 X
		6, // 上架时间正序(新的在前)
		7, // 上架时间逆序 X
		18, // 销量正序
		19, // 销量逆序
		8, // 价格升序
		9, // 价格降序
		20, // 访问量正序
		21, // 访问量逆序 X
		100, // 无需排序 
		
		需要扩展13:信用降序
	 * @param otype
	 * @return ShopItemSearchParam.orderStyle
	 */
	public static int adapt(String otype)	{
		int o = Integer.parseInt(otype);
		switch(o)	{
		case 20:
			return ShopItemSearchParam.ORDERSTYLE_人气降序;
		case 8:
			return ShopItemSearchParam.ORDERSTYLE_价格升序;
		case 9:
			return ShopItemSearchParam.ORDERSTYLE_价格降序;
		case 2:
		case 6:
			return ShopItemSearchParam.ORDERSTYLE_最新发布降序;
		case 18:
			return ShopItemSearchParam.ORDERSTYLE_销量升序;
		case 19:
			return ShopItemSearchParam.ORDERSTYLE_销量降序;
		case 100:
			return ShopItemSearchParam.ORDERSTYLE_默认排序;
		case 13:
			return ShopItemSearchParam.ORDERSTYLE_信用降序;
		case 77:
			return ShopItemSearchParam.ORDERSTYLE_综合分排序;
		default:
			return ShopItemSearchParam.ORDERSTYLE_默认排序;
		}
	}

}
