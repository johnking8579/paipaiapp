package com.qq.qqbuy.shop.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.qq.qqbuy.shop.po.ShopCategoryItem;
import com.qq.qqbuy.shop.po.ShopInfo;
import com.qq.qqbuy.thirdparty.http.search.po.ShopItem;

public class ShopInfoPageVo 	{
	public Map<ShopCategoryItem, List<ShopItem>> categoryItem = new HashMap<ShopCategoryItem, List<ShopItem>>();
	public ShopInfo shopInfo; 
	public long likeCount; 
	public boolean isInFav; 
	public boolean hasWeidian; 
	public String weidianUrl;
	
	public String toJsonStr() {
		return toJsonTree().toString();
	}
	
	public JsonElement toJsonTree() {
		Gson gson = new GsonBuilder().serializeNulls().create();
		JsonObject json = gson.toJsonTree(this).getAsJsonObject();
		
		JsonArray categoryItemArr = new JsonArray();
		for(Entry<ShopCategoryItem, List<ShopItem>> entry : categoryItem.entrySet())	{
			JsonObject j = new JsonObject();
			j.addProperty("categoryId", entry.getKey().getCategoryId());
			j.addProperty("categoryName", entry.getKey().getDesc());
			
			JsonArray items = new JsonArray();
			for(ShopItem item : entry.getValue())	{
				JsonObject i = new JsonObject();
				i.addProperty("image", item.getImgLL());
				i.addProperty("image120", item.getImg160());
				i.addProperty("image200", item.getImgL());
				i.addProperty("image80", item.getImg());
				i.addProperty("itemId", item.getCommId());
				i.addProperty("num", 0);	//库存数
				i.addProperty("price", item.getPrice());
				i.addProperty("state", 0);
				i.addProperty("soldNum", item.getSaleNum());
				i.addProperty("title", item.getTitle());
				items.add(i);
			}
			j.add("item", items);
			
			categoryItemArr.add(j);
		}
		json.add("categoryItem", categoryItemArr);
		return json;
	}
	
}