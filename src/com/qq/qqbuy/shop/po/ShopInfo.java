package com.qq.qqbuy.shop.po;

import com.qq.qqbuy.common.util.region.City;
import com.qq.qqbuy.common.util.region.RegionManager;
import com.qq.qqbuy.item.util.SellerCreditUtil;


/**
 * 
 * @ClassName: ShopInfo 
 * 
 * @Description: 店铺相关信息 
 * @author wendyhu 
 * @date 2013-3-8 下午07:25:23
 */
public class ShopInfo  
{
    /**
     * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)
     */
    private static final long serialVersionUID = 1L;
    private String logo = "";
    /**
     * 注册时间 
     */
    private String regTime = "";
    private String sellerUin;//卖家uin号
    private String shopName;// 店铺名称
    private String mainBusiness;//主要经营的项目
    private int sellerLocation;// 卖家位置
    private int sellerLevelCount;//卖家等级
    private int guaranteeCompensation;//诚保标志
    private String property; /** 店铺二进制属性串：
    [11]:开通财付通
    [12]:冻结用户
    [15]:警告用户
    [16]:禁止用户
    [22]:金牌卖家
    [27]:禁止购买
    [28]:禁止投诉或举报
    [29]:禁止发布商品
    [30]:禁止留言
    [31]:禁止评价
    [32]:禁止举报
    [33]:保证金用户
    [37]:基础诚保用户
    [49]:开通CRM功能
    [50]:官字卖家
    [51]:新女装裳品廊
    [52]:VIP用户
    [53]:B2C卖家
    [54]:腾讯官方商家
    [55]:腾讯慈善商家
    [56]:营业执照认证商家
    [57]:诚保二级卖家
    [59]:QQ商城用户
    [60]:主营虚拟类目卖家
    [62]:QQ秀特权
    [63]:时尚标记尚品会
    [64]:3C保证金
    [65]:减金额优惠
    [66]:打折优惠
    [67]:送赠品优惠
    [68]:加钱换购优惠
    [69]:包快递优惠
    [70]:B2C C2C合并卖家
    [71]:绿钻用户
    [72]:直通车
    [73]:OPENAPI用户
    [74]:彩钻用户
    [75]:店铺3.0灰度属性
    [76]:3c卖家
    [77]:卖家支持手机客服
    [78]:卖家参加买立减，送促销活动
    [79]:卖家开始买立减，送促销活动
    [116]:网游快冲
    [117]:新店铺
    [118]:当期销售量
    [119]:QQ商城卖家授权修改店名和店招标记
    [173]:卖家处于加分状态
    [174]:受邀请用户*/
    private int itemCountOnSale;//当前在销售的商品数量
    private int goodDescriptionMatch;//货品符合度
    private int attitudeOfService;//服务质量
    private int speedOfDelivery;//物流速度
    private int totalEval;//总评分
    private int goodEval;//好评数
    private int normalEval;//中评数
    private int badEval;//差评数
    private String goodEvalRate="";//好评率
//    private String promotions;//前提条件：三星以上信用卖家，该店铺当前支持的满减满送促销优惠（本串固定长度为5个字符，依次是：减免金额，折扣，赠品，换购，免邮费），字符为1代表有相应的优惠，为0则没有
//    private long concernNum;//本店铺被关注数量
    private String mobileNo;
    private String telephoneBefore;
    /**
	 * sellerCredit:用户的卖家信用
	 */
	private long sellerCredit;
	private String sellerCreditLevel;	//根据sellerCredit换算
	
	// TODO 店铺类型，0：拍拍；1：网购
	private int shopType;
	
	/**
	 * 换算sellerCreditLevel
	 * @param sellerCredit
	 */
	public void setSellerCredit(long sellerCredit) {
		this.sellerCredit = sellerCredit;
		this.sellerCreditLevel = SellerCreditUtil.getSellerCreditLevel(new Long(sellerCredit).intValue());
	}
	
	
	
	public String getSellerCreditLevel() {
		return sellerCreditLevel;
	}
	
    public int getShopType() {
		return shopType;
	}
    
	public void setShopType(int shopType) {
		this.shopType = shopType;
	}
	 
    public String getGoodEvalRate()
    {
        return goodEvalRate;
    }
    public void setGoodEvalRate(String goodEvalRate)
    {
        this.goodEvalRate = goodEvalRate;
    }
    public int getTotalEval()
    {
        return totalEval;
    }
    public void setTotalEval(int totalEval)
    {
        this.totalEval = totalEval;
    }
    public String getRegTime()
    {
        return regTime;
    }
    public void setRegTime(String regTime)
    {
        this.regTime = regTime;
    }
    public String getLogo()
    {
        return logo;
    }
    public void setLogo(String logo)
    {
        this.logo = logo;
    }
    public String getSellerUin()
    {
        return sellerUin;
    }
    public void setSellerUin(String sellerUin)
    {
        this.sellerUin = sellerUin;
    }
    public String getShopName()
    {
        return shopName;
    }
    public void setShopName(String shopName)
    {
        this.shopName = shopName;
    }
    public String getMainBusiness()
    {
        return mainBusiness;
    }
    public void setMainBusiness(String mainBusiness)
    {
        this.mainBusiness = mainBusiness;
    }
    public int getSellerLocation()
    {
        return sellerLocation;
    }
    public void setSellerLocation(int sellerLocation)
    {
        this.sellerLocation = sellerLocation;
    }
    public int getSellerLevelCount()
    {
        return sellerLevelCount;
    }
    public void setSellerLevelCount(int sellerLevelCount)
    {
        this.sellerLevelCount = sellerLevelCount;
    }
    public int getGuaranteeCompensation()
    {
        return guaranteeCompensation;
    }
    public void setGuaranteeCompensation(int guaranteeCompensation)
    {
        this.guaranteeCompensation = guaranteeCompensation;
    }
    public String getProperty()
    {
        return property;
    }
    public void setProperty(String property)
    {
        this.property = property;
    }
    public int getItemCountOnSale()
    {
        return itemCountOnSale;
    }
    public void setItemCountOnSale(int itemCountOnSale)
    {
        this.itemCountOnSale = itemCountOnSale;
    }
    public int getGoodDescriptionMatch()
    {
        return goodDescriptionMatch;
    }
    public void setGoodDescriptionMatch(int goodDescriptionMatch)
    {
        this.goodDescriptionMatch = goodDescriptionMatch;
    }
    public int getAttitudeOfService()
    {
        return attitudeOfService;
    }
    public void setAttitudeOfService(int attitudeOfService)
    {
        this.attitudeOfService = attitudeOfService;
    }
    public int getSpeedOfDelivery()
    {
        return speedOfDelivery;
    }
    public void setSpeedOfDelivery(int speedOfDelivery)
    {
        this.speedOfDelivery = speedOfDelivery;
    }
    public int getGoodEval()
    {
        return goodEval;
    }
    public void setGoodEval(int goodEval)
    {
        this.goodEval = goodEval;
    }
    public int getNormalEval()
    {
        return normalEval;
    }
    public void setNormalEval(int normalEval)
    {
        this.normalEval = normalEval;
    }
    public int getBadEval()
    {
        return badEval;
    }
    public void setBadEval(int badEval)
    {
        this.badEval = badEval;
    }
//    public String getPromotions()
//    {
//        return promotions;
//    }
//    public void setPromotions(String promotions)
//    {
//        this.promotions = promotions;
//    }
//    public long getConcernNum()
//    {
//        return concernNum;
//    }
//    public void setConcernNum(long concernNum)
//    {
//        this.concernNum = concernNum;
//    }
    
    
    public String getMobileNo() {
        return mobileNo;
    }
    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }
	public String getTelephoneBefore() {
		return telephoneBefore;
	}
	public void setTelephoneBefore(String telephoneBefore) {
		this.telephoneBefore = telephoneBefore;
	}
	public long SellerCredit() {
		return sellerCredit;
	}
	
    public String getLocationDesc() {
    	City city  =  RegionManager.getCityById(this.getSellerLocation());
    	if(city != null) {
    		return city.cityName;
    	}
    	return "未选择";
    }
    public boolean isMallShop() {
    	if (property.length() > 58) {
    		if ('1' == property.charAt(58)) {
    			return true;
    		}
    	}
    	return false;
    }

	public void setSellerCreditLevel(String sellerCreditLevel) {
		this.sellerCreditLevel = sellerCreditLevel;
	}

	public long getSellerCredit() {
		return sellerCredit;
	}
    
}
