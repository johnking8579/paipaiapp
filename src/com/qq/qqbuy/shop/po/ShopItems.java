package com.qq.qqbuy.shop.po;

import java.util.ArrayList;
import java.util.List;

import com.qq.qqbuy.common.po.CommonPo;

public class ShopItems extends CommonPo
{

    /**
     * 商品列表
     */
    private List<ShopSimpleItem> items = new ArrayList<ShopSimpleItem>();
    
    /**
     * 查询到的数量
     */
    private long findNum = 0;
    
    /**
     * 总共数量
     */
    private long totalNum = 0;

    public List<ShopSimpleItem> getItems()
    {
        return items;
    }

    public void setItems(List<ShopSimpleItem> items)
    {
        this.items = items;
    }

    public long getFindNum()
    {
        return findNum;
    }

    public void setFindNum(long findNum)
    {
        this.findNum = findNum;
    }

    public long getTotalNum()
    {
        return totalNum;
    }

    public void setTotalNum(long totalNum)
    {
        this.totalNum = totalNum;
    }
    
    
    
}
