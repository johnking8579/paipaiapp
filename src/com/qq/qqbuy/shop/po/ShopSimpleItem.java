package com.qq.qqbuy.shop.po;

import java.util.HashMap;
import java.util.Map;

import com.qq.qqbuy.common.po.CommonPo;
import com.qq.qqbuy.common.util.SumUtil;

/**
 * 
 * @ClassName: ShopItem
 * 
 * @Description: 店铺的简单商品信息
 * @author wendyhu
 * @date 2013-3-8 下午07:49:42
 */
public class ShopSimpleItem extends CommonPo
{
    /**
     * 商品单价
     * 
     * 版本 >= 0
     */
    private long price;

    /**
     * 商品数量
     * 
     * 版本 >= 0
     */
    private long num;

    /**
     * 商品新旧程度 值见{@link com.paipai.c2c.item.constants.ItemConstants}
     * 
     * 版本 >= 0
     */
    private long newType;

    /**
     * 出售方式 值见{@link com.paipai.c2c.item.constants.ItemConstants}
     * 
     * 版本 >= 0
     */
    private long dealType;

    /**
     * 店铺推荐
     * 
     * 版本 >= 0
     */
    private long isRecommend;

    /**
     * 商品状态 值见{@link com.paipai.c2c.item.constants.ItemConstants}
     * 
     * 版本 >= 0
     */
    private long state;

    /**
     * 运费承担方式 值见
     * {@link com.paipai.c2c.item.constants.ItemConstants#TRANSPORT_SELLER_PAY}等
     * 
     * 版本 >= 0
     */
    private long transportPriceType;

    /**
     * 商品发货方式 值见
     * {@link com.paipai.c2c.item.constants.ItemConstants#SEND_TYPE_MONEY}等
     * 
     * 版本 >= 0
     */
    private long sendType;

    /**
     * 平邮价格
     * 
     * 版本 >= 0
     */
    private long normalMailPrice;

    /**
     * 快递价格
     * 
     * 版本 >= 0
     */
    private long expressMailPrice;

    /**
     * 商品ID
     * 
     * 版本 >= 0
     */
    private String itemId = new String();

    /**
     * 商品名称
     * 
     * 版本 >= 0
     */
    private String title = new String();

    /**
     * 商品主图300*300
     */
    private String image = "";
     
     /**
      * 80*80商品主图
      */
     private String image80 = "";
     
     /**
      * 120*120商品主图
      */
     private String image120 = "";
     
     /**
      * 200*200商品主图
      */
     private String image200 = "";

     /**
      * 已经售出
      */
     private long soldNum = 0;
    
    private Map properties = new HashMap();

    public long getSoldNum()
    {
        return soldNum;
    }

    public void setSoldNum(long soldNum)
    {
        this.soldNum = soldNum;
    }

    public long getPrice()
    {
        return price;
    }

    public void setPrice(long price)
    {
        this.price = price;
    }

    public long getNum()
    {
        return num;
    }

    public void setNum(long num)
    {
        this.num = num;
    }

    public long getNewType()
    {
        return newType;
    }

    public void setNewType(long newType)
    {
        this.newType = newType;
    }

    public long getDealType()
    {
        return dealType;
    }

    public void setDealType(long dealType)
    {
        this.dealType = dealType;
    }

    public long getIsRecommend()
    {
        return isRecommend;
    }

    public void setIsRecommend(long isRecommend)
    {
        this.isRecommend = isRecommend;
    }

    public long getState()
    {
        return state;
    }

    public void setState(long state)
    {
        this.state = state;
    }

    public long getTransportPriceType()
    {
        return transportPriceType;
    }

    public void setTransportPriceType(long transportPriceType)
    {
        this.transportPriceType = transportPriceType;
    }

    public long getSendType()
    {
        return sendType;
    }

    public void setSendType(long sendType)
    {
        this.sendType = sendType;
    }

    public long getNormalMailPrice()
    {
        return normalMailPrice;
    }

    public void setNormalMailPrice(long normalMailPrice)
    {
        this.normalMailPrice = normalMailPrice;
    }

    public long getExpressMailPrice()
    {
        return expressMailPrice;
    }

    public void setExpressMailPrice(long expressMailPrice)
    {
        this.expressMailPrice = expressMailPrice;
    }

    public String getItemId()
    {
        return itemId;
    }

    public void setItemId(String itemId)
    {
        this.itemId = itemId;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }

    @Override
    public String toString()
    {
        return "ShopSimpleItem [dealType=" + dealType + ", expressMailPrice="
                + expressMailPrice + ", image=" + image + ", isRecommend="
                + isRecommend + ", itemId=" + itemId + ", newType=" + newType
                + ", normalMailPrice=" + normalMailPrice + ", num=" + num
                + ", price=" + price + ", sendType=" + sendType + ", state="
                + state + ", title=" + title + ", transportPriceType="
                + transportPriceType + "]";
    }

	public String getImage80() {
		return image80;
	}

	public void setImage80(String image80) {
		this.image80 = image80;
	}
	
	public String getImage120() {
		return image120;
	}

	public void setImage120(String image120) {
		this.image120 = image120;
	}

	public String getImage200() {
		return image200;
	}

	public void setImage200(String image200) {
		this.image200 = image200;
	}

	/**
	 * 获取手拍商品价格
	 * @return
	 */
	public String getQgoPrice() {
		if (this.isQgo()) {
			if (this.getTransportPriceType() == 1) {
				return SumUtil.getFormattedCash(this.getPrice() + 1500); 
			} else {
				return SumUtil.getFormattedCash(this.getPrice() + this.normalMailPrice);
			}
		}
		return SumUtil.getFormattedCash(this.getPrice());
	}

	public Map getProperties() {
		return properties;
	}

	public void setProperties(Map properties) {
		this.properties = properties;
	}
	
	/**
	 * 是否为手拍货到付款
	 * @return
	 */
	public boolean isQgo() {
		if (this.properties != null) {
			for (Object obj :this.properties.keySet()) {
				if ("128".equals(obj.toString())) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * 是否为PC货到付款
	 * @return
	 */
	public boolean isCod() {
		if (this.properties != null) {
			for (Object obj :this.properties.keySet()) {
				if ("8".equals(obj.toString())) {
					return true;
				}
			}
		}
		return false;
	}
	
}
