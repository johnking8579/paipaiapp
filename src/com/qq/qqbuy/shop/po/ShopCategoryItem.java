package com.qq.qqbuy.shop.po;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @ClassName: ShopCategoryItem
 * 
 * @Description: 店铺分类
 * @author wendyhu
 * @date 2013-3-8 下午08:08:26
 */
public class ShopCategoryItem {
	/**
	 * 自定义分类id
	 * 
	 * 版本 >= 0
	 */
	private long categoryId = 0;

	/**
	 * 自定义分类名称
	 * 
	 * 版本 >= 0
	 */
	private String desc = new String();

	/**
	 * 图片url
	 * 
	 * 版本 >= 0
	 */
	private String picUrl = new String();

	/**
	 * 父自定义分类id,如果是一级分类，则为空
	 * 
	 * 版本 >= 0
	 */
	private long parentId = 0;

	/**
	 * 子分类
	 */
	private List<ShopCategoryItem> childs = new ArrayList<ShopCategoryItem>();

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public long getParentId() {
		return parentId;
	}

	public void setParentId(long parentId) {
		this.parentId = parentId;
	}

	public List<ShopCategoryItem> getChilds() {
		return childs;
	}

	public void setChilds(List<ShopCategoryItem> childs) {
		this.childs = childs;
	}
}
