package com.qq.qqbuy.shop.dao;

public interface ShopDao {

	/**
	 * 根据qq号在mysql库查询白名单列表查询是否存在
	 * @param uin
	 * @return
	 */
	boolean isDecoratedByUin(long uin);

}
