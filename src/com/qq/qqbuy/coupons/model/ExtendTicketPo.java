package com.qq.qqbuy.coupons.model;



/**
 * 

 * @ClassName: ExtendTicketPo 

 * @Description: 发券信息po
 
 * @author lhn

 * @date 2015-2-4 下午1:45:46 

 *
 */
public class ExtendTicketPo {
	/**
	 * 业务号
	 */
	private String businessid;
	/**
	 * 客户id
	 */
	private long wid;
	/**
	 * 已获取几张
	 */
	private int getnum;
	/**
	 * 设备号
	 */
	private String deviceid;
	/**
	 * 发放是否针对人类型（0：设备 false，1：客户id true）
	 */
	private boolean target;
	/**
	 * 批次号
	 */
	private String packetstockid;
	/**
	 * 失效时间
	 */
	private Long endtime;
	/**
	 * 一些领取信息：以json存放
	 */
	private String commondata;
	/**
	 * 领取时间
	 */
	private Long createtime;
	private String reserve1;
	private String reserve2;
	public String getBusinessid() {
		return businessid;
	}
	public void setBusinessid(String businessid) {
		this.businessid = businessid;
	}
	public long getWid() {
		return wid;
	}
	public void setWid(long wid) {
		this.wid = wid;
	}
	public int getGetnum() {
		return getnum;
	}
	public void setGetnum(int getnum) {
		this.getnum = getnum;
	}
	public String getDeviceid() {
		return deviceid;
	}
	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}
	public boolean isTarget() {
		return target;
	}
	public void setTarget(boolean target) {
		this.target = target;
	}
	public String getPacketstockid() {
		return packetstockid;
	}
	public void setPacketstockid(String packetstockid) {
		this.packetstockid = packetstockid;
	}
	public Long getEndtime() {
		return endtime;
	}
	public void setEndtime(Long endtime) {
		this.endtime = endtime;
	}
	public String getCommondata() {
		return commondata;
	}
	public void setCommondata(String commondata) {
		this.commondata = commondata;
	}
	public Long getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Long createtime) {
		this.createtime = createtime;
	}
	public String getReserve1() {
		return reserve1;
	}
	public void setReserve1(String reserve1) {
		this.reserve1 = reserve1;
	}
	public String getReserve2() {
		return reserve2;
	}
	public void setReserve2(String reserve2) {
		this.reserve2 = reserve2;
	}
	
	
}
