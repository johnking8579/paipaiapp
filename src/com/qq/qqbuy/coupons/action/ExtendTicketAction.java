package com.qq.qqbuy.coupons.action;



import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.qq.qqbuy.common.JsonOutput;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.action.AuthRequiredAction;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.common.util.StrMD5;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.coupons.biz.ExtendTicketBiz;
import com.qq.qqbuy.coupons.model.ExtendTicketPo;
import com.qq.qqbuy.login.dao.LoginToDayDao;
import com.qq.qqbuy.login.po.LoginToDay;
import com.qq.qqbuy.virtualgoods.action.TimeUtils;


/**
 * 

 * @ClassName: ExtendTicketAction 

 * @Description: 发放优惠券
 
 * @author lianghaining1@jd.com

 * @date 2015-2-4 上午11:45:41 

 *
 */
public class ExtendTicketAction extends AuthRequiredAction{
	private ExtendTicketBiz extendTicketBiz;
	private LoginToDayDao loginToDayDaoImpl;
	private String callback;
	private static Long endtime=TimeUtils.formatDateStringToInt("2015-03-26 23:59:00")*1000;
	//批次号1
	private static String packetstockid1="71959000";
	//批次号2
	private static String packetstockid2="71919002";
	// 每一个人本批次可以领取券的个数
	private static int allnum = 1;
	private static String businessid="phonerecharge201501";
	//MD5加密码密钥
	private static String KEY_FOR_SEND ="FEELHN13586FW%^f$^FCCC%$fydsfuewf$d%6CASJSAHU";
	//领取第几批
	private String batch;
	//标志号  batch1
	private String validateKey;
	
	
	private static final String LIMIT_PACKET_1 = "70652081";
	private static final String LIMIT_PACKET_2 = "70589096";
	//是否不验证
	private boolean nocheck = false;
	
	private Long uin;
	
	//限制时间
	private  static final long LIMIT_TIME=TimeUtils.formatDateStringToInt("2015-03-15 00:00:00")*1000;
	
	private static Logger limitLog = LogManager.getLogger("limitolderLog");
	//输出信息
	JsonOutput out = new JsonOutput();
	/**
	 * 
	
	 * @Title: sendTicket 
	
	 * @Description: 发放优惠券方法入口
	
	 * @param @return    设定文件 
	
	 * @return String    返回类型 
	
	 * @throws
	 */
	public String sendTicket() {
		Log.run.info("----------------------------------sendTicket---------------------");
		// 验证是否登录了
		if (checkLogin()) {
			if (!("batch1").equals(batch)&& !("batch2").equals(batch)) {
				out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER + "");
				out.setMsg("批次参数有误");
				return doPrint(out.toJsonStr());
			}
			//如果登录了，则直接调用别人的发券。不解释
			try {
				String ip = getHttpHeadIp() ;
				int port = getRequest().getRemotePort() ;
				if (ip != null && ip.length() > 6) {
					this.setCp(ip);
				} else {
					this.setCp("127.0.0.1");
				}
				long vkInt = Util.genVisitkey(this.getCp(), port) ;
				Map<String, String> reqParam = new HashMap<String, String>();
				reqParam.put("platform", "android".equals(getMt())?"1":"2");
				reqParam.put("appver",getVersionCode());
				reqParam.put("device_id", getMk());
				String md5 = new StrMD5(getMk()+"http://wd.paipai.com/wxd/user/getuserinfo").getResult();
				reqParam.put("wgtoken", md5);
				String url ="";
				if ("batch1".equals(batch)) {
					url ="http://wd.paipai.com/wxd/charges/ receivevoucherbj";
				}else if ("batch2".equals(batch)){
					url ="http://wd.paipai.com/wxd/charges/ registersharebj";
				}
				String result = HttpUtil.post(url, 
	            		reqParam, 10000, 15000, "GBK", "GBK", true ,"wg_uin=" + getWid()+"; wg_skey="+getSk()+"; visitkey="+vkInt+";","pay.paipai.com",true,"beijingpaipaiapp");		
				JsonParser jsonParser = new JsonParser();
				JsonElement json= jsonParser.parse(result);
				String retCode  = json.getAsJsonObject().get("ret").getAsString();	
				String err  = json.getAsJsonObject().get("err").getAsString();	
				if("0".equals(retCode)){
					 out.setErrCode(retCode);
				     out.setMsg("领取成功");
				}else {
					out.setErrCode(retCode);
				    out.setMsg(err);
				}
			} catch (Exception e) {
	            			
	            out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP + "");
				out.setMsg("调用发券接口错误");
				}
		} else {// 如果没有登录则
			out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL + "");
			out.setMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
		}
		return doPrint(out.toJsonStr());
	}

	
	public String oldsendTicket() {
		Log.run.info("----------------------------------sendTicket---------------------");
		// 验证是否登录了
		if (checkLogin()||nocheck) {
			//如果登录了，就用登录的
			if(checkLogin()){
				uin = getWid();
			}
			if (StringUtil.isEmpty(getMk())) {
				out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER + "");
				out.setMsg("设备参数有误");
				return doPrint(out.toJsonStr());
			}
			if (StringUtil.isEmpty(batch) || StringUtils.isEmpty(validateKey)) {
				out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER + "");
				out.setMsg("参数有误");
				return doPrint(out.toJsonStr());
			}
			if (!batch.equals("batch1") && !batch.equals("batch2")) {
				out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER + "");
				out.setMsg("批次参数有误");
				return doPrint(out.toJsonStr());
			}
			Log.run.info("StrMD5:"+getAppToken() + batch + KEY_FOR_SEND+":"+validateKey);
			// 验证一下加密后的数据
			try {
				if ((new StrMD5(URLEncoder.encode(getAppToken(),"utf-8") + batch + KEY_FOR_SEND).getResult()
						.equals(validateKey))||nocheck) {

					 ExtendTicketPo exp = null;
					//先要对上次领取的过的进行限制不能再领取
					
					List<ExtendTicketPo> limitexp = null;
					try {
						limitexp = this.extendTicketBiz.getLimitExp(LIMIT_PACKET_1,LIMIT_PACKET_2,getMk());
					} catch (Exception e) {
						out.setErrCode(ErrConstant.ERRCODE_DAO_QUERY_DB_FAIL + "");
						out.setMsg("领取失败,数据获取异常");
						return doPrint(out.toJsonStr());  
					}
					LoginToDay login = null;
					Log.run.info("loginToDayDao"+loginToDayDaoImpl+",wid"+getWid());
					try {
						 login = this.loginToDayDaoImpl.findoldLogBywid(getWid());
					} catch (Exception e1) {
						Log.run.info("查询用户登录信息失败！");
						e1.printStackTrace();
					}
					Log.run.info(new Gson().toJson(login));
					Log.run.info("统计屏蔽 信息");
					if(limitexp!=null&&limitexp.size()>0){
						Log.run.info("limitexpyonghu"+getWid());
						limitLog.info("limitexpyonghu"+getWid());
					}
					if(login!=null&&login.getInsertTime()<=LIMIT_TIME){
						Log.run.info("loginoldyonghu---"+getWid()+":"+login.getInsertTime());
						limitLog.info("loginoldyonghu---"+getWid()+":初次登录时间>>>>"+TimeUtils.formatIntToDateString(login.getInsertTime()/1000)+",毫秒>>>>>"+login.getInsertTime());
					}
					//什么样的才能领呢，上次没有领取并且是老用户
					if((limitexp==null||(limitexp!=null&&limitexp.size()<=0))&&(login==null||(login!=null&&login.getInsertTime()>LIMIT_TIME))){
								String result = "";
								try {
									if (batch.equals("batch1")) {
										// 如果登录了，验证是否已经不能再领取了allnum
										exp = this.extendTicketBiz.getExpBywid(packetstockid1,
												getMk(), false);
									} else if (batch.equals("batch2")) {
										exp = this.extendTicketBiz.getExpBywid(packetstockid2,
												getMk(), false);
									}
								} catch (Exception e) {
									out.setErrCode(ErrConstant.ERRCODE_DAO_QUERY_DB_FAIL + "");
									out.setMsg("领取失败");
									return doPrint(out.toJsonStr()); 
								}
							
								Log.run.info("expexpexp:"+exp);
								limitLog.info("newuserrequest:"+getWid());
								String commondata = "";
								// 如果为null或者不为null但领取的数据小于限制的数量则可以领取
								if (exp == null || (exp != null && exp.getGetnum() < allnum)) {
									limitLog.info("newuserrequestallsuccess:"+getWid());
									if (batch.equals("batch1")) {
										limitLog.info("newuserrequest"+packetstockid1+"success:"+getWid());
										result = this.extendTicketBiz.sendTicket(uin,
												packetstockid1, getMk(), getHttpHeadIp(),
												endtime, allnum, businessid, commondata,getSk());
										// 如果第一批发券失败则去第二批中去再拿一次
										/*if (result != null && result.equals("9")) {
											result = this.extendTicketBiz.sendTicket(uin,
													packetstockid2, getMk(), getHttpHeadIp(),
													endtime, allnum, businessid, commondata,getSk());
										 }*/
									} else if (batch.equals("batch2")) {
										limitLog.info("newuserrequest"+packetstockid2+"success:"+getWid());
										result = this.extendTicketBiz.sendTicket(uin,
												packetstockid2, getMk(), getHttpHeadIp(),
												endtime, allnum, businessid, commondata,getSk());
									}
										/*	iRet = 999 用户未登录
											iRet = 100000001 参数有误
											iRet = 1806  店铺优惠券过期
											iRet = 10001  非法参数
											iRet = 1000001 批量店铺优惠券必须为同一卖家
											iRet = 1000002 店铺优惠券已发完
											iRet = 1000003 个人领取达到上限  
											iRet = 其他  系统错误*/
									if(result==null||!result.equals("0")){
										out.setErrCode(result);
										//out.setMsg("很抱歉优惠券已抢光！");
										if(("999").equals(result))
											out.setMsg("用户未登录");
										else if(("100000001").equals(result))
											out.setMsg("参数有误");
										else if(("1806").equals(result))
											out.setMsg("优惠券过期");
										else if(("10001").equals(result))
											out.setMsg("非法参数");
										else if(("1000001").equals(result))
											out.setMsg("批量店铺优惠券必须为同一卖家");
										else if(("1000002").equals(result))
											out.setMsg("本轮券已发完，表桑心，期待我们下轮活动！");
										else if(("1000003").equals(result))
											out.setMsg("个人领取达到上限 ");
										else
											out.setMsg("系统错误");
										
									 }else {
										out.setErrCode(result);
										out.setMsg("成功");
									 }
								} else {
									limitLog.info("newuserrequessuccessbutnoget:"+getWid());
									out.setErrCode(ErrConstant.ERRCODE_PROMOTE_OTHER_ERROR + "");
									out.setMsg("您已经领过券了，不要太贪心哦！");
								}
					}else {
						limitLog.info("bingbiyonghu"+getWid());
						Log.run.info("bingbiyonghu"+getWid());
						out.setErrCode(ErrConstant.ERRCODE_PROMOTE_OTHER_ERROR + "");
						out.setMsg("您是老客人啦~去逛逛别的吧，期待下次活动！");
					}
				} else {
					out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER + "");
					out.setMsg("密钥参数有误");
					return doPrint(out.toJsonStr());
				}
			} catch (UnsupportedEncodingException e) {
				Log.run.info("编码错误？ 。。。怎么产生的没有UTF-8编码？这不扯");
				e.printStackTrace();
			}

		} else {// 如果没有登录则
			out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL + "");
			out.setMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
		}
		return doPrint(out.toJsonStr());
	} 
	
	public void saveEx(){
		this.extendTicketBiz.saveExp(345433386L,
				packetstockid1, "lhntest", "127.0.0.1",
				endtime, allnum, businessid, "");
	}
	
	public String getExp(){
		List<ExtendTicketPo> limitexp = null;
		try {
			limitexp = this.extendTicketBiz.getLimitExp(LIMIT_PACKET_1,LIMIT_PACKET_2,"lhntest");
		} catch (Exception e) {
			out.setErrCode(ErrConstant.ERRCODE_DAO_QUERY_DB_FAIL + "");
			out.setMsg("领取失败,数据获取异常");
				return doPrint(out.toJsonStr());  
		}
		if(limitexp==null||(limitexp!=null&&limitexp.size()<=0)){
				ExtendTicketPo p = this.extendTicketBiz.getExpBywid(packetstockid1, "lhntest", false);
				return doPrint(new Gson().toJson(p));
		}else {
			out.setErrCode(ErrConstant.ERRCODE_DAO_QUERY_DB_FAIL + "");
			out.setMsg("不能领取了");
			return doPrint(out.toJsonStr());
		}
		
	}
	
	public String testMD(){
		boolean is  = (new StrMD5(getAppToken() + batch + KEY_FOR_SEND).getResult()
				.equals(validateKey));
		return is==true?"true":"false";
	}
	
	public ExtendTicketBiz getExtendTicketBiz() {
		return extendTicketBiz;
	}

	public void setExtendTicketBiz(ExtendTicketBiz extendTicketBiz) {
		this.extendTicketBiz = extendTicketBiz;
	}

	public String getCallback() {
		return callback;
	}

	public void setCallback(String callback) {
		this.callback = callback;
	}

	public JsonOutput getOut() {
		return out;
	}

	public void setOut(JsonOutput out) {
		this.out = out;
	}


	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}
	
	
	public String getValidateKey() {
		return validateKey;
	}

	public void setValidateKey(String validateKey) {
		this.validateKey = validateKey;
	}
	
	
	public Long getUin() {
		return uin;
	}

	public void setUin(Long uin) {
		this.uin = uin;
	}
	

	public LoginToDayDao getLoginToDayDaoImpl() {
		return loginToDayDaoImpl;
	}


	public void setLoginToDayDaoImpl(LoginToDayDao loginToDayDaoImpl) {
		this.loginToDayDaoImpl = loginToDayDaoImpl;
	}


	public static void main(String[] args) throws UnsupportedEncodingException {
		System.out.println(URLEncoder.encode("wtOwLs3k5d2DR4aO16wTqYN8VHzLR558RnyWf7PDWycW/Ecsi+QnFsvh1NNME/P6sr9DBO5xpoEpocNjJVk9kEPsI71tFvLWUBAAZiI3z3T66YNtTNoz7MeW77r96D6HIqNEwPqtjPPL+DCrEIUhouP1w+212JcpDpGjG+oekJCbLqFKNlnBEj3LI1JFkpkeubHTxnZxnRwF1u43jjBpI3adz9Hjr+mCjAf5zRANXZop5iRzBFKUjszD1tv7OMkAwoafoCWPNM7ZvCJsrlZBchBT+PJDwdNvWNCjo+2lfwqaI=","utf-8"));
		System.out.println(new StrMD5("wti8jgJefeyzX02ZaYMtqGnCkdN1%2BuX6vr1rZnOC%2FkHYJELPmzlscmBqWtO4cVFt18yhTPNL9gJfhnMb2X3C9f%2Bm5HI6cXVQgTBSqbjAFNM%2BX9cQHO6t6ji%2FhbtH9SnArGQzJV0JzerXkHoGPbest4Kfj77tk%2F2qNQCrCdsA9V4FvE3hZVBQWXq6YXHMn2MZQZLprzDCDv98V274qFqblzU6MvKaKRD9ZLm%2B9TbpbKSpqFOzcmaoUCXvpLUZFr0UVmRQyXZ1wZp7p7KsuALl61IGgUHK00Zorm" + "batch2" + KEY_FOR_SEND).getResult());
		boolean si =new StrMD5(URLEncoder.encode("wtKP1/OVf2AeKyenLuew6b3ixSbszBoutaHSY0elMnQw/G3k84IPlzSaFuoh4IRJwbiL4YX/8TIimIDguH4Dp0BcUs+IXnJTsQrB3NwmKM4ySb2SbVH1TPsqEDnvG2pwEBPfcrZKf/iJMriIR8MiNVLPVEtb8EOh5uPOzex43iSTRaiCbgETMXkuEBCy+KMeInORc5mBb71Oa1FmN3eb8WZdER3YbjdyESrsjRheawRKs3FBH5XIMx6xc/UdkEIXhg9JNPI9RlWIyjACvAk3ZieA==","utf-8") + "batch1" + KEY_FOR_SEND).getResult().equals("a9fcdd326a09a21778da60429be2e3e3");
		System.out.println(si);
		System.out.println(System.currentTimeMillis()); 
		System.out.println("时间 ："+TimeUtils.formatDateStringToInt("2015-03-21 00:00:00")*1000);
		System.out.println(TimeUtils.formatIntToDateString(1426816803));
		String recoresult="try{SendRedpacket({\"uin\":\"22174907\",\"selleruin\":\"1302000556\",\"packetId\":\"1951671652,69446021\",\"retCode\":\"0\",\"retMsg\":\"success\"});}catch(e){}";
		String st = "try{SendRedpacket(";
		String ed = ");}catch(e){}";
		int stn = recoresult.indexOf(st);
		int edn = recoresult.indexOf(ed);
		//如果没有jsonp中的特殊字段则返回null
		if (stn >= 0 && edn > 0) {
			recoresult = recoresult.substring(stn + st.length(), edn);
			//取出json中的字段
			// 取出库存里的东西
			JsonParser jsonParser = new JsonParser();
			JsonElement json= jsonParser.parse(recoresult);
			String uin  = json.getAsJsonObject().get("uin").getAsString();
			System.out.println(uin);
	}
	}



	
}
