package com.qq.qqbuy.applog.action;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.qq.qqbuy.common.action.BasicAction;

/**
 * 
 * 
 * @ClassName: AddAppLog
 * 
 * @Description: 用于记录App传来的信息
 * 
 * @author lhn
 * 
 * @date 2014-12-2 下午3:54:40
 * 
 * 
 */
public class AddAppLog extends BasicAction {
	static Logger log = LogManager.getLogger(AddAppLog.class.getName());

	private String context;// 用于保存的信息

	/**
	 * 
	 * 
	 * @Title: addAppLog
	 * 
	 * @Description: 将App端传入的数据保存至log
	 * 
	 * @param @return 设定文件
	 * 
	 * @return String 返回类型
	 * 
	 * @throws
	 */
	public void addAppLog() {
		if (context != null && !"".equals(context)) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			StringBuffer sb = new StringBuffer();
			sb.append("IP:" + getHttpHeadIp()).append("\t");
			sb.append("addTime:" + sdf.format(new Date())).append("\t");
			sb.append(context);
			log.info(sb.toString());
		}
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

}
