package com.qq.qqbuy.favorite.po;

import java.util.ArrayList;
import java.util.List;

import com.qq.qqbuy.common.po.CommonPo;

public class FavoriteUpdatePo extends CommonPo
{
    /**
     * 添加结果
     *
     * 版本 >= 0
     */
     private List<Long> result = new ArrayList<Long>();

    /**
     * 总的收藏数
     *
     * 版本 >= 0
     */
     private long totalCount;
     
     private String[] items = new String[]{} ;

    public List<Long> getResult()
    {
        return result;
    }

    public void setResult(List<Long> result)
    {
        this.result = result;
    }

    public long getTotalCount()
    {
        return totalCount;
    }

    public void setTotalCount(long totalCount)
    {
        this.totalCount = totalCount;
    }

    
    public String[] getItems() {
		return items;
	}

	public void setItems(String[] items) {
		this.items = items;
	}

	@Override
    public String toString()
    {
        return "FavoriteUpdatePo [result=" + result + ", totalCount="
                + totalCount + ", errCode=" + errCode + ", msg=" + msg
                + ", retCode=" + retCode + "]";
    }
    
}
