package com.qq.qqbuy.favorite.po;

import com.qq.qqbuy.common.po.CommonPo;

public class FavoriteShopDetailPo extends CommonPo
{

    /**
     * 收藏店铺的用户
     * 
     * 版本 >= 0
     */
    private long UserId;

    /**
     * 被收藏的店铺的id，即卖家qq号
     * 
     * 版本 >= 0
     */
    private long ShopId;

    /**
     * 店铺名称
     * 
     * 版本 >= 0
     */
    private String shopName = new String();

    /**
     * 店铺图片
     * 
     * 版本 >= 0
     */
    private String shopLogoPos = new String();

    /**
     * 店铺卖家昵称
     * 
     * 版本 >= 0
     */
    private String sellerNickName = new String();

    /**
     * 店铺信用
     * 
     * 版本 >= 0
     */
    private long shopCredit;

    /**
     * 收藏店铺的总评分，
     * add for favorite shop list interface
     */
    private int totalEval;
    
    /**
     * 店铺好评
     * 
     * 版本 >= 0
     */
    private String shopGoodRate = new String();

    /**
     * 店铺收藏人数
     * 
     * 版本 >= 0
     */
    private long favoriteHeat;

    /**
     * 收藏该店铺的时间
     * 
     * 版本 >= 0
     */
    private long addTime;

   
    
    public long getUserId()
    {
        return UserId;
    }

    public void setUserId(long userId)
    {
        UserId = userId;
    }

    public long getShopId()
    {
        return ShopId;
    }

    public void setShopId(long shopId)
    {
        ShopId = shopId;
    }

    public String getShopName()
    {
        return shopName;
    }

    public void setShopName(String shopName)
    {
        this.shopName = shopName;
    }

    public String getShopLogoPos()
    {
        return shopLogoPos;
    }

    public void setShopLogoPos(String shopLogoPos)
    {
        this.shopLogoPos = shopLogoPos;
    }

    public String getSellerNickName()
    {
        return sellerNickName;
    }

    public void setSellerNickName(String sellerNickName)
    {
        this.sellerNickName = sellerNickName;
    }

    public long getShopCredit()
    {
        return shopCredit;
    }

    public void setShopCredit(long shopCredit)
    {
        this.shopCredit = shopCredit;
    }

    public String getShopGoodRate()
    {
        return shopGoodRate;
    }

    public void setShopGoodRate(String shopGoodRate)
    {
        this.shopGoodRate = shopGoodRate;
    }

    public long getFavoriteHeat()
    {
        return favoriteHeat;
    }

    public void setFavoriteHeat(long favoriteHeat)
    {
        this.favoriteHeat = favoriteHeat;
    }

    public long getAddTime()
    {
        return addTime;
    }

    public void setAddTime(long addTime)
    {
        this.addTime = addTime;
    }

	public int getTotalEval() {
		return totalEval;
	}

	public void setTotalEval(int totalEval) {
		this.totalEval = totalEval;
	}

}
