package com.qq.qqbuy.favorite.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.qq.qqbuy.bi.biz.BiBiz;
import com.qq.qqbuy.bi.biz.Dap;
import com.qq.qqbuy.bi.biz.ExposureMsg;
import com.qq.qqbuy.common.ChannelParam;
import com.qq.qqbuy.common.JsonOutput;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.action.AuthRequiredAction;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.exception.NotLoginException;
import com.qq.qqbuy.common.po.CommonPo;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.discover.biz.DiscoverBiz;
import com.qq.qqbuy.favorite.biz.FavoriteBiz;
import com.qq.qqbuy.favorite.po.FavoriteItemsPo;
import com.qq.qqbuy.favorite.po.FavoriteShopDetailPo;
import com.qq.qqbuy.favorite.po.FavoriteShopDetails;
import com.qq.qqbuy.favorite.po.FavoriteUpdatePo;
import com.qq.qqbuy.item.util.ItemUtil;
import com.qq.qqbuy.thirdparty.idl.item.ItemClient;

/**
 * 
 * @ClassName: ApiFavAction
 * 
 * @Description: 收藏夹相关api
 * @author wendyhu
 * @date 2012-12-27 下午04:05:21
 */
public class ApiFavAction extends AuthRequiredAction {
	
	private BiBiz biBiz;
	private FavoriteBiz favBiz;
	private DiscoverBiz discoverBiz;
	/**
	 * 商品id，以逗号","分隔多个商品
	 * atti:喜欢不喜欢, 取值"like","unlike"
	 */
	private String items = "", ids, atti, daps="";
	private long shopId = 0; 
	private int pageNo = 0, pageSize = 5;
	private String jsonStr = "{}";
	private JsonOutput out = new JsonOutput();
	Gson gson = new GsonBuilder().serializeNulls().create();
	
	/**
	 * 添加商品收藏
	 * @return
	 */
	public String addFavItems() {
		
		if(!checkLoginAndGetSkey())	{
    		throw new NotLoginException();
    	}
		Log.run.info("addFavItems#getChannel:" + getChannel());
		String pprdP = null;
		try {
			pprdP = new ChannelParam(getChannel()).getPprdP();
		} catch (Exception e) {
			Log.run.error("addFavItems#getChannel:" + e.getMessage());
		}
		
		//先判断卖家是否是当前QQ。如果有一个是自己家的商品则提示不能收藏
		String[] item= items.split(",");
		boolean issamneqq =true;
		long nowqq =getQq();
		for(int i =0 ;i<item.length;i++){
			if(ItemUtil.getSellerUin(item[i])==nowqq){
				issamneqq=false;
				break;
			}
		}
		
			FavoriteUpdatePo po = favBiz.addFavItemList(items.split(","), getQq(), getHttpHeadIp(), getSk(), pprdP);
			out.setRetCode(po.errCode).setData(gson.toJsonTree(po));
	   if(!issamneqq) {
			Log.run.info("不能收藏自己商品issamneqq++----------------------issamneqq"+issamneqq+"ErrConstant:"+ErrConstant.ERRCODE_FAV_ITEM);
			out.setRetCode(ErrConstant.ERRCODE_FAV_ITEM).setMsg("不能收藏自己的商品");
		}
		
		return doPrint(out.toJsonStr());
	}
	
	/**
	 * 
	 * @Title: delFavItems
	 * @Description: 删除收藏
	 * @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	public String delFavItems() {
		jsonStr = "{}";
		if (checkLoginAndGetSkey()) {
			if (getQq() > 10000 && StringUtils.isNotBlank(items)) {
				JsonConfig jsoncfg = new JsonConfig();
				String[] excludes = { "size", "empty" };
				jsoncfg.setExcludes(excludes);
				String[] itemArray = items.split(",");
				if (itemArray != null) {
					Vector<String> addItemIdList = new Vector<String>(
							itemArray.length);
					for (String item : itemArray) {
						addItemIdList.add(item);
					}
					CommonPo ret = favBiz.delFavItemList(getQq(), getMk(),
							getSk(), addItemIdList);
					if (ret != null && ret.errCode == 0) {
						Map<String,String[]> map = new HashMap<String,String[]>();
						map.put("items", itemArray);
						jsonStr = JSONSerializer.toJSON(map, jsoncfg)
						.toString();
					}						
					else if (ret != null)
						setErrCodeAndMsg((int) ret.errCode, ret.msg);
				}
			} else {
				setErrCodeAndMsg(ErrConstant.ERRCODE_INVALID_PARAMETER,
						"参数校验不合法");
			}
		} else {
			setErrCodeAndMsg(ErrConstant.ERRCODE_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
		}
		return SUCCESS;
	}

	/**
	 * 
	 * @Title: 查询FavItems
	 * @Description: 增加
	 * @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	public String getFavItems() {
		jsonStr = "{}";
		if (checkLoginAndGetSkey()) {
			if (getQq() > 10000) {
				JsonConfig jsoncfg = new JsonConfig();
				String[] excludes = { "size", "empty" };
				jsoncfg.setExcludes(excludes);
				FavoriteItemsPo ret = favBiz.getFavItemList(getQq(), getMk(),
						getSk(), pageNo, pageSize);
				if (ret != null && ret.errCode == 0)
					jsonStr = JSONSerializer.toJSON(ret, jsoncfg).toString();
				else if (ret != null)
					setErrCodeAndMsg((int) ret.errCode, ret.msg);
			} else {
				setErrCodeAndMsg(ErrConstant.ERRCODE_INVALID_PARAMETER,
						"参数校验不合法");
			}
		} else {
			setErrCodeAndMsg(ErrConstant.ERRCODE_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
		}
		return SUCCESS;
	}
	
	/**
	 * 添加店铺收藏
	 * @return
	 */
	public String addFavShop()	{
		if(!checkLoginAndGetSkey())	{
    		throw new NotLoginException();
    	}
		Log.run.info("addFavItems#getChannel:" + getChannel());
		String pprdP = null;
		try {
			pprdP = new ChannelParam(getChannel()).getPprdP();
		} catch (Exception e) {
			Log.run.error("addFavItems#getChannel:" + e.getMessage());
		}
		CommonPo po = favBiz.addShop2Fav(getQq(), getMk(), getSk(), shopId, pprdP);
		out.setRetCode(po.errCode).setData(gson.toJsonTree(po));
		return doPrint(out);
	}
	
	/**
	 * 
	 * @Title: addFavShop
	 * @Description: 查询某个店铺是否已经收藏了
	 * @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	public String getFavShopInfo() {
		jsonStr = "{}";
		if (checkLoginAndGetSkey()) {
			if (getQq() > 10000 && shopId > 10000) {
				JsonConfig jsoncfg = new JsonConfig();
				String[] excludes = { "size", "empty" };
				jsoncfg.setExcludes(excludes);
				FavoriteShopDetailPo ret = favBiz.getShopIdFavInfo(getQq(),
						getMk(), getSk(), shopId);
				if (ret != null && ret.errCode == 0)
					jsonStr = JSONSerializer.toJSON(ret, jsoncfg).toString();
				else if (ret != null)
					setErrCodeAndMsg((int) ret.errCode, ret.msg);
			} else {
				setErrCodeAndMsg(ErrConstant.ERRCODE_INVALID_PARAMETER,
						"参数校验不合法");
			}
		} else {
			setErrCodeAndMsg(ErrConstant.ERRCODE_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
		}
		return SUCCESS;
	}

	/**
	 * 
	 * @Title: delFavItems
	 * @Description: 删除收藏
	 * @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	public String delFavShops() {
		jsonStr = "{}";
		if (checkLoginAndGetSkey()) {
			if (getQq() > 10000 && StringUtils.isNotBlank(items)) {
				JsonConfig jsoncfg = new JsonConfig();
				String[] excludes = { "size", "empty" };
				jsoncfg.setExcludes(excludes);
				String[] itemArray = items.split(",");
				if (itemArray != null) {
					Vector<Long> addItemIdList = new Vector<Long>(
							itemArray.length);
					for (String item : itemArray) {
						addItemIdList.add(StringUtil.toLong(item, 0));
					}
					CommonPo ret = favBiz.delShopItemList(getQq(), getMk(),
							getSk(), addItemIdList);
					if (ret != null && ret.errCode == 0)
					jsonStr = JSONSerializer.toJSON(ret, jsoncfg)
							.toString();
					else if (ret != null)
						setErrCodeAndMsg((int) ret.errCode, ret.msg);
				}
			} else {
				setErrCodeAndMsg(ErrConstant.ERRCODE_INVALID_PARAMETER,
						"参数校验不合法");
			}
		} else {
			setErrCodeAndMsg(ErrConstant.ERRCODE_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
		}
		return SUCCESS;
	}

	/**
	 * 
	 * @Title: 查询用户的店铺收藏夹
	 * @Description: 增加
	 * @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	public String getFavShops() {
		jsonStr = "{}";
		if (checkLoginAndGetSkey()) {
			if (getQq() > 10000) {
				JsonConfig jsoncfg = new JsonConfig();
				String[] excludes = { "size", "empty" };
				jsoncfg.setExcludes(excludes);
				FavoriteShopDetails ret = favBiz.getFavShopList(getQq(),
						getMk(), getSk(), pageNo, pageSize);
				if (ret != null && ret.errCode == 0)
					jsonStr = JSONSerializer.toJSON(ret, jsoncfg).toString();
				else if (ret != null)
					setErrCodeAndMsg((int) ret.errCode, ret.msg);
			} else {
				setErrCodeAndMsg(ErrConstant.ERRCODE_INVALID_PARAMETER,
						"参数校验不合法");
			}
		} else {
			setErrCodeAndMsg(ErrConstant.ERRCODE_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
		}
		return SUCCESS;
	}
	
	
	
	
	/**
	 * @title 我喜欢的店铺
	 * 返回店铺推荐内容（店铺收藏首先，否则取BI的）。   
	 * @Description: 增加
	 * @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	public String getMyFavShops() {
//		Log.run.debug("ApiFavAction.getMyFavShops()");
		JsonObject object = new JsonObject();
		JsonArray favShop = new JsonArray();
		try {
			if (checkLoginAndGetSkey()) {
				if (getQq() > 10000) {
					FavoriteShopDetails ret = favBiz.getFavShopList(getQq(), getMk(), getSk(), 0, 40);//pagesize最大40
					List<FavoriteShopDetailPo> items2 = ret.getItems();
					
					Log.run.debug("qq:" + getQq() + "  getFavShopListsize:" + items2.size());
					for (FavoriteShopDetailPo favoriteShopDetailPo : items2) {
						favShop.add(new JsonPrimitive(favoriteShopDetailPo.getShopId()));
					}
					object.add("sub", favShop);
				} else {
					out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
					out.setMsg("参数校验不合法");
					return doPrint(out.setData(object));
				}
			}
		} catch (Exception e) {
			Log.run.error("ApiFavAction#getMyFavShops e.getMessage:" + e.getMessage(), e);
		} finally{
			//如果没有数据（可能为收藏店铺，可能没有登录），则取BI推荐的数据
			if(favShop.size() == 0){
				long wid = getWidFromToken(getAppToken());
				//数据上报开始
				if(StringUtil.isNotBlank(daps)){
					for(String s : daps.split(","))	{
						try {
							Dap dap = Dap.fromDap62(s);
							long lCatId = ItemClient.fetchItemInfo(dap.getShopidOrItemcode())
									.getItemPo().getOItemBase().getLeafClassId();
							ExposureMsg msg = ExposureMsg.buildItemMsg(s, 0L, 0L, lCatId,	//TODO 怎么取三级类目ID
									wid, getHttpHeadIp(), getMk(), getMt(), getLongitude(), getLatitude(), "");
							biBiz.uploadMsg(msg);
						} catch (Exception e) {
							Log.run.error("数据上报失败。"+e.getMessage(), e);
						}
					}
				}
				//数据上报结束
				
				try {
					object = this.discoverBiz.recommendMyFavShop(wid, getHttpHeadIp(), 10, getMk()) ;
//					Log.run.info("读取BI数据,获取条数："+favShop.size());
//					object.add("sub", favShop);
				} catch (Exception e) {
					Log.run.error("从BI获取我喜欢的店铺出差！"+e.getMessage(), e);
				}
			}
		}
		
		return doPrint(out.setData(object));
	}
	
	
	public long getShopId() {
		return shopId;
	}

	public void setShopId(long shopId) {
		this.shopId = shopId;
	}

	public void setFavBiz(FavoriteBiz favBiz) {
		this.favBiz = favBiz;
	}

	public String getItems() {
		return items;
	}

	public void setItems(String items) {
		this.items = items;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getJsonStr() {
		return jsonStr;
	}

	public void setJsonStr(String jsonStr) {
		this.jsonStr = jsonStr;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getAtti() {
		return atti;
	}

	public void setAtti(String atti) {
		this.atti = atti;
	}

	public DiscoverBiz getDiscoverBiz() {
		return discoverBiz;
	}

	public void setDiscoverBiz(DiscoverBiz discoverBiz) {
		this.discoverBiz = discoverBiz;
	}

	public FavoriteBiz getFavBiz() {
		return favBiz;
	}

	public BiBiz getBiBiz() {
		return biBiz;
	}

	public void setBiBiz(BiBiz biBiz) {
		this.biBiz = biBiz;
	}

	public String getDaps() {
		return daps;
	}

	public void setDaps(String daps) {
		this.daps = daps;
	}

}
