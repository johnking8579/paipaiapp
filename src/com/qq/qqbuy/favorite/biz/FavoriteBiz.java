package com.qq.qqbuy.favorite.biz;

import java.util.List;
import java.util.Vector;

import com.qq.qqbuy.common.po.CommonPo;
import com.qq.qqbuy.favorite.po.FavoriteItemPo;
import com.qq.qqbuy.favorite.po.FavoriteItemsPo;
import com.qq.qqbuy.favorite.po.FavoriteShopDetailPo;
import com.qq.qqbuy.favorite.po.FavoriteShopDetails;
import com.qq.qqbuy.favorite.po.FavoriteUpdatePo;

/**
 * 
 * @ClassName: FavoriteBiz
 * 
 * @Description: 收藏夹biz
 * @author wendyhu
 * @date 2012-12-27 上午11:25:38
 */
public interface FavoriteBiz
{

    /**
     * 
     * @Title: addFavItemList
     * 
     * @Description: 添加商品到收藏夹
     * 
     * @param uin
     *            操作者qq号码
     * @param sk
     *            操作者登陆态
     * @param addItemIdList
     *            商品列表
     * 
     * @return 设定文件
     * @return FavoriteUpdatePo 返回类型
     * @throws
     */
    FavoriteUpdatePo addFavItemList(String[] itemcodes, long uin, String clientIp,
			String sk, String reserve);

    /**
     * 
     * @Title: delFavItemList
     * 
     * @Description: 删除收藏夹里的商品
     * @param uin
     *            操作者qq号码
     * @param mk
     *            机器码
     * @param sk
     *            操作者登陆态
     * @return 设定文件
     * @return FavoriteUpdatePo 返回类型
     * @throws
     */
    public CommonPo delFavItemList(long uin, String mk, String sk,
            Vector<String> delItemIdList);

    /**
     * 
     * @Title: delFavItemList
     * 
     * @Description: 查询收藏夹里的商品
     * @param uin
     *            操作者qq号码
     * @param mk
     *            机器码
     * @param sk
     *            操作者登陆态
     * @param startPage
     *            起始页
     * @param pageSize
     *            每页显示的大小
     * 
     * @return 设定文件
     * @return FavoriteUpdatePo 返回类型
     * @throws
     */
    public FavoriteItemsPo getFavItemList(long uin, String mk, String sk,
            int startPage, int pageSize);
    
    /**
     * 
     * @Title: addShop2Fav
     * @Description: 添加店铺到收藏夹
     * @param uin
     *            操作者qq号码
     * @param mk
     *            机器码
     * @param sk
     *            操作者登陆态
     * @param shopId
     *            被收藏者的店铺id
     * @return 设定文件
     * @return CommonPo 返回类型
     * @throws
     */
    public CommonPo addShop2Fav(long uin, String mk, String sk, long shopId, String reserve);

    /**
     * 
     * @Title: getShopIdFavInfo
     * @Description: 查询某个店铺是被收藏的信息
     * @param uin
     *            操作者qq号码
     * @param mk
     *            机器码
     * @param sk
     *            操作者登陆态
     * @param shopId
     *            被收藏者的店铺id
     * @return FavoriteShopDetailPo 返回类型
     * @throws
     */
    public FavoriteShopDetailPo getShopIdFavInfo(long uin, String mk,
            String sk, long shopId);
    
    
    /**
     * 
     * @Title: delFavItemList
     * 
     * @Description: 删除收藏夹里的商品
     * @param uin
     *            操作者qq号码
     * @param mk
     *            机器码
     * @param sk
     *            操作者登陆态
     * @return 设定文件
     * @return FavoriteUpdatePo 返回类型
     * @throws
     */
    public CommonPo delShopItemList(long uin, String mk, String sk,
            Vector<Long> delShopIdList);

    /**
     * 
     * @Title: delFavItemList
     * 
     * @Description: 查询收藏夹里的店铺
     * @param uin
     *            操作者qq号码
     * @param mk
     *            机器码
     * @param sk
     *            操作者登陆态
     * @param startPage
     *            起始页
     * @param pageSize
     *            每页显示的大小
     * 
     * @return 设定文件
     * @return FavoriteUpdatePo 返回类型
     * @throws
     */
    public FavoriteShopDetails getFavShopList(long uin, String mk, String sk,
            int startPage, int pageSize);

	boolean isShopInFav(long uin, String mk, String sk, long shopId);

	List<FavoriteItemPo> getAllFavItem(long uin, String mk, String sk);

}
