package com.qq.qqbuy.wuliu.biz;

import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.wuliu.po.LogisticLogInfo;

public interface PPWuliuBiz {

//    public WuliuLogInfo getWuliuLog(String dealId) throws BusinessException;
    
    //使用QQ速递接口开发
//    public LogisticLogInfo getWuliuLogEx(long buyerUin, String dealId) throws BusinessException;

    public LogisticLogInfo getWuliuLogEx(long buyerUin, String dealId, short historyFlag) throws BusinessException;
}
