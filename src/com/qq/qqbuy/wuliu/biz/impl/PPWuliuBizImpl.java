package com.qq.qqbuy.wuliu.biz.impl;


import java.util.Collections;
import java.util.Vector;

import net.sf.json.JSONException;

import com.paipai.util.string.StringUtil;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.thirdparty.idl.dealpc.PcDealQueryClient;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.GetDealInfoList2Resp;
import com.qq.qqbuy.thirdparty.idl.logistics.protocol.RetrieveTrackInfoResp;
import com.qq.qqbuy.thirdparty.idl.logistics.protocol.TrackInfoBo;
import com.qq.qqbuy.thirdparty.idl.logistics.protocol.TrackStep;
import com.qq.qqbuy.wuliu.biz.PPWuliuBiz;
import com.qq.qqbuy.wuliu.po.LogisticCompanyPo;
import com.qq.qqbuy.wuliu.po.LogisticLog;
import com.qq.qqbuy.wuliu.po.LogisticLogInfo;
import com.qq.qqbuy.wuliu.po.TraceInfo;
import com.qq.qqbuy.wuliu.po.WuliuLog;
import com.qq.qqbuy.wuliu.util.LogisticCompanyManager;
import com.qq.qqbuy.wuliu.util.WuliClient;

public class PPWuliuBizImpl implements PPWuliuBiz {
	
	private static long MILLISECOND_OF_ONEDAY = 24*60*60*1000;

//    @Override
//    public WuliuLogInfo getWuliuLog(String dealId) throws BusinessException {
//
//        WuliuLogInfo wuliuLogInfo = new WuliuLogInfo();
//        SysGetDealInfoResp dealResp = DealQueryClient.getDealInfo(dealId);
//        if (dealResp == null){
//            throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "IDL STUB调用失败");
//        }
//        if (dealResp.result != 0){
//            throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "IDL调用远端失败");
//        }
//        
//        CDealInfo dealInfo = dealResp.getODealInfo();
//        CShippingInfo cShippingInfo = dealInfo.getShippingInfo();
//        //没有物流信息
//        if (cShippingInfo == null
//                || StringUtils.isBlank(cShippingInfo.getWuliuCompany())
//                || StringUtils.isBlank(cShippingInfo.getWuliuCode()))
//        {
//            return wuliuLogInfo;
//        }
//        
//        String companyName = cShippingInfo.getWuliuCompany();
//        String wuliuCode = cShippingInfo.getWuliuCode();
//        
//        wuliuLogInfo.setCompanyName(companyName);
//        wuliuLogInfo.setWuliuCode(wuliuCode);
//        
//        Vector<WuliuLog> dealLogList = new Vector<WuliuLog>();
//        SysGetWuliuDealLogListResp resp = WuliuClient.getWuliuLogList(wuliuCode, companyName);
//        Vector<DealLogPo> logList = resp.getDealLogList();
//        for (DealLogPo dealLogPo : logList)
//        {
//            WuliuLog wuliuLog = new WuliuLog();
//            wuliuLog.setDealTime(dealLogPo.getCreateTime() * 1000);
//            wuliuLog.setWlStateDesc(dealLogPo.getWlStateDesc());
//
//            dealLogList.add(wuliuLog);
//        }
//        // 由于从接口获取的处理日志时间顺序不确定。有可能是顺序，也有可能是倒序的,这里需要做处理
//        sortDealLog(dealLogList);
//        
//        wuliuLogInfo.setWuliuLog(dealLogList);
//        
//        return wuliuLogInfo;
//        
//        
//    }
    
    
    /**
     * 将物流跟踪列表按时间排序
     * 
     * @param dealLogList
     *            待排序的物流跟踪列表
     */
    private void sortDealLog(Vector<WuliuLog> dealLogList)
    {

        if (dealLogList.size() > 0)
        {
            // 如果最后一项的时间小于一天，则不需倒序
            if (dealLogList.lastElement().getDealTime() < MILLISECOND_OF_ONEDAY)
            {
                return;
            }
            // 如果第一项的时间小于一天，则需倒序
            else if (dealLogList.firstElement().getDealTime() < MILLISECOND_OF_ONEDAY)
            {
                Collections.reverse(dealLogList);
                return;
            }
            // 如果第一项的时间 晚于 最后一项的时间，则倒序
            else if (dealLogList.firstElement().getDealTime() > dealLogList
                    .lastElement().getDealTime())
            {
                Collections.reverse(dealLogList);
                return;
            }
        }
    }
    
//    @Override
//    public LogisticLogInfo getWuliuLogEx(long buyerUin, String dealId) throws BusinessException
//    {
//    	LogisticLogInfo logisticLogInfo = new LogisticLogInfo();
//    	LogisticLog logisticLog = new LogisticLog();
//		logisticLogInfo.setData(logisticLog);
//    	GetDealInfoList2Resp res = PcDealQueryClient.queryDealWithWuli(dealId);
//    	if(res == null || res.getResult()!= 0  || res.getODealInfoList() == null 
//    			|| res.getODealInfoList().size()<=0 )
//    	{
//    		throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "IDL调用远端失败");
//    	}
//    	
//    	//获取PC侧订单查询中返回的物流信息
//    	com.qq.qqbuy.thirdparty.idl.deelpc.protocol.CDealInfo dealInfo = res.getODealInfoList().get(0);
//    	if(dealInfo.getBuyerUin() != buyerUin)
//    	{
//    		throw BusinessException.createInstance(BusinessErrorType.INVALID_DEAL_NOTEXIST, "订单不存在");
//    	}
//    	com.qq.qqbuy.thirdparty.idl.deelpc.protocol.CShippingInfo shippingInfo = dealInfo.getShippingInfo();
//    	if(shippingInfo == null )
//    	{
//    		throw BusinessException.createInstance(BusinessErrorType.WULIU_DEAL_NOT_HAS_WULIUINFO, "订单不包括物流信息");
//    	}
//    	//获取物流公司名称,根据名称获取到物流公司的ID；
//    	String wuliuCompanyName = shippingInfo.getWuliuCompany();
//    	String deliverId =  shippingInfo.getWuliuCode();
//    	
//    	logisticLog.setCompanyName(wuliuCompanyName);
//    	logisticLog.setDeliverId(deliverId);
//    	
//       	if(StringUtil.isEmpty(wuliuCompanyName) || StringUtil.isEmpty(deliverId))
//    	{
//       		return logisticLogInfo;
//    	}
//       	
//    	LogisticCompanyPo logisticCompanyPo = LogisticCompanyManager.getInstance().getCompanyId(wuliuCompanyName);
//    	
//      	if(StringUtil.isEmpty(companyId)  )
//    	{
//      		return logisticLogInfo;
//    	}
//
//      	String queryUrl = WuliuUtils.generateLogisticsQueryUrl(companyId, deliverId, 0);
//   	
//		String logisjson =  HttpUtil.get(queryUrl,"sudi.qq.com", 1000,1000, BizConstant.GBK_CHARSET,  true);
//		
//		if(StringUtil.isEmpty(logisjson))
//		{
//			return logisticLogInfo;
//		}
//		
//		int indexBegin = logisjson.indexOf("{\"retCode\":0");
//		int indexEnd = logisjson.indexOf(",\"supportQuery\"");
//		
//		if(indexBegin == -1 || indexEnd == -1)
//		{
//			return logisticLogInfo;
//		}
//
//		logisjson = logisjson.substring(indexBegin, indexEnd);
//		
//		logisjson = logisjson + "}}";
//			
//		LogisticLogInfo comp = (LogisticLogInfo) JSONUtil.json2pojo(logisjson,LogisticLogInfo.class);
//		comp.setRetCode(0);
//
//    	return  comp;
//    }
//    
//    @Override
//    public LogisticLogInfo getWuliuLogEx(long buyerUin, String dealId, short historyFlag) throws BusinessException
//    {
//        LogisticLogInfo logisticLogInfo = new LogisticLogInfo();
//        LogisticLog logisticLog = new LogisticLog();
//        logisticLogInfo.setData(logisticLog);
//        GetDealInfoList2Resp res = PcDealQueryClient.queryDealWithWuli(dealId,historyFlag);
//        if(res == null || res.getResult()!= 0  || res.getODealInfoList() == null 
//                || res.getODealInfoList().size()<=0 )
//        {
//            throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "IDL调用远端失败");
//        }
//        
//        //获取PC侧订单查询中返回的物流信息
//        com.qq.qqbuy.thirdparty.idl.deelpc.protocol.CDealInfo dealInfo = res.getODealInfoList().get(0);
//        if(dealInfo.getBuyerUin() != buyerUin)
//        {
//            throw BusinessException.createInstance(BusinessErrorType.INVALID_DEAL_NOTEXIST, "订单不存在");
//        }
//        com.qq.qqbuy.thirdparty.idl.deelpc.protocol.CShippingInfo shippingInfo = dealInfo.getShippingInfo();
//        if(shippingInfo == null )
//        {
//            throw BusinessException.createInstance(BusinessErrorType.WULIU_DEAL_NOT_HAS_WULIUINFO, "订单不包括物流信息");
//        }
//        //获取物流公司名称,根据名称获取到物流公司的ID；
//        Log.run.info("WuliuCompany name is : " + shippingInfo.getWuliuCompany()+";WuliuCompany id is : " + shippingInfo.getWuliuCompanyId());
//        String wuliuCompanyName = shippingInfo.getWuliuCompany();
//        String deliverId =  shippingInfo.getWuliuCode();
//        
//        logisticLog.setCompanyName(wuliuCompanyName);
//        logisticLog.setDeliverId(deliverId);
//        
//        if(StringUtil.isEmpty(wuliuCompanyName) || StringUtil.isEmpty(deliverId))
//        {
//            return logisticLogInfo;
//        }
//        
//        String companyId = LogisticCompanyManager.getInstance().getCompanyId(wuliuCompanyName);
//        
//        if(StringUtil.isEmpty(companyId)  )
//        {
//            return logisticLogInfo;
//        }
//
//        String queryUrl = WuliuUtils.generateLogisticsQueryUrl(companyId, deliverId, 0);
//    
//        String logisjson =  HttpUtil.get(queryUrl,"sudi.qq.com", 1000,1000, BizConstant.GBK_CHARSET,  true);
//        
//        if(StringUtil.isEmpty(logisjson))
//        {
//            return logisticLogInfo;
//        }
//        
//        int indexBegin = logisjson.indexOf("{\"retCode\":0");
//        int indexEnd = logisjson.indexOf(",\"supportQuery\"");
//        
//        if(indexBegin == -1 || indexEnd == -1)
//        {
//            return logisticLogInfo;
//        }
//
//        logisjson = logisjson.substring(indexBegin, indexEnd);
//        
//        logisjson = logisjson + "}}";
//            
//        LogisticLogInfo comp = (LogisticLogInfo) JSONUtil.json2pojo(logisjson,LogisticLogInfo.class);
//        comp.setRetCode(0);
//
//        return  comp;
//    }
    
    
    @Override
    public LogisticLogInfo getWuliuLogEx(long buyerUin, String dealId, short historyFlag) throws BusinessException
    {
        LogisticLogInfo logisticLogInfo = new LogisticLogInfo();
        LogisticLog logisticLog = new LogisticLog();
        logisticLogInfo.setData(logisticLog);
        GetDealInfoList2Resp res = PcDealQueryClient.queryDealWithWuli(dealId,historyFlag);
        if(res == null || res.getResult()!= 0  || res.getODealInfoList() == null 
                || res.getODealInfoList().size()<=0 )
        {
            throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "IDL调用远端失败");
        }
        
        //获取PC侧订单查询中返回的物流信息
        com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CDealInfo dealInfo = res.getODealInfoList().get(0);
        if(dealInfo.getBuyerUin() != buyerUin)
        {
            throw BusinessException.createInstance(BusinessErrorType.INVALID_DEAL_NOTEXIST, "订单不存在");
        }
        com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CShippingInfo shippingInfo = dealInfo.getShippingInfo();
        if(shippingInfo == null )
        {
            throw BusinessException.createInstance(BusinessErrorType.WULIU_DEAL_NOT_HAS_WULIUINFO, "订单不包括物流信息");
        }
        //获取物流公司名称,根据名称获取到物流公司的ID；
        Log.run.info("WuliuCompany name is : " + shippingInfo.getWuliuCompany()+";WuliuCompany id is : " + shippingInfo.getWuliuCompanyId());
        String wuliuCompanyName = shippingInfo.getWuliuCompany();
        String deliverId =  shippingInfo.getWuliuCode();
        
        logisticLog.setCompanyName(wuliuCompanyName);
        logisticLog.setDeliverId(deliverId);
        
        if(StringUtil.isEmpty(wuliuCompanyName) || StringUtil.isEmpty(deliverId))
        {
            return logisticLogInfo;
        }
        
        LogisticCompanyPo logisticCompanyPo = LogisticCompanyManager.getInstance().getCompanyId(wuliuCompanyName);
        
        if(null == logisticCompanyPo)
        {
            return logisticLogInfo;
        }
        
        try {
			RetrieveTrackInfoResp resp = WuliClient.getLogisticLogInfo(logisticCompanyPo.getId(), deliverId);
			TrackInfoBo traceInfoDo = resp.getTraceInfoDo();
			Vector<TrackStep> steps = traceInfoDo.getSteps();
			
			logisticLog.setCompanyId(traceInfoDo.getCompanyCode());
			logisticLog.setCompanyPhone(logisticCompanyPo.getContact());
			logisticLog.setCompanyUrl(logisticCompanyPo.getUrl());
			
			TraceInfo[] traceInfo = new TraceInfo[steps.size()];
			for (int i = 0 ; i < steps.size() ; i ++) {
				TraceInfo info = new TraceInfo();
				info.setDesc(steps.get(i).getDesc());
				info.setTime(steps.get(i).getEventTime());
				traceInfo[i] = info;
			}
			logisticLog.setTraceInfos(traceInfo);
		} catch (JSONException e) {
			Log.run.error(e.getMessage(), e);
		}
        return  logisticLogInfo;
    }

}
