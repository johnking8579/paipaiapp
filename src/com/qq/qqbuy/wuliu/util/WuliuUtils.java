package com.qq.qqbuy.wuliu.util;



import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.wuliu.po.LogisticCompanyPo;

/**
 * 物流详情工具类
 * 
 * @author minghe
 * 
 */
public class WuliuUtils {

//	private static String QQ_SUDI_LOGISTICS_URLPREF = "http://sudi.qq.com/logistics/index.php/querylogis/getlogisjson";

//	private static String SOURSE = "QQbuymobile";// 移动电商请求来源
//	private static String SOURSE_KEY = "W8nzRNZxnbX4M7umSyUxoBkqxAsNQJ9V";// 移动电商请求来源密钥

//	/**
//	 * 生成QQ速递物流查询接口访问签名
//	 * 
//	 * @param companyId
//	 *            物流商ID
//	 * @param deliverId
//	 *            运单号
//	 * @param detailNumber
//	 *            物流跟踪信息的条数（最新的几条），为0则显示全部
//	 * @return
//	 */
//	public static String generateSign(String companyId, String deliverId, int detailNumber) {
//		StringBuffer sb = new StringBuffer();
//		sb.append("companyId").append(companyId).append("deliverId").append(deliverId).append("detailNumber").append(detailNumber).append("source")
//				.append(SOURSE).append(SOURSE_KEY);
//		return MD5Util.encrypt(sb.toString());
//	}

//	/**
//	 * 生成QQ速递物流详情查询URL
//	 * 
//	 * @param companyId
//	 *            物流商ID
//	 * @param deliverId
//	 *            运单号
//	 * @param detailNumber
//	 *            物流跟踪信息的条数（最新的几条），为0则显示全部
//	 * @return
//	 */
//	public static String generateLogisticsQueryUrl(String companyId, String deliverId, int detailNumber) {
//		StringBuffer sbf = new StringBuffer();
//		sbf.append(QQ_SUDI_LOGISTICS_URLPREF);
//		sbf.append("?").append("deliverId=").append(deliverId);
//		sbf.append("&").append("companyId=").append(companyId);
//		sbf.append("&").append("detailNumber=").append(detailNumber);
//		sbf.append("&").append("source=").append(SOURSE);
//		sbf.append("&").append("sign=").append(generateSign(companyId, deliverId, detailNumber));
//		return sbf.toString();
//	}

//	/**
//	 * 从QQ速递获取物流公司列表
//	 * 
//	 * @return
//	 */
//	public static List<LogisticCompanyPo> getLogisticCompanyInfo() {
//		List<LogisticCompanyPo> list = new ArrayList<LogisticCompanyPo>();
//		String jsonStr = LogisticClient.getLogisticCompanyInfo();
//		if (StringUtils.isEmpty(jsonStr)) {
//			return null;
//		}
//		JSONObject obj = JSONObject.fromObject(jsonStr);
//		JSONArray companyListObj = obj.getJSONArray("data");
//		for (int i = 0; i < companyListObj.size(); i++) {
//			JSONObject jsonObject = companyListObj.getJSONObject(i);
//			LogisticCompanyPo comp = (LogisticCompanyPo) JSONObject.toBean(jsonObject, LogisticCompanyPo.class);
//			list.add(comp);
//		}
//		return list;
//	}

	
	private volatile static WuliuUtils wuliuUtils = null;
	private static List<LogisticCompanyPo> logisticCompanyPos = new ArrayList<LogisticCompanyPo>();
	
	/**
	 * 初始化加載文件中的物流公司
	 * 读取文件，加载物流公司
	 */
	private WuliuUtils(){
		Log.run.info("init_logisticCompanies。");
		SAXReader saxReader = new SAXReader();   
		try {   
			InputStream in = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("res/logisticCompanies.xml");
			Document document = saxReader.read(in);   
			Element employees=document.getRootElement();   
			for(Iterator i = employees.elementIterator(); i.hasNext();){
				Element employee = (Element) i.next();  
				
				LogisticCompanyPo logisticCompanyPo = new LogisticCompanyPo();
				logisticCompanyPo.setId(employee.element("id").getText());
				logisticCompanyPo.setCode(employee.element("code").getText());
				logisticCompanyPo.setContact(employee.element("contact").getText());
				logisticCompanyPo.setName(employee.element("name").getText());
				logisticCompanyPo.setPinyin(employee.element("pinyin").getText());
//				logisticCompanyPo.setSupportQuery(employee.element("id").getText());
				logisticCompanyPo.setUrl(employee.element("url").getText());
				logisticCompanyPo.setWanggouid(employee.element("wanggouid").getText());
				
				logisticCompanyPos.add(logisticCompanyPo);
			}  
		} catch (DocumentException e) {   
			Log.run.error(e.getMessage(), e);
		}
	}
	
	public static WuliuUtils getInstance(){
		//先检查实例是否存在，如果不存在才进入下面的同步块
        if(null == wuliuUtils){
            //同步块，线程安全的创建实例
            synchronized (WuliuUtils.class) {
                //再次检查实例是否存在，如果不存在才真正的创建实例
                if(null == wuliuUtils){
                	Log.run.info("实例化WuliuUtils");
                	wuliuUtils = new WuliuUtils();
                }
            }
        }
		return wuliuUtils;
	}
	
	/**
	 * 查询所有的物流公司列表
	 * @return
	 */
	public List<LogisticCompanyPo> getLogisticCompanyPos() {
		return logisticCompanyPos;
	}

}
