package com.qq.qqbuy.wuliu.util;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.logistics.protocol.RetrieveTrackInfoReq;
import com.qq.qqbuy.thirdparty.idl.logistics.protocol.RetrieveTrackInfoResp;

/**
 * 查询物流
 * @author liubenlong3
 *
 */
public class WuliClient  extends SupportIDLBaseClient {
	
	/**
	 * 获取物流详情
	 * 接口人：徐成宽，张亚博
	 * @param dealCode
	 * @return
	 */
	public static RetrieveTrackInfoResp getLogisticLogInfo(String companyCode, String deliverId) {
        RetrieveTrackInfoResp resp = new RetrieveTrackInfoResp();
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
        
        try {
	        RetrieveTrackInfoReq req = new RetrieveTrackInfoReq();
	        req.setDeliverId(deliverId);
	        req.setCompanyCode(companyCode);
	        req.setSource((short)10);//设置调用者来源，必填。新调用方请联系我们分配id号：用于区分不同平台数据来源0 - all全部平台适用1 - QQ网购2 -拍拍3 -速递4 -微信5 - soso6 - QQ团购7 - 高朋8 - QQ腾爱 - 拍拍无线10
            stub.invoke(req, resp);
        } catch (Exception e) {
            Log.run.error(e.getMessage(), e);
        } 
        
        return resp;
    }

}
