package com.qq.qqbuy.wuliu.util;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.qq.qqbuy.wuliu.po.LogisticCompanyPo;
import com.qq.qqbuy.wuliu.resource.LogisticCompanyResource;

/**
 * 物流公司管理类
 * @author winsonwu
 * @Created 2013-1-8 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class LogisticCompanyManager {

	private static long CACHE_TIME = 10 * 24 * 60 * 60; //缓存时间为10天
	private LogisticCompanyResource resource;
	private static LogisticCompanyManager eInstance = null;

	private LogisticCompanyManager() {
		resource = new LogisticCompanyResource();
		resource.setModificationCheckInterval(CACHE_TIME);
	}

	public static LogisticCompanyManager getInstance() {
		if (eInstance == null) {
			synchronized (LogisticCompanyManager.class) {
				if (eInstance == null) {
					eInstance = new LogisticCompanyManager();
				}
			}
		}
		return eInstance;
	}

	/**
	 * 根据物流商名称获取物流商Id
	 * @param name 物流商名称
	 * @return 当未获取到返回 ""
	 */
	@SuppressWarnings("unchecked")
	public LogisticCompanyPo getCompanyId(String name) {
		if (StringUtils.isEmpty(name)) {
			return null;
		}
		if (resource.requiresChecking()) {
			List<LogisticCompanyPo> companyPos = WuliuUtils.getInstance().getLogisticCompanyPos();
			if (companyPos != null) {
				resource.setData(companyPos);
				resource.touch();
			}
		}
		List<LogisticCompanyPo> logisticCompanyPos = (List<LogisticCompanyPo>) resource.getData();
		if(logisticCompanyPos == null){
			return null;
		}
		//首先根据companyName判断是否有映射
		for (LogisticCompanyPo logisticCompanyPo : logisticCompanyPos) {
			String companyName = logisticCompanyPo.getName();
			if (name.equals(companyName)) {//通过名称匹配
				return logisticCompanyPo;
			}
		}
		//companyName无法映射时候使用code和name互相映射
		for (LogisticCompanyPo logisticCompanyPo : logisticCompanyPos) {
			String code = logisticCompanyPo.getCode();
			if(code != null && !"".equals(code.trim())){
				String[] codes = code.split(";");
				for(String cod : codes){
					if(name.contains(cod) || cod.contains(name)){
						return logisticCompanyPo;
					}
				}
			}
		}
		//companyName无映射时使用拼音映射
		for (LogisticCompanyPo logisticCompanyPo : logisticCompanyPos) {
			String pinyin = logisticCompanyPo.getPinyin();
			if(name.equals(pinyin)){//通过拼音匹配
				return logisticCompanyPo;
			}
		}
		return null;
	}

}
