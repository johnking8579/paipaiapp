package com.qq.qqbuy.wuliu.resource;




public class LogisticCompanyResource {
	
	protected static final long MILLIS_PER_SECOND = 1000L;
	protected long modificationCheckInterval;
	protected long lastModified;
	protected long nextCheck;
	protected String name;
	protected String encoding;
	protected Object data;
	protected int type;
	
	public boolean requiresChecking() {
		if (modificationCheckInterval <= 0L)
			return false;
		else
			return System.currentTimeMillis() >= nextCheck;
	}

	public void touch() {
		nextCheck = System.currentTimeMillis() + MILLIS_PER_SECOND * modificationCheckInterval;
	}
	
	public boolean isSourceModified() {
		return false;
	}

	public long getModificationCheckInterval() {
		return modificationCheckInterval;
	}

	public void setModificationCheckInterval(long modificationCheckInterval) {
		this.modificationCheckInterval = modificationCheckInterval;
	}

	public long getLastModified() {
		return lastModified;
	}

	public void setLastModified(long lastModified) {
		this.lastModified = lastModified;
	}

	public long getNextCheck() {
		return nextCheck;
	}

	public void setNextCheck(long nextCheck) {
		this.nextCheck = nextCheck;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	
}
