package com.qq.qqbuy.wuliu.po;


/**
 * 物流公司po
 * @author minghe
 *
 */
public class LogisticCompanyPo {

	private String id; // 物流商Id
	private String wanggouid;//网购id，，暂时没用到
	private String name; // 物流商名称
	private String code;// 物流商编码--又名 公司关键字
	private String pinyin;// 物流商名称的拼音
	private String contact;// 物流商的联系电话
	private String url;// 物流商的网址
	private String supportQuery;// 是否支持查询，1：是，0：否
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getPinyin() {
		return pinyin;
	}
	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getSupportQuery() {
		return supportQuery;
	}
	public void setSupportQuery(String supportQuery) {
		this.supportQuery = supportQuery;
	}
	public String getWanggouid() {
		return wanggouid;
	}
	public void setWanggouid(String wanggouid) {
		this.wanggouid = wanggouid;
	}

}
