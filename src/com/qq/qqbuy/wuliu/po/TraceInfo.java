

package com.qq.qqbuy.wuliu.po;




/**
 *物流日志
 *
 *@date 2011-05-19 08:24::16
 *
 *@since version:1
*/
public class TraceInfo 
{
	
	 private String time ;
	 
	 private String desc ;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	 
}
