package com.qq.qqbuy.wuliu.po;

import java.util.List;
import java.util.Vector;

public class WuliuLogInfo {

    private String companyName;
    
    private String wuliuCode;
    
    private List<WuliuLog> wuliuLog = new Vector<WuliuLog>();

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getWuliuCode() {
        return wuliuCode;
    }

    public void setWuliuCode(String wuliuCode) {
        this.wuliuCode = wuliuCode;
    }

    public List<WuliuLog> getWuliuLog() {
        return wuliuLog;
    }

    public void setWuliuLog(List<WuliuLog> wuliuLog) {
        this.wuliuLog = wuliuLog;
    }
    
    
    
}
