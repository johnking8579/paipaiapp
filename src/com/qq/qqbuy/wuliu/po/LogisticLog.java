

package com.qq.qqbuy.wuliu.po;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;




/**
 *物流日志
 *
 *@date 2011-05-19 08:24::16
 *
 *@since version:1
*/
public class LogisticLog 
{

	 private String deliverId ;
	 
	 private String companyId ;

	 private String companyName ;
	 
	 private String companyUrl;
	 private String companyPhone;
	 
	 private TraceInfo[] traceInfos;
	 
	// private List<TraceInfo> traceInfos = new ArrayList<TraceInfo>();

	public String getDeliverId() {
		return deliverId;
	}

	public void setDeliverId(String deliverId) {
		this.deliverId = deliverId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyUrl() {
		return companyUrl;
	}

	public void setCompanyUrl(String companyUrl) {
		this.companyUrl = companyUrl;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public TraceInfo[] getTraceInfos() 
	{

		return traceInfos;
	}

	public void setTraceInfos(TraceInfo[] traceInfos) 
	{
		this.traceInfos = traceInfos;
		
		//将其逆序
		if(this.traceInfos!= null)
		{
			int len = this.traceInfos.length;
			
			for(int i=0, j= len-1; j>i ; i++ , j-- )
			{
				TraceInfo temp = traceInfos[i];
				this.traceInfos[i] = this.traceInfos[j];
				this.traceInfos[j] = temp;
				
			}
			
		}
	}
}
