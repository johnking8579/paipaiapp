package com.qq.qqbuy.wuliu.po;


public class LogisticLogInfo {

	//查询结果: 0: 成功，其他：失败
    private int retCode = -1;
    
    private LogisticLog data = new LogisticLog();

	public int getRetCode() {
		return retCode;
	}

	public void setRetCode(int retCode) {
		this.retCode = retCode;
	}

	public LogisticLog getData() {
		return data;
	}

	public void setData(LogisticLog data) {
		this.data = data;
	}
}
