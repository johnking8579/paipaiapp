package com.qq.qqbuy.shippingFee.biz;

import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.deal.po.ShipCalcVo;
import com.qq.qqbuy.item.po.wap2.ItemResponse;
import com.qq.qqbuy.shippingFee.po.CodShipPo;
import com.qq.qqbuy.shippingFee.po.SFTplDetailInfo;
import com.qq.qqbuy.shippingFee.po.ShipTempPo;
import java.util.List;

public interface SFBiz {

    public SFTplDetailInfo getSFTpl(long sellerUin, int innerId) throws BusinessException;
    
    public ShipTempPo calcShipfee4SingleBuy(long rid, ItemResponse item, long buyNum) throws BusinessException;
    
    public CodShipPo calcCodShipfee(long rid, long sellerUin, long cityId, long dealTotalFee, long shipCodId, long buyNum) throws BusinessException;

	public CodShipPo calcCodShipfeeV2(long rid, long sellerUin, long cityId,
			long dealTotalFee, long shipCodId, long buyNum)throws BusinessException;

	public CodShipPo calcCodShipfeeWithCgi(long rid, String ci, long dealTotalFee, long buyNum, long wid, String sk, int isMailFree) throws BusinessException;

    public List<ShipCalcVo> getTakeCheapShipCalcInfo(long wid, String sk, long adid, String itemCode, String attrStr, long buyNum)throws BusinessException;

}
