package com.qq.qqbuy.shippingFee.biz.impl;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.PaiPaiConfig;
import com.qq.qqbuy.common.constant.C2CDefineConstant;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.deal.po.ShipCalcVo;
import com.qq.qqbuy.deal.util.DealConstant;
import com.qq.qqbuy.item.po.wap2.ItemResponse;
import com.qq.qqbuy.shippingFee.biz.SFBiz;
import com.qq.qqbuy.shippingFee.po.CodShipPo;
import com.qq.qqbuy.shippingFee.po.Rule;
import com.qq.qqbuy.shippingFee.po.SFTplDetailInfo;
import com.qq.qqbuy.shippingFee.po.ShipTempPo;
import com.qq.qqbuy.shippingFee.util.ShipTemplateConstant;
import com.qq.qqbuy.shippingFee.util.ShipTemplateUtil;
import com.qq.qqbuy.thirdparty.idl.codFee.CODFeeClient;
import com.qq.qqbuy.thirdparty.idl.codFee.protocol.CODFee;
import com.qq.qqbuy.thirdparty.idl.codFee.protocol.GetTotalFeeResp;
import com.qq.qqbuy.thirdparty.idl.shippingFee.ShippingfeeClient;
import com.qq.qqbuy.thirdparty.idl.shippingFee.protocol.ApiCalcShipTemplateFeeResp;
import com.qq.qqbuy.thirdparty.idl.shippingFee.protocol.ApiShipInfo;
import com.qq.qqbuy.thirdparty.idl.shippingFeeV2.ShippingfeeV2Client;
import com.qq.qqbuy.thirdparty.idl.shippingFeeV2.protocol.GetTemplateDetailNoLoginResp;
import com.qq.qqbuy.thirdparty.idl.shippingFeeV2.protocol.TemplateDetailInfo;
import com.qq.qqbuy.thirdparty.idl.shippingFeeV2.protocol.TemplateSimpleInfo;
import com.qq.qqbuy.thirdparty.openapi.OpenApi;
import com.qq.qqbuy.thirdparty.openapi.OpenApiProxy;
import com.qq.qqbuy.thirdparty.openapi.po.GetFreightTemplateRequest;
import com.qq.qqbuy.thirdparty.openapi.po.GetFreightTemplateResponse;
import com.qq.qqbuy.thirdparty.openapi.po.GetFreightTemplateResponse.FreightRule;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;

public class SFBizImpl implements SFBiz {

	/**
	 * 获取货到付款服务费相关cgi接口
	 */
	protected static final String COD_CGI_URL = "http://auction.paipai.com/cgi-bin/auction_fixup_confirm/changeaddr";

	protected static final String TAKECHEAP_COD_CGI_URL = "http://b.paipai.com/mfixupconfirm/changeaddr";
	/**
	 * 获取货到付款服务费相关cgi接口HOST
	 */
	protected static final String COD_CGI_HOST = "auction.paipai.com";

	protected static final String TAKECHEAP_COD_CGI_HOST = "b.paipai.com";

    public SFTplDetailInfo getSFTpl(long sellerUin, int innerId) throws BusinessException{
        GetFreightTemplateRequest req = new GetFreightTemplateRequest();
        req.setSellerUin(sellerUin);
        req.setFreightId(innerId);
        OpenApiProxy proxy = OpenApi.getProxy();
        GetFreightTemplateResponse resp = proxy.getFreightTemplate(req);
        if (resp.common.clientErrorCode != 0) {
            throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "OpenApi客户端调用失败");
        }
        if (resp.common.serverErrorCode != 0) {
            throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "OpenApi调用远端失败");
        }
        
        SFTplDetailInfo info = new SFTplDetailInfo();
        info.setId(resp.freightId);
        info.setName(resp.name);
        for (FreightRule frule : resp.freightRuleList) {
            Rule rule = new Rule();
            rule.setRegion(Arrays.asList( frule.dest.split("、")));
            rule.setPriceEms(frule.priceEms);
            rule.setPriceEmsAdd(frule.priceEmsAdd);
            rule.setPriceExpress(frule.priceExpress);
            rule.setPriceExpressAdd(frule.priceExpressAdd);
            rule.setPriceNormal(frule.priceNormal);
            rule.setPriceNormalAdd(frule.priceNormalAdd);
            info.getRules().add(rule);
        }
        
        return info;
        
    }
    
    public ShipTempPo calcShipfee4SingleBuy(long rid, ItemResponse item, long buyNum) throws BusinessException {
		ShipTempPo po = new ShipTempPo();
		if (item != null) {
			long shipTptId = 0;
			long shipCodId = 0;
			// 在线支付逻辑
			int whoPayShipFee = (int)item.getPpItemInfo().sellerPayFreight;
			if (whoPayShipFee == C2CDefineConstant.TRANSPORT_SELLER_PAY) {  
				// 卖家承担运费;
				po.setSuppordPayOnline(true);
			} else if (whoPayShipFee == C2CDefineConstant.TRANSPORT_BUYER_PAY) {
				// 买家承担运费，并直接为商品设置运费，没有使用运费模版
				po.setEmsFee(item.getPpItemInfo().getEmsPrice());
				po.setNormalFee(item.getPpItemInfo().getMailPrice());
				po.setExpressFee(item.getPpItemInfo().getExpressPrice());
				po.setSuppordPayOnline(true);
			} else if (whoPayShipFee >= 10) {
				// 买家承担运费，并使用运费模版，whoPayShipFee的取值为运费模版ID
				shipTptId = whoPayShipFee;
			}
			
			// 根据商品是否支持货到付款标志位来决定是否拉取货到付款运费模版
			if (item.getPpItemInfo().isSupportCodForPP()) {
				shipCodId = item.getPpItemInfo().getCodShipTempId();
			}
			
			// 根据运费模版Id查询回运费信息(在线支付/货到付款运费模版一起查询)
			if (shipTptId > 0 || shipCodId > 0) {
				// 存在普通运费模版或货到付款运费模版
				ApiCalcShipTemplateFeeResp shipCalcResp = ShippingfeeClient.calcTemplateFee4SingleItem(item.getPpItemInfo().getSellerUin(), rid, shipTptId, shipCodId, item.getPpItemInfo().getCityId(), buyNum);
				if (shipCalcResp == null || shipCalcResp.getResult() != 0) {
					throw BusinessException.createInstance(ErrConstant.ERRCODE_DEAL_CALC_SHIPPING_FAIL, "计算运送方式失败");
				}
				Vector<ApiShipInfo> shipInfos = ShipTemplateUtil.getApiShipInfoList(shipCalcResp);
				if (shipInfos != null && shipInfos.size() > 0) {
					for (ApiShipInfo shipInfo : shipInfos) {
						if (shipTptId == shipInfo.getShipTptId()) {
							// 普通运费模版
							po.setEmsFee(shipInfo.getEmsFee());
							po.setExpressFee(shipInfo.getExpressFee());
							po.setNormalFee(shipInfo.getNormalFee());
							po.setSuppordPayOnline(true);
						} else if (shipCodId == shipInfo.getShipTptId()) {
							// 货到付款运费模版
							if (ShipTemplateConstant.SHIPPING_TMP_TYPE_COD_SELLER == shipInfo.getCodTptType()) {  //卖家承担服务费
								po.setCodFee(shipInfo.getNormalFee());
								po.setSuppordCod(true);
							} else {
								throw BusinessException.createInstance(ErrConstant.ERRCODE_SHIP_CODTMPID_NO_SUPPORT, "卖家需承担服务费");
							}
						}
					}
				}
			}
		}
		return po;
	}
    
	/**
	 * 计算商品货到付款运费，服务费
	 */
	public CodShipPo calcCodShipfee(long rid, long sellerUin, long cityId, long dealTotalFee, long shipCodId,
			long buyNum) throws BusinessException {
		CodShipPo po = new CodShipPo();

		ApiCalcShipTemplateFeeResp shipCalcResp = ShippingfeeClient.calcTemplateFee4SingleItem(sellerUin, rid, 0,
				shipCodId, cityId, buyNum);
		if (shipCalcResp == null || shipCalcResp.getResult() != 0) {
			throw BusinessException.createInstance(ErrConstant.ERRCODE_DEAL_CALC_SHIPPING_FAIL, "计算运送方式失败");
		}
		Vector<ApiShipInfo> shipInfos = ShipTemplateUtil.getApiShipInfoList(shipCalcResp);
		if (shipInfos != null && shipInfos.size() > 0) {
			for (ApiShipInfo shipInfo : shipInfos) {

				if (shipCodId == shipInfo.getShipTptId()) {
					po.setFee(shipInfo.getNormalFee());

					int innerCompanyId = 0;
					// 货到付款（宅寄送）
					if (((1 << (DealConstant.COD_SHIP_ZJS - 1)) & shipInfo.getProperty()) != 0) {
						innerCompanyId = DealConstant.COD_SHIP_ZJS;
						po.setMailType(DealConstant.MAIL_TYPE_COD_ZJS);
					}
					// 货到付款（顺风）
					if (((1 << (DealConstant.COD_SHIP_SF - 1)) & shipInfo.getProperty()) != 0) {
						innerCompanyId = DealConstant.COD_SHIP_SF;
						po.setMailType(DealConstant.MAIL_TYPE_COD_SF);
					}
					// 货到付款（优速）
					if (((1 << (DealConstant.COD_SHIP_YS - 1)) & shipInfo.getProperty()) != 0) {
						innerCompanyId = DealConstant.COD_SHIP_YS;
						po.setMailType(DealConstant.MAIL_TYPE_COD_YS);
					}
					// 货到付款（圆通）
					if (((1 << (DealConstant.COD_SHIP_YT - 1)) & shipInfo.getProperty()) != 0) {
						innerCompanyId = DealConstant.COD_SHIP_YT;
						po.setMailType(DealConstant.MAIL_TYPE_COD_YT);
					}

					int sellerPayCod = 0; // 0买家承担, 1卖家承担
					if (ShipTemplateConstant.SHIPPING_TMP_TYPE_COD_BUYER == shipInfo.getCodTptType()) {
						sellerPayCod = DealConstant.COD_BUYER_FEE;
						po.setCodShipFree(0);
					} else if (ShipTemplateConstant.SHIPPING_TMP_TYPE_COD_SELLER == shipInfo.getCodTptType()) {
						sellerPayCod = DealConstant.COD_SELLER_FEE;
						po.setCodShipFree(1);
					}
					Log.run.info("deal codFee : innerCompanyId= "+innerCompanyId+" --sellerPayCod:"+sellerPayCod
							+"--dealTotalFee:"+dealTotalFee+"--po.fee:"+po.getFee()+"--buyNum:"+buyNum);
					// 计算服务费
					GetTotalFeeResp codFeeResp = CODFeeClient.getCodFee(innerCompanyId, sellerPayCod,
							dealTotalFee + po.getFee() * buyNum);
					CODFee codFee = null;
					if (codFeeResp == null) {
						throw BusinessException.createInstance(ErrConstant.ERRCODE_SHIP_CALC_CODCOUNT_FAIL,
								"货到付款运费返回值空");
					}

					long codCountFee = 0;
					if (codFeeResp.getResult() == 0 && (codFee = codFeeResp.getRspData()) != null) {
						if (sellerPayCod == DealConstant.COD_BUYER_FEE) {
							codCountFee = dealTotalFee + po.getFee() + codFee.getRealTotalFee();
							codCountFee = (long) (Math.ceil(codCountFee / 100.0) * 100) - dealTotalFee - po.getFee();
						} else if (sellerPayCod == DealConstant.COD_SELLER_FEE) {
							codCountFee = dealTotalFee + po.getFee() - codFee.getDealAdjustFee();
							codCountFee = (long) (Math.floor(codCountFee / 100.0) * 100) - dealTotalFee - po.getFee();
						}
					} else {
						throw BusinessException.createInstance((int) codFeeResp.getResult(), "货到付款运费计算异常",
								codFeeResp.getResult(), codFeeResp.getErrMsg());
					}

					po.setCodCountFee(codCountFee);
				}
			}
		}
		return po;
	}
	
	/**
	 * 计算商品货到付款运费，服务费
	 */
	@Override
	public CodShipPo calcCodShipfeeV2(long rid, long sellerUin, long cityId, long dealTotalFee, long shipCodId,
			long buyNum) throws BusinessException {
		CodShipPo po = new CodShipPo();
		
		ApiCalcShipTemplateFeeResp shipCalcResp = ShippingfeeClient.calcTemplateFee4SingleItem(sellerUin, rid, 0,
				shipCodId, cityId, buyNum);
		if (shipCalcResp == null || shipCalcResp.getResult() != 0) {
			throw BusinessException.createInstance(ErrConstant.ERRCODE_DEAL_CALC_SHIPPING_FAIL, "计算运送方式失败");
		}
		long property = 0;
		GetTemplateDetailNoLoginResp shipResp = ShippingfeeV2Client
				.getShippingTemplateDetail(sellerUin, (short) shipCodId);
		if (shipResp != null && shipResp.getResult() == 0) {
			TemplateDetailInfo detail = null;
			TemplateSimpleInfo simple = null;
			if ((detail = shipResp.getDetailInfo()) != null
					&& (simple = detail.getSimple()) != null) {
				property = simple.getProperty();
			}
		}
		Vector<ApiShipInfo> shipInfos = ShipTemplateUtil.getApiShipInfoList(shipCalcResp);
		if (shipInfos != null && shipInfos.size() > 0) {
			for (ApiShipInfo shipInfo : shipInfos) {
				if (shipCodId == shipInfo.getShipTptId()) {
					po.setFee(shipInfo.getNormalFee());
					int innerCompanyId = 0;
					if ((property & DealConstant.COD_SHIP_ZJS) == DealConstant.COD_SHIP_ZJS) {
						innerCompanyId = DealConstant.COD_SHIP_ZJS;
						po.setMailType(DealConstant.MAIL_TYPE_COD_ZJS);
					} else if ((property & DealConstant.COD_SHIP_SF) == DealConstant.COD_SHIP_SF) {
						innerCompanyId = DealConstant.COD_SHIP_SF;
						po.setMailType(DealConstant.MAIL_TYPE_COD_SF);
					} else if (((1 << (DealConstant.COD_SHIP_YS - 1)) & property) != 0) {
						innerCompanyId = DealConstant.COD_SHIP_YS;
						po.setMailType(DealConstant.MAIL_TYPE_COD_YS);
					} else if (((1 << (DealConstant.COD_SHIP_YT - 1)) & property) != 0) {
						innerCompanyId = DealConstant.COD_SHIP_YT;
						po.setMailType(DealConstant.MAIL_TYPE_COD_YT);
					}
					int sellerPayCod = 0; // 0买家承担, 1卖家承担
					long shiftProperty = property >> 30;
					if (shiftProperty == 1) {
						sellerPayCod = DealConstant.COD_BUYER_FEE;
					} else if (shiftProperty == 2) {
						sellerPayCod = DealConstant.COD_SELLER_FEE;
					}
					Log.run.info("deal codFee : innerCompanyId= "+innerCompanyId+" --sellerPayCod:"+sellerPayCod
							+"--dealTotalFee:"+dealTotalFee+"--po.fee:"+po.getFee()+"--buyNum:"+buyNum);
					// 计算服务费
//					GetTotalFeeResp codFeeResp = CODFeeClient.getCodFee(innerCompanyId, sellerPayCod,
//							dealTotalFee + po.getFee() * buyNum);
					GetTotalFeeResp codFeeResp = CODFeeClient.getCodFee(innerCompanyId, sellerPayCod,
							dealTotalFee);
					CODFee codFee = null;
					if (codFeeResp == null) {
						throw BusinessException.createInstance(ErrConstant.ERRCODE_SHIP_CALC_CODCOUNT_FAIL,
								"货到付款运费返回值空");
					}
					long codCountFee = 0;
					if (codFeeResp.getResult() == 0 && (codFee = codFeeResp.getRspData()) != null) {
						if (sellerPayCod == DealConstant.COD_BUYER_FEE) {
							codCountFee = dealTotalFee + po.getFee() + codFee.getRealTotalFee();
							codCountFee = (long) (Math.ceil(codCountFee / 100.0) * 100) - dealTotalFee - po.getFee();
						} else if (sellerPayCod == DealConstant.COD_SELLER_FEE) {
							codCountFee = dealTotalFee + po.getFee() - codFee.getDealAdjustFee();
							codCountFee = (long) (Math.floor(codCountFee / 100.0) * 100) - dealTotalFee - po.getFee();
						}
					} else {
						throw BusinessException.createInstance((int) codFeeResp.getResult(), "货到付款运费计算异常",
								codFeeResp.getResult(), codFeeResp.getErrMsg());
					}
					po.setCodCountFee(codCountFee);
				}
			}
		}
		return po;
	}

	/**
	 * 通过cgi接口获取货到付款服务费
	 * @throws BusinessException 
	 */
	@Override
	public CodShipPo calcCodShipfeeWithCgi(long rid, String ci,long dealTotalFee, long buyNum, long wid, String sk,int isMailFree) throws BusinessException {
		CodShipPo po = new CodShipPo();
		try {
			po.setItemCode(ci);
			StringBuffer url = new StringBuffer();
			url.append(TAKECHEAP_COD_CGI_URL).append("?cid=").append(ci)//
			.append("&num=").append(buyNum).append("&addressid=").append(rid);
			int timeOut = 3000;
			String cookie = "wg_uin=" + wid + ";wg_skey=" + sk;   
			String result = HttpUtil.get(url.toString().replace(TAKECHEAP_COD_CGI_HOST,PaiPaiConfig.getPaipaiCommonIp()),TAKECHEAP_COD_CGI_HOST, timeOut, timeOut, "gbk", false,false,cookie,null);
			String head = "try{fixupConfirmCallBack(";
			String tail = ");}catch(e){}";
			if(!result.startsWith(head)){
				throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"货到付款服务费计算异常");
			}
			int index = result.indexOf(head);
			int end = result.indexOf(tail);
			if(index >= 0 && end > 0){
				result = result.substring(index + head.length(), end);
			}
			ObjectMapper jsonMapper = new ObjectMapper();
		
			JsonNode rootNode = jsonMapper.readValue(result, JsonNode.class);
			
			int errorCode = rootNode.path("errorId").getIntValue();
			if(errorCode != 0){
				throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"货到付款服务费计算异常");
			}
			JsonNode cods = rootNode.path("codDatas");
			Iterator<String> fieldNames = cods.getFieldNames();
			while (fieldNames.hasNext()) {
				String type = fieldNames.next();
				JsonNode cod = cods.path(type);
				if(null != cod){
					String mailfee = cod.path("shipFee").getTextValue();
					String freeCodFee = cod.path("freeCodFee").getTextValue();
					String ruleStr = cod.path("ruleData").getTextValue();
					getCodBycgi(po,dealTotalFee,buyNum,mailfee,freeCodFee,ruleStr,type,isMailFree);
				}
			}
		} catch (JsonParseException e) {
			Log.run.error("接口返回值转换json对象异常！");
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"货到付款服务费计算异常");
		} catch (JsonMappingException e) {
			Log.run.error("json对象字段映射异常！");
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"货到付款服务费计算异常");
		} catch (IOException e) {
			Log.run.error("");
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"货到付款服务费计算异常");
		}
		return po;
	}

	/**
	 * 获取拍便宜运费模板
	 * @param wid
	 * @param sk
	 * @param adid
	 * @param itemCode
	 * @param attrStr
	 * @param buyNum
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public List<ShipCalcVo> getTakeCheapShipCalcInfo(long wid, String sk, long adid, String itemCode, String attrStr, long buyNum) throws BusinessException {

		List<ShipCalcVo> shipCalcInfos = new LinkedList<ShipCalcVo>();
		if(adid == 0){
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"拍便宜运费模板信息获取失败！收货地址id为空！");
		}
		try {
			attrStr = URLEncoder.encode(attrStr,"utf8");
			StringBuffer url = new StringBuffer();
			url.append(TAKECHEAP_COD_CGI_URL).append("?cid=").append(itemCode)//
					.append("&num=").append(buyNum).append("&addressid=")//
					.append(adid).append("&stock=").append(attrStr);
			int timeOut = 3000;
			String cookie = "wg_uin=" + wid + ";wg_skey=" + sk;
			String result = HttpUtil.get(url.toString().replace(TAKECHEAP_COD_CGI_HOST, PaiPaiConfig.getPaipaiCommonIp()),TAKECHEAP_COD_CGI_HOST, timeOut, timeOut, "gbk", false,false,cookie,null);
			String head = "try{fixupConfirmCallBack(";
			String tail = ");}catch(e){}";
			if(null == result || !result.startsWith(head)){
				throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"拍便宜运费模板信息获取失败！");
			}
			int index = result.indexOf(head);
			int end = result.indexOf(tail);
			if(index >= 0 && end > 0){
				result = result.substring(index + head.length(), end);
			}
			System.out.println(result);
			ObjectMapper jsonMapper = new ObjectMapper();
			JsonNode rootNode = jsonMapper.readValue(result, JsonNode.class);
			int errorCode = rootNode.path("errorId").getIntValue();
			if(errorCode != 0){
				if (errorCode == 778899){
					throw BusinessException.createInstance(errorCode,"拍便宜运费模板信息获取失败");
				}else if(errorCode == 1){
				}else{
					throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"拍便宜运费模板信息获取失败");
				}
			}
			JsonNode shippingFee = rootNode.path("shippingFee");
			int free = shippingFee.get("free").asInt();
			if(free == 1){
				shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_FREE, 0L));
			}else{
				long express = shippingFee.get("express").asLong(0);
				long normal = shippingFee.get("normal").asLong(0);
				long ems = shippingFee.get("ems").asLong(0);
				if (express > 0) {
					shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EXPRESS, express));
				}
				if (normal > 0) {
					shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_NORMAL, normal));
				}
				if (ems > 0) {
					shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EMS, ems));
				}
				if(express ==0 && normal == 0 && ems == 0){
					shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EXPRESS, express));
					shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_NORMAL, normal));
					shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EMS, ems));
				}
			}
		} catch (JsonParseException e) {
			Log.run.error("接口返回值转换json对象异常！");
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"拍便宜运费模板信息获取失败");
		} catch (JsonMappingException e) {
			Log.run.error("json对象字段映射异常！");
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"拍便宜运费模板信息获取失败");
		}catch (UnsupportedEncodingException e) {
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"商品SKU参数异常");
		} catch (IOException e) {
			Log.run.error("");
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"拍便宜运费模板信息获取失败");
		}
		return shipCalcInfos;
	}

	/**
	 * 和PC端逻辑保持一致立即购买计算货到付款的服务费
	 * @param dealTotalFee 商品总价
	 * @param buyNum 商品个数
	 * @param mailfee 运费
	 * @param freeCodFee 货到付款专用的是否卖家承担运费  0 : 否 1：是
	 * @param rules 计算规则
	 * @param type 快递公司
	 * @param isMailFree 是否参加包邮优惠活动 0：否  1：是
	 */
	private void getCodBycgi(CodShipPo po,long dealTotalFee, long buyNum, String mailfee,
			String freeCodFee, String rules,String type,int isMailFree) {
		long fee = 0;
		int isFree = 0;
		long favFee = 0;
		long serverFee = 0;
		long[] rule = new long[5];
		if(null != mailfee){
			fee = Long.parseLong(mailfee);
		}
		if(null != freeCodFee){
			isFree = Integer.parseInt(freeCodFee);
		}
		if(null != rules){
			String[] strs = rules.split("\\|");
			for(int i=0;i< strs.length;i++){
				rule[i] = Long.parseLong(strs[i]);
			}
		}
		po.setCodShipFree(isFree);
		int codType = Integer.parseInt(type);
		boolean isSellerPay = (isFree == 1);
		if(isSellerPay){
			po.setCodCountFee(0);
			if(codType == 11){
				po.setMailType(DealConstant.MAIL_TYPE_COD_ZJS);
				po.setName("货到付款方式，宅急送");
			}else if(codType == 12){//顺丰 200|500|9000|1|1000000
				po.setMailType(DealConstant.MAIL_TYPE_COD_SF);
				po.setName("货到付款方式，顺丰");
			}else if(codType == 13){
				po.setMailType(DealConstant.MAIL_TYPE_COD_YS);
				po.setName("货到付款方式，优速");
			}else if(codType == 14){
				po.setMailType(DealConstant.MAIL_TYPE_COD_YT);
				po.setName("货到付款方式，圆通");
			}else if(codType == 16){
				po.setMailType(DealConstant.MAIL_TYPE_COD_JD);
				po.setName("货到付款方式，京东快递");
			}
		}else{
			if(codType == 12){//顺丰 200|500|9000|1|1000000
				po.setMailType(DealConstant.MAIL_TYPE_COD_SF);
				po.setName("货到付款方式，顺丰");
				double COD_FEE_RADIX = 10000;
				long b = dealTotalFee + fee;
				double amount = b/(1-rule[0]/COD_FEE_RADIX)+100;
				amount -= amount % 100;
				if(amount*(rule[0]/COD_FEE_RADIX) <= rule[1]*1){
					long n = b+100;
					n -= n % 100;
					serverFee = n + rule[1]*1 - b;
				}else{
					long amount2 = Math.round(amount); // 四舍五入
					serverFee = amount2 - b;
				}
			}else if(codType == 14){//圆通 "ruleData":"100|400|10000|400|500000
				po.setMailType(DealConstant.MAIL_TYPE_COD_YT);
				po.setName("货到付款方式，圆通");
				double COD_FEE_RADIX = 10000;
				long b = dealTotalFee + fee;
				// 揽收金额+1向下取整 Cf = [Cd+1]
				double amount = b * (COD_FEE_RADIX / (COD_FEE_RADIX - rule[0]));
				long amount2 = Math.round(amount); // 四舍五入
				if(amount2 % 100 != 0){//不为整数
					amount2 = amount2 + 100;
				}
				amount2 -= amount2 % 100;
				//新增类货款参数
				long totalFee = (b % 100 == 0) ? b : b + 100;
				totalFee -= totalFee % 100;
				//服务费
				if(amount2 < rule[1] * (COD_FEE_RADIX / rule[0])){
					serverFee = totalFee + rule[1]*1 - b;
				}else{
					serverFee = amount2 - b;
				}  
			}else if(codType == 11){//宅急送 150|500|10000|500|1000000   
				//宅急送物流服务费=(单价*数量-所有可抵扣的现金促销金额+运费模板匹配运费)*n%， 【四舍五入精确到分】
				po.setMailType(DealConstant.MAIL_TYPE_COD_ZJS);
				po.setName("货到付款方式，宅急送");
				long n = Math.round((dealTotalFee + fee)*rule[0]*0.0001);
				if(n < rule[1]*1){// 注意其上下限
					n = rule[1]*1;
				}
				if(n > rule[2]*1){
					n = rule[2]*1;
				}
				serverFee = n; 
			}else if(codType == 13){//优速 100|400|300000|0|300000
				po.setMailType(DealConstant.MAIL_TYPE_COD_YS);
				po.setName("货到付款方式，优速");
				long n = Math.round((dealTotalFee + fee)*rule[0]*0.0001);
				if(n < rule[1]*1){// 注意其上下限
					n = rule[1]*1;
				}
				if(n > rule[2]*1){
					n = rule[2]*1;
				}
				serverFee = n; 
			}else if (codType == 16){//京东 0|0|300000|1|2000000
				//京东物流服务费=(单价*数量-所有可抵扣的现金促销金额+运费模板匹配运费)*n%， 【四舍五入精确到分】
				po.setMailType(DealConstant.MAIL_TYPE_COD_JD);
				po.setName("货到付款方式，京东快递");
				long n = Math.round((dealTotalFee + fee)*rule[0]*0.0001);
				if(n < rule[1]*1){// 注意其上下限
					n = rule[1]*1;
				}
				if(n > rule[2]*1){
					n = rule[2]*1;
				}
				serverFee = n;
			}
		}
		serverFee = getCodTempValue(dealTotalFee,serverFee,fee,isMailFree,isSellerPay);
		//计算优惠费用，当服务费小于0时设置服务费为0，负数转正作为优惠费用
		if(serverFee < 0){
			favFee = -serverFee;
			serverFee = 0;
		}
		if(isMailFree == 1){
			fee = 0;
		}
		po.setCodCountFee(serverFee);
		po.setFee(fee);
		po.setFavFee(favFee);
		//最终价格=商品批价之后的总价+服务费+运费-优惠费
		po.setFinalFee(dealTotalFee+po.getCodCountFee()+po.getFee()-po.getFavFee());
	}

	/**
	 * 计算最终服务费
	 * @param dealTotalFee
	 * @param serverFee
	 * @param fee
	 * @param isMailFree
	 * @return
	 */
	private long getCodTempValue(long dealTotalFee, long serverFee,
			long fee, int isMailFree,boolean isSellerPay) {
		long otherFees = (isMailFree == 1) ? serverFee : (fee + serverFee);
		double tempTotalFee = (dealTotalFee + otherFees)*0.01;
		//卖家承担运费：舍尾；买家承担：四舍五入
		long mjyfj = (long) (isSellerPay || (isMailFree == 1) ? Math.floor(tempTotalFee)*100 : Math.round(tempTotalFee)*100);
		serverFee = mjyfj - dealTotalFee - ((isMailFree ==1) ? 0 : fee);
		
		return serverFee;
	}

	public static void main(String[] args) {
		long rid = 57;
		long buyNum = 1;
		int isMailFree = 0;
		String ci = "0E0A632F000000000401000043BBBA57";
		String stock = "颜色:冷灰色";
		long dealTotalFee = 1992;
		long wid = 3115117569l;
		String sk = "za36A0DEDB";
		SFBizImpl sfBiz = new SFBizImpl();
//		List<ShipCalcVo> shipCalcVos = sfBiz.getTakeCheapShipCalcInfo(wid, sk, rid, ci, stock, buyNum);
		sfBiz.calcCodShipfeeWithCgi(rid, ci, dealTotalFee, buyNum, wid, sk, isMailFree);
	}
}
