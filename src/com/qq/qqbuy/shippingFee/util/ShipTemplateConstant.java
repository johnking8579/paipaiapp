package com.qq.qqbuy.shippingFee.util;

/**
 * @author winsonwu
 * @Created 2013-3-8
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public interface ShipTemplateConstant {
	/****************************************************** 外部系统提供的常量 **********************************************/
	// 普通运费模版
	public static final int SHIPPING_TMP_TYPE_NORMAL = 0;
	// COD运费模版且买家承担COD服务费
	public static final int SHIPPING_TMP_TYPE_COD_BUYER = 1;
	// COD运费模版且卖家承担COD服务费
	public static final int SHIPPING_TMP_TYPE_COD_SELLER = 2;
	
	/****************************************************** 自身系统定义的常量  *********************************************/
	// 运送方式：免运费
	public static final int SHIPCALC_TYPE_FREE = 0;
	// 运送方式：快递
	public static final int SHIPCALC_TYPE_EXPRESS = 1;
	// 运送方式：平邮
	public static final int SHIPCALC_TYPE_NORMAL = 2;
	// 运送方式：EMS
	public static final int SHIPCALC_TYPE_EMS = 3;
	// 运送方式：货到付款
	public static final int SHIPCALC_TYPE_COD = 4;

}
