package com.qq.qqbuy.shippingFee.util;

import java.util.Vector;

import com.qq.qqbuy.cmdy.util.CmdyConstant;
import com.qq.qqbuy.common.constant.C2CDefineConstant;
import com.qq.qqbuy.thirdparty.idl.shippingFee.protocol.ApiCalcShipTemplateFeeResp;
import com.qq.qqbuy.thirdparty.idl.shippingFee.protocol.ApiShipInfo;
import com.qq.qqbuy.thirdparty.idl.shippingFee.protocol.ApiShipInfoList;

/**
 * 运费模版相关Util
 * 计算一口价、购物车下单流程的运费
 * @author winsonwu
 * @Created 2013-3-8
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class ShipTemplateUtil implements ShipTemplateConstant, CmdyConstant, C2CDefineConstant {
	
	public static Vector<ApiShipInfo> getApiShipInfoList(ApiCalcShipTemplateFeeResp resp) {
		if (resp != null) {
			ApiShipInfoList shipList = resp.getApiShipInfoListOut();
			if (shipList != null) {
				return shipList.getVecShipInfo();
			}
		}
		return null;
	}
	
}
