package com.qq.qqbuy.shippingFee.po;

/**
 * 货到付款费用元数据
 * 
 * @author matrixshi
 * 
 */
public class CodShipPo {

	//商品标示
	private String itemCode;
	
	// 邮递类型：下单接口使用
	private int mailType;

	// 运送方式名称
	private String name;

	// 运费，单位为分
	private long fee;

	/**
	 * 货到付款商品最终的价格
	 */
	private long finalFee;
	
	/**
	 * 货到付款服务费
	 */
	private long codCountFee;

	/**
	 * 货到付款优惠费
	 */
	private long favFee;
	
	/**
	 * 货到付款是否免服务费，1：免服务费，0：不免服务费
	 */
	private int codShipFree;

	public int getMailType() {
		return mailType;
	}

	public void setMailType(int mailType) {
		this.mailType = mailType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getFee() {
		return fee;
	}

	public void setFee(long fee) {
		this.fee = fee;
	}

	public long getCodCountFee() {
		return codCountFee;
	}

	public void setCodCountFee(long codCountFee) {
		this.codCountFee = codCountFee;
	}

	public int getCodShipFree() {
		return codShipFree;
	}

	public void setCodShipFree(int codShipFree) {
		this.codShipFree = codShipFree;
	}

	public long getFavFee() {
		return favFee;
	}

	public void setFavFee(long favFee) {
		this.favFee = favFee;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public long getFinalFee() {
		return finalFee;
	}

	public void setFinalFee(long finalFee) {
		this.finalFee = finalFee;
	}

}
