package com.qq.qqbuy.shippingFee.po;

import java.util.Vector;

public class SFTplDetailInfo {

    
    /**
     * 版本 >= 0
     */
     private long id;

    /**
     * 版本 >= 0
     */
     private long uin;

    /**
     * 版本 >= 0
     */
     private short innerId;

    /**
     * 版本 >= 0
     */
     private long property;

    /**
     * 版本 >= 0
     */
     private String name = ""; 

    /**
     * 版本 >= 0
     */
     private String desc = ""; 

     
     private Vector<Rule> rules = new Vector<Rule>();


    public long getId() {
        return id;
    }


    public void setId(long id) {
        this.id = id;
    }


    public long getUin() {
        return uin;
    }


    public void setUin(long uin) {
        this.uin = uin;
    }


    public short getInnerId() {
        return innerId;
    }


    public void setInnerId(short innerId) {
        this.innerId = innerId;
    }


    public long getProperty() {
        return property;
    }


    public void setProperty(long property) {
        this.property = property;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getDesc() {
        return desc;
    }


    public void setDesc(String desc) {
        this.desc = desc;
    }


    public Vector<Rule> getRules() {
        return rules;
    }


    public void setRules(Vector<Rule> rules) {
        this.rules = rules;
    }
     
     
     
     
}
