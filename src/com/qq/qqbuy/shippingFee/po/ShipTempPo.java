package com.qq.qqbuy.shippingFee.po;

/**
 * 兼容购物车和一口价“调接口计算运费”返回的数据结构
 * 为移动侧计算展示的运费项提供统一的数据结构
 * @author winsonwu
 * @Created 2013-3-8 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class ShipTempPo {
	
	// 是否买家支付
	private boolean buyerPay = true;

	// 平邮费用
	private long normalFee;

	// 快递费用
	private long expressFee;

	// EMS费用
	private long emsFee;
	
	// 是否支持在线支付
	private boolean isSuppordPayOnline;
	
	// 是否支持货到付款
	private boolean isSuppordCod;
	
	// 货到付款运费
	private long codFee;
	
	public boolean isBuyerPay() {
		return buyerPay;
	}

	public void setBuyerPay(boolean buyerPay) {
		this.buyerPay = buyerPay;
	}

	public long getNormalFee() {
		return normalFee;
	}

	public void setNormalFee(long normalFee) {
		this.normalFee = normalFee;
	}

	public long getExpressFee() {
		return expressFee;
	}

	public void setExpressFee(long expressFee) {
		this.expressFee = expressFee;
	}

	public long getEmsFee() {
		return emsFee;
	}

	public void setEmsFee(long emsFee) {
		this.emsFee = emsFee;
	}

	public long getCodFee() {
		return codFee;
	}

	public void setCodFee(long codFee) {
		this.codFee = codFee;
	}

	public boolean isSuppordCod() {
		return isSuppordCod;
	}

	public void setSuppordCod(boolean isSuppordCod) {
		this.isSuppordCod = isSuppordCod;
	}

	public boolean isSuppordPayOnline() {
		return isSuppordPayOnline;
	}

	public void setSuppordPayOnline(boolean isSuppordPayOnline) {
		this.isSuppordPayOnline = isSuppordPayOnline;
	}
	
}
