package com.qq.qqbuy.dealopr.po;

import java.util.ArrayList;
import java.util.List;

public class WQueryDealListResult
{
	private String queryType;

	/** pageIndex:当前为第几页*/
	private long pageIndex;
	
	/** pageTotal:总页数*/
	private long pageTotal;
	
	/** countTotal:满足查询条件的记录总数*/
	private long countTotal;
	
    private List<WDealInfo> wdealList = new ArrayList<WDealInfo>();

	public String getQueryType() {
		return queryType;
	}

	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}

	public long getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(long pageIndex) {
		this.pageIndex = pageIndex;
	}

	public long getPageTotal() {
		return pageTotal;
	}

	public void setPageTotal(long pageTotal) {
		this.pageTotal = pageTotal;
	}

	public long getCountTotal() {
		return countTotal;
	}

	public void setCountTotal(long countTotal) {
		this.countTotal = countTotal;
	}

	public List<WDealInfo> getWdealList() {
		return wdealList;
	}

	public void setWdealList(List<WDealInfo> wdealList) {
		this.wdealList = wdealList;
	}
}
