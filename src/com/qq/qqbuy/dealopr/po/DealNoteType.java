package com.qq.qqbuy.dealopr.po;
public enum DealNoteType
{
  RED(1, "红色"), 
  YELLOW(2, "黄色"), 
  GREEN(3, "绿色"), 
  BLUE(4, "蓝色"), 
  PINK(5, "粉红色"), 
  UN_LABEL(0, "未标注");

  private final int code;
  private final String showMeg;

  private DealNoteType(int code, String showMeg) { this.code = code;
    this.showMeg = showMeg; }

  public int getCode()
  {
    return this.code;
  }

  public String getShowMeg() {
    return this.showMeg;
  }

  public static DealNoteType getEnumByCode(int code) {
    for (DealNoteType type : values()) {
      if (type.code == code) {
        return type;
      }
    }
    return UN_LABEL;
  }

  public static DealNoteType getEnumByValue(String value) {
    for (DealNoteType dnt : values()) {
      if (dnt.name().equalsIgnoreCase(value)) {
        return dnt;
      }
    }
    return UN_LABEL;
  }
}

