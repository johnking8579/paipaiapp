package com.qq.qqbuy.dealopr.po;
public enum DealType
{
  SELL_TYPE_ALL(0, "所有类型"), 
  SELL_TYPE_BIN(1, "一口价"), 
  SELL_TYPE_AUCTION_SINGLE(2, "单件拍卖"), 
  SELL_TYPE_B2C(3, "b2c订单"),
  SELL_TYPE_TAKECHAEAP(10,"拍便宜订单"),
  UNKNOW(-1, "未知类型");

  private final int code;
  private final String showMeg;

  private DealType(int code, String showMeg) { this.code = code;
    this.showMeg = showMeg; }

  public static DealType getEnumByCode(int code)
  {
    for (DealType type : values()) {
      if (type.code == code)
        return type;
    }
    return UNKNOW;
  }

  public String getShowMeg() {
    return this.showMeg;
  }

  public int getCode() {
    return this.code;
  }
}
