package com.qq.qqbuy.dealopr.po;
public enum DealPayType
{
  UNKNOW(0, "未定"),
  TENPAY(1, "财付通"),
  //TENPAY(1, "在线支付"),
  //OFF_LINE(2, "线下交易"),
  OFF_LINE(2, "货到付款"), 
  DEAL_BIZ_FLAG_HDFK(3, "货到付款流程"),
  WXPAY(5,"微信支付"),
  JDPAY(8, "京东收银台支付"),
  UNIONPAY(10,"银联支付");

  private final int code;
  private final String showMeg;

  private DealPayType(int code, String showMeg) { this.code = code;
    this.showMeg = showMeg; }

  public static DealPayType getEnumByCode(int code)
  {
    for (DealPayType type : values()) {
      if (type.code == code) {
        return type;
      }
    }
    return UNKNOW;
  }

  public static DealPayType getEunmByName(String name) {
    return valueOf(name);
  }

  public int getCode() {
    return this.code;
  }

  public String getShowMeg() {
    return this.showMeg;
  }
}

