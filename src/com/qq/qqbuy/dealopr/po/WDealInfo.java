package com.qq.qqbuy.dealopr.po;

import java.util.Date;
import java.util.Vector;

import org.apache.commons.lang.StringEscapeUtils;

import com.qq.qqbuy.common.util.DateUtils;
import com.qq.qqbuy.common.util.SumUtil;
import com.qq.qqbuy.dealopr.util.DealOprConstant;
import com.qq.qqbuy.item.util.ItemUtil;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.DealInfo;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CDealInfo;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CTradoInfo;

public class WDealInfo
{
	/*
	 * PC侧返回的订单信息
	 */
	private CDealInfo pcDeal = new CDealInfo();
	
	 //订单类型: 256(手拍货到付款订单);货到付款255(PC订单")：
	private int dealType = 255;
	private String dealTypeStr ="在线支付" ;

	/*
	 * 手拍侧返回的订单信息dealType = 256时有效
	 */
    private DealInfo codDeal = new DealInfo();
	  
	 //视图使用: 订单第一个商品图片
	private String firstItemPic80;
	public String getFirstItemPic80() 
	{
		if(pcDeal != null)
		{
			Vector<CTradoInfo> tradeList = pcDeal.getTradeList();
			if( tradeList != null && tradeList.size()>0)
			{
				CTradoInfo tradoInfo = pcDeal.getTradeList().get(0);
				if(tradoInfo!= null)
				{
					this.firstItemPic80 = ItemUtil.getImgUrl(DealOprConstant.ItemPicURLPrefix + tradoInfo.getItemLogo(),"80x80");
				}
			}
		}
		return firstItemPic80;
	}
	 //视图使用: 订单第一个商品ID
	private String firstItemId;
	public String getFirstItemId() 
	{
		if(pcDeal != null)
		{
			Vector<CTradoInfo> tradeList = pcDeal.getTradeList();
			if( tradeList != null && tradeList.size()>0)
			{
				CTradoInfo tradoInfo = pcDeal.getTradeList().get(0);
				if(tradoInfo!= null)
				{
					this.firstItemId = tradoInfo.getDisItemId();
				}
			}
		}
		return firstItemId;
	}
	
	//视图使用: 订单商品名称列表
	private String itemTitleList;
	public String getItemTitleList() 
	{
		if(pcDeal!= null)
		{
			this.itemTitleList = pcDeal.getItemTitleList();
		}
		return itemTitleList;
	}
	
	//视图使用: 订单第一类商品数量
	private long firstItemNum;
	public long getFirstItemNum() 
	{
		if(pcDeal != null &&  pcDeal.getTradeList()!= null && pcDeal.getTradeList().get(0)!= null)
		{
			this.firstItemNum = pcDeal.getTradeList().get(0).getDealItemNum();

		}
		return firstItemNum;
	}
	
	//视图使用: 订单中所有类商品数量
	private long itemTotalCount;
	public  long getItemTotalCount() 
	{
		if(pcDeal != null &&  pcDeal.getTradeList()!= null)
		{
			for(CTradoInfo tradInfo : pcDeal.getTradeList())
			{
				if( tradInfo!= null)
				{
					this.itemTotalCount  = this.itemTotalCount  + tradInfo.getDealItemCount();
				}
			}
			
		}
		return itemTotalCount;
	}
	//视图使用:pc拍拍上订单状态
	private int pcDealState;
	public int getPcDealState() 
	{
		if(this.dealType == 255 && pcDeal != null)
		{
			this.pcDealState =  pcDeal.getDealState();	
			
			//如果订单下单(待付款)时间与当前时间相差了15天，则将状态强制设置为关闭
			if(pcDeal != null && pcDeal.getDealState()==1)
			{
				Date dealCreateTime = new Date(pcDeal.getDealCreateTime()*1000l);
				Date nowTime = new Date();
				long  outDays=( nowTime.getTime() - dealCreateTime.getTime())/(24*60*60*1000);
				if(outDays >= 15)
				{
					this.pcDealState = 46; //交易取消
				}
			}
		}
		return pcDealState;
	}
	//视图使用:手拍上订单状态
	private int codDealState;
	public int getCodDealState() 
	{
		if(this.dealType == 256 && codDeal != null)
		{
			this.codDealState =  codDeal.getSubstate();	
			
			//如果订单下单(待用户确认)时间与当前时间相差了15天，则将状态强制设置为关闭
			if( codDeal.getSubstate()==1 && pcDeal != null)
			{
				Date dealCreateTime = new Date(pcDeal.getDealCreateTime()*1000l);
				Date nowTime = new Date();
				long  outDays=( nowTime.getTime() - dealCreateTime.getTime())/(24*60*60*1000);
				if(outDays >= 15)
				{
					this.codDealState = 8; //交易取消
				}
			}
		}
		return codDealState;
	}
	
	//视图使用:订单状态描述
	private String dealStateDesc;
	public String getDealStateDesc() 
	{
		//货到付款，状态为
		if(this.dealType == 256)
		{
			if(codDeal != null)
			{
				String subStateDesc = DealOprConstant.CodDealStateKeyDesMap.get(getCodDealState());	
				this.dealStateDesc = subStateDesc;
			}
		}
		else if(this.dealType == 255)
		{
			if(pcDeal != null)
			{
				//int stateCode =  pcDeal.getDealState();
				this.dealStateDesc = DealOprConstant.PcDealStateKeyDesMap.get(getPcDealState());
			}
		}
		return dealStateDesc;
	}
	
	//视图使用： 订单类型描述
	public String getDealTypeStr() 
	{
		//手拍货到付款
		if(this.dealType == 256)
		{
			this.dealTypeStr  = "货到付款";
		}
		//PC侧的货到付款订单(订单状态为46~47)，订单类型描述为""
		if(this.dealType == 255 && pcDeal != null && pcDeal.getDealState()>40 && pcDeal.getDealState()<47)
		{
			this.dealTypeStr = null;
		}
		
		return dealTypeStr;
	}
	//视图使用： 订单号
	private String dealCode;
	public String getDealCode() 
	{
		if(pcDeal != null)
		{
			this.dealCode = pcDeal.getDisDealId();
		}
		return dealCode;
	}
	
	//视图使用：第一个商品的属性
	private String firstItemAttr;
	public String getFirstItemAttr() 
	{
		if(pcDeal != null && pcDeal.getTradeList()!= null && pcDeal.getTradeList().size()>0)
		{
			CTradoInfo tradoInfo = pcDeal.getTradeList().get(0);
			if(tradoInfo!= null)
			{
				this.firstItemAttr = tradoInfo.getItemAttrOptionValue();
			}
		}
		return firstItemAttr;
	}
	
	//视图使用：下单总金额
	private long payFeeTotal;
	public long getPayFeeTotal() 
	{
		if(this.dealType == 256 && codDeal != null)
		{
			String refer = codDeal.getReferer();
			long referVlaue = codDeal.getPayFeeTotal();
			try
			{
				referVlaue = Long.valueOf(refer).longValue();
			}
			catch(Exception e)
			{
				//do nothing
			}
			this.payFeeTotal = referVlaue;
		}
		else if(pcDeal != null)
		{
			this.payFeeTotal = pcDeal.getDealPayFeeTotal();
		}
		
		return payFeeTotal;
	}
	//itemPrice
	private String itemPrice ;
	public String getItemPrice() 
	{
		if(pcDeal != null)
		{
			this.itemPrice = SumUtil.getFormattedCash(pcDeal.getTradeList().get(0).getItemPrice());
		}

		return itemPrice;
	}	
	
	//下单时间
	private String createTime;
	public String getCreateTime() 
	{
		if(pcDeal != null)
		{
			this.createTime = DateUtils.FORMAT_YMD_HMS.format(new Date(pcDeal.getDealCreateTime()*1000l));
		}
		return createTime;
	}
	
	//收货地址
	private String recvAddr;
	public String getRecvAddr() 
	{
		if(pcDeal != null)
		{ 
			this.recvAddr = StringEscapeUtils.unescapeHtml(pcDeal.getReceiveAddr()) + "(" + StringEscapeUtils.unescapeHtml(pcDeal.getReceiveName()) + " " + StringEscapeUtils.unescapeHtml(pcDeal.getStrReceiveMobile()) +")";
		}
		return recvAddr;
	}
	
	//商品列表
	private Vector<CTradoInfo> itemList;
	public Vector<CTradoInfo> getItemList() 
	{
		if(pcDeal != null)
		{
			this.itemList = pcDeal.getTradeList();
		}
		return itemList;
	}
	
	//视图使用：折扣（红包）金额
	private long discountFee = 0l;
	public long getDiscountFee()
	{
		//重新清零，防止重复计算
		this.discountFee = 0l;
		Vector<CTradoInfo> list = getItemList();
		if(list != null)
		{
			for(CTradoInfo tradoInfo:list)
			{
				this.discountFee = this.discountFee + tradoInfo.getDiscountFee();
			}
		}
		
		return this.discountFee;
	}
	
	//买家留言
	private String buyerBuyRemark;
	public String getBuyerBuyRemark()
	{
		if(pcDeal!= null && pcDeal.getBuyerBuyRemark()!= null)
		{
			String dres = pcDeal.getBuyerBuyRemark();
			//删除附加费用部分
			int indexBegin = dres.indexOf("[需加收");
			String str = "元货到付款手续费]";
			int indexEnd = dres.indexOf(str);
			this.buyerBuyRemark = dres;
			if(indexBegin > 0 && indexEnd > 0) {
				this.buyerBuyRemark = dres.substring(0, indexBegin);
				this.buyerBuyRemark += dres.substring(indexEnd+str.length());
			}
		}
		return this.buyerBuyRemark;
	}
	//卖家uin
	private long sellerUin;
	public long getSellerUin()
	{
		if(pcDeal!= null)
		{
			this.sellerUin = pcDeal.getSellerUin();
		}
		else if(codDeal != null)
		{
			this.sellerUin = codDeal.getSellerUin();
		}
		return sellerUin;
	}
	
	//--------------------------------
	
	public CDealInfo getPcDeal() {
		return pcDeal;
	}

	public void setPcDeal(CDealInfo pcDeal) {
		this.pcDeal = pcDeal;
	}

	public int getDealType() {
		return dealType;
	}

	public void setDealType(int dealType) 
	{
		this.dealType = dealType;
	}
	public void setDealTypeStr(String dealTypeStr) {
		this.dealTypeStr = dealTypeStr;
	}

	public DealInfo getCodDeal() {
		return codDeal;
	}

	public void setCodDeal(DealInfo codDeal) {
		this.codDeal = codDeal;
	}
}
