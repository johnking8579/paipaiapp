package com.qq.qqbuy.dealopr.util;

import java.util.HashMap;
import java.util.Map;

public class DealOprConstant 
{
	
	public final static String ItemPicURLPrefix = "http://image.paipai.com/";
	public final static Map<Integer, String> CodDealStateKeyDesMap = new HashMap<Integer, String>();
	public final static Map<Integer, String> PcDealStateKeyDesMap = new HashMap<Integer, String>();
	static 
	{
		//--------------------256--------------------------
		CodDealStateKeyDesMap.put(1,"待用户确认"); //待短信确认
		CodDealStateKeyDesMap.put(2,"待卖家发货"); //待供货商确认
		CodDealStateKeyDesMap.put(3,"待发货"); //待发货
		CodDealStateKeyDesMap.put(6,"已发货"); //待确认收货
		CodDealStateKeyDesMap.put(7,"交易成功"); //成功订单
		CodDealStateKeyDesMap.put(8,"交易取消"); //已取消订单
		CodDealStateKeyDesMap.put(9,"交易取消"); //拒收订单
		
		//--------------------255--------------------------
		PcDealStateKeyDesMap.put(1,"待付款");   // DealState = 1,   待付款
		PcDealStateKeyDesMap.put(2,"待卖家发货");// DealState = 2, 已付款，待发货
		PcDealStateKeyDesMap.put(3,"已发货");//		DealState = 3, //已发货
		PcDealStateKeyDesMap.put(4,"交易成功"); //		DealState = 4, //交易成功
		PcDealStateKeyDesMap.put(5,"交易关闭"); //		DealState = 5, //交易关闭
		PcDealStateKeyDesMap.put(6,"交易暂停"); //		DealState = 6, //交易暂停
		PcDealStateKeyDesMap.put(7,"款项处理中"); //		DealState = 7, //款项处理中(确认收货)
		PcDealStateKeyDesMap.put(8,"款项处理中");//		DealState = 8, //款项处理中(有退款)
		PcDealStateKeyDesMap.put(9,"待卖家发货");//		DealState = 9, //配货中
		PcDealStateKeyDesMap.put(41,"货到付款等待发货");//	    DealState = 41, // 货到付款等待发货
		PcDealStateKeyDesMap.put(42,"货到付款已发货");//		DealState = 42, // 货到付款已发货
		PcDealStateKeyDesMap.put(43,"货到付款已签收");//		DealState = 43, // 货到付款已签收
		PcDealStateKeyDesMap.put(44,"买家拒签");//		DealState = 44, // 货到付款拒签
		PcDealStateKeyDesMap.put(45,"交易成功");//		DealState = 45, // 货到付款成功
		PcDealStateKeyDesMap.put(46,"交易取消");//		DealState = 46, // 货到付款取消
	}
	public static enum DealTransportType
	{
		TRANSPORT_NONE, //卖家包邮，无需买家关心运送
		TRANSPORT_MAIL, //邮政寄送
		TRANSPORT_EXPRESS, //快递
		TRANSPORT_EMS, //EMS
		TRANSPORT_UNKNOWN, //未知的运输方式
	}
}
