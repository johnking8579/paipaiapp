package com.qq.qqbuy.dealopr.util;

import java.util.Date;

import com.paipai.util.string.StringUtil;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.util.DateUtils;
import com.qq.qqbuy.common.util.TssHelper;
import com.qq.qqbuy.dealopr.po.DealNoteType;
import com.qq.qqbuy.dealopr.po.DealPayType;
import com.qq.qqbuy.dealopr.po.DealRateState;
import com.qq.qqbuy.dealopr.po.DealState;
import com.qq.qqbuy.dealopr.po.DealType;
import com.qq.qqbuy.item.constant.TransportType;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CDealInfo;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CTradoInfo;
import com.qq.qqbuy.thirdparty.openapi.po.GetDealDetailResponse;
import com.qq.qqbuy.thirdparty.openapi.po.GetDealDetailResponse.Item;

/**
 * 将IDL的字段映射为openAPI字段
 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
 */
public class DealDetailConveter {
	
	public static GetDealDetailResponse convertDealDetail(CDealInfo dealSrc) {
	try {
		if(dealSrc!=null){
			GetDealDetailResponse res = new GetDealDetailResponse();
			res.PayId                   = dealSrc.getPayId();
			res.buyerName				=  StringUtil.isEmpty(dealSrc.getBuyerName())?dealSrc.getBuyerNick():dealSrc.getBuyerName();
			res.buyerUin				= dealSrc.getBuyerUin();
			res.buyerRemark 			= dealSrc.getBuyerBuyRemark();
			res.dealCode 				= dealSrc.getDisDealId();
			res.dealDesc 				= dealSrc.getDealDesc();
			
			res.dealDetailLink 			= "http://pay.paipai.com/cgi-bin/pay_state_info?flag=0&sDealID=" + dealSrc.getDisDealId(); 
			res.dealNoteType 			= DealNoteType.getEnumByCode((int)dealSrc.getDealNoteType()).name();
			res.dealNote 				= dealSrc.getDealNote();
			res.dealState 				= DealState.getDealStateByCode(dealSrc.getDealState()).name();
			res.dealStateDesc 			= DealState.getDealStateByCode(dealSrc.getDealState()).getDesc();
			
			res.dealType 				= DealType.getEnumByCode(dealSrc.getDealType()).name();
			res.dealTypeDesc 			= DealType.getEnumByCode(dealSrc.getDealType()).getShowMeg();
			res.dealPayType 			= DealPayType.getEnumByCode(dealSrc.getDealPayType()).name();
			res.dealPayTypeDesc 		= DealPayType.getEnumByCode(dealSrc.getDealPayType()).getShowMeg();
			res.dealRateState 			= DealRateState.getEnumByCode((int)dealSrc.getDealRateState()).name();
			
			res.dealRateStateDesc 		= DealRateState.getEnumByCode((int)dealSrc.getDealRateState()).getShowMeg();
			res.propertymask            = dealSrc.getPropertymask()+"";
			
			res.createTime 				= parseDateToString(dealSrc.getDealCreateTime());
			
			res.dealEndTime				= parseDateToString(dealSrc.getDealEndTime());
			res.lastUpdateTime			= parseDateToString(dealSrc.getLastUpdateTime());
			res.payTime 				= parseDateToString(dealSrc.getPayTime());
			
			res.payReturnTime 			= parseDateToString(dealSrc.getPayReturnTime());
			res.recvfeeReturnTime 		= parseDateToString(dealSrc.getRecvfeeReturnTime());
			res.recvfeeTime 			= parseDateToString(dealSrc.getRecvfeeTime());
			res.sellerConsignmentTime 	= parseDateToString(dealSrc.getSellerConsignmentTime());
			res.hasInvoice				= StringUtil.isNotEmpty(dealSrc.getDealInvoiceTitle())?1:0;
			
			res.invoiceContent 			= dealSrc.getDealInvoiceContent();
			res.invoiceTitle			= dealSrc.getDealInvoiceTitle();
			res.tenpayCode 				= dealSrc.getDealCftPayid();
			
			res.transportType 			= TransportType.valueOf(dealSrc.getMailType()).name();
			res.transportTypeDesc 		= TransportType.valueOf(dealSrc.getMailType()).getShowMeg();
			
			res.whoPayShippingfee		= dealSrc.getWhoPayShippingfee();
			res.wuliuId 				= dealSrc.getWuliuId()+"";
			res.receiverAddress 		= dealSrc.getReceiveAddr();
			res.receiverMobile 			= dealSrc.getStrReceiveMobile();
			res.receiverName 			= dealSrc.getReceiveName();
			
			res.receiverPhone 			= dealSrc.getReceiveTel();
			res.receiverPostcode 		= dealSrc.getReceivePostcode();
			res.sellerRecvRefund		= dealSrc.getSellerRecvRefund();
			res.buyerRecvRefund			= dealSrc.getBuyerRecvRefund();
			res.couponFee				= dealSrc.getCouponFee();
			
			res.dealPayFeePoint			= dealSrc.getDealPayFeeScore();
			res.dealPayFeeTicket		= dealSrc.getDealPayFeeTicket();
			res.dealPayFeeTotal			= dealSrc.getDealPayFeeTotal();
			res.freight					= dealSrc.getDealPayFeeShipping();
//			String shippingfeeCalc 		= elm.getChildText("shippingfeeCalc");
			if(StringUtil.isNumeric(dealSrc.getShippingfeeCalc())){
				res.shippingfeeCalc		= Long.parseLong(dealSrc.getShippingfeeCalc());
			}
			res.totalCash				= dealSrc.getDealPayFeeCash();
			res.sellerCrm 				= dealSrc.getSellerCrm()+"";
			res.sellerName 				= dealSrc.getSellerName();
			res.sellerUin				= dealSrc.getSellerUin();
			if ((DealState.DS_DEAL_END_NORMAL.equals(dealSrc.getDealState())) && (dealSrc.getSellerRecvRefund() == 0L)){
				res.dealState=DealState.DS_CLOSED.name();
			}
			if(dealSrc.getTradeList()!=null){
				for (CTradoInfo itemSrc : dealSrc.getTradeList()) {
					 Item item = res.new Item();
				     convertItem(itemSrc, item);
				     res.itemList.add(item);
				 }
			}
			
			return res;
		}
	} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.run.warn("convert detail po exp",e);
			return null;
	}
	return null;
}
	
	private static void convertItem(CTradoInfo itemSrc,GetDealDetailResponse.Item itemDest){
		//itemDest.closeReasonDesc(itemSrc.getCloseReasonDesc());
	    //itemDest.dealCode=itemSrc.getDisTradeId());
	    itemDest.itemName=itemSrc.getItemName();

	    if ((itemSrc.getDisItemId() != null) && (itemSrc.getDisItemId().length() > 18)) {
	      itemDest.itemCode=new StringBuffer(itemSrc.getDisItemId()).replace(14, 16, "00").toString();
	    }
	    itemDest.itemCodeHistory=itemSrc.getDisItemId();
	    itemDest.itemLocalCode=itemSrc.getItemLocalCode();

	    itemDest.itemDetailLink="http://auction1.paipai.com/" + itemSrc.getDisItemId();
	    itemDest.itemPic80=TssHelper.getImgUrl(itemSrc.getItemLogo(), "", false);

	   // item.account = itemSrc.getd
	    itemDest.itemDealPrice=itemSrc.getItemPrice();
	    itemDest.itemAdjustPrice=itemSrc.getItemAdjustPrice();
	    itemDest.itemDealCount=itemSrc.getDealItemCount();
	    
	    //itemDest.itemFlag = itemSrc.geti
		//item.account = itemNode.getChildText("account");

	   // itemDest.itemDiscountFee(itemSrc.getDiscountFee());
	   
	    //itemDest.itemPromotionDesc=itemSrc.getItemPromotionDesc();
	    itemDest.itemRetailPrice=itemSrc.getItemOriginalPrice();
	   // itemDest.productCode=itemSrc.getProductCode());
	    if (itemSrc.getTradeRefundId() > 0L) {
	      itemDest.refundState=DealState.getDealStateByCode(itemSrc.getTradeRefundState()).name();
	    }
	    itemDest.refundStateDesc=DealState.getDealStateByCode(itemSrc.getTradeRefundState()).getDesc();
	    //itemDest.shippingfeeDesc=itemSrc.getShippingfeeDesc();
	    itemDest.stockLocalCode=itemSrc.getStockLocalCode();
	    itemDest.stockAttr=itemSrc.getItemAttrOptionValue();
	    itemDest.dealSubCode=itemSrc.getTradeId()+"";

	    itemDest.itemDealState=DealState.getDealStateByCode(itemSrc.getTradeState()).name();
	    itemDest.itemDealStateDesc=DealState.getDealStateByCode(itemSrc.getTradeState()).getDesc();

//	    if (itemSrc.getTradePropertymask() != 0L) {
//	      StringBuffer buffer = new StringBuffer();
//	      for (TradePropertyMask tpm : TradePropertyMask.values()) {
//	        if ((itemSrc.getTradePropertymask() & tpm.getCode()) != 0L) {
//	          buffer.append(tpm.getCode()).append("_");
//	        }
//	      }
//	      if (buffer.length() > 1) {
//	        buffer.delete(buffer.length() - 1, buffer.length());
//	      }
//	      itemDest.setTradePropertymask(buffer.toString());
//	    }

//	    Vector vector = itemSrc.getRefundList();
//	    if ((vector != null) && (vector.size() > 0))
//	      for (CRefundInfo info : vector) {
//	        DealRefundInfo refund = new DealRefundInfo();
//	        copyRefundInfo(info, refund);
//	        itemDest.getDealRefundInfos().add(refund);
//	      }
	}
	private static String parseDateToString(long time) throws Exception{
		return DateUtils.formatDateToString(new Date(time * 1000L), DateUtils.FORMAT_YMD_HMS);
	}
}

