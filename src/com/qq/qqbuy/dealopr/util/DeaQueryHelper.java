package com.qq.qqbuy.dealopr.util;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.dealopr.po.WDealInfo;
import com.qq.qqbuy.dealopr.po.WQueryDealListResult;
import com.qq.qqbuy.thirdparty.idl.deal.DealQueryClient;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.BuyerGetDealListByConditionReq;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.BuyerGetDealListByConditionResp;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.DealCondition;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.DealInfo;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CDealInfo;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.GetDealInfoList2Resp;

public class DeaQueryHelper 
{    
    public static WQueryDealListResult convertPcDealListResp(long uin, GetDealInfoList2Resp res)  
    throws BusinessException 
    {    	
    	WQueryDealListResult result = new WQueryDealListResult();
    	
    	result.setCountTotal(res.getTotalNum());
   
    	Vector<CDealInfo> dealList = res.getODealInfoList(); 	
    	if(dealList == null || dealList.size() == 0)
    	{
    			return result;
    	}
    	
    	//---bigen:查询列表中的订单是否为货到付款订单，如果是，则标记状态信息----
    	Map<String,DealInfo> codDealInfos = new HashMap<String, DealInfo>();
    	final int size = dealList.size();
//    	for(int i=0; i < size ; i++)
//    	{
//    		String curDealCode = dealList.get(i).getDisDealId();
//    		BuyerGetDealDetailByDealIDReq reqCod = new BuyerGetDealDetailByDealIDReq();
//    	
//    		try
//    		{
//    			reqCod.setDealCode(curDealCode);
//    			reqCod.setUin(uin);
//    			BuyerGetDealDetailByDealIDResp resCod = DealQueryClient.getDealDetailByDealID(reqCod);
//    			if(resCod.getResult() == 0 && resCod.getErrCode() == 0 && resCod.getBuyerDealDetail().getBuyAmount()>0)
//    			{
//    	 			DealInfo  codDealInfo = resCod.getBuyerDealDetail();
//        			codDealInfos.put(curDealCode, codDealInfo);
//    			}
//    		}
//    		catch(Exception e)
//    		{
//            	Log.run.error("DeaQueryHelper==>convertPcDealListResp error.");
//                throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL,e);
//    		}
//    	}		 
		//-----end: 查询列表中的订单是否为货到付款订单，如果是，则标记状态信息----
    	
		 //查询手拍货到付款订单
    	//--------begin:修改利用批量查询接口-----------------
    	List<String> list = new ArrayList<String>();
    	for(int i=0; i < size ; i++)
    	{
    		String dealCodeForQuerry = dealList.get(i).getDisDealId();
    		list.add(dealCodeForQuerry);
    	}
		BuyerGetDealListByConditionReq req=new BuyerGetDealListByConditionReq();
		req.setUin(uin);
		DealCondition filter=new DealCondition();
		filter.setBuyerUin(uin);
		filter.setMultiDealCode(getDealListStr(list));
		req.setCondition(filter);
		BuyerGetDealListByConditionResp listResp=DealQueryClient.getDealListByCondition(req);
		if(listResp!=null && listResp.getErrCode()==0 
				 && listResp.getBuyerDealList()!=null)
		{
			 for(DealInfo codDealInfo:listResp.getBuyerDealList())
			 {
    			codDealInfos.put(codDealInfo.getDealCode(), codDealInfo);
			 }
		 }
		else
		{
        	Log.run.error("DeaQueryHelper==>convertPcDealListResp error.");
            throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL);
		}
		//---------end:修改利用批量查询接口---------
    	List<WDealInfo> wDealInfoList = new ArrayList<WDealInfo>();
    	for(int i=0; i < size ; i++)
    	{
    		WDealInfo wDealInfo = new WDealInfo();
    		
    		CDealInfo cDealInfo = dealList.get(i);
    		wDealInfo.setPcDeal(cDealInfo);
    		
    		//检查是否为货到付款订单，如果是，则设置货到付款订单信息
    		if(codDealInfos.containsKey(cDealInfo.getDisDealId()))
    		{
    			DealInfo dealInfo = codDealInfos.get(cDealInfo.getDisDealId());
    			wDealInfo.setDealType(256);
    			wDealInfo.setCodDeal(dealInfo);
    		}
    		wDealInfoList.add(wDealInfo);
    	
    	}
    	result.setWdealList(wDealInfoList);
    	
		return result;

    }
    
    private static String getDealListStr( List<String> list){
   	 if(list!=null && list.size()>0){
   		 StringBuilder dealIds=new StringBuilder();
   		 for(int i=0;i<list.size();i++){
   			 dealIds.append(list.get(i));
   			 if(i<list.size()-1){
   				 dealIds.append(";");
   			 }
   		 }
   		 return dealIds.toString();
   	 }
        return null;
    }
}
