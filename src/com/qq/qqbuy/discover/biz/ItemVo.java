package com.qq.qqbuy.discover.biz;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

public class ItemVo 	{
	public long itemId, sellerUin;
	public int score, tabId;
	public String dap;
	public int poolId;
	
	public String toJsonStr()	{
		return new GsonBuilder().serializeNulls().create().toJson(this);
	}
	
	public JsonElement toJsonTree()	{
		return new GsonBuilder().serializeNulls().create().toJsonTree(this);
	}
}
