package com.qq.qqbuy.discover.biz.impl;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/**
 * struct STPolicySysHead
		{
		    uint32_t 		   	uiLength;           //消息的总长度, 2+syshead+protobuf协议+3的总长度
		    uint64_t    		ullUin;             //Q号
			uint64_t    		ullVistiKey;        //visitkey	//X
			uint64_t    		ullSceneID;         //场景ID  	//随便填一个完整的场景ID
			uint64_t    		ullMsgID;			//消息ID,标示唯一一次请求的，用以追踪一个请求	//随机
			uint64_t    		ullCreateTime;      //请求创建时间	//毫秒数
		    uint64_t    		ullExpireTime;  	//请求过期时间	//创建时间加个随机值
			uint32_t    		uiVersion;          //协议版本		//1
			char        		szReserved[4];		//保留字段		//X
		    STPolicySubSysHead  stSubHead;			//子系统头	
			char        		pPkg[0];			//包数据		//标记位,无值无空间
		};
 */

public class STPolicySysHead {
	private int allMsglength;
	private long uin;
	private long visitKey;
	private long sceneId;
	private long msgId;
	private long createTime = System.currentTimeMillis();
	private long expireTime = createTime + 20000;
	private int version = 1;
	private byte[] reserved = new byte[4];
	private STPolicySubSysHead subHead;
	
	public void setAllMsglength(int allMsglength) {
		this.allMsglength = allMsglength;
	}

	public void setUin(long uin) {
		this.uin = uin;
	}

	public void setSceneId(long sceneId) {
		this.sceneId = sceneId;
	}
	
	public void setSubHead(STPolicySubSysHead subHead) {
		this.subHead = subHead;
	}
	
	public long getMsgId() {
		return msgId;
	}

	public byte[] toByteArray()	{
		assertSubHeadNotNull();
		return ByteBuffer.allocate(length())
		.order(ByteOrder.BIG_ENDIAN)
		.putInt(allMsglength)
		.putLong(uin)
		.putLong(visitKey)
		.putLong(sceneId)
		.putLong(msgId)
		.putLong(createTime)
		.putLong(expireTime)
		.putInt(version)
		.put(reserved)
		.put(subHead.toByteArray())
		.array();
	}
	
	public static STPolicySysHead deserialize(byte[] headBytes)	{
		ByteBuffer buf = ByteBuffer.wrap(headBytes);
		STPolicySysHead head = new STPolicySysHead();
		head.allMsglength = buf.getInt();
		head.uin = buf.getLong();
		head.visitKey = buf.getLong();
		head.sceneId = buf.getLong();
		head.msgId = buf.getLong();
		head.createTime = buf.getLong();
		head.expireTime = buf.getLong();
		head.version = buf.getInt();
		byte[] reserveBytes = new byte[4];
		buf.get(reserveBytes);
		byte[] subHeadBytes = new byte[STPolicySubSysHead.length()];
		buf.get(subHeadBytes);
		return head;
	}
	
	public static int length()	{
		return 4 + 8*6 + 4 + 4 + STPolicySubSysHead.length();
	}
	
	private void assertSubHeadNotNull()	{
		if(subHead == null)
			throw new UnsupportedOperationException("STPolicySysHead的STPolicySubSysHead不能为空");
	}

	@Override
	public String toString() {
		return "STPolicySysHead [allMsglength=" + allMsglength + ", uin=" + uin
				+ ", visitKey=" + visitKey + ", sceneId=" + sceneId
				+ ", msgId=" + msgId + ", createTime=" + createTime
				+ ", expireTime=" + expireTime + ", version=" + version
				+ ", reserved=" + Arrays.toString(reserved) + ", subHead="
				+ subHead + "]";
	}
}
