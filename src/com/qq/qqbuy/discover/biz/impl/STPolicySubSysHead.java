package com.qq.qqbuy.discover.biz.impl;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Random;

/**
 * struct STPolicySubSysHead
		{
			uint64_t    ullSrcID;        	//源系统类型，指发送该消息的内部系统	//x
			uint64_t    ullDesID;       	//目的系统类型，指接收该消息的内部系统	//X
			uint32_t    uiRespIP;           //回包的ip	//X
		    uint32_t    uiRespPort;         //回报的port	//X
		    uint64_t    ullSeq;             //序列号		//随机LONG
			uint64_t    ullSendTime;        //发送时间		//毫秒
		    uint64_t    ullExpireTime;      //过期时间		//毫秒 +随便写
		    char        szEcho[64];   	    //回送字段, 应用不能更改,必须保证一字不改地在响应中回带给调用者，以支持回传方式的异步svr	/X 64位CHAR(1个字节)
		};
 */
public class STPolicySubSysHead {
	private long srcId;
	private long desId;
	private int respIp;
	private int respPort;
	private long seq = Math.abs(new Random().nextLong());
	private long sendTime = System.currentTimeMillis();
	private long expireTime = sendTime + 20000; 
	private byte[] echo = new byte[64];
	
	public byte[] toByteArray()	{
		ByteBuffer buf = ByteBuffer.allocate(length());
		buf.order(ByteOrder.BIG_ENDIAN);
		buf.putLong(srcId);
		buf.putLong(desId);
		buf.putInt(respIp);
		buf.putInt(respPort);
		buf.putLong(seq);
		buf.putLong(sendTime);
		buf.putLong(expireTime);
		buf.put(echo);
		return buf.array();
	}
	
	public static int length()	{
		return 8+8+4+4+8+8+8+64;	//112
	}
}
