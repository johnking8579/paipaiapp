package com.qq.qqbuy.discover.biz.impl;

public interface BIConstant {
	
	public static final String 
		ECHO_PARAM_机器码 	= 	"deviceid",
		ECHO_PARAM_设备类型 	= 	"devicetype",
		ECHO_PARAM_经度 		= 	"longtitude",
		ECHO_PARAM_纬度 		= 	"latitude",
		ECHO_PARAM_策略ID		= 	"PolicyID",
		ECHO_PARAM_消息ID策略ID	=	"CLICK_TOURL_PARAM";	//格式为msgid:sceneId:policyid
	
	/**
	 * 首页-发现页感兴趣的商品	5000	1006
		购物车-猜你喜欢		5000	1007
		支付完成页-猜你喜欢	5000	1008
		首页-店铺推荐		5000	1009
		发现页-拍便宜推荐		5000	1010
		发现页-微店推荐		5000	1011
		发现页-商品推荐		5000	1012
		精选优店-你喜欢的店铺	5000	1013
		商祥页-看了又看		5000	1020
	 */
	public static final int
		SCENE_大场景ID 		= 	5000,
		SCENE_发现页商品推荐 	= 	1006,
		SCENE_店铺推荐小场景ID 	=	1009,
		
		/**
		 * 购物车下的商品推荐
		 */
		GWCRECOMMEND_小场景ID 	= 	1007,
		/**
		 * 支付下的商品推荐
		 */
		PAYRECOMMEND_小场景ID 	= 	1008,
		/**
		 * 优品推荐商品池1
		 */
		RECOMMEND_POOLID_FIRST = 25402,
		/**
		 * 优品推荐商品池2
		 */
		RECOMMEND_POOLID_SECOND = 25707,
		/**
		 * 优品推荐商品池3
		 */
		RECOMMEND_POOLID_THIRD = 25706;
}
