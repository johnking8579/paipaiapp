package com.qq.qqbuy.discover.biz.impl;
//有修改
import static com.qq.qqbuy.discover.biz.impl.BIConstant.ECHO_PARAM_消息ID策略ID;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.qq.qqbuy.bi.biz.Dap;
import com.qq.qqbuy.bi.biz.Scene;
import com.qq.qqbuy.common.Uint32;
import com.qq.qqbuy.common.util.ItemCode;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.discover.biz.ItemVo;
import com.qq.qqbuy.discover.biz.RecommendBiz;
import com.qq.qqbuy.discover.biz.impl.EcDataPolicyProtocV1.CProPolicyBody;
import com.qq.qqbuy.discover.biz.impl.EcDataPolicyProtocV1.CProPolicyCmd;
import com.qq.qqbuy.discover.biz.impl.EcDataPolicyProtocV1.CProPolicyEchoParam;
import com.qq.qqbuy.discover.biz.impl.EcDataPolicyProtocV1.CProPolicyHead;
import com.qq.qqbuy.discover.biz.impl.EcDataPolicyProtocV1.CProPolicyItem;

public class RecommendBizImpl implements RecommendBiz{
	static Logger log = LogManager.getLogger(RecommendBizImpl.class);
	
	//p_jdhaolu(鲁浩) 01-08 11:09:49;;	我这边就搞了个app推荐和拍便宜推荐
	@Override
	public List<ItemVo> discoverItem(String clientIp, String itemIds,long wid, int pageSize) throws Exception{
		
//		if(poolIds.length == 0)	{
//			poolIds = new int[]{0};		//如果poolIds无值, 则对sceneId只传主场景+小场景+10000
//		}

		//组装 protocol buffer协议（谷歌的一个协议）
		//CProPolicyBody是提供接口的人生成的
		String[] split = itemIds.split(",");
		List<CProPolicyBody> bodys = new ArrayList<CProPolicyBody>();
		for (String itemId : split) {
			ItemCode ic = new ItemCode(itemId);
			
			CProPolicyBody body1 = CProPolicyBody.newBuilder()
			.setUllSceneID(new Scene(BIConstant.SCENE_大场景ID, BIConstant.PAYRECOMMEND_小场景ID, 0).longValue())
			.setUiItemNum(pageSize)
//			.setUiPageNo(1)
//			.setUiPageSize(pageSize)
//			.setUiParamNum(4)
//			.addProParam(CProPolicyEchoParam.newBuilder().setStrKey(ECHO_PARAM_机器码).setStrValue(mk).build())
//			.addProParam(CProPolicyEchoParam.newBuilder().setStrKey(ECHO_PARAM_设备类型).setStrValue(deviceType).build())
//			.addProParam(CProPolicyEchoParam.newBuilder().setStrKey(ECHO_PARAM_经度).setStrValue(longtitude).build())
//			.addProParam(CProPolicyEchoParam.newBuilder().setStrKey(ECHO_PARAM_纬度).setStrValue(latitude).build())
			.addProItem(CProPolicyItem.newBuilder().setUllItemID(ic.getItemId())
					.setUiSellUin(Long.valueOf(ic.getSellerUin()).intValue())
					.build())
			.build();
			bodys.add(body1);
		}
		
		CProPolicyHead head = CProPolicyHead.newBuilder()
			.setUiVersion(1)
			.setUiUin((int)wid)
			.setUiClientIP((int)Util.convertIp(clientIp))
			.setUiBodyNum(bodys.size()).build();
		
		CProPolicyCmd request = CProPolicyCmd.newBuilder()
			.setProHead(head).addAllProBody(bodys).build();
		CProPolicyCmd response = new DiscoverBizImpl().wrapAndSend(request);
		return parseFrom(response);
	}
	
	private List<ItemVo> parseFrom(CProPolicyCmd cmd)	{
		String msgId = "0";
		long policyId = 0L;
//		int scene1 = 0, scene2 = 0;
		long seneID = 0;
		int count = cmd.getProBody(0).getUiParamNum();
		if(count > 0)	{
			for(CProPolicyEchoParam p : cmd.getProBody(0).getProParamList())	{
				if(ECHO_PARAM_消息ID策略ID.equals(p.getStrKey()))	{
					String[] arr = p.getStrValue().split(":");
					msgId = arr[0];
//					int[] ints = Util.decSceneId(arr[1]);
//					scene1 = ints[0];
//					scene2 = ints[1];
					seneID = Long.parseLong(arr[1]);
					policyId = Long.parseLong(arr[2]);
					break;
				}
			}
		}
		
		List<CProPolicyItem> items = cmd.getProBody(0).getProItemList();
		List<ItemVo> vos = new ArrayList<ItemVo>();
		for(CProPolicyItem i : items)	{
			long sellerUin = new Uint32(i.getUiSellUin()).longValue();	//把超出int值的负数还原为long 
			ItemVo vo = new ItemVo();
			vo.itemId = i.getUllItemID();
			vo.sellerUin = sellerUin;
//			vo.score = i.getUiScore();
//			vo.tabId = i.getUiReserved(0);
//			vo.poolId = i.getUiReserved(5);
//			String sceneId = Util.encSceneId(scene1, scene2, i.getUiReserved(5));
			Dap dap = new Dap(msgId, seneID+"", policyId, new ItemCode(sellerUin, i.getUllItemID()).toString());
			vo.dap = dap.toString();
			vos.add(vo);
		}
		return vos;
	}
}
