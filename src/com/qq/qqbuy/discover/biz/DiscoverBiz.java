package com.qq.qqbuy.discover.biz;

import java.io.IOException;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.qq.qqbuy.bi.biz.Dap;

public interface DiscoverBiz {

	List<ItemVo> discoverItem(String clientIp, String deviceType, String mk,
			String longtitude, String latitude, long wid, int[] poolIds, int pageSize)
			throws Exception;
	
	List<ItemVo> discoverRecommendItems(String clientIp, String deviceType, String mk,
			String longtitude, String latitude, long wid, int[] poolIds, int pageSize)
			throws Exception;
	
	JsonElement discoverAct() throws IOException;
	
	JsonElement discoverBanner() throws IOException;

	JsonElement discoverSets() throws IOException;

	List<Dap> discoverShop(String clientIp, String deviceType, String mk,
			String longtitude, String latitude, long wid, int pageSize)
			throws IOException;
	/**
	 * 推荐拍便宜商品
	 * @param wid
	 * @param clientIp
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	JsonArray recommendGoodsPPY(Long wid, String clientIp, int pageSize) throws Exception;
	/**
	 * 推荐全局商品
	 * @param wid
	 * @param clientIp
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	JsonArray recommendGoodsGlobal(Long wid, String clientIp, int pageSize) throws Exception;
	/**
	 * 推荐店铺
	 * @param wid
	 * @param clientIp
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	JsonArray recommendShopWD(Long wid, String clientIp, int pageSize, String mk) throws Exception;
	
	/**
	 * 推荐我喜欢的店铺
	 * @param wid
	 * @param clientIp
	 * @param pageSize
	 * @param mk
	 * @return
	 * @throws Exception
	 */
	JsonObject recommendMyFavShop(Long wid, String clientIp, int pageSize, String mk) throws Exception;

	/**
	 * 获取海外购推荐商品
	 * @param activeId
	 * @param poolId
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	JsonArray discoverMartItems(long activeId,int poolId,int pageSize,int sortType)throws Exception;
}
