package com.qq.qqbuy.discover.biz;

import java.io.IOException;
import java.util.List;

import com.google.gson.JsonElement;

public interface RecommendBiz {

	/**
	 * 
	 * @param clientIp	客户端IP
	 * @param itemId	商品ID
	 * @param categoryId	品类ID
	 * @param wid
	 * @param poolIds
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	List<ItemVo> discoverItem(String clientIp, String itemId,
			long wid, int pageSize)
			throws Exception;
	
}
