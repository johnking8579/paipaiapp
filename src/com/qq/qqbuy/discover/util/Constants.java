package com.qq.qqbuy.discover.util;

public class Constants {
	
	public static final String DISCOVER_BI_HOSTS = "bi.paipai.com" ;

	public static final String DISCOVER_SHOW_URL = "http://bi.paipai.com/json/show?nojsonp=1&sf=$SF&hid=$HID&mid=$MID&low=0&type=$TYPE&uin=$UIN&ip=$IP&q=111|$PAGESIZE||" ;
	
	public static final String RECOMMEND_GOODS_PPY_SF = "1417487082";
	
	public static final String RECOMMEND_GOODS_PPY_MID = "1010";

	public static final String RECOMMEND_GOODS_PPY_HID = "5000";
	
	public static final String RECOMMEND_GOODS_PPY_TYPE = "4";
	
	public static final String RECOMMEND_SHOP_WD_SF = "1417487189";
	
	public static final String RECOMMEND_SHOP_WD_MID = "1011";

	public static final String RECOMMEND_SHOP_WD_HID = "5000";

	public static final String RECOMMEND_SHOP_WD_TYPE = "2";

	public static final String RECOMMEND_GOODS_GLOBAL_SF = "1417487183";
	
	public static final String RECOMMEND_GOODS_GLOBAL_MID = "1012";

	public static final String RECOMMEND_GOODS_GLOBAL_HID = "5000";
	
	public static final String RECOMMEND_GOODS_GLOBAL_TYPE = "0";		
	
	public static final int readTimeout = 15000;// 读超时时间
	
    public static final int connectTimeout = 10000;// 连接超时时间
	
	
}
