package com.qq.qqbuy.discover.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.qq.qqbuy.bi.biz.BiBiz;
import com.qq.qqbuy.bi.biz.Dap;
import com.qq.qqbuy.bi.biz.ExposureMsg;
import com.qq.qqbuy.common.JsonOutput;
import com.qq.qqbuy.common.action.AuthRequiredAction;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.util.AntiXssHelper;
import com.qq.qqbuy.common.util.CoderUtil;
import com.qq.qqbuy.common.util.ItemCode;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.discover.biz.DiscoverBiz;
import com.qq.qqbuy.discover.biz.ItemVo;
import com.qq.qqbuy.discover.biz.impl.BIConstant;
import com.qq.qqbuy.favorite.biz.FavoriteBiz;
import com.qq.qqbuy.favorite.po.FavoriteItemPo;
import com.qq.qqbuy.item.biz.ItemBiz;
import com.qq.qqbuy.item.biz.PPItemBiz;
import com.qq.qqbuy.item.po.ItemBO;
import com.qq.qqbuy.item.util.SellerCreditUtil;
import com.qq.qqbuy.shop.biz.ShopBiz;
import com.qq.qqbuy.shop.po.ShopItems;
import com.qq.qqbuy.shop.po.ShopSimpleItem;
import com.qq.qqbuy.thirdparty.idl.item.ItemClient;
import com.qq.qqbuy.thirdparty.idl.item.protocol.TabData;
import com.qq.qqbuy.thirdparty.idl.shop.CVItemFilterConstants;
import com.qq.qqbuy.thirdparty.idl.shop.ShopClient;
import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiShopInfo;
import com.qq.qqbuy.thirdparty.idl.userinfo.ApiUserClient;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.apiuser.APIUserProfile;

public class DiscoverAction extends AuthRequiredAction{	
	static Logger log = LogManager.getLogger(DiscoverAction.class);

	BiBiz biBiz;
	ShopBiz shopBiz;
	DiscoverBiz discoverBiz;
	FavoriteBiz favoriteBiz;
	ItemBiz itemBiz;
	PPItemBiz ppItemBiz;
	private ShopClient shopClient = new ShopClient();
	
	JsonOutput out = new JsonOutput();
	Gson gson = new GsonBuilder().serializeNulls().create();
	
	/**
	 * tabids: COSS商品的tabId,多个以逗号隔开
	 * pageSize2表示需要显示详细信息的
	 */
	String ids, tabIds, poolIds, daps, shopIds;
	int pageSize = 100,pageSize2 = 10,count2 = 4;
	long msgId;
	//海外购参数
	/**
	 * 海外购活动id
	 */
	long activeId = 35090;
	/**
	 * 海外购商品池id
	 */
	int poolId = 28008;
	/**
	 * 海外购BI推荐排序，0:bi排序  1:人工排序
	 */
	int sortType = 0;
	public int getSortType() {
		return sortType;
	}

	public void setSortType(int sortType) {
		this.sortType = sortType;
	}

	public int getPoolId() {
		return poolId;
	}

	public void setPoolId(int poolId) {
		this.poolId = poolId;
	}

	public long getActiveId() {
		return activeId;
	}

	public void setActiveId(long activeId) {
		this.activeId = activeId;
	}

	private long parseToken()	{
		long wid = getWidFromToken(getAppToken());
		return wid == -1 ? 0 : wid;
	}
	
	public String list(){
		return doPrint("abc");
	}
	
	/**
	 * 卖场快车商品详细信息接口, 根据tabid获得商品详情及特殊图片, 并上报曝光至BI
	 * @return
	 */
	public String tabDetail()	{
		long wid = parseToken();
		List<Integer> tabIdArr = Util.toIntList(tabIds.split(","));
		String[] dapArr = daps.split(",");
		if(tabIdArr.size() != dapArr.length)	{
			throw new IllegalArgumentException("tabIds和daps个数不对应");
		}
		
		List<FavoriteItemPo> allFavItem = new ArrayList<FavoriteItemPo>();
		if(checkLoginAndGetSkey())	{
			allFavItem = favoriteBiz.getAllFavItem(wid, getMk(), getSk());
		}
		
		List<TabData> tabs = ItemClient.getTabDetail(wid, getHttpHeadIp(), getMk(),	tabIdArr);
		JsonArray ja = new JsonArray();
		for(TabData t : tabs)	{
			JsonObject item = new JsonObject();
			item.addProperty("itemId", t.getStrCommodityId());
			item.addProperty("itemCode", t.getStrCommodityId());
			item.addProperty("infoUrl", "http://auction1.paipai.com/" + t.getStrCommodityId());
			Map<String,String> custom = t.getMapCustomData();
			item.addProperty("uploadPicUrl1", custom.get("uploadPicUrl1"));
			item.addProperty("uploadPicUrl2", custom.get("uploadPicUrl2"));
			item.addProperty("uploadPicUrl3", custom.get("uploadPicUrl3"));
			item.addProperty("uploadPicUrl4", custom.get("uploadPicUrl4"));
			item.addProperty("title", t.getStrTitle());
			item.addProperty("price", t.getDwActivePrice());
			
			long commentCount = 0, storeNum = 0;
			try {
				ItemBO bo = ppItemBiz.fetchItemInfo(t.getStrCommodityId(), false);
				ppItemBiz.fetchCmtShopInfo4Item(t.getStrCommodityId(), bo, getMk());
				commentCount = bo.getCommentCount();
				storeNum = bo.getStoreNum();
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
			item.addProperty("commentCount", commentCount);
			item.addProperty("storeNum", storeNum);
			
			boolean isInFav = false;
			for(FavoriteItemPo f : allFavItem)	{
				if(t.getStrCommodityId().equals(f.getItemId()))	{
					isInFav = true;
					break;
				}
			}
			item.addProperty("isInFav", isInFav);
			
			ja.add(item);
		}
		JsonObject json = new JsonObject();
		json.add("items", ja);
		out.setData(json);
		
		try {
			uploadExposure(tabs, dapArr, wid);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		return doPrint(out.toJsonStr());
	}
	
	
	/**
	 * 将每一个商品做为曝光数据, 提交至BI
	 */
	private void uploadExposure(List<TabData> tabs, String[] dapArr, long wid){
		for(TabData t : tabs)	{
			for(String d : dapArr)	{
				try {
					Dap dap = Dap.fromDap62(d);
					if(dap.getShopidOrItemcode().equals(t.getStrCommodityId())) 	{
						ExposureMsg msg = ExposureMsg.buildItemMsg(d, t.getDwClassIdL1(), t.getDwClassIdL2(), t.getDwClassIdL4(), 
								wid, getHttpHeadIp(), getMk(), getMt(), getLongitude(), getLatitude(), "");
						biBiz.uploadMsg(msg);
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}
		}
	}
	
	public String shopItem() 	{
		JsonArray arr = new JsonArray();
		
		//数据上报开始
		if(null != daps && !"".equals(daps)){
			for(String s : daps.split(","))	{
				try {
					Dap dap = Dap.fromDap62(s);
					long lCatId = ItemClient.fetchItemInfo(dap.getShopidOrItemcode())
							.getItemPo().getOItemBase().getLeafClassId();
					ExposureMsg msg = ExposureMsg.buildItemMsg(s, 0L, 0L, lCatId,	//TODO 怎么取三级类目ID
							parseToken(), getHttpHeadIp(), getMk(), getMt(), getLongitude(), getLatitude(), "");
					biBiz.uploadMsg(msg);
				} catch (Exception e) {
					log.error("数据上报失败。"+e.getMessage(), e);
				}
			}
		}
		//数据上报结束
		
		for(String id : shopIds.split(","))	{
//			ShopInfo s = shopBiz.getShopInfo(Long.parseLong(id));
			ApiShopInfo apiShopInfo = shopClient.getShopInfo(Long.parseLong(id));//查询店铺基本信息
			if(0 == apiShopInfo.getShopID())	{
				continue;
			}
			
			Map<String, String> filter1 = new HashMap<String, String>();
			filter1.put(CVItemFilterConstants.ITEM_FILTER_KEY_STATE, "1110");	// 只查询出售中的商品
			List<ShopSimpleItem> items1 = shopBiz.getShopComdyList(Long.parseLong(id), 
								getMk(), 0, (new Random().nextInt(2) == 0 ? 3 : 5), 
								CVItemFilterConstants.C2C_WW_COMDY_FIND_ORDERY_BY_ADDTIME_DESC, 
								filter1).getItems();
			if (items1.size() == 0) {
				continue ;
			}
			JsonArray goods = new JsonArray();
			for(int j=0 ; j<items1.size(); j++)	{
				JsonObject object = new JsonObject();
				object.addProperty("pic", items1.get(j).getImage());
				object.addProperty("price", items1.get(j).getPrice());
				object.addProperty("ic", items1.get(j).getItemId());
				//itemWeiDian	商品的微店商品地址[参照商品详情接口api/item/getItemBasic]
				object.addProperty("itemWeiDian", "http://b.paipai.com/itemweb/item?scence=101&ic=" + items1.get(j).getItemId());
				goods.add(object);
			}
			
			APIUserProfile profile = ApiUserClient.getUserSimpleInfo(apiShopInfo.getShopID());
			int sellerCredit = profile.getSellerCredit() ;//卖家信用 
			
			JsonObject shopJson = new JsonObject();
			shopJson.addProperty("shopId", apiShopInfo.getShopID() + "");
			shopJson.addProperty("shopName", apiShopInfo.getShopName());
			shopJson.addProperty("logo", "http://img" + (apiShopInfo.getShopID() % 8) + ".paipaiimg.com/" + apiShopInfo.getMainLogoName());
			//shopWeiDian	商品所属店铺的微店地址[参照商品详情接口api/item/getItemBasic]
			shopJson.addProperty("shopWeiDian", "http://wd.paipai.com/mshop/" + apiShopInfo.getShopID());
			shopJson.add("goods", goods);
			shopJson.addProperty("newItemCount", shopBiz.getNewItemCountAndCache(Long.parseLong(id), getMk()));
			shopJson.addProperty("sellerCredit", sellerCredit);
			shopJson.addProperty("sellerCreditLevel", SellerCreditUtil.getSellerCreditLevel(sellerCredit));
			arr.add(shopJson);
		}
		
		//微店调用时需要解决跨域的问题--微店调用者：赵立飞
    	String callback = getRequest().getParameter("callback");
    	if(!StringUtil.isBlank(callback)){
    		callback = CoderUtil.decodeXssFilter(AntiXssHelper.htmlAttributeEncode(callback));
    		return doPrint(callback + "(" + out.setData(arr).toJsonStr() + ")");
    	}
		
		return doPrint(out.setData(arr));
	}
	
	
	public String hwShopItem() 	{
		JsonArray arr = new JsonArray();		
		Boolean isLogin = checkLoginAndGetSkey() ;
		String[] ids = shopIds.split(",") ;
		for(int i = 0; i< Math.min(ids.length, 40); i++) {			
			try {
				Long shopId = Long.parseLong( ids[i] ) ;
				ApiShopInfo apiShopInfo = shopClient.getShopInfo(shopId);//查询店铺基本信息
				if(0 == apiShopInfo.getShopID()) {
					continue;
				}						
				JsonObject shopJson = new JsonObject();
				shopJson.addProperty("shopId", shopId);
				shopJson.addProperty("shopLogo", "http://img" + (apiShopInfo.getShopID() % 8) + ".paipaiimg.com/" + apiShopInfo.getMainLogoName());
				shopJson.addProperty("shopName", apiShopInfo.getShopName());						
				//shopJson.addProperty("newItemCount", shopBiz.getNewItemCountAndCache(shopId, getMk()));	//使用缓存
				boolean isStore = false ;
				if (isLogin) {
					isStore = favoriteBiz.isShopInFav(getWid(), getMk(), getSk(), shopId);
				}		
				shopJson.addProperty("isStore", isStore);
				//查询最新的4个商品
				Map<String, String> filter2 = new HashMap<String, String>();
				filter2.put(CVItemFilterConstants.ITEM_FILTER_KEY_STATE, 
						CVItemFilterConstants.ITEM_FILTER_STATE_SELLING);	// 只查询出售中的商品
				ShopItems items = shopBiz.getShopComdyList(shopId, 
						getMk(), 0, 3, 
						CVItemFilterConstants.C2C_WW_COMDY_FIND_ORDERY_BY_ADDTIME_DESC, 
						filter2);
				
				int minSize = Math.min(4, items.getItems().size()) ;
				JsonArray jsonArrayTmp2 = new JsonArray() ;
				for (int j=0 ; j< minSize; j++) {
					JsonObject jsonObjectTemp2 = new JsonObject() ;
					ShopSimpleItem  simpleItem = items.getItems().get(j) ;
					jsonObjectTemp2.addProperty("itemCode", simpleItem.getItemId());
					jsonObjectTemp2.addProperty("itemName", simpleItem.getTitle());
					jsonObjectTemp2.addProperty("soldNum", simpleItem.getSoldNum());
					jsonObjectTemp2.addProperty("price", simpleItem.getPrice());
					jsonObjectTemp2.addProperty("itemPic", simpleItem.getImage120());
					jsonArrayTmp2.add(jsonObjectTemp2) ;
				}
				shopJson.add("itemList", jsonArrayTmp2);				

				arr.add(shopJson);
			} catch (Exception ex) {
				log.error(ex.getMessage(), ex);
			}
			
		}
		return doPrint(out.setData(arr));
	}
	
	/**
	 * 获取店铺活动信息
	 * @return
	 * @throws Exception
	 */
	public String getShopNewItemAndActive() throws Exception	{
		if(StringUtil.isBlank(shopIds)){
			out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER + "");
			out.setMsg("参数校验不合法");
			return SUCCESS;
		}
		
		JsonArray arr = new JsonArray();
		for(String id : shopIds.split(","))	{
			JsonObject shopJson = new JsonObject();
			shopJson.addProperty("shopId", id);
			shopJson.addProperty("active", ppItemBiz.getCurrentActive(Long.parseLong(id)));//店铺活动信息
			arr.add(shopJson);
		}
		return doPrint(out.setData(arr));
	}
	
	public String banner() throws IOException	{
		JsonObject j1 = new JsonObject(), j2 = new JsonObject();
		
		try {
			j1 = discoverBiz.discoverBanner().getAsJsonObject();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		try {
			j2 = discoverBiz.discoverSets().getAsJsonObject();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		for(Entry<String,JsonElement> e : j2.entrySet())	{
			j1.add(e.getKey(), e.getValue());
		}		//合并banner/sets输出
		return doPrint(out.setData(j1));
	}
	
	/**
	 * 与PC端优品推荐保持一致
	 * 固定的三个商品池
	 * 获取优品推荐相关商品
	 * 返回带详情的商品个数暂定为10
	 * 总个数暂定为100
	 * @return
	 * @throws Exception 
	 */
	public String recommendItems() throws Exception{
		long wid = parseToken();	
		//优品推荐使用商品池数组
		int[] poolIdArr = new int[]{
				BIConstant.RECOMMEND_POOLID_FIRST,
				BIConstant.RECOMMEND_POOLID_SECOND,
				BIConstant.RECOMMEND_POOLID_THIRD
			};
		//以下数据用来进行数据上报
		if(null != daps && !"".equals(daps.trim())){
			String[] updapArr = daps.split(",");
			for(String d : updapArr){
				try {
					Dap dap = Dap.fromDap62(d);
					long lCatId = ItemClient.fetchItemInfo(dap.getShopidOrItemcode())
							.getItemPo().getOItemBase().getLeafClassId();
					//暂时没有接口可以根据itemCode获取一级目录、二级目录和子目录
					ExposureMsg msg = ExposureMsg.buildItemMsg(d, 0L, 0L, lCatId, 
							wid, getHttpHeadIp(), getMk(), getMt(), getLongitude(), getLatitude(), "");
					biBiz.uploadMsg(msg);
				} catch (Exception e) {
					log.error("曝光数据上报失败:" + e.getMessage(), e);
				}
			}
		}
		//以上数据用来进行数据上报
		//获取所有pageSize数量的推荐优品基础信息
		List<ItemVo> vos = discoverBiz.discoverRecommendItems(getHttpHeadIp(), getMt(), getMk(),	
							getLongitude(), getLatitude(), wid, poolIdArr, pageSize);
		int size = vos.size();
		//要显示详情的优品的tabids
		int[] tabIdArr = new int[pageSize2];
		Map<String, JsonObject> dataMap = new LinkedHashMap<String,JsonObject>();
		for(int i = 0;i < size;i++)	{
			JsonObject json = new JsonObject();
			ItemVo item = vos.get(i);
			if(i < pageSize2){
				tabIdArr[i] = item.tabId;
			}
			String dap = item.dap;
			String itemCode = new ItemCode(item.sellerUin, item.itemId).toString();
			json.addProperty("itemCode", itemCode);
			json.addProperty("tabId", item.tabId);
			json.addProperty("dap", "".equals(dap.trim()) ? dap : dap.substring(0, dap.lastIndexOf(":")));
			json.addProperty("poolId", item.poolId);
			json.addProperty("uploadPicUrl1", "");
			json.addProperty("title", "");
			json.addProperty("price", 0);
			json.addProperty("url", "");
			dataMap.put(itemCode, json);
		}
		//获取指定pageSize2数量的优品详情
		List<TabData> tabs = ItemClient.getTabDetail(wid, getHttpHeadIp(), getMk(),	Util.arrToList(tabIdArr));
		for(TabData t : tabs){
			String itemCode = t.getStrCommodityId();
			if(dataMap.keySet().contains(itemCode)){
				JsonObject object = dataMap.get(itemCode);
				Map<String,String> custom = t.getMapCustomData();
				object.addProperty("uploadPicUrl1", custom.get("uploadPicUrl1"));
				object.addProperty("title", t.getStrTitle());
				object.addProperty("price", t.getDwActivePrice());
				object.addProperty("url", "http://auction1.paipai.com/" + t.getStrCommodityId());
			}
		}
		JsonArray items = new JsonArray();
		for(JsonObject obj : dataMap.values()){
			items.add(obj);
		}
		JsonObject json = new JsonObject();
		json.add("items", items);
		
		return doPrint(out.setData(json).toJsonStr());
	}
	
	/**
	 * 标签推荐，根据用户标签获取推荐商品信息
	 * @return
	 * @throws Exception 
	 */
	public String recommendItemsByTag() throws Exception	{
		long wid = parseToken();		
		int[] poolIdArr = new int[0];
		//以下数据用来进行数据上报
		if(null != daps && !"".equals(daps.trim())){
			String[] updapArr = daps.split(",");
			for(String d : updapArr){
				try {
					Dap dap = Dap.fromDap62(d);
					long lCatId = ItemClient.fetchItemInfo(dap.getShopidOrItemcode())
							.getItemPo().getOItemBase().getLeafClassId();
					//暂时没有接口可以根据itemCode获取一级目录、二级目录和子目录
					ExposureMsg msg = ExposureMsg.buildItemMsg(d, 0L, 0L, lCatId, 
							wid, getHttpHeadIp(), getMk(), getMt(), getLongitude(), getLatitude(), "");
					biBiz.uploadMsg(msg);
				} catch (Exception e) {
					log.error("曝光数据上报失败:" + e.getMessage(), e);
				}
			}
		}
		//以上数据用来进行数据上报
		if(poolIds != null && !"".equals(poolIds))	{
			poolIdArr = Util.toIntArray(poolIds.split(","));
		}
		//获取所有pageSize数量的推荐优品基础信息
		List<ItemVo> vos = discoverBiz.discoverItem(getHttpHeadIp(), getMt(), getMk(),	
							getLongitude(), getLatitude(), wid, poolIdArr, pageSize);
		int size = vos.size();
		//要显示详情的商品的tabids
		int[] tabIdArr = new int[pageSize2];
		Map<String, JsonObject> dataMap = new LinkedHashMap<String,JsonObject>();
		for(int i = 0;i < size;i++)	{
			JsonObject json = new JsonObject();
			ItemVo item = vos.get(i);
			if(i < pageSize2){
				tabIdArr[i] = item.tabId;
			}
			String dap = item.dap;
			String itemCode = new ItemCode(item.sellerUin, item.itemId).toString();
			json.addProperty("itemCode", itemCode);
			json.addProperty("tabId", item.tabId);
			json.addProperty("dap", "".equals(dap.trim()) ? dap : dap.substring(0, dap.lastIndexOf(":")));
			json.addProperty("poolId", item.poolId);
			json.addProperty("uploadPicUrl1", "");
			json.addProperty("title", "");
			json.addProperty("price", 0);
			json.addProperty("url", "");
			dataMap.put(itemCode, json);
		}
		//获取指定pageSize2数量的优品详情
		List<TabData> tabs = ItemClient.getTabDetail(wid, getHttpHeadIp(), getMk(),	Util.arrToList(tabIdArr));
		for(TabData t : tabs){
			String itemCode = t.getStrCommodityId();
			if(dataMap.keySet().contains(itemCode)){
				JsonObject object = dataMap.get(itemCode);
				Map<String,String> custom = t.getMapCustomData();
				object.addProperty("uploadPicUrl1", custom.get("uploadPicUrl1"));
				object.addProperty("title", t.getStrTitle());
				object.addProperty("price", t.getDwActivePrice());
				object.addProperty("url", "http://auction1.paipai.com/" + t.getStrCommodityId());
			}
		}
		JsonArray items = new JsonArray();
		for(JsonObject obj : dataMap.values()){
			items.add(obj);
		}
		JsonObject json = new JsonObject();
		json.add("items", items);
		
		return doPrint(out.setData(json).toJsonStr());
	}

	/**
	 * 海外购推荐
	 * @return
	 * @throws Exception
	 */
	public String recommendItemsByMart() throws Exception {

		JsonArray items = discoverBiz.discoverMartItems(activeId, poolId, pageSize,sortType);
		JsonObject json = new JsonObject();
		json.add("items", items);
		return doPrint(out.setData(json).toJsonStr());
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	public int getPageSize2() {
		return pageSize2;
	}

	public void setPageSize2(int pageSize2) {
		this.pageSize2 = pageSize2;
	}

	public JsonOutput getOut() {
		return out;
	}

	public void setOut(JsonOutput out) {
		this.out = out;
	}

	public void setDiscoverBiz(DiscoverBiz discoverBiz) {
		this.discoverBiz = discoverBiz;
	}

	public String getTabIds() {
		return tabIds;
	}

	public void setTabIds(String tabIds) {
		this.tabIds = tabIds;
	}

	public String getPoolIds() {
		return poolIds;
	}

	public void setPoolIds(String poolIds) {
		this.poolIds = poolIds;
	}
	public void setBiBiz(BiBiz biBiz) {
		this.biBiz = biBiz;
	}

	public long getMsgId() {
		return msgId;
	}

	public void setMsgId(long msgId) {
		this.msgId = msgId;
	}

	public String getDaps() {
		return daps;
	}

	public void setDaps(String daps) {
		this.daps = daps;
	}

	public String getShopIds() {
		return shopIds;
	}

	public void setShopIds(String shopIds) {
		this.shopIds = shopIds;
	}

	public int getCount2() {
		return count2;
	}

	public void setCount2(int count2) {
		this.count2 = count2;
	}

	public void setFavoriteBiz(FavoriteBiz favoriteBiz) {
		this.favoriteBiz = favoriteBiz;
	}

	public void setShopBiz(ShopBiz shopBiz) {
		this.shopBiz = shopBiz;
	}

	public void setItemBiz(ItemBiz itemBiz) {
		this.itemBiz = itemBiz;
	}

	public void setPpItemBiz(PPItemBiz ppItemBiz) {
		this.ppItemBiz = ppItemBiz;
	}
	
}
