package com.qq.qqbuy.discover.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Vector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.qq.qqbuy.bi.biz.BiBiz;
import com.qq.qqbuy.bi.biz.Dap;
import com.qq.qqbuy.bi.biz.ExposureMsg;
import com.qq.qqbuy.common.JsonOutput;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.PaiPaiConfig;
import com.qq.qqbuy.common.action.AuthRequiredAction;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.common.util.ItemCode;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.discover.biz.DiscoverBiz;
import com.qq.qqbuy.discover.biz.ItemVo;
import com.qq.qqbuy.discover.biz.RecommendBiz;
import com.qq.qqbuy.discover.util.Constants;
import com.qq.qqbuy.favorite.biz.FavoriteBiz;
import com.qq.qqbuy.item.biz.ItemBiz;
import com.qq.qqbuy.item.biz.PPItemBiz;
import com.qq.qqbuy.shop.biz.ShopBiz;
import com.qq.qqbuy.shop.po.ShopItems;
import com.qq.qqbuy.shop.po.ShopSimpleItem;
import com.qq.qqbuy.thirdparty.idl.item.ItemClient;
import com.qq.qqbuy.thirdparty.idl.item.protocol.FetchItemInfoResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.TabData;
import com.qq.qqbuy.thirdparty.idl.shop.CVItemFilterConstants;
import com.qq.qqbuy.thirdparty.idl.shop.ShopClient;
import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiShopInfo;

public class RecommendAction extends AuthRequiredAction{	
	static Logger log = LogManager.getLogger(RecommendAction.class);
	
	BiBiz biBiz;
	ShopBiz shopBiz;
	RecommendBiz recommendBiz;
	FavoriteBiz favoriteBiz;
	DiscoverBiz discoverBiz;
	ItemBiz itemBiz;
	PPItemBiz ppItemBiz;
	private ShopClient shopClient = new ShopClient();
	JsonOutput out = new JsonOutput();
	Gson gson = new GsonBuilder().serializeNulls().create();
	
	/**
	 * tabids: COSS商品的tabId,多个以逗号隔开
	 */
	String ids, tabIds, poolIds, daps, shopIds, ics,keys;
	int pageSize = 10,pageSize2 = 10, count2 = 4, categoryId, isVirtual = 0/*0：实物；1：虚拟*/;
	int psGoodsPPY = 5,psShopWD = 5, psGoodsGlobal=15;
	long msgId;
	
	private long parseToken()	{
		long wid = getWidFromToken(getAppToken());
		return wid == -1 ? 0 : wid;
	}
	
	
	/**
	 * 支付成功页  商品推荐. 
	 * 现在购物车下没有推荐，所以这里改为支付成功页面了，对应的场景id做了修改：com.qq.qqbuy.discover.biz.impl.RecommendBizImpl.discoverItem(String, String, long, int)
	 * 以后需要购物车推荐商品时再添加参数区分，因为两者场景id不同。
	 * 201502012
	 * 刘本龙
	 * @return
	 * @throws Exception 
	 */
	public String recommendGoods() throws Exception	{
		
//		log.debug("RecommendAction.recommendGoods()::::com.qq.qqbuy.discover.web.RecommendAction.list()"+
//				";ics:"+ics+";categoryId:"+categoryId);
		
		long wid = getWidFromToken(getAppToken());		
		
		JsonArray array = new JsonArray();
		
		//如果是虚拟商品
		if(isVirtual == 1){
			try {//这里与发现页推荐的全站商品goodsGlobal是一样的（recommendShow）
				String url = Constants.DISCOVER_SHOW_URL.replace(Constants.DISCOVER_BI_HOSTS, PaiPaiConfig.getPaipaiCommonIp()).replace("$SF", Constants.RECOMMEND_GOODS_GLOBAL_SF)
						.replace("$HID", Constants.RECOMMEND_GOODS_GLOBAL_HID).replace("$MID", Constants.RECOMMEND_GOODS_GLOBAL_MID)
						.replace("$TYPE", Constants.RECOMMEND_GOODS_GLOBAL_TYPE).replace("$UIN", String.valueOf(wid))
						.replace("$IP", getHttpHeadIp()).replace("$PAGESIZE", String.valueOf(pageSize)) ;
				
				String strResult = HttpUtil.get(url,Constants.DISCOVER_BI_HOSTS, Constants.connectTimeout, Constants.readTimeout, "GBK", false);
				
				JsonParser jp = new JsonParser();
				JsonObject jsonResult = (JsonObject)jp.parse(strResult);
				if (jsonResult.get("retCode").getAsString().equals("0")) {
					JsonArray jsonArrayTmp = jsonResult.get("data").getAsJsonObject().get("111").getAsJsonArray() ;
					for (int i=0;i<jsonArrayTmp.size();i++) {
						JsonObject jsonObjectTmp = jsonArrayTmp.get(i).getAsJsonObject() ;
						JsonObject jsonObject = new JsonObject() ;
						String itemCode = jsonObjectTmp.get("strCommodityId").getAsString();
						jsonObject.addProperty("itemCode", itemCode);		
						jsonObject.addProperty("title", jsonObjectTmp.get("strTitle").getAsString());
						
						try {
							String[] split = jsonObjectTmp.get("strDap").getAsString().split(":");
							Dap dap = new Dap(split[0], split[1], Long.parseLong(split[2]), split[3]);
							String to62Str = dap.to62Str();
							jsonObject.addProperty("dap", to62Str.substring(0, to62Str.lastIndexOf(":")));		
						} catch (NumberFormatException e) {
							Log.run.error(e.getMessage(), e);
						} catch (Exception e) {
							Log.run.error(e.getMessage(), e);
						}
						
						Number dwActivePrice = jsonObjectTmp.get("dwActivePrice").getAsNumber();
						if(null != dwActivePrice && 0 != dwActivePrice.longValue()){//优先取活动价，没有活动价则取全局商品价
							jsonObject.addProperty("price", dwActivePrice);
						}else{
							jsonObject.addProperty("price", jsonObjectTmp.get("dwPrice").getAsNumber());
						}
						jsonObject.addProperty("imgUrl", jsonObjectTmp.get("strItemPic").getAsString());
						array.add(jsonObject);
					}
				}
			} catch(Exception ex) {
				Log.run.info(ex.toString(), ex) ;
			}
		}else{
			if(null == ics || "".equals(ics))	{
				setErrCodeAndMsg(ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验不合法");
				Log.run.error("RecommendAction.recommendGoods() ics:"+ics);
				//购买的商品ID为空了。！
				out.setErrCode("256");
				out.setMsg("用户收藏或者购买的商品ics为空了！无法推荐商品！");
				return doPrint(out.toJsonStr());
			}
			
			List<ItemVo> vos = recommendBiz.discoverItem(getHttpHeadIp(), ics, wid, pageSize);
			Log.run.info("RecommendAction.recommendGoods() vos size :" + vos.size());
			///购物车这个是从全站商品里面推荐的。使用的是华宇的接口。
			for (ItemVo i : vos) {
				String img = "";
				JsonObject json = new JsonObject();
				FetchItemInfoResp itemInfo = ItemClient.fetchItemInfo(new ItemCode(i.sellerUin, i.itemId).toString());
				if (itemInfo != null && itemInfo.getResult() == 0) {
					json.addProperty("itemCode", itemInfo.getItemPo().getOItemBase().getItemId());
					json.addProperty("title", itemInfo.getItemPo().getOItemBase().getTitle());
					Vector<String> imgs = itemInfo.getItemPo().getOItemImg().getVecImg();
					if(null != imgs && !imgs.isEmpty()){
						img = imgs.get(0);
					}
					json.addProperty("imgUrl", img);
					json.addProperty("price", itemInfo.getItemPo().getOItemBase().getPrice());
					json.addProperty("dap", "".equals(i.dap.trim()) ? i.dap : i.dap.substring(0, i.dap.lastIndexOf(":")));
					
					array.add(json);
				}			
			}
		}
		
		return doPrint(out.setData(array).toJsonStr());
	}
	
	
	/**
	 * 推荐店铺
	 * pageSize		取得多少店铺ID
	 * pageSize2	前多少个店铺需要全量信息
	 * @author liubenlong3
	 */
	public String recommendShop() throws Exception	{
		long wid = getWidFromToken(getAppToken());
		
		//数据上报开始
		if(null != daps && !"".equals(daps)){
			for(String s : daps.split(","))	{
				try {
					Dap dap = Dap.fromDap62(s);
					long lCatId = ItemClient.fetchItemInfo(dap.getShopidOrItemcode())
							.getItemPo().getOItemBase().getLeafClassId();
					ExposureMsg msg = ExposureMsg.buildItemMsg(s, 0L, 0L, lCatId,	//TODO 怎么取三级类目ID
							wid, getHttpHeadIp(), getMk(), getMt(), getLongitude(), getLatitude(), "");
					biBiz.uploadMsg(msg);
				} catch (Exception e) {
					log.error("数据上报失败。"+e.getMessage(), e);
				}
			}
		}
		//数据上报结束
		
		List<Long> shopIds = new ArrayList<Long>();
		try {
			List<Dap> shopDaps = discoverBiz.discoverShop(getHttpHeadIp(), getMt(), getMk(), 
									getLongitude(), getLatitude(), wid, pageSize);
			for(Dap d : shopDaps)	{
				shopIds.add(Long.parseLong(d.getShopidOrItemcode()));
			}
		} catch (Exception e1) {
			log.error(e1.getMessage(), e1);
		}
		int totalSize = shopIds.size();
		int endIndex = totalSize > pageSize2 ? pageSize2 : totalSize;
		
		List<Long> subListWithData = shopIds.subList(0, endIndex);
		//不带详情的数据
		List<Long> subList = shopIds.subList(endIndex, totalSize);
		
		/**
		 * 查询店铺信息
		 * 这里可以转发到http://app.paipai.com/api/discover/shopItem.xhtml 接口！！！2014.10.7
		 */
		JsonArray arr = new JsonArray();
		
		for (final long shopId : subListWithData) {
			try {
				ApiShopInfo apiShopInfo = shopClient.getShopInfo(shopId);//查询店铺基本信息
				if(0 == apiShopInfo.getShopID())	{
					continue;
				}
				List<String> jointPics = shopBiz.joinPicAndCache(shopId);
				if(jointPics.isEmpty())	{
					continue;	//没有图片说明该商店没有可售商品, 去除. 以后可能用不到了,这种店铺直接由BI推荐时过滤
				}
				
				JsonObject shopJson = new JsonObject();
				shopJson.addProperty("shopId", apiShopInfo.getShopID() + "");
				shopJson.addProperty("shopName", apiShopInfo.getShopName());
				shopJson.addProperty("logo", "http://img" + (apiShopInfo.getShopID() % 8) + ".paipaiimg.com/" + apiShopInfo.getMainLogoName());
				shopJson.add("itemPic", new Gson().toJsonTree(jointPics));
				shopJson.addProperty("newItemCount", shopBiz.getNewItemCountAndCache(shopId, getMk()));
				arr.add(shopJson);
			} catch (Exception e) {
				log.error(e.getMessage(), e);	//防止单个店铺出问题时影响整个接口
			}
		}
		
		for (Long l : subList) {
			JsonObject shopJson = new JsonObject();
			shopJson.addProperty("shopId", l.toString());
			arr.add(shopJson);
		}

		return doPrint(out.setData(arr));
	}
	
	/**
	 * 推荐发现综合推荐（5个拍便宜商品，15个全站商品，10个店铺，每个店铺含4个最新上架商品）
	 * @author liubenlong3
	 */
	public String recommendShow() throws Exception	{
		long wid = getWidFromToken(getAppToken());
		JsonObject jsonObject = new JsonObject();
		psGoodsGlobal = 15 ;
		psGoodsPPY = 5 ;
		psShopWD = 5 ;
		String ip = getHttpHeadIp() ;
		JsonArray goodsGlobal = this.discoverBiz.recommendGoodsGlobal(wid, ip, psGoodsGlobal) ;
		JsonArray goodsPPY = this.discoverBiz.recommendGoodsPPY(wid, ip, psGoodsPPY) ;
		JsonArray shopWD = this.discoverBiz.recommendShopWD(wid, ip, psShopWD,getMk()) ;
		//begin IOS处理逻辑有问题，暂时在微店数不够5个情况下，再请求推荐接口推荐给用户店铺，凑够5个
		if (psShopWD > shopWD.size()) {
			try {
				Set<Long> shopIdSet = new HashSet<Long>() ;
				for(int i=0; i<shopWD.size(); i++) {
					shopIdSet.add(shopWD.get(i).getAsJsonObject().get("shopId").getAsLong());//防重处理
				}				
				List<Dap> shopDaps = discoverBiz.discoverShop(getHttpHeadIp(), getMt(), getMk(), 
										getLongitude(), getLatitude(), wid, 10);	//多取防重			
				for (Dap shopDap : shopDaps) {
					if (shopWD.size() >= psShopWD) {
						break ;
					}
					try {
						long shopId = Long.parseLong(shopDap.getShopidOrItemcode());
						if (shopIdSet.contains(shopId)) {
							continue ;
						}
						ApiShopInfo apiShopInfo = shopClient.getShopInfo(shopId);//查询店铺基本信息
						if(0 == apiShopInfo.getShopID()) {
							continue;
						}
						JsonObject shopJson = new JsonObject();
						shopJson.addProperty("shopId", shopId);
						
						String to62Str = shopDap.to62Str();
						shopJson.addProperty("dap", to62Str.substring(0, to62Str.lastIndexOf(":")));
						
						shopJson.addProperty("shopLogo", "http://img" + (apiShopInfo.getShopID() % 8) + ".paipaiimg.com/" + apiShopInfo.getMainLogoName());
						shopJson.addProperty("shopName", apiShopInfo.getShopName());						
						shopJson.addProperty("newItemCount", shopBiz.getNewItemCountAndCache(shopId, getMk()));	//使用缓存
						//查询最新的4个商品
						Map<String, String> filter2 = new HashMap<String, String>();
						filter2.put(CVItemFilterConstants.ITEM_FILTER_KEY_STATE, 
								CVItemFilterConstants.ITEM_FILTER_STATE_SELLING);	// 只查询出售中的商品
						ShopItems items = shopBiz.getShopComdyList(shopId, 
								getMk(), 0, 4, 
								CVItemFilterConstants.C2C_WW_COMDY_FIND_ORDERY_BY_ADDTIME_DESC, 
								filter2);
						
						int minSize = Math.min(4, items.getItems().size()) ;
						JsonArray jsonArrayTmp2 = new JsonArray() ;
						for (int j=0 ; j< minSize; j++) {
							JsonObject jsonObjectTemp2 = new JsonObject() ;
							ShopSimpleItem  simpleItem = items.getItems().get(j) ;
							jsonObjectTemp2.addProperty("itemCode", simpleItem.getItemId());
							jsonObjectTemp2.addProperty("itemName", simpleItem.getTitle());
							jsonObjectTemp2.addProperty("soldNum", simpleItem.getSoldNum());
							jsonObjectTemp2.addProperty("price", simpleItem.getPrice());
							jsonObjectTemp2.addProperty("itemPic", simpleItem.getImage120());
							jsonArrayTmp2.add(jsonObjectTemp2) ;
						}
						shopJson.add("itemList", jsonArrayTmp2);
						shopWD.add(shopJson);
						shopIdSet.add(shopId) ;						
					} catch (Exception e) {
						log.error(e.getMessage(), e);	//防止单个店铺出问题时影响整个接口
					}
				}
			} catch (Exception e1) {
				log.error(e1.getMessage(), e1);
			}
		}
		//end IOS处理逻辑有问题，暂时在微店数不够5个情况下，再请求推荐接口推荐给用户店铺，凑够5个
		jsonObject.addProperty("wid", wid);
		jsonObject.addProperty("mk", getMk());
		jsonObject.add("shopWD", shopWD);
		jsonObject.add("goodsGlobal", goodsGlobal);
		jsonObject.add("goodsPPY", goodsPPY) ;
		return doPrint(out.setData(jsonObject));
	} 
	
	
	/**
	 * 推荐首页综合推荐（店铺s、优品i、标签c、活动p），含曝光上报集成到这个接口。
	 * 优品不在这里取了***
	 * @author liubenlong3
	 */
	public String recommendMultiple() throws Exception	{
		long wid = getWidFromToken(getAppToken());

		//数据上报开始
		if(null != daps && !"".equals(daps)){
			for(String s : daps.split(","))	{
				try {
					Dap dap = Dap.fromDap62(s);
					long lCatId = ItemClient.fetchItemInfo(dap.getShopidOrItemcode())
							.getItemPo().getOItemBase().getLeafClassId();
					ExposureMsg msg = ExposureMsg.buildItemMsg(s, 0L, 0L, lCatId,	//TODO 怎么取三级类目ID
							wid, getHttpHeadIp(), getMk(), getMt(), getLongitude(), getLatitude(), "");
					biBiz.uploadMsg(msg);
				} catch (Exception e) {
					log.error("数据上报失败。"+e.getMessage(), e);
				}
			}
		}
		//数据上报结束

		/*
		 * 计算优品和标签需要请求的数据量。
		 * 店铺20%，活动10%。但是PPMS中数据不一定有那么多的数据。如果不够则用优品和标签推荐的顶替。
		 */
		int countS = (int)(pageSize*0.2),	//店铺s
			countP = (int)(pageSize*0.1);	//活动p
		
		//获取推荐店铺
		List<Dap> shopIds = new ArrayList<Dap>();
		try {
			shopIds = discoverBiz.discoverShop(getHttpHeadIp(), getMt(), getMk(), 
										getLongitude(), getLatitude(), wid, countS);
//			for(Dap d : shopDaps)	{
//				shopIds.add(Long.parseLong(d.getShopidOrItemcode()));
//			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		//获取活动列表
		JsonArray acts = new JsonArray();
		try {
			acts = discoverBiz.discoverAct().getAsJsonArray();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		if(countS > shopIds.size()) 
			countS = shopIds.size();
		if(countP > acts.size()) 
			countP = acts.size();
		int countC = pageSize - countS - countP;	//标签c
		
//		Log.run.debug("countS:"+countS+",countP:"+countP+",countC:"+countC);
		
		List<Dap> shopIdLists = shopIds.subList(0, countS);
		
		List<JsonObject> actLists = new ArrayList<JsonObject>();
		for (JsonElement jsonElement : acts) {
			actLists.add(jsonElement.getAsJsonObject());
		}
		Collections.shuffle(actLists);
		actLists = actLists.subList(0, countP);
		
		List<JsonObject> listTag = getTags(countC);
		
		JsonObject [] o = new JsonObject[this.pageSize];//定义一个pagesize（总数据量）大小的数组
		Log.run.debug("RecommendAction.recommendMultiple()--shopIdLists:"+shopIdLists.size()+",actLists:"+actLists.size()+",listTag:"+listTag.size());
		
		sort(o, shopIdLists, actLists, listTag);		//排序
		
		JsonArray array = new JsonArray();
		JsonObject tmpAct = chooseTmpAct();
		if(tmpAct != null)	{
			array.add(tmpAct);		//根据需要添加一个临时活动
		}
		
//		Log.run.debug("sort end....o.length:" + o.length);
		
		getInfo(o);//获取详情
		
//		Log.run.debug("getInfo end....o.length:" + o.length);
		
		for (int i = 0 ; i < o.length ; i ++) {
			if(o[i] != null)	{	//可能会有空数据,例如移除某些无可售商品的店铺后
//				if(i >= pageSize2 && //暂时先取全部的详情。手机端如果改为分页请求（pagesize2生效），这里在修改。同时修改getInfo方法
//						("C".equals(o[i].get("t").getAsString()) 
//								|| "I".equals(o[i].get("t").getAsString()))){
//					o[i].remove("itemCode");
//				}
				//下架商品屏蔽
				String type = o[i].get("t").getAsString();
				if("C".equals(type)){
					JsonElement priceNode = o[i].get("price");
					if(null != priceNode && !priceNode.isJsonNull()){
						long price = priceNode.getAsLong();
						if (price > 0 ){
							array.add(o[i]);
						}
					}else{
						Log.run.info("RecommendAction#recommendMultiple#badItem is:"+o[i].toString());
					}
				}else{
					array.add(o[i]);
				}
			}
		}
//		Log.run.debug("array.size:" + array.size());
		
		return doPrint(out.setData(array));
	}
	
	/**
	 * 临时活动
	 * @return
	 * @throws ParseException 
	 */
	private JsonObject chooseTmpAct() throws ParseException	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		long cur = System.currentTimeMillis();
		String[] arr;
		if(cur > sdf.parse("2014-12-08 00:00:00").getTime() && cur < sdf.parse("2014-12-13 00:00:00").getTime())	{
			arr = new String[]{"http://www.paipai.com/promote/2014/4559/index.shtml?=20486.2.2", "12.12新节拍", "http://pics3.paipaiimg.com/update/20141205/3c_154022472.jpg"};
		} else	{
			return null;
		}
		
		JsonObject json = new JsonObject();
        json.addProperty("id", 2343242);	//随便写了个ID
		json.addProperty("url", arr[0]);
		json.addProperty("title", arr[1]);
		json.addProperty("img", arr[2]);
		json.addProperty("t", "P");
		return json;
	}
	
	private void getInfo(JsonObject[] o) throws BusinessException {
		List<Integer> tabIds = new ArrayList<Integer>();
		
		//店铺s、标签c、活动p
		for (int i=0; i < o.length; i++) {
			if(o[i] == null)	continue;
			String t = o[i].get("t").getAsString();
			if("S".equals(t)){
				long shopId = o[i].get("shopId").getAsLong();
//				ShopInfo s = shopBiz.getShopInfo(shopId);
				ApiShopInfo apiShopInfo = shopClient.getShopInfo(shopId);//查询店铺基本信息
				if(0 == apiShopInfo.getShopID())	{
					continue;
				}
				
				List<String> jointPics = shopBiz.joinPicAndCache(shopId);
				if(jointPics.isEmpty())	{
//					Log.run.debug("移除"+o[i].toString());
					
					o[i] = null;		//移除无可售商品的店铺
				} else	{
					o[i].addProperty("shopName", apiShopInfo.getShopName());
					o[i].addProperty("logo", "http://img" + (apiShopInfo.getShopID() % 8) + ".paipaiimg.com/" + apiShopInfo.getMainLogoName());
					o[i].add("itemPic", new Gson().toJsonTree(jointPics));
					o[i].addProperty("newItemCount", shopBiz.getNewItemCountAndCache(shopId, getMk()));
				}
			} else if("C".equals(t) || "I".equals(t)){
				tabIds.add(o[i].get("tabId").getAsInt());
			}
		}
		
		if(tabIds.size() > 0)	{
			List<TabData> tabs = ItemClient.getTabDetail(parseToken(), getHttpHeadIp(), getMk(), tabIds);
			for (TabData t : tabs) {
				for (JsonObject json : o) {
					if(json == null)	continue;
					if("C".equals(json.get("t").getAsString()) || "I".equals(json.get("t").getAsString())){
						if(t.getStrCommodityId().equals(json.get("itemCode").getAsString()) && t.getDwActivePrice() > 0){
							
							Log.run.debug("json.get(itemCode).getAsString():"+json.get("itemCode").getAsString());
							
							Map<String,String> custom = t.getMapCustomData();
							json.addProperty("uploadPicUrl1", custom.get("uploadPicUrl1"));
							json.addProperty("title", t.getStrTitle());
							json.addProperty("price", t.getDwActivePrice());
							json.addProperty("url", "http://auction1.paipai.com/" + t.getStrCommodityId());
						}
					}
				}
			}
		}
	}


	private void sort(JsonObject[] o, List<Dap> shopIdLists,
			List<JsonObject> actLists, List<JsonObject> listTag) {
		/* 
		 * 排序
		 * 前4个要有一个店铺，一个广告，其他随意
		 * 接下来每10个要有一个广告，两个店铺。其他随意。
		 */ 
		//前4个
		ArrayList<Integer> randomNoRepeat = getRandomNoRepeat(0,4,2);
		ArrayList<Integer> temp = new ArrayList<Integer>();
		if(shopIdLists.size() > 0){
			JsonObject object = new JsonObject();
			object.addProperty("t", "S");
			object.addProperty("shopId", shopIdLists.get(0).getShopidOrItemcode());
			String to62Str = shopIdLists.get(0).to62Str();
			object.addProperty("dap", to62Str.substring(0, to62Str.lastIndexOf(":")));
			
			o[randomNoRepeat.get(0)] = object;
			shopIdLists.remove(0);
			temp.add(randomNoRepeat.get(0));
		}
		
		if(actLists.size()>0){
			JsonObject jsonObject = actLists.get(0);
			jsonObject.addProperty("t", "P");
			
			o[randomNoRepeat.get(1)] = jsonObject;
			actLists.remove(0);
			temp.add(randomNoRepeat.get(1));
		}
		
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < 4; i++) {
			list.add(i);
		}
		list.removeAll(temp);
		for (Integer i : list) {
			if(getRandomNoRepeat(0,2,1).get(0)==0){
				if(listTag.size() > 0){
					o[i] = listTag.get(0);
					listTag.remove(0);
				}else break;
//				countCI ++ ;
			}else{
				if(listTag.size() > 0){
					o[i] = listTag.get(0);
					listTag.remove(0);
				}else break;
//				countCI ++ ;
			}
		}
		//----------------------
		///接下来每10个
		for(int begin = 4 ; begin < pageSize ; begin = begin + 10){
			int end = (begin + 10)>pageSize?pageSize:(begin + 10);
			
//			Log.run.debug("begin："+begin+",end:"+end);
			
			temp.clear();
			
			//其中三个有1个是广告，两个店铺
			randomNoRepeat = getRandomNoRepeat(begin, end,3);
			if(shopIdLists.size() > 0){
				JsonObject object = new JsonObject();
				object.addProperty("t", "S");
				object.addProperty("shopId", shopIdLists.get(0).getShopidOrItemcode());
				
				String to62Str = shopIdLists.get(0).to62Str();
				object.addProperty("dap", to62Str.substring(0, to62Str.lastIndexOf(":")));
				
				o[randomNoRepeat.get(0)] = object;
				shopIdLists.remove(0);
				temp.add(randomNoRepeat.get(0));
			}
			
			if(actLists.size() > 0){
				JsonObject jsonObject = actLists.get(0);
				jsonObject.addProperty("t", "P");
				
				o[randomNoRepeat.get(1)] = jsonObject;
				actLists.remove(0);
				temp.add(randomNoRepeat.get(1));
			}
			
			if(shopIdLists.size() > 0){
				JsonObject object = new JsonObject();
				object.addProperty("t", "S");
				object.addProperty("shopId", shopIdLists.get(0).getShopidOrItemcode());
				
				String to62Str = shopIdLists.get(0).to62Str();
				object.addProperty("dap", to62Str.substring(0, to62Str.lastIndexOf(":")));
				
				o[randomNoRepeat.get(2)] = object;
				shopIdLists.remove(0);
				temp.add(randomNoRepeat.get(2));
			}
			
			
			list.clear();
			for (int i = begin; i < end; i ++) {
				list.add(i);
			}
			list.removeAll(temp);
			for (Integer i : list) {
				if(getRandomNoRepeat(0,2,1).get(0)==0){
					if(listTag.size() > 0){
						o[i] = listTag.get(0);
						listTag.remove(0);
					}else break;
//					if(i < this.pageSize2)countCI ++ ;
				}else{
					if(listTag.size() > 0){
						o[i] = listTag.get(0);
						listTag.remove(0);
					}else break;
//					if(i < this.pageSize2)countCI ++ ;
				}
			}
			
//			for (int i = begin ; i < end; i ++) {
//				Log.run.debug(o[i].toString());
//			}
		}
		
	}

	private List<JsonObject> getTags(int countC) throws Exception {
		//取标签推荐--数量按照上面计算结果取
		int[] poolIdArr = new int[0];
		if(poolIds != null && !"".equals(poolIds))	{
			poolIdArr = Util.toIntArray(poolIds.split(","));
		}
		List<ItemVo> miniTag = discoverBiz.discoverItem(getHttpHeadIp(), getMt(), getMk(),	
							getLongitude(), getLatitude(), parseToken(), poolIdArr, countC);
		List<JsonObject> listTag = new ArrayList<JsonObject>();
		int size = miniTag.size();
		for(int i = 0;i < size;i++)	{
			JsonObject json = new JsonObject();
			ItemVo item = miniTag.get(i);
			String dap = item.dap;
			String itemCode = new ItemCode(item.sellerUin, item.itemId).toString();
			json.addProperty("t", "C");
			json.addProperty("itemCode", itemCode);
			json.addProperty("tabId", item.tabId);
			json.addProperty("dap", "".equals(dap.trim()) ? dap : dap.substring(0, dap.lastIndexOf(":")));
			json.addProperty("poolId", item.poolId);
			listTag.add(json);
		}
		return listTag;
	}


//	private List<JsonObject> getVos(int countI) {
//		//取优品推荐--数量按照上面计算结果取
//		
//		//优品推荐使用商品池数组
//		int[] poolIdArr = new int[]{
//				BIConstant.RECOMMEND_POOLID_FIRST,
//				BIConstant.RECOMMEND_POOLID_SECOND,
//				BIConstant.RECOMMEND_POOLID_THIRD
//			};
//		
//		List<ItemVo> vos = null;
//		try {
//			vos = discoverBiz.discoverRecommendItems(getHttpHeadIp(), getMt(), getMk(),	
//					getLongitude(), getLatitude(), parseToken(), poolIdArr, countI);
//		} catch (Exception e) {
//			log.error(e.getMessage(), e);
//		}
//		
//		List<JsonObject> listVos = new ArrayList<JsonObject>();
//		int size = vos.size();
//		for(int i = 0;i < size;i++)	{
//			JsonObject json = new JsonObject();
//			ItemVo item = vos.get(i);
//			String dap = item.dap;
//			String itemCode = Util.encryptItemCode(item.sellerUin, item.itemId);
//			json.addProperty("t", "I");
//			json.addProperty("itemCode", itemCode);
//			json.addProperty("tabId", item.tabId);
//			json.addProperty("dap", "".equals(dap.trim()) ? dap : dap.substring(0, dap.lastIndexOf(":")));
//			json.addProperty("poolId", item.poolId);
//			listVos.add(json);
//		}
//		
//		return listVos;
//	}
	
	
	
	
	
	/**
	 * 推荐首页综合推荐（店铺s、优品i、标签c、活动p），含曝光上报集成到这个接口。
	 * 
	 * 手机端翻页  获取数据详情
	 * @author liubenlong3
	 */
	public String RMNextPage() throws Exception	{
		
		if(null == keys || "".equals(keys)){
			throw new IllegalArgumentException("null error");
		}
		
		String[] split = keys.split(",");
		int[] tabIdArrI = new int[split.length];
		int[] tabIdArrC = new int[split.length];
		
		JsonObject[] jsonObjects = new JsonObject[split.length];
		
		for (int i = 0 ; i < split.length ; i ++) {
			String type = "",id = "";
			try {
				String[] split2 = split[i].split("\\|");
				type = split2[0];
				id = split2[1];
			} catch (Exception e) {
				throw new IllegalArgumentException("参数格式不正确。");
			}
			
			if("S".equals(type)){
//				ShopInfo s = shopBiz.getShopInfo(Long.parseLong(id));
				ApiShopInfo apiShopInfo = shopClient.getShopInfo(Long.parseLong(id));//查询店铺基本信息
				if(0 == apiShopInfo.getShopID())	{
					continue;
				}
				List<String> jointPic = shopBiz.joinPicAndCache(Long.parseLong(id));
				
				JsonObject object = new JsonObject();
				object.addProperty("t", "S");
				object.addProperty("shopId", id);
				object.addProperty("shopName", apiShopInfo.getShopName());
				object.addProperty("logo", "http://img" + (apiShopInfo.getShopID() % 8) + ".paipaiimg.com/" + apiShopInfo.getMainLogoName());
				object.add("itemPic", new Gson().toJsonTree(jointPic));
				object.addProperty("newItemCount", shopBiz.getNewItemCountAndCache(Long.parseLong(id), getMk()));
				
				jsonObjects[i] =  object;
			}else if("C".equals(type)){
				tabIdArrC[i] = Integer.parseInt(id);
			}else if("I".equals(type)){
				tabIdArrI[i] = Integer.parseInt(id);
			}
		}
		
		List<TabData> tabs = ItemClient.getTabDetail(parseToken(), getHttpHeadIp(), getMk(), Util.arrToList(tabIdArrC));
		
		if(null !=  tabs)
			for(int i = 0 ; i< tabIdArrC.length ; i ++){
				if(tabIdArrC[i] != 0){
					JsonObject object = new JsonObject();
					
					object .addProperty("t", "C");
					object .addProperty("tabId", split[i].split("\\|")[1]);
					String itemCode = tabs.get(i).getStrCommodityId();
					object .addProperty("itemCode", itemCode);
					Map<String,String> custom = tabs.get(i).getMapCustomData();
					object .addProperty("uploadPicUrl1", custom.get("uploadPicUrl1"));
					object.addProperty("title", tabs.get(i).getStrTitle());
					object.addProperty("price", tabs.get(i).getDwActivePrice());
					object.addProperty("url", "http://auction1.paipai.com/" + tabs.get(i).getStrCommodityId());
					jsonObjects[i] =  object;
				}
			}
		
		tabs = ItemClient.getTabDetail(parseToken(), getHttpHeadIp(), getMk(), Util.arrToList(tabIdArrI));
		
		if(null !=  tabs)
			for(int i = 0 ; i< tabIdArrI.length ; i ++){
				if(tabIdArrI[i] != 0){
					JsonObject object = new JsonObject();
					
					object .addProperty("t", "I");
					object .addProperty("tabId", split[i].split("\\|")[1]);
					String itemCode = tabs.get(i).getStrCommodityId();
					object .addProperty("itemCode", itemCode);
					Map<String,String> custom = tabs.get(i).getMapCustomData();
					object .addProperty("uploadPicUrl1", custom.get("uploadPicUrl1"));
					object.addProperty("title", tabs.get(i).getStrTitle());
					object.addProperty("price", tabs.get(i).getDwActivePrice());
					object.addProperty("url", "http://auction1.paipai.com/" + tabs.get(i).getStrCommodityId());
					jsonObjects[i] =  object;
				}
			}
		
		JsonArray array = new JsonArray();
		for(JsonObject jsonObject : jsonObjects){
			array.add(jsonObject);
		}
		
		return doPrint(out.setData(array));
	}
	
	
	/**
	 * 可能含start，单没有end
	 * @param start
	 * @param end
	 * @param count
	 * @return
	 */
	private static ArrayList<Integer> getRandomNoRepeat(int start,int end,int count){
		HashSet<Integer> set = new HashSet<Integer>();
		int es = end-start;
		Random random = new Random();
		
		while (true) {
			set.add(random.nextInt(es)+start);
			if(set.size() == count)break;
		}
		
		ArrayList<Integer> arrayList = new ArrayList<Integer>(set);
		Collections.shuffle(arrayList);//打乱顺序
		return arrayList;
	}
	
	
	/**
	 * 模拟标签推荐
	 * @param length
	 * @return
	 */
	public static String getRandomString(int length) { //length表示生成字符串的长度
	    String base = "abcdefghijklmnopqrstuvwxyz0123456789";   
	    Random random = new Random();   
	    StringBuffer sb = new StringBuffer();   
	    for (int i = 0; i < length; i++) {   
	        int number = random.nextInt(base.length());   
	        sb.append(base.charAt(number));   
	    }   
	    return sb.toString();   
	 }   
	
	
	/**
	 * 打乱顺序
	 * @param array
	 * @param size	截取的数据大小
	 * @return
	 */
	private List<String> shuffle(JsonArray array,int size){
		List<String> shopIds1 = new ArrayList<String>();
		for (JsonElement jsonElement : array) {
			shopIds1.add(jsonElement.getAsJsonPrimitive().getAsString());
		}
		Collections.shuffle(shopIds1);
		return shopIds1.subList(0, size);
	}
	
	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public JsonOutput getOut() {
		return out;
	}

	public void setOut(JsonOutput out) {
		this.out = out;
	}

	public void setRecommendBiz(RecommendBiz recommendBiz) {
		this.recommendBiz = recommendBiz;
	}

	public String getTabIds() {
		return tabIds;
	}

	public void setTabIds(String tabIds) {
		this.tabIds = tabIds;
	}

	public String getPoolIds() {
		return poolIds;
	}

	public void setPoolIds(String poolIds) {
		this.poolIds = poolIds;
	}
	public void setBiBiz(BiBiz biBiz) {
		this.biBiz = biBiz;
	}

	public long getMsgId() {
		return msgId;
	}

	public void setMsgId(long msgId) {
		this.msgId = msgId;
	}

	public String getDaps() {
		return daps;
	}

	public void setDaps(String daps) {
		this.daps = daps;
	}

	public String getShopIds() {
		return shopIds;
	}

	public void setShopIds(String shopIds) {
		this.shopIds = shopIds;
	}

	public int getCount2() {
		return count2;
	}

	public void setCount2(int count2) {
		this.count2 = count2;
	}

	public void setFavoriteBiz(FavoriteBiz favoriteBiz) {
		this.favoriteBiz = favoriteBiz;
	}

	public void setShopBiz(ShopBiz shopBiz) {
		this.shopBiz = shopBiz;
	}

	public void setItemBiz(ItemBiz itemBiz) {
		this.itemBiz = itemBiz;
	}

	public void setPpItemBiz(PPItemBiz ppItemBiz) {
		this.ppItemBiz = ppItemBiz;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public void setDiscoverBiz(DiscoverBiz discoverBiz) {
		this.discoverBiz = discoverBiz;
	}

	public int getPageSize2() {
		return pageSize2;
	}

	public void setPageSize2(int pageSize2) {
		this.pageSize2 = pageSize2;
	}

	public String getIcs() {
		return ics;
	}

	public void setIcs(String ics) {
		this.ics = ics;
	}

	public String getKeys() {
		return keys;
	}

	public void setKeys(String keys) {
		this.keys = keys;
	}
	

	public int getPsGoodsPPY() {
		return psGoodsPPY;
	}


	public void setPsGoodsPPY(int psGoodsPPY) {
		this.psGoodsPPY = psGoodsPPY;
	}


	public int getPsShopWD() {
		return psShopWD;
	}


	public void setPsShopWD(int psShopWD) {
		this.psShopWD = psShopWD;
	}


	public int getPsGoodsGlobal() {
		return psGoodsGlobal;
	}


	public void setPsGoodsGlobal(int psGoodsGlobal) {
		this.psGoodsGlobal = psGoodsGlobal;
	}


	public int getIsVirtual() {
		return isVirtual;
	}


	public void setIsVirtual(int isVirtual) {
		this.isVirtual = isVirtual;
	}

}
