package com.qq.qqbuy.tenpay.po;

public class PayInfo
{

    /**
     * 订单id
     */
    public String dealCode = "";
    
    /**
     * 用户uin
     */
    public long sellerUin;
    
    
    @Override
    public String toString()
    {
        return "PayInfo [dealCode=" + dealCode
                +", uin=" + sellerUin + "]";
    }
    
}
