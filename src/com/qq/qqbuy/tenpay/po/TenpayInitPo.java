package com.qq.qqbuy.tenpay.po;

/**
 * 用于封装Init接口完成后以后使用到的数据。
 * @author winsonwu
 * @Created 2012-12-13
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class TenpayInitPo {
	
	private String tokenId;
	
	private String md5KeySpid;

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	public String getMd5KeySpid() {
		return md5KeySpid;
	}

	public void setMd5KeySpid(String md5KeySpid) {
		this.md5KeySpid = md5KeySpid;
	}
	
}
