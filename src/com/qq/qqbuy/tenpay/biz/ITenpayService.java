package com.qq.qqbuy.tenpay.biz;

import java.util.Map;

/**
 * 仅是和财付通支付相关操作，不关心订单逻辑
 * 
 * @author wendyhu
 * 
 */
public interface ITenpayService 
{

    /**
     * 从财付通获取付款的token号码
     * 
     * @param reqParam
     *            去财付通获取token的参数
     * 
     * @return 返回token信息
     */
    public String getToken(Map<String, String> reqParam);

    /**
     * 财付通支付成功后回调的方法
     * 
     * @param respParam
     *            财付通http回调的请求参数,除了sign这个签名参数
     *            
     * @param sign
     *            签名
     *              
     * @return
     */
    public boolean callBack(Map<String, String> respParam,String sign);
    
    public void setMd5SeedKey(String md5SeedKey);
    
    /**
     * 根据不同的商户号获取回对应的加密串种子
     * @param spid
     * @return
     */
    public String getMd5Key(String spid);
    
    public String getErrMsg();
}
