//package com.qq.qqbuy.tenpay.biz.impl;
//
//import java.io.IOException;
//import java.io.StringReader;
//import java.util.List;
//import java.util.Map;
//
//import org.jdom.Document;
//import org.jdom.Element;
//import org.jdom.JDOMException;
//import org.jdom.input.SAXBuilder;
//
//import com.qq.qqbuy.common.Log;
//import com.qq.qqbuy.common.PaiPaiConfig;
//import com.qq.qqbuy.common.util.HttpUtil;
//import com.qq.qqbuy.common.util.MD5Util;
//import com.qq.qqbuy.common.util.StringUtil;
//import com.qq.qqbuy.tenpay.biz.TenpayServiceAdapter;
//
///**
// * 
// * C2C 支付实现
// * 
// * 这里要求的参数有如下：
// * 
// * @param ver
// *            版本号,ver默认值是1.0。目前版本ver取值应为2.0
// * @param charset
// *            1 UTF-8, 2 GB2312, 默认为1 UTF-8
// * @param bank_type
// *            银行类型:财付通支付填0
// * @param desc
// *            商品描述,32个字符以内
// * @param purchaser_id
// *            用户(买方)的财付通帐户(QQ 或EMAIL)。若商户没有传该参数，则在财付通支付页面，买家需要输入其财付通帐户。
// * @param bargainor_id
// *            商户号,由财付通统一分配的10位正整数(120XXXXXXX)号
// * @param sp_billno
// *            商户系统内部的定单号,32个字符内、可包含字母
// * @param total_fee
// *            总金额,以分为单位,不允许包含任何字、符号
// * @param fee_type
// *            现金支付币种,目前只支持人民币,默认值是1-人民币
// * @param notify_url
// *            接收财付通通知的URL,需给绝对路径，255字符内 格式如:http://wap.tenpay.com/tenpay.asp
// * @param callback_url
// *            交易完成后跳转的URL,需给绝对路径，255字符内 格式如:http://wap.tenpay.com/tenpay.asp
// * @param attach
// *            商户附加信息,可做扩展参数，255字符内
// * @param time_start
// *            订单生成时间，格式为yyyymmddhhmmss，如2009年12月25日9点10分10秒表示为20091225091010。
// *            时区为GMT+8 beijing。该时间取自商户服务器
// * @param time_expire
// *            订单失效时间，格式为yyyymmddhhmmss，如2009年12月27日9点10分10秒表示为20091227091010。
// *            时区为GMT+8 beijing。该时间取自商户服务器
// * 
// * @param spid
// *            商户申请的spid
// * 
// *            * @param sp_time_stamp 拍拍时间戳 linux时间戳
// * 
// * @param price
// *            产品价格，以分为单位
// * 
// * @param transport_fee
// *            物流费用，以分为单位
// * 
// * @param fee1
// *            现金支付金额，以分为单位
// * 
// * @param fee2
// *            代金券金额
// * 
// * @param fee3
// *            其它费用
// * 
// * @return tokenid
// * 
// * @author wendyhu
// * 
// */
//public class UppServiceTenpayServiceImpl extends TenpayServiceAdapter
//{
////    protected String hostUrl = "http://wap.tenpay.com/cgi-bin/wappayv2.0/wappay_init.cgi";// 请求的url
//
//	/**
//	 * 临时，后续校验
//	 * @author liubenlong3
//	 */
//    @Override
//    protected boolean validator(Map<String, String> reqParam) {
//    	
//    	Log.key.info("UppServiceTenpayServiceImpl.validator()");
//    	
////        if(hostUrl == null || hostUrl.trim().length()< 10 )
////            return false;
////        
////        if (reqParam != null && reqParam.get("partner") != null
////                && reqParam.get("charset") != null
////                && reqParam.get("bank_type") != null
////                && reqParam.get("desc") != null
////                && reqParam.get("purchaser_id") != null
////                && reqParam.get("bargainor_id") != null
////                && reqParam.get("sp_billno") != null
////                && reqParam.get("transaction_id") != null
////                && reqParam.get("total_fee") != null
////                && reqParam.get("fee_type") != null
////                && reqParam.get("notify_url") != null
////                && reqParam.get("callback_url") != null
////                && reqParam.get("attach") != null
////                && reqParam.get("time_start") != null
////                && reqParam.get("time_expire") != null
////                && reqParam.get("sign_sp_id") != null
////                && reqParam.get("sp_time_stamp") != null
////                && reqParam.get("price") != null
////                && reqParam.get("transport_fee") != null
////                && reqParam.get("fee1") != null
////                && reqParam.get("fee2") != null
////                && reqParam.get("fee3") != null)
//            return true;
//
////        return false;
//    }
//
//    /**
//     * 解析tokenid
//     * 
//     * @param xmlContent
//     * @return
//     */
//    protected String parseContent(String xmlContent){
//		try {
//			String URL = "";
//			SAXBuilder builder = new SAXBuilder();
//			Document doc = builder.build(new StringReader(xmlContent));
//			Element root = doc.getRootElement();
//			// 如果成功则有这个字段
//			if (root != null && root.getChildText("Status") != null){
//			    String Status = StringUtil.removeInvalidWML(root.getChildText(
//			            "Status").trim().replaceAll("</?[^>]+>", "")
//			            .replaceAll("&nbsp;", ""));
//			    System.out.println("Status:"+Status);
//			}
//			
//			String Message = root.getChildText("Message");
//			System.out.println("Message："+Message);
//			String Sign = root.getChildText("Sign");
//			System.out.println("Sign:"+Sign);
//			
//			//拼接返回的URL
//			List<Element> children = root.getChildren("Form");
//			for (Element e : children) {
//				if (e != null && e.getChildText("Url") != null){
//				    URL = StringUtil.removeInvalidWML(
//				    		e.getChildText("Url").trim().replaceAll("</?[^>]+>", "").replaceAll("&nbsp;", ""));
//				    System.out.println("URL:"+URL);
//				}
//				
//				List<Element> children2 = e.getChild("Parameters").getChildren("Parameter");
//				
//				if(children2.size() > 0)URL += "?";
//				
//				for (Element child : children2) {
//					String Name = "";
//					String Value = "";
//					if (child != null && child.getChildText("Name") != null){
//					    Name = StringUtil.removeInvalidWML(
//					    		child.getChildText("Name").trim().replaceAll("</?[^>]+>", "").replaceAll("&nbsp;", ""));
//					    System.out.println("Name:"+Name);
//					}
//					if (child != null && child.getChildText("Value") != null){
//					    Value = StringUtil.removeInvalidWML(
//					    		child.getChildText("Value").trim().replaceAll("</?[^>]+>", "").replaceAll("&nbsp;", ""));
//					    System.out.println("Value:"+Value);
//					}
//					
//					URL += Name + "=" + Value +"&";
//				}
//				
//				URL = URL.substring(0, URL.length()-1);
//				
//			}
//			
//			return URL;
//		} catch (JDOMException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//        return null;
//    }
//
//    
//    
//    /**
//     * 访问统一支付平台
//     * 获取支付服务商参数
//     * 
//     * @author liubenlong3
//     * 
//     * @return	返回的是URL，这个需要优化，先调流程
//     * 
//     */
//    @Override
//    public String getToken(Map<String, String> reqParam) {
//    	
//    	Log.key.info("UppServiceTenpayServiceImpl.getToken()");
//    	
//        String URL = "";
//        
//        // 1、检查参数是否合法
//        if (!validator(reqParam)){
//            return URL;
//        }
//
//        // 2、获取sign签名
//        String sign = MD5Util.md5Sign(reqParam, "sign_key", "83f9821dc8a952ce4eefdab603e43de5", "gb2312");
//        reqParam.put("sign", sign);
//
//        String ipPort = PaiPaiConfig.getPaipaiCommonIp();
//        Log.run.debug("ipPort: "+ipPort);
//        
//        // 3、发送并获取http回包
//        String xmlContent = HttpUtil.post("http://" + ipPort + "/cgi-bin/unipay/api/1.1/payment/gateway", 
//        		reqParam, connectTimeout, readTimeout,
//                "gb2312", encoding, false, null, "pay.paipai.com");
//        
//        Log.key.info("UppServiceTenpayServiceImpl.getToken()---xmlContent: " + xmlContent);
//
//        // 4、解析回包token
//        URL = parseContent(xmlContent);
//
//        Log.key.info("UppServiceTenpayServiceImpl.getToken()---URL: " + URL);
//        return URL;
//    }
//    
//    
//    
////    /**
////     * 从财付通获取付款的token号码
////     * 
////     * @param reqParam
////     *            去财付通获取token的参数
////     * 
////     * @return 返回token信息
////     */
////    public String getToken(Map<String, String> reqParam)
////    {
////        String tokenId = "";
////        logger.info("###getToken param:[ " + reqParam.toString() + "]");
////        long begin = System.nanoTime();
////        
////        //1、发送并获取http回包
////        String xmlContent = getRespXmlContent(reqParam);
////        long getContentTime = System.nanoTime();
////        
////        //2、解析回包token
////        tokenId = parseContent(xmlContent);
////        long parseTime = System.nanoTime();
////        
////        logger.info("###getToken param:[ " + reqParam.toString() +" ] result:[ " + xmlContent + " ] token:[ " + tokenId + " ] tc:[ " + 
////                (parseTime - begin) + " ] pc:[ " + (parseTime - getContentTime) + " ] gc:[ " + (getContentTime - begin)
////                + " ] " );
////        
////        return tokenId;
////    }
//}
