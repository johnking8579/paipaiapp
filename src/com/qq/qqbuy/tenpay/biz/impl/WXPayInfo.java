package com.qq.qqbuy.tenpay.biz.impl;

import com.qq.qqbuy.common.po.CommonPo;

/**
 * 统一支付平台--微信支付返回值【获取支付服务商参数接口】
 * 
 * @author liubenlong3
 *
 */
public class WXPayInfo extends CommonPo
{
	private String appid;//应用唯一标识，在微信开放平台提交应用审核通过后获得
	private String noncestr;//32位内的随机串，防重发
	private String package1;//订单详情
	private String partnerid;//
	private String prepayid;//
	private String sign;//签名
	private String timestamp;//时间戳，为1970 年1 月1 日00:00 到请求发起时间的秒数
	
	public String getAppid() {
		return appid;
	}
	public void setAppid(String appid) {
		this.appid = appid;
	}
	public String getNoncestr() {
		return noncestr;
	}
	public void setNoncestr(String noncestr) {
		this.noncestr = noncestr;
	}
	public String getPackage1() {
		return package1;
	}
	public void setPackage1(String package1) {
		this.package1 = package1;
	}
	public String getPartnerid() {
		return partnerid;
	}
	public void setPartnerid(String partnerid) {
		this.partnerid = partnerid;
	}
	public String getPrepayid() {
		return prepayid;
	}
	public void setPrepayid(String prepayid) {
		this.prepayid = prepayid;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	
	@Override
	public String toString() {
		return "WXPayInfo [appid=" + appid + ", noncestr=" + noncestr
				+ ", package1=" + package1 + ", partnerid=" + partnerid
				+ ", prepayid=" + prepayid + ", sign=" + sign + ", timestamp="
				+ timestamp + "]";
	}
	
	
	
}
