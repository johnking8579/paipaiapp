package com.qq.qqbuy.tenpay.biz.impl;

import com.qq.qqbuy.common.po.CommonPo;

/**
 * 
 * @ClassName: PayTokenInfo
 * 
 * @Description: 去第三方支付时拿到的支付token
 * 
 * @author wendyhu
 * @date 2012-12-21 下午07:21:44
 */
public class PayTokenInfo extends CommonPo
{
    /**
     * 第三方支付返回的支付token
     */
    private String token = "";
    
    /**
     * 财付通分配给这边的appid
     */
    private String bargainorId = "9334687204";
    
    /**
     * 支付成功后的回跳
     */
    private String tenPayUrl = "";
    
    /**
     * 支付成功后客户端跳转url
     */
    private String h5PayUrl = "";
    
    //类型 t=0：合单支付  t=1:单笔c2c支付
    private int t = 1;
    
    //0:由调用方自动选择支付方式，1：插件支付 2：h5支付
    private int pt = 2;
    
    public int getPt()
    {
        return pt;
    }

    public void setPt(int pt)
    {
        this.pt = pt;
    }

    private String callbackUrl = "";

    public String getCallbackUrl()
    {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl)
    {
        this.callbackUrl = callbackUrl;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String getBargainorId()
    {
        return bargainorId;
    }

    public void setBargainorId(String bargainorId)
    {
        this.bargainorId = bargainorId;
    }

    public String getTenPayUrl()
    {
        return tenPayUrl;
    }

    public void setTenPayUrl(String tenPayUrl)
    {
        this.tenPayUrl = tenPayUrl;
    }

    public int getT()
    {
        return t;
    }

    public void setT(int t)
    {
        this.t = t;
    }
    
    public String getH5PayUrl()
    {
        return h5PayUrl;
    }

    public void setH5PayUrl(String h5PayUrl)
    {
        this.h5PayUrl = h5PayUrl;
    }

    @Override
    public String toString()
    {
        return "PayTokenInfo [bargainorId=" + bargainorId + ", h5PayUrl="
                + h5PayUrl + ", t=" + t + ", callbackUrl=" + callbackUrl + ", tenPayUrl="
                + tenPayUrl + ", token=" + token + ", errCode=" + errCode
                + ", msg=" + msg + ", retCode=" + retCode + "]";
    }
}
