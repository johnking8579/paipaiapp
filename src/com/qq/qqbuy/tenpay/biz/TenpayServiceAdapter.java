package com.qq.qqbuy.tenpay.biz;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.TenpayConstant;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.common.util.MD5Util;
import com.qq.qqbuy.common.util.StringUtil;

/**
 * 财付通支付相关的封装
 * 
 * @author wendyhu
 * 
 */
public abstract class TenpayServiceAdapter  implements
        ITenpayService
{
    protected static final Logger logger = Log.key;
    protected String md5SeedKey = "f4becb5fb2e007c5fb7238b619b8956b";// 加密种子串
    protected String md5Charset = "utf-8";// md5字符集
    protected String hostUrl = "http://wap.tenpay.com/cgi-bin/wappayv2.0/wappay_init.cgi";// 请求的url
    protected int readTimeout = 15000;// 读超时时间
    protected int connectTimeout = 10000;// 连接超时时间
    protected String requestCharset = "UTF-8";// 请求的字符编码
    protected String encoding = "gb2312";// 回复的字符编码
    protected boolean isNeedProxy = true;
    private static int refCount = 0;
    protected Map<String, String> spidMd5 = new HashMap<String, String>();// spid的md5
    private String errMsg = "";

    public String getErrMsg()
    {
        return errMsg;
    }

    public Map<String, String> getSpidMd5()
    {
        return spidMd5;
    }

    public void setSpidMd5(Map<String, String> spidMd5)
    {
        this.spidMd5 = spidMd5;
    }

    public TenpayServiceAdapter()
    {
        refCount++;
    }

//    @Override
//    public int getObjCount()
//    {
//        return refCount;
//    }

    protected void finalize()
    {
        refCount--;
        try
        {
            super.finalize();
        } catch (Throwable e)
        {
            logger.error("CFTServiceClient finalize failure", e);
        }
    }

    public boolean isNeedProxy()
    {
        return isNeedProxy;
    }

    public void setNeedProxy(boolean isNeedProxy)
    {
        this.isNeedProxy = isNeedProxy;
    }

    public String getHostUrl()
    {
        return hostUrl;
    }

    public void setHostUrl(String hostUrl)
    {
        this.hostUrl = hostUrl;
    }

    public int getReadTimeout()
    {
        return readTimeout;
    }

    public void setReadTimeout(int readTimeout)
    {
        this.readTimeout = readTimeout;
    }

    public int getConnectTimeout()
    {
        return connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout)
    {
        this.connectTimeout = connectTimeout;
    }

    public String getRequestCharset()
    {
        return requestCharset;
    }

    public void setRequestCharset(String requestCharset)
    {
        this.requestCharset = requestCharset;
    }

    public String getEncoding()
    {
        return encoding;
    }

    public void setEncoding(String encoding)
    {
        this.encoding = encoding;
    }

    public String getMd5SeedKey()
    {
        return md5SeedKey;
    }

    public void setMd5SeedKey(String md5SeedKey)
    {
        this.md5SeedKey = md5SeedKey;
    }

    public String getMd5Charset()
    {
        return md5Charset;
    }

    public void setMd5Charset(String md5Charset)
    {
        this.md5Charset = md5Charset;
    }

    /**
     * 检查参数是否合法
     * 
     * @param reqParam
     *            请求参数
     * 
     * @return
     */
    protected abstract boolean validator(Map<String, String> reqParam);

    /**
     * 初使化签名
     * 
     * @param reqParam
     *            请求参数
     * 
     * @return
     */
    protected String initSign(Map<String, String> reqParam)
    {
        return MD5Util.md5Sign(reqParam, "key",
                getMd5Key(reqParam.get(TenpayConstant.PARAM_SELLER_SPID)),
                md5Charset).toUpperCase();
    }

    /**
     * 获取回包
     * 
     * @param reqParam
     * @return
     */
    protected String getRespXmlContent(Map<String, String> reqParam)
    {
        return HttpUtil.post(hostUrl, reqParam, connectTimeout, readTimeout,
                requestCharset, encoding, isNeedProxy);
    }

    /**
     * 解析tokenid
     * 
     * @param xmlContent
     * @return
     */
    protected String parseContent(String xmlContent)
    {
        String tokenId = "";
        if (xmlContent == null || xmlContent.trim().length() < 10)
        {
            logger.error("parseContent error for xmlContent is empty");
            return tokenId;
        }
        SAXBuilder builder = new SAXBuilder();
        try
        {
            Document doc = builder.build(new StringReader(xmlContent));
            Element root = doc.getRootElement();

            // 如果成功则有这个字段

            if (root != null && root.getChildText("token_id") != null)
                tokenId = StringUtil.removeInvalidWML(root.getChildText(
                        "token_id").trim().replaceAll("</?[^>]+>", "")
                        .replaceAll("&nbsp;", ""));

            if (tokenId != null && tokenId.length() != 0)
            {
                return tokenId;
            } else
            {
                String errinfo = StringUtil.removeInvalidWML(root.getChildText(
                        "err_info").trim().replaceAll("</?[^>]+>", "")
                        .replaceAll("&nbsp;", ""));
                errMsg = errinfo;
                logger.error("parseContent error for [" + xmlContent
                        + "] err:[" + errinfo + "]");
                return "";
            }
        } catch (Exception e)
        {
            logger.error("parseContent error for [" + xmlContent + "]", e);
        }
        return tokenId;
    }

    /**
     * 从财付通获取付款的token号码
     * 
     * @param reqParam
     *            去财付通获取token的参数
     * 
     * @return 返回token信息
     */
    public String getToken(Map<String, String> reqParam)
    {
        String tokenId = "";
        logger.info("###****getToken param:[ " + reqParam.toString() + "]");
        long begin = System.nanoTime();
        // 1、检查参数是否合法
        if (!validator(reqParam))
            return tokenId;
        long validatorTime = System.nanoTime();

        // 2、获取sign签名
        String sign = initSign(reqParam);
        reqParam.put("sign", sign);
        long initSignTime = System.nanoTime();
        // log统计

        // 3、发送并获取http回包
        String xmlContent = getRespXmlContent(reqParam);
        long getContentTime = System.nanoTime();

        // 4、解析回包token
        tokenId = parseContent(xmlContent);
        long parseTime = System.nanoTime();

        logger.info("###getToken param:[ " + reqParam.toString()
                + " ] result:[ " + xmlContent + " ] token:[ " + tokenId
                + " ] tc:[ " + (parseTime - begin) + " ] pc:[ "
                + (parseTime - getContentTime) + " ] gc:[ "
                + (getContentTime - initSignTime) + " ] ic:[ "
                + (initSignTime - validatorTime) + " vc:[ "
                + (validatorTime - begin) + " ]");
        Log.payMent.info("getToken param:[ " + reqParam.toString()
                                          + " ] result:[ " + xmlContent + " ] token:[ " + tokenId
                                          + " ] ") ;
        return tokenId;
    }

    /**
     * 
     * @Title: getMd5Key
     * @Description: 获取md5Key
     * @param param
     * @return String 返回类型
     * 
     * @throws
     */
    public String getMd5Key(String spid)
    {
        return md5SeedKey;
    }

    /**
     * 
     * @Title: notify
     * @Description:财付通成功后的回跳
     * @param param
     * @param sign
     * @return boolean 返回类型
     * 
     * @throws
     */
    protected boolean notify(Map<String, String> param, String sign)
    {
        return true;
    }

    /**
     * 财付通支付成功后回调的方法
     * 
     * @param respParam
     *            财付通http回调的请求参数
     * 
     * @return
     */
    public boolean callBack(Map<String, String> param, String sign)
    {
        logger.info("###cft_callBack param:[ " + param.toString()
                + " ] sign: [ " + sign + "]");
        if (param == null || param.size() == 0 || sign == null
                || sign.length() != 32)
            return false;

        // 1、获取字符集
        String md5Charset = TenpayConstant.getCharset(StringUtil.toLong(param
                .get(TenpayConstant.PARAM_CHARSET), 1));

        String md5Key = getMd5Key(param.get(TenpayConstant.PARAM_SELLER_SPID));

        // 2、验证签名
        String signTmp = MD5Util.md5Sign(param, "key", md5Key, md5Charset)
                .toUpperCase();
        if (!sign.equalsIgnoreCase(signTmp))
        {
            logger.info("###cft_callBack false param:[ " + param.toString()
                    + " ] sign: [ " + sign + "]" + " md5Key: [ " + md5Key
                    + " ]" + " signTmp: [ " + signTmp + " ]");
            return false;
        }

        // 3、回调知道
        boolean ret = notify(param, sign);
        logger.info("###cft_callBack " + ret + " param:[ " + param.toString()
                + " ] sign: [ " + sign + "]" + " md5Key: [ " + md5Key + " ]"
                + " signTmp: [ " + signTmp + " ]");

        return ret;
    }
}
