package com.qq.qqbuy.tenpay;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.deal.biz.DrawIdGenerator;

/**
 * 财付通相关, 加解密算法. URL.
 * @author JingYing
 * @date 2014年11月23日
 */
public class TenpayUtil {
	
	public static final String 
		确认收货页面_手机用 = "https://www.tenpay.com/app/mpay/mobile_auth.cgi?",
		KEY = "tencent*20090925@cft_paipai";
	
	private static final byte[] 
		defaultKEY = { (byte)0x4a, (byte)0x08, (byte)128, (byte)0x58, (byte)0x13, (byte)0xad, (byte)0x46, (byte)0x89 },
		defaultIV = { 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18 };
	
	
	/**
	 * 财付通的通用签名方式
	 * @param params
	 * @return
	 */
	public static String signParam(Map<String,String> params)	{
		TreeMap<String,String> tree = new TreeMap<String,String>(params);
		StringBuilder sb = new StringBuilder();
		for(Entry<String,String> e : tree.entrySet())	{
			if(e.getValue()!= null && !"".equals(e.getValue()))	{
				sb.append(e.getKey()).append("=").append(e.getValue()).append("&");
			}
		}
		if(sb.toString().endsWith("&"))
			sb.deleteCharAt(sb.length()-1);
		
		byte[] toBeSign;
		try {
			toBeSign = (sb.toString() + KEY).getBytes("utf-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}		
		sb.append("&sign=").append(Util.toMd5(toBeSign).toUpperCase());
		return sb.toString();
	}
	
	/**
	 * 生成"确认收货"的WAP页面
	 * @param uin
	 * @param dealId
	 * @param tradeIds
	 * @param sellerFee 给卖家的钱,分
	 * @param returnUrl 财付通回调地址, 不能加url参数,财付通回调时会直接在returnUrl后加"?token=xxxxx"
	 * @param tenpayTransId
	 * @param lskey QQ登录态
	 * @return
	 */
	public static String genConfirmRecvUrl(long uin, String tenpayTransId, String dealId, long[] tradeIds, long sellerFee, String returnUrl, String lskey)	{
		Map<String, String> params = new LinkedHashMap<String, String>();
		params.put("uin", uin + "");
		params.put("trans_num", "1");
		params.put("trans_id_0", tenpayTransId);
		params.put("draw_id_0", new DrawIdGenerator().genRecvDrawId(tenpayTransId, tradeIds));
		params.put("deal_id_0", dealId);
		params.put("buyer_fee_0", "0");
		params.put("seller_fee_0", sellerFee+"");
		params.put("channel", "1");
		params.put("remark", "");
		params.put("return_url", returnUrl);
		params.put("timestamp", (System.currentTimeMillis() / 1000) + "");
		byte[] toSign;
		try {
			toSign = (Util.paramMapToString(params) + KEY).getBytes("utf-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
		params.put("sign", Util.toMd5(toSign));	//按照规定的参数顺序来签名, 跟一般的财付通签名不同
		//以下字段不参与签名
		if(lskey != null)	{
			params.put("lskey", lskey);
		}
		params.put("magic_key", "");
		params.put("tag", "");
		return 确认收货页面_手机用 + Util.paramMapToString(params);
	}
	
	/**
	 * 财付通加密算法
	 * @param src
	 * @return
	 * @throws Exception
	 */
	public static String encrypt(String src) throws Exception {
		return CftBase64.byteArrayToBase64(
					encrypt(src, defaultKEY,defaultIV));
	}
	
	
	/**
	 * 财付通DES加密方法
	 * @param src 需要加密的源串
	 * @param key 加密所需的密文
	 * @param IV 加密所需的向量
	 */
	 public static byte[] encrypt(String src, byte[] key, byte[] IV) {
		SecretKey deskey = new SecretKeySpec(key, "DES");
		// Cipher c1 = Cipher.getInstance("DES/CBC/NoPadding","BC");
		try {
			Cipher c1 = Cipher.getInstance("DES/CBC/NoPadding");
			IvParameterSpec IVSpec = new IvParameterSpec(IV);
			c1.init(Cipher.ENCRYPT_MODE, deskey, IVSpec);

			int iLen = 8 - src.getBytes().length % 8;
			if (iLen != 8) {
				ByteBuffer buffer = ByteBuffer.allocate(src.getBytes().length
						+ iLen);
				buffer.put(src.getBytes());
				src = new String(buffer.array());
				buffer.clear();
			}
			return c1.doFinal(src.getBytes("GBK"));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
	
	/**
	 * 解密财付通
	 * @param src
	 * @return
	 * @throws Exception 
	 */
	public static String decrypt(String src)	{
		return new String(
					decrypt(defaultKEY, defaultIV, 
							CftBase64.base64ToByteArray(src)));
	}
	
	/**
	 * 财付通DES解密方法
	 * @param key 解密所需的密钥
	 * @param IV 解密所需的向量
	 * @param src 需要解密的源串
	 */
	private static byte[] decrypt(byte[] key, byte[] IV, byte[] src) {
		SecretKey deskey = new SecretKeySpec(key, "DES");
		// Cipher c1 = Cipher.getInstance("DES/CBC/NoPadding","BC");
		try {
			Cipher c1 = Cipher.getInstance("DES/CBC/NoPadding");
			IvParameterSpec IVSpec = new IvParameterSpec(IV);
			c1.init(Cipher.DECRYPT_MODE, deskey, IVSpec);
			byte[] decryptByte = c1.doFinal(src);
			if (decryptByte != null) {
				int iPos = decryptByte.length;
				for (int i = iPos - 1; i >= 0; i--) {
					if (decryptByte[i] == 0) {
						iPos--;
					} else {
						break;
					}
				}
				byte[] ret = new byte[iPos];
				for (int i = 0; i < iPos; i++) {
					ret[i] = decryptByte[i];
				}
				return ret;
			}
			return decryptByte;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
