package com.qq.qqbuy.version.dao;

import com.qq.qqbuy.version.po.VersionApp;
/**
 * app版本更新DAO接口
 * @author zhaohuayu
 *
 */
public interface VersionAppDao {

	/**
	 * 插入一条app的版本信息
	 * @param versionApp
	 * @return
	 * @throws Exception
	 */
	public int insertVersionApp(VersionApp versionApp) throws Exception ;
	/**
	 * 根据appKey和client得到最新的app版本信息
	 * @param appKey app标识
	 * @param client 平台类型 ios android
	 * @return
	 * @throws Exception
	 */
	public VersionApp findLatestVersionApp(String appKey, String client) throws Exception ; 
}
