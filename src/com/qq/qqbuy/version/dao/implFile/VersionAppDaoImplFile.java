package com.qq.qqbuy.version.dao.implFile;

import java.io.InputStream;
import java.util.Properties;

import com.qq.qqbuy.version.dao.VersionAppDao;
import com.qq.qqbuy.version.po.VersionApp;
/**
 * app版本更新DAO接口
 * @author zhaohuayu
 *
 */
public class VersionAppDaoImplFile implements VersionAppDao {

	/**
	 * 插入一条app的版本信息
	 * @param versionApp
	 * @return
	 * @throws Exception
	 */
	public int insertVersionApp(VersionApp versionApp) throws Exception {
		return 0 ;
	}
	/**
	 * 根据appKey和client得到最新的app版本信息
	 * @param appKey app标识
	 * @param client 平台类型 ios android
	 * @return
	 * @throws Exception
	 */
	public VersionApp findLatestVersionApp(String appKey, String client) throws Exception {
		InputStream inStream = ClassLoader.getSystemClassLoader().getResourceAsStream("versionApp.properties") ;
		Properties p = new Properties() ;
		p.load(inStream) ;
		inStream.close();
		String a = p.getProperty("versionDesc") ;
		System.out.println(a);
		return null ;
	}
}
