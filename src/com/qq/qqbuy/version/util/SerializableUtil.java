package com.qq.qqbuy.version.util;

import org.apache.commons.lang3.StringUtils;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.version.po.VersionInfo;

public class SerializableUtil {

	public static final char SEPARATOR_CHAR = 1 ;
	public static String serialize(VersionInfo versionInfo) {
		StringBuffer sb = new StringBuffer() ;
		sb.append(versionInfo.getAdid()).append(SEPARATOR_CHAR)
			.append(versionInfo.getAppID()).append(SEPARATOR_CHAR)
			.append(versionInfo.getAppVersion()).append(SEPARATOR_CHAR)
			.append(versionInfo.getBrand()).append(SEPARATOR_CHAR)
			.append(versionInfo.getChannel()).append(SEPARATOR_CHAR)
			.append(versionInfo.getClient()).append(SEPARATOR_CHAR)
			.append(versionInfo.getFirstAppVersion()).append(SEPARATOR_CHAR)
			.append(versionInfo.getImei()).append(SEPARATOR_CHAR)
			.append(versionInfo.getInsertDateTime()).append(SEPARATOR_CHAR)
			.append(versionInfo.getLanguage()).append(SEPARATOR_CHAR)
			.append(versionInfo.getLatitude()).append(SEPARATOR_CHAR)
			.append(versionInfo.getLongitude()).append(SEPARATOR_CHAR)
			.append(versionInfo.getMk()).append(SEPARATOR_CHAR)
			.append(versionInfo.getModel()).append(SEPARATOR_CHAR)
			.append(versionInfo.getMt()).append(SEPARATOR_CHAR)
			.append(versionInfo.getNetworkServer()).append(SEPARATOR_CHAR)
			.append(versionInfo.getNetworkType()).append(SEPARATOR_CHAR)
			.append(versionInfo.getOpenUdid()).append(SEPARATOR_CHAR)
			.append(versionInfo.getOsType()).append(SEPARATOR_CHAR)
			.append(versionInfo.getOsVersion()).append(SEPARATOR_CHAR)
			.append(versionInfo.getScreen()).append(SEPARATOR_CHAR)
			.append(versionInfo.getStatus()).append(SEPARATOR_CHAR)
			.append(versionInfo.getUpdateDateTime()).append(SEPARATOR_CHAR)
			.append(versionInfo.getUpdateTime()).append(SEPARATOR_CHAR)
			.append(versionInfo.getVersionChannel()).append(SEPARATOR_CHAR)
			.append(versionInfo.getVersionCode()).append(SEPARATOR_CHAR)
			.append(versionInfo.getWid()).append(SEPARATOR_CHAR)
			.append(versionInfo.getWifiMac()).append(SEPARATOR_CHAR)
			.append(versionInfo.getMk2()).append(SEPARATOR_CHAR);
		return sb.toString();
	}
	
	public static VersionInfo desSerialize(String versionInfoStr) {
		try {				
			if (StringUtils.isEmpty(versionInfoStr)) {
				return null ;
			}
			String[] items = versionInfoStr.split(String.valueOf(SEPARATOR_CHAR)) ;
			if (items.length != 22) {
				return null ;
			}
			/*VersionInfo versionInfo = new VersionInfo() ;		
			versionInfo.setAdid(items[0]) ;
			versionInfo.setAppKey(items[1]) ;
			versionInfo.setAppVersion(items[2]) ;
			versionInfo.setBrand(items[3]) ;
			versionInfo.setChannel(items[4]) ;
			versionInfo.setClient(items[5]) ;
			versionInfo.setFirstAppVersion(items[6]) ;
			versionInfo.setImei(items[7]) ;
			versionInfo.setInsertDateTime(items[8]) ;
			versionInfo.setLanguage(items[9]) ;
			versionInfo.setModel(items[10]) ;
			versionInfo.setNetworkServer(items[11]) ;
			versionInfo.setNetworkType(items[12]) ;
			versionInfo.setOpenUdid(items[13]) ;
			versionInfo.setOsType(items[14]) ;
			versionInfo.setOsVersion(items[15]) ;
			versionInfo.setScreen(items[16]) ;
			versionInfo.setStatus(Integer.parseInt(items[17])) ;
			versionInfo.setUpdateDateTime(items[18]) ;
			versionInfo.setUpdateTime(items[19]) ;
			versionInfo.setUuId(items[20]) ;
			versionInfo.setWifiMac(items[21]) ;	*/		
			
			return null ;
		} catch (Exception ex) {
			ex.printStackTrace() ;
			Log.run.info("desSerialize error ;versionInfoStr = " + versionInfoStr) ;
			return null ;
		}
		
	}
}
