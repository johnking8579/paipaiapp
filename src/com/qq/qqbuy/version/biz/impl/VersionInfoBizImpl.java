package com.qq.qqbuy.version.biz.impl;

import com.qq.qqbuy.common.util.DateUtils;
import com.qq.qqbuy.version.biz.VersionInfoBiz;
import com.qq.qqbuy.version.dao.VersionInfoDao;
import com.qq.qqbuy.version.po.VersionInfo;
import com.qq.qqbuy.version.util.VersionInfoConstants;

public class VersionInfoBizImpl implements VersionInfoBiz{

	private VersionInfoDao versionInfoDao ;
	
	@Override
	public Long reportVersionInfo(VersionInfo versionInfo) throws Exception{
		String mk = versionInfo.getMk() ;
		String appID = versionInfo.getAppID() ;
		VersionInfo versionInfoOld = versionInfoDao.findVersionInfo(mk,appID) ;
		String nowTime = DateUtils.getNowDateStr() ;
		if (null == versionInfoOld) {
			versionInfo.setFirstAppVersion(versionInfo.getAppVersion()) ;
			versionInfo.setInsertDateTime(nowTime);
			versionInfo.setStatus(0);
			versionInfo.setUpdateDateTime(nowTime);
			versionInfo.setUpdateTime(nowTime);
			int dbResult = versionInfoDao.insertVersionInfo(versionInfo) ;
			if (1 == dbResult) {
				return VersionInfoConstants.REPORT_SUCCESS ;
			} else {
				return VersionInfoConstants.REPORT_ERROR ;
			}
		} else {		
			versionInfo.setUpdateDateTime(nowTime);
			versionInfo.setUpdateTime(nowTime);
			int dbResult = versionInfoDao.updateVersionInfo(mk, appID , versionInfo) ;
			if (1 == dbResult) {
				return VersionInfoConstants.REPORT_SUCCESS ;
			} else {
				return VersionInfoConstants.REPORT_ERROR ;
			}
		}
	}

	@Override
	public VersionInfo findVersionInfo(String mk, String appID) throws Exception{
		return versionInfoDao.findVersionInfo(mk,appID) ;
	}

	/**
	 * 设备信息表插入或更新设备信息
	 *
	 * @param versionInfo
	 * @throws Exception
	 */
	@Override
	public void insertOrUpdateVersionInfo(VersionInfo versionInfo) throws Exception {

	}

	public VersionInfoDao getVersionInfoDao() {
		return versionInfoDao;
	}

	public void setVersionInfoDao(VersionInfoDao versionInfoDao) {
		this.versionInfoDao = versionInfoDao;
	}
	
}
