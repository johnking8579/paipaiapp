package com.qq.qqbuy.version.biz;

import com.qq.qqbuy.version.po.VersionApp;
/**
 * app版本更新服务层接口
 * @author zhaohuayu
 *
 */
public interface VersionAppBiz {

	/**
	 * 插入一条app的版本信息
	 * @param versionApp
	 * @return
	 * @throws Exception
	 */
	public Long reportVersionApp(VersionApp versionApp) throws Exception ;
	/**
	 * 根据appKey和client得到最新的app版本信息
	 * @param appKey app标识
	 * @param client 平台类型 ios android
	 * @return
	 * @throws Exception
	 */
	public VersionApp findLatestVersionApp(String appKey, String client) throws Exception ; 
}
