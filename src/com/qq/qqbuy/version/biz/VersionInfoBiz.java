package com.qq.qqbuy.version.biz;

import com.qq.qqbuy.version.po.VersionInfo;
/**
 * 手机信息业务层接口
 * @author zhaohuayu
 *
 */
public interface VersionInfoBiz {
	/**
	 * 上报用户手机信息
	 * @param versionInfo
	 * @return
	 */
	public Long reportVersionInfo(VersionInfo versionInfo) throws Exception ;
	/**
	 * 根据uuid查找用户手机信息
	 * @param mk
	 * @param appID
	 * @return
	 */
	public VersionInfo findVersionInfo(String mk,String appID) throws Exception;

	/**
	 * 设备信息表插入或更新设备信息
	 * @param versionInfo
	 * @throws Exception
	 */
	void insertOrUpdateVersionInfo(VersionInfo versionInfo) throws Exception;
}
