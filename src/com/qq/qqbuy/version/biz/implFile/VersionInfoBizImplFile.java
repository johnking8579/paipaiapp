package com.qq.qqbuy.version.biz.implFile;

import com.paipai.app.util.DbUtil;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.DateUtils;
import com.qq.qqbuy.version.biz.VersionInfoBiz;
import com.qq.qqbuy.version.dao.VersionInfoDao;
import com.qq.qqbuy.version.po.VersionInfo;
import com.qq.qqbuy.version.util.VersionInfoConstants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class VersionInfoBizImplFile implements VersionInfoBiz {

	private VersionInfoDao versionInfoDao;

	@Override
	public Long reportVersionInfo(VersionInfo versionInfo) throws Exception {
		String nowTime = DateUtils.getNowDateStr();
		versionInfo.setFirstAppVersion(versionInfo.getAppVersion());
		versionInfo.setInsertDateTime(nowTime);
		versionInfo.setStatus(0);
		versionInfo.setUpdateDateTime(nowTime);
		versionInfo.setUpdateTime(nowTime);
		int dbResult = versionInfoDao.insertVersionInfo(versionInfo);
		if (1 == dbResult) {
			return VersionInfoConstants.REPORT_SUCCESS;
		} else {
			return VersionInfoConstants.REPORT_ERROR;
		}

	}

	@Override
	public VersionInfo findVersionInfo(String mk, String appID)
			throws Exception {
		return null ;
	}

	/**
	 * 设备信息表插入或更新设备信息
	 *
	 * @param versionInfo
	 * @throws Exception
	 */
	@Override
	public void insertOrUpdateVersionInfo(VersionInfo versionInfo) throws Exception {
		String cmdId = "0x8776";
		String mk = versionInfo.getMk();
		Connection connect = DbUtil.getConnect(cmdId, mk);
		String tableName = DbUtil.getTableName(cmdId, mk);
		PreparedStatement ps = null;
		Statement st = null;
		ResultSet rs = null;
		String sql = "";
		long maxid = 1;
		boolean isExist = false;
		if (connect == null){
			throw BusinessException.createInstance(VersionInfoConstants.CONNECT_ERROR, "获取数据库连接失败！");
		}
		if (tableName == null ){
			throw BusinessException.createInstance(VersionInfoConstants.TABLENAME_ERROR, "获取设备信息表名失败！");
		}
		/**
		 * 入库信息
		 */
		String mk2 = versionInfo.getMk2();
		String mt = versionInfo.getMt();
		String client = versionInfo.getClient();
		String appVersion = versionInfo.getAppVersion();
		String versionCode = versionInfo.getVersionCode();
		String wifiMac = versionInfo.getWifiMac();
		String imei = versionInfo.getImei();
		String networkType = versionInfo.getNetworkType();
		/**
		 * 网络类型
		 */
		int netType = 0;//未知
		if (networkType != null && !"".equals(networkType)) {
			if ("2G".equals(networkType.toUpperCase())){
				netType = 2;
			}else if ("3G".equals(networkType)) {
				netType = 3;
			}else if ("4G".equals(networkType)) {
				netType = 4;
			}else if ("WIFI".equals(networkType)) {
				netType = 5;
			}else{//其他
				netType = 6;
			}
		}
		String networkServer = versionInfo.getNetworkServer();
		String osType = versionInfo.getOsType();
		String osVersion = versionInfo.getOsVersion();
		String firstAppVersion = versionInfo.getFirstAppVersion();
		String channel = versionInfo.getChannel();
		String brand = versionInfo.getBrand();
		String model = versionInfo.getModel();
		String screen = versionInfo.getScreen();
		String openUdid = versionInfo.getOpenUdid();
		String adid = versionInfo.getAdid();
		String language = versionInfo.getLanguage();
		String longitude = versionInfo.getLongitude();
		String latitude = versionInfo.getLatitude();
		Long wid = versionInfo.getWid();
		try {
			/**
			 * 判断数据库中是否存在
			 */
			sql = "select * from " + tableName + " where mk=?";
			ps = connect.prepareStatement(sql);
			ps.setString(1,mk);
			rs = ps.executeQuery();
			isExist = rs.next();
			if (isExist){//存在走更新操作
				String dbhistory = rs.getString("history");
				StringBuffer historyBuffer = new StringBuffer();
				historyBuffer.append(dbhistory).append(",");
				String dbmk2 = rs.getString("mk2");
				if (dbmk2 != null && mk2 != null && !mk2.equals(dbmk2)){
					historyBuffer.append("mk2=").append(mk2).append(";");
				}
//				String dbpinid = rs.getString("pinid");
//				String dbapp_key = rs.getString("app_key");
				String dbmt = rs.getString("mt");
				if (dbmt != null && mt != null && !dbmt.equals(mt)){
					historyBuffer.append("mt=").append(mt).append(";");
				}
				String dbclient = rs.getString("client");
				if (dbclient != null && client != null && !dbclient.equals(client)){
					historyBuffer.append("client=").append(client).append(";");
				}
				String dbapp_version = rs.getString("app_version");
				if (dbapp_version != null && appVersion != null && !dbapp_version.equals(appVersion)){
					historyBuffer.append("app_version=").append(appVersion).append(";");
				}
				int dbapp_version_code = rs.getInt("app_version_code");
				if (versionCode != null && Integer.parseInt(versionCode) > dbapp_version_code) {
					historyBuffer.append("app_version_code=").append(versionCode).append(";");
				}
				String dbwifi_mac = rs.getString("wifi_mac");
				if (dbwifi_mac != null && wifiMac != null && !wifiMac.equals(dbwifi_mac)) {
					historyBuffer.append("wifi_mac=").append(wifiMac).append(";");
				}
				String dbimei = rs.getString("imei");
				if (dbimei != null && imei != null && !imei.equals(dbimei)) {
					historyBuffer.append("imei=").append(imei).append(";");
				}
				int dbnetwork_type = rs.getInt("network_type");
				if (netType != dbnetwork_type) {
					historyBuffer.append("network_type=").append(netType).append(";");
				}
				String dbnetwork_server = rs.getString("network_server");
				if (dbnetwork_server != null && networkServer != null && !networkServer.equals(dbnetwork_server)) {
					historyBuffer.append("network_server=").append(networkServer).append(";");
				}
				String dbos_type = rs.getString("os_type");
				if (dbos_type != null && osType != null && !osType.equals(dbos_type)) {
					historyBuffer.append("os_type=").append(osType).append(";");
				}
				String dbos_version = rs.getString("os_version");
				if (dbos_version != null && osVersion != null && !osVersion.equals(dbos_version)) {
					historyBuffer.append("os_version=").append(osVersion).append(";");
				}
				String dbchannel = rs.getString("channel");
				if (dbchannel != null && channel != null && !channel.equals(dbchannel)) {
					historyBuffer.append("channel=").append(channel).append(";");
				}
				String dbbrand = rs.getString("brand");
				if (dbbrand != null && brand != null && !brand.equals(dbbrand)) {
					historyBuffer.append("brand=").append(brand).append(";");
				}
				String dbmodel = rs.getString("model");
				if (dbmodel != null && model != null && !model.equals(dbmodel)) {
					historyBuffer.append("model=").append(model).append(";");
				}
				String dbscreen = rs.getString("screen");
				if (dbscreen != null && screen != null && !screen.equals(dbscreen)) {
					historyBuffer.append("screen=").append(screen).append(";");
				}
				String dbopenudid = rs.getString("openudid");
				if (dbopenudid != null && openUdid != null && !openUdid.equals(dbopenudid)) {
					historyBuffer.append("openudid=").append(openUdid).append(";");
				}
				String dbadid = rs.getString("adid");
				if (dbadid != null && adid != null && !adid.equals(dbadid)) {
					historyBuffer.append("adid=").append(adid).append(";");
				}
				String dblanguage = rs.getString("language");
				if (dblanguage != null && language != null && !language.equals(dblanguage)) {
					historyBuffer.append("language=").append(language).append(";");
				}
				String dblongitude = rs.getString("longitude");
				try {
					if (dblongitude != null && longitude != null) {
						String dbsubstr =  dblongitude;
						String sustr = longitude;
						if (!"0".equals(dblongitude)){
							dbsubstr = dblongitude.substring(0,dblongitude.indexOf(".")+3);
						}
						if(!"0".equals(longitude)){
							sustr = longitude.substring(0,longitude.indexOf(".")+3);
						}
						if (!dbsubstr.equals(sustr)){
							historyBuffer.append("longitude=").append(longitude).append(";");
						}
					}
				}catch (Exception e){
				}
				String dblatitude = rs.getString("latitude");
				try {
					if (dblatitude != null && latitude != null) {
						String dbsubstr =  dblatitude;
						String sustr = latitude;
						if (!"0".equals(dblatitude)){
							dbsubstr = dblatitude.substring(0,dblatitude.indexOf(".")+3);
						}
						if(!"0".equals(latitude)){
							sustr = latitude.substring(0,latitude.indexOf(".")+3);
						}
						if (!dbsubstr.equals(sustr)){
							historyBuffer.append("longitude=").append(longitude).append(";");
						}
					}
				}catch (Exception e){
				}
				long dbwid = rs.getLong("wid");
				if (dbwid != wid) {
					historyBuffer.append("wid=").append(wid).append(";");
				}
				String history = historyBuffer.toString();
				//拼接update语句
				sql = "update " + tableName + " SET mk2=?,pinid=?,app_key=?,mt=?,client=?,app_version=?,app_version_code=?,wifi_mac=?,imei=?,network_type=?,network_server=?,os_type=?,os_version=?,channel=?,update_time=?,brand=?,model=?,screen=?,openudid=?,adid=?,language=?,longitude=?,latitude=?,wid=?,history=? WHERE mk=?";
				ps = connect.prepareStatement(sql);
				ps.setString(1,mk2);
				ps.setString(2,"");
				ps.setString(3,"");
				ps.setString(4,mt);
				ps.setString(5,client);
				ps.setString(6,appVersion);
				ps.setInt(7, Integer.parseInt(versionCode));
				ps.setString(8,wifiMac);
				ps.setString(9,imei);
				ps.setInt(10, netType);
				ps.setString(11,networkServer);
				ps.setString(12,osType);
				ps.setString(13,osVersion);
				ps.setString(14,channel);
				ps.setLong(15, System.currentTimeMillis());
				ps.setString(16,brand);
				ps.setString(17,model);
				ps.setString(18,screen);
				ps.setString(19,openUdid);
				ps.setString(20,adid);
				ps.setString(21,language);
				ps.setString(22,longitude);
				ps.setString(23,latitude);
				ps.setLong(24, wid);
				ps.setString(25,history);
				ps.setString(26,mk);
				ps.executeUpdate();
			}else{//不存在走插入操作
				//插入时先获取ID
				sql = "select max(id) from " + tableName;
				st = connect.createStatement();
				rs = st.executeQuery(sql);
				while (rs.next()){
					maxid = rs.getLong(1)+1;
				}
				//拼接插入sql语句
				sql = "insert into " + tableName + " (id,mk,mk2,pinid,app_key,mt,client,app_version,app_version_code,wifi_mac,imei,network_type,network_server,os_type,os_version,first_app_version,channel,insert_time,update_time,brand,model,screen,openudid,adid,language,longitude,latitude,wid,history,reserve1,reserve2,reserve3,reserve4,reserve5,reserve6) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				ps = connect.prepareStatement(sql);
				ps.setLong(1, maxid);
				ps.setString(2, mk);
				ps.setString(3,mk2);
				ps.setString(4,"");
				ps.setString(5,"");
				ps.setString(6,mt);
				ps.setString(7, client);
				ps.setString(8,appVersion);
				ps.setInt(9, Integer.parseInt(versionCode));
				ps.setString(10, wifiMac);
				ps.setString(11,imei);
				ps.setInt(12, netType);
				ps.setString(13,networkServer);
				ps.setString(14,osType);
				ps.setString(15, osVersion);
				ps.setString(16,firstAppVersion);
				ps.setString(17,channel);
				ps.setLong(18, System.currentTimeMillis());
				ps.setLong(19, 0);
				ps.setString(20,brand);
				ps.setString(21,model);
				ps.setString(22,screen);
				ps.setString(23,openUdid);
				ps.setString(24,adid);
				ps.setString(25,language);
				ps.setString(26,longitude);
				ps.setString(27,latitude);
				ps.setLong(28, wid);
				ps.setString(29,"");
				ps.setString(30,"");
				ps.setString(31,"");
				ps.setString(32,"");
				ps.setString(33,"");
				ps.setString(34,"");
				ps.setString(35, "");
				ps.execute();
			}
		}catch (Exception e){
			e.printStackTrace();
		}finally {
			if (rs != null){
				rs.close();
			}
			if (st != null){
				st.close();
			}
			if (ps != null){
				ps.close();
			}
			if (connect != null){
				connect.close();
			}
		}
	}

	public VersionInfoDao getVersionInfoDao() {
		return versionInfoDao;
	}

	public void setVersionInfoDao(VersionInfoDao versionInfoDao) {
		this.versionInfoDao = versionInfoDao;
	}

}
