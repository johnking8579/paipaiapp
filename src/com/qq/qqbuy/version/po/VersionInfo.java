package com.qq.qqbuy.version.po;

import java.util.Date;

/**
 * 安装paipai应用的手机信息收集
 * @author zhaohuayu
 *
 */
public class VersionInfo {
	/**
	 * 主键id
	 */
	private Long id ;	
	/**
	 * 手机平台：未知、iPhone、iPad、android、WP、symbian、meego
	 */
	private String client;
	/**
	 * 最新APP版本
	 */
	private String appVersion ;
	/**
	 * wifi_mac
	 */
	private String wifiMac ;
	/**
	 * imei码
	 */
	private String imei ;
	/**
	 * 网络类型：wifi、2G、3G等
	 */
	private String networkType;
	/**
	 * 运营商信息
	 */
	private String networkServer;
	/**
	 * 手机操作系统
	 */
	private String osType;
	/**
	 * 手机操作系统版本
	 */
	private String osVersion ;
	/**
	 * 第一次安装APP版本
	 */
	private String firstAppVersion;
	/**
	 * 推广渠道
	 */
	private String versionChannel;
	/**
	 * 最近一次升级时间
	 */
	private String updateTime;
	/**
	 * 手机品牌
	 */
	private String brand;
	/**
	 * 手机型号
	 */
	private String model;
	/**
	 * 屏幕大小
	 */
	private String screen;
	/**
	 * 苹果新设备标识
	 */
	private String openUdid;
	/**
	 * 苹果广告标识idfa
	 */
	private String adid;
	/**
	 * 语言
	 */
	private String language;
	/**
	 * 记录插入时间
	 */
	private String insertDateTime;
	/**
	 * 记录更新时间
	 */
	private String updateDateTime;
	/**
	 * 用户唯一标识
	 */
	private Long wid = 0L;
	/**
	 * Mobile Key 	设备唯一编号
	 */
	private String mk ;
	/**
	 * 设备类型  android\ios
	 */
	private String mt;
	/**
	 * 经度
	 */
	private String longitude;
	/**
	 * 纬度
	 */
	private String latitude ;
	/**
	 * 拍拍多个APP的唯一识别码
	 */
	private String appID ;
	/**
	 * App版本号
	 */
	private String versionCode ;
	/**
	 * 广告渠道参数
	 */
	private String channel ;
	/**
	 * 记录的状态（备用）  
	 * 默认0：可用  
	 * 1：不可用
	 */
	private int status = 0;
	
	/**
	 * 通过mk2获取pinid
	 * @author liubenlong3
	 */
	private String mk2;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getAppVersion() {
		return appVersion;
	}
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	public String getWifiMac() {
		return wifiMac;
	}
	public void setWifiMac(String wifiMac) {
		this.wifiMac = wifiMac;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getNetworkType() {
		return networkType;
	}
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
	public String getNetworkServer() {
		return networkServer;
	}
	public void setNetworkServer(String networkServer) {
		this.networkServer = networkServer;
	}
	public String getOsType() {
		return osType;
	}
	public void setOsType(String osType) {
		this.osType = osType;
	}
	public String getOsVersion() {
		return osVersion;
	}
	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}
	public String getFirstAppVersion() {
		return firstAppVersion;
	}
	public void setFirstAppVersion(String firstAppVersion) {
		this.firstAppVersion = firstAppVersion;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}	
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getScreen() {
		return screen;
	}
	public void setScreen(String screen) {
		this.screen = screen;
	}
	public String getOpenUdid() {
		return openUdid;
	}
	public void setOpenUdid(String openUdid) {
		this.openUdid = openUdid;
	}	
	public String getAdid() {
		return adid;
	}
	public void setAdid(String adid) {
		this.adid = adid;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}	
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getInsertDateTime() {
		return insertDateTime;
	}
	public void setInsertDateTime(String insertDateTime) {
		this.insertDateTime = insertDateTime;
	}
	public String getUpdateDateTime() {
		return updateDateTime;
	}
	public void setUpdateDateTime(String updateDateTime) {
		this.updateDateTime = updateDateTime;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getVersionChannel() {
		return versionChannel;
	}
	public void setVersionChannel(String versionChannel) {
		this.versionChannel = versionChannel;
	}
	public Long getWid() {
		return wid;
	}
	public void setWid(Long wid) {
		this.wid = wid;
	}
	public String getMk() {
		return mk;
	}
	public void setMk(String mk) {
		this.mk = mk;
	}
	public String getMt() {
		return mt;
	}
	public void setMt(String mt) {
		this.mt = mt;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getAppID() {
		return appID;
	}
	public void setAppID(String appID) {
		this.appID = appID;
	}
	public String getVersionCode() {
		return versionCode;
	}
	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}
	public String getMk2() {
		return mk2;
	}
	public void setMk2(String mk2) {
		this.mk2 = mk2;
	}
	
	
}
