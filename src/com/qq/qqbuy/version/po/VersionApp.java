package com.qq.qqbuy.version.po;
/**
 * app更新Bean
 * @author zhaohuayu
 *
 */
public class VersionApp {
	
	private String appKey ;//appKey  app唯一标识。 1,2……
	private String appName ;//app名称
	private String appVersion;//app的版本号
	private String appDesc;//app的描述信息
	private String versionDesc;//版本的描述信息
	private String client;//手机平台  android or ios
	private int status;//状态 默认为0：可用     1：不可用
	private String downloadUrl;//下载地址
	private String insertDateTime;//记录插入时间
	private int isForced ;//是否强制升级  0:建议  1：强制升级
	
	public String getAppKey() {
		return appKey;
	}
	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getAppVersion() {
		return appVersion;
	}
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	public String getAppDesc() {
		return appDesc;
	}
	public void setAppDesc(String appDesc) {
		this.appDesc = appDesc;
	}
	public String getVersionDesc() {
		return versionDesc;
	}
	public void setVersionDesc(String versionDesc) {
		this.versionDesc = versionDesc;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getDownloadUrl() {
		return downloadUrl;
	}
	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}
	public String getInsertDateTime() {
		return insertDateTime;
	}
	public void setInsertDateTime(String insertDateTime) {
		this.insertDateTime = insertDateTime;
	}
	public int getIsForced() {
		return isForced;
	}
	public void setIsForced(int isForced) {
		this.isForced = isForced;
	}		
	
}
