package com.qq.qqbuy.version.action;

import java.net.URLEncoder;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.qq.qqbuy.common.ConfigHelper;
import com.qq.qqbuy.common.JsonOutput;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.action.PaipaiApiBaseAction;

import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.exception.ExternalInterfaceException;

import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.common.util.StrMD5;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.thirdparty.idl.item.ItemClient;
import com.qq.qqbuy.thirdparty.idl.item.protocol.FetchItemInfoResp;
import com.qq.qqbuy.thirdparty.idl.shop.ShopClient;
import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiShopInfo;
import com.qq.qqbuy.version.biz.VersionAppBiz;
import com.qq.qqbuy.version.po.VersionApp;
import com.qq.qqbuy.version.util.VersionInfoConstants;
import org.springframework.web.util.HtmlUtils;
import org.springframework.web.util.JavaScriptUtils;

import javax.servlet.http.HttpServletRequest;
//import com.qq.qgo.share.base.Log;

public class VersionAppAction extends PaipaiApiBaseAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8220138547563098226L;
	
	//默认下载链接
	private static final String DEFAULT_UNION_URL = "http://static.paipaiimg.com/paipaiapp/paipai_v3.0.0_release_f_default.apk";
	private static final String DEFAULT_PAGE_TYPE = "home";	
	private static final String DEFAULT_UNION_TYPE = "f_default";
	private String osname;
	private String appDesc ;
	private String appKey;
	private String appName;
	private String appVersion;
	private String client;
	private String downloadUrl;
	private String versionDesc ;	
	private String verifyCode;// 参数校验
	String datastr = ""; //处理结果
	Long actResult = -1L;
	
	private String page_type = DEFAULT_PAGE_TYPE;//	 页面类型（商详、店祥、活动、其他）	 唤起协议
	private String page_param;//	 页面参数（商品ID、店铺ID、活动URL）	 唤起协议
	
	//所有广告数据结构：K1:V1,K2:V2,K3:V3
	private String gdt_vid;//	 广点通微信侧广告	 广告需求
	private String qz_gdt;//	 广点通非微信侧广告	 广告需求
	private String qz_express;// 直通车广告（搜索/情景）	 广告需求
	private String jd_pop;//	 拍拍商务舱	 广告需求
	private String pps;//	 焦点投放/易推广	 广告需求
	private String pprd_p;//	广告部参数
	
	private String union_type;//	 下载渠道	 渠道需求、统计需求
	private String ptag;//	 页面来源	 渠道需求、统计需求
	private String order_track;//	 订单跟踪（本期暂不实现）	 统计需求 
	private String tourl;
	private int isLoadH5 = 0;
	private VersionAppBiz versionAppBiz;
	private JsonOutput out = new JsonOutput();
	//H5唤起页面类型列表
	private static List<String>  pageTypes= new ArrayList<String>();
	static {
		pageTypes.add("home");//首页
		pageTypes.add("productDetail");//商详
		pageTypes.add("shopDetail");//店祥
		pageTypes.add("activity");//活动
		pageTypes.add("searchPage");//搜索页
		pageTypes.add("buyCircle");//购物圈
		pageTypes.add("buyCircleDetail");//购物圈详情
		pageTypes.add("buyCircleHome");//购物圈首页
		pageTypes.add("buyCirclePersion");//购物圈个人首页
	}
	static Logger log = LogManager.getLogger(VersionAppAction.class);
	
	public String h5CllApp(){

		//参数xss过滤
		xssFilter();

		String h5CallApp = ConfigHelper.getStringByKey(union_type, DEFAULT_UNION_URL);
		if(StringUtil.isNotEmpty(union_type) && !union_type.equals(DEFAULT_UNION_TYPE) && h5CallApp.equals(DEFAULT_UNION_URL)){
			return "error";
		}
		log.debug("h5CallApp::"+h5CallApp);
		String userAgent = getRequest().getHeader("User-Agent");
		if(StringUtil.isEmpty(page_type) || !pageTypes.contains(page_type)){
			return "error";
		}else{//参数正常情况下判断商品或者店铺是否存在，不存在跳转首页
			if("productDetail".equals(page_type)){//商品类型时判断商品是否存在
				if(StringUtil.isEmpty(page_param)){
					page_type = DEFAULT_PAGE_TYPE;
				}else{
					FetchItemInfoResp resp = ItemClient.fetchItemInfo(page_param);
					if(resp == null || resp.result != 0){
						page_type = DEFAULT_PAGE_TYPE;
					}
				}
			}else if("shopDetail".equals(page_type)){//店铺类型时判断店铺是否存在
				if(StringUtil.isEmpty(page_param)){
					page_type = DEFAULT_PAGE_TYPE;
				}else{
					try {
						ApiShopInfo shopResp = new ShopClient().getShopInfo(Long.parseLong(page_param));
						if(shopResp.getShopID() == 0){
							page_type = DEFAULT_PAGE_TYPE;
						}
					} catch (NumberFormatException e) {
						page_type = DEFAULT_PAGE_TYPE;
					} catch (ExternalInterfaceException e) {
						page_type = DEFAULT_PAGE_TYPE;
					}
				}
			}else if("activity".equals(page_type)){//活动类型
				String regex = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
				Pattern compile = Pattern.compile(regex);
				page_param = page_param.replaceAll("\\\\", "");
				boolean flag = compile.matcher(page_param).matches();
				Log.run.info("1.page_type is : " + page_type + ";and page_param is : " + page_param);
				if(flag){//是正常的URL格式再判断域名是否为paipai
					Pattern p =  Pattern.compile("(?<=//|)((\\w)+\\.)+\\w+");
					Matcher m = p.matcher(page_param);
					if(!m.find()|| !m.group().contains("paipai")){
						page_type = DEFAULT_PAGE_TYPE;
						page_param = "";
						Log.run.info("2.page_type is : " + page_type + ";and page_param is : " + page_param);
					}else{
						if(isLoadH5 != 1){
							isLoadH5 = 0;
						}
					}
				}else{
					page_type = DEFAULT_PAGE_TYPE;
					page_param = "";
					Log.run.info("3.page_type is : " + page_type + ";and page_param is : " + page_param);
				}
			}
		}
		String agent = userAgent.toLowerCase();
		if(agent.indexOf("android") != -1){//安卓系统
			osname = "android";
		}else if(agent.indexOf("iphone") != -1 || agent.indexOf("ipad") != -1 || agent.indexOf("ipod") != -1){
			osname = "ios";
		}else{
			osname = "pc";
		}
		return "h5callapp";
	}
	
	public String downLoad(){

		Log.run.info("1.page_type is : " + page_type + ";and page_param is : " + page_param);
		//参数xss过滤
		xssFilter();
		Log.run.info("2.page_type is : " + page_type + ";and page_param is : " + page_param);
		String userAgent = getRequest().getHeader("User-Agent");
		String downLoadUrl = ConfigHelper.getStringByKey(union_type, DEFAULT_UNION_URL);
		//参数校验，如果union_type不在下载渠道列表中设置为默认值
		if(StringUtil.isEmpty(union_type) || StringUtil.isBlank(union_type)){
			union_type = DEFAULT_UNION_TYPE;
		}else if(!union_type.equals(DEFAULT_PAGE_TYPE) && downLoadUrl.equals(DEFAULT_UNION_URL)){
			union_type = DEFAULT_UNION_TYPE;
		}
		downLoadUrl = ConfigHelper.getStringByKey(union_type, DEFAULT_UNION_URL);
		//ptag:页面来源，格式如下：20381.13.17、20381.1.7，即：五位数字.1-2位数字.1-2位数字
		if(null != ptag){
			Pattern pattern = Pattern.compile("^[0-9]{5}\\.[0-9]{1,2}+\\.[0-9]{1,2}+$");
			Matcher matcher = pattern.matcher(ptag);
			boolean matches = matcher.matches();
			if(!matches){
				ptag = "";
			}
		}else{
			ptag = "";
		}
		//page_type:页面类型（商详、店祥、活动、其他）：考虑到以后会增加，这里需要设置一个可以增删的列表，当不匹配下面类型时，默认跳向Home
		//page_param:页面参数（商品ID、店铺ID、活动URL）：格式对应关系如下
		if(page_type.equals("home")){//首页时page_param为空
			page_param = "";
		}else if(page_type.equals("productDetail")){//商详时page_param格式为32位，字母+数字
			Pattern compile = Pattern.compile("^[0-9a-zA-Z]{32}$");
			boolean flag = compile.matcher(page_param).matches();
			if(!flag){
				page_type = DEFAULT_PAGE_TYPE;
				page_param = "";
			}
		}else if(page_type.equals("shopDetail")){//店祥时为qq号，5-10位开始数字不能为0
			Pattern compile = Pattern.compile("^[1-9]{1}[0-9]{4,9}+$");
			boolean flag = compile.matcher(page_param).matches();
			if(!flag){
				page_type = DEFAULT_PAGE_TYPE;
				page_param = "";
			}
		}else if(page_type.equals("activity")){//活动页面，URL格式
			String regex = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
			Pattern compile = Pattern.compile(regex);
			page_param = page_param.replaceAll("\\\\", "");
			boolean flag = compile.matcher(page_param).matches();
			Log.run.info("3.page_type is : " + page_type + ";and page_param is : " + page_param);
			if(flag){//是正常的URL格式再判断域名是否为paipai
				Pattern p =  Pattern.compile("(?<=//|)((\\w)+\\.)+\\w+");
				Matcher m = p.matcher(page_param);
				if(!m.find()|| !m.group().contains("paipai")){
					page_type = DEFAULT_PAGE_TYPE;
					page_param = "";
					Log.run.info("4.page_type is : " + page_type + ";and page_param is : " + page_param);
				}
			}else{
				page_type = DEFAULT_PAGE_TYPE;
				page_param = "";
				Log.run.info("5.page_type is : " + page_type + ";and page_param is : " + page_param);
			}
		}else if(page_type.equals("searchPage")){
			//搜索页参数不处理
		}else if(page_type.equals("buyCircle")){
			//购物圈参数不处理
		}else if (page_type.equals("buyCircleDetail")){
			//购物圈参数不处理
		}else if (page_type.equals("buyCircleHome")){
			//购物圈参数不处理
		}else if (page_type.equals("buyCirclePersion")){
			//购物圈参数不处理
		}else {//不在以上六者中的
			page_type = DEFAULT_PAGE_TYPE;
			page_param = "";
		}
		String agent = userAgent.toLowerCase();
		Log.run.info("h5CallApp:" + downLoadUrl+";agent=" + agent);
		if(agent.indexOf("iphone") != -1 || agent.indexOf("ipad") != -1 || agent.indexOf("ipod") != -1){
			tourl = "https://itunes.apple.com/cn/app/pai-pai-wang-gou/id916547897";
		}else{
			tourl = downLoadUrl;
		}
		StringBuffer buffer = new StringBuffer();
		buffer.append(getRequest().getRemoteHost()).append("\t")
			.append(System.currentTimeMillis()).append("\t")
			.append(union_type).append("\t")
			.append(ptag).append("\t")
			.append(downLoadUrl).append("\t")//来源链接
			.append(userAgent).append("\t")
			.append(tourl).append("\t")
			.append(page_type).append("\t")
			.append(page_param).append("\t")
			.append(gdt_vid).append("\t")
			.append(qz_gdt).append("\t")
			.append(qz_express).append("\t")
			.append(jd_pop).append("\t")
			.append(pprd_p).append("\t")
			.append(pps);
		Log.h5CallApp.info(buffer.toString());
		return "downLoad";
	}

	private void xssFilter(){

		//html转义
		page_type = HtmlUtils.htmlEscape(page_type);
		union_type = HtmlUtils.htmlEscape(union_type);
		page_param = HtmlUtils.htmlEscape(page_param);
		gdt_vid = HtmlUtils.htmlEscape(gdt_vid);
		qz_gdt = HtmlUtils.htmlEscape(qz_gdt);
		qz_express = HtmlUtils.htmlEscape(qz_express);
		jd_pop = HtmlUtils.htmlEscape(jd_pop);
		pps = HtmlUtils.htmlEscape(pps);
		pprd_p = HtmlUtils.htmlEscape(pprd_p);
		ptag = HtmlUtils.htmlEscape(ptag);
		order_track = HtmlUtils.htmlEscape(order_track);
		tourl = HtmlUtils.htmlEscape(tourl);
		//javascript转义
		page_type = JavaScriptUtils.javaScriptEscape(page_type);
		union_type = JavaScriptUtils.javaScriptEscape(union_type);
		page_param = JavaScriptUtils.javaScriptEscape(page_param);
		gdt_vid = JavaScriptUtils.javaScriptEscape(gdt_vid);
		qz_gdt = JavaScriptUtils.javaScriptEscape(qz_gdt);
		qz_express = JavaScriptUtils.javaScriptEscape(qz_express);
		jd_pop = JavaScriptUtils.javaScriptEscape(jd_pop);
		pps = JavaScriptUtils.javaScriptEscape(pps);
		pprd_p = JavaScriptUtils.javaScriptEscape(pprd_p);
		ptag = JavaScriptUtils.javaScriptEscape(ptag);
		order_track = JavaScriptUtils.javaScriptEscape(order_track);
		tourl = JavaScriptUtils.javaScriptEscape(tourl);

	}

	public String index(){
		return "index";
	}
	
	public String reportVersionApp() {
		long start = System.currentTimeMillis();		
		try {
			// 参数校验
			validateForReport();
			VersionApp versionApp = convertToVersinoApp();
			actResult = versionAppBiz.reportVersionApp(versionApp);
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg4BExp(e, "appDesc=" + appDesc + ",appKey=" + appKey
					+ ",appName=" + appName + ",appVersion=" + appVersion
					+ ",client=" + client + ",downloadUrl=" + downloadUrl + ",versionDesc="
					+ versionDesc + ",verifyCode=" + verifyCode);
			return RST_API_FAILURE;
		} catch (Throwable e) {
			setErrcodeAndMsg4Throwable(e,  "appDesc=" + appDesc + ",appKey=" + appKey
					+ ",appName=" + appName + ",appVersion=" + appVersion
					+ ",client=" + client + ",downloadUrl=" + downloadUrl + ",versionDesc="
					+ versionDesc + ",verifyCode=" + verifyCode);
			return RST_API_FAILURE;
		} 
	}

	public String findLatestVersionApp() {
		long start = System.currentTimeMillis();
		try {
			validateForFindVersionApp();
			VersionApp versionApp = versionAppBiz.findLatestVersionApp(appKey, client);
			datastr = obj2Json(versionApp);			
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg4BExp(e, "appKey=" + appKey + ",client=" + client + ",verifyCode="
					+ verifyCode);
			return RST_API_FAILURE;
		} catch (Throwable e) {
			setErrcodeAndMsg4Throwable(e, "appKey=" + appKey + ",client=" + client + ",verifyCode="
					+ verifyCode);
			return RST_API_FAILURE;
		} 
	}

	private void validateForReport() throws BusinessException {
		if (StringUtils.isEmpty(appKey) || StringUtils.isEmpty(appVersion)
				|| StringUtils.isEmpty(client)
				|| StringUtils.isEmpty(downloadUrl)
				|| StringUtils.isEmpty(versionDesc)) {
			throw BusinessException.createInstance(
					BusinessErrorType.PARAM_ERROR, "param is empty");
		}
		StringBuffer sb = new StringBuffer();
		sb.append(appDesc).append(appKey).append(appName).append(appVersion)
				.append(client).append(downloadUrl).append(versionDesc)				
				.append(VersionInfoConstants.REPORT_VERSION_APP_KEY);
		StrMD5 md5 = new StrMD5(sb.toString());
		if (!verifyCode.equals(md5.getResult())) {
			throw BusinessException.createInstance(
					BusinessErrorType.PARAM_ERROR, "verifyCode error");
		}
	}

	private void validateForFindVersionApp() throws BusinessException {
		if (StringUtils.isEmpty(client) ||StringUtils.isEmpty(appKey) || StringUtils.isEmpty(verifyCode)) {
			throw BusinessException.createInstance(
					BusinessErrorType.PARAM_ERROR, "param is empty");
		}
		StringBuffer sb = new StringBuffer();
		sb.append(appKey).append(client).append(VersionInfoConstants.FIND_VERSION_INFO_KEY);
		StrMD5 md5 = new StrMD5(sb.toString());
		if (!verifyCode.equals(md5.getResult())) {
			throw BusinessException.createInstance(
					BusinessErrorType.PARAM_ERROR, "verifyCode error");
		}
	}

	private VersionApp convertToVersinoApp() {
		VersionApp versionApp = new VersionApp();
		versionApp.setAppDesc(appDesc);
		versionApp.setAppKey(appKey);
		versionApp.setAppName(appName);
		versionApp.setAppVersion(appVersion);
		versionApp.setClient(client);
		versionApp.setDownloadUrl(downloadUrl);
		versionApp.getVersionDesc();
		return versionApp;
	}
	
	public String checkAppUpdate()	{
		checkLogin();
		try {
			String s = Util.readTxt("res/checkAppUpdate.json");
			JsonObject text = new JsonParser().parse(s).getAsJsonObject();
			
			JsonObject androidios = text.get(getMt()).getAsJsonObject();
			int must = androidios.get("mustversion").getAsInt();
			int newvc = androidios.get("newversion").getAsInt();
			int clientvc = Integer.parseInt(getVersionCode()); 
			String update = null;
			//如果 must < client < newest : 可升
			//如果 client <= must : 必升
			if(clientvc < newvc)	{
				if(clientvc <= must)	{
					update = "m";
				} else if(clientvc > must){
					update = "p";
				}
			} else	{
				update = "n";
			}
			
			
//			服务器端依据mk2获取pinid的接口信息如下：
			String pinid = "";
			try {
				Log.run.debug("----VersionInfoAction.report()-------开始请求pinid.  mk2: " + getMk2());
				//接口人：肖志轶、王志勇、焦文建
				String pinidJson = HttpUtil.get("http://maintain.jd.com/getpin?uid=" + getMk2() + "&callback=", 10000, 10000, "gb2312", true);
				if(pinidJson.indexOf("pinid") != -1){//pinid不一定可以去得到。提供服务方定时执行setpinid，将数据同步到redis中，我们才可以取到数据
					JsonParser parser = new JsonParser();
					pinid = parser.parse(pinidJson).getAsJsonObject().get("pinid").getAsString();
					Log.run.debug("----VersionInfoAction.report()-------pinid：" + pinid);
				}
			} catch (Exception e) {
				Log.run.error(e.getMessage(), e);
			}
			
			JsonObject json = new JsonObject();
			json.add("feature", text.get("feature"));
			json.addProperty("update", update);
			json.addProperty("url", androidios.get("url").getAsString());
			json.addProperty("version", androidios.get("version").getAsString());
			json.addProperty("versioncode", androidios.get("newversion").getAsInt());
			json.addProperty("info", androidios.get("info").getAsString());
			json.addProperty("pinid", pinid);
			
			out.setData(json);
		} catch (Exception e) {
			Log.run.error(e.getMessage(), e);
			out.setErrCode("256").setMsg(e.getMessage());
		}
		return doPrint(out);
	}

	public String getAppDesc() {
		return appDesc;
	}

	public void setAppDesc(String appDesc) {
		this.appDesc = appDesc;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getVersionDesc() {
		return versionDesc;
	}

	public void setVersionDesc(String versionDesc) {
		this.versionDesc = versionDesc;
	}

	public String getVerifyCode() {
		return verifyCode;
	}

	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}

	public String getDatastr() {
		return datastr;
	}

	public void setDatastr(String datastr) {
		this.datastr = datastr;
	}	

	public Long getActResult() {
		return actResult;
	}

	public void setActResult(Long actResult) {
		this.actResult = actResult;
	}

	public VersionAppBiz getVersionAppBiz() {
		return versionAppBiz;
	}

	public void setVersionAppBiz(VersionAppBiz versionAppBiz) {
		this.versionAppBiz = versionAppBiz;
	}

	public String getPage_type() {
		return page_type;
	}

	public void setPage_type(String page_type) {
		this.page_type = page_type;
	}

	public String getPage_param() {
		return page_param;
	}

	public void setPage_param(String page_param) {
		this.page_param = page_param;
	}

	public String getGdt_vid() {
		return gdt_vid;
	}

	public void setGdt_vid(String gdt_vid) {
		this.gdt_vid = gdt_vid;
	}

	public String getQz_gdt() {
		return qz_gdt;
	}

	public void setQz_gdt(String qz_gdt) {
		this.qz_gdt = qz_gdt;
	}

	public String getQz_express() {
		return qz_express;
	}

	public void setQz_express(String qz_express) {
		this.qz_express = qz_express;
	}

	public String getJd_pop() {
		return jd_pop;
	}

	public void setJd_pop(String jd_pop) {
		this.jd_pop = jd_pop;
	}

	public String getPps() {
		return pps;
	}

	public void setPps(String pps) {
		this.pps = pps;
	}

	public String getUnion_type() {
		return union_type;
	}

	public void setUnion_type(String union_type) {
		this.union_type = union_type;
	}

	public String getPtag() {
		return ptag;
	}

	public void setPtag(String ptag) {
		this.ptag = ptag;
	}

	public String getOrder_track() {
		return order_track;
	}

	public void setOrder_track(String order_track) {
		this.order_track = order_track;
	}

	public JsonOutput getOut() {
		return out;
	}

	public void setOut(JsonOutput out) {
		this.out = out;
	}

	public String getTourl() {
		return tourl;
	}

	public void setTourl(String tourl) {
		this.tourl = tourl;
	}

	public String getPprd_p() {
		return pprd_p;
	}

	public void setPprd_p(String pprd_p) {
		this.pprd_p = pprd_p;
	}

	public String getOsname() {
		return osname;
	}

	public void setOsname(String osname) {
		this.osname = osname;
	}

	public static void main(String[] args) {
//		String itemCode = "125E263200000000040100003C22E3B4";
//		String ptag = "20381.3.7";
//		Pattern pattern = Pattern.compile("^[0-9]{5}\\.[0-9]{1,2}+\\.[0-9]{1,2}+$");
//		boolean flag = Pattern.compile("^[0-9a-zA-Z]{32}$").matcher(itemCode).matches();
//		Matcher matcher = pattern.matcher(ptag);
//		boolean matches = matcher.matches();
		//http://m.paipai.com/jujingpin/index.html
//		String url = "http://1111.paipai.com/?ptag=20419.47.1&app=1";
		String html = "http://app.paipai.com/schoolyard/schoolyard.html";
		String s1 = HtmlUtils.htmlEscape(html);
		System.out.println(s1);
		String s = JavaScriptUtils.javaScriptEscape(s1);
		System.out.println(s);
		String regex = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
//		String url = "http://app.qgo.paipai.com/promote/2014/3141?dsadasd";
		String host = "";
		System.out.println(Pattern.compile(regex).matcher(html).matches());
//		if(url!=null || !url.trim().equals("")){
////			Pattern p = Pattern.compile("(http://|https://)?([^/]*)",Pattern.CASE_INSENSITIVE);
//			Pattern compile = Pattern.compile("[^//]*?\\.(com|cn|net|org|biz|info|cc|tv)", Pattern.CASE_INSENSITIVE);
//			Matcher m = compile.matcher(url);
//			if(m.find())
//				host = m.group();  
//		}
		 Pattern p =  Pattern.compile("(?<=//|)((\\w)+\\.)+\\w+");
		  Matcher matcher = p.matcher(html);
		  if(matcher.find()){
		   host = matcher.group();
		  }
		System.out.println(host);
	}

	public int getIsLoadH5() {
		return isLoadH5;
	}

	public void setIsLoadH5(int isLoadH5) {
		this.isLoadH5 = isLoadH5;
	}
}
