package com.qq.qqbuy.version.action;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.qq.qqbuy.common.JsonOutput;
import com.qq.qqbuy.common.action.PaipaiApiBaseAction;

import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;

import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.common.util.StrMD5;
import com.qq.qqbuy.version.biz.VersionInfoBiz;
import com.qq.qqbuy.version.po.VersionInfo;
import com.qq.qqbuy.version.util.VersionInfoConstants;

public class VersionInfoAction extends PaipaiApiBaseAction {
	
	private String client;// 手机平台：未知、iPhone、iPad、android、WP、symbian、meego
	private String appVersion;// 最新APP版本
	private String wifiMac;// wifi_mac
	private String imei;// imei码
	private String networkType;// 网络类型：wifi、2G、3G等
	private String networkServer;// 运营商信息
	private String osType;// 手机操作系统
	private String osVersion; // 手机操作系统版本
	private String versionChannel; // 推广渠道
	private String brand;// 手机品牌
	private String model;// 手机型号
	private String screen;// 屏幕大小
	private String openUdid;// 苹果新设备标识
	private String adid;// 苹果广告标识idfa
	private String language;// 语言
	private String verifyCode;// 参数校验
	private String appToken;//APP 所特有的登录态字符串
//	private String mk ;//Mobile Key 	设备唯一编号
	private String mt ;//设备类型  android\ios
	private String longitude ;//经度
	private String latitude ;//纬度
	private String appID;//拍拍多个APP的唯一识别码
	private String versionCode ;//App版本号
	private String channel ;//广告渠道参数
	String datastr = ""; //处理结果
	private String mk2;
	Long actResult = -1L;
	
	private VersionInfoBiz versionInfoBiz;

	public String report() {
		long start = System.currentTimeMillis();	
//		JsonOutput out = new JsonOutput();
		
		try {
			// 参数校验
			validateForReport();
			VersionInfo versionInfo = convertToVersinoInfo();
			actResult = versionInfoBiz.reportVersionInfo(versionInfo);
			versionInfoBiz.insertOrUpdateVersionInfo(versionInfo);
//			服务器端依据mk2获取pinid的接口信息如下：
//			runlog("----VersionInfoAction.report()-------开始请求pinid.  mk2: " + mk2);
//			String pinidJson = HttpUtil.get("http://maintain.jd.com/getpin?uid=" + mk2 + "&callback=", 10000, 10000, "gb2312", true);
//			String pinid = "";
//			if(pinidJson.indexOf("pinid") != -1){//pinid不一定可以去得到。提供服务方定时执行setpinid，将数据同步到redis中，我们才可以取到数据
//				JsonParser parser = new JsonParser();
//				pinid = parser.parse(pinidJson).getAsJsonObject().get("pinid").getAsString();
//				runlog("----VersionInfoAction.report()-------pinid：" + pinid);
//			}
//			
//			JsonObject jsonObject = new  JsonObject();
//			jsonObject.addProperty("pinid", pinid);
//			out.setData(jsonObject);
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg4BExp(e,  "client=" + client + ",appVersion=" + appVersion
					+ ",wifiMac=" + wifiMac + ",imei=" + imei + ",networkType="
					+ networkType + ",networkServer=" + networkServer
					+ ",osType=" + osType + ",osVersion=" + osVersion
					+ ",versionChannel=" + versionChannel + ",brand=" + brand + ",model="
					+ model + ",screen=" + screen + ",openUdid=" + openUdid
					+ ",adid=" + adid + ",language=" + language
					+ ",verifyCode=" + verifyCode + ",appToken=" + appToken
					+ ",mk=" + getMk() + ",mt=" + mt+ ",longitude=" + longitude + ",latitude=" + latitude
					+ ",appID=" + appID + ",versionCode=" + versionCode+ ",channel=" + channel);
			return RST_API_FAILURE;
		} catch (Throwable e) {
			setErrcodeAndMsg4Throwable(e, "client=" + client + ",appVersion=" + appVersion
					+ ",wifiMac=" + wifiMac + ",imei=" + imei + ",networkType="
					+ networkType + ",networkServer=" + networkServer
					+ ",osType=" + osType + ",osVersion=" + osVersion
					+ ",versionChannel=" + versionChannel + ",brand=" + brand + ",model="
					+ model + ",screen=" + screen + ",openUdid=" + openUdid
					+ ",adid=" + adid + ",language=" + language
					+ ",verifyCode=" + verifyCode + ",appToken=" + appToken
					+ ",mk=" + getMk() + ",mt=" + mt+ ",longitude=" + longitude + ",latitude=" + latitude
					+ ",appID=" + appID + ",versionCode=" + versionCode+ ",channel=" + channel);
			return RST_API_FAILURE;
		}  
//		return doPrint(out.toJsonStr());
	}
	
	public String find() {
		long start = System.currentTimeMillis();
		try {
			validateForFindVersionInfo();
			VersionInfo versionInfo = versionInfoBiz.findVersionInfo(getMk(),appID);
			datastr = obj2Json(versionInfo);			
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg4BExp(e, "appID=" + appID + ",mk=" + getMk() + ",verifyCode="
					+ verifyCode);
			return RST_API_FAILURE;
		} catch (Throwable e) {
			setErrcodeAndMsg4Throwable(e, "appID=" + appID + ",mk=" + getMk() + ",verifyCode="
					+ verifyCode);
			return RST_API_FAILURE;
		}  
	}

	private void validateForReport() throws BusinessException, UnsupportedEncodingException {
		if (StringUtils.isEmpty(appID) || StringUtils.isEmpty(getMk())
				|| StringUtils.isEmpty(appVersion)
				|| StringUtils.isEmpty(verifyCode)) {
			throw BusinessException.createInstance(
					BusinessErrorType.PARAM_ERROR, "param is empty");
		}
		StringBuffer sb = new StringBuffer();
		sb.append(client).append(appVersion)
				.append(wifiMac).append(imei).append(networkType)
				.append(networkServer).append(osType).append(osVersion)
				.append(versionChannel).append(brand).append(model).append(screen)
				.append(openUdid).append(adid).append(language).append(URLEncoder.encode(appToken,"utf-8"))
				.append(getMk()).append(mt).append(longitude).append(latitude)
				.append(appID).append(versionCode).append(channel)
				.append(VersionInfoConstants.REPORT_VERSION_INFO_KEY);
		StrMD5 md5 = new StrMD5(sb.toString());
		if (!verifyCode.equals(md5.getResult())) {
			throw BusinessException.createInstance(
					BusinessErrorType.PARAM_ERROR, "verifyCode error");
		}
	}

	private void validateForFindVersionInfo() throws BusinessException {
		if (StringUtils.isEmpty(getMk()) ||StringUtils.isEmpty(appID) || StringUtils.isEmpty(verifyCode)) {
			throw BusinessException.createInstance(
					BusinessErrorType.PARAM_ERROR, "param is empty");
		}
		StringBuffer sb = new StringBuffer();
		sb.append(appID).append(getMk()).append(VersionInfoConstants.FIND_VERSION_INFO_KEY);
		StrMD5 md5 = new StrMD5(sb.toString());
		if (!verifyCode.equals(md5.getResult())) {
			throw BusinessException.createInstance(
					BusinessErrorType.PARAM_ERROR, "verifyCode error");
		}
	}

	private VersionInfo convertToVersinoInfo() {
		VersionInfo versionInfo = new VersionInfo();
		versionInfo.setAdid(adid);
		versionInfo.setAppID(appID);
		versionInfo.setAppVersion(versionCode);
		versionInfo.setBrand(brand);
		versionInfo.setChannel(channel);
		versionInfo.setClient(client);
		versionInfo.setImei(imei);
		versionInfo.setLanguage(language);
		versionInfo.setModel(model);
		versionInfo.setNetworkServer(networkServer);
		versionInfo.setNetworkType(networkType);
		versionInfo.setOpenUdid(openUdid);
		versionInfo.setOsType(osType);
		versionInfo.setOsVersion(osVersion);
		versionInfo.setScreen(screen);
		versionInfo.setMk(getMk());
		versionInfo.setMt(mt);
		versionInfo.setWid(this.getWidFromToken(appToken)) ;
		versionInfo.setLongitude(longitude);
		versionInfo.setLatitude(latitude);
		versionInfo.setVersionChannel(versionChannel);
		versionInfo.setChannel(channel);
		versionInfo.setVersionCode(versionCode);
		versionInfo.setWifiMac(wifiMac);
		versionInfo.setMk2(mk2);
		return versionInfo;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getWifiMac() {
		return wifiMac;
	}

	public void setWifiMac(String wifiMac) {
		this.wifiMac = wifiMac;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	public String getNetworkServer() {
		return networkServer;
	}

	public void setNetworkServer(String networkServer) {
		this.networkServer = networkServer;
	}

	public String getOsType() {
		return osType;
	}

	public void setOsType(String osType) {
		this.osType = osType;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getScreen() {
		return screen;
	}

	public void setScreen(String screen) {
		this.screen = screen;
	}

	public String getOpenUdid() {
		return openUdid;
	}

	public void setOpenUdid(String openUdid) {
		this.openUdid = openUdid;
	}
	

	public String getAdid() {
		return adid;
	}

	public void setAdid(String adid) {
		this.adid = adid;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getVerifyCode() {
		return verifyCode;
	}

	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}

	public VersionInfoBiz getVersionInfoBiz() {
		return versionInfoBiz;
	}

	public void setVersionInfoBiz(VersionInfoBiz versionInfoBiz) {
		this.versionInfoBiz = versionInfoBiz;
	}

	public String getDatastr() {
		return datastr;
	}

	public void setDatastr(String datastr) {
		this.datastr = datastr;
	}

	public Long getActResult() {
		return actResult;
	}

	public void setActResult(Long actResult) {
		this.actResult = actResult;
	}

	public String getVersionChannel() {
		return versionChannel;
	}

	public void setVersionChannel(String versionChannel) {
		this.versionChannel = versionChannel;
	}

	public String getAppToken() {
		return appToken;
	}

	public void setAppToken(String appToken) {
		this.appToken = appToken;
	}

//	public String getMk() {
//		return mk;
//	}
//
//	public void setMk(String mk) {
//		this.mk = mk;
//	}

	public String getMt() {
		return mt;
	}

	public void setMt(String mt) {
		this.mt = mt;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getAppID() {
		return appID;
	}

	public void setAppID(String appID) {
		this.appID = appID;
	}

	public String getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}
	public String getMk2() {
		return mk2;
	}

	public void setMk2(String mk2) {
		this.mk2 = mk2;
	}

	
}
