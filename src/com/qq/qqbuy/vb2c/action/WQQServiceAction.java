package com.qq.qqbuy.vb2c.action;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.action.PaipaiApiBaseAction;

import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.AntiXssHelper;
import com.qq.qqbuy.common.util.CoderUtil;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.vb2c.biz.Vb2cQQServiceBiz;
import com.qq.qqbuy.vb2c.po.QQService4Chong;
import com.qq.qqbuy.vb2c.po.QQServiceOrderDealOutPo;
import com.qq.qqbuy.vb2c.po.ValidateQQServicePo;
import com.qq.qqbuy.vb2c.po.WQQServiceOutPo;
import com.qq.qqbuy.vb2c.util.TenpayVb2cUtil;
import com.qq.qqbuy.vb2c.util.Vb2cChecker;
import com.qq.qqbuy.vb2c.util.VersionType;

/**
 * Wap2.0的QQ服务
 * @author winsonwu
 * @Created 2012-5-10
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class WQQServiceAction extends PaipaiApiBaseAction {

	private static final long serialVersionUID = 7223807708778038691L;
	
	private int gid;  //gameID, QQ服务对应的网游ID
	private int amount;  //QQ服务的面额
	private String bc;   //buyCount, 购买数量, 由于通过input text输入，故这里需要使用String类型
	private String cuin; //用户充值的QQ号
	private int f; //from, 来源, f=1时候进行充值支付流程，detail处使用
	private String s; //vb2cTag
	
	private QQService4Chong[] qqServices;
	private WQQServiceOutPo qqServiceDetailPo;
	private String tenpayUrl;
	
	// Temp变量
	private ValidateQQServicePo validateProductPo;
	private QQService4Chong validateQQService;
	
	/************************* 注入参数 **************************/
	private Vb2cQQServiceBiz qqServiceBiz;
	
	public String buy() {
		long start = System.currentTimeMillis();
		try {
			String backUrl = getActionPath("", "w"); 
			getLoginInfo(backUrl);
			
			qqServices = QQService4Chong.queryQQServicesByType(QQService4Chong.SERVICE_TYPE_PRIMARY);
			
			return RST_SUCCESS;
		} catch (Exception e) {
		//	result.setErrCode(ErrConstant.ERRCODE_FIND_THROWABLE_EXP);
			result.setMsg("Wap2.0虚拟QQ服务列表出现异常");
			Log.run.warn("Wap2.0虚拟QQ服务列表异常", e);
			return RST_FAILURE;
		} finally {
			result.setDtag(dtag);
		}
	}
	
	public String detail() {
		long start = System.currentTimeMillis();
		try {
			String backUrl = getActionPath("", "w", "gid", gid, "amount", amount, "bc", bc, "cuin", cuin, "f", f, "s", s);
			getLoginInfo(backUrl);
			
			if (f == 1) {
				// 进入充值流程
				if (validateQQServiceOrderDeal()) {
//					getLoginInfo(backUrl); // TODO winson 本地测试
					String checkResult = checkLogin(backUrl);
					if(checkResult != null){
						return checkResult;
					}
					validateProductPo.setSid(getSid());
					validateProductPo.setMkey(getMachineKey());
					validateProductPo.setSkey(getSk());
					validateProductPo.setUin(getQq());
					
					//处理下单逻辑
					try {
						String commonCftAttach = TenpayVb2cUtil.genWap2Vb2cCommonCtfAttach(getSid(), getPgid(), getIcfa(), getO_icfa(), getGcfa());
						QQServiceOrderDealOutPo po = qqServiceBiz.orderDeal(VersionType.VERSION_WAP2, validateProductPo, getQq(), cuin, s, commonCftAttach);
//						QQServiceOrderDealOutPo po = qqServiceBiz.orderDeal(validateProductPo, 381936231, "381936231", "vb2c", commonCftAttach);  // TODO winson 测试
						tenpayUrl = po.getTenpayUrl();
						return "cft-pay";
					} catch (BusinessException e) {
						result.setErrCode(e.getErrCode());
						result.setMsg(e.getErrMsg());
						Log.run.warn("vb2c qqservice order deal get Token fail!");
					}
				}
			}
			
			qqServiceDetailPo = qqServiceBiz.wap2Detail(validateQQService);
			
			return RST_SUCCESS;
		} catch (Exception e) {
			//result.setErrCode(ErrConstant.ERRCODE_FIND_THROWABLE_EXP);
			result.setMsg("Wap2.0虚拟QQ服务详情出现异常");
			Log.run.warn("Wap2.0虚拟QQ服务详情出现异常", e);
			return RST_FAILURE;
		} finally {
			result.setDtag(dtag);
		}
		
	}
	
	private boolean validateQQServiceOrderDeal() {
		int buyCount = StringUtil.toInt(bc, -1);
		validateProductPo = Vb2cChecker.checkQQServiceProduct(result, gid, amount, buyCount);
		if (result.getErrCode() != 0) {
			return false;
		}
//		if (!Vb2cChecker.checkQQUin(result, cuin)) {
//			return false;
//		}
		return true;
	}
	
	private boolean validateQQServiceDetail() {
		validateQQService = Vb2cChecker.checkQQServiceGameId(result, gid);
		if (result.getErrCode() != 0) {
			return false;
		}
		return true;
	}

	public QQService4Chong[] getQqServices() {
		return qqServices;
	}

	public void setQqServices(QQService4Chong[] qqServices) {
		this.qqServices = qqServices;
	}

	public Vb2cQQServiceBiz getQqServiceBiz() {
		return qqServiceBiz;
	}

	public void setQqServiceBiz(Vb2cQQServiceBiz qqServiceBiz) {
		this.qqServiceBiz = qqServiceBiz;
	}

	public int getGid() {
		return gid;
	}

	public void setGid(int gid) {
		this.gid = gid;
	}

	public String getBc() {
		return bc;
	}

	public void setBc(String bc) {
		this.bc = CoderUtil.decodeWML(AntiXssHelper.xmlAttributeEncode(bc));
	}

	public int getF() {
		return f;
	}

	public void setF(int f) {
		this.f = f;
	}

	public String getCuin() {
		return cuin;
	}

	public void setCuin(String cuin) {
		this.cuin = CoderUtil.decodeWML(AntiXssHelper.xmlAttributeEncode(cuin));
	}

	public String getS() {
		return s;
	}

	public void setS(String s) {
		this.s = CoderUtil.decodeWML(AntiXssHelper.xmlAttributeEncode(s));
	}

	public String getTenpayUrl() {
		return tenpayUrl;
	}

	public void setTenpayUrl(String tenpayUrl) {
		this.tenpayUrl = tenpayUrl;
	}

	public WQQServiceOutPo getQqServiceDetailPo() {
		return qqServiceDetailPo;
	}

	public void setQqServiceDetailPo(WQQServiceOutPo qqServiceDetailPo) {
		this.qqServiceDetailPo = qqServiceDetailPo;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
}
