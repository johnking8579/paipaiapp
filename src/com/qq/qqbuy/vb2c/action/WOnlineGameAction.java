package com.qq.qqbuy.vb2c.action;

import java.util.ArrayList;
import java.util.List;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.action.PaipaiApiBaseAction;

import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.AntiXssHelper;
import com.qq.qqbuy.common.util.CoderUtil;
import com.qq.qqbuy.common.util.url.URLUtil;
import com.qq.qqbuy.vb2c.biz.Vb2cGameBiz;
import com.qq.qqbuy.vb2c.po.OnlineGame;
import com.qq.qqbuy.vb2c.po.ValidateGamePo;
import com.qq.qqbuy.vb2c.po.Vb2cOrderDealOutPo;
import com.qq.qqbuy.vb2c.po.WGameDetailOutPo;
import com.qq.qqbuy.vb2c.po.game.Product;
import com.qq.qqbuy.vb2c.po.game.SectionServer;
import com.qq.qqbuy.vb2c.util.GameCache4File;
import com.qq.qqbuy.vb2c.util.TenpayVb2cUtil;
import com.qq.qqbuy.vb2c.util.Vb2cChecker;
import com.qq.qqbuy.vb2c.util.Vb2cUtil;
import com.qq.qqbuy.vb2c.util.VersionType;
import com.qq.qqbuy.vb2c.util.WGameDetailIndexer;

/**
 * 网游
 * @author winsonwu
 * @Created 2012-4-23
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class WOnlineGameAction extends PaipaiApiBaseAction {
	
	private static final long serialVersionUID = 5200355584148810154L;

	private String cname; //categoryName,取值为9，A-Z
	private int id; //gameId, 代表游戏
	private int i; //index, 用于区别不同的页面[1=面额选择页, 2=充值类型选择页, 3=分区选择页, 4=服务器选择页, 5=数量选择页, 6=确认页面]
	private int oi; //前一个page的index
	private int amount;
	private String ct; //充值类型
	private String sc; //sectionCode,分区的编码
	private String svc; //serverCode,服务器的编码
	private int bc; //buyCount,购买数量
	private String ac; //account, 充值账户
	private String ac1; //account1, 充值账户确认
	private String wp;  //war permit, 战网通行证
	private String wp1;  //战网通行证确认
	private String s; //vb2cTag
	
	private List<OnlineGame> games = new ArrayList<OnlineGame>();
	private List<OnlineGame> hotgames = new ArrayList<OnlineGame>();
	private List<String> categories = new ArrayList<String>();
	private WGameDetailOutPo gameDetailPo;
	private String tenpayUrl;

	//临时变量
	private ValidateGamePo validateGamePo;  
	private ValidateGamePo validateGameDetailPo;  
	
	private Vb2cGameBiz gameBiz;
	
	public String buy() {
		long start = System.currentTimeMillis();
		try {
			Log.run.debug("chargeType: " + ct);
			String enCodeCt = CoderUtil.encodeUtf8(ct);
			String backUrl = getActionPath("/w/vb2c/buyGame.xhtml", "w", "id", id, "i", i, "oi", oi, "amount", amount, 
					"ct", enCodeCt, "sc", sc, "svc", svc, "bc", bc, "ac", ac, "ac1", ac1); 
			getLoginInfo(backUrl);
			
			hotgames = GameCache4File.getHotOnlineGames();
			
			categories = Vb2cUtil.getGameCategories();
			
			return RST_SUCCESS;
		} catch (Exception e) {
			//result.setErrCode(ErrConstant.ERRCODE_FIND_THROWABLE_EXP);
			result.setMsg("Wap2.0网游充值出现异常");
			Log.run.warn("Wap2.0网游充值出现异常", e);
			return RST_FAILURE;
		} finally {
			result.setDtag(dtag);
		}
	}
	
	public String list() {
		long start = System.currentTimeMillis();
		try {
			Log.run.debug("chargeType: " + ct);
			String enCodeCt = CoderUtil.encodeUtf8(ct);
			String backUrl = getActionPath("/w/vb2c/listGame.xhtml", "w", "id", id, "i", i, "oi", oi, "amount", amount, 
					"ct", enCodeCt, "sc", sc, "svc", svc, "bc", bc, "ac", ac, "ac1", ac1);
			getLoginInfo(backUrl);
			
			if (!validateListInput()) {
				return RST_FAILURE;
			}
			
			games = GameCache4File.getGamesByCategory(cname);
			
			return RST_SUCCESS;
		} catch (Exception e) {
			//result.setErrCode(ErrConstant.ERRCODE_FIND_THROWABLE_EXP);
			result.setMsg("Wap2.0网游列表访问异常");
			Log.run.warn("Wap2.0网游列表访问异常", e);
			return RST_FAILURE;
		} finally {
			result.setDtag(dtag);
		}
	}
	
	
	public String detail() {
		long start = System.currentTimeMillis();
		try {
			Log.run.debug("chargeType: " + ct);
			// 过来的参数需要先进行Decoder，因为url里面对参数进行了encode才传递过来
			ct = CoderUtil.decodeUtf8(ct);
			ac = CoderUtil.decodeUtf8(ac);
			ac1 = CoderUtil.decodeUtf8(ac1);
			wp = CoderUtil.decodeUtf8(wp);
			wp1 = CoderUtil.decodeUtf8(wp1);
			
			String enCodeCt = CoderUtil.encodeUtf8(ct);
			String backUrl = getActionPath("/w/vb2c/detailGame.xhtml", "w", "id", id, "i", i, "oi", oi, "amount", amount, 
					"ct", enCodeCt, "sc", sc, "svc", svc, "bc", bc, "ac", ac, "ac1", ac1, "wp", wp, "wp1", wp1, "s", s);
			getLoginInfo(backUrl);
			ValidateGamePo gamePo = null;
			if (i == 7) {
				// 立即充值
				if (validateOrderDeal()) {
					String checkResult = checkLogin(backUrl);
					if(checkResult != null){
						return checkResult;
					}
					validateGamePo.setSid(getSid());
					validateGamePo.setMkey(getMachineKey());
					validateGamePo.setSkey(getSk());
					validateGamePo.setUin(getQq());
					
					//处理下单逻辑
					try {
						String account = ac;
						if (Vb2cChecker.isWOW(id)) {
							account = wp + "*" + ac;
						}
						String commonCftAttach = TenpayVb2cUtil.genWap2Vb2cCommonCtfAttach(getSid(), getPgid(), getIcfa(), getO_icfa(), getGcfa());
						Vb2cOrderDealOutPo po = gameBiz.orderDeal(VersionType.VERSION_WAP2, validateGamePo, getQq(), account, s, commonCftAttach);
						tenpayUrl = po.getTenpayUrl();
						return "cft-pay";
					} catch (BusinessException e) {
						result.setErrCode(e.getErrCode());
						result.setMsg(e.getErrMsg());
						Log.run.warn("vb2c game order deal get Token fail!");
					}
				} 
				// 处理参数检查错误逻辑
				i = oi; //错误页面停留回上一个页面
				gamePo = validateGamePo;
			} else {
				//参数处理
				if (i<=0 || i>6) {
					i = 1; //默认到面额选择页面
				}
				
				// 参数校验
				if (!validateDetailInput()) {
					i = oi;  //参数校验失败，页面展示应该在上一个页面
				}
				
				gamePo = validateGameDetailPo;
			}
			gameDetailPo = gameBiz.wap2detail(gamePo, i, ct, ac, ac1, wp, wp1);
			return RST_SUCCESS;
		} catch (Exception e) {
		//	result.setErrCode(ErrConstant.ERRCODE_FIND_THROWABLE_EXP);
			result.setMsg("Wap2.0网游详情访问异常");
			Log.run.warn("Wap2.0网游详情访问异常", e);
			return RST_FAILURE;
		} finally {
			result.setDtag(dtag);
		}
	}
	
	public boolean validateListInput() {
		return Vb2cChecker.checkOnlineGameCategory(result, cname);
	}
	
	public boolean validateDetailInput() {
		// 检查GameId
		validateGameDetailPo = Vb2cChecker.checkGameId(result, id);
		if (result.getErrCode() != 0) {
			return false;
		}
		// 根据各个Index来校验相应的参数
		switch (i) {
		case WGameDetailIndexer.WGAME_DETAIL_INDEX_AMOUNT:
			break;
		case WGameDetailIndexer.WGAME_DETAIL_INDEX_CHARGETYPE:
			break;
		case WGameDetailIndexer.WGAME_DETAIL_INDEX_SECTION:
			break;
		case WGameDetailIndexer.WGAME_DETAIL_INDEX_SERVER:
			// 检验网游区编码
			SectionServer currSectionServer = Vb2cChecker.checkGameSectionCode(result, validateGameDetailPo.getSectionServers(), sc);
			if (result.getErrCode() != 0) {
				return false;
			}
			validateGameDetailPo.setSectionCode(sc);
			validateGameDetailPo.setCurrSectionServer(currSectionServer);
			break;
		case WGameDetailIndexer.WGAME_DETAIL_INDEX_BUYCOUNT:
			// 检查网游的面额和充值类型
			Product currProduct = Vb2cChecker.checkGameAmountChargeType(result, validateGameDetailPo.getProducts(), amount, ct);
			if (result.getErrCode() != 0) {
				return false;
			}
			validateGameDetailPo.setAmount(amount);
			validateGameDetailPo.setChargeType(ct);
			validateGameDetailPo.setCurrProduct(currProduct);
			break;
		case WGameDetailIndexer.WGAME_DETAIL_INDEX_ALLINFO:
			// 检查网游的面额、充值类型、购买数量、帐号、区、服务器信息
			validateGameDetailPo = Vb2cChecker.checkGame(result, id, amount, ct, bc, sc, svc); //此处重新校验，故validateDetailGamePo重新赋值
			if (result.getErrCode() != 0) {
				return false;
			}
			// 校验网游输入的帐号: ac、ac1
			if (!Vb2cChecker.checkOnlineGameAccount(result, id, ac, ac1)) {
				return false;
			}
			break;
		}
		
		return true;
	}
	
	public boolean validateOrderDeal() {
		// 校验网游ID、面额、充值类型、购买数量、网游区、服务器的参数：id、amount、ct、bc、sc、svc
		validateGamePo = Vb2cChecker.checkGame(result, id, amount, ct, bc, sc, svc);
		if (result.getErrCode() != 0) {
			return false;
		}
		// 校验网游输入的帐号: ac、ac1
		if (!Vb2cChecker.checkOnlineGameAccount(result, id, ac, ac1)) {
			return false;
		}
		
		// 校验魔兽世界的战网通行证： wp、wp1
		if (!Vb2cChecker.checkWarPermit(result, id, wp, wp1)) {
			return false;
		}
		
		return true;
	}
	
	public List<OnlineGame> getHotgames() {
		return hotgames;
	}

	public void setHotgames(List<OnlineGame> hotgames) {
		this.hotgames = hotgames;
	}

	public List<String> getCategories() {
		return categories;
	}

	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = CoderUtil.decodeWML(AntiXssHelper.xmlAttributeEncode(cname));
	}

	public List<OnlineGame> getGames() {
		return games;
	}

	public void setGames(List<OnlineGame> games) {
		this.games = games;
	}

	public int getI() {
		return i;
	}

	public void setI(int i) {
		this.i = i;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getCt() {
		return ct;
	}

	public void setCt(String ct) {
		this.ct = CoderUtil.decodeWML(AntiXssHelper.xmlAttributeEncode(ct));
	}

	public String getSc() {
		return sc;
	}

	public void setSc(String sc) {
		this.sc = CoderUtil.decodeWML(AntiXssHelper.xmlAttributeEncode(sc));
	}

	public String getSvc() {
		return svc;
	}

	public void setSvc(String svc) {
		this.svc = CoderUtil.decodeWML(AntiXssHelper.xmlAttributeEncode(svc));
	}

	public int getBc() {
		return bc;
	}

	public void setBc(int bc) {
		this.bc = bc;
	}

	public String getAc() {
		return ac;
	}

	public void setAc(String ac) {
		this.ac = URLUtil.decodeCheckUrlValue(CoderUtil.decodeWML(AntiXssHelper.xmlAttributeEncode(ac)));
	}

	public String getAc1() {
		return ac1;
	}

	public void setAc1(String ac1) {
		this.ac1 = URLUtil.decodeCheckUrlValue(CoderUtil.decodeWML(AntiXssHelper.xmlAttributeEncode(ac1)));
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOi() {
		return oi;
	}

	public void setOi(int oi) {
		this.oi = oi;
	}

	public Vb2cGameBiz getGameBiz() {
		return gameBiz;
	}

	public void setGameBiz(Vb2cGameBiz gameBiz) {
		this.gameBiz = gameBiz;
	}

	public String getTenpayUrl() {
		return tenpayUrl;
	}

	public void setTenpayUrl(String tenpayUrl) {
		this.tenpayUrl = tenpayUrl;
	}

	public String getS() {
		return s;
	}

	public void setS(String s) {
		this.s = CoderUtil.decodeWML(AntiXssHelper.xmlAttributeEncode(s));
	}

	public WGameDetailOutPo getGameDetailPo() {
		return gameDetailPo;
	}

	public void setGameDetailPo(WGameDetailOutPo gameDetailPo) {
		this.gameDetailPo = gameDetailPo;
	}

	public String getWp() {
		return wp;
	}

	public void setWp(String wp) {
		this.wp = URLUtil.decodeCheckUrlValue(CoderUtil.decodeWML(AntiXssHelper.xmlAttributeEncode(wp)));
	}

	public String getWp1() {
		return wp1;
	}

	public void setWp1(String wp1) {
		this.wp1 = URLUtil.decodeCheckUrlValue(CoderUtil.decodeWML(AntiXssHelper.xmlAttributeEncode(wp1)));
	}
	
}
