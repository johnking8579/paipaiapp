package com.qq.qqbuy.vb2c.action;


import com.qq.qqbuy.common.action.PaipaiApiBaseAction;

import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.AntiXssHelper;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.vb2c.biz.TenpayVb2cBiz;
import com.qq.qqbuy.vb2c.po.CftVb2cAttach;
import com.qq.qqbuy.vb2c.util.TenpayVb2cUtil;

/**
 * 虚拟支付：财付通支付成功失败回调页面:Wap2.0
 * 
 * @author winsonwu
 * 
 */
public class WTenpayVB2CAction extends PaipaiApiBaseAction {

	private static final long serialVersionUID = 7895800240502901676L;

	private TenpayVb2cBiz tenpayVb2cBiz;

	/********************* 支付成功后财付通的回跳参数 ***********************************/
	/**
	 * 版本号,ver默认值是1.0。目前版本ver取值应为2.0
	 */
	private String ver;

	/**
	 * 1 UTF-8, 2 GB2312, 默认为1 UTF-8
	 */
	private String charset;

	/**
	 * 支付结果： 0—成功；其它—失败
	 */
	private String pay_result;

	/**
	 * 财付通交易号(订单号)
	 */
	private String transaction_id;

	/**
	 * 商户系统内部的定单号，此参数仅在对账时提供。
	 */
	private String sp_billno;

	/**
	 * 订单总金额，以分为单位;
	 */
	private String total_fee;

	/**
	 * 现金支付币种
	 */
	private String fee_type;

	/**
	 * 卖方账号（商户spid）;
	 */
	private String bargainor_id;

	/**
	 * 商家数据包，原样返回
	 */
	private String attach;

	/**
	 * MD5签名结果,详见“商户签名规则”
	 */
	private String sign = "";
	
	/**
     * 银行类型:财付通支付填0
     */
    private String bank_type;
    
    /**
     * 银行订单号，若为财付通余额支付则为空
     */
    private String bank_billno;
    
    /**
     * 支付结果信息，支付成功时为空
     */
    private String pay_info;
    
    /**
     * 买家唯一标识，由财付通生成。注意不同于purchase_id财付通帐户
     */
    private String purchase_alias;
    
    /**
     * 支付完成时间，格式为yymmddhhmmss，如2009年12月27日9点10分10秒表示为20091227091010。时区为GMT+8
     * beijing。该时间取自财付通服务器
     */
    private String time_end="";
    
    /**
     * 商户号
     */
    private String sign_sp_id;
    
    /********************************************************** JSP页面展示的内容 ***********************************************************/
    private CftVb2cAttach attachPo; //财付通返回的商户自定义数据

	// 财付通在页面上回调的url
	public String tenpayCallBack() {
		String cftCallbackParam = "ver:" + ver + " charset:" + charset
				+ " pay_result:" + pay_result + " transaction_id:"
				+ transaction_id + " sp_billno:" + sp_billno + " total_fee:"
				+ total_fee + " fee_type:" + fee_type + " bargainor_id:"
				+ bargainor_id + " attach:" + attach + " sign_sp_id:" + sign_sp_id + " sign:" + sign;

		long start = System.currentTimeMillis();
		try {
			if (!StringUtil.isEmpty(attach)) {
				attachPo = TenpayVb2cUtil.deSerializeCftAttach(attach);
				// 设置相应的请求参数(统计日志、登录态)
				setSid(attachPo.getSid());
				setPageId(attachPo.getPgId());
				setIcfa(attachPo.getIcfa());
				setO_icfa(attachPo.getO_icfa());
				setGcfa(attachPo.getGcfa());
			}
			
			// Sid登录态校验
			String backUrl = getActionPath("", "w", "ver", ver, "charset", charset, "pay_result", pay_result, 
					"transaction_id", transaction_id, "sp_billno", sp_billno, "total_fee", total_fee, "fee_type", fee_type, "bargainor_id", bargainor_id,
					"attach", attach, "sign_sp_id", sign_sp_id, "sign", sign);
			getLoginInfo(backUrl);
			
			// 判断MD5和财付通返回的支付结果
			boolean isSuccess = tenpayVb2cBiz.tenpayCallBack(ver, charset, pay_result, transaction_id, sp_billno, total_fee, fee_type, bargainor_id, attach, sign_sp_id, sign);
			
			return isSuccess ? RST_SUCCESS : RST_FAILURE;
		} catch (BusinessException e) {
			setErrCodeAndMsg4BExp(e);
			return RST_FAILURE;
		} catch (Exception e) {
			//setErrCodeAndMsg(ErrConstant.ERRCODE_FIND_THROWABLE_EXP, e
			//		.getClass().getSimpleName());
			return RST_FAILURE;
		} finally {
			result.setDtag(dtag);
		}
	}

	// 财付通在后台回调的url
	public String tenpayNotify() {
		String cfgNotifyParam = "ver:" + ver + " charset:" + charset + " bank_type:" + bank_type + " bank_billno:" + bank_billno + 
				" pay_result:" + pay_result + " pay_info:" + pay_info + " purchase_alias:" + purchase_alias + " bargainor_id:" + bargainor_id
				+ " transaction_id:" + transaction_id + " sp_billno:" + sp_billno + " total_fee:" + total_fee + " fee_type:" + fee_type +
				" attach:" + attach + " time_end:" + time_end + " sign_sp_id:" + sign_sp_id + " sign:" + sign; 
		
		long start = System.currentTimeMillis();
		try {
			 // 鉴权,财付通回跳不用验证登陆
			 initLoginParam(sign);
			
			 boolean isMsgSent = tenpayVb2cBiz.tenpayNotify(ver, charset, bank_type, bank_billno, pay_result, pay_info,
					 purchase_alias, bargainor_id, transaction_id, sp_billno, total_fee, fee_type, attach, time_end, sign_sp_id, sign);

			return isMsgSent ? RST_SUCCESS : RST_FAILURE;
		} catch (BusinessException e) {
			setErrCodeAndMsg4BExp(e);
			return RST_FAILURE;
		} catch (Exception e) {
			//setErrCodeAndMsg(ErrConstant.ERRCODE_FIND_THROWABLE_EXP, e
			//		.getClass().getSimpleName());
			return RST_FAILURE;
		} finally {
			result.setDtag(dtag);
		}
	}

    public String weixinFocus() {
        return SUCCESS;
    }

	public String getVer() {
		return ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public String getPay_result() {
		return pay_result;
	}

	public void setPay_result(String payResult) {
		pay_result = payResult;
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transactionId) {
		transaction_id = transactionId;
	}

	public String getSp_billno() {
		return sp_billno;
	}

	public void setSp_billno(String spBillno) {
		sp_billno = spBillno;
	}

	public String getTotal_fee() {
		return total_fee;
	}

	public void setTotal_fee(String totalFee) {
		total_fee = totalFee;
	}

	public String getFee_type() {
		return fee_type;
	}

	public void setFee_type(String feeType) {
		fee_type = feeType;
	}

	public String getBargainor_id() {
		return bargainor_id;
	}

	public void setBargainor_id(String bargainorId) {
		bargainor_id = bargainorId;
	}

	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = AntiXssHelper.htmlAttributeEncode(attach);;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public TenpayVb2cBiz getTenpayVb2cBiz() {
		return tenpayVb2cBiz;
	}

	public void setTenpayVb2cBiz(TenpayVb2cBiz tenpayVb2cBiz) {
		this.tenpayVb2cBiz = tenpayVb2cBiz;
	}

	public String getBank_type() {
		return bank_type;
	}

	public void setBank_type(String bankType) {
		bank_type = bankType;
	}

	public String getBank_billno() {
		return bank_billno;
	}

	public void setBank_billno(String bankBillno) {
		bank_billno = bankBillno;
	}

	public String getPay_info() {
		return pay_info;
	}

	public void setPay_info(String payInfo) {
		pay_info = payInfo;
	}

	public String getPurchase_alias() {
		return purchase_alias;
	}

	public void setPurchase_alias(String purchaseAlias) {
		purchase_alias = purchaseAlias;
	}

	public String getTime_end() {
		return time_end;
	}

	public void setTime_end(String timeEnd) {
		time_end = timeEnd;
	}

	public String getSign_sp_id() {
		return sign_sp_id;
	}

	public void setSign_sp_id(String signSpId) {
		sign_sp_id = signSpId;
	}

	public CftVb2cAttach getAttachPo() {
		return attachPo;
	}

	public void setAttachPo(CftVb2cAttach attachPo) {
		this.attachPo = attachPo;
	}
	
}
