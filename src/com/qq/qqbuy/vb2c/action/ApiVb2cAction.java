package com.qq.qqbuy.vb2c.action;

import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

import org.apache.commons.lang.StringUtils;

import com.qq.qqbuy.common.action.PaipaiApiBaseAction;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.util.Base64;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.tenpay.biz.impl.PayTokenInfo;
import com.qq.qqbuy.vb2c.biz.Vb2cDealBiz;
import com.qq.qqbuy.vb2c.po.MobileLocationPo;
import com.qq.qqbuy.vb2c.po.Vb2cDealInfo;
import com.qq.qqbuy.vb2c.po.Vb2cDealInfos;
import com.qq.qqbuy.vb2c.util.CftCallBackUrlFactory;

public class ApiVb2cAction extends PaipaiApiBaseAction
{
    /**
     * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)
     */
    private static final long serialVersionUID = 1L;

    private Vb2cDealBiz vb2cDealBiz;

    /**
     * 返回json
     */
    private String jsonStr = "{}";

    // 起始页 从1开始
    private int pn = 1;

    // 每页的大小，默认为50
    private int ps = 10;

    // 获取订单状态，1-等待买家付款 ；2-等待发货； 3-发货中；4-退款中；5-交易成功；6-交易完成，已退款； 7-交易取消
    private int state = 0;
    // 查询的订单类型 1是话费，3是网游
    private String dealType = "";

    private String dealId = "";
    
    // 手机号
    private String mo = "";
    

    public String getMo() {
		return mo;
	}

	public void setMo(String mo) {
		this.mo = mo;
	}

	public String getDealId()
    {
        return dealId;
    }

    public void setDealId(String dealId)
    {
        this.dealId = dealId;
    }

    public Vb2cDealBiz getVb2cDealBiz()
    {
        return vb2cDealBiz;
    }

    public void setVb2cDealBiz(Vb2cDealBiz vb2cDealBiz)
    {
        this.vb2cDealBiz = vb2cDealBiz;
    }

    public String getJsonStr()
    {
        return jsonStr;
    }

    public void setJsonStr(String jsonStr)
    {
        this.jsonStr = jsonStr;
    }

    public int getPn()
    {
        return pn;
    }

    public void setPn(int pn)
    {
        this.pn = pn;
    }

    public int getPs()
    {
        return ps;
    }

    public void setPs(int ps)
    {
        this.ps = ps;
    }

    public int getState()
    {
        return state;
    }

    public void setState(int state)
    {
        this.state = state;
    }

    public String getDealType()
    {
        return dealType;
    }

    public void setDealType(String dealType)
    {
        this.dealType = dealType;
    }

    /**
     * 
     * @Title: getDeals
     * @Description: 查询订单列表
     * @return 设定文件
     * @return String 返回类型
     * @throws
     */
    public String getDeals()
    {
        boolean isNeedReport = true;
        long start = System.currentTimeMillis();
        jsonStr = "{}";
        if (!checkLogin())
        {
            setErrCodeAndMsg(ErrConstant.ERRCODE_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
        } else
        {
            Vb2cDealInfos deals = vb2cDealBiz.getDealList(getQq(), pn, ps,
                    state, getMk(), getSk(), dealType);
            if (deals != null)
            {
                JsonConfig jsoncfg = new JsonConfig();
                String[] excludes =
                { "size", "empty" };
                jsoncfg.setExcludes(excludes);
                jsonStr = JSONSerializer.toJSON(deals, jsoncfg).toString();
            }
            else
            {
                isNeedReport = false;
            }
        }
        long timeCost = System.currentTimeMillis() - start;
        return SUCCESS;
    }

    /**
     * 
     * @Title: getDealInfo
     * @Description: 查询单个订单信息
     * @return 设定文件
     * @return String 返回类型
     * @throws
     */
    public String getDealInfo()
    {

        long start = System.currentTimeMillis();
        jsonStr = "{}";
        long qq = 0;
        if (!checkLogin())
        {
            setErrCodeAndMsg(ErrConstant.ERRCODE_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
        } else
        {
            qq = getQq();
            Vb2cDealInfo dealinfo = vb2cDealBiz.getDealItem(qq, dealId,
                    getMk(), getSk(), qq);
            if (dealinfo != null)
            {
                JsonConfig jsoncfg = new JsonConfig();
                String[] excludes =
                { "size", "empty" };
                jsoncfg.setExcludes(excludes);
                jsonStr = JSONSerializer.toJSON(dealinfo, jsoncfg).toString();
            }
        }
        long timeCost = System.currentTimeMillis() - start;
        return SUCCESS;
    }

    /**
     * 
     * @Title: getDeals
     * @Description: 查询订单列表
     * @return 设定文件
     * @return String 返回类型
     * @throws
     */
    public String getToken()
    {

        long start = System.currentTimeMillis();
        jsonStr = "{}";
        long qq = 0;
        if (!checkLogin())
        {
            setErrCodeAndMsg(ErrConstant.ERRCODE_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
        } else
        {
            qq = getQq();
            PayTokenInfo payInfo = vb2cDealBiz.getToken(qq, dealId, getMk(),
                    getSk());
            if (payInfo != null)
            {
                this.result.setMsg(payInfo.msg);
                if (StringUtils.isNotBlank(payInfo.getToken()))
                {
                    String loginKey = "uin=" + getQq() + "&session_id="
                            + getLk();
                    String h5Url = payInfo.getH5PayUrl() + "&login_key="
                            + Base64.encode(loginKey.getBytes());
                    payInfo.setH5PayUrl(h5Url);
                    payInfo.setCallbackUrl(CftCallBackUrlFactory
                            .getCftCallBackUrl(null));
                    JsonConfig jsoncfg = new JsonConfig();
                    String[] excludes =
                    { "size", "empty" };
                    jsoncfg.setExcludes(excludes);
                    jsonStr = JSONSerializer.toJSON(payInfo, jsoncfg)
                            .toString();
                }
            }
        }
        long timeCost = System.currentTimeMillis() - start;
        return SUCCESS;
    }
    
    public String mobileLocation() {
    	long start = System.currentTimeMillis();
    	jsonStr = "{}";
    	if (StringUtils.isNotBlank(mo) && Util.isMobileNO(mo)) {
			MobileLocationPo mobileLocationPo = vb2cDealBiz.getMobileLocation(mo);
			if (mobileLocationPo != null)
            {
                JsonConfig jsoncfg = new JsonConfig();
                String[] excludes =
                { "size", "empty" };
                jsoncfg.setExcludes(excludes);
                jsonStr = JSONSerializer.toJSON(mobileLocationPo, jsoncfg).toString();
            }
		} else {
			setErrCodeAndMsg(ErrConstant.ERRCODE_INVALID_PARAMETER, "手机号格式不对");
		}
    	long timeCost = System.currentTimeMillis() - start;
        return SUCCESS;
    }
}
