package com.qq.qqbuy.vb2c.po;

/**
 * 用于封装财付通中商户信息的Attach
 * @author winsonwu
 * @Created 2012-5-12
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class CftVb2cAttach {
	
	// Common Attach
	private int versionType;
	private String sid;
	private String pgId;
	private String icfa;
	private String o_icfa;
	private String gcfa;
	
	private int type;
	private long qq;
	private int amount;
	private String mobile;
	private int gameId;
	private int buyCount;
	private String account;
	private String tGameName;
	private String chargeType;
	private String tSectionName;
	private String tServerName;
	private String tSectionCode;
	private String tServerCode;
	
	private String qqServiceDisName; //QQ服务展示名称
	
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public String getPgId() {
		return pgId;
	}
	public void setPgId(String pgId) {
		this.pgId = pgId;
	}
	public String getIcfa() {
		return icfa;
	}
	public void setIcfa(String icfa) {
		this.icfa = icfa;
	}
	public String getO_icfa() {
		return o_icfa;
	}
	public void setO_icfa(String oIcfa) {
		o_icfa = oIcfa;
	}
	public String getGcfa() {
		return gcfa;
	}
	public void setGcfa(String gcfa) {
		this.gcfa = gcfa;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public long getQq() {
		return qq;
	}
	public void setQq(long qq) {
		this.qq = qq;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public int getGameId() {
		return gameId;
	}
	public void setGameId(int gameId) {
		this.gameId = gameId;
	}
	public int getBuyCount() {
		return buyCount;
	}
	public void setBuyCount(int buyCount) {
		this.buyCount = buyCount;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String gettGameName() {
		return tGameName;
	}
	public void settGameName(String tGameName) {
		this.tGameName = tGameName;
	}
	public String getChargeType() {
		return chargeType;
	}
	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}
	public String gettSectionName() {
		return tSectionName;
	}
	public void settSectionName(String tSectionName) {
		this.tSectionName = tSectionName;
	}
	public String gettServerName() {
		return tServerName;
	}
	public void settServerName(String tServerName) {
		this.tServerName = tServerName;
	}
	public String gettSectionCode() {
		return tSectionCode;
	}
	public void settSectionCode(String tSectionCode) {
		this.tSectionCode = tSectionCode;
	}
	public String gettServerCode() {
		return tServerCode;
	}
	public void settServerCode(String tServerCode) {
		this.tServerCode = tServerCode;
	}
	public String getQqServiceDisName() {
		return qqServiceDisName;
	}
	public void setQqServiceDisName(String qqServiceDisName) {
		this.qqServiceDisName = qqServiceDisName;
	}
	public int getVersionType() {
		return versionType;
	}
	public void setVersionType(int versionType) {
		this.versionType = versionType;
	}
	
}
