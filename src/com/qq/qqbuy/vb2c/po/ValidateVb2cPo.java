package com.qq.qqbuy.vb2c.po;

/**
 * @author winsonwu
 * @Created 2012-6-6
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class ValidateVb2cPo {

	private String sid;
	private String mkey;
	private String skey;
	private long uin;
	
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public String getMkey() {
		return mkey;
	}
	public void setMkey(String mkey) {
		this.mkey = mkey;
	}
	public String getSkey() {
		return skey;
	}
	public void setSkey(String skey) {
		this.skey = skey;
	}
	public long getUin() {
		return uin;
	}
	public void setUin(long uin) {
		this.uin = uin;
	}
	
}
