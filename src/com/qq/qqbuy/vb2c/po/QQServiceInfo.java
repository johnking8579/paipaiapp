package com.qq.qqbuy.vb2c.po;

import java.util.ArrayList;
import java.util.List;

/**
 * 用于生成相应的Json格式
 * 
 * @author winsonwu
 * @Created 2011-10-21
 */
public class QQServiceInfo
{
    private String serviceType;
    private int id;
    private String name;
    private String code;
    private int count;
    private String unit;
    private int price;
    private int type;
    private int minCount;
    private int maxCount;
    private int discount = 100;
    private String priceTips;
    private String tips;
    private List<Integer> options = new ArrayList<Integer>();

    public static QQServiceInfo getInstance(QQService qqService)
    {
        QQServiceInfo info = new QQServiceInfo();
        info.setServiceType(qqService.getServiceType());
        info.setId(qqService.getId());
        info.setName(qqService.getName());
        info.setCode(qqService.getCode());
        info.setCount(qqService.getCount());
        info.setUnit(qqService.getUnit());
        info.setPrice(qqService.getPrice());
        info.setType(qqService.getType());
        info.setMinCount(qqService.getMinCount());
        info.setMaxCount(qqService.getMaxCount());
        info.setDiscount(qqService.getDiscount());
        info.setPriceTips(qqService.getPriceTips());
        info.setTips(qqService.getTips());
        info.setOptions(qqService.getOptions());
        return info;
    }

    public String getServiceType()
    {
        return serviceType;
    }

    public void setServiceType(String serviceType)
    {
        this.serviceType = serviceType;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public int getCount()
    {
        return count;
    }

    public void setCount(int count)
    {
        this.count = count;
    }

    public String getUnit()
    {
        return unit;
    }

    public void setUnit(String unit)
    {
        this.unit = unit;
    }

    public int getPrice()
    {
        return price;
    }

    public void setPrice(int price)
    {
        this.price = price;
    }

    public int getType()
    {
        return type;
    }

    public void setType(int type)
    {
        this.type = type;
    }

    public int getMinCount()
    {
        return minCount;
    }

    public void setMinCount(int minCount)
    {
        this.minCount = minCount;
    }

    public int getMaxCount()
    {
        return maxCount;
    }

    public void setMaxCount(int maxCount)
    {
        this.maxCount = maxCount;
    }

    public int getDiscount()
    {
        return discount;
    }

    public void setDiscount(int discount)
    {
        this.discount = discount;
    }

    public String getPriceTips()
    {
        return priceTips;
    }

    public void setPriceTips(String priceTips)
    {
        this.priceTips = priceTips;
    }

    public String getTips()
    {
        return tips;
    }

    public void setTips(String tips)
    {
        this.tips = tips;
    }

    public List<Integer> getOptions()
    {
        return options;
    }

    public void setOptions(List<Integer> options)
    {
        this.options = options;
    }

}
