package com.qq.qqbuy.vb2c.po;

import java.util.ArrayList;
import java.util.List;

import com.qq.qqbuy.common.po.Pair;

/**
 * Wap2.0网游detail部分数据的封装
 * @author winsonwu
 * @Created 2012-5-24
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class WGameDetailOutPo {

	private ValidateGamePo gameInfo;
	private List<Pair> options = new ArrayList<Pair>();
	private int nextIndex;
	private int preIndex;
	private String encodeChargeType;
	private String encodeAccount;
	private String encodeConfirmAccount;
	private String encodewp;
	private String encodewp1;
	private String totalFee;
	
	public ValidateGamePo getGameInfo() {
		return gameInfo;
	}
	public void setGameInfo(ValidateGamePo gameInfo) {
		this.gameInfo = gameInfo;
	}
	public List<Pair> getOptions() {
		return options;
	}
	public void setOptions(List<Pair> options) {
		this.options = options;
	}
	public int getNextIndex() {
		return nextIndex;
	}
	public void setNextIndex(int nextIndex) {
		this.nextIndex = nextIndex;
	}
	public String getEncodeChargeType() {
		return encodeChargeType;
	}
	public void setEncodeChargeType(String encodeChargeType) {
		this.encodeChargeType = encodeChargeType;
	}
	public String getTotalFee() {
		return totalFee;
	}
	public void setTotalFee(String totalFee) {
		this.totalFee = totalFee;
	}
	public String getEncodeAccount() {
		return encodeAccount;
	}
	public void setEncodeAccount(String encodeAccount) {
		this.encodeAccount = encodeAccount;
	}
	public String getEncodeConfirmAccount() {
		return encodeConfirmAccount;
	}
	public void setEncodeConfirmAccount(String encodeConfirmAccount) {
		this.encodeConfirmAccount = encodeConfirmAccount;
	}
	public int getPreIndex() {
		return preIndex;
	}
	public void setPreIndex(int preIndex) {
		this.preIndex = preIndex;
	}
	public String getEncodewp() {
		return encodewp;
	}
	public void setEncodewp(String encodewp) {
		this.encodewp = encodewp;
	}
	public String getEncodewp1() {
		return encodewp1;
	}
	public void setEncodewp1(String encodewp1) {
		this.encodewp1 = encodewp1;
	}
	
}
