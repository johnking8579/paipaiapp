package com.qq.qqbuy.vb2c.po;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.paipai.util.string.StringUtil;

/**
 * 
 * @author winsonwu
 * 
 */
public enum QQService implements Serializable
{

    //winson注：所有人民币的单位全部采用“分” 
    //winson注：增加QQService类型时候，需要修改getQQServiceInfo()的ID值校验规则
    //int id, String name, String code, int count, String unit, int price, int type
    //int minCount, int maxCount, int discount, String priceTips, String tips, String serviceType, 
    QDian                   (0,   "Q点",           "-QQPOINT",  10,           "Q点",  10,     0, 
                             10,  2400,            93,          "",           "财付通用户9.3折，目前仅支持财付通支付，其它支付方式即将开通", "qd"),
    QBi                     (27,  "Q币",           "QQACCT_SAVE",1,           "Q币",  100,    1,
                             1,   240,             93,          "",           "财付通用户9.3折，目前仅支持财付通支付，其它支付方式即将开通", "qb"),
    QQMember                (1,   "QQ会员",        "LTMCLUB",   3,            "月",   1000,   0,
                             1,   24,              93,          "10元/月",    "为您提供等级加速、游戏礼包、电影票等50余项QQ、游戏、生活三大领域特权体验。", "service"),
    QQShowRedDia            (2,   "QQ秀红钻",      "XXQQF",     3,            "月",   1000,   0,
                             1,   24,              93,          "10元/月",    "QQ秀红钻贵族，5万件QQ秀免费穿、免费送！还能免费送鲜花！", "service"),
    QQSpaceYellowDia        (3,   "QQ空间黄钻",    "XXJZGW",    3,            "月",   1000,   0,
                             1,   24,              93,          "10元/月",    "QQ空间装扮、道具、信纸免费，1G海量相册、专享VIP上传、自定义个性域名等强大特权！", "service"),
    QQGameBlueDia           (4,   "QQ游戏蓝钻",    "XXQGAME",   3,            "月",   1000,   0,
                             1,   24,              93,          "10元/月",    "QQ游戏踢人、防踢，还有双倍积分等特权！", "service"),
    QQMusicGreenDia         (5,   "QQ音乐绿钻",    "XXZXYY",    3,            "月",   1000,   0,
                             1,   24,              93,          "10元/月",    "百万空间背景音乐免费用！还可上传本地音乐，观看高清音乐视频！下载高品质mp3！", "service"),   
    QQPetPinkDia            (6,   "QQ宠物粉钻",    "PETVIP",    3,            "月",   1000,   0,
                             1,   24,              93,          "10元/月",    "粉钻贵族在QQ宠物中享超多尊贵特权：粉钻图标、免费用餐清洗、购物打折等！", "service"),        
    SuperQQ                 (7,   "超级QQ",        "XXSQQM",    3,            "月",   1000,   0,
                             1,   24,              93,          "10元/月",    "超级QQ为您提供最高1.8倍QQ等级加速，还有30多项特权助您成为手机VIP会员！", "service"),  
    DNFBlackDia             (8,   "DNF黑钻",       "DNFHZ",     3,            "月",   2000,   0,
                             1,   24,              93,          "20元/月",    "地下城与勇士黑钻贵族，疲劳上限增加、通关经验奖励、通关翻牌奖励和尊贵游戏标识。", "service"), 
    QQSpeedPurpleDia        (9,   "飞车紫钻",      "QQFCZZ",    3,            "月",   1000,   0,
                             1,   24,              93,          "10元/月",    "QQ飞车踢人防踢，经验加成，专属道具与任务，疯狂折扣等特权！", "service"),    
    SearchCelestialVIP      (10,  "寻仙VIP",       "XXVIP",     3,            "月",   2000,   0,
                             1,   24,              93,          "20元/月",    "寻仙VIP会员尊贵标识，自动回红、蓝，8%经验加成，专属场景，独享任务等！", "service"),   
    QQFancyDancePurpleDia   (11,  "QQ炫舞紫钻",    "QQXWZZ",    3,            "月",   2000,   0,
                             1,   24,              93,          "20元/月",    "QQ炫舞踢人防踢功能、对局经验独享加成、紫钻贵族礼包免费送等特权！", "service"), 
    CFMember                (12,  "CF会员",        "CFCLUB",    3,            "月",   3000,   0,
                             1,   24,              93,          "30元/月",    "VIP标识、经验+25%、道具特权、防踢（每局2次）、跨阵营踢人特权、中途退场无惩罚等！", "service"),
    SuperSonicPurpleDia     (13,  "音速紫钻",      "QQR2BY",    3,            "月",    1000,   0,
                             1,   24,              93,          "10元/月",    "QQ音速踢人、防踢，享受音速疯狂折扣等特权！", "service"), 
    QQHallPurpleDia         (14,  "QQ堂紫钻",      "XXQQT",     3,            "月",    1000,   0,
                             1,   24,              93,          "10元/月",    "QQ堂VIP角色和道具独享，还有防踢等特权！", "service"),
    ReadVIP                 (15,  "读书VIP",       "MAGPT",     3,            "月",    1000,   0,
                             1,   24,              93,          "10元/月",    "腾讯读书VIP会员可免费阅读VIP包月书库内所有图书！", "service"),
    QQFantasy               (16,  "QQ幻想世界金子", "-QQHXSJ",   1000,         "点",    100,  1,
                             100, 50000,           93,          "100 金子/1元","", "service"),        
    DNFCoupon               (17,  "DNF点券",       "-DNFDQ",    500,          "点",    100,   1, 
                             100, 50000,           93,          "100 DNF点/1元", "", "service"),
    QQFancyDanceCoupon      (18,  "QQ炫舞点券",    "-XWDQ",     500,           "点",    100,  1,
                             100, 50000,           93,          "100 点券/1元", "", "service"),
    QQSpeedCoupon           (19,  "QQ飞车点券",    "QQKDC",     1000,          "点",    100,  1,
                             100, 50000,           93,          "100 点券/1元", "", "service"),  
    SearchCelestialJade     (20,  "寻仙仙玉",      "-XXXY",     500,           "点",    100,  1,
                             100, 50000,           93,          "100 仙玉/1元", "", "service"),
    DaMingDragonRight       (21,  "大明龙权",      "DMLQ",      1000,           "点",   100,  1,
                             100, 50000,           93,          "100 点券/1元", "", "service"),        
    FlameWarringStates      (22,  "烽火战国",      "-FHZG",     50,             "点",   10,   1,
                             10,  50000,           93,          "10 点券/1元", "", "service"),
    FreeFantasy             (23,  "自由幻想彩玉",   "-FFOWY",   1000,           "点",    100,  1,
                             100, 50000,           93,          "100 彩玉点/1元", "", "service"),        
    SlikRoadHero            (24,  "丝路英雄",      "-SLYX",      100,           "点",    10,   1,
                             10,  10000,           93,          "10 丝路币/1元", "", "service"),
    CrossFire               (25,  "穿越火线",      "-CFDQ",     1000,           "点",    100,  1,
                             100, 50000,           93,          "100 CF点/1元", "", "service"),        
    KingOfBattleFieldAVA    (26,  "战地之王ava点", "-AVAD",      1000,          "点",    100,  1,
                             100, 50000,           93,          "100 AVA点/1元", "", "service");   

    private static final long serialVersionUID = 1093313596339308226L;

    public static final String SERVICE_TYPE_QB = "qb";
    public static final String SERVICE_TYPE_QD = "qd";
    public static final String SERVICE_TYPE_SERVICE = "service";

    public static final boolean checkServiceType(String serviceType)
    {
        if (!StringUtil.isEmpty(serviceType)
                && (SERVICE_TYPE_QB.equalsIgnoreCase(serviceType)
                        || SERVICE_TYPE_QD.equalsIgnoreCase(serviceType) || SERVICE_TYPE_SERVICE
                        .equalsIgnoreCase(serviceType)))
        {
            return true;
        }
        return false;
    }
    
    private static final QQService[] qqServices = new QQService[]
    { QDian, QBi, QQMember, QQShowRedDia, QQSpaceYellowDia, 
      QQGameBlueDia, QQMusicGreenDia, QQPetPinkDia, SuperQQ, DNFBlackDia,
      QQSpeedPurpleDia, SearchCelestialVIP, QQFancyDancePurpleDia, CFMember, SuperSonicPurpleDia, 
      QQHallPurpleDia, ReadVIP, QQFantasy, DNFCoupon, QQFancyDanceCoupon, 
      QQSpeedCoupon, SearchCelestialJade, DaMingDragonRight, FlameWarringStates, FreeFantasy, 
      SlikRoadHero, CrossFire, KingOfBattleFieldAVA 
    };

    /**
     * 服务类型
     */
    private String serviceType;

    /**
     * 编码
     */
    private int id;

    /**
     * 名称
     */
    private String name;

    /**
     * 财付通编码
     */
    private String code;

    /**
     * 默认值
     */
    private int count;

    /**
     * 单位
     */
    private String unit;

    /**
     * 单价,以分为单位
     */
    private int price;

    /**
     * 区分是游戏还是服务，0是服务，1是游戏
     */
    private int type;

    /**
     * 最少可充多少
     */
    private int minCount;

    /**
     * 最多可充多少
     */
    private int maxCount;

    /**
     * 折扣，0.9折扣为90，其余类推
     */
    private int discount = 100;

    /**
     * 充值单价提示
     */
    private String priceTips;

    /**
     * 页面提示语
     */
    private String tips;

    /**
     * 不同的QQService购买时候的不同选项
     */
    private List<Integer> options = new ArrayList<Integer>();

    private QQService(int id, String name, String code, int count, String unit,
            int price, int type, int minCount, int maxCount, int discount,
            String priceTips, String tips, String serviceType)
    {
        this.id = id;
        this.name = name;
        this.code = code;
        this.count = count;
        this.unit = unit;
        this.price = price;
        this.type = type;
        this.minCount = minCount;
        this.maxCount = maxCount;
        this.discount = discount;
        this.priceTips = priceTips;
        this.tips = tips;
        this.serviceType = serviceType;
        // 不同的ServiceType有不同的Options
        if ("qd".equals(serviceType))
        { // Q点
            options.add(10); // Q点：10、50、100、150、200、300、600
            options.add(50);
            options.add(100);
            options.add(150);
            options.add(200);
            options.add(300);
            options.add(600);
        } else if ("qb".equals(serviceType))
        { // Q币
            options.add(1); // Q币：1、5、10、15、20、30、60
            options.add(5);
            options.add(10);
            options.add(15);
            options.add(20);
            options.add(30);
            options.add(60);
        } else
        { // Service
            options.add(1);
            options.add(3);
            options.add(6);
            options.add(9);
            options.add(12);
        }
    }

    public static QQService[] getQQService()
    {
        return qqServices;
    }

    public static QQService getQQServiceInfo(int qqServiceId)
    {
        if (qqServiceId < 0 || qqServiceId > 27)
        {
            return null;
        }
        for (QQService qqService : getQQService())
        {
            if (qqService != null && (qqServiceId == qqService.getId()))
            {
                return qqService;
            }
        }
        return null;
    }

    /************************************************************* Getter And Setter Method *************************************************************/
    public String getServiceType()
    {
        return serviceType;
    }

    public void setServiceType(String serviceType)
    {
        this.serviceType = serviceType;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public int getCount()
    {
        return count;
    }

    public void setCount(int count)
    {
        this.count = count;
    }

    public String getUnit()
    {
        return unit;
    }

    public void setUnit(String unit)
    {
        this.unit = unit;
    }

    public int getPrice()
    {
        return price;
    }

    public void setPrice(int price)
    {
        this.price = price;
    }

    public int getType()
    {
        return type;
    }

    public void setType(int type)
    {
        this.type = type;
    }

    public int getMinCount()
    {
        return minCount;
    }

    public void setMinCount(int minCount)
    {
        this.minCount = minCount;
    }

    public int getMaxCount()
    {
        return maxCount;
    }

    public void setMaxCount(int maxCount)
    {
        this.maxCount = maxCount;
    }

    public int getDiscount()
    {
        return discount;
    }

    public void setDiscount(int discount)
    {
        this.discount = discount;
    }

    public String getPriceTips()
    {
        return priceTips;
    }

    public void setPriceTips(String priceTips)
    {
        this.priceTips = priceTips;
    }

    public String getTips()
    {
        return tips;
    }

    public void setTips(String tips)
    {
        this.tips = tips;
    }

    public List<Integer> getOptions()
    {
        return options;
    }

    public void setOptions(List<Integer> options)
    {
        this.options = options;
    }

}
