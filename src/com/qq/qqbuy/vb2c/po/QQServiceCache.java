package com.qq.qqbuy.vb2c.po;

import java.util.ArrayList;
import java.util.List;

import com.qq.qqbuy.common.util.JSONUtil;

/**
 * 用于缓存QQ服务列表
 * 
 * @author winsonwu
 * @Created 2011-10-21
 */
public class QQServiceCache
{
    private static String qqservicesJsonStr = null;

    public static String getQQServicesJsonStr()
    {
        if (qqservicesJsonStr == null)
        {
            synchronized (QQServiceCache.class)
            {
                if (qqservicesJsonStr == null)
                {
                    // 获取回来特定顺序的QQ服务列表
                    // winson: 27=Q币, 0=Q点, 1=QQ会员, 7=超级QQ, 3=QQ空间黄钻, 2=QQ秀红钻,
                    // 5=QQ音乐绿钻,
                    // 4=QQ游戏蓝钻, 6=QQ宠物粉钻, 8=DNF黑钻, 9=飞车紫钻,
                    // 12=CF会员, 13=音速紫钻, 14=QQ堂紫钻
                    int[] ids = new int[]
                    { 27, 0, 1, 7, 3, 2, 5, 4, 6, 8, 9, 12, 13, 14 };
                    List<QQServiceInfo> qqServiceList = new ArrayList<QQServiceInfo>();
                    for (int i = 0; i < ids.length; i++)
                    {
                        QQService qqService = QQService
                                .getQQServiceInfo(ids[i]);
                        if (qqService != null)
                        {
                            qqServiceList.add(QQServiceInfo
                                    .getInstance(qqService));
                        }
                    }
                    qqservicesJsonStr = JSONUtil.getJsonStr(qqServiceList);
                }
            }
        }
        return qqservicesJsonStr;
    }

}
