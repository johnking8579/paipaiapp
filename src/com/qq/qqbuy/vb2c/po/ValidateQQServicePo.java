package com.qq.qqbuy.vb2c.po;

/**
 * 封装检查QQ服务参数的检查结果
 * @author winsonwu
 * @Created 2012-5-23
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class ValidateQQServicePo extends ValidateVb2cPo {
	
	private int gameId;
	private int amount;
	private int buyCount;
	
	private QQService4Chong qqService;
	private String itemId;
	
	public int getGameId() {
		return gameId;
	}
	public void setGameId(int gameId) {
		this.gameId = gameId;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getBuyCount() {
		return buyCount;
	}
	public void setBuyCount(int buyCount) {
		this.buyCount = buyCount;
	}
	public QQService4Chong getQqService() {
		return qqService;
	}
	public void setQqService(QQService4Chong qqService) {
		this.qqService = qqService;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
}
