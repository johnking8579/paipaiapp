package com.qq.qqbuy.vb2c.po;

/**
 * 用于封装Mobile的请求参数
 * @author winsonwu
 * @Created 2012-7-18
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class MobileParam {

	private String m; //mobile,充值的手机号码
	private int am; //amount, 充值的面额，"以元为单位" 
	private String s; //充值来源
	private int from; // 页面点击来源：1=通过点击“立即充值”跳转来的。
	
	public String getM() {
		return m;
	}
	public void setM(String m) {
		this.m = m;
	}
	public int getAm() {
		return am;
	}
	public void setAm(int am) {
		this.am = am;
	}
	public String getS() {
		return s;
	}
	public void setS(String s) {
		this.s = s;
	}
	public int getFrom() {
		return from;
	}
	public void setFrom(int from) {
		this.from = from;
	}
	
	public static MobileParam createInstance(String m, int am, String s, int from) {
		MobileParam instance = new MobileParam();
		instance.setM(m);
		instance.setAm(am);
		instance.setS(s);
		instance.setFrom(from);
		
		return instance;
	}
	
}
