package com.qq.qqbuy.vb2c.po;

/**
 * @author winsonwu
 * @Created 2012-12-7 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class Vb2cDealInfo
{

    // 订单ID
    private String dealId = new String();

    // 订单类型，0-手机；1-网游；3-农场道具
    private long dealType;

    // 商品名称
    private String itemName = new String();

    // 订单金额
    private long payFee;

    // 购买数量
    private long num;

    // 下单时间
    private long dealGenTime;

    // 订单状态，1-等待买家付款 ；2-等待发货； 3-发货中；4-退款中；5-交易成功；6-交易完成，已退款； 7-交易取消
    private long dealState;

    // 订单状态描述
    private String dealStateDesc = new String();

    // 供应商名称
    private String sellerName = new String();

    // 供应商SPID
    private long sellerSpid;

    // 客服电话
    private String svrPhone = new String();

    // 账户信息
    private String account = "";

    // 充值金额
    private long fee = 0;

    public long getFee()
    {
        return fee;
    }

    public void setFee(long fee)
    {
        this.fee = fee;
    }

    public String getAccount()
    {
        return account;
    }

    public void setAccount(String account)
    {
        this.account = account;
    }

    public String getDealId()
    {
        return dealId;
    }

    public void setDealId(String dealId)
    {
        this.dealId = dealId;
    }

    public long getDealType()
    {
        return dealType;
    }

    public void setDealType(long dealType)
    {
        this.dealType = dealType;
    }

    public String getItemName()
    {
        return itemName;
    }

    public void setItemName(String itemName)
    {
        this.itemName = itemName;
    }

    public long getPayFee()
    {
        return payFee;
    }

    public void setPayFee(long payFee)
    {
        this.payFee = payFee;
    }

    public long getNum()
    {
        return num;
    }

    public void setNum(long num)
    {
        this.num = num;
    }

    public long getDealGenTime()
    {
        return dealGenTime;
    }

    public void setDealGenTime(long dealGenTime)
    {
        this.dealGenTime = dealGenTime;
    }

    public long getDealState()
    {
        return dealState;
    }

    public void setDealState(long dealState)
    {
        this.dealState = dealState;
    }

    public String getDealStateDesc()
    {
        return dealStateDesc;
    }

    public void setDealStateDesc(String dealStateDesc)
    {
        this.dealStateDesc = dealStateDesc;
    }

    public String getSellerName()
    {
        return sellerName;
    }

    public void setSellerName(String sellerName)
    {
        this.sellerName = sellerName;
    }

    public long getSellerSpid()
    {
        return sellerSpid;
    }

    public void setSellerSpid(long sellerSpid)
    {
        this.sellerSpid = sellerSpid;
    }

    public String getSvrPhone()
    {
        return svrPhone;
    }

    public void setSvrPhone(String svrPhone)
    {
        this.svrPhone = svrPhone;
    }

}
