package com.qq.qqbuy.vb2c.po;

/**
 * 封装QQ服务下单后的输出数据
 * @author winsonwu
 * @Created 2012-5-18
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class QQServiceOrderDealOutPo extends Vb2cOrderDealOutPo {

	private ApiQQServiceInfo qqServiceInfo;
	
	public ApiQQServiceInfo getQqServiceInfo() {
		return qqServiceInfo;
	}
	public void setQqServiceInfo(ApiQQServiceInfo qqServiceInfo) {
		this.qqServiceInfo = qqServiceInfo;
	}
	@Override
	public String toString() {
		return "QQServiceOrderDealOutPo [qqServiceInfo=" + qqServiceInfo + super.toString() + "]";
	}
	
}
