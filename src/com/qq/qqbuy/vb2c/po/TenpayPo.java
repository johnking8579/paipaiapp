package com.qq.qqbuy.vb2c.po;

/**
 * @author winsonwu
 * @Created 2012-5-18
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class TenpayPo {
	
	private String tokenId;
	
	private String tenpayUrl;
	
	public TenpayPo(String tokenId, String tenpayUrl) {
		this.tokenId = tokenId;
		this.tenpayUrl = tenpayUrl;
	}

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	public String getTenpayUrl() {
		return tenpayUrl;
	}

	public void setTenpayUrl(String tenpayUrl) {
		this.tenpayUrl = tenpayUrl;
	}
	
}
