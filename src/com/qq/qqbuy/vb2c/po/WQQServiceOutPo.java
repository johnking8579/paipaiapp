package com.qq.qqbuy.vb2c.po;

/**
 * 封装QQ服务中的数据
 * @author winsonwu
 * @Created 2012-5-10
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class WQQServiceOutPo {
	
	private String qqServiceDisName;
	
	private ApiQQServiceInfo currServiceInfo;
	
	private boolean isQbi = true;  
	
	private String pricePerMonth;

	public boolean isQbi() {
		return isQbi;
	}

	public void setQbi(boolean isQbi) {
		this.isQbi = isQbi;
	}

	public String getPricePerMonth() {
		return pricePerMonth;
	}

	public void setPricePerMonth(String pricePerMonth) {
		this.pricePerMonth = pricePerMonth;
	}

	public String getQqServiceDisName() {
		return qqServiceDisName;
	}

	public void setQqServiceDisName(String qqServiceDisName) {
		this.qqServiceDisName = qqServiceDisName;
	}

	public ApiQQServiceInfo getCurrServiceInfo() {
		return currServiceInfo;
	}

	public void setCurrServiceInfo(ApiQQServiceInfo currServiceInfo) {
		this.currServiceInfo = currServiceInfo;
	}
	
}
