package com.qq.qqbuy.vb2c.po.game;

/**
 * @author winsonwu
 * @Created 2012-6-6
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class Section implements java.io.Serializable
{

    private static final long serialVersionUID = -2162303242391398573L;

    /**
     * 服名称
     */
    public String name;

    /**
     * 服编码
     */
    public String code;

    /**
     * @param name
     * @param code
     */
    public Section(String name, String code)
    {
        super();
        this.name = name;
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return "Section [name=" + name + ", code=" + code + "]";
    }
}
