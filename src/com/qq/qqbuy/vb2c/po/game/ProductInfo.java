package com.qq.qqbuy.vb2c.po.game;

import java.util.ArrayList;
import java.util.List;

/**
 * @author winsonwu
 * @Created 2012-6-6
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class ProductInfo implements java.io.Serializable
{

    private static final long serialVersionUID = -2818869159020623105L;

    /**
     * 产品ID
     */
    public String productId;

    /**
     * 产品名称
     */
    public String productName;

    /**
     * 产品图片
     */
    public String productPic;

    /**
     * 充值类型
     */
    public String chargeType;

    /**
     * 充值速度（单位：分钟）
     */
    public String chargeAce;

    /**
     * 可充值数量,格式为（1,2,4,8,9）
     */
    public List<Integer> chargeNum = new ArrayList<Integer>();

    public String getProductId()
    {
        return productId;
    }

    public void setProductId(String productId)
    {
        this.productId = productId;
    }

    public String getProductName()
    {
        return productName;
    }

    public void setProductName(String productName)
    {
        this.productName = productName;
    }

    public String getProductPic()
    {
        return productPic;
    }

    public void setProductPic(String productPic)
    {
        this.productPic = productPic;
    }

    public String getChargeType()
    {
        return chargeType;
    }

    public void setChargeType(String chargeType)
    {
        this.chargeType = chargeType;
    }

    public String getChargeAce()
    {
        return chargeAce;
    }

    public void setChargeAce(String chargeAce)
    {
        this.chargeAce = chargeAce;
    }

    public List<Integer> getChargeNum()
    {
        return chargeNum;
    }

    public void setChargeNum(List<Integer> chargeNum)
    {
        this.chargeNum = chargeNum;
    }

    @Override
    public String toString()
    {
        return "ProductInfo [productId=" + productId + ", productName="
                + productName + ", productPic=" + productPic
                + ", chargeType=" + chargeType + ", chargeAce=" + chargeAce
                + ", chargeNum=" + chargeNum + "]";
    }

}
