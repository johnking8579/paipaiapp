package com.qq.qqbuy.vb2c.po.game;

/**
 * @author winsonwu
 * @Created 2012-6-6
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class Product implements java.io.Serializable
{

    private static final long serialVersionUID = 3730500917345171611L;

    /**
     * 面值（单位：元）
     */
    public int amount;

    public ProductInfo productInfo;

    public int getAmount()
    {
        return amount;
    }

    public void setAmount(int amount)
    {
        this.amount = amount;
    }

    public ProductInfo getProductInfo()
    {
        return productInfo;
    }

    public void setProductInfo(ProductInfo productInfo)
    {
        this.productInfo = productInfo;
    }

    @Override
    public String toString()
    {
        return "Product [amount=" + amount + ", info=" + productInfo + "]";
    }

}
