package com.qq.qqbuy.vb2c.po.game;

import java.util.ArrayList;
import java.util.List;

/**
 * @author winsonwu
 * @Created 2012-6-6
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class SectionServer implements java.io.Serializable
{

    private static final long serialVersionUID = -2422448773892258825L;

    public Section section;

    public List<Server> serverList = new ArrayList<Server>();

    public Section getSection()
    {
        return section;
    }

    public void setSection(Section section)
    {
        this.section = section;
    }

    public List<Server> getServerList()
    {
        return serverList;
    }

    public void setServerList(List<Server> serverList)
    {
        this.serverList = serverList;
    }

    @Override
    public String toString()
    {
        return "SectionServer [section=" + section + ", serverList="
                + serverList + "]";
    }

}
