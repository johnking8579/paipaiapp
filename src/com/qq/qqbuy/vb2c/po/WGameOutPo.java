package com.qq.qqbuy.vb2c.po;

import java.util.List;

import com.qq.qqbuy.common.po.Pair;
import com.qq.qqbuy.vb2c.po.game.Product;
import com.qq.qqbuy.vb2c.po.game.SectionServer;
import com.qq.qqbuy.vb2c.po.game.Server;

/**
 * 封装网游中数据
 * @author winsonwu
 * @Created 2012-4-25
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class WGameOutPo {
	
	private OnlineGame currGame;
	
	private List<Product> products;
	
	private List<SectionServer> ss;
	
	private List<Server> currServerList;
	
	private List<Pair> options;
	
	private String sectionName;
	
	private String serverName;
	
	private String itemPrice;

	public OnlineGame getCurrGame() {
		return currGame;
	}

	public void setCurrGame(OnlineGame currGame) {
		this.currGame = currGame;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public List<SectionServer> getSs() {
		return ss;
	}

	public void setSs(List<SectionServer> ss) {
		this.ss = ss;
	}

	public List<Pair> getOptions() {
		return options;
	}

	public void setOptions(List<Pair> options) {
		this.options = options;
	}

	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(String itemPrice) {
		this.itemPrice = itemPrice;
	}

	public List<Server> getCurrServerList() {
		return currServerList;
	}

	public void setCurrServerList(List<Server> currServerList) {
		this.currServerList = currServerList;
	}
	
}
