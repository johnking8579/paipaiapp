package com.qq.qqbuy.vb2c.po;

import java.util.ArrayList;
import java.util.List;

import com.qq.qqbuy.thirdparty.idl.vb2c.Vb2cClient;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetGameItemResp;

//import com.qq.qqbuy.thirdparty.openapi.OpenApi;
//import com.qq.qqbuy.thirdparty.openapi.protocol.GetGameItemResponse;

/**
 * 
 * @author winsonwu
 * @Created 2012-5-18
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class ApiQQServiceInfo {
	
	private static final int QBI_DEFAULT_BUYCOUNT = 1;  //Q币购买只选择面额，不选择数量，默认数量为1

	private int gid;  //QQ服务对应的网游ID
	private String name;  //QQ服务展示的名称
	private String uint;  //QQ服务购买的单位
	private int minCount; //QQ服务购买的最小数量
	private int maxCount; //QQ服务购买的最大数量
	private String tips;  //QQ服务购买的Tips
	private List<Option> options = new ArrayList<Option>();
	
	// 不同的购买数量对应不同的ProductId，主要是Q币对应不同的，其他都是对应相同的ProductID。
	public static class Option {
		private int amount;
		private int count;
		private int price;
		
		public Option(int amount, int count) {
			this.amount = amount;
			this.count = count;
		}
		
		public Option(int amount, int count, int price) {
			this(amount, count);
			this.price = price;
		}
		
		public int getCount() {
			return count;
		}
		public void setCount(int count) {
			this.count = count;
		}

		public int getAmount() {
			return amount;
		}

		public void setAmount(int amount) {
			this.amount = amount;
		}

		public int getPrice() {
			return price;
		}

		public void setPrice(int price) {
			this.price = price;
		}
		
	}

	public int getGid() {
		return gid;
	}

	public void setGid(int gid) {
		this.gid = gid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUint() {
		return uint;
	}

	public void setUint(String uint) {
		this.uint = uint;
	}

	public int getMinCount() {
		return minCount;
	}

	public void setMinCount(int minCount) {
		this.minCount = minCount;
	}

	public int getMaxCount() {
		return maxCount;
	}

	public void setMaxCount(int maxCount) {
		this.maxCount = maxCount;
	}

	public String getTips() {
		return tips;
	}

	public void setTips(String tips) {
		this.tips = tips;
	}

	public List<Option> getOptions() {
		return options;
	}

	public void setOptions(List<Option> options) {
		this.options = options;
	}

	public static ApiQQServiceInfo createInstance(int gameId) {
		QQService4Chong qqService = QQService4Chong.findInPrimary(gameId);
		return createInstance(qqService);
	}
	
	
	public static ApiQQServiceInfo createInstance(QQService4Chong qqService) {
		ApiQQServiceInfo po = new ApiQQServiceInfo();
		if (qqService != null) {
			po.setGid(qqService.getGameId());
			po.setMaxCount(qqService.getMaxBuyCount());
			po.setMinCount(qqService.getMinBuyCount());
			po.setName(qqService.getName());
			po.setUint(qqService.getChargeType());
			po.setTips(qqService.getTips());
			boolean isQbi = QQService4Chong.isQbi(qqService.getGameId());
			if (isQbi) {
				QQService4Chong[] secondaryQbiServices = QQService4Chong.queryQQServicesByType(QQService4Chong.SERVICE_TYPE_SECONDARY_QBI);
				for (QQService4Chong qbi : secondaryQbiServices) {
					Option qbiOption = new Option(qbi.getAmount(), QBI_DEFAULT_BUYCOUNT);
					GetGameItemResp gameItemResp = Vb2cClient.queryGameItemInfo(qbi.getProductId());
			        if (gameItemResp != null && gameItemResp.getResult() == 0 && gameItemResp.getItemInfo() != null) {
			        	qbiOption.setPrice((int)gameItemResp.getItemInfo().getItemPrice());
			        }
//					GetGameItemResponse itemResp = OpenApi.getProxy().queryOnlineGameItemInfo(qbi.getProductId());
//			        if (itemResp != null && itemResp.isSucceed()) {
//			        	qbiOption.setPrice(itemResp.itemPrice);
//			        }
					po.getOptions().add(qbiOption);
				}	
			} else {
				GetGameItemResp gameItemResp = Vb2cClient.queryGameItemInfo(qqService.getProductId());
				int price = 0;
		        if (gameItemResp != null && gameItemResp.getResult() == 0 && gameItemResp.getItemInfo() != null) {
		        	price = (int)gameItemResp.getItemInfo().getItemPrice();
		        }
//				GetGameItemResponse itemResp = OpenApi.getProxy().queryOnlineGameItemInfo(qqService.getProductId());
//				int price = 0;
//		        if (itemResp != null && itemResp.isSucceed()) {
//		        	price = itemResp.itemPrice;
//		        }
				po.getOptions().add(new Option(qqService.getAmount(), 1, price));
				po.getOptions().add(new Option(qqService.getAmount(), 3, price));
				po.getOptions().add(new Option(qqService.getAmount(), 6, price));
				po.getOptions().add(new Option(qqService.getAmount(), 9, price));
				po.getOptions().add(new Option(qqService.getAmount(), 12, price));
			}
		}
		return po;
	}
	
	public static List<ApiQQServiceInfo> generateQQServiceOutPos() {
		List<ApiQQServiceInfo> pos = new ArrayList<ApiQQServiceInfo>();
		QQService4Chong[] primaryQQServices = QQService4Chong.queryQQServicesByType(QQService4Chong.SERVICE_TYPE_PRIMARY);
		for (QQService4Chong qqService : primaryQQServices) {
			ApiQQServiceInfo po = createInstance(qqService);
			pos.add(po);
		}
		return pos;
	}

	@Override
	public String toString() {
		return "ApiQQServiceInfo [gid=" + gid + ", maxCount=" + maxCount
				+ ", minCount=" + minCount + ", name=" + name + ", options="
				+ options + ", tips=" + tips + ", uint=" + uint + "]";
	}
	
}
