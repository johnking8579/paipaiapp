package com.qq.qqbuy.vb2c.po;


/**
 * 通过充值中心的QQ服务列表，面额固定
 * @author winsonwu
 * @Created 2012-5-9
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public enum QQService4Chong {
	QBi(1, "Q币", 2623, 1, "Q币", "C269EC1E0000000000000000000F730A", 1, 10, "财付通用户9.3折，目前仅支持财付通支付，其它支付方式即将开通"),  
	QBi10(2, "Q币", 2623, 10, "Q币", "C269EC1E0000000000000000000F730B", 1, 10, "财付通用户9.3折，目前仅支持财付通支付，其它支付方式即将开通"),
	QBi30(2, "Q币", 2623, 30, "Q币", "C269EC1E0000000000000000000F730C", 1, 10, "财付通用户9.3折，目前仅支持财付通支付，其它支付方式即将开通"),
	QBi50(2, "Q币", 2623, 50, "Q币", "C269EC1E0000000000000000000F730D", 1, 10, "财付通用户9.3折，目前仅支持财付通支付，其它支付方式即将开通"),
	QBi100(2, "Q币", 2623, 100, "Q币", "C269EC1E0000000000000000000F730E", 1, 10, "财付通用户9.3折，目前仅支持财付通支付，其它支付方式即将开通"),
	QQMember(1, "会员", 2609, 10, "包月", "C269EC1E0000000000000000000F4D80", 1, 24, "为您提供等级加速、游戏礼包、电影票等50余项QQ、游戏、生活三大领域特权体验。"),
	QQYellowDia(1, "黄钻", 2608, 10, "包月", "C269EC1E0000000000000000000F4D7F", 1, 24, "QQ空间装扮、道具、信纸免费，1G海量相册、专享VIP上传、自定义个性域名等强大特权！"),
	QQRedDia(1, "红钻", 2605, 10, "包月", "C269EC1E0000000000000000000F4D72", 1, 24, "QQ秀红钻贵族，5万件QQ秀免费穿、免费送！还能免费送鲜花！"),
	QQGreenDia(1, "绿钻", 2611, 10, "包月", "C269EC1E0000000000000000000F4D82", 1, 24, "百万空间背景音乐免费用！还可上传本地音乐，观看高清音乐视频！下载高品质mp3！"),
	QQBlueDia(1, "蓝钻", 2610, 10, "包月", "C269EC1E0000000000000000000F4D81", 1, 24, "QQ游戏踢人、防踢，还有双倍积分等特权！"),
	QQPinkDia(1, "粉钻", 2603, 10, "包月", "C269EC1E0000000000000000000F4D70", 1, 24, "粉钻贵族在QQ宠物中享超多尊贵特权：粉钻图标、免费用餐清洗、购物打折等！"),
	QQSuper(1, "超级QQ", 1302, 10, "包月", "C269EC1E0000000000000000000F4DBB", 1, 24, "超级QQ为您提供最高1.8倍QQ等级加速，还有30多项特权助您成为手机VIP会员！"),
	SpeedPurpleDia(1, "飞车紫钻", 2602, 10, "包月", "C269EC1E0000000000000000000F4D6F", 1, 24, "QQ飞车踢人防踢，经验加成，专属道具与任务，疯狂折扣等特权！"),       //QQ飞车紫钻
	CFMember(1, "CF会员", 1311, 30, "包月", "C269EC1E0000000000000000000F4DF1", 1, 24, "VIP标识、经验+25%、道具特权、防踢（每局2次）、跨阵营踢人特权、中途退场无惩罚等！"),               //穿越火线会员
	QQHallPurpleDia(1, "QQ堂紫钻", 2614, 10, "包月", "C269EC1E0000000000000000000F4D8F", 1, 24, "QQ堂VIP角色和道具独享，还有防踢等特权！"),
	AVA(1, "AVA精英", 3401, 30, "包月", "C269EC1E0000000000000000000F51E7", 1, 24, "专属特权显示,AVA图标,防踢,特殊踢人,额外特权加成!"),                   //战地之王会员
	QQFancyDanceDia(1, "QQ炫舞紫钻", 2618, 20, "包月", "C269EC1E0000000000000000000F4D97", 1, 24, "开通即可拥有至尊紫钻标识,踢人/防踢,紫色彩笔,更多经验加成!"),
	DNFBlackDia(1, "QQ黑钻", 2604, 20, "包月", "C269EC1E0000000000000000000F4D71", 1, 24, "地下城与勇士黑钻贵族，疲劳上限增加、通关经验奖励、通关翻牌奖励和尊贵游戏标识。"),
	SuperSonicPurpleDia(1, "QQ音速紫钻", 2620, 10, "包月", "C269EC1E0000000000000000000F4D9E", 1, 24, "QQ音速踢人、防踢，享受音速疯狂折扣等特权！"),
	SearchCelestialVIP(1, "寻仙VIP", 3222, 20, "包月", "C269EC1E0000000000000000000F51B3", 1, 24, "寻仙VIP会员尊贵标识，自动回红、蓝，8%经验加成，专属场景，独享任务等！"),
	LockeKingDomVIP(1, "洛克王国VIP", 2110, 10, "包月", "C269EC1E0000000000000000000F4FA8", 1, 24, "专属咕噜球,战斗经验加成,每周可领取专属大礼包,还有专属魔法!");   
	
	private static final QQService4Chong[] primaryQQServices = new QQService4Chong[] {QBi, QQMember, QQYellowDia, QQRedDia, QQGreenDia, QQBlueDia,
		QQPinkDia, QQSuper, SpeedPurpleDia, CFMember, QQHallPurpleDia, AVA, QQFancyDanceDia, DNFBlackDia, SuperSonicPurpleDia, SearchCelestialVIP,
		LockeKingDomVIP};
	
	private static final QQService4Chong[] secondaryQbiServices = new QQService4Chong[] {QBi, QBi10, QBi30, QBi50, QBi100};
	
	private static final QQService4Chong[] allQbiServices = new QQService4Chong[] {QBi, QBi10, QBi30, QBi50, QBi100 , QQMember, QQYellowDia, QQRedDia, QQGreenDia, QQBlueDia,
		QQPinkDia, QQSuper, SpeedPurpleDia, CFMember, QQHallPurpleDia, AVA, QQFancyDanceDia, DNFBlackDia, SuperSonicPurpleDia, SearchCelestialVIP,
		LockeKingDomVIP};
	
	public static QQService4Chong[] getPrimaryqqservices() {
		return primaryQQServices;
	}

	public static QQService4Chong[] getSecondaryqbiservices() {
		return secondaryQbiServices;
	}

	public static QQService4Chong[] getAllqbiservices() {
		return allQbiServices;
	}

	public static int getServiceTypePrimary() {
		return SERVICE_TYPE_PRIMARY;
	}

	public static int getServiceTypeSecondaryQbi() {
		return SERVICE_TYPE_SECONDARY_QBI;
	}

	public static QQService4Chong[] getAllqqservices() {
		return allQqServices;
	}

	public static final int SERVICE_TYPE_PRIMARY = 1;
	public static final int SERVICE_TYPE_SECONDARY_QBI = 2;
	public static final int SERVICE_TYPE_ALL = 3;
	
	private static final QQService4Chong[] allQqServices = new QQService4Chong[] {QBi, QBi10, QBi30, QBi50, QBi100, QQMember, QQYellowDia, QQRedDia, 
		QQGreenDia, QQBlueDia, QQPinkDia, QQSuper, SpeedPurpleDia, CFMember, QQHallPurpleDia, AVA, QQFancyDanceDia, DNFBlackDia, SuperSonicPurpleDia, 
		SearchCelestialVIP, LockeKingDomVIP};
	
	private int type;  //type=1代表主要的QQ服务; type=2代表辅助的QQ服务
	private String name;
	private int gameId;
	private int amount;  //以元为单位
	private String chargeType;
	private String productId;
	private int minBuyCount;
	private int maxBuyCount;
	private String tips;
	
	private QQService4Chong(int type, String name, int gameId, int amount, String chargeType, String productId, int minBuyCount, int maxBuyCount, String tips) {
		this.type = type;
		this.name = name;
		this.gameId = gameId;
		this.amount = amount;
		this.chargeType = chargeType;
		this.productId = productId;
		this.minBuyCount = minBuyCount;
		this.maxBuyCount = maxBuyCount;
		this.tips = tips;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}
	
	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}
	
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public int getMinBuyCount() {
		return minBuyCount;
	}

	public void setMinBuyCount(int minBuyCount) {
		this.minBuyCount = minBuyCount;
	}

	public int getMaxBuyCount() {
		return maxBuyCount;
	}

	public void setMaxBuyCount(int maxBuyCount) {
		this.maxBuyCount = maxBuyCount;
	}
	
	public String getTips() {
		return tips;
	}

	public void setTips(String tips) {
		this.tips = tips;
	}
	
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public static boolean isQbi(int gameId) {
		return gameId == QBi.getGameId();
	}
	
	public static boolean isQbi(QQService4Chong qqService) {
		if (qqService != null) {
			return isQbi(qqService.getGameId());
		}
		return false;
	}

	public static QQService4Chong findInAll(int gameId, int amount) {
		for (QQService4Chong qqService : allQqServices) {
			if (gameId == qqService.getGameId() && amount == qqService.getAmount()) {
				return qqService;
			}
		}
		return null;
	}
	
	public static QQService4Chong findInPrimary(int gameId) {
		for (QQService4Chong qqService : allQqServices) {
			if (SERVICE_TYPE_PRIMARY == qqService.getType() && gameId == qqService.getGameId()) {
				return qqService;
			}
		}
		return null;
	}
	
	public static QQService4Chong[] queryQQServicesByType(int type) {
		if (SERVICE_TYPE_PRIMARY == type) {
			return primaryQQServices;
		} else if (SERVICE_TYPE_SECONDARY_QBI == type) {
			return secondaryQbiServices;
		} else if (SERVICE_TYPE_ALL == type){
			return allQbiServices;
		}
		return new QQService4Chong[0];
	}
	
}
