package com.qq.qqbuy.vb2c.po;

import java.util.Arrays;
import java.util.List;

/**
 * 用于管理手机可充值的金额
 * 
 * @author winsonwu
 * @Created 2011-10-20
 */
public enum MobileAmount
{
    MA10(10, "9.85-10.40 元"), MA20(20, "19.7-20.40 元"), MA30(30, "29.49-30.29 元"), MA50(50, "49-49.80 元"), MA100(100,
            "98-99.50 元"), MA300(300, "294-298.80 元");

//    private static MobileAmount[] mobileAmounts = new MobileAmount[]
//    { MA10, MA20, MA30, MA50, MA100, MA300 };
    
    private static MobileAmount[] mobileAmounts = new MobileAmount[]
    { MA30, MA50, MA100, MA300 };

    private int fen = 0; // 可以充值的金额，以“分”为单位
    private int yuan = 0; // 可以充值的金额，以“元”为单位
    private String tips; // 该充值项对应的Tip描述

    private MobileAmount(int yuan, String tips)
    {
        this.yuan = yuan;
        this.fen = yuan * 100;
        this.tips = tips;
    }

    public static List<MobileAmount> getMobileAmountOptions()
    {
        return Arrays.asList(mobileAmounts);
    }
    
    public static MobileAmount checkAmountYuan(int amountYuan) {
    	for (MobileAmount ma : mobileAmounts) {
    		if (amountYuan == ma.getYuan()) {
    			return ma;
    		}
    	}
    	return null;
    }
    
    /************************************************************* Getter And Setter Method *************************************************************/
    public int getFen()
    {
        return fen;
    }

    public void setFen(int fen)
    {
        this.fen = fen;
    }

    public int getYuan()
    {
        return yuan;
    }

    public void setYuan(int yuan)
    {
        this.yuan = yuan;
    }

    public String getTips()
    {
        return tips;
    }

    public void setTips(String tips)
    {
        this.tips = tips;
    }

}
