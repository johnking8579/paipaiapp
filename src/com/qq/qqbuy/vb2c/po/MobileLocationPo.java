package com.qq.qqbuy.vb2c.po;

/**
 * 
 * @ClassName： MobileLocationPo
 *
 * @Description： 手机号归属信息
 * @author wamiwen
 * @date 2013-8-28 上午09:52:01
 *
 */
public class MobileLocationPo {

	/**
	 * 手机号
	 */
	private String mobile = "";
	
	/**
	 * 归属省份
	 */
	private String province = ""; 
	
	/**
	 * 归属运营商
	 */
	private String isp = "";

	
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getIsp() {
		return isp;
	}

	public void setIsp(String isp) {
		this.isp = isp;
	}
	
}
