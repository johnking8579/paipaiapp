package com.qq.qqbuy.vb2c.po;

/**
 * @author winsonwu
 * @Created 2012-5-22
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class Vb2cOrderDealOutPo {

	private String tokenId;
	private int totalFee;
	private String loginBargainorId;  //用于无线侧校验是否需要免登陆的商户号ID
	private String tenpayUrl;
	
	public String getTokenId() {
		return tokenId;
	}
	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}
	public int getTotalFee() {
		return totalFee;
	}
	public void setTotalFee(int totalFee) {
		this.totalFee = totalFee;
	}
	public String getTenpayUrl() {
		return tenpayUrl;
	}
	public void setTenpayUrl(String tenpayUrl) {
		this.tenpayUrl = tenpayUrl;
	}
	public String getLoginBargainorId() {
		return loginBargainorId;
	}
	public void setLoginBargainorId(String loginBargainorId) {
		this.loginBargainorId = loginBargainorId;
	}
	@Override
	public String toString() {
		return "Vb2cOrderDealOutPo [loginBargainorId=" + loginBargainorId
				+ ", tenpayUrl=" + tenpayUrl + ", tokenId=" + tokenId
				+ ", totalFee=" + totalFee + "]";
	}
	
}
