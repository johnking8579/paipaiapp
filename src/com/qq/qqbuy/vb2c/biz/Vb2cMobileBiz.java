package com.qq.qqbuy.vb2c.biz;

import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.vb2c.po.ValidateMobilePo;
import com.qq.qqbuy.vb2c.po.Vb2cOrderDealOutPo;
import com.qq.qqbuy.vb2c.util.VersionType;

/**
 * @author winsonwu
 * @Created 2012-4-6
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public interface Vb2cMobileBiz {
	
	/**
	 * 话费充值处理逻辑
	 * @param type
	 * @param validatePo
	 * @param vb2cTag
	 * @param cftCommonAttach
	 * @return
	 * @throws BusinessException
	 */
	public Vb2cOrderDealOutPo orderDeal(VersionType type, ValidateMobilePo validatePo, long qq, String vb2cTag, String cftCommonAttach) throws BusinessException;
	
}
