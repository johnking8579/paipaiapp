package com.qq.qqbuy.vb2c.biz;

import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.vb2c.po.QQService4Chong;
import com.qq.qqbuy.vb2c.po.QQServiceOrderDealOutPo;
import com.qq.qqbuy.vb2c.po.ValidateQQServicePo;
import com.qq.qqbuy.vb2c.po.WQQServiceOutPo;
import com.qq.qqbuy.vb2c.util.VersionType;

/**
 * @author winsonwu
 * @Created 2012-4-6
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public interface Vb2cQQServiceBiz {
	
	/**
	 * QQ服务下单逻辑处理
	 * @param type
	 * @param validatePo
	 * @param cuin
	 * @param vb2cTag
	 * @param commonCftAttach
	 * @return
	 * @throws BusinessException
	 */
	public QQServiceOrderDealOutPo orderDeal(VersionType type, ValidateQQServicePo validatePo, long qq, String cuin, String vb2cTag, String commonCftAttach) throws BusinessException;
	
	public WQQServiceOutPo wap2Detail(QQService4Chong qqService) throws BusinessException;

}
