package com.qq.qqbuy.vb2c.biz.impl;

import java.util.Map;

import com.qq.qqbuy.common.ConfigHelper;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.constant.PropertiesConstant;
import com.qq.qqbuy.common.exception.BusinessException;
//import com.qq.qqbuy.thirdparty.openapi.OpenApi;
//import com.qq.qqbuy.thirdparty.openapi.protocol.ChargeMobileResponse;
//import com.qq.qqbuy.thirdparty.openapi.protocol.GetBuyerDRDealDetailResponse;
import com.qq.qqbuy.thirdparty.idl.vb2c.Vb2cClient;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.DealInfo;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetDealInfoResp;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.MobileSubmitResp;
import com.qq.qqbuy.vb2c.biz.TenpayVb2cBiz;
import com.qq.qqbuy.vb2c.biz.Vb2cMobileBiz;
import com.qq.qqbuy.vb2c.po.MobileAmount;
import com.qq.qqbuy.vb2c.po.TenpayPo;
import com.qq.qqbuy.vb2c.po.ValidateMobilePo;
import com.qq.qqbuy.vb2c.po.Vb2cOrderDealOutPo;
import com.qq.qqbuy.vb2c.util.TenpayVb2cUtil;
import com.qq.qqbuy.vb2c.util.VersionType;

/**
 * @author winsonwu
 * @Created 2012-4-6
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class Vb2cMobileBizImpl implements Vb2cMobileBiz {
	
	/************************* 注入变量 ****************************/
	private TenpayVb2cBiz tenpayVb2cBiz;
	
	public Vb2cOrderDealOutPo orderDeal(VersionType type, ValidateMobilePo validatePo, long qq, String vb2cTag, String cftCommonAttach) throws BusinessException {
		Vb2cOrderDealOutPo po = new Vb2cOrderDealOutPo();
		String mobile = validatePo.getMobile();
		MobileAmount currMobileAmount = validatePo.getMobileAmount();
		int amountByFen = currMobileAmount == null ? 0 : currMobileAmount.getFen();
		
		String sid = validatePo.getSid();
		String mkey = validatePo.getMkey();
		String skey = validatePo.getSkey();
		long uin = validatePo.getUin();
		Log.run.debug("Vb2cMobileBizImpl==>orderDeal :sid: " + sid + ", mkey: " + mkey + ", skey: " + skey + ", uin: " + uin);  
		
		//在虚拟充值中心下单
		MobileSubmitResp mobileSubmitResp = Vb2cClient.mobileOrderDeal(uin, mobile, amountByFen, vb2cTag, mkey, skey, uin);
		if (mobileSubmitResp == null || mobileSubmitResp.getResult() != 0 || mobileSubmitResp.getOrderResult() == null) 
		{
			//面值缺货单独提出来用于屏蔽
			if(mobileSubmitResp!=null && mobileSubmitResp.getResult()==1217)
			{
				throw BusinessException.createInstance(ErrConstant.ERRCODE_BIZ_VB2C_MOBILE_SHORTAGE, "下单失败，请重试[特定面额缺货]");
			}
			else
			{
				String msg = mobileSubmitResp == null ? "MobileSubmitResp为null" : "" + mobileSubmitResp.getResult();
				throw BusinessException.createInstance(ErrConstant.ERRCODE_BIZ_VB2C_MOBILE_ORDER_DEAL_FAIL, "下单失败，请重试[" + msg + "]");
			}

		}
	
		//查询订单详情
		GetDealInfoResp dealInfoResp = Vb2cClient.queryDealInfo(uin, mobileSubmitResp.getOrderResult().getDealId(), mkey, skey, uin);
		if (dealInfoResp == null || dealInfoResp.getResult() != 0 || dealInfoResp.getDealInfo() == null) {
			String msg = dealInfoResp == null ? "DealInfoResp为null" : "" + dealInfoResp.getResult();
		    throw BusinessException.createInstance(ErrConstant.ERRCODE_BIZ_VB2C_QUERY_DEAL_DETAIL_FAIL, "获取下单详情失败，请重试[" + msg + "]");
		}
		DealInfo dealInfo = dealInfoResp.getDealInfo();

        
        // 获取TenpayUrl
        Map<String, String> reqParam = TenpayVb2cUtil.genParameter4Mobile(type, /*dealDetailResp.deal*/dealInfo, cftCommonAttach, uin, amountByFen, mobile);
        TenpayPo tenpayPo = tenpayVb2cBiz.genTenPayUrl(reqParam, sid);
        
        po.setTenpayUrl(tenpayPo.getTenpayUrl());
		po.setLoginBargainorId(ConfigHelper.getStringByKey(PropertiesConstant.VB2C_LOGIN_BARGAINOR_ID, ""));
		po.setTokenId(tenpayPo.getTokenId());
		po.setTotalFee(/*dealDetailResp.deal.payFee*/(int)dealInfo.getPayFee());
		return po;
	}
	
	public TenpayVb2cBiz getTenpayVb2cBiz() {
		return tenpayVb2cBiz;
	}

	public void setTenpayVb2cBiz(TenpayVb2cBiz tenpayVb2cBiz) {
		this.tenpayVb2cBiz = tenpayVb2cBiz;
	}
	
}
