package com.qq.qqbuy.vb2c.biz.impl;

import java.util.Map;

import com.paipai.util.string.StringUtil;
import com.qq.qqbuy.common.ConfigHelper;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.constant.PropertiesConstant;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.MD5Util;
import com.qq.qqbuy.tenpay.biz.ITenpayService;
import com.qq.qqbuy.thirdparty.http.vb2c.TenpayVb2cClient;
import com.qq.qqbuy.vb2c.biz.TenpayVb2cBiz;
import com.qq.qqbuy.vb2c.po.TenpayPo;
import com.qq.qqbuy.vb2c.util.TenpayVb2cUtil;
import com.qq.qqbuy.vb2c.util.Vb2cConstant;

/**
 * @author winsonwu
 * @Created 2012-4-18 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class TenpayVb2cBizImpl implements TenpayVb2cBiz {

	private ITenpayService tenpayVb2cService;
	
	public TenpayPo genTenPayUrl(Map<String, String> reqParam, String sid) throws BusinessException {
        String tokenId = tenpayVb2cService.getToken(reqParam);
        if (StringUtil.isEmpty(tokenId)) {
			throw BusinessException.createInstance(ErrConstant.ERRCODE_BIZ_VB2C_GET_TENPAY_TOKEN_FAIL, "虚拟充值获取财付通TokenId失败");
		}
		// 拼凑跳转到财付通页面的链接
		String tenPayUrl = genTenPayUrl(tokenId, sid);
		if (StringUtil.isEmpty(tenPayUrl)) {
			throw BusinessException.createInstance(ErrConstant.ERRCODE_BIZ_VB2C_GEN_TENPAY_WAPURL_FAIL, "拼凑跳转到财付通页面的链接失败，请重试");
		}
		TenpayPo po = new TenpayPo(tokenId, tenPayUrl);
		return po;
	}
	
	public boolean tenpayCallBack(String ver, String charset, String payResult,
			String transactionId, String spBillno, String totalFee,
			String feeType, String bargainorId, String attach, String sign_sp_id, String sign)
			throws BusinessException {
		// 校验sign值
		Map<String, String> param = TenpayVb2cUtil.genParamMap(ver, charset,
				payResult, transactionId, spBillno, totalFee, feeType,
				bargainorId, attach, sign_sp_id);
		if (!tenpayVb2cService.callBack(param, sign)) {
			Log.run.warn("tenpayCallBack sign and param not match,param=" + param + ",sign="
					+ sign);
			return false;
		}

		return "0".equals(payResult); // payResult为0时候代表支付成功
	}

	@Override
	public boolean tenpayNotify(String ver, String charset, String bankType,
			String bankBillno, String payResult, String payInfo,
			String purchaseAlias, String bargainorId, String transactionId,
			String spBillno, String totalFee, String feeType, String attach,
			String timeEnd, String sign_sp_id, String sign) throws BusinessException {
		Map<String, String> param = TenpayVb2cUtil.genParamMap(ver, charset,
				bankType, bankBillno, payResult, payInfo, purchaseAlias,
				bargainorId, transactionId, spBillno, totalFee, feeType,
				attach, timeEnd, sign_sp_id);
		if (!tenpayVb2cService.callBack(param, sign)) {
			Log.run.warn("tenpayNotify sign and param not match,param=" + param + ",sign="
					+ sign);
			return false;
		}
		// 通知虚拟充值中心财付通支付结果
		return TenpayVb2cClient.tenpayNotifyVb2c(ver, charset, bankType,
				bankBillno, payResult, payInfo, purchaseAlias, bargainorId,
				transactionId, spBillno, totalFee, feeType, attach, timeEnd, sign_sp_id,
				sign);
	}
	
	protected String genTenPayUrl(String tokenId, String sid) {
		try {
			StringBuffer sbf = new StringBuffer();
			// 注意: winson
			// 此处拼串的时候要将token_id写在最前面，这可能跟财付通那边解析串时候的bug相关(直接通过?token_id、&sign这样来获取值的)
			// tiehualiu推荐使用tokenId和sid参数即可，sid参数主要在于财付通跳转到HTML5版本的时候，能够自动登录财付通
			String tenpayUrl = ConfigHelper.getStringByKey(
					PropertiesConstant.VB2C_TENPAY_URL_PREFIX,
					Vb2cConstant.VB2C_TENPAY_DEFAULT_URL);
			
			sbf.append(tenpayUrl);
			sbf.append("?token_id=");
			sbf.append(tokenId);
			if (!StringUtil.isEmpty(sid)) {
				sbf.append("&");
				sbf.append("sid=");
				sbf.append(sid);
			}
			
			// TODO winson 以前手拍的拼接逻辑
			// 会传递gateSpId、backUrl等参数，但发现这些参数用处不大，还需要进行MD5的签名，此处出现过校验失败导致访问失败的情况
			// 卖家商户ID：gateSpId = "1000000101";
//			String gateSpId = ConfigHelper.getStringByKey(PropertiesConstant.VB2C_GATE_SPID, "");
//			String callBackUrl = ConfigHelper.getStringByKey(
//					PropertiesConstant.VB2C_TENPAY_CALLBACK_URL_PREFIX,
//					Vb2cConstant.VB2C_TENPAY_CALLBACK_DEFAULT_URL);
			// sbf.append("&amp;");
			// Map<String, String> params = new HashMap<String, String>();
			// params.put("back_url", backUrl);
			// params.put("gate_sp_id", gateSpId);
			// params.put("sign", genSign(params));
			// sbf.append(HttpUtil.toQueryString(params, "utf-8"));
			return sbf.toString();
		} catch (Exception e) {
			Log.run.error("generate the ten pay url fail", e);
		}
		return null;
	}

	protected String genSign(Map<String, String> reqParam) {
		return MD5Util.md5Sign(reqParam, "key","f4becb5fb2e007c5fb7238b619b8956b",
				"utf-8").toUpperCase();
	}

	public ITenpayService getTenpayVb2cService() {
		return tenpayVb2cService;
	}

	public void setTenpayVb2cService(ITenpayService tenpayVb2cService) {
		this.tenpayVb2cService = tenpayVb2cService;
	}

}
