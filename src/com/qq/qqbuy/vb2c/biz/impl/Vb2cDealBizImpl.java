package com.qq.qqbuy.vb2c.biz.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.tenpay.biz.impl.PayTokenInfo;
import com.qq.qqbuy.thirdparty.idl.vb2c.Vb2cClient;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.DealInfo;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetDealInfoResp;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetDealListExResp;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetDealListExRespBo;
import com.qq.qqbuy.vb2c.biz.TenpayVb2cBiz;
import com.qq.qqbuy.vb2c.biz.Vb2cDealBiz;
import com.qq.qqbuy.vb2c.po.MobileLocationPo;
import com.qq.qqbuy.vb2c.po.TenpayPo;
import com.qq.qqbuy.vb2c.po.Vb2cDealInfo;
import com.qq.qqbuy.vb2c.po.Vb2cDealInfos;
import com.qq.qqbuy.vb2c.util.TenpayVb2cUtil;
import com.qq.qqbuy.vb2c.util.Vb2cConstant;
import com.qq.qqbuy.vb2c.util.Vb2cIDLResultConveter;
import com.qq.qqbuy.vb2c.util.VersionType;

public class Vb2cDealBizImpl implements Vb2cDealBiz {
	private TenpayVb2cBiz tenpayVb2cBiz;

	public TenpayVb2cBiz getTenpayVb2cBiz() {
		return tenpayVb2cBiz;
	}

	public void setTenpayVb2cBiz(TenpayVb2cBiz tenpayVb2cBiz) {
		this.tenpayVb2cBiz = tenpayVb2cBiz;
	}

	@Override
	public Vb2cDealInfos getDealList(long buyerUin, int pn, int ps, int state,
			String mkey, String skey, String dealType) {
		Vb2cDealInfos ret = new Vb2cDealInfos();
		List<Vb2cDealInfo> dealInfos = new ArrayList<Vb2cDealInfo>();
		// 1、检查参数
		if (buyerUin > 10000 && StringUtils.isNotBlank(mkey)
				&& StringUtils.isNotBlank(skey)) {
			if (pn <= 0)
				pn = 1;
			if (ps <= 0) {
				ps = 50;
			}

			GetDealListExResp resp = Vb2cClient.queryDealList(buyerUin, pn, ps,
					state, mkey, skey, dealType);
			GetDealListExRespBo bo = null;
			if (resp != null && resp.getResult() == 0
					&& (bo = resp.getResp()) != null) {
				ret.setTotalNum(bo.getTotalNum());
				dealInfos = Vb2cIDLResultConveter.convert(bo);
			}
			ret.setItems(dealInfos);

		}
		return ret;
	}

	@Override
	public Vb2cDealInfo getDealItem(long buyerUin, String dealId, String mkey,
			String skey, long uin) {
		Vb2cDealInfo ret = new Vb2cDealInfo();
		if (buyerUin > 10000 && StringUtils.isNotBlank(mkey)
				&& StringUtils.isNotBlank(skey)
				&& StringUtils.isNotBlank(dealId)) {
			GetDealInfoResp resp = Vb2cClient.queryDealInfo(buyerUin, dealId,
					mkey, skey, uin);
			if (resp != null && resp.getResult() == 0) {
				ret = Vb2cIDLResultConveter.convert(resp.getDealInfo());
			}
		}
		return ret;
	}

	@Override
	public PayTokenInfo getToken(long buyerUin, String dealId, String mkey,
			String skey) {
		PayTokenInfo payTokenInfo = new PayTokenInfo();

		if (buyerUin > 10000 && StringUtils.isNotBlank(mkey)
				&& StringUtils.isNotBlank(skey)
				&& StringUtils.isNotBlank(dealId)) {
			GetDealInfoResp resp = Vb2cClient.queryDealInfo(buyerUin, dealId,
					mkey, skey, buyerUin);
			DealInfo dealInfo = null;
			if (resp != null && resp.getResult() == 0
					&& (dealInfo = resp.getDealInfo()) != null) {
				if (dealInfo.getDealState() != 1) {
					payTokenInfo.msg = "交易已经完成！";
					return payTokenInfo;
				}
				Map<String, String> extInfos = dealInfo.getExtInfo();
				String account = "";
				int faceValue = 0;
				if (extInfos != null) {
					account = extInfos.get("Account");
					faceValue = StringUtil.toInt(extInfos.get("FaceValue"), 0);
				}
				Map<String, String> reqParam = TenpayVb2cUtil
						.genParameter4Mobile(VersionType.VERSION_ANDROID, /*
																		 * dealDetailResp.
																		 * deal
																		 */
						dealInfo, "android", buyerUin, faceValue, account);
				try {
					TenpayPo tenpayPo = tenpayVb2cBiz.genTenPayUrl(reqParam,
							null);
					if (tenpayPo != null) {
						payTokenInfo.setToken(tenpayPo.getTokenId());
						payTokenInfo.setH5PayUrl(tenpayPo.getTenpayUrl());
					}
				} catch (BusinessException e) {
					Log.run.error("Vb2cDealBizImpl#getToken error buyerUin:"
							+ buyerUin + " dealId:" + dealId, e);
				}
			}
		}

		return payTokenInfo;
	}

	@Override
	public MobileLocationPo getMobileLocation(String mo) {
		MobileLocationPo mobileLocationPo = new MobileLocationPo();
		if (StringUtils.isNotBlank(mo) && Util.isMobileNO(mo)) {
			String urlStr = String.format(
					Vb2cConstant.VB2C_GET_MOBILE_LOCATION_URL_FORMAT, mo);
			String retStr = HttpUtil.get(urlStr, Vb2cConstant.CONNECTTIMEOUT,
					Vb2cConstant.READTIMEOUT, Vb2cConstant.DEFAULT_ENCODING,
					true);
			String jsonStr = parseHttpRespContent(retStr);
			try {
				JSONObject jsonObj = JSONObject.fromObject(jsonStr);
				mobileLocationPo.setMobile(jsonObj.optString("mobile", ""));
				mobileLocationPo.setProvince(jsonObj.optString("province", ""));
				mobileLocationPo.setIsp(jsonObj.optString("isp", ""));
			} catch (Exception e) {
				Log.run.error(
						"Vb2cDealBizImpl#getMobileLocation format jsonStr error jsonStr:"
								+ jsonStr, e);
			}
		}
		return mobileLocationPo;
	}

	/**
	 * 
	 * @Title: parseHttpRespContent
	 * 
	 * @Description: 从Http请求返回内容中解析出json格式字符串
	 * @param @param httpRespStr http请求返回内容，形如"try{({mobile:'18665903361',province:'广东',isp:'中国联通',stock:'0',amount:'100',maxprice:'0',minprice:'0'});}catch(e){}"
	 * @param @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	private String parseHttpRespContent(String httpRespStr) {
		String jsonStr = "";
		if (httpRespStr == null || httpRespStr.length() < 18) {
			return jsonStr;
		}
		int indexFirst = httpRespStr.indexOf("(");
		int indexLast = httpRespStr.lastIndexOf(";");
		if (indexFirst > 1 && indexLast > 1 && indexLast > indexFirst) {
			jsonStr = httpRespStr.substring(indexFirst + 1, indexLast - 1);
		}

		return jsonStr;
	}

}
