package com.qq.qqbuy.vb2c.biz.impl;

import java.util.Map;

import com.qq.qqbuy.common.ConfigHelper;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.constant.PropertiesConstant;
import com.qq.qqbuy.common.exception.BusinessException;
//import com.qq.qqbuy.thirdparty.openapi.OpenApi;
//import com.qq.qqbuy.thirdparty.openapi.protocol.ChargeGameResponse;
//import com.qq.qqbuy.thirdparty.openapi.protocol.GetBuyerDRDealDetailResponse;
import com.qq.qqbuy.thirdparty.idl.vb2c.Vb2cClient;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.DealInfo;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GameSubmitResp;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetDealInfoResp;
import com.qq.qqbuy.vb2c.biz.TenpayVb2cBiz;
import com.qq.qqbuy.vb2c.biz.Vb2cQQServiceBiz;
import com.qq.qqbuy.vb2c.po.ApiQQServiceInfo;
import com.qq.qqbuy.vb2c.po.QQService4Chong;
import com.qq.qqbuy.vb2c.po.QQServiceOrderDealOutPo;
import com.qq.qqbuy.vb2c.po.TenpayPo;
import com.qq.qqbuy.vb2c.po.ValidateQQServicePo;
import com.qq.qqbuy.vb2c.po.WQQServiceOutPo;
import com.qq.qqbuy.vb2c.util.TenpayVb2cUtil;
import com.qq.qqbuy.vb2c.util.Vb2cUtil;
import com.qq.qqbuy.vb2c.util.VersionType;

/**
 * @author winsonwu
 * @Created 2012-5-10
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class Vb2cQQServiceBizImpl implements Vb2cQQServiceBiz {

	/***************** 注入变量 ******************************/
	private TenpayVb2cBiz tenpayVb2cBiz;
	
	public QQServiceOrderDealOutPo orderDeal(VersionType type, ValidateQQServicePo validatePo, long qq, String cuin, String vb2cTag, String commonCftAttach) throws BusinessException {
		QQServiceOrderDealOutPo po = new QQServiceOrderDealOutPo();
		
		int gameId = validatePo.getGameId();
		int buyCount = validatePo.getBuyCount();
		String itemId = validatePo.getItemId();
		QQService4Chong qqService = validatePo.getQqService();
		
		// 调IDL接口下单
		String sid = validatePo.getSid();
		String mkey = validatePo.getMkey();
		String skey = validatePo.getSkey();
		long uin = validatePo.getUin();

		// 调IDL接口下单
		GameSubmitResp gameSubmitResp = Vb2cClient.gameOrderDeal(uin, itemId, buyCount, "", "", "", 
				"", cuin, vb2cTag, mkey, skey, uin);
		if (gameSubmitResp == null || gameSubmitResp.getResult() != 0 || gameSubmitResp.getOrderResult() == null) {
			String msg = gameSubmitResp == null ? "GameSubmitResp为null" : "" + gameSubmitResp.getResult();
			//throw BusinessException.createInstance(ErrConstant.ERRCODE_BIZ_VB2C_GAME_ORDER_DEAL_FAIL, "虚拟QQ服务下单失败，请重试[" + msg + "]");
		}
        // 调OpenAPI下单
//        ChargeGameResponse chargeGameResp = OpenApi.getProxy().onlineGameOrderDeal(qq, itemId, buyCount, cuin, "", "", "", "", vb2cTag);
//        if (chargeGameResp == null || !chargeGameResp.isSucceed())
//        {
//        	String msg = OpenApi.genOpenApiErrorMsg(chargeGameResp);
//        	throw BusinessException.createInstance(ErrConstant.ERRCODE_BIZ_VB2C_GAME_ORDER_DEAL_FAIL, "虚拟QQ服务下单失败，请重试[" + msg + "]");
//        }
        
        // 获取订单详情
		GetDealInfoResp dealInfoResp = Vb2cClient.queryDealInfo(uin, gameSubmitResp.getOrderResult().getDealId(), mkey, skey, uin);
		if (dealInfoResp == null || dealInfoResp.getResult() != 0 || dealInfoResp.getDealInfo() == null) {
			String msg = dealInfoResp == null ? "DealInfoResp为null" : "" + dealInfoResp.getResult();
			//throw BusinessException.createInstance(ErrConstant.ERRCODE_BIZ_VB2C_QUERY_DEAL_DETAIL_FAIL, "获取下单详情失败，请重试[" + msg + "]");
		}
		DealInfo dealInfo = dealInfoResp.getDealInfo();
//        GetBuyerDRDealDetailResponse dealDetailResp = OpenApi.getProxy().queryBuyerDirectRechargeDealDetail(qq, chargeGameResp.dealId);
//        if (dealDetailResp == null || !dealDetailResp.isSucceed())
//        {
//        	String msg = OpenApi.genOpenApiErrorMsg(dealDetailResp);
//        	throw BusinessException.createInstance(ErrConstant.ERRCODE_BIZ_VB2C_QUERY_DEAL_DETAIL_FAIL, "获取下单详情失败，请重试[" + msg + "]");
//        }
        
        Map<String, String> reqParam = TenpayVb2cUtil.genParamter4QQService(type, /*dealDetailResp.deal*/dealInfo, commonCftAttach, uin, qqService.getAmount(),
        		gameId, buyCount, cuin, qqService.getName());
        TenpayPo tenpayPo = tenpayVb2cBiz.genTenPayUrl(reqParam, sid);
        
        po.setQqServiceInfo(ApiQQServiceInfo.createInstance(qqService));
		po.setTenpayUrl(tenpayPo.getTenpayUrl());
		po.setLoginBargainorId(ConfigHelper.getStringByKey(PropertiesConstant.VB2C_LOGIN_BARGAINOR_ID, ""));
		po.setTokenId(tenpayPo.getTokenId());
		po.setTotalFee(/*dealDetailResp.deal.payFee*/(int)dealInfo.getPayFee());
		
		return po;
	}
	
	@Override
	public WQQServiceOutPo wap2Detail(QQService4Chong qqService) throws BusinessException {
		WQQServiceOutPo po = new WQQServiceOutPo();
		po.setCurrServiceInfo(ApiQQServiceInfo.createInstance(qqService));
		po.setQbi(QQService4Chong.isQbi(qqService));
		po.setPricePerMonth("" + qqService.getAmount()); 
		po.setQqServiceDisName(Vb2cUtil.genQQServiceDisName(qqService));
		
		return po;
	}

	public TenpayVb2cBiz getTenpayVb2cBiz() {
		return tenpayVb2cBiz;
	}

	public void setTenpayVb2cBiz(TenpayVb2cBiz tenpayVb2cBiz) {
		this.tenpayVb2cBiz = tenpayVb2cBiz;
	}
	
}
