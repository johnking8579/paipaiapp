package com.qq.qqbuy.vb2c.biz;

import java.util.Map;

import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.vb2c.po.TenpayPo;

/**
 * 虚拟充值：和财付通相关的Biz服务
 * 
 * @author winsonwu
 * @Created 2012-4-18 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public interface TenpayVb2cBiz {
	
	/**
	 * 虚拟充值获取财付通支付的Wap2.0跳转页面URL
	 * @param reqParam
	 * @param sid 
	 * @return
	 * @throws BusinessException
	 */
	public TenpayPo genTenPayUrl(Map<String, String> reqParam, String sid) throws BusinessException; 
	
	/**
	 * 虚拟充值在接收到财付通后端Notify的处理：判断参数，通知虚拟充值中心订单支付成功
	 * @param ver 版本号,ver默认值是1.0。目前版本ver取值应为2.0
	 * @param charset 1 UTF-8, 2 GB2312, 默认为1 UTF-8
	 * @param bank_type 银行类型:财付通支付填0
	 * @param bank_billno 银行订单号，若为财付通余额支付则为空
	 * @param pay_result 支付结果： 0—成功；其它—失败
	 * @param pay_info 支付结果信息，支付成功时为空
	 * @param purchase_alias 买家唯一标识，由财付通生成。注意不同于purchase_id财付通帐户
	 * @param bargainor_id 卖方账号（商户spid）
	 * @param transaction_id 财付通交易号(订单号)
	 * @param sp_billno 商户系统内部的定单号，此参数仅在对账时提供
	 * @param total_fee 订单总金额，以分为单位
	 * @param fee_type 现金支付币种
	 * @param attach 商家数据包，原样返回
	 * @param time_end 支付完成时间，格式为yymmddhhmmss，如2009年12月27日9点10分10秒表示为20091227091010。时区为GMT+8 beijing。该时间取自财付通服务器
	 * @param sign_sp_id 商户号
	 * @param sign MD5签名结果,详见“商户签名规则”
	 * @return
	 */
	public boolean tenpayNotify(String ver, String charset, String bank_type, String bank_billno, String pay_result, String pay_info, 
			String purchase_alias, String bargainor_id, String transaction_id, String sp_billno, String total_fee, String fee_type, 
			String attach, String time_end, String sign_sp_id, String sign) throws BusinessException;
	
	/**
	 * 虚拟充值在接收到财付通前端Callback的处理：校验参数的MD5结果
	 * @param ver 版本号,ver默认值是1.0。目前版本ver取值应为2.0
	 * @param charset 1 UTF-8, 2 GB2312, 默认为1 UTF-8
	 * @param pay_result 支付结果： 0—成功；其它—失败
	 * @param transaction_id 财付通交易号(订单号)
	 * @param sp_billno 商户系统内部的定单号，此参数仅在对账时提供。
	 * @param total_fee 订单总金额，以分为单位;
	 * @param fee_type 现金支付币种
	 * @param bargainor_id 卖方账号（商户spid）
	 * @param attach 商家数据包，原样返回
	 * @param sign_sp_id 商户号
	 * @param sign MD5签名结果,详见“商户签名规则”
	 * @return
	 */
	public boolean tenpayCallBack(String ver, String charset, String pay_result,
			String transaction_id, String sp_billno, String total_fee,
			String fee_type, String bargainor_id, String attach, String sign_sp_id, String sign) throws BusinessException;

}
