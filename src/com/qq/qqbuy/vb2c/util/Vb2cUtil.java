package com.qq.qqbuy.vb2c.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.po.Pair;
import com.qq.qqbuy.vb2c.po.QQService4Chong;
import com.qq.qqbuy.vb2c.po.ValidateGamePo;
import com.qq.qqbuy.vb2c.po.game.Product;
import com.qq.qqbuy.vb2c.po.game.ProductInfo;
import com.qq.qqbuy.vb2c.po.game.Section;
import com.qq.qqbuy.vb2c.po.game.SectionServer;
import com.qq.qqbuy.vb2c.po.game.Server;

/**
 * @author winsonwu
 * @Created 2012-4-24
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class Vb2cUtil {
	
	private static List<String> categories = null;
	
	public static String genQQServiceDisName(int gameId, int amount) {
		QQService4Chong qqService = QQService4Chong.findInAll(gameId, amount);
		return genQQServiceDisName(qqService);
	}
	
	public static String genQQServiceDisName(QQService4Chong qqService) {
		if (qqService != null) {
			if (QQService4Chong.isQbi(qqService)) {
				return "充Q币";
			} else {
				return qqService.getName() + "(" + qqService.getAmount() + "/月)";
			}
		}
		return "";
	}
	
	public static List<String> getGameCategories() {
		if (categories == null) {
			synchronized (Vb2cUtil.class) {
				if (categories == null) {
					categories = new ArrayList<String>();
//					categories.add("9");
					for (char i='A'; i<='Z'; i++) {
						categories.add(""+i);
					}
				}
			}
		}
		return categories;
	}
	
	protected static List<Pair> genWapGameAmountChargeTypeOptions(List<Product> products, int type) {
		List<Pair> amountOptions = new ArrayList<Pair>();
		List<Pair> chargeTypeOptions = new ArrayList<Pair>();
		if (products != null && products.size() > 0) {
			for (Product product : products) {
				int a = product.getAmount();
				Pair amountp = new Pair(""+a+"元", ""+a);
				if (!amountOptions.contains(amountp)) {
					amountOptions.add(amountp);
				}
				
				ProductInfo info = product.getProductInfo();
				if (info != null) {
					Pair chargeTypep = new Pair(info.getChargeType(), info.getChargeType());
					if (!chargeTypeOptions.contains(chargeTypep)) {
						chargeTypeOptions.add(chargeTypep);
					}
				}
			}
			// 对选项进行排序
	        Collections.sort(amountOptions, Pair.PAIR_CODE_INT_COMPARATOR);
	        Collections.sort(chargeTypeOptions, Pair.PAIR_NAME_STRING_COMPARATOR);
		}
		switch(type) {
		case WGameDetailIndexer.WGAME_DETAIL_INDEX_AMOUNT:
			return amountOptions;
		case WGameDetailIndexer.WGAME_DETAIL_INDEX_CHARGETYPE:
			return chargeTypeOptions;
		}
		return new ArrayList<Pair>();
	}
	
	protected static List<Pair> genWapGameSectionOptions(List<SectionServer> ss) {
		List<Pair> options = new ArrayList<Pair>();
		if (ss != null && ss.size() > 0) {
			for (SectionServer sectionServer : ss) {
				Section section = sectionServer.getSection();
				if (section != null) {
					Pair p = new Pair(section.getName(), section.getCode());
					if (!options.contains(p)) {
						options.add(p);
					}
				}
			}
			// 对选项进行排序
	        Collections.sort(options, Pair.PAIR_NAME_STRING_COMPARATOR);
		}
		return options;
	}
	
	protected static List<Pair> genWapGameBuyCountOptions(Product currProduct) {
		List<Pair> options = new ArrayList<Pair>();
		if (currProduct != null) {
			ProductInfo info = currProduct.getProductInfo();
			if (info != null) {
				List<Integer> chargeNums = info.getChargeNum();
				if (chargeNums != null && chargeNums.size() > 0) {
					for (Integer chargeNum : chargeNums) {
						Pair p = new Pair(""+chargeNum, ""+chargeNum);
						if (!options.contains(p)) {
							options.add(p);
						}
					}
				}
			}
			// 对选项进行排序
			Collections.sort(options, Pair.PAIR_CODE_INT_COMPARATOR);
		}
		return options;
	}
	
	protected static List<Pair> genWapGameServerOptions(SectionServer sectionServer) {
		List<Pair> options = new ArrayList<Pair>();
		if (sectionServer != null) {
			List<Server> servers = sectionServer.getServerList();
			if (servers != null && servers.size() > 0) {
				for (Server server : servers) {
					Pair p = new Pair(server.getName(), server.getCode());
					if (!options.contains(p)) {
						options.add(p);
					}
				}
			}
			// 对选项进行排序
			Collections.sort(options, Pair.PAIR_NAME_STRING_COMPARATOR);
		}
		return options;
	}
	
	public static List<Pair> genWap2GameDetailOptions(int index, ValidateGamePo gamePo) {
		Log.run.debug("index:" + index + ", gamePo:" + gamePo);
		switch (index) {
		case WGameDetailIndexer.WGAME_DETAIL_INDEX_AMOUNT:
		case WGameDetailIndexer.WGAME_DETAIL_INDEX_CHARGETYPE:
			return genWapGameAmountChargeTypeOptions(gamePo.getProducts(), index);
		case WGameDetailIndexer.WGAME_DETAIL_INDEX_SECTION:
			return genWapGameSectionOptions(gamePo.getSectionServers());
		case WGameDetailIndexer.WGAME_DETAIL_INDEX_SERVER:
			return genWapGameServerOptions(gamePo.getCurrSectionServer());
		case WGameDetailIndexer.WGAME_DETAIL_INDEX_BUYCOUNT:
			return genWapGameBuyCountOptions(gamePo.getCurrProduct());
		case WGameDetailIndexer.WGAME_DETAIL_INDEX_ALLINFO:
			break;
		}
		
		return new ArrayList<Pair>();
	}
	
}
