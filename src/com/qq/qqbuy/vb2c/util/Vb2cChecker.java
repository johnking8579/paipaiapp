package com.qq.qqbuy.vb2c.util;

import java.util.List;

import com.qq.qqbuy.common.BasicResult;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.thirdparty.idl.vb2c.Vb2cClient;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetGameItemResp;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetGameProductResp;
import com.qq.qqbuy.vb2c.po.MobileAmount;
import com.qq.qqbuy.vb2c.po.OnlineGame;
import com.qq.qqbuy.vb2c.po.QQService4Chong;
import com.qq.qqbuy.vb2c.po.ValidateGamePo;
import com.qq.qqbuy.vb2c.po.ValidateMobilePo;
import com.qq.qqbuy.vb2c.po.ValidateQQServicePo;
import com.qq.qqbuy.vb2c.po.game.Product;
import com.qq.qqbuy.vb2c.po.game.ProductInfo;
import com.qq.qqbuy.vb2c.po.game.Section;
import com.qq.qqbuy.vb2c.po.game.SectionServer;
import com.qq.qqbuy.vb2c.po.game.Server;

/**
 * 虚拟充值部分参数校验汇总
 * @author winsonwu
 * @Created 2012-5-21
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class Vb2cChecker  {
	
	//1-等待买家付款 ；2-等待发货； 3-发货中；4-退款中；5-交易成功；6-交易完成，已退款； 7-交易取消
	public static boolean checkDealState(int state) {
		return state >= 1 && state <= 7;
	}
	
	public static boolean checkChannelId(int channel) {
		return channel >= 0;
	}
	
	public static boolean isWOW(int gameId) {
		return gameId == 2220;  //魔兽世界的GameID
	}
	
	public static boolean isStarCraft(int gameId) {
		return gameId == 3218;  //星际争霸2的GameID
	}
	
	/********************************************** 话费部分的参数校验 ********************************************/
	public static ValidateMobilePo checkMobileAmount(BasicResult result, String mobile, int amountByYuan) {
		ValidateMobilePo po = new ValidateMobilePo();
		po.setMobile(mobile);
		po.setAmountByYuan(amountByYuan);
		if (result != null) {
			if (!Util.isMobileNO(mobile)) {
				result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
				result.setMsg("手机号码输入错误,请重新输入11位手机号码");
				return po;
			}

			MobileAmount tMobileAmount = MobileAmount.checkAmountYuan(amountByYuan);
			if (tMobileAmount == null) {
				result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
				result.setMsg("请选择正确的面值");
				return po;
			}
			po.setMobileAmount(tMobileAmount);
		}
		return po;
	}
	
	/********************************************** 网游部分的参数校验 *********************************************/
	public static ValidateGamePo checkGameId(BasicResult result, int gameId) {
		ValidateGamePo po = new ValidateGamePo(gameId);
		if (result != null) {
			// 检查网游ID
			OnlineGame findgame = GameCache4File.getOnlineGameById(gameId);
			if (findgame == null) {
				result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
				result.setMsg("网游编码不正确");
				return po;
			}
			po.setCurrGame(findgame);
			// 调IDL去查询网游Product信息
			GetGameProductResp queryGameProductResp = Vb2cClient.queryGameProductInfo(findgame.getItemName());
			if (queryGameProductResp == null || queryGameProductResp.getResult() != 0) {
				String msg = queryGameProductResp == null ? "GameProductResp为null" : "" + queryGameProductResp.getResult();
				result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
				result.setMsg("查询游戏Product信息失败，请重试[" + msg + "]");
				return po;
			}
			po.setProducts(Vb2cIDLResultConveter.getGameProductList(queryGameProductResp));
			po.setSectionServers(Vb2cIDLResultConveter.getGameSectionServer(queryGameProductResp));
			
//			GetGameProductResponse queryGameDetailResp = OpenApi.getProxy().queryOnlineGameProductInfo(findgame.getItemName());
//            if (queryGameDetailResp == null || !queryGameDetailResp.isSucceed())
//            {
//            	String msg = OpenApi.genOpenApiErrorMsg(queryGameDetailResp);
//                result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
//                result.setMsg("查询游戏详情失败，请重试[" + msg + "]");
//                return po;
//            }
//            po.setProducts(queryGameDetailResp.getProductList());
//            po.setSectionServers(queryGameDetailResp.getSectionServerList());
		}
		return po;
	}
	
	public static Product checkGameAmountChargeType(BasicResult result, List<Product> products, int amount, String chargeType) {
		if (result != null) {
			if (products == null || products.size() <= 0) {
	        	result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
	        	result.setMsg("网游没有相应的产品");
	        	return null;
	        }
			// 检查amount、chargeType的Product是否存在
	        Product currProduct = null;
	        for (Product product : products) {
				ProductInfo info = product.getProductInfo();
				if (info != null && amount == product.getAmount() &&
						chargeType != null && chargeType.equals(info.getChargeType())) {
					currProduct = product;
				}
			}
	        if (currProduct == null) {
	        	result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
	        	result.setMsg("没有找到相应的网游产品, amount:" + amount + "; chargeType:" + chargeType);
	        	return null;
	        }
	        return currProduct;
		}
		return null;
	}
	
	public static ValidateGamePo checkGame(BasicResult result, int gameId, int amount, String chargeType, int buyCount, String sectionCode, String serverCode) {
		if (result != null) {
			// 检查GameId
			ValidateGamePo po = checkGameId(result, gameId);
			if (result.getErrCode() != 0) {
				return po;
			}
			
			// 检查Amount、chargeType
			Product currProduct = checkGameAmountChargeType(result, po.getProducts(), amount, chargeType);
			if (result.getErrCode() != 0) {
				return po;
			}
			po.setAmount(amount);
			po.setChargeType(chargeType);
			po.setCurrProduct(currProduct);
			
			// 检查buyCount
			ProductInfo currProductInfo = currProduct.getProductInfo();
	        List<Integer> chargeNums = currProductInfo.getChargeNum();
	        if (chargeNums == null || !chargeNums.contains(buyCount)) {
	        	result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
	        	result.setMsg("购买数量不正确");
	        	return po;
	        }
	        po.setBuyCount(buyCount);
	        
	        // 检查网游商品信息
	        GetGameItemResp gameItemResp = Vb2cClient.queryGameItemInfo(currProductInfo.productId);
	        if (gameItemResp == null || gameItemResp.getResult() != 0 || gameItemResp.getItemInfo() == null) {
	        	String msg = gameItemResp == null ? "gameItemResp为null" : "" + gameItemResp.getResult();
	        	result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
	        	result.setMsg("获取网游商品信息失败[" + msg + "]");
	        	return po;
	        }
	        po.setItemId(gameItemResp.getItemInfo().getItemId());
	        po.setItemPrice((int)gameItemResp.getItemInfo().getItemPrice());
	        
//	        GetGameItemResponse itemResp = OpenApi.getProxy().queryOnlineGameItemInfo(currProductInfo.productId);
//	        if (itemResp == null || !itemResp.isSucceed())
//	        {
//	        	String msg = OpenApi.genOpenApiErrorMsg(itemResp);
//	        	result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
//	        	result.setMsg("获取网游商品信息失败[" + msg + "]");
//	        	return po;
//	        }
//	        po.setItemId(itemResp.itemId);
//	        po.setItemPrice(itemResp.itemPrice);
	        
	        // 检查sectionCode
	        SectionServer currSectionServer = checkGameSectionCode(result, po.getSectionServers(), sectionCode);
	        if (result.getErrCode() != 0) {
	        	return po;
	        }
	        po.setSectionCode(sectionCode);
	        po.setCurrSectionServer(currSectionServer);
	        Section currSection = currSectionServer == null ? null : currSectionServer.getSection();
			po.setCurrSectionName(currSection == null ? "" : currSection.getName());
	        
	        // 检查serverCode
			if (currSectionServer != null) {
		        Server currServer = checkGameServerCode(result, currSectionServer.getServerList(), serverCode);
		        if (result.getErrCode() != 0) {
		        	return po;
		        }
		        po.setServerCode(serverCode);
		        po.setCurrServer(currServer);
				po.setCurrServerName(currServer == null ? "" : currServer.getName());
			}
	        
	        return po;
		}
		
		return null;
	}
	
	public static SectionServer checkGameSectionCode(BasicResult result, List<SectionServer> sectionServers, String sectionCode) {
		if (result != null && sectionServers != null && sectionServers.size() > 0) {
			if (StringUtil.isEmpty(sectionCode)) {
				result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
				result.setMsg("请先输入网游区编码");
				return null;
			}
			SectionServer findSectionServer = null;
			for (SectionServer sectionServer : sectionServers) {
				Section section = sectionServer.getSection();
				if (section != null && sectionCode.equals(section.getCode())) {
					findSectionServer = sectionServer;
					break;
				}
			}
			if (findSectionServer == null) {
				result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
				result.setMsg("网游区编码输入不正确");
				return null;
			}
			return findSectionServer;
		}
		return null;
	}
	
	public static Server checkGameServerCode(BasicResult result, List<Server> servers, String serverCode) {
		if (result != null) {
			if (servers != null && servers.size() > 0) {
				Server findServer = null;
				for (Server server : servers) {
					if (!StringUtil.isEmpty(serverCode) && serverCode.equals(server.getCode())) {
						findServer = server;
						break;
					}
				}
				if (findServer == null) {
					result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
					result.setMsg("网游服务器编码输入不正确");
					return null;
				}
				return findServer;
			}
		}
		return null;
	}
	
	public static boolean checkOnlineGameCategory(BasicResult result, String category) {
		if (result != null) {
			List<String> cgs = Vb2cUtil.getGameCategories();
			return cgs.contains(category);
		}
		return true;
	}
	
	public static boolean checkOnlineGameAccount(BasicResult result, int gameId, String account, String confirmAccount) {
		if (result != null) {
			if (StringUtil.isEmpty(account)) {
				result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
				result.setMsg("网游帐号不能为空");
				return false;
			}
			String gameAccountValidateRet = GameCountValidator.validateGameAccount(gameId, account);
			if (!StringUtil.isEmpty(gameAccountValidateRet)) {
				result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
				result.setMsg(gameAccountValidateRet);
				return false;
			}
			if (!account.equals(confirmAccount)) {
				result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
				result.setMsg("网游帐号输入不一致");
				return false;
			}
		}
		return true;
	}
	
	public static boolean checkWarPermit(BasicResult result, int gameId, String wp, String wp1) {
		if (result != null) {
			if (isWOW(gameId)) {  //魔兽世界的gameID
				if (StringUtil.isEmpty(wp)) {
					result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
					result.setMsg("战网通行证不能为空");
					return false;
				}
				if (!wp.equals(wp1)) {
					result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
					result.setMsg("战网通行证输入不一致");
					return false;
				}
			}
		}
		return true;
	}
	
	/********************************************** QQ服务部分的参数校验 *******************************************/
	public static QQService4Chong checkQQServiceGameId(BasicResult result, int gameId) {
		if (result != null) {
			QQService4Chong qqService = QQService4Chong.findInPrimary(gameId);
			if (qqService == null) {
				result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
				result.setMsg("QQ服务编码不正确, gameId:" + gameId);
				return null;
			}
			return qqService;
		}
		return null;
	}
	
	public static ValidateQQServicePo checkQQServiceProduct(BasicResult result, int gameId, int amount, int buyCount) {
		ValidateQQServicePo po = new ValidateQQServicePo();
		po.setGameId(gameId);
		po.setAmount(amount);
		po.setBuyCount(buyCount);
		if (result != null) {
			// 检查gameId、amount参数
			QQService4Chong qqService = QQService4Chong.findInAll(gameId, amount);
			if (qqService == null) {
				result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
				result.setMsg("QQ服务编码或面额不正确, gameId:" + gameId + "; amount:" + amount);
				return po;
			}
			po.setQqService(qqService);
			
			// 检查buyCount参数
			if (buyCount < qqService.getMinBuyCount() || buyCount > qqService.getMaxBuyCount()) {
				result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
				result.setMsg("购买数量不正确，应该在" + qqService.getMinBuyCount() + "到" + qqService.getMaxBuyCount() + "之间");
				return po;
			}
			
			// 检查网游商品信息
			GetGameItemResp gameItemResp = Vb2cClient.queryGameItemInfo(qqService.getProductId());
	        if (gameItemResp == null || gameItemResp.getResult() != 0 || gameItemResp.getItemInfo() == null) {
	        	String msg = gameItemResp == null ? "gameItemResp为null" : "" + gameItemResp.getResult();
	        	result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
	        	result.setMsg("获取QQ服务商品信息失败[" + msg + "]");
	        	return po;
	        }
	        po.setItemId(gameItemResp.getItemInfo().getItemId());
//	        GetGameItemResponse itemResp = OpenApi.getProxy().queryOnlineGameItemInfo(qqService.getProductId());
//	        if (itemResp == null || !itemResp.isSucceed()) {
//	        	String msg = OpenApi.genOpenApiErrorMsg(itemResp);
//	        	result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
//	        	result.setMsg("获取QQ服务商品信息失败[" + msg + "]");
//	        	return po;
//	        }
//	        po.setItemId(itemResp.itemId);
			
		}
		return po;
	}
}
	
