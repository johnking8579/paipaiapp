package com.qq.qqbuy.vb2c.util;

import com.qq.qqbuy.common.ConfigHelper;
import com.qq.qqbuy.common.constant.PropertiesConstant;

/**
 * 用来分版本管理财付通的CallBack回调URL
 * @author winsonwu
 * @Created 2012-7-4
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class CftCallBackUrlFactory {
	
	protected static String DEFAULT_CFT_CALLBACK_URL = Vb2cConstant.VB2C_TENPAY_CALLBACK_DEFAULT_URL;
	
	public static String getCftCallBackUrl(VersionType type) {
		if (VersionType.VERSION_WAP2.equals(type)) {
			return ConfigHelper.getStringByKey(PropertiesConstant.VB2C_TENPAY_CALLBACK_URL_PREFIX_W, DEFAULT_CFT_CALLBACK_URL);
		} else if (VersionType.VERSION_TOUCH.equals(type)) {
			return ConfigHelper.getStringByKey(PropertiesConstant.VB2C_TENPAY_CALLBACK_URL_PREFIX_T, DEFAULT_CFT_CALLBACK_URL);
		} else if (VersionType.VERSION_ANDROID.equals(type)) {
			// no special call back url
		} else if (VersionType.VERSION_IOS.equals(type)) {
			// 暂时使用Wap站的CallBackUrl，只用于截获url并将充值是否成功的标识截取出来。会存在一些安全问题。 winson
			return ConfigHelper.getStringByKey(PropertiesConstant.VB2C_TENPAY_CALLBACK_URL_PREFIX_W, DEFAULT_CFT_CALLBACK_URL);
		}
		return DEFAULT_CFT_CALLBACK_URL;
	}

}
