package com.qq.qqbuy.vb2c.util;

import java.util.List;

import com.qq.qqbuy.vb2c.po.ValidateGamePo;
import com.qq.qqbuy.vb2c.po.game.SectionServer;
import com.qq.qqbuy.vb2c.po.game.Server;

/**
 * 负责网游详情多个页面之间Index的跳转管理
 * 1:面额选择页面
 * 2：充值类型选择页面
 * 3：区选择页面
 * 4：服务器选择页面
 * 5：数量、帐号选择页面
 * 6：所有信息展示页面
 * 7：充值跳转页面
 * 跳转图
 * 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 //区服信息都存在
 * 		  -> 3 -> 5                //只存在区信息
 *        -> 5                     //区服信息都不存在
 * @author winsonwu
 * @Created 2012-5-24
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class WGameDetailIndexer {
	
	public static final int WGAME_DETAIL_INDEX_AMOUNT = 1;
	public static final int WGAME_DETAIL_INDEX_CHARGETYPE = 2;
	public static final int WGAME_DETAIL_INDEX_SECTION = 3;
	public static final int WGAME_DETAIL_INDEX_SERVER = 4;
	public static final int WGAME_DETAIL_INDEX_BUYCOUNT = 5;
	public static final int WGAME_DETAIL_INDEX_ALLINFO = 6;
	public static final int WGAME_DETAIL_INDEX_CHARGE = 7;
	
//	public static PgID getQQServiceDetailPgId(int gameId) {
//		PgID sPgId = (gameId == QQService4Chong.QBi.getGameId()) ? PgID.VB2C_QQSERVICE_DETAIL_QBI : PgID.VB2C_QQSERVICE_DETAIL_BAOYUE;
//		return sPgId;
//	}
//	
//	public static PgID getGameDetailPgId(int index) {
//		switch (index) {
//		case WGAME_DETAIL_INDEX_AMOUNT:
//			return PgID.VB2C_GAME_DETAIL_AMOUNT;
//		case WGAME_DETAIL_INDEX_CHARGETYPE:
//			return PgID.VB2C_GAME_DETAIL_CHARGETYPE;
//		case WGAME_DETAIL_INDEX_SECTION:
//			return PgID.VB2C_GAME_DETAIL_SECTION;
//		case WGAME_DETAIL_INDEX_SERVER:
//			return PgID.VB2C_GAME_DETAIL_SERVER;
//		case WGAME_DETAIL_INDEX_BUYCOUNT:
//			return PgID.VB2C_GAME_DETAIL_ACCOUNT;
//		case WGAME_DETAIL_INDEX_ALLINFO:
//			return PgID.VB2C_GAME_DETAIL_CONFIRM;
//		}
//		return PgID.VB2C_GAME_FAIL;
//	}

	public static int toNext(int currIndex, ValidateGamePo gameInfo) {
		// winson，注意，此处是隔代计算的
		List<SectionServer> sectionServers = gameInfo.getSectionServers();
		switch (currIndex) {
		case WGAME_DETAIL_INDEX_AMOUNT:
			return WGAME_DETAIL_INDEX_CHARGETYPE;
		case WGAME_DETAIL_INDEX_CHARGETYPE:
			if (gameInfo != null) {
				if (sectionServers == null || sectionServers.size() <= 0) {
					return WGAME_DETAIL_INDEX_BUYCOUNT;
				}
			}
			return WGAME_DETAIL_INDEX_SECTION;
		case WGAME_DETAIL_INDEX_SECTION:
			if (gameInfo != null) {
				if (sectionServers != null && sectionServers.size() >= 1) {
					SectionServer firstSectionServer = sectionServers.get(0);
					List<Server> servers = firstSectionServer.getServerList();
					if (servers != null && servers.size() > 0) {
						return WGAME_DETAIL_INDEX_SERVER;
					}
				}
				return WGAME_DETAIL_INDEX_BUYCOUNT;
			}
		case WGAME_DETAIL_INDEX_SERVER:
			return WGAME_DETAIL_INDEX_BUYCOUNT;
		case WGAME_DETAIL_INDEX_BUYCOUNT:
			return WGAME_DETAIL_INDEX_ALLINFO;
		case WGAME_DETAIL_INDEX_ALLINFO:
			return WGAME_DETAIL_INDEX_CHARGE;
		}
		return -1;  
	}
	
	public static int toPre(int currIndex, ValidateGamePo gameInfo) {
		List<SectionServer> sectionServers = gameInfo.getSectionServers();
		switch (currIndex) {
		case WGAME_DETAIL_INDEX_ALLINFO:
			return WGAME_DETAIL_INDEX_BUYCOUNT;
		case WGAME_DETAIL_INDEX_BUYCOUNT:
			if (sectionServers == null || sectionServers.size() <= 0) {
				return WGAME_DETAIL_INDEX_CHARGETYPE;
			}
			SectionServer firstSectionServer = sectionServers.get(0);
			if (firstSectionServer != null) {
				List<Server> servers = firstSectionServer.getServerList();
				if (servers != null && servers.size() > 0) {
					return WGAME_DETAIL_INDEX_SERVER;
				}
			}
			return WGAME_DETAIL_INDEX_SECTION;
		case WGAME_DETAIL_INDEX_SERVER:
			return WGAME_DETAIL_INDEX_SECTION;
		case WGAME_DETAIL_INDEX_SECTION:
			return WGAME_DETAIL_INDEX_CHARGETYPE;
		case WGAME_DETAIL_INDEX_CHARGETYPE:
			return WGAME_DETAIL_INDEX_AMOUNT;
		}
		return -1;
	}
	
}
