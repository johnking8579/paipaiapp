package com.qq.qqbuy.vb2c.util;

import com.paipai.util.format.DateFormat;
import com.qq.qqbuy.common.ConfigHelper;
import com.qq.qqbuy.common.constant.PropertiesConstant;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.DealInfo;
import com.qq.qqbuy.vb2c.po.CftVb2cAttach;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author winsonwu
 * @Created 2012-4-18
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class TenpayVb2cUtil {
	
	// 此处的数量用于接收到财付通信息时候解析attach时候使用，如增加字段，注意修改·
	public static final int CFT_ATTACH_COMMON_COUNT = 6;
	public static final int CFT_ATTACH_MOBILE_COUNT = 4;
	public static final int CFT_ATTACH_GAME_COUNT = 12;
	public static final int CFT_ATTACH_QQSERVICE_COUNT = 6;
	
	public static CftVb2cAttach deSerializeCftAttach(String attach) {
		CftVb2cAttach po = new CftVb2cAttach();
		if (!StringUtil.isEmpty(attach)) {
			String[] attachs = attach.split("~");
			if (attachs != null && attachs.length >= CFT_ATTACH_COMMON_COUNT + 1) {  //+1是为了保证type的获取
				int type = StringUtil.toInt(attachs[0], -1);
				po.setType(type);
				// Common Attach部分：String sid, String pgId, String icfa, String o_icfa, String gcfa
				po.setVersionType(StringUtil.toInt(attachs[1], -1));
				po.setSid(attachs[2]);
				po.setPgId(attachs[3]);
				po.setIcfa(attachs[4]);
				po.setO_icfa(attachs[5]);
				po.setGcfa(attachs[6]);
				switch (type) {
				case Vb2cConstant.VB2C_TYPE_MOBILE:
					if (attachs.length == CFT_ATTACH_COMMON_COUNT + CFT_ATTACH_MOBILE_COUNT) {
						// Mobile Attach部分：long qq, int amount, String mobile
						po.setQq(StringUtil.toLong(attachs[CFT_ATTACH_COMMON_COUNT + 1], -1));
						int amount = StringUtil.toInt(attachs[CFT_ATTACH_COMMON_COUNT + 2], -1);
						po.setAmount(amount / 100); //此处的amount值采用分为单位，此处展示需要以元为单位。
						po.setMobile(attachs[CFT_ATTACH_COMMON_COUNT + 3]);
					}
					break;
				case Vb2cConstant.VB2C_TYPE_GAME:
					if (attachs.length == CFT_ATTACH_COMMON_COUNT + CFT_ATTACH_GAME_COUNT) {
						// Game Attach部分：long qq, int amount, int gameId, int buyCount, String account, 
						// String tGameName,  String chargeType, String tSectionName, String tServerName,  String sectionCode, String serverCode
						po.setQq(StringUtil.toLong(attachs[CFT_ATTACH_COMMON_COUNT + 1], -1));
						po.setAmount(StringUtil.toInt(attachs[CFT_ATTACH_COMMON_COUNT + 2], -1));
						po.setGameId(StringUtil.toInt(attachs[CFT_ATTACH_COMMON_COUNT + 3], -1));
						po.setBuyCount(StringUtil.toInt(attachs[CFT_ATTACH_COMMON_COUNT + 4], -1));
						po.setAccount(attachs[CFT_ATTACH_COMMON_COUNT + 5]);
						po.settGameName(attachs[CFT_ATTACH_COMMON_COUNT + 6]);
						po.setChargeType(attachs[CFT_ATTACH_COMMON_COUNT + 7]);
						po.settSectionName(attachs[CFT_ATTACH_COMMON_COUNT + 8]);
						po.settServerName(attachs[CFT_ATTACH_COMMON_COUNT + 9]);
						po.settSectionCode(attachs[CFT_ATTACH_COMMON_COUNT + 10]);
						po.settServerCode(attachs[CFT_ATTACH_COMMON_COUNT + 11]);
					}
					break;
				case Vb2cConstant.VB2C_TYPE_QQSERVICE:
					if (attachs.length == CFT_ATTACH_COMMON_COUNT + CFT_ATTACH_QQSERVICE_COUNT) {
						// QQService Attach部分：long qq, int amount, int gameId, int buyCount, String account
						po.setQq(StringUtil.toLong(attachs[CFT_ATTACH_COMMON_COUNT + 1], -1));
						int amount = StringUtil.toInt(attachs[CFT_ATTACH_COMMON_COUNT + 2], -1);
						po.setAmount(amount);
						int gameId = StringUtil.toInt(attachs[CFT_ATTACH_COMMON_COUNT + 3], -1);
						po.setGameId(gameId);
						po.setBuyCount(StringUtil.toInt(attachs[CFT_ATTACH_COMMON_COUNT + 4], -1));
						po.setAccount(attachs[CFT_ATTACH_COMMON_COUNT + 5]);
						
						// 设置QQ服务展示名称
						po.setQqServiceDisName(Vb2cUtil.genQQServiceDisName(gameId, amount));
					}
					break;
				}
			}
		}
		return po;
	}
	
	public static String genApiVb2cCommonCftAttach(boolean isIos) {
		StringBuffer sbf = new StringBuffer();
		sbf.append(isIos ? Vb2cConstant.VERSION_TYPE_IOS : Vb2cConstant.VERSION_TYPE_ANDROID);
		return sbf.toString();
	}
	
	/**
	 * @param sid wap2.0登录态
	 * @param pgId wap2.0日志统计，页面ID
	 * @param icfa wap2.0日志统计，内部渠道号
	 * @param o_icfa wap2.0日志统计，旧内部渠道号
	 * @param gcfa wap2.0日志统计，外部渠道号
	 */
	public static String genWap2Vb2cCommonCtfAttach(String sid, String pgId, String icfa, String o_icfa, String gcfa) {
		StringBuffer sbf = new StringBuffer();
		sbf.append(Vb2cConstant.VERSION_TYPE_WAP2);
		//sbf.append("~").append(handleWeiXinSid(sid));   // winsonwu: 此处微信等登录态过来存在特殊字符，但这个特殊字符在虚拟侧接受的时候存在问题。当存在特殊字符的时候，sid不传。
		sbf.append("~").append(sid);   // winsonwu: 此处微信等登录态过来存在特殊字符，但这个特殊字符在虚拟侧接受的时候存在问题。当存在特殊字符的时候，sid不传。
		sbf.append("~").append(pgId);
		sbf.append("~").append(StringUtil.defaultIfEmpty(icfa, "0"));
		sbf.append("~").append(StringUtil.defaultIfEmpty(o_icfa, "0"));
		sbf.append("~").append(StringUtil.defaultIfEmpty(gcfa, "0"));
		return sbf.toString();
	}
	
	private static String handleWeiXinSid(String sid) {
		if (sid != null && (sid.contains("%") || sid.startsWith("_*"))) {
			return "00";   //_*开头的是微信的sid，存在%则认为sid经过encode之后，存在特殊字符。直接不传递sid
		}
		return "";
	}
	
	/**
	 * 产生虚拟话费充值到财付通的商户信息
	 * @param commonAttach 公共的Attach信息@see appendCommonCtfAttach
	 * @param qq  充值QQ
	 * @param amount 充值面额
	 * @param mobile 充值手机号码 
	 * @return
	 */
	protected static String genWap2Vb2cMobilCtfAttach(String commonAttach, long qq, int amount, String mobile) {
		StringBuffer sbf = new StringBuffer();
		sbf.append(Vb2cConstant.VB2C_TYPE_MOBILE);
		sbf.append("~").append(commonAttach);
		sbf.append("~").append(qq);
		sbf.append("~").append(amount);
		sbf.append("~").append(mobile);
		return sbf.toString();
	}
	
	/**
	 * 产生虚拟QQ服务充值到财付通的商户信息
	 * @param commonAttach 公共的Attach信息@see appendCommonCtfAttach
	 * @param qq 充值QQ
	 * @param amount QQ服务面额
	 * @param gameId QQ服务对应的网游ID
	 * @param buyCount QQ服务购买数量
	 * @param account 充值帐号
	 * @return
	 */
	protected static String genWapVb2cQQServiceCftAttach(String commonAttach, long qq, int amount, int gameId, int buyCount, String account) {
		StringBuffer sbf = new StringBuffer();
		sbf.append(Vb2cConstant.VB2C_TYPE_QQSERVICE);
		sbf.append("~").append(commonAttach);
		sbf.append("~").append(qq);
		sbf.append("~").append(amount);
		sbf.append("~").append(gameId);
		sbf.append("~").append(buyCount);
		sbf.append("~").append(account);
		return sbf.toString();
	}
	
	/**
	 * 产生虚拟网游充值到财付通的商户信息
	 * @param commonAttach 公共的Attach信息@see appendCommonCtfAttach
	 * @param qq 充值QQ
	 * @param amount 网游面额
	 * @param gameId 网游ID
	 * @param buyCount 网游购买数量
	 * @param account 网游充值帐号
	 * @param tGameName 网游名称
	 * @param chargeType 网游充值类型：chargeType
	 * @param tSectionName 网游区中文名
	 * @param tServerName  网游服务器中文名
	 * @param sectionCode 网游区编码
	 * @param serverCode 网游服务器编码
	 * @return
	 */
	protected static String genWapVb2cGameCftAttach(String commonAttach, long qq, int amount, int gameId, int buyCount, String account, 
			String tGameName,  String chargeType, String tSectionName, String tServerName,  String sectionCode, String serverCode) {
		StringBuffer sbf = new StringBuffer();
		sbf.append(Vb2cConstant.VB2C_TYPE_GAME);
		sbf.append("~").append(commonAttach);
		sbf.append("~").append(qq);
		sbf.append("~").append(amount);
		sbf.append("~").append(gameId);
		sbf.append("~").append(buyCount);
		sbf.append("~").append(account);
		sbf.append("~").append(tGameName);
		sbf.append("~").append(chargeType);
		sbf.append("~").append(tSectionName);
		sbf.append("~").append(tServerName);
		sbf.append("~").append(sectionCode);
		sbf.append("~").append(serverCode);
		
		return sbf.toString();
	}
	
	public static Map<String, String> genParameter4Mobile(VersionType type, DealInfo dealDetail, String commonAttach, 
			long qq, int amount, String mobile) {
		String desc = "" + amount / 100 + "元话费充值"; // 商品描述
		String attach = genWap2Vb2cMobilCtfAttach(commonAttach, qq, amount, mobile);
		return genParameter(type, dealDetail, qq, desc, attach);
	}
	
	public static Map<String, String> genParamter4Game(VersionType type, DealInfo dealDetail, String commonAttach, 
			long qq, int amount, int gameId, int buyCount, String account, 
			String tGameName,  String chargeType, String tSectionName, String tServerName,  String sectionCode, String serverCode) {
		String desc = tGameName + "充值"; // 商品描述
		String attach = genWapVb2cGameCftAttach(commonAttach, qq, amount, gameId, buyCount, 
				account, tGameName, chargeType, tSectionName, tServerName, sectionCode, serverCode);
		return genParameter(type, dealDetail, qq, desc, attach);
	}
	
	public static Map<String, String> genParamter4QQService(VersionType type, DealInfo dealDetail, String commonAttach, 
			long qq, int amount, int gameId, int buyCount, String account, String tQQServiceName) {
		String desc = tQQServiceName + "充值"; //商品描述
		String attach = genWapVb2cQQServiceCftAttach(commonAttach, qq, amount, gameId, buyCount, account);
		return genParameter(type, dealDetail, qq, desc, attach);
	}
	
	protected static Map<String, String> genParameter(VersionType type, DealInfo dealDetail, long payQQ, String desc, String attach) {
		Map<String, String> mp = new HashMap<String, String>();
        String ver = "2.0"; // 版本号
        int charset = 1; // 代表采用UTF-8
        int bankType = 0; // 代表采用财付通
        
        String purchaserId = "" + payQQ; // 用户(买方)的财付通账户
//        String spBillno = dealDetail.dealId; // 商户系统内部的定单号
        String spBillno = dealDetail.getDealId();  //使用IDL方式
        // 订单生成时间，格式为yyyyMMddHHmmss
        String timeStart = DateFormat.formatDate(new Date(), "yyyyMMddHHmmss");
        // 订单失效时间，格式为yyyyMMddHHmmss，目前取值“半个小时”
        String timeExpire = DateFormat.formatDate(
                new Date(System.currentTimeMillis() + 1000 * 60 * 30),
                "yyyyMMddHHmmss");
        
        // 财付通Notify的URL
        String notifyUrl = ConfigHelper.getStringByKey(
				PropertiesConstant.VB2C_TENPAY_NOTIFY_URL_PREFIX,
				Vb2cConstant.VB2C_TENPAY_NOTIFY_DETAULT_URL);
        String callBackUrl = CftCallBackUrlFactory.getCftCallBackUrl(type);
        
		// 设置卖家商户ID
		// gateSpId = "1000000101";
        String spid = ConfigHelper.getStringByKey(PropertiesConstant.VB2C_GATE_SPID, "");
        
        mp.put("ver", ver);
        mp.put("charset", charset + "");
        mp.put("bank_type", bankType + "");
        mp.put("desc", desc);  // 商品描述
        mp.put("purchaser_id", purchaserId);
//        mp.put("bargainor_id", "" + dealDetail.sellerSpid); //商户号
        mp.put("bargainor_id", "" + dealDetail.getSellerSpid()); //使用IDL方式
        mp.put("transaction_id", spBillno);
        mp.put("sp_billno", spBillno);
//        mp.put("total_fee", dealDetail.payFee + ""); // 总金额，以分为单位
        mp.put("total_fee", dealDetail.getPayFee() + "");  //使用IDL方式
        mp.put("fee_type", "1"); // 现金支付币种，1代表人民币
        mp.put("notify_url", notifyUrl); // 接收财付通通知的URL,需给绝对路径，255字符内
        mp.put("callback_url", callBackUrl); // 交易完成后跳转的URL,需给绝对路径，255字符内
        mp.put("attach", attach);
        mp.put("time_start", timeStart);
        mp.put("time_expire", timeExpire);
        mp.put("sign_sp_id", spid);
        return mp;
	}

	public static Map<String, String> genParamMap(String ver, String charset, String payResult,
			String transactionId, String spBillno, String totalFee,
			String feeType, String bargainorId, String attach, String sign_sp_id) {
		// 校验sign值
        Map<String, String> param = new HashMap<String, String>();

        // 只有参数非空时才去检测
        if (StringUtil.isNotEmpty(ver))
            param.put("ver", ver);
        if (StringUtil.isNotEmpty(charset))
            param.put("charset", charset);
        if (StringUtil.isNotEmpty(payResult))
            param.put("pay_result", payResult);
        if (StringUtil.isNotEmpty(transactionId))
            param.put("transaction_id", transactionId);
        if (StringUtil.isNotEmpty(spBillno))
            param.put("sp_billno", spBillno);
        if (StringUtil.isNotEmpty(totalFee))
            param.put("total_fee", totalFee);
        if (StringUtil.isNotEmpty(feeType))
            param.put("fee_type", feeType);
        if (StringUtil.isNotEmpty(bargainorId))
            param.put("bargainor_id", bargainorId);
        if (StringUtil.isNotEmpty(attach))
            param.put("attach", attach);
        if (StringUtil.isNotEmpty(sign_sp_id)) {
        	param.put("sign_sp_id", sign_sp_id);
        }
        
        return param;
	}
	
	public static Map<String, String> genParamMap(String ver, String charset, String bankType,
			String bankBillno, String payResult, String payInfo,
			String purchaseAlias, String bargainorId, String transactionId,
			String spBillno, String totalFee, String feeType, String attach,
			String timeEnd, String signSpId) {
		// 校验sign值
        Map<String, String> param = new HashMap<String, String>();
        // 只有参数非空时才去检测
        if (StringUtil.isNotEmpty(ver))
            param.put("ver", ver);
        if (StringUtil.isNotEmpty(charset))
            param.put("charset", charset);
        if (StringUtil.isNotEmpty(bankType))
            param.put("bank_type", bankType);
        if (StringUtil.isNotEmpty(bankBillno))
            param.put("bank_billno", bankBillno);
        if (StringUtil.isNotEmpty(payResult))
            param.put("pay_result", payResult);
        if (StringUtil.isNotEmpty(payInfo))
            param.put("pay_info", payInfo);
        if (StringUtil.isNotEmpty(purchaseAlias))
            param.put("purchase_alias", purchaseAlias);
        if (StringUtil.isNotEmpty(bargainorId))
            param.put("bargainor_id", bargainorId);
        if (StringUtil.isNotEmpty(transactionId))
            param.put("transaction_id", transactionId);
        if (StringUtil.isNotEmpty(spBillno))
            param.put("sp_billno", spBillno);
        if (StringUtil.isNotEmpty(totalFee))
            param.put("total_fee", totalFee);
        if (StringUtil.isNotEmpty(feeType))
            param.put("fee_type", feeType);
        if (StringUtil.isNotEmpty(attach))
            param.put("attach", attach);
        if (StringUtil.isNotEmpty(timeEnd))
            param.put("time_end", timeEnd);
        if (StringUtil.isNotEmpty(signSpId)) {
        	param.put("sign_sp_id", signSpId);
        }
        
        return param;
	}
}
