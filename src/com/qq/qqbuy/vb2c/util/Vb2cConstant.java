package com.qq.qqbuy.vb2c.util;

/**
 * @author winsonwu
 * @Created 2012-4-18 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class Vb2cConstant {

	public static final String VB2C_TENPAY_NOTIFY_DETAULT_URL = "http://virtual.paipai.com/mobile/WirelessPaySuccess";
	public static final String VB2C_TENPAY_CALLBACK_DEFAULT_URL = "http://m.wanggou.com/w/vb2c/tenpayCallBack.xhtml";
	public static final String VB2C_TENPAY_DEFAULT_URL = "http://wap.tenpay.com/cgi-bin/wappayv2.0/wappay_gate.cgi";
	public static final int VB2C_TENPAY_NOTIFY_DEFAULT_RETRY_COUNTS = 5;
	public static final int DEFAULT_AMOUNT_FEN = 5000;

	public static final int VB2C_TYPE_MOBILE = 1;
	public static final int VB2C_TYPE_GAME = 2;
	public static final int VB2C_TYPE_QQSERVICE = 3;

	public static final int VERSION_TYPE_ANDROID = 1;
	public static final int VERSION_TYPE_WAP2 = 2;
	public static final int VERSION_TYPE_IOS = 3;

	// 查询手机号归属信息URL格式
	public static final String VB2C_GET_MOBILE_LOCATION_URL_FORMAT = "http://virtual.paipai.com/extinfo/GetMobileProductInfo?mobile=%s&amount=100";
	public static final int READTIMEOUT = 15000;// 读超时时间
    public static final int CONNECTTIMEOUT = 10000;// 连接超时时间
    public static final String DEFAULT_ENCODING = "gb2312";// 默认字符编码
}
