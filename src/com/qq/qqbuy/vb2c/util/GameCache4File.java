package com.qq.qqbuy.vb2c.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.vb2c.po.OnlineGame;
import com.qq.qqbuy.vb2c.po.OnlineGameList;

/**
 * 使用File来存储网游列表信息，存储文件见conf/res/gamelist.txt
 * 
 * @author winsonwu
 * @Created 2012-5-16 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class GameCache4File {

	private static final String gameListFileLocation = "/res/gamelist.txt";
	// 目前网游列表的Category为9、A-Z、CLIENT
	public static final String Category_9 = "9";
	public static final String Category_hotGame = "CLIENT";

	// 存放category-该category下的Category列表
	private static Map<String, List<OnlineGame>> onlineGames = null;

	protected static Map<String, List<OnlineGame>> getOnlineGamesMapNotNull() {
		if (onlineGames == null) {
			synchronized (GameCache4File.class) {
				if (onlineGames == null) {
					onlineGames = queryAllCategoryOnlineGames4File();
				}
			}
		}
		return onlineGames;
	}

	public static List<OnlineGame> getHotOnlineGames() {
		Map<String, List<OnlineGame>> map = getOnlineGamesMapNotNull();
		return map.get(Category_hotGame);
	}

	public static List<OnlineGame> getGamesByCategory(String categoryName) {
		Map<String, List<OnlineGame>> map = getOnlineGamesMapNotNull();
		return map.get(categoryName);
	}

	public static OnlineGame getOnlineGameById(int id) {
		Map<String, List<OnlineGame>> onlineGames = getOnlineGamesMapNotNull();
		Collection<List<OnlineGame>> gameLists = onlineGames.values();
		if (gameLists != null) {
			for (List<OnlineGame> games : gameLists) {
				for (OnlineGame game : games) {
					if (game != null && id == game.getId()) {
						return game;
					}
				}
			}
		}
		return null;
	}
	
	public static List<OnlineGameList> getOnlineGamesByFirstLetter() {
		List<OnlineGameList> gameLists = new ArrayList<OnlineGameList>();
        Map<String, List<OnlineGame>> onlineGames = getOnlineGamesMapNotNull();
        // 获取Category为9的OnlineGame列表
//        OnlineGameList gl = new OnlineGameList(Category_9,
//                onlineGames.get(Category_9));
//        gameLists.add(gl);

        // 获取Category为A~Z的OnlineGame列表
        for (char i = 'A'; i <= 'Z'; i++)
        {
            String category = "" + i;
            OnlineGameList ogame = new OnlineGameList(category, onlineGames
                    .get(category));
            gameLists.add(ogame);
        }
        return gameLists;
	}
	
	public static Map<String, List<OnlineGame>> queryAllCategoryOnlineGames4File() {
		Map<String, List<OnlineGame>> map = new HashMap<String, List<OnlineGame>>();
		BufferedReader br = null;
		try {
			File gameListFile = new File(GameCache4File.class.getResource(
					gameListFileLocation).getPath());
			br = new BufferedReader(new InputStreamReader(
					new FileInputStream(gameListFile), "gbk"));  //使用UTF-8编码文件，获取到的第一行的数据好像会乱码(第一个字符?) winson

			String data = null;
			while ((data = br.readLine()) != null) {
				String[] strs = data.split(",");
				if (strs != null && strs.length == 4) {
					OnlineGame game = new OnlineGame();
					String category = strs[0];
					game.setCategory(category);
					game.setId(StringUtil.toInt(strs[1], -1));
					game.setItemName(strs[2]);
					game.setName(strs[3]);
					List<OnlineGame> categoryGames = map.get(category);
					if (categoryGames == null) {
						categoryGames = new ArrayList<OnlineGame>();
						map.put(category, categoryGames);
					}
					if (categoryGames != null && !categoryGames.contains(game)) {
						categoryGames.add(game);
					}
				}
			}
		} catch (IOException e) {
			Log.run.warn("read game list info text file fail with exception!", e);
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// ignore
			}
		}
		return map;
	}

}
