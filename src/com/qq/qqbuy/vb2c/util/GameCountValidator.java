package com.qq.qqbuy.vb2c.util;

import java.util.regex.Pattern;

import com.qq.qqbuy.common.ConfigHelper;
import com.qq.qqbuy.common.constant.PropertiesConstant;
import com.qq.qqbuy.common.util.StringUtil;

/**
 * 网游账户的名称校验
 * @author winsonwu
 * @Created 2012-5-13
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class GameCountValidator {

	private static final String pattern_2220 = "^[\\w\\.@_\\*]+$";  //魔兽世界
	private static final String pattern_3218 = "^.+@.+$";  //星际争霸2
	private static final String pattern_tencent = "^[1-9]\\d{4,14}$";//"^\\d{4,15}$"; //腾讯游戏 edit by jaybai 2012-8-7 更换正则,以前的不能过滤0开通的
	private static final String pattern_sohu = "^.*@((changyou\\.com)|(game\\.sohu\\.com)|(sohu\\.com)|(chinaren\\.com)|(sogou\\.com)|(17173\\.com))$"; //搜狐游戏
	private static final String pattern_shengda = "^([\\w@\\.]+)|(.*\\.bnb1)$"; //盛大游戏
	private static final String pattern_wangyi = "^[\\w@\\._-]+$"; //网易游戏
	
	public static String validateGameAccount(int gameId, String account) {
		String pattern = getGameValidatePattern(gameId);
		if (!StringUtil.isEmpty(pattern)) {
			boolean result = Pattern.matches(pattern, account);
			return result ? null : "帐号输入不正确";
		}
		return null;
	}
	
	protected static String getGameValidatePattern(int gameId) {
		if (Vb2cChecker.isWOW(gameId)) {
			return pattern_2220;
		}
		if (Vb2cChecker.isStarCraft(gameId)) {
			return pattern_3218;
		}
		String tencentGameList = ConfigHelper.getStringByKey(PropertiesConstant.VB2C_GAME_LIST_TENCENT, "");
		if (tencentGameList.contains(";"+gameId+";")) {
			return pattern_tencent;
		}
		String sohuGameList = ConfigHelper.getStringByKey(PropertiesConstant.VB2C_GAME_LIST_SOHU, "");
		if (sohuGameList.contains(";"+gameId+";")) {
			return pattern_sohu;
		}
		String shengdaGameList = ConfigHelper.getStringByKey(PropertiesConstant.VB2C_GAME_LIST_SHENGDA, "");
		if (shengdaGameList.contains(";"+gameId+";")) {
			return pattern_shengda;
		}
		String wangyiGameList = ConfigHelper.getStringByKey(PropertiesConstant.VB2C_GAME_LIST_WANGYI, "");
		if (wangyiGameList.contains(";"+gameId+";")) {
			return pattern_wangyi;
		}
		return null;
	}
	
}
