package com.qq.qqbuy.shoppingaround.model;

public class CommonLikes {
	private Long id;
	private String itemid;
	private int itemtype;
	private int likesnum;
	
	
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getItemid() {
		return itemid;
	}
	public void setItemid(String itemid) {
		this.itemid = itemid;
	}
	public int getItemtype() {
		return itemtype;
	}
	public void setItemtype(int itemtype) {
		this.itemtype = itemtype;
	}
	public int getLikesnum() {
		return likesnum;
	}
	public void setLikesnum(int likesnum) {
		this.likesnum = likesnum;
	}
	
	
}
