package com.qq.qqbuy.shoppingaround.dao;

import java.util.List;

import com.qq.qqbuy.shoppingaround.common.CurrentPage;
import com.qq.qqbuy.shoppingaround.model.Comment;
import com.qq.qqbuy.shoppingaround.model.CommonLikes;
import com.qq.qqbuy.shoppingaround.model.LikeInfo;


/**
 * 
 * @ClassName: ShoppingAroundDao 
 * @Description:购物圈数据库操作接口
 * @author lhn
 * @date 2015-3-4 下午3:58:37 
 *
 */
public interface ShoppingAroundDao {
	/**
	 * @param wid 
	 * 
	 * @Title: getLikeInfo 
	 * @Description: 根据itemid以及itemtype来查出个人点赞记录 
	 * @param @param itemid
	 * @param @param itemtype
	 * @param @return    设定文件 
	 * @return LikeInfo    返回类型 
	 * @throws
	 */
	LikeInfo getLikeInfo(String itemid, int itemtype, Long wid);
	/**
	 * 
	 * @Title: getCommonLikes 
	 * @Description: 根据itemid以及itemtype来查出点赞信息 
	 * @param @param itemid
	 * @param @param itemtype
	 * @param @return    设定文件 
	 * @return CommonLikes    返回类型 
	 * @throws
	 */
	CommonLikes getCommonLikes(String itemid, int itemtype);
	/**
	 * @param wid 
	 * 
	 * @Title: InsertCommonLikes 
	 * @Description: 插入一条点赞记录
	 * @param @param itemid
	 * @param @param itemtype    设定文件 
	 * @return void    返回类型 
	 * @throws
	 */
	void InsertCommonLikes(String itemid, int itemtype,int num, Long wid);
	/**
	 * @param cz 
	 * 
	 * @Title: UpdateCommonLikes 
	 * @Description: 更新点赞记录
	 * @param @param itemid
	 * @param @param itemtype
	 * @param @param i    设定文件 
	 * @return void    返回类型 
	 * @throws
	 */
	void UpdateCommonLikes(String itemid, int itemtype, int i,Long wid, int status);
	/**
	 * 
	 * @Title: getCommentList 
	 * @Description: 获取员工列表 
	 * @param @param itemid
	 * @param @param itemtype
	 * @param @param pagenum
	 * @param @param pagesize
	 * @param @return    设定文件 
	 * @return List<Comment> 评论列表
	 * @throws
	 */
	CurrentPage<Comment> getCommentList(String itemid, int itemtype, int pagenum,
			int pagesize);
	
}
