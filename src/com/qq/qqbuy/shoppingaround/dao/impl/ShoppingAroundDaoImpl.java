package com.qq.qqbuy.shoppingaround.dao.impl;

import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Transactional;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.shoppingaround.common.CurrentPage;
import com.qq.qqbuy.shoppingaround.dao.ShoppingAroundDao;
import com.qq.qqbuy.shoppingaround.model.Comment;
import com.qq.qqbuy.shoppingaround.model.CommonLikes;
import com.qq.qqbuy.shoppingaround.model.LikeInfo;
import com.qq.qqbuy.virtualgoods.action.TimeUtils;

@Transactional
public class ShoppingAroundDaoImpl implements ShoppingAroundDao {
	private JdbcTemplate  shoppingAroundJdbcTemplate;

	
	@Override
	public  LikeInfo getLikeInfo(String itemid, int itemtype,Long wid) {
		List<LikeInfo> list = shoppingAroundJdbcTemplate.query(
				"select * from pp_shoping_likeinfo where itemid= ? and itemtype=? and wid=?",
				new Object[] { itemid, itemtype,wid}, new LikeinfoPoRowMapper());
		if(list==null){
			return null;
		}else if(list!=null&&list.size()>0) {
			return list.get(0);
		}else {
			return null;
		}
		
	}
	
	
	class LikeinfoPoRowMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int index) throws SQLException {
			LikeInfo likeinfo = new LikeInfo();
			likeinfo.setId(rs.getLong("id"));
			likeinfo.setItemid(rs.getString("itemid"));
			likeinfo.setItemtype(rs.getInt("itemtype"));
			likeinfo.setLikenum(rs.getInt("likenum"));
			likeinfo.setLiketime(rs.getLong("liketime"));
			likeinfo.setStatus(rs.getInt("status"));
			likeinfo.setWid(rs.getLong("wid"));
			return likeinfo;
		}
	}
	class CommonLikesPoRowMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int index) throws SQLException {
			CommonLikes commonLikes = new CommonLikes();
			commonLikes.setId(rs.getLong("id"));
			commonLikes.setItemid(rs.getString("itemid"));
			commonLikes.setItemtype(rs.getInt("itemtype"));
			commonLikes.setLikesnum(rs.getInt("likesnum"));
			return commonLikes;
		}
	}	
	
	class CommentPoRowMapper implements RowMapper{
		public Object mapRow(ResultSet rs, int index) throws SQLException {
			Comment comment = new Comment();
			comment.setId(rs.getLong("id"));
			comment.setItemid(rs.getString("itemid"));
			comment.setItemtype(rs.getInt("itemtype"));
			comment.setCommenttime(rs.getLong("commenttime"));
			comment.setTitle(rs.getString("title"));
			comment.setContext(rs.getString("context"));
			comment.setIP(rs.getString("IP"));
			comment.setIsshow(rs.getInt("isshow"));
			comment.setWid(rs.getLong("wid"));
			comment.setNickname(rs.getString("nickname"));
			comment.setHead(rs.getString("head"));
			comment.setUsertype(rs.getInt("usertype"));
			return comment;
			
		}
	}
	@Override
	public CommonLikes getCommonLikes(String itemid, int itemtype) {
		List<CommonLikes> list = shoppingAroundJdbcTemplate.query("select * from pp_shoping_item where itemid=? and itemtype=?", new Object[]{itemid,itemtype}, new CommonLikesPoRowMapper());
		if(list==null){
			return null;
		}else if(list!=null&&list.size()>0) {
			return list.get(0);
		}else {
			return null;
		}
	}

	@Override
	public void InsertCommonLikes(String itemid, int itemtype,int num,Long wid) {
				this.shoppingAroundJdbcTemplate.update("INSERT INTO pp_shoping_item(itemid,itemtype,likesnum) values(?,?,?)", new Object[]{itemid,itemtype,num});
				this.shoppingAroundJdbcTemplate.update("insert into pp_shoping_likeinfo(itemtype,liketime,wid,itemid,likenum,status) values(?,?,?,?,?,?)",new Object[]{itemtype,TimeUtils.getCurrentTime()*1000,wid,itemid,1,1});
		}


	@Override
	public void UpdateCommonLikes(String itemid, int itemtype, int num,Long wid,int status) {
		this.shoppingAroundJdbcTemplate.update("update  pp_shoping_item set likesnum=? where itemid=? and itemtype=?", new Object[]{num,itemid,itemtype});
		if(status==0){
			this.shoppingAroundJdbcTemplate.update("update  pp_shoping_likeinfo set stattus=? where itemid=? and itemtype=? and wid=?", new Object[]{status,itemid,itemtype,wid});
		}else {
			this.shoppingAroundJdbcTemplate.update("update  pp_shoping_likeinfo set likesnum=likesnum+1,liketime=? stattus=? where itemid=? and itemtype=? and wid=?", new Object[]{TimeUtils.getCurrentTime()*1000,status,itemid,itemtype,wid});
		}
	}


	@Override
	public CurrentPage<Comment> getCommentList(String itemid, int itemtype,
			int pagenum, int pagesize) {
		Log.run.info("-------getCommentList------");
		CurrentPage<Comment> currentPage = new CurrentPage<Comment>();
		//查出一共多少数
		int count = shoppingAroundJdbcTemplate.queryForInt("select count(*) from pp_shoping_comment");
		//查出一共多少页
		int totalnum = count%pagesize==0?count/pagesize:(count/pagesize+1);
		//获取请求分页的sql
		String sql  = " select * from pp_shoping_comment where itemid=? and itemtype=? limit "+((pagenum-1)*pagesize)+","+pagesize;
		Log.run.info("内容：---"+count+","+totalnum+","+sql);
		//执行所获取到的分页sql
		List<Comment> comments = shoppingAroundJdbcTemplate.query(sql, new Object[]{itemid,itemtype}, new CommentPoRowMapper());
		currentPage.setPageItems(comments);
		currentPage.setPagenum(pagenum);
		currentPage.setTotalnum(totalnum);
		currentPage.setPagesize(pagesize);
		currentPage.setTotalcount(count);
		return currentPage;
	}
	
	
	
	
	
	//-------------------get set ------------------------------------
	public JdbcTemplate getShoppingAroundJdbcTemplate() {
		return shoppingAroundJdbcTemplate;
	}

	public void setShoppingAroundJdbcTemplate(
			JdbcTemplate shoppingAroundJdbcTemplate) {
		this.shoppingAroundJdbcTemplate = shoppingAroundJdbcTemplate;
	}


}
