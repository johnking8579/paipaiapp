package com.qq.qqbuy.shoppingaround.biz.impl;

import java.util.List;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.shoppingaround.biz.ShoppingAroundBiz;
import com.qq.qqbuy.shoppingaround.common.CurrentPage;
import com.qq.qqbuy.shoppingaround.dao.ShoppingAroundDao;
import com.qq.qqbuy.shoppingaround.model.Comment;
import com.qq.qqbuy.shoppingaround.model.CommonLikes;
import com.qq.qqbuy.shoppingaround.model.LikeInfo;
import com.qq.qqbuy.virtualgoods.action.TimeUtils;
/**
 * 
 * @ClassName: ShoppingAroundBizImpl 
 * @Description:实现 
 * @author lhn
 * @date 2015-3-4 下午3:49:59 
 *
 */
public class ShoppingAroundBizImpl implements ShoppingAroundBiz {
	private ShoppingAroundDao shoppingAroundDao;
	private static final int LIMIT_TIME = 24;//24小时内不能再点赞了
	

	@Override
	public String setLike(String itemid, int itemtype,Long wid) {
		
		Log.run.info("-----------点赞结束记录setLike begin----------");
		String msgstatus="0";
		//首先根据iD和类型来查出该人点赞记录
		LikeInfo likeinf = this.shoppingAroundDao.getLikeInfo(itemid,itemtype,wid);
		Log.run.info("-----------点赞结束记录setLike likeinf----------"+likeinf);
		if(likeinf==null){//如果为空则表示没有点过赞则进行点赞
			//点赞前先查一下是否已经有数据。如果没有则插入，如果有更新(理论上这个一定为null 因为没有点过赞)
			CommonLikes commonLikes = this.shoppingAroundDao.getCommonLikes(itemid,itemtype);
			Log.run.info("-----------点赞结束记录setLike commonLikes----------"+commonLikes);
					if(commonLikes==null){//如果为空则表示从来没有点过赞。则插入一条数据并记录
						this.shoppingAroundDao.InsertCommonLikes(itemid,itemtype,1,wid);
						msgstatus="10";
					}else {//如果不为空则更新增加一
						this.shoppingAroundDao.UpdateCommonLikes(itemid,itemtype,commonLikes.getLikesnum()+1,wid,1);
						//同时更新个人记录
						msgstatus="11";
					}
					
		}else {
			//如果有点赞记录。则再判断该人是否在限制时间内
			long limittime = likeinf.getLiketime()/1000;
			long nowtime = TimeUtils.getCurrentTime();
			if((nowtime>=limittime+LIMIT_TIME*60*60&&likeinf.getStatus()==1)||likeinf.getStatus()==0){
				//可以点赞
				//点赞前先查一下是否已经有数据。如果没有则插入，如果有更新(理论上一定不为null，因为点过赞)
				CommonLikes commonLikes = this.shoppingAroundDao.getCommonLikes(itemid,itemtype);
				Log.run.info("-----------点赞结束记录setLike commonLikes2----------"+commonLikes);
						if(commonLikes==null){//如果为空则表示从来没有点过赞。则插入一条数据
							this.shoppingAroundDao.InsertCommonLikes(itemid,itemtype,1,wid);
							msgstatus="21";
						}else {//如果不为空则更新增加一
							this.shoppingAroundDao.UpdateCommonLikes(itemid,itemtype,commonLikes.getLikesnum()+1,wid,1);
							msgstatus="20";
						}
			}else {//点赞过多，请LIMIT_TIME小时后再来吧
				msgstatus = "110";
			}
		}
		return msgstatus;
	}

	@Override
	public String cancelLike(String itemid, int itemtype,Long wid) {
		String msgstatus="0";
		Log.run.info("---------------cancelLike---------");
		//首先根据iD和类型来查出该人点赞记录
		LikeInfo likeinfo = this.shoppingAroundDao.getLikeInfo(itemid,itemtype,wid);
		if(likeinfo!=null){
			//如果有则就去取消
			//取消点赞前先查一下是否已经有数据。理论上肯定有
			CommonLikes commonLikes = this.shoppingAroundDao.getCommonLikes(itemid,itemtype);
			Log.run.info("-----------点赞结束记录setLike commonLikes----------"+commonLikes);
					if(commonLikes==null){//如果为空则表示从来没有点过赞。则插入一条数据并记录
						msgstatus="110";
					}else {//如果不为空则更新增加一
						this.shoppingAroundDao.UpdateCommonLikes(itemid,itemtype,commonLikes.getLikesnum()-1,wid,0);
						//同时更新个人记录
						msgstatus="10";
					}
		}else {
			//如果没有。则直接返回未找到点赞计录无法取消
			msgstatus = "110";
		}
		return msgstatus;
	}
	@Override
	public String getLikeNum(String itemid, int itemtype) {
		CommonLikes commonLikes = this.shoppingAroundDao.getCommonLikes(itemid,itemtype);
		if(commonLikes!=null){
			return commonLikes.getLikesnum()+"";
		}else {
			return "0";
		}
	}

	@Override
	public CurrentPage<Comment> getCommentList(String itemid, int itemtype,
			int pagenum, int pagesize) {
		
		return this.shoppingAroundDao.getCommentList(itemid,itemtype,pagenum,pagesize);
	}

	
	public ShoppingAroundDao getShoppingAroundDao() {
		return shoppingAroundDao;
	}


	public void setShoppingAroundDao(ShoppingAroundDao shoppingAroundDao) {
		this.shoppingAroundDao = shoppingAroundDao;
	}



}
