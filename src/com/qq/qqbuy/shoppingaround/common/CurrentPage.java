package com.qq.qqbuy.shoppingaround.common;

import java.util.ArrayList;
import java.util.List;

import com.paipai.lang.int64;

/**
 * 
 * @ClassName: CurrentPage 
 * @Description: 统用的 页面类
 * @author lhn
 * @date 2015-3-10 上午10:49:02 
 * 

 * @param <E>
 */
public class CurrentPage<E> {
    private int pagenum;//当前页数
    private int pagesize;//一页多少条
    private int totalnum;//总页数
    private int totalcount;//总的个数
    private List<E> pageItems = new ArrayList<E>();//数据
	public int getPagenum() {
		return pagenum;
	}
	public void setPagenum(int pagenum) {
		this.pagenum = pagenum;
	}
	public int getPagesize() {
		return pagesize;
	}
	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}
	public int getTotalnum() {
		return totalnum;
	}
	public void setTotalnum(int totalnum) {
		this.totalnum = totalnum;
	}
	public List<E> getPageItems() {
		return pageItems;
	}
	public void setPageItems(List<E> pageItems) {
		this.pageItems = pageItems;
	}
	public int getTotalcount() {
		return totalcount;
	}
	public void setTotalcount(int totalcount) {
		this.totalcount = totalcount;
	}
    
}
