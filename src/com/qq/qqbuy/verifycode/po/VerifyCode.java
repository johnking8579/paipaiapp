package com.qq.qqbuy.verifycode.po;

/**
 * 
 * VerifyCode的一些属性信息
 * 
 * @author wendyhu
 *
 */
public class VerifyCode {		
	public long uin;//用户uin
	public String lskey  = "";//用户登陆返回的token信息
	public String mobilekey = "";//用户登陆的手机信息
	public String clientIp = ""; //用户登陆的机器ip
	public int capType; //验证码的难度,目前只有0,1,2,3
	public int picType; //图片类型(0:jpg 1:png 2:gif)
	public long appId  ; //业务类型
	public long time = System.currentTimeMillis(); //当前时间
	public String reserve  = ""; //保留字段
	public long getUin() {
		return uin;
	}
	public void setUin(long uin) {
		this.uin = uin;
	}
	public String getLskey() {
		return lskey;
	}
	public void setLskey(String lskey) {
		if(lskey == null)
			return;
		this.lskey = lskey;
	}
	public String getMobilekey() {
		return mobilekey;
	}
	public void setMobilekey(String mobilekey) {
		if(mobilekey == null)
			return;
		this.mobilekey = mobilekey;
	}
	public String getClientIp() {
		return clientIp;
	}
	public void setClientIp(String clientIp) {
		if(clientIp == null)
			return;
		this.clientIp = clientIp;
	}
	public int getCapType() {
		return capType;
	}
	public void setCapType(int capType) {
		this.capType = capType;
	}
	public int getPicType() {
		return picType;
	}
	public void setPicType(int picType) {
		this.picType = picType;
	}
	public long getAppId() {
		return appId;
	}
	public void setAppId(long appId) {
		this.appId = appId;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	public String getReserve() {
		return reserve;
	}
	public void setReserve(String reserve) {
		if(reserve == null)
			return;
		this.reserve = reserve;
	}
	
	public boolean isValid(){
		if( uin< 10000
				|| capType <0
				|| picType< 0)
			return false;
		return true;
	}
	public String toString(){
		StringBuilder  buffer = new StringBuilder();
		buffer.append("uin=" + uin);
		buffer.append("&lskey=" + lskey);
		buffer.append("&mobilekey=" + mobilekey);
		buffer.append("&clientIp=" + clientIp);
		buffer.append("&capType=" + capType);
		buffer.append("&picType=" + picType);
		buffer.append("&appId=" + appId);
		buffer.append("&time=" + time);
		buffer.append("&reserve=" + reserve);
		
		return buffer.toString();
	}
}
