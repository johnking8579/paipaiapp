package com.qq.qqbuy.verifycode;

public class Constants {
	
	public static final String CONFIG_FILE_HOST_KEY = "verify_host"; //获取验证码的url
	public static final String VERIFYCODE_URL_PARAM_MSG = "?msg="; //验证码的url参数
	public static final String VERIFYCODE_URL_PARAM_SIGN = "&sign="; //验证码的url参数
	
	/**
	 * 规则
	 */
	public static final int NO_RULE = 0;
	
	public static final int QQVIP_RULE = 1;//会员
	public static final int CAIZUAN_RULE = 2;//彩钻
	public static final int HONGZUAN_RULE = 3;//红钻
	public static final int HUANGZUAN_RULE = 4;//黄钻
	public static final int LUZUAN_RULE = 5;//绿钻
	public static final int LANZUAN_RULE = 6;//蓝钻
	public static final int NEWFISH_RULE = 7;//新人
	public static final int SUPPERQ_RULE = 8;//超级qq
	public static final int LV3_RULE = 9;//彩钻LV3或会员
	

}
