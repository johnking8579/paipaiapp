package com.qq.qqbuy.verifycode;

import com.paipai.util.encrypt.MD5Util;
import com.qq.qqbuy.common.util.url.Base64Util;
import com.qq.qqbuy.verifycode.po.VerifyCode;

public class VerifyCodeHelper {
	private String hostUrl;//验证码的url
	private String magicStr;//魔术字符
	
	public String getHostUrl() {
		return hostUrl;
	}

	public void setHostUrl(String hostUrl) {
		this.hostUrl = hostUrl;
	}

	/**
	 * 获取验证码的url
	 * 
	 * @param verifyCode 验证码url的一些参数
	 * @return
	 */
	public String getVerifyCodeUrl(VerifyCode verifyCode){
		String urlParam = verifyCode.toString();
		String sign = MD5Util.encrypt(urlParam + magicStr);
		return hostUrl + Base64Util.encode(urlParam + Constants.VERIFYCODE_URL_PARAM_SIGN+ sign) ;
	}
	
	public String getMagicStr() {
		return magicStr;
	}

	public void setMagicStr(String magicStr) {
		this.magicStr = magicStr;
	}

	/**
	 * 检查验证码的url是否正确
	 * 
	 * @param url 验证码的url
	 *  
	 * @return
	 */
	public boolean checkVerifyCodeUrl(String url){
		
		if( url == null || url.trim().isEmpty() || url.trim().indexOf(hostUrl)!= 0){
			return false;
		}
		
		String param = Base64Util.decode(url.trim().substring(hostUrl.length()));
		int indexPos = param.indexOf( Constants.VERIFYCODE_URL_PARAM_SIGN);
		if(indexPos <0){
			return false;
		}
		
		String urlParam = param.substring(0,indexPos);
		String sign = param.substring(indexPos + Constants.VERIFYCODE_URL_PARAM_SIGN.length());
		if( MD5Util.encrypt(urlParam + magicStr).equals(sign))
			return true;
		return false;
	}
	
	/**
	 * 检查验证码的url中的参数是否正确
	 * 
	 * @param param url中的参数
	 * @return
	 */
	public boolean checkVerifyCodeUrlParam(String param){
		
		if( param == null || param.trim().isEmpty()){
			return false;
		}
		param = Base64Util.decode(param);
		int indexPos = param.indexOf( Constants.VERIFYCODE_URL_PARAM_SIGN);
		if(indexPos <0){
			return false;
		}	
		String urlParam = param.substring(0,indexPos);
		String sign = param.substring(indexPos + Constants.VERIFYCODE_URL_PARAM_SIGN.length());
		if( MD5Util.encrypt(urlParam + magicStr).equals(sign))
			return true;
		return false;
	}
	
	public String getVerifyCodeParam(String param){
		
		if( param == null || param.trim().isEmpty()){
			return null;
		}
		
		param = Base64Util.decode(param);
		int indexPos = param.indexOf( Constants.VERIFYCODE_URL_PARAM_SIGN);
		if(indexPos <0){
			return null;
		}
		
		String urlParam = param.substring(0,indexPos);
		String sign = param.substring(indexPos + Constants.VERIFYCODE_URL_PARAM_SIGN.length());
		if( MD5Util.encrypt(urlParam + magicStr).equals(sign))
			return param;
		
		return null;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		VerifyCodeHelper help = new VerifyCodeHelper();
		help.setHostUrl("http://qqbuy.qq.com/verifycode?msg=");
		help.setMagicStr("6&**#@$)m");
		VerifyCode verifyCode = new VerifyCode();
		verifyCode.setAppId(234);
		verifyCode.setCapType(0);
		String url =help.getVerifyCodeUrl(verifyCode);
		System.out.println("url:"+ url);
		System.out.println(help.checkVerifyCodeUrl(url));
		

	}

}
