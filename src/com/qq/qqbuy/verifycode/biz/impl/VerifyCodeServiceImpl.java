package com.qq.qqbuy.verifycode.biz.impl;

import java.util.Hashtable;

import org.apache.commons.lang.StringUtils;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.po.LoginResult;
import com.qq.qqbuy.common.po.QQBuyServiceComm;
import com.qq.qqbuy.common.util.HttpParamUtil;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.thirdparty.idl.login.LoginClient;
import com.qq.qqbuy.thirdparty.idl.login.protocol.CheckLoginByWgUidResp;
import com.qq.qqbuy.thirdparty.idl.userinfo.UserInfoClient;
import com.qq.qqbuy.thirdparty.idl.verifycode.VerifyCodeClient;
import com.qq.qqbuy.thirdparty.idl.verifycode.protocol.CheckLoginReq;
import com.qq.qqbuy.thirdparty.idl.verifycode.protocol.CheckLoginResp;
import com.qq.qqbuy.verifycode.biz.VerifyCodeService;

public class VerifyCodeServiceImpl implements
        VerifyCodeService
{
    /** 对象创建计数 **/
    private static int refCount = 0;

    private long appId = 1;

    /** socket 连接超时设置 **/
    private int connectTimeout = 5000;

    /** socket 读取超时设置 **/
    private int timeout = 3000;

    private String serviceName = "qgo.sys.loginsvc";

    /** 验证码可以使用的时间,最大不超过1200秒 */
    private int validTime = 300;

    public String getServiceName()
    {
        return serviceName;
    }

    public int getValidTime()
    {
        return validTime;
    }

    public void setValidTime(int validTime)
    {
        this.validTime = validTime;
    }

    public void setServiceName(String serviceName)
    {
        this.serviceName = serviceName;
    }

    public int getConnectTimeout()
    {
        return connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout)
    {
        this.connectTimeout = connectTimeout;
    }

    public int getTimeout()
    {
        return timeout;
    }

    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
    }

    public String getBusinesModule()
    {
//        String packageAndClass = getClass().getName();
//        return LoggerUtil.getBusiModuleInPackage(packageAndClass, logger);
    	return getClass().getName();
    }

    public VerifyCodeServiceImpl()
    {
        refCount++;
    }

    protected void finalize()
    {
        refCount--;
        try
        {
            super.finalize();
        } catch (Throwable e)
        {
        	e.printStackTrace();
        }
    }

    public boolean CheckLoginApp(long uid ,String skey)
    {
        return VerifyCodeClient.INSTANCE.CheckLoginApp( uid, skey, serviceName,
                connectTimeout, connectTimeout);
    }
    public boolean CheckLogin(CheckLoginReq req, CheckLoginResp resp)
    {
        return VerifyCodeClient.INSTANCE.CheckLogin(req, resp, serviceName,
                connectTimeout, connectTimeout);
    }
    @Override
    public String getSkey(long uin, String authCode, String skey, String mk, int type)
    {
        return VerifyCodeClient.INSTANCE.genSkey(uin, authCode, skey, mk,type);
    }

    // public boolean CheckVerifyCode(CheckVerifyCodeReq req,
    // CheckVerifyCodeResp resp)
    // {
    // return VerifyCodeClient.INSTANCE.CheckVerifyCode(req, resp, appId,
    // serviceName,
    // connectTimeout, connectTimeout);
    // }
    //
    // public boolean GetVerifyCode(GetVerifyCodeReq req, GetVerifyCodeResp
    // resp)
    // {
    // return VerifyCodeClient.INSTANCE.GetVerifyCode(req, resp, appId,
    // serviceName,
    // connectTimeout, connectTimeout);
    // }
    //    

    // public boolean getVerifyCode(GetVerifyCodePo verifyCodePo) {
    // GetVerifyCodeReq req = new GetVerifyCodeReq();
    // req.setCommParm(verifyCodePo.getCommParm());
    // req.setCapType(verifyCodePo.getCapType());
    // req.setPicType(verifyCodePo.getPicType());
    // GetVerifyCodeResp resp = new GetVerifyCodeResp();
    // boolean ret = GetVerifyCode(req, resp);
    // logger.log(getBusinesModule(), "ret:" + ret + " req:" + req + " resp:" +
    // resp);
    // if (ret)
    // {
    // verifyCodePo.setVerifyCodePic(resp.getPic());
    // verifyCodePo.setSignature(resp.getSig());
    // }
    // return ret;
    // }

    // public boolean checkLogin(QQBuyServiceComm serviceCommBean)
    // {
    // long start = System.currentTimeMillis();
    // CheckLoginReq req = new CheckLoginReq();
    // req.setCommParm(serviceCommBean);
    // CheckLoginResp resp = new CheckLoginResp();
    // resp.setResult(-7);
    // boolean ret = CheckLogin(req, resp);
    // serviceCommBean.setRetMsg(resp.getRetMsg());
    // serviceCommBean.setA8(resp.getA8());
    // serviceCommBean.setCrtTime(resp.getCrtTime());
    // long timeCost = System.currentTimeMillis() - start;
    // String info = "VerifyCodeServiceImpl#checkLogin" + '\t' +
    // resp.getResult() + "\t" + timeCost
    // + "ms";
    // Log.run.debug("checkLogin: req:" + req.toString() + " resp:"
    // +resp.toString() + " startTime:" + start + " costTime:" + timeCost);
    // return ret;
    // }

//    @Override
//    public int getObjCount()
//    {
//        return refCount;
//    }

    public long getAppId()
    {
        return appId;
    }

    public void setAppId(long appId)
    {
        this.appId = appId;
    }

    @Override
    public LoginResult checkLoginForPt(long uin, String clientIp, String skey,
            String lskey)
    {
    	Log.run.info("===========checkLoginForPt================uin========="+uin+"====clientIp:"+clientIp+"====lskey:"+lskey);
        LoginResult result = new LoginResult();
        long start = System.currentTimeMillis();
        if (uin < 10000
                || ( StringUtils.isEmpty(lskey))
                || StringUtils.isEmpty(clientIp))
            return result;
        //2、调用后台的验证登陆的接口
        boolean ret = CheckLoginApp( uin,lskey);
        if (ret == true){
            result.setRetCode(0);
            result.setRetMsg("");
       }
        return result;
    }

    @Override
    public LoginResult checkLoginForSid(String clientIp, String sid,
            String statSid, String sidFromCookie, String sidFromQCookie)
    {
        LoginResult result = new LoginResult();

        if (StringUtils.isEmpty(sid) || StringUtils.isEmpty(clientIp))
            return result;
        
        // 1、初使化参数
        long start = System.currentTimeMillis();
        CheckLoginReq req = new CheckLoginReq();
        QQBuyServiceComm serviceCommBean = new QQBuyServiceComm();
        serviceCommBean.setSid(sid);
        serviceCommBean.setIp(clientIp);
        serviceCommBean.setStatSid(statSid);
        serviceCommBean.setSidFromCookie(sidFromCookie);
        serviceCommBean.setSidFromQCookie(sidFromQCookie);
        req.setCommParm(serviceCommBean);
        CheckLoginResp resp = new CheckLoginResp();
        resp.setResult(-7);
        
        //2、调用后台的验证登陆的接口
        CheckLogin(req, resp);
        if (resp != null)
        {
            //3.1 解析出用户uin和返回码
            String retMsg = resp.getRetMsg();
            if (retMsg != null)
            {
                Hashtable<String, String[]> ht = HttpParamUtil
                        .parseQueryString(retMsg);
                String[] qqs = ht.get("uin");
                if (qqs != null && qqs.length > 0 && qqs[0].length() > 4)
                    result.setUin(StringUtil.toLong(qqs[0], 0));

                String[] codes = ht.get("code");
                if (codes != null && codes.length > 0 && codes[0].length() > 0)
                    result.setCode((int) StringUtil.toLong(codes[0], 0));

            }

            result.setRetCode(resp.getResult());
            result.setRetMsg(retMsg);
            
            //3.2 获取用户昵称
            if (result.getUin() > 10000 && resp.getA8() != null
                    && resp.getCrtTime() != null)
            {
                UserInfoClient userInfoClient = new UserInfoClient();
                result.setNickName(userInfoClient.getUserInfo(result.getUin(),
                        resp.getA8(), resp.getCrtTime(), clientIp));
            }

        }
        long timeCost = System.currentTimeMillis() - start;
        Log.run.debug("checkLoginForSid: req:" + req.toString() + " resp:"
                + resp.toString() + result + " costTime:"
                + timeCost);
        return result;
    }

	@Override
	public boolean checkLoginForQQSdk(String mk, Long uin, String sk) {
		CheckLoginByWgUidResp resp = null ;
		resp = LoginClient.checkLoginByWgUid(mk, uin, sk) ;
		if (resp == null) {
			Log.run.info("checkLoginForQQSdk: resp is null,login error! mk=" + mk + ",uin="+uin+",sk="+sk);
			return false ;
		}
		if (resp.getResult() == 0) {
			Log.run.info("checkLoginForQQSdk: resp is success,login success! result = "+resp.getResult()
					+",errMess"+resp.getErrmsg()+",mk=" + mk + ",uin="+uin+",sk="+sk);
			return true ;
		} else {
			Log.run.info("checkLoginForQQSdk: resp is error,login error! result = "+resp.getResult()
					+",errMess"+resp.getErrmsg()+",mk=" + mk + ",uin="+uin+",sk="+sk);
			return false ;
		}		
	}

    // /* (非 Javadoc)
    // * <p>Title: CheckVerifyCode</p>
    // * <p>Description: </p>
    // * @param verifyCodePo
    // * @return
    // * @see
    // com.qq.qqbuy.common.biz.VerifyCodeService#CheckVerifyCode(com.qq.qqbuy.common.po.GetVerifyCodePo)
    // */
    // @Override
    // public boolean CheckVerifyCode(GetVerifyCodePo verifyCodePo)
    // {
    // CheckVerifyCodeReq req = new CheckVerifyCodeReq();
    // req.setCommParm(verifyCodePo.getCommParm());
    // req.setCapType(verifyCodePo.getCapType());
    // req.setCode(verifyCodePo.getCode());
    // req.setSig(verifyCodePo.getSignature());
    // req.setValidTime(validTime);
    // CheckVerifyCodeResp resp = new CheckVerifyCodeResp();
    // boolean ret = CheckVerifyCode(req, resp);
    // logger.log(getBusinesModule(), "ret:" + ret + " req:" + req + " resp:" +
    // resp);
    // return ret;
    // }

}
