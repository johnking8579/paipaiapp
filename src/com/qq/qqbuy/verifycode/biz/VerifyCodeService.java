package com.qq.qqbuy.verifycode.biz;

import com.qq.qqbuy.common.po.LoginResult;

/**
 * 验证码服务接口
 * 
 * @author wendyhu
 * 
 */
public interface VerifyCodeService  {

    // /**
    // * 获取验证码接口， 通过GetVerifyCodePo构造出GetVerifyCodeReq，并将GetVerifyCodeResp的结果带回来
    // * @param verifyCodePo
    // * @return
    // */
    // public boolean getVerifyCode(GetVerifyCodePo verifyCodePo);
    //    
    //    
    // /**
    // * 检查验证码接口
    // *
    // * @param req
    // * @param resp
    // * @return
    // */
    // public boolean CheckVerifyCode(GetVerifyCodePo verifyCodePo);

    /**
     * 用于ptlogin验证登陆的接口
     * 
     * @Description: 用于ptlogin验证登陆的接口
     * 
     * @param uin
     *            : 用户qq号码 必填
     * @param clientIp
     *            : 用户qq号码登陆ip,必填
     * @param skey
     *            :强登陆态key，,和lskey配合，必须填其中一个
     * @param lskey
     *            : 弱登陆态key,和skey配合，必须填其中一个
     * 
     * @return LoginResult 返回类型
     */
    public LoginResult checkLoginForPt(long uin, String clientIp, String skey,
	    String lskey);

    /**
     * opensdk校验是否登陆
     * @param mk 机器码
     * @param uin 用户wid
     * @param sk getToken时的sk
     * @return
     */
    public boolean checkLoginForQQSdk(String mk, Long uin, String sk) ;
    /**
     * 用于ptlogin验证登陆的接口
     * 
     * @Description: 用于ptlogin验证登陆的接口
     * 
     * @param clientIp
     *            : 用户qq号码登陆ip,必填
     * @param sid
     *            : 无线的sid 必填
     * @param statSid
     *            :从cookie 或者qcookie中获取，cookie name 是 ng_st_sid_v2
     * @param sidFromCookie
     *            :从cookie中获取，cookie name 是ng_c_sid
     * @param sidFromQCookie
     *            从qcookie中获取，cookie name 是sid
     * 
     * @return LoginResult 返回类型
     */
    public LoginResult checkLoginForSid(String clientIp, String sid,
	    String statSid, String sidFromCookie, String sidFromQCookie);

    /**
     * 
     * @Title: getSkey
     * @Description: 通过lskey换成skey
     * @param uin
     *            用户qq号码
     * @param authCode
     *            弱登陆态
     * @param skey
     *            移动电商生成的skey
     * @param mk
     *            用来验证skey是否合法
     * @param type
     *            1:代表是uk里面的lskey，那么authCode就填lskey 2:代表通过sid验证，那么authCode就填sid
     * @return 设定文件
     * @return String 返回类型
     * @throws
     */
    public String getSkey(long uin, String authCode, String skey, String mk,
	    int type);

    /**
     * 检查用户是否登录，QQBuyServiceComm用于封装CheckLoginReq、CheckLoginResp来进行IDL的调用
     * 
     * @param serviceCommBean
     * @return
     */
    // public boolean checkLogin(QQBuyServiceComm serviceCommBean);

    // /**
    // * 获取验证码接口
    // *
    // * @param req
    // * @param resp
    // * @return
    // */
    // public boolean GetVerifyCode(GetVerifyCodeReq req, GetVerifyCodeResp
    // resp);
    //	
    //	
    //	
    // /**
    // * 检查验证码接口
    // *
    // * @param req
    // * @param resp
    // * @return
    // */
    // public boolean CheckVerifyCode(CheckVerifyCodeReq req,
    // CheckVerifyCodeResp resp);
    //	
    //	
    // /**
    // * 检查用户是否登陆接口
    // *
    // * @param req
    // * @param resp
    // * @return
    // */
    // public boolean CheckLogin(CheckLoginReq req, CheckLoginResp resp);

}
