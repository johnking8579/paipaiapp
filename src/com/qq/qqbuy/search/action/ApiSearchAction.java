package com.qq.qqbuy.search.action;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;

import com.qq.qqbuy.common.util.Util;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.paipai.util.string.StringUtil;
import com.qq.qqbuy.common.JsonOutput;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.action.PaipaiApiBaseAction;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.AntiXssHelper;
import com.qq.qqbuy.common.util.CoderUtil;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.search.biz.SearchBiz;
import com.qq.qqbuy.search.po.SearchPo;
import com.qq.qqbuy.search.util.SearchConstant;
import com.qq.qqbuy.thirdparty.http.search.SearchConditionFactory;
import com.qq.qqbuy.thirdparty.http.search.po.SearchCondition;

/**
 * @author winsonwu
 * @Created 2012-12-19
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class ApiSearchAction extends PaipaiApiBaseAction {
	static Logger log = LogManager.getLogger(ApiSearchAction.class);
	private static final long serialVersionUID = 3605169194662707010L;
	
	private String k; // keyword,搜索关键词
	private String keyWords= ""; //分词后的关键字
	private int pn=1;
	private int ps=5;
	private int os=SearchConstant.SEARCH_SORT_DEFAULT; // orderStyle,排序方式
	/**
	 * 0,204260-0,705506-55,5707
	说明：0 表示后面的是导航ID，非0 表示 属性
	204260 表示手机的导航
	705506 表示安卓手机的导航
	55 品牌属性
	5707 联想 （属性值ID）
	 */
	private String p; // path
	private int as;  //用于区分主动搜索和被动搜索， as=1代表主动搜索，as=0代表被动搜索，as参数不需要传递到链接里面，只会在搜索框中才会传递改参数进来
	private long bp ;  //低价
	private long ep ;  //高价
	private String addr ;  //卖家所在地
	private int fm ;  //是否免邮，1为免邮，0为所有
	private int cod ;//2为搜PC货到付款  0为所有
	private int degree = 1;//新旧程度，1全新,2二手, 4陈列品
	private int reightInsurance = 0;//运费险，1：有；0：无
	private int  em=0;  //标红参数 为0,则不对标题标红, 其他进行标红
	private int showQQvip;  //是否只搜索QQ商城的商品：0=都搜索, 1=只搜索QQ商城的商品
	private SearchPo searchPo;
	private SearchBiz searchBiz; 
	private String searchRCResult = "";
	JsonOutput out = new JsonOutput();
	
	
	private String keyWord;
	private int pageNum;
	private int pageSize;
	private String address;
	private String shopType;
	private String mainSale;
	private String level;
	private int sex = -1;
	private int orderStyle = -1;
	
	
	
	
	
	public String list()	{
		keyWords = "" ;
		try {
			SearchCondition condition = SearchConditionFactory.createSearchCondition( 
										k, p, pn, ps, os,bp,ep, 0, 0,addr,fm,cod,degree,reightInsurance);
			long uin = 0L;
			if(checkLoginAndGetSkey()){
				uin = getWid();
			}
			int version = 0;
			try {
				version = Integer.parseInt(getVersionCode());
			} catch (NumberFormatException e) {
			}
			searchPo = searchBiz.searchList(condition, uin,version);
			Log.run.info("searchPo.getTotalNum()"+searchPo.getTotalNum());
			//如果没有搜索到内容则进行分词
			if(searchPo.getTotalNum()==0){				
				try {
					//分词接口URL
					String recourl = "http://10.213.138.53:8081/reco_word_service.cgi?keywords="
							+ this.k;
					String recoresult = "";
					recoresult = HttpUtil.get(recourl, 10000, 10000, "gb2312",
							false);
					Log.run.info("URLRESULT:" + recoresult);
					JsonParser recoJsonParser = new JsonParser();
					String result ="";
					if(recoresult!=null&&!"".equals(recoresult)){
						JsonElement recoJsonElement = recoJsonParser.parse(recoresult);
						Log.run.info("json:" + recoJsonElement);
						result = recoJsonElement.getAsJsonObject().get("result").getAsString();
						Log.run.info("result:" + result);
						String[] results = result.split("\\|");						
						// 如果返回结果有有效值
						if (results != null && result.length() > 1) {			
							//取出该组合的词组成一个新的搜索词
							StringBuffer sb = new StringBuffer();	
							StringBuffer kwsb = new StringBuffer() ;
							//取出被拆分后的词。并组成数据
							String[] reco = results[0].split("\\,");	
							if (reco.length <= 1) {
								sb.append(results[0]) ;
								kwsb.append(results[0]) ;
							} else {
								//获取拆分后的词的组合，目前只取第一种组合
								String[] reconum = results[1].split("\\,");
								for(int i=0;i<reconum.length;i++){
									sb.append(reco[Integer.valueOf(reconum[i])]);	
									if (0 == i) {
										kwsb.append(reco[Integer.valueOf(reconum[i])]);	
									} else {
										kwsb.append("~~~").append(reco[Integer.valueOf(reconum[i])]);	
									}
								}
							}
							//并把新的搜索词作为关键词来查询结果
							k = sb.toString();
							if (!StringUtils.isEmpty(StringUtils.trim(k))) { //防止空串
								condition = SearchConditionFactory.createSearchCondition(
										k, p, pn, ps, os,bp,ep, 0, 0,addr,fm,cod,degree,reightInsurance);
								searchPo = searchBiz.searchList(condition,uin,version);
								keyWords = kwsb.toString() ;
							} 
						}
						
					}
					
				} catch (Exception e) {
					Log.run.info("调用分词接口异常" + e.toString());
				}						
			}
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg4BExp(e);
			return RST_FAILURE;
		} 
	}
	
	
	/**
	 * 给手机端提供搜索热词  集合PPMS
	 * @return
	 */
	public String searchShop(){
		
//		Map<String, String> reqParam =  new HashMap<String, String>();
//		reqParam.put("KeyWord", "手机");
//		reqParam.put("PageType", "3");
//		reqParam.put("Platform", "1");
//		reqParam.put("charSet", "gb2312");
//		reqParam.put("PageNum", "1");
//		reqParam.put("sf", "1001");
		
		
		StringBuffer url = new StringBuffer();
		try {
			url.append("http://ss.paipai.com/json/search?")
				.append("KeyWord=").append(URLDecoder.decode(this.keyWord, "utf-8")).append("&")
				.append("PageType=").append(3).append("&")
				.append("Platform=").append(1).append("&")
				.append("charSet=").append("utf-8").append("&")
				.append("sf=").append("1001").append("&")
				.append("PageNum=").append(this.pageNum).append("&")
				.append("PageSize=").append(this.pageSize).append("&")
			;
			
			if(StringUtil.isNotBlank(this.address)){
				url.append("Address=").append(URLDecoder.decode(this.address, "utf-8")).append("&");
			}
			if(StringUtil.isNotBlank(this.shopType)){
				url.append("ShopType=").append(this.shopType).append("&");
			}
			if(StringUtil.isNotBlank(this.mainSale)){
				url.append("MainSale=").append(this.mainSale).append("&");
			}
			if(StringUtil.isNotBlank(this.level)){
				url.append("Level=").append(this.level).append("&");
			}
			if(this.sex != -1){
				url.append("Sex=").append(this.sex).append("&");
			}
			if(this.orderStyle != -1){
				url.append("OrderStyle=").append(this.orderStyle).append("&");
			}
		} catch (Exception e1) {
			log.error("参数有误。"+e1.getMessage(),e1);
		}
		
		
		log.debug("url::"+url.toString().substring(0, url.length()-1));
		
		
		String string = "";
		try {
			string = HttpUtil.getWithProxy(url.toString().substring(0, url.length()-1), null, 10000, 15000, "gbk", true, false, null, "10.6.223.129",80);
//			string = HttpUtil.getWithProxy("http://ss.paipai.com/json/search", 
//			    		reqParam , 10000, 15000, "gb2312", "gb2312", true,null,null,true,"10.6.223.129",80);
		} catch (Exception e) {
			Log.run.error("请求店铺搜索接口失败：" + e.getMessage(), e);
			out.setErrCode(com.qq.qqbuy.common.constant.ErrConstant.ERRCODE_SEARCHSHOP_ERROR+"");
			return doPrint(out.toJsonStr());
		}
		log.debug("result::"+string);
		
		
		if("".equals(string) || null == string){
			out.setErrCode(com.qq.qqbuy.common.constant.ErrConstant.ERRCODE_SEARCHSHOP_ERROR+"");
			return doPrint(out.toJsonStr());
		}
		
		try {
			JsonParser jsonParser = new JsonParser();
			String replaceAll = string.replaceAll("\"", "\\\"").replaceAll("\n", "").replaceAll("\r", "").replaceAll("<[^>]+>", "");
//			log.debug("replaceAll::"+replaceAll);
			JsonElement jsonElement = jsonParser.parse(replaceAll);
			
			log.debug(jsonElement.getAsJsonObject().get("data"));
			
			out.setData(jsonElement.getAsJsonObject().get("data"));
		} catch (JsonSyntaxException e) {
			log.error(e.getMessage());
		}
		return doPrint(out.toJsonStr());
	}
	
	
	/**
	 * 给手机端提供搜索热词  集合PPMS
	 * @return
	 */
	public String searchRC(){
		
		String url = "http://www.paipai.com/sinclude/app_search_hotwords.html";
		
		String string = HttpUtil.get(url, 10000,10000, "gb2312", true);
		if(string == null || "".equals(string)){
			out.setErrCode(com.qq.qqbuy.common.constant.ErrConstant.ERRCODE_SEARCHRC_ERROR+"");
		}
		
		int begin = string.indexOf("[\"");
		int end = string.indexOf("\"]");
		
		if(begin== -1 || end == -1){
			out.setErrCode(com.qq.qqbuy.common.constant.ErrConstant.ERRCODE_SEARCHRC_ERROR+"");
		}
		
		try {
			JsonElement parse = new JsonParser().parse(string.substring(begin, end + 2));
			out.setData(parse);
		} catch (JsonSyntaxException e) {
			Log.run.error(e.getMessage(), e);
			out.setErrCode(com.qq.qqbuy.common.constant.ErrConstant.ERRCODE_SEARCHRC_ERROR+"");
			return doPrint(out.toJsonStr());
		}
			
		return doPrint(out.toJsonStr());
	}
	
	
	public String getK() {
		return k;
	}

	public void setK(String k) {
		this.k = CoderUtil.decodeWML(AntiXssHelper.xmlAttributeEncode(k));
	}

	public int getPn() {
		return pn;
	}

	public void setPn(int pn) {
		this.pn = pn;
	}

	public int getPs() {
		return ps;
	}

	public void setPs(int ps) {
		this.ps = ps;
	}

	public int getOs() {
		return os;
	}

	public void setOs(int os) {
		this.os = os;
	}

	public String getP() {
		return p;
	}

	public void setP(String p) {
		this.p = CoderUtil.decodeWML(AntiXssHelper.xmlAttributeEncode(p));
	}

	public int getAs() {
		return as;
	}

	public void setAs(int as) {
		this.as = as;
	}
	
	public SearchPo getSearchPo() {
		return searchPo;
	}

	public void setSearchPo(SearchPo searchPo) {
		this.searchPo = searchPo;
	}

	public SearchBiz getSearchBiz() {
		return searchBiz;
	}

	public void setSearchBiz(SearchBiz searchBiz) {
		this.searchBiz = searchBiz;
	}


	public long getEp() {
		return ep;
	}

	public void setEp(long ep) {
		this.ep = ep;
	}

	public long getBp() {
		return bp;
	}

	public void setBp(long bp) {
		this.bp = bp;
	}

	public int getEm() {
		return em;
	}

	public void setEm(int em) {
		this.em = em;
	}

	public int getShowQQvip() {
		return showQQvip;
	}

	public void setShowQQvip(int showQQvip) {
		this.showQQvip = showQQvip;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	public int getFm() {
		return fm;
	}
	public void setFm(int fm) {
		this.fm = fm;
	}
	public int getCod() {
		return cod;
	}
	public void setCod(int cod) {
		this.cod = cod;
	}



	public String getSearchRCResult() {
		return searchRCResult;
	}



	public void setSearchRCResult(String searchRCResult) {
		this.searchRCResult = searchRCResult;
	}


	public String getKeyWord() {
		return keyWord;
	}


	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}


	public int getPageNum() {
		return pageNum;
	}


	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}


	public int getPageSize() {
		return pageSize;
	}


	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getShopType() {
		return shopType;
	}


	public void setShopType(String shopType) {
		this.shopType = shopType;
	}


	public String getMainSale() {
		return mainSale;
	}


	public void setMainSale(String mainSale) {
		this.mainSale = mainSale;
	}


	public String getLevel() {
		return level;
	}


	public void setLevel(String level) {
		this.level = level;
	}


	public int getSex() {
		return sex;
	}


	public void setSex(int sex) {
		this.sex = sex;
	}


	public int getOrderStyle() {
		return orderStyle;
	}


	public void setOrderStyle(int orderStyle) {
		this.orderStyle = orderStyle;
	}
	
	public static void main(String[] args) throws IOException {
		/*String string ="sfsdfs<em>fsfds</em>fsdfsf<em>sdde</em>";
		System.out.println(URLDecoder.decode("%E6%B3%A2%E8%A5%BF%E7%B1%B3%E4%BA%9A", "utf-8"));
		System.out.print(string.replaceAll("<[^>]+>", ""));*/
		long maxValue = Long.MAX_VALUE;
		System.out.println(maxValue);
		String keyWord = "汽车";
		String path = "9001";
		int pn = 1;
		int ps = 18;
		int orderStyle = 15;
		long beginPrice = 0;
		long endPrice = 0;
		String addr = "";
		int freeMail = 0;
		int cod = 0;
		long uin = 419739377l;
		int degree = 1;
		int reightInsurance = 0;
		String cookieStr = "uin="+uin;
		String searchUrl = "http://sse1.paipai.com/comm_json?";
		SearchCondition condition = SearchConditionFactory.createSearchCondition(
				keyWord, path, pn, ps, orderStyle,beginPrice,endPrice, 0, 0,addr,freeMail,cod,degree,reightInsurance);
		String param = Util.paramMapToString(condition.toQueryMap());
		BufferedReader br = null;
		HttpURLConnection conn;
		String s = null;
		StringBuilder sb = new StringBuilder();
		try {
			conn = (HttpURLConnection)new URL(searchUrl+param).openConnection();
			conn.setConnectTimeout(5000);
			conn.setReadTimeout(5000);
			conn.setRequestProperty("Referer", "app.paipai.com");	//服务端根据referer对APP搜索做特殊处理
			conn.setRequestProperty("Cookie", cookieStr);
			br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "gbk"));
			while((s=br.readLine()) != null)	{
				sb.append(s);
			}
			System.out.println(sb.toString());
		} catch(IOException e)	{
			throw new IOException(e.getMessage() + "  " + searchUrl, e);
		} finally	{
			Util.closeStream(br);
		}
		String recourl = "http://10.213.138.53:8081/reco_word_service.cgi?keywords="
				+ URLEncoder.encode("我是中国的人", "utf-8");
		String recoresult = "";
		try {
			 recoresult = HttpUtil.get(recourl, 10000, 10000, "gb2312",
					false);
			 System.out.println(recoresult);
		} catch (Exception e) {
			Log.run.info("调用分词接口异常");
		}
    }


	public String getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}

	public int getDegree() {
		return degree;
	}

	public void setDegree(int degree) {
		this.degree = degree;
	}

	public int getReightInsurance() {
		return reightInsurance;
	}

	public void setReightInsurance(int reightInsurance) {
		this.reightInsurance = reightInsurance;
	}
}
