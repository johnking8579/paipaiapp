package com.qq.qqbuy.search.po;

/**
 * 品类.
 * 
 * @author winsonwu
 */
public class MetaClass implements java.io.Serializable {
    
	private static final long serialVersionUID = 8815091001686673450L;

	/**
     * 品类ID
     */
    private long id;
    
    /**
     * 该品类搜索命中商品数
     */
    private long count;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}
    
}
