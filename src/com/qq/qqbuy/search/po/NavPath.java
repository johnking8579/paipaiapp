package com.qq.qqbuy.search.po;

/**
 * 搜索导航路径项
 * 
 * @author winsonwu
 * 
 */
public class NavPath implements java.io.Serializable {

    private static final long serialVersionUID = 6146639039681872252L;

    /**
     * 路径项ID. 当路径项是类目时，为类目ID；当路径项是属性时，为属性ID
     */
    private long pathId;

    /**
     * 路径名称. 当路径项是类目时，为空串；当路径项是属性时，为属性名称
     */
    private String pathName;

    /**
     * 属性项ID. 当路径项是类目时，为0；当路径项是属性时，为属性项值
     */
    private long itemId;

    /**
     * 属性项名称. 当路径项是类目时，为类目名；当路径项是属性时，为属性项名称
     */
    private String itemName;

    /**
     * 该路径对应的搜索URL（在拍拍WEB上有效，可用于检验验证，不要直接用在移动电商中）
     */
    private String pathUrl;

    /**
     * 选中该节点后的path参数。从pathUrl中截取出来
     */
    private String path;

    public long getPathId() {
	return pathId;
    }

    public void setPathId(long pathId) {
	this.pathId = pathId;
    }

    public String getPathName() {
	return pathName;
    }

    public void setPathName(String pathName) {
	this.pathName = pathName;
    }

    public long getItemId() {
	return itemId;
    }

    public void setItemId(long itemId) {
	this.itemId = itemId;
    }

    public String getItemName() {
	return itemName;
    }

    public void setItemName(String itemName) {
	this.itemName = itemName;
    }

    public String getPathUrl() {
	return pathUrl;
    }

    public void setPathUrl(String pathUrl) {
	this.pathUrl = pathUrl;
    }

    public String getPath() {
	return path;
    }

    public void setPath(String path) {
	this.path = path;
    }

}
