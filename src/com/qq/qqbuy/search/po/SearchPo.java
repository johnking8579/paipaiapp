package com.qq.qqbuy.search.po;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

/**
 * 搜索的统一结果Po
 * @author winsonwu
 * @Created 2012-11-26
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class SearchPo {
	
	private int totalNum;
	
	// 搜索关键字类型：0=无限制, 1=命中Android虚拟充值过滤词库
	private int keyType = 0;

    /**
     * 类目信息
     */
    private List<MetaClass> classes = new ArrayList<MetaClass>();

    /**
     * 路径信息
     */
    private List<NavPath> paths = new ArrayList<NavPath>();
    
    /**
     * 类目聚类集合
     */
    private ClusterSet classCluster = new ClusterSet();
    
    /**
     * 属性聚类集合链表
     */
    private List<ClusterSet> attrClusterSetList = new LinkedList<ClusterSet>();

    /**
     * 商品列表信息
     */
	private Vector<SearchItem> items = new Vector<SearchItem>();

	/**
	 * 优质店铺json信息
	 * @return
	 */
	private String shopList;

	public int getTotalNum() {
		return totalNum;
	}

	public void setTotalNum(int totalNum) {
		this.totalNum = totalNum;
	}

	public List<MetaClass> getClasses() {
		return classes;
	}

	public void setClasses(List<MetaClass> classes) {
		this.classes = classes;
	}

	public List<NavPath> getPaths() {
		return paths;
	}

	public void setPaths(List<NavPath> paths) {
		this.paths = paths;
	}

	public ClusterSet getClassCluster() {
		return classCluster;
	}

	public void setClassCluster(ClusterSet classCluster) {
		this.classCluster = classCluster;
	}

	public List<ClusterSet> getAttrClusterSetList() {
		return attrClusterSetList;
	}

	public void setAttrClusterSetList(List<ClusterSet> attrClusterSetList) {
		this.attrClusterSetList = attrClusterSetList;
	}

	public Vector<SearchItem> getItems() {
		return items;
	}

	public void setItems(Vector<SearchItem> items) {
		this.items = items;
	}

	public int getKeyType() {
		return keyType;
	}

	public void setKeyType(int keyType) {
		this.keyType = keyType;
	}

	public String getShopList() {
		return shopList;
	}

	public void setShopList(String shopList) {
		this.shopList = shopList;
	}
}
