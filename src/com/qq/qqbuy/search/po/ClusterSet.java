package com.qq.qqbuy.search.po;

import java.util.LinkedList;
import java.util.List;

/**
 * 聚类集. 包含多个聚类项. 有类目聚类集和属性聚类集.
 * 
 * @author winsonwu
 */
public class ClusterSet implements java.io.Serializable {
	
	private static final long serialVersionUID = 4120339156082132936L;

	/**
     * 聚类集名称. 当为类目聚类集时，聚类集名称为"cluster"；当为属性聚类集时, 聚类集名称为属性项名
     */
    private String setName;
    
    /**
     * 属性ID，当为属性聚类时有效，当为类目聚类时，该值为0
     */
    private long attrId = 0;
    
    private boolean currCluster;
    
    /**
     * 聚类项链表
     */
    private List<ClusterItem> clusterList = new LinkedList<ClusterItem>();

	public String getSetName() {
		return setName;
	}

	public void setSetName(String setName) {
		this.setName = setName;
	}

	public long getAttrId() {
		return attrId;
	}

	public void setAttrId(long attrId) {
		this.attrId = attrId;
	}

	public List<ClusterItem> getClusterList() {
		return clusterList;
	}

	public void setClusterList(List<ClusterItem> clusterList) {
		this.clusterList = clusterList;
	}

	public boolean isCurrCluster() {
		return currCluster;
	}

	public void setCurrCluster(boolean currCluster) {
		this.currCluster = currCluster;
	}
    
}
