package com.qq.qqbuy.search.util;

/**
 * @author winsonwu
 * @Created 2012-12-20
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class SearchChecker {
    public static final int SEARCH_SORT_TIME_A = 4;
    public static final int SEARCH_SORT_TIME_D = 5;
    public static final int SEARCH_SORT_PRICE_A = 6;
    public static final int SEARCH_SORT_PRICE_D = 7;
    public static final int SEARCH_SORT_CREDIT_A = 8;
    public static final int SEARCH_SORT_CREDIT_D = 9;
    public static final int SEARCH_SORT_RECOMMEND_A = 12;
    public static final int SEARCH_SORT_RECOMMEND_D = 13;
    public static final int SEARCH_SORT_SOLD_COUNT_D = 24;
	public static boolean checkOrderStyle(int orderStyle) {
		if (SearchConstant.SEARCH_SORT_TIME_A == orderStyle ||
			SearchConstant.SEARCH_SORT_TIME_D == orderStyle ||
			SearchConstant.SEARCH_SORT_PRICE_A == orderStyle ||
			SearchConstant.SEARCH_SORT_PRICE_D == orderStyle ||
			SearchConstant.SEARCH_SORT_CREDIT_A == orderStyle ||
			SearchConstant.SEARCH_SORT_CREDIT_D == orderStyle ||
			SearchConstant.SEARCH_SORT_RECOMMEND_A == orderStyle ||
			SearchConstant.SEARCH_SORT_RECOMMEND_D == orderStyle ||
			SearchConstant.SEARCH_SORT_SOLD_COUNT_D == orderStyle ||
			SearchConstant.SEARCH_SORT_STORE_NUM_D == orderStyle ||
			SearchConstant.SEARCH_SORT_STORE_TIME_D == orderStyle ) {
			return true;
		}
		return false;  
	}

}
