package com.qq.qqbuy.search.util;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.util.CoderUtil;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.UrlGenerator;
import com.qq.qqbuy.search.po.SearchNavigation;

/**
 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
 */
public class WSearchUrlGenerator extends UrlGenerator{
	protected static final String WSEARCH_LIST_URL_PREFIX = "/w/s/list.xhtml";//商详首页
	protected static String genListUrl(String sid,String key,String p,int order ,int itemType,int searchType,int pn ,int ps) {
		StringBuilder sbf = new StringBuilder(WSEARCH_LIST_URL_PREFIX);
		boolean isFirst = true;
		isFirst = appendKey(sbf, key, isFirst);
		isFirst = appendSid(sbf, sid, isFirst);
		isFirst = appendPath(sbf, p, isFirst);
		isFirst = appendItemType(sbf, itemType, isFirst);
		isFirst = appendOrder(sbf, order, isFirst);	
		isFirst = appendSearchType(sbf, searchType, isFirst);
		isFirst = appendPn(sbf, pn, isFirst);
		isFirst = appendPs(sbf, ps, isFirst);
		return sbf.toString();
	}
	private static boolean appendKey(StringBuilder sbf, String key, boolean isFirst) {
		if (!StringUtil.isEmpty(key)) {
			sbf.append(isFirst ? "?" : "&amp;");
			sbf.append("k=").append(key);
			return false;
		}
		return isFirst;
	}
	private static boolean appendPath(StringBuilder sbf, String path, boolean isFirst) {
		if (!StringUtil.isEmpty(path)) {
			sbf.append(isFirst ? "?" : "&amp;");
			sbf.append("p=").append(path);
			return false;
		}
		return isFirst;
	}
	private static boolean appendOrder(StringBuilder sbf, int order, boolean isFirst) {
		if (order>0) {
			sbf.append(isFirst ? "?" : "&amp;");
			sbf.append("os=").append(order);
			return false;
		}
		return isFirst;
	}
	private static boolean appendItemType(StringBuilder sbf, int itemType, boolean isFirst) {
		if (itemType>0) {
			sbf.append(isFirst ? "?" : "&amp;");
			sbf.append("it=").append(itemType);
			return false;
		}
		return isFirst;
	}
	private static boolean appendSearchType(StringBuilder sbf, int searchType, boolean isFirst) {
		if (searchType>0) {
			sbf.append(isFirst ? "?" : "&amp;");
			sbf.append("st=").append(searchType);
			return false;
		}
		return isFirst;
	}
	public static SearchNavigation genSearchURL(String sid,String key,String p,int order ,int itemType,int searchType,int pn ,int ps,String backURL) {
		SearchNavigation po=new SearchNavigation();
		po.setListURL(genListUrl(sid, key, p,order, itemType, searchType,pn,ps));
		//po.setBackURL(po.getListURL());
		po.setBackURL(CoderUtil.encodeUtf8(po.getListURL()));
		Log.run.info("genSearchURL:"+po);
		return po;
	}
}

