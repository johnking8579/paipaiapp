/*package com.qq.qqbuy.search.util;

import java.util.List;

import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.search.biz.SearchService;
import com.qq.qqbuy.search.biz.impl.SearchServiceImpl;
import com.qq.qqbuy.search.po.ClusterItem;
import com.qq.qqbuy.search.po.ClusterSet;
import com.qq.qqbuy.search.po.CommoditySearchResponse;
import com.qq.qqbuy.search.po.SearchNavPathPo;
import com.qq.qqbuy.search.po.SearchCriteria;
import com.qq.qqbuy.search.po.SearchInfo;

public class SearchUtil {
	
	private static final String CURRENT_NAVPATH = "SEARCH.CURRENT_NAVPATH";

	
	private SearchUtil(){}
	
	
	*//**
	 * 根据页面参数查询拍拍商品
	 * @param navPath 类目
	 * @param keyWord 关键字
	 * @param itemType 商品类型
	 * @param pageIndex 起始页码
	 * @param pageSize 页大小
	 * @param sortType 排序类型
	 * @return
	 *//*
	public static CommoditySearchResponse search(String navPath, String keyWord, int itemType,
			int pageIndex, int pageSize, int sortType, long uin) {
		SearchService service = new SearchServiceImpl();
		if(service != null) {
			SearchCriteria searchCriteria = new SearchCriteria();
			searchCriteria.navPath = navPath;
			searchCriteria.keyWord = keyWord;
			if(itemType == SearchInfo.ITEMTYPE_COLORDIAMOND) {
				searchCriteria.isSupportColorDiamond = true;
			}			
			searchCriteria.pageIndex = pageIndex;
			searchCriteria.pageSize = pageSize;
			searchCriteria.sortType = sortType;
			
			return service.search(searchCriteria);
		} else  {
			return null;
		}
	}
	
	*//**
	 * “<a href="#">女装</a>&nbsp;>&nbsp;<a href="#">所有分类</a>&nbsp;>&nbsp;<a href="#">女士2010秋冬</a>”搜索“T恤”找到656个：
	 * “所有分类”搜索“T恤”找到656个：
	 * @param searchInfo
	 * @param resp
	 * @param server_qgo
	 * @param context
	 * @return
	 *//*
	public static String genSearchTip(SearchInfo searchInfo, CommoditySearchResponse resp, String server_qgo, MobileLifeContextServiceImpl context) {
		if(resp == null) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		List<SearchNavPathPo> navPathList = resp.navPathList;
		if(navPathList != null && navPathList.size() > 0) {
			//sb.append("\"");
			StringBuilder navPath = new StringBuilder();
			for(int i = 0; i < navPathList.size(); i++) {
				SearchNavPathPo item = navPathList.get(i);
				
				//拼接navpath串
				if(i != 0) {
					navPath.append("-");
				}
				if(item.itemId == 0) {	//是类目，类目的格式为：0,类目ID
					navPath.append("0,").append(item.pathId);
				} else {	//是属性，格式为：属性ID,属性值
					navPath.append(item.pathId).append(",").append(item.itemId);
				} 
				
				//拼接所有分类
				if(i == 0) {
					sb.append("<a href=\"").append(server_qgo).append("aid=template&amp;tid=searchindex\">所有分类</a>");
				}
				sb.append("&gt;");
				if(item != null) {
					if(i == (navPathList.size() - 1)) {	//最后一个只拼接文字
						sb.append(item.ItemName);
					} else {
						sb.append("<a href=\"").append(server_qgo).append("aid=s_result&amp;navpath=").append(navPath);
						if(!StringUtil.isEmpty(searchInfo.keyWord)) {
							sb.append("&amp;keyword=").append(searchInfo.keyWord);
						}
						sb.append("&amp;src=").append(searchInfo.source).append("&amp;itemType=").append(searchInfo.itemType);
						sb.append("\" >").append(context.filterInvalidWML(item.ItemName)).append("</a>");						
					}
				}
			}
			context.setVariable(CURRENT_NAVPATH, navPath.toString());
			sb.append("&nbsp;中");
		} else {
			sb.append("<a href=\"").append(server_qgo).append("aid=template&amp;tid=searchindex\">所有分类</a>&nbsp;中");
		}
		//查询关键字
		if(!StringUtil.isEmpty(searchInfo.keyWord)) {
			if(context.isWap10()) {	//1.0页面
				sb.append("搜 \"").append(context.filterInvalidWML(searchInfo.keyWord)).append("\"");
			} else {
				sb.append("搜<span class=\"redtext\">").append(searchInfo.keyWord).append("</span>");
			}			
		}
		sb.append("找到").append(resp.totalCount).append("个商品：");
		
		return sb.toString();
	}
	
	*//**
	 * 生成子类目链接
	 * <p><a href="#">男装(198)</a><a href="#">女装(187)</a><a href="#">更多&gt;&gt;</a></p>
	 * @param searchInfo
	 * @param resp
	 * @param server_qgo
	 * @param context
	 * @return
	 *//*
	public static String genClassClusterSetContent(SearchInfo searchInfo, CommoditySearchResponse resp, String server_qgo, MobileLifeContextServiceImpl context) {
		if(resp == null) {
			return "";
		}
		
		//获取当前的navpath
		String navPath = genCurrentNavPath(resp, context);
		
		StringBuilder sb = new StringBuilder();
		//是否有更多
		boolean hasMore = false;
		if(resp.classClusterSet != null 
				&& "cluster".equals(resp.classClusterSet.setName)
				&& resp.classClusterSet.clusterList != null
				&& resp.classClusterSet.clusterList.size() > 0) {
			boolean isFirst = true;
			int count = 0;
			for(ClusterItem item: resp.classClusterSet.clusterList) {
				if(item.clusterCount > 0) {	//
					count++;
					if(count <= 2) {	//每页最多两条
						if(!isFirst) {
							sb.append("&nbsp;");
						}
						sb.append("<a href=\"").append(server_qgo).append("aid=s_result&amp;navpath=").append(navPath).append("-0,").append(item.clusterId);
						if(!StringUtil.isEmpty(searchInfo.keyWord)) {
							sb.append("&amp;keyword=").append(searchInfo.keyWord);
						}			
						sb.append("&amp;src=").append(searchInfo.source).append("&amp;itemType=").append(searchInfo.itemType);
						sb.append("\" >").append(context.filterInvalidWML(item.clusterName)).append("(").append(item.clusterCount).append(")").append("</a>");					
					} else {
						hasMore = true;
						break;
					}
					isFirst = false;
				}
			}			
		}
		StringBuilder content = new StringBuilder();
		if(sb.length() > 0) {	//有内容
			content.append("<p>");
			content.append(sb);
			if(hasMore) {
				content.append("&nbsp;");
				content.append("<a href=\"").append(server_qgo).append("aid=s_classmore&amp;navpath=").append(navPath);
				if(!StringUtil.isEmpty(searchInfo.keyWord)) {
					content.append("&amp;keyword=").append(searchInfo.keyWord);
				}				
				content.append("&amp;src=").append(searchInfo.source).append("&amp;itemType=").append(searchInfo.itemType);
				content.append("\" >").append("更多&gt;&gt;").append("</a>");
			}
			content.append("</p>");
		}
		
		return content.toString();
	}
	
	*//**
	 * 生成点击所有类目列表
	 * @param searchInfo
	 * @param resp
	 * @param server_qgo
	 * @param context
	 * @return
	 *//*
	public static String genAllClassClusterSetContent(SearchInfo searchInfo, CommoditySearchResponse resp, String server_qgo, MobileLifeContextServiceImpl context) { 
		if(resp == null) {
			return "";
		}
		
		//获取当前的navpath
		String navPath = genCurrentNavPath(resp, context);
		
		StringBuilder content = new StringBuilder();

		if(resp.classClusterSet != null 
				&& "cluster".equals(resp.classClusterSet.setName)
				&& resp.classClusterSet.clusterList != null
				&& resp.classClusterSet.clusterList.size() > 0) {

			for(ClusterItem item: resp.classClusterSet.clusterList) {
				if(item.clusterCount > 0) {	//
					content.append("<a href=\"").append(server_qgo).append("aid=s_result&amp;navpath=").append(navPath).append("-0,").append(item.clusterId);
					if(!StringUtil.isEmpty(searchInfo.keyWord)) {
						content.append("&amp;keyword=").append(searchInfo.keyWord);
					}						
					content.append("&amp;src=").append(searchInfo.source).append("&amp;itemType=").append(searchInfo.itemType);
					content.append("\" >").append(context.filterInvalidWML(item.clusterName)).append("(").append(item.clusterCount).append(")").append("</a><br/>");					
				}
			}			
		}		
		return content.toString();
		
	}

	*//**
	 * 生成至当前页面的navpath
	 * @param resp
	 * @return
	 *//*
	public static String genCurrentNavPath(CommoditySearchResponse resp, MobileLifeContextServiceImpl context) {
		String navPath = context.getVariable(CURRENT_NAVPATH);
		if(navPath != null) {
			return navPath;
		}
		
		List<SearchNavPathPo> navPathList = resp.navPathList;
		StringBuilder navPathSB = new StringBuilder();
		if(navPathList != null && navPathList.size() > 0) {			
			for(int i = 0; i < navPathList.size(); i++) {
				SearchNavPathPo item = navPathList.get(i);
				
				//拼接navpath串
				if(i != 0) {
					navPathSB.append("-");
				}
				if(item.itemId == 0) {	//是类目，类目的格式为：0,类目ID
					navPathSB.append("0,").append(item.pathId);
				}
			}
		}
		//放入context
		context.setVariable(CURRENT_NAVPATH, navPathSB.toString());
		
		return navPathSB.toString();
	}
	
	*//**
	 *  <p>按品牌选择：</p>
  		<p><a href="#">only(198)</a>&nbsp;<a href="#">优衣库(187)</a>&nbsp;<a href="#">更多&gt;&gt;</a></p>
  		<p>按款式：</p>
  		<p><a href="#">圆领(40)</a>&nbsp;<a href="#">短款(33)</a>&nbsp;<a href="#">更多&gt;&gt;</a></p>
  		<p>按其它属性选择：</p>
  		<p><a href="#">版型</a>&nbsp;<a href="#">领型</a>&nbsp;<a href="#">更多&gt;&gt;</a></p>
	 * @param searchInfo
	 * @param resp
	 * @param server_qgo
	 * @param context
	 * @return
	 *//*
	public static String genAttrClusterSetContent(SearchInfo searchInfo, CommoditySearchResponse resp, String server_qgo, MobileLifeContextServiceImpl context) {
		if(resp == null) {
			return "";
		}
		
		//获取当前的navpath
		String navPath = genCurrentNavPath(resp, context);
		
		StringBuilder content = new StringBuilder();

		List<ClusterSet> attrClusterSetList = resp.attrClusterSetList;
		if(attrClusterSetList != null && attrClusterSetList.size() > 0) {
			//属性个数
			int attrCount = 0;
			//是否有多于两个的属性
			boolean appendMoreAttr = false;
			
			for(ClusterSet set: attrClusterSetList) {	//多种属性				
				List<ClusterItem> clusterList = set.clusterList;
				if(clusterList != null && clusterList.size() > 0) {

					int valueCount = 0;
					StringBuilder attrContent = new StringBuilder();
					for(int i = 0; i < clusterList.size(); i++) {	//属性有多值
						ClusterItem item = clusterList.get(i);
						if(item.clusterCount > 0) {							
							if(valueCount < 2) {
								attrContent.append("<a href=\"").append(server_qgo).append("aid=s_result&amp;navpath=").append(navPath);
								attrContent.append("-").append(set.attrId).append(",").append(item.clusterId);
								if(!StringUtil.isEmpty(searchInfo.keyWord)) {
									attrContent.append("&amp;keyword=").append(searchInfo.keyWord);
								}						
								attrContent.append("&amp;src=").append(searchInfo.source).append("&amp;itemType=").append(searchInfo.itemType);
								attrContent.append("\" >").append(context.filterInvalidWML(item.clusterName)).append("(").append(item.clusterCount).append(")").append("</a>&nbsp;");
							} else if(valueCount == 2) { //第三个显示更多
								attrContent.append("<a href=\"").append(server_qgo).append("aid=s_attrmore&amp;navpath=").append(navPath);
								attrContent.append("&amp;attrid=").append(set.attrId);
								if(!StringUtil.isEmpty(searchInfo.keyWord)) {
									attrContent.append("&amp;keyword=").append(searchInfo.keyWord);
								}						
								attrContent.append("&amp;src=").append(searchInfo.source).append("&amp;itemType=").append(searchInfo.itemType);
								attrContent.append("\" >").append("更多&gt;&gt;").append("</a>");
								break;
							}
							valueCount++;
						}
					}
					
					if(attrCount < 2 && valueCount > 0) {	//前两个属性正常显示
						content.append("<p>按").append(context.filterInvalidWML(set.setName)).append("选择:</p><p>");		
						content.append(attrContent);
						content.append("</p>");
					} else if(2 <= attrCount && attrCount <= 3 && valueCount > 0){	//第三、四个属性只拼接属性名称
						if(!appendMoreAttr) {	//没有拼接<p>按其它属性选择：</p>
							content.append("<p>按其它属性选择：</p><p>");
							appendMoreAttr = true;
						}
						content.append("<a href=\"").append(server_qgo).append("aid=s_attrmore&amp;navpath=").append(navPath);
						content.append("&amp;attrid=").append(set.attrId);
						if(!StringUtil.isEmpty(searchInfo.keyWord)) {
							content.append("&amp;keyword=").append(searchInfo.keyWord);
						}			
						content.append("&amp;src=").append(searchInfo.source).append("&amp;itemType=").append(searchInfo.itemType);
						content.append("\" >").append(set.setName).append("</a>&nbsp;");
					} else if(valueCount > 0){	//显示更多属性
						content.append("<a href=\"").append(server_qgo).append("aid=s_allattr&amp;navpath=").append(navPath);
						if(!StringUtil.isEmpty(searchInfo.keyWord)) {
							content.append("&amp;keyword=").append(searchInfo.keyWord);
						}				
						content.append("&amp;src=").append(searchInfo.source).append("&amp;itemType=").append(searchInfo.itemType);
						content.append("\" >").append("更多&gt;&gt;").append("</a>&nbsp;");
						break;
					}
					
					if(valueCount > 0) {//属性值有非零个产品
						attrCount++;
					}
				}	
			}
			if(appendMoreAttr) {	//补齐最后一个</p>
				content.append("</p>");
			}
		}
		return content.toString();		
	}
	
	*//**
	 * 获取某个属性下面所有的属性值列表
	 * @param keyWord
	 * @param resp
	 * @param server_ml
	 * @param request
	 * @param attrId
	 * @return
	 *//*
	public static String genSingleAttrClusterSetContent(
			SearchInfo searchInfo, 
			CommoditySearchResponse resp, 
			String server_qgo, 
			MobileLifeContextServiceImpl context,
			String attrId) {
		if(resp == null || StringUtil.isEmpty(attrId)) {
			return "";
		}
		
		//获取当前的navpath
		String navPath = genCurrentNavPath(resp, context);
		
		StringBuilder content = new StringBuilder();

		List<ClusterSet> attrClusterSetList = resp.attrClusterSetList;
		if(attrClusterSetList != null && attrClusterSetList.size() > 0) {
			for(ClusterSet set: attrClusterSetList) {	//多种属性
				if(!attrId.equals(""+set.attrId)) {	//只抓取对应的属性
					continue;
				}
				List<ClusterItem> clusterList = set.clusterList;
				if(clusterList != null && clusterList.size() > 0) {				
					content.append("按").append(context.filterInvalidWML(set.setName)).append("选择:<br/>");
					for(int i = 0; i < clusterList.size(); i++) {	//属性有多值
						ClusterItem item = clusterList.get(i);
						if(item.clusterCount > 0) {	
							content.append("<a href=\"").append(server_qgo).append("aid=s_result&amp;navpath=").append(navPath);
							content.append("-").append(set.attrId).append(",").append(item.clusterId);
							if(!StringUtil.isEmpty(searchInfo.keyWord)) {
								content.append("&amp;keyword=").append(searchInfo.keyWord);
							}						
							content.append("&amp;src=").append(searchInfo.source).append("&amp;itemType=").append(searchInfo.itemType);
							content.append("\" >").append(context.filterInvalidWML(item.clusterName)).append("(").append(item.clusterCount).append(")").append("</a><br/>");
						}
					}
					content.append("<br/>");
				}	
			}
		}
		return content.toString();		
	}
	
	*//**
	 * 生成所有属性及属性值列表
	 * @param searchInfo
	 * @param resp
	 * @param server_qgo
	 * @param context
	 * @return
	 *//*
	public static String genAllAttrClusterSetContent(SearchInfo searchInfo, CommoditySearchResponse resp, String server_qgo, MobileLifeContextServiceImpl context) {
		if(resp == null) {
			return "";
		}
		
		//获取当前的navpath
		String navPath = genCurrentNavPath(resp, context);
		
		StringBuilder content = new StringBuilder();

		List<ClusterSet> attrClusterSetList = resp.attrClusterSetList;
		if(attrClusterSetList != null && attrClusterSetList.size() > 0) {		
			for(ClusterSet set: attrClusterSetList) {	//多种属性				
				List<ClusterItem> clusterList = set.clusterList;
				if(clusterList != null && clusterList.size() > 0) {

					int valueCount = 0;
					StringBuilder attrContent = new StringBuilder();
					for(int i = 0; i < clusterList.size(); i++) {	//属性有多值
						ClusterItem item = clusterList.get(i);
						if(item.clusterCount > 0) {			
							attrContent.append("<a href=\"").append(server_qgo).append("aid=s_result&amp;navpath=").append(navPath);
							attrContent.append("-").append(set.attrId).append(",").append(item.clusterId);
							if(!StringUtil.isEmpty(searchInfo.keyWord)) {
								attrContent.append("&amp;keyword=").append(searchInfo.keyWord);
							}						
							attrContent.append("&amp;src=").append(searchInfo.source).append("&amp;itemType=").append(searchInfo.itemType);
							attrContent.append("\" >").append(context.filterInvalidWML(item.clusterName)).append("(").append(item.clusterCount).append(")").append("</a>&nbsp;");

							valueCount++;
						}
					}
					
					if(valueCount > 0) {	
						content.append("按").append(context.filterInvalidWML(set.setName)).append("选择:<br/>");		
						content.append(attrContent);
						content.append("<br/>");
					}

				}	
			}
		}
		return content.toString();
	}
	
	*//**
	 * 生成排序页面文字提示
	 * <span class="c">默认</span>&nbsp;<a href="#">价格&uarr;</a>&nbsp;<a href="#">销量&darr;</a>
	 * @param keyWord
	 * @param sortType
	 * @param resp
	 * @param server_qgo
	 * @param context
	 * @return
	 *//*
	public static String genSortTip(SearchInfo searchInfo, CommoditySearchResponse resp, String server_qgo, MobileLifeContextServiceImpl context) {
		if(resp == null) {
			return "";
		}
		
		//获取当前的navpath
		String navPath = genCurrentNavPath(resp, context);
		
		String url = server_qgo+"aid=s_result&amp;navpath="+navPath+(StringUtil.isEmpty(searchInfo.keyWord)?"":"&amp;keyword="+searchInfo.keyWord)+
					"&amp;src="+searchInfo.source+"&amp;itemType="+searchInfo.itemType+
					"&amp;sort=";
		String currentLinkBegin = "<span class=\"c\">";
		if(context.isWap10()) {
			currentLinkBegin = "<b>";
		}
		
		String currentLinkEnd = "</span>";
		if(context.isWap10()) {
			currentLinkEnd = "</b>";
		}
//		boolean showFullOrderBy = ProfileManager.getBoolByKey("mobilelife.search_showOrderBy", false);
		boolean showFullOrderBy = false;   //TODO winsonwu
		int sortType = searchInfo.sort;
		if(showFullOrderBy) {
			if(sortType == CommoditySearchConst.SORT_BY_PRICE_A) {	//价格升序
				return "<a href=\""+url+"80"+"\">默认</a>&nbsp;<a href=\""+url+CommoditySearchConst.SORT_BY_PRICE_D+"\">"+currentLinkBegin+"价格↑"+currentLinkEnd+"</a>&nbsp;<a href=\""+url+CommoditySearchConst.SORT_BY_SOLD_COUNT_D+"\">销量↓</a>";
			} else if(sortType == CommoditySearchConst.SORT_BY_PRICE_D) {	//价格降序
				return "<a href=\""+url+"80"+"\">默认</a>&nbsp;<a href=\""+url+CommoditySearchConst.SORT_BY_PRICE_A+"\">"+currentLinkBegin+"价格↓"+currentLinkEnd+"</a>&nbsp;<a href=\""+url+CommoditySearchConst.SORT_BY_SOLD_COUNT_D+"\">销量↓</a>";
			} else if(sortType == CommoditySearchConst.SORT_BY_SOLD_COUNT_D) {	//销量降序
				return "<a href=\""+url+"80"+"\">默认</a>&nbsp;<a href=\""+url+CommoditySearchConst.SORT_BY_PRICE_D+"\">价格↑</a>&nbsp;<a href=\""+url+CommoditySearchConst.SORT_BY_DEFAULT+"\">"+currentLinkBegin+"销量↓"+currentLinkEnd+"</a>";
			} else {	//默认排序
				return currentLinkBegin+"默认"+currentLinkEnd+"&nbsp;<a href=\""+url+CommoditySearchConst.SORT_BY_PRICE_A+"\">价格↑</a>&nbsp;<a href=\""+url+CommoditySearchConst.SORT_BY_SOLD_COUNT_D+"\">销量↓</a>";
			}
		} else {//初期不开销量排序
			if(sortType == CommoditySearchConst.SORT_BY_PRICE_A) {	//价格升序
				return "<a href=\""+url+"80"+"\">默认</a>&nbsp;<a href=\""+url+CommoditySearchConst.SORT_BY_PRICE_D+"\">"+currentLinkBegin+"价格↑"+currentLinkEnd+"</a>";
			} else if(sortType == CommoditySearchConst.SORT_BY_PRICE_D) {	//价格降序
				return "<a href=\""+url+"80"+"\">默认</a>&nbsp;<a href=\""+url+CommoditySearchConst.SORT_BY_PRICE_A+"\">"+currentLinkBegin+"价格↓"+currentLinkEnd+"</a>";
			} else {	//默认排序
				return currentLinkBegin+"默认"+currentLinkEnd+"&nbsp;<a href=\""+url+CommoditySearchConst.SORT_BY_PRICE_A+"\">价格↑</a>";
			}	
		}
	}
}
*/