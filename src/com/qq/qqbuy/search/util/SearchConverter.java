package com.qq.qqbuy.search.util;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.search.po.ClusterItem;
import com.qq.qqbuy.search.po.ClusterSet;
import com.qq.qqbuy.search.po.SearchItem;
import com.qq.qqbuy.search.po.MetaClass;
import com.qq.qqbuy.search.po.NavPath;
import com.qq.qqbuy.search.po.SearchPo;
import com.qq.qqbuy.thirdparty.http.search.po.SearchResult;
import com.qq.qqbuy.thirdparty.http.search.po.SearchResultClass;
import com.qq.qqbuy.thirdparty.http.search.po.SearchResultCluster;
import com.qq.qqbuy.thirdparty.http.search.po.SearchResultItem;
import com.qq.qqbuy.thirdparty.http.search.po.SearchResultPath;

/**
 * @author winsonwu
 * @Created 2012-6-14 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class SearchConverter {

    public final static String IMG_FMT_300X300 = "300x300";

    public final static String IMG_FMT_120X120 = "120x120";

    /******************************************** 新版对象转换 *************************************************************/
    public static SearchPo convert(SearchResult result) {
	SearchPo po = new SearchPo();
	if (result != null) {
	    po.setTotalNum(result.getTotalNum());
		po.setShopList(result.getShopList());
	    // 转换Class
	    List<SearchResultClass> classes = result.getClasses();
	    if (classes != null && classes.size() > 0) {
		List<MetaClass> mcs = po.getClasses();
		for (int i = 0; i < classes.size(); i++) {
		    SearchResultClass clazz = classes.get(i);
		    mcs.add(convert(clazz));
		}
	    }

	    // 转换导航
	    List<SearchResultPath> paths = result.getResultPaths();
	    if (paths != null && paths.size() > 0) {
		List<NavPath> pts = po.getPaths();
		for (int i = 0; i < paths.size(); i++) {
		    SearchResultPath path = paths.get(i);
		    pts.add(convert(path));
		}
	    }

	    LinkedHashMap<String, List<SearchResultCluster>> properties = result
		    .getPropertyClusters();
	    if (properties != null && properties.size() > 0) {
		for (Entry<String, List<SearchResultCluster>> entry : properties
			.entrySet()) {
		    ClusterSet cluster = new ClusterSet();
		    String key = entry.getKey();
		    if (!StringUtil.isEmpty(key)) {
			cluster.setSetName(key);
			List<SearchResultCluster> values = entry.getValue();
			if (values != null && values.size() > 0) {
			    if (StringUtil.isNumeric(values.get(0).getAttrId())) {
				cluster.setAttrId(Long.valueOf(values.get(0)
					.getAttrId())); // 设置属性的attr，取值为所有属性中的第一个，同一属性下不同属性值的attrid一样
			    }
			    for (SearchResultCluster value : values) {
				cluster.getClusterList().add(convert(value));
			    }
			}
			if (SearchConstant.SEARCH_PROPERTY_CLUSTER.equals(key)) {
			    // 转换聚类
			    po.setClassCluster(cluster);
			} else {
			    // 转换属性
			    po.getAttrClusterSetList().add(cluster);
			}
		    }
		}
	    }

	    // 转换Item
	    List<SearchResultItem> items = result.getResultItems();
	    if (items != null && items.size() > 0) {
		Vector<SearchItem> itemPos = new Vector<SearchItem>();
		for (int i = 0; i < items.size(); i++) {
		    SearchResultItem item = items.get(i);
		    itemPos.add(convert(item));
		}
		po.setItems(itemPos);
	    }
	}

	return po;
    }

    private static MetaClass convert(SearchResultClass clazz) {
	MetaClass po = new MetaClass();
	if (clazz != null) {
	    po.setId(StringUtil.toLong(clazz.getClassId(), 0));
	    po.setCount(StringUtil.toLong(clazz.getCount(), 0));
	}
	return po;
    }

    private static NavPath convert(SearchResultPath path) {
	NavPath po = new NavPath();
	if (path != null) {
	    po.setItemId(StringUtil.toLong(path.getItemId(), 0));
	    po.setItemName(path.getItemName());
	    po.setPathId(StringUtil.toLong(path.getAttrId()));
	    po.setPathName(path.getAttrName());
	    po.setPathUrl(path.getPathUrl());

	    po.setPath(genPath(path.getPathUrl()));
	}
	return po;
    }

    private static ClusterItem convert(SearchResultCluster cluster) {
	ClusterItem po = new ClusterItem();
	if (cluster != null) {
	    po.setClusterCount(StringUtil.toLong(cluster.getClusterCount(), 0));
	    po.setClusterId(StringUtil.toLong(cluster.getClusterId(), 0));
	    po.setClusterName(cluster.getClusterName());
	    po.setClusterUrl(cluster.getClusterUrl());

	    po.setPath(genPath(cluster.getClusterUrl()));
	}
	return po;
    }

    // "http://sse1.paipai.com/0,6017-0,243364/s-q3xzz3l--1-1-77-243364--3-4-3----2-2--160-1-0-sf,30.html"
    // //存在keyWord的时候
    // "http://list1.paipai.com/0,204260/l---1-5-77-204260--3-4-3----2-2-512-128-1-0-dtag,1.html"
    // //只提供path的时候
    protected static String[] suffixs = new String[] {
	    "http://sse1.paipai.com/", "http://list1.paipai.com/" };

    private static String genPath(String url) {
	if (url != null) {
	    for (String suffix : suffixs) {
		if (url.startsWith(suffix)) {
		    String temp = url.substring(suffix.length());
		    int index = temp.indexOf('/');
		    if (index > 0 && index < temp.length()) {
			return temp.substring(0, index);
		    }
		}
	    }
	}
	return "";
    }

    private static SearchItem convert(SearchResultItem item) {
		SearchItem po = new SearchItem();
		if (item != null) {
			po.setMainImg(item.getImg());
			// po.setMainImg60(null);
			po.setMainImg80(item.getImg());
			po.setMainImg160(item.getImg160());
			po.setMainImg200(item.getImgL());
			po.setMainImg300(item.getImgLL());
			po.setMainImg120(convert300x300To120x120Img(po.getMainImg300()));
			po.setTitle(item.getTitle());
			po.setLink(item.getLink());
			po.setItemCode(item.getCommId());
			po.setSellerUin(StringUtil.toLong(item.getQq(), 0));
			po.setSellerNickName(item.getNickName());
			po.setSellerCredit(item.getCredit());
			po.setSellerLevel(StringUtil.toLong(item.getStar(), 0));
			po.setExpress(item.getExpress());// 运费
			po.setExpress2(item.getExpress2());
			po.setMailPrice(item.getMailPrice());
			po.setExpressPrice(item.getExpressPrice());
			po.setEmsPrice(item.getEmsPrice());
			po.setPrice(item.getPrice());
			po.setMarketPrice(item.getMarketPrice());
			po.setShopAddr(item.getAddr());
			po.setSoldNum(item.getSaleNum());
			po.setFavNum(item.getFavNum());
			po.setUserValidStatus(item.getUserValidStatus());
			po.setLegend1Status(item.getLegend1Status());
			po.setLegend2Status(item.getLegend2Status());
			po.setLegend3Status(item.getLegend3Status());
			po.setLegend4Status(item.getLegend4Status());
			po.setCodStatus(item.getCashDeliveryStatus());
			po.setRedPacketStatus(item.getVouchersStatus());
			po.setAutoShipStatus(item.getAutoShipStatus());
			po.setValidClothStatus(item.getValidClothStatus());
			po.setQqVipPrice(item.getQqVipPrice());
			po.setPropertyString(item.getPropertyIds());
			long commProperty = StringUtil.toLong(item.getCommProperty(), 0);
			po.setCommProperty(commProperty);// 属性
			// commProperty的属性标识位解析
			po.setCommPrpertyQQShop((commProperty & 0x100) == 0x100 ? (byte) 1
				: 0);
			po.setPromotion((commProperty & 0x8000000000L) != 0 ? (byte) 1 : 0);
			po.setFreeMail((commProperty & SearchConstant.PROP_SHIP_FREE) == SearchConstant.PROP_SHIP_FREE);// 包邮
			String salePromotionInfo = item.getSalePromotionInfo();
			if (!StringUtil.isEmpty(salePromotionInfo)) {
				String[] data = StringUtil.split(salePromotionInfo, "|");
				if (data != null && data.length > 0) {
					if (!StringUtil.isEmpty(data[0])) {
						po.setSupportColorDiamond(true);
						po.setVipDiamondPrice(getVipDiamondPrice(data[0]));
					}
					if (data.length > 1) {
						if (!StringUtil.isEmpty(data[1])) {
							po.setRedPacket(StringUtil.toInt(data[1], 0));
						}
					}
				}
			}
			/**
			 * 添加商品活动标识，43：闪购 44：拍便宜
			 */
			po.setActivePriceType(item.getActivePriceType());
			po.setIstakeCheap(item.getIstakeCheap());
			po.setIsFlashSale(item.getIsFlashSale());
			/**
			 * 添加拍便宜商品开团链接
			 */
			po.setTakeCheapLink(item.getTakeCheapLink());
			/**
			 * 搜索优化新增用户个性标识
			 */
			po.setPersonalizedTypeTag(item.getPersonalizedTypeTag());
		}

		return po;
    }

    private static String getVipDiamondPrice(String data) {
	if (StringUtils.isNotEmpty(data)) {
	    String[] prices = data.split("\\:");
	    if (prices != null && prices.length > 0) {
		String priceLevel = prices[0];
		String[] price1 = priceLevel.split("\\,");
		if (price1 != null && price1.length == 3) {
		    return price1[2];
		}
	    }
	}
	return null;
    }

    /**
     * 
     * @Title: convert300x300To120x120Img
     * 
     * @Description: 300x300尺寸图片url转120x120
     * @param @param imgUrl300x300
     * @param @return 设定文件
     * @return String 返回类型
     * @throws
     */
    private static String convert300x300To120x120Img(String imgUrl300x300) {
	if (StringUtil.isBlank(imgUrl300x300)) {
	    return "";
	} else if (imgUrl300x300.contains(IMG_FMT_300X300)) {
	    String imgUrl120x120 = imgUrl300x300.replace(IMG_FMT_300X300,
		    IMG_FMT_120X120);
	    return imgUrl120x120;
	} else {
	    return imgUrl300x300;
	}
    }
}
