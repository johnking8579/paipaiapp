package com.qq.qqbuy.search.biz.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.CharArrayBuffer;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.PaiPaiConfig;
import com.qq.qqbuy.common.client.http.HttpClient;
import com.qq.qqbuy.common.client.http.HttpResp;
import com.qq.qqbuy.common.client.http.ReqHead;
import com.qq.qqbuy.common.client.log.OnlyTimeLogParser;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.item.po.ItemBO;
import com.qq.qqbuy.search.biz.PicSearchBiz;
import com.qq.qqbuy.thirdparty.idl.item.ItemClient;
import com.qq.qqbuy.thirdparty.idl.item.protocol.FetchItemInfoResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ItemBase;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ItemPo_v2;

/**
 * Created by wanghao5 on 2015/2/26.
 */
public class PicSearchBizImpl implements PicSearchBiz{

    /**
     * 拍图购接口
     */
    private static final String PIC_SEARCH_URL = "http://pp.basicfinder.com/search_api.php";

    /**
     * 颜色购接口
     */
    private static final String COLRO_SEARCH_URL = "http://pp.basicfinder.com/search_color_api.php";

    /**
     * 分类搜索接口
     */
    private static final String CATEGORY_SEARCH_URL = "http://pp.basicfinder.com/cate_api.php";

    /**
     * 根据上传图片搜索类似商品
     * @param file 文件流
     * @param fileName 文件名称
     * @param contentType
     * @param isNeedProxy 是否使用代理
     * @param encode 编码
     * @param connectTimeout 接口数据读取超时时间
     * @param readTimeout
     * @param num 获取商品数量
     * @return
     * @throws BusinessException
     */
    @Override
    public JsonArray picSearchItems(File file,String fileName,String contentType,boolean isNeedProxy,String encode,int connectTimeout, int readTimeout,int num) throws BusinessException {
        JsonArray itemList = new JsonArray();
        CloseableHttpClient httpclient = null;
        /**
         * 增加代理服务器访问外网
         */
        if (!EnvManager.isLocal() && isNeedProxy) {
            InetSocketAddress addr = PaiPaiConfig.getHttpProxyHostQgo();
            String hostName = addr.getHostName();
            int port = addr.getPort();
            HttpHost proxy = new HttpHost(hostName, port);
            Log.run.info("Http Proxy is:" + hostName + ":" + port);
            DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);
            httpclient = HttpClients.custom().setRoutePlanner(routePlanner).build();
        } else {
            httpclient = HttpClients.createDefault();
        }
        try {
            /**
             * 创建post请求，并添加文件上传参数
             */
            HttpPost httppost = new HttpPost(PIC_SEARCH_URL);
            FileBody bin = new FileBody(file,ContentType.MULTIPART_FORM_DATA,fileName);
            String reqnum = String.valueOf(num <= 0? 50 : num);
            StringBody stringBody = new StringBody(reqnum, ContentType.TEXT_PLAIN);
            HttpEntity reqEntity = MultipartEntityBuilder.create().addPart("image", bin).addPart("reqnum",stringBody).build();
            httppost.setEntity(reqEntity);
            /**
             * 设置超时时间
             */
            RequestConfig config = RequestConfig.custom().setConnectionRequestTimeout(connectTimeout)//
                    .setConnectTimeout(connectTimeout).setSocketTimeout(readTimeout).build();
            httppost.setConfig(config);
            /**
             * 执行post请求
             */
            CloseableHttpResponse response = httpclient.execute(httppost);
            try {
                HttpEntity resEntity = response.getEntity();
                String result = "";
                if (resEntity != null) {
                    InputStream content = resEntity.getContent();
                    InputStreamReader reader = new InputStreamReader(content, Charset.forName(encode));
                    int i = (int)resEntity.getContentLength();
                    if(i < 0) {
                        i = 4096;
                    }
                    CharArrayBuffer buffer = new CharArrayBuffer(i);
                    char[] tmp = new char[1024];
                    int len;
                    while((len = reader.read(tmp)) != -1) {
                        buffer.append(tmp, 0, len);
                    }
                    result = buffer.toString();
                }
                Log.run.info("PicSearchResult is :" + result);
                EntityUtils.consume(resEntity);
                itemList = getItemJsonArray(result);
            } finally {
                response.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return itemList;
    }

    /**
     * 根据选择的颜色搜索商品
     * @param red 颜色RGB格式中的Red
     * @param green 颜色RGB格式中的Green
     * @param blue 颜色RGB格式中的Blue
     * @param isNeedProxy 是否使用代理
     * @param encode 编码
     * @param connectTimeout 接口数据读取超时时间
     * @param readTimeout
     * @param num 获取商品数量
     * @return
     * @throws BusinessException
     */
    @Override
    public JsonArray colorSearchItems(String red,String green,String blue, boolean isNeedProxy, String encode,int connectTimeout, int readTimeout,int num) throws BusinessException {
        JsonArray itemList = new JsonArray();
        CloseableHttpClient httpclient = null;
        /**
         * 增加代理服务器访问外网
         */
        if (!EnvManager.isLocal() && isNeedProxy) {
            InetSocketAddress addr = PaiPaiConfig.getHttpProxyHostQgo();
            String hostName = addr.getHostName();
            int port = addr.getPort();
            HttpHost proxy = new HttpHost(hostName, port);
            Log.run.info("Http Proxy is:" + hostName + ":" + port);
            DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);
            httpclient = HttpClients.custom().setRoutePlanner(routePlanner).build();
        } else {
            httpclient = HttpClients.createDefault();
        }
        try {
            /**
             * 设置post请求并添加参数
             */
            HttpPost httppost = new HttpPost(COLRO_SEARCH_URL);
            String reqnum = String.valueOf(num <= 0? 50 : num);
            List<NameValuePair> parames = new ArrayList<NameValuePair>();
            parames.add(new BasicNameValuePair("R",red));
            parames.add(new BasicNameValuePair("G",green));
            parames.add(new BasicNameValuePair("B",blue));
            parames.add(new BasicNameValuePair("reqnum",reqnum));
            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(parames,encode);
            httppost.setEntity(formEntity);
            /**
             * 设置超时时间
             */
            RequestConfig config = RequestConfig.custom().setConnectionRequestTimeout(connectTimeout)//
                                    .setConnectTimeout(connectTimeout).setSocketTimeout(readTimeout).build();
            httppost.setConfig(config);
            /**
             * 执行post请求
             */
            CloseableHttpResponse response = httpclient.execute(httppost);
            try {
                HttpEntity resEntity = response.getEntity();
                String result = "";
                if (resEntity != null) {
                    InputStream content = resEntity.getContent();
                    InputStreamReader reader = new InputStreamReader(content, Charset.forName(encode));
                    int i = (int)resEntity.getContentLength();
                    if(i < 0) {
                        i = 4096;
                    }
                    CharArrayBuffer buffer = new CharArrayBuffer(i);
                    char[] tmp = new char[1024];
                    int len;
                    while((len = reader.read(tmp)) != -1) {
                        buffer.append(tmp, 0, len);
                    }
                    result = buffer.toString();
                }
                System.out.println(result);
                Log.run.info("PicSearchResult is :" + result);
                EntityUtils.consume(resEntity);
                itemList = getItemJsonArray(result);
            } finally {
                response.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return itemList;
    }

    /**
     * 根据图片分类搜索商品
     *
     * @param category         商品分类
     * @param image            文件流
     * @param imageFileName    文件名称
     * @param imageContentType 类型
     * @param isNeedProxy      是否使用代理
     * @param encode           编码
     * @param connectTimeOut   连接时间
     * @param readTimeOut      读取时间
     * @param num              返回商品数量
     * @return
     */
    @Override
    public JsonArray cateSearchItems(long category, File image, String imageFileName, String imageContentType, boolean isNeedProxy, String encode, int connectTimeOut, int readTimeOut, int num) throws BusinessException {

        JsonArray itemList = new JsonArray();
        try {
            HttpClient httpClient = new HttpClient();
            httpClient.setTimeout(connectTimeOut, readTimeOut);
            /**
             * 构建参数对象
             */
            String reqnum = String.valueOf(num <= 0? 50 : num);
            StringBody number = new StringBody(reqnum, ContentType.TEXT_PLAIN);
            StringBody cate = new StringBody(String.valueOf(category), ContentType.TEXT_PLAIN);
            FileBody bin = new FileBody(image,ContentType.MULTIPART_FORM_DATA,imageFileName);
            /**
             * 生成请求头
             */
            String boundary = HttpClient.generateBoundary();
            ReqHead head = new ReqHead().setContentType("multipart/form-data;boundary=" + boundary);
            /**
             * 生成请求体
             */
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            MultipartEntityBuilder.create()
                    .setBoundary(boundary)
                    .addPart("reqnum", number)
                    .addPart("cate",cate)
                    .addPart("image", bin)
                    .build().writeTo(bos);
            ByteArrayInputStream body = new ByteArrayInputStream(bos.toByteArray());
            /**
             * 执行post请求
             */
            String url = "";
            if(category == 0){//所有商品
                url = PIC_SEARCH_URL;
            } else {
                url = CATEGORY_SEARCH_URL;
            }
            HttpResp resp = httpClient.post(url, head, body, new OnlyTimeLogParser());
            byte[] response = resp.getResponse();
            String result = new String(response);
            Log.run.info("PicSearchResult is :" + result);
            itemList = getItemJsonArray(result);
        } catch (Exception e) {
            Log.run.error("PicSearchBizImpl#cateSearchItems error:"+e.getMessage());
            e.printStackTrace();
        }
        return itemList;
    }

    /**
     * 根据返回结果解析获取ItemCode并获取商品信息
     * @param result
     * @throws IOException
     */
    private JsonArray getItemJsonArray(String result) throws IOException {
        JsonArray itemList = new JsonArray();
        ObjectMapper jsonMapper = new ObjectMapper();
        JsonNode rootNode = jsonMapper.readValue(result, JsonNode.class);
        int errorCode = rootNode.path("retcode").getIntValue();
        if(errorCode != 200){
            String errmsg = rootNode.path("errmsg").asText();
            throw BusinessException.createInstance(errorCode, errmsg);
        }
        JsonNode cods = rootNode.path("data");
        String text = cods.asText();
        JsonNode itemNodes = jsonMapper.readValue(text, JsonNode.class);
        if(!itemNodes.isNull()){
            Iterator<JsonNode> items = itemNodes.path("result").getElements();
            while (items.hasNext()){
                JsonNode item = items.next();
                String src = item.path("src").asText();
                if(StringUtil.isNotBlank(src)){
                    String img = "http://img0.paipaiimg.com/00000000/"+ src;
                    String[] split = src.split("-");
                    String s = split[2];
                    int end = s.indexOf(".");
                    String itemcode = split[2].substring(0, end);
                    String title = "";
                    long price = 0l;
                    JsonObject json = new JsonObject();
                    json.addProperty("itemCode",itemcode);
                    json.addProperty("img",img);
                    ItemBO itemBO = getItemByItemCode(itemcode);
                    if(itemBO != null){
                        title = itemBO.getItemName();
                        price = itemBO.getItemPrice();
                        json.addProperty("title",title);
                        json.addProperty("price",price);
                        itemList.add(json);
                    }
                }
            }
        }
        return itemList;
    }

    /**
     * 根据商品ID获取商品基本信息
     * @param itemcode
     * @return
     */
    private ItemBO getItemByItemCode(String itemcode) {
        ItemBO ret = null;
        if (StringUtil.isNotBlank(itemcode)) {
            FetchItemInfoResp resp = ItemClient.fetchItemInfo(itemcode);
            ItemPo_v2 itemPo = null;
            ItemBase oItemBase = null;
            if (resp != null && resp.getResult() == 0
                    && (itemPo = resp.getItemPo()) != null
                    && (oItemBase = itemPo.getOItemBase()) != null
                    && StringUtil.isNotEmpty(oItemBase.getTitle())
                    && oItemBase.getPrice()>0 &&
                    (oItemBase.getState()==2 || oItemBase.getState()==7 || oItemBase.getState()==64)) {
                ret = new ItemBO();
                ret.setItemCode(oItemBase.getItemId());
                ret.setItemName(oItemBase.getTitle());
                ret.setItemPrice(oItemBase.getPrice());
                ret.setSellerUin(oItemBase.getQqUin());
            }
        }
        return ret;
    }

    public static void main(String[] args) {
       /* String src = "item-53F05007-52B1268B00000000040100003CF3A5F4.3.jpg";
        String[] split = src.split("-");
        String s = split[2];
        int end = s.indexOf(".");
        String itemcode = split[2].substring(0, end);*/
        int connectTimeOut = 10000;
        int readTimeOut = 10000;
        String red = "200";
        String green = "200";
        String blue = "100";
        PicSearchBiz biz = new PicSearchBizImpl();
        File file = new File("D:/test1.jpg");
        String fileName = "test1.jpg";
        String contentType = "";
        String encode = "utf8";
        boolean isNeedProxy = false;
        int num = 50;
        long cate = 1;
        biz.picSearchItems(file,fileName,contentType,isNeedProxy,encode,connectTimeOut,readTimeOut,num);
        biz.colorSearchItems(red,green,blue,isNeedProxy,encode,connectTimeOut,readTimeOut,num);
        biz.cateSearchItems(cate,file,fileName,contentType,isNeedProxy,encode,connectTimeOut,readTimeOut,num);
    }
}
