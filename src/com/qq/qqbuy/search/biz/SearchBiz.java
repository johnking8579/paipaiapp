package com.qq.qqbuy.search.biz;

import com.qq.qqbuy.search.po.SearchPo;
import com.qq.qqbuy.thirdparty.http.search.po.SearchCondition;

/**
 * @author winsonwu
 * @Created 2012-12-20
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public interface SearchBiz {
	
	public SearchPo searchList(SearchCondition condition, Long wid,int versionCode);
	
	public SearchPo searchMoreCluster(SearchCondition condition,int versionCode);

}
