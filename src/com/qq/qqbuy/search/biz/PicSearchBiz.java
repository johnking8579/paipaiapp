package com.qq.qqbuy.search.biz;

import com.google.gson.JsonArray;
import com.qq.qqbuy.common.exception.BusinessException;

import java.io.File;

/**
 * Created by wanghao5 on 2015/2/26.
 */
public interface PicSearchBiz {

    /**
     * 根据上传图片搜索类似商品
     * @return
     * @throws BusinessException
     * @param file 文件流
     * @param fileName 文件名称
     * @param isNeedProxy 是否使用代理
     * @param encode 编码
     * @param connectTimeout 接口链接超时时间
     * @param connectTimeout 接口数据读取超时时间
     * @param num 获取商品数量
     */
    public JsonArray picSearchItems(File file,String fileName,String contentType,boolean isNeedProxy, String encode,int connectTimeout, int readTimeout,int num) throws BusinessException;

    /**
     * 根据选择的颜色搜索商品
     * @param red 颜色RGB格式中的Red
     * @param green 颜色RGB格式中的Green
     * @param blue 颜色RGB格式中的Blue
     * @param isNeedProxy 是否使用代理
     * @param encode 编码
     * @param connectTimeout 接口链接超时时间
     * @param connectTimeout 接口数据读取超时时间
     * @param num 获取商品数量
     * @return 商品信息集合
     * @throws BusinessException
     */
    public JsonArray colorSearchItems(String red,String green,String blue, boolean isNeedProxy, String encode,int connectTimeout, int readTimeout,int num) throws BusinessException;

    /**
     * 根据图片分类搜索商品
     * @param category 商品分类
     * @param image 文件流
     * @param imageFileName 文件名称
     * @param imageContentType 类型
     * @param isNeedProxy 是否使用代理
     * @param encode 编码
     * @param connectTimeOut 连接时间
     * @param readTimeOut 读取时间
     * @param num 返回商品数量
     * @return
     */
    public JsonArray cateSearchItems(long category, File image, String imageFileName, String imageContentType, boolean isNeedProxy, String encode, int connectTimeOut, int readTimeOut, int num) throws BusinessException;
}
