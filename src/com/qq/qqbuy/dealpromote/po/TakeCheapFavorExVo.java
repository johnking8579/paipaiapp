package com.qq.qqbuy.dealpromote.po;

public class TakeCheapFavorExVo {

	private int available;//是否满足条件
	private String desc;//优惠信息描述
	private long favMoney;// 优惠面值
	private long flag;			// 优惠标识
	private long id;//优惠ID
	private int mailFree;//是否包快递标识，0：不包快递 1：包快递
	private long minimum;//使用门槛
	private String msg;//显示信息
	private int selected;//是否选中标识，0：未选中 1：选中
	private long type;//优惠类型
	private String indate;//日期

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public long getFavMoney() {
		return favMoney;
	}

	public void setFavMoney(long favMoney) {
		this.favMoney = favMoney;
	}

	public long getFlag() {
		return flag;
	}

	public void setFlag(long flag) {
		this.flag = flag;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getMailFree() {
		return mailFree;
	}

	public void setMailFree(int mailFree) {
		this.mailFree = mailFree;
	}

	public long getMinimum() {
		return minimum;
	}

	public void setMinimum(long minimum) {
		this.minimum = minimum;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public int getSelected() {
		return selected;
	}

	public void setSelected(int selected) {
		this.selected = selected;
	}

	public long getType() {
		return type;
	}

	public void setType(long type) {
		this.type = type;
	}

	public int getAvailable() {
		return available;
	}

	public void setAvailable(int available) {
		this.available = available;
	}

	public String getIndate() {
		return indate;
	}

	public void setIndate(String indate) {
		this.indate = indate;
	}
}
