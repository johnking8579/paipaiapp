package com.qq.qqbuy.dealpromote.po;

import java.util.Vector;
import java.util.Map;
import java.util.HashMap;

import com.paipai.lang.uint16_t;
import com.paipai.lang.uint64_t;
import com.qq.qqbuy.common.constant.BizConstant;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.deal.po.ShipCalcVo;
import com.qq.qqbuy.deal.util.DealConstant;
import com.qq.qqbuy.dealpromote.util.DealPromoteUtil;
import com.qq.qqbuy.thirdparty.idl.promote.protocol.ItemPromote;
import com.qq.qqbuy.thirdparty.idl.promote.protocol.FavorFilter;
import com.qq.qqbuy.thirdparty.idl.promote.protocol.DealPromote;
import com.qq.qqbuy.thirdparty.idl.userinfo.ApiUserClient;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.apiuser.APIUserProfile;


/**
 * 
 * @author candeladiao
 * @Created 2013-09-09 <a href="mailto:289303723@qq.com">Candela</a>
 */
public class DealPromotePo {
	private int errorCode = 0;
	private String errorMsg = "";
	private long buyerUin; 		// 买家QQ号
	private long reqSource;		// 请求场景
	
	/**
	 * 批价订单列表
	 *
	 * 版本 >= 0
	 */
	private Vector<DealPromote> dealPromoteList = new Vector<DealPromote>();
	
	private Map<Long,ShipCalcVo> mailInfos = new HashMap<Long, ShipCalcVo>();
	
	private Map<Long,Long> shopTypes = new HashMap<Long, Long>();
	
	private static final int PROMOTE_PARAM_EMPTY_ERROR = 100;
	private static final int PROMOTE_DEAL_PARAM_ERROR = 101;
//	private static final int PROMOTE_FAVOR_PARAM_ERROR = 102;
	private static final int PROMOTE_ITEM_PARAM_ERROR = 103;
	
	public DealPromotePo(){
		super();
	}
	
	public static DealPromotePo createInstance(long buyerUin, String dealListStr, int ver,int useRedPackage) {
		if (dealListStr == null) {
			return null;
		}
		
		DealPromotePo po = new DealPromotePo();
		String[] dealStrList = dealListStr.split(",");
		if (dealStrList.length == 0){
			po.setErrorCode(PROMOTE_PARAM_EMPTY_ERROR);
			po.setErrorMsg("参数列表为空");
			return po;
		}
		
		po.setBuyerUin(buyerUin);
		for (int i = 0; i < dealStrList.length; i++){
			String dealItemStr = dealStrList[i];
			String[] dealStr = dealItemStr.split("~");
			if ((ver == 0 && dealStr.length != 7) || (ver > 0 && dealStr.length != 8) || ver >4 && dealStr.length != 9) {
				po.setErrorCode(PROMOTE_DEAL_PARAM_ERROR);
				po.setErrorMsg("订单参数错误");
				return po;
			}
			
			int index = 0;
			DealPromote dealPromote = new DealPromote();
			
			// 订单ID
			dealPromote.setDealId(StringUtil.toLong(dealStr[index], 0));
			dealPromote.setSeqId(System.currentTimeMillis() / 1000);
			
			// 卖家QQ
			index++;
			long sellerUin = StringUtil.toLong(dealStr[index], 0);
			dealPromote.setSellerUin(sellerUin);
			dealPromote.setBuyerUin(buyerUin);
			// 场景ID add by wanghao 2015/04/20
			long sceneId = 0;
			if(ver > 4){
				index++;
				sceneId = StringUtil.toLong(dealStr[index], 0);
			}
			// 场景ID add by wanghao 2015/04/20
			dealPromote.setSceneId(sceneId);
			// 是否货到付款
			index++;
			dealPromote.setPayOnReciew(StringUtil.toLong(dealStr[index], 0));
			
			// 是否参加促销
			if (ver > 0) {
				index++;
				dealPromote.setMarketStat(StringUtil.toLong(dealStr[index], 0));
			}
			
			// 快递费
			index++;
			if (StringUtil.toLong(dealStr[index], 0) > 0) {
				ShipCalcVo shipCalc = ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_COD_ZJS, StringUtil.toLong(dealStr[index], 0));
				po.getMailInfos().put(dealPromote.getDealId(), shipCalc);
			}
			
			// 是否网购卖家
			index++;
			long shopType = StringUtil.toInt(dealStr[index], 0);
			po.getShopTypes().put(sellerUin, shopType);
			
			if ((shopType & BizConstant.SHOP_B2C_SELLER) > 0) {
				dealPromote.setUserPropery(1);// 网购卖家
			} else {
				/**
				 * 当useRedPackage == 1时批价使用红包
				 */
				if(useRedPackage == 1 && hasRedPackageUser(buyerUin)){
					dealPromote.setUserPropery(2);// 拍拍卖家
				}else{
					dealPromote.setUserPropery(0);// 拍拍卖家
				}
			}
			
			// 订单维度优惠列表
			index++;
			String favorItemStr = dealStr[index];
			String[] favorPropertyStr = favorItemStr.split("\\$");
			for (String favorStr : favorPropertyStr) {
				String [] propertyStr = favorStr.split(";");
				if (propertyStr.length >= 2){
					FavorFilter favorFilter = new FavorFilter();
					
					favorFilter.setType(StringUtil.toInt(propertyStr[0], 0));
					
					for (int j = 1; j < propertyStr.length; j++) {
						String favorId = propertyStr[j];
						favorFilter.getFavorIdList().add(new uint64_t(StringUtil.toLong(favorId, 0)));
					}
					
					dealPromote.getFavorFilterList().add(favorFilter);
				}
			}
			
			
			// 商品列表
			int promotion = 1;
			index++;
			String ItemPromoteStr = dealStr[index];
			String[] itemPropertyStr = ItemPromoteStr.split("\\$");
			for (String itemStr  : itemPropertyStr) {
				ItemPromote itemPromote = new ItemPromote();
				String [] propertyStr = itemStr.split(";");
				
				if (propertyStr.length != 5) {
					po.setErrorCode(PROMOTE_ITEM_PARAM_ERROR);
					po.setErrorMsg("商品参数错误：");
					return po;
				}
					
				if (!StringUtil.isEmpty(propertyStr[0])) {
					itemPromote.setItemId(propertyStr[0]);
				}
				
				itemPromote.setItemCnt(StringUtil.toLong(propertyStr[1], 0));
				itemPromote.setPriceType(StringUtil.toInt(propertyStr[2], 0));

				if (!StringUtil.isEmpty(propertyStr[3])) {
					itemPromote.setProperty(propertyStr[3]);
				}
				
				if (!StringUtil.isEmpty(propertyStr[4]) && StringUtil.toLong(propertyStr[4], 0) != 0) {
					// 红包
					FavorFilter favorFilter = new FavorFilter();
					
					favorFilter.setType(DealPromoteUtil.ORDER_PROMOTE_FAVOR_CMDY_REDPACKET);
					favorFilter.getFavorIdList().add(new uint64_t(StringUtil.toLong(propertyStr[4], 0)));
					itemPromote.getFavorFilterList().add(favorFilter);
				}
				
				// 兼容老版本，取商品属性，判断商品是否参加促销 
				if (ver == 0 && !DealPromoteUtil.getPromotionInfoOfItem(itemPromote.getItemId())){
					promotion = 0;
				}
			
				dealPromote.getItemPromoteList().add(itemPromote);
			}
			
			// 兼容老版本
			if (ver == 0) {
				dealPromote.setMarketStat(promotion);
			}
			
			po.getDealPromoteList().add(dealPromote);
		}
		
		return po;
	}

	/**
	 * 判断用户是否是红包用户
	 */
	public static boolean hasRedPackageUser(long buyerUin) {

		boolean hasRedPackage = false;
		APIUserProfile profile = ApiUserClient.getUserSimpleInfo(buyerUin);
		if (profile != null) {
			int buyerProp = 0;
			uint16_t key = new uint16_t(121);
			if (profile.getProp().get(key) != null) {
				buyerProp = profile.getProp().get(key).intValue();
			}
			if (buyerProp == 1){
				hasRedPackage = true;
			}
		}
		return hasRedPackage;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public long getBuyerUin() {
		return buyerUin;
	}

	public void setBuyerUin(long buyerUin) {
		this.buyerUin = buyerUin;
	}
	
	public long getReqSource() {
		return reqSource;
	}

	public void setReqSource(long reqSource) {
		this.reqSource = reqSource;
	}

	public Map<Long, ShipCalcVo> getMailInfos() {
		return mailInfos;
	}

	public void setMailInfos(Map<Long, ShipCalcVo> mailInfos) {
		this.mailInfos = mailInfos;
	}

	public Map<Long, Long> getShopTypes() {
		return shopTypes;
	}

	public void setShopTypes(Map<Long, Long> shopTypes) {
		this.shopTypes = shopTypes;
	}

	public Vector<DealPromote> getDealPromoteList() {
		return dealPromoteList;
	}

	public void setDealPromoteList(Vector<DealPromote> dealPromoteList) {
		this.dealPromoteList = dealPromoteList;
	}

//	/********************************************* 对象转换区 **************************************/
//	public static CmdyMakeOrderPo deSerialize(MakeOrderPo req) {
//	}
	
}
