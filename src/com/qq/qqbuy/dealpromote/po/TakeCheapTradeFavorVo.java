package com.qq.qqbuy.dealpromote.po;

import com.qq.qqbuy.deal.po.ShipCalcVo;

import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class TakeCheapTradeFavorVo {
	private String orderId;	// 订单ID,分单时生成
	private long shopId;	// 卖家QQ
	private String shopName;// 店铺名称
	private long shopProp;//店铺属性
	private long shopType;	// 店铺类型
	private long amount;	// 订单总价
	private long errType;	// 错误类型
	private String errMsg;	// 错误信息
	private long postStat;	// 是否包邮：1包邮  0不包邮
	private long payOnReceive;// 是否货到付款
	private long promotion;	// 是否参加促销
	private int fapiao;//是否开发票
	//订单运费信息
	private List<ShipCalcVo> shipCalcVos = new LinkedList<ShipCalcVo>();
	//优惠信息列表
	private Vector<TakeCheapFavorExVo> salePromotion = new Vector<TakeCheapFavorExVo>();
	//店铺优惠券列表
	private Vector<TakeCheapFavorExVo> shopCoupon = new Vector<TakeCheapFavorExVo>();
	//网购优惠券列表
	private Vector<TakeCheapFavorExVo> wgCoupon = new Vector<TakeCheapFavorExVo>();
	//积分红包列表
	private Vector<TakeCheapFavorExVo> balance = new Vector<TakeCheapFavorExVo>();
	//批价商品信息列表
	private Vector<TakeCheapItemPromoteResultVo> commodity = new Vector<TakeCheapItemPromoteResultVo>();

	public TakeCheapTradeFavorVo() {
	}

	public long getShopId() {
		return shopId;
	}

	public void setShopId(long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public long getShopType() {
		return shopType;
	}

	public void setShopType(long shopType) {
		this.shopType = shopType;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public long getErrType() {
		return errType;
	}

	public void setErrType(long errType) {
		this.errType = errType;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public long getPostStat() {
		return postStat;
	}

	public void setPostStat(long postStat) {
		this.postStat = postStat;
	}

	public long getPayOnReceive() {
		return payOnReceive;
	}

	public void setPayOnReceive(long payOnReceive) {
		this.payOnReceive = payOnReceive;
	}

	public long getPromotion() {
		return promotion;
	}

	public void setPromotion(long promotion) {
		this.promotion = promotion;
	}

	public int getFapiao() {
		return fapiao;
	}

	public void setFapiao(int fapiao) {
		this.fapiao = fapiao;
	}

	public Vector<TakeCheapFavorExVo> getSalePromotion() {
		return salePromotion;
	}

	public void setSalePromotion(Vector<TakeCheapFavorExVo> salePromotion) {
		this.salePromotion = salePromotion;
	}

	public Vector<TakeCheapItemPromoteResultVo> getCommodity() {
		return commodity;
	}

	public void setCommodity(Vector<TakeCheapItemPromoteResultVo> commodity) {
		this.commodity = commodity;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Vector<TakeCheapFavorExVo> getShopCoupon() {
		return shopCoupon;
	}

	public void setShopCoupon(Vector<TakeCheapFavorExVo> shopCoupon) {
		this.shopCoupon = shopCoupon;
	}

	public Vector<TakeCheapFavorExVo> getWgCoupon() {
		return wgCoupon;
	}

	public void setWgCoupon(Vector<TakeCheapFavorExVo> wgCoupon) {
		this.wgCoupon = wgCoupon;
	}

	public Vector<TakeCheapFavorExVo> getBalance() {
		return balance;
	}

	public void setBalance(Vector<TakeCheapFavorExVo> balance) {
		this.balance = balance;
	}

	public long getShopProp() {
		return shopProp;
	}

	public void setShopProp(long shopProp) {
		this.shopProp = shopProp;
	}

	public List<ShipCalcVo> getShipCalcVos() {
		return shipCalcVos;
	}

	public void setShipCalcVos(List<ShipCalcVo> shipCalcVos) {
		this.shipCalcVos = shipCalcVos;
	}
}
