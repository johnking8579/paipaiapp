package com.qq.qqbuy.dealpromote.po;

/**
 * 拍便宜批价公共信息对象
 * Created by wanghao5 on 2014/12/30.
 */
public class TakeCheapCommPo {
    /**
     * 转换异常码
     */
    private int errorCode = 0;
    /**
     * 转换异常信息
     */
    private String errorMsg = "";
    /**
     * 商品编码
     */
    private String itemCode = "";
    /**
     * sku串
     */
    private String attrStr = "";
    /**
     * 购买数量
     */
    private int buyNum = 0;
    /**
     * skuId
     */
    private long skuId = 0;

    private static final int PROMOTE_PARAM_EMPTY_ERROR = 100;
    private static final int PROMOTE_DEAL_PARAM_ERROR = 101;

    /**
     * 团购信息串转换为订单对象
     * @param commlist
     * @return
     */
    public static TakeCheapCommPo ctreateInstaance(String commlist) {

        if (commlist == null) {
            return null;
        }
        TakeCheapCommPo dealPo = new TakeCheapCommPo();
        String[] dealStrList = commlist.split(",");
        int length = dealStrList.length;
        if (length == 0){
            dealPo.setErrorCode(PROMOTE_PARAM_EMPTY_ERROR);
            dealPo.setErrorMsg("参数列表为空");
            return dealPo;
        }
        if (length != 4) {
            dealPo.setErrorCode(PROMOTE_DEAL_PARAM_ERROR);
            dealPo.setErrorMsg("订单参数错误");
            return dealPo;
        }
        dealPo.setItemCode(dealStrList[0]);
        dealPo.setAttrStr(dealStrList[1]);
        dealPo.setBuyNum(Integer.parseInt(dealStrList[2]));
        dealPo.setSkuId(Long.parseLong(dealStrList[3]));
        return dealPo;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getAttrStr() {
        return attrStr;
    }

    public void setAttrStr(String attrStr) {
        this.attrStr = attrStr;
    }

    public int getBuyNum() {
        return buyNum;
    }

    public void setBuyNum(int buyNum) {
        this.buyNum = buyNum;
    }

    public long getSkuId() {
        return skuId;
    }

    public void setSkuId(long skuId) {
        this.skuId = skuId;
    }
}
