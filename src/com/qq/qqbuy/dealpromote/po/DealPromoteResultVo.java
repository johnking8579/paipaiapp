package com.qq.qqbuy.dealpromote.po;

import java.util.Vector;

public class DealPromoteResultVo {
	private Vector<FavorExVo> FavorExList = new Vector<FavorExVo>(); // 订单维度优惠列表
	private Vector<PromoteLogVo> PromoteLogList = new Vector<PromoteLogVo>(); // 订单维度批价流水
	 
	 
	public Vector<FavorExVo> getFavorExList() {
		return FavorExList;
	}
	public void setFavorExList(Vector<FavorExVo> favorExList) {
		FavorExList = favorExList;
	}
	public Vector<PromoteLogVo> getPromoteLogList() {
		return PromoteLogList;
	}
	public void setPromoteLogList(Vector<PromoteLogVo> promoteLogList) {
		PromoteLogList = promoteLogList;
	}
	 
	 
}
