package com.qq.qqbuy.dealpromote.po;

import java.util.Vector;

import com.qq.qqbuy.deal.po.ShipCalcVo;

public class TradeFavorVo {
	private long dealId;	// 订单ID,分单时生成
	private long sellerUin;	// 卖家QQ
	private String shopName;// 店铺名称
	private long shopType;	// 店铺类型
	private long tradeFee;	// 订单总价
	private long favorFee;	// 订单优惠总价
	private long errType;	// 错误类型
	private String errMsg;	// 错误信息
	private long postStat;	// 是否包邮：1包邮  0不包邮
	private long payOnReceive;// 是否货到付款
	private long promotion;	// 是否参加促销
	private int isSupportWXPay ;//是否支持微信支付  0 不支持    1 支持
	
	private Vector<ShipCalcVo> shipCalcInfos = new Vector<ShipCalcVo>();  	// 运送方式
	private Vector<FavorExVo> shopCoupons = new Vector<FavorExVo>();		// 店铺优惠券
	private Vector<FavorExVo> shopPromotions = new Vector<FavorExVo>();		// 店铺促销
	private Vector<FavorExVo> cashCoupons = new Vector<FavorExVo>();		// 代金券
	private Vector<FavorExVo> otherCoupons = new Vector<FavorExVo>();		// 其他优惠
	
//	private Vector<ItemPromoteFavorVo> itemPromoteFavors = new Vector<ItemPromoteFavorVo>(); 	// 商品维度可选优惠集合
//	private Vector<DealPromoteFavorVo> dealPromoteFavors = new Vector<DealPromoteFavorVo>();	// 订单维度可选优惠集合
	private Vector<ItemPromoteResultVo> itemPromoteResult = new Vector<ItemPromoteResultVo>();	// 商品维度批价结果
//	private Vector<DealPromoteResultVo> dealPromoteResult = new Vector<DealPromoteResultVo>();	// 订单维度批价结果
	
	public long getDealId() {
		return dealId;
	}
	public void setDealId(long dealId) {
		this.dealId = dealId;
	}
	public long getSellerUin() {
		return sellerUin;
	}
	public void setSellerUin(long sellerUin) {
		this.sellerUin = sellerUin;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public long getShopType() {
		return shopType;
	}
	public void setShopType(long shopType) {
		this.shopType = shopType;
	}
	public long getTradeFee() {
		return tradeFee;
	}
	public void setTradeFee(long tradeFee) {
		this.tradeFee = tradeFee;
	}
	public long getFavorFee() {
		return favorFee;
	}
	public void setFavorFee(long favorFee) {
		this.favorFee = favorFee;
	}
	public long getErrType() {
		return errType;
	}
	public void setErrType(long errType) {
		this.errType = errType;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public long getPostStat() {
		return postStat;
	}
	public void setPostStat(long postStat) {
		this.postStat = postStat;
	}	
	public long getPayOnReceive() {
		return payOnReceive;
	}
	public void setPayOnReceive(long payOnReceive) {
		this.payOnReceive = payOnReceive;
	}	
	public long getPromotion() {
		return promotion;
	}
	public void setPromotion(long promotion) {
		this.promotion = promotion;
	}
	public Vector<ShipCalcVo> getShipCalcInfos() {
		return shipCalcInfos;
	}
	public void setShipCalcInfos(Vector<ShipCalcVo> shipCalcInfos) {
		this.shipCalcInfos = shipCalcInfos;
	}
	public Vector<FavorExVo> getShopCoupons() {
		return shopCoupons;
	}
	public void setShopCoupons(Vector<FavorExVo> shopCoupons) {
		this.shopCoupons = shopCoupons;
	}
	public Vector<FavorExVo> getShopPromotions() {
		return shopPromotions;
	}
	public void setShopPromotions(Vector<FavorExVo> shopPromotions) {
		this.shopPromotions = shopPromotions;
	}
	public Vector<FavorExVo> getCashCoupons() {
		return cashCoupons;
	}
	public void setCashCoupons(Vector<FavorExVo> cashCoupons) {
		this.cashCoupons = cashCoupons;
	}
	public Vector<FavorExVo> getOtherCoupons() {
		return otherCoupons;
	}
	public void setOtherCoupons(Vector<FavorExVo> otherCoupons) {
		this.otherCoupons = otherCoupons;
	}
	//	public Vector<ItemPromoteFavorVo> getItemPromoteFavors() {
//		return itemPromoteFavors;
//	}
//	public void setItemPromoteFavors(Vector<ItemPromoteFavorVo> itemPromoteFavors) {
//		this.itemPromoteFavors = itemPromoteFavors;
//	}
//	public Vector<DealPromoteFavorVo> getDealPromoteFavors() {
//		return dealPromoteFavors;
//	}
//	public void setDealPromoteFavors(Vector<DealPromoteFavorVo> dealPromoteFavors) {
//		this.dealPromoteFavors = dealPromoteFavors;
//	}
	public Vector<ItemPromoteResultVo> getItemPromoteResult() {
		return itemPromoteResult;
	}
	public void setItemPromoteResult(Vector<ItemPromoteResultVo> itemPromoteResult) {
		this.itemPromoteResult = itemPromoteResult;
	}
	public int getIsSupportWXPay() {
		return isSupportWXPay;
	}
	public void setIsSupportWXPay(int isSupportWXPay) {
		this.isSupportWXPay = isSupportWXPay;
	}
	
	
//	public Vector<DealPromoteResultVo> getDealResult() {
//		return dealResult;
//	}
//	public void setDealResult(Vector<DealPromoteResultVo> dealResult) {
//		this.dealResult = dealResult;
//	}
	
}
