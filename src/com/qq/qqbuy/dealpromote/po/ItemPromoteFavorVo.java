package com.qq.qqbuy.dealpromote.po;

import java.util.Vector;

public class ItemPromoteFavorVo {
	private String itemId; 					// 商品ID
	private String attr;					// 库存属性
	private Vector<FavorExVo> favorExList = new Vector<FavorExVo>();	// 可选优惠列表
//	private Vector<ItemPriceVo> ItemPriceList = new Vector<ItemPriceVo>();
	
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getAttr() {
		return attr;
	}
	public void setAttr(String attr) {
		this.attr = attr;
	}
	public Vector<FavorExVo> getFavorExList() {
		return favorExList;
	}
	public void setFavorExList(Vector<FavorExVo> favorExList) {
		this.favorExList = favorExList;
	}
//	public Vector<ItemPriceVo> getItemPriceList() {
//		return ItemPriceList;
//	}
//	public void setItemPriceList(Vector<ItemPriceVo> itemPriceList) {
//		ItemPriceList = itemPriceList;
//	}
}
