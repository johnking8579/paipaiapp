package com.qq.qqbuy.dealpromote.po;

public class TakeCheapItemPromoteResultVo {
	private String cid;		// 商品ID
	private String cName;	// 商品名称
	private String image;	// 商品主图
	private String stockAttr;		// 商品属性
	private long amount;	// 商品总价
	private long price;			// 商品原价
	private long priceType;		// 批价价格类型
	private String priceDesc;	// 商品价格描述
	private long num;			// 商品购买数量
	private long redsum;		//
	private long maxBuy;//限购数量
	private long stockNum;//库存数量
	private long comdyType;//商品类型

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getStockAttr() {
		return stockAttr;
	}

	public void setStockAttr(String stockAttr) {
		this.stockAttr = stockAttr;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public long getPriceType() {
		return priceType;
	}

	public void setPriceType(long priceType) {
		this.priceType = priceType;
	}

	public String getPriceDesc() {
		return priceDesc;
	}

	public void setPriceDesc(String priceDesc) {
		this.priceDesc = priceDesc;
	}

	public long getNum() {
		return num;
	}

	public void setNum(long num) {
		this.num = num;
	}

	public long getRedsum() {
		return redsum;
	}

	public void setRedsum(long redsum) {
		this.redsum = redsum;
	}

	public long getMaxBuy() {
		return maxBuy;
	}

	public void setMaxBuy(long maxBuy) {
		this.maxBuy = maxBuy;
	}

	public long getStockNum() {
		return stockNum;
	}

	public void setStockNum(long stockNum) {
		this.stockNum = stockNum;
	}

	public long getComdyType() {
		return comdyType;
	}

	public void setComdyType(long comdyType) {
		this.comdyType = comdyType;
	}
}
