package com.qq.qqbuy.dealpromote.po;

import java.util.Vector;

public class DealPromoteFavorVo {
	private Vector<FavorExVo> favorExList = new Vector<FavorExVo>();	// 订单可选优惠列表

	public Vector<FavorExVo> getFavorExList() {
		return favorExList;
	}

	public void setFavorExList(Vector<FavorExVo> favorExList) {
		this.favorExList = favorExList;
	}
	
	
}
