package com.qq.qqbuy.dealpromote.po;

import java.util.Vector;

public class ItemPromoteResultVo {
	private String itemId;		// 商品ID
//	private long result;		// 商品批价结果
	private String itemName;	// 商品名称
	private String mainLogo;	// 商品主图
	private String attr;		// 商品属性
	private long totalMoney;	// 商品总价
//	private long totalFavorFee;	// 商品总优惠
	private long price;			// 商品原价
//	private long promotionPrice;// 商品促销价
	private long priceType;		// 批价价格类型
	private long orderPriceType;// 下单价格类型
	private String priceDesc;	// 商品价格描述 
	private long num;			// 商品购买数量
//	private long favorType;		// 商品单价优惠类型
//	private String favorDesc;	// 商品单价优惠描述
//	private long adjustFee;		// 商品调价
	private long allowFee;		// 商品支持红包的最大面值
//	private long divideFee;		// 商品分摊总的优惠
	private Vector<FavorExVo> redPackages = new Vector<FavorExVo>(); // 红包
	private Vector<FavorExVo> otherFavorList = new Vector<FavorExVo>(); // 其他可选优惠列表
//	private Vector<PromoteLogVo> PromoteLogList = new Vector<PromoteLogVo>();// 商品维度批价流水
//	private long marketPrice;	// 商品市场价
//	private long activeFlag; 	// 大促活动标识
//	private long redPackageValue;// 商品支持红包面值
	
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getMainLogo() {
		return mainLogo;
	}
	public void setMainLogo(String mainLogo) {
		this.mainLogo = mainLogo;
	}
	public String getAttr() {
		return attr;
	}
	public void setAttr(String attr) {
		this.attr = attr;
	}
	public long getTotalMoney() {
		return totalMoney;
	}
	public void setTotalMoney(long totalMoney) {
		this.totalMoney = totalMoney;
	}
//	public long getTotalFavorFee() {
//		return totalFavorFee;
//	}
//	public void setTotalFavorFee(long totalFavorFee) {
//		this.totalFavorFee = totalFavorFee;
//	}
	public long getPrice() {
		return price;
	}
	public void setPrice(long price) {
		this.price = price;
	}
	public long getPriceType() {
		return priceType;
	}
	public void setPriceType(long priceType) {
		this.priceType = priceType;
	}
	
	public long getOrderPriceType() {
		return orderPriceType;
	}
	public void setOrderPriceType(long orderPriceType) {
		this.orderPriceType = orderPriceType;
	}
	public String getPriceDesc() {
		return priceDesc;
	}
	public void setPriceDesc(String priceDesc) {
		this.priceDesc = priceDesc;
	}
	//	public long getPromotionPrice() {
//		return promotionPrice;
//	}
//	public void setPromotionPrice(long promotionPrice) {
//		this.promotionPrice = promotionPrice;
//	}
	public long getNum() {
		return num;
	}
	public void setNum(long num) {
		this.num = num;
	}
	public long getAllowFee() {
		return allowFee;
	}
	public void setAllowFee(long allowFee) {
		this.allowFee = allowFee;
	}
	//	public long getFavorType() {
//		return favorType;
//	}
//	public void setFavorType(long favorType) {
//		this.favorType = favorType;
//	}
//	public String getFavorDesc() {
//		return favorDesc;
//	}
//	public void setFavorDesc(String favorDesc) {
//		this.favorDesc = favorDesc;
//	}
//	public long getAllowFee() {
//		return allowFee;
//	}
//	public void setAllowFee(long allowFee) {
//		this.allowFee = allowFee;
//	}
	public Vector<FavorExVo> getRedPackages() {
		return redPackages;
	}
	public void setRedPackages(Vector<FavorExVo> redPackages) {
		this.redPackages = redPackages;
	}
	public Vector<FavorExVo> getOtherFavorList() {
		return otherFavorList;
	}
	public void setOtherFavorList(Vector<FavorExVo> otherFavorList) {
		this.otherFavorList = otherFavorList;
	}
//	public long getRedPackageValue() {
//		return redPackageValue;
//	}
//	public void setRedPackageValue(long redPackageValue) {
//		this.redPackageValue = redPackageValue;
//	}

//	public long getActiveFlag() {
//		return activeFlag;
//	}
//	public void setActiveFlag(long activeFlag) {
//		this.activeFlag = activeFlag;
//	}
	
}
