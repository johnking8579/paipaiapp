package com.qq.qqbuy.dealpromote.po;

public class ItemPriceVo {
	private long type; 					// 商品多价类型
	private long level; 				// 多价类型等级
	private long promotePrice; 			// 多价类型价格
	private String name = new String(); // 类型名称
	private String desc = new String(); // 类型描述
	private long checkStat; 			// 选中标识
	
	public long getType() {
		return type;
	}
	public void setType(long type) {
		this.type = type;
	}
	public long getLevel() {
		return level;
	}
	public void setLevel(long level) {
		this.level = level;
	}
	public long getPromotePrice() {
		return promotePrice;
	}
	public void setPromotePrice(long promotePrice) {
		this.promotePrice = promotePrice;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public long getCheckStat() {
		return checkStat;
	}
	public void setCheckStat(long checkStat) {
		this.checkStat = checkStat;
	}
}
