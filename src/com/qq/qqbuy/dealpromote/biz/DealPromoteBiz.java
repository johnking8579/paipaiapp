package com.qq.qqbuy.dealpromote.biz;

import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.deal.po.ConfirmOrderVo;
import com.qq.qqbuy.dealpromote.po.DealFavorVo;
import com.qq.qqbuy.dealpromote.po.DealPromotePo;
import com.qq.qqbuy.dealpromote.po.TakeCheapDealFavorVo;
import com.qq.qqbuy.thirdparty.idl.pay.recommend.protocol.PayRecommend;

import java.util.Vector;

public interface DealPromoteBiz {
	public DealFavorVo queryPromote(DealPromotePo po, ConfirmOrderVo confirmVo, String sk, String mk)
	throws BusinessException;

	/**
	 * 拍便宜大V团批价
	 * @return
	 * @throws BusinessException
	 * @param wid
	 * @param sk
	 * @param gid
	 * @param random
	 * @param openid 微信/京东登陆返回
	 */
	public DealFavorVo takeCheapQueryPromote(long wid, String sk, String gid, double random,String openid)throws BusinessException;

	/**
	 * 拍便宜分单接口
	 * @param dealVo
	 * @param wid 买家qq号
	 * @param sk
	 * @param scene 场景
	 * @param isCanEdit 是否可编辑
	 * @param bid
	 * @param sid
	 * @param commlist 商品信息串
	 * @param openid 微信/京东登陆返回
	 * @param useRedPackage 是否使用红包批价，1：是 0：否
	 */
	public String takeCheapOrderView(TakeCheapDealFavorVo dealVo, long wid, String sk, int scene, int isCanEdit, int bid, String sid, String commlist,String openid,int useRedPackage)throws BusinessException;

	/**
	 * 拍便宜批价接口
	 * @param wid
	 * @param sk
	 * @param reqSource 请求源
	 * @param orderFrom 订单来源，团团降为10，其他为8
	 * @param sid
	 * @param dealStrs 批价参数封装对象
	 * @param openid 微信/京东登陆返回
	 * @return
	 * @throws BusinessException
	 */
	public TakeCheapDealFavorVo takeCheapQuery(TakeCheapDealFavorVo dealVo,long wid, String sk, int reqSource,int orderFrom,String sid,String dealStrs,String openid,int useRedPackage,long versionCode)throws BusinessException;

	/**
	 * 获取推荐支付方式列表
	 * @param mk 设备码
	 * @param wid 买家qq
	 * @param userType 用户类型  用户类型：1=QQ qq账号及绑定了qq账号的微信账号(login_type=wx切uin<39亿); 2=WX wx账号非绑定qq(login_type=wx切uin>39亿); 3=JD JD账号
	 * @param dealTotalFee 订单总价
	 * @param itemCodes 订单商品id串,用半角逗号分隔
	 * @param sellerIds 订单商品卖家qq号串,用半角逗号分隔(顺序跟itemCodes一一对应)
	 * @param categorys 订单的商品类目,用半角逗号分隔(顺序跟itemCodes一一对应)
	 * @param supportcods 是否支持cod	串, 1=支持	2=不支持 ,用半角逗号分隔，例如1,2,1(顺序跟itemCodes一一对应)
	 * @return
	 */
	public Vector<PayRecommend> getPayRecommends(String mk,long wid,int userType,long dealTotalFee,String itemCodes,String sellerIds,String categorys,String supportcods);
}
