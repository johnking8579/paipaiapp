package com.qq.qqbuy.bi.biz;

import com.paipai.util.string.StringUtil;


public class PvMsgBuilder {

	private PvMsg msg = new PvMsg();
	
	private String ifNull(String s)	{
		return s==null ? "" : s;
	}

	public PvMsgBuilder buildCurUrl(String curUrl) {
		msg.setCurUrl(ifNull(curUrl));
		return this;
	}

	public PvMsgBuilder buildStrValue(String wpaipai, String mk, String mt, String sku, String shopid, 
									String catId, String leafCatId, Dap dap, String lon, 
									String la, String ppext) {
		String s = String.format("wpaipai:%s|deviceID:%s|deviceType:%s|uiKAID:|phone:|os:|retina:|" +
				"webp:|network:|browser:|screen:|phoneRegion:|appVersion:|isJD:|sku:%s|" +
				"shop:%s|category:%s|leaf:%s|MsgID:%s|PolicyID:%s|SceneID:%s|shopType:|skus:|" +
				"latitude:%s|longtitude:%s|ppext:%s", 
				StringUtil.isEmpty(wpaipai)?"2001":wpaipai , ifNull(mk), ifNull(mt),
				ifNull(sku),
				ifNull(shopid), ifNull(catId), ifNull(leafCatId), dap==null?"":dap.getMsgId().toString(), dap==null?"":dap.getPolicyId()+"", dap==null?"":dap.getScene().toString(),
				ifNull(lon), ifNull(la), ifNull(ppext)
		);
		msg.setStrValue(s);
		return this;
	}

	public PvMsgBuilder buildStrRefer(String refer) {
		msg.setStrRefer(ifNull(refer));
		return this;
	}

	public PvMsgBuilder buildStParam(String wid, String visitkey, String pprdP) {
		msg.setStrStParam(String.format("uin:%s|visitkey:%s|uintype:%s|pprd:%s", wid, ifNull(visitkey), 1, ifNull(pprdP)));
		return this;
	}
	
	public PvMsgBuilder buildStrPvid(String pvid) {
		msg.setStrPvid(ifNull(pvid));
		return this;
	}
	
	public PvMsgBuilder buildVisitkey(String visitkey) {
		msg.setStrVisitkey(ifNull(visitkey));
		return this;
	}
	
	public PvMsgBuilder buildDwUin(String dwUin) {
		msg.setDwUin(ifNull(dwUin));
		return this;
	}
	
	public PvMsgBuilder buildStrAgent(String userAgent) {
		msg.setStrAgent(ifNull(userAgent));
		return this;
	}
	
	public PvMsgBuilder buildClientIp(String clientIp) {
		msg.setClientIp(ifNull(clientIp));
		return this;
	}
	
	public PvMsgBuilder buildStrKey(String sk) {
		msg.setStrKey(ifNull(sk));
		return this;
	}
	
	public PvMsg build() {
		return msg;
	}

}
