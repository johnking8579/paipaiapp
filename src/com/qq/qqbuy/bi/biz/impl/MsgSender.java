package com.qq.qqbuy.bi.biz.impl;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.qq.qqbuy.bi.biz.BiSendable;
import com.qq.qqbuy.common.env.EnvManager;

/**
 * 使用BlockingQueue统一异步上报信息, 减少主线程等待时间, 控制发送频率,减少发送端口占用率
 * TODO 自动调整线程数和休眠时间,保证端口占用率和延迟时间都可接受
 * @author JingYing 2014-9-26
 *
 */
public class MsgSender {
	static Logger log = LogManager.getLogger(MsgSender.class);
	
	public static final int 
		WORKER_COUNT = 2,		//发送线程的数量
		SLEEP_MILLIS = 1;		//每个线程发送后的休眠数(毫秒)
			
	private static BlockingQueue<BiSendable> msgQueue = new LinkedBlockingQueue<BiSendable>();

	public static void addToQueue(BiSendable msg) {
		msgQueue.add(msg);
	}

	static {
		ExecutorService sendWorker = Executors.newFixedThreadPool(WORKER_COUNT);
		for(int i=0; i<WORKER_COUNT; i++)	{
			sendWorker.execute(new Runnable(){

				@Override
				public void run() {
					while(!Thread.interrupted())	{
						try {
							Thread.sleep(SLEEP_MILLIS);
							if(msgQueue.size() > 10000)	{
								log.warn("发送队列积压消息数:" + msgQueue.size());
							}
							msgQueue.take().send();		//获得前一直阻塞
						} catch (Exception e) {
							log.error(e.getMessage(), e);
						}
					}
				}
				
			});	
		}
		sendWorker.shutdown();
	}
	
	public static void main(String[] args) {
		Thread producer = new Thread(new Runnable() {
			
			@Override
			public void run() {
				while(true)	{
					try {
						Thread.sleep(new Random().nextInt(5) * 10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					MsgSender.addToQueue(new BiSendable() {
						
						@Override
						public void send() throws IOException {
							System.out.println(System.currentTimeMillis());
						}
					});
					System.out.println("队列数:" + MsgSender.msgQueue.size());
				}
				
			}
		});
		
		producer.start();
		try {
			producer.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

}
