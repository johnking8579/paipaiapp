package com.qq.qqbuy.bi.biz;

import java.math.BigInteger;

/**
 * 场景ID示例: 4000 1006 23844
	将以上三段数字分别变成二进制, 分别占16位+16位+32位, 不够的话前面补0,最后把完整的二进制串变成十进制, 即为场景ID 
	scene的数值范围为uint_64,注意可能会超long范围
 * @author JingYing
 * @date 2015年1月27日
 */
public class Scene {
	private int i1, i2, i3;
	
	public Scene(int i1, int i2, int i3)	{
		this.i1 = i1;
		this.i2 = i2;
		this.i3 = i3;
	}
	
	/**
	 * 解码sceneId
	 * @param sceneId
	 * @return
	 */
	public static Scene parse(String sceneStr)	{
		String s = new BigInteger(sceneStr).toString(2);
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<64-s.length(); i++)	{
			sb.append("0");
		}
		sb.append(s);
		return new Scene(
			Integer.parseInt(sb.substring(0 , 16), 2),
			Integer.parseInt(sb.substring(16, 32), 2),
			Integer.parseInt(sb.substring(32, 64), 2)
		);
	}
	
	/**
	 * scene的数值范围为uint_64, 超出long型
	 * @return
	 */
	public long longValue()	{
		return new BigInteger(toString()).longValue();
	}
	
	public String toString()	{
		String 
			s1 = Integer.toBinaryString(i1),
			s2 = Integer.toBinaryString(i2),
			s3 = Integer.toBinaryString(i3);
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<16-s1.length(); i++)	{
			sb.append("0");
		}
		sb.append(s1);
		
		for(int i=0; i<16-s2.length(); i++)	{
			sb.append("0");
		}
		sb.append(s2);
		
		for(int i=0; i<32-s3.length(); i++)	{
			sb.append("0");
		}
		sb.append(s3);
		return new BigInteger(sb.toString(), 2).toString();
	}

	public int getI1() {
		return i1;
	}

	public int getI2() {
		return i2;
	}

	public int getI3() {
		return i3;
	}

}
