package com.qq.qqbuy.bi.biz;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Random;

import com.paipai.component.configagent.ConfigHelper;
import com.qq.qqbuy.common.client.udp.UdpClient;
import com.qq.qqbuy.common.env.EnvManager;

/**
 * PV 和 click都发往这一个地址
 */
public class PvclickSender {
	
	private Random rand = new Random();
	
	public void send(byte[] bs) throws IOException	{
		String ipport = "10.222.137.114:40010";  //测试地址,不一定能用
		if(EnvManager.isIdc())	{
			InetSocketAddress addr = ConfigHelper.getSvcAddressBySet(8080, "pp_new_log_rcv_srv", 0, rand.nextInt(100));
			ipport = addr.getHostName() + ":" + addr.getPort();
		}
		new UdpClient().send(ipport, bs);
	}
	
}
