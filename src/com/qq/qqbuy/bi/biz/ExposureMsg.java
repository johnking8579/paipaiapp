package com.qq.qqbuy.bi.biz;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.paipai.component.configagent.ConfigHelper;
import com.qq.qqbuy.common.client.udp.UdpClient;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.util.Util;

public class ExposureMsg implements BiSendable{
	
	private class Item	{
		public String itemCode;
		public long aCategoryID, bCategoryID, lCategoryID;
	}

	private String uin, visitkey, clientIp, 
				logtime = System.currentTimeMillis()/1000 + "",
				cur_url = "", 
				refer_url = "";
	private String msgID, policyID, sceneID;
	private long locID, planID, pvID, traceID, openID;
	private String deviceID, deviceType, longitude, latitude; 
	private List<Item> items;	//其实BI不支持多item上报
	
	/**
	 * 拼装商品曝光信息, 曝光信息涉及到BI推荐, 必须有DAP才有意义
	 * @param dap62 62进制的DAP
	 * @param aCatid 商品的第一层类目ID
	 * @param bCatid 商品的第2层类目ID
	 * @param lCatid 商品的叶子类目ID
	 * @param buyerUin 买家QQ
	 * @param clientIp 手机IP, 使用com.qq.qqbuy.common.action.BasicAction#getHttpHeadIp();
	 * @param deviceId 
	 * @param deviceType
	 * @param longitude
	 * @param latitude
	 * @return
	 */
	public static ExposureMsg buildItemMsg(String dap62, long aCatid, long bCatid, long lCatid, 
									long buyerUin, String clientIp, String deviceId, 
									String deviceType, String longitude, String latitude, String curUrl)	{
		ExposureMsg msg = new ExposureMsg();
		Dap dap = Dap.fromDap62(dap62);
		
		Item item = msg.new Item();
		item.aCategoryID = aCatid;
		item.bCategoryID = bCatid;
		item.lCategoryID = lCatid;
		item.itemCode = dap.getShopidOrItemcode();
		msg.items = Arrays.asList(new Item[]{item});
		
		msg.clientIp = clientIp;
		msg.deviceID = deviceId;
		msg.deviceType = deviceType;
		msg.msgID = dap.getMsgId().toString();
		msg.policyID = dap.getPolicyId()+"";
		msg.sceneID = dap.getScene().toString();
		msg.uin = buyerUin+"";
		msg.latitude = latitude;
		msg.longitude = longitude;
		msg.cur_url = curUrl;
		return msg;
	}
	
	public static ExposureMsg buildShopMsg(String dap62, long buyerUin, String clientIp, String deviceId, 
									String deviceType, String longitude, String latitude, String curUrl)	{
		return buildItemMsg(dap62, 0, 0, 0, buyerUin, clientIp, deviceId, deviceType, longitude, latitude, curUrl);
	}
	
	private ExposureMsg()	{}
	
	@Override
	public void send() throws IOException {
		String ipport = "10.198.16.12:31302";
		if(EnvManager.isIdc())	{
			InetSocketAddress addr = ConfigHelper.getSvcAddressBySet(8080, "pp_bi_exppvcgi_recv", 0, this.hashCode()%3);
			ipport = addr.getHostName() + ":" + addr.getPort();
		}
		new UdpClient().send(ipport, toByteArray());
	}
	
	private String joinProperty()	{
		List<String> 
			itemcodes = new ArrayList<String>(), 
			acat = new ArrayList<String>(), 
			bcat = new ArrayList<String>(), 
			lcat = new ArrayList<String>(); 
		for(Item i : items)	{
			itemcodes.add(i.itemCode);
			acat.add(i.aCategoryID+"");
			bcat.add(i.bCategoryID+"");
			lcat.add(i.lCategoryID+"");
		}
		
		StringBuilder data = new StringBuilder()
			.append("MsgID:").append(msgID)
			.append("|PolicyID:").append(policyID)
			.append("|SceneID:").append(sceneID)
			.append("|LocID:").append(locID)
			.append("|PlanID:").append(planID)
			.append("|PvID:").append(pvID)
			.append("|TraceId:").append(traceID)
			.append("|ItemID:").append(Util.join(itemcodes, ","))
			.append("|OpenID:").append(openID)
			.append("|DeviceID:").append(deviceID)
			.append("|DeviceType:").append(deviceType)
			.append("|ACategoryID:").append(Util.join(acat, ","))
			.append("|BCategoryID:").append(Util.join(bcat, ","))
			.append("|LCategoryID:").append(Util.join(lcat, ","))
			.append("|Longtitude:").append(longitude)
			.append("|Latitude:").append(latitude);
		
		List<String> list = new ArrayList<String>();
		list.add(uin);
		list.add(visitkey);
		list.add(clientIp);
		list.add(logtime);
		list.add(data.toString());
		list.add(cur_url);
		list.add(refer_url);
		return Util.join(list, "$$");
	}
	
	/**
	 * 0x2	 		totalLen		msgType			dataLen			Data(变长)		0x3
	 * 1Byte 包头		4Byte 包总长度		1Byte		4Byte数据部分长度	变长 字符串数据协议	1Byte包尾
	 * @param data
	 * @return
	 */
	public byte[] toByteArray() throws UnsupportedEncodingException	{
		byte[] data = joinProperty().getBytes("ascii");
		int totalLen = 1+4+1+4+data.length+1;
		return ByteBuffer.allocate(totalLen)
			.put((byte)0x2)
			.putInt(totalLen)
			.put((byte)1)
			.putInt(data.length)
			.put(data)
			.put((byte)0x3)
			.array();
	}
	
}
