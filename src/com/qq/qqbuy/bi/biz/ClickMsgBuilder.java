package com.qq.qqbuy.bi.biz;

import com.paipai.util.string.StringUtil;

public class ClickMsgBuilder {
	private ClickMsg msg = new ClickMsg();
	
	private String ifNull(String s)	{
		return s==null ? "" : s;
	}

	public ClickMsgBuilder buildToUrl(String toUrl) {
		msg.setStrToUrl(ifNull(toUrl));
		return this;
	}

	public ClickMsgBuilder buildCurUrl(String curUrl) {
		msg.setCurUrl(ifNull(curUrl));
		return this;
	}

	public ClickMsgBuilder buildStrValue(String wpaipai, String ptag, String deviceId, String deviceType, String sku, 
										String category, String leaf, Dap dap, 
										String shopType, String lat, String lon, String ppext) {
		String s = String.format("wpaipai:%s|ptag:%s|wgtag:|deviceID:%s|deviceType:%s|" +
								"sku:%s|category:%s|leaf:%s|MsgID:%s|" +
								"PolicyID:%s|SceneID:%s|shopType:%s|skus:|latitude:%s|" +
								"longtitude:%s|ppext:%s", 
								StringUtil.isEmpty(wpaipai)?"2001":wpaipai ,ifNull(ptag), ifNull(deviceId), ifNull(deviceType),
								ifNull(sku), ifNull(category), ifNull(leaf), dap==null?"":dap.getMsgId().toString(),
								dap==null?"":dap.getPolicyId()+"", dap==null?"":dap.getScene().toString(), ifNull(shopType), ifNull(lat), 
								ifNull(lon), ifNull(ppext)
		);
		msg.setStrValue(s);
		return this;
	}

	public ClickMsgBuilder buildRefer(String refer) {
		msg.setStrRefer(ifNull(refer));
		return this;
	}

	public ClickMsgBuilder buildStParam(String buyerUin, String visitkey, String pprdP) {
		String s = String.format("uin:%s|visitkey:%s|uintype:1|pprd:%s", 
								ifNull(buyerUin), ifNull(visitkey), ifNull(pprdP));
		msg.setStParam(s);
		return this;
	}

	public ClickMsgBuilder buildPvId(String pvid) {
		msg.setStrPvid(ifNull(pvid));
		return this;
	}

	public ClickMsgBuilder buildVisitkey(String visitkey) {
		msg.setStrVisitkey(ifNull(visitkey));
		return this;
	}

	public ClickMsgBuilder buildClientIp(String clientIp) {
		msg.setStrClientIp(ifNull(clientIp));
		return this;
	}

	public ClickMsgBuilder buildDwUin(String dwUin) {
		msg.setDwUin(ifNull(dwUin));
		return this;
	}

	public ClickMsgBuilder buildStrKey(String sk) {
		msg.setSkey(ifNull(sk));
		return this;
	}
	
	public ClickMsg build() {
		return msg;
	}


}
