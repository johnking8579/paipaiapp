package com.qq.qqbuy.bi.biz;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.qq.qqbuy.common.util.Util;

public class PvMsg implements BiSendable{
	
	private String 
			timestamp = "", strp = "paipai.com", curUrl, 
			strValue, strMethod = "GET", strS = "200",
			strRefer, strStParam, strDyParam = "", 
			strPvid, strVer = "1.0", strAgent, 
			strVisitkey, clientIp, dwUin, 
			servTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()),
			strResolution = "", strColor = "", 
			strBrowser = "", strKey = "", strWxOpenId = "";
	
	public String assembly()	{
		String[] arr = new String[]{
			timestamp, strp, curUrl,
			strValue, strMethod, strS,
			strRefer, strStParam, strDyParam,
			strPvid, strVer, strAgent, 
			strVisitkey, clientIp, dwUin, 
			servTime, strResolution, strColor, 
			strBrowser, strKey, strWxOpenId
		};
		return Util.join(arr, "$$") + "$$";
	}

	
	@Override
	public void send() throws IOException {
		String s = assembly();
		byte[] body = s.getBytes("utf-8");
		byte[] bs = ByteBuffer.allocate(2+4+body.length)
				.putShort((short)1)	//1pv, 2click
				.putInt(body.length)
				.put(body)
				.array();
		new PvclickSender().send(bs);
	}
	
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public void setStrp(String strp) {
		this.strp = strp;
	}

	public void setCurUrl(String curUrl) {
		this.curUrl = curUrl;
	}

	public void setStrValue(String strValue) {
		this.strValue = strValue;
	}

	public void setStrMethod(String strMethod) {
		this.strMethod = strMethod;
	}

	public void setStrS(String strS) {
		this.strS = strS;
	}

	public void setStrRefer(String strRefer) {
		this.strRefer = strRefer;
	}

	public void setStrStParam(String strStParam) {
		this.strStParam = strStParam;
	}

	public void setStrDyParam(String strDyParam) {
		this.strDyParam = strDyParam;
	}

	public void setStrPvid(String strPvid) {
		this.strPvid = strPvid;
	}

	public void setStrVer(String strVer) {
		this.strVer = strVer;
	}

	public void setStrAgent(String strAgent) {
		this.strAgent = strAgent;
	}

	public void setStrVisitkey(String strVisitkey) {
		this.strVisitkey = strVisitkey;
	}

	public void setStrResolution(String strResolution) {
		this.strResolution = strResolution;
	}

	public void setStrColor(String strColor) {
		this.strColor = strColor;
	}

	public void setStrBrowser(String strBrowser) {
		this.strBrowser = strBrowser;
	}

	public void setStrKey(String strKey) {
		this.strKey = strKey;
	}

	public void setStrWxOpenId(String strWxOpenId) {
		this.strWxOpenId = strWxOpenId;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	public void setDwUin(String dwUin) {
		this.dwUin = dwUin;
	}

	public void setServTime(String servTime) {
		this.servTime = servTime;
	}

}
