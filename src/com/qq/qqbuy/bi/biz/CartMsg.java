package com.qq.qqbuy.bi.biz;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.paipai.component.configagent.ConfigHelper;
import com.qq.qqbuy.common.client.udp.UdpClient;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.util.Util;

public class CartMsg implements BiSendable{
	static Logger log = LogManager.getLogger(CartMsg.class);
	
	private String uin, itemCode, pprdP, pprdS, pprdT, 
				clientIp, visitkey, addTime, favTime, source = "3";
	private int num;

	public static CartMsg build(long uin, String itemCode, int num, 
								String clientIp, int reqPort, String pprdP)	{
		CartMsg msg = new CartMsg();
		msg.uin = uin+"";
		msg.itemCode = itemCode;
		msg.num = num;
		msg.clientIp = clientIp;
		msg.visitkey = Util.genVisitkey(clientIp, reqPort)+"";
		msg.addTime = System.currentTimeMillis()/1000 + "";
		msg.favTime = msg.addTime;
		msg.pprdP = pprdP;
		return msg;
	}
	
	private String assembly()	{
		List<String> list = new ArrayList<String>();
		list.add(uin);
		list.add("1");	//1加, 2删
		list.add(itemCode);
		list.add(num+"");
		list.add(pprdP != null ? pprdP : "");
		list.add("");
		list.add("");
		list.add(clientIp);
		list.add(visitkey);
		list.add(addTime);
		list.add(favTime);
//		list.add(source);
		return Util.join(list, "\t");
	}
	
	private byte[] toByteArray() throws UnsupportedEncodingException	{
		byte[] head = "shopcar_comm".getBytes("ascii");
		String s = assembly();
		byte[] body = s.getBytes("ascii");
		return ByteBuffer.allocate(4 + 4 + head.length + 4 + body.length)
			.putInt(1)
			.putInt(head.length)
			.put(head)
			.putInt(body.length)
			.put(body)
			.array();
	}

	@Override
	public void send() throws IOException {
		String ipport = "10.128.3.37:19101";
		if(EnvManager.isIdc())	{
			InetSocketAddress addr = ConfigHelper.getSvcAddressBySet(8080, "dwlog_srv", 0, this.hashCode()%3);
			ipport = addr.getHostName() + ":" + addr.getPort();
		}
		new UdpClient().send(ipport, toByteArray());
	}
	
}
