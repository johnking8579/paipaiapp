package com.qq.qqbuy.bi.biz;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.google.protobuf.ByteString;
import com.paipai.component.configagent.ConfigHelper;
import com.qq.qqbuy.bi.biz.impl.BidtLoghead.CBiMsgLogHead;
import com.qq.qqbuy.bi.biz.impl.CPaiLikePathProto.CPaiLikePath;
import com.qq.qqbuy.common.client.udp.UdpClient;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.util.ItemCode;

public class LikeDislikeMsg implements BiSendable	{
	
	private static final String CHARSET = "ascii";	
	
	private long uin, visitkey;
	private String udId, openId = "", clientIp;	
	private int siteId = 3, logTime = (int)(System.currentTimeMillis()/1000);	
	private String curUrl = "", referUrl = "";
	private BigInteger sceneId, policyId, msgId;
	private String terminalInfo;
	private long itemId, sellerUin;
	private int commentValue;	//0--dislike, 1--like&收藏
	private long price, aCatId, bCatId, lCatId;
	private String longitude, latitude;
	
	private LikeDislikeMsg()	{}
	
	/**
	 * 构建"不喜欢"消息
	 * @param itemCode
	 * @param poolId
	 * @param uin
	 * @param mk
	 * @param clientIp
	 * @param policyId
	 * @param msgId
	 * @param mt
	 * @return
	 */
	public static LikeDislikeMsg buildDisLikeMsg(String itemCode, String sceneId, long uin, 
													String mk, String clientIp, String policyId, 
													String msgId, String mt, 
													long price, long aCatId, long bCatId, long lCatId,
													String longitude, String latitude)	{
	 
		LikeDislikeMsg msg = new LikeDislikeMsg();
		ItemCode ic = new ItemCode(itemCode);
		msg.sellerUin = ic.getSellerUin();
		msg.itemId = ic.getItemId();
		msg.sceneId = new BigInteger(sceneId);
		msg.uin = uin;
		msg.udId = mk;
		msg.clientIp = clientIp;
		msg.policyId = new BigInteger(policyId);
		msg.msgId = new BigInteger(msgId);
		msg.terminalInfo = mt;
		msg.commentValue = 0;
		msg.price = price;
		msg.aCatId = aCatId;
		msg.bCatId = bCatId;
		msg.lCatId = lCatId;
		msg.longitude = longitude;
		msg.latitude = latitude;
		return msg;
	}

	private byte[] toProtobuf() throws UnsupportedEncodingException	{
		return CPaiLikePath.newBuilder()
		.setUiUin(uin).setUllVisikey(visitkey)
		.setSzUdID(ByteString.copyFrom(udId, CHARSET))
		.setSzOpenID(ByteString.copyFrom(openId, CHARSET))
		.setSzClientIP(ByteString.copyFrom(clientIp, CHARSET))
		.setUiSiteID(siteId)
		.setUiLogTime(logTime)
		.setSzCurUrl(ByteString.copyFrom(curUrl, CHARSET))
		.setSzReferUrl(ByteString.copyFrom(referUrl, CHARSET))
		.setUllSceneID(sceneId.longValue())
		.setUllPolicyID(policyId.longValue())
		.setUllMsgID(msgId.longValue())
		.setSzTerminalInfo(ByteString.copyFrom(terminalInfo, CHARSET))
		.setUiItemID(itemId).setUiSellerUin(sellerUin)
		.setUiCommentValue(commentValue)
		.setUiPrice((int)price)
		.setUiACatogryID((int)aCatId)
		.setUiBCatogryID((int)bCatId)
		.setUiLCatogryID((int)lCatId)
		.setSzLongtitude(ByteString.copyFrom(longitude, CHARSET))
		.setSzLatitude(ByteString.copyFrom(latitude, CHARSET))
		.build()
		.toByteArray();
	}
	
	private byte[] genMsgHead(int bodyLen) throws UnsupportedEncodingException	{
		return CBiMsgLogHead.newBuilder()
				.setUlSceneID(sceneId.longValue())
				.setUlPolicyID(policyId.longValue())
				.setUlMsgID(msgId.longValue())
				.setUlUin(uin)
				.setUlVisitkey(visitkey)
				.setSzOpenID(ByteString.copyFrom(openId, CHARSET))
				.setUiMsgType(5)
				.setUiBodyLen(bodyLen)
				.build().toByteArray();
	}
	
	private byte[] toByteArray() throws UnsupportedEncodingException	{
		byte[] body = toProtobuf();
		byte[] head = genMsgHead(body.length);
		int totalLen = 1 + 4 + 4 + head.length + body.length + 1;
		return ByteBuffer.allocate(totalLen)
				.order(ByteOrder.BIG_ENDIAN)
				.put((byte)0x2)
				.putInt(totalLen)
				.putInt(head.length)
				.put(head)
				.put(body)
				.put((byte)0x3)
				.array();
	}
	
	@Override
	public void send() throws IOException {
		//pp_bidt_recv, 测试10.185.5.83:10305, 正式:10.213.139.112:10301	
		String ipport = "10.213.139.112:10301";
		if(EnvManager.isIdc())	{
			InetSocketAddress addr = ConfigHelper.getSvcAddressBySet(8080, "pp_bidt_recv", 0, this.hashCode()%3);
			ipport = addr.getHostName() + ":" + addr.getPort();
		}
		new UdpClient().send(ipport, toByteArray());
	}
}
