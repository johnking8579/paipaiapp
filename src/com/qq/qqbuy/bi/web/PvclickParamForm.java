package com.qq.qqbuy.bi.web;


/**
 * 埋点上传用的所有参数. 需要对每个字段都设置get/set方法, 以便反射获取
 * @author JingYing 2014-10-16
 *
 */
public class PvclickParamForm {
	private String ptag, launchType, sku, shop, activityURL, share, sortID,
		inURL, cate1Name, cate2Name, keyword, listType, sortType, searchCondition, tag,
		tags, type, toSku, result, orderID, orderCate, filterType, leaf, comodity,ic,wpaipai;
	
	/**
	 * 从param里解析所有参数. 格式与URL参数一样: k=v&a=b
	 * @param params
	 * @return
	 */
	public static PvclickParamForm parse(String...params)	{
		PvclickParamForm f = new PvclickParamForm();
		for(String param : params)	{
			for(String s : param.split("&"))	{
				String[] kv = s.split("=", 2);
				if(kv.length == 2)	{
					try {
						String setter = getSetter(kv[0]);
						PvclickParamForm.class.getMethod(setter, String.class).invoke(f, kv[1]);
					} catch (Exception e) {
						//e.printStackTrace();
					}
				}
			}
		}
		return f;
	}
	
	private static String getSetter(String field)	{
		StringBuilder setMethod = new StringBuilder("set");
		setMethod.append(field.substring(0,1).toUpperCase());
		if(field.length() > 1){
			setMethod.append(field.substring(1));
		}
		return setMethod.toString();
	}
	
	public String getPtag() {
		return ptag;
	}

	public void setPtag(String ptag) {
		this.ptag = ptag;
	}

	public String getLaunchType() {
		return launchType;
	}

	public void setLaunchType(String launchType) {
		this.launchType = launchType;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getShop() {
		return shop;
	}

	public void setShop(String shop) {
		this.shop = shop;
	}

	public String getActivityURL() {
		return activityURL;
	}

	public void setActivityURL(String activityURL) {
		this.activityURL = activityURL;
	}

	public String getShare() {
		return share;
	}

	public void setShare(String share) {
		this.share = share;
	}

	public String getSortID() {
		return sortID;
	}

	public void setSortID(String sortID) {
		this.sortID = sortID;
	}

	public String getInURL() {
		return inURL;
	}

	public void setInURL(String inURL) {
		this.inURL = inURL;
	}

	public String getCate1Name() {
		return cate1Name;
	}

	public void setCate1Name(String cate1Name) {
		this.cate1Name = cate1Name;
	}

	public String getCate2Name() {
		return cate2Name;
	}

	public void setCate2Name(String cate2Name) {
		this.cate2Name = cate2Name;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getListType() {
		return listType;
	}

	public void setListType(String listType) {
		this.listType = listType;
	}

	public String getSortType() {
		return sortType;
	}

	public void setSortType(String sortType) {
		this.sortType = sortType;
	}

	public String getSearchCondition() {
		return searchCondition;
	}

	public void setSearchCondition(String searchCondition) {
		this.searchCondition = searchCondition;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getToSku() {
		return toSku;
	}

	public void setToSku(String toSku) {
		this.toSku = toSku;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public String getOrderCate() {
		return orderCate;
	}

	public void setOrderCate(String orderCate) {
		this.orderCate = orderCate;
	}

	public String getFilterType() {
		return filterType;
	}

	public void setFilterType(String filterType) {
		this.filterType = filterType;
	}

	public String getTag() {
		return tag;
	}

	public String getLeaf() {
		return leaf;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public void setLeaf(String leaf) {
		this.leaf = leaf;
	}

	public String getComodity() {
		return comodity;
	}

	public void setComodity(String comodity) {
		this.comodity = comodity;
	}

	public String getIc() {
		return ic;
	}

	public void setIc(String ic) {
		this.ic = ic;
	}

	public String getWpaipai() {
		return wpaipai;
	}

	public void setWpaipai(String wpaipai) {
		this.wpaipai = wpaipai;
	}
	
	
}
