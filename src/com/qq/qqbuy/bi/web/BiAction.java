 	
package com.qq.qqbuy.bi.web;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.qq.qqbuy.bi.biz.BiBiz;
import com.qq.qqbuy.bi.biz.BiSendable;
import com.qq.qqbuy.bi.biz.ClickMsgBuilder;
import com.qq.qqbuy.bi.biz.Dap;
import com.qq.qqbuy.bi.biz.ExposureMsg;
import com.qq.qqbuy.bi.biz.LikeDislikeMsg;
import com.qq.qqbuy.bi.biz.PvMsgBuilder;
import com.qq.qqbuy.bi.biz.ShareMsg;
import com.qq.qqbuy.common.ChannelParam;
import com.qq.qqbuy.common.JsonOutput;
import com.qq.qqbuy.common.action.AuthRequiredAction;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.item.biz.PPItemBiz;
import com.qq.qqbuy.item.po.ItemBO;
import com.qq.qqbuy.thirdparty.idl.item.ItemClient;
import com.qq.qqbuy.thirdparty.idl.item.protocol.FetchItemInfoResp;

/**
 * 向Business Intelligent部门上传数据
 * 包括分享行为, 喜欢, 不喜欢等
 * @author JingYing 2014-9-1
 *
 */
public class BiAction extends AuthRequiredAction{
	
	public static Logger log = LogManager.getLogger(BiAction.class),
				log2 = LogManager.getLogger(BiAction.class.getName() + ".upPv");
	
	BiBiz biBiz;
	PPItemBiz ppItemBiz;
	JsonOutput out = new JsonOutput();
	
	int dest;
	String[] p;
	String shareDesc, ids, daps, dap; 
	String content, imei, osVersion, appVersion, networkType, networkServer, verifyCode, pinid;	//upPv相关参数

	
	/**
	 * 对商品标注喜欢或不喜欢
	 * @return
	 */
	public String dislikeItem()	{
		long wid = getWidFromToken(getAppToken());
		String[] dapArr = daps.split(",");
		for(String s : dapArr)	{
			try {
				Dap d = Dap.fromDap62(s);
				
				long price = 0L, classId = 0L;
				try {
					ItemBO i = ppItemBiz.fetchItemInfo(d.getShopidOrItemcode(), false);
					price = i.getActivityPrice();
					classId = i.getClassId();
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
				
				LikeDislikeMsg msg = LikeDislikeMsg.buildDisLikeMsg(
						d.getShopidOrItemcode(), d.getScene().toString(), 
						wid, getMk(), getHttpHeadIp(), 
						d.getPolicyId()+"", d.getMsgId().toString(), getMt(), 
						price, 0, 0, classId, getLongitude(), getLatitude());			//TODO 从哪儿取商品类目?
				biBiz.uploadMsg(msg);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return doPrint(out);
	}
	
	  
	/**
	 * 分享上报
	 * @return
	 */
	public String shareItem()	{
		long wid = getWidFromToken(getAppToken());
		Dap d = Dap.fromDap62(dap);
		ShareMsg msg = ShareMsg.build(d.getShopidOrItemcode(), d.getScene().toString(), wid, 	//TODO
						getMk(), getHttpHeadIp(), d.getPolicyId()+"", 
						d.getMsgId()+"", getMt(), dest, getLongitude(), getLatitude());
		biBiz.uploadMsg(msg);
		return doPrint(out);
	}
	
	/**
	 * 上报曝光
	 * @return
	 * @throws Exception 
	 * @deprecated 旧版本用到过
	 */
	public String expoItem()	{
		checkLogin();		//对登录态进行续期
		long wid = getWidFromToken(getAppToken());
		for(String s : daps.split(","))	{
			try {
				Dap dap = Dap.fromDap62(s);
				FetchItemInfoResp resp = ItemClient.fetchItemInfo(dap.getShopidOrItemcode()) ;
				if (resp.getResult() == 0 ) {
					long lCatId = resp.getItemPo().getOItemBase().getLeafClassId();
					ExposureMsg msg = ExposureMsg.buildItemMsg(s, 0L, 0L, lCatId,	//只取叶子类目ID
					wid, getHttpHeadIp(), getMk(), getMt(), getLongitude(), getLatitude(), "");
					biBiz.uploadMsg(msg);
				}
			} catch (Exception e) {
				log.error("上报曝光时异常:" + e.getMessage());
			}
		}
		return doPrint(out);
	}
	
	/**
	 * 上报曝光
	 * @return
	 * @throws Exception 
	 */
	public String exposure() throws Exception	{
		long wid = getWidFromToken(getAppToken());
		Exception exp = null;
		
		JsonObject json;
		try {
			json = new JsonParser().parse(content).getAsJsonObject();
		} catch (Exception e1) {
			throw new IllegalArgumentException(e1);
		}
		for(Entry<String, JsonElement> entry : json.entrySet())	{
			for(JsonElement je : entry.getValue().getAsJsonArray())	{
				try {
					Dap dap = Dap.fromDap62(je.getAsString());
					if(dap.isItem())	{
						FetchItemInfoResp resp = ItemClient.fetchItemInfo(dap.getShopidOrItemcode()) ;
						if (resp.getResult() == 0 ) {
							long lCatId = resp.getItemPo().getOItemBase().getLeafClassId();
							ExposureMsg msg = ExposureMsg.buildItemMsg(je.getAsString(), 0L, 0L, lCatId,	//只取叶子类目ID
									wid, getHttpHeadIp(), getMk(), getMt(), getLongitude(), getLatitude(), entry.getKey());
							biBiz.uploadMsg(msg);
						}
					} else if(dap.isShop())	{
						ExposureMsg msg = ExposureMsg.buildShopMsg(je.getAsString(), wid, getHttpHeadIp(), 
								getMk(), getMt(), getLongitude(), getLatitude(), entry.getKey());
						biBiz.uploadMsg(msg);
					}
				} catch (Exception e) {
					exp = e;
				}
			}
		}
		
		if(exp != null)		throw exp;
		return doPrint(out);
	}
	
	/**
	 * 单独上报PV/CLICK
	 * 数据来源: 请求URL/请求消息体/URL-channel(电子渠道/pprdP)/
	 * 
	 * device_time:当前毫秒
	 * page_name:页面url
	 * is_http:0/1
	 * duration: 毫秒
	 * page_par: 页面参数
	 * ptag :
	 * click_par: 点击参数
	 * refer:
	 * rawPvid: |时间戳|随机数
	 * 
	 * @return
	 * @throws Exception 
	 */
	public String upPv() throws Exception 	{
		log2.info(Util.paramMapToString(getRequest().getParameterMap()));	//业务日志,不要删
		ChannelParam channel = null;
		try {
			channel = new ChannelParam(Util.urlDecode(getChannel(), "utf-8"));
		} catch (Exception e) {
			log.error(e);
		}
		checkLoginAndGetSkey();		//如果登陆的话,需要解析sk
		networkServer = Util.urlDecode(networkServer, "utf-8");
		Exception exp = null;
		BiSendable msg;
		
		for(String s : content.split("\\\\n"))	{
			try {
				PvclickLineBodyForm line = new PvclickLineBodyForm(s);
				if("".equals(line.getPtag()))	{
					msg = handlePv(getQq(), channel, line);
				} else	{
					msg = handleClick(getQq(), channel, line);
				}
				log2.debug(new Gson().toJson(msg));
				biBiz.uploadMsg(msg);
			} catch (Exception e) {
				exp = e;
			}
		}
		if(exp != null)		throw exp;
		return doPrint(out);
	}
	
	
	private BiSendable handlePv(long wid, ChannelParam channel, PvclickLineBodyForm line)	{
		PvclickParamForm form = PvclickParamForm.parse(line.getClickParam(), line.getPageParam());
		String curUrl = line.getPageName();
		if(Util.isNotEmpty(curUrl) && Util.isNotEmpty(line.getPageParam()))	{
			curUrl += ("?" + line.getPageParam());
		}
		Dap dap = null;		//前端在传DAP时, 不用把itemcode拼接进来, 服务端给什么样的DAP就回传什么DAP
		if(Util.isNotEmpty(line.getDap()))	{
			String dapstr = line.getDap();
			if(Util.appearCount(line.getDap(), ":") == 2)	{	
				dapstr += ":" + form.getSku();	//前端有可能传10进制的完整dap
			}
			try {
				dap = Dap.parse(dapstr);
				curUrl += ("&DAP=" + dap.toDecimalStr());
			} catch (Exception e1) {
				log.error(e1.getMessage());
			}
		}
		String[] arr = line.getRawPvid().split("\\|");
		String mkHashcode = "0";
		if(Util.isNotEmpty(getMk()))	{
			mkHashcode = getMk().hashCode() + "";
		}
		StringBuilder pvid = new StringBuilder(mkHashcode);
		if(arr.length > 2)	{
			pvid.append(arr[1]).append(arr[2]);
		}
		
		Map<String,String> extMap = new HashMap<String,String>();
		extMap.put("appKey", getAppID());
		extMap.put("pinid", pinid);
		if(channel != null)	{
			extMap.put("market", channel.getMarket());
		}
		extMap.put("inURL", form.getInURL());
		extMap.put("launchType", form.getLaunchType());
		extMap.put("searchCondition", form.getSearchCondition());
		extMap.put("orderID", form.getOrderID());
		extMap.put("keyword", form.getKeyword());
		extMap.put("orderCate", form.getOrderCate());
		extMap.put("deviceTime", line.getDeviceTime());
		extMap.put("isHttp", line.getIsHttp());
		extMap.put("duration", line.getDuration());
		extMap.put("comodity", form.getComodity()) ;
		extMap.put("ic", form.getIc()) ;
		filterMap(extMap);
		StringBuilder ppext = new StringBuilder();
		for(Entry<String,String> e : extMap.entrySet())	{
			ppext.append(e.getKey()).append(",").append(e.getValue()).append(";");
		}
		if(ppext.toString().endsWith(";"))	{
			ppext.deleteCharAt(ppext.length()-1);
		}
		
		return new PvMsgBuilder()
			.buildClientIp(getHttpHeadIp())
			.buildCurUrl(curUrl)
			.buildDwUin(wid+"")
			.buildStParam(wid+"", mkHashcode, channel == null ? "" : channel.generatePprd())
			.buildStrPvid(pvid.toString())
			.buildStrAgent(getRequest().getHeader("user-agent"))
			.buildStrRefer(line.getRefer())
			.buildStrValue(form.getWpaipai(), getMk(), getMt(), form.getSku(), form.getShop(), "", form.getLeaf(), dap, getLongitude(), getLatitude(), ppext.toString())
			.buildVisitkey(mkHashcode)
			.buildStrKey(getSk())
			.build();
	}
	
	
	private BiSendable handleClick(long wid, ChannelParam channel, PvclickLineBodyForm line)	{
		PvclickParamForm form = PvclickParamForm.parse(line.getClickParam(), line.getPageParam());
		
		String curUrl = line.getPageName();
		if(Util.isNotEmpty(curUrl) && Util.isNotEmpty(line.getPageParam()))	{
			curUrl += ("?" + line.getPageParam());
		}
		String toUrl = "http://app.paipai.com/api/item/getItem.xhtml?ic=" + form.getSku();
		Dap dap = null;
		if(Util.isNotEmpty(line.getDap()))	{
			String dapstr = line.getDap();
			if(Util.appearCount(line.getDap(), ":") == 2)	{
				dapstr += ":" + form.getSku();
			}
			try {
				dap = Dap.parse(dapstr);
				curUrl += ("&DAP=" + dap.toDecimalStr());
				toUrl += ("&DAP=" + dap.toDecimalStr());
			} catch (Exception e1) {
				log.error(e1.getMessage());
			}
		}
		String[] arr = line.getRawPvid().split("\\|");
		String mkHashcode = "0";
		if(Util.isNotEmpty(getMk()))	{
			mkHashcode = getMk().hashCode() + "";
		}
		StringBuilder pvid = new StringBuilder(mkHashcode);
		if(arr.length > 2)	{
			pvid.append(arr[1]).append(arr[2]);
		}
		
		Map<String,String> extMap = new HashMap<String,String>();
		extMap.put("uiKAID", adapteNetworkOper(networkServer));	
		extMap.put("os", getOsVersion());
		extMap.put("network", networkType);
		extMap.put("appVersion", appVersion);
		extMap.put("appKey", getAppID());
		extMap.put("pinid", pinid);
		if(channel != null){
			extMap.put("market", channel.getMarket());
		}
		extMap.put("toSku", form.getToSku());
		extMap.put("activityURL", form.getActivityURL());
		extMap.put("share", form.getShare());
		extMap.put("sortID", form.getSortID());	
		extMap.put("cate1Name", form.getCate1Name());
		extMap.put("cate2Name", form.getCate2Name());
		extMap.put("keyword", form.getKeyword());
		extMap.put("listType", form.getListType());
		extMap.put("sortType", form.getSortType());
		extMap.put("tag", form.getTag());
		extMap.put("tags", form.getTags());
		extMap.put("type", form.getType());
		extMap.put("result", form.getResult());
		extMap.put("orderID", form.getOrderID());
		extMap.put("filterType", form.getFilterType());
		extMap.put("deviceTime", line.getDeviceTime());
		extMap.put("isHttp", line.getIsHttp());
		extMap.put("duration", line.getDuration());
		extMap.put("ic", form.getIc()) ;
		extMap.put("comodity", form.getComodity());
		extMap.put("launchType", form.getLaunchType());
		extMap.put("ptag", form.getPtag());
		filterMap(extMap);
		StringBuilder ppext = new StringBuilder();
		for(Entry<String,String> e : extMap.entrySet())	{
			ppext.append(e.getKey()).append(",").append(e.getValue()).append(";");
		}
		if(ppext.toString().endsWith(";"))	{
			ppext.deleteCharAt(ppext.length()-1);
		}
		
		return new ClickMsgBuilder()
			.buildClientIp(getHttpHeadIp())
			.buildCurUrl(curUrl)
			.buildDwUin(wid+"")
			.buildStParam(wid+"", getMk().hashCode()+"", channel == null ? "" : channel.generatePprd())	
			.buildPvId(pvid.toString())
			.buildRefer(line.getRefer())
			.buildStrValue(form.getWpaipai(), line.getPtag(), getMk(), getMt(), form.getSku(), 
						"", form.getLeaf(), dap, "", getLatitude(), getLongitude(), ppext.toString())
			.buildToUrl(toUrl)
			.buildVisitkey(mkHashcode)
			.buildStrKey(getSk())
			.build();
	}
	
	private String adapteNetworkOper(String networkOper)	{
		if(Util.isEmpty(networkOper))	{
			return "";
		} else if(networkOper.contains("移动"))	{
			return "1";
		} else if(networkOper.contains("联通"))	{
			return "2";
		} else if(networkOper.contains("电信"))	{
			return "3";
		} else	{
			return "";
		}
	}
	
	private void filterMap(Map<String,String> map)	{
		for(Entry e : map.entrySet())	{
			if(e.getValue() == null)	{
				e.setValue("");
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	public String getShareDesc() {
		return shareDesc;
	}

	public void setShareDesc(String shareDesc) {
		this.shareDesc = shareDesc;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public void setBiBiz(BiBiz biBiz) {
		this.biBiz = biBiz;
	}

	public String getDaps() {
		return daps;
	}

	public void setDaps(String daps) {
		this.daps = daps;
	}
	
	public String getDap() {
		return dap;
	}
	
	public void setDap(String dap) {
		this.dap = dap;
	}

	public void setPpItemBiz(PPItemBiz ppItemBiz) {
		this.ppItemBiz = ppItemBiz;
	}

	public String[] getP() {
		return p;
	}

	public void setP(String[] p) {
		this.p = p;
	}

	public int getDest() {
		return dest;
	}

	public void setDest(int dest) {
		this.dest = dest;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	public String getNetworkServer() {
		return networkServer;
	}

	public void setNetworkServer(String networkServer) {
		this.networkServer = networkServer;
	}

	public String getVerifyCode() {
		return verifyCode;
	}

	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}

	public PPItemBiz getPpItemBiz() {
		return ppItemBiz;
	}

	public String getPinid() {
		return pinid;
	}

	public void setPinid(String pinid) {
		this.pinid = pinid;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
