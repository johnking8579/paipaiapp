package com.qq.qqbuy.bi.web;

public class PvclickLineBodyForm {
	
	/**
	 * device_time:当前毫秒
	 * page_name:页面url
	 * is_http:0/1
	 * duration: 毫秒
	 * page_par: 页面参数
	 * ptag : 无值时为click, 有则为pv
	 * click_par: 点击参数
	 * refer:
	 * rawPvid:
	 * dap:
	 */
	private String deviceTime, pageName, isHttp, duration, pageParam, ptag, clickParam, refer, rawPvid, dap; 
	
	public PvclickLineBodyForm(String line)	{
		String[] arr = line.split("\\|\\|\\|", 100);
		try {
			this.deviceTime = arr[0];
			this.pageName = arr[1];
			this.isHttp = arr[2];
			this.duration = arr[3];
			this.pageParam  = arr[4];
			this.ptag = arr[5];
			this.clickParam = arr[6];
			this.refer = arr[7];
			this.rawPvid = arr[8];
			this.dap = arr[9];
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new IllegalArgumentException("行参数无法解析,应当为10组字符串以|||隔开:" + line);
		}
	}
	

	public String getDeviceTime() {
		return deviceTime;
	}

	public String getPageName() {
		return pageName;
	}

	public String getIsHttp() {
		return isHttp;
	}

	public String getDuration() {
		return duration;
	}

	public String getPageParam() {
		return pageParam;
	}

	public String getPtag() {
		return ptag;
	}

	public String getClickParam() {
		return clickParam;
	}

	public String getRefer() {
		return refer;
	}

	public String getRawPvid() {
		return rawPvid;
	}
	
	public String getDap()	{
		return dap;
	}
}
