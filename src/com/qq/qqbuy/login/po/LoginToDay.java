package com.qq.qqbuy.login.po;

import com.qq.qqbuy.common.constant.CommonConstants;

/**
 * 登陆数据按天记录
 * @author zhaohuayu
 *
 */
public class LoginToDay {

	private long wid ;
	private String loginDay ;
	private String loginType ;
	private int loginTimes;
	private long insertTime ;
	private long updateTime ;
	private String mk;
	private String mt;
	private String longitude;
	private String latitude ;
	private String appID ;
	private String versionCode ;
	private String loginIp ;
	private String ntext ;
	private String ext1 ;
	private String ext2 ;
	private String ext3 ;
	
	public String getAppendInfo() {
		StringBuffer sb = new StringBuffer() ;
		sb.append(loginType).append(CommonConstants.STR_PARTITION)
			.append(updateTime).append(CommonConstants.STR_PARTITION)
			.append(mk).append(CommonConstants.STR_PARTITION)
			.append(mt).append(CommonConstants.STR_PARTITION)
			.append(longitude).append(CommonConstants.STR_PARTITION)
			.append(latitude).append(CommonConstants.STR_PARTITION)
			.append(appID).append(CommonConstants.STR_PARTITION)
			.append(versionCode).append(CommonConstants.STR_PARTITION)
			.append(loginIp).append(CommonConstants.STR_PARTITION)
			.append(ext1).append(CommonConstants.STR_PARTITION)
			.append(ext2).append(CommonConstants.STR_PARTITION)
			.append(ext3);
		return sb.toString() ;
	}
	public long getWid() {
		return wid;
	}
	public void setWid(long wid) {
		this.wid = wid;
	}
	public String getLoginDay() {
		return loginDay;
	}
	public void setLoginDay(String loginDay) {
		this.loginDay = loginDay;
	}
	public String getLoginType() {
		return loginType;
	}
	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}
	public int getLoginTimes() {
		return loginTimes;
	}
	public void setLoginTimes(int loginTimes) {
		this.loginTimes = loginTimes;
	}
	public long getInsertTime() {
		return insertTime;
	}
	public void setInsertTime(long insertTime) {
		this.insertTime = insertTime;
	}
	public long getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(long updateTime) {
		this.updateTime = updateTime;
	}
	public String getMk() {
		return mk;
	}
	public void setMk(String mk) {
		this.mk = mk;
	}
	public String getMt() {
		return mt;
	}
	public void setMt(String mt) {
		this.mt = mt;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getAppID() {
		return appID;
	}
	public void setAppID(String appID) {
		this.appID = appID;
	}
	public String getVersionCode() {
		return versionCode;
	}
	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}
	public String getLoginIp() {
		return loginIp;
	}
	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}
	public String getNtext() {
		return ntext;
	}
	public void setNtext(String ntext) {
		this.ntext = ntext;
	}
	public String getExt1() {
		return ext1;
	}
	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}
	public String getExt2() {
		return ext2;
	}
	public void setExt2(String ext2) {
		this.ext2 = ext2;
	}
	public String getExt3() {
		return ext3;
	}
	public void setExt3(String ext3) {
		this.ext3 = ext3;
	}
	
}
