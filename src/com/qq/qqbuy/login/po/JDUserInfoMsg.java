package com.qq.qqbuy.login.po;

public class JDUserInfoMsg {

	String pin;
	int userLevel;
	String nickname;
	String yunBigImageUrl;
	String yunMidImageUrl;
	String yunSmaImageUrl;
	String levelName;
	String verifyEmail;
	String verifyMobile;

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public int getUserLevel() {
		return userLevel;
	}

	public void setUserLevel(int userLevel) {
		this.userLevel = userLevel;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getYunBigImageUrl() {
		return yunBigImageUrl;
	}

	public void setYunBigImageUrl(String yunBigImageUrl) {
		this.yunBigImageUrl = yunBigImageUrl;
	}

	public String getYunMidImageUrl() {
		return yunMidImageUrl;
	}

	public void setYunMidImageUrl(String yunMidImageUrl) {
		this.yunMidImageUrl = yunMidImageUrl;
	}

	public String getYunSmaImageUrl() {
		return yunSmaImageUrl;
	}

	public void setYunSmaImageUrl(String yunSmaImageUrl) {
		this.yunSmaImageUrl = yunSmaImageUrl;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public String getVerifyEmail() {
		return verifyEmail;
	}

	public void setVerifyEmail(String verifyEmail) {
		this.verifyEmail = verifyEmail;
	}

	public String getVerifyMobile() {
		return verifyMobile;
	}

	public void setVerifyMobile(String verifyMobile) {
		this.verifyMobile = verifyMobile;
	}

}
