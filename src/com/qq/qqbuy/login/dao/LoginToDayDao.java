package com.qq.qqbuy.login.dao;

import java.util.List;

import com.qq.qqbuy.login.po.LoginToDay;
/**
 * 按天记录数据
 * @author zhaohuayu
 *
 */
public interface LoginToDayDao {

	/**
	 * 插入数据
	 * @param loginToDay
	 * @throws Exception
	 */
	public void insertLoginToDay(LoginToDay loginToDay) throws Exception ;
	/**
	 * 更新数据
	 * @param loginToDay
	 * @throws Exception
	 */
	public void updateLoginToDay(LoginToDay loginToDay) throws Exception ;	
	/**
	 * 根据WID和LOGINDAY找到对象
	 * @param wid
	 * @param loginDay
	 * @return
	 * @throws Exception
	 */
	public LoginToDay findByWidDay(long wid, String loginDay) throws Exception ;
	
	/**
	 * 根据INSERT起止时间查登陆日志数据
	 * @param beforeTime
	 * @param endTime
	 * @return
	 * @throws Exception
	 */
	public List<LoginToDay> findByinsertTime(long beforeTime, long endTime) throws Exception ;
	/**
	 * 
	 * @Title: findoldLog 
	 * @Description：查出最旧的一条数据
	 * @param @return
	 * @param @throws Exception    设定文件 
	 * @return LoginToDay    返回类型 
	 * @throws
	 */
	public LoginToDay findoldLogBywid(long wid) throws Exception;
}
