package com.qq.qqbuy.login.biz;

import com.qq.qqbuy.login.po.AppToken;
import com.qq.qqbuy.login.po.LoginToDay;

public interface LoginToDayBiz {

	/**
	 * loginToDay入库，不存在添加，存在更新
	 * @param loginToDay
	 * @throws Exception
	 */
	public void writeLoginToDay(LoginToDay loginToDay) throws Exception ;
	/**
	 * 
	 * @param appToken
	 * @param loginType
	 * @return
	 * @throws Exception
	 */
	public LoginToDay convertToLoginToDay(AppToken appToken, String ip) throws Exception ;
}
