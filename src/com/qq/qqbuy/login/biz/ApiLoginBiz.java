package com.qq.qqbuy.login.biz;

import com.jd.wlogin.api.ClientInfo;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.login.po.UnifromLoginPo;

public interface ApiLoginBiz {

	/**
	 * 校验登陆是否合法及返回用户详细信息
	 * @param accessToken
	 * @param openId
	 * @param oAuthKey
	 * @return
	 * @throws Exception 
	 */
	public String checkQQLogin(String accessToken, String openId) throws Exception  ;
	
	/**
	 * 校验登陆是否合法及返回用户详细信息
	 * @param pin 用户表示
	 * @param appId 分配给业务的唯一AppId
	 * @param tgt 用户登录态凭据 a2
	 * @param clientInfo 详细信息
	 * @return
	 * @throws Exception
	 */
	public int checkJDLogin(String pin, String tgt, ClientInfo clientInfo) throws Exception  ;
	
	/**
	 * 根据PAIPAI登陆态WID SK判断用户是否登陆
	 * @param wid
	 * @param sk
	 * @param mk
	 * @return
	 * @throws Exception
	 */
	public boolean checkLoginForWidSk(Long wid, String sk, String mk) throws Exception ;
	/**
	 * 
	 * @param pin 京东用户唯一标示	
	 * @return
	 */
	public String getJDUserInfo(String pin)  ;
	/**
	 * openId换Uin
	 * @param accessToken
	 * @param openId
	 * @return
	 * @throws Exception
	 */
	public String getUinFromQQTencentb (String accessToken, String openId) throws Exception ;
	/**
	 * @param mk 机器标识
	 * @param uin 用户昵称
	 * @param openId qqSDK登陆生成的openId
	 * @param nickname 用户昵称
	 * @return
	 * @throws BusinessException 
	 */
	public UnifromLoginPo getWidFromQQIdl (String mk, Long uin, String openId, String nickName) throws BusinessException ;
	/**
	 * @param mk 机器标识
	 * @param uin 用户昵称
	 * @param lk WT登录态
	 * @param nickname 用户昵称
	 * @return
	 * @throws BusinessException 
	 */
	public UnifromLoginPo getWidFromWTIdl (String mk, Long uin, String lk, String openId, String nickName) throws BusinessException ;
	/**
	 * 
	 * @param mk
	 * @param uin
	 * @param sk
	 * @return
	 * @throws BusinessException
	 */
	public Long loginOutForQQSdk(String mk, Long uin, String sk) throws BusinessException ;
	/**
	 * 
	 * @param mk
	 * @param openId
	 * @return
	 * @throws BusinessException
	 */
	public UnifromLoginPo getWidFromJDIdl (String mk, String openId) throws BusinessException ;
	
	/**
	 * 
	 * @param mk
	 * @param openId
	 * @param accessToken
	 * @param unionid
	 * @return
	 * @throws BusinessException
	 */
	public UnifromLoginPo getWidFromWXIdl (String mk, String openId, String accessToken, String unionid) throws BusinessException ;
	 
	/**
	 * 微信登录，根据code获取appId 和 accessToken
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public String getIDTokenForWXSdk (String code) throws Exception  ;
	/**
	 * 
	 * @param accessToken
	 * @param appId
	 * @return
	 * @throws Exception
	 */
	public String getWXUserInfo (String accessToken, String openid) throws Exception;
}
