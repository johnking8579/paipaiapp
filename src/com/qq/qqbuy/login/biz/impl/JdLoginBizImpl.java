package com.qq.qqbuy.login.biz.impl;


import com.jd.wlogin.api.ClientInfo;
import com.jd.wlogin.api.WLoginHelper;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.TokenUtil;
import com.qq.qqbuy.login.biz.JdLoginBiz;
import com.qq.qqbuy.login.po.JDUserInfo;
import com.qq.qqbuy.login.po.UnifromLoginPo;
import com.qq.qqbuy.thirdparty.idl.jdlogin.JdLoginClient;
import com.qq.qqbuy.thirdparty.idl.jdlogin.protocol.LoginInfoPo;
import com.qq.qqbuy.thirdparty.idl.jdlogin.protocol.UniformLoginResp;
import com.qq.qqbuy.thirdparty.idl.jdlogin.protocol.WanggouUserLogoutResp;
import net.sf.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class JdLoginBizImpl implements JdLoginBiz {

	protected final int readTimeout = 15000;// 读超时时间
    protected final int connectTimeout = 10000;// 连接超时时间
    protected final String encoding = "UTF-8";// 回复的字符编码
    protected final boolean isNeedProxy = true;
    
	@Override
	public String checkQQLogin(String accessToken, String openId)throws Exception  {
	StringBuffer getUrl = new StringBuffer();
		getUrl.append(TokenUtil.QQ_OPEN_SDK_CHECK_URL).append("?access_token=").append(accessToken).append("&openid=")
				.append(openId).append("&oauth_consumer_key=").append(TokenUtil.QQ_OPEN_SDK_GET_UIN_APP_ID);
		return HttpUtil.get(getUrl.toString(), connectTimeout, readTimeout,
	            encoding, isNeedProxy);
	}

	@Override
	public String getUinFromQQTencentb(String accessToken, String openId)
			throws Exception {
		StringBuffer getUrl = new StringBuffer();
		getUrl.append(TokenUtil.QQ_OPEN_SDK_GET_UIN)
		.append("?accesstoken=").append(accessToken)
		.append("&openid=").append(openId)
		.append("&appid=").append(TokenUtil.QQ_OPEN_SDK_GET_UIN_APP_ID)
		.append("&ref=").append(TokenUtil.QQ_OPEN_SDK_GET_UIN_REF)
		.append("&appkey=").append(TokenUtil.QQ_OPEN_SDK_GET_UIN_APP_KEY);
		return HttpUtil.get(getUrl.toString(), connectTimeout, readTimeout,
	            encoding, false);
	}

	@Override
	public UnifromLoginPo getWidFromQQIdl(String mk, Long uin, String openId,
			String nickName) throws BusinessException {		 
		
		LoginInfoPo loginInfoPo = new LoginInfoPo() ;
		loginInfoPo.setLoginFrom(TokenUtil.QQ_OPEN_SDK_IDL_LOGINFROM) ;
		loginInfoPo.setLoginIntensityType(TokenUtil.LOGIN_INTENSITY_TYPE_APP) ;
		loginInfoPo.setAccountType(TokenUtil.QQ_OPEN_SDK_IDL_ACCOUNT_TYPE) ;
		loginInfoPo.setQQNumber(uin) ;
		loginInfoPo.setOpenid(openId) ;  
		loginInfoPo.setOpenidFrom(TokenUtil.QQ_OPEN_SDK_IDL_OPENIDFROM); 
		loginInfoPo.setReserveStr(nickName) ;
		UniformLoginResp  uniformLoginResp = JdLoginClient.uniformLogin(mk, uin, loginInfoPo, TokenUtil.QQ_OPEN_SDK_IDL_SOURCE);
		if (uniformLoginResp == null) {
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "IDL getWidFromQQIdl 调用失败");
		}
		if (uniformLoginResp.getResult() != 0) {
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "IDL getWidFromQQIdl 调用远端失败" + uniformLoginResp.getErrmsg() );
		}
		UnifromLoginPo unifromLoginPo = new UnifromLoginPo() ;
		unifromLoginPo.setLsk(uniformLoginResp.getSkey()) ;
		unifromLoginPo.setNickName(nickName) ;
		unifromLoginPo.setWid(uniformLoginResp.getWgUid()) ;
		return unifromLoginPo;
	}

	@Override
	public Long loginOutForQQSdk(String mk, Long uin, String sk)
			throws BusinessException {
		WanggouUserLogoutResp resp = JdLoginClient.wanggouUserLogout(mk, uin, sk) ;
		if (resp == null) {
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "IDL loginOutForQQSdkIdl 调用失败");
		}
		Log.run.info("IDL loginOutForQQSdkIdl ,result="+resp.getResult()+",errMess=" + resp.getErrmsg()+",mk="+mk+",uin="+uin+",sk="+sk) ;
		return resp.getResult() ;
	}

	@Override
	public int checkJDLogin(String pin, String tgt, ClientInfo clientInfo)
			throws Exception {
		//如果是idc环境则
		if(EnvManager.isIdc()){
			WLoginHelper.getInstance().setLoginServiceAddr("10.198.15.151:10075|10.198.15.170:10075|10.198.15.151:10076|10.198.15.170:10076"); 
			WLoginHelper.getInstance().setReportServiceAddr("10.198.15.151:10074|10.198.15.170:10074");
		}else{
			WLoginHelper.getInstance().setLoginServiceAddr("10.129.128.150:10131"); 
			WLoginHelper.getInstance().setReportServiceAddr("10.129.128.150:10131");
		}
		/*public static final int kSuccess = 0;              // 调用成功
		public static final int kNetworkFailed = -1;       // 网络失败
		public static final int kMessageCheckFailed = -2;  // 返回数据包格式错误
		public static final int kRecvMsgDecodeFailed = -3; // 返回数据包解包失败
		public static final int kInputParameterError = -4; // 输入参数错误
        */
		int result = WLoginHelper.getInstance().verifyLogin(pin, TokenUtil.JD_OPEN_SDK_GET_UIN_APP_ID, tgt, clientInfo);
		String errMess = WLoginHelper.getInstance().getErrMsg() ;
		int errCode = WLoginHelper.getInstance().getErrCode() ;
		String sysErrMess = WLoginHelper.getInstance().getSysErrMsg() ;
		int sysErrCode = WLoginHelper.getInstance().getSysErrCode() ;
		/*REPLY_CODE_A2_INVALID = 0xb, 				// A2无效
		REPLY_CODE_A2_OVERTIME_FOR_TIME = 0xc, 		// A2过期
		REPLY_CODE_A2_OVERTIME_FOR_CHANGE_PWD = 0xd, 	// 因为用户改密码A2票过期
		REPLY_CODE_A2_OVERTIME_FOR_SECURITY = 0xce,   	// 因为安全策略A2票过期
		 */	
		int status = WLoginHelper.getInstance().getStatus() ; 
		Log.run.info("checkJDLogin result=" + result +  ",status=" 
					+ status + ",errCode=" + errCode + ",errMess=" + errMess 
					+ ",sysErrCode=" + sysErrCode + ",sysErrMess=" + sysErrMess) ;
		if (result != 0 ) {
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "checkJDLogin 调用失败 result error. result=" + result +  ",status=" 
					+ status + ",errCode=" + errCode + ",errMess=" + errMess+ ",sysErrCode=" + sysErrCode + ",sysErrMess=" + sysErrMess);
		} else if (status != 0 ) {
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "checkJDLogin 调用失败 result error. result=" + result +  ",status=" 
					+ status + ",errCode=" + errCode + ",errMess=" + errMess+ ",sysErrCode=" + sysErrCode + ",sysErrMess=" + sysErrMess);
		}
			
		
		return 0;
	}

	@Override
	public UnifromLoginPo getWidFromJDIdl(String mk, String openId) throws BusinessException {		 
		
		LoginInfoPo loginInfoPo = new LoginInfoPo() ;
		loginInfoPo.setLoginFrom(TokenUtil.JD_OPEN_SDK_IDL_LOGINFROM) ;
		loginInfoPo.setLoginIntensityType(TokenUtil.LOGIN_INTENSITY_TYPE_APP) ;
		loginInfoPo.setAccountType(TokenUtil.JD_OPEN_SDK_IDL_ACCOUNT_TYPE) ;
		try {
			loginInfoPo.setOpenid(new String(openId.getBytes(),"GBK")) ;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "PIN get GBK error! e= " + e.toString());
		}  
		loginInfoPo.setOpenidFrom(TokenUtil.JD_OPEN_SDK_IDL_OPENIDFROM); 
		loginInfoPo.setPasswd("jd") ;
		loginInfoPo.setQQSkey("jd") ;
		loginInfoPo.setAppid("jd") ;
		Long uin = System.currentTimeMillis() ;
		UniformLoginResp  uniformLoginResp = JdLoginClient.uniformLogin(mk, uin, loginInfoPo, TokenUtil.JD_OPEN_SDK_IDL_SOURCE);
		if (uniformLoginResp == null) {
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "IDL getWidFromJDIdl 调用失败");
		}
		if (uniformLoginResp.getResult() != 0) {
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "IDL getWidFromJDIdl 调用远端失败" + uniformLoginResp.getErrmsg() );
		}
		UnifromLoginPo unifromLoginPo = new UnifromLoginPo() ;
		unifromLoginPo.setLsk(uniformLoginResp.getSkey()) ;
		unifromLoginPo.setNickName(uniformLoginResp.getNickname()) ;
		unifromLoginPo.setWid(uniformLoginResp.getWgUid()) ;
		return unifromLoginPo;
	}

	@Override
	public String getJDUserInfo(String pin)  {
		// TODO Auto-generated method stub
		String userInfo = "{}" ;
		try {
			StringBuffer getUrl = new StringBuffer();
			if(EnvManager.isIdc()){
			getUrl.append(TokenUtil.JD_OPEN_SDK_GET_USER_INFO_URL);
			}else {
			getUrl.append("http://soa.u.jd.test/u/getUserInfo");
			}
			getUrl.append("?pin=").append(pin)
			.append("&token=").append(TokenUtil.JD_OPEN_SDK_GET_USER_INFO_TOKEN);
			userInfo =  HttpUtil.getJdPopHttpSvcProxyHost(getUrl.toString(), connectTimeout, readTimeout,
		            encoding, true);
			JSONObject jsonUserInfo = JSONObject.fromObject(userInfo) ;
			
			if (jsonUserInfo.getString("r").equals("0")) {
				JDUserInfo jdUserInfo = new JDUserInfo() ;
				jdUserInfo.setR(jsonUserInfo.getString("r")) ;
				jdUserInfo.setEx(jsonUserInfo.getString("ex")) ;
				jdUserInfo.setExecOk(jsonUserInfo.getBoolean("execOk")) ;
				JSONObject msg = jsonUserInfo.getJSONObject("msg");
				jdUserInfo.getMsg().setYunBigImageUrl(msg.getString("yunBigImageUrl"));
				jdUserInfo.getMsg().setYunMidImageUrl(msg.getString("yunMidImageUrl"));
				jdUserInfo.getMsg().setYunSmaImageUrl(msg.getString("yunSmaImageUrl"));
				jdUserInfo.getMsg().setPin(msg.getString("pin"));
				jdUserInfo.getMsg().setUserLevel(msg.getInt("userLevel"));
				jdUserInfo.getMsg().setNickname(msg.getString("nickname"));
				jdUserInfo.getMsg().setLevelName(msg.getString("levelName"));
				jdUserInfo.getMsg().setVerifyEmail(msg.getString("verifyEmail"));
				jdUserInfo.getMsg().setVerifyMobile(msg.getString("verifyMobile"));
				userInfo = JSONObject.fromObject(jdUserInfo).toString() ;
			} 
		} catch (Exception ex){
			Log.run.info("getJDUserInfo error " + ex.toString() ) ;
			userInfo = "{}" ;
		}
		return StringUtil.isEmpty(userInfo) || "\"\"".equals(userInfo)?"{}":userInfo ;
	}

	@Override
	public String getIDTokenForWXSdk(String code) throws Exception {
		// TODO Auto-generated method stub
		StringBuffer getUrl = new StringBuffer();
		getUrl.append(TokenUtil.WX_OPEN_SDK_GET_ID_TOKEN_URL)
		.append("?appid=").append(TokenUtil.WX_OPEN_SDK_GET_UIN_APP_ID)
		.append("&secret=").append(TokenUtil.WX_OPEN_SDK_GET_UIN_APP_SECRET)
		.append("&code=").append(code)
		.append("&grant_type=").append("authorization_code");
		return HttpUtil.get(getUrl.toString(), connectTimeout, readTimeout,
	            encoding, false);
	}

	@Override
	public String getWXUserInfo(String accessToken, String openid)
			throws Exception {
		StringBuffer getUrl = new StringBuffer();
		getUrl.append(TokenUtil.WX_OPEN_SDK_GET_USERINFO_URL)
		.append("?access_token=").append(accessToken)
		.append("&openid=").append(openid);
		return HttpUtil.get(getUrl.toString(), connectTimeout, readTimeout,
	            encoding, false);
	}

	@Override
	public UnifromLoginPo getWidFromWXIdl(String mk, String openId,
			String accessToken, String unionid) throws BusinessException {
		LoginInfoPo loginInfoPo = new LoginInfoPo() ;
		loginInfoPo.setLoginFrom(TokenUtil.WX_OPEN_SDK_IDL_LOGINFROM) ;
		loginInfoPo.setLoginIntensityType(TokenUtil.LOGIN_INTENSITY_TYPE_APP) ;
		loginInfoPo.setAccountType(TokenUtil.WX_OPEN_SDK_IDL_ACCOUNT_TYPE) ;
		loginInfoPo.setOpenid(openId) ; 
		loginInfoPo.setAppid(TokenUtil.WX_OPEN_SDK_GET_UIN_APP_ID);
		loginInfoPo.setPasswd(accessToken) ;
		loginInfoPo.setOpenidFrom(TokenUtil.WX_OPEN_SDK_IDL_OPENIDFROM); 
		loginInfoPo.setReserveStr(unionid) ;
		Long uin = System.currentTimeMillis() ;
		UniformLoginResp  uniformLoginResp = JdLoginClient.uniformLogin(mk, uin, loginInfoPo, TokenUtil.WX_OPEN_SDK_IDL_SOURCE);
		if (uniformLoginResp == null) {
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "IDL getWidFromQQIdl 调用失败");
		}
		if (uniformLoginResp.getResult() != 0) {
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "IDL getWidFromQQIdl 调用远端失败" + uniformLoginResp.getErrmsg() );
		}
		UnifromLoginPo unifromLoginPo = new UnifromLoginPo() ;
		unifromLoginPo.setLsk(uniformLoginResp.getSkey()) ;
		unifromLoginPo.setWid(uniformLoginResp.getWgUid()) ;
		return unifromLoginPo;
	}

	@Override
	public UnifromLoginPo getWidFromWTIdl(String mk, Long uin, String lk, String openId, String nickName)
			throws BusinessException {
		LoginInfoPo loginInfoPo = new LoginInfoPo() ;
		loginInfoPo.setLoginFrom(TokenUtil.WT_IDL_LOGINFROM) ;
		loginInfoPo.setLoginIntensityType(TokenUtil.LOGIN_INTENSITY_TYPE_APP) ;
		loginInfoPo.setAccountType(TokenUtil.WT_ACCOUNT_TYPE) ;
		loginInfoPo.setOpenid(openId) ; 
		loginInfoPo.setQQNumber(uin) ;
		loginInfoPo.setPasswd(lk) ;
		loginInfoPo.setReserveStr(nickName) ;
		UniformLoginResp  uniformLoginResp = JdLoginClient.uniformLogin(mk, uin, loginInfoPo, TokenUtil.WT_SOURCE);
		if (uniformLoginResp == null) {
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "IDL getWidFromWTIdl 调用失败");
		}
		if (uniformLoginResp.getResult() != 0) {
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "IDL getWidFromWTIdl 调用远端失败" + uniformLoginResp.getErrmsg() );
		}
		UnifromLoginPo unifromLoginPo = new UnifromLoginPo() ;
		unifromLoginPo.setLsk(uniformLoginResp.getSkey()) ;
		unifromLoginPo.setNickName(nickName) ;
		unifromLoginPo.setWid(uniformLoginResp.getWgUid()) ;
		return unifromLoginPo;
	}

}
