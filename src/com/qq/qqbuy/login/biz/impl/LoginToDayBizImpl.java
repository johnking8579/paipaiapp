package com.qq.qqbuy.login.biz.impl;

import java.util.Date;

import com.qq.qqbuy.common.util.DateUtils;
import com.qq.qqbuy.login.biz.LoginToDayBiz;
import com.qq.qqbuy.login.dao.LoginToDayDao;
import com.qq.qqbuy.login.po.AppToken;
import com.qq.qqbuy.login.po.LoginToDay;

public class LoginToDayBizImpl implements LoginToDayBiz {

	private LoginToDayDao loginToDayDao ;
	@Override
	public void writeLoginToDay(LoginToDay loginToDay) throws Exception {
		// TODO Auto-generated method stub
		LoginToDay loginToDayDB = loginToDayDao.findByWidDay(loginToDay.getWid(), loginToDay.getLoginDay()) ;
		if (loginToDayDB == null) {
			loginToDayDao.insertLoginToDay(loginToDay) ;
		} else {
			loginToDayDao.updateLoginToDay(loginToDay) ;
		}
	}

	@Override
	public LoginToDay convertToLoginToDay(AppToken appToken, String ip)
			throws Exception {
		// TODO Auto-generated method stub
		Date date = new Date() ;
		LoginToDay loginToDay = new LoginToDay() ;
		loginToDay.setAppID(appToken.getAppID()) ;
		loginToDay.setInsertTime(date.getTime()) ;
		loginToDay.setLatitude(appToken.getLatitude());
		loginToDay.setLoginDay(DateUtils.praseDateToStr(date)) ;
		loginToDay.setLoginIp(ip) ;
		loginToDay.setLoginTimes(1) ;
		loginToDay.setLoginType(appToken.getType()) ;
		loginToDay.setLongitude(appToken.getLongitude());
		loginToDay.setMk(appToken.getMk());
		loginToDay.setMt(appToken.getMt()) ;
		loginToDay.setUpdateTime(date.getTime());
		loginToDay.setVersionCode(appToken.getVersionCode());
		loginToDay.setWid(appToken.getWid());		
 		return loginToDay;
	}

	public LoginToDayDao getLoginToDayDao() {
		return loginToDayDao;
	}

	public void setLoginToDayDao(LoginToDayDao loginToDayDao) {
		this.loginToDayDao = loginToDayDao;
	}
	

}
