package com.qq.qqbuy.item.util;

import java.util.Map;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;

import com.paipai.component.c2cplatform.WebStubCntl;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.util.CoderUtil;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.TssHelper;
import com.qq.qqbuy.item.po.FreightInfo;
import com.qq.qqbuy.item.po.ItemInfoWithPrice;
import com.qq.qqbuy.item.po.MItem4J;
import com.qq.qqbuy.item.po.Mstock4J;
import com.qq.qqbuy.item.po.PPFreight4J;
import com.qq.qqbuy.item.po.PPItem4J;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ApiGetItemListReq;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ApiGetItemListResp;
import com.qq.qqbuy.thirdparty.openapi.po.GetItemResponse;
import com.qq.qqbuy.thirdparty.openapi.po.GetItemResponse.ExtendAttr;

/**
 * 商品相关工具类
 * 
 * @author rickwang
 */
public class ItemUtil {

	/**
	 * 判断商品id是否合法
	 * 
	 * @param itemId
	 *            商品ID
	 * @return true/false
	 */
	public static boolean isValidItemCode(String itemId) {
		if (itemId == null || itemId.length() != 32) {
			return false;
		}
		if (getSellerUin(itemId) < 10000) {
			return false;
		}
		return true;
	}

	/**
	 * 判断是否是快照商品id
	 * 
	 * @param itemId
	 *            商品ID
	 * @return true/false
	 */
	public static boolean isSnapId(String itemId) {
		if (itemId == null || itemId.length() != 32) {
			return false;
		}
		if (!"00".equals(itemId.substring(14, 16))) {
			return true;
		}
		return false;
	}

	/**
	 * 如果是快照商品id就替换商品
	 * 
	 * @param itemId
	 *            商品ID
	 * @return true/false
	 */
	public static String replaceSnapId(String itemId) {
		if (isSnapId(itemId)) {
			return itemId.substring(0, 14) + "00" + itemId.substring(16);
		}
		return itemId;

	}

	/**
	 * 根据商品ID取卖家QQ号
	 * 
	 * @param itemId
	 *            商品ID
	 * @return 成功返回相应的卖家QQ号，失败返回0
	 */
	public static long getSellerUin(final String itemId) {
		if (itemId == null || itemId.length() < 8) {
			return 0;
		}
		String uinStr = itemId.substring(0, 8);
		try {
			long uin = Long.parseLong(uinStr, 16);
			uin = reverseByteOrder(uin);
			return uin;
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	/**
	 * 转换字节序
	 * 
	 * @param in
	 *            待转换字节序的长整型数
	 * @return 转换字节序后的长整型数
	 */
	public static long reverseByteOrder(long in) {
		long out = 0;
		out = (in & 0xff) << 24;
		out |= (in & 0xff00) << 8;
		out |= (in & 0xff0000) >> 8;
		out |= (in & 0xff000000) >> 24;
		return out;
	}

	public static void main(String[] args) throws Exception {
		String itemId = "62B297140000000000623A6F0001C48F";
		long uin = getSellerUin(itemId);
		System.out.println(uin);
		System.out.println(isValidItemCode("62B297140000000000623A6F0001C48F"));
		System.out.println(isSnapId("3A9FD30000000000003A3A8707554074"));
		System.out.println(replaceSnapId("3A9FD30000005418703A3A8707554074"));

		String gfgf = TssHelper.getItemSrcImageUrl(65656, "item-000FA1BA-695BF6320000000000693B570807F04C.1.jpg");
		System.out.println(gfgf);
		Vector<String> item = new Vector<String>();
		item.add("3A9FD30000005418703A3A8707554074");
		testItem(item);
	}

	private static void testItem(Vector<String> itemList) throws Exception {
		ApiGetItemListReq req = new ApiGetItemListReq();
		ApiGetItemListResp resp = new ApiGetItemListResp();
		req.setSource("Qgo_itemAction");
		req.setItemIdList(itemList);
		WebStubCntl stub = new WebStubCntl();
		stub.setDwOperatorId(80000935);
		stub.setConnetTimeOut(3000);
		stub.setTimeout_ms(3000);
		Log.run.info("GetItemDetailByIDL,ItemIdList Size is:" + itemList.size());
		Log.run.info("ApiGetItemList begin,params=" + req.getItemIdList());
		stub.invoke(req, resp);
		resp.getApiItemList();
	}

	public static boolean isMobileItem(Map<Integer, Integer> properties) {
		if (properties != null) {
			Integer mobileItem = properties.get(128);
			if (mobileItem != null && mobileItem == 1) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 根据商品信息获取货到付款页面显示信息
	 * 
	 * @param itemRes
	 *            商品信息
	 * @param freightRsp
	 *            运费规则
	 * @param raddress
	 *            买家地址
	 * @return
	 */
	public static FreightInfo getFreightInfo(ItemInfoWithPrice itemInfoRes) {
		FreightInfo freightInfo = new FreightInfo();
		if (itemInfoRes == null) {
			return freightInfo;
		}
		try {
			MItem4J mItem = itemInfoRes.getMitem();
			PPItem4J ppItem = itemInfoRes.getPpItem();
			PPFreight4J ppFreight = ppItem.getFreight();
			// 设置商品价格
			freightInfo.price = mItem.getItemPrice();
			freightInfo.marketPrice = ppItem.getMarketPrice();
			freightInfo.maxPrice = mItem.getItemPrice();
			freightInfo.minPrice = mItem.getItemPrice();
			freightInfo.originalPrice = ppItem.getItemPrice();
			Vector<Mstock4J> mStocks = mItem.getStockList();
			for (Mstock4J mStock : mStocks) {
				if (mStock.getStockPrice() > freightInfo.maxPrice) {
					freightInfo.maxPrice = mStock.getStockPrice();
				}
				if (mStock.getStockPrice() < freightInfo.minPrice) {
					freightInfo.minPrice = mStock.getStockPrice();
				}
			}
			freightInfo.isSupportPayWhenArraive = mItem.isSupportCOD();
			// 判断是否为买卖宝卖家QQ号码
			freightInfo.isMMB = (mItem.getSellerType() == 1);

			freightInfo.isFree4Freight = mItem.isFree4Freight();
			freightInfo.savingFee = mItem.getSavingFee();
			freightInfo.additionalFee = mItem.getAdditionalFee();
			freightInfo.priceNormalAdd = ppFreight.getPriceNormalAdd();
			freightInfo.fee4Freight = mItem.getMailFee();
			freightInfo.setVersion(3);
			Log.run.debug("itemId=" + itemInfoRes.getMitem().getItemCode() + " fee4Freight=" + freightInfo.fee4Freight
					+ " priceNormalAdd=" + freightInfo.priceNormalAdd + " payWhenArraive ="
					+ freightInfo.isSupportPayWhenArraive + " price=" + freightInfo.price);
		} catch (Exception e) {
			Log.run.debug("getFreightInfo error,itemId[" + itemInfoRes.getMitem().getItemCode() + "]");
		}
		return freightInfo;
	}

	public static String getImgUrl(final String docPic, final String size) {
		if (docPic == null || size == null) {
			return "";
		}
		int pos = docPic.lastIndexOf('.');
		String imgURL = docPic.substring(0, pos + 1) + size + docPic.substring(pos);

		pos = imgURL.lastIndexOf('/');
		String temp = imgURL.substring(0, imgURL.lastIndexOf('/', pos - 1));
		if (!"http:/".equals(temp)) {
			imgURL = imgURL.substring(0, imgURL.lastIndexOf('/', pos - 1)) + imgURL.substring(pos);
		}
		return imgURL;
	}

	public static int getRedPacketAvariable(GetItemResponse itemRes) {
		int type = -1;
		for (ExtendAttr ea : itemRes.extendList) {
			if (ea.extendCode == 133L) {
				type = StringUtil.toInt(ea.extendValue, -1);
				break;
			}
		}
		if (itemRes.properties.get(2) == null || itemRes.properties.get(2) != 1)
			return 0;
		if (type != 105 && type != 110 && type != 120)
			return 0;
		return type - 100;
	}

	public static String getFirstPageStrByMaxLen(String str, int len) {
		return StringUtil.isNotEmpty(str) && str.length() > len ? str.substring(0, len) : str;
	}

	public static boolean isMoreThanOnePage(String str, int len) {
		return !(StringUtil.isEmpty(str) || str.length() <= len);
	}

	public static String getStrByPsAndPn(String str, int ps, int pn) {
		if (StringUtils.isEmpty(str) || str.length() <= (pn - 1) * ps) {
			return "";
		}
		if (str.length() > ps * pn) {
			return str.substring((pn - 1) * ps, pn * ps);
		}

		return str.substring((pn - 1) * ps);
	}

	public static int getTotalPage(String str, int len) {
		int totalPage = 0;
		if (StringUtils.isNotEmpty(str)) {
			totalPage = str.length() % len == 0 ? str.length() / len : str.length() / len + 1;
		} else {
			totalPage = 1;
		}
		return totalPage;
	}

	public static String genImageUrl(String url, String imageFormat) {
		if (url != null && url.length() > 10) {
			int pos = url.lastIndexOf("/");
			if (pos > 1)
				pos = url.indexOf(".", pos + 1);
			if (pos > 1)
				pos = url.indexOf(".", pos + 1);
			if (pos > 1)
				return url.substring(0, pos) + "." + imageFormat + url.substring(pos);
		}
		return url;
	}

	/**
	 * 根据商品图片名称获取URL
	 */
	public static String getImgUrlByName(String imgName, String imgType) {
		if (imgName == null || imgName.endsWith("noimage.jpg")) {
			return "";
		}
		return TssHelper.getImgUrl(imgName, imgType);
	}

	/**
	 * 构造商品ID
	 */
	public static String makeItemCode(long sellerUin, long itemID) {
		return String.format("%8s00000000%016X", new Object[] { CoderUtil.htonls(sellerUin), Long.valueOf(itemID) });
	}
}
