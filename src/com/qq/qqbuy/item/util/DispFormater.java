package com.qq.qqbuy.item.util;

import java.util.Date;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.util.DateUtils;
import com.qq.qqbuy.common.util.StringUtil;

/**
 * 用于Wap2页面展现内容和数据结构存在一定的格式化
 * @author winsonwu
 *
 */
public class DispFormater {
	
	// 用户评分等：保留一个小数位，传入的数据是乘以了100的
	public static String scoreDispFormater(long score) {
		return String.format("%.1f", score / 100.0);
	}
	
	// 价格分数等：保留两个小数位，传入的数据以分为单位，转换后的数据以元为单位
	public static String priceDispFormater(long score) {
		return String.format("%.2f", score / 100.0);
	}
	
	// 日期的转换：YYYY-MM-DD HH:mm格式
	public static String dateDispFormatterYMDHM(long time) {
		try {
			Date d = new Date(time*1000L);
			return DateUtils.formatDateToString(d, DateUtils.FORMAT_YMD_HM);
		} catch (Exception e) {
			Log.run.error("DispFormater#dateDispFormatterYMDHM fail to format time: " + time, e);
		}
		return "";
	}
	
	// 日期的转换：YYYY-MM-DD的格式，如2012-02-15
	public static String dateDispFormater(long time) {
		try {
			Date d = new Date(time*1000L);
			return DateUtils.formatDateToString(d, DateUtils.FORMAT_YMD);
		} catch (Exception e) {
			Log.run.error("DispFormater#dateDispFormater fail to format time: " + time, e);
		}
		return "";
	}
	
	public static String dateDispFormaterChinese(long time) {
		try {
			Date d = new Date(time*1000L);
			return DateUtils.formatDateToString(d, DateUtils.FORMAT_YYYYMMDD_HMS_CHINESE);
		} catch (Exception e) {
			Log.run.error("DispFormater#dateDispFormater fail to format time: " + time, e);
		}
		return "";
	}
	
	// 当前选择库存属性中|转化成_，主要是跳转到无线登录页面之后回跳到购物车页面由于|失败
	public static String currSkuAttrFormatter(String currSkuAttr) {
		if (!StringUtil.isEmpty(currSkuAttr)) {
			return currSkuAttr.replaceAll("\\|", "_");
		}
		return currSkuAttr;
	}
	
	public static String contentDispFormater(String content, int start, int end, boolean isAddEllipse) {
		if (!StringUtil.isEmpty(content) && start >= 0 && start < content.length() 
				&& end >=0 && end < content.length() && start <= end) {
			String subContent = content.substring(start, end);
			if (isAddEllipse) {
				return subContent + "...";
			}
			return subContent;
		}
		return content;
	}
	
	public static String filterChineseSymbol(String str) {
		if (!StringUtil.isEmpty(str)) {
			//替换英文的左右括号
			String _doc = str.replaceAll("\\s*\\(\\s*", "(").replaceAll("\\s*\\)\\s*", ")");
			//替换中文的左右括号
			_doc = _doc.replaceAll("\\s*（\\s*", "(").replaceAll("\\s*）\\s*", ")");
			//替换英文的冒号
			_doc = _doc.replaceAll("\\s*:\\s*", ":&nbsp;");
			//替换中文的冒号
			_doc = _doc.replaceAll("\\s*：\\s*", ":&nbsp;");
			return _doc;
		}
		return str;
	}
	
	public static void main(String[] args) {
//		System.out.println(scoreDispFormater(400));
//		
//		System.out.println(priceDispFormater(3405));
//		
//		System.out.println(dateDispFormater(1329285178));
		
//		String content = "客服回复：亲，很抱歉给你带来这样不好的购物体验。如果你的商品在保修期内出现质量问题，你是可以致电咱们售后的，会有专人为你服务帮你解决你的问题的。另如果你的签收时间和系统预计时间不符，出现物流延迟，你是可以致电咱们申请晚一补十的。感谢你的反馈，祝你愉快。";
//		System.out.println(contentDispFormater(content, 0, 84, true));
//		
//		
//		System.out.println(currSkuAttrFormatter("28:12|32:45"));
		
		String str = "闪：电发:货（仅广深）";
		System.out.println(filterChineseSymbol(str));
	}

}
