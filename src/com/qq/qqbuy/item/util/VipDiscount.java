package com.qq.qqbuy.item.util;

import java.text.DecimalFormat;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;

import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.item.po.ItemInfoWithPrice;
import com.qq.qqbuy.item.po.Mstock4J;
import com.qq.qqbuy.thirdparty.openapi.po.GetItemResponse;
import com.qq.qqbuy.thirdparty.openapi.po.GetItemResponse.Stock;

public class VipDiscount {
	public VipDiscount() {
	}
	public static final int CAIZUANLEVEL = 44;//彩钻等级
	public static final int SUPERQQLEVEL = 11;//超Q等级
	public static final int CAIZUANFLAG = 13;//彩钻点亮熄灭状态
	private String discount = "10000|10000|10000|10000|10000|10000";
	
	private Vector<Integer> discounts = new Vector<Integer>();
	private Vector<Integer> discountPrice =  new Vector<Integer>();
	public VipDiscount(String discount){
		if(discount != null && !"".equals(discount)){
		this.discount = discount;
		}
	}
	
	public VipDiscount(Vector<Integer> discounts,Vector<Integer> discountPrice){
		if(discounts != null && discountPrice != null){
			this.discounts = discounts;
			this.discountPrice = discountPrice;
		}
	}
	

	
	/**（灰度）
	 * 4 = 彩钻一级价 5 = 彩钻2级价,6 = 彩钻3级价,7= 彩钻4级价 8 = 彩钻5级价,9 = 彩钻6级价
	 * @param colorDiamondLevel
	 * @param superQQLevel
	 * @return
	 */
	public static int getPriceType(int colorDiamondLevel,int superQQLevel,long qq){
		int level = getLevel(colorDiamondLevel,superQQLevel);
		if(level <= 0){
			return 0;
		}
		if(level == 1){
			return 4;
		}
		if(level == 2){
			return 5;
		}
		if(level == 3){
			return 6;
		}
		if(level == 4){
			return 7;
		}
		if(level == 5){
			return 8;
		}
		if(level == 6){
			return 9;
		}
		return 0;
	}
	
	/**
	 * 获取折扣
	 * @param level
	 * @return
	 */
	public float getDiscountForPaipai(int level){
		try{
			if(discounts.size()>level-1){
				Integer _discount = discounts.get(level-1);
				float r_discount =(float)((10000-_discount)/10000.0f);
				return r_discount;
			}	
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0f;
	}
	/**
	 * 获取折扣后的价格（灰度）成本三期
	 * @param price
	 * @param level
	 * @return
	 */
	public  long getDiscountPriceNew(int level){
		if(level <= 0){
			return 0;
		}
		if(discountPrice.size() > 0){
			if(discountPrice.size() >= level){
				if(discountPrice.get(level -1) > 0){
					return discountPrice.get(level -1);
				}
			}
		}
		return 0;
	}
	
	/**
	 * 
	 * @param colorDiamondLevel
	 * @param superQQLevel
	 * @return
	 */
	public static int getLevel(int colorDiamondLevel,int superQQLevel){
		if(colorDiamondLevel > 0 && colorDiamondLevel >= superQQLevel){
			return colorDiamondLevel;
		}
		if(superQQLevel > 0 && superQQLevel <= 6){
			return superQQLevel;
		}else if(superQQLevel > 6){
			return 6;
		}
		return colorDiamondLevel;
		
	}
	/**
	 * 返回商品适用的折扣等级
	 * @param superQQLevel
	 * @return 只处理超Q等级
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 */
	public  static int getLevel(int superQQLevel){		
		return getLevel(0,superQQLevel);	
	}
	/**
	 * 获取折扣价
	 * @param price
	 * @param level
	 * @return
	 */
	public  String getDiscountInterval(long fromprice,long toprice,int level){
		String r_price = getReducePrice(fromprice,level)+ "-" + getReducePrice(toprice,level);
		return r_price;
	}
	
	

	
	/**
	 * 获取折扣的价格（灰度）
	 * @param price
	 * @param level
	 * @return
	 */
	public  String getDiscountPriceForPaipai(long price,int level){
	
		double truePrice =  price * (getDiscountForPaipai(level));
		DecimalFormat df = new DecimalFormat("0.00");
		String r_price = df.format((double)truePrice/100);
		return r_price;
	}
	
	/**
	 * 获取减少的
	 * @param price
	 * @param level
	 * @return
	 */
	public  String getReducePrice(long price,int level){		
		    DecimalFormat df = new DecimalFormat("0.00");	
			long reduce_p = price - this.getDiscountPriceNew(level);
			return df.format((double)reduce_p/100);		
	}
	
	
	
	/**
	 * 获取彩钻优惠提示（灰度）新的
	 * @param fromprice  区间最小价格
	 * @param toprice    区间最大价格
	 * @param colorDiamondLevel 彩钻等级
	 * @param superQQLevel  超Q等级
	 * @return
	 */
	public  String getDiscountTip(GetItemResponse res,ItemInfoWithPrice itemInfoWithPrice,int level){
	
		if(ItemUtil.isMobileItem(res.properties)){
			return getDiscountTipForQgo(res,itemInfoWithPrice,level);
		}else{
			return getDiscountTipForPaipai(res,level);
		}									
	}
	/**
	 * 获取彩钻优惠提示（灰度）新的
	 * @param fromprice  区间最小价格
	 * @param toprice    区间最大价格
	 * @param colorDiamondLevel 彩钻等级
	 * @param superQQLevel  超Q等级
	 * @return
	 */
	public  String getDiscountTip(GetItemResponse res,ItemInfoWithPrice itemInfoWithPrice){	
		return getDiscountTip(res, itemInfoWithPrice,0);									
	}
	/**
	 * 获取彩钻优惠提示（灰度）新的
	 * @param fromprice  区间最小价格
	 * @param toprice    区间最大价格
	 * @param colorDiamondLevel 彩钻等级
	 * @param superQQLevel  超Q等级
	 * @return
	 */
	public  long  getPaipaiDiscount(GetItemResponse res,long  price,int superQQLevel,int vipDiamondLevel){	
		if(!parseDiscount(res)){
			return 0;
		}						
		if(!ItemUtil.isMobileItem(res.properties)){	
			int level=getLevel(superQQLevel,vipDiamondLevel);
			if(level>0){
				double discount= price*getDiscountForPaipai(getLevel(superQQLevel,vipDiamondLevel));
				return (long)discount;				
			}			
		}
		return 0;
		
	}

	/**
	 * 获取等级对应折扣
	 * @param itemInfoWithPrice
	 * @param res
	 * @param level
	 * @return
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 */
	public String getDiscount(ItemInfoWithPrice itemInfoWithPrice ,GetItemResponse res,int level){
		if(level <= 0){
			return "";
		}
		DecimalFormat df = new DecimalFormat("0.00");
		if(res.isMobileItem() && itemInfoWithPrice!=null){
	        if(itemInfoWithPrice.getMitem().getHasIntervalPrice()){
	        	long maxprice = 0;
	    		long minprice = 0;
	    		maxprice=minprice=itemInfoWithPrice.getMitem().getStockDiscountPrice(level);
	    		Vector<Mstock4J> mStocks = itemInfoWithPrice.getMitem().getStockList();
	    		if(null!=mStocks){
	    			for(Mstock4J stock : mStocks){
	        			Integer price=(int)stock.getStockDiscountPrice(level);
	      				  if(maxprice <=price){
	        					maxprice = price;
	        				}
	        				if(minprice > price ||price==0){
	        					minprice = price;
	        				}  			    			
	        		}
	        		return df.format((double)(minprice)/100) + "-" + df.format((double)(maxprice)/100);
	    		}	    		
	        }		
			        return df.format((double)(itemInfoWithPrice.getMitem().getStockDiscountPrice(level))/100);	
		}else{
			if(res.stockList!=null){
				long maxprice = 0;
	    		long minprice = 0;
				for(Stock stock:res.stockList){
					Integer price=(int)stock.getStockDiscountPrice(level);
					if(maxprice < price){
    					maxprice = price;
    				}
    				if(minprice > price || minprice == 0){
    					minprice = price;
    				}  				
		     	}	
				if(minprice!=maxprice){
					   return df.format((double)(minprice)/100) + "-" + df.format((double)(maxprice)/100);
				}else{
					   return df.format((double)(minprice)/100);
				}
			}
		           return df.format((double)(res.getStockDiscountPrice(level))/100);		
		}		
	}
	
   /**
    * 获取等级对应折后价格
    * @param itemInfoWithPrice
    * @param res
    * @param level
    * @return
    * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
    */
	public String getAfterDiscountPrice(ItemInfoWithPrice itemInfoWithPrice,GetItemResponse res,int level){

		if(level <= 0){
			return "";
		}
		DecimalFormat df = new DecimalFormat("0.00");
		if(res.isMobileItem() && itemInfoWithPrice!=null){
	        if(itemInfoWithPrice.getMitem().getHasIntervalPrice()){
	        	long maxprice = 0;
	    		long minprice = 0;
	    		maxprice=minprice=itemInfoWithPrice.getMitem().getStockAfterDiscountPrice(level);
	    		Vector<Mstock4J> mStocks = itemInfoWithPrice.getMitem().getStockList();
	    		if(null!=mStocks){
	    			for(Mstock4J stock : mStocks){
	        			Integer price=(int)stock.getStockAfterDiscountPrice(level);
	      				  if(maxprice < price){
	        					maxprice = price;
	        				}
	        				if(minprice > price ||price==0){
	        					minprice = price;
	        				}  			    			
	        		}
	        		return df.format((double)(minprice)/100) + "-" + df.format((double)(maxprice)/100);
	    		}	    		
	        }		
			        return df.format((double)(itemInfoWithPrice.getMitem().getStockAfterDiscountPrice(level))/100);	
		}else{
			if(res.stockList!=null){
				long maxprice = 0;
	    		long minprice = 0;
				for(Stock stock:res.stockList){
					Integer price=(int)stock.getStockAfterDiscountPrice(level);
					if(maxprice < price){
    					maxprice = price;
    				}
    				if(minprice > price || minprice == 0){
    					minprice = price;
    				}  				
		     	}	
				if(minprice!=maxprice){
					   return df.format((double)(minprice)/100) + "-" + df.format((double)(maxprice)/100);
				}else{
					   return df.format((double)(minprice)/100);
				}
			}
		         	   return df.format((double)(res.getStockDiscountPrice(level))/100);		
		}	
	}
	/**
	 * 获取折扣后的价格（灰度）
	 * @param price
	 * @param level
	 * @return
	 */
	public  String getDiscountPrice(long price,int level,long qq){
		double truePrice =  price * (1-getDiscountForPaipai(level));
		DecimalFormat df = new DecimalFormat("0.00");
		String r_price = df.format((double)truePrice/100);
		return r_price;
	}
	/**
	 * 商详首页展示折扣价，手拍商品
	 * @param res
	 * @param itemInfoWithPrice
	 * @param level
	 * @return
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 */
	public  String getDiscountTipForQgo(GetItemResponse res,ItemInfoWithPrice itemInfoWithPrice,int level){
		long fromprice =itemInfoWithPrice.getMitem().getItemPrice() ; 
		long toprice = fromprice;
		discountPrice=itemInfoWithPrice.getMitem().getDiscountPrice();
		if(discountPrice==null || discountPrice.size()<1){
			return "";
		}		
		Vector<Mstock4J> mStockList = itemInfoWithPrice.getMitem().getStockList();
		if(mStockList!=null){
			for (Mstock4J mStock : mStockList) {
				if (mStock.getStockPrice() > toprice) {
					toprice=mStock.getStockPrice();
				}
				if (mStock.getStockPrice() < fromprice) {
					fromprice=mStock.getStockPrice();
				}
			}	
		}					
          return getDiscountTip(fromprice,toprice);		
	}
	/**
	 * 商详首页展示折扣价，非手拍商品
	 * @param res
	 * @param itemInfoWithPrice
	 * @param level
	 * @return
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 */
	public  String getDiscountTipForPaipai(GetItemResponse res,int level){		
			long fromprice =res.itemPrice ; 
			long toprice = fromprice;
			discountPrice=res.getDiscountPrice();	
			if(discountPrice==null || discountPrice.size()<1){
				return "";
			}
			if(res.stockList!=null){
				for(Stock stock:res.stockList){
					if (stock.stockPrice > toprice) {
						toprice=stock.stockPrice;
					}
					if (stock.stockPrice < fromprice) {
						fromprice=stock.stockPrice;
					}					
		     	}			
			}
		    return getDiscountTip(fromprice,toprice);				
	}
	
	 private String getDiscountTip(long fromprice,long topprice){
		    StringBuilder r_text = new StringBuilder(30);
			String minDiscount=getReducePrice(fromprice,1);
			r_text.append(minDiscount);//1级价格
			 String maxDisdount=getReducePrice(fromprice,6);
			if(fromprice != topprice){
				if(StringUtils.isNotEmpty(minDiscount)&& !minDiscount.equals(maxDisdount)){
					  r_text.append("-" + maxDisdount );//1级价格
				}
			}else if(!minDiscount.equals( maxDisdount)){
				r_text.append("-") .append(maxDisdount);
			}
			return r_text.toString();		 
	 }
	/**
	 * 商详页使用 
	 *解析商品属性判断是否支持彩钻价
	 * @param res
	 * @return 支持返回true，否则返回false
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 */
     private boolean parseDiscount(GetItemResponse res){
    	 if(StringUtils.isNotEmpty(res.colorDiamond)){
    		 return false;
    	 }
 		   String diss[] = StringUtil.split(res.colorDiamond, "|");	
 		    if(diss==null ||diss.length==0){
 				return false;
 			}
 		    for(String dis:diss){
 				if(!StringUtils.isNumeric(dis)){
 					return false;
 				}
 			     discounts.add(Integer.parseInt(dis));
 		   }
 		    return true;
     }
	public Vector<Integer> getDiscounts() {
		return discounts;
	}

	public void setDiscounts(Vector<Integer> discounts) {
		this.discounts = discounts;
	}


}
