package com.qq.qqbuy.item.util;

/**
 * Created by zhaohuayu on 2014/12/5.
 */
public class SellerCreditUtil {

	/**
	 * 根据卖家信用 计算卖家信用等级
	 * @param sellerCredit
	 * @return
	 */
    public static String getSellerCreditLevel(int sellerCredit) {
        if(sellerCredit <=4 && sellerCredit >=1) {
            return "A1" ;
        }
        if(sellerCredit <=10 && sellerCredit >=5) {
            return "A2" ;
        }
        if(sellerCredit <=20 && sellerCredit >=11) {
            return "A3" ;
        }
        if(sellerCredit <=40 && sellerCredit >=21) {
            return "A4" ;
        }
        if(sellerCredit <=100 && sellerCredit >=41) {
            return "A5" ;
        }
        if(sellerCredit <=300 && sellerCredit >=101) {
            return "B1" ;
        }
        if(sellerCredit <=1000 && sellerCredit >=301) {
            return "B2" ;
        }
        if(sellerCredit <=3000 && sellerCredit >=1001) {
            return "B3" ;
        }
        if(sellerCredit <=5000 && sellerCredit >=3001) {
            return "B4" ;
        }
        if(sellerCredit <=10000 && sellerCredit >=5001) {
            return "B5" ;
        }
        if(sellerCredit <=20000 && sellerCredit >=10001) {
            return "C1" ;
        }
        if(sellerCredit <=50000 && sellerCredit >=20001) {
            return "C2" ;
        }
        if(sellerCredit <=100000 && sellerCredit >=50001) {
            return "C3" ;
        }
        if(sellerCredit <=200000 && sellerCredit >=100001) {
            return "C4" ;
        }
        if(sellerCredit <=500000 && sellerCredit >=200001) {
            return "C5" ;
        }
        if(sellerCredit <=1000000 && sellerCredit >=500001) {
            return "D1" ;
        }
        if(sellerCredit <=2000000 && sellerCredit >=1000001) {
            return "D2" ;
        }
        if(sellerCredit <=5000000 && sellerCredit >=2000001) {
            return "D3" ;
        }
        if(sellerCredit <=10000000 && sellerCredit >=5000001) {
            return "D4" ;
        }
        if(sellerCredit >10000000) {
            return "D5" ;
        }
        return "0" ;
    }

    public static void main(String[] args) {
        System.out.println(getSellerCreditLevel(100));
    }
}
