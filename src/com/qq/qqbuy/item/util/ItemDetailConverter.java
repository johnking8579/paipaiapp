package com.qq.qqbuy.item.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;

import com.paipai.lang.uint32_t;
import com.paipai.util.string.StringUtil;
import com.qq.qqbuy.common.util.HTMLFilterService;
import com.qq.qqbuy.common.util.POUtil;
import com.qq.qqbuy.common.util.PageUtil;
import com.qq.qqbuy.item.constant.ItemState;
import com.qq.qqbuy.item.po.ItemAttrBO;
import com.qq.qqbuy.item.po.ItemAttrOptionBO;
import com.qq.qqbuy.item.po.ItemInfoWithPrice;
import com.qq.qqbuy.item.po.ItemPriceExtPo4J;
import com.qq.qqbuy.item.po.MItem4J;
import com.qq.qqbuy.item.po.MItemExtInfo4J;
import com.qq.qqbuy.item.po.MItemPic4J;
import com.qq.qqbuy.item.po.Mstock4J;
import com.qq.qqbuy.item.po.PPFreight4J;
import com.qq.qqbuy.item.po.PPItem4J;
import com.qq.qqbuy.item.po.PPStock4J;
import com.qq.qqbuy.item.po.eval.EvalPo;
import com.qq.qqbuy.item.po.eval.EvalRecordPo4J;
import com.qq.qqbuy.thirdparty.idl.item.protocol.GetItemExtInfoResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.GetItemInfoResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.MItemPic;
import com.qq.qqbuy.thirdparty.idl.item.protocol.MStock;
import com.qq.qqbuy.thirdparty.idl.item.protocol.PPStock;
import com.qq.qqbuy.thirdparty.idl.item.protocol.eval.EvalRecordPo;
import com.qq.qqbuy.thirdparty.idl.item.protocol.eval.GetComdyEvalListResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.qgo.ItemPriceExtPo;
import com.qq.qqbuy.thirdparty.idl.item.protocol.qgo.ItemStock;
import com.qq.qqbuy.thirdparty.idl.item.protocol.qgo.MMItemInfo;
import com.qq.qqbuy.thirdparty.idl.item.protocol.qgo.PPItemInfo;
import com.qq.qqbuy.thirdparty.openapi.po.GetItemResponse;
import com.qq.qqbuy.thirdparty.openapi.po.GetItemResponse.ExtendAttr;
import com.qq.qqbuy.thirdparty.openapi.po.GetItemResponse.ParsedAttr;
import com.qq.qqbuy.thirdparty.openapi.po.GetItemResponse.ParsedAttr.AttrOption;
import com.qq.qqbuy.thirdparty.openapi.po.GetItemResponse.Stock;

public class ItemDetailConverter {
	
	public static ItemInfoWithPrice convert(GetItemInfoResp getItemInfoResp) {
		if (getItemInfoResp != null) {
			ItemInfoWithPrice itemInfoWithPrice = new ItemInfoWithPrice();
			MItem4J mItem4J = new MItem4J();
			String[] icg=new String[]{"oPriceExt"};
			POUtil.copyProperties(getItemInfoResp.getMItem(), mItem4J,icg);
			mItem4J.setFree4Freight(getItemInfoResp.getMItem().getIsFree4Freight());//eclipse自动生成的set/get方法与接口的set/get方法名不一致导致赋值不成功
			if (getItemInfoResp.getMItem().getStockList() != null) {
				String[] ifg=new String[]{"oPriceExt"};
				Vector<Mstock4J> stockList = new Vector<Mstock4J>();
				for (MStock mStock : getItemInfoResp.getMItem().getStockList()) {
					Mstock4J mstock4J = new Mstock4J();
					POUtil.copyProperties(mStock, mstock4J,ifg);
					stockList.add(mstock4J);
				}
				mItem4J.setStockList(stockList);
			}

			PPItem4J ppItem4J = new PPItem4J();
			String[] ig=new String[]{"stockList","freight"};
			POUtil.copyProperties(getItemInfoResp.getPpItem(), ppItem4J,ig);
			if(getItemInfoResp.getPpItem().getFreight()!=null){
				PPFreight4J ppFreight4J=new PPFreight4J();
				ppFreight4J.setPriceNormal(getItemInfoResp.getPpItem().getFreight().getPriceNormal());
				ppFreight4J.setPriceNormalAdd(getItemInfoResp.getPpItem().getFreight().getPriceNormalAdd());
				ppItem4J.setFreight(ppFreight4J);
			}		
			
			
			if (getItemInfoResp.getPpItem().getStockList() != null) {
				Vector<PPStock4J> ppStockList = new Vector<PPStock4J>();
				for (PPStock ppStock : getItemInfoResp.getPpItem()
						.getStockList()) {
					PPStock4J ppStock4J = new PPStock4J();
					POUtil.copyProperties(ppStock, ppStock4J);
					ppStockList.add(ppStock4J);
				}
				ppItem4J.setStockList(ppStockList);
			}

			itemInfoWithPrice.setMitem(mItem4J);
			itemInfoWithPrice.setPpItem(ppItem4J);
			return itemInfoWithPrice;
		}
		return null;
	}

	public static MItemExtInfo4J convert(GetItemExtInfoResp getItemExtInfoResp) {
		if (getItemExtInfoResp != null) {
			MItemExtInfo4J mitemExtInfo4J = new MItemExtInfo4J();
			mitemExtInfo4J.setItemName(getItemExtInfoResp.getItemExtInfo()
					.getItemName());
			mitemExtInfo4J.setRaleItems(getItemExtInfoResp.getItemExtInfo()
					.getRaleItems());

			if (getItemExtInfoResp.getItemExtInfo().getItemPics() != null) {
				Vector<MItemPic4J> mitemPic4JList = new Vector<MItemPic4J>();
				for (MItemPic mitemPic : getItemExtInfoResp.getItemExtInfo()
						.getItemPics()) {
					MItemPic4J mitemPic4J = new MItemPic4J();
					mitemPic4J.setPicDesc(processUnknownStr(mitemPic.getPicDesc()));
					mitemPic4J.setPicIndex(mitemPic.getPicIndex());
					mitemPic4J.setPicUrl(mitemPic.getPicUrl());
					mitemPic4JList.add(mitemPic4J);
				}
				mitemExtInfo4J.setItemPics(mitemPic4JList);
			}
			return mitemExtInfo4J;
		}
		return null;
	}
	public static  MItem4J parseMMItem(MMItemInfo mitem){
		if(mitem!=null){
			MItem4J mmItem = new MItem4J();
			mmItem.setFree4Freight(mitem.getIsFree4Freight());
			mmItem.setDiscountPrice(converLongToInteger(mitem.getDiscountPrice()));
			mmItem.setAdditionalFee(mitem.getAdditionalFee());
			mmItem.setItemCode(mitem.getItemCode());
			mmItem.setItemPrice(mitem.getItemPrice());
			mmItem.setNormalPrice(mitem.getItemPrice());

			mmItem.setMailFee(mitem.getMailFee());
			mmItem.setMailFeeAdd(mitem.getMailFeeAdd());
			mmItem.setSupportCOD(mitem.getSupportCOD());
			mmItem.setSellerType(mitem.getSellerType());
			mmItem.setSavingFee(mitem.getSavingFee());
			mmItem.setoPriceExt(convertItemExtPriceInfo(mitem.getOItemPriceExt()));
			//if(grayUtil.isAllowGray()){//活动价
	            	 if(itemInActivity(mmItem.getoPriceExt())){
	            		 mmItem.setItemPrice((int)mitem.getOItemPriceExt().getActivityPrice());
	            	 }
	        // }
			if(mitem.getStockList()!=null){
				for(com.qq.qqbuy.thirdparty.idl.item.protocol.qgo.ItemStock stock:mitem.getStockList()){
					Mstock4J stockPO=new Mstock4J();
					stockPO.setStockAttr(stock.getStockAttr());
					stockPO.setSoldCount(stock.getSoldCount());
					stockPO.setStockCount(stock.getStockCount());
					stockPO.setDiscountPrice(stock.getDiscountPrice());
					stockPO.setStockPrice(stock.getStockPrice());
					stockPO.setNormalPrice(stock.getStockPrice());
					stockPO.setoPriceExt(convertItemExtPriceInfo(stock.getOPriceExt()));
					//if(grayUtil.isAllowGray()){//活动价
		            	 if(itemInActivity(stockPO.getoPriceExt())){
		            		 stockPO.setStockPrice(stockPO.getoPriceExt().getActivityPrice());
		            	 }
					//}
					mmItem.getStockList().add(stockPO);
				}
			}
		
		return mmItem;
		}
		return null;
		
	}
//	public static GetItemResponse parsePPItem(PPItemInfo ppItem,boolean needParseAttr){
//		  if(ppItem!=null ){
//			  ItemBO itemBO=new ItemBO();
//			  
//			  // 1、商品基本信息
//			  itemBO.setBuyLimit(ppItem.getBuyLimit());
//			  itemBO.setCityId(ppItem.getCity());
//			  itemBO.setCountryId(ppItem.getCountry());
//			  itemBO.setEmsPrice(ppItem.getEmsMailPrice());
//			  itemBO.setExpressPrice(ppItem.getExpressMailPrice());
//			  itemBO.setItemState(ItemState.getItemState(ppItem.getState()));
//			  itemBO.setStateDesc(ItemState.getShowMeg(ppItem.getState()));
//              itemBO.setFreightId(ppItem.getShippingfeeId());
//              itemBO.setItemCode(ppItem.getItemId());
//              itemBO.setItemLocalCode(ppItem.getItemLocalCode());
//              itemBO.setItemName(ppItem.getTitle());
//              itemBO.setItemPrice(ppItem.getPrice());
//
//              
//              //2、数量
//              itemBO.setBuyCount(ppItem.getBuyCount());
//              itemBO.setBuyNum(ppItem.getBuyNum());  
//              itemBO.setStockCount(ppItem.getNum());
//              itemBO.setTotalBuyCount(ppItem.getTotalBuyCount());
//              itemBO.setTotalBuyNum(ppItem.getToalBuyNum());
//              itemBO.setVisitCount(ppItem.getVisitCount());
//              
//              //3、促销价
//
//              ItemPriceExtPo oItemPriceExt = ppItem.getOItemPriceExt();
//              if (oItemPriceExt != null)
//              {
//            	  itemBO.setColorPrice(converLongToUin32(oItemPriceExt.getColorPrice()));
//            	  itemBO.setActivityPrice(oItemPriceExt.getActivityPrice());
//            	  itemBO.setShopVipPrice(converLongToUin32(oItemPriceExt.getShopVipPrice()));
//                 
//                  
//              }
//              
//              // 4、扩展属性，目前这里主要是用于红包的使用
//              long redPrice = ppItem.getRedPrice();
//              if (redPrice > 0)
//              {
//            	  itemBO.setRedPrice(redPrice);
//                  Map<ItemExtend, String> redMap = new HashMap<ItemExtend, String>(
//                          1);
//                  redMap.put(ItemExtend.getItemExtend(133), ""
//                          + (redPrice + 100));
//                  itemBO.setExtendInfo(redMap);
//              }
//
//              // 5、处理图片信息
//              Vector<String>  vecImg = null;
//              if (ppItem.getVecImg() != null&& !ppItem.getVecImg().isEmpty())
//              {          
//                  if (ppItem.getVecImg().size() > 0)
//                	  itemBO.setPicLink(ppItem.getVecImg().get(0));
//                  if (ppItem.getVecImg().size() > 1)
//                	  itemBO.setExtPicsLink(ppItem.getVecImg().subList(1,ppItem.getVecImg().size() - 1));
//                  itemBO.setVecImg(vecImg);
//              }
//	         	          
//	          // 6、库存信息           
//              List<ItemStockBo> itemStockBo = new ArrayList<ItemStockBo>();
//              if (ppItem.getStockList() != null
//                      && !ppItem.getStockList().isEmpty())
//              {   int index=0;
//                  for (com.qq.qqbuy.thirdparty.idl.item.protocol.qgo.ItemStock stockItem : ppItem.getStockList())
//                  {
//                      ItemStockBo oItemStockBo = new ItemStockBo();
//                   
//                      // 促销价
//                      if (( stockItem.getOPriceExt()) != null)
//                      {
//                          oItemStockBo.setColorPrice(converLongToUin32(stockItem.getOPriceExt()
//                                  .getColorPrice()));
//                          oItemStockBo.setActivityPrice(stockItem.getOPriceExt()
//                                  .getActivityPrice());
//                          oItemStockBo.setShopVipPrice(converLongToUin32(stockItem.getOPriceExt()
//                                  .getShopVipPrice()));
//                      }
//                      oItemStockBo.setIndex(index++);              
//                      oItemStockBo.setSellerUin(ppItem.getQqUin());
//                      oItemStockBo.setSoldCount(stockItem.getSoldCount());
//                      oItemStockBo.setStockAttr(stockItem.getStockAttr());                    
//                      oItemStockBo.setStockCount(stockItem.getStockCount());
//                      oItemStockBo.setStockId("" + stockItem.getStockId());
//                      oItemStockBo.setStockPrice(stockItem.getStockPrice());
//                      itemStockBo.add(oItemStockBo);
//                  }
//              }
//              
//	          
//               //如果没有库存属性信息，就生成一个以支持老版本            
//              if (ppItem.getStockList().isEmpty())
//              {
//                  ItemStockBo oItemStockBo = new ItemStockBo();                  
//                  oItemStockBo.setSellerUin(ppItem.getQqUin());       
//                  oItemStockBo.setSoldCount(ppItem.getPayCount());
//                  oItemStockBo.setStockAttr("");
//                  oItemStockBo.setStockDesc("");
//                  oItemStockBo.setStockId("");
//                  oItemStockBo.setStockLocalCode(ppItem.getItemLocalCode());
//                  oItemStockBo.setStockPrice(ppItem.getPrice());
//                  oItemStockBo.setActivityPrice(itemBO.getActivityPrice());
//                  oItemStockBo.setColorPrice(itemBO.getColorPrice());              
//                  oItemStockBo.setIndex(0);
//                  oItemStockBo.setShopVipPrice(itemBO.getShopVipPrice());
//                  itemStockBo.add(oItemStockBo);
//              }
//              itemBO.setItemStockBo(itemStockBo);
//              
//              // 6、property值转换
//			  itemBO.setProperties(ppItem.getProperty());
//			 	
//			     
//	          // 7、解析商品类目信息
//	          if (needParseAttr)
//	          {
//	              try
//	              {	     
//	                  itemBO.setParsedAttrList(ItemHelper.parseAttr(ppItem.getAttrText(), ppItem.getLeafClassId()));
//	              } catch (Exception e)
//	              {
//	              Log.run.error("getItemFromPaipai2 parse item  attr error",e);
//	              }
//		       }
//	          return 	  ItemDetailConverter.convertItemInfo(ppItem.getItemDetail(), itemBO);
//		  }
//		 return null;	          
//	}
	private static Vector<uint32_t> converLongToUin32(Vector<Long> list){
		if(list!=null&& !list.isEmpty()){
			Vector<uint32_t> resultList=new Vector<uint32_t> ();
			 for(Long element:list){
				 if(element!=null){
					 resultList.add(new uint32_t(element.longValue()));
				 }
			}
			 return resultList;
		}
		return null;
	}
	public static Vector<Integer> converLongToInteger(Vector<Long> list){
		if(list!=null&& !list.isEmpty()){
			Vector<Integer> resultList=new Vector<Integer> ();
			 for(Long element:list){
				 resultList.add(element.intValue());
			}
			 return resultList;
		}
		return null;
	}
	/**
	 * 转换商品促销价格信息
	 * @param itemPriceExt
	 * @return
	 * @date:2013-4-6
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
	public static ItemPriceExtPo4J  convertItemExtPriceInfo(ItemPriceExtPo itemPriceExt){
    	ItemPriceExtPo4J itemPriceExtPO=new ItemPriceExtPo4J();
    	itemPriceExtPO.setActiveProperty(itemPriceExt.getActiveProperty());
    	itemPriceExtPO.setActivityPrice(itemPriceExt.getActivityPrice());
    	itemPriceExtPO.setActivityDesc(itemPriceExt.getActivityDesc());
    	itemPriceExtPO.setState(itemPriceExt.getState());
    	itemPriceExtPO.setBeginTime(itemPriceExt.getBeginTime());
    	itemPriceExtPO.setEndTime(itemPriceExt.getEndTime());
    	itemPriceExtPO.setColorPrice((itemPriceExt.getColorPrice()));
    	itemPriceExtPO.setShopVipPrice((itemPriceExt.getShopVipPrice()));
    	itemPriceExtPO.setActivityNeedShow(itemPriceExt.getIsActivityNeedShow());
    	return itemPriceExtPO;
    }
	public static EvalPo convert(GetComdyEvalListResp getComdyEvalListResp, int ps) {
		EvalPo evalPo=new EvalPo();
		if (getComdyEvalListResp != null) {
			evalPo.setTotalNum(getComdyEvalListResp.getTotalNum());
			if(getComdyEvalListResp.getEvalInfoList()!=null){				
				Vector<EvalRecordPo4J> evalInfoList = new Vector<EvalRecordPo4J>();
				Vector<EvalRecordPo> evalInfoListResp = getComdyEvalListResp.getEvalInfoList() ;
				int size = Math.min(ps, evalInfoListResp.size()) ;
				for(int i=0 ; i< size ; i++) {
					EvalRecordPo evalRecordPo = evalInfoListResp.get(i) ;
					if (evalRecordPo.getBuyerExplain().endsWith("[该评价来自手机拍拍客户端]")) {
						evalRecordPo.setBuyerExplain(evalRecordPo.getBuyerExplain().substring(0,evalRecordPo.getBuyerExplain().lastIndexOf("[该评价来自手机拍拍客户端]")));
					}
					EvalRecordPo4J evalRecordPo4J=new EvalRecordPo4J();
					//evalRecordPo4J.setBuyerEvalDelFlag(evalRecordPo.getBuyerEvalDelFlag());
					evalRecordPo4J.setBuyerEvalLevel(evalRecordPo.getBuyerEvalLevel());
					evalRecordPo4J.setBuyerEvalTime(evalRecordPo.getBuyerEvalTime());
					evalRecordPo4J.setBuyerExplain(evalRecordPo.getBuyerExplain());
					evalRecordPo4J.setBuyerNickName(evalRecordPo.getBuyerNickName());
					//evalRecordPo4J.setBuyerScore(evalRecordPo.getBuyerScore());
					evalRecordPo4J.setBuyerUin(evalRecordPo.getBuyerUin());
					//evalRecordPo4J.setCommodityId(evalRecordPo.getCommodityId());
					//evalRecordPo4J.setCommodityLogo(evalRecordPo.getCommodityLogo());
					//evalRecordPo4J.setCommodityTitle(evalRecordPo.getCommodityTitle());
					//evalRecordPo4J.setDealFinishTime(evalRecordPo.getDealFinishTime());
					//evalRecordPo4J.setDealId(evalRecordPo.getDealId());
					//evalRecordPo4J.setDealPayment(evalRecordPo.getDealPayment());
					//evalRecordPo4J.setDsr1(evalRecordPo.getDsr1());
					//evalRecordPo4J.setDsr2(evalRecordPo.getDsr2());
					//evalRecordPo4J.setDsr3(evalRecordPo.getDsr3());
					//evalRecordPo4J.setDsrEvalTime(evalRecordPo.getDsrEvalTime());
					//evalRecordPo4J.setDsrFlag(evalRecordPo.getDsrFlag());
					evalRecordPo4J.setEvalCreateTime(evalRecordPo.getEvalCreateTime());
					//evalRecordPo4J.setFdealId(evalRecordPo.getFDealId());
					//evalRecordPo4J.setFtradeId(evalRecordPo.getFtradeId());
					//evalRecordPo4J.setNumCanReply(evalRecordPo.getNumCanReply());
					evalRecordPo4J.setPaytype(evalRecordPo.getPaytype());
					//evalRecordPo4J.setPeerSysEval(evalRecordPo.getPeerSysEval());
					evalRecordPo4J.setPrice(evalRecordPo.getPrice());
					//evalRecordPo4J.setScoreFlag(evalRecordPo.getScoreFlag());
					//evalRecordPo4J.setScoreTime(evalRecordPo.getScoreTime());
					//evalRecordPo4J.setSelfSysEval(evalRecordPo.getSelfSysEval());
					//evalRecordPo4J.setSellerEvalDelFlag(evalRecordPo.getSellerEvalDelFlag());
					//evalRecordPo4J.setSellerEvalLevel(evalRecordPo.getSellerEvalLevel());
					//evalRecordPo4J.setSellerEvalTime(evalRecordPo.getSellerEvalTime());
					//evalRecordPo4J.setSellerExplain(evalRecordPo.getSellerExplain());
					//evalRecordPo4J.setSellerNickName(evalRecordPo.getSellerNickName());
					//evalRecordPo4J.setSellerScore(evalRecordPo.getSellerScore());
					//evalRecordPo4J.setSellerUin(evalRecordPo.getSellerUin());
					//evalRecordPo4J.setSnapId(evalRecordPo.getSnapId());
					//evalRecordPo4J.setBuyerReason(evalRecordPo.getBuyerReason());
					//evalRecordPo4J.setSellerReason(evalRecordPo.getSellerReason());
					/*if(evalRecordPo.getReplyMsg()!=null){
					  Vector<CEvalReply4J> replyMsg = new Vector<CEvalReply4J>();
						for(CEvalReply cevalReply:evalRecordPo.getReplyMsg()){
							CEvalReply4J cevalReply4J =new CEvalReply4J ();
							cevalReply4J.setContent(cevalReply.getContent());
							cevalReply4J.setCreateTime(cevalReply.getCreateTime());
							cevalReply4J.setDealId(cevalReply.getDealId());
							cevalReply4J.setFdealId(cevalReply.getFDealId());
							cevalReply4J.setId(cevalReply.getId());
							cevalReply4J.setUin(cevalReply.getUin());
							replyMsg.add(cevalReply4J);
						}
						//evalRecordPo4J.setReplyMsg(replyMsg);
					}*/
					//暂时这么干，之后要通过uin获得图片信息，去QQ化
					//小于39亿为qq=wid
					if (evalRecordPo.getBuyerUin() < 3900000000L) {
						evalRecordPo4J.setBuyerLogo("http://qlogo.store.qq.com/qzone/"+evalRecordPo.getBuyerUin() +"/"+evalRecordPo.getBuyerUin() +"/100");
					} else {
						evalRecordPo4J.setBuyerLogo("");
					}					
					evalInfoList.add(evalRecordPo4J); 
              }
				evalPo.setEvalInfolist(evalInfoList);
			}
		}
		return evalPo;
	}


	/**
	 * 1、dsr=5 && length >=10
	 * 2、dsr=5 || length >=10
	 * 3、第一条优评
	 * @param getComdyEvalListResp
	 * @param ps
	 * @return
	 */
	public static EvalPo convertForBest(GetComdyEvalListResp getComdyEvalListResp,int ps) {
		EvalPo evalPo=new EvalPo();
		EvalRecordPo4J evalRecordPo4JBest=null; //最优评价
		EvalRecordPo4J evalRecordPo4JGood=null;//第一条评价
		EvalRecordPo4J evalRecordPo4JBetter=null;//满足两个条件的评价

		if (getComdyEvalListResp != null) {
			if(getComdyEvalListResp.getEvalInfoList()!=null){				
				Vector<EvalRecordPo4J> evalInfoList = new Vector<EvalRecordPo4J>();
				Vector<EvalRecordPo> evalInfoListResp = getComdyEvalListResp.getEvalInfoList() ;
				int size = Math.min(ps, evalInfoListResp.size()) ;
				for(int i=0 ; i< size ; i++) {
					EvalRecordPo evalRecordPo = evalInfoListResp.get(i) ;
					if (evalRecordPo.getBuyerExplain().endsWith("[该评价来自手机拍拍客户端]")) {
						evalRecordPo.setBuyerExplain(evalRecordPo.getBuyerExplain().substring(0,evalRecordPo.getBuyerExplain().lastIndexOf("[该评价来自手机拍拍客户端]")));
					}
					if (evalRecordPo.getDsr1() == 5 && evalRecordPo.getDsr2() == 5
							&& evalRecordPo.getDsr3() == 5 && evalRecordPo.getBuyerExplain().length() >= 10) {
						if (evalRecordPo4JBest == null || evalRecordPo.getBuyerExplain().length() > evalRecordPo4JBest.getBuyerExplain().length()) {
							evalRecordPo4JBest = converToEvalRecordPo(evalRecordPo) ;
						}
					} else if (evalRecordPo4JBest == null && evalRecordPo4JBetter == null) {
							if ((evalRecordPo.getDsr1() == 5 && evalRecordPo.getDsr2() == 5 && evalRecordPo.getDsr3() == 5) 
									|| evalRecordPo.getBuyerExplain().length() >= 10) {
								evalRecordPo4JBetter = converToEvalRecordPo(evalRecordPo) ;
							} else if (evalRecordPo4JGood == null && i == 0) {
								evalRecordPo4JGood = converToEvalRecordPo(evalRecordPo) ;
							}						
					}				
					
				}
				if (evalRecordPo4JBest != null){
					evalInfoList.add(evalRecordPo4JBest);
					evalPo.setTotalNum(1);
					evalPo.setEvalInfolist(evalInfoList);
				} else if (evalRecordPo4JBetter != null) {
					evalInfoList.add(evalRecordPo4JBetter);
					evalPo.setTotalNum(1);
					evalPo.setEvalInfolist(evalInfoList);
				} else if (evalRecordPo4JGood != null) {
					evalInfoList.add(evalRecordPo4JGood);
					evalPo.setTotalNum(1);
					evalPo.setEvalInfolist(evalInfoList);
				} 
			}
		}
		return evalPo;
	}

	
	private static EvalRecordPo4J converToEvalRecordPo (EvalRecordPo evalRecordPo) {
		EvalRecordPo4J evalRecordPo4JTmp=new EvalRecordPo4J();
		evalRecordPo4JTmp.setBuyerEvalLevel(evalRecordPo.getBuyerEvalLevel());
		evalRecordPo4JTmp.setBuyerEvalTime(evalRecordPo.getBuyerEvalTime());
		evalRecordPo4JTmp.setBuyerExplain(evalRecordPo.getBuyerExplain());
		evalRecordPo4JTmp.setBuyerNickName(evalRecordPo.getBuyerNickName());
		evalRecordPo4JTmp.setBuyerUin(evalRecordPo.getBuyerUin());
		evalRecordPo4JTmp.setEvalCreateTime(evalRecordPo.getEvalCreateTime());
		evalRecordPo4JTmp.setPaytype(evalRecordPo.getPaytype());
		evalRecordPo4JTmp.setPrice(evalRecordPo.getPrice());
		//暂时这么干，之后要通过uin获得图片信息，去QQ化
		//小于39亿为qq=wid
		if (evalRecordPo.getBuyerUin() < 3900000000L) {
			evalRecordPo4JTmp.setBuyerLogo("http://qlogo.store.qq.com/qzone/"+evalRecordPo.getBuyerUin() +"/"+evalRecordPo.getBuyerUin() +"/100");
		} else {
			evalRecordPo4JTmp.setBuyerLogo("");
		}
		return evalRecordPo4JTmp ;
	}
	/**
	 * 转换拍拍商品信息
	 * @param itemBO
	 * @return
	 * @date:2013-4-9
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
	public static GetItemResponse convertPPItemInfo(PPItemInfo itemBO){
		GetItemResponse res=new GetItemResponse();
		if(itemBO!=null){
			 res.itemCode = itemBO.getItemId();
             res.itemLocalCode = itemBO.getItemLocalCode();
             res.itemName = itemBO.getTitle();

             res.itemState = ItemState.getInfo(itemBO.getState());//IS_FOR_SALE
             res.stateDesc = ItemState.getShowMeg(itemBO.getState());
             res.properties=itemBO.getProperty();
             res.detailInfo=processWapItemDetail(itemBO.getItemDetail());
             
             res.stockCount = itemBO.getNum();
             res.itemPrice = itemBO.getPrice();


             res.marketPrice = itemBO.getMarketPrice();
             res.expressPrice = itemBO.getExpressMailPrice();
             res.emsPrice = itemBO.getEmsMailPrice();
             res.mailPrice = itemBO.getNormalMailPrice();

             res.classId = itemBO.getLeafClassId()+"";
             res.cityId = itemBO.getCity();
             res.provinceId = itemBO.getProvince();
             res.countryId = itemBO.getCountry();
             res.buyLimit = itemBO.getBuyLimit();
             res.freightId = itemBO.getShippingfeeId()+"";
             res.codShipTempId=itemBO.getCodShipTempId();//货到付款运费模板
             
             // 5、处理图片信息
             if (itemBO.getVecImg() != null&& !itemBO.getVecImg().isEmpty())
             {          
                 if (itemBO.getVecImg().size() > 0)
                	 res.picLink=itemBO.getVecImg().get(0);
                 if (itemBO.getVecImg().size() > 1)
                	 res.picLinks= itemBO.getVecImg().subList(1,itemBO.getVecImg().size() );
             }
             res.sellerPayFreight = itemBO.getTransportPriceType();
             res.sellerName = itemBO.getQqNick();
             res.sellerUin = itemBO.getQqUin();
             res.visitCount = itemBO.getVisitCount();
             res.soldCount = itemBO.getPayNum();
             res.soldTotalCount = itemBO.getTotalPayNum();
             res.soldTimes = itemBO.getPayCount();
             res.soldTotalTimes = itemBO.getTotalPayCount();
             res.buyNum = itemBO.getBuyNum();
             res.totalBuyNum = itemBO.getToalBuyNum();
             res.buyCount = itemBO.getBuyCount();
             res.totalBuyCount = itemBO.getTotalBuyCount();
             res.weight = itemBO.getWeight();
             res.setoItemPriceExt(convertItemExtPriceInfo(itemBO.getOItemPriceExt()));//促销价
             //解析库存
             if(itemBO.getStockList()!=null){
            	 for(ItemStock item:itemBO.getStockList()){
            		 Stock stock=res.new Stock();
            		 stock.stockId=item.getStockId();
            		 stock.stockAttr=item.getStockAttr();
            		 stock.stockCount=item.getStockCount();
            		 stock.soldCount=item.getSoldCount();
            		 stock.stockPrice=item.getStockPrice();   
            		 stock.setNormalPrice(item.getStockPrice());
            		 stock.setoItemPriceExt(convertItemExtPriceInfo(item.getOPriceExt()));
            		 stock.setDiscountPrice(converLongToInteger(item.getOPriceExt().getColorPrice()));//彩钻价
            		 //if(grayUtil.isAllowGray()){
            			 if(itemInActivity(stock.getoItemPriceExt())){
            				 stock.stockPrice=item.getOPriceExt().getActivityPrice();
            			 }
            		 //} 
            		 res.getStockList().add(stock);
                 } 
             }
             //解析扩展属性
             if(itemBO.getMultiMapExt()!=null){
            	 for(Integer key:itemBO.getMultiMapExt().keySet()){
            		 ExtendAttr attr = res.new ExtendAttr();
                     attr.extendCode = key;
                     //attr.extendName = extendNode.getChildText("extendName");
                     //attr.showMeg = extendNode.getChildText("showMeg");
                     attr.extendValue =itemBO.getMultiMapExt().get(key);
                     res.extendList.add(attr);
                 }
                 res.colorDiamond = itemBO.getMultiMapExt().get(40);
             }
             res.setDiscountPrice(converLongToInteger(itemBO.getOItemPriceExt().getColorPrice()));//彩钻价
             //if(grayUtil.isAllowGray()){//活动价
            	 if(itemInActivity(res.getoItemPriceExt())){
                	 res.itemPrice= itemBO.getOItemPriceExt().getActivityPrice();
            	 }
            // }
             res.setSupportPromotion(itemInActivity(res.getoItemPriceExt()));
             res.setNormalPrice(itemBO.getPrice());//正常价
		}
		return res;
		
	}
	
	private static void convertItemAttr(List<ItemAttrBO> parsedAttrList,GetItemResponse res ){
		ArrayList<ParsedAttr> parsedAttrListResult = new ArrayList<ParsedAttr>();
		if(parsedAttrList!=null && !parsedAttrList.isEmpty()){
			for(ItemAttrBO itemAttrBO:parsedAttrList){
				if(itemAttrBO!=null){
					ParsedAttr parsedAttr=res.new ParsedAttr();
					parsedAttr.attrId=itemAttrBO.getAttrId()+"";
					parsedAttr.attrName=itemAttrBO.getAttrName();
					if(itemAttrBO.getAttrOptionList()!=null){
						for(ItemAttrOptionBO option:itemAttrBO.getAttrOptionList()){
							AttrOption attrOption=parsedAttr.new AttrOption();
							attrOption.attrOptionId=option.getAttrOptionId()+"";
							attrOption.attrOptionName=option.getAttrOptionName();
							parsedAttr.attrOptionList.add(attrOption);
						}
					}
					parsedAttrListResult.add(parsedAttr);
					res.parsedAttrList=parsedAttrListResult;
				}
			}
		}
	}
	public static String processWapItemDetail(String detailInfo){
    	String detail2Show = "";
		if (StringUtils.isNotEmpty(detailInfo) ) {
		        detail2Show = PageUtil.filterHtml(detailInfo);
				detail2Show= HTMLFilterService.handleHTMLContent(detail2Show);
			    detail2Show = PageUtil.trimDetail(detail2Show);
			    detail2Show = PageUtil.processChineseColon(detail2Show);
		}
	    return detail2Show;
    }
	
	private static boolean itemInActivity(ItemPriceExtPo4J price){
		return price!=null && price.getState()==2 && price.getActivityPrice()>0;
		
	}
	 private static String processUnknownStr(String str){
		 if(StringUtil.isNotEmpty(str) && str.startsWith("~")){
			 return "";
		 }
		 return str;
	 }
}
