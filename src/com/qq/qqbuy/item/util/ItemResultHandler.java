package com.qq.qqbuy.item.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.Pager;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.HTMLFilterService;
import com.qq.qqbuy.common.util.PageUtil;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.item.biz.PPItemBiz;
import com.qq.qqbuy.item.constant.ItemConstantForWap;
import com.qq.qqbuy.item.constant.ItemConstants;
import com.qq.qqbuy.item.constant.ItemState;
import com.qq.qqbuy.item.po.ItemBO;
import com.qq.qqbuy.item.po.ItemInfoWithPrice;
import com.qq.qqbuy.item.po.ItemPic;
import com.qq.qqbuy.item.po.ItemStock;
import com.qq.qqbuy.item.po.MItem4J;
import com.qq.qqbuy.item.po.MItemExtInfo4J;
import com.qq.qqbuy.item.po.MItemPic4J;
import com.qq.qqbuy.item.po.Mstock4J;
import com.qq.qqbuy.item.po.PPItem4J;
import com.qq.qqbuy.item.po.SaleAttrPair;
import com.qq.qqbuy.item.po.wap2.ItemDetailInfoVO;
import com.qq.qqbuy.item.po.wap2.ItemDetailVO;
import com.qq.qqbuy.item.po.wap2.ItemDiscountVO;
import com.qq.qqbuy.item.po.wap2.ItemPicVO;
import com.qq.qqbuy.item.po.wap2.ItemPropVO;
import com.qq.qqbuy.item.po.wap2.ItemStockAttrVO;
import com.qq.qqbuy.thirdparty.openapi.po.GetItemResponse;
import com.qq.qqbuy.thirdparty.openapi.po.GetItemResponse.ExtendAttr;
import com.qq.qqbuy.thirdparty.openapi.po.GetItemResponse.ParsedAttr;
import com.qq.qqbuy.thirdparty.openapi.po.GetItemResponse.ParsedAttr.AttrOption;
import com.qq.qqbuy.thirdparty.openapi.po.GetItemResponse.Stock;
import com.qq.qqbuy.user.po.User;
import com.qq.qqbuy.user.util.UserDetailUtil;

public class ItemResultHandler {

	/**
	 * 解析商品属性
	 * @param parsedAttrList
	 * @param context
	 * @return
	 */
	public static String getItemAttrStr(ArrayList<ParsedAttr> parsedAttrList) {
		if (parsedAttrList == null) {
			return "";
		}

		String itemDetail = "";
		for (int i = 0; i < parsedAttrList.size(); i++) {
			ParsedAttr attr = parsedAttrList.get(i);
			ArrayList<AttrOption> attrOptionList = attr.attrOptionList;
			if (attrOptionList != null && attrOptionList.size() > 0) {
				String value = attrOptionList.get(attrOptionList.size() - 1).attrOptionName;
				//对于空属性不加入
				if(StringUtil.isEmpty(value)||"-".equals(value))
				{
					continue;
				}
				itemDetail += attr.attrName + ":";
				for (int j = 0; j < attrOptionList.size() - 1; j++) {
					AttrOption attropt = attrOptionList.get(j);
					itemDetail += attropt.attrOptionName + "/";
				}
				itemDetail += attrOptionList.get(attrOptionList.size() - 1).attrOptionName
						+ "<br/>";
			}
		}
		return itemDetail;
	}

	public static  String getItemDetail(String detailInfo){
		return processWapItemDetail(detailInfo);
	}
	public static String processWapItemDetail(String detailInfo){
    	String detail2Show = "";
		if (StringUtils.isNotEmpty(detailInfo) ) {
		detail2Show=  PageUtil.unEscapeHtml(detailInfo);
    	detail2Show = PageUtil.filterHtml(detail2Show);
		detail2Show= HTMLFilterService.handleHTMLContent(detail2Show);
	    detail2Show = PageUtil.trimDetail(detail2Show);
	    detail2Show = PageUtil.processChineseColon(detail2Show);
		}
	    return detail2Show;
    }
	/**
	 * 拼装商详首页的页面展示信息
	 * @param res
	 * @param mitemExtInfo4J
	 * @param itemInfoRes
	 * @param itemDetailVO
	 */
	public static void handleWap2ItemInfo(GetItemResponse res,
			MItemExtInfo4J mitemExtInfo4J,
			ItemInfoWithPrice itemInfoRes,
			ItemDetailVO itemDetailVO){
		//convertStock(res,itemInfoRes);
		List<Mstock4J> mstock4JList=convertStock(res,itemInfoRes);
	    ItemStock stock = new ItemStock(mstock4JList);	
	    itemDetailVO.setItemStock(stock);
	    if(validateShowQuickBuy(stock)){
	    	stock.parseItemStockForQuickBuy(mstock4JList);
	    	itemDetailVO.setViewSkuSaleAttrPoForQuickBuy(stock.getViewSkuSaleAttrPoForQuickBuy());
	    	itemDetailVO.setQuickBuy(true);
		}
	    itemDetailVO.setNormalPrice(res.getNormalPrice());//正常价
	    itemDetailVO.setSupportPromotion(res.isSupportPromotion());
	    itemDetailVO.setItemCode(res.itemCode);
		itemDetailVO.setItemName(res.itemName);
	    itemDetailVO.setStockCount(res.stockCount);
		itemDetailVO.setState("IS_FOR_SALE".equals(res.itemState)?1:-1);
		//itemDetailVO.setAttrInfo(getItemAttrStr(res.parsedAttrList));
		itemDetailVO.setParsedAttrList(res.getParsedAttrListForNew());
		itemDetailVO.setDetailInfo(res.detailInfo);
		itemDetailVO.setSoldCount(res.soldTotalCount);//总售出数量,已支付
		itemDetailVO.setMainItemPic(res.picLink);
		itemDetailVO.setSellerName(res.sellerName);
		itemDetailVO.setSellerUin(res.sellerUin);
		itemDetailVO.setFree4Freight(res.sellerPayFreight==ItemConstants.TRANSPORT_SELLER_PAY);
		itemDetailVO.setRedPacketAvariable(getRedPacketAvariable(res));
		itemDetailVO.setPicNum(res.picLinks!=null?res.picLinks.size()+1:1);
		itemDetailVO.setViewSkuSaleAttrPo(stock.getViewSkuSaleAttrPo());	
		itemDetailVO.setPrice(res.itemPrice);
		itemDetailVO.setMarketPrice(res.marketPrice);
		itemDetailVO.setMaxPrice(res.itemPrice);
		itemDetailVO.setMinPrice(res.itemPrice);	
		itemDetailVO.setNormalMaxPrice(res.getNormalPrice());
		itemDetailVO.setNormalMinPrice(res.getNormalPrice());
		itemDetailVO.setSupportPCCod(res.isSupportCodForPP());
		itemDetailVO.setClassId(StringUtil.toInt(res.classId,0));//类目ID
		if(res.stockList!=null){
			for(Stock stockItem:res.stockList){
				if (stockItem.stockPrice > itemDetailVO.getMaxPrice()) {
					itemDetailVO.setMaxPrice(stockItem.stockPrice);
				}
				if (stockItem.stockPrice < itemDetailVO.getMinPrice()) {
					itemDetailVO.setMinPrice(stockItem.stockPrice);
				}
				if(stockItem.getNormalPrice()>itemDetailVO.getNormalMaxPrice()){
					itemDetailVO.setNormalMaxPrice(stockItem.getNormalPrice());
				}
				if(stockItem.getNormalPrice()<itemDetailVO.getNormalMinPrice()){
					itemDetailVO.setNormalMinPrice(stockItem.getNormalPrice());
				}
			}
		}		
		if(ItemUtil.isMobileItem(res.properties)){
			itemDetailVO.setQgoItem(true);
			if(itemInfoRes!=null){								
				MItem4J mItem = itemInfoRes.getMitem();
				PPItem4J ppItem = itemInfoRes.getPpItem();
				itemDetailVO.setSavingFee(mItem.getSavingFee());
				// 设置商品价格		
				itemDetailVO.setSupportCod(mItem.isSupportCOD());
				itemDetailVO.setPrice(mItem.getItemPrice());
				//itemDetailVO.setMarketPrice(ppItem.getMarketPrice());
				itemDetailVO.setNormalPrice(mItem.getNormalPrice());//正常价
				itemDetailVO.setMaxPrice(mItem.getItemPrice());
				itemDetailVO.setMinPrice(mItem.getItemPrice());
				itemDetailVO.setNormalMaxPrice(mItem.getNormalPrice());
				itemDetailVO.setNormalMinPrice(mItem.getNormalPrice());
				Vector<Mstock4J> mStockList = mItem.getStockList();
				for (Mstock4J mStock : mStockList) {
					if (mStock.getStockPrice() > itemDetailVO.getMaxPrice()) {
						itemDetailVO.setMaxPrice(mStock.getStockPrice());
					}
					if (mStock.getStockPrice() < itemDetailVO.getMinPrice()) {
						itemDetailVO.setMinPrice(mStock.getStockPrice());
					}
					if(mStock.getNormalPrice()>itemDetailVO.getNormalMaxPrice()){
						itemDetailVO.setNormalMaxPrice(mStock.getNormalPrice());
					}
					if(mStock.getNormalPrice()<itemDetailVO.getNormalMinPrice()){
						itemDetailVO.setNormalMinPrice(mStock.getNormalPrice());
					}
				}				
				itemDetailVO.setOriginalPrice(ppItem.getItemPrice());
				itemDetailVO.setFree4Freight(mItem.isFree4Freight());
			}
			if(mitemExtInfo4J!=null ){
				itemDetailVO.setMitemExtInfo4J(mitemExtInfo4J);
				if(mitemExtInfo4J.getItemPics()!=null 
						&& mitemExtInfo4J.getItemPics().size()>0){
					itemDetailVO.setPicNum(mitemExtInfo4J.getItemPics().size());
				}
				if(StringUtils.isNotEmpty(mitemExtInfo4J.getItemName())){
					itemDetailVO.setItemName(mitemExtInfo4J.getItemName());
				}
			}					
		}
		Pager<ItemPic> picPage=processItemPic(res,mitemExtInfo4J, itemInfoRes);	
		itemDetailVO.setPicPage(picPage);
	}
	 /**
	   * 拼装商详描述页的折扣信息
	   * @param res
	   * @param mitemExtInfo4J
	   * @param itemInfoRes
	   * @param itemDetailVO
	   * @param pn
	   */
		public static void handleWap2ItemDiscountInfo(GetItemResponse res,
				MItemExtInfo4J mitemExtInfo4J,
				ItemInfoWithPrice itemInfoRes,
				int pn,
				ItemDetailInfoVO itemDetailVO){
			handleWap2ItemInfo(res,mitemExtInfo4J,itemInfoRes,itemDetailVO);
		    itemDetailVO.setPn(pn);	 
		}
  /**
   * 拼装商详描述页的展示信息
   * @param res
   * @param mitemExtInfo4J
   * @param itemInfoRes
   * @param itemDetailVO
   * @param pn
   */
	public static void handleWap2ItemDetailInfo(GetItemResponse res,
			MItemExtInfo4J mitemExtInfo4J,
			ItemInfoWithPrice itemInfoRes,
			int pn,
			ItemDetailInfoVO itemDetailVO){
		handleWap2ItemInfo(res,mitemExtInfo4J,itemInfoRes,itemDetailVO);
	    itemDetailVO.setPn(pn);	 
	}
	
	 /**
	   * 拼装商详属性页的展示信息
	   * @param res
	   * @param mitemExtInfo4J
	   * @param itemInfoRes
	   * @param itemDetailVO
	   * @param pn
	   */
		public static void handleWap2ItemPropInfo(GetItemResponse res,
				MItemExtInfo4J mitemExtInfo4J,
				ItemInfoWithPrice itemInfoRes,
				int pn,
				ItemPropVO itemPropVO){
			handleWap2ItemInfo(res,mitemExtInfo4J,itemInfoRes,itemPropVO);
			itemPropVO.setPn(pn);	 
		}
	public static void handleWap2ItemStock(GetItemResponse res,
			ItemInfoWithPrice itemInfoWithPrice,
			MItemExtInfo4J mitemExtInfo4J,
			ItemStockAttrVO itemStockAttrVO){
		//handleWap2ItemInfo(res,mitemExtInfo4J,itemInfoWithPrice,itemStockAttrVO);
		itemStockAttrVO.setPrice(res.itemPrice);
		if(ItemUtil.isMobileItem(res.properties)){
			itemStockAttrVO.setPrice(itemInfoWithPrice.getMitem().getItemPrice());
		}
		itemStockAttrVO.setItemName(res.itemName);
		itemStockAttrVO.setItemCode(res.itemCode);
		List<Mstock4J> mstock4JList=convertStock(res,itemInfoWithPrice);
		ItemStock stock = new ItemStock(mstock4JList);	
		itemStockAttrVO.setItemStock(stock);
		itemStockAttrVO.setMstockList(mstock4JList);		
	}
//	public static void handleOneDayItemStock(GetItemResponse res,
//			List<SnapupCmdyStock> stockList,
//			ItemStockAttrVO itemStockAttrVO){
//		itemStockAttrVO.setPrice(res.itemPrice);
//		itemStockAttrVO.setItemName(res.itemName);
//		itemStockAttrVO.setItemCode(res.itemCode);
//		List<Mstock4J> mstock4JList=convertStock(stockList);
//		ItemStock stock = new ItemStock(mstock4JList);	
//		itemStockAttrVO.setItemStock(stock);
//		itemStockAttrVO.setMstockList(mstock4JList);		
//	}
	
	
	private static  List<Mstock4J>  convertStock(GetItemResponse res,ItemInfoWithPrice itemInfoWithPrice){
		if(ItemUtil.isMobileItem(res.properties)&&itemInfoWithPrice!=null){
			return itemInfoWithPrice.getMitem().getStockList();							
		}else{
			if(res.stockList!=null){
				List<Mstock4J> mstock4JList=new ArrayList<Mstock4J>();
				for(Stock stock:res.stockList){
					Mstock4J mstock4J=new Mstock4J();
					mstock4J.setSoldCount((int)stock.soldCount);
					mstock4J.setStockCount((int)stock.stockCount);
					mstock4J.setStockId(stock.stockId);
					mstock4J.setStockAttr(stock.stockAttr);
					mstock4J.setStockPrice(stock.stockPrice);
					mstock4JList.add(mstock4J);
				}
			   return mstock4JList;	
			}	
			return null;
		}
	}
//	private static  List<Mstock4J>  convertStock(List<SnapupCmdyStock> stockList){
//			if(stockList!=null){
//				List<Mstock4J> mstock4JList=new ArrayList<Mstock4J>();
//				for(SnapupCmdyStock stock:stockList){
//					Mstock4J mstock4J=new Mstock4J();
//					mstock4J.setSoldCount((int)stock.getSoldNum());
//					mstock4J.setStockCount((int)stock.getRemainNum());//剩余库存
//					mstock4J.setStockId(stock.getStockId());
//					mstock4J.setStockAttr(stock.getAttr());
//					mstock4J.setStockPrice(stock.getPrice());
//					mstock4JList.add(mstock4J);
//				}
//			   return mstock4JList;	
//			}
//			return null;
//	}

	
	
	public static void handleSelectAttr(ItemStockAttrVO itemStockAttrVO,String sa){
		if(itemStockAttrVO.getMstockList()!=null && itemStockAttrVO.getMstockList().size()>0){
			ItemStock STOCK=itemStockAttrVO.getItemStock();
			itemStockAttrVO.getItemStock().parseSelectItemStock(itemStockAttrVO.getMstockList());
			if(StringUtils.isNotEmpty(sa)){
				 List<Mstock4J>  twoStepStockList =STOCK.getTwoStepAttrPo2().get(sa);
				 itemStockAttrVO.setTwoStepStockList(twoStepStockList);
				 itemStockAttrVO.setViewSkuSaleAttrPo(STOCK.getOneStepAttrPo());				
			}
			 itemStockAttrVO.setViewSaleAttr(STOCK.getViewSkuSaleAttrPo());
//			Map<String, List<Mstock4J>> viewSkuSaleAttrPo=new ItemStock().parseSelectItemStock(itemStockAttrVO.getMstockList());
//			itemStockAttrVO.setViewSkuSaleAttrPo(viewSkuSaleAttrPo);
		}
		
	}
//	public static void handleUserInfo(GetUserDetailInfoResponse res,ItemDetailVO itemDetailVO){
//		itemDetailVO.setTelephone(UserDetailUtil.getSellerTelephone(res));
//	}
	
	public static void handleUserInfo(User res,ItemDetailVO itemDetailVO){
		itemDetailVO.setTelephone(UserDetailUtil.getSellerTelephone(res));
	}
	private static int getRedPacketAvariable(GetItemResponse itemRes ){
		int type = -1;
		for(ExtendAttr ea : itemRes.extendList){
			if(ea.extendCode == 133L){
			    type = StringUtil.toInt(ea.extendValue,-1);
			    break;
			}
		}
		if (itemRes.properties.get(2) == null || itemRes.properties.get(2) != 1)
            return 0;
        if (type != 105 && type != 110 && type != 120)
            return 0;
        return type - 100;
	}
	public static void  handleWap2ItemPic(GetItemResponse res,
			MItemExtInfo4J mitemExtInfo4J,
			ItemInfoWithPrice itemInfoRes,
			int pn,
			ItemPicVO itemPicVO)
	{
		 handleWap2ItemInfo(res,mitemExtInfo4J,itemInfoRes,itemPicVO);
		 Pager<ItemPic> picPage=processItemPic(res,mitemExtInfo4J, itemInfoRes);		
		 //pn=pn>picPage.getElements().size()?1:pn;
		 picPage.setPageNo(pn>picPage.getElements().size()?1:pn);////到达最后一张图时跳到从第1开始
		 itemPicVO.setPicPage(picPage);
	   
	}
	private static Pager<ItemPic> processItemPic(
			GetItemResponse res,			
			MItemExtInfo4J mitemExtInfo4J,
			ItemInfoWithPrice itemInfoRes){
		 Pager<ItemPic> picPage=new Pager<ItemPic>();
		 List<String> picLinks=res.picLinks;
		 if(picLinks!=null){
			 ItemPic itemPic=new ItemPic();
			 itemPic.setPicUrl(res.picLink);
			 itemPic.setPicIndex(0);
			 picPage.getElements().add(itemPic);
			for(int i=1;i<=picLinks.size();i++){ 
				itemPic=new ItemPic();
				itemPic.setPicIndex(i);
				itemPic.setPicUrl(picLinks.get(i-1));
				picPage.getElements().add(itemPic);
			}
		 }
		 if(ItemUtil.isMobileItem(res.properties)){
			// 获取商品在卖家管理后台设置的信息			
		    if(mitemExtInfo4J!=null &&mitemExtInfo4J.getItemPics()!=null 
		    	&& mitemExtInfo4J.getItemPics().size()>0){
		    	picPage.getElements().clear();
				for(MItemPic4J pic:mitemExtInfo4J.getItemPics()){ 
					ItemPic itemPic=new ItemPic();
					itemPic.setPicUrl(pic.getPicUrl());
					itemPic.setPicDesc(pic.getPicDesc());
					itemPic.setPicIndex(pic.getPicIndex());
					picPage.getElements().add(itemPic);
				}
			 }
		 }
		 return picPage;
		
	}
	public static void  handleWap2ItemAssociateItem(
			MItemExtInfo4J mitemExtInfo4J,
			PPItemBiz ppitemBiz,
			ItemDetailVO itemDetailVO){
		Vector<String> associateItemList= parseAssociateItemList(mitemExtInfo4J);
		if(associateItemList!=null && associateItemList.size()>0){
			try {
				 List<ItemBO> associateItems= ppitemBiz.getItemList(associateItemList);
				 List<ItemBO>  associateItemLists=new ArrayList<ItemBO>();
				if(associateItems!=null){
					for(ItemBO itemBo:associateItems){
						if(ItemState.isForSale(itemBo.getItemState().getCode())){
							associateItemLists.add(itemBo);
						}
					}
				}
				itemDetailVO.setAssociateItemList(associateItemLists);
			} catch (BusinessException e) {
				Log.run.warn("ItemResultHandler#handleWap2ItemAssociateItem fail,mitemExtInfo4J["+mitemExtInfo4J+"]",e);
			}
		}
	}
	/*
	 * 解析商品的关联推荐商品
	 * @param mitemExtInfo4J
	 * @return
	 * @date:2013-2-25
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
	private static Vector<String> parseAssociateItemList(MItemExtInfo4J mitemExtInfo4J){
		if(mitemExtInfo4J!=null &&StringUtil.isNotEmpty(mitemExtInfo4J.getRaleItems())){
			Vector<String> associateItemList=new Vector<String>();
			String[] itemList=mitemExtInfo4J.getRaleItems().split("\\|");
			if(itemList!=null){
				for(String item:itemList){
					if(StringUtil.isNotEmpty(item)){
						associateItemList.add(item);					
					}
				}
			}
			return associateItemList;		
		}
		return null;		
	}
	
	/**
	 * 商品折扣查看
	 * @param res
	 * @param mitemExtInfo4J
	 * @param itemInfoRes
	 * @param uin
	 * @param itemDiscountVO
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 */
	public static void  handleWap2ItemDiscount(GetItemResponse res,
			MItemExtInfo4J mitemExtInfo4J,
			ItemInfoWithPrice itemInfoRes,
			long uin,
			int superQQLevel,
			int vipDiamondLevel,
			ItemDiscountVO itemDiscountVO)
	{
		 handleWap2ItemInfo(res,mitemExtInfo4J,itemInfoRes,itemDiscountVO);
		 itemDiscountVO.setHaveLogin(uin>0);
		 VipDiscount vipDiscount=new VipDiscount();
		 if(uin>0){
			 int level=VipDiscount.getLevel(superQQLevel,vipDiamondLevel);//获取折扣等级
			 itemDiscountVO.setSuperQQLevel(superQQLevel);			 
			 itemDiscountVO.setBuyerVipLevel(vipDiamondLevel);			 
			 itemDiscountVO.setLevelDiscount(vipDiscount.getDiscount(itemInfoRes,res, level));
	    	 itemDiscountVO.setLevelPrice(vipDiscount.getAfterDiscountPrice(itemInfoRes, res,level));
	    	 itemDiscountVO.setLevelTip(getLevelTip(superQQLevel,vipDiamondLevel,itemDiscountVO.getLevelDiscount()));
	    	 
		 }

        //折扣价
		 itemDiscountVO.setLevel1Price(vipDiscount.getAfterDiscountPrice(itemInfoRes,res, 1));
		 itemDiscountVO.setLevel3Price(vipDiscount.getAfterDiscountPrice(itemInfoRes,res, 3));
		 itemDiscountVO.setLevel5Price(vipDiscount.getAfterDiscountPrice(itemInfoRes,res, 5));
		 //折扣
		 itemDiscountVO.setLevel1Discount(vipDiscount.getDiscount(itemInfoRes, res,1));		 
		 itemDiscountVO.setLevel3Discount(vipDiscount.getDiscount(itemInfoRes, res,3));
		 itemDiscountVO.setLevel5Discount(vipDiscount.getDiscount(itemInfoRes,res, 5));

	}
	private static String getLevelTip(int superQQLevel,
			int vipDiamondLevel,String price){
		StringBuilder tip=new StringBuilder(30);
//		if(superQQLevel>0 && !grayUtil.isAllowGray()){
//			tip.append("您是超Q").append(superQQLevel).append("级");
//			if(vipDiamondLevel>0){
//				 tip.append("/").append("彩钻").append(vipDiamondLevel).append("级");
//			}
//			tip.append(",可享受￥").append(price).append("的折扣");
//		}else {
			if(vipDiamondLevel>0){
				tip.append("您是彩钻").append(vipDiamondLevel).append("级");
				tip.append(",可享受立减￥").append(price).append("的折扣");
			}else{
				tip.append("您还不是彩钻用户,无法享受折扣价。<br/>在拍拍全场购物单笔金额超过10元(不含Q币),即可成为彩钻用户,享受购物特权。");
			}			
		//}
		
		return tip.toString();		
	}
    /**
     * 有1个属性，或者两个属性（第一个属性的值少于3个）,且无区间价时
     * 展示快捷购买
     * @param stock
     * @return 是否展示快捷购买
     * @date:2013-2-27
     * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
     * @version 1.0
     */
	private static boolean validateShowQuickBuy(ItemStock stock ){
		if(stock.getViewSkuSaleAttrPo()!=null
				&& stock.getViewSkuSaleAttrPo().size()>0){
			if(stock.maxPrice!=stock.minPrice){//有区间价时不展示快捷购买
				return false;
			}
			if(stock.getViewSkuSaleAttrPo().size()==2){//有两个属性
				Set<String>  entry=stock.getViewSkuSaleAttrPo().keySet();
				Object[] enrtys=entry.toArray();
				if(enrtys!=null && enrtys.length>0 ){
					 String key=(String)enrtys[0];//取第一个属性
					 List<SaleAttrPair> pairs=stock.getViewSkuSaleAttrPo().get(key);//取第一个属性的值列表
					 return pairs!=null && pairs.size()<=ItemConstantForWap.MAX_ATTR_COUNT_FOR_QUICK_BUY;//第一个属性的值少于3个
				}
			}else if(stock.getViewSkuSaleAttrPo().size()==1){//有1个属性
				return true;
			}
		}
		 return false; 
	}
    
		
}
