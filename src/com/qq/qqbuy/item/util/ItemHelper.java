package com.qq.qqbuy.item.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;

import com.paipai.lang.uint16_t;
import com.paipai.lang.uint8_t;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.TssHelper;
import com.qq.qqbuy.item.constant.ItemState;
import com.qq.qqbuy.item.po.ItemAttrBO;
import com.qq.qqbuy.item.po.ItemAttrOptionBO;
import com.qq.qqbuy.item.po.ItemBO;
import com.qq.qqbuy.item.po.ItemStockBo;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ApiItem;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ApiStock;
import com.qq.qqbuy.thirdparty.idl.parseAttrText.ParseAttrTextClient;
import com.qq.qqbuy.thirdparty.idl.parseAttrText.v3.AttrBo_v3;
import com.qq.qqbuy.thirdparty.idl.parseAttrText.v3.OptionBo_v3;
import com.qq.qqbuy.thirdparty.idl.parseAttrText.v3.ParseAttrTextReq;
import com.qq.qqbuy.thirdparty.idl.parseAttrText.v3.ParseAttrTextResp;
import com.qq.qqbuy.thirdparty.openapi.po.GetItemResponse;
import com.qq.qqbuy.thirdparty.openapi.po.GetItemResponse.Stock;

public class ItemHelper
{

    /**
     * 图片格式
     */
    private static String imageFormat = "300x300";
    private static String imageFormat80 = "80x80";

    /**
     * 虚拟商品的前缀
     */
    private static final String VIRTUAL_PRE = "[暂停售卖] ";
    public static void copyItem2ItemBO(ApiItem item, ItemBO itembo)
    {
        //过虑虚拟商品
        //目前就是判断属性位为7的
        boolean isNotSoldComdy = false,isVirtualCmdy = false;
        //1、获取属性信息
        List<Integer> properties = new ArrayList<Integer>();
        if ((item.getProperty() != null) && (item.getProperty().size() > 0))
        {
            Set<uint16_t> set = item.getProperty().keySet();
            for (uint16_t key : set)
            {
                if(key.getValue() == 7)
                {
                    isNotSoldComdy = true;
                    isVirtualCmdy = true;
                }
                properties.add(key.getValue());
            }
        }
        //如果不是一口价的，就强制下架
        if(!(item.getState() == 2 || item.getState() == 7 || item.getState() == 64) )
            isNotSoldComdy = true;
        itembo.setProperties(properties);
        Map<String, String> itemProperty = transferItemProperty(item
                .getProperty());
        // String[] split = item.getAttrText().split("\\^");
        // if (split.length >= 1)
        // itembo.setAttrInfo(split[0]);
        // if (split.length >= 2) {
        // itembo.setCustomAttr(split[1]);
        // }

        itembo.setBuyLimit(item.getBuyLimit());

        /*if (("29".equals(item.getShopCategory()))
                || (item.getShopCategory() == null)
                || (item.getShopCategory().trim().length() == 0))
        {
            itembo.setCategoryId("0");
        } else
            itembo.setCategoryId(item.getShopCategory());
*/
        //itembo.setCityId(item.getCity());
        //itembo.setClassId(item.getLeafClassId());
        //itembo.setCountryId(item.getCountry());
        // itembo.setCreateTime(item.getStat().getAddTime() * 1000L);
        // itembo.setDepositPrice(item.getAuction().getPlegeMoney());
        itembo.setEmsPrice(item.getEmsMailPrice());
        itembo.setExpressPrice(item.getExpressMailPrice());
        // itembo.setGuarantee14Days(getItemPropertyFlag(itemProperty,
        // ItemConstants.ItemProperty.SIMP_PROP_14_DAYS_SECURITY.getIndex()));
        // itembo.setGuarantee7Days(getItemPropertyFlag(itemProperty,
        // ItemConstants.ItemProperty.SIMP_PROP_7_DAYS_SECURITY.getIndex()));
        // itembo.setGuaranteeCompensation(getItemPropertyFlag(itemProperty,
        // ItemConstants.ItemProperty.SIMP_PROP_GENUINE.getIndex()));
        // itembo.setGuaranteeRepair(getItemPropertyFlag(itemProperty,
        // ItemConstants.ItemProperty.SIMP_PROP_REPAIR.getIndex()));
        // itembo.setIncrementPrice(item.getAuction().getPriceAddStep());
        // itembo.setInvoiceItem(getItemPropertyFlag(itemProperty,
        // ItemConstants.ItemProperty.SIMP_PROP_INVOICE.getIndex()));
        itembo.setItemCode(item.getItemId());
        //itembo.setItemLocalCode(item.getStockId());

        //如果是虚拟商品，则在标题前加上前缀
        if(isVirtualCmdy)
            itembo.setItemName(VIRTUAL_PRE + item.getTitle());
        else
            itembo.setItemName(item.getTitle());
        
        itembo.setItemPrice(item.getPrice());
        itembo.setItemState(ItemState.getItemState(item.getState()));
        // itembo.setCodId(item.getCODId());

        // uint8_t freeReturn = (uint8_t)item.getProperty().get(new
        // uint16_t(67));
        // if (freeReturn != null) {
        // if (freeReturn.getValue() == 0)
        // itembo.setFreeReturn(0);
        // else
        // itembo.setFreeReturn(1);
        // }
        // else {
        // itembo.setFreeReturn(0);
        // }
        //
        // uint8_t quick = (uint8_t)item.getProperty().get(new uint16_t(68));
        // if (quick != null) {
        // if (quick.getValue() == 0)
        // itembo.setQuick(0);
        // else
        // itembo.setQuick(1);
        // }
        // else {
        // itembo.setQuick(0);
        // }

        if ((item.getStock() != null) && (item.getStock().size() > 0))
        {
            for (ApiStock bo : item.getStock())
            {
                ItemStockBo stockBo = new ItemStockBo();
                itembo.getItemStockBo().add(stockBo);
                stockBo.setStockAttr(bo.getAttr());
                //stockBo.setCreateTime(bo.getAddTime() * 1000);
                stockBo.setStockDesc(bo.getDesc());
                stockBo.setIndex(bo.getIndex());
                stockBo.setStockId(String.valueOf(bo.getAttrHash()));
               // stockBo.setStockLocalCode(bo.getStockId());
                //stockBo.setLastModifyTime(bo.getLastModifyTime() * 1000L);
                stockBo.setStockPrice(bo.getPrice());
               // stockBo.setSellerUin(item.getQQUin());
                stockBo.setSoldCount(bo.getSoldNum());
                if(isNotSoldComdy)
                    stockBo.setStockCount(0);
                else
                    stockBo.setStockCount(bo.getNum());
            }
        }

        // itembo.setLastModifyTime(item.getStat().getLastModified() * 1000L);
        // itembo.setLastToSaleTime(item.getStat().getUploadTime() * 1000L);
        // itembo.setLastToStoreTime(item.getStat().getDownloadTime() * 1000L);
        itembo.setMailPrice(item.getNormalMailPrice());
        itembo.setMarketPrice(item.getStat().getMarketPrice());
        // itembo.setPayTypes(PayType.valuesOf(Long.valueOf(item.getSendType())));
        Vector<String> logos = item.getLogo();
        if ((logos != null) && (!logos.isEmpty()))
        {
            itembo.setPicLink(ItemUtil.genImageUrl(
                    TssHelper.getItemSrcImageUrl(item.getQQUin(),
                            (String) logos.get(0)), imageFormat));            
            
        }

        if (item.getExtMap().containsKey("3001"))
        {
            itembo.getExtPicsLink().add(
                    ItemUtil.genImageUrl(TssHelper.getItemSrcImageUrl(item
                            .getQQUin(), (String) item.getExtMap().get("3001")
                            .get(0)), imageFormat));
            /*itembo.getOriginalExtPicsLink().add(TssHelper.getItemSrcImageUrl(item.getQQUin(),
            		(String) item.getExtMap().get("3001")
                    .get(0)));*/
        }
        if (item.getExtMap().containsKey("3002"))
        {
            itembo.getExtPicsLink().add(
                    ItemUtil.genImageUrl(TssHelper.getItemSrcImageUrl(item
                            .getQQUin(), (String) item.getExtMap().get("3002")
                            .get(0)), imageFormat));
           /* itembo.getOriginalExtPicsLink().add(TssHelper.getItemSrcImageUrl(item.getQQUin(),
            		(String) item.getExtMap().get("3002")
                    .get(0)));*/
        }
        if (item.getExtMap().containsKey("3003"))
        {
            itembo.getExtPicsLink().add(
                    ItemUtil.genImageUrl(TssHelper.getItemSrcImageUrl(item
                            .getQQUin(), (String) item.getExtMap().get("3003")
                            .get(0)), imageFormat));
           /* itembo.getOriginalExtPicsLink().add(TssHelper.getItemSrcImageUrl(item.getQQUin(),
            		(String) item.getExtMap().get("3003")
                    .get(0)));*/
        }
        if (item.getExtMap().containsKey("3004"))
        {
            itembo.getExtPicsLink().add(
                    ItemUtil.genImageUrl(TssHelper.getItemSrcImageUrl(item
                            .getQQUin(), (String) item.getExtMap().get("3004")
                            .get(0)), imageFormat));
           /* itembo.getOriginalExtPicsLink().add(TssHelper.getItemSrcImageUrl(item.getQQUin(),
            		(String) item.getExtMap().get("3004")
                    .get(0)));*/
        }
        // itembo.setProtectPrice(item.getAuction().getPlegeMoney());
        //itembo.setProvinceId(item.getProvince());

        // if ((item.getExtMap().get("34") != null) &&
        // (item.getExtMap().get("34").get(0) != null) &&
        // (!"".equals(((String)item.getExtMap().get("34").get(0)).trim())))
        // {
        // itembo.setQqvipDiscount(Short.parseShort((String)item.getExtMap().get("34").get(0)));
        // }
        // else itembo.setQqvipDiscount((short)10000);
        //
        // itembo.setQqvipItem((byte)((getItemPropertyFlag(itemProperty,
        // ItemConstants.ItemProperty.SIMP_PROP_SPECIAL_AUTH.getIndex()) == 1)
        // && (item.getExtMap().get("34") != null) ? 1 : 0));
        //
        // itembo.setRecommendItem((byte)(int)item.getIsRecommend());
        //
        // itembo.setReloadCount(item.getReloadCnt());
        // itembo.setSecondHandItem((byte)(item.getNewType() == 2L ? 1 : 0));
        //itembo.setSellerName(item.getQQNick());
        itembo.setSellerPayFreight((byte) (int) item.getTransportPriceType());
        if (item.getTransportPriceType() >= 10L)
        {
            itembo.setFreightId(item.getTransportPriceType());
        }
        //itembo.setSellerUin(item.getQQUin());
        // itembo.setSellType(SellType.getSellType(item.getDealType()));
        itembo.setStateDesc(ItemState.getShowMeg(item.getState()));
        if(isNotSoldComdy)
            itembo.setStockCount(0);
        else
            itembo.setStockCount(item.getNum());
        // itembo.setSupportTenpay((byte)(int)item.getSupportPayAgency());
        // itembo.setTheme(Theme.getTheme(item.getCustomStyleType()));
        // itembo.setValidDuration(item.getValidTime());
        //itembo.setVisitCount(item.getStat().getVisitCount());
        // itembo.setWeight(item.getWeight());
        // itembo.setWindowItem(getItemPropertyFlag(itemProperty,
        // ItemConstants.ItemProperty.SIMP_PROP_SHOP_WINDOW.getIndex()));
        // itembo.setSoldCount(item.getStat().getPayNum());
        // itembo.setSoldTimes(item.getStat().getPayCount());
        // itembo.setSoldTotalCount(item.getStat().getTotalPayNum());
        // itembo.setSoldTotalTimes(item.getStat().getTotalPayCount());
        itembo.setBuyNum(item.getStat().getBuyNum());
        //itembo.setBuyCount(item.getStat().getBuyCount());
        //itembo.setTotalBuyNum(item.getStat().getToalBuyNum());
        //itembo.setSoldTotalCount(item.getStat().getTotalPayNum());
       // itembo.setTotalBuyCount(item.getStat().getTotalBuyCount());
        // if ((item.getExtMap().get("39") != null) &&
        // (item.getExtMap().get("39").get(0) != null) &&
        // (!"".equals(((String)item.getExtMap().get("39").get(0)).trim())))
        // {
        // itembo.setSizeTableId(Long.parseLong((String)item.getExtMap().get("39").get(0)));
        // }
        /*if ((itemProperty != null) && (!itemProperty.isEmpty()))
        {
            for (String key : itemProperty.keySet())
            {
                if ((Integer.parseInt(key) < 32)
                        && (Integer.parseInt((String) itemProperty.get(key)) == 1))
                {
                    itembo.setItemProperty(itembo.getItemProperty()
                            | 1 << Integer.parseInt(key));
                }
            }
        }*/
        //itembo.setRelatedItems(item.getRelatedItems());
        // itembo.setProductId(item.getProductId());
    }

    public static List<ItemAttrBO> parseAttr(String attr, long classId)
            throws Exception
    {
        List<ItemAttrBO> result = new ArrayList<ItemAttrBO>();
        String attrText = "";
        //
        // ParseAttrTextReq parseAttrTextReq = new ParseAttrTextReq();
        // ParseAttrTextResp parseAttrTextResp = new ParseAttrTextResp();
        // parseAttrTextReq.setAttrString(attr);
        // parseAttrTextReq.setNavId(classId);
        // String errMeg = invokeProtocol(parseAttrTextReq, parseAttrTextResp);
        // if (errMeg != null) {
        // this.errorCode = 0L;
        // this.userErrorMessage = new
        // StringBuilder().append("翻译属性串失败：").append(this.userErrorMessage).toString();
        // logger.error(new
        // StringBuilder().append(this.uin).append("翻译属性串[").append(attr).append("]失败：").append(errMeg).toString());
        // }

        ParseAttrTextReq parseAttrTextReq = new ParseAttrTextReq();
        parseAttrTextReq.setAttrString(attr);
        parseAttrTextReq.setNavId(classId);
        ParseAttrTextResp parseAttrTextResp = ParseAttrTextClient
                .parseAttrTextV3(parseAttrTextReq);

        if (parseAttrTextResp.getResult() != 0)
        {
            String errMsg = new StringBuilder().append("翻译属性串[").append(attr)
                    .append("]失败：").append(parseAttrTextResp.getResult())
                    .toString();
            Log.run.error(errMsg);
            throw new Exception(errMsg);
        }
        if (parseAttrTextResp.getAttrBoList() != null)
        {
            for (AttrBo_v3 attrBo : parseAttrTextResp.getAttrBoList())
            {
                ItemAttrBO tmp = new ItemAttrBO();
                tmp.setAttrId(attrBo.getAttrId());
                tmp.setAttrName(attrBo.getName());
                List<ItemAttrOptionBO> attrOptionList = tmp.getAttrOptionList();
                String option = "";
                if (attrBo.getOptions() != null)
                {
                    for (OptionBo_v3 optionBo : attrBo.getOptions())
                    {
                        ItemAttrOptionBO tmp2 = new ItemAttrOptionBO();
                        tmp2.setAttrId(attrBo.getAttrId());
                        tmp2.setAttrOptionId(optionBo.getOptionId());
                        tmp2.setAttrOptionName(optionBo.getName());
                        attrOptionList.add(tmp2);
                        if (option.length() != 0)
                        {
                            option = new StringBuilder().append(option).append(
                                    "&").toString();
                        }
                        option = new StringBuilder().append(option).append(
                                optionBo.getName()).toString();
                    }
                }
                result.add(tmp);
                attrText = new StringBuilder().append(attrText).append("#")
                        .append(attrBo.getName()).append(":").append(option)
                        .toString();
            }
        }

        Log.run.debug(new StringBuilder().append(" ---------------- ").append(
                attrText).toString());

        String[] attrKey = attr.split("\\^");
        if (attrKey.length == 2)
        {
            String customAttr = attrKey[1];
            if (customAttr != null)
            {
                String[] kvs = customAttr.split("\\|");
                for (String each : kvs)
                {
                    String[] kv = each.split(":");
                    String k = kv[0];
                    String v = kv[1];
                    boolean existAttr = false;
                    for (ItemAttrBO eachAttr : result)
                    {
                        if (!k.equals(eachAttr.getAttrIdHexStr()))
                        {
                            if (k.equals(eachAttr.getAttrName()))
                            {
                                String[] vs = v.split("\\&");
                                for (String eachOptionValue : vs)
                                {
                                    if ((eachOptionValue != null)
                                            && (!"".equals(eachOptionValue
                                                    .trim())))
                                    {
                                        List<ItemAttrOptionBO> atrrOptions = eachAttr
                                                .getAttrOptionList();
                                        boolean existAttrOption = false;
                                        for (ItemAttrOptionBO optionbo : atrrOptions)
                                        {
                                            if (eachOptionValue.equals(optionbo
                                                    .getAttrOptionName()))
                                            {
                                                existAttrOption = true;
                                                break;
                                            }
                                        }
                                        if (!existAttrOption)
                                        {
                                            ItemAttrOptionBO tmp = new ItemAttrOptionBO();
                                            tmp
                                                    .setAttrOptionName(eachOptionValue);
                                            atrrOptions.add(tmp);
                                        }
                                    }
                                }
                                existAttr = true;
                                break;
                            }
                        } else
                        {
                            existAttr = true;
                            break;
                        }
                    }
                    if (!existAttr)
                    {
                        ItemAttrBO attrbo = new ItemAttrBO();
                        attrbo.setAttrName(k);
                        String[] vs = v.split("\\&");
                        for (String eachOptionValue : vs)
                        {
                            if ((eachOptionValue != null)
                                    && (!"".equals(eachOptionValue.trim())))
                            {
                                ItemAttrOptionBO tmp = new ItemAttrOptionBO();
                                tmp.setAttrOptionName(eachOptionValue);
                                attrbo.getAttrOptionList().add(tmp);
                            }
                        }
                        result.add(attrbo);
                    }
                }
            }
        }

        return result;
    }

    // public static void copyItemBO2GetItemResponse(ItemBO itembo,
    // GetItemResponse resp){
    // resp.itemCode=itembo.getItemCode();
    // resp.itemLocalCode=itembo.getItemLocalCode();
    // resp.itemName=itembo.getItemName();
    // resp.itemState=itembo.getItemState().getInfo();
    // resp.itemCode=itembo.getItemState().getCode()+"";
    // resp.stateDesc=itembo.getStateDesc();
    // // resp.relatedItems=itembo.getRelatedItems();
    // resp.itemProperty=itembo.getItemProperty()+"";
    // // resp.properties=itembo.getProperties();
    // resp.itemPrice=itembo.getItemPrice();
    // resp.marketPrice=itembo.getMarketPrice();
    // resp.expressPrice=itembo.getExpressPrice();
    // resp.emsPrice=itembo.getEmsPrice();
    // resp.mailPrice=itembo.getMailPrice();
    // resp.stockCount=itembo.getStockCount();
    // resp.categoryId=itembo.getCategoryId();
    // resp.classId=itembo.getClassId()+"";
    // // resp.productId=itembo.getProductId();
    // resp.freightId=itembo.getFreightId()+"";
    // // resp.codId=itembo.getCodId();
    // resp.cityId=itembo.getCityId();
    // resp.provinceId=itembo.getProvinceId();
    // resp.countryId=itembo.getCountryId();
    // // resp.quick=itembo.getQuick();
    // // resp.freeReturn=itembo.getFreeReturn();
    // resp.buyLimit=itembo.getBuyLimit();
    // resp.attr = itembo.getAttrInfo();
    // resp.customAttr = itembo.getCustomAttr();
    // resp.guarantee14Days=itembo.getGuarantee14Days();
    // resp.guarantee7Days=itembo.getGuarantee7Days();
    // resp.guaranteeCompensation=itembo.getGuaranteeCompensation();
    // resp.guaranteeRepair=itembo.getGuaranteeRepair();
    // resp.invoiceItem=itembo.getInvoiceItem();
    // // resp.sellType=itembo.getSellType().getCode();
    // resp.picLink=itembo.getPicLink();
    // resp.qqvipDiscount=itembo.getQqvipDiscount();
    // resp.qqvipItem=itembo.getQqvipItem();
    // resp.recommendItem=itembo.getRecommendItem();
    // resp.regionInfo=itembo.getRegionInfo();
    // resp.reloadCount=itembo.getReloadCount();
    // resp.secondHandItem=itembo.getSecondHandItem();
    // resp.sellerPayFreight=itembo.getSellerPayFreight();
    // resp.sellerUin=itembo.getSellerUin();
    // resp.sellerName=itembo.getSellerName();
    // resp.theme=itembo.getTheme().name();
    // resp.validDuration=itembo.getValidDuration();
    // resp.visitCount=itembo.getVisitCount();
    // resp.soldCount=itembo.getSoldCount();
    // resp.soldTotalCount=itembo.getSoldTotalCount();
    // resp.soldTimes=itembo.getSoldTimes();
    // resp.soldTotalTimes=itembo.getSoldTotalTimes();
    // resp.buyNum=itembo.getBuyNum();
    // resp.totalBuyNum=itembo.getTotalBuyNum();
    // resp.buyCount=itembo.getBuyCount();
    // resp.totalBuyCount=itembo.getTotalBuyCount();
    // resp.weight=itembo.getWeight();
    // resp.windowItem=itembo.getWindowItem();
    // resp.sizeTableId=itembo.getSizeTableId();
    // resp.createTime=getStringFromDate(itembo.getCreateTime());
    // resp.lastModifyTime=getStringFromDate(itembo.getLastModifyTime());
    // resp.lastToSaleTime=getStringFromDate(itembo.getLastToSaleTime());
    // resp.lastToStoreTime=getStringFromDate(itembo.getLastToStoreTime());
    //        
    //        
    //        
    // // 转换商品属性
    // String propertiesStr = itembo.getProperties();
    // if (propertiesStr != null && !propertiesStr.trim().isEmpty()) {
    // String[] properties = propertiesStr.split("\\|");
    // if (properties != null && properties.length > 0) {
    // for (String propertyStr : properties) {
    // if (propertyStr != null) {
    // int index = propertyStr.indexOf('_');
    // if (index != -1) {
    // String keyStr = propertyStr.substring(0, index);
    // String valueStr = propertyStr.substring(index + 1);
    // try {
    // int key = Integer.parseInt(keyStr);
    // int value = Integer.parseInt(valueStr);
    // resp.properties.put(key, value);
    // } catch (NumberFormatException e) {
    // continue;
    // }
    // }
    // }
    // }
    // }
    // }
    //        
    //        
    // // copy parsedAttr
    // if (itembo.getParsedAttrList() != null){
    // List<ItemAttrBO> list = new LinkedList<ItemAttrBO>();
    // list = itembo.getParsedAttrList();
    // ItemAttrBO attrBo = null;
    // Iterator<ItemAttrBO> itr = list.iterator();
    // while (itr.hasNext()) {
    // ParsedAttr attr = resp.new ParsedAttr();
    // attrBo = (ItemAttrBO) itr.next();
    // attr.attrId = attrBo.getAttrId()+"";
    // attr.attrName = attrBo.getAttrName();
    //
    // List<ItemAttrOptionBO> optNodes = attrBo.getAttrOptionList();
    // if (null != optNodes) {
    // // List optlist = optNodes.getChildren("attrOption");
    // ItemAttrOptionBO optNode = null;
    // Iterator<ItemAttrOptionBO> oitr = optNodes.iterator();
    // while (oitr.hasNext()) {
    // AttrOption opt = attr.new AttrOption();
    // optNode = oitr.next();
    // opt.attrOptionId = optNode.getAttrOptionId() + "";
    // opt.attrOptionName = optNode.getAttrOptionName();
    // attr.attrOptionList.add(opt);
    // }
    // }
    // resp.parsedAttrList.add(attr);
    // }
    // }
    //        
    //        
    // // extend info
    //        
    // Map<ItemExtend, String> extendList = itembo.getExtendInfo();
    // if (extendList != null) {
    // Set<Entry<ItemExtend, String>> extendNodes = extendList.entrySet();
    // if (extendNodes != null && extendNodes.size() > 0) {
    // Iterator<Entry<ItemExtend, String>> exItr = extendNodes.iterator();
    // while (exItr.hasNext()) {
    // Entry<ItemExtend, String> extendNode = exItr.next();
    // if (extendNode != null) {
    // ExtendAttr attr = resp.new ExtendAttr();
    // attr.extendCode = extendNode.getKey().getCode();
    // attr.extendName = extendNode.getKey().name();
    // attr.showMeg = extendNode.getKey().getShowMeg();
    // attr.extendValue = extendNode.getValue();
    // resp.extendList.add(attr);
    //                        
    // //增加彩钻折扣解析，第40个扩展字段
    // if(attr.extendCode == 40) {
    // resp.colorDiamond = attr.extendValue;
    // }
    // }
    // }
    // }
    // }
    //        
    // // 转化库存
    // List<ItemStockBo> stockListNode = itembo.getItemStockBo();
    // if (stockListNode != null) {
    // Iterator<ItemStockBo> stockItr = stockListNode.iterator();
    // while (stockItr.hasNext()) {
    // Stock stock = resp.new Stock();
    // ItemStockBo elmNode = (ItemStockBo) stockItr.next();
    // stock.stockId = elmNode.getStockId();
    // stock.stockLocalCode = elmNode.getStockLocalCode();
    // stock.stockAttr = elmNode.getStockAttr();
    // stock.stockDesc = elmNode.getStockDesc();
    // stock.soldCount = elmNode.getSoldCount();
    // stock.stockPrice = elmNode.getStockPrice();
    // stock.stockCount = elmNode.getStockCount();
    // stock.createTime = getStringFromDate(elmNode.getCreateTime());
    // stock.lastModifyTime = getStringFromDate(elmNode.getLastModifyTime());
    // resp.stockList.add(stock);
    // }
    // }
    //        
    //        
    // // 转化图片
    // List<String> list = new ArrayList<String>(10);
    // if (itembo.getExtPicsLink() != null){
    // for (String link : itembo.getExtPicsLink()) {
    // list.add(link);
    // }
    // }
    // resp.picLinks = list;
    // }

    private static Map<String, String> transferItemProperty(
            Map<uint16_t, uint8_t> pro)
    {
        Map<String, String> map = new HashMap<String, String>();
        if (pro != null)
        {
            for (uint16_t k : pro.keySet())
            {
                map.put(k.toString(), ((uint8_t) pro.get(k)).toString());
            }
        }
        return map;
    }

    private static byte getItemPropertyFlag(Map<String, String> pro, int index)
    {
        byte re = 0;
        String value = (String) pro.get(Integer.toString(index));
        if (value != null)
        {
            re = Byte.parseByte(value);
        }
        return re;
    }

    public static String getStringFromDate(Date date)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(date);
    }
    /**
     * 计算彩钻折扣价格
     * 
     * @param colorDiamond 彩钻折扣列表
     * @param itemPrice 商品净价格
     * @param freightPrice 首件运费
     * @param additionalFee 货到付款服务费
     * @return
     */
    private static Vector<Integer> getColorDiamondPrice(Vector<Integer> colorDiamond, long itemPrice) {
        if (colorDiamond != null) {
            Vector<Integer> discountPriceList = new Vector<Integer>();
            for (Integer discount : colorDiamond) {
                // 彩钻折扣是用四位数表示的 9折表示为9000
                long discountPrice = ((discount.longValue() * itemPrice) / 10000);
                discountPriceList.add(new Integer((int) discountPrice));
            }
            return discountPriceList;
        } else {
            return null;
        }
    }
    
    public static  void parseColorDiamond(GetItemResponse res){
    	 if(res!=null && StringUtils.isNotEmpty(res.colorDiamond)){
	          Vector<Integer> discountList = new Vector<Integer>();
	    	  String discounts[] = StringUtil.split(res.colorDiamond, "|");		    	  
	 		    if(discounts!=null && discounts.length>=0){
	 		    	for(String discount:discounts){
	 		    		if(StringUtil.isNumeric(discount)){
	 		    			discountList.add(new Integer(discount));
	 		    		}else{
	 		    			Log.run.warn("parseColorDiamond error, item["+res.itemCode+"]");
	 		    			return;
	 		    		}
	 		    	}	 			
	 			}	
		    	res.setDiscountPrice(getColorDiamondPrice(discountList,res.itemPrice));
		    	if(res.stockList!=null){
		    		 for(Stock stock:res.stockList){
			 		    	stock.setDiscountPrice(getColorDiamondPrice(discountList,stock.stockPrice));
			 		    }
		    	}	 		   
	      }
    }
    public static void main(String args[])
    {
        String fsfas = TssHelper.getItemSrcImageUrl(9999,
                "item-000FA1BA-695BF6320000000000693B570807F04C.0.jpg");
        System.out.println(fsfas);
    }
    
}
