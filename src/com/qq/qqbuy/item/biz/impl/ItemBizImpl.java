package com.qq.qqbuy.item.biz.impl;

import java.util.List;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.item.biz.ItemBiz;
import com.qq.qqbuy.item.constant.ItemState;
import com.qq.qqbuy.item.po.ItemAttrBO;
import com.qq.qqbuy.item.po.ItemInfoWithPrice;
import com.qq.qqbuy.item.po.MItemExtInfo4J;
import com.qq.qqbuy.item.po.eval.EvalPo;
import com.qq.qqbuy.item.po.wap2.ItemResponse;
import com.qq.qqbuy.item.util.ItemDetailConverter;
import com.qq.qqbuy.item.util.ItemHelper;
import com.qq.qqbuy.item.util.ItemUtil;
import com.qq.qqbuy.thirdparty.idl.item.ItemClient;
import com.qq.qqbuy.thirdparty.idl.item.protocol.GetItemExtInfoResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.GetItemInfoResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.eval.GetComdyEvalListResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.qgo.GetItemResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.qgo.PPItemInfo;

public class ItemBizImpl implements ItemBiz {

	@Override
	public ItemInfoWithPrice getItemFromQgo(String itemCode)
			throws BusinessException {
		checkParameter(itemCode);
		GetItemInfoResp getItemInfoResp = ItemClient.getItemInfo(itemCode);
		if (getItemInfoResp == null || getItemInfoResp.getErrCode() != 0) {
			throw BusinessException
					.createInstance(BusinessErrorType.LOAD_ITEM_FAILED);
		}
		return ItemDetailConverter.convert(getItemInfoResp);
	}

	@Override
	public MItemExtInfo4J getItemExtInfo(String itemCode)
			throws BusinessException {
		checkParameter(itemCode);
		GetItemExtInfoResp getItemExtInfoResp = ItemClient
				.getItemExtInfo(itemCode);
		if (getItemExtInfoResp == null || getItemExtInfoResp.getErrCode() != 0) {
			throw BusinessException
					.createInstance(BusinessErrorType.LOAD_ITEM_EXT_FAILED);
		}
		return ItemDetailConverter.convert(getItemExtInfoResp);

	}

	@Override
	public EvalPo getItemEval(String itemCode, long sellerUin, int pn, int ps,
			int needReply, int needHistory, int evalLevel)
			throws BusinessException {
		checkParameter(itemCode);
		sellerUin = ItemUtil.getSellerUin(itemCode);
		GetComdyEvalListResp resp = ItemClient.getItemEval(itemCode, sellerUin,
				pn, 10, needReply, needHistory, evalLevel);
		/*if (resp == null || resp.getResult() != 0) {
			throw BusinessException
					.createInstance(BusinessErrorType.LOAD_ITEM_CMT_FAILED);
		}*/
		Log.run.info("获取商品评价失败===============");
		//该片注释的原因为：如果获取商品评价失败后直接整 个商品详情就会显示不出结果不合理，注释掉
		return ItemDetailConverter.convert(resp, ps);
	}

	@Override
	public EvalPo getItemEvalFroBest(String itemCode, long sellerUin, int pn, int ps,
							  int needReply, int needHistory, int evalLevel)
			throws BusinessException {
		checkParameter(itemCode);
		sellerUin = ItemUtil.getSellerUin(itemCode);
		GetComdyEvalListResp resp = ItemClient.getItemEval(itemCode, sellerUin,
				1, 1, needReply, needHistory, evalLevel);
		if (resp == null || resp.getResult() != 0) {
			Log.run.info("获取商品评价失败===============");
		}
		return ItemDetailConverter.convertForBest(resp, ps);
	}
	@Override
	public ItemResponse getItemFromPaipai2(String itemCode,
			boolean needParseAttr, boolean needDetailInfo,
			boolean needExtendInfo, boolean needQgoItemInfo)
			throws BusinessException {
		checkParameter(itemCode);
		ItemResponse itemRes = new ItemResponse();
		GetItemResp resp = ItemClient.getItem(itemCode, needQgoItemInfo, needDetailInfo,
				needParseAttr);
		if (resp != null && resp.getErrCode() ==0) {
			PPItemInfo ppItem = resp.getPpItemInfo();
			List<ItemAttrBO> itemProp =null;
			if (ppItem != null && StringUtil.isNotEmpty(ppItem.getItemId())) {
				if(ItemState.isForSale(ppItem.getState())){//在售商品才继续
					if (needParseAttr) {
						try {
							itemProp=ItemHelper.parseAttr(ppItem.getAttrText(), ppItem.getLeafClassId());
						} catch (Exception e) {
							Log.run.error(
									"getItemFromPaipai2#parse item attr error,itemCode["+ itemCode + "]", e);
						}
					}
				}
				
				//不拉商品详情
//				if (needDetailInfo && ItemUtil.isMobileItem(ppItem.getProperty())) {
//					try {
//						String detail=ppItem.getItemDetail();
//						boolean isMobile=ItemUtil.isMobileItem(ppItem.getProperty());
//						if(StringUtil.isEmpty(detail) && !isMobile){
//						   detail=getItemDetail(itemCode, isMobile);
//						}
//						ppItem.setItemDetail(detail);
//					} catch (Exception e) {
//						Log.run.error(
//								"getItemFromPaipai2#getItemDetail error,itemCode["+ itemCode + "]", e);
//					}
//				}
				itemRes.setPpItemInfo(ItemDetailConverter.convertPPItemInfo(ppItem));
				itemRes.getPpItemInfo().setParsedAttrListForNew(itemProp);
			}
			itemRes.setMmItemInfo(ItemDetailConverter.parseMMItem(resp.getMmItemInfo()));
		}else{
			throw BusinessException
			.createInstance(BusinessErrorType.LOAD_ITEM_FAILED);
		}
		return itemRes;
	}

//	private static String getItemDetail(String itemCode, boolean isMobile)
//			throws BusinessException {
//		String itemDetail = "";
//		//int mobileDetailDownloadFail = 0;
//		if (!isMobile && StringUtils.isEmpty(itemDetail)) {	
//			itemDetail = getItemDetail(itemCode);
//		}
//		return itemDetail == null ? "" : itemDetail;
//
//	}

//	private static String getTssFilename(String code, int fileIndex,
//			TssDataType dataType) {
//		if ((code == null) || (dataType == null))
//			return null;
//		long timeNow = (long) (System.currentTimeMillis() * 0.001D) % 268435455L;
//		int codeLen = code.length();
//		if (codeLen < 32)
//			code = String.format("%32s", new Object[] { code }).replace(' ',
//					'0');
//		else if (codeLen > 32) {
//			code = code.substring(0, 32);
//		}
//		return String.format("%s-%08X-%s.%X.%s", new Object[] { "mitm",
//				Long.valueOf(timeNow), code, Integer.valueOf(fileIndex % 16),
//				TssDataType.getTssDataTypeName(dataType) });
//	}

//	private static String getItemDetail(String itemCode)
//			throws BusinessException {
//
//		ApiDownLoadDescResp resp = ItemClient.getItemDetail(itemCode);
//		if (resp == null || resp.result != 0) {
//			throw BusinessException
//					.createInstance(BusinessErrorType.ERRCODE_ITEM_DETAIL_FAIL);
//		}
//
//		return resp.getDescFileContent();
//	}
	
	private void checkParameter(String itemCode ) throws BusinessException{
		if(!ItemUtil.isValidItemCode(itemCode)){
			throw BusinessException
			.createInstance(BusinessErrorType.ITEM_ID_ILLEGAL); 
		}
	}

}
