package com.qq.qqbuy.item.biz.impl;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.paipai.lang.uint16_t;
import com.paipai.lang.uint32_t;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.PaiPaiConfig;
import com.qq.qqbuy.common.SpringHelper;
import com.qq.qqbuy.common.client.cache.CacheCallback;
import com.qq.qqbuy.common.client.cache.CacheClient;
import com.qq.qqbuy.common.client.cache.CacheKey;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.exception.ExternalInterfaceException;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.TssFileType;
import com.qq.qqbuy.common.util.TssHelper;
import com.qq.qqbuy.item.biz.PPItemBiz;
import com.qq.qqbuy.item.constant.ItemConstants;
import com.qq.qqbuy.item.constant.ItemExtend;
import com.qq.qqbuy.item.constant.ItemState;
import com.qq.qqbuy.item.po.ItemAttrBO;
import com.qq.qqbuy.item.po.ItemBO;
import com.qq.qqbuy.item.po.ItemBOBasic;
import com.qq.qqbuy.item.po.ItemBOExtInfo;
import com.qq.qqbuy.item.po.ItemBOPrice;
import com.qq.qqbuy.item.po.ItemStockBo;
import com.qq.qqbuy.item.po.ShopItemInfo;
import com.qq.qqbuy.item.util.ItemHelper;
import com.qq.qqbuy.item.util.ItemUtil;
import com.qq.qqbuy.item.util.SellerCreditUtil;
import com.qq.qqbuy.shop.biz.ShopBiz;
import com.qq.qqbuy.shop.po.ShopInfo;
import com.qq.qqbuy.shop.po.ShopSimpleItem;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.IsFavoritedResp;
import com.qq.qqbuy.thirdparty.idl.item.ItemClient;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ApiDownLoadDescResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ApiGetItemListResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ApiGetItemResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ApiItem;
import com.qq.qqbuy.thirdparty.idl.item.protocol.FetchItemInfoResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ItemBase;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ItemExt;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ItemImg;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ItemNum;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ItemPo_v2;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ItemPriceExt;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ItemProp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ItemStock;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ItemStockList;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ItemTime;
import com.qq.qqbuy.thirdparty.idl.item.protocol.eval.GetComdyEvalListResp;
import com.qq.qqbuy.thirdparty.idl.selfmarketactive.SelfMarketActiveClient;
import com.qq.qqbuy.thirdparty.idl.selfmarketactive.protocol.SelfMarkContentOrderBo;
import com.qq.qqbuy.thirdparty.idl.selfmarketactive.protocol.SelfMarketActiveForOrderRespBo;
import com.qq.qqbuy.thirdparty.idl.selfmarketactive.protocol.SelfMarketActiveOrderBo;
import com.qq.qqbuy.thirdparty.idl.selfmarketactive.protocol.SelfMarketForQueryResp;
import com.qq.qqbuy.thirdparty.idl.stat.StatClient;
import com.qq.qqbuy.thirdparty.idl.stat.protocol.GetFavoritesByIdResp;
import com.qq.qqbuy.thirdparty.idl.userinfo.ApiUserClient;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.apiuser.APIUserProfile;

public class PPItemBizImpl implements PPItemBiz {

	private static final String imageFormat400 = "400x400";
	
	protected final String ACTIVE_BY_UIN_HOST = "my.paipai.com";
	/**
	 * 获得当前活动CGI接口
	 */
	protected final String ACTIVE_BY_UIN_URL = "http://my.paipai.com/selfmarket/show" ;
	
	protected final String USER_BASIC_HOST = "bases.paipai.com";
	/**
	 * 活动用户等级等基本信息接口
	 */
	protected final String USER_BASIC_URL = "http://bases.paipai.com/buyer/buyerdiscount" ;
	
	protected final String WEIDIAN_PRICE_HOST = "party.paipai.com" ;
	//微店商品价格接口
	protected final String WEIDIAN_PRICE_URL = "http://party.paipai.com/cgi-bin/v2/wxprice_favor_query";
	//PC侧商品地址前缀
	private static final String ITEM_URL_PREFIZ = "http://auction1.paipai.com/" ;
	//PC侧商品的微店商品地址前缀
	private static final String ITEM_URL_WEI_DIAN = "http://b.paipai.com/itemweb/item?scence=101&ic=" ;
	//PC侧微店地址前缀
	private static final String SHOP_URL_WEI_DIAN = "http://wd.paipai.com/mshop/" ;
	
	private static final String VIRTUAL_PRE = "[暂停售卖] ";
	
	private static final List<String> COLOR_LEVEL_1_LIST = new ArrayList<String> () ;
	
	private static final List<String> COLOR_LEVEL_2_LIST = new ArrayList<String> () ;
	
	private static final List<String> COLOR_LEVEL_3_LIST = new ArrayList<String> () ;
	
	private static final int RESULT_PRICE_NORMAL = 0 ;
	
	private static final int RESULT_PRICE_ACTIVE = 1 ;
	
	private static final int RESULT_PRICE_COLOR = 2 ;
	
	private static final int RESULT_PRICE_SHOPVIP = 3 ;
	
	private static final int RESULT_PRICE_WEIDIAN = 4;
	
	static {
		COLOR_LEVEL_1_LIST.add("1") ;
		COLOR_LEVEL_1_LIST.add("2") ;
		
		COLOR_LEVEL_2_LIST.add("3") ;
		COLOR_LEVEL_2_LIST.add("4") ;
		
		COLOR_LEVEL_3_LIST.add("5") ;
		COLOR_LEVEL_3_LIST.add("6") ;
		COLOR_LEVEL_3_LIST.add("7") ;
		COLOR_LEVEL_3_LIST.add("8") ;
		
	}
	
	private CacheClient cacheClient;

	// public String getImageFormat()
	// {
	// return imageFormat;
	// }
	//
	// public void setImageFormat(String imageFormat)
	// {
	// this.imageFormat = imageFormat;
	// }

	@Override
	public ItemBO getItem(String itemCode, boolean needParseAttr, boolean needExtendInfo) throws BusinessException {
		// TODO Auto-generated method stub

		ApiGetItemResp apiGetItemResp = ItemClient.getApiItem(itemCode);

		if (apiGetItemResp == null) {
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "IDL STUB调用失败");
		}
		if (apiGetItemResp.result != 0) {
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "IDL调用远端失败");
		}

		// 调用成功
		ApiItem idlItem = apiGetItemResp.getApiItem();
		ItemBO itembo = new ItemBO();
		ItemHelper.copyItem2ItemBO(idlItem, itembo);

		// 解析商品类目信息
		if (needParseAttr) {
			List<ItemAttrBO> itemAttrBO;
			try {
				itemAttrBO = ItemHelper.parseAttr(idlItem.getAttrText(), idlItem.getLeafClassId());
			} catch (Exception e) {
				throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "解析商品类目失败");
			}
			itembo.setParsedAttrList(itemAttrBO);
		}

		// 解析商品的扩展信息
		if (needExtendInfo) {
			// TLog.debug(new
			// StringBuilder().append("封装商品的扩展信息:").append(item.getExtMap().keySet()).toString());
			for (String key : idlItem.getExtMap().keySet()) {
				Vector<String> vec = idlItem.getExtMap().get(key);
				String strVal = "";
				for (String val : vec) {
					strVal = new StringBuilder().append(strVal).append(val).append(",").toString();
				}
				if (strVal.length() > 0) {
					strVal = strVal.substring(0, strVal.length() - 1);
				}
				itembo.getExtendInfo().put(ItemExtend.getItemExtend(Integer.parseInt(key)), strVal);
			}
		}

		return itembo;
	}

	public String getItemDetail(String itemCode) throws BusinessException {

	    String detailFile = TssHelper.getTssFilenameV2(TssFileType.item, itemCode, 1, TssHelper.TSS_EXTENSION_MOBILE_TXT);
		ApiDownLoadDescResp resp = ItemClient.getItemDetail(itemCode,detailFile);
		if (resp == null || resp.result != 0 || StringUtils.isEmpty(resp.getDescFileContent())) {
			detailFile = TssHelper.getTssFilenameV2(TssFileType.item, itemCode, 1, TssHelper.TSS_EXTENSION_TXT);
			resp = ItemClient.getItemDetail(itemCode,detailFile);
			if (resp == null) {
				Log.run.info("getItemDetail, fileName = " + detailFile + ",itemCode=" + itemCode + ",resp is null") ;
				return "{}" ;
			}
			if (resp.result != 0) {
				Log.run.info("getItemDetail, fileName = " + detailFile + ",itemCode=" + itemCode + ",resp =" + resp.result) ;
				return "{}" ;
			}

			return resp.getDescFileContent();
		} else {
			return resp.getDescFileContent();
		}
		
	}

	@Override
	public List<ItemBO> getItemList(Vector<String> itemCodeList) throws BusinessException {
		ApiGetItemListResp resp = ItemClient.getItemList(itemCodeList);
		if (resp == null) {
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "IDL STUB调用失败");
		}
		if (resp.result != 0) {
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL, "IDL调用远端失败");
		}
		List<ItemBO> itemList = new LinkedList<ItemBO>();
		if (resp.getApiItemList() != null) {
			for (Object api : resp.getApiItemList()) {
				ApiItem idlItem = (ApiItem) api;
				ItemBO itembo = new ItemBO();
				ItemHelper.copyItem2ItemBO(idlItem, itembo);
				itemList.add(itembo);
			}
		}
		return itemList;
	}

	/*public static void main(String[] args) throws BusinessException {
		PPItemBizImpl biz = new PPItemBizImpl();
		ItemBO item = biz.getItem("D664F63200000000040100001B1D7080", true, true);
		System.out.println(item);
		Vector<String> itemCodeList = new Vector<String>();
		itemCodeList.add("373CAF4600000000040100000DB4BC1F");
		itemCodeList.add("373CAF4600000000040100000DB4BC1F");

		List<ItemBO> pp = biz.getItemList(itemCodeList);
		System.out.println(pp);
		System.out.println(pp.size());

	}
*/
	/**
	 * 
	 * @Title: genImageUrl
	 * 
	 * @Description: 生成图片url
	 * @param url
	 *            原图url
	 * @param imageFormat
	 *            图片格式
	 * @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	private String genImageUrl(String url, String imageFormat) {
		if (url != null && url.length() > 10) {
			int pos = url.lastIndexOf("/");
			if (pos > 1)
				pos = url.indexOf(".", pos + 1);
			if (pos > 1)
				pos = url.indexOf(".", pos + 1);
			if (pos > 1)
				return url.substring(0, pos) + "." + imageFormat + url.substring(pos);
		}
		return url;
	}

	
	
	@Override
	public ItemBOBasic getItemBasic(String itemCode) {
		// 过虑虚拟商品
				// 目前就是判断属性位为7的
				/*boolean isNotSoldComdy = false;
				boolean isSupportCmdy = true;*/
				boolean isVirtualCmdy = false;
				boolean isVirtalForCard = false;
				//促销
				boolean promoteNo = false;
				boolean promoteStart = false;		
				// 是不是大促活动
				boolean isPromotion = false;
				
				ItemBOBasic ret = new ItemBOBasic();
				
				if (StringUtil.isNotBlank(itemCode)) {
					FetchItemInfoResp resp = ItemClient.fetchItemInfo(itemCode);
					ItemPo_v2 itemPo = null;					
					ItemBase oItemBase = null;// 商品基本信息，包含商品的基本信息(商品名称，所在地，最低价格，可使用的红包等)
					//ItemStockList oItemStockList = null;// 库存信息					
					ItemImg oItemImg = null;//图片信息

					if (resp != null && resp.getResult() == 0 && (itemPo = resp.getItemPo()) != null
							&& (oItemBase = itemPo.getOItemBase()) != null) {
						ItemNum oItemNum = itemPo.getOItemNum();//统计信息
						oItemImg = itemPo.getOItemImg();//图片信息
						//oItemStockList = itemPo.getOItemStockList();//库存信息
						ItemProp itemProp = itemPo.getOItemProp();//属性信息
						ret.setLeafClassId(oItemBase.getLeafClassId()) ;
						//虚拟卡
						if (oItemBase.getLeafClassId() == 200082) {
							isVirtalForCard = true;
						}				
						/*if (!(oItemBase.getState() == 2 || oItemBase.getState() == 7 || oItemBase.getState() == 64)) {
							isNotSoldComdy = true;
						}*/
						if (itemProp != null) {
							BitSet bitSet = itemProp.getBitProp();
							if (bitSet != null) {
								String tmpBitSet = bitSet.toString();
								tmpBitSet = tmpBitSet.replace("{", "");
								tmpBitSet = tmpBitSet.replace("}", "");
								String[] array = tmpBitSet.split(",");
								if (array != null) {
									for (String key : array) {
										if (key != null) {
											int keyProp = (int) StringUtil.toLong(key.trim(), 0);
											if (keyProp == 7) {
												/*if (!isVirtalForCard)
													isNotSoldComdy = true;
												isSupportCmdy = false;
												isNotSoldComdy = true;*/
												isVirtualCmdy = true;
											}
											/*if (keyProp == 26) {
												// 今日特价 拍下即扣商品  (抢购)
												isSupportCmdy = false;
												isNotSoldComdy = true;
											}
											if (keyProp == 30) {
												// 禁止购买商品
												isNotSoldComdy = true;
												isSupportCmdy = false;
											}
											if (keyProp == 58) {
												// 京东POP
												isNotSoldComdy = true;
												isSupportCmdy = false;
											}
											if (keyProp == 39) {
												// Auction3标识
												isSupportCmdy = false;
												isNotSoldComdy = true;
											}
											if (keyProp == 170) {
												//分期付款商品
												isSupportCmdy = false;
											}
											//拍卖商品
											if (keyProp == 60 || keyProp == 61 || keyProp == 62 || keyProp == 63) {
												isSupportCmdy = false;
												isNotSoldComdy = true;
											}
											//下单即扣，不支持购物车
											if (keyProp == 45) {
												isSupportCmdy = false;
											}*/
											if(keyProp == ItemConstants.INDEX_PROP_PROMOTION_EVENT_NO)
												promoteNo = true;
											if(keyProp == ItemConstants.INDEX_PROP_PROMOTIONAL_EVENT_START)
												promoteStart = true;									
											
											if (keyProp == 53) {
												isPromotion = true;
											}
											ret.getProperties().add(keyProp) ;
										}
									}
									
								}
							}
						}
						/**
						 * 支持立即购买，对于一口价类型支持立即购买
						 */
						if (itemPo.getOItemBase().getDealType() == 1) {
							ret.setFixOrderFlag(1);
						} /*else {
							isNotSoldComdy = true; //非一口价不能购买
							isSupportCmdy = false; //非一口价不能加入购物车
						}*/
						
						// 0、 促销价等
						ItemPriceExt oItemPriceExt = itemPo.getOItemPriceExt();
						if (oItemPriceExt != null) {
							ret.setActivityDes(oItemPriceExt.getActivityDesc()) ;
							//ret.setColorPrice(oItemPriceExt.getColorPrice());
							ret.setActivityPrice(oItemPriceExt.getActivityPrice());
							//ret.setShopVipPrice(oItemPriceExt.getShopVipPrice());
							// 大促活动
							if (oItemPriceExt.getActivityType() == (long) ItemConstants.ITEM_ACTIVETYPE_BIGSELL) {
								isPromotion = true;
							}
							/**
							 * 如果是NBA活动和快捷支付类型就不支持购物车
							 */
							/*if (oItemPriceExt.getActivityType() == 0x80000001L
									|| oItemPriceExt.getActivityType() == 0x800000002L) {
								isSupportCmdy = false;
							}*/

							/**
							 * 如果是特价类，并且正在抢购的，就把库存信息设置成0
							 */
							/*long currTime = System.currentTimeMillis() / 1000;
							if (oItemPriceExt.getActivityType() == 41 && currTime >= oItemPriceExt.getBeginTime()
									&& currTime <= oItemPriceExt.getEndTime()) {
								isNotSoldComdy = true;
							}*/

						}

						/*if (isSupportCmdy) {
							ret.setCartFlag(1); 
						}
						if (isNotSoldComdy) {
							ret.setIsNotSoldComdy(1) ;
						}*/
							

						// 1、商品统计信息
						if (oItemNum != null) {
							ret.setBuyCount(oItemNum.getBuyCount());
//							ret.setBuyNum(oItemNum.getBuyNum());
							//月销售量
							ret.setBuyNum(oItemNum.getCurrPayNum());
							//ret.setStockCount(oItemNum.getNum());
							/*if (isNotSoldComdy) {
								ret.setStockCount(0);
							} else {
								ret.setStockCount(oItemNum.getNum());
							}	*/					
							ret.setTotalBuyCount(oItemNum.getTotalBuyCount());
							ret.setTotalBuyNum(oItemNum.getToalBuyNum());
							ret.setVisitCount(oItemNum.getVisitCount());

						}

						// 2、商品基本信息
						ret.setBuyLimit(oItemBase.getBuyLimit());
						ret.setCityId(oItemBase.getCity());
						ret.setCountryId(oItemBase.getCountry());
						ret.setEmsPrice(oItemBase.getEmsMailPrice());
						ret.setExpressPrice(oItemBase.getExpressMailPrice());
						ret.setItemState(ItemState.getItemState(oItemBase.getState()));
						ret.setStateDesc(ItemState.getShowMeg(oItemBase.getState()));
						// 3、扩展属性，目前这里主要是用于红包的使用
						long redPrice = oItemBase.getRedPrice();
						if (redPrice > 0) {
							ret.setRedPrice(redPrice);
							Map<ItemExtend, String> redMap = new HashMap<ItemExtend, String>(1);
							redMap.put(ItemExtend.getItemExtend(133), "" + (redPrice + 100));
							ret.setExtendInfo(redMap);
						}

						// 4、处理图片信息
						Vector<String> vecImgTmp = null, vecImg = null;
						int size = 0;
						if (oItemImg != null && (vecImgTmp = oItemImg.getVecImg()) != null && !vecImgTmp.isEmpty()) {
							size = vecImgTmp.size();
							vecImg = new Vector<String>(size);
							for (String iamgeUrl : vecImgTmp) {
								// 跳转商详图片url至400X400
								vecImg.add(genImageUrl(iamgeUrl, imageFormat400));
							}
							if (size > 0)
								ret.setPicLink(vecImg.get(0));					
							for (int i=1;i<size;i++) {
								String imgUrl = vecImg.get(i) ;
								if (!imgUrl.contains("paipaiimg.com/showimg")) {
									ret.getExtPicsLink().add(imgUrl) ;
								}							
							}
								//ret.setExtPicsLink(vecImg.subList(1, vecImg.size()));
							ret.setVecImg(vecImg);
						}

						ret.setFreightId(oItemBase.getShippingfeeId());
						ret.setCodFreightId(oItemBase.getCodShipTempId());
						ret.setItemCode(oItemBase.getItemId());
						ret.setItemUrl(ITEM_URL_PREFIZ + oItemBase.getItemId()) ;//商品pc端链接地址
						ret.setItemWeiDian(ITEM_URL_WEI_DIAN + oItemBase.getItemId()) ;
					    ret.setItemLocalCode(oItemBase.getItemLocalCode());
						if (isVirtualCmdy && !isVirtalForCard)
							ret.setItemName(VIRTUAL_PRE + oItemBase.getTitle());
						else
							ret.setItemName(oItemBase.getTitle());
						ret.setItemPrice(oItemBase.getPrice());

						// 5、库存信息
						/*Vector<ItemStock> itemStockList = null;
						int index = 0;
						List<ItemStockBo> itemStockBo = new ArrayList<ItemStockBo>();
						if (oItemStockList != null && (itemStockList = oItemStockList.getOItemStock()) != null
								&& !itemStockList.isEmpty()) {
							for (ItemStock stockItem : itemStockList) {
								ItemStockBo oItemStockBo = new ItemStockBo();
								oItemStockBo.setCreateTime(stockItem.getAddTime() * 1000);
								ItemPriceExt oItemPriceExt2 = null;
								// 促销价等
								if ((oItemPriceExt2 = stockItem.getOPriceExt()) != null) {
									// 大促活动
									if (oItemPriceExt2.getActivityType() == (long) ItemConstants.ITEM_ACTIVETYPE_BIGSELL) {
										isPromotion = true;
									}
									oItemStockBo.setColorPrice(oItemPriceExt2.getColorPrice());
									oItemStockBo.setActivityPrice(oItemPriceExt2.getActivityPrice());
									oItemStockBo.setShopVipPrice(oItemPriceExt2.getShopVipPrice());
								}
								oItemStockBo.setIndex(index++);
								oItemStockBo.setLastModifyTime(stockItem.getLastModifyTime() * 1000);
								oItemStockBo.setSellerUin(oItemBase.getQqUin());
								oItemStockBo.setSoldCount(stockItem.getSoldNum());
								oItemStockBo.setStockAttr(stockItem.getAttr());
								if (isNotSoldComdy)
									oItemStockBo.setStockCount(0);//不让下单，过滤作用
								else
								oItemStockBo.setStockCount(stockItem.getNum());
								oItemStockBo.setStockDesc(stockItem.getDesc());
								oItemStockBo.setStockId("" + stockItem.getStockId());
								oItemStockBo.setStockLocalCode(stockItem.getStockCode());
								oItemStockBo.setStockPrice(stockItem.getPrice());
								itemStockBo.add(oItemStockBo);
							}
						}*/
						ret.setMailPrice(oItemBase.getNormalMailPrice());
						ret.setMarketPrice(oItemBase.getMarketPrice());

						/*// 属性值转换
						// 6、解析商品类目信息
						if (needParseAttr) {
							String attrText = oItemBase.getAttrText();
							Log.run.info("----------------------------attrText----------------------"+attrText);
							List<ItemAttrBO> itemAttrBO;
							try {
								itemAttrBO = ItemHelper.parseAttr(attrText, oItemBase.getLeafClassId());
								ret.setParsedAttrList(itemAttrBO);
							} catch (Exception e) {
							}
						}*/

						ret.setProvinceId(oItemBase.getProductId());

						// 7、关联商品======================================================?
						Vector<String> relatedItems = null;
						if ((relatedItems = oItemBase.getRelatedItems()) != null) {
							StringBuffer buf = new StringBuffer();
							for (String relatedItem : relatedItems) {
								buf.append(relatedItem + "|");
							}
							ret.setRelatedItems(buf.toString());
						}
						ret.setSellerName(oItemBase.getQqNick());
						/*卖家或者买家承担运费的情况 1 卖家承担运费 2 买家承担运费 3 同城交易，无需运费 大于或等于10
						 * 买家承担运费，表示支持运费模板，该值即为运费模板ID*/
						ret.setSellerPayFreight(oItemBase.getTransportPriceType());
						if (ret.getFreightId() >= 10 && ret.getSellerPayFreight() == 2) {
							ret.setSellerPayFreight(ret.getFreightId());
						}
						ret.setSellerUin(oItemBase.getQqUin());
						ret.setShopWeiDian(SHOP_URL_WEI_DIAN + oItemBase.getQqUin()) ;
						/**
						 * 如果没有库存属性信息，就生成一个以支持老版本
						 */
						ItemTime oItemTime = itemPo.getOItemTime();
						if (oItemTime != null) {
							//设置商品的开始时间与商品的结束时间
							ret.setBeginTime(oItemTime.getUpTime());
							ret.setEndTime((oItemTime.getUpTime()+oItemTime.getValidTime()));
						}
						/*if (itemStockBo.isEmpty()) {
							ItemStockBo oItemStockBo = new ItemStockBo();
							oItemStockBo.setActivityPrice(ret.getActivityPrice());
							oItemStockBo.setColorPrice(ret.getColorPrice());
							if (oItemTime != null) {
								oItemStockBo.setCreateTime(oItemTime.getAddTime() * 1000);
								oItemStockBo.setLastModifyTime(oItemTime.getLastModifiedTime() * 1000);
							}
							oItemStockBo.setIndex(0);
							oItemStockBo.setSellerUin(oItemBase.getQqUin());
							oItemStockBo.setShopVipPrice(ret.getShopVipPrice());
							if (isNotSoldComdy)
								oItemStockBo.setStockCount(0);
							else
							oItemStockBo.setStockCount(oItemNum.getNum());
							oItemStockBo.setSoldCount(oItemNum.getPayCount());//========和前面不一致，错了吧？
							oItemStockBo.setStockAttr("");
							oItemStockBo.setStockDesc("");
							oItemStockBo.setStockId("");
							oItemStockBo.setStockLocalCode(oItemBase.getItemLocalCode());
							oItemStockBo.setStockPrice(oItemBase.getPrice());
							itemStockBo.add(oItemStockBo);
						}
						ret.setItemStockBo(itemStockBo);*/

						/*// 对于加款卡可以立即见购买
						if (isVirtalForCard) {
							ret.setBuyNowFlag(1);//===================================?
						}*/
						/**
						 * 设置促销信息
						 */
						int marketStat = 1;
						int sellerProp = 0;
						APIUserProfile profile = ApiUserClient.getUserSimpleInfo(oItemBase.getQqUin());
						int sellerCredit = profile.getSellerCredit() ;
						ret.setSellerCredit(sellerCredit);
						ret.setSellerCreditLevel(SellerCreditUtil.getSellerCreditLevel(sellerCredit));
						if (profile != null) {
							uint16_t key = new uint16_t(ItemConstants.ORDER_USER_SELLER_HAS_PROMOTION);//=======================数据字典
							if (profile.getProp().get(key) != null)
								sellerProp = profile.getProp().get(key).intValue();
							
							if ((sellerProp == 1 && !promoteNo) || (sellerProp == 1 && promoteStart))
								marketStat = 1;
							else
								marketStat = 0;
						}
						ret.setMarketStat(marketStat);	
						
					}

					if (isPromotion)
						ret.setPromotion(1);
					
					/*try {
						GetFavoritesByIdResp statResp = StatClient.fetchItemInfo(Long.parseLong(itemCode.substring(itemCode.length()-16), 16)) ;
						Map<uint32_t,Integer>  storeMap = statResp.getStatData() ;
						uint32_t key = new uint32_t(1) ;
						if (storeMap == null || storeMap.get(key) == null) {
							ret.setStoreNum(0) ;
						} else {
							ret.setStoreNum(storeMap.get(key)) ;
						}
					} catch (Exception ex) {
						ret.setStoreNum(0) ;
						Log.run.info("get storeNum error :" + ex.toString()) ;
					}*/
					
					
					
				}
				return ret;
	}

	public ItemBOPrice getItemPrice(String itemCode) {
		// 过虑虚拟商品
		// 目前就是判断属性位为7的
		boolean isNotSoldComdy = false;
		boolean isSupportCmdy = true;
		//boolean isVirtualCmdy = false;
		boolean isVirtalForCard = false;
		//促销
		//boolean promoteNo = false;
		//boolean promoteStart = false;		
		// 是不是大促活动
		//boolean isPromotion = false;
		
		ItemBOPrice ret = new ItemBOPrice();
		
		if (StringUtil.isNotBlank(itemCode)) {
			FetchItemInfoResp resp = ItemClient.fetchItemInfo(itemCode);
			ItemPo_v2 itemPo = null;
			// 商品基本信息，包含商品的基本信息(商品名称，所在地，最低价格，可使用的红包等)
			ItemBase oItemBase = null;
			// 库存信息
			ItemStockList oItemStockList = null;
			//商品扩展信息 add by wanghao for color picture
			ItemExt oItemExt = null;
			/**
			 * 商品图片信息，包含商品的图片信息(包含商品的主图，5个小图等)
			 * 
			 * 版本 >= 0
			 */
			//ItemImg oItemImg = null;

			if (resp != null && resp.getResult() == 0 && (itemPo = resp.getItemPo()) != null
					&& (oItemBase = itemPo.getOItemBase()) != null) {
				ItemNum oItemNum = itemPo.getOItemNum();//统计信息
				//oItemImg = itemPo.getOItemImg();//图片信息
				oItemStockList = itemPo.getOItemStockList();//库存信息
				ItemProp itemProp = itemPo.getOItemProp();//属性信息
				//商品扩展信息 add by wanghao for color picture start
				oItemExt = itemPo.getOItemExt();
				Map<uint32_t,String> extMap = null;
				Map<String,String> picUrl = new HashMap<String,String>();
				if(null != oItemExt && null != oItemExt.getMapExt() && !oItemExt.getMapExt().isEmpty()){
					extMap = oItemExt.getMapExt();
					String extStr = extMap.get(new uint32_t(3005));//获取颜色图片集合
					if(null != extStr && !"".equals(extStr.trim())){
						String[] split = extStr.split(";");
						for(String str : split){
							String[] strs = str.split("\\|");
							if(str.startsWith("颜色") && strs.length == 2){
								String url = "http://img6.paipaiimg.com/" + strs[1];
								picUrl.put(strs[0], url);
							}
						}
					}
				}
				//add by wanghao end
				//ret.setLeafClassId(oItemBase.getLeafClassId()) ;
				//虚拟卡
				if (oItemBase.getLeafClassId() == 200082) {
					isVirtalForCard = true;
				}				
				if (!(oItemBase.getState() == 2 || oItemBase.getState() == 7 || oItemBase.getState() == 64)) {
					isNotSoldComdy = true;
				}
				if (itemProp != null) {
					BitSet bitSet = itemProp.getBitProp();
					if (bitSet != null) {
						String tmpBitSet = bitSet.toString();
						tmpBitSet = tmpBitSet.replace("{", "");
						tmpBitSet = tmpBitSet.replace("}", "");
						String[] array = tmpBitSet.split(",");
						if (array != null) {
							for (String key : array) {
								if (key != null) {
									int keyProp = (int) StringUtil.toLong(key.trim(), 0);
									if (keyProp == 7) {
										if (!isVirtalForCard)
											isNotSoldComdy = true;
										isSupportCmdy = false;
										isNotSoldComdy = true;
										//isVirtualCmdy = true;
									}
									if (keyProp == 26) {
										// 今日特价 拍下即扣商品  (抢购)
										isSupportCmdy = false;
										isNotSoldComdy = true;
									}
									if (keyProp == 30) {
										// 禁止购买商品
										isNotSoldComdy = true;
										isSupportCmdy = false;
									}
									if (keyProp == 58) {
										// 京东POP
										isNotSoldComdy = true;
										isSupportCmdy = false;
									}
									if (keyProp == 39) {
										// Auction3标识
										isSupportCmdy = false;
										isNotSoldComdy = true;
									}
									if (keyProp == 170) {
										//分期付款商品
										isSupportCmdy = false;
									}
									//拍卖商品
									if (keyProp == 60 || keyProp == 61 || keyProp == 62 || keyProp == 63) {
										isSupportCmdy = false;
										isNotSoldComdy = true;
									}
									//下单即扣，不支持购物车
									if (keyProp == 45) {
										isSupportCmdy = false;
									}
									/*if(keyProp == ItemConstants.INDEX_PROP_PROMOTION_EVENT_NO)
										promoteNo = true;
									if(keyProp == ItemConstants.INDEX_PROP_PROMOTIONAL_EVENT_START)
										promoteStart = true;									
									
									if (keyProp == 53) {
										isPromotion = true;
									}*/
									//ret.getProperties().add(keyProp) ;
								}
							}
							
						}
					}
				}
				/**
				 * 支持立即购买，对于一口价类型支持立即购买
				 */
				if (itemPo.getOItemBase().getDealType() == 1) {
					ret.setFixOrderFlag(1);
				} else {
					isNotSoldComdy = true; //非一口价不能购买
					isSupportCmdy = false; //非一口价不能加入购物车
				}
				
				// 0、 促销价等
				ItemPriceExt oItemPriceExt = itemPo.getOItemPriceExt();
				if (oItemPriceExt != null) {
					ret.setActivityDes(oItemPriceExt.getActivityDesc()) ;
					ret.setColorPrice(oItemPriceExt.getColorPrice());
					ret.setActivityPrice(oItemPriceExt.getActivityPrice());
					ret.setShopVipPrice(oItemPriceExt.getShopVipPrice());
					// 大促活动
					/*if (oItemPriceExt.getActivityType() == (long) ItemConstants.ITEM_ACTIVETYPE_BIGSELL) {
						isPromotion = true;
					}*/
					/**
					 * 如果是NBA活动和快捷支付类型就不支持购物车
					 */
					if (oItemPriceExt.getActivityType() == 0x80000001L
							|| oItemPriceExt.getActivityType() == 0x800000002L) {
						isSupportCmdy = false;
					}

					/**
					 * 如果是特价类，并且正在抢购的，就把库存信息设置成0
					 */
					long currTime = System.currentTimeMillis() / 1000;
					if (oItemPriceExt.getActivityType() == 41 && currTime >= oItemPriceExt.getBeginTime()
							&& currTime <= oItemPriceExt.getEndTime()) {
						isNotSoldComdy = true;
					}

				}

				if (isSupportCmdy) {
					ret.setCartFlag(1); 
				}
				if (isNotSoldComdy) {
					ret.setIsNotSoldComdy(1) ;
				}
					

				// 1、商品统计信息
				if (oItemNum != null) {
					/*ret.setBuyCount(oItemNum.getBuyCount());
//					ret.setBuyNum(oItemNum.getBuyNum());
					//月销售量
					ret.setBuyNum(oItemNum.getCurrPayNum());*/
					if (isNotSoldComdy) {
						ret.setStockCount(0);
					} else {
						ret.setStockCount(oItemNum.getNum());
					}						
					/*ret.setTotalBuyCount(oItemNum.getTotalBuyCount());
					ret.setTotalBuyNum(oItemNum.getToalBuyNum());
					ret.setVisitCount(oItemNum.getVisitCount());*/

				}

				// 2、商品基本信息
				/*ret.setBuyLimit(oItemBase.getBuyLimit());
				ret.setCityId(oItemBase.getCity());
				ret.setCountryId(oItemBase.getCountry());
				ret.setEmsPrice(oItemBase.getEmsMailPrice());
				ret.setExpressPrice(oItemBase.getExpressMailPrice());
				ret.setItemState(ItemState.getItemState(oItemBase.getState()));
				ret.setStateDesc(ItemState.getShowMeg(oItemBase.getState()));*/
				// 3、扩展属性，目前这里主要是用于红包的使用
				/*long redPrice = oItemBase.getRedPrice();
				if (redPrice > 0) {
					ret.setRedPrice(redPrice);
					Map<ItemExtend, String> redMap = new HashMap<ItemExtend, String>(1);
					redMap.put(ItemExtend.getItemExtend(133), "" + (redPrice + 100));
					ret.setExtendInfo(redMap);
				}
*/
				// 4、处理图片信息
				/*Vector<String> vecImgTmp = null, vecImg = null;
				int size = 0;
				if (oItemImg != null && (vecImgTmp = oItemImg.getVecImg()) != null && !vecImgTmp.isEmpty()) {
					size = vecImgTmp.size();
					vecImg = new Vector<String>(size);
					for (String iamgeUrl : vecImgTmp) {
						// 跳转商详图片url至400X400
						vecImg.add(genImageUrl(iamgeUrl, imageFormat400));
					}
					if (size > 0)
						ret.setPicLink(vecImg.get(0));					
					for (int i=1;i<size;i++) {
						String imgUrl = vecImg.get(i) ;
						if (!imgUrl.contains("paipaiimg.com/showimg")) {
							ret.getExtPicsLink().add(imgUrl) ;
						}							
					}
						//ret.setExtPicsLink(vecImg.subList(1, vecImg.size()));
					ret.setVecImg(vecImg);
				}*/

				/*ret.setFreightId(oItemBase.getShippingfeeId());
				ret.setCodFreightId(oItemBase.getCodShipTempId());*/
				ret.setItemCode(oItemBase.getItemId());
				/*ret.setItemUrl(ITEM_URL_PREFIZ + oItemBase.getItemId()) ;//商品pc端链接地址
				ret.setItemWeiDian(ITEM_URL_WEI_DIAN + oItemBase.getItemId()) ;
			    ret.setItemLocalCode(oItemBase.getItemLocalCode());
				if (isVirtualCmdy && !isVirtalForCard)
					ret.setItemName(VIRTUAL_PRE + oItemBase.getTitle());
				else
					ret.setItemName(oItemBase.getTitle());*/
				ret.setItemPrice(oItemBase.getPrice());

				// 5、库存信息
				Vector<ItemStock> itemStockList = null;
				int index = 0;
				List<ItemStockBo> itemStockBo = new ArrayList<ItemStockBo>();
				if (oItemStockList != null && (itemStockList = oItemStockList.getOItemStock()) != null
						&& !itemStockList.isEmpty()) {
					for (ItemStock stockItem : itemStockList) {
						ItemStockBo oItemStockBo = new ItemStockBo();
						oItemStockBo.setCreateTime(stockItem.getAddTime() * 1000);
						ItemPriceExt oItemPriceExt2 = null;
						// 促销价等
						if ((oItemPriceExt2 = stockItem.getOPriceExt()) != null) {
							// 大促活动
							/*if (oItemPriceExt2.getActivityType() == (long) ItemConstants.ITEM_ACTIVETYPE_BIGSELL) {
								isPromotion = true;
							}*/
							oItemStockBo.setColorPrice(oItemPriceExt2.getColorPrice());
							oItemStockBo.setActivityPrice(oItemPriceExt2.getActivityPrice());
							oItemStockBo.setShopVipPrice(oItemPriceExt2.getShopVipPrice());
						}
						oItemStockBo.setIndex(index++);
						oItemStockBo.setLastModifyTime(stockItem.getLastModifyTime() * 1000);
						oItemStockBo.setSellerUin(oItemBase.getQqUin());
						oItemStockBo.setSoldCount(stockItem.getSoldNum());
						oItemStockBo.setStockAttr(stockItem.getAttr());
						//当颜色对应的有图片信息时，在颜色属性后面追加图片URL add by wanghao start
						String attr = stockItem.getAttr();
						StringBuffer sb = new StringBuffer();
						if(null != attr && !"".equals(attr.trim())){
							String[] split = attr.split("\\|");
							for(String str : split){
								if(null != str && !"".equals(str.trim()) && str.startsWith("颜色")){
									String url = picUrl.get(str);
									oItemStockBo.setColorPicUrl((null != url) ? url : "");
								}
							}
						}
						//add by wanghao end
						if (isNotSoldComdy)
							oItemStockBo.setStockCount(0);//不让下单，过滤作用
						else
							oItemStockBo.setStockCount(stockItem.getNum());
						oItemStockBo.setStockDesc(stockItem.getDesc());
						oItemStockBo.setStockId("" + stockItem.getStockId());
						oItemStockBo.setStockLocalCode(stockItem.getStockCode());
						oItemStockBo.setStockPrice(stockItem.getPrice());
						itemStockBo.add(oItemStockBo);
					}
				}
				/*ret.setMailPrice(oItemBase.getNormalMailPrice());
				ret.setMarketPrice(oItemBase.getMarketPrice());
*/
				// 属性值转换
				// 6、解析商品类目信息
				/*if (needParseAttr) {
					String attrText = oItemBase.getAttrText();
					Log.run.info("----------------------------attrText----------------------"+attrText);
					List<ItemAttrBO> itemAttrBO;
					try {
						itemAttrBO = ItemHelper.parseAttr(attrText, oItemBase.getLeafClassId());
						ret.setParsedAttrList(itemAttrBO);
					} catch (Exception e) {
					}
				}

				ret.setProvinceId(oItemBase.getProductId());
*/
				// 7、关联商品======================================================?
				/*Vector<String> relatedItems = null;
				if ((relatedItems = oItemBase.getRelatedItems()) != null) {
					StringBuffer buf = new StringBuffer();
					for (String relatedItem : relatedItems) {
						buf.append(relatedItem + "|");
					}
					ret.setRelatedItems(buf.toString());
				}
				ret.setSellerName(oItemBase.getQqNick());
				卖家或者买家承担运费的情况 1 卖家承担运费 2 买家承担运费 3 同城交易，无需运费 大于或等于10
				 * 买家承担运费，表示支持运费模板，该值即为运费模板ID
				ret.setSellerPayFreight(oItemBase.getTransportPriceType());
				if (ret.getFreightId() >= 10 && ret.getSellerPayFreight() == 2) {
					ret.setSellerPayFreight(ret.getFreightId());
				}*/
				ret.setSellerUin(oItemBase.getQqUin());
				/*ret.setShopWeiDian(SHOP_URL_WEI_DIAN + oItemBase.getQqUin()) ;
				*//**
				 * 如果没有库存属性信息，就生成一个以支持老版本
				 *//*
				ItemTime oItemTime = itemPo.getOItemTime();
				if (oItemTime != null) {
					//设置商品的开始时间与商品的结束时间
					ret.setBeginTime(oItemTime.getUpTime());
					ret.setEndTime((oItemTime.getUpTime()+oItemTime.getValidTime()));
				}*/
				if (itemStockBo.isEmpty()) {
					ItemStockBo oItemStockBo = new ItemStockBo();
					oItemStockBo.setActivityPrice(ret.getActivityPrice());
					oItemStockBo.setColorPrice(ret.getColorPrice());
					/*if (oItemTime != null) {
						oItemStockBo.setCreateTime(oItemTime.getAddTime() * 1000);
						oItemStockBo.setLastModifyTime(oItemTime.getLastModifiedTime() * 1000);
					}*/
					oItemStockBo.setIndex(0);
					oItemStockBo.setSellerUin(oItemBase.getQqUin());
					oItemStockBo.setShopVipPrice(ret.getShopVipPrice());
					if (isNotSoldComdy)
						oItemStockBo.setStockCount(0);
					else
						oItemStockBo.setStockCount(oItemNum.getNum());
					oItemStockBo.setSoldCount(oItemNum.getPayCount());//========和前面不一致，错了吧？
					oItemStockBo.setStockAttr("");
					oItemStockBo.setStockDesc("");
					oItemStockBo.setStockId("");
					oItemStockBo.setStockLocalCode(oItemBase.getItemLocalCode());
					oItemStockBo.setStockPrice(oItemBase.getPrice());
					itemStockBo.add(oItemStockBo);
				}
				ret.setItemStockBo(itemStockBo);

				// 对于加款卡可以立即见购买
				if (isVirtalForCard) {
					ret.setBuyNowFlag(1);//===================================?
				}
				/**
				 * 设置促销信息==================================？
				 */
				/*int marketStat = 1;
				int sellerProp = 0;
				APIUserProfile profile = ApiUserClient.getUsetSimpleInfo(oItemBase.getQqUin());
				if (profile != null) {
					uint16_t key = new uint16_t(ItemConstants.ORDER_USER_SELLER_HAS_PROMOTION);//=======================数据字典
					if (profile.getProp().get(key) != null)
						sellerProp = profile.getProp().get(key).intValue();
					
					if ((sellerProp == 1 && !promoteNo) || (sellerProp == 1 && promoteStart))
						marketStat = 1;
					else
						marketStat = 0;
				}
				ret.setMarketStat(marketStat);	
			*/	
			}

			/*if (isPromotion)
				ret.setPromotion(1);*/
			
			/*try {
				GetFavoritesByIdResp statResp = StatClient.fetchItemInfo(Long.parseLong(itemCode.substring(itemCode.length()-16), 16)) ;
				Map<uint32_t,Integer>  storeMap = statResp.getStatData() ;
				uint32_t key = new uint32_t(1) ;
				if (storeMap == null || storeMap.get(key) == null) {
					ret.setStoreNum(0) ;
				} else {
					ret.setStoreNum(storeMap.get(key)) ;
				}
			} catch (Exception ex) {
				ret.setStoreNum(0) ;
				Log.run.info("get storeNum error :" + ex.toString()) ;
			}*/
			
			
			
		}
		return ret;
	}
	
	@Override
	public ItemBOExtInfo getItemExtInfo(String itemCode) {		
		
		ItemBOExtInfo ret = new ItemBOExtInfo();
		
		if (StringUtil.isNotBlank(itemCode)) {
			FetchItemInfoResp resp = ItemClient.fetchItemInfo(itemCode);
			ItemPo_v2 itemPo = null;
			// 商品基本信息，包含商品的基本信息(商品名称，所在地，最低价格，可使用的红包等)
			ItemBase oItemBase = null;			
			/**
			 * 商品图片信息，包含商品的图片信息(包含商品的主图，5个小图等)
			 * 
			 * 版本 >= 0
			 */
			if (resp != null && resp.getResult() == 0 && (itemPo = resp.getItemPo()) != null
					&& (oItemBase = itemPo.getOItemBase()) != null) {				
				
				// 3、扩展属性，目前这里主要是用于红包的使用
				long redPrice = oItemBase.getRedPrice();
				if (redPrice > 0) {
					ret.setRedPrice(redPrice);
					Map<ItemExtend, String> redMap = new HashMap<ItemExtend, String>(1);
					redMap.put(ItemExtend.getItemExtend(133), "" + (redPrice + 100));
					ret.setExtendInfo(redMap);
				}
				ret.setItemCode(oItemBase.getItemId());
				// 属性值转换
				// 6、解析商品类目信息
				
				String attrText = oItemBase.getAttrText();
				List<ItemAttrBO> itemAttrBO;
				try {
					itemAttrBO = ItemHelper.parseAttr(attrText, oItemBase.getLeafClassId());
					ret.setParsedAttrList(itemAttrBO);
				} catch (Exception e) {
				}				
			}
			try {
				GetFavoritesByIdResp statResp = StatClient.findFavCount(Long.parseLong(itemCode.substring(itemCode.length() - 16), 16)) ;
				Map<uint32_t,Integer>  storeMap = statResp.getStatData() ;
				uint32_t key = new uint32_t(1) ;
				if (storeMap == null || storeMap.get(key) == null) {
					ret.setStoreNum(0) ;
				} else {
					ret.setStoreNum(storeMap.get(key)) ;
				}
			} catch (Exception ex) {
				ret.setStoreNum(0) ;
				Log.run.info("get storeNum error :" + ex.toString()) ;
			}
			
		}
		return ret;
	}

	
	@Override
	public ItemBO fetchItemInfo(String itemCode, boolean needParseAttr) {
		// 过虑虚拟商品
		// 目前就是判断属性位为7的
		boolean isNotSoldComdy = false;
		boolean isSupportCmdy = true;
		boolean isVirtualCmdy = false;
		boolean isVirtalForCard = false;
		//促销
		boolean promoteNo = false;
		boolean promoteStart = false;		
		// 是不是大促活动
		boolean isPromotion = false;
		
		ItemBO ret = new ItemBO();
		
		if (StringUtil.isNotBlank(itemCode)) {
			FetchItemInfoResp resp = ItemClient.fetchItemInfo(itemCode);
			ItemPo_v2 itemPo = null;
			// 商品基本信息，包含商品的基本信息(商品名称，所在地，最低价格，可使用的红包等)
			ItemBase oItemBase = null;
			// 库存信息
			ItemStockList oItemStockList = null;
			/**
			 * 商品图片信息，包含商品的图片信息(包含商品的主图，5个小图等)
			 * 
			 * 版本 >= 0
			 */
			ItemImg oItemImg = null;

			if (resp != null && resp.getResult() == 0 && (itemPo = resp.getItemPo()) != null
					&& (oItemBase = itemPo.getOItemBase()) != null) {
				ItemNum oItemNum = itemPo.getOItemNum();//统计信息
				oItemImg = itemPo.getOItemImg();//图片信息
				oItemStockList = itemPo.getOItemStockList();//库存信息
				ItemProp itemProp = itemPo.getOItemProp();//属性信息
				ret.setLeafClassId(oItemBase.getLeafClassId()) ;
				//虚拟卡
				if (oItemBase.getLeafClassId() == 200082) {
					isVirtalForCard = true;
				}				
				if (!(oItemBase.getState() == 2 || oItemBase.getState() == 7 || oItemBase.getState() == 64)) {
					isNotSoldComdy = true;
				}
				if (itemProp != null) {
					BitSet bitSet = itemProp.getBitProp();
					if (bitSet != null) {
						String tmpBitSet = bitSet.toString();
						tmpBitSet = tmpBitSet.replace("{", "");
						tmpBitSet = tmpBitSet.replace("}", "");
						String[] array = tmpBitSet.split(",");
						if (array != null) {
							for (String key : array) {
								if (key != null) {
									int keyProp = (int) StringUtil.toLong(key.trim(), 0);
									if (keyProp == 7) {
										if (!isVirtalForCard)
											isNotSoldComdy = true;
										isSupportCmdy = false;
										isNotSoldComdy = true;
										isVirtualCmdy = true;
									}
									if (keyProp == 26) {
										// 今日特价 拍下即扣商品  (抢购)
										isSupportCmdy = false;
										isNotSoldComdy = true;
									}
									if (keyProp == 30) {
										// 禁止购买商品
										isNotSoldComdy = true;
										isSupportCmdy = false;
									}
									if (keyProp == 58) {
										// 京东POP
										isNotSoldComdy = true;
										isSupportCmdy = false;
									}
									if (keyProp == 39) {
										// Auction3标识
										isSupportCmdy = false;
										isNotSoldComdy = true;
									}
									if (keyProp == 170) {
										//分期付款商品
										isSupportCmdy = false;
									}
									//拍卖商品
									if (keyProp == 60 || keyProp == 61 || keyProp == 62 || keyProp == 63) {
										isSupportCmdy = false;
										isNotSoldComdy = true;
									}
									//下单即扣，不支持购物车
									if (keyProp == 45) {
										isSupportCmdy = false;
									}
									if(keyProp == ItemConstants.INDEX_PROP_PROMOTION_EVENT_NO)
										promoteNo = true;
									if(keyProp == ItemConstants.INDEX_PROP_PROMOTIONAL_EVENT_START)
										promoteStart = true;									
									
									if (keyProp == 53) {
										isPromotion = true;
									}
									ret.getProperties().add(keyProp) ;
								}
							}
							
						}
					}
				}
				/**
				 * 支持立即购买，对于一口价类型支持立即购买
				 */
				if (itemPo.getOItemBase().getDealType() == 1) {
					ret.setFixOrderFlag(1);
				} else {
					isNotSoldComdy = true; //非一口价不能购买
					isSupportCmdy = false; //非一口价不能加入购物车
				}
				
				// 0、 促销价等
				ItemPriceExt oItemPriceExt = itemPo.getOItemPriceExt();
				if (oItemPriceExt != null) {
					ret.setActivityDes(oItemPriceExt.getActivityDesc()) ;
					ret.setColorPrice(oItemPriceExt.getColorPrice());
					ret.setActivityPrice(oItemPriceExt.getActivityPrice());
					ret.setShopVipPrice(oItemPriceExt.getShopVipPrice());
					// 大促活动
					if (oItemPriceExt.getActivityType() == (long) ItemConstants.ITEM_ACTIVETYPE_BIGSELL) {
						isPromotion = true;
					}
					/**
					 * 如果是NBA活动和快捷支付类型就不支持购物车
					 */
					if (oItemPriceExt.getActivityType() == 0x80000001L
							|| oItemPriceExt.getActivityType() == 0x800000002L) {
						isSupportCmdy = false;
					}

					/**
					 * 如果是特价类，并且正在抢购的，就把库存信息设置成0
					 */
					long currTime = System.currentTimeMillis() / 1000;
					if (oItemPriceExt.getActivityType() == 41 && currTime >= oItemPriceExt.getBeginTime()
							&& currTime <= oItemPriceExt.getEndTime()) {
						isNotSoldComdy = true;
					}

				}

				if (isSupportCmdy) {
					ret.setCartFlag(1); 
				}
				if (isNotSoldComdy) {
					ret.setIsNotSoldComdy(1) ;
				}
					

				// 1、商品统计信息
				if (oItemNum != null) {
					ret.setBuyCount(oItemNum.getBuyCount());
//					ret.setBuyNum(oItemNum.getBuyNum());
					//月销售量
					ret.setBuyNum(oItemNum.getCurrPayNum());
					if (isNotSoldComdy) {
						ret.setStockCount(0);
					} else {
						ret.setStockCount(oItemNum.getNum());
					}						
					ret.setTotalBuyCount(oItemNum.getTotalBuyCount());
					ret.setTotalBuyNum(oItemNum.getToalBuyNum());
					ret.setVisitCount(oItemNum.getVisitCount());

				}

				// 2、商品基本信息
				ret.setBuyLimit(oItemBase.getBuyLimit());
				ret.setCityId(oItemBase.getCity());
				ret.setCountryId(oItemBase.getCountry());
				ret.setEmsPrice(oItemBase.getEmsMailPrice());
				ret.setExpressPrice(oItemBase.getExpressMailPrice());
				ret.setItemState(ItemState.getItemState(oItemBase.getState()));
				ret.setStateDesc(ItemState.getShowMeg(oItemBase.getState()));
				// 3、扩展属性，目前这里主要是用于红包的使用
				long redPrice = oItemBase.getRedPrice();
				if (redPrice > 0) {
					ret.setRedPrice(redPrice);
					Map<ItemExtend, String> redMap = new HashMap<ItemExtend, String>(1);
					redMap.put(ItemExtend.getItemExtend(133), "" + (redPrice + 100));
					ret.setExtendInfo(redMap);
				}

				// 4、处理图片信息
				Vector<String> vecImgTmp = null, vecImg = null;
				int size = 0;
				if (oItemImg != null && (vecImgTmp = oItemImg.getVecImg()) != null && !vecImgTmp.isEmpty()) {
					size = vecImgTmp.size();
					vecImg = new Vector<String>(size);
					for (String iamgeUrl : vecImgTmp) {
						// 跳转商详图片url至400X400
						vecImg.add(genImageUrl(iamgeUrl, imageFormat400));
					}
					if (size > 0)
						ret.setPicLink(vecImg.get(0));					
					for (int i=1;i<size;i++) {
						String imgUrl = vecImg.get(i) ;
						if (!imgUrl.contains("paipaiimg.com/showimg")) {
							ret.getExtPicsLink().add(imgUrl) ;
						}							
					}
						//ret.setExtPicsLink(vecImg.subList(1, vecImg.size()));
					ret.setVecImg(vecImg);
				}

				ret.setFreightId(oItemBase.getShippingfeeId());
				ret.setCodFreightId(oItemBase.getCodShipTempId());
				ret.setItemCode(oItemBase.getItemId());
				ret.setItemUrl(ITEM_URL_PREFIZ + oItemBase.getItemId()) ;//商品pc端链接地址
				ret.setItemWeiDian(ITEM_URL_WEI_DIAN + oItemBase.getItemId()) ;
			    ret.setItemLocalCode(oItemBase.getItemLocalCode());
				if (isVirtualCmdy && !isVirtalForCard)
					ret.setItemName(VIRTUAL_PRE + oItemBase.getTitle());
				else
					ret.setItemName(oItemBase.getTitle());
				ret.setItemPrice(oItemBase.getPrice());

				// 5、库存信息
				Vector<ItemStock> itemStockList = null;
				int index = 0;
				List<ItemStockBo> itemStockBo = new ArrayList<ItemStockBo>();
				if (oItemStockList != null && (itemStockList = oItemStockList.getOItemStock()) != null
						&& !itemStockList.isEmpty()) {
					for (ItemStock stockItem : itemStockList) {
						ItemStockBo oItemStockBo = new ItemStockBo();
						oItemStockBo.setCreateTime(stockItem.getAddTime() * 1000);
						ItemPriceExt oItemPriceExt2 = null;
						// 促销价等
						if ((oItemPriceExt2 = stockItem.getOPriceExt()) != null) {
							// 大促活动
							if (oItemPriceExt2.getActivityType() == (long) ItemConstants.ITEM_ACTIVETYPE_BIGSELL) {
								isPromotion = true;
							}
							oItemStockBo.setColorPrice(oItemPriceExt2.getColorPrice());
							oItemStockBo.setActivityPrice(oItemPriceExt2.getActivityPrice());
							oItemStockBo.setShopVipPrice(oItemPriceExt2.getShopVipPrice());
						}
						oItemStockBo.setIndex(index++);
						oItemStockBo.setLastModifyTime(stockItem.getLastModifyTime() * 1000);
						oItemStockBo.setSellerUin(oItemBase.getQqUin());
						oItemStockBo.setSoldCount(stockItem.getSoldNum());
						oItemStockBo.setStockAttr(stockItem.getAttr());
						if (isNotSoldComdy)
							oItemStockBo.setStockCount(0);//不让下单，过滤作用
						else
							oItemStockBo.setStockCount(stockItem.getNum());
						oItemStockBo.setStockDesc(stockItem.getDesc());
						oItemStockBo.setStockId("" + stockItem.getStockId());
						oItemStockBo.setStockLocalCode(stockItem.getStockCode());
						oItemStockBo.setStockPrice(stockItem.getPrice());
						itemStockBo.add(oItemStockBo);
					}
				}
				ret.setMailPrice(oItemBase.getNormalMailPrice());
				ret.setMarketPrice(oItemBase.getMarketPrice());

				// 属性值转换
				// 6、解析商品类目信息
				if (needParseAttr) {
					String attrText = oItemBase.getAttrText();
					Log.run.info("----------------------------attrText----------------------"+attrText);
					List<ItemAttrBO> itemAttrBO;
					try {
						itemAttrBO = ItemHelper.parseAttr(attrText, oItemBase.getLeafClassId());
						ret.setParsedAttrList(itemAttrBO);
					} catch (Exception e) {
					}
				}

				ret.setProvinceId(oItemBase.getProductId());

				// 7、关联商品======================================================?
				Vector<String> relatedItems = null;
				if ((relatedItems = oItemBase.getRelatedItems()) != null) {
					StringBuffer buf = new StringBuffer();
					for (String relatedItem : relatedItems) {
						buf.append(relatedItem + "|");
					}
					ret.setRelatedItems(buf.toString());
				}
				ret.setSellerName(oItemBase.getQqNick());
				/*卖家或者买家承担运费的情况 1 卖家承担运费 2 买家承担运费 3 同城交易，无需运费 大于或等于10
				 * 买家承担运费，表示支持运费模板，该值即为运费模板ID*/
				ret.setSellerPayFreight(oItemBase.getTransportPriceType());
				if (ret.getFreightId() >= 10 && ret.getSellerPayFreight() == 2) {
					ret.setSellerPayFreight(ret.getFreightId());
				}
				ret.setSellerUin(oItemBase.getQqUin());
				ret.setShopWeiDian(SHOP_URL_WEI_DIAN + oItemBase.getQqUin()) ;
				/**
				 * 如果没有库存属性信息，就生成一个以支持老版本
				 */
				ItemTime oItemTime = itemPo.getOItemTime();
				if (oItemTime != null) {
					//设置商品的开始时间与商品的结束时间
					ret.setBeginTime(oItemTime.getUpTime());
					ret.setEndTime((oItemTime.getUpTime()+oItemTime.getValidTime()));
				}
				if (itemStockBo.isEmpty()) {
					ItemStockBo oItemStockBo = new ItemStockBo();
					oItemStockBo.setActivityPrice(ret.getActivityPrice());
					oItemStockBo.setColorPrice(ret.getColorPrice());
					if (oItemTime != null) {
						oItemStockBo.setCreateTime(oItemTime.getAddTime() * 1000);
						oItemStockBo.setLastModifyTime(oItemTime.getLastModifiedTime() * 1000);
					}
					oItemStockBo.setIndex(0);
					oItemStockBo.setSellerUin(oItemBase.getQqUin());
					oItemStockBo.setShopVipPrice(ret.getShopVipPrice());
					if (isNotSoldComdy)
						oItemStockBo.setStockCount(0);
					else
						oItemStockBo.setStockCount(oItemNum.getNum());
					oItemStockBo.setSoldCount(oItemNum.getPayCount());//========和前面不一致，错了吧？
					oItemStockBo.setStockAttr("");
					oItemStockBo.setStockDesc("");
					oItemStockBo.setStockId("");
					oItemStockBo.setStockLocalCode(oItemBase.getItemLocalCode());
					oItemStockBo.setStockPrice(oItemBase.getPrice());
					itemStockBo.add(oItemStockBo);
				}
				ret.setItemStockBo(itemStockBo);

				// 对于加款卡可以立即见购买
				if (isVirtalForCard) {
					ret.setBuyNowFlag(1);//===================================?
				}
				/**
				 * 设置促销信息==================================？
				 */
				int marketStat = 1;
				int sellerProp = 0;
				APIUserProfile profile = ApiUserClient.getUserSimpleInfo(oItemBase.getQqUin());
				if (profile != null) {
					uint16_t key = new uint16_t(ItemConstants.ORDER_USER_SELLER_HAS_PROMOTION);//=======================数据字典
					if (profile.getProp().get(key) != null)
						sellerProp = profile.getProp().get(key).intValue();
					
					if ((sellerProp == 1 && !promoteNo) || (sellerProp == 1 && promoteStart))
						marketStat = 1;
					else
						marketStat = 0;
				}
				ret.setMarketStat(marketStat);	
				
			}

			if (isPromotion)
				ret.setPromotion(1);
			
			try {
				GetFavoritesByIdResp statResp = StatClient.findFavCount(Long.parseLong(itemCode.substring(itemCode.length() - 16), 16)) ;
				Map<uint32_t,Integer>  storeMap = statResp.getStatData() ;
				uint32_t key = new uint32_t(1) ;
				if (storeMap == null || storeMap.get(key) == null) {
					ret.setStoreNum(0) ;
				} else {
					ret.setStoreNum(storeMap.get(key)) ;
				}
			} catch (Exception ex) {
				ret.setStoreNum(0) ;
				Log.run.info("get storeNum error :" + ex.toString()) ;
			}
			
			
			
		}
		return ret;
	}

	@Override
	public void fetchCmtShopInfo4Item(String itemCode, ItemBO item, String mk) {
		if (!ItemUtil.isValidItemCode(itemCode) || item == null) {
			return;
		}
		long sellerUin = ItemUtil.getSellerUin(itemCode);		
		// 获取商品评论总数
		GetComdyEvalListResp resp = ItemClient.getItemEval(itemCode, sellerUin, 1, 1, 0, 0, 0);
		if (resp != null && resp.getResult() == 0) {
			item.setCommentCount(resp.getTotalNum());
		}
		
		ShopBiz shopBiz = (ShopBiz) SpringHelper.getBean("shopBiz");
		try {
			ShopInfo shopInfo = shopBiz.getShopInfo(sellerUin);
			item.setAttitudeOfService(shopInfo.getAttitudeOfService()) ;
			item.setSpeedOfDelivery(shopInfo.getSpeedOfDelivery()) ;
			item.setGoodDescriptionMatch(shopInfo.getGoodDescriptionMatch());
			item.setShopMark(shopInfo.getTotalEval());	
			item.setShopLogo(shopInfo.getLogo()) ;
			item.setShopName(shopInfo.getShopName()) ;
			List<ShopSimpleItem> list = shopBiz.getRecommedComdyList(sellerUin,
		            mk);
			if (list != null) {
				for (ShopSimpleItem shopItem : list) {
					ShopItemInfo shopItemInfo = new ShopItemInfo() ;
					shopItemInfo.setImgUrl(shopItem.getImage());
					shopItemInfo.setItemCode(shopItem.getItemId());
					item.getShopRecommendItem().add(shopItemInfo) ;
				}
			}			
		} catch (ExternalInterfaceException e) {
			Log.run.warn("shop mark missed", e);
		}

	}
	
	@Override
	public void fetchCmtShopInfo4Item(String itemCode, ItemBOExtInfo item, String mk) {
		if (!ItemUtil.isValidItemCode(itemCode) || item == null) {
			return;
		}
		long sellerUin = ItemUtil.getSellerUin(itemCode);		
		// 获取商品评论总数
		GetComdyEvalListResp resp = ItemClient.getItemEval(itemCode, sellerUin, 1, 1, 0, 0, 0);
		if (resp != null && resp.getResult() == 0) {
			item.setCommentCount(resp.getTotalNum());
		}
		
		ShopBiz shopBiz = (ShopBiz) SpringHelper.getBean("shopBiz");
		try {
			ShopInfo shopInfo = shopBiz.getShopInfo(sellerUin);
			item.setAttitudeOfService(shopInfo.getAttitudeOfService()) ;
			item.setSpeedOfDelivery(shopInfo.getSpeedOfDelivery()) ;
			item.setGoodDescriptionMatch(shopInfo.getGoodDescriptionMatch());
			item.setShopMark(shopInfo.getTotalEval());	
			item.setShopLogo(shopInfo.getLogo()) ;
			item.setShopName(shopInfo.getShopName()) ;
			List<ShopSimpleItem> list = shopBiz.getRecommedComdyList(sellerUin,
		            mk);
			if (list != null) {
				for (ShopSimpleItem shopItem : list) {
					ShopItemInfo shopItemInfo = new ShopItemInfo() ;
					shopItemInfo.setImgUrl(shopItem.getImage());
					shopItemInfo.setItemName(shopItem.getTitle());
					shopItemInfo.setBuyNum(shopItem.getSoldNum());
					long currentBuyNum = 0l ;
					FetchItemInfoResp fetchItemInfoResp = ItemClient.fetchItemInfo(shopItem.getItemId()) ;
					if (fetchItemInfoResp != null && fetchItemInfoResp.getResult() == 0 ) {
						currentBuyNum = fetchItemInfoResp.getItemPo().getOItemNum().getCurrPayNum() ;
					}					
					shopItemInfo.setCurrentBuyNum(currentBuyNum) ;
					shopItemInfo.setPrice(shopItem.getPrice());
					shopItemInfo.setItemCode(shopItem.getItemId());
					item.getShopRecommendItem().add(shopItemInfo) ;
				}
			}
		} catch (ExternalInterfaceException e) {
			Log.run.warn("shop mark missed", e);
		}
		try {
			GetFavoritesByIdResp statResp = StatClient.findFavCount(sellerUin) ;
			Map<uint32_t,Integer>  storeMap = statResp.getStatData() ;
			item.setShopStoreNum(storeMap.get(new uint32_t(2)));
		} catch (Exception e) {
			Log.run.warn("get shop num error", e);
		}

	}

	@Override
	public String getCurrentActive(Long sellerUin) {
		String result = "{}" ;
		try {			
			SelfMarketForQueryResp resp = SelfMarketActiveClient.selfMarketForQuery(sellerUin) ;
			if (resp != null && resp.getResult() == 0 && 
					resp.getSelfMarketActiveForOrderResp().getActive().getVecContent().size() > 0) {				
				JSONObject resultJsonObj = new JSONObject() ;
				
				SelfMarketActiveForOrderRespBo resultBo = resp.getSelfMarketActiveForOrderResp();
				long uin = resultBo.getUin() ;
				SelfMarketActiveOrderBo activeBo = resultBo.getActive() ;
				String beginTime = activeBo.getBeginTime().substring(0, 10) ;
				String endTime = activeBo.getEndTime().substring(0, 10) ;
				String remark = activeBo.getActiveDesc() ;				
				resultJsonObj.put("uin", uin) ;
				resultJsonObj.put("beginTime", beginTime) ;
				resultJsonObj.put("endTime", endTime) ;
				resultJsonObj.put("remark", remark) ;				
				JSONArray content = new JSONArray() ;
				for (SelfMarkContentOrderBo orderBo : activeBo.getVecContent()) {
					JSONObject jsonObj = new JSONObject() ;
					String desc = orderBo.getContentDesc() ;
					jsonObj.put("desc", desc) ;
					content.add(jsonObj) ;
				}
				resultJsonObj.put("content", content) ;
				result = resultJsonObj.toString() ;
			} 			
		} catch (Exception ex) {
			Log.run.info("get currentActive error " + ex.toString() + ",result=" + result) ;
			result = "{}" ;
		}		
		return result ;
	}

	@Override
	public void handlePrice(ItemBO item, long sellerUin, long wid, String sk) {
		// TODO Auto-generated method stub
		String resultStr = "";
		List<ItemStockBo> itemStockBos = item.getItemStockBo() ;
		try {
			if (wid == sellerUin) {
				item.setIsNotSoldComdy(1) ;
				item.setStockCount(0) ;
				item.setCartFlag(0) ;
				for (int i = 0 ; i < itemStockBos.size(); i++) {
					itemStockBos.get(i).setStockCount(0) ;
				}
			}
			StringBuffer getUrl = new StringBuffer();
			String cookie = "wg_uin=" + wid + "; wg_skey=" + sk;   
			getUrl.append(this.USER_BASIC_URL).append("?callback=_callbackCommodityPrice&selleruin=").append(sellerUin);
			resultStr = HttpUtil.get(getUrl.toString(),null, 10000, 15000, "gbk", true,false,cookie,null);
			Log.run.info("get cookie is : " + cookie + "get colorLever url : " + getUrl.toString());
			if (!StringUtil.isEmpty(resultStr) ) {				
				String result = resultStr.substring(resultStr.indexOf("callbackCommodityPrice(") + 23,resultStr.indexOf("})") +1) ;							
				JSONObject resultJson = JSONObject.fromObject(result) ;	
				String colordiscountLevel = "0" ;
				String shopvipdiscountLevel = "0" ;
				int userColorLever = 0 ;
				int shopVipLever = 0 ;
				if (resultJson.getString("errCode").equals("0")) {
					JSONObject json = JSONObject.fromObject(resultJson.getJSONArray("data").get(0)) ;
					colordiscountLevel = json.getString("colordiscountLevel") ;
					shopvipdiscountLevel = json.getString("shopvipdiscountLevel") ;										
					shopVipLever = Integer.parseInt(shopvipdiscountLevel) ;
					userColorLever = Integer.parseInt(colordiscountLevel) ;
					if (userColorLever > 6) {
						userColorLever = 6 ;
					}
					if (userColorLever != 0) {
						if (item.getColorPrice().size() != 0) {
							item.setItemColorPrice(item.getColorPrice().get(userColorLever - 1).longValue()) ;
							for (int i = 0 ;i < itemStockBos.size(); i++) {
								if (itemStockBos.get(i).getColorPrice().size() != 0 ) {
									itemStockBos.get(i).setItemColorPrice(itemStockBos.get(i).getColorPrice().get(userColorLever - 1).longValue());
								}
							}
						}
					} 
					//店铺vip等级下标不减1
					if (shopVipLever != 0) {
						if (item.getShopVipPrice().size() != 0) {
							item.setItemShopVipPrice(item.getShopVipPrice().get(shopVipLever).longValue()) ;
							for (int i = 0 ;i < itemStockBos.size(); i++) {
								if (itemStockBos.get(i).getShopVipPrice().size() != 0 ) {
									itemStockBos.get(i).setItemShopVipPrice(itemStockBos.get(i).getShopVipPrice().get(shopVipLever).longValue());
								}
							}
						}
					}
				}
				Log.run.info("resultJson = " + resultJson 
						+ ",colordiscountLevel=" + colordiscountLevel 
						+ ",shopvipdiscountLevel=" + shopvipdiscountLevel
						+ ",userColorLever=" + userColorLever
						+ ",shopVipLever=" + shopVipLever
						+ ",wid=" + wid) ;
			}
			
		} catch (Exception ex) {
			Log.run.info("handlePrice error " + ex.toString() + ",resultStr=" + resultStr) ;
		}		
	}
	
	@Override
	public void handlePrice(ItemBOPrice item, long sellerUin, long wid, String sk) {
		// TODO Auto-generated method stub
		String resultStr = "";
		List<ItemStockBo> itemStockBos = item.getItemStockBo() ;
		try {
			if (wid == sellerUin) {
				item.setIsNotSoldComdy(2) ;
				//item.setStockCount(0) ;
				//item.setCartFlag(0) ;
				//for (int i = 0 ; i < itemStockBos.size(); i++) {
					//itemStockBos.get(i).setStockCount(0) ;
				//}
			}
			StringBuffer getUrl = new StringBuffer();
			String cookie = "wg_uin=" + wid + "; wg_skey=" + sk;   
			getUrl.append(this.USER_BASIC_URL).append("?callback=_callbackCommodityPrice&selleruin=").append(sellerUin);
			resultStr = HttpUtil.get(getUrl.toString().replace(this.USER_BASIC_HOST, PaiPaiConfig.getPaipaiCommonIp()),this.USER_BASIC_HOST, 10000, 15000, "gbk", false,false,cookie,null);
			Log.run.info("get cookie is : " + cookie + "get colorLever url : " + getUrl.toString());
			if (!StringUtil.isEmpty(resultStr) ) {				
				String result = resultStr.substring(resultStr.indexOf("callbackCommodityPrice(") + 23,resultStr.indexOf("})") +1) ;							
				JSONObject resultJson = JSONObject.fromObject(result) ;	
				String colordiscountLevel = "0" ;
				String shopvipdiscountLevel = "0" ;
				int userColorLever = 0 ;
				int shopVipLever = 0 ;
				if (resultJson.getString("errCode").equals("0")) {
					JSONObject json = JSONObject.fromObject(resultJson.getJSONArray("data").get(0)) ;
					colordiscountLevel = json.getString("colordiscountLevel") ;
					shopvipdiscountLevel = json.getString("shopvipdiscountLevel") ;										
					shopVipLever = Integer.parseInt(shopvipdiscountLevel) ;
					userColorLever = Integer.parseInt(colordiscountLevel) ;
					if (userColorLever > 6) {
						userColorLever = 6 ;
					}
					if (userColorLever != 0) {
						if (item.getColorPrice().size() != 0) {
							item.setItemColorPrice(item.getColorPrice().get(userColorLever - 1).longValue()) ;
							for (int i = 0 ;i < itemStockBos.size(); i++) {
								if (itemStockBos.get(i).getColorPrice().size() != 0 ) {
									itemStockBos.get(i).setItemColorPrice(itemStockBos.get(i).getColorPrice().get(userColorLever - 1).longValue());
								}
							}
						}
					} 
					//店铺vip等级下标不减1
					if (shopVipLever != 0) {
						if (item.getShopVipPrice().size() != 0) {
							item.setItemShopVipPrice(item.getShopVipPrice().get(shopVipLever).longValue()) ;
							for (int i = 0 ;i < itemStockBos.size(); i++) {
								if (itemStockBos.get(i).getShopVipPrice().size() != 0 ) {
									itemStockBos.get(i).setItemShopVipPrice(itemStockBos.get(i).getShopVipPrice().get(shopVipLever).longValue());
								}
							}
						}
					}
				}
				Log.run.info("resultJson = " + resultJson 
						+ ",colordiscountLevel=" + colordiscountLevel 
						+ ",shopvipdiscountLevel=" + shopvipdiscountLevel
						+ ",userColorLever=" + userColorLever
						+ ",shopVipLever=" + shopVipLever
						+ ",wid=" + wid) ;
			}
			
		} catch (Exception ex) {
			Log.run.info("handlePrice error " + ex.toString() + ",resultStr=" + resultStr) ;
		}		
	}
	

	@Override
	public void computePrice(ItemBO item) {
		// TODO Auto-generated method stub
		item.setResultPrice(item.getItemPrice()) ;
		item.setResultPriceType(RESULT_PRICE_NORMAL) ;
		if (item.getActivityPrice() != 0 && item.getActivityPrice() <= item.getResultPrice()) {
			item.setResultPrice(item.getActivityPrice()) ;
			item.setResultPriceType(RESULT_PRICE_ACTIVE) ;
		}
		if (item.getItemColorPrice() != 0 && item.getItemColorPrice() <= item.getResultPrice()) {
			item.setResultPrice(item.getItemColorPrice()) ;
			item.setResultPriceType(RESULT_PRICE_COLOR) ;
		}
		if (item.getItemShopVipPrice() != 0 && item.getItemShopVipPrice() <= item.getResultPrice()) {
			item.setResultPrice(item.getItemShopVipPrice()) ;
			item.setResultPriceType(RESULT_PRICE_SHOPVIP) ;
		}
		for (int i = 0 ; i < item.getItemStockBo().size(); i++) {
			ItemStockBo itemStockBo = item.getItemStockBo().get(i) ;
			itemStockBo.setResultPrice(itemStockBo.getStockPrice()) ;
			itemStockBo.setResultPriceType(RESULT_PRICE_NORMAL) ;
			if (itemStockBo.getActivityPrice() != 0 && itemStockBo.getActivityPrice() <= itemStockBo.getResultPrice()) {
				itemStockBo.setResultPrice(itemStockBo.getActivityPrice()) ;
				itemStockBo.setResultPriceType(RESULT_PRICE_ACTIVE) ;
			}
			if (itemStockBo.getItemColorPrice() != 0 && itemStockBo.getItemColorPrice() <= itemStockBo.getResultPrice()) {
				itemStockBo.setResultPrice(itemStockBo.getItemColorPrice()) ;
				itemStockBo.setResultPriceType(RESULT_PRICE_COLOR) ;
			}
			if (itemStockBo.getItemShopVipPrice() != 0 && itemStockBo.getItemShopVipPrice() <= itemStockBo.getResultPrice()) {
				itemStockBo.setResultPrice(itemStockBo.getItemShopVipPrice()) ;
				itemStockBo.setResultPriceType(RESULT_PRICE_SHOPVIP) ;
			}
		}
	}
	
	@Override
	public void computePrice(ItemBOPrice item) {
		// TODO Auto-generated method stub
		item.setResultPrice(item.getItemPrice()) ;
		item.setResultPriceType(RESULT_PRICE_NORMAL) ;
		if (item.getActivityPrice() != 0 && item.getActivityPrice() <= item.getResultPrice()) {
			item.setResultPrice(item.getActivityPrice()) ;
			item.setResultPriceType(RESULT_PRICE_ACTIVE) ;
		}
		if (item.getItemColorPrice() != 0 && item.getItemColorPrice() <= item.getResultPrice()) {
			item.setResultPrice(item.getItemColorPrice()) ;
			item.setResultPriceType(RESULT_PRICE_COLOR) ;
		}
		if (item.getItemShopVipPrice() != 0 && item.getItemShopVipPrice() <= item.getResultPrice()) {
			item.setResultPrice(item.getItemShopVipPrice()) ;
			item.setResultPriceType(RESULT_PRICE_SHOPVIP) ;
		}
		for (int i = 0 ; i < item.getItemStockBo().size(); i++) {
			ItemStockBo itemStockBo = item.getItemStockBo().get(i) ;
			itemStockBo.setResultPrice(itemStockBo.getStockPrice()) ;
			itemStockBo.setResultPriceType(RESULT_PRICE_NORMAL) ;
			if (itemStockBo.getActivityPrice() != 0 && itemStockBo.getActivityPrice() <= itemStockBo.getResultPrice()) {
				itemStockBo.setResultPrice(itemStockBo.getActivityPrice()) ;
				itemStockBo.setResultPriceType(RESULT_PRICE_ACTIVE) ;
			}
			if (itemStockBo.getItemColorPrice() != 0 && itemStockBo.getItemColorPrice() <= itemStockBo.getResultPrice()) {
				itemStockBo.setResultPrice(itemStockBo.getItemColorPrice()) ;
				itemStockBo.setResultPriceType(RESULT_PRICE_COLOR) ;
			}
			if (itemStockBo.getItemShopVipPrice() != 0 && itemStockBo.getItemShopVipPrice() <= itemStockBo.getResultPrice()) {
				itemStockBo.setResultPrice(itemStockBo.getItemShopVipPrice()) ;
				itemStockBo.setResultPriceType(RESULT_PRICE_SHOPVIP) ;
			}
		}
	}

	/**
	 * 使用cgi接口获取商品微店价格
	 */
	@Override
	public void wxPrice(ItemBO item, Long sellerUin, String ic) {
		// TODO Auto-generated method stub
		String resultStr = "";
		StringBuffer getUrl = new StringBuffer();
		getUrl.append(this.WEIDIAN_PRICE_URL).append("?type=0&abletype=15&uin=").append(sellerUin).append("&itemid=").append(ic);
		try {
			resultStr = HttpUtil.get(getUrl.toString(),null, 10000, 15000, "gbk", true,false,"",null);
			if (!StringUtil.isEmpty(resultStr) ) {				
				String result = resultStr.substring(resultStr.indexOf("wxPriceQueryCallBack(") + 21, resultStr.indexOf("})") + 1);
				JSONObject resultJson = JSONObject.fromObject(result);	
				if(null != resultJson && !resultJson.isNullObject()){
					long resultPrice = item.getResultPrice();
					int wdpricestate = resultJson.getInt("wdpricestate");
					int wdpricechange = resultJson.getInt("wdpricechange");
					int wdpricevalue = resultJson.getInt("wdpricevalue");
					if(wdpricestate == 0 && wdpricevalue > 0){//微店折扣生效
						resultPrice = (resultPrice*wdpricevalue)/1000;
						if(wdpricechange == 1){//1、抹掉分
							resultPrice = resultPrice /10 * 10 ;
							/*if(resultPrice >= 10){
								int jue = getNumberAtLast(resultPrice,2);
								resultPrice = resultPrice - jue*10;
							}*/
						}else if(wdpricechange == 2){//2、抹掉角
							/*int fen = getNumberAtLast(resultPrice,1);
							resultPrice = resultPrice - fen;*/
							resultPrice = resultPrice /100 * 100 ;
						}
						item.setResultPrice((resultPrice <= 0)? 1 : resultPrice);
						item.setResultPriceType(RESULT_PRICE_WEIDIAN);
						for (int i = 0 ; i < item.getItemStockBo().size(); i++) { 
							ItemStockBo itemStockBo = item.getItemStockBo().get(i) ;
							Long resultSocketPrice = itemStockBo.getResultPrice() ;
							resultSocketPrice = (resultSocketPrice*wdpricevalue)/1000;
							if(wdpricechange == 1){//1、抹掉分
								resultSocketPrice = resultSocketPrice / 10 * 10 ;
								/*if(resultSocketPrice >= 10){
									int jue = getNumberAtLast(resultSocketPrice,2);
									resultSocketPrice = resultSocketPrice - jue*10;
								}*/
							}else if(wdpricechange == 2){//2、抹掉分
								resultSocketPrice = resultSocketPrice / 100 * 100 ;
								/*int fen = getNumberAtLast(resultSocketPrice,1);
								resultSocketPrice = resultSocketPrice - fen;*/
							}
							itemStockBo.setResultPrice((resultSocketPrice <= 0)? 1 : resultSocketPrice);
							itemStockBo.setResultPriceType(RESULT_PRICE_WEIDIAN);
						}
					}
				}
			}
		} catch (Exception ex) {
			Log.run.info("handlePrice error " + ex.toString() + ",resultStr=" + resultStr) ;
		}		
	}
	
	/**
	 * 使用cgi接口获取商品微店价格
	 */
	@Override
	public void wxPrice(ItemBOPrice item, Long sellerUin, String ic) {
		// TODO Auto-generated method stub
		String resultStr = "";
		StringBuffer getUrl = new StringBuffer();
		getUrl.append(this.WEIDIAN_PRICE_URL).append("?type=0&abletype=15&uin=").append(sellerUin).append("&itemid=").append(ic);
		try {
			resultStr = HttpUtil.get(getUrl.toString().replace(this.WEIDIAN_PRICE_HOST, PaiPaiConfig.getPaipaiCommonIp()),this.WEIDIAN_PRICE_HOST, 10000, 15000, "gbk", false,false,"",null);
			if (!StringUtil.isEmpty(resultStr) ) {				
				String result = resultStr.substring(resultStr.indexOf("wxPriceQueryCallBack(") + 21, resultStr.indexOf("})") + 1);
				JSONObject resultJson = JSONObject.fromObject(result);	
				if(null != resultJson && !resultJson.isNullObject()){
					long resultPrice = item.getResultPrice();
					int wdpricestate = resultJson.getInt("wdpricestate");
					int wdpricechange = resultJson.getInt("wdpricechange");
					int wdpricevalue = resultJson.getInt("wdpricevalue");
					if(wdpricestate == 0 && wdpricevalue > 0){//微店折扣生效
						resultPrice = (resultPrice*wdpricevalue)/1000;
						if(wdpricechange == 1){
							resultPrice = resultPrice /10 * 10 ;
							/*if(resultPrice >= 10){
								int jue = getNumberAtLast(resultPrice,2);
								resultPrice = resultPrice - jue*10;
							}*/
						}else if(wdpricechange == 2){
							resultPrice = resultPrice /100 * 100 ;
							/*int fen = getNumberAtLast(resultPrice,1);
							resultPrice = resultPrice - fen;*/
						}
						item.setResultPrice((resultPrice <= 0)? 1 : resultPrice);
						item.setResultPriceType(RESULT_PRICE_WEIDIAN);
						for (int i = 0 ; i < item.getItemStockBo().size(); i++) { 
							ItemStockBo itemStockBo = item.getItemStockBo().get(i) ;
							Long resultSocketPrice = itemStockBo.getResultPrice() ;
							resultSocketPrice = (resultSocketPrice*wdpricevalue)/1000;
							if(wdpricechange == 1){
								resultSocketPrice = resultSocketPrice / 10 * 10 ;
								/*if(resultSocketPrice >= 10){
									int jue = getNumberAtLast(resultSocketPrice,2);
									resultSocketPrice = resultSocketPrice - jue*10;
								}*/
							}else if(wdpricechange == 2){
								resultSocketPrice = resultSocketPrice / 100 * 100 ;
								/*int fen = getNumberAtLast(resultSocketPrice,1);
								resultSocketPrice = resultSocketPrice - fen;*/
							}
							itemStockBo.setResultPrice((resultSocketPrice <= 0)? 1 : resultSocketPrice);
							itemStockBo.setResultPriceType(RESULT_PRICE_WEIDIAN);
						}
					}
				}
			}
		} catch (Exception ex) {
			Log.run.info("handlePrice error " + ex.toString() + ",resultStr=" + resultStr) ;
		}		
	}
	
	
	 /**
     * 根据商品id以及当前用户的QQ号查出该人是否收藏了该商品
     * @param itemCode
     * @param qq
     * @return
     */
	@Override
	public boolean getIsFavorite(String itemCode, long qq,String clientIp, String sk, String reserve) {
		boolean isFavorite =false;
		if(ItemUtil.isValidItemCode(itemCode)&&qq>10000){
			IsFavoritedResp resp = ItemClient.getIsFavoritedDetail(itemCode,qq,clientIp,sk,reserve);
			if(resp!=null){
				isFavorite= resp.getBFavorited();
			}
		}
		return isFavorite;
	}

	/**
	 * @param id 商品id
	 * @param flag 是否登陆
	 * @param wid qq号
	 * @param sk
	 * @return
	 */
	@Override
	public ItemBO exclusiveItem(String id, boolean flag, Long wid, String sk) {

		ItemBO ret = new ItemBO();
		if (StringUtil.isNotBlank(id)) {
			FetchItemInfoResp resp = ItemClient.fetchItemInfo(id);
			ItemPo_v2 itemPo = null;
			ItemBase oItemBase = null;
			ItemImg oItemImg = null;
			if (resp != null && resp.getResult() == 0 && (itemPo = resp.getItemPo()) != null
					&& (oItemBase = itemPo.getOItemBase()) != null) {
				oItemImg = itemPo.getOItemImg();//图片信息
				// 0、 促销价等
				ItemPriceExt oItemPriceExt = itemPo.getOItemPriceExt();
				if (oItemPriceExt != null) {
					ret.setActivityDes(oItemPriceExt.getActivityDesc()) ;
					ret.setColorPrice(oItemPriceExt.getColorPrice());
					ret.setActivityPrice(oItemPriceExt.getActivityPrice());
					ret.setShopVipPrice(oItemPriceExt.getShopVipPrice());
				}
				// 4、处理图片信息
				Vector<String> vecImgTmp = null, vecImg = null;
				int size = 0;
				if (oItemImg != null && (vecImgTmp = oItemImg.getVecImg()) != null && !vecImgTmp.isEmpty()) {
					size = vecImgTmp.size();
					vecImg = new Vector<String>(size);
					for (String iamgeUrl : vecImgTmp) {
						// 跳转商详图片url至400X400
						vecImg.add(genImageUrl(iamgeUrl, imageFormat400));
					}
					if (size > 0)
						ret.setPicLink(vecImg.get(0));
				}
				ret.setItemCode(oItemBase.getItemId());
				ret.setItemName(oItemBase.getTitle());
				ret.setItemPrice(oItemBase.getPrice());
				ret.setSellerUin(oItemBase.getQqUin());
			}
		}
		//用户登陆计算vip价钱
		if(flag){
			String resultStr = "";
			try {
				StringBuffer getUrl = new StringBuffer();
				String cookie = "wg_uin=" + wid + "; wg_skey=" + sk;
				getUrl.append(this.USER_BASIC_URL).append("?callback=_callbackCommodityPrice&selleruin=").append(ret.getSellerUin());
				resultStr = HttpUtil.get(getUrl.toString(),null, 10000, 15000, "gbk", true,false,cookie,null);
				Log.run.info("get cookie is : " + cookie + "get colorLever url : " + getUrl.toString());
				if (!StringUtil.isEmpty(resultStr) ) {
					String result = resultStr.substring(resultStr.indexOf("callbackCommodityPrice(") + 23,resultStr.indexOf("})") +1) ;
					JSONObject resultJson = JSONObject.fromObject(result) ;
					String colordiscountLevel = "0" ;
					String shopvipdiscountLevel = "0" ;
					int userColorLever = 0 ;
					int shopVipLever = 0 ;
					if (resultJson.getString("errCode").equals("0")) {
						JSONObject json = JSONObject.fromObject(resultJson.getJSONArray("data").get(0)) ;
						colordiscountLevel = json.getString("colordiscountLevel") ;
						shopvipdiscountLevel = json.getString("shopvipdiscountLevel") ;
						shopVipLever = Integer.parseInt(shopvipdiscountLevel) ;
						userColorLever = Integer.parseInt(colordiscountLevel) ;
						if (userColorLever > 6) {
							userColorLever = 6 ;
						}
						if (userColorLever != 0) {
							if (ret.getColorPrice().size() != 0) {
								ret.setItemColorPrice(ret.getColorPrice().get(userColorLever - 1).longValue()) ;
							}
						}
						//店铺vip等级下标不减1
						if (shopVipLever != 0) {
							if (ret.getShopVipPrice().size() != 0) {
								ret.setItemShopVipPrice(ret.getShopVipPrice().get(shopVipLever).longValue()) ;
							}
						}
					}
				}
			} catch (Exception ex) {
				Log.run.info("handlePrice error " + ex.toString() + ",resultStr=" + resultStr) ;
			}
		}
		return ret;
	}
	
	
	/**
	 * @param id 商品id
	 * @param flag 是否登陆
	 * @param wid qq号
	 * @param sk
	 * @return
	 */
	@Deprecated
	@Override
	public ItemBO exclusiveItemSlow(String id, boolean flag, Long wid, String sk) {

		ItemBO ret = new ItemBO();
		if (StringUtil.isNotBlank(id)) {
			FetchItemInfoResp resp = ItemClient.fetchItemInfo(id);
			ItemPo_v2 itemPo = null;
			ItemBase oItemBase = null;
			ItemImg oItemImg = null;
			if (resp != null && resp.getResult() == 0 && (itemPo = resp.getItemPo()) != null
					&& (oItemBase = itemPo.getOItemBase()) != null) {
				
				if(!(itemPo.getOItemBase().getState()  == 2 || itemPo.getOItemBase().getState()  == 7 || itemPo.getOItemBase().getState()  == 64)){
					Log.run.debug(oItemBase.getItemId() + "  不是正常在售商品");
					return null;
				}
				
				oItemImg = itemPo.getOItemImg();//图片信息
				// 0、 促销价等
				ItemPriceExt oItemPriceExt = itemPo.getOItemPriceExt();
				if (oItemPriceExt != null) {
					ret.setActivityDes(oItemPriceExt.getActivityDesc()) ;
					ret.setColorPrice(oItemPriceExt.getColorPrice());
					ret.setActivityPrice(oItemPriceExt.getActivityPrice());
					ret.setShopVipPrice(oItemPriceExt.getShopVipPrice());
				}
				// 4、处理图片信息
				Vector<String> vecImgTmp = null, vecImg = null;
				int size = 0;
				if (oItemImg != null && (vecImgTmp = oItemImg.getVecImg()) != null && !vecImgTmp.isEmpty()) {
					size = vecImgTmp.size();
					vecImg = new Vector<String>(size);
					for (String iamgeUrl : vecImgTmp) {
						// 跳转商详图片url至400X400
						vecImg.add(genImageUrl(iamgeUrl, imageFormat400));
					}
					if (size > 0)
						ret.setPicLink(vecImg.get(0));
				}
				ret.setItemCode(oItemBase.getItemId());
				ret.setItemName(oItemBase.getTitle());
				ret.setItemPrice(oItemBase.getPrice());
				ret.setSellerUin(oItemBase.getQqUin());
			}
		}
		//用户登陆计算vip价钱
		if(flag){
			String resultStr = "";
			try {
				StringBuffer getUrl = new StringBuffer();
				String cookie = "wg_uin=" + wid + "; wg_skey=" + sk;
				getUrl.append(this.USER_BASIC_URL).append("?callback=_callbackCommodityPrice&selleruin=").append(ret.getSellerUin());
				resultStr = HttpUtil.get(getUrl.toString(),null, 10000, 15000, "gbk", true,false,cookie,null);
				Log.run.info("get cookie is : " + cookie + "get colorLever url : " + getUrl.toString());
				if (!StringUtil.isEmpty(resultStr) ) {
					String result = resultStr.substring(resultStr.indexOf("callbackCommodityPrice(") + 23,resultStr.indexOf("})") +1) ;
					JSONObject resultJson = JSONObject.fromObject(result) ;
					String colordiscountLevel = "0" ;
					String shopvipdiscountLevel = "0" ;
					int userColorLever = 0 ;
					int shopVipLever = 0 ;
					if (resultJson.getString("errCode").equals("0")) {
						JSONObject json = JSONObject.fromObject(resultJson.getJSONArray("data").get(0)) ;
						colordiscountLevel = json.getString("colordiscountLevel") ;
						shopvipdiscountLevel = json.getString("shopvipdiscountLevel") ;
						shopVipLever = Integer.parseInt(shopvipdiscountLevel) ;
						userColorLever = Integer.parseInt(colordiscountLevel) ;
						if (userColorLever > 6) {
							userColorLever = 6 ;
						}
						if (userColorLever != 0) {
							if (ret.getColorPrice().size() != 0) {
								ret.setItemColorPrice(ret.getColorPrice().get(userColorLever - 1).longValue()) ;
							}
						}
						//店铺vip等级下标不减1
						if (shopVipLever != 0) {
							if (ret.getShopVipPrice().size() != 0) {
								ret.setItemShopVipPrice(ret.getShopVipPrice().get(shopVipLever).longValue()) ;
							}
						}
					}
				}
			} catch (Exception ex) {
				Log.run.info("handlePrice error " + ex.toString() + ",resultStr=" + resultStr) ;
			}
		}
		return ret;
	}

	/**
	 * 获取app专享商品信息列表并使用redis缓存
	 *
	 * @param id
	 * @param flag
	 * @param wid
	 * @param sk
	 * @return
	 */
	@Override
	public JsonObject exclusiveItemAndCache(final String id, final boolean flag, final Long wid, final String sk) {
		String jsonStr = (String) cacheClient.getAndSetNx(CacheKey.手机专享商品 + id, 3600*24, new CacheCallback() {
			@Override
			public Object execute(CacheClient cacheClient) {
				String itemStr = null;
				try {
					ItemBO item = exclusiveItem(id,flag,wid,sk);
					long sellerUin = item.getSellerUin();
					long computePrice = item.getItemPrice();
					Log.run.info("computer price is itemPrice : " + computePrice);
					if (item.getActivityPrice() != 0 && item.getActivityPrice() <= computePrice) {
                        computePrice = item.getActivityPrice();
                        Log.run.info("computer price is activityPrice : " + computePrice);
                    }
					if (item.getItemColorPrice() != 0 && item.getItemColorPrice() <= computePrice) {
                        computePrice = item.getItemColorPrice();
                        Log.run.info("computer price is colorPrice : " + computePrice);
                    }
					if (item.getItemShopVipPrice() != 0 && item.getItemShopVipPrice() <= computePrice) {
                        computePrice = item.getItemShopVipPrice();
                        Log.run.info("computer price is vipPrice : " + computePrice);
                    }
					item.setResultPrice(computePrice);
					wxPrice(item, sellerUin, id);
					long exclusivePrice = item.getResultPrice();
					long savePrice = computePrice - exclusivePrice;
					JsonObject json = new JsonObject();
					json.addProperty("itemCode", item.getItemCode());
					json.addProperty("picLink", item.getPicLink());
					json.addProperty("title", item.getItemName());
					json.addProperty("exclusivePrice", exclusivePrice < 0 ? 0 : exclusivePrice);
					json.addProperty("savePrice", savePrice < 0 ? 0 : savePrice);
					Log.run.info("app item json :" + json);
					itemStr = new GsonBuilder().serializeNulls().create().toJson(json);
					Log.run.info("app item str :" + itemStr);
				} catch (Exception e) {
					Log.run.error("exclusiveItemAndCache error : " + e);
				}
				return itemStr;
			}
		});
		Log.run.info("exclusiveItemAndCache:" + jsonStr);
		JsonParser parser = new JsonParser();
		JsonObject item = (JsonObject) parser.parse(jsonStr);
		return item;
	}

	/**
	 * 根据商品ID获取商品基本信息
	 * @param itemCode
	 * @return
	 */
	@Override
	public JsonObject getItemBaseInfoByItemCode(String itemCode) {
		JsonObject json = null;
		if (StringUtil.isNotBlank(itemCode)) {	
			FetchItemInfoResp resp = ItemClient.fetchItemInfo(itemCode);
			ItemPo_v2 itemPo = null;
			ItemBase oItemBase = null;
			ItemImg oItemImg = null;
			if (resp != null && resp.getResult() == 0
					&& (itemPo = resp.getItemPo()) != null
					&& (oItemBase = itemPo.getOItemBase()) != null
					&& StringUtil.isNotEmpty(oItemBase.getTitle())
					&& oItemBase.getPrice()>0 &&
					(oItemBase.getState()==2 || oItemBase.getState()==7 || oItemBase.getState()==64)) {
				json = new JsonObject();
				String imgUrl = "";
				json.addProperty("title",oItemBase.getTitle());
				json.addProperty("price", oItemBase.getPrice());
				//处理图片信息
				oItemImg = itemPo.getOItemImg();//图片信息
				Vector<String> vecImgTmp = null, vecImg = null;
				int size = 0;
				if (oItemImg != null && (vecImgTmp = oItemImg.getVecImg()) != null && !vecImgTmp.isEmpty()) {
					size = vecImgTmp.size();
					vecImg = new Vector<String>(size);
					for (String iamgeUrl : vecImgTmp) {
						// 跳转商详图片url至400X400
						vecImg.add(genImageUrl(iamgeUrl, imageFormat400));
					}
					if (size > 0)
						imgUrl = vecImg.get(0);
				}
				
				json.addProperty("imgUrl", imgUrl);
				json.addProperty("itemUrl", ITEM_URL_PREFIZ + oItemBase.getItemId());
				json.addProperty("itemCode", itemCode);
			}
		}
		return json;
	}
	
	
	@Override
	@Deprecated
	public JsonObject getItemBaseInfoByItemCodeSlow(String itemCode, boolean flag, Long wid, String sk) {
		JsonObject json = null;
		if (StringUtil.isNotBlank(itemCode)) {	
			ItemBO item = exclusiveItemSlow(itemCode,flag,wid,sk);
			
			if(null != item && StringUtil.isNotBlank(item.getItemName()) && item.getItemPrice() > 0){
				long sellerUin = item.getSellerUin();
				
				
				long computePrice = item.getItemPrice();
				Log.run.info("computer price is itemPrice : " + computePrice);
				if (item.getActivityPrice() != 0 && item.getActivityPrice() <= computePrice) {
                    computePrice = item.getActivityPrice();
                    Log.run.info("computer price is activityPrice : " + computePrice);
                }
				if (item.getItemColorPrice() != 0 && item.getItemColorPrice() <= computePrice) {
                    computePrice = item.getItemColorPrice();
                    Log.run.info("computer price is colorPrice : " + computePrice);
                }
				if (item.getItemShopVipPrice() != 0 && item.getItemShopVipPrice() <= computePrice) {
                    computePrice = item.getItemShopVipPrice();
                    Log.run.info("computer price is vipPrice : " + computePrice);
                }
				item.setResultPrice(computePrice);
				
				
				wxPrice(item, sellerUin, itemCode);
				
				json = new JsonObject();
				json.addProperty("title",item.getItemName());
				json.addProperty("itemPrice", item.getItemPrice());
				json.addProperty("resultPrice", item.getResultPrice());
				json.addProperty("itemCode", itemCode);
				json.addProperty("itemUrl", ITEM_URL_PREFIZ + item.getItemCode());
				json.addProperty("imgUrl", item.getPicLink());
			}
		}
		return json;
	}

	/**
	 * 获取整数倒数第几位的数字
	 * @param number
	 * @param index
	 * @return
	 */
	public static int getNumberAtLast(long number, int index) {  
		for (; index > 1; index--) {  
			number /= 10;  
		}  
		return (int) (number % 10);  
	} 
	
	
	/*public static void main (String[] agrs) {
		StringBuffer getUrl = new StringBuffer();
		String cookie = "wg_skey=za73D78454; wg_uin=2097792742";   
		getUrl.append("http://bases.paipai.com/buyer/buyerdiscount?callback=_callbackCommodityPrice&selleruin=").append("1275000334");
		String resultStr = HttpUtil.get(getUrl.toString(),null, 10000, 15000, "gbk", false,false,cookie,null);
		System.out.println(resultStr);
		System.out.println(resultStr.indexOf("callbackCommodityPrice("));
		System.out.println(resultStr.indexOf("})"));
		String result = resultStr.substring(resultStr.indexOf("callbackCommodityPrice(") + 23,resultStr.indexOf("})") +1) ;						
		System.out.println(result);
		JSONObject resultJson = JSONObject.fromObject(result) ;
		if (resultJson.getString("errCode").equals("0")) {
			JSONObject json = JSONObject.fromObject(resultJson.getJSONArray("data").get(0)) ;
			String colordiscountLevel = json.getString("colordiscountLevel") ;
			String shopvipdiscountLevel = json.getString("shopvipdiscountLevel") ;
			//List<ItemStockBo> itemStockBos = item.getItemStockBo() ;
			int shopVipLever = Integer.parseInt(shopvipdiscountLevel) ;
			System.out.println(colordiscountLevel + "---------------" + shopvipdiscountLevel + "==================" + shopVipLever);
		}
		System.out.println(resultStr);
	}*/
	public static void main(String[] args) {
		/*"mapExt": {
			"3005": "颜色:宝蓝色|item-54363AE1-0E0A632F00000000040100003F8CB34A.9.jpg;颜色:浅紫色|item-54363AE1-0E0A632F00000001040100003F8CB34A.9.jpg;颜色:深紫色|item-54363AE1-0E0A632F00000002040100003F8CB34A.9.jpg;颜色:紫色|item-54363AE1-0E0A632F00000003040100003F8CB34A.9.jpg",
			"3001": "item-54363AE1-0E0A632F00000000040100003F8CB34A.1.jpg",
			"3002": "item-54363AE1-0E0A632F00000000040100003F8CB34A.2.jpg"
		},*/
		Map<uint32_t, String> picMap = new HashMap<uint32_t,String>();
		picMap.put(new uint32_t(3005), "颜色:宝蓝色|item-54363AE1-0E0A632F00000001040100003F8CB34A.9.jpg;颜色:浅紫色|item-54363AE1-0E0A632F00000001040100003F8CB34A.9.jpg;颜色:深紫色|item-54363AE1-0E0A632F00000002040100003F8CB34A.9.jpg;颜色:紫色|item-54363AE1-0E0A632F00000003040100003F8CB34A.9.jpg");
		picMap.put(new uint32_t(3001), "item-54363AE1-0E0A632F00000000040100003F8CB34A.1.jpg");
		picMap.put(new uint32_t(3002), "item-54363AE1-0E0A632F00000000040100003F8CB34A.2.jpg");
		Map<String,String> picUrl = new HashMap<String,String>();
		if(null != picMap && !picMap.isEmpty()){
			String extStr = picMap.get(new uint32_t(3005));
			if(null != extStr && !"".equals(extStr.trim())){
				String[] split = extStr.split(";");
				for(String str : split){
					String[] strs = str.split("\\|");
					if(str.startsWith("颜色") && strs.length == 2){
						String url = "http://img6.paipaiimg.com/" + strs[1];
						picUrl.put(strs[0], url);
					}
				}
			}
		}
		//当颜色对应的有图片信息时，在颜色属性后面追加图片URL add by wanghao start
		String attr = "颜色:红色|尺码:30";
		if(null != attr && !"".equals(attr.trim())){
			String[] split = attr.split("\\|");
			for(String str : split){
				if(null != str && !"".equals(str.trim()) && str.startsWith("颜色")){
					String url = picUrl.get(str);
					url = (null != url)? url : "";
					System.out.println(url);
				}
			}
		}
		Map<String, String> map = new HashMap<String,String>();
		map.put("ddd", "as");
		System.out.println(map.get("ddd"));
	}

	public void setCacheClient(CacheClient cacheClient) {
		this.cacheClient = cacheClient;
	}
}
