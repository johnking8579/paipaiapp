package com.qq.qqbuy.item.biz;

import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.item.po.ItemInfoWithPrice;
import com.qq.qqbuy.item.po.MItemExtInfo4J;
import com.qq.qqbuy.item.po.eval.EvalPo;
import com.qq.qqbuy.item.po.wap2.ItemResponse;

/**
 * 手机拍拍的商品信息BIZ
 * @version 1.0
 * @author jiaqiangxu
 *
 */
public interface ItemBiz {
	
	/**
	 * 获取手机拍拍商品信息(含价格和折扣),为经过计算后的在手机拍拍销售的价格，仅当需要价格时调用此接口
	 * @param itemCode 商品ID
	 * @return ItemInfoWithPrice 含价格的商品基本信息
	 */
	ItemInfoWithPrice getItemFromQgo(String itemCode)throws BusinessException;
	/**
	 * 查询手机拍拍商品的扩展信息，包括图片和关联商品
	 * @param itemCode 商品ID
	 * @return
	 * @throws Exception
	 */
	MItemExtInfo4J getItemExtInfo(String itemCode) throws BusinessException;
	/**
	 * 查询拍拍商品的评论
	 * @param itemCode 商品ID
	 * @param sellerUin 商品卖家
	 * @param pn 页码，从1开始
	 * @param ps 页大小
	 * @param needReply 是否需要回复,1为需要,0不需要
	 * @param needHistory 是否需要历史信息，1为需要,0不需要
	 * @param evalLevel 评价级别，0-5依次增加
	 * @return
	 * @throws Exception
	 */
	EvalPo getItemEval(String itemCode,
			long sellerUin,int pn,int ps,
			int needReply,int needHistory,int evalLevel) throws BusinessException;
	/**
	 * 查询拍拍商品的评论
	 * @param itemCode 商品ID
	 * @param sellerUin 商品卖家
	 * @param pn 页码，从1开始
	 * @param ps 从多少中挑选一条最优
	 * @param needReply 是否需要回复,1为需要,0不需要
	 * @param needHistory 是否需要历史信息，1为需要,0不需要
	 * @param evalLevel 评价级别，0-5依次增加
	 * @return
	 * @throws Exception
	 */
	EvalPo getItemEvalFroBest(String itemCode,
					   long sellerUin,int pn,int ps,
					   int needReply,int needHistory,int evalLevel) throws BusinessException;
	/**
	 * 查询商品信息,可选择是否拉取手拍信息
	 * @param itemCode
	 * @param needParseAttr 仅商详页查询时true，下单查商品时请传false
	 * @param needDetailInfo 仅商详页查询时true，下单查商品时请传false
	 * @param needExtendInfo  
	 * @param needQgoItemInfo 是否需要拉手拍信息
	 * @return
	 * @throws BusinessException
	 * @date:2013-4-9
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
    ItemResponse getItemFromPaipai2(String itemCode,boolean needParseAttr, 
    		boolean needDetailInfo, boolean needExtendInfo,boolean needQgoItemInfo) throws BusinessException;

	  
}
