package com.qq.qqbuy.item.biz;

import java.util.List;
import java.util.Vector;

import com.google.gson.JsonObject;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.item.po.*;

public interface PPItemBiz {

    
    /**
     * 从拍拍侧获取商品的基本信息。
     * 取商品非价格信息都调此接口
     * @param itemCode 商品ID
     * @param needParseAttr 是否需要解析属性
     * @param needExtendInfo 是否需要扩展信息 
     * @return GetItemResponse 商品信息
     */
    ItemBO getItem(String itemCode,boolean needParseAttr, boolean needExtendInfo) throws BusinessException;
    /**
     * 获得商品图文信息
     * @param itemCode
     * @return
     * @throws BusinessException
     */
    String getItemDetail(String itemCode) throws BusinessException;
    /**
     * 从拍拍侧批量获取商品的基本信息
     * @param itemCodeList 商品ID列表
     * @return Vector<ItemBO> 商品信息列表
     */
    List<ItemBO> getItemList(Vector<String> itemCodeList) throws BusinessException;
    
    
    /**
     * @Title: fetchItemInfo 
     * 
     * add by wendyhu
     * 
     * @Description:  商品详情新接口
     * @param itemCode 商品id
     * @param needParseAttr 是否需要解析属性
     * @return ItemBO    返回类型 
     * @throws
     */
    ItemBO fetchItemInfo(String itemCode,  boolean needParseAttr);
    
    /**
     * @Title: getItemBasic 
     * 
     * add by huayu
     * 
     * @Description:  商品详情新接口
     * @param itemCode 商品id
     * @return ItemBO    返回类型 
     * @throws
     */
    ItemBOExtInfo getItemExtInfo(String itemCode);
    /**
     * @Title: getItemBasic 
     * 
     * add by huayu
     * 
     * @Description:  商品详情新接口
     * @param itemCode 商品id
     * @return ItemBO    返回类型 
     * @throws
     */
    ItemBOBasic getItemBasic(String itemCode);
    
    /**
     * @Title: getItemBasic 
     * 
     * add by huayu
     * 
     * @Description:  商品详情新接口
     * @param itemCode 商品id
     * @return ItemBO    返回类型 
     * @throws
     */
    ItemBOPrice getItemPrice(String itemCode);
    
    /**
     * 
     * @Title: fetchCmtShopInfo4Item
     *
     * @Description: 商详接口返回内容中附加上店铺信息和评论信息
     * @param @param itemCode
     * @param @param item  设定文件
     * @param mk 手机唯一标识
     * @return void  返回类型
     * @throws
     */
    void fetchCmtShopInfo4Item(String itemCode, ItemBO item, String mk);
    
    /**
     * 
     * @Title: fetchCmtShopInfo4Item
     *
     * @Description: 商详接口返回内容中附加上店铺信息和评论信息
     * @param @param itemCode
     * @param @param item  设定文件
     * @param mk 手机唯一标识
     * @return void  返回类型
     * @throws
     */
    void fetchCmtShopInfo4Item(String itemCode, ItemBOExtInfo item, String mk);
    /**
     * 
     * @param sellerUin 卖家UIN
     * @return 店铺活动信息
     */
    String getCurrentActive(Long sellerUin) ;
    /**
     * 用户登陆，计算登陆后的彩钻和VIP价格
     * @param item
     * @param sellerUin
     * @param wid
     * @param sk
     */
    void handlePrice (ItemBO item, long sellerUin, long wid , String sk) ;
    
    /**
     * 用户登陆，计算登陆后的彩钻和VIP价格
     * @param item
     * @param sellerUin
     * @param wid
     * @param sk
     */
    void handlePrice (ItemBOPrice item, long sellerUin, long wid , String sk) ;
    /**
     * 计算最终价格
     * @param item
     */
    void computePrice (ItemBO item) ;
    /**
     * 计算最终价格
     * @param item
     */
    void computePrice (ItemBOPrice item) ;
    /**
     * 计算微店价格
     * @param item 
     * @param sellerUin
     * @param ic
     */
    void wxPrice(ItemBO item, Long sellerUin, String ic);
    /**
     * 计算微店价格
     * @param item 
     * @param sellerUin
     * @param ic
     */
    void wxPrice(ItemBOPrice item, Long sellerUin, String ic);
    /**
     * 根据商品id以及当前用户的QQ号查出该人是否收藏了该商品
     * @param ic
     * @param qq
     * @param pprdP 
     * @param string2 
     * @param string 
     * @return
     */
	boolean getIsFavorite(String ic, long qq,String clientIp, String sk, String reserve);

    /**
     * 获取app专享商品信息
     * @param id 商品id
     * @param flag 是否登陆
     * @param wid qq号
     * @param sk
     */
    ItemBO exclusiveItem(String id, boolean flag, Long wid, String sk);
    
    /**
     * 获取app专享商品信息
     * @param id 商品id
     * @param flag 是否登陆
     * @param wid qq号
     * @param sk
     */
    ItemBO exclusiveItemSlow(String id, boolean flag, Long wid, String sk);

    /**
     * 获取app专享商品信息列表并使用redis缓存
     * @param id
     * @param flag
     * @param wid
     * @param sk
     * @return
     */
    JsonObject exclusiveItemAndCache(String id, boolean flag, Long wid, String sk);

    /**
     * 根据商品ID获取商品基本信息
     * @param itemCode
     * @return
     */
    JsonObject getItemBaseInfoByItemCode(String itemCode);
    
    /**
     * 根据商品ID获取商品基本信息
     * 计算微店价，速度比较慢，不建议使用
     * @param itemCode
     * @return
     */
    @Deprecated
    JsonObject getItemBaseInfoByItemCodeSlow(String itemCode, boolean flag, Long wid, String sk);
}
