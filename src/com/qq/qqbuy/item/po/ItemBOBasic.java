package com.qq.qqbuy.item.po;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.qq.qqbuy.item.constant.ItemExtend;
import com.qq.qqbuy.item.constant.ItemState;

public class ItemBOBasic {
	
	private Long leafClassId ;	
	/**
	 * 商品编码，商品在拍拍上标识的唯一编码
	 */
	private String itemCode;
	/**
	 * 商品链接地址
	 */
	private String itemUrl;
	/**
	 * 商品的微店商品地址
	 */
	private String itemWeiDian ;
	/**
	 * 商品所属店铺的微店地址
	 */
	private String shopWeiDian ;

	/**
	 * 商品名称
	 */
	private String itemName;

	/**
	 * 商家对商品的编码，商家自行保证该编码的唯一性， 否则根据该编码查询可能出错。
	 */
	private String itemLocalCode;

	/**
	 * 商品状态,所有状态代码及含义请看本文档页后说明
	 */
	private ItemState itemState = ItemState.getItemState(2);

	/**
	 * 商品状态的说明
	 */
	private String stateDesc;

	/**
	 * 商品的运费模板id
	 */
	private long freightId;

	/**
	 * 货到付款运费模板id,为0表示没有货到付款运费模板id，非0表示存在
	 */
	private long codFreightId;	

	/**
	 * 卖家昵称
	 */
	private String sellerName;

	
	/**
	 * 商品最后上架
	 */
	private long beginTime;
	

	/**
	 * 商品结束时间
	 */
	private long endTime;

	/**
	 * 商品的种类id（店铺自定义分类）
	 */
	private String categoryId;

	/**
	 * 商品的类目id
	 */
	private long classId;

	/**
	 * 商品库存总数量
	 */
	//private long stockCount;
	
	/**
	 * 商品的邮寄费用
	 */
	private long mailPrice;

	/**
	 * 商品的快递费用
	 */
	private long expressPrice;

	/**
	 * 商品的EMS费用
	 */
	private long emsPrice;

	/**
	 * 商品图片连接
	 */
	private String picLink;
	

	/**
	 * 商品图片连接
	 */
	private List<String> extPicsLink = new ArrayList();
	/**
	 * 商品原始图片连接
	 */
	private List<String> originalExtPicsLink = new ArrayList();

	/**
	 * 商品销售单价
	 */
	private long itemPrice;

	/**
	 * 卖家或者买家承担运费的情况 1 卖家承担运费 2 买家承担运费 3 同城交易，无需运费 大于或等于10
	 * 买家承担运费，表示支持运费模板，该值即为运费模板ID
	 */
	private long sellerPayFreight;

	/**
	 * 购买时的数量限制
	 */
	private long buyLimit;

	 /**
	 * 销售的商品数量
	 */
	private long soldTotalCount;
	

	/**
	 * 近期购买商品数量
	 */
	private long buyNum;

	/**
	 * 购买商品的总数量
	 */
	private long totalBuyNum;

	/**
	 * 近期下单的订单次数
	 */
	private long buyCount;

	/**
	 * 下单的订单总次数
	 */
	private long totalBuyCount;

	/**
	 * 国家id
	 */
	private long countryId;

	/**
	 * 省份id
	 */
	private long provinceId;

	/**
	 * 城市id
	 */
	private long cityId;

	/**
	 * 商品的市场价格
	 */
	private long marketPrice;

	 /**
	 * 是否提供保修服务 1是 0否
	 */
	 private byte guaranteeRepair;
	


	/**
	 * 访问的次数
	 */
	private long visitCount;

	/**
	 * 卖家QQ号
	 */
	private long sellerUin;

	/**
	 * 卖家信用等级（具体数值）
	 */
	private int sellerCredit;
	/**
	 * 卖家信用等级（等级）
	 */
	private String sellerCreditLevel ;

	
	private Map<ItemExtend, String> extendInfo = new Hashtable();

	private List<Integer> properties = new ArrayList<Integer>();

	// /**
	// * 商品属性
	// */
	private long itemProperty;
/*
	*//**
	 * 推荐搭配商品编码，多个以‘|’号隔开
	 */
	private String relatedItems;

	// *********************版本大于1的字段***********************/

	/**
	 * 图片url
	 */
	private Vector<String> vecImg;

	/**
	 * 可以使用的红包面值最小为多少
	 */
	private long redPrice;

	
	/**
	 * 活动价格
	 * 
	 * 版本 >= 0
	 */
	private long activityPrice;
	
	private String activityDes ;

	
	/**
	 * 店铺类型，0：拍拍，1：网购
	 */
	private int shopType;
	
	/**
	 * 是否一口价商品，1：一口价，0：不是
	 */
	private int fixOrderFlag = 0;
	
	/**
	 * 大促
	 */
	private int promotion = 0;
	
	/**
	 * 是否参加促销标识
	 */
	private long marketStat;	
	

	
	public int getPromotion() {
	    return promotion;
	}

	public void setPromotion(int promotion) {
	    this.promotion = promotion;
	}

	
	public Vector<String> getVecImg() {
		return vecImg;
	}

	public void setVecImg(Vector<String> vecImg) {
		this.vecImg = vecImg;
	}

	public long getRedPrice() {
		return redPrice;
	}

	public void setRedPrice(long redPrice) {
		this.redPrice = redPrice;
	}

	public static void main(String[] args) {
		new ItemBOBasic().toString();
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemLocalCode() {
		return itemLocalCode;
	}

	public void setItemLocalCode(String itemLocalCode) {
		this.itemLocalCode = itemLocalCode;
	}

	public ItemState getItemState() {
		return itemState;
	}

	public void setItemState(ItemState itemState) {
		this.itemState = itemState;
	}

	public String getStateDesc() {
		return stateDesc;
	}

	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}

	public long getFreightId() {
		return freightId;
	}

	public void setFreightId(long freightId) {
		this.freightId = freightId;
	}

	public long getCodFreightId() {
		return codFreightId;
	}

	public void setCodFreightId(long codFreightId) {
		this.codFreightId = codFreightId;
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public long getClassId() {
		return classId;
	}

	public void setClassId(long classId) {
		this.classId = classId;
	}

	/*public long getStockCount() {
		return stockCount;
	}

	public void setStockCount(long stockCount) {
		this.stockCount = stockCount;
	}*/

	public long getMailPrice() {
		return mailPrice;
	}

	public void setMailPrice(long mailPrice) {
		this.mailPrice = mailPrice;
	}

	public long getExpressPrice() {
		return expressPrice;
	}

	public void setExpressPrice(long expressPrice) {
		this.expressPrice = expressPrice;
	}

	public long getEmsPrice() {
		return emsPrice;
	}

	public void setEmsPrice(long emsPrice) {
		this.emsPrice = emsPrice;
	}

	public String getPicLink() {
		return picLink;
	}
	

	public void setPicLink(String picLink) {
		this.picLink = picLink;
	}

	public List<String> getExtPicsLink() {
		return extPicsLink;
	}

	public void setExtPicsLink(List<String> extPicsLink) {
		this.extPicsLink = extPicsLink;
	}

	public long getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(long itemPrice) {
		this.itemPrice = itemPrice;
	}

	public long getSellerPayFreight() {
		return sellerPayFreight;
	}

	public void setSellerPayFreight(long sellerPayFreight) {
		this.sellerPayFreight = sellerPayFreight;
	}

	public long getBuyLimit() {
		return buyLimit;
	}

	public void setBuyLimit(long buyLimit) {
		this.buyLimit = buyLimit;
	}

	public long getBuyNum() {
		return buyNum;
	}

	public void setBuyNum(long buyNum) {
		this.buyNum = buyNum;
	}

	public long getTotalBuyNum() {
		return totalBuyNum;
	}

	public void setTotalBuyNum(long totalBuyNum) {
		this.totalBuyNum = totalBuyNum;
	}

	public long getBuyCount() {
		return buyCount;
	}

	public void setBuyCount(long buyCount) {
		this.buyCount = buyCount;
	}

	public long getTotalBuyCount() {
		return totalBuyCount;
	}

	public void setTotalBuyCount(long totalBuyCount) {
		this.totalBuyCount = totalBuyCount;
	}

	

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public long getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(long provinceId) {
		this.provinceId = provinceId;
	}

	public long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	public long getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(long marketPrice) {
		this.marketPrice = marketPrice;
	}

	public long getVisitCount() {
		return visitCount;
	}

	public void setVisitCount(long visitCount) {
		this.visitCount = visitCount;
	}

	public long getSellerUin() {
		return sellerUin;
	}

	public void setSellerUin(long sellerUin) {
		this.sellerUin = sellerUin;
	}


	public Map<ItemExtend, String> getExtendInfo() {
		return extendInfo;
	}

	public void setExtendInfo(Map<ItemExtend, String> extendInfo) {
		this.extendInfo = extendInfo;
	}

	

	public long getItemProperty() {
		return itemProperty;
	}

	public void setItemProperty(long itemProperty) {
		this.itemProperty = itemProperty;
	}

	public String getRelatedItems() {
		return relatedItems;
	}

	public void setRelatedItems(String relatedItems) {
		this.relatedItems = relatedItems;
	}

	
	public List<String> getOriginalExtPicsLink() {
		return originalExtPicsLink;
	}

	public void setOriginalExtPicsLink(List<String> originalExtPicsLink) {
		this.originalExtPicsLink = originalExtPicsLink;
	}

	public long getSoldTotalCount() {
		return soldTotalCount;
	}

	public List<Integer> getProperties() {
		return properties;
	}

	public void setProperties(List<Integer> properties) {
		this.properties = properties;
	}

	public void setSoldTotalCount(long soldTotalCount) {
		this.soldTotalCount = soldTotalCount;
	}


	public int getShopType() {
		return shopType;
	}

	public void setShopType(int shopType) {
		this.shopType = shopType;
	}

	public int getFixOrderFlag() {
		return fixOrderFlag;
	}

	public void setFixOrderFlag(int fixOrderFlag) {
		this.fixOrderFlag = fixOrderFlag;
	}

	public long getMarketStat() {
		return marketStat;
	}

	public void setMarketStat(long marketStat) {
		this.marketStat = marketStat;
	}


	
	public String getActivityDes() {
		return activityDes;
	}

	public void setActivityDes(String activityDes) {
		this.activityDes = activityDes;
	}

	
	public String getItemUrl() {
		return itemUrl;
	}

	public void setItemUrl(String itemUrl) {
		this.itemUrl = itemUrl;
	}
	

	public String getItemWeiDian() {
		return itemWeiDian;
	}

	public void setItemWeiDian(String itemWeiDian) {
		this.itemWeiDian = itemWeiDian;
	}

	public String getShopWeiDian() {
		return shopWeiDian;
	}

	public void setShopWeiDian(String shopWeiDian) {
		this.shopWeiDian = shopWeiDian;
	}


	public long getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(long beginTime) {
		this.beginTime = beginTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public Long getLeafClassId() {
		return leafClassId;
	}

	public void setLeafClassId(Long leafClassId) {
		this.leafClassId = leafClassId;
	}

	public byte getGuaranteeRepair() {
		return guaranteeRepair;
	}

	public void setGuaranteeRepair(byte guaranteeRepair) {
		this.guaranteeRepair = guaranteeRepair;
	}

	public long getActivityPrice() {
		return activityPrice;
	}

	public void setActivityPrice(long activityPrice) {
		this.activityPrice = activityPrice;
	}

	public int getSellerCredit() {
		return sellerCredit;
	}

	public void setSellerCredit(int sellerCredit) {
		this.sellerCredit = sellerCredit;
	}

	public String getSellerCreditLevel() {
		return sellerCreditLevel;
	}

	public void setSellerCreditLevel(String sellerCreditLevel) {
		this.sellerCreditLevel = sellerCreditLevel;
	}
}