package com.qq.qqbuy.item.po;

import java.math.BigDecimal;
import com.qq.qqbuy.common.util.SumUtil;
import com.qq.qqbuy.search.po.SearchItem;

/**
 * 货到付款优化-成本分摊一期对象
 * @author simonshao
 *
 */
public class FreightInfo {
		
	public static long ADDITIONAL_FEE_VER1 = 500;
	
	public static long ADDITIONAL_FEE_VER2 = 1500;
	
	public boolean isSupportPayWhenArraive = true;
	
	//卖家是否是特殊卖家，不收取货到付款附加费，且全包邮
	public boolean isMMB = false;
	
	//接口版本号
	private int version = 0;
	
	//是否包邮
	public boolean isFree4Freight = false;
	
	//运费
	public long fee4Freight = 0;
	
	//运费加量价格
	public long priceNormalAdd = -1;
	
	//加收费用
	public long additionalFee = 0;

	//商品价格
	public long price = 0;
	
	//最大价格
	public long maxPrice = 0;
	
	//最小价格
	public long minPrice = 0;
	
	//商品市场价格
	public long marketPrice = 0;
	
	//原价
	public long originalPrice = 0; 
	
	
	//类目搜索商品详情
	private SearchItem commodity;
	
	//类目搜索商品价格，单位元
	public double priceFromSearch = 0;
	
	//类目搜索商品运费，单位元
	public double fee4FreightFromSearch = 0;
	
	// 多买立减金额
	public long  savingFee = 0;
	
	/**
	 * 用于商品详情页，下单流程及订单页面
	 */
	public FreightInfo(){};
	
	
	/**
	 * 用于类目搜索列表
	 * @param commodity
	 */
	public FreightInfo(SearchItem commodity) {
		this.commodity = commodity;
	}
	
	/**
	 * 返回类目搜索商品价格，单位元
	 * @return
	 */
	public String getTotalPrice4Search() {
     	 BigDecimal multiple=new BigDecimal(Double.toString(100.0));
		 BigDecimal priFromSearch = new BigDecimal(Double.toString(priceFromSearch)).multiply(multiple);
		 BigDecimal fee4FreFromSearch = new BigDecimal(Double.toString(fee4FreightFromSearch)).multiply(multiple);
		 return  SumUtil.getFormattedCash((long)(priFromSearch.doubleValue()+fee4FreFromSearch.doubleValue()+additionalFee));
	}
	
	/**
	 * 返回总价
	 * 
	 * @return
	 */
	public long getTotalPrice() {	//fee4Freight, additionalFee的值不可能同时大于零
		if(version == 3){
			return price;
		}
		return price+fee4Freight+additionalFee;
	}
	
	
	/**
	 * 获取最大价格
	 * @return
	 */
	public long getMaxTotalPrice(){
		if(version == 3){
			return maxPrice;
		}
		return maxPrice+fee4Freight+additionalFee;
	}
	
	/*
	 * 获取最小价格
	 */
	public long getMinTotalPrice(){
		if(version == 3){
			return minPrice;
		}
		return minPrice+fee4Freight+additionalFee;
	}
	
	/**
	 * 返回市场总价
	 * @return
	 */
	public long getTotalMarketPrice() {//fee4Freight, additionalFee的值不可能同时大于零

		return marketPrice+fee4Freight+additionalFee;
	}
	
	/**
	 * 是有区间价
	 * @return
	 */
	public boolean hasIntervalPrice(){
		if(maxPrice > minPrice && minPrice > 0){
			return true;
		}
		return false;
	}
	
	/**
	 * 节省价格，等于运费-增加一件的费用
	 * @return
	 */
	public long savingPrice() {
		if(savingFee > 0){
			return savingFee;
		}
		if(priceNormalAdd != -1) {	//只有设置邮费后才使用规则
			return fee4Freight - priceNormalAdd;
		} else {
			return -1;
		}		
	}
	
	
	public long getAdditionalFee(String dealCreateTime, long dealCount) {
	
		if(!isSupportPayWhenArraive || isMMB || !isFree4Freight) {	//不支持货到付款、买卖宝、不包邮，不增加费用
			return 0;
		}			
		return dealCount*FreightInfo.ADDITIONAL_FEE_VER2;
		
	}
	
	
	
	/**
	 * 获取新的货到付款标识
	 * @param mItem
	 * @return
	 */
	public boolean getSupportPayWhenArraive(MItem4J mItem){
		if(mItem == null){
			return false;
		}
		isSupportPayWhenArraive = mItem.isSupportCOD();
		
		return isSupportPayWhenArraive;
	}

	public int getVersion() {
		return version;
	}


	public void setVersion(int version) {
		this.version = version;
	}


	@Override
	public String toString() {
		return "FreightInfo [isSupportPayWhenArraive="
				+ isSupportPayWhenArraive + ", isMMB=" + isMMB
				+ ", isFree4Freight=" + isFree4Freight + ", fee4Freight="
				+ fee4Freight + ", priceNormalAdd=" + priceNormalAdd
				+ ", additionalFee=" + additionalFee + ", price=" + price
				+ ", marketPrice=" + marketPrice + "]";
	}


	public boolean isSupportPayWhenArraive() {
		return isSupportPayWhenArraive;
	}


	public void setSupportPayWhenArraive(boolean isSupportPayWhenArraive) {
		this.isSupportPayWhenArraive = isSupportPayWhenArraive;
	}


	public long getOriginalPrice() {
		return originalPrice;
	}


	public void setOriginalPrice(long originalPrice) {
		this.originalPrice = originalPrice;
	}


	public SearchItem getCommodity() {
		return commodity;
	}


	public void setCommodity(SearchItem commodity) {
		this.commodity = commodity;
	}

}
