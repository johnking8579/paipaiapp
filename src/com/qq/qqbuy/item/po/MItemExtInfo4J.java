 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.item.po;

import java.util.Vector;



/**
 *商品信息
 *
 *@date 2011-11-15 11:14::15
 *
 *@since version:0
*/
public class MItemExtInfo4J  
{


	/**
	 * 商品名称
	 *
	 * 版本 >= 0
	 */
	private String itemName = new String();

	/**
	 * 关联商品
	 *
	 * 版本 >= 0
	 */
	 private String raleItems = new String();

	/**
	 * 商品图片
	 *
	 * 版本 >= 0
	 */
	 private Vector<MItemPic4J> itemPics = new Vector<MItemPic4J>();

	

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getRaleItems() {
		return raleItems;
	}

	public void setRaleItems(String raleItems) {
		this.raleItems = raleItems;
	}

	public Vector<MItemPic4J> getItemPics() {
		return itemPics;
	}

	public void setItemPics(Vector<MItemPic4J> itemPics) {
		this.itemPics = itemPics;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MItemExtInfo4J [itemName=").append(itemName).append(
				", itemPics=").append(itemPics).append(", raleItems=").append(
				raleItems).append("]");
		return builder.toString();
	}





	
}
