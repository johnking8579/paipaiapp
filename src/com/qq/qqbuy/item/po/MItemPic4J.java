
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.item.po;

import com.paipai.util.string.StringUtil;
import com.qq.qqbuy.item.util.ItemUtil;

/**
 *商品图片信息
 * 
 *@date 2011-11-15 11:14::15
 * 
 *@since version:0
 */
public class MItemPic4J {

	/**
	 * 图片序号
	 * 
	 * 版本 >= 0
	 */
	private int picIndex;

	/**
	 * 图片URL
	 * 
	 * 版本 >= 0
	 */
	private String picUrl = new String();

	/**
	 * 图片描述
	 * 
	 * 版本 >= 0
	 */
	private String picDesc = new String();

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MItemPic4J [picDesc=").append(picDesc).append(
				", picIndex=").append(picIndex).append(", picUrl=").append(
				picUrl).append("]");
		return builder.toString();
	}
	public String getPicUrl160() {
		//手拍自己上传的商品具有160尺寸
		if(StringUtil.isNotEmpty(picUrl)&&picUrl.contains("mimg")){
			return ItemUtil.getImgUrl(picUrl,"160x160");
		}
		return ItemUtil.getImgUrl(picUrl,"80x80");
	}
	public int getPicIndex() {
		return picIndex;
	}

	public void setPicIndex(int picIndex) {
		this.picIndex = picIndex;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getPicDesc() {
		return picDesc;
	}

	public void setPicDesc(String picDesc) {
		this.picDesc = picDesc;
	}

}
