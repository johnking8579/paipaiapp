package com.qq.qqbuy.item.po;

public class Commodity {

	/**
	 * 商品搜索结果中的商品对象
	 * 
	 * @author rickwang
	 */	    
	    /**
	     * 商品图片链接80 x 80
	     */
	    public String img;
	    
	    /**
	     * 商品大图链接200 x 200
	     */
	    public String imgL;
	    
	    /**
	     * 商品超大图链接300 x 300
	     */
	    public String imgLL;
	    
	    /**
	     * 商品标题
	     */
	    public String title;
	    
	    /**
	     * 商品详情链接（拍拍WEB端链接，移动电商中不要直接使用，可用于验证）
	     */
	    public String link;
	    
	    /**
	     * 商品ID
	     */
	    public String itemCode;
	    
	    /**
	     * 卖家QQ号
	     */
	    public long sellerUin;
	    
	    /**
	     * 卖家昵称(店铺名)
	     */
	    public String sellerNickName;
	    
	    /**
	     * 卖家信用值
	     */
	    public long sellerCredit;
	    
	    /**
	     * 卖家等级
	     */
	    public long sellerLevel;
	    
	    /**
	     * 邮递费用，注意可能为文字“免邮费”或者以元为单位的邮递费用值，如“10.00”（已处理为2位小数）
	     * expressFee与expressFee2字段详细含义如下:
	     * 1) 如果是免运费：expressFee为"免运费"，expressFee2无意义
	     * 2) 如果expressFee和expressFee2都有价格数据：expressFee表示平邮价格 expressFee2表示快递价格
	     * 3) 如果expressFee有价格数据，expressFee2为空串，则expressFee表示快递价格
	     */
	    public String expressFee;
	    
	    /**
	     * 邮递费用2.请参见expressFee
	     */
	    public String expressFee2;
	    
	    /**
	     * 商品价格，以元为单位，如“22.00”（已处理为2位小数）
	     */
	    public String price;
	    
	    /**
	     * 店铺所在地
	     */
	    public String shopAddr;
	    
	    /**
	     * 已售件数
	     */
	    public long soldNum;
	    
	    /**
	     * 人气
	     */
	    public long favNum;
	    
	    /**
	     * 商家认证状态
	     */
	    public String userValidStatus;
	    
	    /**
	     * 先行赔付状态
	     */
	    public String legend1Status;
	    
	    /**
	     * 7天退货状态
	     */
	    public String legend2Status;
	    
	    /**
	     * 诚保代充状态
	     */
	    public String legend3Status;
	    
	    /**
	     * 假一赔三状态
	     */
	    public String legend4Status;
	    
	    /**
	     * 货到付款状态
	     */
	    public String codStatus;
	    
	    /**
	     * 红包商品状态
	     */
	    public String redPacketStatus;
	    
	    /**
	     * 自动发货状态
	     */
	    public String autoShipStatus;
	    
	    /**
	     * 裳品廊状态
	     */
	    public String validClothStatus;
	    
	    /**
	     * QQ会员价格,以元为单位，已处理为两位小数的格式，如“12.00”
	     */
	    public String qqVipPrice;
	    
	    /**
	     * 商品属性串，如"6e20:b|6eba:800|6e17:6|6ec0:200000"
	     */
	    public String propertyString;
	    
	    /**
	     * 是否支持彩钻
	     */
	    public boolean isSupportColorDiamond;
	    
	    /**
	     * 支持的红包面额
	     */
	    public int redPacket;

		@Override
		public String toString() {
			return "Commodity [img=" + img + ", imgL=" + imgL + ", imgLL=" + imgLL
					+ ", title=" + title + ", link=" + link + ", itemCode="
					+ itemCode + ", sellerUin=" + sellerUin + ", sellerNickName="
					+ sellerNickName + ", sellerCredit=" + sellerCredit
					+ ", sellerLevel=" + sellerLevel + ", expressFee=" + expressFee
					+ ", expressFee2=" + expressFee2 + ", price=" + price
					+ ", shopAddr=" + shopAddr + ", soldNum=" + soldNum
					+ ", favNum=" + favNum + ", userValidStatus=" + userValidStatus
					+ ", legend1Status=" + legend1Status + ", legend2Status="
					+ legend2Status + ", legend3Status=" + legend3Status
					+ ", legend4Status=" + legend4Status + ", codStatus="
					+ codStatus + ", redPacketStatus=" + redPacketStatus
					+ ", autoShipStatus=" + autoShipStatus + ", validClothStatus="
					+ validClothStatus + ", qqVipPrice=" + qqVipPrice
					+ ", propertyString=" + propertyString
					+ ", isSupportColorDiamond=" + isSupportColorDiamond
					+ ", redPacket=" + redPacket + "]";
		}
	 
	}


