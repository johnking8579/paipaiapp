package com.qq.qqbuy.item.po.eval;

public class CEvalReply4J {
	/**
	 * 回复者QQ号
	 * 
	 * 版本 >= 0
	 */
	private long uin;

	/**
	 * 订单id-int型
	 * 
	 * 版本 >= 0
	 */
	private long fdealId;

	/**
	 * 创建时间
	 * 
	 * 版本 >= 0
	 */
	private long createTime;

	/**
	 * 订单id-str型
	 * 
	 * 版本 >= 0
	 */
	private String dealId = new String();

	/**
	 * 内容
	 * 
	 * 版本 >= 0
	 */
	private String content = new String();

	/**
	 * id
	 * 
	 * 版本 >= 0
	 */
	private long id;

	public long getUin() {
		return uin;
	}

	public void setUin(long uin) {
		this.uin = uin;
	}

	public long getFdealId() {
		return fdealId;
	}

	public void setFdealId(long fdealId) {
		this.fdealId = fdealId;
	}

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

	public String getDealId() {
		return dealId;
	}

	public void setDealId(String dealId) {
		this.dealId = dealId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CEvalReply4J [content=").append(content).append(
				", createTime=").append(createTime).append(", dealId=").append(
				dealId).append(", fdealId=").append(fdealId).append(", id=")
				.append(id).append(", uin=").append(uin).append("]");
		return builder.toString();
	}

	
}
