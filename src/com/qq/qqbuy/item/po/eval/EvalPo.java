package com.qq.qqbuy.item.po.eval;

import java.util.Vector;

public class EvalPo {
	    private Vector<EvalRecordPo4J> evalInfolist = new Vector<EvalRecordPo4J>();
	   
		private long totalNum;

		public Vector<EvalRecordPo4J> getEvalInfolist() {
			return evalInfolist;
		}

		public void setEvalInfolist(Vector<EvalRecordPo4J> evalInfolist) {
			this.evalInfolist = evalInfolist;
		}

		public long getTotalNum() {
			return totalNum;
		}

		public void setTotalNum(long totalNum) {
			this.totalNum = totalNum;
		}
}
	
