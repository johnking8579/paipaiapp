package com.qq.qqbuy.item.po.eval;

import java.util.Date;
import java.util.Vector;

import com.paipai.lang.uint32_t;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.util.DateUtils;

public class EvalRecordPo4J {
	/**
	 * 子订单id-int型
	 * 
	 * 版本 >= 0
	 */
	/*private long fdealId;*/

	/**
	 * 子订单id-str型
	 * 
	 * 版本 >= 0
	 */
	/*private String dealId = new String();*/

	/**
	 * 商品id
	 * 
	 * 版本 >= 0
	 */
	/*private String commodityId = new String();*/

	/**
	 * 商品logo
	 * 
	 * 版本 >= 0
	 */
	/*private String commodityLogo = new String();*/

	/**
	 * 商品名称
	 * 
	 * 版本 >= 0
	 */
	/*private String commodityTitle = new String();*/

	/**
	 * 快照号
	 * 
	 * 版本 >= 0
	 */
	/*private String snapId = new String();*/

	/**
	 * 买家号
	 * 
	 * 版本 >= 0
	 */
	private long buyerUin;
	/**
	 * 买家图像
	 */
	private String buyerLogo;

	/**
	 * 卖家号
	 * 
	 * 版本 >= 0
	 *//*
	private long sellerUin;*/

	/**
	 * 卖家nick
	 * 
	 * 版本 >= 0
	 */
	/*private String sellerNickName = new String();*/

	public String getBuyerLogo() {
		return buyerLogo;
	}

	public void setBuyerLogo(String buyerLogo) {
		this.buyerLogo = buyerLogo;
	}

	/**
	 * 买家nick
	 * 
	 * 版本 >= 0
	 */
	private String buyerNickName = new String();

	/**
	 * 买家做评时间
	 * 
	 * 版本 >= 0
	 */
	private long buyerEvalTime;

	/**
	 * 卖家做评时间
	 * 
	 * 版本 >= 0
	 */
	/*private long sellerEvalTime;*/

	/**
	 * 买家评价内容
	 * 
	 * 版本 >= 0
	 */
	private String buyerExplain = new String();

	/**
	 * 卖家评价内容
	 * 
	 * 版本 >= 0
	 */
	/*private String sellerExplain = new String();*/

	/**
	 * 买家评价原因
	 * 
	 * 版本 >= 0
	 *//*
	private Vector<uint32_t> buyerReason = new Vector<uint32_t>();*/

	/**
	 * 卖家评价原因
	 * 
	 * 版本 >= 0
	 */
	/*private Vector<uint32_t> sellerReason = new Vector<uint32_t>();*/

	/**
	 * 买家得分
	 * 
	 * 版本 >= 0
	 *//*
	private int buyerScore;

	*//**
	 * 卖家得分
	 * 
	 * 版本 >= 0
	 *//*
	private int sellerScore;*/

	/**
	 * 实际交易总额，单位分
	 * 
	 * 版本 >= 0
	 *//*
	private long dealPayment;*/

	/**
	 * 系统计分标记
	 * 
	 * 版本 >= 0
	 *//*
	private long scoreFlag;*/

	/**
	 * 系统计分时间
	 * 
	 * 版本 >= 0
	 */
	/*private long scoreTime;
*/
	/**
	 * 商品价格
	 * 
	 * 版本 >= 0
	 */
	private long price;

	/**
	 * 评价创建时间
	 * 
	 * 版本 >= 0
	 */
	private long evalCreateTime;

	/**
	 * 订单支付类型
	 * 
	 * 版本 >= 0
	 */
	private long paytype;

	/**
	 * 订单结束时间
	 * 
	 * 版本 >= 0
	 *//*
	private long dealFinishTime;*/

	/**
	 * 订单支付时间
	 * 
	 * 版本 >= 0
	 *//*
	private long dealPayTime;*/

	/**
	 * 买家评价级别1：差评，2：中评，3：好评
	 * 
	 * 版本 >= 0
	 */
	private short buyerEvalLevel;

	/**
	 * 卖家评价级别1：差评，2：中评，3：好评
	 * 
	 * 版本 >= 0
	 */
	/*private short sellerEvalLevel;*/

	/**
	 * 买家记录是否被删除,0不删除1删除
	 * 
	 * 版本 >= 0
	 *//*
	private short buyerEvalDelFlag;

	*//**
	 * 卖家记录是否被删除，0不删除1删除
	 * 
	 * 版本 >= 0
	 *//*
	private short sellerEvalDelFlag;*/

	/**
	 * 对方是否为系统做评
	 * 
	 * 版本 >= 0
	 *//*
	private short peerSysEval;

	*//**
	 * 自己是否为系统做评
	 * 
	 * 版本 >= 0
	 *//*
	private short selfSysEval;*/

	/**
	 * 评价回复
	 * 
	 * 版本 >= 0
	 *//*
	private Vector<CEvalReply4J> replyMsg = new Vector<CEvalReply4J>();*/

	/**
	 * 可以回复次数
	 * 
	 * 版本 >= 0
	 *//*
	private long numCanReply;*/

	/**
	 * 子订单对应的大订单号
	 * 
	 * 版本 >= 0
	 *//*
	private String ftradeId = new String();*/

	/**
	 * 发货速度
	 * 
	 * 版本 >= 0
	 *//*
	private long dsr1;

	*//**
	 * 商品质量
	 * 
	 * 版本 >= 0
	 *//*
	private long dsr2;

	*//**
	 * 服务态度
	 * 
	 * 版本 >= 0
	 *//*
	private long dsr3;*/

	/**
	 * dsr做评时间
	 * 
	 * 版本 >= 0
	 */
	/*private long dsrEvalTime;*/

	/**
	 * 0表示有效，1为无效
	 * 
	 * 版本 >= 0
	 */
	/*private long dsrFlag;*/
/*
	public long getFdealId() {
		return fdealId;
	}

	public void setFdealId(long fdealId) {
		this.fdealId = fdealId;
	}

	public String getDealId() {
		return dealId;
	}

	public void setDealId(String dealId) {
		this.dealId = dealId;
	}

	public String getCommodityId() {
		return commodityId;
	}

	public void setCommodityId(String commodityId) {
		this.commodityId = commodityId;
	}

	public String getCommodityLogo() {
		return commodityLogo;
	}

	public void setCommodityLogo(String commodityLogo) {
		this.commodityLogo = commodityLogo;
	}

	public String getCommodityTitle() {
		return commodityTitle;
	}

	public void setCommodityTitle(String commodityTitle) {
		this.commodityTitle = commodityTitle;
	}

	public String getSnapId() {
		return snapId;
	}

	public void setSnapId(String snapId) {
		this.snapId = snapId;
	}*/

	public long getBuyerUin() {
		return buyerUin;
	}

	public void setBuyerUin(long buyerUin) {
		this.buyerUin = buyerUin;
	}
/*
	public long getSellerUin() {
		return sellerUin;
	}

	public void setSellerUin(long sellerUin) {
		this.sellerUin = sellerUin;
	}

	public String getSellerNickName() {
		return sellerNickName;
	}

	public void setSellerNickName(String sellerNickName) {
		this.sellerNickName = sellerNickName;
	}*/

	public String getBuyerNickName() {
		return buyerNickName;
	}

	public void setBuyerNickName(String buyerNickName) {
		this.buyerNickName = buyerNickName;
	}

	public long getBuyerEvalTime() {
		return buyerEvalTime;
	}
	/*public String getBuyerEvalTimeStr() {
		try {
			return DateUtils.formatDateToString(new Date(buyerEvalTime*1000l),DateUtils.FORMAT_YMD);
		} catch (Exception e) {
		      Log.run.warn("getBuyerEvalTime error",e);
		      return "";
		}
	}*/
	public void setBuyerEvalTime(long buyerEvalTime) {
		this.buyerEvalTime = buyerEvalTime;
	}

	/*public long getSellerEvalTime() {
		return sellerEvalTime;
	}

	public void setSellerEvalTime(long sellerEvalTime) {
		this.sellerEvalTime = sellerEvalTime;
	}
*/
	public String getBuyerExplain() {
		return buyerExplain;
	}

	public void setBuyerExplain(String buyerExplain) {
		this.buyerExplain = buyerExplain;
	}

	/*public String getSellerExplain() {
		return sellerExplain;
	}

	public void setSellerExplain(String sellerExplain) {
		this.sellerExplain = sellerExplain;
	}*/

	/*public Vector<uint32_t> getBuyerReason() {
		return buyerReason;
	}

	public void setBuyerReason(Vector<uint32_t> buyerReason) {
		this.buyerReason = buyerReason;
	}

	public Vector<uint32_t> getSellerReason() {
		return sellerReason;
	}

	public void setSellerReason(Vector<uint32_t> sellerReason) {
		this.sellerReason = sellerReason;
	}

	public int getBuyerScore() {
		return buyerScore;
	}

	public void setBuyerScore(int buyerScore) {
		this.buyerScore = buyerScore;
	}

	public int getSellerScore() {
		return sellerScore;
	}

	public void setSellerScore(int sellerScore) {
		this.sellerScore = sellerScore;
	}

	public long getDealPayment() {
		return dealPayment;
	}

	public void setDealPayment(long dealPayment) {
		this.dealPayment = dealPayment;
	}

	public long getScoreFlag() {
		return scoreFlag;
	}

	public void setScoreFlag(long scoreFlag) {
		this.scoreFlag = scoreFlag;
	}

	public long getScoreTime() {
		return scoreTime;
	}

	public void setScoreTime(long scoreTime) {
		this.scoreTime = scoreTime;
	}
*/
	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public long getEvalCreateTime() {
		return evalCreateTime;
	}

	public void setEvalCreateTime(long evalCreateTime) {
		this.evalCreateTime = evalCreateTime;
	}

	public long getPaytype() {
		return paytype;
	}

	public void setPaytype(long paytype) {
		this.paytype = paytype;
	}

	/*public long getDealFinishTime() {
		return dealFinishTime;
	}

	public void setDealFinishTime(long dealFinishTime) {
		this.dealFinishTime = dealFinishTime;
	}

	public long getDealPayTime() {
		return dealPayTime;
	}

	public void setDealPayTime(long dealPayTime) {
		this.dealPayTime = dealPayTime;
	}*/

	public short getBuyerEvalLevel() {
		return buyerEvalLevel;
	}

	public void setBuyerEvalLevel(short buyerEvalLevel) {
		this.buyerEvalLevel = buyerEvalLevel;
	}

	/*public short getSellerEvalLevel() {
		return sellerEvalLevel;
	}

	public void setSellerEvalLevel(short sellerEvalLevel) {
		this.sellerEvalLevel = sellerEvalLevel;
	}*/

	/*public short getBuyerEvalDelFlag() {
		return buyerEvalDelFlag;
	}

	public void setBuyerEvalDelFlag(short buyerEvalDelFlag) {
		this.buyerEvalDelFlag = buyerEvalDelFlag;
	}

	public short getSellerEvalDelFlag() {
		return sellerEvalDelFlag;
	}

	public void setSellerEvalDelFlag(short sellerEvalDelFlag) {
		this.sellerEvalDelFlag = sellerEvalDelFlag;
	}

	public short getPeerSysEval() {
		return peerSysEval;
	}

	public void setPeerSysEval(short peerSysEval) {
		this.peerSysEval = peerSysEval;
	}

	public short getSelfSysEval() {
		return selfSysEval;
	}

	public void setSelfSysEval(short selfSysEval) {
		this.selfSysEval = selfSysEval;
	}*/

	/*public Vector<CEvalReply4J> getReplyMsg() {
		return replyMsg;
	}

	public void setReplyMsg(Vector<CEvalReply4J> replyMsg) {
		this.replyMsg = replyMsg;
	}
*/
	/*public long getNumCanReply() {
		return numCanReply;
	}

	public void setNumCanReply(long numCanReply) {
		this.numCanReply = numCanReply;
	}

	public String getFtradeId() {
		return ftradeId;
	}

	public void setFtradeId(String ftradeId) {
		this.ftradeId = ftradeId;
	}

	public long getDsr1() {
		return dsr1;
	}

	public void setDsr1(long dsr1) {
		this.dsr1 = dsr1;
	}

	public long getDsr2() {
		return dsr2;
	}

	public void setDsr2(long dsr2) {
		this.dsr2 = dsr2;
	}

	public long getDsr3() {
		return dsr3;
	}

	public void setDsr3(long dsr3) {
		this.dsr3 = dsr3;
	}

	public long getDsrEvalTime() {
		return dsrEvalTime;
	}

	public void setDsrEvalTime(long dsrEvalTime) {
		this.dsrEvalTime = dsrEvalTime;
	}

	public long getDsrFlag() {
		return dsrFlag;
	}

	public void setDsrFlag(long dsrFlag) {
		this.dsrFlag = dsrFlag;
	}*/

	/*@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EvalRecordPo4J [buyerEvalDelFlag=").append(
				buyerEvalDelFlag).append(", buyerEvalLevel=").append(
				buyerEvalLevel).append(", buyerEvalTime=")
				.append(buyerEvalTime).append(", buyerExplain=").append(
						buyerExplain).append(", buyerNickName=").append(
						buyerNickName).append(", buyerReason=").append(
						buyerReason).append(", buyerScore=").append(buyerScore)
				.append(", buyerUin=").append(buyerUin)
				.append(", commodityId=").append(commodityId).append(
						", commodityLogo=").append(commodityLogo).append(
						", commodityTitle=").append(commodityTitle).append(
						", dealFinishTime=").append(dealFinishTime).append(
						", dealId=").append(dealId).append(", dealPayTime=")
				.append(dealPayTime).append(", dealPayment=").append(
						dealPayment).append(", dsr1=").append(dsr1).append(
						", dsr2=").append(dsr2).append(", dsr3=").append(dsr3)
				.append(", dsrEvalTime=").append(dsrEvalTime).append(
						", dsrFlag=").append(dsrFlag).append(
						", evalCreateTime=").append(evalCreateTime).append(
						", fdealId=").append(fdealId).append(", ftradeId=")
				.append(ftradeId).append(", numCanReply=").append(numCanReply)
				.append(", paytype=").append(paytype).append(", peerSysEval=")
				.append(peerSysEval).append(", price=").append(price).append(
						", replyMsg=").append(replyMsg).append(", scoreFlag=")
				.append(scoreFlag).append(", scoreTime=").append(scoreTime)
				.append(", selfSysEval=").append(selfSysEval).append(
						", sellerEvalDelFlag=").append(sellerEvalDelFlag)
				.append(", sellerEvalLevel=").append(sellerEvalLevel).append(
						", sellerEvalTime=").append(sellerEvalTime).append(
						", sellerExplain=").append(sellerExplain).append(
						", sellerNickName=").append(sellerNickName).append(
						", sellerReason=").append(sellerReason).append(
						", sellerScore=").append(sellerScore).append(
						", sellerUin=").append(sellerUin).append(", snapId=")
				.append(snapId).append("]");
		return builder.toString();
	}
*/


	
}
