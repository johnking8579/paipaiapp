package com.qq.qqbuy.item.po;

public class ItemNavigation {
	private String indexUrl;
	private String detailUrl;
	private String detailUrlNoPn;

	private String picUrl;
	private String picUrlNoIndex;

	private String propUrl;

	private String introUrl;

	private String listCtUrl;
	private String listCtUrlNoPnPs;

	private String confirmOrderURL;
	public String getDetailUrl() {
		return detailUrl;
	}

	public void setDetailUrl(String detailUrl) {
		this.detailUrl = detailUrl;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getPropUrl() {
		return propUrl;
	}

	public void setPropUrl(String propUrl) {
		this.propUrl = propUrl;
	}

	public String getIntroUrl() {
		return introUrl;
	}

	public void setIntroUrl(String introUrl) {
		this.introUrl = introUrl;
	}

	public String getListCtUrl() {
		return listCtUrl;
	}

	public void setListCtUrl(String listCtUrl) {
		this.listCtUrl = listCtUrl;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ItemNavigation [detailUrl=").append(detailUrl).append(
				", introUrl=").append(introUrl).append(", listCtUrl=").append(
				listCtUrl).append(", picUrl=").append(picUrl).append(
				", propUrl=").append(propUrl).append("]");
		return builder.toString();
	}

	public String getIndexUrl() {
		return indexUrl;
	}

	public void setIndexUrl(String indexUrl) {
		this.indexUrl = indexUrl;
	}

	public String getDetailUrlNoPn() {
		return detailUrlNoPn;
	}

	public void setDetailUrlNoPn(String detailUrlNoPn) {
		this.detailUrlNoPn = detailUrlNoPn;
	}

	public String getPicUrlNoIndex() {
		return picUrlNoIndex;
	}

	public void setPicUrlNoIndex(String picUrlNoIndex) {
		this.picUrlNoIndex = picUrlNoIndex;
	}

	public String getListCtUrlNoPnPs() {
		return listCtUrlNoPnPs;
	}

	public void setListCtUrlNoPnPs(String listCtUrlNoPnPs) {
		this.listCtUrlNoPnPs = listCtUrlNoPnPs;
	}

	public String getConfirmOrderURL() {
		return confirmOrderURL;
	}

	public void setConfirmOrderURL(String confirmOrderURL) {
		this.confirmOrderURL = confirmOrderURL;
	}

}
