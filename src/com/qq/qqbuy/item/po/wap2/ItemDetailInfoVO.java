package com.qq.qqbuy.item.po.wap2;


import com.qq.qqbuy.item.util.ItemUtil;


/**
 * 商品详情页展示
 * 
 * @author jiaqiangxu
 * 
 */
public class ItemDetailInfoVO extends  ItemDetailVO{
	private int pn;
	private int totalPage;
	

	public String getCurrPageDetailInfo() {
		return ItemUtil.getStrByPsAndPn(super.getDetailInfo(), DETAIL_INFO_SIZE, pn);	
	}

	public int getTotalPage() {
		return ItemUtil.getTotalPage(super.getDetailInfo(), DETAIL_INFO_SIZE);

	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getPn() {
		return pn;
	}

	public void setPn(int pn) {
		this.pn = pn;
	}
	

}
