package com.qq.qqbuy.item.po.wap2;

import com.paipai.util.string.StringUtil;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.item.po.MItem4J;
import com.qq.qqbuy.item.po.Mstock4J;
import com.qq.qqbuy.thirdparty.openapi.po.GetItemResponse;
import com.qq.qqbuy.thirdparty.openapi.po.GetItemResponse.Stock;

/**
 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
 */
public class ItemResponse {
	/**
	 * 拍拍商品信息
	 */
	 private GetItemResponse ppItemInfo = new GetItemResponse();

		/**
		 * 手拍商品信息
		 *
		 * 版本 >= 0
		 */
	 private MItem4J mmItemInfo = new MItem4J();

		public GetItemResponse getPpItemInfo() {
			return ppItemInfo;
		}

		public void setPpItemInfo(GetItemResponse ppItemInfo) {
			this.ppItemInfo = ppItemInfo;
		}

		public MItem4J getMmItemInfo() {
			return mmItemInfo;
		}

		public void setMmItemInfo(MItem4J mmItemInfo) {
			this.mmItemInfo = mmItemInfo;
		}
		/**
		 * 通过stockATTR获取当前库存
		 * @param stockAttr
		 * @return
		 * @throws BusinessException
		 * @date:2013-4-10
		 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
		 * @version 1.0
		 */
		public Mstock4J getCurrentStock(String stockAttr) throws BusinessException{
			if (getPpItemInfo().isMobileItem()) {//手拍商品
				if (StringUtil.isEmpty(stockAttr) ) {
					Mstock4J stock = new Mstock4J();
					stock.setStockPrice(getMmItemInfo().getItemPrice());
					stock.setDiscountPrice(getMmItemInfo().getDiscountPrice());
					return stock;
				} else {
					for (Mstock4J stock : getMmItemInfo().getStockList()) {
						if (stockAttr.equals(stock.getStockAttr())){
							return stock;
						}
					}
				}
			} else {//非手拍商品
				Mstock4J stock = new Mstock4J();
				if (StringUtil.isEmpty(stockAttr)) {
					stock.setStockPrice(getPpItemInfo().getItemPrice());
					stock.setDiscountPrice(getPpItemInfo().discountPrice);
					return stock;
				}
				for (Stock respStock : getPpItemInfo().stockList) {
					if (stockAttr.equals(respStock.stockAttr)){
						stock.setStockId(respStock.stockId);
						stock.setStockAttr(respStock.stockAttr);
						stock.setStockPrice(respStock.stockPrice);
						stock.setDiscountPrice(respStock.discountPrice);
						return stock;
					}
				}
			}								
			throw BusinessException.createInstance(BusinessErrorType.INVALID_STOCKATTR);
		}
		

}

