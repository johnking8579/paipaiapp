package com.qq.qqbuy.item.po.wap2;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 商品VIP折扣
 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
 */
public class ItemDiscountVO extends ItemDetailVO{
	
	private int superQQLevel;
	private int buyerVipLevel;
	private String levelTip;
	private boolean haveLogin;
	private String level1Price;
	private String level1Discount;
	private String level3Price;
	private String level3Discount;
	private String level5Price;
	private String level5Discount;
	private String levelPrice;
	private String levelDiscount;
	private Map <Integer,String> discount=new LinkedHashMap<Integer,String>();
	private Map <Integer,String> afterDiscount=new LinkedHashMap<Integer,String>();

	public int getSuperQQLevel() {
		return superQQLevel;
	}
	public void setSuperQQLevel(int superQQLevel) {
		this.superQQLevel = superQQLevel;
	}
	public int getBuyerVipLevel() {
		return buyerVipLevel;
	}
	public void setBuyerVipLevel(int buyerVipLevel) {
		this.buyerVipLevel = buyerVipLevel;
	}
	public String getLevel1Price() {
		return level1Price;
	}
	public void setLevel1Price(String level1Price) {
		this.level1Price = level1Price;
	}
	public String getLevel1Discount() {
		return level1Discount;
	}
	public void setLevel1Discount(String level1Discount) {
		this.level1Discount = level1Discount;
	}
	public String getLevel3Price() {
		return level3Price;
	}
	public void setLevel3Price(String level3Price) {
		this.level3Price = level3Price;
	}
	public String getLevel3Discount() {
		return level3Discount;
	}
	public void setLevel3Discount(String level3Discount) {
		this.level3Discount = level3Discount;
	}
	public String getLevel5Price() {
		return level5Price;
	}
	public void setLevel5Price(String level5Price) {
		this.level5Price = level5Price;
	}
	public String getLevel5Discount() {
		return level5Discount;
	}
	public void setLevel5Discount(String level5Discount) {
		this.level5Discount = level5Discount;
	}
	public boolean isHaveLogin() {
		return haveLogin;
	}
	public void setHaveLogin(boolean haveLogin) {
		this.haveLogin = haveLogin;
	}
	public String getLevelPrice() {
		return levelPrice;
	}
	public void setLevelPrice(String levelPrice) {
		this.levelPrice = levelPrice;
	}
	public String getLevelDiscount() {
		return levelDiscount;
	}
	public void setLevelDiscount(String levelDiscount) {
		this.levelDiscount = levelDiscount;
	}
	public Map<Integer, String> getDiscount() {
		return discount;
	}
	public void setDiscount(Map<Integer, String> discount) {
		this.discount = discount;
	}
	public Map<Integer, String> getAfterDiscount() {
		return afterDiscount;
	}
	public void setAfterDiscount(Map<Integer, String> afterDiscount) {
		this.afterDiscount = afterDiscount;
	}

	public String getLevelDiscount(int level) {
		if(discount!=null){
			for(Integer i:discount.keySet()){
				if(i.intValue()==level){
					
				}
			}
		}
		return level3Discount;
	}
	public String getLevelTip() {
		return levelTip;
	}
	public void setLevelTip(String levelTip) {
		this.levelTip = levelTip;
	}
}

