package com.qq.qqbuy.item.po.wap2;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.paipai.util.string.StringUtil;
import com.qq.qqbuy.common.Pager;
import com.qq.qqbuy.common.util.SumUtil;
import com.qq.qqbuy.item.constant.ItemConstantForWap;
import com.qq.qqbuy.item.po.ItemAttrBO;
import com.qq.qqbuy.item.po.ItemBO;
import com.qq.qqbuy.item.po.ItemPic;
import com.qq.qqbuy.item.po.ItemStock;
import com.qq.qqbuy.item.po.MItemExtInfo4J;
import com.qq.qqbuy.item.po.Mstock4J;
import com.qq.qqbuy.item.po.PromotionItem;
import com.qq.qqbuy.item.po.SaleAttrPair;
import com.qq.qqbuy.item.util.ItemUtil;
import com.qq.qqbuy.item.util.JunePromotionManager;

/**
 * 供页面展示
 * 
 * @author jiaqiangxu
 * 
 */
public class ItemDetailVO  {
	
	private int pn=1;
	public static final String DEFAULT_INDEX_PAGE_PIC_SIZE="160x160";
	public final  int DETAIL_INFO_SIZE=ItemConstantForWap.MAX_DISPLAY_DETAIL_INFO;
	public final  int ATTR_INFO_SIZE=ItemConstantForWap.MAX_DISPLAY_ATTR_INFO;
	private String itemCode;
	private String itemName;
	private long price;
	private long normalPrice;
	private long marketPrice;
	private long sellerUin;
	private String sellerName;
	private String shopName;
	private String telephone;
	private long soldCount;
	private int classId;
	private boolean supportPromotion;
	private long stockCount;
	private int state;
	private String mainItemPic;
	private boolean supportCod;
	private boolean supportPCCod;//PC货到付款
	private boolean qgoItem;
	public boolean free4Freight = false;
	private String detailInfo;
	private String attrInfo;//
	
	public List<ItemAttrBO> parsedAttrList=new ArrayList<ItemAttrBO>();
	private int picNum;//
	//最大价格
	public long maxPrice = 0;	
	//最小价格
	public long minPrice = 0;	
	
	//最大正常价格
	public long normalMaxPrice = 0;	
	//最小正常价格
	public long normalMinPrice = 0;	
	//原价
	public long originalPrice = 0; 
	//支持红包
	private long redPacketAvariable;

	private Map<String,List<SaleAttrPair>> viewSkuSaleAttrPo;
	private Map<String,List<SaleAttrPair>> viewSkuSaleAttrPoForQuickBuy= new LinkedHashMap<String,List<SaleAttrPair>>();

	private MItemExtInfo4J mitemExtInfo4J;//手拍商品图片
	private Pager<ItemPic> picPage=new Pager<ItemPic>();

	private String discountPrice;//超Q彩钻价
	private List<ItemBO> associateItemList;//关联商品推荐
	private boolean quickBuy;//是否展示快捷购买的标记
	private ItemStock itemStock;
	private List<Mstock4J> stockList;
	
	private long savingFee;//多买立减
	private int tuanPepole;//参团人数
	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public String getSellerName() {
		return sellerName;
	}
	public String getSellerNameStr() {
		return StringUtil.isEmpty(sellerName)?sellerUin+"":sellerName;
	}
	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public long getSoldCount() {
		return soldCount;
	}

	public void setSoldCount(long soldCount) {
		this.soldCount = soldCount;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Map<String, List<SaleAttrPair>> getViewSkuSaleAttrPo() {
		return viewSkuSaleAttrPo;
	}

	public void setViewSkuSaleAttrPo(
			Map<String, List<SaleAttrPair>> viewSkuSaleAttrPo) {
		this.viewSkuSaleAttrPo = viewSkuSaleAttrPo;
	}

	public String getDetailInfo() {
		if(StringUtils.isNotEmpty(detailInfo) && detailInfo.length()>600){
			return detailInfo.substring(0, 600);
		}
		return detailInfo;
	}
	public String getFirstPageDetailInfo() {
		return ItemUtil.getFirstPageStrByMaxLen(detailInfo,DETAIL_INFO_SIZE);
	}
	public boolean getDetailInfoMoreThanOnePage() {
		return ItemUtil.isMoreThanOnePage(detailInfo,DETAIL_INFO_SIZE);
	}
	public boolean getAttrInfoMoreThanOnePage() {
		return ItemUtil.isMoreThanOnePage(attrInfo,ATTR_INFO_SIZE);
	}
	public void setDetailInfo(String detailInfo) {
		this.detailInfo = detailInfo;
	}

	public String getAttrInfo() {
		return attrInfo;
	}
	public String getFirstPageAttrInfo() {
		return ItemUtil.getFirstPageStrByMaxLen(attrInfo,ATTR_INFO_SIZE);
	}
	public void setAttrInfo(String attrInfo) {
		this.attrInfo = attrInfo;
	}

	public long getPrice() {
		return price;
	}
	public String getPriceStr() {
		return SumUtil.getFormattedCash(price);
	}
	public void setPrice(long price) {
		this.price = price;
	}

	public long getMarketPrice() {
		return marketPrice;
	}
	public String getMarketPriceStr() {
		return SumUtil.getFormattedCash(marketPrice);
	}
	
	public void setMarketPrice(long marketPrice) {
		this.marketPrice = marketPrice;
	}

	

	public long getMaxPrice() {
		return maxPrice;
	}
	public String getMaxPriceStr() {
		return SumUtil.getFormattedCash(maxPrice);
	}

	public void setMaxPrice(long maxPrice) {
		this.maxPrice = maxPrice;
	}

	public MItemExtInfo4J getMitemExtInfo4J() {
		return mitemExtInfo4J;
	}

	public void setMitemExtInfo4J(MItemExtInfo4J mitemExtInfo4J) {
		this.mitemExtInfo4J = mitemExtInfo4J;
	}

	public long getMinPrice() {
		return minPrice;
	}
	public String getMinPriceStr() {
		return SumUtil.getFormattedCash(minPrice);
	}

	public void setMinPrice(long minPrice) {
		this.minPrice = minPrice;
	}

	public long getOriginalPrice() {
		return originalPrice;
	}

	public void setOriginalPrice(long originalPrice) {
		this.originalPrice = originalPrice;
	}

	public String getMainItemPicStr() {
		if(mitemExtInfo4J != null && mitemExtInfo4J.getItemPics().size()>0){
			return ItemUtil.getImgUrl(mitemExtInfo4J.getItemPics().get(0).getPicUrl(),DEFAULT_INDEX_PAGE_PIC_SIZE);
		}else{
			return ItemUtil.getImgUrl(mainItemPic,DEFAULT_INDEX_PAGE_PIC_SIZE);
	    }
	}
	public boolean getHasIntervalPrice() {
		return maxPrice>minPrice && minPrice>0 ;
	}
	public boolean getHasNormalIntervalPrice() {
		return normalMaxPrice>normalMinPrice && normalMinPrice>0 ;
	}
	public void setMainItemPic(String mainItemPic) {
		this.mainItemPic = mainItemPic;
	}

	public long getRedPacketAvariable() {
		return redPacketAvariable;
	}

	public void setRedPacketAvariable(long redPacketAvariable) {
		this.redPacketAvariable = redPacketAvariable;
	}

	public long getSellerUin() {
		return sellerUin;
	}

	public void setSellerUin(long sellerUin) {
		this.sellerUin = sellerUin;
	}

	public String getMainItemPic() {
		return mainItemPic;
	}

	public boolean isSupportCod() {
		return supportCod;
	}

	public void setSupportCod(boolean supportCod) {
		this.supportCod = supportCod;
	}

	public boolean isQgoItem() {
		return qgoItem;
	}

	public void setQgoItem(boolean qgoItem) {
		this.qgoItem = qgoItem;
	}

	public boolean isFree4Freight() {
		return free4Freight;
	}

	public void setFree4Freight(boolean free4Freight) {
		this.free4Freight = free4Freight;
	}

	public int getPicNum() {
		return picNum;
	}

	public void setPicNum(int picNum) {
		this.picNum = picNum;
	}

	public long getStockCount() {
		return stockCount;
	}

	public void setStockCount(long stockCount) {
		this.stockCount = stockCount;
	}

	public String getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(String discountPrice) {
		this.discountPrice = discountPrice;
	}

	public int getClassId() {
		return classId;
	}

	public void setClassId(int classId) {
		this.classId = classId;
	}


	public Pager<ItemPic> getPicPage() {
		return picPage;
	}

	public void setPicPage(Pager<ItemPic> picPage) {
		this.picPage = picPage;
	}

	public List<ItemBO> getAssociateItemList() {

		return associateItemList;
	}

	public void setAssociateItemList(List<ItemBO> associateItemList) {
		this.associateItemList = associateItemList;
	}

	public boolean isQuickBuy() {
		return quickBuy;
	}

	public void setQuickBuy(boolean quickBuy) {
		this.quickBuy = quickBuy;
	}

	public Map<String, List<SaleAttrPair>> getViewSkuSaleAttrPoForQuickBuy() {
		return viewSkuSaleAttrPoForQuickBuy;
	}

	public void setViewSkuSaleAttrPoForQuickBuy(
			Map<String, List<SaleAttrPair>> viewSkuSaleAttrPoForQuickBuy) {
		this.viewSkuSaleAttrPoForQuickBuy = viewSkuSaleAttrPoForQuickBuy;
	}

	public ItemStock getItemStock() {
		return itemStock;
	}

	public void setItemStock(ItemStock itemStock) {
		this.itemStock = itemStock;
	}

	public List<Mstock4J> getStockList() {
		return stockList;
	}

	public void setStockList(List<Mstock4J> stockList) {
		this.stockList = stockList;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public boolean isSupportPCCod() {
		return supportPCCod;
	}

	public void setSupportPCCod(boolean supportPCCod) {
		this.supportPCCod = supportPCCod;
	}

	public boolean isSupportPromotion() {
		return  supportPromotion;
	}

	public void setSupportPromotion(boolean supportPromotion) {
		this.supportPromotion = supportPromotion;
	}

	public long getNormalPrice() {
		return normalPrice;
	}
	public String getNormalPriceStr() {
		return SumUtil.getFormattedCash(normalPrice);
	}

	public void setNormalPrice(long normalPrice) {
		this.normalPrice = normalPrice;
	}

	public long getNormalMaxPrice() {
		return normalMaxPrice;
	}

	public void setNormalMaxPrice(long normalMaxPrice) {
		this.normalMaxPrice = normalMaxPrice;
	}

	public long getNormalMinPrice() {
		return normalMinPrice;
	}

	public void setNormalMinPrice(long normalMinPrice) {
		this.normalMinPrice = normalMinPrice;
	}
	
	public String getDisplayPrice(){
		if(getHasIntervalPrice()){
			return getMinPriceStr() +"-" +getMaxPriceStr();
		}else{
			return getPriceStr();
		}		
	}
	
	public String getDisplayNormalPrice(){
		if(getHasNormalIntervalPrice()){
			return SumUtil.getFormattedCash(getNormalMinPrice()) +"-" +SumUtil.getFormattedCash(getNormalMaxPrice());
		}else{
			return SumUtil.getFormattedCash(normalPrice);
		}		
	}
	public List<ItemAttrBO> getCurrPageAttrInfo() {
		if(parsedAttrList!=null){			
			int end =parsedAttrList.size()<=ATTR_INFO_SIZE?parsedAttrList.size():ATTR_INFO_SIZE;
			if(pn==1){
				return  parsedAttrList.subList(0, end);
			}else{
				return  parsedAttrList.subList((pn-1)*ATTR_INFO_SIZE, getParsedAttrList().size());//第二页展示完所有属性
			}
		}
		
		return null;
	}
	public int getAttrInfoSize() {
		return ATTR_INFO_SIZE;
	}

	public void setParsedAttrList(List<ItemAttrBO> parsedAttrList) {
		this.parsedAttrList = parsedAttrList;
	}

	public List<ItemAttrBO> getParsedAttrList() {
		return parsedAttrList;
	}

	public int getPn() {
		return pn;
	}

	public void setPn(int pn) {
		this.pn = pn;
	}

	public long getSavingFee() {
		return savingFee;
	}

	public void setSavingFee(long savingFee) {
		this.savingFee = savingFee;
	}
	public String getSavingFeeStr() {
		return SumUtil.getFormattedCash(savingFee);
	}
	/**
	 * 是否属于6月大促商品
	 * @return
	 * @date:2013-5-27
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
	public boolean getItemForJunePromotion(){
		return JunePromotionManager.validateItemExist(itemCode);
	}
	/**
	 * 是否属于6月大促商品
	 * @return
	 * @date:2013-5-27
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
	public boolean getSpecialItem(){
		PromotionItem pp=JunePromotionManager.validateSpecialItem(itemCode);
		return pp!=null && pp.getIc() !=null;
	}
	
	public boolean getSpecialActNotBegin(){
		PromotionItem pp=JunePromotionManager.validateSpecialItem(itemCode);
		if(pp!=null){
			return  pp.validateSpecialItemNotBegin();
		}
		return false;
	}
	
	/**
	 * 活动是否有效
	 * @return
	 * @date:2013-5-27
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
	public  boolean getActEffective() {
		return false;
		//return JunePromotionManager.validateActEffective();
	}

	public int getTuanPepole() {
		return tuanPepole;
	}

	public void setTuanPepole(int tuanPepole) {
		this.tuanPepole = tuanPepole;
	}
}
