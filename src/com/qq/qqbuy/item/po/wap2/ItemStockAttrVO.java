package com.qq.qqbuy.item.po.wap2;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.qq.qqbuy.common.util.SumUtil;
import com.qq.qqbuy.item.po.ItemStock;
import com.qq.qqbuy.item.po.Mstock4J;
import com.qq.qqbuy.item.po.SaleAttrPair;

public class ItemStockAttrVO  {
	
	private String itemName;
	private List<Mstock4J> mstockList=null;
	private List<Mstock4J> twoStepStockList=null;
	private Map<String, List<Mstock4J>> viewSkuSaleAttrPo = new LinkedHashMap<String, List<Mstock4J>>();//单页下单时库存展示项
	private Map<String, List<SaleAttrPair>> viewSaleAttr = new LinkedHashMap<String, List<SaleAttrPair>>();//库存属性排列项
	private ItemStock itemStock;
	private String itemCode;
	private long price;
	private  String confirmOrderURL;
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public List<Mstock4J> getMstockList() {
		return mstockList;
	}
	public void setMstockList(List<Mstock4J> mstockList) {
		this.mstockList = mstockList;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public List<Mstock4J> getTwoStepStockList() {
		return twoStepStockList;
	}
	public void setTwoStepStockList(List<Mstock4J> twoStepStockList) {
		this.twoStepStockList = twoStepStockList;
	}

	public ItemStock getItemStock() {
		return itemStock;
	}
	public void setItemStock(ItemStock itemStock) {
		this.itemStock = itemStock;
	}
	public Map<String, List<Mstock4J>> getViewSkuSaleAttrPo() {
		return viewSkuSaleAttrPo;
	}
	public void setViewSkuSaleAttrPo(Map<String, List<Mstock4J>> viewSkuSaleAttrPo) {
		this.viewSkuSaleAttrPo = viewSkuSaleAttrPo;
	}
	public Map<String, List<SaleAttrPair>> getViewSaleAttr() {
		return viewSaleAttr;
	}
	public void setViewSaleAttr(Map<String, List<SaleAttrPair>> viewSaleAttr) {
		this.viewSaleAttr = viewSaleAttr;
	}

	public long getPrice() {
		return price;
	}
	public String getPriceStr() {
		return SumUtil.getFormattedCash(price);
	}
	public void setPrice(long price) {
		this.price = price;
	}
	public String getConfirmOrderURL() {
		return confirmOrderURL;
	}
	public void setConfirmOrderURL(String confirmOrderURL) {
		this.confirmOrderURL = confirmOrderURL;
	}
}
