package com.qq.qqbuy.item.po.wap2;

import java.util.ArrayList;

import com.qq.qqbuy.item.po.ItemAttrBO;


/**
 * 商品属性页展示
 * 
 * @author jiaqiangxu
 * 
 */
public class ItemPropVO extends  ItemDetailVO{
	private int pn=1;
	private int totalPage;
	//private final  int DETAIL_INFO_SIZE=200;

//	public ArrayList<ItemAttrBO> getCurrPageAttrInfo() {
//		if(getParsedAttrList()!=null){			
//			int end =getParsedAttrList().size()<=ATTR_INFO_SIZE?getParsedAttrList().size():ATTR_INFO_SIZE;
//			if(pn==1){
//				return (ArrayList<ItemAttrBO>) getParsedAttrList().subList(0, end);//第一页最多展示ATTR_INFO_SIZE个属性
//			}else{
//				return (ArrayList<ItemAttrBO>) getParsedAttrList().subList((pn-1*ATTR_INFO_SIZE), getParsedAttrList().size());//第二页展示完所有属性
//			}
//		}
//		return null;
//	}
   
	public int getTotalPage() {	
		
		return getParsedAttrList()==null ||getParsedAttrList().size()<=ATTR_INFO_SIZE?1:2;
//		int len=getParsedAttrList()!=null?getParsedAttrList().size():0;
//		totalPage=ItemUtil.getTotalPage(len, ATTR_INFO_SIZE);
//		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getPn() {
		return pn;
	}

	public void setPn(int pn) {
		this.pn = pn;
	}
	

}
