package com.qq.qqbuy.item.po.wap2;

import com.qq.qqbuy.common.Pager;
import com.qq.qqbuy.item.po.ItemPic;



public class ItemPicVO extends ItemDetailVO {
	
	private Pager<ItemPic> picPage=new Pager<ItemPic>();

	public Pager<ItemPic> getPicPage() {
		return picPage;
	}

	public void setPicPage(Pager<ItemPic> picPage) {
		this.picPage = picPage;
	}




}
