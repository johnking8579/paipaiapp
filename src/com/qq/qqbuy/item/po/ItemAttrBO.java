package com.qq.qqbuy.item.po;

import java.util.ArrayList;
import java.util.List;

import com.qq.qqbuy.common.util.StringUtil;

public class ItemAttrBO
{
  private long attrId;
  private String attrName;
  private List<ItemAttrOptionBO> attrOptionList = new ArrayList<ItemAttrOptionBO>();

  public long getAttrId() {
    return this.attrId;
  }
  public String getAttrOptionDisplay(){
		StringBuilder s=new StringBuilder();
		if(attrOptionList!=null){
			for(int i=0;i<attrOptionList.size();i++){
				ItemAttrOptionBO attrOption=attrOptionList.get(i);
				if(attrOption!=null && StringUtil.isNotEmpty(attrOption.getAttrOptionName())){
					s.append(attrOption.getAttrOptionName());
					if(i!=attrOptionList.size()-1){
						s.append("、");
					}
				}					
			}
		}
		return s.toString();
	}
	
  public String getAttrIdHexStr() {
    return Long.toHexString(this.attrId);
  }

  public void setAttrId(long attrId) {
    this.attrId = attrId;
  }

  public String getAttrName() {
    return this.attrName;
  }

  public void setAttrName(String attrName) {
    this.attrName = attrName;
  }

  public List<ItemAttrOptionBO> getAttrOptionList() {
    return this.attrOptionList;
  }

  public void setAttrOptionList(List<ItemAttrOptionBO> attrOptionList) {
    this.attrOptionList = attrOptionList;
  }
}