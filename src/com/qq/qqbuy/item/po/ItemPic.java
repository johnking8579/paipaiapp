package com.qq.qqbuy.item.po;

import com.paipai.util.string.StringUtil;
import com.qq.qqbuy.item.util.ItemUtil;

public class ItemPic {
	
	/**
	 * 图片序号
	 * 
	 * 版本 >= 0
	 */
	private int picIndex;

	/**
	 * 图片URL
	 * 
	 * 版本 >= 0
	 */
	private String picUrl = new String();

	/**
	 * 图片描述
	 * 
	 * 版本 >= 0
	 */
	private String picDesc = new String();

	public int getPicIndex() {
		return picIndex;
	}

	public void setPicIndex(int picIndex) {
		this.picIndex = picIndex;
	}

	public String getPicUrl() {
		return picUrl;
	}
	public String getPicUrlStr() {
		if(picIndex==0){
			return ItemUtil.getImgUrl(picUrl,"200x200");
		}
		return ItemUtil.getImgUrl(picUrl,"300x300");
	}
	public String getPicUrl80() {
		return ItemUtil.getImgUrl(picUrl,"80x80");
	}
	public String getPicUrl160() {
		return ItemUtil.getImgUrl(picUrl,"160x160");
	}
	public boolean getQgoItemPic() {
		return StringUtil.isNotEmpty(picUrl)&&picUrl.contains("mimg");
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getPicDesc() {
		return picDesc;
	}

	public void setPicDesc(String picDesc) {
		this.picDesc = picDesc;
	}

}
