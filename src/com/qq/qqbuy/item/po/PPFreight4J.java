package com.qq.qqbuy.item.po;

public class PPFreight4J {
	/**
	 * 首件运费
	 * 
	 * 版本 >= 0
	 */
	private int priceNormal;

	/**
	 * 每增加一见的运费
	 * 
	 * 版本 >= 0
	 */
	private int priceNormalAdd;

	public int getPriceNormal() {
		return priceNormal;
	}

	public void setPriceNormal(int priceNormal) {
		this.priceNormal = priceNormal;
	}

	public int getPriceNormalAdd() {
		return priceNormalAdd;
	}

	public void setPriceNormalAdd(int priceNormalAdd) {
		this.priceNormalAdd = priceNormalAdd;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PPFreight4J [priceNormal=").append(priceNormal).append(
				", priceNormalAdd=").append(priceNormalAdd).append("]");
		return builder.toString();
	}
}
