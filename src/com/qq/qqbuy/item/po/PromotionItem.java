package com.qq.qqbuy.item.po;

import java.util.Date;

import com.qq.qqbuy.common.util.DateUtils;

/**
 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
 */
public class PromotionItem {
	
	private String ic;
	private String beginTime;
	private String endTime;
	public String getIc() {
		return ic;
	}
	public void setIc(String ic) {
		this.ic = ic;
	}
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public boolean validateSpecialItemNotBegin(){
		Date now = new Date();
		try {
			Date start = DateUtils.parseStringToDate(getBeginTime(),
					DateUtils.FORMAT_YMD_HMS);
			return now.before(start) ;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	


}

