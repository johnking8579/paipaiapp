package com.qq.qqbuy.item.po;

import java.util.Vector;

/**
 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
 */
public class ItemPriceExtPo4J {
	
	    private Vector<Long> colorPrice = new Vector<Long>();

		/**
		 * 活动价格
		 *
		 * 版本 >= 0
		 */
		 private long activityPrice;

		/**
		 * 活动类型
		 *
		 * 版本 >= 0
		 */
		 private long activityType;

		/**
		 * 活动信息描述
		 *
		 * 版本 >= 0
		 */
		 private String activityDesc = new String();

		/**
		 * 活动的当前状态:0-活动尚未开始，1-活动已经开始
		 *
		 * 版本 >= 0
		 */
		 private long state;

		/**
		 * 活动的开始时间，为uinxi_time_stamp的格式，单位为秒
		 *
		 * 版本 >= 0
		 */
		 private long beginTime;

		/**
		 * 活动的结束时间，为uinxi_time_stamp的格式，单位为秒
		 *
		 * 版本 >= 0
		 */
		 private long endTime;

		/**
		 * 店铺VIP价格列表,如果商品不支持店铺VIP，则本Vector的为空；如果支持店铺VIP，则本Vector的size应为4，分别保存会员价，黄金会员价，白金会员价，钻石会员价
		 *
		 * 版本 >= 0
		 */
		 private Vector<Long> shopVipPrice = new Vector<Long>();

		/**
		 * 活动信息是否需要显示
		 *
		 * 版本 >= 0
		 */
		 private boolean isActivityNeedShow;

		/**
		 * 活动属性信息
		 *
		 * 版本 >= 0
		 */
		 private long activeProperty;

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("ItemPriceExtPo4J [activeProperty=").append(
					activeProperty).append(", activityDesc=").append(
					activityDesc).append(", activityPrice=").append(
					activityPrice).append(", activityType=").append(
					activityType).append(", beginTime=").append(beginTime)
					.append(", colorPrice=").append(colorPrice).append(
							", endTime=").append(endTime).append(
							", isActivityNeedShow=").append(isActivityNeedShow)
					.append(", shopVipPrice=").append(shopVipPrice).append(
							", state=").append(state).append("]");
			return builder.toString();
		}

		public Vector<Long> getColorPrice() {
			return colorPrice;
		}

		public void setColorPrice(Vector<Long> colorPrice) {
			this.colorPrice = colorPrice;
		}

		public long getActivityPrice() {
			return activityPrice;
		}

		public void setActivityPrice(long activityPrice) {
			this.activityPrice = activityPrice;
		}

		public long getActivityType() {
			return activityType;
		}

		public void setActivityType(long activityType) {
			this.activityType = activityType;
		}

		public String getActivityDesc() {
			return activityDesc;
		}

		public void setActivityDesc(String activityDesc) {
			this.activityDesc = activityDesc;
		}

		public long getState() {
			return state;
		}

		public void setState(long state) {
			this.state = state;
		}

		public long getBeginTime() {
			return beginTime;
		}

		public void setBeginTime(long beginTime) {
			this.beginTime = beginTime;
		}

		public long getEndTime() {
			return endTime;
		}

		public void setEndTime(long endTime) {
			this.endTime = endTime;
		}

		public Vector<Long> getShopVipPrice() {
			return shopVipPrice;
		}

		public void setShopVipPrice(Vector<Long> shopVipPrice) {
			this.shopVipPrice = shopVipPrice;
		}

		public boolean isActivityNeedShow() {
			return isActivityNeedShow;
		}

		public void setActivityNeedShow(boolean isActivityNeedShow) {
			this.isActivityNeedShow = isActivityNeedShow;
		}

		public long getActiveProperty() {
			return activeProperty;
		}

		public void setActiveProperty(long activeProperty) {
			this.activeProperty = activeProperty;
		}

		

}

