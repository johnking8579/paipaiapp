package com.qq.qqbuy.item.po;

import java.net.URLEncoder;

import com.qq.qqbuy.common.util.StringUtil;

/**
 * 用于封装库存属性的真实值和描述值
 * 
 * @author winsonwu
 * @Created 2012-2-24
 */
public class SaleAttrPair {
	private String attr = "";//用于展示,也用于区分标识唯一属性
	private long order;
	private int state = 0; // 0=缺货, 1=有货
	private int curr = 0; // 0=非当前选中, 1=当前选中
	private int currState = 0; // 0=缺货, 1=有货
	private String stockId = new String();
	private String stockAttr = new String();//真实完整的库存属性窜

	public SaleAttrPair(String attr, String order) {
		this.attr = attr;
		this.order = StringUtil.toLong(order, 0);
	}

	public SaleAttrPair(String attr) {
		this.attr = attr;
	}

	public void setCurrTrue() {
		setCurr(1);
	}

	public void setCurrFalse() {
		setCurr(0);
	}

	public void setStateShouldSell() {
		setState(1);
	}

	public void setStateNoSell() {
		setState(0);
	}

	public void setCurrStateShouldSell() {
		setCurrState(1);
	}

	public void setCurrStateNoSell() {
		setCurrState(0);
	}

	public String getAttr() {
		return attr;
	}

	public void setAttr(String attr) {
		this.attr = attr;
	}

	public long getOrder() {
		return order;
	}

	public void setOrder(long order) {
		this.order = order;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getCurr() {
		return curr;
	}

	public void setCurr(int curr) {
		this.curr = curr;
	}

	public int getCurrState() {
		return currState;
	}

	public void setCurrState(int currState) {
		this.currState = currState;
	}

	public boolean isEquals(String attr) {
		if (this.attr != null && this.attr.equals(attr)) {
			return true;
		}
		return false;
	}

	public boolean equals(Object other) { // 重写equals方法，后面最好重写hashCode方法
		if (this == other) // 先检查是否其自反性，后比较other是否为空。这样效率高
			return true;
		if (other == null)
			return false;
		SaleAttrPair t = (SaleAttrPair) other;
		return getAttr().equals(t.getAttr());
	}
	public int hashCode(){                 //hashCode主要是用来提高hash系统的查询效率。当hashCode中不进行任何操作时，可以直接让其返回 一常数，或者不进行重写。
		  int result = getAttr().hashCode();
		  result = 29 * result;
		  return result;
		  //return 0;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SaleAttrPair [attr=");
		builder.append(attr);
		builder.append(", curr=");
		builder.append(curr);
		builder.append(", order=");
		builder.append(order);
		builder.append(", state=");
		builder.append(state);
		builder.append("]");
		return builder.toString();
	}

	public String getStockId() {
		return stockId;
	}

	public void setStockId(String stockId) {
		this.stockId = stockId;
	}

	public String getStockAttr() {
		return stockAttr;
	}
	public String getStockAttrEncoded() {
		if(StringUtil.isNotEmpty(stockAttr)){
			try {
				return URLEncoder.encode(stockAttr, "utf-8");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return stockAttr;	
	}
	public String getStockAttrStr() {
		String[] attr = StringUtil.split(stockAttr, "|");
		if(attr!=null ){
			String att=null;
			if(attr.length>1){
				att= attr[1];	
			}else{
				att= attr[0];
			}	
		    return att;
		}
		return "";

	}
	public void setStockAttr(String stockAttr) {
		this.stockAttr = stockAttr;
	}

}
