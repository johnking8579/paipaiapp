package com.qq.qqbuy.item.po;

public class ShopItemInfo {
	/**
	 * 商品ID
	 */
	private String itemCode = "";
	
		
	/**
	 * 商品图片url
	 */
	 private String imgUrl = "";
	 
	 private String itemName = "";
	 
	 private long price ;
	 
	 private long buyNum ;
	 
	 private long currentBuyNum ;
	 
	 

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	

	public long getBuyNum() {
		return buyNum;
	}

	public void setBuyNum(long buyNum) {
		this.buyNum = buyNum;
	}

	public long getCurrentBuyNum() {
		return currentBuyNum;
	}

	public void setCurrentBuyNum(long currentBuyNum) {
		this.currentBuyNum = currentBuyNum;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}	

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	 
}
