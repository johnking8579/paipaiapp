package com.qq.qqbuy.item.po;

import java.util.Vector;

import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.thirdparty.idl.item.protocol.qgo.ItemPriceExtPo;

public class MItem4J {

	/**
	 * 商品编码
	 * 
	 * 版本 >= 0
	 */
	private String itemCode = new String();

	/**
	 * 是否包邮
	 * 
	 * 版本 >= 0
	 */
	private boolean isFree4Freight;

	/**
	 * 多买立减金额
	 * 
	 * 版本 >= 0
	 */
	private int savingFee;

	/**
	 * 在线支付立减金额
	 * 
	 * 版本 >= 0
	 */
	private int additionalFee;

	/**
	 * 是否支持货到付款
	 * 
	 * 版本 >= 0
	 */
	private boolean supportCOD;

	/**
	 * 卖家类型
	 * 
	 * 版本 >= 0
	 */
	private int sellerType;

	/**
	 * 商品价格,成本分摊后的
	 * 
	 * 版本 >= 0
	 */
	private int itemPrice;
	
	/**
	 * 商品正常销售价格,非活动价,成本分摊后的
	 * 
	 * 版本 >= 0
	 */
	private int normalPrice;

	/**
	 * 商品价格,成本分摊后的
	 * 
	 * 版本 >= 0
	 */
	private int mailFee;

	/**
	 * 商品价格,成本分摊后的
	 * 
	 * 版本 >= 0
	 */
	private int mailFeeAdd;

	/**
	 * 商品1~6级彩钻价格
	 * 
	 * 版本 >= 0
	 */
	private Vector<Integer> discountPrice = new Vector<Integer>();

	/**
	 * 库存信息
	 * 
	 * 版本 >= 0
	 */
	private Vector<Mstock4J> stockList = new Vector<Mstock4J>();
	
	private ItemPriceExtPo4J oPriceExt = new ItemPriceExtPo4J();

	
	/**
	 * 获取该商品某一属性多件时的总货到付款金额
	 * @param itemCount
	 * @param stockAttr
	 * @return
	 * @throws BusinessException
	 */
	public long caculateTotalCodFee(int itemCount,String stockAttr) throws BusinessException{
		Mstock4J stock = getCurrentStock(stockAttr);
		if (stock == null) {
			 throw BusinessException.createInstance(BusinessErrorType.INVALID_STOCKATTR);
		}
		long totalFee = stock.getStockPrice() * itemCount - this.getSavingFee() * (itemCount -1);
		return totalFee;
	}
	
	/**
	 * 获取商品属性
	 * @param stockAttr
	 * @return
	 */
	public Mstock4J getCurrentStock(String stockAttr) {
		for (Mstock4J stock : this.getStockList()) {
			if (stock.getStockAttr().equals(stockAttr)){
				return stock;
			}
		}
		return null;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public boolean isFree4Freight() {
		return isFree4Freight;
	}

	public void setFree4Freight(boolean isFree4Freight) {
		this.isFree4Freight = isFree4Freight;
	}

	public int getSavingFee() {
		return savingFee;
	}

	public void setSavingFee(int savingFee) {
		this.savingFee = savingFee;
	}

	public int getAdditionalFee() {
		return additionalFee;
	}

	public void setAdditionalFee(int additionalFee) {
		this.additionalFee = additionalFee;
	}

	public boolean isSupportCOD() {
		return supportCOD;
	}

	public void setSupportCOD(boolean supportCOD) {
		this.supportCOD = supportCOD;
	}

	public int getSellerType() {
		return sellerType;
	}

	public void setSellerType(int sellerType) {
		this.sellerType = sellerType;
	}

	public int getItemPrice() {
		return itemPrice;
	}
	/**
	 * 通过等级获取折扣减免价格
	 * @param level
	 * @return
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 */
	public long getStockDiscountPrice(int level){
			if(discountPrice ==null){
				return 0;
			}
			if(discountPrice.size() >= level){ 
				return (int)itemPrice - discountPrice.get(level-1);
			}			
		  return 0;		
	}
	/**
	 * 通过等级获取折扣后的价格
	 * @param level
	 * @return
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 */
	public long getStockAfterDiscountPrice(int level){
			if(discountPrice ==null){
				return itemPrice;
			}
			if(discountPrice.size() >= level){ 
				return discountPrice.get(level-1);
			}			
		  return itemPrice;		
	}
	public void setItemPrice(int itemPrice) {
		this.itemPrice = itemPrice;
	}

	public int getMailFee() {
		return mailFee;
	}

	public void setMailFee(int mailFee) {
		this.mailFee = mailFee;
	}

	public int getMailFeeAdd() {
		return mailFeeAdd;
	}

	public void setMailFeeAdd(int mailFeeAdd) {
		this.mailFeeAdd = mailFeeAdd;
	}

	public Vector<Integer> getDiscountPrice() {
		return discountPrice;
	}
	/**
	 * 获取各等级折扣减免的价格列表
	 * @return
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 */
	public Vector<Integer> getDiscountReducePrice() {
		 Vector<Integer> discountReducePrice = new Vector<Integer>();
         if(discountPrice!=null && discountPrice.size()>0){
    	   for(Integer price:discountPrice){
   			discountReducePrice.add((int)itemPrice-price);
   		   } 
         }		
	    return discountReducePrice;
	}

	public void setDiscountPrice(Vector<Integer> discountPrice) {
		this.discountPrice = discountPrice;
	}

	public Vector<Mstock4J> getStockList() {
		return stockList;
	}
	public boolean getHasIntervalPrice() {
		for(Mstock4J stock : stockList){
			Integer price = (int)stock.getStockPrice();
			if(itemPrice !=price ){
			 return true;
			}						
		}
		return false;
	}
	public long getMinPrice() {
		long minprice=0;
		for(Mstock4J stock : stockList){
			Integer price = (int)stock.getStockPrice();				
			if(minprice > price || minprice == 0){
			  minprice = price;
			}						
		}
		return minprice;
	}
	public long getMaxPrice() {
		long maxprice=0;
		for(Mstock4J stock : stockList){
			Integer price = (int)stock.getStockPrice();				
			if(maxprice < price ){
				maxprice = price;
			}						
		}
		return maxprice;
	}
	public void setStockList(Vector<Mstock4J> stockList) {
		this.stockList = stockList;
	}
	public long caculateTotalCodFee(String stockAttr,int itemCount) throws BusinessException {
		Mstock4J mstock=null;
		for (Mstock4J stock : getStockList()) {
			if (stock.getStockAttr().equals(stockAttr)){
				mstock=stock;
				break;
			}
		}
		if (mstock == null) {
			 throw BusinessException.createInstance(BusinessErrorType.INVALID_STOCKATTR);
		}
		return mstock.getStockPrice() * itemCount - additionalFee * (itemCount -1);
	}


	public ItemPriceExtPo4J getoPriceExt() {
		return oPriceExt;
	}

	public void setoPriceExt(ItemPriceExtPo4J oPriceExt) {
		this.oPriceExt = oPriceExt;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MItem4J [additionalFee=").append(additionalFee).append(
				", discountPrice=").append(discountPrice).append(
				", isFree4Freight=").append(isFree4Freight).append(
				", itemCode=").append(itemCode).append(", itemPrice=").append(
				itemPrice).append(", mailFee=").append(mailFee).append(
				", mailFeeAdd=").append(mailFeeAdd).append(", oPriceExt=")
				.append(oPriceExt).append(", savingFee=").append(savingFee)
				.append(", sellerType=").append(sellerType).append(
						", stockList=").append(stockList).append(
						", supportCOD=").append(supportCOD).append("]");
		return builder.toString();
	}

	public int getNormalPrice() {
		return normalPrice;
	}

	public void setNormalPrice(int normalPrice) {
		this.normalPrice = normalPrice;
	}


}
