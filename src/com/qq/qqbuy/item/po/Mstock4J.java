package com.qq.qqbuy.item.po;

import java.net.URLEncoder;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;

import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.SumUtil;
import com.qq.qqbuy.thirdparty.idl.item.protocol.qgo.ItemPriceExtPo;

public class Mstock4J {
	/**
	 * 商品库存id
	 *
	 * 版本 >= 0
	 */
	 private String stockId = new String();

	/**
	 * 商品库存价格
	 *
	 * 版本 >= 0
	 */
	 private long stockPrice;

	/**
	 * 商品的库存属性串
	 *
	 * 版本 >= 0
	 */
	 private String stockAttr = new String();

	/**
	 * 商品的该库存对应的销售数量
	 *
	 * 版本 >= 0
	 */
	 private int soldCount;

	/**
	 * 商品的库存数量
	 *
	 * 版本 >= 0
	 */
	 private int stockCount;

	/**
	 * 商品1~6级彩钻价格
	 *
	 * 版本 >= 0
	 */
	 private Vector<Integer> discountPrice = new Vector<Integer>();
	 
	 private String twoStepDisplayAttrValue;
	 /**
		 * 促销价
		 *
		 * 版本 >= 0
		 */
	private ItemPriceExtPo4J oPriceExt = new ItemPriceExtPo4J();
	
	/**
	 * 商品正常销售价格,非活动价,成本分摊后的
	 * 
	 * 版本 >= 0
	 */
	private int normalPrice;



	public String getStockId() {
		return stockId;
	}

	public void setStockId(String stockId) {
		this.stockId = stockId;
	}

	public long getStockPrice() {
		return stockPrice;
	}
	public String getStockPriceStr() {
		return SumUtil.getFormattedCash(stockPrice);
	}

	public void setStockPrice(long stockPrice) {
		this.stockPrice = stockPrice;
	}

	public String getStockAttr() {
		return stockAttr;
	}

	public void setStockAttr(String stockAttr) {
		this.stockAttr = stockAttr;
	}
	public String getStockAttrStr() {
		if(StringUtils.isNotEmpty(stockAttr)){
			String[] attr = StringUtil.split(stockAttr, "|");
			if (attr != null && attr.length>1 && stockAttr.length()>=attr[0].length()+1) {
				return stockAttr.substring(attr[0].length() + 1);
			}
		}
			return stockAttr;		
		
	}
	public String getOneStepStockAttrStr() {
		if(StringUtils.isNotEmpty(stockAttr)){
			String[] attr = StringUtil.split(stockAttr, "|");
			if (attr != null && attr.length>1 && stockAttr.length()>=attr[0].length()+1) {
				//String stocks= stockAttr.substring(attr[0].length() + 1);
				StringBuilder displayAtt=new StringBuilder();
				String[] stocks=StringUtil.split(stockAttr.substring(attr[0].length() + 1), "|");
				if(stocks!=null){
					for(String s:stocks){
						String[] att=s.split(":");
						if(att!=null && att.length==2){
							displayAtt.append(att[1]).append("|");
						}
					}
					if(displayAtt.toString().endsWith("|")){
						return displayAtt.substring(0,displayAtt.length()-1);
					}
					return displayAtt.toString();
				}
				
			}
		}
			return stockAttr;		
		
	}
	public String getStockAttrStrEncoded() {
		if(StringUtil.isNotEmpty(stockAttr)){
			try {
				return URLEncoder.encode(stockAttr, "utf-8");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return stockAttr;		
	}
		
	public int getSoldCount() {
		return soldCount;
	}

	public void setSoldCount(int soldCount) {
		this.soldCount = soldCount;
	}

	public int getStockCount() {
		return stockCount;
	}

	public void setStockCount(int stockCount) {
		this.stockCount = stockCount;
	}

	public Vector<Integer> getDiscountPrice() {
		return discountPrice;
	}
	/**
	 * 获取各等级折扣减免的价格列表
	 * @return
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 */
	public Vector<Integer> getDiscountReducePrice() {
		 Vector<Integer> discountReducePrice = new Vector<Integer>();
         if(discountPrice!=null && discountPrice.size()>0){
    	   for(Integer price:discountPrice){
   			discountReducePrice.add((int)stockPrice-price);
   		   } 
         }		
	    return discountReducePrice;
	}


	public void setDiscountPrice(Vector<Integer> discountPrice) {
		this.discountPrice = discountPrice;
	}
	/**
	 * 通过等级获取折扣减免价格
	 * @param level
	 * @return
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 */
	public long getStockDiscountPrice(int level){
		      if(discountPrice== null){
		    	  return 0;
		      }
			if(discountPrice.size()>= level){ 
				return (int)stockPrice - discountPrice.get(level-1);
			}			
		  return 0;		
	}
	/**
	 * 通过等级获取折扣后的价格
	 * @param level
	 * @return
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 */
	public long getStockAfterDiscountPrice(int level){
		   if(discountPrice== null){
		    	  return stockPrice;
		      }
			if(discountPrice.size() >= level){ 
				return discountPrice.get(level-1);
			}			
		  return stockPrice;		
	}
	public boolean equals(Object other) { // 重写equals方法，后面最好重写hashCode方法
		if (this == other) // 先检查是否其自反性，后比较other是否为空。这样效率高
			return true;
		if (other == null)
			return false;
		Mstock4J t = (Mstock4J) other;
		return getStockAttr().equals(t.getStockAttr());
	}
	public int hashCode(){                 //hashCode主要是用来提高hash系统的查询效率。当hashCode中不进行任何操作时，可以直接让其返回 一常数，或者不进行重写。
		  int result = getStockAttr().hashCode();
		  result = 29 * result;
		  return result;
		  //return 0;
		 }
	
	public static void main(String args[]){
		Mstock4J ff=new Mstock4J();
		ff.setStockAttr("2|3");
		System.out.println(ff.getStockAttrStr());
	}

	public String getTwoStepDisplayAttrValue() {
		return twoStepDisplayAttrValue;
	}

	public void setTwoStepDisplayAttrValue(String twoStepDisplayAttrValue) {
		this.twoStepDisplayAttrValue = twoStepDisplayAttrValue;
	}

	public ItemPriceExtPo4J getoPriceExt() {
		return oPriceExt;
	}

	public void setoPriceExt(ItemPriceExtPo4J oPriceExt) {
		this.oPriceExt = oPriceExt;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Mstock4J [discountPrice=").append(discountPrice)
				.append(", oPriceExt=").append(oPriceExt)
				.append(", soldCount=").append(soldCount)
				.append(", stockAttr=").append(stockAttr).append(
						", stockCount=").append(stockCount)
				.append(", stockId=").append(stockId).append(", stockPrice=")
				.append(stockPrice).append(", twoStepDisplayAttrValue=")
				.append(twoStepDisplayAttrValue).append("]");
		return builder.toString();
	}

	public int getNormalPrice() {
		return normalPrice;
	}

	public void setNormalPrice(int normalPrice) {
		this.normalPrice = normalPrice;
	}

	
}
