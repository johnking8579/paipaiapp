package com.qq.qqbuy.item.po;

public class PPStock4J {
	/**
	 * 商品库存id
	 * 
	 * 版本 >= 0
	 */
	private String stockId = new String();

	/**
	 * 商品库存价格
	 * 
	 * 版本 >= 0
	 */
	private int stockPrice;

	public String getStockId() {
		return stockId;
	}

	public void setStockId(String stockId) {
		this.stockId = stockId;
	}

	public int getStockPrice() {
		return stockPrice;
	}

	public void setStockPrice(int stockPrice) {
		this.stockPrice = stockPrice;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PPStock4J [stockId=").append(stockId).append(
				", stockPrice=").append(stockPrice).append("]");
		return builder.toString();
	}
}
