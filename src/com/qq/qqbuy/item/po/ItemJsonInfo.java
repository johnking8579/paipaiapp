package com.qq.qqbuy.item.po;

/**
 * 

 * @ClassName: ItemJsonInfo 

 * @Description: 看了又看方便显示 
 
 * @author lhn

 * @date 2014-12-8 上午11:56:41 

 *
 */
public class ItemJsonInfo{

	private String strCommodityId;
	private String strTitle;
	private String ddwSellerUin;
	private String dwPrice;
	private String dwMarketPrice;
	private String dwActivePrice;
	private String dwBeginTime;
	private String dwEndTime;
	private String dwWdPriceValue;
	private String dwWdPriceChange;
	private String dwLeafClassId;
	private String dwOrderNum;
	private String dwState;
	private String dwStock;
	private String strItemUrl;
	private String strItemPic;
	private String strShopName;
	private String strShopUrl;
	private String strShopPic;
	private String strMaterialId;
	private String strMaterialPic;
	private String strMaterialUrl;
	private String strBeginTuanUrl;
	private String strTuanText;
	private String dwTuanNum;
	private String strDap;
	
	
	public String getStrCommodityId() {
		return strCommodityId;
	}
	public void setStrCommodityId(String strCommodityId) {
		this.strCommodityId = strCommodityId;
	}
	public String getStrTitle() {
		return strTitle;
	}
	public void setStrTitle(String strTitle) {
		this.strTitle = strTitle;
	}
	public String getDdwSellerUin() {
		return ddwSellerUin;
	}
	public void setDdwSellerUin(String ddwSellerUin) {
		this.ddwSellerUin = ddwSellerUin;
	}
	public String getDwPrice() {
		return dwPrice;
	}
	public void setDwPrice(String dwPrice) {
		this.dwPrice = dwPrice;
	}
	public String getDwMarketPrice() {
		return dwMarketPrice;
	}
	public void setDwMarketPrice(String dwMarketPrice) {
		this.dwMarketPrice = dwMarketPrice;
	}
	public String getDwActivePrice() {
		return dwActivePrice;
	}
	public void setDwActivePrice(String dwActivePrice) {
		this.dwActivePrice = dwActivePrice;
	}
	public String getDwBeginTime() {
		return dwBeginTime;
	}
	public void setDwBeginTime(String dwBeginTime) {
		this.dwBeginTime = dwBeginTime;
	}
	public String getDwEndTime() {
		return dwEndTime;
	}
	public void setDwEndTime(String dwEndTime) {
		this.dwEndTime = dwEndTime;
	}
	public String getDwWdPriceValue() {
		return dwWdPriceValue;
	}
	public void setDwWdPriceValue(String dwWdPriceValue) {
		this.dwWdPriceValue = dwWdPriceValue;
	}
	public String getDwWdPriceChange() {
		return dwWdPriceChange;
	}
	public void setDwWdPriceChange(String dwWdPriceChange) {
		this.dwWdPriceChange = dwWdPriceChange;
	}
	public String getDwLeafClassId() {
		return dwLeafClassId;
	}
	public void setDwLeafClassId(String dwLeafClassId) {
		this.dwLeafClassId = dwLeafClassId;
	}
	public String getDwOrderNum() {
		return dwOrderNum;
	}
	public void setDwOrderNum(String dwOrderNum) {
		this.dwOrderNum = dwOrderNum;
	}
	public String getDwState() {
		return dwState;
	}
	public void setDwState(String dwState) {
		this.dwState = dwState;
	}
	public String getDwStock() {
		return dwStock;
	}
	public void setDwStock(String dwStock) {
		this.dwStock = dwStock;
	}
	public String getStrItemUrl() {
		return strItemUrl;
	}
	public void setStrItemUrl(String strItemUrl) {
		this.strItemUrl = strItemUrl;
	}
	public String getStrItemPic() {
		return strItemPic;
	}
	public void setStrItemPic(String strItemPic) {
		this.strItemPic = strItemPic;
	}
	public String getStrShopName() {
		return strShopName;
	}
	public void setStrShopName(String strShopName) {
		this.strShopName = strShopName;
	}
	public String getStrShopUrl() {
		return strShopUrl;
	}
	public void setStrShopUrl(String strShopUrl) {
		this.strShopUrl = strShopUrl;
	}
	public String getStrShopPic() {
		return strShopPic;
	}
	public void setStrShopPic(String strShopPic) {
		this.strShopPic = strShopPic;
	}
	public String getStrMaterialId() {
		return strMaterialId;
	}
	public void setStrMaterialId(String strMaterialId) {
		this.strMaterialId = strMaterialId;
	}
	public String getStrMaterialPic() {
		return strMaterialPic;
	}
	public void setStrMaterialPic(String strMaterialPic) {
		this.strMaterialPic = strMaterialPic;
	}
	public String getStrMaterialUrl() {
		return strMaterialUrl;
	}
	public void setStrMaterialUrl(String strMaterialUrl) {
		this.strMaterialUrl = strMaterialUrl;
	}
	public String getStrBeginTuanUrl() {
		return strBeginTuanUrl;
	}
	public void setStrBeginTuanUrl(String strBeginTuanUrl) {
		this.strBeginTuanUrl = strBeginTuanUrl;
	}
	public String getStrTuanText() {
		return strTuanText;
	}
	public void setStrTuanText(String strTuanText) {
		this.strTuanText = strTuanText;
	}
	public String getDwTuanNum() {
		return dwTuanNum;
	}
	public void setDwTuanNum(String dwTuanNum) {
		this.dwTuanNum = dwTuanNum;
	}
	public String getStrDap() {
		return strDap;
	}
	public void setStrDap(String strDap) {
		this.strDap = strDap;
	}
	
	
}
