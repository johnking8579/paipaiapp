package com.qq.qqbuy.item.po;

public class ItemAttrOptionBO
{
  private long attrOptionId;
  private String attrOptionName;
  private long attrId;

  public String getAttrOptionIdHexStr()
  {
    return Long.toHexString(this.attrOptionId);
  }

  public long getAttrOptionId() {
    return this.attrOptionId;
  }

  public void setAttrOptionId(long attrOptionId) {
    this.attrOptionId = attrOptionId;
  }

  public String getAttrOptionName() {
    return this.attrOptionName;
  }

  public void setAttrOptionName(String attrOptionName) {
    this.attrOptionName = attrOptionName;
  }

  public long getAttrId() {
    return this.attrId;
  }

  public void setAttrId(long attrId) {
    this.attrId = attrId;
  }
}