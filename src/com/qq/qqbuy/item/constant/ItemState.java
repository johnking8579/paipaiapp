package com.qq.qqbuy.item.constant;

public enum ItemState
{
  ALL(0L, "内部使用，所有状态", ""), 
  IN_STORE(1L, "用户下架,发布到仓库中", "IS_IN_STORE"), 
  IN_BIN(2L, "一口价出售中", "IS_FOR_SALE"), 
  IN_AUCTION_NO_BID(3L, "竞拍未出价", "IS_FOR_SALE"), 
  IN_AUCTION_BID(4L, "竞拍中，已出价", "IS_FOR_SALE"), 
  FAIL_AUCTION(5L, "流拍，到期下架", "IS_IN_STORE"), 
  SOLD(6L, "售完", "IS_SOLD_OUT"), 
  IN_SELLING(7L, "销售中商品，组合状态", "IS_FOR_SALE"), 
  IN_STORE_OR_FAIL_AUCTION(8L, "发布到仓库或流拍到仓库，组合状态", "IS_IN_STORE"), 
  DELETE(9L, "商品被删除", "IS_PRE_DELETE"), 
  FORCE_DELETE(10L, "商品被强制删除, 通过专门的接口才能查看", "IS_PRE_DELETE"), 
  IN_STORE_FAIL_SOLD_AUCTION(11L, "发布到仓库,流拍,已售完到仓库，组合状态", "IS_IN_STORE"), 
  BOSS_DOWNLOAD(12L, "BOSS下架的商品", "IS_IN_STORE"), 
  NEVER_UPLOAD(13L, " 从未上架商品", "IS_IN_STORE"), 
  CUSTOM_UPLOAD(64L, "自定义时间上架状态", "IS_SALE_ON_TIME");

  private final long code;
  private final String info;
  private final String showMeg;

  private ItemState(long code, String showMeg, String info) {
    this.code = code;
    this.info = info;
    this.showMeg = showMeg;
  }

  public static ItemState getItemState(long code)
  {
    for (ItemState state : values()) {
      if (state.code == code)
        return state;
    }
    throw new RuntimeException("不存在这种状态:" + code);
  }

  public static String getInfo(long code) {
    return getItemState(code).info;
  }

  public static String getName(long code) {
    return getItemState(code).name();
  }

  public static String getShowMeg(long code) {
    return getItemState(code).showMeg;
  }
  public static boolean isForSale(long code) {
	    return "IS_FOR_SALE".equals(getInfo(code));
	  }
  public static ItemState getItemState(String info) {
    for (ItemState state : values()) {
      if (state.info.equals(info))
        return state;
    }
    return null;
  }

  public long getCode() {
    return this.code;
  }

  public String getInfo() {
    return this.info;
  }

  public String getShowMeg() {
    return this.showMeg;
  }

  public static class State
  {
    public static final String IS_FOR_SALE = "IS_FOR_SALE";
    public static final String IS_IN_STORE = "IS_IN_STORE";
    public static final String IS_PRE_DELETE = "IS_PRE_DELETE";
    public static final String IS_SALE_ON_TIME = "IS_SALE_ON_TIME";
    public static final String IS_SOLD_OUT = "IS_SOLD_OUT";
  }
}