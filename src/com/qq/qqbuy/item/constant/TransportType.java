package com.qq.qqbuy.item.constant;

public enum TransportType
{
  TRANSPORT_NONE(0, "卖家包邮，无需买家关心运送"), 

  TRANSPORT_MAIL(1, "邮政寄送"), 

  TRANSPORT_EXPRESS(2, "快递"), 

  TRANSPORT_EMS(3, "EMS"), 

  TRANSPORT_OFFLINE(4,"货到付款"),

  TRANSPORT_UNKNOWN(-1, "未知的运输方式");

  private final int typeCode;
  private final String showMeg;

  private TransportType(int typeCode, String showMeg) {
    this.typeCode = typeCode;
    this.showMeg = showMeg;
  }

  public int getTypeCode() {
    return this.typeCode;
  }

  public static TransportType valueOf(long typeCode) {
    for (TransportType type : values()) {
      if (type.getTypeCode() == typeCode) {
        return type;
      }
    }
    return TRANSPORT_UNKNOWN;
  }

  public String getShowMeg() {
    return this.showMeg;
  }
}