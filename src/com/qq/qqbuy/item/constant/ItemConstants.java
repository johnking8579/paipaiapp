package com.qq.qqbuy.item.constant;

public class ItemConstants {
	
	/**
	 * 促销相关商品，卖家属性
	 */
	public static final int ORDER_USER_SELLER_HAS_PROMOTION = 70; // 卖家新促销活动标记
	public static final int INDEX_PROP_PROMOTION_EVENT_NO = 44; // 不参加优惠促销的标记
	public static final int INDEX_PROP_PROMOTIONAL_EVENT_START = 35; // 促销活动开始满立减活动
	
	public static final int COMMODITYID_LENGTH = 32;
	public static final int ITEM_ACTIVETYPE_BIGSELL = 46; // 大促的活动类型，大促不再跟平时的推荐位活动搅在一起了
	public static final String DEFAULT_SHOP_CLASSID = "0010000000";
	public static final int SECONDS_A_DAY = 86400;
	public static final int DEFAULT_THEME_ID = 0;
	public static final int MIN_TITLE_LEN = 3;
	public static final int MAX_TITLE_LEN = 60;
	public static final int MIN_COMDYDESC_LEN = 3;
	public static final int MAX_COMDYDESC_LEN = 50000;
	public static final int MAX_COMMODITY_NUM = 1000000000;
	public static final int MAX_COMMODITY_PRICE = 100000000;
	public static final int COMMODITY_STATE_IN_STORE = 1;
	public static final int COMMODITY_STATE_IN_BIN = 2;
	public static final int COMMODITY_STATE_IN_AUCTION_NO_BID = 3;
	public static final int COMMODITY_STATE_IN_AUCTION_BID = 4;
	public static final int COMMODITY_STATE_FAIL_AUCTION = 5;
	public static final int COMMODITY_STATE_SOLD = 6;
	public static final int COMMODITY_STATE_IN_SELLING = 7;
	public static final int COMMODITY_STATE_IN_STORE_OR_FAIL_AUCTION = 8;
	public static final int COMMODITY_STATE_DELETE = 9;
	public static final int COMMODITY_STATE_FORCE_DELETE = 10;
	public static final int COMMODITY_STATE_IN_STORE_FAIL_SOLD_AUCTION = 11;
	public static final int COMMODITY_STATE_BOSS_DOWNLOAD = 12;
	public static final int COMMODITY_STATE_NEVER_UPLOAD = 13;
	public static final int COMMODITY_STATE_CUSTOM_UPLOAD = 64;
	public static final int NEW_OLD_TYPE_NEW = 1;
	public static final int NEW_OLD_TYPE_OLD = 2;
	public static final int NEW_OLD_TYPE_EXHIBIT = 4;
	public static final int NEW_OLD_TYPE_ITEM = 8;
	public static final int SELL_TYPE_BIN = 1;
	public static final int SELL_TYPE_AUCTION_SINGLE = 2;
	public static final int SELL_TYPE_B2C = 3;
	public static final int TRANSPORT_SELLER_PAY = 1;
	public static final int TRANSPORT_BUYER_PAY = 2;
	public static final int TRANSPORT_NO_NEED_PAY = 3;
	public static final int TRANSPORT_WITH_SHIPPINGFEE_BORDER = 10;
	public static final int SEND_TYPE_MONEY = 1;
	public static final int SEND_TYPE_ITEM = 2;
	public static final int SEND_TYPE_BAOBEI = 4;
	public static final int SEND_TYPE_TENPAY = 8;
	public static final int SEND_TYPE_DELIVERY_FIRST = 16;
	public static final int EXID_SPID = 0;
	public static final int EXID_QCC_QID = 1;
	public static final int EXID_JNET_CARDID = 2;
	public static final int EXID_INFO_CFTHB = 3;
	public static final int EXID_QQTMC_ITEM = 4;
	public static final int EXID_JNET_CARDINFO = 5;
	public static final int EXID_PPHB = 6;
	public static final int EXID_QQSHOWID = 7;
	public static final int EXID_BANKTMC = 8;
	public static final int EXID_FRIENDS_MARK = 9;
	public static final int EXID_AUTO_CARD = 10;
	public static final int EXID_WULIU_SELLER = 11;
	public static final int EXID_WULIU_BUYER = 12;
	public static final int EXID_DEAL_ETCINFO_FLAG = 13;
	public static final int EXID_CARD_FIRST_VISITTIME = 14;
	public static final int EXID_CARD_HINTINFO = 15;
	public static final int EXID_QQVIDEO_ID = 16;
	public static final int EXID_QCC_PRESENT = 17;
	public static final int EXID_OUFEI_CARDID = 18;
	public static final int EXID_OUFEI_CARDINFO = 19;
	public static final int EXID_91KA_CARDID = 20;
	public static final int EXID_91KA_CARDINFO = 21;
	public static final int EXID_FRIEND_TYPE = 22;
	public static final int EXID_WISH_BASEINFO = 23;
	public static final int EXID_WISH_NOTEINFO = 24;
	public static final int EXID_CARD_CHARGETYPE = 25;
	public static final int EXID_CARD_ATTENTION = 26;
	public static final int EXID_ISD_ID = 27;
	public static final int EXID_EXPECT_RECVTIME = 28;
	public static final int EXID_FLASH_SEND = 29;
	public static final int EXID_PROMOTE_INFO = 30;
	public static final int EXID_TO_NAME = 31;
	public static final int EXID_SEND_NAME = 32;
	public static final int EXID_FIRST_PRICE = 33;
	public static final int EXID_MEMBER_PRICE = 34;
	public static final int EXID_REDBRICK_DISCOUNT = 35;
	public static final int EXID_GREENBRICK_DISCOUNT = 36;
	public static final int EXID_AUTOCARD_ID = 37;
	public static final int EXID_AUTOCARD_INFO = 38;
	public static final int EXID_SIZETABLE_ID = 39;
	public static final int EXID_COMDY_DOWN_REASON = 128;
	public static final int EXID_ITEM_STOCK_ID = 129;
	public static final int EXID_DEAL_COMDY_ATTR_STOCK = 130;
	public static final int EXID_DEAL_SEND_GOOD_NO = 131;
	public static final int EXID_ITEM_EMS_PRICE = 132;
	public static final int EXID_ITEM_NEW_RED_PAG = 133;
	public static final int EXID_ITEM_DEAL_MSG = 134;
	public static final int EXID_ITEM_REBATE_RATE = 135;
	public static final int EXID_ITEM_PROMOTION_DESC = 136;
	public static final int EXID_ITEM_ACCESSORY_DESC = 137;
	public static final int EXID_ITEM_PROMOTION_TYPE = 138;
	public static final int EXID_ITEM_PRODUCT_ATTR = 139;
	public static final int EXID_ITEM_TEJIA_TYPE = 140;
	public static final int EXID_ITEM_HAS_RULE_INFO = 141;
	public static final int EXID_REFUND_FIRST_ACTION_TIME = 142;
	public static final int EXID_NOTSAVEDB_START = 1024;
	public static final int EXID_VISITKEY = 1025;
	public static final int EXID_BUYER_REMARK = 1026;
	public static final int EXID_BIN_NUMLIMIT = 1027;
	public static final int EXID_PPHB_ID = 1028;
	public static final int EXID_DEAL_GEN_LOG = 1029;
	public static final int EXID_DEAL_MODIFYPRICE_LOG = 1030;
	public static final int EXID_DEAL_SOURCE_FLAG = 1031;
	public static final int EXID_TMP_GENUINE_PRODUCT = 1032;
	public static final int EXID_DEAL_GEN_TIMESTAMP = 1033;
	public static final int EXID_MAX = 2048;
	public static final int EXID_COMDY_LOGO1 = 3001;
	public static final int EXID_COMDY_LOGO2 = 3002;
	public static final int EXID_COMDY_LOGO3 = 3003;
	public static final int EXID_COMDY_LOGO4 = 3004;
	public static final int EXID_COMDY_ATTR_OFFSET = 20000;
	public static final int REPAIR_TYPE_YES = 1;
	public static final int REPAIR_TYPE_NO = 2;
	public static final int INVOIC_TYPE_YES = 1;
	public static final int INVOIC_TYPE_NO = 2;

	public static enum SUPPOPT_PAY_AGENCY {
		SUPPORT_PAY_YES(1, "支持财付通"), SUPPORT_PAY_NO(2, "不知道财付通");

		private final int value;
		private final String showMeg;

		private SUPPOPT_PAY_AGENCY(int value, String showMeg) {
			this.value = value;
			this.showMeg = showMeg;
		}

		public int getValue() {
			return this.value;
		}

		public String getShowMeg() {
			return this.showMeg;
		}
	}

	public static enum ItemProperty {
		SIMP_PROP_AUTO_CARD_HINT_INFO(1, 0, "卡密提示信息"), SIMP_PROP_DEPOSIT_SELL(2, 1, "抵押金拍卖"), SIMP_PROP_DISCOUNT_SELL(
				4, 2, "折扣商品(貌似废弃了)"), SIMP_PROP_CASH_ON_DELIVERY(8, 3, "货到付款商品"), SIMP_PROP_CITY_SELL(16, 4,
				"购买地域限制商品;暂未实现"), SIMP_PROP_MOBILE_AUTH(32, 5, "手机认证用户发布的商品  【废除】"), SIMP_PROP_RECOMM_AUTH(64, 6,
				"推荐位商品"), SIMP_PROP_VIRTUAL_ITEM(128, 7, "虚拟商品"), SIMP_PROP_QZONE_ITEM(256, 8, "QZONE商品"), SIMP_PROP_JUNWANG_ITEM(
				512, 9, "骏网商品 (蓝色自动发货)"), SIMP_PROP_AUDIT_FIRST(1024, 10, "先审后发"), SIMP_PROP_AUCTION_HALL(2048, 11,
				"拍卖大厅商品"), SIMP_PROP_LOW_QUALITY(4096, 12, "低质商品,搜索隐藏 "), SIMP_PROP_QQ_TMC(8192, 13, "QQ 特卖场商品"), SIMP_PROP_SPECIAL_AUTH(
				16384, 14, "特权商品(会员折扣或红钻折扣或绿钻折扣商品都会打上)"), SIMP_PROP_AUTO_SEND(32768, 15, "卡密自动发货商品 (黄色色自动发货)"), SIMP_PROP_GOOD_EVAL(
				65536, 16, "是否有评价"), SIMP_PROP_LEAVE_MSG(131072, 17, "是否有留言"), SIMP_PROP_14_DAYS_SECURITY(262144, 18,
				"14天包退"), SIMP_PROP_7_DAYS_SECURITY(524288, 19, "7天包换"), SIMP_PROP_QQVIDEO(1048576, 20, "有QQvideo的商品"), SIMP_PROP_CLASS_GIRLS(
				2097152, 21, "女装商品"), SIMP_PROP_CLASS_MOBILE(4194304, 22, "官字商品"), SIMP_PROP_SEND_FAST(8388608, 23,
				"闪电发货商品(诚保代充)"), SIMP_PROP_SHOP_WINDOW(16777216, 24, "橱窗商品"), SIMP_PROP_GENUINE(33554432, 25,
				"正品保证，假一赔三"), SIMP_PROP_TAKE_WHEN_BIDED(67108864, 26, "拍下即扣商品"), SIMP_PROP_HAS_PROMOT_INFO(134217728,
				27, "商品带有促销信息"), SIMP_PROP_HAS_STOCK_RECORD(268435456, 28, "商品有库存记录"), SIMP_PROP_HAS_STOCK_ID(
				536870912, 29, "商品无库存记录,但是有货号,此时需要从扩展表取出货号"), SIMP_RPOP_CANT_BUY(1073741824, 30, "禁止购买"), SIMP_RPOP_COMDY_XXXXX(
				-2147483648, 31, "不能用"),

		SIMP_RPOP_THROUGH_TRAIN(1, 32, "直通车属性"), SIMP_PROP_THROUGH_OPENAPI_USER(2, 33, "通过OPEN_API发布的商品"), SIMP_PROP_PROMOTIONAL_EVENT_JOIN(
				4, 34, "直通车属性"), SIMP_PROP_PROMOTIONAL_EVENT_START(8, 35, "促销活动开始满立减活动"), SIMP_PROP_SEARCH_SORT_DUBIOUS(
				16, 36, "搜索排序可疑标识"),

		SIMP_PROP_REPAIR(1, 160, "保修"), SIMP_PROP_INVOICE(2, 161, "发票"), SIMP_PROP_PRICEADD(4, 162, "加价类型"), SIMP_PROP_PASS_AUTH(
				8, 163, "通过商品审核"), SIMP_PROP_LIMIT_DEPOSIT(16, 164, "抵押金拍卖限制条件(bit, 上架; nobit, 价格)"), SIMP_PROP_HAS_RELOAD(
				32, 165, "用户设置了自动上架"), SIMP_INDEX_COMDY_GRAY(64, 166, "商品灰度属"), SIMP_PROP_COMDY_SPH(128, 167, "尚品会"),

		SIMP_PROP_ITEM_ACCESSORY(512, 169, "配件"), SIMP_PROP_ITEM_INSTALLMENT(1024, 170, "分期付款"), SIMP_PROP_ITEM_BUYER_CAN_TALK(
				2048, 171, "表示订单是否要求买单提供信息"), SIMP_PROP_ITEM_BUYER_MUST_TALK(4096, 172, "表示订单是否要求买单提供信息,买家必须回答"), SIMP_PROP_COMDY_B2C_NEW(
				8192, 173, "B2C new"), SIMP_PROP_ITEM_COUPON(16384, 174, "购物券"), SIMP_PROP_ITEM_HAS_EXT(32768, 175,
				"是否存在扩展信息"),

		SIMP_INDEX_PROP_MAX(-2147483648, 256, "最大值,不能使用");

		private final int value;
		private final int index;
		private final String showMeg;

		private ItemProperty(int value, int index, String showMeg) {
			this.value = value;
			this.index = index;
			this.showMeg = showMeg;
		}

		public int getValue() {
			return this.value;
		}

		public int getIndex() {
			return this.index;
		}

		public String getShowMeg() {
			return this.showMeg;
		}
	}
}