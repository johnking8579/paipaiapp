package com.qq.qqbuy.item.constant;
/**
 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
 */
public class ItemConstantForWap extends ItemConstants{
	public static int  MAX_DISPLAY_DETAIL_INFO=200;
	public static int MAX_DISPLAY_ATTR_INFO=12;
	public static int MIN_STOCK_COUNT_FOR_TWO_STEP=12;
	public static final int MAX_ATTR_COUNT_FOR_QUICK_BUY = 2;

}

