package com.qq.qqbuy.common;

/**
 * 系统初始化时要做的操作, 使用spring初始化
 * @author JingYing
 * @date 2014年12月31日
 */
public class SystemInit {
	
	public void init()	{
		System.setProperty("sun.net.http.allowRestrictedHeaders", "true");  //使用httpURLConnection时允许加host头信息
		Monitor.initHeartbeatAndJvm();	//启动心跳监控和JVM监控
	}

}
