package com.qq.qqbuy.common;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.qq.qqbuy.common.util.Util;


/**
 * 配置工具类，用于返回不同类型的配置项
 * 不依赖于spring, 因为有些配置项需要在spring初始化前调用.
 * 
 */
public class ConfigHelper {
	
	private static Properties instance;
	
	static {
		String prop = choosePropFromXml();
		if(prop.contains("${"))	{	//表示为拍拍环境, 使用System.getProperty控制配置文件
			String key = prop.substring(prop.indexOf("${")+2, prop.indexOf("}"));
			String sysVar = System.getProperty(key);
			if(sysVar == null)
				throw new RuntimeException("没有找到系统变量[" + sysVar + "],无法定位到properties配置文件" + prop);
			prop = prop.replace("${" + key + "}", sysVar);
		}
		prop = prop.replace("classpath:", "").trim();
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(prop);
		if(is == null)	
			throw new RuntimeException("没有找到 classpath下关键配置文件:" + prop);
		
		instance = new Properties();
		try {
			instance.load(is);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally	{
			Util.closeStream(is);
		}
	}
	
	/**
	 * 从biz-context-cfg.xml查找需要加载的properties
	 * @return
	 */
	private static String choosePropFromXml()	{
		String xml = "conf-spring/biz-context-cfg.xml";
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(xml);
		if(is == null)
			throw new RuntimeException("没有找到classpath下关键配置文件:" + xml);
		try {
			SAXReader reader = new SAXReader();
			reader.setEntityResolver(new EntityResolver() {
				@Override
				public InputSource resolveEntity(String publicId, String systemId)
						throws SAXException, IOException {
					return new InputSource(new ByteArrayInputStream("".getBytes()));
				}
			});
			Element root = reader.read(is).getRootElement();
			String propFile = null;
			for(Element bean : (List<Element>)root.elements("bean"))	{
				if("org.springframework.beans.factory.config.PropertyPlaceholderConfigurer".equals(bean.attributeValue("class")))	{
					for(Element prop : (List<Element>)bean.elements("property"))	{
						if("location".equals(prop.attributeValue("name")))	{
							propFile = prop.elementText("value");
						}
					}
				}
			}
			if(propFile == null)
				throw new RuntimeException("在spring配置文件[" + xml + "]中没有查到properties文件:" + propFile);
			return propFile;
		} catch (DocumentException e) {
			throw new RuntimeException(e);
		} finally	{
			Util.closeStream(is);
		}
	}

	public static String getProperty(String key) {
		return instance.getProperty(key);
	}

	public static String getStringByKey(String key, String defaultStr) {
		if(null == key){
			return defaultStr;
		}
		return instance.getProperty(key, defaultStr);
	}

	public static int getIntByKey(String key, int defaultInt) {
		String resultValue = instance.getProperty(key);
		try {
			return Integer.parseInt(resultValue);
		} catch (Throwable e) {
			Log.run.error("ConfigHelper.getIntByKey(" + key + "," + defaultInt
					+ ")=" + resultValue + ", return default " + defaultInt);
			return defaultInt;
		}
	}
	
	/**
	 * 是否开启服务降级
	 * @return
	 */
	public static boolean isSysDegrade()	{
		try {
			return Boolean.parseBoolean(instance.getProperty("sys.degrade"));
		} catch (Exception e) {
			return false;
		}
	}
}
