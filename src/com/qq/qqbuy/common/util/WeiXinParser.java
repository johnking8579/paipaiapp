package com.qq.qqbuy.common.util;


/**
 * 
 * @ClassName: WeiXingParser 
 * 
 * @Description: 微信登陆key解析方法
 * @author wendyhu 
 * @date 2012-11-7 下午03:00:16
 */
public class WeiXinParser
{
    /**
     * 
     * @Title: parse 
     * 
     * @Description: 解析微信登陆key
     *  
     * @param content
     * @return    设定文件 
     * @return WeiXingLoginKey    返回类型 
     * @throws
     */
    public static WeiXinLoginKey parse(String content)
    {
        //1、初使化参数
        long uin = 0;
        String skey = "";
        long time = 0;
        do
        {
            if(null == content)
                break;
            
            //2、解密串 oz8QHhhgU8J1hg4MAWd/h2Fum5rqTdjq2UkO0U9VBJNK8Ap3H/GIqZxMyC7CATvx
            String result = AES.decrypt(content, null);

            if( null == result)
                break;
            
            //3、解析串 //30565469+JGdrMzc3VDVZVg==+1352103905
            String[] array = result.split("\\+");
            if(null == array || array.length != 3)
                break;
            
            uin = StringUtil.toLong(array[0], 0);
            skey = AES.decode(array[1]);
            time = StringUtil.toLong(array[2], 0);
            
        }while(false);
        
        return new WeiXinLoginKey(uin, skey, time);
    }
}
