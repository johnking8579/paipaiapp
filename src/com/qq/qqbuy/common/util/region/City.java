package com.qq.qqbuy.common.util.region;

import java.util.ArrayList;
import java.util.List;

public class City {
	/**
	 * 城市ID
	 */
	public int cityId;
	/**
	 * 省ID
	 */
	public int provinceId;
	/**
	 * 城市名
	 */
	public String cityName;
	/**
	 * 该城市所有的区列表
	 */
	public List<Area> areaList = new ArrayList<Area>();
	
	public String toString() {
		return "[cityId="+cityId+",provinceId="+provinceId+",cityName="+cityName+"]";
	}

	public int getCityId() {
		return cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public int getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(int provinceId) {
		this.provinceId = provinceId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public List<Area> getAreaList() {
		return areaList;
	}

	public void setAreaList(List<Area> areaList) {
		this.areaList = areaList;
	}
	
}

