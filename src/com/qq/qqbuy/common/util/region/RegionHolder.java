package com.qq.qqbuy.common.util.region;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 区域编码信息的临时包装类
 * 
 * @author ethonchan
 * 
 */
public class RegionHolder {
	ArrayList<Province> provinceList = new ArrayList<Province>();
	HashMap<Integer, Province> provinceMap = new HashMap<Integer, Province>();
	HashMap<Integer, City> cityMap = new HashMap<Integer, City>();
	HashMap<Integer, Area> areaMap = new HashMap<Integer, Area>();

	/**
	 * 获取省列表
	 * 
	 * @return
	 */
	public ArrayList<Province> getProvinceList() {
		return provinceList;
	}

	/**
	 * 获取某个省的信息
	 * 
	 * @param provinceId
	 * @return
	 */
	public Province getProvinceById(int provinceId) {
		return provinceMap.get(provinceId);
	}

	/**
	 * 获取某个城市的信息
	 * 
	 * @param cityId
	 * @return
	 */
	public City getCityById(int cityId) {
		return cityMap.get(cityId);
	}

	/**
	 * 获取某个区的信息
	 * 
	 * @param areaId
	 * @return
	 */
	public Area getAreaById(int areaId) {
		return areaMap.get(areaId);
	}

	/**
	 * 根据地址ID获取地址信息
	 * 
	 * @param regionId
	 * @return
	 */
	public String getAddressByRegionId(long regionId) {
		String saddr = "";
		Area area = getAreaById((int) regionId);
		if (area != null) {
			Province p = getProvinceById(area.provinceId);
			if (p != null) {
				saddr = p.provinceName;
			}

			City c = getCityById(area.cityId);
			if (c != null) {
				saddr += c.cityName;
			}

			saddr += area.name;
		} else {
			City c = getCityById((int) regionId);
			if (c != null) {
				Province p = getProvinceById(c.provinceId);
				if (p != null) {
					saddr = p.provinceName;
				}

				saddr += c.cityName;
			} else {
				Province p = getProvinceById((int) regionId);
				if (p != null) {
					saddr = p.provinceName;
				}
			}
		}

		return saddr;
	}

	public int getProvinceIdByRegionId(long regionId) {
		Area area = getAreaById((int) regionId);
		if (area != null) {
			return area.provinceId;
		} else {
			City c = getCityById((int) regionId);
			if (c != null) {
				return c.provinceId;
			} else {
				Province p = getProvinceById((int) regionId);
				if (p != null) {
					return p.provinceId;
				}
			}
		}

		return -1;
	}

	public int getCityIdByRegionId(long regionId) {
		Area area = getAreaById((int) regionId);
		if (area != null) {
			return area.cityId;
		} else {
			City c = getCityById((int) regionId);
			if (c != null) {
				return c.cityId;
			}
		}

		return -1;
	}

	public int getAreaIdByRegionId(long regionId) {
		Area area = getAreaById((int) regionId);
		if (area != null) {
			return area.areaId;
		}

		return -1;
	}
}
