package com.qq.qqbuy.common.util.region;

import java.util.ArrayList;
import java.util.List;

public class Province {
	/**
	 * 省ID
	 */
	public int provinceId;
	/**
	 * 省名
	 */
	public String provinceName;
	/**
	 * 该省所有的城市列表
	 */
	public List<City> cityList = new ArrayList<City>();
	
	public String toString() {
		return "[provinceId="+provinceId+",provinceName="+provinceName+"]";
	}

	public int getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(int provinceId) {
		this.provinceId = provinceId;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public List<City> getCityList() {
		return cityList;
	}

	public void setCityList(List<City> cityList) {
		this.cityList = cityList;
	}

	
}
