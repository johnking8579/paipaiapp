package com.qq.qqbuy.common.util.region;

public class Area {
	/**
	 * 区ID
	 */
	public int areaId;
	/**
	 * 城市ID
	 */
	public int cityId;
	/**
	 * 省ID
	 */
	public int provinceId;
	/**
	 * 区名
	 */
	public String name;
	
	public String toString() {
		return "[areaId="+areaId+",cityId="+cityId+",provinceId="+provinceId+",name="+name+"]";
	}

	public int getAreaId() {
		return areaId;
	}

	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}

	public int getCityId() {
		return cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public int getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(int provinceId) {
		this.provinceId = provinceId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
