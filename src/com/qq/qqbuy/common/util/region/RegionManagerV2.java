package com.qq.qqbuy.common.util.region;



import java.io.File;
import java.util.ArrayList;

/**
 * 新版（version=2）收货地址管理类
 * @author ethonchan
 *
 */
public class RegionManagerV2 {
	
	//	省市信息列表
	public static RegionHolder regionHolder = new RegionHolder();
	
	static {
		init();
	}
	
	/**
	 * 初始化，从文件中加载地区信息
	 */
	public static void init() {
		File file = new File(RegionManagerV2.class.getResource("/res/pp_regions_v2.xml").getPath());
		RegionHolder holder = RegionUtil.parseRegionFile(file);
		regionHolder = holder;
	}
	
	/**
	 * 获取省列表
	 * @return
	 */
	public static ArrayList<Province> getProvinceList() {
		return regionHolder.getProvinceList();
	}
	
	/**
	 * 获取某个省的信息
	 * @param provinceId
	 * @return
	 */
	public static Province getProvinceById(int provinceId) {
		return regionHolder.getProvinceById(provinceId);
	}
	
	/**
	 * 获取某个城市的信息
	 * @param cityId
	 * @return
	 */
	public static City getCityById(int cityId) {
		return regionHolder.getCityById(cityId);
	}
	
	/**
	 * 获取某个区的信息
	 * @param areaId
	 * @return
	 */
	public static Area getAreaById(int areaId) {
		return regionHolder.getAreaById(areaId);
	}
	
	/**
	 * 根据地址ID获取地址信息
	 * @param regionId
	 * @return
	 */
	public static String getAddressByRegionId(long regionId) {
		return	regionHolder.getAddressByRegionId(regionId);
	}
	
	public static int getProvinceIdByRegionId(long regionId) {
		return	regionHolder.getProvinceIdByRegionId(regionId);
	}
	
	public static int getCityIdByRegionId(long regionId) {
		return	regionHolder.getCityIdByRegionId(regionId);
	}
	
	public static int getAreaIdByRegionId(long regionId) {
		return	regionHolder.getAreaIdByRegionId(regionId);
	}
	
	public static void main(String[] args) {
		System.out.println(RegionManagerV2.getProvinceById(25));
		System.out.println(RegionManagerV2.getCityById(2500));
		System.out.println(RegionManagerV2.getAreaById(42099));
	}
}
