package com.qq.qqbuy.common.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class XssUtil {
	private static Map<Character, String> dangerChar = new HashMap<Character, String>();
	private static Map<String, Character> dangerCharReverse = new HashMap<String, Character>();
	
	static	{
		dangerChar.put(Character.valueOf('<'), "&lt;");
		dangerChar.put(Character.valueOf('>'), "&gt;");
		dangerChar.put(Character.valueOf('\''), "&#39;");
		dangerChar.put(Character.valueOf('"'), "&quot;");
		dangerChar.put(Character.valueOf('&'), "&amp;");
		
		for(Entry<Character, String> e : dangerChar.entrySet())	{
			dangerCharReverse.put(e.getValue(), e.getKey());
		}
	}

	/**
	 * 把危险字符进行htmlEncode
	 * @param src
	 * @return
	 */
	public static String encode(String src) {
		if (src == null) return null;
		StringBuilder sb = new StringBuilder(src.length() + 32);
		for(char ch : src.toCharArray())	{
			String rep = (String) dangerChar.get(Character.valueOf(ch));
			sb.append(rep == null ? ch : rep);
		}
		return sb.toString();
	}
	
	/**
	 * 把htmlEncode后的字符串还原
	 * @param src
	 * @return
	 */
	public static String decode(String src)	{
		if (src == null) return null;
		String s = src;
		for(Entry<String, Character> e : dangerCharReverse.entrySet())	{
			s = s.replace(e.getKey(), e.getValue().toString());
		}
		return s;
	}
	
	
	public static void main(String[] args) {
		String e = encode("12'\"345<67>89");
		String d = decode(e);
		System.out.println(e);
		System.out.println(d);
	}

}
