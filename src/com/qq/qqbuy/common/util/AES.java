package com.qq.qqbuy.common.util;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.thoughtworks.xstream.core.util.Base64Encoder;

/**
 * 
 * @ClassName: AES
 * @Description: 这里实现的一些AES加密
 * 
 * @author wendyhu
 * @date 2012-11-6 下午06:48:45
 */
public class AES
{
    public static Logger log = LogManager.getLogger(AES.class);
    /**
     * 密钥
     */
    private final static byte[] DEFAULT_SECURITY = "E&2C!k]#k)w}(@!a"
            .getBytes();
    private final static Key DEFAULT_KEY = new SecretKeySpec(DEFAULT_SECURITY,
            "AES");
    static
    {
//        Security.addProvider(new BouncyCastleProvider());
    }

    /**
     * 
     * @Title: Encrypt
     * @Description: AES/CBC/PKCS7 的AES加密
     * 
     * @param src
     *            源串
     * @param keyStr
     *            密钥
     * @return
     * @throws Exception
     *             设定文件
     * @return String 返回类型
     * @throws
     */
    public static String encrypt(byte[] src, byte[] keyStr)
    {
        String ret = "";
        if (src == null)
        {
            return null;
        }

        // 判断Key是否为16位
        if (null != keyStr && keyStr.length != 16)
        {
            return null;
        }

        // Security.addProvider(new BouncyCastleProvider());
        Key key = DEFAULT_KEY;
        if (null != keyStr)
            key = new SecretKeySpec(keyStr, "AES");
        try
        {
            Cipher in = Cipher.getInstance("AES/CBC/PKCS7Padding", "BC");
            if (null != keyStr)
                in.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(keyStr));
            else
                in.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(
                        DEFAULT_SECURITY));
            byte[] enc = in.doFinal(src);
            ret = new Base64Encoder().encode(enc);
            log.debug("src:" + new String(src) + " ret:" + ret);
        } catch (NoSuchAlgorithmException e)
        {
            log.error("src:" + new String(src) + " ret:" + ret, e);
        } catch (NoSuchProviderException e)
        {
            log.error("src:" + new String(src) + " ret:" + ret, e);
        } catch (NoSuchPaddingException e)
        {
            log.error("src:" + new String(src) + " ret:" + ret, e);
        } catch (InvalidKeyException e)
        {
            log.error("src:" + new String(src) + " ret:" + ret, e);
        } catch (InvalidAlgorithmParameterException e)
        {
            log.error("src:" + new String(src) + " ret:" + ret, e);
        } catch (IllegalBlockSizeException e)
        {
            log.error("src:" + new String(src) + " ret:" + ret, e);
        } catch (BadPaddingException e)
        {
            log.error("src:" + new String(src) + " ret:" + ret, e);
        }

        return ret;// 此处使用BASE64做转码功能，同时能起到2次加密的作用。
    }

    /**
     * 
     * @Title: Decrypt
     * @Description:
     * @param src
     * @param keyStr
     * @return
     * @throws Exception
     *             设定文件
     * @return String 返回类型
     * @throws
     */
    public static String decrypt(String src, byte[] keyStr)
    {
        String ret = "";
        // 判断Key是否正确
        if (src == null)
        {
            return null;
        }
        // 判断Key是否为16位
        if (null != keyStr && keyStr.length != 16)
        {
            return null;
        }

        // Security.addProvider(new BouncyCastleProvider());
        Key key = DEFAULT_KEY;
        if (null != keyStr)
            key = new SecretKeySpec(keyStr, "AES");
        try
        {
            byte[] encrypted = new Base64Encoder().decode(src);// 先用base64解密
            Cipher out = Cipher.getInstance("AES/CBC/PKCS7Padding", "BC");
            if (null != keyStr)
                out.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(keyStr));
            else
                out.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(
                        DEFAULT_SECURITY));
            byte[] dec = out.doFinal(encrypted);
            ret = new String(dec);
            log.debug("src:" + src + " ret:" + ret);
        } catch (BadPaddingException e)
        {
            return null;
        } catch (NoSuchAlgorithmException e)
        {
            log.error("src:" + src + " ret:" + ret, e);
        } catch (NoSuchProviderException e)
        {
            log.error("src:" + src + " ret:" + ret, e);
        } catch (NoSuchPaddingException e)
        {
            log.error("src:" + src + " ret:" + ret, e);
        } catch (InvalidKeyException e)
        {
            log.error("src:" + src + " ret:" + ret, e);
        } catch (InvalidAlgorithmParameterException e)
        {
            log.error("src:" + src + " ret:" + ret, e);
        } catch (IllegalBlockSizeException e)
        {
            log.error("src:" + src + " ret:" + ret, e);
        }

        return ret;
    }

    /**
     * 
     * @Title: decode
     * @Description: base 64 解码
     * @param source
     * @return 待解码串
     * @return String 解码后的串
     * @throws
     */
    public static String decode(String source)
    {
        String ret = "";
        if (null == source)
            return ret;
        ret = new String(new Base64Encoder().decode(source));
        return ret;
    }

}
