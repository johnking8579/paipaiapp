package com.qq.qqbuy.common.util;

import com.paipai.util.encrypt.MD5Util;

/**
 * 从paipai-3rd.jar反编译,并整合TssHelperV2.java
 * @author JingYing
 * @date 2014年12月28日
 */
public class TssHelper {

	private static String noNeedMd5Check = "7392e876";
	public static final String TSS_EXTENSION_TXT = "txt";
	public static final String TSS_EXTENSION_GIF = "gif";
	public static final String TSS_EXTENSION_JPG = "jpg";
	public static final String TSS_EXTENSION_MOBILE_TXT = "mobile.txt";// 图文详情h5页面内容
	
	private static final String 
			IMG_DOMAIN = "http://image.paipai.com/",
			NO_IMG = "http://img0.paipai.com/ABD2D1F8/noimage.jpg";

	public static String getTssFilenameV2(TssFileType fileType, String code,
			int fileIndex, String extension) {
		if (fileType == null || code == null || extension == null)
			return "参数不能为null";
		long timeNow = (long) ((double) System.currentTimeMillis() * 0.001D) % 268435455L;
		int codeLen = code.length();
		if (codeLen < 32)
			code = String.format("%32s", code).replace(' ', '0');
		else if (codeLen > 32)
			code = code.substring(0, 32);
		return String.format("%s-%08X-%s.%X.%s", TssFileType.getTssFileTypeName(fileType),
							Long.valueOf(timeNow), code, Integer.valueOf(fileIndex % 16), extension);
	}

	public static String getImgUrl(String srcName, String picSizeType) {
		if (srcName == null || !isTssFileName(srcName)) {
			return NO_IMG;
		}
		int dotPos = srcName.lastIndexOf(".");
		if (dotPos < 0)
			return NO_IMG;
		if (dotPos == 0) {
			return GetImgDomain(srcName) + "ABD2D1F8/noimage.jpg";
		}

		String imgFileName = getImageNameFromSrcName(srcName, picSizeType);
		byte[] md5Value = MD5Util.encrypt("a4f&g83)43fAw!" + imgFileName).getBytes();
		byte[] urlMd5 = { md5Value[22], md5Value[10], md5Value[30],
				md5Value[14], md5Value[9], md5Value[27], md5Value[19], md5Value[13] };
		return GetImgDomain(srcName) + new String(urlMd5) + "/" + imgFileName;
	}

	public static String getImgUrl(String srcName, String picSizeType,
			boolean needMd5Check) {
		if (needMd5Check) {
			return getImgUrl(srcName, picSizeType);
		}
		if (srcName == null || !isTssFileName(srcName)) {
			return NO_IMG;
		}
		int dotPos = srcName.lastIndexOf(".");
		if (dotPos < 0)
			return NO_IMG;
		if (dotPos == 0) {
			return GetImgDomain(srcName) + "ABD2D1F8/noimage.jpg";
		}
		String imgFileName = getImageNameFromSrcName(srcName, picSizeType);
		return GetImgDomain(srcName) + noNeedMd5Check + "/" + imgFileName;
	}

	private static String GetImgDomain(String srcName) {
		if (srcName == null) {
			return IMG_DOMAIN;
		}
		int pos = srcName.indexOf(45);
		if (pos < 0) {
			return IMG_DOMAIN;
		}
		try {
			int routeId = Integer.parseInt(srcName.substring(pos + 8, pos + 9), 16);
			return "http://img" + String.valueOf(routeId % 8) + ".paipaiimg.com/";
		} catch (Exception e) {
		}
		return IMG_DOMAIN;
	}

	private static boolean isTssFileName(String fileName) {
		if (fileName == null) {
			return false;
		}
		int pos = fileName.indexOf("-");
		if (pos < 0) {
			return false;
		}
		return TssFileType.isValidateType(fileName.substring(0, pos));
	}

	public static String getImageNameFromSrcName(String srcName,
			String picSizeType) {
		if (srcName == null) {
			return "noimage.jpg";
		}
		int dotPos = srcName.lastIndexOf(".");
		if (dotPos < 0) {
			return "noimage.jpg";
		}
		String sufFix = srcName.substring(dotPos).toLowerCase();
		String preFix = srcName.substring(0, dotPos);
		if (sufFix.length() == 0) {
			return "noimage.jpg";
		}
		if ((picSizeType == null) || ("".equals(picSizeType))) {
			return srcName;
		}
		return preFix + "." + picSizeType + sufFix;
	}

	public static String getItemSrcImageUrl(long qqUin, String logoName) {
		return getImgUrl(logoName, "");
	}
	
	public static void main(String[] args) {
//		String fsfas = TssHelper.getItemSrcImageUrl(9999, "item-000FA1BA-695BF6320000000000693B570807F04C.0.jpg");
//		String fsfas = com.paipai.the3rd.tss.TssHelper.getItemSrcImageUrl(9999, "item-000FA1BA-695BF6320000000000693B570807F04C.0.jpg");
//        System.out.println(fsfas);
	}

}
