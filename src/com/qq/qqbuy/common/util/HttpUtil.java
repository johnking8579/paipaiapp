package com.qq.qqbuy.common.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.PaiPaiConfig;
import com.qq.qqbuy.common.client.log.PacketLogParser;
import com.qq.qqbuy.common.client.log.UrlConnProxy;
import com.qq.qqbuy.common.env.EnvManager;

/**
 * http请求接口
 * 
 * @author wendyhu
 * 
 */
public class HttpUtil {
	
	/**
	 * http proxy管理器
	 * 重新整理下代理服务器的获取方式, 配置中心的key=qgo.sys.proxy
	 */
	public static class ProxyManager {

		/**
		 * 用于获取能够访问外网的代理，主要用于去财付通获取tokenId
		 * @return
		 */
		public static Proxy getProxy() {
			InetSocketAddress addr = PaiPaiConfig.getHttpProxyHostQgo();
			if("0.0.0.0".equals(addr.getAddress()))	{
				throw new RuntimeException("在配置中心没有找到代理服务器地址:" + PaiPaiConfig.LOGIN_HTTP_PROXYSER);
			}
			return new Proxy(Proxy.Type.HTTP, addr);
		}
		
		
		/**
		 * 访问京东用的代理服务器
		 * @return
		 */
		public static Proxy getJdPopHttpSvcProxy() {
			InetSocketAddress addr = PaiPaiConfig.getJdPopHttpSvcProxyHost();
			String host = addr.getHostName();
			int port = addr.getPort();
			Log.run.info("Use Http proxy from PaiPai Config Center jdpophttpsvc with host="
					+ host + " and port=" + port);
			return new Proxy(Proxy.Type.HTTP, new InetSocketAddress(host, port));
		}
	}

	private static final Logger logger = Log.run;

	public static String getXMLContent(String sUrl, String encoding,
			int connectTimeout, int readTimeout, boolean isNeedProxy) {
		long begin = System.currentTimeMillis();
		String res = "";
		String result = "fail";
		int retCode = -1;
		URL url = null;
		try {
			url = new URL(sUrl);
			HttpURLConnection conn = null;
			/**
			 * 增加代理服务器访问外网
			 */
			if (!EnvManager.isLocal() && isNeedProxy) {
				conn = (HttpURLConnection) url.openConnection(ProxyManager
						.getProxy());
			} else {
				conn = (HttpURLConnection) url.openConnection();
			}

			conn.setConnectTimeout(connectTimeout);
			conn.setReadTimeout(readTimeout);
			retCode = conn.getResponseCode();

			if (retCode == 200) {
				BufferedInputStream read = new BufferedInputStream(
						conn.getInputStream());
				byte[] b = new byte[1024 * 100];
				int len = 0;
				while (len != -1) {
					len = read.read(b);
					if (len > 0) {
						res += new String(b, 0, len, encoding);
					}
				}
				result = "succ";
			}
		} catch (Exception e) {
			result = "fail";
			logger.info("getXMLContent|fail|" + res, e);
		} finally {
			long end = System.currentTimeMillis();
			logger.info("getXMLContent|" + result + "|" + (end - begin) + "|"
					+ retCode + "|" + url.toExternalForm());
		}

		return res;
	}

	protected static String get(String urlStr, String hostName,
			int connectTimeout, int readTimeout, String encoding,
			boolean isNeedProxy, boolean needHeadRefer, String cookieStr) {
		HttpURLConnection huc = null;
		String result = "fail";
		int rspCode = 0;
		StringWriter writer = null;
		BufferedReader reader = null;

		try {
			URL url = new URL(urlStr);

			// 增加代理服务器访问外网
			if (!EnvManager.isLocal() && isNeedProxy) {
				// proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(
				// "172.23.28.199", 8080));
				huc = (HttpURLConnection) url.openConnection(ProxyManager
						.getProxy());
			} else {
				huc = (HttpURLConnection) url.openConnection();
			}

			huc.setRequestMethod("GET");
			huc.setRequestProperty("Content-Type", "text/html");
			if (!StringUtil.isEmpty(hostName)) {
				huc.setRequestProperty("Host", hostName);
			} else {
				huc.setRequestProperty("Host", url.getHost());
			}
			if (needHeadRefer) {
				huc.setRequestProperty("Referer", "app.paipai.com");
			}
			if (!StringUtil.isEmpty(cookieStr)) {
				Log.run.info("设置cookie为："+cookieStr);
				huc.setRequestProperty("Cookie", cookieStr);
			}
			huc.setConnectTimeout(connectTimeout);
			huc.setReadTimeout(readTimeout);

//			huc.connect();

			reader = new BufferedReader(new InputStreamReader(
					new UrlConnProxy(huc, true, encoding).getInputStream(), encoding));

			char[] buffer = new char[4096];
			writer = new StringWriter();
			int n;
			while (-1 != (n = reader.read(buffer))) {
				writer.write(buffer, 0, n);
			}
			String content = writer.toString();

			rspCode = huc.getResponseCode();

			result = "SUCCESS";

			return content;
		} catch (Exception e) {
			result = "fail";
			logger.error("HTTP GET |" + result + "|" + rspCode + "|" + urlStr,
					e);
			return null;
		} finally {
			try {
				if (huc != null)
					huc.disconnect();
				if (writer != null)
					writer.close();
				if (reader != null)
					reader.close();
			} catch (Exception e) {
			}
		}
	}
	
	/**
	 * 用于统一支付平台--微信支付【设置代理】
	 * @param urlStr
	 * @param params
	 * @param connectTimeout
	 * @param readTimeout
	 * @param requestCharset
	 * @param encoding
	 * @param isNeedProxy
	 * @param cookieStr
	 * @param host
	 * @param isEncode
	 * @return
	 * 
	 * @author liubenlong3
	 */
	public static String post4upp(String urlStr, Map<String, String> params,
			int connectTimeout, int readTimeout, String requestCharset,
			final String encoding, boolean isNeedProxy, String cookieStr,
			String host, boolean isEncode) {
		HttpURLConnection huc = null;
		String lurl = urlStr;
		String res = "";
		int rspCode = -1;
		String result = "fail";
		long begin = System.currentTimeMillis();
		try {
			URL url;
			final String param = toQueryString(params, requestCharset, isEncode);
			if (param != null) {
				// String paramStr = StringUtil.trim();
				// lurl += paramStr;
				lurl += "?";
				lurl += StringUtil.trim(param);
			}
			url = new URL(urlStr);

			/**
			 * 增加代理服务器访问外网
			 * DEV和Test环境配置10.6.222.234:80，IDC环境配置172.27.28.234或10.129.128.150:8080
			 */
			if (!EnvManager.isLocal() && isNeedProxy) {
				Log.run.info("hucamma----");
				huc = (HttpURLConnection) url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("172.27.28.234", 8080)));
			} else {
					huc = (HttpURLConnection) url.openConnection();
			}

			huc.setConnectTimeout(connectTimeout);
			huc.setReadTimeout(readTimeout);
			huc.setRequestMethod("POST");
			if (!StringUtil.isEmpty(host))
				huc.setRequestProperty("host", host);
			huc.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			if (!StringUtil.isEmpty(cookieStr)) {
				logger.info("设置cookie为："+cookieStr);
				huc.setRequestProperty("Cookie", cookieStr);
			}
			if (param != null) {
				huc.setDoOutput(true);
				OutputStreamWriter wr = new OutputStreamWriter(
						huc.getOutputStream(), requestCharset);
				// wr.write(paramStr);
				wr.write(StringUtil.trim(param));
				wr.flush();
			}

			rspCode = huc.getResponseCode();

			if (rspCode == 200) {
				BufferedInputStream read = new BufferedInputStream(
						new UrlConnProxy(huc, new PacketLogParser(){

							@Override
							public String getServiceName() {
								return null;
							}

							@Override
							public String parseRequest(byte[] request)
									throws Exception {
								return StringUtil.trim(param);
							}

							@Override
							public String parseResponse(byte[] response)
									throws Exception {
								if (response==null) {
									return "";
								}
								return new String(response, encoding);
							}
							
						}).getInputStream());
				byte[] b = new byte[1024 * 100];
				int len = 0;
				while (len != -1) {
					len = read.read(b);
					if (len > 0) {
						res += new String(b, 0, len, encoding);
					}
				}
				result = "succ";
			} else {
				logger.error("post|" + result + "|" + rspCode + "|" + lurl);
			}
			logger.info("res:"+res);
			return res;
		} catch (Exception e) {
			result = "fail";
			logger.error("post|" + result + "|" + rspCode + "|" + lurl, e);
			return "";
		} finally {
			if (huc != null)
				huc.disconnect();
			long end = System.currentTimeMillis();
			logger.info("post|" + result + "|" + rspCode + "|" + (end - begin)
					+ "|" + lurl);
		}
	}
	
	
	
	
	/**
	 * 用于统一支付平台--微信支付【设置代理】
	 * @param urlStr
	 * @param params
	 * @param connectTimeout
	 * @param readTimeout
	 * @param requestCharset
	 * @param encoding
	 * @param isNeedProxy
	 * @param cookieStr
	 * @param host
	 * @param isEncode
	 * @return
	 * 
	 * @author liubenlong3
	 */
	public static String getWithProxy(String urlStr, String hostName,
			int connectTimeout, int readTimeout, String encoding,
			boolean isNeedProxy, boolean needHeadRefer, String cookieStr,String proxyIP,int proxyPort) {
		HttpURLConnection huc = null;
		String result = "fail";
		int rspCode = 0;
		StringWriter writer = null;
		BufferedReader reader = null;

		long begin = System.currentTimeMillis();
		try {
			URL url = new URL(urlStr);

			// 增加代理服务器访问外网
			if (!EnvManager.isLocal() && isNeedProxy) {
				huc = (HttpURLConnection) url.openConnection(ProxyManager.getProxy());
			} else {
				huc = (HttpURLConnection) url.openConnection();
			}

			huc.setRequestMethod("GET");
			huc.setRequestProperty("Content-Type", "text/html");
			if (EnvManager.isGamma() && isNeedProxy) {
				huc = (HttpURLConnection) url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyIP, proxyPort)));
			} else {
				huc.setRequestProperty("Host", url.getHost());
			}
			if (needHeadRefer) {
				huc.setRequestProperty("Referer", "app.paipai.com");
			}
			if (!StringUtil.isEmpty(cookieStr)) {
				huc.setRequestProperty("Cookie", cookieStr);
			}
			huc.setConnectTimeout(connectTimeout);
			huc.setReadTimeout(readTimeout);

			huc.connect();

	/*		reader = new BufferedReader(new InputStreamReader(
					huc.getInputStream(), encoding));
			*/
			reader = new BufferedReader(new InputStreamReader(
					new UrlConnProxy(huc, true, encoding).getInputStream(), encoding));

			char[] buffer = new char[4096];
			writer = new StringWriter();
			int n;
			while (-1 != (n = reader.read(buffer))) {
				writer.write(buffer, 0, n);
			}
			String content = writer.toString();

			rspCode = huc.getResponseCode();

			result = "SUCCESS";

			return content;
		} catch (Exception e) {
			result = "fail";
			logger.error("HTTP GET |" + result + "|" + rspCode + "|" + urlStr,
					e);
			return null;
		} finally {
			try {
				if (huc != null)
					huc.disconnect();
				if (writer != null)
					writer.close();
				if (reader != null)
					reader.close();
			} catch (Exception e) {
			}

			long timeCost = System.currentTimeMillis() - begin;
			logger.info("HTTP GET |" + result + "|" + rspCode + "|" + timeCost
					+ "ms" + "|" + urlStr + "|" + hostName);
		}
	}

	
//	public static String getWithProxy(String urlStr, Map<String, String> params,
//			int connectTimeout, int readTimeout, String requestCharset,
//			String encoding, boolean isNeedProxy, String cookieStr,
//			String host, boolean isEncode,String proxyIP,int proxyPort) {
//		HttpURLConnection huc = null;
//		String lurl = urlStr;
//		String res = "";
//		int rspCode = -1;
//		String result = "fail";
//		long begin = System.currentTimeMillis();
//		try {
//			URL url;
//			String param = toQueryString(params, requestCharset, isEncode);
//			if (param != null) {
//				// String paramStr = StringUtil.trim();
//				// lurl += paramStr;
//				lurl += "?";
//				lurl += StringUtil.trim(param);
//			}
//			url = new URL(urlStr);
//
//			if (EnvManager.isGAMMAEnvCurrent() && isNeedProxy) {
//				huc = (HttpURLConnection) url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyIP, proxyPort)));
//			} else {
//				huc = (HttpURLConnection) url.openConnection();
//			}
//
//			huc.setConnectTimeout(connectTimeout);
//			huc.setReadTimeout(readTimeout);
//			huc.setRequestMethod("GET");
//			if (!StringUtil.isEmpty(host))
//				huc.setRequestProperty("host", host);
//			huc.setRequestProperty("Content-Type",
//					"application/x-www-form-urlencoded");
//			if (!StringUtil.isEmpty(cookieStr)) {
//				huc.setRequestProperty("Cookie", cookieStr);
//			}
//			if (param != null) {
//				huc.setDoOutput(true);
//				OutputStreamWriter wr = new OutputStreamWriter(
//						huc.getOutputStream(), requestCharset);
//				// wr.write(paramStr);
//				wr.write(StringUtil.trim(param));
//				wr.flush();
//			}
//
//			rspCode = huc.getResponseCode();
//
//			if (rspCode == 200) {
//				BufferedInputStream read = new BufferedInputStream(
//						huc.getInputStream());
//				byte[] b = new byte[1024 * 100];
//				int len = 0;
//				while (len != -1) {
//					len = read.read(b);
//					if (len > 0) {
//						res += new String(b, 0, len, encoding);
//					}
//				}
//				result = "succ";
//			} else {
//				logger.error("post|" + result + "|" + rspCode + "|" + lurl);
//			}
//
//			return res;
//		} catch (Exception e) {
//			result = "fail";
//			logger.error("post|" + result + "|" + rspCode + "|" + lurl, e);
//			return "";
//		} finally {
//			if (huc != null)
//				huc.disconnect();
//			long end = System.currentTimeMillis();
//			logger.info("post|" + result + "|" + rspCode + "|" + (end - begin)
//					+ "|" + lurl);
//		}
//	}
	
	
	protected static String getJdPopHttpSvcProxyHost(String urlStr, String hostName,
			int connectTimeout, int readTimeout, String encoding,
			boolean isNeedProxy, boolean needHeadRefer, String cookieStr) {
		HttpURLConnection huc = null;
		String result = "fail";
		int rspCode = 0;
		StringWriter writer = null;
		BufferedReader reader = null;

		long begin = System.currentTimeMillis();
		try {
			URL url = new URL(urlStr);

			// 增加代理服务器访问外网
			if (!EnvManager.isLocal() && isNeedProxy) {
				// proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(
				// "172.23.28.199", 8080));
				huc = (HttpURLConnection) url.openConnection(ProxyManager
						.getJdPopHttpSvcProxy());
			} else {
				huc = (HttpURLConnection) url.openConnection();
			}

			huc.setRequestMethod("GET");
			huc.setRequestProperty("Content-Type", "text/html");
			if (!StringUtil.isEmpty(hostName)) {
				huc.setRequestProperty("Host", hostName);
			} else {
				huc.setRequestProperty("Host", url.getHost());
			}
			if (needHeadRefer) {
				huc.setRequestProperty("Referer", "app.paipai.com");
			}
			if (!StringUtil.isEmpty(cookieStr)) {
				huc.setRequestProperty("Cookie", cookieStr);
			}
			huc.setConnectTimeout(connectTimeout);
			huc.setReadTimeout(readTimeout);

			huc.connect();

			reader = new BufferedReader(new InputStreamReader(
					new UrlConnProxy(huc, true, encoding).getInputStream(), encoding));

			char[] buffer = new char[4096];
			writer = new StringWriter();
			int n;
			while (-1 != (n = reader.read(buffer))) {
				writer.write(buffer, 0, n);
			}
			String content = writer.toString();

			rspCode = huc.getResponseCode();

			result = "SUCCESS";

			return content;
		} catch (Exception e) {
			result = "fail";
			logger.error("HTTP GET |" + result + "|" + rspCode + "|" + urlStr,
					e);
			return null;
		} finally {
			try {
				if (huc != null)
					huc.disconnect();
				if (writer != null)
					writer.close();
				if (reader != null)
					reader.close();
			} catch (Exception e) {
			}

			long timeCost = System.currentTimeMillis() - begin;
			logger.info("HTTP GET |" + result + "|" + rspCode + "|" + timeCost
					+ "ms" + "|" + urlStr + "|" + hostName);
		}
	}
	
	
	public static String get(String urlStr, String hostName,
			int connectTimeout, int readTimeout, String encoding,
			boolean isNeedProxy, boolean needHeadRefer, String cookieStr,String refer) {
		HttpURLConnection huc = null;
		String result = "fail";
		int rspCode = 0;
		StringWriter writer = null;
		BufferedReader reader = null;

		try {
			URL url = new URL(urlStr);

			// 增加代理服务器访问外网
			if (!EnvManager.isLocal() && isNeedProxy) {
				// proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(
				// "172.23.28.199", 8080));
				huc = (HttpURLConnection) url.openConnection(ProxyManager
						.getProxy());
			} else {
				huc = (HttpURLConnection) url.openConnection();
			}

			huc.setRequestMethod("GET");
			huc.setRequestProperty("Content-Type", "text/html");
			if (!StringUtil.isEmpty(hostName)) {
				huc.setRequestProperty("Host", hostName);
			} else {
				huc.setRequestProperty("Host", url.getHost());
			}
			if (needHeadRefer) {
				huc.setRequestProperty("Referer", refer);
			}
			if (!StringUtil.isEmpty(cookieStr)) {
				huc.setRequestProperty("Cookie", cookieStr);
			}
			huc.setConnectTimeout(connectTimeout);
			huc.setReadTimeout(readTimeout);

			huc.connect();

			reader = new BufferedReader(new InputStreamReader(
					new UrlConnProxy(huc, true, encoding).getInputStream(), encoding));

			char[] buffer = new char[4096];
			writer = new StringWriter();
			int n;
			while (-1 != (n = reader.read(buffer))) {
				writer.write(buffer, 0, n);
			}
			String content = writer.toString();

			rspCode = huc.getResponseCode();

			result = "SUCCESS";

			return content;
		} catch (Exception e) {
			result = "fail";
			logger.error("HTTP GET |" + result + "|" + rspCode + "|" + urlStr,
					e);
			return null;
		} finally {
			try {
				if (huc != null)
					huc.disconnect();
				if (writer != null)
					writer.close();
				if (reader != null)
					reader.close();
			} catch (Exception e) {
			}
		}
	}

	/**
	 * 通过GET的方式通过URL和HttpURLConnection去完成HTTP请求，如果返回null，则代表请求过程发生异常或出现HTTP错误(
	 * 非200) 主要用于读取静态的文件
	 * 
	 * @param urlStr
	 * @param connectTimeout
	 * @param readTimeout
	 * @param encoding
	 * @param isNeedProxy
	 * @return
	 */
	public static String get(String urlStr, int connectTimeout,
			int readTimeout, String encoding, boolean isNeedProxy) {
		return get(urlStr, null, connectTimeout, readTimeout, encoding,
				isNeedProxy, false, null);
	}
	
	public static String getJdPopHttpSvcProxyHost(String urlStr, int connectTimeout,
			int readTimeout, String encoding, boolean isNeedProxy) {
		return getJdPopHttpSvcProxyHost(urlStr, null, connectTimeout, readTimeout, encoding,
				isNeedProxy, false, null);
	}

	/**
	 * 通过GET的方式通过URL和HttpURLConnection去完成HTTP请求，如果返回null，则代表请求过程发生异常或出现HTTP错(
	 * 非200) 用于发送HTTP请求时要带上Cookie的情况 Add by wamiwen
	 * 
	 * @param urlStr
	 * @param connectTimeout
	 * @param readTimeout
	 * @param encoding
	 * @param isNeedProxy
	 * @param cookieStr
	 * @return
	 */
	public static String get(String urlStr, int connectTimeout,
			int readTimeout, String encoding, boolean isNeedProxy,
			String cookieStr) {
		return get(urlStr, null, connectTimeout, readTimeout, encoding,
				isNeedProxy, false, cookieStr);
	}

	public static String get(String urlStr, String hostName,
			int connectTimeout, int readTimeout, String encoding,
			boolean isNeedProxy) {
		return get(urlStr, hostName, connectTimeout, readTimeout, encoding,
				isNeedProxy, false, null);
	}

	/**
	 * POST
	 * 
	 * @param urlStr
	 *            请求串
	 * @param params
	 *            参数
	 * @param connectTimeout
	 *            连接超时时间
	 * @param readTimeout
	 *            读超时时间
	 * @param requestCharset
	 *            编码方式
	 * @param encoding
	 *            解码方式
	 * @param isNeedProxy
	 *            是否要代理
	 * 
	 * @return
	 */
	public static String post(String urlStr, Map<String, String> params,
			int connectTimeout, int readTimeout, String requestCharset,
			String encoding, boolean isNeedProxy) {
		return post(urlStr, params, connectTimeout, readTimeout,
				requestCharset, encoding, isNeedProxy, null);
	}

	/**
	 * use in vb2c only, transfer-encoding will always be trunked.
	 * <p>
	 * 使用UTF-8编码请求URL并有UTF-8编码解析回包
	 * </p>
	 * 
	 * @param urlStr
	 * @param params
	 * @param connectTimeout
	 * @param readTimeOut
	 * @param requestCharset
	 * @param defaultResponseCharset
	 * @return
	 * @throws IOException
	 */
	public static String post(String urlStr, Map<String, String> params,
			String charset, String host, Proxy proxy, int connectTimeout,
			int readTimeOut) throws IOException {

		HttpURLConnection huc = null;
		try {
			if (StringUtils.isEmpty(charset)) {
				charset = com.qq.qqbuy.common.constant.BizConstant.OPENAPI_CHARSET;
			}
			URL url;
			String paramStr = StringUtil.trim(toQueryString(params, charset));
			url = new URL(urlStr);
			if (proxy != null) {
				huc = (HttpURLConnection) url.openConnection(proxy);
			} else {
				huc = (HttpURLConnection) url.openConnection();
			}

			huc.setConnectTimeout(connectTimeout);
			huc.setReadTimeout(readTimeOut);
			huc.setRequestMethod("POST");
			// TEST ONLY
			if (StringUtils.isNotEmpty(host)) {
				huc.setRequestProperty("Host", host);
			}
			huc.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			if (!StringUtil.isEmpty(paramStr)) {
				huc.setDoOutput(true);
				OutputStreamWriter wr = new OutputStreamWriter(
						huc.getOutputStream(), charset);
				wr.write(paramStr);
				wr.flush();
			}
			InputStream is = huc.getInputStream();
			byte[] buf = new byte[1024];
			ByteArrayOutputStream bos = new ByteArrayOutputStream(2048);
			int length = -1;
			while ((length = is.read(buf)) >= 0) {
				bos.write(buf, 0, length);
			}
			buf = bos.toByteArray();
			return new String(buf, charset);
		} finally {
			if (huc != null)
				huc.disconnect();
		}
	}

	/**
	 * 通过POST的方式通过URL和HttpURLConnection去完成HTTP请求，如果返回null，则代表请求过程发生异常或出现HTTP错(
	 * 非200) 用于发送HTTP请求时要带上Cookie的情况 Add by wamiwen
	 * 
	 * @param urlStr
	 *            请求串
	 * @param params
	 *            参数
	 * @param connectTimeout
	 *            连接超时时间
	 * @param readTimeout
	 *            读超时时间
	 * @param requestCharset
	 *            编码方式
	 * @param encoding
	 *            解码方式
	 * @param isNeedProxy
	 *            是否要代理
	 * @param cookieStr
	 *            信息
	 * @return
	 */
	public static String post(String urlStr, Map<String, String> params,
			int connectTimeout, int readTimeout, String requestCharset,
			String encoding, boolean isNeedProxy, String cookieStr) {
		return post(urlStr, params, connectTimeout, readTimeout,
				requestCharset, encoding, isNeedProxy, cookieStr, null);
	}

	/**
	 * 通过POST的方式通过URL和HttpURLConnection去完成HTTP请求，如果返回null，则代表请求过程发生异常或出现HTTP错(
	 * 非200) 用于发送HTTP请求时要带上Cookie的情况 Add by wamiwen
	 * 
	 * @param urlStr
	 *            请求串
	 * @param params
	 *            参数
	 * @param connectTimeout
	 *            连接超时时间
	 * @param readTimeout
	 *            读超时时间
	 * @param requestCharset
	 *            编码方式
	 * @param encoding
	 *            解码方式
	 * @param isNeedProxy
	 *            是否要代理
	 * @param cookieStr
	 *            信息
	 * @param host
	 *            host域名
	 * @return
	 */
	public static String post(String urlStr, Map<String, String> params,
			int connectTimeout, int readTimeout, String requestCharset,
			String encoding, boolean isNeedProxy, String cookieStr, String host) {
		return post(urlStr, params, connectTimeout, readTimeout,
				requestCharset, encoding, isNeedProxy, cookieStr, host, true);
	}

	/**
	 * 通过POST的方式通过URL和HttpURLConnection去完成HTTP请求，如果返回null，则代表请求过程发生异常或出现HTTP错(
	 * 非200) 用于发送HTTP请求时要带上Cookie的情况 Add by wamiwen
	 * 
	 * @param urlStr
	 *            请求串
	 * @param params
	 *            参数
	 * @param connectTimeout
	 *            连接超时时间
	 * @param readTimeout
	 *            读超时时间
	 * @param requestCharset
	 *            编码方式
	 * @param encoding
	 *            解码方式
	 * @param isNeedProxy
	 *            是否要代理
	 * @param cookieStr
	 *            cookie信息
	 * @param host
	 *            host域名
	 * 
	 * @return
	 */
	public static String post(String urlStr, Map<String, String> params,
			int connectTimeout, int readTimeout, String requestCharset,
			final String encoding, boolean isNeedProxy, String cookieStr,
			String host, boolean isEncode) {
		HttpURLConnection huc = null;
		String lurl = urlStr;
		String res = "";
		int rspCode = -1;
		String result = "fail";
		long begin = System.currentTimeMillis();
		try {
			URL url;
			final String param = toQueryString(params, requestCharset, isEncode);
			if (param != null) {
				// String paramStr = StringUtil.trim();
				// lurl += paramStr;
				lurl += "?";
				lurl += StringUtil.trim(param);
			}
			url = new URL(urlStr);

			/**
			 * 增加代理服务器访问外网
			 */
			if (!EnvManager.isLocal() && isNeedProxy) {
				huc = (HttpURLConnection) url.openConnection(ProxyManager
						.getProxy());
			} else {
				huc = (HttpURLConnection) url.openConnection();
			}

			huc.setConnectTimeout(connectTimeout);
			huc.setReadTimeout(readTimeout);
			huc.setRequestMethod("POST");
			if (!StringUtil.isEmpty(host))
				huc.setRequestProperty("host", host);
			huc.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			if (!StringUtil.isEmpty(cookieStr)) {
				Log.run.info("cookieStr-----------"+cookieStr);
				huc.setRequestProperty("Cookie", cookieStr);
			}
			if (param != null) {
				huc.setDoOutput(true);
				OutputStreamWriter wr = new OutputStreamWriter(
						huc.getOutputStream(), requestCharset);
				// wr.write(paramStr);
				wr.write(StringUtil.trim(param));
				wr.flush();
			}

			rspCode = huc.getResponseCode();

			if (rspCode == 200) {
				
//				BufferedInputStream read = new BufferedInputStream(
//						new UrlConnProxy(huc, true, encoding).getInputStream());
				BufferedInputStream read = new BufferedInputStream(
						new UrlConnProxy(huc, new PacketLogParser(){

							@Override
							public String getServiceName() {
								return null;
							}

							@Override
							public String parseRequest(byte[] request)
									throws Exception {
								return StringUtil.trim(param);
							}

							@Override
							public String parseResponse(byte[] response)
									throws Exception {
								if (response==null) {
									return "";
								}
								return new String(response, encoding);
							}
							
						}).getInputStream());
				
				byte[] b = new byte[1024 * 100];
				int len = 0;
				while (len != -1) {
					len = read.read(b);
					if (len > 0) {
						res += new String(b, 0, len, encoding);
					}
				}
				result = "succ";
			} else {
				logger.error("post|" + result + "|" + rspCode + "|" + lurl);
			}

			return res;
		} catch (Exception e) {
			result = "fail";
			logger.error("post|" + result + "|" + rspCode + "|" + lurl, e);
			return "";
		} finally {
			if (huc != null)
				huc.disconnect();
			long end = System.currentTimeMillis();
			logger.info("post|" + result + "|" + rspCode + "|" + (end - begin)
					+ "|" + lurl);
		}
	}

	/**
	 * POST上传文件 Add by wamiwen
	 * 
	 * @param urlStr
	 * @param params
	 * @param file
	 * @param name
	 * @param filename
	 * @param connectTimeout
	 * @param readTimeout
	 * @param requestCharset
	 * @param encoding
	 * @param isNeedProxy
	 * @return
	 */
	public static String uploadFile(String urlStr, Map<String, String> params,
			File file, String name, String filename, int connectTimeout,
			int readTimeout, String requestCharset, String encoding,
			boolean isNeedProxy) {
		if (!file.exists()) {
			logger.error("uploadFile fail! file not exist!");
			return "";
		}

		URL url = null;
		HttpURLConnection conn = null;

		OutputStream out = null;
		String res = "";
		String result = "fail";
		int rspCode = -1;

		long begin = System.currentTimeMillis();
		try {
			url = new URL(urlStr);

			/**
			 * 增加代理服务器访问外网
			 */
			if (!EnvManager.isLocal() && isNeedProxy) {
				conn = (HttpURLConnection) url.openConnection(ProxyManager
						.getProxy());
			} else {
				conn = (HttpURLConnection) url.openConnection();
			}
			conn.setConnectTimeout(connectTimeout);
			conn.setReadTimeout(readTimeout);

			final String POST_BOUNDARY = java.util.UUID.randomUUID().toString();
			final String POST_PREFIX = "--";
			final String CRLF = "\r\n";
			final String POST_END = POST_PREFIX + POST_BOUNDARY + POST_PREFIX
					+ CRLF;

			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(false);
			conn.setRequestMethod("POST");
			conn.setRequestProperty(
					"User-Agent",
					"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11");
			conn.setRequestProperty("Charsert", requestCharset);
			conn.setRequestProperty("Content-Type",
					"multipart/form-data; boundary=" + POST_BOUNDARY);

			StringBuilder sb = new StringBuilder();
			if (params != null) {
				for (String keyString : params.keySet()) {
					sb.append(POST_PREFIX).append(POST_BOUNDARY);
					sb.append(CRLF);
					sb.append("Content-Disposition: form-data; ");
					sb.append("name=\"").append(keyString).append("\"");
					sb.append(CRLF);
					sb.append(CRLF);
					sb.append(params.get(keyString));
					sb.append(CRLF);
				}
			}

			sb.append(POST_PREFIX).append(POST_BOUNDARY);
			sb.append(CRLF);
			sb.append("Content-Disposition: form-data; ");
			sb.append("name=\"").append(name).append("\"; ");
			sb.append("filename=\"").append(filename).append("\"");
			sb.append(CRLF);
			sb.append("Content-Type: image/png");
			sb.append(CRLF);
			sb.append(CRLF);

			conn.connect();
			out = conn.getOutputStream();

			/**
			 * 传参数
			 */
			if (sb.length() > 0) {
				out.write(sb.toString().getBytes(requestCharset));
			}

			/**
			 * 传输文件
			 */
			DataInputStream in = new DataInputStream(new FileInputStream(file));
			int bytes = 0;
			byte[] bufferOut = new byte[1024];
			while ((bytes = in.read(bufferOut)) != -1) {
				out.write(bufferOut, 0, bytes);
			}
			out.write(CRLF.getBytes(requestCharset));
			in.close();

			/**
			 * 传结束符
			 */
			out.write(POST_END.getBytes(requestCharset));

			out.close();

			rspCode = conn.getResponseCode();
			if (rspCode == 200) {
				BufferedInputStream read = new BufferedInputStream(
						conn.getInputStream());
				byte[] b = new byte[1024 * 100];
				int len = 0;
				while (len != -1) {
					len = read.read(b);
					if (len > 0) {
						res += new String(b, 0, len, encoding);
					}
				}
				result = "succ";
				logger.info("uploadFile|" + result + "|" + rspCode + "|" + res);
			} else {
				logger.error("uploadFile|" + result + "|" + rspCode + "|"
						+ urlStr);
			}

			return res;
		} catch (Exception e) {
			result = "fail";
			logger.error("uploadFile|" + result + "|" + rspCode + "|" + urlStr,
					e);
			return "";
		} finally {
			if (conn != null)
				conn.disconnect();
			long end = System.currentTimeMillis();
			logger.info("uploadFile|" + result + "|" + rspCode + "|"
					+ (end - begin) + "|" + urlStr);
		}

	}

	public static String toQueryString(Map<String, String> params,
			String charset, boolean isEncode)
			throws UnsupportedEncodingException {
		if (null == params || params.size() == 0) {
			return null;
		}
		StringBuilder str = new StringBuilder("");
		for (Map.Entry<String, String> e : params.entrySet()) {
			appendParam(e.getKey(), e.getValue(), str, "&", charset, isEncode);
		}
		return str.toString();
	}

	public static String toQueryString(Map<String, String> params,
			String charset) throws UnsupportedEncodingException {
		return toQueryString(params, charset, true);
	}

	private static StringBuilder appendParam(String key, String value,
			StringBuilder sb, String ampersand, String charset, boolean isEncode)
			throws UnsupportedEncodingException {
		if (sb.length() > 0) {
			sb.append(ampersand);
		}
		sb.append(URLEncoder.encode(key, charset));
		sb.append("=");
		if (value != null) {
			if (isEncode)
				sb.append(URLEncoder.encode(value, charset));
			else
				sb.append(value);
		}
		return sb;
	}

	public static String decode(String str) {
		try {
			return URLDecoder.decode(str, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}
	
	public static String getCookie(String urlStr, String hostName, int connectTimeout, int readTimeout,
			String encoding, boolean isNeedProxy, boolean needHeadRefer, boolean needCookie, String cookieStr) {
		HttpURLConnection huc = null;
		String result = "fail";
		int rspCode = 0;

		long begin = System.currentTimeMillis();
		try {
			URL url = new URL(urlStr);

			// 增加代理服务器访问外网
			if (!EnvManager.isLocal() && isNeedProxy) {
				huc = (HttpURLConnection) url.openConnection(ProxyManager.getProxy());
			} else {
				huc = (HttpURLConnection) url.openConnection();
			}

			huc.setRequestMethod("GET");
			huc.setRequestProperty("Content-Type", "text/html");
			if (!StringUtil.isEmpty(hostName)) {
				huc.setRequestProperty("Host", hostName);
			} else {
				huc.setRequestProperty("Host", url.getHost());
			}
			if (needHeadRefer) {
				huc.setRequestProperty("Referer", "api.m.paipai.com");
			}
			if (!StringUtil.isEmpty(cookieStr)) {
				huc.setRequestProperty("Cookie", cookieStr);
			}
			huc.setConnectTimeout(connectTimeout);
			huc.setReadTimeout(readTimeout);
			huc.connect();
			
			// 取cookie值
			String visitkeyCookie = "";
			if (needCookie) {
				String key = null;
				for (int i = 1; (key = huc.getHeaderFieldKey(i)) != null; i++) {
					if (key.equalsIgnoreCase("set-cookie")) {
						String cookies[] = huc.getHeaderField(i).split(";");
						for (String cookie : cookies) {
							String cookieItems[] = cookie.split("=");
							if ("visitkey".equalsIgnoreCase(cookieItems[0].trim())) {
								visitkeyCookie = cookieItems[1].trim();
								break;
							}
						}
						break;
					}
				}
			}
						
			rspCode = huc.getResponseCode();

			result = "SUCCESS";

			return visitkeyCookie;
		} catch (Exception e) {
			result = "fail";
			logger.error("HTTP GET |" + result + "|" + rspCode + "|" + urlStr, e);
			return null;
		} finally {
			try {
				if (huc != null)
					huc.disconnect();
			} catch (Exception e) {
			}

			long timeCost = System.currentTimeMillis() - begin;
			logger.info("HTTP GET |" + result + "|" + rspCode + "|" + timeCost + "ms" + "|" + urlStr + "|" + hostName);
		}
	}
	
	public static String post(String urlStr, Map<String, String> params,
			int connectTimeout, int readTimeout, String requestCharset,
			final String encoding, boolean isNeedProxy, String cookieStr,
			String host, boolean isEncode,String agent) {
		HttpURLConnection huc = null;
		String lurl = urlStr;
		String res = "";
		int rspCode = -1;
		String result = "fail";
		long begin = System.currentTimeMillis();
		try {
			URL url;
			final String param = toQueryString(params, requestCharset, isEncode);
			if (param != null) {
				// String paramStr = StringUtil.trim();
				// lurl += paramStr;
				lurl += "?";
				lurl += StringUtil.trim(param);
			}
			url = new URL(urlStr);

			/**
			 * 增加代理服务器访问外网
			 */
			if (!EnvManager.isLocal() && isNeedProxy) {
				huc = (HttpURLConnection) url.openConnection(ProxyManager
						.getProxy());
			} else {
				huc = (HttpURLConnection) url.openConnection();
			}

			huc.setConnectTimeout(connectTimeout);
			huc.setReadTimeout(readTimeout);
			huc.setRequestMethod("POST");
			if(!StringUtil.isEmpty(agent)){
			huc.setRequestProperty("User-agent",agent);
			}
			if (!StringUtil.isEmpty(host))
				huc.setRequestProperty("host", host);
			huc.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			if (!StringUtil.isEmpty(cookieStr)) {
				Log.run.info("cookieStr-----------"+cookieStr+",agent---"+agent);
				huc.setRequestProperty("Cookie", cookieStr);
			}
			if (param != null) {
				huc.setDoOutput(true);
				OutputStreamWriter wr = new OutputStreamWriter(
						huc.getOutputStream(), requestCharset);
				// wr.write(paramStr);
				wr.write(StringUtil.trim(param));
				wr.flush();
			}

			rspCode = huc.getResponseCode();

			if (rspCode == 200) {
				
//				BufferedInputStream read = new BufferedInputStream(
//						new UrlConnProxy(huc, true, encoding).getInputStream());
				BufferedInputStream read = new BufferedInputStream(
						new UrlConnProxy(huc, new PacketLogParser(){

							@Override
							public String getServiceName() {
								return null;
							}

							@Override
							public String parseRequest(byte[] request)
									throws Exception {
								return StringUtil.trim(param);
							}

							@Override
							public String parseResponse(byte[] response)
									throws Exception {
								if (response==null) {
									return "";
								}
								return new String(response, encoding);
							}
							
						}).getInputStream());
				
				byte[] b = new byte[1024 * 100];
				int len = 0;
				while (len != -1) {
					len = read.read(b);
					if (len > 0) {
						res += new String(b, 0, len, encoding);
					}
				}
				result = "succ";
			} else {
				logger.error("post|" + result + "|" + rspCode + "|" + lurl);
			}

			return res;
		} catch (Exception e) {
			result = "fail";
			logger.error("post|" + result + "|" + rspCode + "|" + lurl, e);
			return "";
		} finally {
			if (huc != null)
				huc.disconnect();
			long end = System.currentTimeMillis();
			logger.info("post|" + result + "|" + rspCode + "|" + (end - begin)
					+ "|" + lurl);
		}
	}
	
}
