package com.qq.qqbuy.common.util;

import java.net.URLDecoder;
import java.net.URLEncoder;

import net.sf.json.JSONObject;

import com.qq.qqbuy.login.po.AppToken;
import com.qq.qqbuy.login.po.JDSdkAppToken;
import com.qq.qqbuy.login.po.QQSdkAppToken;
import com.qq.qqbuy.login.po.WXSdkAppToken;
import com.qq.qqbuy.login.po.WtLoginAppToken;

public class TokenUtil {
	public static final String LOGIN_THREEDES_KEY = "jdppxltdwAoimbvy862wzqvy";// 生成token进行des加密的秘钥
	public static final String LOGIN_KEY = "paipai@YPFEwe8eiouFHSAfjauief%&Fas";// 参数加密密钥
	public static final String LOGIN_TYPE_QQ = "qq";// qq opensdk login
	public static final String LOGIN_TYPE_WT = "wt";// wtlgoin
	public static final String LOGIN_TYPE_WT_SK = "wtsk";// wtlgoin 用户wt调用获取sk接口
	public static final String LOGIN_TYPE_JD = "jd";// jd opensdk login
	public static final String LOGIN_TYPE_WX = "wx";// wx openSdk login
	public static final String LOGIN_TYPE_PP = "pp";// paipai登陆态根据WID，SK换取TOKEN
	public static final char TOKEN_PARTITION = 1;//token 各个字段分割符
	
	public static final int LOGIN_INTENSITY_TYPE_APP  = 4; //app login
	
	public static final String QQ_OPEN_SDK_CHECK_URL = "https://openmobile.qq.com/user/get_simple_userinfo" ;//QQsdk登陆校验
	public static final String QQ_OPEN_SDK_GET_UIN = "http://openmqzone.tencentb2.com:8080/cgi-bin/appstage/trans_openid_uin" ;//用openid转uin
	public static final String QQ_OPEN_SDK_GET_UIN_REF = "paipaiapp" ;
	public static final String QQ_OPEN_SDK_GET_UIN_APP_KEY = "d3d5b55004c24db4af15241e1895a5fd" ;
	public static final String QQ_OPEN_SDK_GET_UIN_APP_ID = "1102335388" ; //应用的appid
	public static final int JD_OPEN_SDK_GET_UIN_APP_ID = 107 ; //应用的appid
	public static final String WX_OPEN_SDK_GET_UIN_APP_ID = "wx6e04fccd996dc0d1" ;//微信APPID
//	public static final String WX_OPEN_SDK_GET_UIN_APP_ID = "wx33b21f044ebf5163" ;//微信APPID----微店
	public static final String WX_OPEN_SDK_GET_UIN_APP_SECRET = "e2262c7e0ec6e0106d11ffb50d15739b";//应用密钥AppSecret
	
	public static final String OPEN_SDK_IDL_AUTHCODE = "zxcvbnm";

	public static final Long WT_IDL_LOGINFROM = 13l;
	public static final Long WT_ACCOUNT_TYPE = 1L;
	public static final String 	WT_SOURCE = "appWTLOGIN" ;
	
	public static final Long QQ_OPEN_SDK_IDL_LOGINFROM = 13l;
	public static final Long QQ_OPEN_SDK_IDL_ACCOUNT_TYPE = 1L;
	public static final Long QQ_OPEN_SDK_IDL_OPENIDFROM = 10L;
	public static final String 	QQ_OPEN_SDK_IDL_SOURCE = "appQQSDK" ;

	public static final String JD_OPEN_SDK_GET_USER_INFO_URL = "http://soa.u.jd.com/u/getUserInfo" ;//jdsdk登陆获取用户信息
	public static final Long JD_OPEN_SDK_IDL_ACCOUNT_TYPE = 3L;
	public static final int JD_OPEN_SDK_IDL_LOGINFROM = 12;
	public static final Long JD_OPEN_SDK_IDL_OPENIDFROM = 9L;
	public static final String JD_OPEN_SDK_IDL_SOURCE = "appJDSDK" ;
	public static final String JD_PIC_URL = "http://jss.jd.com/outLinkServicePoint/" ;
	
	public static final String JD_OPEN_SDK_GET_USER_INFO_TOKEN = "LKKJR8934KFJ7897FLNY847364L" ;
	public static final String JD_CHECK_LOGIN_PROXY_IP_PORT = "10.198.15.151:10074|10.198.15.170:10074|10.198.15.151:10075|10.198.15.170:10075|10.198.15.151:10076|10.198.15.170:10076" ;

	public static final String WX_OPEN_SDK_GET_ID_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token"; //微信登录获取用户access_token
	public static final String WX_OPEN_SDK_GET_USERINFO_URL = "https://api.weixin.qq.com/sns/userinfo"; //获取微信用户用户信息
	
	public static final Long WX_OPEN_SDK_IDL_LOGINFROM = 7L;
	public static final Long WX_OPEN_SDK_IDL_ACCOUNT_TYPE = 3L;
	public static final Long WX_OPEN_SDK_IDL_OPENIDFROM = 11L;
	public static final String WX_OPEN_SDK_IDL_SOURCE = "appWXSDK" ;
	/**
	 * 将Token转换成返回的加密字符串
	 * 
	 * @param token
	 * @return
	 * @throws EncodingException
	 */
	public static String wtTokenToStr(WtLoginAppToken token) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(token.getAppID()).append(TOKEN_PARTITION)
			.append(token.getDeadline()).append(TOKEN_PARTITION)
			.append(token.getLatitude()).append(TOKEN_PARTITION)
			.append(token.getLk()).append(TOKEN_PARTITION)
			.append(token.getLongitude()).append(TOKEN_PARTITION)
			.append(token.getLsk()).append(TOKEN_PARTITION)
			.append(token.getMk()).append(TOKEN_PARTITION)
			.append(token.getMt()).append(TOKEN_PARTITION)
			.append(token.getQq()).append(TOKEN_PARTITION)
			.append(token.getSalt()).append(TOKEN_PARTITION)
			.append(token.getVersionCode()).append(TOKEN_PARTITION)
			.append(token.getWid()) ;			
		return URLEncoder.encode(token.getType() + ThreeDesUtil.threeDESencode(
						sb.toString(), LOGIN_THREEDES_KEY), "utf-8");
	}

	/**
	 * 将加密好的token转换成Loken对象
	 * 
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public static WtLoginAppToken strToWtToken(String token) throws Exception {
		String tokenStr = ThreeDesUtil.threeDESdecrypt(token.substring(2),
				LOGIN_THREEDES_KEY);
		String[] tokenArr = tokenStr.split(String.valueOf(TOKEN_PARTITION)) ;
		if ( 12 == tokenArr.length) {
			WtLoginAppToken appToken = new WtLoginAppToken();
			appToken.setAppID(tokenArr[0]) ;
			appToken.setDeadline(Long.parseLong(tokenArr[1])) ;
			appToken.setLatitude(tokenArr[2]) ;
			appToken.setLk(tokenArr[3]) ;
			appToken.setLongitude(tokenArr[4]);
			appToken.setLsk(tokenArr[5]) ;
			appToken.setMk(tokenArr[6]) ;
			appToken.setMt(tokenArr[7]) ;
			appToken.setQq(Long.parseLong(tokenArr[8])) ;
			appToken.setSalt(Long.parseLong(tokenArr[9])) ;
			appToken.setVersionCode(tokenArr[10]) ;
			appToken.setWid(Long.parseLong(tokenArr[11])) ;
			appToken.setType(token.substring(0,2));
			return appToken;
		} else {
			return null ;
		}	
		
	}

	
	/**
	 * 将Token转换成返回的加密字符串
	 * 
	 * @param token
	 * @return
	 * @throws EncodingException
	 */
	public static String qqTokenToStr(QQSdkAppToken token) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(token.getAccessToken()).append(TOKEN_PARTITION)
			.append(token.getAppID()).append(TOKEN_PARTITION)
			.append(token.getDeadline()).append(TOKEN_PARTITION)
			.append(token.getExpiresIn()).append(TOKEN_PARTITION)
			.append(token.getLatitude()).append(TOKEN_PARTITION)
			.append(token.getLongitude()).append(TOKEN_PARTITION)
			.append(token.getLsk()).append(TOKEN_PARTITION)
			.append(token.getMk()).append(TOKEN_PARTITION)
			.append(token.getMt()).append(TOKEN_PARTITION)
			.append(token.getOpenId()).append(TOKEN_PARTITION)
			.append(token.getSalt()).append(TOKEN_PARTITION)
			.append(token.getVersionCode()).append(TOKEN_PARTITION)
			.append(token.getWid()) ;			
		return URLEncoder.encode(token.getType() + ThreeDesUtil.threeDESencode(
						sb.toString(), LOGIN_THREEDES_KEY), "utf-8");
	}

	/**
	 * 将加密好的token转换成Loken对象
	 * 
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public static QQSdkAppToken strToQqToken(String token) throws Exception {
		String tokenStr = ThreeDesUtil.threeDESdecrypt(token.substring(2),
				LOGIN_THREEDES_KEY);
		String[] tokenArr = tokenStr.split(String.valueOf(TOKEN_PARTITION)) ;
		if ( 13 == tokenArr.length) {
			QQSdkAppToken appToken = new QQSdkAppToken();
			appToken.setAccessToken(tokenArr[0]) ;
			appToken.setAppID(tokenArr[1]) ;
			appToken.setDeadline(Long.parseLong(tokenArr[2])) ;
			appToken.setExpiresIn(tokenArr[3]);
			appToken.setLatitude(tokenArr[4]) ;
			appToken.setLongitude(tokenArr[5]);
			appToken.setLsk(tokenArr[6]) ;
			appToken.setMk(tokenArr[7]) ;
			appToken.setMt(tokenArr[8]) ;
			appToken.setOpenId(tokenArr[9]) ;
			appToken.setSalt(Long.parseLong(tokenArr[10])) ;
			appToken.setVersionCode(tokenArr[11]) ;
			appToken.setWid(Long.parseLong(tokenArr[12]));
			appToken.setType(token.substring(0,2));
			return appToken;
		} else {
			return null ;
		}	
		
	}
	
	/**
	 * 将Token转换成返回的加密字符串
	 * 
	 * @param token
	 * @return
	 * @throws EncodingException
	 */
	public static String jdTokenToStr(JDSdkAppToken token) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(token.getA2()).append(TOKEN_PARTITION)
			.append(token.getAppID()).append(TOKEN_PARTITION)
			.append(token.getDeadline()).append(TOKEN_PARTITION)
			.append(token.getLatitude()).append(TOKEN_PARTITION)
			.append(token.getLongitude()).append(TOKEN_PARTITION)
			.append(token.getLsk()).append(TOKEN_PARTITION)
			.append(token.getMk()).append(TOKEN_PARTITION)
			.append(token.getMt()).append(TOKEN_PARTITION)
			.append(token.getPin()).append(TOKEN_PARTITION)
			.append(token.getSalt()).append(TOKEN_PARTITION)
			.append(token.getVersionCode()).append(TOKEN_PARTITION)
			.append(token.getWid()) ;			
		return URLEncoder.encode(token.getType() + ThreeDesUtil.threeDESencode(
						sb.toString(), LOGIN_THREEDES_KEY), "utf-8");
	}

	/**
	 * 将加密好的token转换成Loken对象
	 * 
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public static JDSdkAppToken strToJdToken(String token) throws Exception {
		String tokenStr = ThreeDesUtil.threeDESdecrypt(token.substring(2),
				LOGIN_THREEDES_KEY);
		String[] tokenArr = tokenStr.split(String.valueOf(TOKEN_PARTITION)) ;
		if ( 12 == tokenArr.length) {
			JDSdkAppToken appToken = new JDSdkAppToken();
			appToken.setA2(tokenArr[0]) ;
			appToken.setAppID(tokenArr[1]) ;
			appToken.setDeadline(Long.parseLong(tokenArr[2])) ;
			appToken.setLatitude(tokenArr[3]) ;
			appToken.setLongitude(tokenArr[4]);
			appToken.setLsk(tokenArr[5]) ;
			appToken.setMk(tokenArr[6]) ;
			appToken.setMt(tokenArr[7]) ;
			appToken.setPin(tokenArr[8]) ;
			appToken.setSalt(Long.parseLong(tokenArr[9])) ;
			appToken.setVersionCode(tokenArr[10]) ;
			appToken.setWid(Long.parseLong(tokenArr[11]));
			appToken.setType(token.substring(0,2));
			return appToken;
		} else {
			return null ;
		}	
		
	}
	
	/**
	 * 将Token转换成返回的加密字符串
	 * 
	 * @param token
	 * @return
	 * @throws EncodingException
	 */
	public static String wxTokenToStr(WXSdkAppToken token) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(token.getAppID()).append(TOKEN_PARTITION)
			.append(token.getCode()).append(TOKEN_PARTITION)
			.append(token.getDeadline()).append(TOKEN_PARTITION)
			.append(token.getLatitude()).append(TOKEN_PARTITION)
			.append(token.getLongitude()).append(TOKEN_PARTITION)
			.append(token.getLsk()).append(TOKEN_PARTITION)
			.append(token.getMk()).append(TOKEN_PARTITION)
			.append(token.getMt()).append(TOKEN_PARTITION)
			.append(token.getSalt()).append(TOKEN_PARTITION)
			.append(token.getVersionCode()).append(TOKEN_PARTITION)
			.append(token.getWid()).append(TOKEN_PARTITION)
			.append(token.getOpenId()).append(TOKEN_PARTITION)
			.append(token.getAccessToken()).append(TOKEN_PARTITION)
			.append(token.getUnionid());			
		return URLEncoder.encode(token.getType() + ThreeDesUtil.threeDESencode(
						sb.toString(), LOGIN_THREEDES_KEY), "utf-8");
	}

	/**
	 * 将加密好的token转换成Loken对象
	 * 
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public static WXSdkAppToken strToWxToken(String token) throws Exception {
		String tokenStr = ThreeDesUtil.threeDESdecrypt(token.substring(2),
				LOGIN_THREEDES_KEY);
		String[] tokenArr = tokenStr.split(String.valueOf(TOKEN_PARTITION)) ;
		if ( 14 == tokenArr.length) {
			WXSdkAppToken appToken = new WXSdkAppToken();
			appToken.setAppID(tokenArr[0]) ;
			appToken.setCode(tokenArr[1]);
			appToken.setDeadline(Long.parseLong(tokenArr[2])) ;
			appToken.setLatitude(tokenArr[3]) ;
			appToken.setLongitude(tokenArr[4]);
			appToken.setLsk(tokenArr[5]) ;
			appToken.setMk(tokenArr[6]) ;
			appToken.setMt(tokenArr[7]) ;
			appToken.setSalt(Long.parseLong(tokenArr[8])) ;
			appToken.setVersionCode(tokenArr[9]) ;
			appToken.setWid(Long.parseLong(tokenArr[10]));
			appToken.setOpenId(tokenArr[11]) ;
			appToken.setAccessToken(tokenArr[12]) ;
			appToken.setUnionid(tokenArr[13]) ;
			appToken.setType(token.substring(0,2));
			return appToken;
		} else {
			return null ;
		}	
		
	}
	
	/**
	 * 将Token转换成返回的加密字符串
	 * 
	 * @param token
	 * @return
	 * @throws EncodingException
	 */
	public static String appTokenToStr(AppToken token) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append(token.getAppID()).append(TOKEN_PARTITION)
			.append(token.getDeadline()).append(TOKEN_PARTITION)
			.append(token.getLatitude()).append(TOKEN_PARTITION)
			.append(token.getLongitude()).append(TOKEN_PARTITION)
			.append(token.getLsk()).append(TOKEN_PARTITION)
			.append(token.getMk()).append(TOKEN_PARTITION)
			.append(token.getMt()).append(TOKEN_PARTITION)
			.append(token.getSalt()).append(TOKEN_PARTITION)
			.append(token.getVersionCode()).append(TOKEN_PARTITION)
			.append(token.getWid());
		return URLEncoder.encode(token.getType() + ThreeDesUtil.threeDESencode(
						sb.toString(), LOGIN_THREEDES_KEY), "utf-8");
	}

	/**
	 * 将加密好的token转换成Loken对象
	 * 
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public static AppToken strToAppToken(String token) throws Exception {
		String tokenStr = ThreeDesUtil.threeDESdecrypt(token.substring(2),
				LOGIN_THREEDES_KEY);
		String[] tokenArr = tokenStr.split(String.valueOf(TOKEN_PARTITION)) ;
		if ( 10 == tokenArr.length) {
			AppToken appToken = new AppToken();
			appToken.setAppID(tokenArr[0]) ;
			appToken.setDeadline(Long.parseLong(tokenArr[1])) ;
			appToken.setLatitude(tokenArr[2]) ;
			appToken.setLongitude(tokenArr[3]);
			appToken.setLsk(tokenArr[4]) ;
			appToken.setMk(tokenArr[5]) ;
			appToken.setMt(tokenArr[6]) ;
			appToken.setSalt(Long.parseLong(tokenArr[7])) ;
			appToken.setVersionCode(tokenArr[8]) ;
			appToken.setWid(Long.parseLong(tokenArr[9]));
			appToken.setType(token.substring(0,2));
			return appToken;
		} else {
			return null ;
		}	
		
	}

	public static void main(String[] args) throws Exception {
		JDSdkAppToken appToken = new JDSdkAppToken();
		appToken.setA2("a2");
		appToken.setAppID("appId") ;
		appToken.setDeadline(1l) ;
		appToken.setLatitude("latitude") ;
		appToken.setLongitude("longitude");
		appToken.setLsk("lsk") ;
		appToken.setMk("mk") ;
		appToken.setMt("mt") ;
		appToken.setPin("asdf") ;
		appToken.setSalt(3L) ;
		appToken.setVersionCode("versionCode") ;
		appToken.setWid(4L) ;
		appToken.setType("jd");
		String result = jdTokenToStr(appToken) ;
		System.out.println(result);
		JDSdkAppToken result2 = strToJdToken(URLDecoder.decode(result, "utf-8"));
		JSONObject ob = JSONObject.fromObject(result2) ;
		System.out.println(ob.toString());

	}

}
