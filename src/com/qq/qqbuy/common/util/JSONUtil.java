package com.qq.qqbuy.common.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

/**
 * 用于封装Json的操作
 * 
 * @author winsonwu
 * @Created 2011-10-21
 */
public class JSONUtil
{
    public static String getJsonStr(Object obj)
    {
        JSON json = JSONSerializer.toJSON(obj);
        return json.toString();
    }

    /**
     * Converts a given JavaScript native object and converts it to the relevant
     * JSON string.
     * 
     * @param object
     *            JavaScript object
     * @return String JSON
     * @throws JSONException
     */
//    public static String toJSONString(Object object) throws JSONException
//    {
//        JSONStringer json = new JSONStringer();
//
//        if (object instanceof NativeArray)
//        {
//            nativeArrayToJSONString((NativeArray) object, json);
//        } else if (object instanceof NativeObject)
//        {
//            nativeObjectToJSONString((NativeObject) object, json);
//        } else
//        {
//            // TODO what else should this support?
//            throw new JSONException(
//                    "Only native objects and arrays are currently supported by the toJSONString method.");
//        }
//
//        return json.toString();
//    }

    /**
     * Build a JSON string for a native object
     * 
     * @param nativeObject
     * @param json
     * @throws JSONException
     */
//    private static void nativeObjectToJSONString(NativeObject nativeObject,
//            JSONStringer json) throws JSONException
//    {
//        json.object();
//
//        Object[] ids = nativeObject.getIds();
//        for (Object id : ids)
//        {
//            String key = id.toString();
//            json.key(key);
//
//            Object value = nativeObject.get(key, nativeObject);
//            valueToJSONString(value, json);
//        }
//
//        json.endObject();
//    }

//    /**
//     * Build JSON string for a native array
//     * 
//     * @param nativeArray
//     * @param json
//     */
//    private static void nativeArrayToJSONString(NativeArray nativeArray,
//            JSONStringer json) throws JSONException
//    {
//        Object[] propIds = nativeArray.getIds();
//        if (isArray(propIds) == true)
//        {
//            json.array();
//
//            for (int i = 0; i < propIds.length; i++)
//            {
//                Object propId = propIds[i];
//                if (propId instanceof Integer)
//                {
//                    Object value = nativeArray.get((Integer) propId,
//                            nativeArray);
//                    valueToJSONString(value, json);
//                }
//            }
//
//            json.endArray();
//        } else
//        {
//            json.object();
//
//            for (Object propId : propIds)
//            {
//                Object value = nativeArray.get(propId.toString(), nativeArray);
//                json.key(propId.toString());
//                valueToJSONString(value, json);
//            }
//
//            json.endObject();
//        }
//    }

    /**
     * Look at the id's of a native array and try to determine whether it's
     * actually an Array or a HashMap
     * 
     * @param ids
     *            id's of the native array
     * @return boolean true if it's an array, false otherwise (ie it's a map)
     */
    private static boolean isArray(Object[] ids)
    {
        boolean result = true;
        for (Object id : ids)
        {
            if (id instanceof Integer == false)
            {
                result = false;
                break;
            }
        }
        return result;
    }

//    /**
//     * Convert value to JSON string
//     * 
//     * @param value
//     * @param json
//     * @throws JSONException
//     */
//    private static void valueToJSONString(Object value, JSONStringer json)
//            throws JSONException
//    {
//        if (value instanceof IdScriptableObject
//                && ((IdScriptableObject) value).getClassName().equals("Date") == true)
//        {
//            // Get the UTC values of the date
//            Object year = NativeObject.callMethod((IdScriptableObject) value,
//                    "getUTCFullYear", null);
//            Object month = NativeObject.callMethod((IdScriptableObject) value,
//                    "getUTCMonth", null);
//            Object date = NativeObject.callMethod((IdScriptableObject) value,
//                    "getUTCDate", null);
//            Object hours = NativeObject.callMethod((IdScriptableObject) value,
//                    "getUTCHours", null);
//            Object minutes = NativeObject.callMethod(
//                    (IdScriptableObject) value, "getUTCMinutes", null);
//            Object seconds = NativeObject.callMethod(
//                    (IdScriptableObject) value, "getUTCSeconds", null);
//            Object milliSeconds = NativeObject.callMethod(
//                    (IdScriptableObject) value, "getUTCMilliseconds", null);
//
//            // Build the JSON object to represent the UTC date
//            json.object().key("zone").value("UTC").key("year").value(year)
//                    .key("month").value(month).key("date").value(date)
//                    .key("hours").value(hours).key("minutes").value(minutes)
//                    .key("seconds").value(seconds).key("milliseconds")
//                    .value(milliSeconds).endObject();
//
//        } else if (value instanceof NativeJavaObject)
//        {
//            Object javaValue = Context.jsToJava(value, Object.class);
//            json.value(javaValue);
//        } else if (value instanceof NativeArray)
//        {
//            // Output the native object
//            nativeArrayToJSONString((NativeArray) value, json);
//        } else if (value instanceof NativeObject)
//        {
//            // Output the native array
//            nativeObjectToJSONString((NativeObject) value, json);
//        } else
//        {
//            json.value(value);
//        }
//    }
//    
    /**
     * 
     * @Title: quote 
     * @Description: 转义字符
     * @param string 待转义的字符
     * @return  
     *        String    返回类型 
     *
     * @throws
     */
    public static String quote( String string ) {
        if( string == null || string.length() == 0 ) {
           return "";
        }

        char b;
        char c = 0;
        int i;
        int len = string.length();
        StringBuffer sb = new StringBuffer( len * 2 );
        String t;
        char[] chars = string.toCharArray();
        char[] buffer = new char[1030];
        int bufferIndex = 0;
        for( i = 0; i < len; i += 1 ) {
           if( bufferIndex > 1024 ) {
              sb.append( buffer, 0, bufferIndex );
              bufferIndex = 0;
           }
           b = c;
           c = chars[i];
           switch( c ) {
              case '\\':
              case '"':
                 buffer[bufferIndex++] = '\\';
                 buffer[bufferIndex++] = c;
                 break;
              case '/':
                 if( b == '<' ) {
                    buffer[bufferIndex++] = '\\';
                 }
                 buffer[bufferIndex++] = c;
                 break;
              default:
                 if( c < ' ' ) {
                    switch( c ) {
                       case '\b':
                          buffer[bufferIndex++] = '\\';
                          buffer[bufferIndex++] = 'b';
                          break;
                       case '\t':
                          buffer[bufferIndex++] = '\\';
                          buffer[bufferIndex++] = 't';
                          break;
                       case '\n':
                          buffer[bufferIndex++] = '\\';
                          buffer[bufferIndex++] = 'n';
                          break;
                       case '\f':
                          buffer[bufferIndex++] = '\\';
                          buffer[bufferIndex++] = 'f';
                          break;
                       case '\r':
                          buffer[bufferIndex++] = '\\';
                          buffer[bufferIndex++] = 'r';
                          break;
                       default:
                          t = "000" + Integer.toHexString( c );
                          int tLength = t.length();
                          buffer[bufferIndex++] = '\\';
                          buffer[bufferIndex++] = 'u';
                          buffer[bufferIndex++] = t.charAt( tLength - 4 );
                          buffer[bufferIndex++] = t.charAt( tLength - 3 );
                          buffer[bufferIndex++] = t.charAt( tLength - 2 );
                          buffer[bufferIndex++] = t.charAt( tLength - 1 );
                    }
                 } else {
                    buffer[bufferIndex++] = c;
                 }
           }
        }
        sb.append( buffer, 0, bufferIndex );
        return sb.toString();
     }
    /**
     * 将Json格式的字符串转换成Map对象
     * @param jsonString JSON数据格式字符串
     * @return map集合
     */
    public static Map<String, Object> json2Map(String jsonString) {
           return (Map<String, Object>) json2pojo(jsonString,Map.class);
       }
    /**
     * 将Json格式的字符串转换成指定的对象返回
     * @param jsonStr 要转化的Json格式的字符串
     * @param javaBean 指定转化对象类型
     * @return 转化后的对象
     */
    public static Object json2pojo(String jsonStr,Class javaBean){
     JSONObject jsonObj = JSONObject.fromObject(jsonStr);
     Object obj = JSONObject.toBean(jsonObj, javaBean);
     return obj;
    }
    /**
	 * 从json HASH表达式中获取一个map，改map支持嵌套功能
	 * 
	 * @param jsonString
	 * @return
	 */
	public static Map getMap4Json(String jsonString) {
		JSONObject jsonObject = JSONObject.fromObject(jsonString);
		Iterator keyIter = jsonObject.keys();
		String key;
		Object value;
		Map valueMap = new HashMap();

		while (keyIter.hasNext()) {
			key = (String) keyIter.next();
			value = jsonObject.get(key);
			valueMap.put(key, value);
		}

		return valueMap;
	}
	/**
	 * 从json数组中得到相应java数组
	 * 
	 * @param jsonString
	 * @return
	 */
	public static Object[] getObjectArray4Json(String jsonString) {
		JSONArray jsonArray = JSONArray.fromObject(jsonString);
		return jsonArray.toArray();

	}
    public static String JsonCharFilter(String sourceStr)
      {
          return quote(sourceStr);
      }
    
    public static void main(String args[]) {
        System.out.println(JsonCharFilter("测了\"提醒我"));
    }

}
