package com.qq.qqbuy.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 用来过滤掉HTML的特定标签
 * @author winsonwu
 * @Created 2012-2-21
 */
public class HTMLFilterService
{
    
    public static String handleHTMLContent(String content) {
        if (StringUtil.isEmpty(content)) {
            return content;
        }
        content = rmCssContent(content);
        String wapSpecialTips = checkNewLine(content);              
        wapSpecialTips = filterHtml(wapSpecialTips);
        wapSpecialTips = AntiXssHelper.htmlEncode(wapSpecialTips);  //防止XXS漏洞攻击
        wapSpecialTips = CoderUtil.decodeWML(wapSpecialTips); //还原特殊字符
        return recoverBR(wapSpecialTips);
    }
    
    private static String rmCssContent(String content) {
    	if (!StringUtil.isEmpty(content)) {
    		int start = content.indexOf("<style type=\"text/css\">");
    		String endStr = "</style>";
    		int end = content.indexOf(endStr);
    		if (start >=0 && start <content.length() &&
    			end >= 0 && end < content.length() &&
    			end > start) {
//    			System.out.println(content.substring(start, end+8));
//    			System.out.println(content.substring(0, start) + content.substring(end+8));
    			return content.substring(0, start) + content.substring(end+endStr.length());
    		}
    	}
    	return content;
    }
    
    /**
     * 将文字中的</li>, <br/>, <br />替换成换行符\r\n
     * @param text
     * @return
     */
    private static String checkNewLine(String text) {
        if(text == null) {
            return "";
        }       
        return text.replace("</li>", "\r\n").replaceAll("<br\\s?/>", "\r\n");
    }
    
    //替换html标签正则表达式
    private final static String regxpForHtml = "<([^>]*)>";
    
    /**
     * 剔除html标签
     * @param str
     * @return
     */
    private static String filterHtml(String str) {
        Pattern pattern = Pattern.compile(regxpForHtml);
        Matcher matcher = pattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        boolean result1 = matcher.find();
        while (result1) {
            matcher.appendReplacement(sb, "");
            result1 = matcher.find();
        }
        matcher.appendTail(sb);
        return sb.toString();
    }
    
    /**
     * 将\r\n替换为html的换行
     * 合并多个连续换行为单个换行
     * @param text
     * @return
     */
    private static String recoverBR(String text) {
        if(text == null) {
            return "";
        }       
        String a = text.replace("&nbsp;","").replace("\r\n", "<br/>").replace("\n", "<br/>").replace("\r", "<br/>").replaceAll("<br\\/>(\\s*<br\\/>)*", "<br/>").replaceFirst("\\s*<br\\/>\\s*","<br/>");
        
        a = removeFirstLastBR(a.trim());
        
        //判断是否为空串
        String b = a.replaceAll("<br\\/>", "").replaceAll("\\\\r", "").replaceAll("\\\\n", "").replaceAll("\\s*", "");
        if (StringUtil.isEmpty(b.trim())) {
        	return "";
        } else {
        	return a;
        }
    }
    
    private static String removeFirstLastBR(String text) {
    	if (!StringUtil.isEmpty(text)) {
    		String tmp = text;
    		if (tmp.startsWith("<br/>")) {
    			tmp = tmp.substring(5);
    		}
    		if (tmp.endsWith("<br/>")) {
    			tmp = tmp.substring(0, tmp.length()-5);
    		}
    		return tmp;
    	}
    	return "";
    }
    
}

