/**   
 * @Title: HttpParamUtil.java 
 * @Package com.qq.qqbuy.common.util 
 * @Description: http参数解析
 * @author Wendyhu wendyhu@tencent.com  
 * @date 2012-2-15 上午11:22:48 
 * @version V1.0   
 */
package com.qq.qqbuy.common.util;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.StringTokenizer;

/** 
 * @ClassName: HttpParamUtil 
 * @Description: http参数解析
 * @author wendyhu wendyhu@tencent.com
 * @date 2012-2-15 上午11:22:48 
 *  
 */
public class HttpParamUtil
{

    /**
     * 
     * @Title: parseQueryString 
     * @Description: 解析http 参数 
     * @param s
     * @return  
     *        Hashtable<String,String[]>    返回类型 
     *
     * @throws
     */
    public static Hashtable<String,String[]> parseQueryString(String s)
    {
        String valArray[] = null;
        if(s == null)
            throw new IllegalArgumentException();
        Hashtable<String,String[]> ht = new Hashtable<String,String[]>();
        StringBuffer sb = new StringBuffer();
        String key;
        for(StringTokenizer st = new StringTokenizer(s, "&"); st.hasMoreTokens(); ht.put(key, valArray))
        {
            String pair = st.nextToken();
            int pos = pair.indexOf('=');
            if(pos == -1)
                throw new IllegalArgumentException();
            key = parseName(pair.substring(0, pos), sb);
            String val = parseName(pair.substring(pos + 1, pair.length()), sb);
            if(ht.containsKey(key))
            {
                String oldVals[] = (String[])(String[])ht.get(key);
                valArray = new String[oldVals.length + 1];
                for(int i = 0; i < oldVals.length; i++)
                    valArray[i] = oldVals[i];

                valArray[oldVals.length] = val;
            } else
            {
                valArray = new String[1];
                valArray[0] = val;
            }
        }

        return ht;
    }
    
    private static String parseName(String s, StringBuffer sb)
    {
        sb.setLength(0);
        for(int i = 0; i < s.length(); i++)
        {
            char c = s.charAt(i);
            switch(c)
            {
            case 43: // '+'
                sb.append(' ');
                break;

            case 37: // '%'
                try
                {
                    sb.append((char)Integer.parseInt(s.substring(i + 1, i + 3), 16));
                    i += 2;
                    break;
                }
                catch(NumberFormatException numberformatexception)
                {
                    throw new IllegalArgumentException();
                }
                catch(StringIndexOutOfBoundsException stringindexoutofboundsexception)
                {
                    String rest = s.substring(i);
                    sb.append(rest);
                    if(rest.length() == 2)
                        i++;
                }
                break;

            default:
                sb.append(c);
                break;
            }
        }

        return sb.toString();
    }
    
    public static void main(String[] args)
    {
        String str = "uin=5354&code=ver=23434&uin=afdsf";
        Hashtable<String,String[]> ht = parseQueryString(str);
        Iterator<String> it = ht.keySet().iterator();
        for(;it.hasNext();)
        {
            String key = it.next();
            String [] values = ht.get(key);
            
            for(int i = 0;i<values.length;i++)
                System.out.println(key + ":" + values[i]);
        }
    }
    
}
