package com.qq.qqbuy.common.util;

import java.io.IOException;
import java.io.StringReader;

import org.apache.logging.log4j.LogManager;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.QQBuyLogin;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.util.MD5Coding;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.common.util.StringUtil;

/**
 * 
 * @ClassName: TenpayLogin
 * 
 * @Description: 财付通登陆
 * 
 * @author wendyhu
 * @date 2012-12-4 下午06:24:58
 */
public class TenpayLogin
{
    private static final String formatStr = "bargainor_id=%s&request_token=%s&key=%s";

    private String hostName = "cl.tenpay.com";
    /**
     * 财付通的验证登陆的url
     */
    private String gammaTenpayCheckLoginUrl = "http://112.90.142.191/cgi-bin/clappv1.0/cl_verify_login_token.cgi?bargainor_id=%s&request_token=%s&sign=%s";
    private String tenpayCheckLoginUrl = "http://cl.tenpay.com/cgi-bin/clappv1.0/cl_verify_login_token.cgi?bargainor_id=%s&request_token=%s&sign=%s";

    /**
     * 财付通host
     */
    /**
     * 网购侧的商户号
     */
    private String bargainorId = "1211646001";

    /**
     * 网购商户号的加密key
     */
    private String key = "6658d94a9e9301a51c55295a268d4383";

    /**
     * 返回值 0：成功 其它失败
     */
    private int retCode = 0;

    /**
     * 处理结果描述
     */
    private String retMsg = "";

    /**
     * 仅处理成功时有 用户财付通账号，根据和财付通合作协议决定返回明文还是加密串
     */
    private String userId = "";

    /**
     * 仅处理成功时有 用户财付通账号名
     */
    private String userName = "";

    public String getHostName()
    {
        return hostName;
    }

    public void setHostName(String hostName)
    {
        this.hostName = hostName;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getTenpayCheckLoginUrl()
    {
        return tenpayCheckLoginUrl;
    }

    public void setTenpayCheckLoginUrl(String tenpayCheckLoginUrl)
    {
        this.tenpayCheckLoginUrl = tenpayCheckLoginUrl;
    }

    public String getBargainorId()
    {
        return bargainorId;
    }

    public void setBargainorId(String bargainorId)
    {
        this.bargainorId = bargainorId;
    }

    public int getRetCode()
    {
        return retCode;
    }

    public void setRetCode(int retCode)
    {
        this.retCode = retCode;
    }

    public String getRetMsg()
    {
        return retMsg;
    }

    public void setRetMsg(String retMsg)
    {
        this.retMsg = retMsg;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    /*
     * 解析tokenid
     * 
     * @param xmlContent
     * 
     * @return
     */
    private boolean parseContent(String xmlContent)
    {
        boolean ret = false;
        if (xmlContent == null || xmlContent.trim().length() < 10)
        {
            return false;
        }
        SAXBuilder builder = new SAXBuilder();
        Document doc = null;
        Element root = null;
        try
        {
            doc = builder.build(new StringReader(xmlContent));
        } catch (JDOMException e)
        {
            return false;
        } catch (IOException e)
        {
            return false;
        }

        // 如果成功则有这个字段
        if (doc != null && (root = doc.getRootElement()) != null)
        {
            retCode = StringUtil.toInt(root.getChildTextTrim("retcode"), -100);
            retMsg = root.getChildTextTrim("retmsg");
            userId = root.getChildTextTrim("user_id");
            userName = root.getChildTextTrim("name");
            if (retCode == 0)
                ret = true;
        } else
        {
            ret = false;
        }
        return ret;
    }

    /**
     * 
     * @Title: checkLogin
     * 
     * @Description: 去财付通检查这个登陆态是否ok
     * 
     * @param requestToken
     *            财付通返回的登陆态
     * 
     * @return boolean 返回类型
     * @throws
     */
    public boolean checkLogin(String requestToken)
    {
        boolean ret = false;
        String reqParam = String.format(formatStr, bargainorId, requestToken,
                key);
        String sign = MD5Coding.encode2HexStr(reqParam.getBytes());
        String url = "";
        if (EnvManager.isGamma())
        {
            url = String.format(gammaTenpayCheckLoginUrl, bargainorId, requestToken,
                    sign);
        } else
        {

            url = String.format(tenpayCheckLoginUrl, bargainorId, requestToken,
                    sign);
        }
        String resultXml = HttpUtil.get(url, hostName, 1000, 1000, "utf-8",
                true, false, null);
        if (resultXml != null)
        {
            ret = parseContent(resultXml);
        }
        return ret;
    }

    public static void main(String[] args) throws IOException
    {
        // TenpayLogin login = new TenpayLogin();
        // long startTime = System.currentTimeMillis();
        // boolean ret =
        // login.checkLogin("A8E7F7CAFBF193471972AD4AFE107F122CAB0CEDABFDFC09");
        // long endTime = System.currentTimeMillis();
        // System.out.println(endTime - startTime);
        //        
        //        
        // System.out.println(login.getRetCode() + " retMsg:" +
        // login.getRetMsg() + " uid:" + login.getUserId() + " name:" +
        // login.getUserName());
        String xx = "&#61&#61&#61";
        System.out.println(xx.replace("&#61", "="));
    }
}
