package com.qq.qqbuy.common.util;

/**
 * 
 * @ClassName: WeiXingLoginKey 
 * @Description: 微信的登陆key
 * @author wendyhu 
 * @date 2012-11-6 下午07:14:02
 */
public class WeiXinLoginKey
{
    
    /**
     * 用户qq号码
     */
    private long uin = 0;
    
    /**
     * wlogin登陆后返回的skey
     */
    private String skey = "";
    
    /**
     * 时间 
     */
    private long time = 0;

    public long getUin()
    {
        return uin;
    }

    public void setUin(long uin)
    {
        this.uin = uin;
    }

    public String getSkey()
    {
        return skey;
    }

    public void setSkey(String skey)
    {
        this.skey = skey;
    }

    public long getTime()
    {
        return time;
    }

    public void setTime(long time)
    {
        this.time = time;
    }
    
    
    public WeiXinLoginKey()
    {
        this(0, "", 0);
    }
    
    /**
     * 
     * <p>Title: </p> 
     * 
     * <p>微信登陆key </p>
     *  
     * @param uin 用户qq号码
     * @param skey
     * @param time
     */
    public WeiXinLoginKey(long uin, String skey, long time)
    {
        this.uin = uin;
        this.skey = skey;
        this.time = time;
    }

    @Override
    public String toString()
    {
        return " WeiXinLoginKey [skey=" + skey + ", time=" + time + ", uin="
                + uin + "]";
    }
}
