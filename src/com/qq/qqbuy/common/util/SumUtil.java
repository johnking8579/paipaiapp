package com.qq.qqbuy.common.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * 处理金额
 * @author homerwu
 *
 */
public class SumUtil
{
    public static String getFormattedCash(long oldFormat) {
        try {
            BigDecimal cash = new BigDecimal(oldFormat);
            DecimalFormat df = new DecimalFormat("0.00");
            return df.format(cash.divide(new BigDecimal(100), 2,
                    BigDecimal.ROUND_HALF_UP).doubleValue());
        } catch (Exception e) {
            return "";
        }
    }
    
    public static double getFormattedCash2(long oldFormat) {
        try {
            BigDecimal cash = new BigDecimal(oldFormat);
            return cash.divide(new BigDecimal(100), 2,
                    BigDecimal.ROUND_HALF_UP).doubleValue();
        } catch (Exception e) {
            return 0;
        }
    }

}
