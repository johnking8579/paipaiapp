package com.qq.qqbuy.common.util.url;


/**
 * Copyright (C) 2005-2011 TENCENT Inc.All Rights Reserved.		
 * 																	
 * FileName：StringParam.java					
 *			
 * Description：简要描述本文件的内容							 												
 * History：
 * 版本号    作者           日期          简要介绍相关操作
 *  1.0   wendyhu        2011-09-29           Create	
 */


/** 
 * 描述
 * @author wendyhu（最新修改者）
 * @version 1.0（新版本号）
 */
public class UrlCodeUtil {
	
	 /**	 
	 * 从加密串中取出字符串类型。
	 * @param str 源串。
	 * @param name 需要取出的参数名称。
	 * @return String 返回取出的值。	 
	 */  
    public static String getString(String str, String name)
    {
        int start = str.indexOf(name + "=");
        if (start == -1)
        {
            return null;
        }

        int end = str.indexOf('&', start);
        if (end == -1)
        {
            end = str.length();
        }

        String value = str.substring(start + name.length() + 1, end);
        String tmp = decode(value);  
        return tmp;
    }
    
	/**
	 * 验证字符串是否为整数,大于0
	 * */
	public static boolean isLong(String str){
		try{
			long ret=Long.parseLong(str);
			if(ret>0){
				return true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return false;
	}
	
	 /**	 
	 * 从加密串中取出字符串类型。
	 * @param str 源串。
	 * @param name 需要取出的参数名称。
	 * @return String 返回取出的值。	 
	 */  
    public static String getString(String str, String name,String ext)
    {
        int start = str.indexOf(name + "=");
        if (start == -1)
        {
            return null;
        }

        int end = str.indexOf(ext, start);
        if (end == -1)
        {
            end = str.length();
        }

        String value = str.substring(start + name.length() + 1, end);
        String tmp = decode(value);  
        return tmp;
    }    
    

	 /**	 
   	 * @param String
   	 * @param String
	 * @return int
   	 */
    public static int getInt(String str, String name)
    {
        int start = str.indexOf(name + "=");
        if (start == -1)
        {
            return -1;
        }

        int end = str.indexOf('&', start);
        if (end == -1)
        {
            end = str.length();
        }
        String value = str.substring(start + name.length() + 1, end);
        
        if(isLong(value)){
        	return Integer.parseInt(value);
        }else{
        	return 0;	
        }        
    }
    public static long getLong(String str, String name)
    {
        int start = str.indexOf(name + "=");
        if (start == -1)
        {
            return 0;
        }

        int end = str.indexOf('&', start);
        if (end == -1)
        {
            end = str.length();
        }
        String value = str.substring(start + name.length() + 1, end);
        if(isLong(value)){
        	return Long.parseLong(value);
        }else{
        	return 0;	
        }  
    }
	 /**	 
   	 * @param str 源串
   	 * @param name 字段名
   	 * @param ext 分隔符
	 * @return int
   	 */
    public static int getInt(String str, String name,String ext)
    {
        int start = str.indexOf(name + "=");
        if (start == -1)
        {
            return -1;
        }

        int end = str.indexOf(ext, start);
        if (end == -1)
        {
            end = str.length();
        }
        String value = str.substring(start + name.length() + 1, end);
        if(isLong(value)){
        	return Integer.parseInt(value);
        }else{
        	return 0;	
        }   
    }
    
    public static long getLong(String str, String name,String ext)
    {
        int start = str.indexOf(name + "=");
        if (start == -1)
        {
            return -1;
        }

        int end = str.indexOf(ext, start);
        if (end == -1)
        {
            end = str.length();
        }
        String value = str.substring(start + name.length() + 1, end);
        if(isLong(value)){
        	return Long.parseLong(value);
        }else{
        	return 0;	
        }  
    }
    
    /**
     * 将字符串转义
     * 
     * @param str
     * @return
     */
    public static String encode(String str)
    {
        StringBuffer sb = new StringBuffer();

        char[] cs = str.toCharArray();
        int count = cs.length;
        for (int i = 0; i < count; i++)
        {
            if (cs[i] == '%')
            {
                sb.append("%25");
            }
            else if (cs[i] == '&')
            {
                sb.append("%26");
            }
            else if (cs[i] == '=')
            {
                sb.append("%3d");
            }
            else if (cs[i] == '\r')
            {
                sb.append("%0d");
            }
            else if (cs[i] == '\n')
            {
                sb.append("%0a");
            }
            else if (cs[i] == ' ')
            {
                sb.append("%20");
            }
            else
            {
                sb.append(cs[i]);
            }
        }

        return sb.toString();
    }

    /**
     * 反转义
     * 
     * @param str
     * @return
     */
    public static String decode(String str)
    {
        StringBuffer sb = new StringBuffer();

        char[] cs = str.toCharArray();
        int count = cs.length;
        for (int i = 0; i < count;)
        {
            if (cs[i] == '%')
            {
                if (cs[i + 1] == '2' && cs[i + 2] == '5')
                    sb.append("%");
                else if (cs[i + 1] == '2' && cs[i + 2] == '6')
                    sb.append("&");
                else if (cs[i + 1] == '3'
                        && (cs[i + 2] == 'd' || cs[i + 2] == 'D'))
                    sb.append("=");
                else if (cs[i + 1] == '0'
                        && (cs[i + 2] == 'd' || cs[i + 2] == 'D'))
                    sb.append("\r");
                else if (cs[i + 1] == '0'
                        && (cs[i + 2] == 'a' || cs[i + 2] == 'A'))
                    sb.append("\n");
                i = i + 3;
            }
            else
            {
                sb.append(cs[i]);
                i = i + 1;
            }
        }

        return sb.toString();
    }
    
    public static long getLong(String str, long defaultValue)
    {
        try{
            defaultValue = Long.parseLong(str);
        }catch(Exception e)
        {
        }
        return defaultValue;
    }
    
}

