package com.qq.qqbuy.common.util.url;

public class URLUtil
{
    public static String getParamStr(Object... objs) {
        if (objs == null || objs.length == 0) {
            return "";
        }
        
        StringBuffer sbf = new StringBuffer();
        
        for (int i = 0; i < objs.length; i += 2)
        {
        	if (objs[i] == null || objs[i+1] == null) {
        		continue;   //winson: 可选参数可能为null
        	}
        	if (i > 0) {
        		sbf.append("&");
        	}
            sbf.append(objs[i].toString());
            sbf.append("=");
            if (i + 1 < objs.length)
            {
                if (objs[i + 1] == null)
                {
                    sbf.append("");
                } else
                {
                    sbf.append(checkUrlValue(objs[i + 1].toString()));
                }
            }
        }
        return sbf.toString();
    }
    
    // winson 无线侧登录的backUrl中不能存在空格，需要讲空格转换成+ 
    private static String checkUrlValue(String value) {
    	if (value == null) {
    		return "";
    	}
    	return value.replaceAll(" ", "%2B");
    }
    
    // winson 无线侧登录后返回的url中参数空格被替换成了+，需使用该方法转义回来
    public static String decodeCheckUrlValue(String value) {
    	if (value == null) {
    		return "";
    	}
    	return value.replace("+", " ");
    }
    
}
