package com.qq.qqbuy.common.util.url;


import com.qq.qqbuy.common.util.Base64;



public class Base64Util {
	
	/**
	 * base64加密，并且url进行转义
	 * 
	 * @param src
	 * @return
	 */
	public static String encode(String src){
		return UrlCodeUtil.encode(Base64.encode(src.getBytes()));
	}
	
	/**
	 * base64解密，并对url进行转义
	 *  
	 * @param src
	 * @return
	 */
	public static String decode(String src){
			return new String(Base64.decode( UrlCodeUtil.decode(src)));
	}
	
	public static String checkCodeVerifyCodeParam(String param){	
		if( param == null || param.trim().isEmpty()){
			return null;
		}
		
		param = Base64Util.decode(param);
		System.out.println(param);
		return param;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String src = "uin=373193317&token=~!@#$%^&*()_+&machinekey=23432&clientIp=172.25.34.90&capType=0&picType=0&appId=1&time=13153215reserve=&sign=555555555555555555555555555555555";
		String encodeStr = encode(src); 
		String decodeStr = decode(encodeStr);
		System.out.println("src:"+ src);
		System.out.println("encodeStr:"+ encodeStr);
		System.out.println("decodeStr:"+ decodeStr);
		System.out.println("src == decodeStr :"+ (src.equals(decodeStr)));
		String encode2 = encodeStr;
		System.out.println("encode2:" + encode2);
		String checkOk = checkCodeVerifyCodeParam(encode2);
		System.out.println("encode2:" + encode2 + " checkOk:" + checkOk);
		
	}

}
