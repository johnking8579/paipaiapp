package com.qq.qqbuy.common.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class MD5Util
{
    private static final String MD5 = "MD5";

    public static String encrypt(byte[] value)
    {
        if (value == null)
            return "";

        MessageDigest md = null;
        String strDes = null;

        try
        {
            md = MessageDigest.getInstance(MD5);
            md.update(value);
            strDes = bytes2Hex(md.digest()); // to HexString
        } catch (NoSuchAlgorithmException e)
        {
            throw new RuntimeException(e);
        }
        return strDes;
    }

    public static String bytes2Hex(byte[] byteArray)
    {
        StringBuffer strBuf = new StringBuffer();
        String tmp = null;
        for (int i = 0; i < byteArray.length; i++)
        {
            tmp = Integer.toHexString(byteArray[i] & 0xFF);
            if (tmp.length() == 1)
            {
                strBuf.append("0");
            }
            strBuf.append(tmp);
        }
        return strBuf.toString();
    }

    public static String md5SignwithKey(Map<String, String> map, String charset)
    {
        return md5Sign(map, "key", "f4becb5fb2e007c5fb7238b619b8956b", charset);
    }

    public static String md5Sign(Map<String, String> map, String keyName, String keyValue,
            String charset)
    {
        if (map == null || keyValue == null || keyValue.length() == 0)
        {
            return null;
        }

        ArrayList<String> lst = new ArrayList<String>();
        String s = "";
        for (String k : map.keySet())
        {
            lst.add(k);
        }

        Collections.sort(lst, new Comparator<String>()
        {
            public int compare(String o1, String o2)
            {
                return o1.compareTo(o2);
            }
        });

        for (int i = 0; i < lst.size(); i++)
        {
            String k = lst.get(i);
            String v = map.get(k);
            s += k + "=" + v + "&";
        }
        s += keyName + "=" + keyValue;
        String sign = "";
        try
        {
            sign =  MD5Coding.encode2HexStr(s.getBytes(charset));//encrypt(s.getBytes(charset));
        } catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
        return sign;
    }

    public static void main(String[] args) throws UnsupportedEncodingException
    {
        HashMap<String, String> mp = new HashMap<String, String>();
        mp.put("ver", "2.0");
        mp.put("charset", "1");
        mp.put("pay_result", "0");
        mp.put("transaction_id", "1204527301201008209009346613");
        mp.put("sp_billno", "20100820151913777");
        mp.put("total_fee", "1");
        mp.put("fee_type", "1");
        mp.put("bargainor_id", "1204527301");
        mp.put("attach", new String("583873140~qq三国".getBytes("gb2312"),
                "gb2312"));
        // mp.put("sign",sign);
        mp.put("sign_sp_id", "1000000101");
        String msign = MD5Util.md5SignwithKey(mp, "utf8");

        System.out.println("msign:" + msign);
        String s = "[]583873140~qq三国";
        System.out.println("s:" + StringUtil.removeInvalidWML(s));
       
        // String s1 = new String(s.getBytes("gb2312"),"gb2312");
//        System.out.println(HexUtil.bytes2HexStr(s.getBytes("gb2312")));
//
//        System.out.println(new String(HexUtil.hexStr2Bytes(HexUtil
//                .bytes2HexStr(s.getBytes("gb2312"))), "gb2312"));
//        System.out.println(new String(HexUtil
//                .hexStr2Bytes("3538333837333134307E7171C8FDB9FA"), "gb2312"));
        // 18F0E558F90854029B4EE7238934AD3F

        /*
         * String s =
         * "attach=583873140~qq三国&bargainor_id=1204527301&charset=1&fee_type=1&pay_result=0&sign_sp_id=1000000101&sp_billno=201008201242294437&total_fee=1&transaction_id=1204527301201008209009332710&ver=2.0&key=f4becb5fb2e007c5fb7238b619b8956b"
         * ; String s1 = new String(s.getBytes("UTF-8"),"gb2312"); String sign =
         * MD5Coding.encode2HexStr(s1.getBytes());
         * System.out.println(s.getBytes(
         * "UTF-8").length+"|"+s1.getBytes("UTF-8").length+"|"+sign);
         */
    }
}
