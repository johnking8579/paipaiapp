package com.qq.qqbuy.common.util;

/**
 * uk item为移动电商请求参数里的uk字段解码后的元素
 * 
 * @author wendyhu
 *
 */
public class UKItem
{

    /**
     * 用户名
     */
    private String name = null;
    
    /**
     * 登陆后wlogin返回的lskey
     */
    private String lskey = null;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getLskey()
    {
        return lskey;
    }

    public void setLskey(String lskey)
    {
        this.lskey = lskey;
    }
    
    
}
