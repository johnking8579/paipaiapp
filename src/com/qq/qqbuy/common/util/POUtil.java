package com.qq.qqbuy.common.util;

import org.springframework.beans.BeanUtils;

public class POUtil {
    
    public static void copyProperties(Object src, Object des) {
        BeanUtils.copyProperties(src, des);
    }
    public static void copyProperties(Object src, Object des,String ignoreProperties[]) {
        BeanUtils.copyProperties(src, des,ignoreProperties);
    }
}
