package com.qq.qqbuy.common.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;

import com.qq.qqbuy.common.Log;

public class DateUtils
{

    public static final String[] weeks = new String[]
    { "星期天", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };

    public static final SimpleDateFormat FORMAT_YMD = new SimpleDateFormat(
            "yyyy-MM-dd");

    public static final SimpleDateFormat FORMAT_YMD_HMS = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss");

    public static final SimpleDateFormat FORMAT_YMD_HMS_CHINESE = new SimpleDateFormat(
            "yy年MM月dd日  HH:mm:ss");

    public static final SimpleDateFormat FORMAT_MD_CHINESE = new SimpleDateFormat(
            "M月d日 ");

    public static final SimpleDateFormat FORMAT_YYYYMMDD_HMS_CHINESE = new SimpleDateFormat(
            "yyyy年MM月dd日HH:mm:ss");

    public static final SimpleDateFormat FORMAT_MD = new SimpleDateFormat(
            "MM.dd");

    public static final SimpleDateFormat FORMAT_yMdHms = new SimpleDateFormat(
            "yyyyMMddHHmmss");

    public static final SimpleDateFormat FORMAT_YMD_HM = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm");
	public static final SimpleDateFormat FORMAT_YMD_WITHOUT_ = new SimpleDateFormat("yyyyMMdd");


    public static String formatDateToStringYMD(Date date) throws Exception
    {
        return FORMAT_YMD.format(date);
    }

    public static Date parseStringToDateYMD(String str) throws Exception
    {
        return FORMAT_YMD.parse(str);
    }

    public static String formatToStringYMD(long currentTimeMillis)
    {
            return FORMAT_YMD.format(currentTimeMillis);
    }

    public static String formatDateToString(Date date, SimpleDateFormat format)
            throws Exception
    {
        return format.format(date);
    }

    public static Date parseStringToDate(String str, SimpleDateFormat format)
            throws Exception
    {
        return format.parse(str);
    }
    
    public static long parseStringToLong(String str, SimpleDateFormat format)
    		throws Exception
    {
    	Date date = parseStringToDate(str, format);
    	return date.getTime();
    }

    public static Date parseStringToDateNoExp(String str,
            SimpleDateFormat format)
    {
        try
        {
            return parseStringToDate(str, format);
        } catch (Exception e)
        {
            Log.run
                    .warn("parse date with str=" + str + ", format=" + format,
                            e);
        }
        return null;
    }

    public static String format(Object value, String format)
    {
        SimpleDateFormat sdf;
        if (StringUtils.isBlank(format))
        {
            sdf = FORMAT_YMD_HMS;
        } else
        {
            sdf = new SimpleDateFormat(format);
        }
        if (value instanceof java.util.Date)
        {
            return sdf.format(value);
        } else if (value instanceof java.sql.Date)
        {
            java.sql.Date sqld = (java.sql.Date) value;
            java.util.Date date = new Date(sqld.getTime());
            return sdf.format(date);
        } else if (value instanceof java.sql.Timestamp)
        {
            java.sql.Timestamp time = (java.sql.Timestamp) value;
            java.util.Date date = new Date(time.getTime());
            return sdf.format(date);
        }
        return value.toString();
    }

    public static SimpleDateFormat getDateFormat(String value,
            String customizeType)
    {
        SimpleDateFormat sdf;
        if (StringUtils.isBlank(customizeType))
        {
            int length = value.length();
            if (length > 16)
            {
                sdf = FORMAT_YMD_HMS;
            } else if (length > 10)
            {
                sdf = FORMAT_YMD_HM;
            } else
            {
                sdf = FORMAT_YMD;
            }
        } else
        {
            sdf = new SimpleDateFormat(customizeType);
        }
        return sdf;
    }

    /**
     * long转成cst 时间
     * 
     * @param date
     * @return
     */
    public static String L2CST(long date)
    {
        DateFormat df = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss 'GMT'",
                Locale.US);
        return df.format(new Date(date));
    }

    public static String queryWeekChinese(Date d)
    {
        if (d != null)
        {
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            int index = c.get(Calendar.DAY_OF_WEEK) - 1;
            if (index >= 0 && index < weeks.length)
            {
                return weeks[index];
            }
        }
        return "";
    }

    /**
     * 获取当天剩余时间,单位ms
     * 
     * @return
     * @throws Exception
     */
    public static long getExpireTime() throws Exception
    {
        Calendar calendar = Calendar.getInstance();
        long start = calendar.getTimeInMillis();
        Date time = DateUtils.parseStringToDate(DateUtils.formatDateToString(
                new Date(), DateUtils.FORMAT_YMD)
                + " 23:59:59", DateUtils.FORMAT_YMD_HMS);
        calendar.setTime(time);
        long end = calendar.getTimeInMillis();
        return end - start;
    }
    /**
     * date to str
     * @param date
     * @return
     * @throws Exception
     */
    public static String formatDateToStr(Date date) throws Exception {
    	SimpleDateFormat FORMAT_yMdHms = new SimpleDateFormat("yyyyMMddHHmmss");
    	return FORMAT_yMdHms.format(date);
    }
    /**
     * str to date
     * @param dateStr
     * @return
     * @throws Exception
     */
    public static Date praseStrToDate(String dateStr) throws Exception {
    	SimpleDateFormat FORMAT_yMdHms = new SimpleDateFormat("yyyyMMddHHmmss");
    	return FORMAT_yMdHms.parse(dateStr);
    }
    
    public static String praseDateToStr(Date date) throws Exception {
    	SimpleDateFormat FORMAT_yMd = new SimpleDateFormat("yyyyMMdd");
    	return FORMAT_yMd.format(date) ;
    }
    /**
     * get now
     * @return
     * @throws Exception
     */
    public static String getNowDateStr() throws Exception {
    	return formatDateToStr(new Date()) ;
    }

    public static void main(String args[]) throws Exception
    {
        Date dt = DateUtils.parseStringToDateNoExp("2012-12-13 16:50:10",
                DateUtils.FORMAT_YMD_HMS);
        String uuString = DateUtils.formatDateToString(dt,
                DateUtils.FORMAT_yMdHms);

        System.out.println(dt.getTime());
        System.out.println(uuString);
    }

    public static boolean isAfter(String compareTime, String comparedTime)
    {
        boolean ret = false;
        try
        {
            Date firstTime = DateUtils.parseStringToDate(compareTime,
                    DateUtils.FORMAT_YMD_HMS);
            Date secondTime = DateUtils.parseStringToDate(comparedTime,
                    DateUtils.FORMAT_YMD_HMS);
            ret = firstTime.after(secondTime);
        } catch (Exception e)
        {
        }
        return ret;
    }
    public static Date parseStringToDateWITHOUT_ (String str) throws Exception{
		return FORMAT_YMD_WITHOUT_.parse(str);
	}

}
