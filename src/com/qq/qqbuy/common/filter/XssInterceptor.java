package com.qq.qqbuy.common.filter;

import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.qq.qqbuy.common.util.AntiXssHelper;

public class XssInterceptor implements Interceptor {

	private String[] whitelist = new String[]{};	//哪些url参数名不进行html转义

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		Map<String, Object> params = invocation.getInvocationContext().getParameters();
		for (Entry<String, Object> e : params.entrySet()) {
			if(Arrays.binarySearch(whitelist, e.getKey()) >= 0)	
				continue;
			if (e.getValue() instanceof String) {
				e.setValue(AntiXssHelper.htmlEncode((String)e.getValue()));
			} else if (e.getValue() instanceof String[]) {
				String[] val = (String[]) e.getValue();
				for(int i=0; i<val.length; i++)	{
					val[i] = AntiXssHelper.htmlEncode(val[i]);
				}
				e.setValue(val);
			}
		}
		return invocation.invoke();
	}
	
	public void setWhitelist(String whitelist) {
		if (whitelist != null) {
			this.whitelist = whitelist.split(",");
			Arrays.sort(this.whitelist);
		}
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init() {
		// TODO Auto-generated method stub

	}
}
