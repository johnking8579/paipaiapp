package com.qq.qqbuy.common.filter;

import java.util.Map.Entry;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.google.gson.JsonObject;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.qq.qqbuy.common.Monitor;
import com.qq.qqbuy.common.Tuple2;
import com.qq.qqbuy.common.client.log.RequestThreadLocal;
import com.qq.qqbuy.common.util.TokenUtil;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.login.po.AppToken;

/**
 * 记录REQUEST请求时长, 并向静态变量中存放requestId, 方便随时获取. 使用UMP上报接口时间
 * @author JingYing 2014-10-15
 *
 */
public class TimerAndTokenInterceptor implements Interceptor{
	
	static Logger log = LogManager.getLogger();
	static final String LOCALHOST_IP = "0:0:0:0:0:0:0:1";
	
	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		Object caller = Monitor.registerInfo(ServletActionContext.getRequest().getRequestURI());//上报方法执行时间
		long start = System.currentTimeMillis();
		long wid = this.parseWid(ServletActionContext.getRequest().getParameter("appToken"));
		String requestId = Util.randChar(8);
		RequestThreadLocal.set(requestId, wid);
		
		try {
			String result = invocation.invoke();
			log.debug(genLogStr(ServletActionContext.getRequest(), start));
			return result;
		} finally {
			Monitor.registerInfoEnd(caller); 
		}
	}
	
	/**
	 * 拼接日志串
	 * @param req
	 * @param startTime
	 * @return
	 */
	private String genLogStr(HttpServletRequest req, long startTime)	{
		StringBuffer fullUrl = req.getRequestURL();
		String query = Util.paramMapToString(req.getParameterMap(), "utf-8");	//取get和post参数,并把参数还原成utf-8编码
		if(query != null && !"".equals(query))	{
			fullUrl.append("?").append(query);
		}
		
		JsonObject queryJson = new JsonObject();
		for(Entry<String,String[]> e : req.getParameterMap().entrySet())	{
			if(!"appToken".equals(e.getKey()))	{								//不保存apptoken参数, 太长了
				queryJson.addProperty(e.getKey(), e.getValue()[0]);	
			}
		}
		
		StringBuffer buffer = new StringBuffer()
		.append(req.getRequestURI()).append("\t")	       								//url接口名称
		.append(RequestThreadLocal.getRequestId()).append("\t")							//请求id
		.append(System.currentTimeMillis() - startTime).append("\t")					//花费时间
		.append(getHttpHeadIp(req)).append("\t")										//请求IP
		.append(RequestThreadLocal.getWid()).append("\t")							//用户账号
		.append(req.getMethod()).append(" ")										//GET-POST
		.append(fullUrl.toString()).append("\t")										//完整url
		.append(queryJson.get("mk")).append("\t")										//mk
		.append(queryJson.get("mt")).append("\t")										//mt
		.append(queryJson.get("versionCode")).append("\t")								//app版本app_version
		.append(queryJson.get("osVersion")).append("\t")								//os_version操作系统版本
		.append(queryJson.toString())										//interface_params接口参数，采用json格式
		;
		return buffer.toString();
	}
	
	/**
	 * 获得nginx请求头的ip
	 * @param req
	 * @return
	 */
	private String getHttpHeadIp(HttpServletRequest req)	{
		try {
			String ip = req.getHeader("x-real-ip");	//gamma反向代理服务器既有x-forwarded-for, 又有x-real-ip, 应该取后者
			if(ip == null)	{
				ip = req.getRemoteAddr();
			}
			if(LOCALHOST_IP.equals(ip))	{
				ip = "127.0.0.1";
			}
			return ip;
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 解析appToken
	 * @param appToken
	 * @return
	 */
	private long parseWid(String appToken) {
		if (Util.isEmpty(appToken) || appToken.length() < 10) {
			return 0L ;
		}
		AppToken token = null;
		try {
			if (appToken.startsWith(TokenUtil.LOGIN_TYPE_WT)) {
				token = TokenUtil.strToWtToken(appToken);
			} else if (appToken.startsWith(TokenUtil.LOGIN_TYPE_JD)) {
				token = TokenUtil.strToJdToken(appToken);   
			} else if (appToken.startsWith(TokenUtil.LOGIN_TYPE_QQ)) {
				token = TokenUtil.strToQqToken(appToken);   
			} else if (appToken.startsWith(TokenUtil.LOGIN_TYPE_WX)) {
				token = TokenUtil.strToWxToken(appToken);   
			} else if (appToken.startsWith(TokenUtil.LOGIN_TYPE_PP)) {
				token = TokenUtil.strToAppToken(appToken);   
			} 
		} catch (Exception e) {
			return 0L;
		}   
		return token == null ? 0L : token.getWid();
    }
	
	@Override
	public void destroy() {
	}
	
	@Override
	public void init() {
	}
	
}
