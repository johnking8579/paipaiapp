package com.qq.qqbuy.common.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.qq.qqbuy.common.Log;

public class WebconsoleFilter implements Filter{
	
	private static final String URL = "struts/webconsole.html";

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		Log.run.debug(req.getRequestURI());
		
		if(req.getRequestURI().endsWith(URL))	{
			Log.run.debug(404);
			((HttpServletResponse)response).sendError(404);
		} else	{
			Log.run.debug("继续执行。。。。。。。。");
			chain.doFilter(request, response);
		}
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}
