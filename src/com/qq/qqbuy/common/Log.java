package com.qq.qqbuy.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * 通用的日志对象
 * 
 * @author homerwu
 */
public class Log {
    
	  /**
     * 运行日志
     */
    public static Logger run = LogManager.getLogger("run");

    /**
     * 关键log
     */
    public static Logger key = LogManager.getLogger("key");
    /**
     * 记录h5跳转app日志
     */
    public static Logger h5CallApp = LogManager.getLogger("h5CallApp");
    
    /**
     * 记录下单日志
     */
    public static Logger makeorder = LogManager.getLogger("makeorder");
    
    /**
     * 记录财付通和统一支付平台调用日志
     */
    public static Logger payMent = LogManager.getLogger("payMent");
    
    /**
     * 登录成功流水日志
     */
    public static Logger loginSuc = LogManager.getLogger("loginSuc");
    
    /**
     * 登录失败流水日志
     */
    public static Logger loginErr = LogManager.getLogger("loginErr");
    
}
