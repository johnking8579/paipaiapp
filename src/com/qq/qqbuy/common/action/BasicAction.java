package com.qq.qqbuy.common.action;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.qq.qqbuy.common.BasicResult;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.AntiXssHelper;
import com.qq.qqbuy.common.util.CoderUtil;

/**
 * 所有Action的基类
 */
@SuppressWarnings("serial")
public abstract class BasicAction extends ActionSupport {
	
	/**
	 * 用于捕获Action中的异常
	 */
	protected BusinessException be;

	/**
   	 * 执行结果: success
   	 */
   	public static final String RST_SUCCESS = "success";

	/**
	 * 执行结果: failure
	 */
	public static final String RST_FAILURE = "failure";
	
	public static final String RST_API_FAILURE = "api-failure";

	/**
	 * 用于diaptch,forward情况下的跳转url设定
	 */
	protected String dispatchUrl = "";

	/**
	 * 处理结果的公共部分
	 */
	protected BasicResult result = new BasicResult();

	/**
	 * 用于调用者对应答和请求进行匹配. 由调用者在请求中传递给服务端, 服务端原样在应答中返回
	 */
	protected String dtag = null;

	/**
	 * 请求参数：用于Json接口参数版本兼容处理
	 */
	public String version = "0";
	
    protected HttpServletRequest getRequest()	{
		return ServletActionContext.getRequest();
	}
	
	protected HttpServletResponse getResponse()	{
		return ServletActionContext.getResponse();
	}
	
	/**
	 * 使用servlet方式, 向response写入字符串
	 * @param obj
	 * @return
	 */
	protected String doPrint(Object obj) {
		HttpServletResponse resp = ServletActionContext.getResponse();
		resp.setContentType("text/html;charset=utf-8");
		PrintWriter pw;
		try {
			pw = resp.getWriter();
			pw.print(obj);// 不要使用println,防止客户端接收到/r/n
			pw.flush();
			pw.close();
		} catch (IOException e) {
			Log.run.error(e.getMessage(), e);
		}
		return null;
	}
	
	/**
	 * 获得basePath
	 * @return
	 */
	protected String getBasePath()	{
		StringBuilder sb = new StringBuilder()
			.append(getRequest().getScheme())
			.append("://")
			.append(getRequest().getServerName());
        if (getRequest().getServerPort() != 80) {
            sb.append(":" + getRequest().getServerPort());
        }
        sb.append(getRequest().getContextPath());
        return sb.toString();
	}
	
	/**
	 * 获得反向代理服务器得到的请求端IP
	 * @return
	 */
	protected String getHttpHeadIp()	{
		HttpServletRequest req = ServletActionContext.getRequest();
		String ip = req.getHeader("x-real-ip");	//反向代理服务器既有x-forwarded-for, 又有x-real-ip, 应该取后者
		if(ip == null)	{
			ip = req.getRemoteAddr();
		}
		if("0:0:0:0:0:0:0:1".equals(ip))	{
			ip = "127.0.0.1";
		}
		return ip;
	}
	
	protected byte[] readReqBody() throws IOException	{
		InputStream is = null;
		try {
			is = ServletActionContext.getRequest().getInputStream();
			BufferedInputStream bis = new BufferedInputStream(is);	
			ByteArrayOutputStream bos = new ByteArrayOutputStream();	
			byte[] buf = new byte[1024];
			int len = -1;
			while((len=bis.read(buf)) != -1)	{
				bos.write(buf, 0, len);
			}
			return bos.toByteArray();
		} finally	{
			if(is != null)
				try {
					is.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
		}
	}
	

	protected void setErrCodeAndMsg4BExp(BusinessException e) {
		if (e != null) {
			be = e;
			this.result.setErrCode(e.getErrCode());
			this.result.setMsg(e.getErrMsg());
			Log.run.warn("biz exception found====>errcode:" + e.getErrCode() + ",errmsg:" + e.getErrMsg()+",", e);
		}
	}
	
	protected void setErrCodeAndMsg4BExp(BusinessException e, String attachMsg) {
		if (e != null) {
			be = e;
			this.result.setErrCode(e.getErrCode());
			this.result.setMsg(e.getErrMsg() + "[" + attachMsg + "]");
			Log.run.warn("biz exception found====>errcode:" + e.getErrCode() + ",errmsg:" + e.getErrMsg()+"," + attachMsg, e);
		}
	}
	
	protected void setErrcodeAndMsg4Throwable(Throwable e, String attachMsg) {
		setErrCodeAndMsg(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP, "捕获到未知异常：" + e.getClass().getSimpleName() + "[" + attachMsg + "]");
		Log.run.error(e.getMessage(), e);//打印堆栈信息
	}

	protected void setErrCodeAndMsg(int errCode, String errMsg) {
		be = BusinessException.createInstance(errCode, errMsg);
		this.result.setErrCode(errCode);
		this.result.setMsg(errMsg);
	}


	public BasicResult getResult() {
		return result;
	}

	public String getDtag() {
		return dtag;
	}

	public void setDtag(String dtag) {
		this.dtag = CoderUtil.decodeWML(AntiXssHelper.xmlAttributeEncode(dtag));
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String ver) {
		this.version = ver;
	}

	public String getDispatchUrl() {
		return dispatchUrl;
	}

	public void setDispatchUrl(String dispatchUrl) {
		this.dispatchUrl = dispatchUrl;
	}
    
}
