package com.qq.qqbuy.common.action;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.BitSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;

import com.qq.qqbuy.common.BitSetJsonBeanProcessor;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.BizConstant;
import com.qq.qqbuy.common.util.AntiXssHelper;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.url.URLUtil;
import com.qq.qqbuy.thirdparty.idl.userinfo.UserInfoClient;

/* @FileName: ApiPaipaiBaseAction.java    
 * @Description: TODO 
 * @author: homerwu   
 * @date:2012-12-19 下午06:40:52    
 */
public class PaipaiApiBaseAction extends AuthRequiredAction{
	
	protected String dataStr;
	
	protected final int TYPE_ANDROID = 1;
	protected final int TYPE_WAP2 = 2;

    public static final String GO_TO_LOGIN = "gotoLogin";
    public static final String GO_TO_VERIFY_PIC = "gotoVerifyPic";

    protected String loginURL; // 登陆跳转URL
    protected String loginBackUrl;// 登陆跳转backUrl,报错业务方传过来的backUrl
    protected String logoutURL;// 退出跳转URL
    protected String verifyPicURL; // 验证码验证URL

    protected int isLogin = 0; // 是否已登陆标识，0、未登录，1、已登陆，用于页尾判断是否已登陆
    
	protected boolean needAlarm = true; //接口告警是否上报，默认为true

    protected String ptg = ""; // ptag，表示页面，用于统计
    protected String ptag = ""; // ptag，表示页面，用于统计
    private String icfa; // 内部渠道号
    private String o_icfa;// 旧内部渠道号
    private String gcfa; // 外部渠道号
    protected String pps1 = "0";// 外部pps参数
    protected String pps2 = "0";// 内部pps参数

    private String qqbuyURLWithoutSid;
    private String qqbuyURLWithSid;
    
    private String tptag; //用于模版页纬度的统计
    
    private boolean fromBaidu;//是否来自百度的流量
    
    protected String obj2Json(Object obj, JsonConfig config) {
        if (obj != null) {
            return JSONSerializer.toJSON(obj, config).toString();
        }
        return null;
    }
	
	protected String obj2Json(Object obj) {
	    JsonConfig config = new JsonConfig();
        String[] excludes = { "size", "empty" };
        config.setExcludes(excludes);
        config.registerJsonBeanProcessor(BitSet.class, new BitSetJsonBeanProcessor()); //对BitSet类型的json处理特殊化
        
		return obj2Json(obj,new JsonConfig());
	}
	
	
    /******************************* login logout相关 *********************************/
    /**
     * 获取退出URL
     * 
     * @return
     */
    public String getLogoutURL()
    {
        return AntiXssHelper.urlFilterAndEncode("http://pt.3g.qq.com/s?sid="
                + getSid() + "&amp;aid=loginC1&amp;bid_code=paipai&amp;go_url="
                + getQqbuyURLWithoutSid());
    }

    /**
     * 获取登陆URL 注意重写loginReturnUrl()方法
     * 
     * @return
     */
    public String getLoginURL()
    {
        String backUrl = this.getLoginBackUrl();
        if (!StringUtil.isEmpty(getPgid()))
        {
            backUrl += "&amp;pgid=" + getPgid();
        }
        this.loginURL = this.getLoginUrlByType(backUrl,
                BizConstant.ACESS_TYPE_WAP2);// 默认为wap2
        return this.loginURL;
    }

    public void setLoginURL(String loginURL)
    {
        this.loginURL = loginURL;
    }

    /**
     * QQ网购WAP站登陆态验证方法 SID验证成功后会调用接口拉取用户昵称
     * 
     * @param backUrl
     *            验证不通过回调URL
     */
    public String checkLogin(String backUrl)
    {
        this.setLoginBackUrl(backUrl);// 设置登录跳转backUrl
        if (!checkLogin())
        {
            setPageInfo();
            if (code == 300)
            { // code=300表示sid需要验证码验证，跳转到无线统一验证码验证页面
                setVerifyPicURL(getDefaultVerifyPicUrl(backUrl));
                return GO_TO_VERIFY_PIC;
            } else
            {// code == 201表示sid是游客sid，跳转到无线统一登陆页面
                return GO_TO_LOGIN;
            }
        }
        if (code == 201)
        {
            setPageInfo();
            return GO_TO_LOGIN;
        }
        isLogin = 1;
        setPageInfo();
        return null;
    }

    /**
     * 不需要必须要求登陆态的action调用，主要是获取nickName
     */
    public void getLoginInfo(String backUrl)
    {
        this.setLoginBackUrl(backUrl);// 设置登录跳转backUrl
        UserInfoClient userInfoClient = new UserInfoClient();
        // 1、检查是否已经登陆
        boolean ret = checkLogin();
        if (ret)
        {
            if (code != 201)
            {
                isLogin = 1;
            }
        } else
        {
            if (StringUtils.isEmpty(getSid()) || "00".equals(getSid())
                    || "null".equals(getSid()))
            { // 如果sid为空，则生成一个游客sid
                String clientIp = getCp();
                if (StringUtils.isEmpty(clientIp))
                {
                    clientIp = ServletActionContext.getRequest().getHeader(
                            "X-Real-IP");
                }
                setSid(userInfoClient.getTempSid(clientIp));
            }
        }

        setPageInfo();
    }

    /**
     * 获取action url
     * 
     * @param actionPath
     *            业务名 :/deal/confirmOrder.xhtml
     * @param sysType
     *            wap,touch,api
     * @param objs
     *            params key value
     * @return
     */
    protected String getActionPath(String actionPath, String sysType,
            Object... objs)
    {

        String basePath = getRequest().getScheme() + "://"
                + getRequest().getServerName();
        int port = getRequest().getServerPort();
        if (port != 80)
        {
            basePath += ":" + port;
        }
        basePath += getRequest().getContextPath();
        String path = basePath + actionPath + "?";
        String params = URLUtil.getParamStr(objs);

        String url = path + params;

        if (!StringUtil.isEmpty(sysType))
        {
            url += "&amp;t=" + sysType;
        }
        if (!StringUtil.isEmpty(this.gcfa))
        {
            url += "&amp;gcfa=" + this.gcfa;
        }
        if (!StringUtil.isEmpty(this.icfa))
        {
            url += "&amp;icfa=" + this.icfa;
        }
        if (!StringUtil.isEmpty(this.o_icfa))
        {
            url += "&amp;o_icfa=" + this.o_icfa;
        }
        if (!StringUtil.isEmpty(this.pps1))
        {
            url += "&amp;pps1=" + this.pps1;
        }
        if (!StringUtil.isEmpty(this.pps2))
        {
            url += "&amp;pps2=" + this.pps2;
        }
        if (!StringUtil.isEmpty(this.tptag))
        {
            url += "&amp;tptag=" + this.tptag;
        }
        return url;
    }
    /**
     * 获取 url，不带域名
     * 
     * @param path
     *            业务名 :/deal/confirmOrder.xhtml
     * @param sysType
     *            wap,touch,api
     * @param objs
     *            params key value
     * @return
     */
    protected String getURLWithOutgetContextPath(String path, String sysType,
            Object... objs)
    {

        String params = URLUtil.getParamStr(objs);

        String url = path +"?"+ params;

        if (!StringUtil.isEmpty(sysType))
        {
            url += "&amp;t=" + sysType;
        }
        if (!StringUtil.isEmpty(this.gcfa))
        {
            url += "&amp;gcfa=" + this.gcfa;
        }
        if (!StringUtil.isEmpty(this.icfa))
        {
            url += "&amp;icfa=" + this.icfa;
        }
        if (!StringUtil.isEmpty(this.o_icfa))
        {
            url += "&amp;o_icfa=" + this.o_icfa;
        }
        if (!StringUtil.isEmpty(this.pps1))
        {
            url += "&amp;pps1=" + this.pps1;
        }
        if (!StringUtil.isEmpty(this.pps2))
        {
            url += "&amp;pps2=" + this.pps2;
        }
        if (!StringUtil.isEmpty(this.tptag))
        {
            url += "&amp;tptag=" + this.tptag;
        }
        return url;
    }
    /******************************* 统计 *************************************/
    public void setPageInfo()
    {
        HttpServletRequest request = ServletActionContext.getRequest();
        int type = BizConstant.ACESS_TYPE_WAP2;
        if (request.getRequestURI().indexOf("/t/") != -1)
        {
            type = BizConstant.ACESS_TYPE_TOUCH;
        } else if (request.getRequestURI().indexOf("/pc/") != -1)
        {
            type = BizConstant.ACESS_TYPE_PC;
        }
        Log.run.debug("pageid:" + request.getParameter("pageid") + ":"
                + getPgid());
        String prePgid = type
                + "."
                + (StringUtils.isEmpty(getPgid()) ? request
                        .getParameter("pgid") : getPgid());
        if (StringUtils.isEmpty(this.getPgid()))
        {
            prePgid = type + ".";
        }
        //设置pps参数
		initPpsParams();
        request.setAttribute("prePgid", prePgid);
        request.setAttribute("qq", this.getQq());
        request.setAttribute("sid", this.getSid());
        request.setAttribute("pps1", this.getPps1());
        request.setAttribute("pps2", this.getPps2());
        request.setAttribute("tptag", this.getTptag());
        //模版页不存在时跳转到failer.jsp呈现需要
        request.setAttribute("isLogin", this.getIsLogin());
        request.setAttribute("nickNameStr", this.getNickNameStr());
        request.setAttribute("icfa", this.getIcfa());
        request.setAttribute("gcfa", this.getGcfa());
        request.setAttribute("o_icfa", this.getO_icfa());
      
    }

    public void setPageId(String pgid)
    {
        HttpServletRequest request = ServletActionContext.getRequest();
        request.setAttribute("pgid", pgid);
        setPgid(pgid);
    }
    
    public void setTempPageTag(String tptag)
    {
        HttpServletRequest request = ServletActionContext.getRequest();
        request.setAttribute("tptag", tptag);
        setTptag(tptag);
    }
    
    protected void initPpsParams()
    {
        // 设置PPS1的值：如果从广点通过来，则将pps1的值修改为“qz_gdt.XXX”或者"qz_express.XXX"形式
        String qz_express = this.getRequest().getParameter("qz_express");
        String qz_gdt = this.getRequest().getParameter("qz_gdt");
        String pps_gdt = qz_gdt != null ? qz_gdt
                : (qz_express != null ? qz_express : "0");
        if (qz_gdt != null)
        {
            // pps1 = "qz_gdt." + pps_gdt;
            setPps1("qz_gdt." + pps_gdt);
        } else if (qz_express != null)
        {
            // pps1 = "qz_express." + pps_gdt;
            setPps1("qz_express." + pps_gdt);
        }
        Log.run.debug("initPpsParams result: pps1=" + getPps1() + "|" + "pps2="
                + getPps2());
    }

    /******************************* 工具方法 *********************************/

    /**
     * 根据类型获取登陆访问路径
     * http://pt.3g.qq.com/s?aid=nLogin&bid_code=qqbuyLogin&loginTitle
     * =QQ%E7%BD%91%E8%B4%AD&go_url= loginTitle登陆系统名称，需encode
     * go_url登录成功后返回url地址，需encode，其次go_url必须是.qq.com的域名
     * 
     * @return
     */
    public String getLoginUrlByType(String backUrl, int type)
    {
        String loginUrl = "http://pt.3g.qq.com/s?aid=loginC1&amp;bid_code=paipai&amp;loginTitle=%E6%89%8B%E6%9C%BA%E6%8B%8D%E6%8B%8D&amp;g_ut=2";

        if (type == BizConstant.ACESS_TYPE_TOUCH)
        {
            loginUrl = "http://pt.3g.qq.com/s?aid=touchLogin&amp;bid_code=qqbuyLogin&amp;loginTitle=%E6%89%8B%E6%9C%BA%E6%8B%8D%E6%8B%8D&amp;g_ut=2";
        }

        try
        {
            if (StringUtils.isNotEmpty(backUrl))
            {
                loginUrl = loginUrl + "&amp;go_url="
                        + URLEncoder.encode(backUrl, "UTF-8");
            } else
            {
                return null;
            }
        } catch (Exception e)
        {
            Log.run.warn("getWapLoginUrl failed", e);
            return null;
        }
        return AntiXssHelper.urlFilterAndEncode(loginUrl);
    }

    /**
     * 获取默认验证码验证访问路径
     * http://pt.3g.qq.com/sl?flag=XXXXXXX&loginsid=XXXXXX&goUrl=XXXXXXXX
     * flag是bid的加密,hAiH2W6AQ5RuqdgSZhwp7NQkRUWos9o5s,注意不要修改 loginsid是要验证的sid
     * goUrl是验证后要跳转的地址
     * 
     * @return
     */
    public String getDefaultVerifyPicUrl(String backUrl)
    {
        String verifyPicUrl = "http://pt.3g.qq.com/sl?flag=hAiH2W6AQ5RuqdgSZhwp7NQkRUWos9o5&amp;loginsid="
                + getSid();
        try
        {
            if (StringUtils.isNotEmpty(backUrl))
            {
                verifyPicUrl = verifyPicUrl + "&amp;goUrl="
                        + URLEncoder.encode(backUrl, "UTF-8");
            } else
            {
                return null;
            }
        } catch (Exception e)
        {
            return null;
        }
        return AntiXssHelper.urlFilterAndEncode(verifyPicUrl);
    }

    /**
     * 定义QQ网购WAP站登录后回跳的页面 子Action如果有特殊需要则需要override本方法,如需要添加参数等
     * 
     * @return
     */
    protected String loginReturnUrl()
    {
        HttpServletRequest request = ServletActionContext.getRequest();
        return request.getScheme() + "://" + request.getServerName()
                + request.getContextPath() + request.getRequestURI();
    }

    protected String basePath()
    {
        String basePath;
        HttpServletRequest request = ServletActionContext.getRequest();
        int port = request.getServerPort();
        basePath = request.getScheme() + "://" + request.getServerName();
        if (port != 80)
        {
            basePath += ":" + port;
        }
        basePath += request.getContextPath();
        return basePath;
    }

    /**
     * 获取QQ网购WAP站访问路径，含sid 例如：http://m.buy.qq.com?sid=XXXX
     * 
     * @return
     */
    public String getQqbuyURLWithSid()
    {
        if (!StringUtil.isEmpty(qqbuyURLWithSid))
        {
            return qqbuyURLWithSid;
        } else
        {
            HttpServletRequest request = ServletActionContext.getRequest();
            qqbuyURLWithoutSid = request.getScheme() + "://"
                    + request.getServerName() + request.getContextPath()
                    + "?sid=" + getSid();
            return qqbuyURLWithSid;
        }
    }

    /**
     * 获取QQ网购WAP站访问路径，不含sid 例如：http://m.buy.qq.com
     * 
     * @return
     */
    public String getQqbuyURLWithoutSid()
    {
        if (!StringUtil.isEmpty(qqbuyURLWithoutSid))
        {
            return qqbuyURLWithoutSid;
        } else
        {
            HttpServletRequest request = ServletActionContext.getRequest();
            qqbuyURLWithoutSid = request.getScheme() + "://"
                    + request.getServerName() + request.getContextPath();
            return qqbuyURLWithoutSid;
        }
    }

    /**
     * redirect转发请求 如跳转到登陆页和验证码验证页
     * 
     * @param url
     * @throws IOException
     */
    public boolean redirect(String url)
    {
        HttpServletResponse response = ServletActionContext.getResponse();
        try
        {
            response.sendRedirect(url);
        } catch (IOException e)
        {
            Log.run.error("redirect to " + url + " error: " + e.getMessage());
            return false;
        }
        return true;
    }
    
    protected void setAttrToReq(String attrKey, Object attrValue) {
    	this.getRequest().setAttribute(attrKey, attrValue);
    }
    
    protected Object getAttrFromReq(String attrKey, Object defaultValue) {
    	return getRequest().getAttribute(attrKey);
    }

    /******************************* 用户昵称相关 *********************************/
    public String getNickNameStr()
    {
        String nickNameStr = nickName;
        if (StringUtil.isEmpty(nickNameStr))
        {
            nickNameStr = getQq() > 0 ? "" + getQq() : "";
        }
        try
        {
            nickNameStr = StringUtil.bSubstring(nickNameStr, 8);// 截取8个字节长度
            
        } catch (Exception e)
        {
            Log.run.warn("getNickNameStr error!", e);
        }
        return AntiXssHelper.htmlEncode(nickNameStr);
    }

    /******************************* setter getter *********************************/
    public void setLogoutURL(String logoutURL)
    {
        this.logoutURL = logoutURL;
    }

    public int getIsLogin()
    {
        return isLogin;
    }

    public void setIsLogin(int isLogin)
    {
        this.isLogin = isLogin;
    }

    public String getVerifyPicURL()
    {
        return this.verifyPicURL;
    }

    public void setVerifyPicURL(String verifyPicURL)
    {
        this.verifyPicURL = verifyPicURL;
    }

    public void setQqbuyURLWithoutSid(String qqbuyURLWithoutSid)
    {
        this.qqbuyURLWithoutSid = qqbuyURLWithoutSid;
    }

    public void setQqbuyURLWithSid(String qqbuyURLWithSid)
    {
        this.qqbuyURLWithSid = qqbuyURLWithSid;
    }

    public String getPtg()
    {
        return ptg;
    }

    public void setPtg(String ptg)
    {
        this.ptg = ptg;
    }

    public String getLoginBackUrl()
    {
        return loginBackUrl;
    }

    public void setLoginBackUrl(String loginBackUrl)
    {
        this.loginBackUrl = loginBackUrl;
    }

    public String getPps1()
    {
        return pps1;
    }

    public void setPps1(String pps1)
    {
    	pps1 = moveRepeatParaValue(pps1);
        this.pps1 = AntiXssHelper.htmlEncode(pps1);
    }

    public String getPps2()
    {
        return pps2;
    }

    public void setPps2(String pps2)
    {
    	pps2 = moveRepeatParaValue(pps2);
        this.pps2 = AntiXssHelper.htmlEncode(pps2);
    }

    public String getO_icfa()
    {
        if (StringUtils.isNotEmpty(this.icfa))
        {
            return this.icfa;
        }
        return this.o_icfa;
    }

    public String getOriginalO_icfa()
    {
        return this.o_icfa;
    }

    public void setO_icfa(String oIcfa)
    {
    	oIcfa = moveRepeatParaValue(oIcfa);
        this.o_icfa = AntiXssHelper.htmlAttributeEncode(oIcfa);
    }

    public String getIcfa()
    {
        return this.icfa;
    }

    public void setIcfa(String icfa)
    {
    	icfa = moveRepeatParaValue(icfa);
        this.icfa = AntiXssHelper.htmlAttributeEncode(icfa);
    }

    public String getGcfa()
    {
        return AntiXssHelper.htmlAttributeEncode(gcfa);
    }

    public void setGcfa(String gcfa)
    {
    	gcfa = moveRepeatParaValue(gcfa);
        this.gcfa = AntiXssHelper.htmlAttributeEncode(gcfa);
    }
    
    
    public String getPtag() {
        return ptag;
    }

    public void setPtag(String ptag) {
        this.ptag = ptag;
    }

    public static void main(String[] args)throws Exception {
		System.out.println(URLEncoder.encode("手机拍拍","utf-8"));
	}

	public boolean isNeedAlarm() {
		return needAlarm;
	}

	public void setNeedAlarm(boolean needAlarm) {
		this.needAlarm = needAlarm;
	}
	
	//去除重复携带的参数，strusts当作数据处理,如：&gcfa=19910011&_lp=1...&gcfa=19910011&o_icfa=&pps1=0&pps2=0传入值变成'19910011,19910011'
	private String moveRepeatParaValue(String paraValue)
	{
		if(paraValue==null || paraValue.trim().equals(""))
		{
			return paraValue;
		}
		String newValue = paraValue;
		String[] values = paraValue.split(",");
		if(values != null && values.length>=2)
		{
			newValue=values[0];
		}

		return newValue;
	}

	public String getTptag() 
	{
		return AntiXssHelper.htmlAttributeEncode(tptag);
	}

	public void setTptag(String tptag) 
	{
		tptag = moveRepeatParaValue(tptag);
        this.tptag = AntiXssHelper.htmlAttributeEncode(tptag);
	}

	public boolean isFromBaidu() {
		String refer=this.getRequest().getHeader("referer");
		return StringUtil.isNotEmpty(refer) && refer.trim().startsWith("http://m.baidu.com");
	}

	public void setFromBaidu(boolean fromBaidu) {
		this.fromBaidu = fromBaidu;
	}
	
	public String getDataStr() {
		return dataStr;
	}

	public void setDataStr(String dataStr) {
		this.dataStr = dataStr;
	}
}
