package com.qq.qqbuy.common;

import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class RegionUtil {

	public static final Map<String,Node> RegionNodeMap = new HashMap<String,Node>();
	static {
		try {
			// step 1: 获得dom解析器工厂（工作的作用是用于创建具体的解析器）  
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();          
	          
	        // step 2:获得具体的dom解析器  
	        DocumentBuilder db = dbf.newDocumentBuilder();            
	          
	        // step3: 解析一个xml文档，获得Document对象（根结点）          
	        Document document = db.parse(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("region.xml"));        
	          
	        NodeList provinceList = document.getElementsByTagName("province");            
	        for(int i = 0; i < provinceList.getLength(); i++)  
	        {  
	            Element provinceElement = (Element)provinceList.item(i); 
	            String provinceId = provinceElement.getAttribute("id") ;
	            String provinceName = provinceElement.getAttribute("name") ;
	            Node provinceNode = new Node(provinceId,provinceName,null,null,null,null) ;
	            RegionNodeMap.put(provinceId, provinceNode) ;
	            
	            NodeList city1List = provinceElement.getElementsByTagName("city1");              
	            for(int j = 0; j < city1List.getLength(); j++)  
	            {  
	                Element city1Element = (Element)city1List.item(j);  
	                String city1Id = city1Element.getAttribute("id") ;
	                String city1Name = city1Element.getAttribute("name") ;
	                Node city1Node = new Node(provinceId,provinceName,city1Id,city1Name,null,null) ;
	                RegionNodeMap.put(city1Id, city1Node) ;
	                
	                NodeList city2List = city1Element.getElementsByTagName("city2");  
	                for(int k = 0; k < city2List.getLength(); k++)  
	                {  
	                    Element city2Element = (Element)city2List.item(k);  
	                    String city2Id = city2Element.getAttribute("id") ;
	                    String city2Name = city2Element.getAttribute("name") ;
	                    Node city2Node = new Node(provinceId,provinceName,city1Id,city1Name,city2Id,city2Name) ;
	                    RegionNodeMap.put(city2Id, city2Node) ;
	                }
	            }
	        }
		} catch (Exception ex) {
			Log.run.warn("RegionUtil constants error", ex);
		}
        
	}
	
	
	public static class Node {
		public Node(String provinceId, String provinceName, String city1Id,
				String city1Name, String city2Id, String city2Name) {
			this.provinceId = provinceId ;
			this.provinceName = provinceName ;
			this.city1Id = city1Id ;
			this.city1Name = city1Name ;
			this.city2Id = city2Id ;
			this.city2Name = city2Name ;
		}
		public String toString() {
			return this.provinceId + "-" + this.provinceName + "-" + 
					 this.city1Id + "-" + this.city1Name +  "-" + 
					 this.city2Id + "-" + this.city2Name  ; 
					
		}
		String provinceName ;
		String provinceId ;
		String city1Name ;
		String city1Id;
		String city2Name;
		String city2Id ;
		public String getProvinceName() {
			return provinceName;
		}
		public void setProvinceName(String provinceName) {
			this.provinceName = provinceName;
		}
		public String getProvinceId() {
			return provinceId;
		}
		public void setProvinceId(String provinceId) {
			this.provinceId = provinceId;
		}
		public String getCity1Name() {
			return city1Name;
		}
		public void setCity1Name(String city1Name) {
			this.city1Name = city1Name;
		}
		public String getCity1Id() {
			return city1Id;
		}
		public void setCity1Id(String city1Id) {
			this.city1Id = city1Id;
		}
		public String getCity2Name() {
			return city2Name;
		}
		public void setCity2Name(String city2Name) {
			this.city2Name = city2Name;
		}
		public String getCity2Id() {
			return city2Id;
		}
		public void setCity2Id(String city2Id) {
			this.city2Id = city2Id;
		}    		
	}
}
