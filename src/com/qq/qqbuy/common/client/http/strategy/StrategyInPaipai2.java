package com.qq.qqbuy.common.client.http.strategy;

import java.net.Proxy;
import java.util.HashSet;
import java.util.Set;

import com.qq.qqbuy.common.PaiPaiConfig;
import com.qq.qqbuy.common.client.http.ConnStrategy;
import com.qq.qqbuy.common.util.Util;

/**
 * 腾讯机房策略 - 旧版策略,以内网nginx路由为主
 * 如果url为*.paipai.com(除了www.paipai.com等), 则修改url为ip地址,加上host头,不使用代理
 * 如果url为内网ip,不使用代理, 如果url为外网ip或www.paipai.com, 使用代理.
 * 如果url为特殊的jd.com, 使用专用代理服务器
 * @author JingYing
 * @date 2015年4月15日
 */
public class StrategyInPaipai2 implements ConnStrategy{
	
	private static final Set<String> 
					publicPaipai = new HashSet<String>(),	//必须通过公网访问的*.paipai.com
					intraJd = new HashSet<String>();		//使用专用代理服务器访问的*.jd.com
	static {
		publicPaipai.add("www.paipai.com");
		intraJd.add("soa.u.jd.com");
	};
	
	@Override
	public String[] changeUrlAndHost(String originalUrl) {
		String[] hostIp = Util.parseHostIp(originalUrl);
		if(isIntraPaipai(hostIp[0]))	{
			String nginxIpport = PaiPaiConfig.getPaipaiCommonIp();
			String targetStr = hostIp[0];
			if(Util.isNotEmpty(hostIp[1]))	{
				targetStr = hostIp[0]+":"+hostIp[1];
			}
			return new String[]{originalUrl.replaceFirst(targetStr, nginxIpport), hostIp[0]};
		} else	{
			return new String[]{originalUrl, null};
		}
	}
	

	@Override
	public Proxy chooseProxy(String url) {
		String host = Util.parseHost(url);
		if(intraJd.contains(host))	{
			return new Proxy(Proxy.Type.HTTP, PaiPaiConfig.getJdPopHttpSvcProxyHost());
		} else if(isIntraPaipai(host))	{
			return Proxy.NO_PROXY;
		} else {
			boolean useProxy = host.matches("(\\d|[.])+") ? !Util.isIntraNet(host) : true;
			return useProxy ? new Proxy(Proxy.Type.HTTP, PaiPaiConfig.getHttpProxyHostQgo()) : Proxy.NO_PROXY;
		}
	}
	
	private boolean isIntraPaipai(String host)	{
		return host.matches(".+[.]paipai.com") 
					&& !publicPaipai.contains(host);
	}
	
}
