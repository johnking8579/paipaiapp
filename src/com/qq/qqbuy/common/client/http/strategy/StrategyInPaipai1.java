package com.qq.qqbuy.common.client.http.strategy;

import java.net.Proxy;
import java.util.HashSet;
import java.util.Set;

import com.qq.qqbuy.common.PaiPaiConfig;
import com.qq.qqbuy.common.client.http.ConnStrategy;
import com.qq.qqbuy.common.util.Util;

/**
 * 腾讯机房策略 - 迁移期策略,访问公网为主.
 * 如果host为域名,则使用代理服务器.如果将域名配在host文件中指向内网ip,可能会有问题.
 * 如果ip为内网ip,不使用代理服务器. 如果为公网ip,使用代理服务器
 * 如果url为特殊的jd.com, 使用专用代理服务器
 * @author JingYing
 * @date 2015年4月15日
 */
public class StrategyInPaipai1 implements ConnStrategy{
	
	private static final Set<String> intraJd = new HashSet<String>();		//使用专线代理服务器访问的*.jd.com
	static {
		intraJd.add("soa.u.jd.com");
	};

	@Override
	public String[] changeUrlAndHost(String originalUrl) {
		return new String[]{originalUrl, null};
	}

	@Override
	public Proxy chooseProxy(String url) {
		String host = Util.parseHost(url);
		if(intraJd.contains(host))	{
			return new Proxy(Proxy.Type.HTTP, PaiPaiConfig.getJdPopHttpSvcProxyHost());
		} else	{
			boolean useProxy = host.matches("(\\d|[.])+") ? !Util.isIntraNet(host) : true;
			return useProxy ? new Proxy(Proxy.Type.HTTP, PaiPaiConfig.getHttpProxyHostQgo()) : Proxy.NO_PROXY; 
		}
	}

}
