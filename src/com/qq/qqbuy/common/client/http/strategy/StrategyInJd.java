package com.qq.qqbuy.common.client.http.strategy;

import java.net.Proxy;

import com.qq.qqbuy.common.client.http.ConnStrategy;

/**
 * 京东机房策略 - 迁移期,直接访问公网为主.不使用代理服务器.
 * @author JingYing
 * @date 2015年4月15日
 */
public class StrategyInJd implements ConnStrategy{

	@Override
	public String[] changeUrlAndHost(String originalUrl) {
		return new String[]{originalUrl, null};
	}

	@Override
	public Proxy chooseProxy(String url) {
		return Proxy.NO_PROXY;
	}

}
