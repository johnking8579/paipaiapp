package com.qq.qqbuy.common.client.http.strategy;

import org.aopalliance.intercept.MethodInvocation;

import com.qq.qqbuy.common.client.AbstractHttpConnAdvice;
import com.qq.qqbuy.common.client.http.ConnStrategy;
import com.qq.qqbuy.common.client.http.HttpConn;
import com.qq.qqbuy.common.client.http.ReqHead;
import com.qq.qqbuy.common.env.EnvManager;

public class HttpStrategyAdvice extends AbstractHttpConnAdvice{
	
	private ConnStrategy connStrategy;
	
	public HttpStrategyAdvice(ConnStrategy connStrategy) {
		this.connStrategy = connStrategy;
	}

	@Override
	public Object aroundConnect(MethodInvocation invocation, String url) throws Throwable	{
		Object[] args = invocation.getArguments();
		if(connStrategy == null)	{
			connStrategy = createStrategy();
		}
		
		HttpConn impl = (HttpConn)invocation.getThis();
		impl.setProxy(connStrategy.chooseProxy((String)args[1]));
		
		String[] urlHost = connStrategy.changeUrlAndHost((String)args[1]);
		args[1] = urlHost[0];
		args[2] = handleHead((ReqHead)args[2], urlHost[1]);
		return invocation.proceed();
	}
	
	/**
	 * 根据部署环境选择连接策略
	 * @return
	 */
	private ConnStrategy createStrategy()	{
		if(EnvManager.isLocal())	{
			return new StrategyInLocal();
		} else if(EnvManager.isJd())	{
			return new StrategyInJd();
		} else if(EnvManager.isPaipai())	{
			return new StrategyInPaipai2();
		} else	{
			throw new UnsupportedOperationException("不识别的运行环境, envId=" + EnvManager.getEnvId());
		} 
	}
	
	/**
	 * 处理请求头中的host
	 * @param head
	 * @param hostFromStrategy
	 * @return
	 */
	private ReqHead handleHead(ReqHead head, String hostFromStrategy)	{
		if(hostFromStrategy == null)	return head;
		if(head == null)	head = new ReqHead();
		return head.setHost(hostFromStrategy);
	}


}
