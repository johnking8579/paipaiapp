package com.qq.qqbuy.common.client.http.strategy;

import java.net.Proxy;

import com.qq.qqbuy.common.client.http.ConnStrategy;

/**
 * 本地开发机策略. 都使用公网
 * @author JingYing
 * @date 2015年4月15日
 */
public class StrategyInLocal implements ConnStrategy{

	@Override
	public String[] changeUrlAndHost(String originalUrl) {
		return new String[]{originalUrl, null};
	}

	@Override
	public Proxy chooseProxy(String url) {
		return Proxy.NO_PROXY;
	}

}
