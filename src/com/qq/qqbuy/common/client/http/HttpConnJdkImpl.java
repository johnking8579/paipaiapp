package com.qq.qqbuy.common.client.http;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.util.Map.Entry;

public class HttpConnJdkImpl implements HttpConn{

	private Proxy proxy = Proxy.NO_PROXY;
	private int connectTimeout, readTimeout;

	public HttpResp post(String url, ReqHead head, InputStream body) throws IOException	{
		return connect("POST", url, head, body);
	}

	@Override
	public HttpResp connect(String method, String url, ReqHead head, InputStream body) throws IOException 	{
		HttpURLConnection conn = (HttpURLConnection)new URL(url).openConnection(proxy);
		if(connectTimeout != 0)		conn.setConnectTimeout(connectTimeout);
		if(readTimeout != 0)		conn.setReadTimeout(readTimeout);
		conn.setRequestMethod(method);
		if(head != null)	{
			for(Entry<String,String> e : head.toMap().entrySet())	{
				conn.setRequestProperty(e.getKey(), e.getValue());
			}
		}
		
		if("POST".equals(method) && body != null)	{
			try {
				conn.setDoOutput(true);
				OutputStream os = conn.getOutputStream();
				byte[] buf = new byte[1024];
				int len = 0;
				while((len=body.read(buf)) > 0)	{
					os.write(buf, 0, len);
				}
				os.flush();
				os.close();
			} finally	{
				closeStream(body);
			}
		} else if("GET".equals(method) && body != null)	{
			throw new UnsupportedOperationException("请求体有数据时应该使用POST");
		}
		
		HttpResp resp = new HttpResp();
		BufferedInputStream bis = null;
		ByteArrayOutputStream bos = null;
		try {
			bis = new BufferedInputStream(conn.getInputStream());
			bos = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			int len = 0;
			while((len=bis.read(buf)) > 0)	{
				bos.write(buf, 0, len);
			}
			bos.flush();
		} finally	{
			closeStream(bis, bos);
		}
		resp.setResponse(bos == null ? null : bos.toByteArray());
		resp.setResponseCode(conn.getResponseCode());
		resp.setRespHead(RespHead.parse(conn.getHeaderFields()));
		return resp;
		
	}
	
	private void closeStream(Closeable...closeable)	{
		for(Closeable c : closeable)	{
			try {
				if(c != null)	c.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public Proxy getProxy() {
		return proxy;
	}

	@Override
	public void setProxy(Proxy proxy) {
		this.proxy = proxy;
	}

	public int getConnectTimeout() {
		return connectTimeout;
	}

	public void setTimeout(int connectTimeout, int readTimeout) {
		this.connectTimeout = connectTimeout;
		this.readTimeout = readTimeout;
	}

	public int getReadTimeout() {
		return readTimeout;
	}

}
