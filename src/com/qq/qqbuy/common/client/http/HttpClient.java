package com.qq.qqbuy.common.client.http;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.util.Random;

import org.springframework.aop.framework.ProxyFactory;

import com.qq.qqbuy.common.ConfigHelper;
import com.qq.qqbuy.common.client.degrade.HttpCacheResponseAdvice;
import com.qq.qqbuy.common.client.degrade.HttpMockResponseAdvice;
import com.qq.qqbuy.common.client.http.strategy.HttpStrategyAdvice;
import com.qq.qqbuy.common.client.log.HttpConnLogAdvice;
import com.qq.qqbuy.common.client.log.PacketLogParser;
import com.qq.qqbuy.common.client.router.Router;
import com.qq.qqbuy.common.client.router.http.HttpConnRouterImpl;
import com.qq.qqbuy.common.client.ump.HttpUmpAdvice;

/**
 * 1.降级+缓存
 * 2.根据部署环境自动判定连接策略
 * 3.记录日志
 * 4.ump监控
 * 
 * 调用示例见main()
 */
public class HttpClient {
	private int connectTimeout = 5000, 			//默认连接超时时间, 毫秒为单位
				readTimeout = 5000;				//默认读取超时时间, 毫秒为单位
	private HttpConn impl;
	
	/**
	 * @param url
	 * @param head 请求头 没有特殊请求头时传null
	 * @param logParser 如何记录到invoke-detail和invoke-outline日志中, 不记录的话传null.有两个默认实现OnlyTimeLogParser, Utf8RespLogParser
	 * @return
	 * @throws IOException
	 */
	public HttpResp get(String url, ReqHead head, PacketLogParser logParser) throws IOException	{
		return connect("GET", url, head, null, logParser, null, null);
	}
	
	/**
	 * 
	 * @param url
	 * @param head
	 * @param logParser
	 * @param strategy 设置http发起策略. 为null时有默认的策略,如无特殊需要,不要设置该值,以便将来统一切换.
	 * @param defaultResp 该服务如果被降级,返回什么response.如果为null,则使用CacheRespPool里的缓存 
	 * @return
	 * @throws IOException
	 */
	public HttpResp get(String url, ReqHead head, PacketLogParser logParser, ConnStrategy strategy, byte[] defaultResp) throws IOException	{
		return connect("GET", url, head, null, logParser, strategy, defaultResp);
	}
	
	/**
	 * post提交
	 * @param url
	 * @param head 请求头 没有特殊请求头时传null
	 * @param body 请求体,使用后自动关闭流
	 * 		如果使用multipart/form-data上传文件, 参照HttpUtil.uploadFile(), 或用apache-httpclient.jar拼接请求体, 例如:
		<pre>
		String boundary = HttpClient.generateBoundary();
		ReqHead head = new ReqHead().setContentType("multipart/form-data;boundary=" + boundary);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		MultipartEntityBuilder.create()
			.setBoundary(boundary)
			.addPart("name1", new StringBody("value1", ContentType.TEXT_PLAIN))
			.addPart("name2", new FileBody(new File("c:/aaa.txt"),ContentType.MULTIPART_FORM_DATA, "aaa.txt"))
			.addPart("name3", new ByteArrayBody("1111111111".getBytes(), ContentType.APPLICATION_OCTET_STREAM, "value3"))
			.build().writeTo(bos);
		ByteArrayInputStream body = new ByteArrayInputStream(bos.toByteArray());
		new HttpClient().post("url", head, body, null);
		</pre>
	 * @param logParser 如何记录到invoke-detail和invoke-outline日志中, 不记录的话传null.	
	 * 			如果只记录时间,不记录请求体响应体,传new OnlyTimeLogParser()
	 * @return
	 * @throws IOException
	 */
	public HttpResp post(String url, ReqHead head, InputStream body, PacketLogParser logParser) throws IOException	{
		return connect("POST", url, head, body, logParser, null, null);
	}
	
	/**
	 * 同上
	 * @param url
	 * @param head
	 * @param body
	 * @param logParser
	 * @param strategy
	 * @param defaultResp 如果该服务被降级后,使用什么样的mock响应体. 默认为null,将使用该uri的缓存,见(HttpCacheResponseAdvice)
	 * @return
	 * @throws IOException
	 */
	public HttpResp post(String url, ReqHead head, InputStream body, PacketLogParser logParser, ConnStrategy strategy, byte[] defaultResp) throws IOException	{
		return connect("POST", url, head, body, logParser, strategy, defaultResp);
	}
	
	/**
	 * 自动生成Content-Type=multipart/form-data;boundary=${boundary}的分隔线
	 * @return
	 */
	public static String generateBoundary() {
        StringBuilder buffer = new StringBuilder();
        Random rand = new Random();
        int count = rand.nextInt(11) + 30; // a random size from 30 to 40
        for (int i = 0; i < count; i++) {
            buffer.append(MULTIPART_CHARS[rand.nextInt(MULTIPART_CHARS.length)]);
        }
        return buffer.toString();
    }
	
	private HttpResp connect(String method, String url, ReqHead head, InputStream body, 
							PacketLogParser logParser, ConnStrategy strategy, byte[] defaultResp) throws IOException	{
		prepareImpl();
		
		ProxyFactory factory = new ProxyFactory(impl);
		factory.setInterfaces(new Class[]{HttpConn.class});
		factory.setProxyTargetClass(false);
		if(ConfigHelper.isSysDegrade())	{
			factory.addAdvice(new HttpMockResponseAdvice(defaultResp));
			factory.addAdvice(new HttpCacheResponseAdvice());
		}
		factory.addAdvice(new HttpStrategyAdvice(strategy));
		if(logParser != null)	{
			factory.addAdvice(new HttpConnLogAdvice(logParser));
		}
		factory.addAdvice(new HttpUmpAdvice());
		impl = (HttpConn)factory.getProxy();
		
		return impl.connect(method, url, head, body);
	}
	
	private final static char[] MULTIPART_CHARS = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
	private int router = Router.NONE;
	
	private void prepareImpl()	{
		if(router == Router.NONE)	{
			if(!(impl instanceof HttpConnJdkImpl))	{
				impl = new HttpConnJdkImpl();
			}
			HttpConnJdkImpl i1 = (HttpConnJdkImpl)impl;
			i1.setTimeout(connectTimeout, readTimeout);
		} else {
			if(!(impl instanceof HttpConnRouterImpl))	{
				impl = new HttpConnRouterImpl(router);
			}
			HttpConnRouterImpl i3 = (HttpConnRouterImpl)impl;
			i3.setRouter(router);
		}
	}
	
	public HttpClient setRouter(int router) {
		this.router = router;
		return this;
	}

	public HttpClient setTimeout(int connectTimeout, int readTimeout) {
		this.connectTimeout = connectTimeout;
		this.readTimeout = readTimeout;
		return this;
	}
	
	public static void main(String[] args) throws IOException {
		//简单get
		HttpResp resp1 = new HttpClient().get("http://www.baidu.com", null, null);
		System.out.println(new String(resp1.getResponse(), "gbk"));
		
		//post
		InputStream body = new ByteArrayInputStream("bodybody".getBytes("utf-8"));
		ReqHead head = new ReqHead().setUserAgent("chrome");
		HttpResp resp2 = new HttpClient().setTimeout(3000, 3000).post("https://www.baidu.com/s?wd=111", head, body, null);
		System.out.println(new String(resp2.getResponse(), "utf-8"));
		
		//带日志记录get
		HttpResp resp3 = new HttpClient().get("http://www.baidu.com", null, new PacketLogParser() {
			
			@Override
			public String parseResponse(byte[] response) throws Exception {
				if(response != null)	return new String(response, "utf-8");
				return null;
			}
			
			@Override
			public String parseRequest(byte[] request) throws Exception {
				return "请求体请求体";	//或return null
			}
			
			@Override
			public String getServiceName() {
				return null;	//一般用于tcp/udp请求.
			}
		});
		System.out.println(new String(resp3.getResponse(), "utf-8"));
		
		
		//不使用默认的连接策略.用自定义连接策略.
		HttpResp resp4 = new HttpClient().get("http://bi.paipai.com", null, null, new ConnStrategy() {
			
			@Override
			public Proxy chooseProxy(String url) {
				return Proxy.NO_PROXY;	//直接连接bi.paipai.com,不使用默认的代理服务器
			}
			
			@Override
			public String[] changeUrlAndHost(String originalUrl) {
				return new String[]{"http://bi.paipai.com", null};
			}
		}, null);
		
		System.out.println(new String(resp4.getResponse(), "gbk"));
		
	}
}
