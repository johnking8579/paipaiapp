package com.qq.qqbuy.common.client.log;

import java.io.IOException;

import org.aopalliance.intercept.MethodInvocation;

import com.qq.qqbuy.common.client.AbstractHttpConnAdvice;
import com.qq.qqbuy.common.client.http.HttpResp;
import com.qq.qqbuy.common.client.http.ReqHead;
import com.qq.qqbuy.common.util.Util;


public class HttpConnLogAdvice extends AbstractHttpConnAdvice{
	
	private PacketLogParser logParser;
	
	public HttpConnLogAdvice(PacketLogParser logParser)	{
		this.logParser = logParser;
	}
	
	@Override
	public Object aroundConnect(MethodInvocation invocation, String url) throws Throwable	{
		ReqHead head = (ReqHead)invocation.getArguments()[2];
		
		HttpLogItem item = new HttpLogItem();
		item.setInvokeStack(Thread.currentThread().getStackTrace());
		item.setUrl(url);
		if(head != null && head.getHost() != null)	{
			item.setHost(head.getHost());		//首选头信息中的host
		} else	{
			item.setHost(Util.parseHost(url));
		}
	
		long start = System.currentTimeMillis();
		try {
			HttpResp resp = (HttpResp)invocation.proceed();
			item.setCostMillis(System.currentTimeMillis() - start);
			item.setContentType(resp.getRespHead().getContentType());
			item.setContentLength(resp.getRespHead().getContentLength());
			item.setRespCode(resp.getResponseCode() + "");

			try {
				if(logParser != null){ 					
					item.setReqBody(logParser.parseRequest(null));
					if(resp.getResponse().length > 0)	{
						item.setRespText(logParser.parseResponse(resp.getResponse()));
					}
				}
			} catch (Throwable e) {	//防止自定义的logParser向外抛出异常
				e.printStackTrace();
			}
			return resp;
		} catch (IOException e) {
			item.setCostMillis(System.currentTimeMillis() - start);
			item.setException(e);
			throw new IOException(url, e);
		} finally	{
			item.log();
		}
	}

}
