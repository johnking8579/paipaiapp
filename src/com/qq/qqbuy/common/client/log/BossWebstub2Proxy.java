package com.qq.qqbuy.common.client.log;

import java.lang.reflect.Method;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.paipai.component.c2cplatform.IServiceObject;
import com.qq.qqbuy.common.Monitor;
import com.qq.qqbuy.common.Tuple3;
import com.qq.qqbuy.thirdparty.idl.boss.BossWebStub2;

public class BossWebstub2Proxy implements MethodInterceptor{
	
	private static Logger log = LogManager.getLogger(BossWebstub2Proxy.class);
	
	private static Gson gson = new Gson();
	
	public static BossWebStub2 newProxy()	{
		Enhancer e = new Enhancer();
		e.setSuperclass(BossWebStub2.class);
		e.setCallback(new BossWebstub2Proxy());
		return (BossWebStub2)e.create();
	}

	@Override
	public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
		if("invoke".equals(method.getName()))	{
			BossLogItem item = new BossLogItem();
			item.setInvokeStack(Thread.currentThread().getStackTrace());
			
			long start = System.currentTimeMillis();
			Object result = null;
			
			try {
				Tuple3<String, IServiceObject, IServiceObject> tuple3 = this.getReqResp(args);
				String key = String.format("ao://0x%s:%s", Long.toHexString(tuple3.getB().getCmdId()), tuple3.getA());
				Object caller = Monitor.registerInfo(key);
				result = proxy.invokeSuper(obj, args);
				Monitor.registerInfoEnd(caller);
				return result;
			} catch (Exception e) {
				item.setException(e);
				throw e;
			} finally	{

				try {
					Tuple3<String, IServiceObject, IServiceObject> tuple3 = this.getReqResp(args);	//invoke结束后, resp才有值
					IServiceObject req = tuple3.getB(), resp = tuple3.getC();
					//item.setIpPort(ipPort);	//TODO 无法获得ipport
					item.getReqArgs().put(req.getClass().getSimpleName(), gson.toJson(args[0]));
					item.getRespArgs().put(resp.getClass().getSimpleName(), gson.toJson(args[0]));
					item.setCmdId(req.getCmdId());
				} catch (Exception e1) {
					log.error("BossWebstub2.class的签名可能发生变化:" + e1.getMessage(), e1);
				}
				
				item.setInvokeResult(result+"");
				item.setCostMillis(System.currentTimeMillis() - start);
				item.log();		//抛出异常或正常返回前都要记录日志
			}
		} else	{
			return proxy.invokeSuper(obj, args);
		}
	}
	
	/**
	 * 
	 * @param args
	 * @return BossCmd+Request+Response
	 */
	private Tuple3<String, IServiceObject, IServiceObject> getReqResp(Object[] args)	{
		Tuple3<String, IServiceObject, IServiceObject> tuple3 = new Tuple3<String, IServiceObject, IServiceObject>();
		if(args[0] instanceof String)	{	//根据参数个数及类型, 判断调用的是哪个invoke方法. 如果增加了invoke,则要重新判断
			tuple3.setA((String)args[0]);
			tuple3.setB((IServiceObject)args[1]);
			if(args.length >= 3 && args[2] instanceof IServiceObject)	{
				tuple3.setC((IServiceObject)args[2]);
			}
		} else	{
			tuple3.setB((IServiceObject)args[0]);
			tuple3.setC((IServiceObject)args[1]);
		}
		return tuple3;
	}
	
}


