package com.qq.qqbuy.common.client.log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;

public class HttpLogItem extends AbstractLogItem	{
	
	private String url;
	private String reqBody, contentType, respCode, host, respText;	//请求的消息体, 响应的contenttype,响应码, 请求host, 响应的文本
	private long costMillis;			//连接+读取的总时间,不包括日志解析时间. HttpClient用.
	private long contentLength;
	private Throwable exception;		//抛出的异常
	private StackTraceElement[] invokeStack;	//调用栈轨迹
	
	@Deprecated	//改成httpClient后, 使用costMillis
	private long connectMillis;			//建立连接时间 . 注:该指标数据不准确.仅供参考 -Jingying
	@Deprecated	//改成httpClient后, 使用costMillis
	private long connectCostMillis;		//建立TCP连接及获取响应头所需要的时间,主指标,体现服务器端响应速度. 注:根据API调用方式,该值可能不包含建立TCP连接所需要时间.
	@Deprecated	//改成httpClient后, 使用costMillis
	private long readBodyCostMillis;	//读完响应消息体所需要的时间.副指标.体现大数据量下网络连接速度
	
	
	@Override
	public String getProtocol() {
		return PROTOCOL_HTTP;
	}

	@Override
	public String getServiceUrl() {
		return url;
	}

	@Override
	public boolean hasError() {
		return exception != null || respCode.startsWith("4") || respCode.startsWith("5");
	}

	@Override
	public long getCostMillis() {
		return costMillis != 0 ? costMillis : connectCostMillis;
	}
	
	@Override
	public StackTraceElement[] getInvokeStack() {
		return invokeStack;
	}

	@Override
	public List<String> getParticularProp() {	
		return Arrays.asList(new String[]{
				host, respCode, contentType, contentLength+""		//不记录响应体
		});
	}

	@Override
	public String toText() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format(">> requestId(%s),host(%s),请求地址:%s\n", getRequestId(), getHost(), url));
		if(reqBody!=null && !"".equals(reqBody))	{
			sb.append(String.format(">> 请求消息体: %s\n", reqBody));
		}
		if(exception == null)	{
			sb.append(String.format("<< 响应码(%s),响应content-type(%s),耗时(%s)毫秒\n", 
									respCode, contentType, getCostMillis()));
			if(respText != null)	{	//为空时的原因:1.调用api时选择不解析响应体   2.无响应体
				String filterResp = respText.replace("\r\n", "").replace("\n", "").replace("\t", "");
				sb.append(String.format("<< 响应消息体:%s\n", filterResp));
			}
			
			sb.append(">> 调用栈:\n");
			if(invokeStack != null)	{
				for(String s : filterInvokeStack())	{
					sb.append("\t").append(s).append("\n");
				}
			} else	{
				sb.append("null\n");
			}
		} else	{
			StringWriter sw = new StringWriter(1000);
			exception.printStackTrace(new PrintWriter(sw));
			sb.append("<< 异常:").append(sw.toString()).append("\n");
		}
		sb.append("============================================================");	//60
		return sb.toString();
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public void setConnectCostMillis(long connectCostMillis) {
		this.connectCostMillis = connectCostMillis;
	}

	public void setException(Throwable exception) {
		this.exception = exception;
	}

	public void setInvokeStack(StackTraceElement[] invokeStack) {
		this.invokeStack = invokeStack;
	}

	public void setRespText(String respText) {
		this.respText = respText;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public void setReadBodyCostMillis(long readBodyCostMillis) {
		this.readBodyCostMillis = readBodyCostMillis;
	}

	public void setContentLength(long contentLength) {
		this.contentLength = contentLength;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public long getConnectMillis() {
		return connectMillis;
	}

	public void setConnectMillis(long connectMillis) {
		this.connectMillis = connectMillis;
	}

	public String getReqBody() {
		return reqBody;
	}

	public void setReqBody(String reqBody) {
		this.reqBody = reqBody;
	}

	public void setCostMillis(long costMillis) {
		this.costMillis = costMillis;
	}
	
}
