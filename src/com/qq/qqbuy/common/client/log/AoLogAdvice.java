package com.qq.qqbuy.common.client.log;

import java.util.Arrays;
import java.util.Map;

import org.aopalliance.intercept.MethodInvocation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.ReflectGetValue;
import com.qq.qqbuy.common.client.AbstractWebStubAdvice;


/**
 * AsynWebStub的日志监控代理类
 * 调用方在WebStubFactory
 * @author JingYing 2014-9-29
 *
 */
public class AoLogAdvice extends AbstractWebStubAdvice	{
	
	static Logger log = LogManager.getLogger();
	
	private static Gson gson;
	private static byte[] blankSkey = new byte[10];	//AsynWebstub中的skey默认为new byte[10]
	
	static	{
		gson = new GsonBuilder().addSerializationExclusionStrategy(new ExclusionStrategy(){
			@Override
			public boolean shouldSkipClass(Class<?> c) {
				return false;
			}
			@Override
			public boolean shouldSkipField(FieldAttributes f) {
				return f.getName().endsWith("_u");
			}
		}).create();
	}
	
	/**
	 * 监控AsynWebStub.invoke(Object o1, Object o2)
	 * 1.记录AO运行时长
	 * 2.记录AO调用的栈轨迹
	 * 3.记录AO的request和response
	 * 4.记录AO请求命令字/IP/端口
	 * 5.记录AO服务是否异常
	 * 7.去掉gson参数中, key为_u的字段 
	 */
	@Override
	public Object aroundInvoke(MethodInvocation invocation, long cmdId) throws Throwable {
		Object[] args = invocation.getArguments();
		AoLogItem item = new AoLogItem();
		item.setInvokeStack(Thread.currentThread().getStackTrace());
		String reqName = args[0].getClass().getSimpleName();
		
		AsynWebStub a = (AsynWebStub)invocation.getThis();
		item.setCmdId(cmdId);
		if(a.getSkey() != null && !Arrays.equals(a.getSkey(), blankSkey))
			item.getStubParam().put("skey", new String(a.getSkey()));
		if(a.getClientIP() != 0)
			item.getStubParam().put("clientIp", a.getClientIP()+"");
		if(a.getOperator() != 0)
			item.getStubParam().put("operator", a.getOperator()+"");
		if(a.getUin() != 0)
			item.getStubParam().put("uin", a.getUin()+"");	//在invoke前读uin, 否则会被asyncwebstub修改
		
		Object result = null;
		long start = System.currentTimeMillis();
		try {
			result = invocation.proceed();
			return result;
		} catch (Exception e) {
			item.setException(e);
			throw e;
		} finally	{
			item.setCostMillis(System.currentTimeMillis() - start);
			if(item.getCostMillis() > a.getConnectTimeout() || item.getCostMillis() > a.getReadTimeout())	{
				item.setIsTimeout(true);	//当调用总时间超过预设的connecttimeout或readtimeout时间,报超时.由于无法从AsynWebStub.invoke()中判断是否超时, 所以有一点误差.
			}
			item.setReqIpport(a.getIp() + ":" + a.getPort());	//invoke后,stub中才能取得IP
			item.setEncoding(a.getStringEncodecharset());
			item.setInvokeResult(String.valueOf(result));
			//查出返回的resp中result的值如果不为0了则设置返回的result
			try {
				Map<Object, Object> rt = ReflectGetValue.getFileValue(args[1], "result");
				if(rt!=null){
					String rs = (String) rt.get("result");
					if(rt!=null&&!"0".equals(rt)){
						Long r = Long.valueOf(rs);
						item.setResult(r);
					}
				}
			} catch (Exception e2) {
				log.fatal("可能返回的Resp反射取值时错误:" + e2.getMessage(), e2);
			}
		
			try {
				item.getRequest().put(reqName, gson.toJson(args[0]));
				String resp = gson.toJson(args[1]);
				if(resp != null && resp.length() > 10000)	{
					resp = resp.substring(0, 10000) + "....too long, truncate";		//截断超级长的response,防止日志过大
				}
				item.getResponse().put(args[1].getClass().getSimpleName(), resp);
			} catch (Exception e) {
				log.fatal("AsynWebStub.class的签名可能发生了变化:" + e.getMessage(), e);
			}
			item.log();		//抛出异常或正常返回前都要记录日志
		}
	}
}
