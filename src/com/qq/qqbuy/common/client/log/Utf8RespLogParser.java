package com.qq.qqbuy.common.client.log;

/**
 * PacketLogParser和简单实现类.
 * 不记录请求体, 只记响应体, 将响应体以utf8编码写入日志
 */
public class Utf8RespLogParser implements PacketLogParser {

	@Override
	public String getServiceName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String parseRequest(byte[] request) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String parseResponse(byte[] response) throws Exception {
		if(response == null)	return null;
		return new String(response, "utf-8");
	}

}
