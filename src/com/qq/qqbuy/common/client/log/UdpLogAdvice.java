package com.qq.qqbuy.common.client.log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;

import org.aopalliance.intercept.MethodInvocation;

import com.qq.qqbuy.common.client.AbstractUdpConnAdvice;

public class UdpLogAdvice extends AbstractUdpConnAdvice{
	
	private PacketLogParser logParser;
	
	public UdpLogAdvice(PacketLogParser logParser) {
		this.logParser = logParser;
	}
	
	@Override
	public Object aroundSend(MethodInvocation invocation, String host, int port) throws Throwable {
		UdpLogItem item = new UdpLogItem();
		item.setInvokeStack(Thread.currentThread().getStackTrace());
		item.setRemoteSocketAddress(host + ":" + port);
		try {
			item.setServiceName(logParser.getServiceName());
			item.setReq(logParser.parseRequest(new byte[0]));
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		try {
			return invocation.proceed();
		} catch(IOException e)	{
			item.setException(e);
			throw e;
		} finally	{
			item.log();
		}
	}

	@Override
	public Object aroundSendAndReceive(MethodInvocation invocation, String host, int port) throws Throwable {
		UdpLogItem item = new UdpLogItem();
		item.setInvokeStack(Thread.currentThread().getStackTrace());
		item.setRemoteSocketAddress(host + ":" + port);
		DatagramPacket resp = null;
		
		long start = System.currentTimeMillis();
		try {
			resp = (DatagramPacket)invocation.proceed();
			return resp;
		} catch(IOException e)	{
			item.setException(e);
			throw e;
		} finally	{
			item.setCostMillis(System.currentTimeMillis() - start);
			try {
				if(logParser != null && item.getException() == null)	{
					item.setServiceName(logParser.getServiceName());
					item.setReq(logParser.parseRequest(new byte[0]));
					if(resp != null)	{
						item.setResp(logParser.parseResponse(resp.getData()));
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			item.log();
		}
	}

}
