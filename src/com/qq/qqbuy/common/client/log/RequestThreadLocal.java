package com.qq.qqbuy.common.client.log;

import com.qq.qqbuy.common.Tuple2;

public class RequestThreadLocal {
	
	/**
	 * string=requestId
	 * long=wid
	 */
	private static ThreadLocal<Tuple2<String,Long>> t = new ThreadLocal<Tuple2<String,Long>>();
	
	
	public static void set(String requestId, long wid)	{
		t.set(new Tuple2<String,Long>(requestId, wid));
	}
	
	public static String getRequestId()	{
		Tuple2<String,Long> tuple = t.get();
		return tuple == null ? null : tuple.getA(); 
	}
	
	public static long getWid()	{
		Tuple2<String,Long> tuple = t.get();
		return tuple == null ? 0L : tuple.getB(); 
	}
}
