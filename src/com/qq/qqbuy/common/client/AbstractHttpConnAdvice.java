package com.qq.qqbuy.common.client;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public abstract class AbstractHttpConnAdvice implements MethodInterceptor{
	
	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		if("connect".equals(invocation.getMethod().getName()))	{
			String url = (String)invocation.getArguments()[1];
			return aroundConnect(invocation, url);
		} else	{
			return invocation.proceed();
		}
	}
	
	public abstract Object aroundConnect(MethodInvocation invocation, String url) throws Throwable;
	
}
