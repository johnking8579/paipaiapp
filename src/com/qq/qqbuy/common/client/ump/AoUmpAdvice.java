package com.qq.qqbuy.common.client.ump;

import org.aopalliance.intercept.MethodInvocation;

import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.qq.qqbuy.common.Monitor;
import com.qq.qqbuy.common.client.AbstractWebStubAdvice;


public class AoUmpAdvice extends AbstractWebStubAdvice	{
	
	@Override
	public Object aroundInvoke(MethodInvocation invocation, long cmdId) throws Throwable {
		Object[] args = invocation.getArguments();
		String reqName = args[0].getClass().getSimpleName();
		Object caller = Monitor.registerInfo(genKey4Ump(cmdId, reqName));
		long start = System.currentTimeMillis();
		try {
			Object result = invocation.proceed();
			long cost = System.currentTimeMillis() - start;
			AsynWebStub stub = (AsynWebStub)invocation.getThis();
			if(cost > stub.getConnectTimeout() || cost > stub.getReadTimeout())	{
				//当调用总时间超过预设的connecttimeout或readtimeout时间,报超时.由于无法从AsynWebStub.invoke()中判断是否超时, 所以有一点误差.
				Monitor.functionError(caller);
			}
			Monitor.registerInfoEnd(caller);
			return result;
		} catch (Exception e) {
			Monitor.functionError(caller);
			Monitor.registerInfoEnd(caller);
			throw e;
		}
	}
}
