package com.qq.qqbuy.common.client.ump;

import org.aopalliance.intercept.MethodInvocation;

import com.qq.qqbuy.common.Monitor;
import com.qq.qqbuy.common.client.AbstractHttpConnAdvice;
import com.qq.qqbuy.common.util.Util;

/**
 * UMP上报.由于PacketLogParser受调用方控制,所以UMP上报放到单独的切面中.
 * @author JingYing
 * @date 2015年5月6日
 */
public class HttpUmpAdvice extends AbstractHttpConnAdvice{
	
	@Override
	public Object aroundConnect(MethodInvocation invocation, String url) throws Throwable	{
		Object caller = Monitor.registerInfo(Util.truncateParam(url));
		try {
			return invocation.proceed();
		} catch (Exception e) {
			Monitor.functionError(caller);
			throw e;
		} finally	{
			Monitor.registerInfoEnd(caller);
		}
	}

}
