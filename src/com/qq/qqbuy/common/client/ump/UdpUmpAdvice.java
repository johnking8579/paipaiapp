package com.qq.qqbuy.common.client.ump;

import org.aopalliance.intercept.MethodInvocation;

import com.qq.qqbuy.common.Monitor;
import com.qq.qqbuy.common.client.AbstractUdpConnAdvice;

public class UdpUmpAdvice extends AbstractUdpConnAdvice{
	
	private String serviceName;
	
	public UdpUmpAdvice(String serviceName) {
		this.serviceName = serviceName;
	}
	
	@Override
	public Object aroundSend(MethodInvocation invocation, String host, int port) throws Throwable {
		return invocation.proceed();
	}

	@Override
	public Object aroundSendAndReceive(MethodInvocation invocation, String host, int port) throws Throwable {
		 Object caller = Monitor.registerInfo(genKey4Ump(host,  port, serviceName));
		 try {
			return invocation.proceed();
		} catch (Exception e) {
			Monitor.functionError(caller);
			throw e;
		} finally	{
			Monitor.registerInfoEnd(caller);
		}
	}

}
