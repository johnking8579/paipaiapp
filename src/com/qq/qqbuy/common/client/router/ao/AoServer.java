package com.qq.qqbuy.common.client.router.ao;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.protobuf.ByteString;
import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.paipai.util.io.ByteStream;
import com.qq.qqbuy.common.client.ao.AoClient;
import com.qq.qqbuy.common.client.router.Router;
import com.qq.qqbuy.common.client.router.ao.AoIdl.AoRouterReq;
import com.qq.qqbuy.common.client.router.ao.AoIdl.AoRouterResp;
import com.qq.qqbuy.common.client.router.ao.AoIdl.AoRouterResp.Builder;
import com.qq.qqbuy.common.util.Util;

public class AoServer {
	
	private static Logger log = LogManager.getLogger();
	
	public byte[] handle(byte[] in, String clientIp)	{
		AsynWebStub webStub = AoClient.newProxyWebStub();
//		AsynWebStub webStub = new AsynWebStub();
		
		webStub.setTimeout(3000, 3000);
		
		try {
			AoRouterReq routerReq = AoRouterReq.parseFrom(in);
			if(checkAuth(routerReq.getAuth()))	return null;
			
			this.copy(routerReq, webStub);
			Object reqObj = Class.forName(routerReq.getReqClass() + "Req").newInstance();
			Object respObj = Class.forName(routerReq.getReqClass() + "Resp").newInstance();
			ByteStream reqBs = AoUtil.toByteStream(routerReq.getReqBody());
			AoUtil.invokeUnSerialize(reqObj, reqBs);
			
			int i = webStub.invoke(reqObj, respObj);
			
			Builder builder = AoRouterResp.newBuilder().setErr("")
						.setInvokeResult(i).setIp(Util.getLocalIp());
			if(i == 0)	{
				byte[] respBytes = AoUtil.invokeSerialize(respObj);
				builder.setResponse(ByteString.copyFrom(respBytes));
			}
			return builder.build().toByteArray();
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			return AoRouterResp.newBuilder()
					.setErr(sw.toString())
					.setIp(Util.getLocalIp())
					.build().toByteArray();
		}
		
	}
	
	
	private void copy(AoRouterReq req, AsynWebStub stub) {
//		if(req.getStubIp() != null && req.getStubPort() != 0)
//			stub.setIpAndPort(req.getStubIp(), req.getStubPort());
		
		stub.setConfigType(req.getStubConfigType());
		stub.setClientIP(req.getStubClientIp());
		stub.setMachineKey(req.getStubMachineKey().toByteArray());
		stub.setOperator(req.getStubOperator());
		stub.setSkey(req.getStubSkey().toByteArray());
		stub.setStringDecodecharset(req.getStubStringDecodecharset());
		stub.setStringEncodecharset(req.getStubStringEncodecharset());
		stub.setUin(req.getStubUin());
	}
	
	private boolean checkAuth(String auth)	{
		return !Router.SERVER_AUTH.equals(auth);
	}
	
	

}
