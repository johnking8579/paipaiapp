package com.qq.qqbuy.common.client.router.ao;

import com.google.protobuf.ByteString;
import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

public class AoUtil {
	
	public static ByteStream toByteStream(ByteString pb)	{
		byte[] bs = pb.toByteArray();
		return new ByteStream(bs, bs.length);
	}
	
	public static ByteString toByteString(ByteStream ao)	{
		return ByteString.copyFrom(ao.asByteBuffer());
	}
	
	public static byte[] invokeSerialize(Object obj) throws Exception	{
		ByteStream bs = null;
		if(obj instanceof ICanSerializeObject)	{
			int size = ((ICanSerializeObject)obj).getSize();
			bs = new ByteStream(new byte[size], size);
			((ICanSerializeObject)obj).serialize(bs);
		} else if(obj instanceof IServiceObject)	{
			int size = (Integer)obj.getClass().getMethod("getSize").invoke(obj);
			bs = new ByteStream(new byte[size], size);
			((IServiceObject)obj).Serialize(bs);
		} else	{
			throw new UnsupportedOperationException("不支持serialize的类型:" + obj.getClass().getName());
		}
		return bs.asByteBuffer().array();
	}
	
	
	public static void invokeUnSerialize(Object obj, ByteStream bs) throws Exception	{
		if(obj instanceof ICanSerializeObject)	{
			((ICanSerializeObject)obj).unSerialize(bs);
		} else if(obj instanceof IServiceObject)	{
			((IServiceObject)obj).Serialize(bs);
		} else	{
			throw new UnsupportedOperationException("不支持unSerialize的类型:" + obj.getClass().getName());
		}
	}

}
