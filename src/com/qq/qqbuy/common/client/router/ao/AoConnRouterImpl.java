package com.qq.qqbuy.common.client.router.ao;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.protobuf.ByteString;
import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.paipai.util.io.ByteStream;
import com.qq.qqbuy.common.client.ao.AoConn;
import com.qq.qqbuy.common.client.http.HttpConnJdkImpl;
import com.qq.qqbuy.common.client.http.ReqHead;
import com.qq.qqbuy.common.client.router.ClientRouterException;
import com.qq.qqbuy.common.client.router.Router;
import com.qq.qqbuy.common.client.router.ao.AoIdl.AoRouterReq;
import com.qq.qqbuy.common.client.router.ao.AoIdl.AoRouterResp;
import com.qq.qqbuy.common.util.Util;

public class AoConnRouterImpl implements AoConn {
	
	private static Logger log = LogManager.getLogger();
	
	private int router = Router.PP_GAMMA;
	
	public AoConnRouterImpl(int router) {
		this.router = router;
	}
	
	@Override
	public int connect(AsynWebStub stub, Object req, Object resp)  {
		String reqFullname = req.getClass().getName();
		String reqClass = reqFullname.substring(0, reqFullname.length()-3);
		byte[] seria;
		try {
			seria = AoUtil.invokeSerialize(req);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		byte[] pb = AoRouterReq.newBuilder()
			.setAuth(Router.SERVER_AUTH)
			.setReqClass(reqClass)
			.setReqBody(ByteString.copyFrom(seria))
			.setStubClientIp(stub.getClientIP())
			.setStubConfigType(stub.getConfigType())
			.setStubIp(stub.getIp())
			.setStubMachineKey(ByteString.copyFrom(stub.getMachineKey()))
			.setStubOperator(stub.getOperator())
			.setStubPort(stub.getPort())
			.setStubSkey(ByteString.copyFrom(stub.getSkey()))
			.setStubStringDecodecharset(stub.getStringDecodecharset())
			.setStubStringEncodecharset(stub.getStringEncodecharset())
			.setStubUin(stub.getUin())
			.build().toByteArray();
		
		ReqHead head = new ReqHead();
		head.setContentType("application/octet-stream");
		String url = Router.chooseHost(router) + Router.SERVER_URL + "?type=ao";
		try {
			byte[] ret = new HttpConnJdkImpl().post(url, head, new ByteArrayInputStream(pb)).getResponse();
			AoRouterResp routerResp = AoRouterResp.parseFrom(ret);
System.out.println("ip:" + routerResp.getIp());
			if("".equals(routerResp.getErr()))	{
				ByteStream respBs = AoUtil.toByteStream(routerResp.getResponse());
				AoUtil.invokeUnSerialize(resp, respBs);
				return routerResp.getInvokeResult();
			} else	{
				throw new ClientRouterException("服务器端异常:" + routerResp.getErr());
			}
		} catch (Exception e) {
			throw new ClientRouterException(e);
		}
	}
	
	
	/**
	 * 
	 * @param appToken
	 * @param mk
	 * @return
	 */
	public UinSkey parseToken(String appToken, String mk)	{
		List<String> par = new ArrayList<String>();
		par.add("auth=" + Router.SERVER_AUTH);
		par.add("token=" + appToken);
		par.add("mk=" + mk);
		String url = Router.chooseHost(router) + "/api/sometest/wfadfvasdedacxvee.xhtml?" + Util.join(par, "&");  
		try {
			byte[] respBs = new HttpConnJdkImpl().post(url, null, null).getResponse();
			String[] arr = Util.toString(respBs, "utf-8").split("\\|");
			return new UinSkey(
					Long.parseLong(arr[0]), 
					arr[1], 
					Boolean.parseBoolean(arr[2]));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
