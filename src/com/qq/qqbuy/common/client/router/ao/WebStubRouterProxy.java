package com.qq.qqbuy.common.client.router.ao;

import java.lang.reflect.Method;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.qq.qqbuy.common.client.ao.AoClient;

public class WebStubRouterProxy implements MethodInterceptor{
	static Logger log = LogManager.getLogger(WebStubRouterProxy.class);
	private int router;
	
	/**
	 * 创建代理对象
	 * @param target
	 * @return
	 */
	public static AsynWebStub newProxy(int router) {
		Enhancer enhancer = new Enhancer();
		enhancer.setSuperclass(AsynWebStub.class);
		WebStubRouterProxy proxy = new WebStubRouterProxy();
		proxy.router = router;
		enhancer.setCallback(proxy);
		return (AsynWebStub)enhancer.create();		// 创建代理对象
	}
	
	@Override
	public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
		if("invoke".equals(method.getName()) && args.length == 2)	{	//拦截AsynWebStub.invoke(Object o1, Object o2)
			if(!(obj instanceof AsynWebStub))	
				throw new IllegalArgumentException("不识别的对象" + obj);
			Object req = args[0];
			AsynWebStub stub = (AsynWebStub)obj;
			return new AoConnRouterImpl(router).connect(stub, req, args[1]);
		} else	{
			return proxy.invokeSuper(obj, args);
		}
	}
}
