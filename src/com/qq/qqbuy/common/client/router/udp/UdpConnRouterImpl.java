package com.qq.qqbuy.common.client.router.udp;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.protobuf.ByteString;
import com.qq.qqbuy.common.client.http.HttpConnJdkImpl;
import com.qq.qqbuy.common.client.http.ReqHead;
import com.qq.qqbuy.common.client.router.ClientRouterException;
import com.qq.qqbuy.common.client.router.Router;
import com.qq.qqbuy.common.client.router.udp.UdpIdl.UdpRouterReq;
import com.qq.qqbuy.common.client.router.udp.UdpIdl.UdpRouterResp;
import com.qq.qqbuy.common.client.udp.UdpConn;

public class UdpConnRouterImpl implements UdpConn {
	
	private int soTimeout = 300;			//等待响应时长,ms
	private int recvBufSize = 2*1024*1024;	//接收响应的缓冲区大小
	private int router = Router.PP_GAMMA;
	
	private static Logger log = LogManager.getLogger();

	public UdpConnRouterImpl(int router) {
		this.router = router;
	}
	
	public UdpConnRouterImpl(int soTimeout, int recvBufSize, int router) {
		this.soTimeout = soTimeout;
		this.recvBufSize = recvBufSize;
		this.router = router;
	}

	@Override
	public void send(String host, int port, byte[] packet) throws IOException {
		String dest = host + ":" + port;
		byte[] pb = UdpRouterReq.newBuilder().setAuth(Router.SERVER_AUTH)
			.setDest(dest).setIsReceive(false)
			.setPacket(ByteString.copyFrom(packet))
			.setRecvBufSize(recvBufSize).setSoTimeout(soTimeout)
			.build().toByteArray();
		
		ReqHead head = new ReqHead().setContentType("application/octet-stream");
		String url = Router.chooseHost(router) + Router.SERVER_URL + "?type=udp";
		new HttpConnJdkImpl().post(url, head, new ByteArrayInputStream(pb));
	}

	@Override
	public DatagramPacket sendAndReceive(String host, int port, byte[] packet) throws IOException {
		String dest = host + ":" + port;
		byte[] pb = UdpRouterReq.newBuilder().setAuth(Router.SERVER_AUTH)
				.setDest(dest).setIsReceive(true)
				.setPacket(ByteString.copyFrom(packet))
				.setRecvBufSize(recvBufSize).setSoTimeout(soTimeout)
				.build().toByteArray();
		
		ReqHead head = new ReqHead().setContentType("application/octet-stream");
		String url = Router.chooseHost(router) + Router.SERVER_URL + "?type=udp";
		byte[] ret = new HttpConnJdkImpl().post(url, head, new ByteArrayInputStream(pb)).getResponse();
		
		UdpRouterResp resp = UdpRouterResp.parseFrom(ret);
		if("".equals(resp.getErr()))	{
			log.info("udp:" + resp.getIp());
			byte[] bs = resp.getResponse().toByteArray(); 
			return new DatagramPacket(bs, bs.length);
		} else	{
			throw new ClientRouterException(resp.getErr());
		}
	}

}
