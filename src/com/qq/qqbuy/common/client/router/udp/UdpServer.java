package com.qq.qqbuy.common.client.router.udp;

import java.net.DatagramPacket;

import com.qq.qqbuy.common.client.router.ClientRouterException;
import com.qq.qqbuy.common.client.router.Router;
import com.qq.qqbuy.common.client.router.udp.UdpIdl.UdpRouterReq;
import com.qq.qqbuy.common.client.udp.UdpConn;
import com.qq.qqbuy.common.client.udp.UdpConnDefaultImpl;

public class UdpServer {
	
	public byte[] handle(byte[] in)	{
		try {
			UdpRouterReq req = UdpRouterReq.parseFrom(in);
			if(checkAuth(req.getAuth()))	return null;
			UdpConn conn = new UdpConnDefaultImpl();
			
			String[] arr = req.getDest().split(":");
			if(arr.length != 2)	
				throw new IllegalArgumentException("不合法的IPPORT:" + req.getDest());
			int port = Integer.parseInt(arr[1]);
			
			if(req.getIsReceive())	{
				DatagramPacket pac = conn.sendAndReceive(arr[0], port, req.getPacket().toByteArray());
				return pac.getData();
			} else	{
				conn.send(arr[0], port, req.getPacket().toByteArray());
				return null;
			}
		} catch (Exception e) {
			throw new ClientRouterException(e);
		}
	}
	
	private boolean checkAuth(String auth)	{
		return !Router.SERVER_AUTH.equals(auth);
	}

}
