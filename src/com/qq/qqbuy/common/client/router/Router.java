package com.qq.qqbuy.common.client.router;

public class Router {
	
	public static final int 
		NONE = 1, 
		PP_GAMMA = 2, PP_IDC = 3,
		JD_GAMMA = 4, JD_IDC = 5,	
		LOCAL_TEST = 6;
	
	public static final String 
			SERVER_URL = "/api/sometest/nmifbergvojvckyukhgjjtytjlkjgrgr.xhtml",
			SERVER_AUTH = "AAAAAAAAAAAAAAKMMMMMMMMMMMMMMMMMMMMMMMEEEEEEEEEEEEEEEEE";
	
	public static String chooseHost(int router)	{
		switch(router)	{
			case Router.PP_GAMMA:
				return "http://test.pp.gamma.xpay.buy.qq.com";
			case Router.PP_IDC:
				return "http://app.paipai.com";
			case Router.JD_GAMMA:
				return "http://dev.pp.xpay.paipai.com";
			case Router.JD_IDC:	
				return "http://app.paipai.com";		//TODO
			case Router.LOCAL_TEST:	
				return "http://localhost:8080/paipaiapp";
			default:
				throw new UnsupportedOperationException();
		}
	}

}
