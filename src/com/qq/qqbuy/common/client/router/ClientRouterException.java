package com.qq.qqbuy.common.client.router;

public class ClientRouterException extends RuntimeException{

	public ClientRouterException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ClientRouterException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ClientRouterException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ClientRouterException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
}
