package com.qq.qqbuy.common.client.router.http;

import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.qq.qqbuy.common.client.http.HttpConn;
import com.qq.qqbuy.common.client.http.HttpResp;
import com.qq.qqbuy.common.client.http.ReqHead;

public class HttpConnRouterImpl implements HttpConn{
	
	private static Logger log = LogManager.getLogger();
	private int router;
	private Proxy proxy = Proxy.NO_PROXY;
	
	public HttpConnRouterImpl(int router)	{
		this.router = router;
	}
	
	@Override
	public void setProxy(Proxy proxy) {
		this.proxy = proxy;
	}

	public int getRouter() {
		return router;
	}

	public void setRouter(int router) {
		this.router = router;
	}

	@Override
	public HttpResp connect(String method, String url, ReqHead head,
			InputStream body) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}


}
