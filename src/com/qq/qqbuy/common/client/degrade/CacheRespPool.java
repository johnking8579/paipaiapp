package com.qq.qqbuy.common.client.degrade;

import java.util.HashMap;
import java.util.Map;

public class CacheRespPool {
	
	/**
	 * http/udp/tcp用
	 */
	private static Map<String, byte[]> respCache = new HashMap<String, byte[]>(200);
	
	/**
	 * ao专用
	 */
	private static Map<String, Object> aoCache = new HashMap<String, Object>();
	
	public static boolean contain(String serviceId)	{
		return respCache.containsKey(serviceId);
	}
	
	public static boolean notContain(String serviceId)	{
		return !respCache.containsKey(serviceId);
	}
	
	public static byte[] get(String serviceId)	{
		return respCache.get(serviceId);
	}
	
	public static void set(String serviceId, byte[] resp)	{
		respCache.put(serviceId, resp);
	}

}
