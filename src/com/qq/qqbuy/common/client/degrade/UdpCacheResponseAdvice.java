package com.qq.qqbuy.common.client.degrade;

import java.net.DatagramPacket;

import org.aopalliance.intercept.MethodInvocation;

import com.qq.qqbuy.common.client.AbstractUdpConnAdvice;

public class UdpCacheResponseAdvice extends AbstractUdpConnAdvice{
	
	private String serviceName;
	
	public UdpCacheResponseAdvice(String serviceName) {
		this.serviceName = serviceName;
	}

	@Override
	public Object aroundSend(MethodInvocation invocation, String host, int port) throws Throwable {
		return invocation.proceed();
	}

	@Override
	public Object aroundSendAndReceive(MethodInvocation invocation, String host, int port)
			throws Throwable {
		Object resp = invocation.proceed();
		if(resp != null)	{
			DatagramPacket pack = (DatagramPacket)resp;
			String serviceId = genKey4CacheResp(host, port, serviceName);
			if(CacheRespPool.notContain(serviceId))	{
				CacheRespPool.set(serviceId, pack.getData());
			}
		}
		return resp;
	}

}
