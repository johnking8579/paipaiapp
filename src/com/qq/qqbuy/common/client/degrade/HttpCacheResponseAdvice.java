package com.qq.qqbuy.common.client.degrade;

import org.aopalliance.intercept.MethodInvocation;

import com.qq.qqbuy.common.client.AbstractHttpConnAdvice;
import com.qq.qqbuy.common.client.http.HttpResp;
import com.qq.qqbuy.common.util.Util;

public class HttpCacheResponseAdvice extends AbstractHttpConnAdvice{
	
	@Override
	public Object aroundConnect(MethodInvocation invocation, String url) throws Throwable {
		HttpResp resp = (HttpResp)invocation.proceed();
		
		if(resp.getResponseCode()>=200 && resp.getResponseCode() <400 
				&& resp.getResponse() != null && resp.getResponse().length > 0)	{
			String uri = Util.truncateParam(url);
			if(!CacheRespPool.contain(uri))	{
				CacheRespPool.set(uri, resp.getResponse());
			}
		}
		return resp;
	}

}
