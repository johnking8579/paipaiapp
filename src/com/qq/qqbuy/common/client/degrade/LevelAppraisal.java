package com.qq.qqbuy.common.client.degrade;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.qq.qqbuy.common.util.Util;

public class LevelAppraisal {
	private static Logger log = LogManager.getLogger();
	private static Map<String,Object> fromXml = new HashMap<String,Object>();
	static	{
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("res/degrade_service.xml");
		if(is == null)	log.warn("没有找到'降级服务'配置文件res/degrade_service.xml,所有服务将正常使用,不会降级");
		try {
			List<Element> services = new SAXReader().read(is).getRootElement().elements("service");
			for(Element service : services)	{
				String serviceId = service.attributeValue("id").toLowerCase();
				List<Element> levels = service.elements("level");
				if(levels.isEmpty())	{
					fromXml.put(serviceId, Integer.parseInt(service.attributeValue("level")));
				} else	{
					Map<String,Integer> actionLevel = new HashMap<String,Integer>();
					for(Element level : levels)	{
						actionLevel.put(level.attributeValue("action"), Integer.parseInt(level.getText()));
					}
					if(!actionLevel.isEmpty())	
						fromXml.put(serviceId, actionLevel);
				}
			}
		} catch (DocumentException e) {
			throw new RuntimeException(e);
		} finally	{
			Util.closeStream(is);
		}
	}
	
	/**
	 * 是否降级使用该服务
	 * @param serviceId
	 * @return
	 */
	public boolean shouldDegrade(String serviceId, StackTraceElement[] trace)	{
		Object o = fromXml.get(serviceId);
		if(o == null)	{
			return false;
		} else if(o instanceof Integer)	{
			return (Integer)o > 0;		//TODO 大于?的level会被降级
		} else if(o instanceof Map)	{
			for(Entry<String,Integer> e : ((Map<String,Integer>)o).entrySet())	{
				for(StackTraceElement t : trace)	{
					if(e.getKey().equals(t.getClassName() + "." + t.getMethodName()))	{
						return true;
					}
				}
			}
			return false;
		}
		throw new UnsupportedOperationException();
	}

}
