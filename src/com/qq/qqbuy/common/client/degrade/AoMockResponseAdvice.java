package com.qq.qqbuy.common.client.degrade;

import org.aopalliance.intercept.MethodInvocation;

import com.qq.qqbuy.common.client.AbstractWebStubAdvice;

public class AoMockResponseAdvice extends AbstractWebStubAdvice{
	
	private LevelAppraisal level = new LevelAppraisal();

	@Override
	public Object aroundInvoke(MethodInvocation invocation, long cmdId)
			throws Throwable {
		
		if(level.shouldDegrade(genKey4CacheResponse(cmdId), Thread.currentThread().getStackTrace()))	{
			Object response = invocation.getArguments()[1];
			if(response instanceof Mockable)	{
				invocation.getArguments()[1] = ((Mockable)response).generateMockResponse();
			} 
			return 0;	//有mockable接口时返回mockable,没有时直接按原样返回
		} else	{
			return invocation.proceed();
		}
	}

}
