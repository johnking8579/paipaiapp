package com.qq.qqbuy.common.client;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.netframework.kernal.NetMessage;

public abstract class AbstractWebStubAdvice implements MethodInterceptor{

	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		if("invoke".equals(invocation.getMethod().getName()) && invocation.getArguments().length == 2)	{	//拦截AsynWebStub.invoke(Object o1, Object o2)
			long cmdId = this.getCmdId(invocation.getArguments()[0]);
			return aroundInvoke(invocation, cmdId);
		} else	{
			return invocation.proceed();
		}
	}
	
	public abstract Object aroundInvoke(MethodInvocation invocation, long cmdId) throws Throwable;
	
	/**
	 * 在AsycWebStub中也能得到cmdId, 但invoke前cmd=null, invoke后=resp.cmdId. invoke后超时未返回,为req.cmdId. 由于不确定,改用request.getCmdId()
	 * @param req
	 * @return
	 */
	private long getCmdId(Object req)	{
		if(req instanceof IServiceObject)	{
			return ((IServiceObject)req).getCmdId();
		} else if(req instanceof NetMessage)	{
			return ((NetMessage)req).getCmdId();
		} else	{
			return 0;
		}
	}
	
	protected String genKey4CacheResponse(long cmdId)	{
		return String.format("ao://0x%sL", Long.toHexString(cmdId)).toLowerCase();
	}
	
	protected String genKey4Ump(long cmdId, String requestName)	{
		return String.format("ao://0x%s(%s)", Long.toHexString(cmdId), requestName);
	}
}
