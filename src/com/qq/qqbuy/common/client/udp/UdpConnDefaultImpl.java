package com.qq.qqbuy.common.client.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UdpConnDefaultImpl implements UdpConn{
	
	private int soTimeout = 500;			//等待响应时长,ms
	private int recvBufSize = 2*1024*1024;	//接收响应的缓冲区大小
	
	public UdpConnDefaultImpl(int soTimeout, int recvBufSize) {
		this.soTimeout = soTimeout;
		this.recvBufSize = recvBufSize;
	}


	public UdpConnDefaultImpl() {
	}


	/**
	 * 只发送不接收. 不使用代理类
	 * @param ipport
	 * @param cont
	 * @throws IOException
	 */
	public void send(String host, int port, byte[] packet) throws IOException	{
		connect(host, port, packet, false);
	}
	
	
	/**
	 * 发送并接收, 使用代理类
	 * @param ipport
	 * @param cont
	 * @param packetLogParser 如何记录request/response
	 * @return
	 * @throws IOException
	 */
	public DatagramPacket sendAndReceive(String host, int port, byte[] cont) throws IOException	{
		return connect(host, port, cont, true);
	}
	
	/**
	 * 
	 * @param hostPort 127.0.0.1:80 or www.domain.com:90
	 * @param cont
	 * @param isReceive
	 * @param useCglibProxy 是否使用CGLIB生成的DatagramSocket代理类
	 * @return
	 * @throws IOException
	 */
	private DatagramPacket connect(String host, int port, byte[] cont, boolean isReceive) 
								throws IOException	{
		DatagramSocket socket = null;
		try	{
			socket = new DatagramSocket();
	        socket.setSoTimeout(soTimeout);
	        DatagramPacket pkt = new DatagramPacket(cont, cont.length, InetAddress.getByName(host), port);
	        socket.send(pkt);
	        if(isReceive)	{
				byte[] recvBuf = new byte[recvBufSize];		
				DatagramPacket recvPacket = new DatagramPacket(recvBuf, recvBuf.length);
				socket.receive(recvPacket);
				
				if(recvPacket.getLength() < recvBuf.length)	{
					byte[] truncate = new byte[recvPacket.getLength()];		//recvPacket.getLength()=UDP协议头中的包长度
					System.arraycopy(recvBuf, 0, truncate, 0, truncate.length);
					recvPacket.setData(truncate);
					return recvPacket;
				} else	{
					return recvPacket;
				}
	        } else	{
	        	return null;
	        }
		} finally	{
			try {
				socket.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
