package com.qq.qqbuy.common.client.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.util.Random;

import org.springframework.aop.framework.ProxyFactory;

import com.paipai.component.configagent.ConfigHelper;
import com.qq.qqbuy.common.Tuple2;
import com.qq.qqbuy.common.client.degrade.UdpCacheResponseAdvice;
import com.qq.qqbuy.common.client.degrade.UdpMockResponseAdvice;
import com.qq.qqbuy.common.client.log.PacketLogParser;
import com.qq.qqbuy.common.client.log.UdpLogAdvice;
import com.qq.qqbuy.common.client.router.Router;
import com.qq.qqbuy.common.client.router.udp.UdpConnRouterImpl;
import com.qq.qqbuy.common.client.ump.UdpUmpAdvice;

/**
 * UDP简单发送类.每次发送完后会关闭端口，不支持一个端口多次发送。
 * 1.日志记录
 * 2.配置中心命令字解析
 * 3.降级
 * @author JingYing 2014-10-13
 *
 */
public class UdpClient {
	
	private UdpConn impl;
	
	public UdpClient()	{
		impl = new UdpConnDefaultImpl();
	}
	
	public UdpClient(int soTimeout, int recvBufSize, int router)	{
		this();
		if(router == Router.NONE)	{
			impl = new UdpConnDefaultImpl(soTimeout, recvBufSize);	//TODO 优化
		} else	{
			impl = new UdpConnRouterImpl(soTimeout, recvBufSize, router);
		}
	}
	
	public void send(String dest, byte[] packet) throws IOException	{
		send(dest, packet, null);
	}
	
	/**
	 * 只发送不接收.
	 * @param dest 配置中心的服务名或者x.xx.xx.xx:1200这种格式的ip:port
	 * @param cont
	 * @throws IOException
	 */
	public void send(String dest, byte[] packet, PacketLogParser packetLogParser) throws IOException	{
		ProxyFactory factory = new ProxyFactory(impl);
		factory.setInterfaces(new Class[]{UdpConn.class});
		factory.setProxyTargetClass(false);
		if(packetLogParser != null)	{
			factory.addAdvice(new UdpLogAdvice(packetLogParser));
		}
		impl = (UdpConn)factory.getProxy();
		
		Tuple2<String,Integer> hostport = toHostPort(dest);
		impl.send(hostport.getA(), hostport.getB(), packet);
	}
	
	/**
	 * 发送并接收, 使用代理类
	 * @param dest 配置中心的服务名或者x.xx.xx.xx:1200这种格式的ip:port
	 * @param packet
	 * @param packetLogParser 如何记录request/response
	 * @return
	 * @throws IOException
	 */
	public DatagramPacket sendAndReceive(String dest, byte[] packet) throws IOException	{
		return sendAndReceive(dest, packet, null, null);
	}
	
	public DatagramPacket sendAndReceive(String dest, byte[] packet, PacketLogParser packetLogParser, byte[] defaultResp) throws IOException	{
		ProxyFactory factory = new ProxyFactory(impl);
		factory.setInterfaces(new Class[]{UdpConn.class});
		factory.setProxyTargetClass(false);
		String serviceName = packetLogParser == null ? null : packetLogParser.getServiceName();
		if(com.qq.qqbuy.common.ConfigHelper.isSysDegrade())	{
			factory.addAdvice(new UdpMockResponseAdvice(serviceName, defaultResp));
			factory.addAdvice(new UdpCacheResponseAdvice(serviceName));
		}
		if(packetLogParser != null)	{
			factory.addAdvice(new UdpLogAdvice(packetLogParser));
		}
		factory.addAdvice(new UdpUmpAdvice(serviceName));
		impl = (UdpConn)factory.getProxy();
		Tuple2<String,Integer> hostport = toHostPort(dest);
		return impl.sendAndReceive(hostport.getA(), hostport.getB(), packet);
	}
	
	/**
	 * 如果dest为配置中心服务名, 则转为ip:port
	 * @param dest
	 * @return
	 */
	private Tuple2<String,Integer> toHostPort(String dest)	{	//只支持域名,ipV4,配置中心命令字. 不支持ipV6
		if(dest.contains(":"))	{
			String[] arr = dest.split(":");
			if(arr.length != 2)	
				throw new IllegalArgumentException("不合法的IPPORT:" + dest);
			return new Tuple2<String, Integer>(arr[0], Integer.parseInt(arr[1]));
		} else	{
			InetSocketAddress addr = ConfigHelper.getSvcAddressBySet(8080, dest, 0, new Random().nextInt(100));
			return new Tuple2<String, Integer>(addr.getHostName(), addr.getPort());
		}
	}
}
