package com.qq.qqbuy.common.client.udp;

import java.io.IOException;
import java.net.DatagramPacket;

public interface UdpConn {
	
	void send(String host, int port, byte[] packet) throws IOException;
	
	DatagramPacket sendAndReceive(String host, int port, byte[] packet) throws IOException;

}
