package com.qq.qqbuy.common.client.cache;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class CacheKey {
	
	/**
	 * redis的键前缀, 所有操作redis的key前缀应当在这里注册, 避免误覆盖别人存放的数据.
	 * 以双斜线结尾
	 */
	public static final String 
		店铺新品数量 = "shop/newItemCount//",
		店铺组合图片 = "shop/joinpics//",
		手机专享商品 = "item/appexclusive//",
		闪购= "shouye/shangou//",
		秒杀_聚精品 = "shouye/miaosha//",
		拍便宜 = "shouye/paipianyi//",
		同步标记 = "synchronized//",
		订单频率 = "deal/submitOrder//";
		
	
	
	
	/**
	 * 检查redisKey是否合法
	 * @param key
	 */
	public static void checkKey(String key)	{
		for(String s : allConstants)	{
			if(key.startsWith(s))	{
				return;
			}
		}
		throw new UnRegCacheKeyException(key);
	}
	
	private static List<String> allConstants = new ArrayList<String>();
	static	{
		for(Field f : CacheKey.class.getFields())	{
			try	{
				allConstants.add((String)f.get(new CacheKey()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
