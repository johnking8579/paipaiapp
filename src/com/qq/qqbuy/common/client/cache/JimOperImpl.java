package com.qq.qqbuy.common.client.cache;

import java.util.concurrent.TimeUnit;

import com.jd.jim.cli.Cluster;

public class JimOperImpl implements CacheOper {
	
	private Cluster jimClient;
	
	public JimOperImpl(Cluster jimClient)	{
		this.jimClient = jimClient;
	}

	@Override
	public Long del(byte[] key)  {
		return jimClient.del(key);
	}

	@Override
	public byte[] get(byte[] key) {
		return jimClient.get(key);
	}
	
	@Override
	public void setex(byte[] key, int seconds, byte[] value) {
		jimClient.setEx(key, value, seconds, TimeUnit.SECONDS);
	}

	@Override
	public long setnx(byte[] key, byte[] value) {
		boolean b = jimClient.setNX(key, value);
		return b ? 1 : 0;
	}

	@Override
	public Long ttl(byte[] key) {
		return jimClient.ttl(key);
	}
}
