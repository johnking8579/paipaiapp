package com.qq.qqbuy.common.client.ao;

import com.paipai.component.c2cplatform.impl.AsynWebStub;

public interface AoConn {
	
	int connect(AsynWebStub stub, Object req, Object resp);

}
