package com.qq.qqbuy.common.client.ao;

import org.springframework.aop.framework.ProxyFactory;

import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.qq.qqbuy.common.ConfigHelper;
import com.qq.qqbuy.common.client.degrade.AoMockResponseAdvice;
import com.qq.qqbuy.common.client.log.AoLogAdvice;
import com.qq.qqbuy.common.client.router.Router;
import com.qq.qqbuy.common.client.router.ao.AoConnRouterImpl;
import com.qq.qqbuy.common.client.ump.AoUmpAdvice;

public class AoClient implements AoConn{
	
	private AoConn impl;
	
	public AoClient()	{
		impl = new AoConnDefaultImpl();
	}
	
	public AoClient(int router) {
		if(router == Router.NONE)	{
			impl = new AoConnDefaultImpl();
		} else	{
			impl = new AoConnRouterImpl(router);
		}
	}

	@Override
	public int connect(AsynWebStub stub, Object req, Object resp) {
		return impl.connect(stub, req, resp);
	}
	
	public static AsynWebStub newProxyWebStub()	{
		ProxyFactory factory = new ProxyFactory(new AsynWebStub());
//		factory.setOptimize(true);
		factory.setProxyTargetClass(true);
		if(ConfigHelper.isSysDegrade())	{
			factory.addAdvice(new AoMockResponseAdvice());
		}
		factory.addAdvice(new AoLogAdvice());
		factory.addAdvice(new AoUmpAdvice());
		return (AsynWebStub)factory.getProxy();
	}
}
