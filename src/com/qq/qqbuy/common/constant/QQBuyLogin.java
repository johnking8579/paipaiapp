package com.qq.qqbuy.common.constant;

public class QQBuyLogin
{
    public final static String XSSSOURCE = "&#61";
    public final static String XSSDEST = "=";
    public final static String LOGIN_PARAM_UIN = "uin";
    public final static String LOGIN_PARAM_LSKEY = "lskey";
    public final static String LOGIN_PARAM_SSKEY = "sskey";
    public final static String LOGIN_PARAM_SID = "sid";
    
    
    /** add by wendyhu 下面几个字段是从cookie中取得的。新的sid协议需要以下字段 **/
    public final static String SID_STATSID_COOKIE_KEY = "ng_st_sid_v2";
    public final static String SID_SIDFROMCOOKIE_COOKIE_KEY = "ng_c_sid";
    public final static String SID_SIDFROMQCOOKIE_COOKIE_KEY = "sid";
    
    /**
     * 没有登陆验证类型
     */
    public final static int NO_LOGIN_TYPE = 0;
    
    /**
     * ptlogin验证
     */
    public final static int PT_LOGIN_TYPE = 1;

    /**
     * sid验证
     */
    public final static int SID_LOGIN_TYPE = 2;

    /**
     * 微信验证
     */
    public final static int WEIXIN_LOGIN_TYPE = 3;
    
    /**
     * 生成微信的sid前缀
     */
    public  static final String WEIXIN_PRE = "_*";
    
    
    public static final int WEIXIN_PRE_LENGTH = WEIXIN_PRE.length();
    
    /**
     * 财付通的验证类型
     */
    public final static int CFT_LOGIN_TYPE = 4;
    
    /**
     * 生成微信的sid前缀
     */
    public  static final String CFT_PRE = "__";
    
    
    public static final int CFT_PRE_LENGTH = CFT_PRE.length();
    
    /**
     * 
     * @Title: reXss 
     * 
     * @Description: 还原被xss过渡的东西 
     * @param sid
     * @return    设定文件 
     * @return String    返回类型 
     * @throws
     */
    public static String reXss(String sid)
    {
        return sid.replace(XSSSOURCE, XSSDEST);
    }
    
    /**
     * 
     * @Title: isWeixinSid 
     * 
     * @Description: 判断是否是weixin的sid
     *  
     * @param weixinSid
     * @return    设定文件 
     * @return boolean    返回类型 
     * @throws
     */
    public static boolean isWeixinSid(String weixinSid)
    {
        return (weixinSid != null && weixinSid.length() > 10  && weixinSid.length() < 100 && weixinSid.startsWith(QQBuyLogin.WEIXIN_PRE));
    }
    
    /**
     * 
     * @Title: isWeixinSid 
     * 
     * @Description: 判断是否是财付通的sid
     *  
     * @param cftSid
     * @return    设定文件 
     * @return boolean    返回类型 
     * @throws
     */
    public static boolean isCftSid(String cftSid)
    {
        return (cftSid != null && cftSid.length() > 10 && cftSid.length() < 100 && cftSid.startsWith(QQBuyLogin.CFT_PRE));
    }
}
