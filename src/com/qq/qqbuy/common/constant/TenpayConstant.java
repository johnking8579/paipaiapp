package com.qq.qqbuy.common.constant;

public class TenpayConstant
{
    /**
     * 财付通返回的参数
     */
    public final static String PARAM_CHARSET = "charset"; //字符集1 UTF-8, 2 GB2312
    
    public final static String PARAM_SELLER_SPID = "bargainor_id";//卖方账号（商户spid）
    
    public final static String CHARSET_UTF_8 = "UTF-8"; //字符集1 UTF-8, 2 GB2312
    
    public final static String CHARSET_GB2312 = "GB2312"; //字符集1 UTF-8, 2 GB2312
    
    /**
     * 定义的来源信息
     */
    public final static String SOURCE_FOR_ANDROID = "qgo_android";
    public final static String SOURCE_FOR_IOS = "qgo_ios";
    public final static String SOURCE_FOR_H5 = "qgo_h5";
    public final static String SOURCE_FOR_WAP = "qgo_wap";
    
    
//    /**
//     * 网购供货商业Spid对应的加密key
//     */
//    public final static Map<String,String> spidMd5 = new HashMap<String,String>();
//    
////    /**
////     * 网购供货商bid对应spid
////     */
////    public final static Map<String,String> bid2Spid = new HashMap<String,String>();
//    static{
//        spidMd5.put("1211646001","6658d94a9e9301a51c55295a268d4383");
//        spidMd5.put("1211646701","867e880a49ff9c012f74d760b537c1c2");
//        spidMd5.put("1211646101","a0aeb816733a63e1ec606278614164f0");
//        spidMd5.put("1211646301","c2c190d392e9ea2eaf8f9daeab9633ad");
//        spidMd5.put("1211646501","bf414c12034a44b5e0d7002fbc6cb971");
//        spidMd5.put("1211918501","f2479904871da718a5854eb03deabb55");
//        spidMd5.put("1211918601","7d461c7df707743e05e7516e2b9aa5e9");
//    };
    
    /**
     * 
     * @Title: getCharset 
     * @Description: 获取字符集 
     * @param charset
     * @return  
     *        String    返回类型 
     *
     * @throws
     */
    public static String getCharset(long charset)
    {
        String ret = null;
        if(charset == 1)
            ret = CHARSET_UTF_8;
        else if(charset == 2)
            ret = CHARSET_GB2312;
        else
            ;
        return ret;
        
    }
    
}
