package com.qq.qqbuy.common.constant;

import java.util.HashSet;
import java.util.Set;

/**
 * 业务错误类
 * 
 * @author matrixshi
 * 
 */
public class BusinessErrConstants {

	public static final long ERR_APP_USER_UNLOGIN = 13; // 用户未登陆
	public static final long ERR_NEED_STOCK_INFO = 359; // 未选择商品的库存属性
	public static final long ERR_APP_BID_RESULT_TOOMANY_NOPAY = 201; // 太多未结束的支付订单
	public static final long ERR_APP_CART_ITEM_INVALID = 103; // 购物车中的商品无效，（无库存时也是）
	public static final long ERR_DAO_COMMODITY_NOFOUND = 466; // 商品不存在
	public static final long ERR_DB_COMM_DUP_DEAL_SEQ = 519; // 重复的seq

	public static final long ERR_APP_HAVE_CREATE_DEAL = 446; // 重复生成订单，序列号已存在
	public static final long ERR_APP_PARAM_INVALID = 14; // 参数错误，不加入业务错误拦截
	public static final long ERR_APP_BID_RESULT_TIMEOUT = 196; // 拍卖已经结束
	public static final long ERR_NOT_FOUND_STOCK = 358; // 未选择库存属性，也即没库存
	public static final long ERR_APP_UIN_NOT_AUTH = 2; // 用户权限检查失败
	public static final long ERR_APP_BIN_OVER_LIMIT = 33; // 一口价商品购买超过卖家对单个买家的最大限制

	public static final long ERR_DB_ADDR_NOT_EXIST = 258; // 用户收货地址不存在
	public static final long ERR_APP_BID_RESULT_BUYEREQUSELLER = 195; // 买家卖家相同
	public static final long ERR_NOT_HAVE_RECVADDR = 442; // 买家未填写收货地址
	public static final long ERR_DB_COMM_INVALID_BID_DURATION = 516; // 该商品已经过了有效期，不能竞拍
	public static final long ERR_APP_SYSTEM = 6; // 系统繁忙,操作失败
	public static final long ERR_DB_COMM_INVALID_BID_COMMODITY_NUM = 517; // 非法的商品竞拍数量，不加入业务错误拦截

	public static final long ERR_APP_PRICE_FAIL = 5; // 出价失败，不加入业务错误拦截
	public static final long ERR_APP_TOOMUCH_OPERATION = 432; // 操作过于频繁
	public static final long ERR_APP_REDUCE_ITEM_NUM = 444; // 扣减商品数失败
	public static final long ERR_REDPACKET_ILLEGAL_STATE = 1794; // 红包状态错误
	public static final long ERR_APP_PUNISH_BUY = 112; // 受处罚:禁止购买
	public static final long ERR_APP_USER_FREEZED = 23; // 用户被冻结

	public static final long ERR_APP_UIN_NOT_AUTH_TWO = 3;// 用户权限检查失败
	public static final long ERR_APP_USER_OPERATOR_OVER_LIMIT = 40; // 用户操作太过频繁
	public static final long ERR_APP_USER_FORBIDDEN = 24; // 用户被禁止
	public static final long ERR_APP_ORDER_TRADE_FEE_EXCEED = 108; // 订单金额太大,超出范围
	public static final long ERR_COD_FETCH_AMOUNT_OVERLIMIT = 2082;// 代收金额不在区间内
	public static final long ERR_CMDY_FORBIDDEN_SELL = 490; // 商品禁止销售

	public static final long ERR_APP_BID_CANNOT_CHECK_REDPAG_ERR = 206; // 用户选择的红包信息不合法
	public static final long ERR_ITEM_BUY_SOCK_NUM_NO_ENOUGH = 696; // 下单扣商品时，库存数不够
	public static final long ERR_REDPACKET_EXPIRED = 1793;// 红包过期
	public static final long ERR_APP_CREATE_DEAL = 445; // 生成订单失败，不加入业务错误拦截
	public static final long ERR_ITEM_MUST_SET_STOCK_ATTR = 735; // 商品有库存,下单时没有库存属性
	public static final long ERR_REDPACKET_NOT_FOUND = 1798; // 红包不存在

	public static final long ERR_APP_BID_CANNOT_USE_REDPAG_ERR = 204; // 系统内部订单附属信息冲突，不能使用红包
	public static final long ERR_APP_BID_ITEMNUM_IS_ZERO = 192;// 商品数量为空
	public static final long ERR_APP_ITEM_SELLTYPE_FAILED = 419; // 购买类型错误，不是一口价商品，不加入业务错误拦截

	public static final Set<Long> ERROR_CODE = new HashSet<Long>();
	static {
		ERROR_CODE.add(ERR_APP_USER_UNLOGIN);
		ERROR_CODE.add(ERR_NEED_STOCK_INFO);
		ERROR_CODE.add(ERR_APP_BID_RESULT_TOOMANY_NOPAY);
		ERROR_CODE.add(ERR_APP_CART_ITEM_INVALID);
		ERROR_CODE.add(ERR_DAO_COMMODITY_NOFOUND);
		ERROR_CODE.add(ERR_DB_COMM_DUP_DEAL_SEQ);

		ERROR_CODE.add(ERR_APP_HAVE_CREATE_DEAL);
		ERROR_CODE.add(ERR_APP_BID_RESULT_TIMEOUT);
		ERROR_CODE.add(ERR_NOT_FOUND_STOCK);
		ERROR_CODE.add(ERR_APP_UIN_NOT_AUTH);
		ERROR_CODE.add(ERR_APP_BIN_OVER_LIMIT);

		ERROR_CODE.add(ERR_DB_ADDR_NOT_EXIST);
		ERROR_CODE.add(ERR_APP_BID_RESULT_BUYEREQUSELLER);
		ERROR_CODE.add(ERR_NOT_HAVE_RECVADDR);
		ERROR_CODE.add(ERR_DB_COMM_INVALID_BID_DURATION);
		ERROR_CODE.add(ERR_APP_SYSTEM);

		ERROR_CODE.add(ERR_APP_TOOMUCH_OPERATION);
		ERROR_CODE.add(ERR_APP_REDUCE_ITEM_NUM);
		ERROR_CODE.add(ERR_REDPACKET_ILLEGAL_STATE);
		ERROR_CODE.add(ERR_APP_PUNISH_BUY);
		ERROR_CODE.add(ERR_APP_USER_FREEZED);

		ERROR_CODE.add(ERR_APP_UIN_NOT_AUTH_TWO);
		ERROR_CODE.add(ERR_APP_USER_OPERATOR_OVER_LIMIT);
		ERROR_CODE.add(ERR_APP_USER_FORBIDDEN);
		ERROR_CODE.add(ERR_APP_ORDER_TRADE_FEE_EXCEED);
		ERROR_CODE.add(ERR_COD_FETCH_AMOUNT_OVERLIMIT);
		ERROR_CODE.add(ERR_CMDY_FORBIDDEN_SELL);

		ERROR_CODE.add(ERR_APP_BID_CANNOT_CHECK_REDPAG_ERR);
		ERROR_CODE.add(ERR_ITEM_BUY_SOCK_NUM_NO_ENOUGH);
		ERROR_CODE.add(ERR_REDPACKET_EXPIRED);
		ERROR_CODE.add(ERR_ITEM_MUST_SET_STOCK_ATTR);
		ERROR_CODE.add(ERR_REDPACKET_NOT_FOUND);

		ERROR_CODE.add(ERR_APP_BID_CANNOT_USE_REDPAG_ERR);
		ERROR_CODE.add(ERR_APP_BID_ITEMNUM_IS_ZERO);
	}

}
