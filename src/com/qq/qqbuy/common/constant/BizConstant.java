package com.qq.qqbuy.common.constant;

/**
 * 存放工程中使用到的常量定义，一般不提供给外部IDL调用者使用
 * 
 * @author winsonwu
 * 
 */
public interface BizConstant {

	/************************************************* OpenAPI调用参数 **************************************************************/
	public static final String OPENAPI_CHARSET = "UTF-8";
	public static final int OPENAPI_HTTP_CONNECT_TIMEOUT = 3000;
	public static final int OPENAPI_HTTP_READ_TIMEOUT = 3000;

	/**
	 * CMS版本
	 */
	public static int CMS_TYPE_WAP2 = 1;// WAP2
	public static int CMS_TYPE_TOUCH = 2;// TOUCH
	public static int CMS_TYPE_O2O = 3;// O2O
	public static int CMS_TYPE_IOS = 4;// IOS
    public static int CMS_TYPE_APP = 5;// IOS

	/**
	 * CMS环境标识
	 */
	public static int ENV_TYPE_IDC = 1;// 正式
	public static int ENV_TYPE_MIX = -1;// 预览

	/**
	 * 日志各版本编号
	 */
	public static final int ACESS_TYPE_ANDRIOD = 1;
	public static final int ACESS_TYPE_IOS = 3;
	public static final int ACESS_TYPE_WAP2 = 2;
	public static final int ACESS_TYPE_TOUCH = 4;
	public static final int ACESS_TYPE_WAP1 = 5;
	public static final int ACESS_TYPE_PC = 6;

	/**
	 * 模版前缀
	 */
	public static final String PREDIX_TPLT = "/tplt";// 模版前缀
	public static final String PREDIX_ADPOS = "/adpos";// 广告位前缀

	/**
	 * 模版资源名称分隔符
	 */
	public static final String SPLITE_CHAR = "~";
	public static final String DEFAULT_CHARSET = "UTF-8";
	public static final String GBK_CHARSET = "GBK";

	public static final int DEFAULT_AREA_ID = 440000;

	public static String wimgPath = "http://3gimg.qq.com/mobilelife/paipai/w/s/";// wap站静态图片路径
	public static String woImgPath = "http://3gimg.qq.com/mobilelife/paipai/w/o/";// wap站运营图片路径
	public static String ppBasePath = "http://qgo.3g.qq.com/g/s";// pp根路径

	public static final String ACTION_RESULT = "actionResult";
	
	/**
	 * 购物车校验参数类型
	 */
	public static int ADD_TYPE = 1;//
	public static int REMOVE_TYPE = 2;//
	public static int MODIFY_TYPE = 3;//
	public static int CONFIRM_TYPE = 4;
	public static int MAKEORDER_TYPE = 5;
	/**
	 * 
	 */
	public static int ICFA_FOR_SEARCH_PC_COD = 19920752;
	
	
	/**
	 * 批价场景类型
	 */
	public static int DEAL_PROMOTE_FROM_SHOPPING_CART = 1;			//购物车页面
	public static int DEAL_PROMOTE_FROM_FIRST_ORDER_CONFIRM = 2; 	//初次进入下单确认页面
	public static int DEAL_PROMOTE_FROM_ORDER_CONFIRM = 3;			//下单确认页面
	public static int DEAL_PROMOTE_FROM_ORDER = 4;    				//下单
	
	
	// 订单id起始
	public static int NORMAL_DEAL_ID_START = 100000;
	public static int PROBLEM_DEAL_ID_START = 200000;
	
	
	// 店铺类型属性
	public static int SHOP_B2C_SELLER = 1;	// 网购卖家
}
