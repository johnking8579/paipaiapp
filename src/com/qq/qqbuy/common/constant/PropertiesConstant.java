package com.qq.qqbuy.common.constant;

/**
 * 用于定义main_XXX.properties文件中的key字符串
 * 
 * @author winsonwu
 * 
 */
public interface PropertiesConstant
{
    // 拍拍那边真实会接受打钱的帐号
    public static final String VB2C_GATE_SPID = "vb2c.gateSpId";
    // 用于标记Adroid应用登录的固定值
    public static final String VB2C_LOGIN_BARGAINOR_ID = "vb2c.bargainorId";
    public final static String VB2C_CHECK_MOBILE_SHORTAGE_URL_PREFIX = "vb2c.check.mobile.shortage.url.prefix";
    public final static String VB2C_TENPAY_NOTIFY_URL_PREFIX = "vb2c.tenpay.notify.url.prefix";
    public final static String VB2C_TENPAY_CALLBACK_URL_PREFIX_W = "vb2c.tenpay.callback.url.prefix.w";
    public final static String VB2C_TENPAY_CALLBACK_URL_PREFIX_T = "vb2c.tenpay.callback.url.prefix.t";
    public final static String VB2C_TENPAY_URL_PREFIX = "vb2c.tenpay.url.prefix";
    public final static String VB2C_TENPAY_NOTIFY_RETRY_COUNTS = "vb2c.tenpay.notify.retry.counts";
    public final static String VB2C_GAME_LIST_TENCENT = "vb2c.game.list.tencent"; 
    public final static String VB2C_GAME_LIST_SOHU = "vb2c.game.list.sohu";
    public final static String VB2C_GAME_LIST_SHENGDA = "vb2c.game.list.shengda";
    public final static String VB2C_GAME_LIST_WANGYI = "vb2c.game.list.wangyi";
}
