package com.qq.qqbuy.common.constant;

/**
 * ITIL值定义
 * 
 * @author matrixshi
 * 
 */
public class ItilConstants {

	public static final long ITIL_APP_TENPAY_REQUEST = 850238; // app财付通支付请求
	public static final long ITIL_APP_TENPAY_SUCCESS = 850239; // app财付通支付成功
	public static final long ITIL_APP_TENPAY_FAILED = 850244; // app财付通支付失败

	public static final long ITIL_APP_CART_ORDERCOMFIRM_REQUEST = 850272; // app购物车订单确认请求
	public static final long ITIL_APP_CART_ORDERCOMFIRM_SUCCESS = 850273; // app购物车订单确认成功
	public static final long ITIL_APP_CART_ORDERCOMFIRM_FAILED = 850274; // app购物车订单确认失败
	public static final long ITIL_APP_CART_ORDERCOMFIRM_BUSINESS_FAILED = 850275; // app购物车订单确认业务失败

	public static final long ITIL_APP_CART_MAKEORDER_REQUEST = 850276; // app购物车下单请求
	public static final long ITIL_APP_CART_MAKEORDER_SUCCESS = 850278; // app购物车下单成功
	public static final long ITIL_APP_CART_MAKEORDER_FAILED = 850279; // app购物车下单失败
	public static final long ITIL_APP_CART_MAKEORDER_BUSINESS_FAILED = 850280; // app购物车下单业务失败

	public static final long ITIL_APP_CART_CODCALC_REQUEST = 850405; // app货到付款费用请求
	public static final long ITIL_APP_CART_CODCALC_SUCCESS = 850406; // app货到付款费用成功
	public static final long ITIL_APP_CART_CODCALC_FAILED = 850408; // app货到付款费用失败
	public static final long ITIL_APP_CART_CODCALC_BUSINESS_FAILED = 850407; // app货到付款费用业务失败

	public static final long ITIL_APP_FIX_MAKEORDER_REQUEST = 850484; // app一口价下单请求
	public static final long ITIL_APP_FIX_MAKEORDER_SUCCESS = 850485; // app一口价下单成功
	public static final long ITIL_APP_FIX_MAKEORDER_FAILED = 850487; // app一口价下单失败
	public static final long ITIL_APP_FIX_MAKEORDER_BUSINESS_FAILED = 850486; // app一口价下单业务失败

	public static final long ITIL_APP_HOMEFLOOR_REQUEST = 850822; // APP首页楼层请求数
	public static final long ITIL_APP_HOMEFLOOR_SUCCESS = 850823; // APP首页楼层成功数
	public static final long ITIL_APP_HOMEFLOOR_FAILED = 850824; // APP首页楼层失败数

	public static final long ITIL_APP_HOMEFAVOR_REQUEST = 851361; // APP猜你喜欢请求数
	public static final long ITIL_APP_HOMEFAVOR_SUCCESS = 851362; // APP猜你喜欢成功数
	public static final long ITIL_APP_HOMEFAVOR_FAILED = 851363; // APP猜你喜欢失败数

	public static final long ITIL_APP_HOMECLICK_REQUEST = 851364; // APP点击上报请求数
	public static final long ITIL_APP_HOMECLICK_SUCCESS = 851365; // APP点击上报成功数
	public static final long ITIL_APP_HOMECLICK_FAILED = 851366; // APP点击上报失败数
}
