package com.qq.qqbuy.common.constant;

/**
 * 对应PC侧c2c_define.h文件的定义
 * 必须写好PC侧对应的定义，方便查找
 * @author winsonwu
 * @Created 2013-3-1
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public interface C2CDefineConstant {

	// 运费由谁承担
	// c2c_define.h#TRANSPORT_TYPE
	public static final int TRANSPORT_SELLER_PAY = 1;    //!<卖家承担运费
    public static final int TRANSPORT_BUYER_PAY = 2;     //!<买家承担运费
    public static final int TRANSPORT_NO_NEED_PAY = 3;   //!<同城交易，无需运费
    public static final int TRANSPORT_PAY_TYPE_NULL = 4;
	public static final int TRANSPORT_WITH_SHIPPINGFEE_BORDER = 0x0a; //!< 支持运费模板的边界值， >=这个值时，表示支持运费模板。 added by woodzheng,2007-11-28
    
	// 邮寄方式(邮递类型)
	// c2c_define.h#POST_TYPE
	public static final int POST_FREE = 0;    //!<卖家承担运费
	public static final int POST_NORMAL = 1;                 //!<平邮
	public static final int POST_EXPRESS = 2;                //!<快递
	public static final int POST_EMS = 3;                    //!<EMS
	public static final int POST_NORMAL_DB =4;               //!< 订单DB存储新的平邮标识的值
	public static final int POST_EXPRESS_DB = 5;             //!< 订单DB存储新的快递标识的值
	////////////////货到付款从10开始，每个物流公司一个邮寄方式
	public static final int POST_COD_ZJS = 11;                  //!< 宅急送
	public static final int POST_COD_SF = 12;                   //!< 顺丰
	public static final int POST_COD_YS = 13;
	public static final int POST_COD_YT = 14;
	
	// 支付方式
	// c2c_define.h#PAY_TYPE
	public static final int PAY_TYPE_UNDEFINED = 0;         //!< 未定义
	public static final int PAY_TYPE_PAY_AGENCY = 1;            //!< 支付中介
	public static final int PAY_TYPE_COD = 2;                  //!< 货到付款
	public static final int PAY_TYPE_FQFK = 3;                  //!< 分期付款
	public static final int PAY_TYPE_SCORE = 4;                  //!< 移动积分
	
	public static final int NEW_PAY_TYPE_PAY_AGENCY = 0;            //!< 支付中介
	public static final int NEW_PAY_TYPE_COD = 1;                  //!< 货到付款
	
}
