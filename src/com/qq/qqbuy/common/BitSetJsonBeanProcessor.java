package com.qq.qqbuy.common;

import java.util.BitSet;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonBeanProcessor;

/**
 * @author winsonwu
 * @Created 2012-7-24
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class BitSetJsonBeanProcessor implements JsonBeanProcessor
{

    @Override
    public JSONObject processBean(Object bean, JsonConfig arg1)
    {
        if (!(bean instanceof BitSet))
        {
            return new JSONObject(true);
        }
        String str = bean.toString();
        if (str != null && str.startsWith("{") && str.endsWith("}")) {
        	str = str.substring(1, str.length()-1);
        }
        return new JSONObject().element("name", str);
    }

}