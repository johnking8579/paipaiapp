package com.qq.qqbuy.common;

import java.util.LinkedList;
import java.util.List;

/**
 * 分页容器
 */
public class Pager<T> {

	private List<T> elements = new LinkedList<T>(); 
	private int pageNo;
	private int pageSize;
	private long totalCount;	

	public Pager() {
	}

	public Pager(List<T> elements, int totalCount) {
		this.elements = elements;
		this.totalCount = totalCount;
	}

	public Pager(List<T> elements, int totalCount, int pageNo, int pageSize) {
		this.elements = elements;
		this.totalCount = totalCount;
		this.pageNo = pageNo;
		this.pageSize = pageSize;
	}

	public List<T> getElements() {
		return elements;
	}

	public void setElements(List<T> elements) {
		this.elements = elements;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
}
