package com.qq.qqbuy.common;

import java.net.InetSocketAddress;
import java.util.Random;

import com.paipai.component.configagent.ConfigHelper;
import com.paipai.component.configagent.Configs;

/**
 * 用于封装需要通过拍拍配置中心获取数据，对外返回的InetSocketAddress不会为null
 * 
 * @author winsonwu
 * 
 */
public class PaiPaiConfig
{

    // 用于自增，均衡负荷
//    public static int qgoSysOpenAPIRoutKey = 0;
//    public static int mwgSysWDBRoutKey = 0;
//    public static int mwgSysRDBRoutKey = 0;
    public static int qgoSysProxy = 0;
    public static int qgoSmsSendHost = 0;
    public static int qgoSysSearch = 0;
    public static int qgoSysDiscover = 0;
    public static int jdpophttpsvcSearch = 0 ;
    private static Random rand = new Random();

    /**************************************************************** 在老的拍拍配置中心配置的 ******************************************************************/
    // 获取OpenAPI服务器地址
//    public static final String QGO_SYS_OPENAPI = "qgo.sys.api.nginx"; //"qgo.sys.openapi"; 使用新的配置项 

    // 配置中心配置的httpproxy
    public final static String LOGIN_HTTP_PROXYSER = "qgo.sys.proxy";
    
    //深圳内网访问京东内网代理命令字
    public final static String JD_POP_HTTP_SVC = "jdpophttpsvc" ;
    
    //获取发送SMS服务器地址
	public static final String QGO_SMS_SEND_HOST = "qgo.sms.send";
	
//	//获取checkBind服务器地址
//	public static final String QGO_SMS_CHECKBIND_HOST = "qgo.sms.checkbind";
//	
//	//获取setBind服务器地址
//	public static final String QGO_SMS_SETBIND_HOST = "qgo.sms.setbind";
//	
//	// pp set1 wdb config
//    public final static String PP_SET1_W = "qgo.sys.wdb";
//    
//	// pp set1 rdb config
//    public final static String PP_SET1_R = "qgo.sys.rdb";
//    
//	// pp set2 wdb config
//    public final static String PP_SET2_W = "qgo.sys.set2.wdb";
//    
//	// pp set2 rdb config
//    public final static String PP_SET2_R = "qgo.sys.set2.rdb";
//    
//    // 拍拍搜索cgi的nginx地址列表
//    public final static String PP_SEARCH = "qgo.sys.search";
    
    // 拍拍推荐cgi的nginx地址列表
    public final static String PP_DISCOVER = "paipai_bi_discover";
    
    //*.paipai.com的内网NGINX的IP地址,有很多
    public final static String PP_DOMAIN_IP = "paipai_app_common";
    
	
	// 获取pp set1 wdb
//	public static String getPPSet1WriteDBLocation()
//    {
//        InetSocketAddress address = getHostFromPaiPai(PP_SET1_W,
//                mwgSysWDBRoutKey++);
//        return address.getHostName() + ":" + address.getPort();
//    }

	// 获取pp set1 rdb
//    public static String getPPSet1ReadDBLocation()
//    {
//        InetSocketAddress address = getHostFromPaiPai(PP_SET1_R,
//                mwgSysRDBRoutKey++);
//        return address.getHostName() + ":" + address.getPort();
//    }
  
    // 获取pp set2 wdb
//	public static String getPPSet2WriteDBLocation()
//    {
//        InetSocketAddress address = getHostFromPaiPai(PP_SET2_W,
//                mwgSysWDBRoutKey++);
//        return address.getHostName() + ":" + address.getPort();
//    }

	// 获取pp set2 rdb
//    public static String getPPSet2ReadDBLocation()
//    {
//        InetSocketAddress address = getHostFromPaiPai(PP_SET2_R,
//                mwgSysRDBRoutKey++);
//        return address.getHostName() + ":" + address.getPort();
//    }
	
//    public static String getPPSearchLocation() {
//    	InetSocketAddress address = getHostFromPaiPai(PP_SEARCH, qgoSysSearch++);
//    	return address.getHostName() + ":" + address.getPort();
//    }
    
    public static String getPPDiscoverLocation() {
    	InetSocketAddress address = getHostFromPaiPai(PP_DISCOVER, qgoSysDiscover++);
    	return address.getHostName() + ":" + address.getPort();
    }
    
    public static String getPaipaiCommonIp()	{
    	InetSocketAddress address = getHostFromPaiPai(PP_DOMAIN_IP, rand.nextInt(100));
    	return address.getHostName() + ":" + address.getPort();
    }

//    public static InetSocketAddress getOpenAPIServerLocation()
//    {
//        return  getHostFromPaiPai(QGO_SYS_OPENAPI,
//                qgoSysOpenAPIRoutKey++);
//    }

    public static InetSocketAddress getHttpProxyHostQgo()
    {
        InetSocketAddress addr = getHostFromPaiPai(LOGIN_HTTP_PROXYSER,
                qgoSysProxy++);
        return addr;
    }
    
    public static InetSocketAddress getJdPopHttpSvcProxyHost()
    {
        InetSocketAddress addr = getHostFromPaiPai(JD_POP_HTTP_SVC,
        		jdpophttpsvcSearch++);
        return addr;
    }

    public static String getSendMsgServerUrl() {
		InetSocketAddress address = getHostFromPaiPai(QGO_SMS_SEND_HOST, qgoSmsSendHost++);
		//http://172.27.31.79:8099/smsproxy/sendSM
		return "http://" + address.getHostName() + ":" + address.getPort() + "/smsproxy/sendSM";
	}

    /**
     * 从拍拍的配置中心取得指定的host
     * 约定了，此接口的address不会返回null
     * @param svcName
     * @param routKey
     * @return 127.0.0.1:8080
     */
    private static InetSocketAddress getHostFromPaiPai(String svcName, int routKey)	{
        return ConfigHelper.getSvcAddressBySet(Configs.PAIPAI_CONFIG_TYPE, svcName, 0, routKey);
    }
}
