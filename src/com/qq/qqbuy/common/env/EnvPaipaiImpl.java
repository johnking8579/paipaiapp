package com.qq.qqbuy.common.env;

public class EnvPaipaiImpl implements Env{
	
	private String envId;
	
	//该变量在启动tomcat时加入
    public static final String ENV_VARIALE_NAME = "qgo.cfg.env";

    @Override
    public boolean isLocal()    {
    	return "local".equals(envId);
    }
    
    @Override
    public boolean isGamma()    {
    	return "idc_preview".equals(System.getProperty(ENV_VARIALE_NAME));
    }

    @Override
    public boolean isIdc()	{
    	return "idc".equals(System.getProperty(ENV_VARIALE_NAME));
    }
   
	@Override
	public boolean isJd() {
		return false;
	}

	@Override
	public boolean isPaipai() {
		return true;
	}

	@Override
	public void setEnvId(String envId) {
		this.envId = envId;
	}

	@Override
	public String getEnvId() {
		return envId;
	}

}
