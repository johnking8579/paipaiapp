package com.qq.qqbuy.common.env;


public interface Env {
	
	String getEnvId();
	
	void setEnvId(String id);
	
	boolean isJd();
	
	boolean isPaipai();
	
	boolean isLocal();

	boolean isGamma();

	boolean isIdc();
}
