package com.qq.qqbuy.common;

/**
 * 把超过Uint32范围的负数转化为正数long
 *
 */
public class Uint32 {  
	
    public final static long 
    	MIN_VALUE = 0,  
     	MAX_VALUE = new Uint32((1L << 32) - 1).longValue();  
      
    private Long value;  
      
    public Uint32(int i) {  
    	value = ((long)i) & 0xFFFFFFFFL;
    }  
    
    public long longValue() {  
    	return value;  
    }  

    private Uint32(long l) {  
        value = l;
    }  
  
    @Override  
    public int hashCode() {  
        return value.hashCode();  
    }  
      
    public String toString() {  
        return String.valueOf(value);  
    }  
      
    public int compareTo(Uint32 obj) {  
        if(obj == null) {  
            return -1;  
        }  
        if(this.value == obj.value) {  
            return 0;  
        }  
        return this.value > obj.value ? 1 : 0;  
    }  
  
    @Override  
    public boolean equals(Object obj) {  
        if (this == obj)  
            return true;  
        if (obj == null)  
            return false;  
        if (getClass() != obj.getClass())  
            return false;  
        final Uint32 other = (Uint32) obj;  
        if (value != other.value)  
            return false;  
        return true;  
    }  
    
    public static void main(String[] args) {
		System.out.println(Integer.MAX_VALUE);
		Long l = 2388981059L;
		int i = l.intValue();
		System.out.println(l.intValue());
		System.out.println(new Uint32(i).longValue());
	}
}  
