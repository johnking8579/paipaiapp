package com.qq.qqbuy.common;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.qq.qqbuy.common.util.Util;

/**
 * 容器类,存放手机APP传过来的channel参数,该参数是JSON格式的
 * 
 * @author JingYing 2014-9-30
 * 
 */
public class ChannelParam {

	private JsonObject json;

	/**
	 * 使用app传入的channel参数, 构造ChannelParam
	 * @throws json解析异常
	 * @param json
	 */
	public ChannelParam(String json) {
		if (json != null) {
			try {
				this.json = new JsonParser().parse(json).getAsJsonObject();
			} catch (Exception e) {
				throw new IllegalArgumentException("无法解析json:" + json);
			}
		}
	}

	public String getPprdP() {
		return getString("pprd_p");
	}

	public String getMarket() {
		return getString("market");
	}

	public String getGdt_vid() {
		return getString("gdt_vid");
	}

	public String getQz_gdt() {
		return getString("qz_gdt");
	}

	public String getQz_express() {
		return getString("qz_express");
	}

	public String getJd_pop() {
		return getString("jd_pop");
	}

	public String getPps() {
		return getString("pps");
	}

	/**
	 * 根据channelParam生成pprd串, 如果有pprd_p,直接使用pprd_p, 否则要根据几个参数生成
	 * 
	 * gdt_vid=giaw5ro2b2y4a01 QZGDT.值 PPRD_P= QZGDT.giaw5ro2b2y4a01
	 * qz_gdt=giaw5ro2b2y4a01 QZGDT.值 PPRD_P=QZGDT.giaw5ro2b2y4a01
	 * qz_express=823117322_12239_1409818564_40188 QZZTC.值 PPRD_P=QZZTC.823117322_12239_1409818564_40188 
	 * jd_pop=paipaitest JDPOP.值    PPRD_P=JDPOP.paipaitest 
	 * pps=cpc.46529082485031409823377940406 值 PPRD_P=值
	 * pps=etg.5560_18921_2_12010507-2786553 值 PPRD_P=值
	 * 
	 * 
	 * 以上如果有多个，则pprd_p =
	 * 算法1-算法2-算法N，例如qz_express=823117322_12239_1409818564_40188&
	 * jd_pop=paipaitest 传了这两个参数，则： pprd_p =
	 * QZZTC.823117322_12239_1409818564_40188-JDPOP.paipaitest
	 * 
	 * @return
	 */
	public String generatePprd() {
		if(Util.isNotEmpty(getPprdP()))	{
			return getPprdP();
		} else	{
			String[] arr = new String[]{
				"QZGDT." + getGdt_vid(),
				"QZGDT." + getQz_gdt(),
				"QZZTC." + getQz_express(),
				"JDPOP." + getJd_pop(),
				getPps()
			};
			return Util.join(arr, "-");
		}
	}

	private String getString(String key) {
		if (json == null)
			return null;
		JsonElement j = json.get(key);
		return j == null ? null : j.getAsString();
	}
}
