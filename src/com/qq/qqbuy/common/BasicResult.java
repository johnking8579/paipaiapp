package com.qq.qqbuy.common;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.qq.qqbuy.common.util.AntiXssHelper;
import com.qq.qqbuy.common.util.CoderUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 接口调用结果的基本类
 *
 * @author rickwang
 */
public class BasicResult {
    public static final String LOGIN_BACK_URL_KEY = "_LOGIN_BACK_URL_KEY";

    /**
     * 错误码. 0表示成功, 其它表示失败
     */
    private int errCode = 0;

    /**
     * 业务返回码. 当errCode为0时，本字段可以为各种有业务意义的取值. 也可以不细分跟errCode相同
     */
    private int retCode = 0;

    /**
     * 当errCode不为0时, 表示错误内容
     */
    private String msg = null;

    /**
     * 用于调用者对应答和请求进行匹配. 由调用者在请求中传递给服务端, 服务端原样在应答中返回
     */
    private String dtag = null;

    /**
     * 其它数据
     */
    public Map<String, String> data = new HashMap<String, String>();

    public BasicResult() {
    }

    public BasicResult(int errCode, String msg) {
        this.errCode = errCode;
        this.msg = msg;
    }

    public static BasicResult successResult() {
        return new BasicResult(0, null);
    }

    public static BasicResult successResult(String msg) {
        return new BasicResult(0, msg);
    }

    public static BasicResult errorResult(int errCode) {
        return new BasicResult(-1, null);
    }

    public static BasicResult errorResult(int errCode, String msg) {
        return new BasicResult(-1, msg);
    }
    
    public void setErrInfo(int errCode, String msg) {
        this.errCode = errCode;
        this.msg = msg;
    }

    public int getErrCode() {
        return errCode;
    }

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }

    public int getRetCode() {
        return retCode;
    }

    public void setRetCode(int retCode) {
        this.retCode = retCode;
    }

    public String getMsg() {
        return CoderUtil.decodeXssFilter(AntiXssHelper.htmlAttributeEncode(msg));
    }
    
    public void setMsg(String msg) {
        this.msg = CoderUtil.decodeXssFilter(AntiXssHelper.htmlAttributeEncode(msg));
    }

    public String getDtag() {
        return dtag;
    }

    public void setDtag(String dtag) {
        this.dtag = dtag;
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }

    public void addData(String key, String value) {
        this.data.put(key, value);
    }

    /**
     * 转换成 json 字符串
     *
     * @return json 字符串
     */
    public String getJsonString() {
        return toJsonString();
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * 转换成 json 字符串
     *
     * @return json 字符串
     */
    public String toJsonString() {
        try {
            JsonConfig jsonConfig = new JsonConfig();
            jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
            jsonConfig.setExcludes(new String[]{"jsonString"});
            return JSONObject.fromObject(this, jsonConfig).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "{errCode: -3}";
        }
    }
}
