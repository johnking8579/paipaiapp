package com.qq.qqbuy.common.exception;

/**
 * 用户未登陆
 * @author JingYing
 * @date 2014年12月12日
 */
public class NotLoginException extends RuntimeException{

	public NotLoginException() {
		super();
	}

	public NotLoginException(String message, Throwable cause) {
		super(message, cause);
	}

	public NotLoginException(String message) {
		super(message);
	}

	public NotLoginException(Throwable cause) {
		super(cause);
	}

}
