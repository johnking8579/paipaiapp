package com.qq.qqbuy.common.exception;

import com.google.gson.JsonObject;
import com.qq.qqbuy.common.JsonOutput;
import com.qq.qqbuy.common.constant.ErrConstant;

/**
 * 把异常转换化为前端可识别的字符串
 */
public class ExceptionTranslator   {
	
	/**
	 * 根据异常种类, 转换成app接口可识别的json串
	 * @param e
	 * @return
	 */
	public static String translateToApi(Exception e)	{
		int errcode;
	 	String msg = "", retCode = "0";
	 	if(e instanceof java.lang.IllegalArgumentException)	{
			errcode = ErrConstant.ERRCODE_INVALID_PARAMETER;
			msg = "参数校验不合法";		
	 	} else if(e instanceof NotLoginException)	{
			errcode = ErrConstant.ERRCODE_CHECKLOGIN_FAIL;
			msg = "鉴权失败";
	 	} else if(e instanceof ExternalInterfaceException)	{
			errcode = ErrConstant.IDL_FAILURE;
			ExternalInterfaceException exp = (ExternalInterfaceException)e;
			retCode = "0".equals(exp.getInvokeResult()) ? exp.getRespCode() : exp.getInvokeResult();	//retcode可能为invoke返回值(WebStubErr.*),也可能为response的result 
			msg = e.getMessage();
	 	} else {
			errcode = ErrConstant.ERRCODE_ACTION_UNKNOW_EXP;
			msg = String.valueOf(e);
	 	}
	 	
	 	return new JsonOutput()
	 		.setErrCode(errcode)
	 		.setRetCode(retCode)
	 		.setMsg(msg)
	 		.setData(new JsonObject())
	 		.toJsonStr();
	}

	
}
