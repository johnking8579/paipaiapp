package com.qq.qqbuy.common.po;

import java.io.Serializable;

/**
 * 
 * @ClassName: CommonPo
 *  
 * @Description: 公共po
 * @author wendyhu 
 * @date 2012-12-13 下午03:01:02
 */
public class CommonPo implements Serializable
{
    /**
     * 错误码
     */
    public long errCode = 0;
    
    /**
     * 返回码
     */
    public long retCode = 0;
    
    /**
     * 返回消息
     */
    public String msg = "";
    
    
    
}
