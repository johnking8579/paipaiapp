package com.qq.qqbuy.common.po;

/**
 * 
 * @ClassName: LoginResult 
 * 
 * @Description: 登陆后的返回状态
 *  
 * @author wendyhu 
 * @date 2012-12-5 下午05:34:01
 */
public class LoginResult
{
    /**
     * 返回值
     */
    private long retCode = -1000;

    /**
     * 返回码
     */
    private int code = 0;
    /**
     * 返回 信息
     */
    private String retMsg = "";

    /**
     * 昵称
     */
    private String nickName = "";
    
    /**
     * qq号码
     */
    private long uin = 0;
    
    
    /**
     * 生成的sid
     */
    private String sid = "";
    
    public String getSid()
    {
        return sid;
    }

    public void setSid(String sid)
    {
        this.sid = sid;
    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public long getUin()
    {
        return uin;
    }

    public void setUin(long uin)
    {
        this.uin = uin;
    }

    public long getRetCode()
    {
        return retCode;
    }

    public void setRetCode(long retCode)
    {
        this.retCode = retCode;
    }

    public String getRetMsg()
    {
        return retMsg;
    }

    public void setRetMsg(String retMsg)
    {
        this.retMsg = retMsg;
    }

    public String getNickName()
    {
        return nickName;
    }

    public void setNickName(String nickName)
    {
        this.nickName = nickName;
    }

    @Override
    public String toString()
    {
        return " LoginResult [code=" + code + ", nickName=" + nickName
                + ", retCode=" + retCode + ", retMsg=" + retMsg + ", sid="
                + sid + ", uin=" + uin + "]";
    }
    
    
}
