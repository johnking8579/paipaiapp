package com.qq.qqbuy.common;

import java.util.LinkedList;
import java.util.List;

/**
 * 分页bean
 * 
 * @author homerwu
 * @deprecated 与Pager类重复
 */
public class Page<T extends Object>
{

    private List<T> elements = new LinkedList<T>();
    private int pageNo;
    private int pageSize;
    private long rowCount;

    public Page()
    {
        this.elements = new LinkedList<T>();
        this.pageNo = 0;
        this.pageSize = 0;
        this.rowCount = 0;
    }

    public List<T> getElements()
    {
        return elements;
    }

    public void setElements(List<T> elements)
    {
        this.elements = elements;
    }

    public int getPageNo()
    {
        return pageNo;
    }

    public void setPageNo(int pageNo)
    {
        this.pageNo = pageNo;
    }

    public int getPageSize()
    {
        return pageSize;
    }

    public void setPageSize(int pageSize)
    {
        this.pageSize = pageSize;
    }

    public long getRowCount()
    {
        return rowCount;
    }

    public void setRowCount(long rowCount)
    {
        this.rowCount = rowCount;
    }

    public long getPageTotal()
    {
        if (pageSize > 0)
        {
            return (rowCount + pageSize - 1) / pageSize;
        } else
        {
            return 0;
        }
    }

    @Override
    public String toString()
    {
        return "Page [elements=" + elements + ", pageNo=" + pageNo
                + ", pageSize=" + pageSize + ", rowCount=" + rowCount + "]";
    }

}
