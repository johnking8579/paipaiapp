package com.qq.qqbuy.common;

/**
 * 对象容器
 */
public class Tuple3<A, B, C> extends Tuple2<A, B> {
	protected C c;
	
	public Tuple3()	{}
	
	public Tuple3(A a, B b, C c)	{
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public C getC() {
		return c;
	}

	public void setC(C c) {
		this.c = c;
	}
}
