package com.qq.qqbuy.gchoice.po;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.qq.qqbuy.common.util.StringUtil;


public class ActiveInfoParamJson implements Comparable<ActiveInfoParamJson>{

	private String activeId;//活动id
	private String areastate;
	private String activeState;
	private String activityName;
	private String activityImg;
	private String activityUrl;
	private String seedNum;
	private String sq_activityImg;
	private String sq_activityUrl;
	private String poolId;//商品池id
	private long arealowestPrice;//活动价格
	private long areasalesVolume;//活动销量
	private String beginTime;//开始时间
	private String endTime;//结束时间
	private String url_more;
	
	private JsonObject twj;//推荐位信息
	private JsonObject maishou;//买手
	
	private String currTime;// 当前时间
	
	
	@Override
	public String toString() {
		Gson gson = new Gson();
		
		return "ActiveInfoParamJson [activeId=" + activeId + ", areastate="
				+ areastate + ", activeState=" + activeState
				+ ", activityName=" + activityName + ", activityImg="
				+ activityImg + ", activityUrl=" + activityUrl + ", seedNum="
				+ seedNum + ", sq_activityImg=" + sq_activityImg
				+ ", sq_activityUrl=" + sq_activityUrl + ", poolId=" + poolId
				+ ", arealowestPrice=" + arealowestPrice + ", areasalesVolume="
				+ areasalesVolume + ", beginTime=" + beginTime + ", url_more=" + url_more + ", endTime="
				+ endTime + ", twj=" + gson.toJson(twj) + ", maishou=" + gson.toJson(maishou) + "]";
	}
	public JsonObject getTwj() {
		return twj;
	}
	public void setTwj(JsonObject twj) {
		this.twj = twj;
	}
	public JsonObject getMaishou() {
		return maishou;
	}
	public void setMaishou(JsonObject maishou) {
		this.maishou = maishou;
	}
	public String getActiveId() {
		return activeId;
	}
	public void setActiveId(String activeId) {
		this.activeId = activeId;
	}
	public String getAreastate() {
		return areastate;
	}
	public void setAreastate(String areastate) {
		this.areastate = areastate;
	}
	public String getActiveState() {
		return activeState;
	}
	public void setActiveState(String activeState) {
		this.activeState = activeState;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getActivityImg() {
		return activityImg;
	}
	public void setActivityImg(String activityImg) {
		this.activityImg = activityImg;
	}
	public String getActivityUrl() {
		return activityUrl;
	}
	public void setActivityUrl(String activityUrl) {
		this.activityUrl = activityUrl;
	}
	public String getSeedNum() {
		return seedNum;
	}
	public void setSeedNum(String seedNum) {
		this.seedNum = seedNum;
	}
	public String getSq_activityImg() {
		return sq_activityImg;
	}
	public void setSq_activityImg(String sq_activityImg) {
		this.sq_activityImg = sq_activityImg;
	}
	public String getSq_activityUrl() {
		return sq_activityUrl;
	}
	public void setSq_activityUrl(String sq_activityUrl) {
		this.sq_activityUrl = sq_activityUrl;
	}
	public String getPoolId() {
		return poolId;
	}
	public void setPoolId(String poolId) {
		this.poolId = poolId;
	}
	public long getArealowestPrice() {
		return arealowestPrice;
	}
	public void setArealowestPrice(long arealowestPrice) {
		this.arealowestPrice = arealowestPrice;
	}
	public long getAreasalesVolume() {
		return areasalesVolume;
	}
	public void setAreasalesVolume(long areasalesVolume) {
		this.areasalesVolume = areasalesVolume;
	}
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getUrl_more() {
		return url_more;
	}
	public void setUrl_more(String url_more) {
		this.url_more = url_more;
	}
	public String getCurrTime() {
		return currTime;
	}
	public void setCurrTime(String currTime) {
		this.currTime = currTime;
	}

	
	@Override
	public int compareTo(ActiveInfoParamJson o) {
		if(null == o || StringUtil.isBlank(o.getBeginTime()) || StringUtil.isBlank(this.beginTime) || 
				Long.parseLong(o.getBeginTime()) < Long.parseLong(this.beginTime)){
			return -1;
		}
		return 1;
	}
}
