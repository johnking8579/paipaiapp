package com.qq.qqbuy.gchoice.po;

/**
 * 

 * @ClassName: GatherChoice 

 * @Description: 聚精品接口返回值 
 
 * @author lhn

 * @date 2014-12-9 下午3:02:37 

 *
 */
public class GatherChoice {
	
	private String gsontitle="聚精品";
	private String areaStartTime="0";//限价开始时间
	private String areaEndTime="0";//限价结束时间
	private String itemId;//商品ID
	private String itemName;//商品名称
	private String marketPrice;//商品原价
	private String stock;//库存量
	private String picUrl;//商品图片URL1
	private String picUrl2;//商品图片URL2
	private String picUrl3;//商品图片URL3
	private String price;//活动价
	private String shopId;//店家ID
	private String nowTime;//当前时间
	private String url;//链接
	private String ext3_colormark;//颜色标识
	
	
	@Override
	public String toString() {
		String ss = "areaStartTime:"+areaStartTime+"areaEndTime:"+areaEndTime+"itemId:"+itemId+"itemName:"+itemName
				+"marketPrice:"+marketPrice+"stock:"+stock+"picUrl:"+picUrl+"picUrl2:"+picUrl2+"picUrl3:"+picUrl3
				+"price:"+price+"shopId:"+shopId+"nowTime:"+nowTime+"url:"+url+"colormark:"+ext3_colormark;
		return ss.toString();
	}
	public String getAreaStartTime() {
		return areaStartTime;
	}
	public void setAreaStartTime(String areaStartTime) {
		this.areaStartTime = areaStartTime;
	}
	public String getAreaEndTime() {
		return areaEndTime;
	}
	public void setAreaEndTime(String areaEndTime) {
		this.areaEndTime = areaEndTime;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getMarketPrice() {
		return marketPrice;
	}
	public void setMarketPrice(String marketPrice) {
		this.marketPrice = marketPrice;
	}
	public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	public String getPicUrl() {
		return picUrl;
	}
	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}
	public String getPicUrl2() {
		return picUrl2;
	}
	public void setPicUrl2(String picUrl2) {
		this.picUrl2 = picUrl2;
	}
	public String getPicUrl3() {
		return picUrl3;
	}
	public void setPicUrl3(String picUrl3) {
		this.picUrl3 = picUrl3;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getShopId() {
		return shopId;
	}
	public void setShopId(String shopId) {
		this.shopId = shopId;
	}
	public String getNowTime() {
		return nowTime;
	}
	public void setNowTime(String nowTime) {
		this.nowTime = nowTime;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getGsontitle() {
		return gsontitle;
	}
	public void setGsontitle(String gsontitle) {
		this.gsontitle = gsontitle;
	}
	public String getExt3_colormark() {
		return ext3_colormark;
	}
	public void setExt3_colormark(String ext3_colormark) {
		this.ext3_colormark = ext3_colormark;
	}
	
}
