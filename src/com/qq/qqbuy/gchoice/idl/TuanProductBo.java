//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: tuantuanjiang.idl.TuanInfoBo.java

package com.qq.qqbuy.gchoice.idl;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import java.util.Map;
import java.util.HashMap;

/**
 *团商品信息
 *
 *@date 2015-03-04 10:57:53
 *
 *@since version:0
*/
public class TuanProductBo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version = 20141028;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 商品id
	 *
	 * 版本 >= 0
	 */
	 private String ProductId = new String();

	/**
	 * 版本 >= 0
	 */
	 private short ProductId_u;

	/**
	 * 商品名称
	 *
	 * 版本 >= 0
	 */
	 private String ProductName = new String();

	/**
	 * 版本 >= 0
	 */
	 private short ProductName_u;

	/**
	 * 商品图片
	 *
	 * 版本 >= 0
	 */
	 private String ImageUrl = new String();

	/**
	 * 版本 >= 0
	 */
	 private short ImageUrl_u;

	/**
	 * 商品市场价格
	 *
	 * 版本 >= 0
	 */
	 private long ProductMarketPrice;

	/**
	 * 版本 >= 0
	 */
	 private short ProductMarketPrice_u;

	/**
	 * 商品成团价格
	 *
	 * 版本 >= 0
	 */
	 private long ProductPrice;

	/**
	 * 版本 >= 0
	 */
	 private short ProductPrice_u;

	/**
	 * 渠道id
	 *
	 * 版本 >= 0
	 */
	 private long Chid;

	/**
	 * 版本 >= 0
	 */
	 private short Chid_u;

	/**
	 * 产品编号
	 *
	 * 版本 >= 0
	 */
	 private String ProductCharId = new String();

	/**
	 * 版本 >= 0
	 */
	 private short ProductCharId_u;

	/**
	 * 错误码
	 *
	 * 版本 >= 0
	 */
	 private long ErrCode;

	/**
	 * 版本 >= 0
	 */
	 private short ErrCode_u;

	/**
	 * 保留输入
	 *
	 * 版本 >= 20141028
	 */
	 private Map<String,String> ReserveIn = new HashMap<String,String>();

	/**
	 * 
	 *
	 * 版本 >= 20141028
	 */
	 private short ReserveIn_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushString(ProductId);
		bs.pushUByte(ProductId_u);
		bs.pushString(ProductName);
		bs.pushUByte(ProductName_u);
		bs.pushString(ImageUrl);
		bs.pushUByte(ImageUrl_u);
		bs.pushUInt(ProductMarketPrice);
		bs.pushUByte(ProductMarketPrice_u);
		bs.pushUInt(ProductPrice);
		bs.pushUByte(ProductPrice_u);
		bs.pushUInt(Chid);
		bs.pushUByte(Chid_u);
		bs.pushString(ProductCharId);
		bs.pushUByte(ProductCharId_u);
		bs.pushUInt(ErrCode);
		bs.pushUByte(ErrCode_u);
		if(  this.Version >= 20141028 ){
				bs.pushObject(ReserveIn);
		}
		if(  this.Version >= 20141028 ){
				bs.pushUByte(ReserveIn_u);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		ProductId = bs.popString();
		ProductId_u = bs.popUByte();
		ProductName = bs.popString();
		ProductName_u = bs.popUByte();
		ImageUrl = bs.popString();
		ImageUrl_u = bs.popUByte();
		ProductMarketPrice = bs.popUInt();
		ProductMarketPrice_u = bs.popUByte();
		ProductPrice = bs.popUInt();
		ProductPrice_u = bs.popUByte();
		Chid = bs.popUInt();
		Chid_u = bs.popUByte();
		ProductCharId = bs.popString();
		ProductCharId_u = bs.popUByte();
		ErrCode = bs.popUInt();
		ErrCode_u = bs.popUByte();
		if(  this.Version >= 20141028 ){
				ReserveIn = (Map<String,String>)bs.popMap(String.class,String.class);
		}
		if(  this.Version >= 20141028 ){
				ReserveIn_u = bs.popUByte();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取商品id
	 * 
	 * 此字段的版本 >= 0
	 * @return ProductId value 类型为:String
	 * 
	 */
	public String getProductId()
	{
		return ProductId;
	}


	/**
	 * 设置商品id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setProductId(String value)
	{
		this.ProductId = value;
		this.ProductId_u = 1;
	}

	public boolean issetProductId()
	{
		return this.ProductId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ProductId_u value 类型为:short
	 * 
	 */
	public short getProductId_u()
	{
		return ProductId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProductId_u(short value)
	{
		this.ProductId_u = value;
	}


	/**
	 * 获取商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @return ProductName value 类型为:String
	 * 
	 */
	public String getProductName()
	{
		return ProductName;
	}


	/**
	 * 设置商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setProductName(String value)
	{
		this.ProductName = value;
		this.ProductName_u = 1;
	}

	public boolean issetProductName()
	{
		return this.ProductName_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ProductName_u value 类型为:short
	 * 
	 */
	public short getProductName_u()
	{
		return ProductName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProductName_u(short value)
	{
		this.ProductName_u = value;
	}


	/**
	 * 获取商品图片
	 * 
	 * 此字段的版本 >= 0
	 * @return ImageUrl value 类型为:String
	 * 
	 */
	public String getImageUrl()
	{
		return ImageUrl;
	}


	/**
	 * 设置商品图片
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setImageUrl(String value)
	{
		this.ImageUrl = value;
		this.ImageUrl_u = 1;
	}

	public boolean issetImageUrl()
	{
		return this.ImageUrl_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ImageUrl_u value 类型为:short
	 * 
	 */
	public short getImageUrl_u()
	{
		return ImageUrl_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setImageUrl_u(short value)
	{
		this.ImageUrl_u = value;
	}


	/**
	 * 获取商品市场价格
	 * 
	 * 此字段的版本 >= 0
	 * @return ProductMarketPrice value 类型为:long
	 * 
	 */
	public long getProductMarketPrice()
	{
		return ProductMarketPrice;
	}


	/**
	 * 设置商品市场价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setProductMarketPrice(long value)
	{
		this.ProductMarketPrice = value;
		this.ProductMarketPrice_u = 1;
	}

	public boolean issetProductMarketPrice()
	{
		return this.ProductMarketPrice_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ProductMarketPrice_u value 类型为:short
	 * 
	 */
	public short getProductMarketPrice_u()
	{
		return ProductMarketPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProductMarketPrice_u(short value)
	{
		this.ProductMarketPrice_u = value;
	}


	/**
	 * 获取商品成团价格
	 * 
	 * 此字段的版本 >= 0
	 * @return ProductPrice value 类型为:long
	 * 
	 */
	public long getProductPrice()
	{
		return ProductPrice;
	}


	/**
	 * 设置商品成团价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setProductPrice(long value)
	{
		this.ProductPrice = value;
		this.ProductPrice_u = 1;
	}

	public boolean issetProductPrice()
	{
		return this.ProductPrice_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ProductPrice_u value 类型为:short
	 * 
	 */
	public short getProductPrice_u()
	{
		return ProductPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProductPrice_u(short value)
	{
		this.ProductPrice_u = value;
	}


	/**
	 * 获取渠道id
	 * 
	 * 此字段的版本 >= 0
	 * @return Chid value 类型为:long
	 * 
	 */
	public long getChid()
	{
		return Chid;
	}


	/**
	 * 设置渠道id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setChid(long value)
	{
		this.Chid = value;
		this.Chid_u = 1;
	}

	public boolean issetChid()
	{
		return this.Chid_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Chid_u value 类型为:short
	 * 
	 */
	public short getChid_u()
	{
		return Chid_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setChid_u(short value)
	{
		this.Chid_u = value;
	}


	/**
	 * 获取产品编号
	 * 
	 * 此字段的版本 >= 0
	 * @return ProductCharId value 类型为:String
	 * 
	 */
	public String getProductCharId()
	{
		return ProductCharId;
	}


	/**
	 * 设置产品编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setProductCharId(String value)
	{
		this.ProductCharId = value;
		this.ProductCharId_u = 1;
	}

	public boolean issetProductCharId()
	{
		return this.ProductCharId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ProductCharId_u value 类型为:short
	 * 
	 */
	public short getProductCharId_u()
	{
		return ProductCharId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProductCharId_u(short value)
	{
		this.ProductCharId_u = value;
	}


	/**
	 * 获取错误码
	 * 
	 * 此字段的版本 >= 0
	 * @return ErrCode value 类型为:long
	 * 
	 */
	public long getErrCode()
	{
		return ErrCode;
	}


	/**
	 * 设置错误码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setErrCode(long value)
	{
		this.ErrCode = value;
		this.ErrCode_u = 1;
	}

	public boolean issetErrCode()
	{
		return this.ErrCode_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ErrCode_u value 类型为:short
	 * 
	 */
	public short getErrCode_u()
	{
		return ErrCode_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setErrCode_u(short value)
	{
		this.ErrCode_u = value;
	}


	/**
	 * 获取保留输入
	 * 
	 * 此字段的版本 >= 20141028
	 * @return ReserveIn value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置保留输入
	 * 
	 * 此字段的版本 >= 20141028
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setReserveIn(Map<String,String> value)
	{
		if (value != null) {
				this.ReserveIn = value;
				this.ReserveIn_u = 1;
		}
	}

	public boolean issetReserveIn()
	{
		return this.ReserveIn_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141028
	 * @return ReserveIn_u value 类型为:short
	 * 
	 */
	public short getReserveIn_u()
	{
		return ReserveIn_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141028
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserveIn_u(short value)
	{
		this.ReserveIn_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(TuanProductBo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ProductId, null);  //计算字段ProductId的长度 size_of(String)
				length += 1;  //计算字段ProductId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ProductName, null);  //计算字段ProductName的长度 size_of(String)
				length += 1;  //计算字段ProductName_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ImageUrl, null);  //计算字段ImageUrl的长度 size_of(String)
				length += 1;  //计算字段ImageUrl_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ProductMarketPrice的长度 size_of(uint32_t)
				length += 1;  //计算字段ProductMarketPrice_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ProductPrice的长度 size_of(uint32_t)
				length += 1;  //计算字段ProductPrice_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Chid的长度 size_of(uint32_t)
				length += 1;  //计算字段Chid_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ProductCharId, null);  //计算字段ProductCharId的长度 size_of(String)
				length += 1;  //计算字段ProductCharId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ErrCode的长度 size_of(uint32_t)
				length += 1;  //计算字段ErrCode_u的长度 size_of(uint8_t)
				if(  this.Version >= 20141028 ){
						length += ByteStream.getObjectSize(ReserveIn, null);  //计算字段ReserveIn的长度 size_of(Map)
				}
				if(  this.Version >= 20141028 ){
						length += 1;  //计算字段ReserveIn_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(TuanProductBo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ProductId, encoding);  //计算字段ProductId的长度 size_of(String)
				length += 1;  //计算字段ProductId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ProductName, encoding);  //计算字段ProductName的长度 size_of(String)
				length += 1;  //计算字段ProductName_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ImageUrl, encoding);  //计算字段ImageUrl的长度 size_of(String)
				length += 1;  //计算字段ImageUrl_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ProductMarketPrice的长度 size_of(uint32_t)
				length += 1;  //计算字段ProductMarketPrice_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ProductPrice的长度 size_of(uint32_t)
				length += 1;  //计算字段ProductPrice_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Chid的长度 size_of(uint32_t)
				length += 1;  //计算字段Chid_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ProductCharId, encoding);  //计算字段ProductCharId的长度 size_of(String)
				length += 1;  //计算字段ProductCharId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ErrCode的长度 size_of(uint32_t)
				length += 1;  //计算字段ErrCode_u的长度 size_of(uint8_t)
				if(  this.Version >= 20141028 ){
						length += ByteStream.getObjectSize(ReserveIn, encoding);  //计算字段ReserveIn的长度 size_of(Map)
				}
				if(  this.Version >= 20141028 ){
						length += 1;  //计算字段ReserveIn_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本20141028所包含的字段*******
 *	long Version;///<版本号
 *	short Version_u;
 *	String ProductId;///<商品id
 *	short ProductId_u;
 *	String ProductName;///<商品名称
 *	short ProductName_u;
 *	String ImageUrl;///<商品图片
 *	short ImageUrl_u;
 *	long ProductMarketPrice;///<商品市场价格
 *	short ProductMarketPrice_u;
 *	long ProductPrice;///<商品成团价格
 *	short ProductPrice_u;
 *	long Chid;///<渠道id
 *	short Chid_u;
 *	String ProductCharId;///<产品编号
 *	short ProductCharId_u;
 *	long ErrCode;///<错误码
 *	short ErrCode_u;
 *	Map<String,String> ReserveIn;///<保留输入
 *	short ReserveIn_u;
 *****以上是版本20141028所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
