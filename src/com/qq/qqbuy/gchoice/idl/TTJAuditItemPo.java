//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.AddAuditItemReq.java

package com.qq.qqbuy.gchoice.idl;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import java.util.Map;
import java.util.HashMap;

/**
 *coss后台审计商品信息
 *
 *@date 2015-03-04 11:43:58
 *
 *@since version:0
*/
public class TTJAuditItemPo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20150122;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 商品id
	 *
	 * 版本 >= 0
	 */
	 private String comodityId = new String();

	/**
	 * 版本 >= 0
	 */
	 private short comodityId_u;

	/**
	 * 区域名称
	 *
	 * 版本 >= 0
	 */
	 private String areaName = new String();

	/**
	 * 版本 >= 0
	 */
	 private short areaName_u;

	/**
	 * 活动期数
	 *
	 * 版本 >= 0
	 */
	 private String activeNum = new String();

	/**
	 * 版本 >= 0
	 */
	 private short activeNum_u;

	/**
	 * 商品活动短名
	 *
	 * 版本 >= 0
	 */
	 private String activeShortName = new String();

	/**
	 * 版本 >= 0
	 */
	 private short activeShortName_u;

	/**
	 * 商品活动描述
	 *
	 * 版本 >= 0
	 */
	 private String comodityDesc = new String();

	/**
	 * 版本 >= 0
	 */
	 private short comodityDesc_u;

	/**
	 * 商品原始库存
	 *
	 * 版本 >= 0
	 */
	 private long stocks;

	/**
	 * 版本 >= 0
	 */
	 private short stocks_u;

	/**
	 * 商品活动图片URL
	 *
	 * 版本 >= 0
	 */
	 private String comoditySmallPic = new String();

	/**
	 * 版本 >= 0
	 */
	 private short comoditySmallPic_u;

	/**
	 * 商品活动价格
	 *
	 * 版本 >= 0
	 */
	 private long activePrice;

	/**
	 * 版本 >= 0
	 */
	 private short activePrice_u;

	/**
	 * 成团人数
	 *
	 * 版本 >= 0
	 */
	 private long leastPeopNum;

	/**
	 * 版本 >= 0
	 */
	 private short leastPeopNum_u;

	/**
	 * 排序rank
	 *
	 * 版本 >= 0
	 */
	 private long rank;

	/**
	 * 版本 >= 0
	 */
	 private short rank_u;

	/**
	 * 本次活动开始时间
	 *
	 * 版本 >= 0
	 */
	 private long startTime;

	/**
	 * 版本 >= 0
	 */
	 private short startTime_u;

	/**
	 * 本次活动结束时间
	 *
	 * 版本 >= 0
	 */
	 private long endTime;

	/**
	 * 版本 >= 0
	 */
	 private short endTime_u;

	/**
	 * 该审核数据写入时间
	 *
	 * 版本 >= 0
	 */
	 private long auditCreateTime;

	/**
	 * 版本 >= 0
	 */
	 private short auditCreateTime_u;

	/**
	 * 该审核最新修改时间
	 *
	 * 版本 >= 0
	 */
	 private long modifyTime;

	/**
	 * 版本 >= 0
	 */
	 private short modifyTime_u;

	/**
	 * coss记录状态, 0: 待审核，1: 初审通过，2: 终审通过, 3: 通过
	 *
	 * 版本 >= 0
	 */
	 private short status = 1;

	/**
	 * 版本 >= 0
	 */
	 private short status_u;

	/**
	 * 保留数据字段，备后用
	 *
	 * 版本 >= 0
	 */
	 private String reserve = new String();

	/**
	 * 版本 >= 0
	 */
	 private short reserve_u;

	/**
	 * 大V团价
	 *
	 * 版本 >= 20141120
	 */
	 private long vPrice;

	/**
	 * 
	 *
	 * 版本 >= 20141120
	 */
	 private short vPrice_u;

	/**
	 * 大V成团人数
	 *
	 * 版本 >= 20141120
	 */
	 private long vPeopNum;

	/**
	 * 
	 *
	 * 版本 >= 20141120
	 */
	 private short vPeopNum_u;

	/**
	 * 大V团优惠价
	 *
	 * 版本 >= 20141120
	 */
	 private long vPromotePrice;

	/**
	 * 
	 *
	 * 版本 >= 20141120
	 */
	 private short vPromotePrice_u;

	/**
	 * COSS开始时间
	 *
	 * 版本 >= 20141120
	 */
	 private long cossStartTime;

	/**
	 * 
	 *
	 * 版本 >= 20141120
	 */
	 private short cossStartTime_u;

	/**
	 * COSS结束时间
	 *
	 * 版本 >= 20141120
	 */
	 private long cossEndTime;

	/**
	 * 
	 *
	 * 版本 >= 20141120
	 */
	 private short cossEndTime_u;

	/**
	 * 保留字段
	 *
	 * 版本 >= 20141120
	 */
	 private Map<String,String> ReserveIn = new HashMap<String,String>();

	/**
	 * 
	 *
	 * 版本 >= 20141120
	 */
	 private short ReserveIn_u;

	/**
	 * 市场价
	 *
	 * 版本 >= 20141215
	 */
	 private long marketPrice;

	/**
	 * 
	 *
	 * 版本 >= 20141215
	 */
	 private short marketPrice_u;

	/**
	 * 活动ID
	 *
	 * 版本 >= 20141215
	 */
	 private long activityId;

	/**
	 * 
	 *
	 * 版本 >= 20141215
	 */
	 private short activityId_u;

	/**
	 * 活动类型
	 *
	 * 版本 >= 20141215
	 */
	 private short activityKind;

	/**
	 * 
	 *
	 * 版本 >= 20141215
	 */
	 private short activityKind_u;

	/**
	 * 优惠价最大人数
	 *
	 * 版本 >= 20141222
	 */
	 private long vPromotePriceMaxOwnerNum;

	/**
	 * 
	 *
	 * 版本 >= 20141222
	 */
	 private short vPromotePriceMaxOwnerNum_u;

	/**
	 * 一人团价格
	 *
	 * 版本 >= 20150122
	 */
	 private long oneUserGroupPrice;

	/**
	 * 
	 *
	 * 版本 >= 20150122
	 */
	 private short oneUserGroupPrice_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushString(comodityId);
		bs.pushUByte(comodityId_u);
		bs.pushString(areaName);
		bs.pushUByte(areaName_u);
		bs.pushString(activeNum);
		bs.pushUByte(activeNum_u);
		bs.pushString(activeShortName);
		bs.pushUByte(activeShortName_u);
		bs.pushString(comodityDesc);
		bs.pushUByte(comodityDesc_u);
		bs.pushLong(stocks);
		bs.pushUByte(stocks_u);
		bs.pushString(comoditySmallPic);
		bs.pushUByte(comoditySmallPic_u);
		bs.pushLong(activePrice);
		bs.pushUByte(activePrice_u);
		bs.pushUInt(leastPeopNum);
		bs.pushUByte(leastPeopNum_u);
		bs.pushUInt(rank);
		bs.pushUByte(rank_u);
		bs.pushUInt(startTime);
		bs.pushUByte(startTime_u);
		bs.pushUInt(endTime);
		bs.pushUByte(endTime_u);
		bs.pushUInt(auditCreateTime);
		bs.pushUByte(auditCreateTime_u);
		bs.pushUInt(modifyTime);
		bs.pushUByte(modifyTime_u);
		bs.pushUByte(status);
		bs.pushUByte(status_u);
		bs.pushString(reserve);
		bs.pushUByte(reserve_u);
		if(  this.version >= 20141120 ){
				bs.pushUInt(vPrice);
		}
		if(  this.version >= 20141120 ){
				bs.pushUByte(vPrice_u);
		}
		if(  this.version >= 20141120 ){
				bs.pushUInt(vPeopNum);
		}
		if(  this.version >= 20141120 ){
				bs.pushUByte(vPeopNum_u);
		}
		if(  this.version >= 20141120 ){
				bs.pushUInt(vPromotePrice);
		}
		if(  this.version >= 20141120 ){
				bs.pushUByte(vPromotePrice_u);
		}
		if(  this.version >= 20141120 ){
				bs.pushUInt(cossStartTime);
		}
		if(  this.version >= 20141120 ){
				bs.pushUByte(cossStartTime_u);
		}
		if(  this.version >= 20141120 ){
				bs.pushUInt(cossEndTime);
		}
		if(  this.version >= 20141120 ){
				bs.pushUByte(cossEndTime_u);
		}
		if(  this.version >= 20141120 ){
				bs.pushObject(ReserveIn);
		}
		if(  this.version >= 20141120 ){
				bs.pushUByte(ReserveIn_u);
		}
		if(  this.version >= 20141215 ){
				bs.pushUInt(marketPrice);
		}
		if(  this.version >= 20141215 ){
				bs.pushUByte(marketPrice_u);
		}
		if(  this.version >= 20141215 ){
				bs.pushUInt(activityId);
		}
		if(  this.version >= 20141215 ){
				bs.pushUByte(activityId_u);
		}
		if(  this.version >= 20141215 ){
				bs.pushUByte(activityKind);
		}
		if(  this.version >= 20141215 ){
				bs.pushUByte(activityKind_u);
		}
		if(  this.version >= 20141222 ){
				bs.pushUInt(vPromotePriceMaxOwnerNum);
		}
		if(  this.version >= 20141222 ){
				bs.pushUByte(vPromotePriceMaxOwnerNum_u);
		}
		if(  this.version >= 20150122 ){
				bs.pushUInt(oneUserGroupPrice);
		}
		if(  this.version >= 20150122 ){
				bs.pushUByte(oneUserGroupPrice_u);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		comodityId = bs.popString();
		comodityId_u = bs.popUByte();
		areaName = bs.popString();
		areaName_u = bs.popUByte();
		activeNum = bs.popString();
		activeNum_u = bs.popUByte();
		activeShortName = bs.popString();
		activeShortName_u = bs.popUByte();
		comodityDesc = bs.popString();
		comodityDesc_u = bs.popUByte();
		stocks = bs.popLong();
		stocks_u = bs.popUByte();
		comoditySmallPic = bs.popString();
		comoditySmallPic_u = bs.popUByte();
		activePrice = bs.popLong();
		activePrice_u = bs.popUByte();
		leastPeopNum = bs.popUInt();
		leastPeopNum_u = bs.popUByte();
		rank = bs.popUInt();
		rank_u = bs.popUByte();
		startTime = bs.popUInt();
		startTime_u = bs.popUByte();
		endTime = bs.popUInt();
		endTime_u = bs.popUByte();
		auditCreateTime = bs.popUInt();
		auditCreateTime_u = bs.popUByte();
		modifyTime = bs.popUInt();
		modifyTime_u = bs.popUByte();
		status = bs.popUByte();
		status_u = bs.popUByte();
		reserve = bs.popString();
		reserve_u = bs.popUByte();
		if(  this.version >= 20141120 ){
				vPrice = bs.popUInt();
		}
		if(  this.version >= 20141120 ){
				vPrice_u = bs.popUByte();
		}
		if(  this.version >= 20141120 ){
				vPeopNum = bs.popUInt();
		}
		if(  this.version >= 20141120 ){
				vPeopNum_u = bs.popUByte();
		}
		if(  this.version >= 20141120 ){
				vPromotePrice = bs.popUInt();
		}
		if(  this.version >= 20141120 ){
				vPromotePrice_u = bs.popUByte();
		}
		if(  this.version >= 20141120 ){
				cossStartTime = bs.popUInt();
		}
		if(  this.version >= 20141120 ){
				cossStartTime_u = bs.popUByte();
		}
		if(  this.version >= 20141120 ){
				cossEndTime = bs.popUInt();
		}
		if(  this.version >= 20141120 ){
				cossEndTime_u = bs.popUByte();
		}
		if(  this.version >= 20141120 ){
				ReserveIn = (Map<String,String>)bs.popMap(String.class,String.class);
		}
		if(  this.version >= 20141120 ){
				ReserveIn_u = bs.popUByte();
		}
		if(  this.version >= 20141215 ){
				marketPrice = bs.popUInt();
		}
		if(  this.version >= 20141215 ){
				marketPrice_u = bs.popUByte();
		}
		if(  this.version >= 20141215 ){
				activityId = bs.popUInt();
		}
		if(  this.version >= 20141215 ){
				activityId_u = bs.popUByte();
		}
		if(  this.version >= 20141215 ){
				activityKind = bs.popUByte();
		}
		if(  this.version >= 20141215 ){
				activityKind_u = bs.popUByte();
		}
		if(  this.version >= 20141222 ){
				vPromotePriceMaxOwnerNum = bs.popUInt();
		}
		if(  this.version >= 20141222 ){
				vPromotePriceMaxOwnerNum_u = bs.popUByte();
		}
		if(  this.version >= 20150122 ){
				oneUserGroupPrice = bs.popUInt();
		}
		if(  this.version >= 20150122 ){
				oneUserGroupPrice_u = bs.popUByte();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取商品id
	 * 
	 * 此字段的版本 >= 0
	 * @return comodityId value 类型为:String
	 * 
	 */
	public String getComodityId()
	{
		return comodityId;
	}


	/**
	 * 设置商品id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setComodityId(String value)
	{
		this.comodityId = value;
		this.comodityId_u = 1;
	}

	public boolean issetComodityId()
	{
		return this.comodityId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return comodityId_u value 类型为:short
	 * 
	 */
	public short getComodityId_u()
	{
		return comodityId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setComodityId_u(short value)
	{
		this.comodityId_u = value;
	}


	/**
	 * 获取区域名称
	 * 
	 * 此字段的版本 >= 0
	 * @return areaName value 类型为:String
	 * 
	 */
	public String getAreaName()
	{
		return areaName;
	}


	/**
	 * 设置区域名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAreaName(String value)
	{
		this.areaName = value;
		this.areaName_u = 1;
	}

	public boolean issetAreaName()
	{
		return this.areaName_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return areaName_u value 类型为:short
	 * 
	 */
	public short getAreaName_u()
	{
		return areaName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAreaName_u(short value)
	{
		this.areaName_u = value;
	}


	/**
	 * 获取活动期数
	 * 
	 * 此字段的版本 >= 0
	 * @return activeNum value 类型为:String
	 * 
	 */
	public String getActiveNum()
	{
		return activeNum;
	}


	/**
	 * 设置活动期数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setActiveNum(String value)
	{
		this.activeNum = value;
		this.activeNum_u = 1;
	}

	public boolean issetActiveNum()
	{
		return this.activeNum_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return activeNum_u value 类型为:short
	 * 
	 */
	public short getActiveNum_u()
	{
		return activeNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setActiveNum_u(short value)
	{
		this.activeNum_u = value;
	}


	/**
	 * 获取商品活动短名
	 * 
	 * 此字段的版本 >= 0
	 * @return activeShortName value 类型为:String
	 * 
	 */
	public String getActiveShortName()
	{
		return activeShortName;
	}


	/**
	 * 设置商品活动短名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setActiveShortName(String value)
	{
		this.activeShortName = value;
		this.activeShortName_u = 1;
	}

	public boolean issetActiveShortName()
	{
		return this.activeShortName_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return activeShortName_u value 类型为:short
	 * 
	 */
	public short getActiveShortName_u()
	{
		return activeShortName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setActiveShortName_u(short value)
	{
		this.activeShortName_u = value;
	}


	/**
	 * 获取商品活动描述
	 * 
	 * 此字段的版本 >= 0
	 * @return comodityDesc value 类型为:String
	 * 
	 */
	public String getComodityDesc()
	{
		return comodityDesc;
	}


	/**
	 * 设置商品活动描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setComodityDesc(String value)
	{
		this.comodityDesc = value;
		this.comodityDesc_u = 1;
	}

	public boolean issetComodityDesc()
	{
		return this.comodityDesc_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return comodityDesc_u value 类型为:short
	 * 
	 */
	public short getComodityDesc_u()
	{
		return comodityDesc_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setComodityDesc_u(short value)
	{
		this.comodityDesc_u = value;
	}


	/**
	 * 获取商品原始库存
	 * 
	 * 此字段的版本 >= 0
	 * @return stocks value 类型为:long
	 * 
	 */
	public long getStocks()
	{
		return stocks;
	}


	/**
	 * 设置商品原始库存
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setStocks(long value)
	{
		this.stocks = value;
		this.stocks_u = 1;
	}

	public boolean issetStocks()
	{
		return this.stocks_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return stocks_u value 类型为:short
	 * 
	 */
	public short getStocks_u()
	{
		return stocks_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStocks_u(short value)
	{
		this.stocks_u = value;
	}


	/**
	 * 获取商品活动图片URL
	 * 
	 * 此字段的版本 >= 0
	 * @return comoditySmallPic value 类型为:String
	 * 
	 */
	public String getComoditySmallPic()
	{
		return comoditySmallPic;
	}


	/**
	 * 设置商品活动图片URL
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setComoditySmallPic(String value)
	{
		this.comoditySmallPic = value;
		this.comoditySmallPic_u = 1;
	}

	public boolean issetComoditySmallPic()
	{
		return this.comoditySmallPic_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return comoditySmallPic_u value 类型为:short
	 * 
	 */
	public short getComoditySmallPic_u()
	{
		return comoditySmallPic_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setComoditySmallPic_u(short value)
	{
		this.comoditySmallPic_u = value;
	}


	/**
	 * 获取商品活动价格
	 * 
	 * 此字段的版本 >= 0
	 * @return activePrice value 类型为:long
	 * 
	 */
	public long getActivePrice()
	{
		return activePrice;
	}


	/**
	 * 设置商品活动价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActivePrice(long value)
	{
		this.activePrice = value;
		this.activePrice_u = 1;
	}

	public boolean issetActivePrice()
	{
		return this.activePrice_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return activePrice_u value 类型为:short
	 * 
	 */
	public short getActivePrice_u()
	{
		return activePrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setActivePrice_u(short value)
	{
		this.activePrice_u = value;
	}


	/**
	 * 获取成团人数
	 * 
	 * 此字段的版本 >= 0
	 * @return leastPeopNum value 类型为:long
	 * 
	 */
	public long getLeastPeopNum()
	{
		return leastPeopNum;
	}


	/**
	 * 设置成团人数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLeastPeopNum(long value)
	{
		this.leastPeopNum = value;
		this.leastPeopNum_u = 1;
	}

	public boolean issetLeastPeopNum()
	{
		return this.leastPeopNum_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return leastPeopNum_u value 类型为:short
	 * 
	 */
	public short getLeastPeopNum_u()
	{
		return leastPeopNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLeastPeopNum_u(short value)
	{
		this.leastPeopNum_u = value;
	}


	/**
	 * 获取排序rank
	 * 
	 * 此字段的版本 >= 0
	 * @return rank value 类型为:long
	 * 
	 */
	public long getRank()
	{
		return rank;
	}


	/**
	 * 设置排序rank
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRank(long value)
	{
		this.rank = value;
		this.rank_u = 1;
	}

	public boolean issetRank()
	{
		return this.rank_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return rank_u value 类型为:short
	 * 
	 */
	public short getRank_u()
	{
		return rank_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRank_u(short value)
	{
		this.rank_u = value;
	}


	/**
	 * 获取本次活动开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @return startTime value 类型为:long
	 * 
	 */
	public long getStartTime()
	{
		return startTime;
	}


	/**
	 * 设置本次活动开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setStartTime(long value)
	{
		this.startTime = value;
		this.startTime_u = 1;
	}

	public boolean issetStartTime()
	{
		return this.startTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return startTime_u value 类型为:short
	 * 
	 */
	public short getStartTime_u()
	{
		return startTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStartTime_u(short value)
	{
		this.startTime_u = value;
	}


	/**
	 * 获取本次活动结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return endTime value 类型为:long
	 * 
	 */
	public long getEndTime()
	{
		return endTime;
	}


	/**
	 * 设置本次活动结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEndTime(long value)
	{
		this.endTime = value;
		this.endTime_u = 1;
	}

	public boolean issetEndTime()
	{
		return this.endTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return endTime_u value 类型为:short
	 * 
	 */
	public short getEndTime_u()
	{
		return endTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEndTime_u(short value)
	{
		this.endTime_u = value;
	}


	/**
	 * 获取该审核数据写入时间
	 * 
	 * 此字段的版本 >= 0
	 * @return auditCreateTime value 类型为:long
	 * 
	 */
	public long getAuditCreateTime()
	{
		return auditCreateTime;
	}


	/**
	 * 设置该审核数据写入时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAuditCreateTime(long value)
	{
		this.auditCreateTime = value;
		this.auditCreateTime_u = 1;
	}

	public boolean issetAuditCreateTime()
	{
		return this.auditCreateTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return auditCreateTime_u value 类型为:short
	 * 
	 */
	public short getAuditCreateTime_u()
	{
		return auditCreateTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAuditCreateTime_u(short value)
	{
		this.auditCreateTime_u = value;
	}


	/**
	 * 获取该审核最新修改时间
	 * 
	 * 此字段的版本 >= 0
	 * @return modifyTime value 类型为:long
	 * 
	 */
	public long getModifyTime()
	{
		return modifyTime;
	}


	/**
	 * 设置该审核最新修改时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setModifyTime(long value)
	{
		this.modifyTime = value;
		this.modifyTime_u = 1;
	}

	public boolean issetModifyTime()
	{
		return this.modifyTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return modifyTime_u value 类型为:short
	 * 
	 */
	public short getModifyTime_u()
	{
		return modifyTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setModifyTime_u(short value)
	{
		this.modifyTime_u = value;
	}


	/**
	 * 获取coss记录状态, 0: 待审核，1: 初审通过，2: 终审通过, 3: 通过
	 * 
	 * 此字段的版本 >= 0
	 * @return status value 类型为:short
	 * 
	 */
	public short getStatus()
	{
		return status;
	}


	/**
	 * 设置coss记录状态, 0: 待审核，1: 初审通过，2: 终审通过, 3: 通过
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStatus(short value)
	{
		this.status = value;
		this.status_u = 1;
	}

	public boolean issetStatus()
	{
		return this.status_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return status_u value 类型为:short
	 * 
	 */
	public short getStatus_u()
	{
		return status_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStatus_u(short value)
	{
		this.status_u = value;
	}


	/**
	 * 获取保留数据字段，备后用
	 * 
	 * 此字段的版本 >= 0
	 * @return reserve value 类型为:String
	 * 
	 */
	public String getReserve()
	{
		return reserve;
	}


	/**
	 * 设置保留数据字段，备后用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserve(String value)
	{
		this.reserve = value;
		this.reserve_u = 1;
	}

	public boolean issetReserve()
	{
		return this.reserve_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return reserve_u value 类型为:short
	 * 
	 */
	public short getReserve_u()
	{
		return reserve_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserve_u(short value)
	{
		this.reserve_u = value;
	}


	/**
	 * 获取大V团价
	 * 
	 * 此字段的版本 >= 20141120
	 * @return vPrice value 类型为:long
	 * 
	 */
	public long getVPrice()
	{
		return vPrice;
	}


	/**
	 * 设置大V团价
	 * 
	 * 此字段的版本 >= 20141120
	 * @param  value 类型为:long
	 * 
	 */
	public void setVPrice(long value)
	{
		this.vPrice = value;
		this.vPrice_u = 1;
	}

	public boolean issetVPrice()
	{
		return this.vPrice_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141120
	 * @return vPrice_u value 类型为:short
	 * 
	 */
	public short getVPrice_u()
	{
		return vPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141120
	 * @param  value 类型为:short
	 * 
	 */
	public void setVPrice_u(short value)
	{
		this.vPrice_u = value;
	}


	/**
	 * 获取大V成团人数
	 * 
	 * 此字段的版本 >= 20141120
	 * @return vPeopNum value 类型为:long
	 * 
	 */
	public long getVPeopNum()
	{
		return vPeopNum;
	}


	/**
	 * 设置大V成团人数
	 * 
	 * 此字段的版本 >= 20141120
	 * @param  value 类型为:long
	 * 
	 */
	public void setVPeopNum(long value)
	{
		this.vPeopNum = value;
		this.vPeopNum_u = 1;
	}

	public boolean issetVPeopNum()
	{
		return this.vPeopNum_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141120
	 * @return vPeopNum_u value 类型为:short
	 * 
	 */
	public short getVPeopNum_u()
	{
		return vPeopNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141120
	 * @param  value 类型为:short
	 * 
	 */
	public void setVPeopNum_u(short value)
	{
		this.vPeopNum_u = value;
	}


	/**
	 * 获取大V团优惠价
	 * 
	 * 此字段的版本 >= 20141120
	 * @return vPromotePrice value 类型为:long
	 * 
	 */
	public long getVPromotePrice()
	{
		return vPromotePrice;
	}


	/**
	 * 设置大V团优惠价
	 * 
	 * 此字段的版本 >= 20141120
	 * @param  value 类型为:long
	 * 
	 */
	public void setVPromotePrice(long value)
	{
		this.vPromotePrice = value;
		this.vPromotePrice_u = 1;
	}

	public boolean issetVPromotePrice()
	{
		return this.vPromotePrice_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141120
	 * @return vPromotePrice_u value 类型为:short
	 * 
	 */
	public short getVPromotePrice_u()
	{
		return vPromotePrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141120
	 * @param  value 类型为:short
	 * 
	 */
	public void setVPromotePrice_u(short value)
	{
		this.vPromotePrice_u = value;
	}


	/**
	 * 获取COSS开始时间
	 * 
	 * 此字段的版本 >= 20141120
	 * @return cossStartTime value 类型为:long
	 * 
	 */
	public long getCossStartTime()
	{
		return cossStartTime;
	}


	/**
	 * 设置COSS开始时间
	 * 
	 * 此字段的版本 >= 20141120
	 * @param  value 类型为:long
	 * 
	 */
	public void setCossStartTime(long value)
	{
		this.cossStartTime = value;
		this.cossStartTime_u = 1;
	}

	public boolean issetCossStartTime()
	{
		return this.cossStartTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141120
	 * @return cossStartTime_u value 类型为:short
	 * 
	 */
	public short getCossStartTime_u()
	{
		return cossStartTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141120
	 * @param  value 类型为:short
	 * 
	 */
	public void setCossStartTime_u(short value)
	{
		this.cossStartTime_u = value;
	}


	/**
	 * 获取COSS结束时间
	 * 
	 * 此字段的版本 >= 20141120
	 * @return cossEndTime value 类型为:long
	 * 
	 */
	public long getCossEndTime()
	{
		return cossEndTime;
	}


	/**
	 * 设置COSS结束时间
	 * 
	 * 此字段的版本 >= 20141120
	 * @param  value 类型为:long
	 * 
	 */
	public void setCossEndTime(long value)
	{
		this.cossEndTime = value;
		this.cossEndTime_u = 1;
	}

	public boolean issetCossEndTime()
	{
		return this.cossEndTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141120
	 * @return cossEndTime_u value 类型为:short
	 * 
	 */
	public short getCossEndTime_u()
	{
		return cossEndTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141120
	 * @param  value 类型为:short
	 * 
	 */
	public void setCossEndTime_u(short value)
	{
		this.cossEndTime_u = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 20141120
	 * @return ReserveIn value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 20141120
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setReserveIn(Map<String,String> value)
	{
		if (value != null) {
				this.ReserveIn = value;
				this.ReserveIn_u = 1;
		}
	}

	public boolean issetReserveIn()
	{
		return this.ReserveIn_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141120
	 * @return ReserveIn_u value 类型为:short
	 * 
	 */
	public short getReserveIn_u()
	{
		return ReserveIn_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141120
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserveIn_u(short value)
	{
		this.ReserveIn_u = value;
	}


	/**
	 * 获取市场价
	 * 
	 * 此字段的版本 >= 20141215
	 * @return marketPrice value 类型为:long
	 * 
	 */
	public long getMarketPrice()
	{
		return marketPrice;
	}


	/**
	 * 设置市场价
	 * 
	 * 此字段的版本 >= 20141215
	 * @param  value 类型为:long
	 * 
	 */
	public void setMarketPrice(long value)
	{
		this.marketPrice = value;
		this.marketPrice_u = 1;
	}

	public boolean issetMarketPrice()
	{
		return this.marketPrice_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141215
	 * @return marketPrice_u value 类型为:short
	 * 
	 */
	public short getMarketPrice_u()
	{
		return marketPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141215
	 * @param  value 类型为:short
	 * 
	 */
	public void setMarketPrice_u(short value)
	{
		this.marketPrice_u = value;
	}


	/**
	 * 获取活动ID
	 * 
	 * 此字段的版本 >= 20141215
	 * @return activityId value 类型为:long
	 * 
	 */
	public long getActivityId()
	{
		return activityId;
	}


	/**
	 * 设置活动ID
	 * 
	 * 此字段的版本 >= 20141215
	 * @param  value 类型为:long
	 * 
	 */
	public void setActivityId(long value)
	{
		this.activityId = value;
		this.activityId_u = 1;
	}

	public boolean issetActivityId()
	{
		return this.activityId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141215
	 * @return activityId_u value 类型为:short
	 * 
	 */
	public short getActivityId_u()
	{
		return activityId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141215
	 * @param  value 类型为:short
	 * 
	 */
	public void setActivityId_u(short value)
	{
		this.activityId_u = value;
	}


	/**
	 * 获取活动类型
	 * 
	 * 此字段的版本 >= 20141215
	 * @return activityKind value 类型为:short
	 * 
	 */
	public short getActivityKind()
	{
		return activityKind;
	}


	/**
	 * 设置活动类型
	 * 
	 * 此字段的版本 >= 20141215
	 * @param  value 类型为:short
	 * 
	 */
	public void setActivityKind(short value)
	{
		this.activityKind = value;
		this.activityKind_u = 1;
	}

	public boolean issetActivityKind()
	{
		return this.activityKind_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141215
	 * @return activityKind_u value 类型为:short
	 * 
	 */
	public short getActivityKind_u()
	{
		return activityKind_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141215
	 * @param  value 类型为:short
	 * 
	 */
	public void setActivityKind_u(short value)
	{
		this.activityKind_u = value;
	}


	/**
	 * 获取优惠价最大人数
	 * 
	 * 此字段的版本 >= 20141222
	 * @return vPromotePriceMaxOwnerNum value 类型为:long
	 * 
	 */
	public long getVPromotePriceMaxOwnerNum()
	{
		return vPromotePriceMaxOwnerNum;
	}


	/**
	 * 设置优惠价最大人数
	 * 
	 * 此字段的版本 >= 20141222
	 * @param  value 类型为:long
	 * 
	 */
	public void setVPromotePriceMaxOwnerNum(long value)
	{
		this.vPromotePriceMaxOwnerNum = value;
		this.vPromotePriceMaxOwnerNum_u = 1;
	}

	public boolean issetVPromotePriceMaxOwnerNum()
	{
		return this.vPromotePriceMaxOwnerNum_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141222
	 * @return vPromotePriceMaxOwnerNum_u value 类型为:short
	 * 
	 */
	public short getVPromotePriceMaxOwnerNum_u()
	{
		return vPromotePriceMaxOwnerNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141222
	 * @param  value 类型为:short
	 * 
	 */
	public void setVPromotePriceMaxOwnerNum_u(short value)
	{
		this.vPromotePriceMaxOwnerNum_u = value;
	}


	/**
	 * 获取一人团价格
	 * 
	 * 此字段的版本 >= 20150122
	 * @return oneUserGroupPrice value 类型为:long
	 * 
	 */
	public long getOneUserGroupPrice()
	{
		return oneUserGroupPrice;
	}


	/**
	 * 设置一人团价格
	 * 
	 * 此字段的版本 >= 20150122
	 * @param  value 类型为:long
	 * 
	 */
	public void setOneUserGroupPrice(long value)
	{
		this.oneUserGroupPrice = value;
		this.oneUserGroupPrice_u = 1;
	}

	public boolean issetOneUserGroupPrice()
	{
		return this.oneUserGroupPrice_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20150122
	 * @return oneUserGroupPrice_u value 类型为:short
	 * 
	 */
	public short getOneUserGroupPrice_u()
	{
		return oneUserGroupPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20150122
	 * @param  value 类型为:short
	 * 
	 */
	public void setOneUserGroupPrice_u(short value)
	{
		this.oneUserGroupPrice_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(TTJAuditItemPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(comodityId, null);  //计算字段comodityId的长度 size_of(String)
				length += 1;  //计算字段comodityId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(areaName, null);  //计算字段areaName的长度 size_of(String)
				length += 1;  //计算字段areaName_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(activeNum, null);  //计算字段activeNum的长度 size_of(String)
				length += 1;  //计算字段activeNum_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(activeShortName, null);  //计算字段activeShortName的长度 size_of(String)
				length += 1;  //计算字段activeShortName_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(comodityDesc, null);  //计算字段comodityDesc的长度 size_of(String)
				length += 1;  //计算字段comodityDesc_u的长度 size_of(uint8_t)
				length += 17;  //计算字段stocks的长度 size_of(uint64_t)
				length += 1;  //计算字段stocks_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(comoditySmallPic, null);  //计算字段comoditySmallPic的长度 size_of(String)
				length += 1;  //计算字段comoditySmallPic_u的长度 size_of(uint8_t)
				length += 17;  //计算字段activePrice的长度 size_of(uint64_t)
				length += 1;  //计算字段activePrice_u的长度 size_of(uint8_t)
				length += 4;  //计算字段leastPeopNum的长度 size_of(uint32_t)
				length += 1;  //计算字段leastPeopNum_u的长度 size_of(uint8_t)
				length += 4;  //计算字段rank的长度 size_of(uint32_t)
				length += 1;  //计算字段rank_u的长度 size_of(uint8_t)
				length += 4;  //计算字段startTime的长度 size_of(uint32_t)
				length += 1;  //计算字段startTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段endTime的长度 size_of(uint32_t)
				length += 1;  //计算字段endTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段auditCreateTime的长度 size_of(uint32_t)
				length += 1;  //计算字段auditCreateTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段modifyTime的长度 size_of(uint32_t)
				length += 1;  //计算字段modifyTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段status的长度 size_of(uint8_t)
				length += 1;  //计算字段status_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(reserve, null);  //计算字段reserve的长度 size_of(String)
				length += 1;  //计算字段reserve_u的长度 size_of(uint8_t)
				if(  this.version >= 20141120 ){
						length += 4;  //计算字段vPrice的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141120 ){
						length += 1;  //计算字段vPrice_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141120 ){
						length += 4;  //计算字段vPeopNum的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141120 ){
						length += 1;  //计算字段vPeopNum_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141120 ){
						length += 4;  //计算字段vPromotePrice的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141120 ){
						length += 1;  //计算字段vPromotePrice_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141120 ){
						length += 4;  //计算字段cossStartTime的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141120 ){
						length += 1;  //计算字段cossStartTime_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141120 ){
						length += 4;  //计算字段cossEndTime的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141120 ){
						length += 1;  //计算字段cossEndTime_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141120 ){
						length += ByteStream.getObjectSize(ReserveIn, null);  //计算字段ReserveIn的长度 size_of(Map)
				}
				if(  this.version >= 20141120 ){
						length += 1;  //计算字段ReserveIn_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141215 ){
						length += 4;  //计算字段marketPrice的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141215 ){
						length += 1;  //计算字段marketPrice_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141215 ){
						length += 4;  //计算字段activityId的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141215 ){
						length += 1;  //计算字段activityId_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141215 ){
						length += 1;  //计算字段activityKind的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141215 ){
						length += 1;  //计算字段activityKind_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141222 ){
						length += 4;  //计算字段vPromotePriceMaxOwnerNum的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141222 ){
						length += 1;  //计算字段vPromotePriceMaxOwnerNum_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20150122 ){
						length += 4;  //计算字段oneUserGroupPrice的长度 size_of(uint32_t)
				}
				if(  this.version >= 20150122 ){
						length += 1;  //计算字段oneUserGroupPrice_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(TTJAuditItemPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(comodityId, encoding);  //计算字段comodityId的长度 size_of(String)
				length += 1;  //计算字段comodityId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(areaName, encoding);  //计算字段areaName的长度 size_of(String)
				length += 1;  //计算字段areaName_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(activeNum, encoding);  //计算字段activeNum的长度 size_of(String)
				length += 1;  //计算字段activeNum_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(activeShortName, encoding);  //计算字段activeShortName的长度 size_of(String)
				length += 1;  //计算字段activeShortName_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(comodityDesc, encoding);  //计算字段comodityDesc的长度 size_of(String)
				length += 1;  //计算字段comodityDesc_u的长度 size_of(uint8_t)
				length += 17;  //计算字段stocks的长度 size_of(uint64_t)
				length += 1;  //计算字段stocks_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(comoditySmallPic, encoding);  //计算字段comoditySmallPic的长度 size_of(String)
				length += 1;  //计算字段comoditySmallPic_u的长度 size_of(uint8_t)
				length += 17;  //计算字段activePrice的长度 size_of(uint64_t)
				length += 1;  //计算字段activePrice_u的长度 size_of(uint8_t)
				length += 4;  //计算字段leastPeopNum的长度 size_of(uint32_t)
				length += 1;  //计算字段leastPeopNum_u的长度 size_of(uint8_t)
				length += 4;  //计算字段rank的长度 size_of(uint32_t)
				length += 1;  //计算字段rank_u的长度 size_of(uint8_t)
				length += 4;  //计算字段startTime的长度 size_of(uint32_t)
				length += 1;  //计算字段startTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段endTime的长度 size_of(uint32_t)
				length += 1;  //计算字段endTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段auditCreateTime的长度 size_of(uint32_t)
				length += 1;  //计算字段auditCreateTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段modifyTime的长度 size_of(uint32_t)
				length += 1;  //计算字段modifyTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段status的长度 size_of(uint8_t)
				length += 1;  //计算字段status_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(reserve, encoding);  //计算字段reserve的长度 size_of(String)
				length += 1;  //计算字段reserve_u的长度 size_of(uint8_t)
				if(  this.version >= 20141120 ){
						length += 4;  //计算字段vPrice的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141120 ){
						length += 1;  //计算字段vPrice_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141120 ){
						length += 4;  //计算字段vPeopNum的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141120 ){
						length += 1;  //计算字段vPeopNum_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141120 ){
						length += 4;  //计算字段vPromotePrice的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141120 ){
						length += 1;  //计算字段vPromotePrice_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141120 ){
						length += 4;  //计算字段cossStartTime的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141120 ){
						length += 1;  //计算字段cossStartTime_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141120 ){
						length += 4;  //计算字段cossEndTime的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141120 ){
						length += 1;  //计算字段cossEndTime_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141120 ){
						length += ByteStream.getObjectSize(ReserveIn, encoding);  //计算字段ReserveIn的长度 size_of(Map)
				}
				if(  this.version >= 20141120 ){
						length += 1;  //计算字段ReserveIn_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141215 ){
						length += 4;  //计算字段marketPrice的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141215 ){
						length += 1;  //计算字段marketPrice_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141215 ){
						length += 4;  //计算字段activityId的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141215 ){
						length += 1;  //计算字段activityId_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141215 ){
						length += 1;  //计算字段activityKind的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141215 ){
						length += 1;  //计算字段activityKind_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141222 ){
						length += 4;  //计算字段vPromotePriceMaxOwnerNum的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141222 ){
						length += 1;  //计算字段vPromotePriceMaxOwnerNum_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20150122 ){
						length += 4;  //计算字段oneUserGroupPrice的长度 size_of(uint32_t)
				}
				if(  this.version >= 20150122 ){
						length += 1;  //计算字段oneUserGroupPrice_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本20150122所包含的字段*******
 *	long version;///<协议版本号
 *	short version_u;
 *	String comodityId;///<商品id
 *	short comodityId_u;
 *	String areaName;///<区域名称
 *	short areaName_u;
 *	String activeNum;///<活动期数
 *	short activeNum_u;
 *	String activeShortName;///<商品活动短名
 *	short activeShortName_u;
 *	String comodityDesc;///<商品活动描述
 *	short comodityDesc_u;
 *	long stocks;///<商品原始库存
 *	short stocks_u;
 *	String comoditySmallPic;///<商品活动图片URL
 *	short comoditySmallPic_u;
 *	long activePrice;///<商品活动价格
 *	short activePrice_u;
 *	long leastPeopNum;///<成团人数
 *	short leastPeopNum_u;
 *	long rank;///<排序rank
 *	short rank_u;
 *	long startTime;///<本次活动开始时间
 *	short startTime_u;
 *	long endTime;///<本次活动结束时间
 *	short endTime_u;
 *	long auditCreateTime;///<该审核数据写入时间
 *	short auditCreateTime_u;
 *	long modifyTime;///<该审核最新修改时间
 *	short modifyTime_u;
 *	short status;///<coss记录状态, 0: 待审核，1: 初审通过，2: 终审通过, 3: 通过
 *	short status_u;
 *	String reserve;///<保留数据字段，备后用
 *	short reserve_u;
 *	long vPrice;///<大V团价
 *	short vPrice_u;
 *	long vPeopNum;///<大V成团人数
 *	short vPeopNum_u;
 *	long vPromotePrice;///<大V团优惠价
 *	short vPromotePrice_u;
 *	long cossStartTime;///<COSS开始时间
 *	short cossStartTime_u;
 *	long cossEndTime;///<COSS结束时间
 *	short cossEndTime_u;
 *	Map<String,String> ReserveIn;///<保留字段
 *	short ReserveIn_u;
 *	long marketPrice;///<市场价
 *	short marketPrice_u;
 *	long activityId;///<活动ID
 *	short activityId_u;
 *	short activityKind;///<活动类型
 *	short activityKind_u;
 *	long vPromotePriceMaxOwnerNum;///<优惠价最大人数
 *	short vPromotePriceMaxOwnerNum_u;
 *	long oneUserGroupPrice;///<一人团价格
 *	short oneUserGroupPrice_u;
 *****以上是版本20150122所包含的字段*******
 *
 *****以下是版本20141222所包含的字段*******
 *	long version;///<协议版本号
 *	short version_u;
 *	String comodityId;///<商品id
 *	short comodityId_u;
 *	String areaName;///<区域名称
 *	short areaName_u;
 *	String activeNum;///<活动期数
 *	short activeNum_u;
 *	String activeShortName;///<商品活动短名
 *	short activeShortName_u;
 *	String comodityDesc;///<商品活动描述
 *	short comodityDesc_u;
 *	long stocks;///<商品原始库存
 *	short stocks_u;
 *	String comoditySmallPic;///<商品活动图片URL
 *	short comoditySmallPic_u;
 *	long activePrice;///<商品活动价格
 *	short activePrice_u;
 *	long leastPeopNum;///<成团人数
 *	short leastPeopNum_u;
 *	long rank;///<排序rank
 *	short rank_u;
 *	long startTime;///<本次活动开始时间
 *	short startTime_u;
 *	long endTime;///<本次活动结束时间
 *	short endTime_u;
 *	long auditCreateTime;///<该审核数据写入时间
 *	short auditCreateTime_u;
 *	long modifyTime;///<该审核最新修改时间
 *	short modifyTime_u;
 *	short status;///<coss记录状态, 0: 待审核，1: 初审通过，2: 终审通过, 3: 通过
 *	short status_u;
 *	String reserve;///<保留数据字段，备后用
 *	short reserve_u;
 *	long vPrice;///<大V团价
 *	short vPrice_u;
 *	long vPeopNum;///<大V成团人数
 *	short vPeopNum_u;
 *	long vPromotePrice;///<大V团优惠价
 *	short vPromotePrice_u;
 *	long cossStartTime;///<COSS开始时间
 *	short cossStartTime_u;
 *	long cossEndTime;///<COSS结束时间
 *	short cossEndTime_u;
 *	Map<String,String> ReserveIn;///<保留字段
 *	short ReserveIn_u;
 *	long marketPrice;///<市场价
 *	short marketPrice_u;
 *	long activityId;///<活动ID
 *	short activityId_u;
 *	short activityKind;///<活动类型
 *	short activityKind_u;
 *	long vPromotePriceMaxOwnerNum;///<优惠价最大人数
 *	short vPromotePriceMaxOwnerNum_u;
 *****以上是版本20141222所包含的字段*******
 *
 *****以下是版本20141215所包含的字段*******
 *	long version;///<协议版本号
 *	short version_u;
 *	String comodityId;///<商品id
 *	short comodityId_u;
 *	String areaName;///<区域名称
 *	short areaName_u;
 *	String activeNum;///<活动期数
 *	short activeNum_u;
 *	String activeShortName;///<商品活动短名
 *	short activeShortName_u;
 *	String comodityDesc;///<商品活动描述
 *	short comodityDesc_u;
 *	long stocks;///<商品原始库存
 *	short stocks_u;
 *	String comoditySmallPic;///<商品活动图片URL
 *	short comoditySmallPic_u;
 *	long activePrice;///<商品活动价格
 *	short activePrice_u;
 *	long leastPeopNum;///<成团人数
 *	short leastPeopNum_u;
 *	long rank;///<排序rank
 *	short rank_u;
 *	long startTime;///<本次活动开始时间
 *	short startTime_u;
 *	long endTime;///<本次活动结束时间
 *	short endTime_u;
 *	long auditCreateTime;///<该审核数据写入时间
 *	short auditCreateTime_u;
 *	long modifyTime;///<该审核最新修改时间
 *	short modifyTime_u;
 *	short status;///<coss记录状态, 0: 待审核，1: 初审通过，2: 终审通过, 3: 通过
 *	short status_u;
 *	String reserve;///<保留数据字段，备后用
 *	short reserve_u;
 *	long vPrice;///<大V团价
 *	short vPrice_u;
 *	long vPeopNum;///<大V成团人数
 *	short vPeopNum_u;
 *	long vPromotePrice;///<大V团优惠价
 *	short vPromotePrice_u;
 *	long cossStartTime;///<COSS开始时间
 *	short cossStartTime_u;
 *	long cossEndTime;///<COSS结束时间
 *	short cossEndTime_u;
 *	Map<String,String> ReserveIn;///<保留字段
 *	short ReserveIn_u;
 *	long marketPrice;///<市场价
 *	short marketPrice_u;
 *	long activityId;///<活动ID
 *	short activityId_u;
 *	short activityKind;///<活动类型
 *	short activityKind_u;
 *****以上是版本20141215所包含的字段*******
 *
 *****以下是版本20141120所包含的字段*******
 *	long version;///<协议版本号
 *	short version_u;
 *	String comodityId;///<商品id
 *	short comodityId_u;
 *	String areaName;///<区域名称
 *	short areaName_u;
 *	String activeNum;///<活动期数
 *	short activeNum_u;
 *	String activeShortName;///<商品活动短名
 *	short activeShortName_u;
 *	String comodityDesc;///<商品活动描述
 *	short comodityDesc_u;
 *	long stocks;///<商品原始库存
 *	short stocks_u;
 *	String comoditySmallPic;///<商品活动图片URL
 *	short comoditySmallPic_u;
 *	long activePrice;///<商品活动价格
 *	short activePrice_u;
 *	long leastPeopNum;///<成团人数
 *	short leastPeopNum_u;
 *	long rank;///<排序rank
 *	short rank_u;
 *	long startTime;///<本次活动开始时间
 *	short startTime_u;
 *	long endTime;///<本次活动结束时间
 *	short endTime_u;
 *	long auditCreateTime;///<该审核数据写入时间
 *	short auditCreateTime_u;
 *	long modifyTime;///<该审核最新修改时间
 *	short modifyTime_u;
 *	short status;///<coss记录状态, 0: 待审核，1: 初审通过，2: 终审通过, 3: 通过
 *	short status_u;
 *	String reserve;///<保留数据字段，备后用
 *	short reserve_u;
 *	long vPrice;///<大V团价
 *	short vPrice_u;
 *	long vPeopNum;///<大V成团人数
 *	short vPeopNum_u;
 *	long vPromotePrice;///<大V团优惠价
 *	short vPromotePrice_u;
 *	long cossStartTime;///<COSS开始时间
 *	short cossStartTime_u;
 *	long cossEndTime;///<COSS结束时间
 *	short cossEndTime_u;
 *	Map<String,String> ReserveIn;///<保留字段
 *	short ReserveIn_u;
 *****以上是版本20141120所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
