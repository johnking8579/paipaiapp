//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: tuantuanjiang.idl.GetTuanByTuanIdResp.java

package com.qq.qqbuy.gchoice.idl;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import java.util.Map;
import java.util.Vector;
import java.util.HashMap;

/**
 *团信息
 *
 *@date 2015-03-04 10:57:53
 *
 *@since version:0
*/
public class TuanInfoBo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version = 20141028;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 团id
	 *
	 * 版本 >= 0
	 */
	 private long TuanId;

	/**
	 * 版本 >= 0
	 */
	 private short TuanId_u;

	/**
	 * 团状态 1.未激活2.已激活3.已成团4.已过期5.已履单6.已撤团
	 *
	 * 版本 >= 0
	 */
	 private long Status;

	/**
	 * 版本 >= 0
	 */
	 private short Status_u;

	/**
	 * 团成员数量上限
	 *
	 * 版本 >= 0
	 */
	 private long TuanMemberNum;

	/**
	 * 版本 >= 0
	 */
	 private short TuanMemberNum_u;

	/**
	 * 开始时间
	 *
	 * 版本 >= 0
	 */
	 private long CreateTime;

	/**
	 * 版本 >= 0
	 */
	 private short CreateTime_u;

	/**
	 * 结束时间
	 *
	 * 版本 >= 0
	 */
	 private long EndTime;

	/**
	 * 版本 >= 0
	 */
	 private short EndTime_u;

	/**
	 * 服务器当时时间
	 *
	 * 版本 >= 0
	 */
	 private long ServerTime;

	/**
	 * 版本 >= 0
	 */
	 private short ServerTime_u;

	/**
	 * 团商品信息
	 *
	 * 版本 >= 0
	 */
	 private TuanProductBo Product = new TuanProductBo();

	/**
	 * 版本 >= 0
	 */
	 private short Product_u;

	/**
	 * 团长序号，默认为0
	 *
	 * 版本 >= 0
	 */
	 private long CreatorIndex;

	/**
	 * 版本 >= 0
	 */
	 private short CreatorIndex_u;

	/**
	 * 已参团成员列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<TuanMemberBo> TuanMembers = new Vector<TuanMemberBo>();

	/**
	 * 版本 >= 0
	 */
	 private short TuanMembers_u;

	/**
	 * 参团成员数量
	 *
	 * 版本 >= 20141028
	 */
	 private long TuanJoinMemberNum;

	/**
	 * 
	 *
	 * 版本 >= 20141028
	 */
	 private short TuanJoinMemberNum_u;

	/**
	 * 团剩余人数
	 *
	 * 版本 >= 20141028
	 */
	 private long TuanLastMemberNum;

	/**
	 * 
	 *
	 * 版本 >= 20141028
	 */
	 private short TuanLastMemberNum_u;

	/**
	 * 保留输入
	 *
	 * 版本 >= 20141028
	 */
	 private Map<String,String> ReserveIn = new HashMap<String,String>();

	/**
	 * 
	 *
	 * 版本 >= 20141028
	 */
	 private short ReserveIn_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushLong(TuanId);
		bs.pushUByte(TuanId_u);
		bs.pushUInt(Status);
		bs.pushUByte(Status_u);
		bs.pushUInt(TuanMemberNum);
		bs.pushUByte(TuanMemberNum_u);
		bs.pushUInt(CreateTime);
		bs.pushUByte(CreateTime_u);
		bs.pushUInt(EndTime);
		bs.pushUByte(EndTime_u);
		bs.pushUInt(ServerTime);
		bs.pushUByte(ServerTime_u);
		bs.pushObject(Product);
		bs.pushUByte(Product_u);
		bs.pushUInt(CreatorIndex);
		bs.pushUByte(CreatorIndex_u);
		bs.pushObject(TuanMembers);
		bs.pushUByte(TuanMembers_u);
		if(  this.Version >= 20141028 ){
				bs.pushUInt(TuanJoinMemberNum);
		}
		if(  this.Version >= 20141028 ){
				bs.pushUByte(TuanJoinMemberNum_u);
		}
		if(  this.Version >= 20141028 ){
				bs.pushUInt(TuanLastMemberNum);
		}
		if(  this.Version >= 20141028 ){
				bs.pushUByte(TuanLastMemberNum_u);
		}
		if(  this.Version >= 20141028 ){
				bs.pushObject(ReserveIn);
		}
		if(  this.Version >= 20141028 ){
				bs.pushUByte(ReserveIn_u);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		TuanId = bs.popLong();
		TuanId_u = bs.popUByte();
		Status = bs.popUInt();
		Status_u = bs.popUByte();
		TuanMemberNum = bs.popUInt();
		TuanMemberNum_u = bs.popUByte();
		CreateTime = bs.popUInt();
		CreateTime_u = bs.popUByte();
		EndTime = bs.popUInt();
		EndTime_u = bs.popUByte();
		ServerTime = bs.popUInt();
		ServerTime_u = bs.popUByte();
		Product = (TuanProductBo) bs.popObject(TuanProductBo.class);
		Product_u = bs.popUByte();
		CreatorIndex = bs.popUInt();
		CreatorIndex_u = bs.popUByte();
		TuanMembers = (Vector<TuanMemberBo>)bs.popVector(TuanMemberBo.class);
		TuanMembers_u = bs.popUByte();
		if(  this.Version >= 20141028 ){
				TuanJoinMemberNum = bs.popUInt();
		}
		if(  this.Version >= 20141028 ){
				TuanJoinMemberNum_u = bs.popUByte();
		}
		if(  this.Version >= 20141028 ){
				TuanLastMemberNum = bs.popUInt();
		}
		if(  this.Version >= 20141028 ){
				TuanLastMemberNum_u = bs.popUByte();
		}
		if(  this.Version >= 20141028 ){
				ReserveIn = (Map<String,String>)bs.popMap(String.class,String.class);
		}
		if(  this.Version >= 20141028 ){
				ReserveIn_u = bs.popUByte();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取团id
	 * 
	 * 此字段的版本 >= 0
	 * @return TuanId value 类型为:long
	 * 
	 */
	public long getTuanId()
	{
		return TuanId;
	}


	/**
	 * 设置团id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTuanId(long value)
	{
		this.TuanId = value;
		this.TuanId_u = 1;
	}

	public boolean issetTuanId()
	{
		return this.TuanId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return TuanId_u value 类型为:short
	 * 
	 */
	public short getTuanId_u()
	{
		return TuanId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTuanId_u(short value)
	{
		this.TuanId_u = value;
	}


	/**
	 * 获取团状态 1.未激活2.已激活3.已成团4.已过期5.已履单6.已撤团
	 * 
	 * 此字段的版本 >= 0
	 * @return Status value 类型为:long
	 * 
	 */
	public long getStatus()
	{
		return Status;
	}


	/**
	 * 设置团状态 1.未激活2.已激活3.已成团4.已过期5.已履单6.已撤团
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setStatus(long value)
	{
		this.Status = value;
		this.Status_u = 1;
	}

	public boolean issetStatus()
	{
		return this.Status_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Status_u value 类型为:short
	 * 
	 */
	public short getStatus_u()
	{
		return Status_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStatus_u(short value)
	{
		this.Status_u = value;
	}


	/**
	 * 获取团成员数量上限
	 * 
	 * 此字段的版本 >= 0
	 * @return TuanMemberNum value 类型为:long
	 * 
	 */
	public long getTuanMemberNum()
	{
		return TuanMemberNum;
	}


	/**
	 * 设置团成员数量上限
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTuanMemberNum(long value)
	{
		this.TuanMemberNum = value;
		this.TuanMemberNum_u = 1;
	}

	public boolean issetTuanMemberNum()
	{
		return this.TuanMemberNum_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return TuanMemberNum_u value 类型为:short
	 * 
	 */
	public short getTuanMemberNum_u()
	{
		return TuanMemberNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTuanMemberNum_u(short value)
	{
		this.TuanMemberNum_u = value;
	}


	/**
	 * 获取开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @return CreateTime value 类型为:long
	 * 
	 */
	public long getCreateTime()
	{
		return CreateTime;
	}


	/**
	 * 设置开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCreateTime(long value)
	{
		this.CreateTime = value;
		this.CreateTime_u = 1;
	}

	public boolean issetCreateTime()
	{
		return this.CreateTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return CreateTime_u value 类型为:short
	 * 
	 */
	public short getCreateTime_u()
	{
		return CreateTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCreateTime_u(short value)
	{
		this.CreateTime_u = value;
	}


	/**
	 * 获取结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return EndTime value 类型为:long
	 * 
	 */
	public long getEndTime()
	{
		return EndTime;
	}


	/**
	 * 设置结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEndTime(long value)
	{
		this.EndTime = value;
		this.EndTime_u = 1;
	}

	public boolean issetEndTime()
	{
		return this.EndTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return EndTime_u value 类型为:short
	 * 
	 */
	public short getEndTime_u()
	{
		return EndTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEndTime_u(short value)
	{
		this.EndTime_u = value;
	}


	/**
	 * 获取服务器当时时间
	 * 
	 * 此字段的版本 >= 0
	 * @return ServerTime value 类型为:long
	 * 
	 */
	public long getServerTime()
	{
		return ServerTime;
	}


	/**
	 * 设置服务器当时时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setServerTime(long value)
	{
		this.ServerTime = value;
		this.ServerTime_u = 1;
	}

	public boolean issetServerTime()
	{
		return this.ServerTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ServerTime_u value 类型为:short
	 * 
	 */
	public short getServerTime_u()
	{
		return ServerTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setServerTime_u(short value)
	{
		this.ServerTime_u = value;
	}


	/**
	 * 获取团商品信息
	 * 
	 * 此字段的版本 >= 0
	 * @return Product value 类型为:TuanProductBo
	 * 
	 */
	public TuanProductBo getProduct()
	{
		return Product;
	}


	/**
	 * 设置团商品信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:TuanProductBo
	 * 
	 */
	public void setProduct(TuanProductBo value)
	{
		if (value != null) {
				this.Product = value;
				this.Product_u = 1;
		}
	}

	public boolean issetProduct()
	{
		return this.Product_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Product_u value 类型为:short
	 * 
	 */
	public short getProduct_u()
	{
		return Product_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProduct_u(short value)
	{
		this.Product_u = value;
	}


	/**
	 * 获取团长序号，默认为0
	 * 
	 * 此字段的版本 >= 0
	 * @return CreatorIndex value 类型为:long
	 * 
	 */
	public long getCreatorIndex()
	{
		return CreatorIndex;
	}


	/**
	 * 设置团长序号，默认为0
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCreatorIndex(long value)
	{
		this.CreatorIndex = value;
		this.CreatorIndex_u = 1;
	}

	public boolean issetCreatorIndex()
	{
		return this.CreatorIndex_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return CreatorIndex_u value 类型为:short
	 * 
	 */
	public short getCreatorIndex_u()
	{
		return CreatorIndex_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCreatorIndex_u(short value)
	{
		this.CreatorIndex_u = value;
	}


	/**
	 * 获取已参团成员列表
	 * 
	 * 此字段的版本 >= 0
	 * @return TuanMembers value 类型为:Vector<TuanMemberBo>
	 * 
	 */
	public Vector<TuanMemberBo> getTuanMembers()
	{
		return TuanMembers;
	}


	/**
	 * 设置已参团成员列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<TuanMemberBo>
	 * 
	 */
	public void setTuanMembers(Vector<TuanMemberBo> value)
	{
		if (value != null) {
				this.TuanMembers = value;
				this.TuanMembers_u = 1;
		}
	}

	public boolean issetTuanMembers()
	{
		return this.TuanMembers_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return TuanMembers_u value 类型为:short
	 * 
	 */
	public short getTuanMembers_u()
	{
		return TuanMembers_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTuanMembers_u(short value)
	{
		this.TuanMembers_u = value;
	}


	/**
	 * 获取参团成员数量
	 * 
	 * 此字段的版本 >= 20141028
	 * @return TuanJoinMemberNum value 类型为:long
	 * 
	 */
	public long getTuanJoinMemberNum()
	{
		return TuanJoinMemberNum;
	}


	/**
	 * 设置参团成员数量
	 * 
	 * 此字段的版本 >= 20141028
	 * @param  value 类型为:long
	 * 
	 */
	public void setTuanJoinMemberNum(long value)
	{
		this.TuanJoinMemberNum = value;
		this.TuanJoinMemberNum_u = 1;
	}

	public boolean issetTuanJoinMemberNum()
	{
		return this.TuanJoinMemberNum_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141028
	 * @return TuanJoinMemberNum_u value 类型为:short
	 * 
	 */
	public short getTuanJoinMemberNum_u()
	{
		return TuanJoinMemberNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141028
	 * @param  value 类型为:short
	 * 
	 */
	public void setTuanJoinMemberNum_u(short value)
	{
		this.TuanJoinMemberNum_u = value;
	}


	/**
	 * 获取团剩余人数
	 * 
	 * 此字段的版本 >= 20141028
	 * @return TuanLastMemberNum value 类型为:long
	 * 
	 */
	public long getTuanLastMemberNum()
	{
		return TuanLastMemberNum;
	}


	/**
	 * 设置团剩余人数
	 * 
	 * 此字段的版本 >= 20141028
	 * @param  value 类型为:long
	 * 
	 */
	public void setTuanLastMemberNum(long value)
	{
		this.TuanLastMemberNum = value;
		this.TuanLastMemberNum_u = 1;
	}

	public boolean issetTuanLastMemberNum()
	{
		return this.TuanLastMemberNum_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141028
	 * @return TuanLastMemberNum_u value 类型为:short
	 * 
	 */
	public short getTuanLastMemberNum_u()
	{
		return TuanLastMemberNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141028
	 * @param  value 类型为:short
	 * 
	 */
	public void setTuanLastMemberNum_u(short value)
	{
		this.TuanLastMemberNum_u = value;
	}


	/**
	 * 获取保留输入
	 * 
	 * 此字段的版本 >= 20141028
	 * @return ReserveIn value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置保留输入
	 * 
	 * 此字段的版本 >= 20141028
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setReserveIn(Map<String,String> value)
	{
		if (value != null) {
				this.ReserveIn = value;
				this.ReserveIn_u = 1;
		}
	}

	public boolean issetReserveIn()
	{
		return this.ReserveIn_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141028
	 * @return ReserveIn_u value 类型为:short
	 * 
	 */
	public short getReserveIn_u()
	{
		return ReserveIn_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141028
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserveIn_u(short value)
	{
		this.ReserveIn_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(TuanInfoBo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段TuanId的长度 size_of(uint64_t)
				length += 1;  //计算字段TuanId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Status的长度 size_of(uint32_t)
				length += 1;  //计算字段Status_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TuanMemberNum的长度 size_of(uint32_t)
				length += 1;  //计算字段TuanMemberNum_u的长度 size_of(uint8_t)
				length += 4;  //计算字段CreateTime的长度 size_of(uint32_t)
				length += 1;  //计算字段CreateTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段EndTime的长度 size_of(uint32_t)
				length += 1;  //计算字段EndTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ServerTime的长度 size_of(uint32_t)
				length += 1;  //计算字段ServerTime_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Product, null);  //计算字段Product的长度 size_of(TuanProductBo)
				length += 1;  //计算字段Product_u的长度 size_of(uint8_t)
				length += 4;  //计算字段CreatorIndex的长度 size_of(uint32_t)
				length += 1;  //计算字段CreatorIndex_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(TuanMembers, null);  //计算字段TuanMembers的长度 size_of(Vector)
				length += 1;  //计算字段TuanMembers_u的长度 size_of(uint8_t)
				if(  this.Version >= 20141028 ){
						length += 4;  //计算字段TuanJoinMemberNum的长度 size_of(uint32_t)
				}
				if(  this.Version >= 20141028 ){
						length += 1;  //计算字段TuanJoinMemberNum_u的长度 size_of(uint8_t)
				}
				if(  this.Version >= 20141028 ){
						length += 4;  //计算字段TuanLastMemberNum的长度 size_of(uint32_t)
				}
				if(  this.Version >= 20141028 ){
						length += 1;  //计算字段TuanLastMemberNum_u的长度 size_of(uint8_t)
				}
				if(  this.Version >= 20141028 ){
						length += ByteStream.getObjectSize(ReserveIn, null);  //计算字段ReserveIn的长度 size_of(Map)
				}
				if(  this.Version >= 20141028 ){
						length += 1;  //计算字段ReserveIn_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(TuanInfoBo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段TuanId的长度 size_of(uint64_t)
				length += 1;  //计算字段TuanId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Status的长度 size_of(uint32_t)
				length += 1;  //计算字段Status_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TuanMemberNum的长度 size_of(uint32_t)
				length += 1;  //计算字段TuanMemberNum_u的长度 size_of(uint8_t)
				length += 4;  //计算字段CreateTime的长度 size_of(uint32_t)
				length += 1;  //计算字段CreateTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段EndTime的长度 size_of(uint32_t)
				length += 1;  //计算字段EndTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ServerTime的长度 size_of(uint32_t)
				length += 1;  //计算字段ServerTime_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Product, encoding);  //计算字段Product的长度 size_of(TuanProductBo)
				length += 1;  //计算字段Product_u的长度 size_of(uint8_t)
				length += 4;  //计算字段CreatorIndex的长度 size_of(uint32_t)
				length += 1;  //计算字段CreatorIndex_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(TuanMembers, encoding);  //计算字段TuanMembers的长度 size_of(Vector)
				length += 1;  //计算字段TuanMembers_u的长度 size_of(uint8_t)
				if(  this.Version >= 20141028 ){
						length += 4;  //计算字段TuanJoinMemberNum的长度 size_of(uint32_t)
				}
				if(  this.Version >= 20141028 ){
						length += 1;  //计算字段TuanJoinMemberNum_u的长度 size_of(uint8_t)
				}
				if(  this.Version >= 20141028 ){
						length += 4;  //计算字段TuanLastMemberNum的长度 size_of(uint32_t)
				}
				if(  this.Version >= 20141028 ){
						length += 1;  //计算字段TuanLastMemberNum_u的长度 size_of(uint8_t)
				}
				if(  this.Version >= 20141028 ){
						length += ByteStream.getObjectSize(ReserveIn, encoding);  //计算字段ReserveIn的长度 size_of(Map)
				}
				if(  this.Version >= 20141028 ){
						length += 1;  //计算字段ReserveIn_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本20141028所包含的字段*******
 *	long Version;///<版本号
 *	short Version_u;
 *	long TuanId;///<团id
 *	short TuanId_u;
 *	long Status;///<团状态 1.未激活2.已激活3.已成团4.已过期5.已履单6.已撤团
 *	short Status_u;
 *	long TuanMemberNum;///<团成员数量上限
 *	short TuanMemberNum_u;
 *	long CreateTime;///<开始时间
 *	short CreateTime_u;
 *	long EndTime;///<结束时间
 *	short EndTime_u;
 *	long ServerTime;///<服务器当时时间
 *	short ServerTime_u;
 *	TuanProductBo Product;///<团商品信息
 *	short Product_u;
 *	long CreatorIndex;///<团长序号，默认为0
 *	short CreatorIndex_u;
 *	Vector<TuanMemberBo> TuanMembers;///<已参团成员列表
 *	short TuanMembers_u;
 *	long TuanJoinMemberNum;///<参团成员数量
 *	short TuanJoinMemberNum_u;
 *	long TuanLastMemberNum;///<团剩余人数
 *	short TuanLastMemberNum_u;
 *	Map<String,String> ReserveIn;///<保留输入
 *	short ReserveIn_u;
 *****以上是版本20141028所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
