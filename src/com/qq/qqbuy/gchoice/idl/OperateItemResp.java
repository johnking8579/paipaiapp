 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.TuanTJCossDao.java

package com.qq.qqbuy.gchoice.idl;

import com.paipai.util.io.ByteStream;
import com.paipai.lang.uint64_t;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *resp
 *
 *@date 2015-03-04 11:43:58
 *
 *@since version:0
*/
public class  OperateItemResp implements IServiceObject
{
	public long result;
	/**
	 * 错误码, 0成功, 非0失败
	 *
	 * 版本 >= 0
	 */
//	int64_t
	 private uint64_t retcode = new uint64_t();

	/**
	 * 错误消息
	 *
	 * 版本 >= 0
	 */
	 private String errmsg = new String();

	/**
	 * 审核记录数据
	 *
	 * 版本 >= 0
	 */
	 private TTJAuditItemPo outItem = new TTJAuditItemPo();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(retcode);
		bs.pushString(errmsg);
		bs.pushObject(outItem);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		retcode = (uint64_t)bs.popObject(uint64_t.class);
		errmsg = bs.popString();
		outItem = (TTJAuditItemPo) bs.popObject(TTJAuditItemPo.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x72078809L;
	}


	/**
	 * 获取错误码, 0成功, 非0失败
	 * 
	 * 此字段的版本 >= 0
	 * @return retcode value 类型为:int64
	 * 
	 */
	public uint64_t getRetcode()
	{
		return retcode;
	}


	/**
	 * 设置错误码, 0成功, 非0失败
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int64
	 * 
	 */
	public void setRetcode(uint64_t value)
	{
		this.retcode = value;
	}


	/**
	 * 获取错误消息
	 * 
	 * 此字段的版本 >= 0
	 * @return errmsg value 类型为:String
	 * 
	 */
	public String getErrmsg()
	{
		return errmsg;
	}


	/**
	 * 设置错误消息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrmsg(String value)
	{
		this.errmsg = value;
	}


	/**
	 * 获取审核记录数据
	 * 
	 * 此字段的版本 >= 0
	 * @return outItem value 类型为:TTJAuditItemPo
	 * 
	 */
	public TTJAuditItemPo getOutItem()
	{
		return outItem;
	}


	/**
	 * 设置审核记录数据
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:TTJAuditItemPo
	 * 
	 */
	public void setOutItem(TTJAuditItemPo value)
	{
		if (value != null) {
				this.outItem = value;
		}else{
				this.outItem = new TTJAuditItemPo();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(OperateItemResp)
				length += ByteStream.getObjectSize(retcode, null);  //计算字段retcode的长度 size_of(int64)
				length += ByteStream.getObjectSize(errmsg, null);  //计算字段errmsg的长度 size_of(String)
				length += ByteStream.getObjectSize(outItem, null);  //计算字段outItem的长度 size_of(TTJAuditItemPo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(OperateItemResp)
				length += ByteStream.getObjectSize(retcode, encoding);  //计算字段retcode的长度 size_of(int64)
				length += ByteStream.getObjectSize(errmsg, encoding);  //计算字段errmsg的长度 size_of(String)
				length += ByteStream.getObjectSize(outItem, encoding);  //计算字段outItem的长度 size_of(TTJAuditItemPo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
