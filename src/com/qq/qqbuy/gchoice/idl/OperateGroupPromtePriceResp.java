 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.TuanTJCossDao.java

package com.qq.qqbuy.gchoice.idl;

import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.lang.int64;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Vector;

/**
 *resp
 *
 *@date 2015-03-04 11:43:58
 *
 *@since version:0
*/
public class  OperateGroupPromtePriceResp implements IServiceObject
{
	public long result;
	/**
	 * 错误码, 0成功, 非0失败
	 *
	 * 版本 >= 0
	 */
	 private int64 retcode = new int64();

	/**
	 * 错误消息
	 *
	 * 版本 >= 0
	 */
	 private String errmsg = new String();

	/**
	 * 优惠价总名额
	 *
	 * 版本 >= 0
	 */
	 private long maxOwnerNum;

	/**
	 * 已获得优惠价的用户们
	 *
	 * 版本 >= 0
	 */
	 private Vector<String> owners = new Vector<String>();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(retcode);
		bs.pushString(errmsg);
		bs.pushUInt(maxOwnerNum);
		bs.pushObject(owners);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		retcode = (com.paipai.lang.int64)bs.popObject(com.paipai.lang.int64.class);
		errmsg = bs.popString();
		maxOwnerNum = bs.popUInt();
		owners = (Vector<String>)bs.popVector(String.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x7207880AL;
	}


	/**
	 * 获取错误码, 0成功, 非0失败
	 * 
	 * 此字段的版本 >= 0
	 * @return retcode value 类型为:int64
	 * 
	 */
	public int64 getRetcode()
	{
		return retcode;
	}


	/**
	 * 设置错误码, 0成功, 非0失败
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int64
	 * 
	 */
	public void setRetcode(int64 value)
	{
		this.retcode = value;
	}


	/**
	 * 获取错误消息
	 * 
	 * 此字段的版本 >= 0
	 * @return errmsg value 类型为:String
	 * 
	 */
	public String getErrmsg()
	{
		return errmsg;
	}


	/**
	 * 设置错误消息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrmsg(String value)
	{
		this.errmsg = value;
	}


	/**
	 * 获取优惠价总名额
	 * 
	 * 此字段的版本 >= 0
	 * @return maxOwnerNum value 类型为:long
	 * 
	 */
	public long getMaxOwnerNum()
	{
		return maxOwnerNum;
	}


	/**
	 * 设置优惠价总名额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMaxOwnerNum(long value)
	{
		this.maxOwnerNum = value;
	}


	/**
	 * 获取已获得优惠价的用户们
	 * 
	 * 此字段的版本 >= 0
	 * @return owners value 类型为:Vector<String>
	 * 
	 */
	public Vector<String> getOwners()
	{
		return owners;
	}


	/**
	 * 设置已获得优惠价的用户们
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<String>
	 * 
	 */
	public void setOwners(Vector<String> value)
	{
		if (value != null) {
				this.owners = value;
		}else{
				this.owners = new Vector<String>();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(OperateGroupPromtePriceResp)
				length += ByteStream.getObjectSize(retcode, null);  //计算字段retcode的长度 size_of(int64)
				length += ByteStream.getObjectSize(errmsg, null);  //计算字段errmsg的长度 size_of(String)
				length += 4;  //计算字段maxOwnerNum的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(owners, null);  //计算字段owners的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(OperateGroupPromtePriceResp)
				length += ByteStream.getObjectSize(retcode, encoding);  //计算字段retcode的长度 size_of(int64)
				length += ByteStream.getObjectSize(errmsg, encoding);  //计算字段errmsg的长度 size_of(String)
				length += 4;  //计算字段maxOwnerNum的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(owners, encoding);  //计算字段owners的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
