 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.TuanTJCossDao.java

package com.qq.qqbuy.gchoice.idl;

import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *req
 *
 *@date 2015-03-04 11:43:58
 *
 *@since version:0
*/
public class  QueryAuditItemsByKeyReq implements IServiceObject
{
	/**
	 * 商品ID
	 *
	 * 版本 >= 0
	 */
	 private String comodityId = new String();

	/**
	 * 区域名称
	 *
	 * 版本 >= 0
	 */
	 private String areaName = new String();

	/**
	 * 活动期数
	 *
	 * 版本 >= 0
	 */
	 private String activeNum = new String();

	/**
	 * coss记录状态
	 *
	 * 版本 >= 0
	 */
	 private short status;


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(comodityId);
		bs.pushString(areaName);
		bs.pushString(activeNum);
		bs.pushUByte(status);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		comodityId = bs.popString();
		areaName = bs.popString();
		activeNum = bs.popString();
		status = bs.popUByte();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x72071804L;
	}


	/**
	 * 获取商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return comodityId value 类型为:String
	 * 
	 */
	public String getComodityId()
	{
		return comodityId;
	}


	/**
	 * 设置商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setComodityId(String value)
	{
		this.comodityId = value;
	}


	/**
	 * 获取区域名称
	 * 
	 * 此字段的版本 >= 0
	 * @return areaName value 类型为:String
	 * 
	 */
	public String getAreaName()
	{
		return areaName;
	}


	/**
	 * 设置区域名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAreaName(String value)
	{
		this.areaName = value;
	}


	/**
	 * 获取活动期数
	 * 
	 * 此字段的版本 >= 0
	 * @return activeNum value 类型为:String
	 * 
	 */
	public String getActiveNum()
	{
		return activeNum;
	}


	/**
	 * 设置活动期数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setActiveNum(String value)
	{
		this.activeNum = value;
	}


	/**
	 * 获取coss记录状态
	 * 
	 * 此字段的版本 >= 0
	 * @return status value 类型为:short
	 * 
	 */
	public short getStatus()
	{
		return status;
	}


	/**
	 * 设置coss记录状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStatus(short value)
	{
		this.status = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(QueryAuditItemsByKeyReq)
				length += ByteStream.getObjectSize(comodityId, null);  //计算字段comodityId的长度 size_of(String)
				length += ByteStream.getObjectSize(areaName, null);  //计算字段areaName的长度 size_of(String)
				length += ByteStream.getObjectSize(activeNum, null);  //计算字段activeNum的长度 size_of(String)
				length += 1;  //计算字段status的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(QueryAuditItemsByKeyReq)
				length += ByteStream.getObjectSize(comodityId, encoding);  //计算字段comodityId的长度 size_of(String)
				length += ByteStream.getObjectSize(areaName, encoding);  //计算字段areaName的长度 size_of(String)
				length += ByteStream.getObjectSize(activeNum, encoding);  //计算字段activeNum的长度 size_of(String)
				length += 1;  //计算字段status的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
