 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.TuanTJCossDao.java

package com.qq.qqbuy.gchoice.idl;

import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Vector;

/**
 *req
 *
 *@date 2015-03-04 11:43:58
 *
 *@since version:0
*/
public class  AddAuditItemReq implements IServiceObject
{
	/**
	 * coss审核商品数据
	 *
	 * 版本 >= 0
	 */
	 private Vector<TTJAuditItemPo> items = new Vector<TTJAuditItemPo>();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(items);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		items = (Vector<TTJAuditItemPo>)bs.popVector(TTJAuditItemPo.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x72071801L;
	}


	/**
	 * 获取coss审核商品数据
	 * 
	 * 此字段的版本 >= 0
	 * @return items value 类型为:Vector<TTJAuditItemPo>
	 * 
	 */
	public Vector<TTJAuditItemPo> getItems()
	{
		return items;
	}


	/**
	 * 设置coss审核商品数据
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<TTJAuditItemPo>
	 * 
	 */
	public void setItems(Vector<TTJAuditItemPo> value)
	{
		if (value != null) {
				this.items = value;
		}else{
				this.items = new Vector<TTJAuditItemPo>();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(AddAuditItemReq)
				length += ByteStream.getObjectSize(items, null);  //计算字段items的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(AddAuditItemReq)
				length += ByteStream.getObjectSize(items, encoding);  //计算字段items的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
