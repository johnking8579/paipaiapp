//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: tuantuanjiang.idl.TuanInfoBo.java
package com.qq.qqbuy.gchoice.idl;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import java.util.Map;
import java.util.HashMap;

/**
 *团成员信息
 *
 *@date 2015-03-04 10:57:53
 *
 *@since version:0
*/
public class TuanMemberBo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version = 20141028;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * uid
	 *
	 * 版本 >= 0
	 */
	 private long Uid;

	/**
	 * 版本 >= 0
	 */
	 private short Uid_u;

	/**
	 * open_id
	 *
	 * 版本 >= 0
	 */
	 private String OpenId = new String();

	/**
	 * 版本 >= 0
	 */
	 private short OpenId_u;

	/**
	 * 用户头像url，key:0、46、64、96、132
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> HeadUrls = new HashMap<String,String>();

	/**
	 * 版本 >= 0
	 */
	 private short HeadUrls_u;

	/**
	 * 用户昵称
	 *
	 * 版本 >= 0
	 */
	 private String UserNickname = new String();

	/**
	 * 版本 >= 0
	 */
	 private short UserNickname_u;

	/**
	 * 团员角色 1-团长，2-团员
	 *
	 * 版本 >= 0
	 */
	 private short TuanRole;

	/**
	 * 版本 >= 0
	 */
	 private short TuanRole_u;

	/**
	 * 订单id
	 *
	 * 版本 >= 0
	 */
	 private String DealId = new String();

	/**
	 * 版本 >= 0
	 */
	 private short DealId_u;

	/**
	 * 订单status，1.未支付2.已参团3.待取消4.已取消5.已履单
	 *
	 * 版本 >= 0
	 */
	 private long DealStatus;

	/**
	 * 版本 >= 0
	 */
	 private short DealStatus_u;

	/**
	 * 订单支付成功时间
	 *
	 * 版本 >= 0
	 */
	 private long DealTime;

	/**
	 * 版本 >= 0
	 */
	 private short DealTime_u;

	/**
	 * 错误码
	 *
	 * 版本 >= 0
	 */
	 private long ErrCode;

	/**
	 * 版本 >= 0
	 */
	 private short ErrCode_u;

	/**
	 * 留言
	 *
	 * 版本 >= 20141028
	 */
	 private String Msg = new String();

	/**
	 * 
	 *
	 * 版本 >= 20141028
	 */
	 private short Msg_u;

	/**
	 * 保留输入
	 *
	 * 版本 >= 20141028
	 */
	 private Map<String,String> ReserveIn = new HashMap<String,String>();

	/**
	 * 
	 *
	 * 版本 >= 20141028
	 */
	 private short ReserveIn_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushLong(Uid);
		bs.pushUByte(Uid_u);
		bs.pushString(OpenId);
		bs.pushUByte(OpenId_u);
		bs.pushObject(HeadUrls);
		bs.pushUByte(HeadUrls_u);
		bs.pushString(UserNickname);
		bs.pushUByte(UserNickname_u);
		bs.pushUByte(TuanRole);
		bs.pushUByte(TuanRole_u);
		bs.pushString(DealId);
		bs.pushUByte(DealId_u);
		bs.pushUInt(DealStatus);
		bs.pushUByte(DealStatus_u);
		bs.pushUInt(DealTime);
		bs.pushUByte(DealTime_u);
		bs.pushUInt(ErrCode);
		bs.pushUByte(ErrCode_u);
		if(  this.Version >= 20141028 ){
				bs.pushString(Msg);
		}
		if(  this.Version >= 20141028 ){
				bs.pushUByte(Msg_u);
		}
		if(  this.Version >= 20141028 ){
				bs.pushObject(ReserveIn);
		}
		if(  this.Version >= 20141028 ){
				bs.pushUByte(ReserveIn_u);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		Uid = bs.popLong();
		Uid_u = bs.popUByte();
		OpenId = bs.popString();
		OpenId_u = bs.popUByte();
		HeadUrls = (Map<String,String>)bs.popMap(String.class,String.class);
		HeadUrls_u = bs.popUByte();
		UserNickname = bs.popString();
		UserNickname_u = bs.popUByte();
		TuanRole = bs.popUByte();
		TuanRole_u = bs.popUByte();
		DealId = bs.popString();
		DealId_u = bs.popUByte();
		DealStatus = bs.popUInt();
		DealStatus_u = bs.popUByte();
		DealTime = bs.popUInt();
		DealTime_u = bs.popUByte();
		ErrCode = bs.popUInt();
		ErrCode_u = bs.popUByte();
		if(  this.Version >= 20141028 ){
				Msg = bs.popString();
		}
		if(  this.Version >= 20141028 ){
				Msg_u = bs.popUByte();
		}
		if(  this.Version >= 20141028 ){
				ReserveIn = (Map<String,String>)bs.popMap(String.class,String.class);
		}
		if(  this.Version >= 20141028 ){
				ReserveIn_u = bs.popUByte();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取uid
	 * 
	 * 此字段的版本 >= 0
	 * @return Uid value 类型为:long
	 * 
	 */
	public long getUid()
	{
		return Uid;
	}


	/**
	 * 设置uid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUid(long value)
	{
		this.Uid = value;
		this.Uid_u = 1;
	}

	public boolean issetUid()
	{
		return this.Uid_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Uid_u value 类型为:short
	 * 
	 */
	public short getUid_u()
	{
		return Uid_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUid_u(short value)
	{
		this.Uid_u = value;
	}


	/**
	 * 获取open_id
	 * 
	 * 此字段的版本 >= 0
	 * @return OpenId value 类型为:String
	 * 
	 */
	public String getOpenId()
	{
		return OpenId;
	}


	/**
	 * 设置open_id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOpenId(String value)
	{
		this.OpenId = value;
		this.OpenId_u = 1;
	}

	public boolean issetOpenId()
	{
		return this.OpenId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return OpenId_u value 类型为:short
	 * 
	 */
	public short getOpenId_u()
	{
		return OpenId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOpenId_u(short value)
	{
		this.OpenId_u = value;
	}


	/**
	 * 获取用户头像url，key:0、46、64、96、132
	 * 
	 * 此字段的版本 >= 0
	 * @return HeadUrls value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getHeadUrls()
	{
		return HeadUrls;
	}


	/**
	 * 设置用户头像url，key:0、46、64、96、132
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setHeadUrls(Map<String,String> value)
	{
		if (value != null) {
				this.HeadUrls = value;
				this.HeadUrls_u = 1;
		}
	}

	public boolean issetHeadUrls()
	{
		return this.HeadUrls_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return HeadUrls_u value 类型为:short
	 * 
	 */
	public short getHeadUrls_u()
	{
		return HeadUrls_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setHeadUrls_u(short value)
	{
		this.HeadUrls_u = value;
	}


	/**
	 * 获取用户昵称
	 * 
	 * 此字段的版本 >= 0
	 * @return UserNickname value 类型为:String
	 * 
	 */
	public String getUserNickname()
	{
		return UserNickname;
	}


	/**
	 * 设置用户昵称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setUserNickname(String value)
	{
		this.UserNickname = value;
		this.UserNickname_u = 1;
	}

	public boolean issetUserNickname()
	{
		return this.UserNickname_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return UserNickname_u value 类型为:short
	 * 
	 */
	public short getUserNickname_u()
	{
		return UserNickname_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUserNickname_u(short value)
	{
		this.UserNickname_u = value;
	}


	/**
	 * 获取团员角色 1-团长，2-团员
	 * 
	 * 此字段的版本 >= 0
	 * @return TuanRole value 类型为:short
	 * 
	 */
	public short getTuanRole()
	{
		return TuanRole;
	}


	/**
	 * 设置团员角色 1-团长，2-团员
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTuanRole(short value)
	{
		this.TuanRole = value;
		this.TuanRole_u = 1;
	}

	public boolean issetTuanRole()
	{
		return this.TuanRole_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return TuanRole_u value 类型为:short
	 * 
	 */
	public short getTuanRole_u()
	{
		return TuanRole_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTuanRole_u(short value)
	{
		this.TuanRole_u = value;
	}


	/**
	 * 获取订单id
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:String
	 * 
	 */
	public String getDealId()
	{
		return DealId;
	}


	/**
	 * 设置订单id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealId(String value)
	{
		this.DealId = value;
		this.DealId_u = 1;
	}

	public boolean issetDealId()
	{
		return this.DealId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId_u value 类型为:short
	 * 
	 */
	public short getDealId_u()
	{
		return DealId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealId_u(short value)
	{
		this.DealId_u = value;
	}


	/**
	 * 获取订单status，1.未支付2.已参团3.待取消4.已取消5.已履单
	 * 
	 * 此字段的版本 >= 0
	 * @return DealStatus value 类型为:long
	 * 
	 */
	public long getDealStatus()
	{
		return DealStatus;
	}


	/**
	 * 设置订单status，1.未支付2.已参团3.待取消4.已取消5.已履单
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealStatus(long value)
	{
		this.DealStatus = value;
		this.DealStatus_u = 1;
	}

	public boolean issetDealStatus()
	{
		return this.DealStatus_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DealStatus_u value 类型为:short
	 * 
	 */
	public short getDealStatus_u()
	{
		return DealStatus_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealStatus_u(short value)
	{
		this.DealStatus_u = value;
	}


	/**
	 * 获取订单支付成功时间
	 * 
	 * 此字段的版本 >= 0
	 * @return DealTime value 类型为:long
	 * 
	 */
	public long getDealTime()
	{
		return DealTime;
	}


	/**
	 * 设置订单支付成功时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealTime(long value)
	{
		this.DealTime = value;
		this.DealTime_u = 1;
	}

	public boolean issetDealTime()
	{
		return this.DealTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DealTime_u value 类型为:short
	 * 
	 */
	public short getDealTime_u()
	{
		return DealTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealTime_u(short value)
	{
		this.DealTime_u = value;
	}


	/**
	 * 获取错误码
	 * 
	 * 此字段的版本 >= 0
	 * @return ErrCode value 类型为:long
	 * 
	 */
	public long getErrCode()
	{
		return ErrCode;
	}


	/**
	 * 设置错误码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setErrCode(long value)
	{
		this.ErrCode = value;
		this.ErrCode_u = 1;
	}

	public boolean issetErrCode()
	{
		return this.ErrCode_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ErrCode_u value 类型为:short
	 * 
	 */
	public short getErrCode_u()
	{
		return ErrCode_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setErrCode_u(short value)
	{
		this.ErrCode_u = value;
	}


	/**
	 * 获取留言
	 * 
	 * 此字段的版本 >= 20141028
	 * @return Msg value 类型为:String
	 * 
	 */
	public String getMsg()
	{
		return Msg;
	}


	/**
	 * 设置留言
	 * 
	 * 此字段的版本 >= 20141028
	 * @param  value 类型为:String
	 * 
	 */
	public void setMsg(String value)
	{
		this.Msg = value;
		this.Msg_u = 1;
	}

	public boolean issetMsg()
	{
		return this.Msg_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141028
	 * @return Msg_u value 类型为:short
	 * 
	 */
	public short getMsg_u()
	{
		return Msg_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141028
	 * @param  value 类型为:short
	 * 
	 */
	public void setMsg_u(short value)
	{
		this.Msg_u = value;
	}


	/**
	 * 获取保留输入
	 * 
	 * 此字段的版本 >= 20141028
	 * @return ReserveIn value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置保留输入
	 * 
	 * 此字段的版本 >= 20141028
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setReserveIn(Map<String,String> value)
	{
		if (value != null) {
				this.ReserveIn = value;
				this.ReserveIn_u = 1;
		}
	}

	public boolean issetReserveIn()
	{
		return this.ReserveIn_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141028
	 * @return ReserveIn_u value 类型为:short
	 * 
	 */
	public short getReserveIn_u()
	{
		return ReserveIn_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141028
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserveIn_u(short value)
	{
		this.ReserveIn_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(TuanMemberBo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段Uid的长度 size_of(uint64_t)
				length += 1;  //计算字段Uid_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(OpenId, null);  //计算字段OpenId的长度 size_of(String)
				length += 1;  //计算字段OpenId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(HeadUrls, null);  //计算字段HeadUrls的长度 size_of(Map)
				length += 1;  //计算字段HeadUrls_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(UserNickname, null);  //计算字段UserNickname的长度 size_of(String)
				length += 1;  //计算字段UserNickname_u的长度 size_of(uint8_t)
				length += 1;  //计算字段TuanRole的长度 size_of(uint8_t)
				length += 1;  //计算字段TuanRole_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(DealId, null);  //计算字段DealId的长度 size_of(String)
				length += 1;  //计算字段DealId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段DealStatus的长度 size_of(uint32_t)
				length += 1;  //计算字段DealStatus_u的长度 size_of(uint8_t)
				length += 4;  //计算字段DealTime的长度 size_of(uint32_t)
				length += 1;  //计算字段DealTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ErrCode的长度 size_of(uint32_t)
				length += 1;  //计算字段ErrCode_u的长度 size_of(uint8_t)
				if(  this.Version >= 20141028 ){
						length += ByteStream.getObjectSize(Msg, null);  //计算字段Msg的长度 size_of(String)
				}
				if(  this.Version >= 20141028 ){
						length += 1;  //计算字段Msg_u的长度 size_of(uint8_t)
				}
				if(  this.Version >= 20141028 ){
						length += ByteStream.getObjectSize(ReserveIn, null);  //计算字段ReserveIn的长度 size_of(Map)
				}
				if(  this.Version >= 20141028 ){
						length += 1;  //计算字段ReserveIn_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(TuanMemberBo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段Uid的长度 size_of(uint64_t)
				length += 1;  //计算字段Uid_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(OpenId, encoding);  //计算字段OpenId的长度 size_of(String)
				length += 1;  //计算字段OpenId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(HeadUrls, encoding);  //计算字段HeadUrls的长度 size_of(Map)
				length += 1;  //计算字段HeadUrls_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(UserNickname, encoding);  //计算字段UserNickname的长度 size_of(String)
				length += 1;  //计算字段UserNickname_u的长度 size_of(uint8_t)
				length += 1;  //计算字段TuanRole的长度 size_of(uint8_t)
				length += 1;  //计算字段TuanRole_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(DealId, encoding);  //计算字段DealId的长度 size_of(String)
				length += 1;  //计算字段DealId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段DealStatus的长度 size_of(uint32_t)
				length += 1;  //计算字段DealStatus_u的长度 size_of(uint8_t)
				length += 4;  //计算字段DealTime的长度 size_of(uint32_t)
				length += 1;  //计算字段DealTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ErrCode的长度 size_of(uint32_t)
				length += 1;  //计算字段ErrCode_u的长度 size_of(uint8_t)
				if(  this.Version >= 20141028 ){
						length += ByteStream.getObjectSize(Msg, encoding);  //计算字段Msg的长度 size_of(String)
				}
				if(  this.Version >= 20141028 ){
						length += 1;  //计算字段Msg_u的长度 size_of(uint8_t)
				}
				if(  this.Version >= 20141028 ){
						length += ByteStream.getObjectSize(ReserveIn, encoding);  //计算字段ReserveIn的长度 size_of(Map)
				}
				if(  this.Version >= 20141028 ){
						length += 1;  //计算字段ReserveIn_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本20141028所包含的字段*******
 *	long Version;///<版本号
 *	short Version_u;
 *	long Uid;///<uid
 *	short Uid_u;
 *	String OpenId;///<open_id
 *	short OpenId_u;
 *	Map<String,String> HeadUrls;///<用户头像url，key:0、46、64、96、132
 *	short HeadUrls_u;
 *	String UserNickname;///<用户昵称
 *	short UserNickname_u;
 *	short TuanRole;///<团员角色 1-团长，2-团员
 *	short TuanRole_u;
 *	String DealId;///<订单id
 *	short DealId_u;
 *	long DealStatus;///<订单status，1.未支付2.已参团3.待取消4.已取消5.已履单
 *	short DealStatus_u;
 *	long DealTime;///<订单支付成功时间
 *	short DealTime_u;
 *	long ErrCode;///<错误码
 *	short ErrCode_u;
 *	String Msg;///<留言
 *	short Msg_u;
 *	Map<String,String> ReserveIn;///<保留输入
 *	short ReserveIn_u;
 *****以上是版本20141028所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
