package com.qq.qqbuy.gchoice.idl;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;

public class GChoiceClient extends SupportIDLBaseClient {
	
	public static OperateItemResp getOperateItem(String itemCode) {
        OperateItemReq req = new OperateItemReq();
       
        
        TTJAuditItemPo ttjAuditItemPo = new TTJAuditItemPo();
        ttjAuditItemPo.setComodityId(itemCode);
        		
        req.setInItem(ttjAuditItemPo);
        req.setOperateId((short)0);

        OperateItemResp resp = new OperateItemResp();
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
        ret = invokePaiPaiIDL(req, resp,stub);

        return ret == SUCCESS ? resp : null;
    }
	
	
	
	public static GetTuanListByItemIdResp getTuanListByItemId(String itemCode, String mk) {
		GetTuanListByItemIdReq req = new GetTuanListByItemIdReq();
       
		req.setItemId(itemCode);
		req.setMachineKey(StringUtil.isBlank(mk) ? "mk_null" : mk);
		req.setSource("ppapp_paipianyi");
		req.setSceneId(1);

        GetTuanListByItemIdResp resp = new GetTuanListByItemIdResp();
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
        ret = invokePaiPaiIDL(req, resp,stub);

        return ret == SUCCESS ? resp : null;
    }
	
}
