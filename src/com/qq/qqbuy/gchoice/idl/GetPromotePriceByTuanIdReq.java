 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: tuantuanjiang.idl.TuanTuanJiangReadAo.java

package com.qq.qqbuy.gchoice.idl;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Map;
import java.util.HashMap;

/**
 *请求参数
 *
 *@date 2015-03-04 10:57:53
 *
 *@since version:0
*/
public class  GetPromotePriceByTuanIdReq implements IServiceObject
{
	/**
	 * 来源，必填
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String MachineKey = new String();

	/**
	 * 场景，必填，拍便宜业务填1
	 *
	 * 版本 >= 0
	 */
	 private long SceneId;

	/**
	 * 团id
	 *
	 * 版本 >= 0
	 */
	 private long tuanId;

	/**
	 * 用户id
	 *
	 * 版本 >= 0
	 */
	 private long uid;

	/**
	 * 请求保留字，拓展用，选填
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> ReserveIn = new HashMap<String,String>();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(Source);
		bs.pushString(MachineKey);
		bs.pushUInt(SceneId);
		bs.pushLong(tuanId);
		bs.pushLong(uid);
		bs.pushObject(ReserveIn);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		Source = bs.popString();
		MachineKey = bs.popString();
		SceneId = bs.popUInt();
		tuanId = bs.popLong();
		uid = bs.popLong();
		ReserveIn = (Map<String,String>)bs.popMap(String.class,String.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x875b1806L;
	}


	/**
	 * 获取来源，必填
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置来源，必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return MachineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return MachineKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.MachineKey = value;
	}


	/**
	 * 获取场景，必填，拍便宜业务填1
	 * 
	 * 此字段的版本 >= 0
	 * @return SceneId value 类型为:long
	 * 
	 */
	public long getSceneId()
	{
		return SceneId;
	}


	/**
	 * 设置场景，必填，拍便宜业务填1
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSceneId(long value)
	{
		this.SceneId = value;
	}


	/**
	 * 获取团id
	 * 
	 * 此字段的版本 >= 0
	 * @return tuanId value 类型为:long
	 * 
	 */
	public long getTuanId()
	{
		return tuanId;
	}


	/**
	 * 设置团id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTuanId(long value)
	{
		this.tuanId = value;
	}


	/**
	 * 获取用户id
	 * 
	 * 此字段的版本 >= 0
	 * @return uid value 类型为:long
	 * 
	 */
	public long getUid()
	{
		return uid;
	}


	/**
	 * 设置用户id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUid(long value)
	{
		this.uid = value;
	}


	/**
	 * 获取请求保留字，拓展用，选填
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveIn value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置请求保留字，拓展用，选填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setReserveIn(Map<String,String> value)
	{
		if (value != null) {
				this.ReserveIn = value;
		}else{
				this.ReserveIn = new HashMap<String,String>();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetPromotePriceByTuanIdReq)
				length += ByteStream.getObjectSize(Source, null);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(MachineKey, null);  //计算字段MachineKey的长度 size_of(String)
				length += 4;  //计算字段SceneId的长度 size_of(uint32_t)
				length += 17;  //计算字段tuanId的长度 size_of(uint64_t)
				length += 17;  //计算字段uid的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(ReserveIn, null);  //计算字段ReserveIn的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetPromotePriceByTuanIdReq)
				length += ByteStream.getObjectSize(Source, encoding);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(MachineKey, encoding);  //计算字段MachineKey的长度 size_of(String)
				length += 4;  //计算字段SceneId的长度 size_of(uint32_t)
				length += 17;  //计算字段tuanId的长度 size_of(uint64_t)
				length += 17;  //计算字段uid的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(ReserveIn, encoding);  //计算字段ReserveIn的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
