package com.qq.qqbuy.gchoice.idl;

 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: tuantuanjiang.idl.TuanTuanJiangReadAo.java



import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Map;
import java.util.HashMap;

/**
 *请求参数
 *
 *@date 2015-03-04 10:57:53
 *
 *@since version:0
*/
public class  GetPriceByItemIdReq implements IServiceObject
{
	/**
	 * 来源，必填
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String MachineKey = new String();

	/**
	 * 场景，必填，拍便宜业务填1
	 *
	 * 版本 >= 0
	 */
	 private long SceneId;

	/**
	 * 商品id
	 *
	 * 版本 >= 0
	 */
	 private String itemId = new String();

	/**
	 * 用户id
	 *
	 * 版本 >= 0
	 */
	 private long uid;

	/**
	 * 订单来源str
	 *
	 * 版本 >= 0
	 */
	 private String dealSource = new String();

	/**
	 * 订单来源int
	 *
	 * 版本 >= 0
	 */
	 private long orderFrom;

	/**
	 * 请求保留字，拓展用，选填
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> ReserveIn = new HashMap<String,String>();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(Source);
		bs.pushString(MachineKey);
		bs.pushUInt(SceneId);
		bs.pushString(itemId);
		bs.pushLong(uid);
		bs.pushString(dealSource);
		bs.pushUInt(orderFrom);
		bs.pushObject(ReserveIn);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		Source = bs.popString();
		MachineKey = bs.popString();
		SceneId = bs.popUInt();
		itemId = bs.popString();
		uid = bs.popLong();
		dealSource = bs.popString();
		orderFrom = bs.popUInt();
		ReserveIn = (Map<String,String>)bs.popMap(String.class,String.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x875b1805L;
	}


	/**
	 * 获取来源，必填
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置来源，必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return MachineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return MachineKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.MachineKey = value;
	}


	/**
	 * 获取场景，必填，拍便宜业务填1
	 * 
	 * 此字段的版本 >= 0
	 * @return SceneId value 类型为:long
	 * 
	 */
	public long getSceneId()
	{
		return SceneId;
	}


	/**
	 * 设置场景，必填，拍便宜业务填1
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSceneId(long value)
	{
		this.SceneId = value;
	}


	/**
	 * 获取商品id
	 * 
	 * 此字段的版本 >= 0
	 * @return itemId value 类型为:String
	 * 
	 */
	public String getItemId()
	{
		return itemId;
	}


	/**
	 * 设置商品id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemId(String value)
	{
		this.itemId = value;
	}


	/**
	 * 获取用户id
	 * 
	 * 此字段的版本 >= 0
	 * @return uid value 类型为:long
	 * 
	 */
	public long getUid()
	{
		return uid;
	}


	/**
	 * 设置用户id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUid(long value)
	{
		this.uid = value;
	}


	/**
	 * 获取订单来源str
	 * 
	 * 此字段的版本 >= 0
	 * @return dealSource value 类型为:String
	 * 
	 */
	public String getDealSource()
	{
		return dealSource;
	}


	/**
	 * 设置订单来源str
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealSource(String value)
	{
		this.dealSource = value;
	}


	/**
	 * 获取订单来源int
	 * 
	 * 此字段的版本 >= 0
	 * @return orderFrom value 类型为:long
	 * 
	 */
	public long getOrderFrom()
	{
		return orderFrom;
	}


	/**
	 * 设置订单来源int
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOrderFrom(long value)
	{
		this.orderFrom = value;
	}


	/**
	 * 获取请求保留字，拓展用，选填
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveIn value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置请求保留字，拓展用，选填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setReserveIn(Map<String,String> value)
	{
		if (value != null) {
				this.ReserveIn = value;
		}else{
				this.ReserveIn = new HashMap<String,String>();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetPriceByItemIdReq)
				length += ByteStream.getObjectSize(Source, null);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(MachineKey, null);  //计算字段MachineKey的长度 size_of(String)
				length += 4;  //计算字段SceneId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(itemId, null);  //计算字段itemId的长度 size_of(String)
				length += 17;  //计算字段uid的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(dealSource, null);  //计算字段dealSource的长度 size_of(String)
				length += 4;  //计算字段orderFrom的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ReserveIn, null);  //计算字段ReserveIn的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetPriceByItemIdReq)
				length += ByteStream.getObjectSize(Source, encoding);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(MachineKey, encoding);  //计算字段MachineKey的长度 size_of(String)
				length += 4;  //计算字段SceneId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(itemId, encoding);  //计算字段itemId的长度 size_of(String)
				length += 17;  //计算字段uid的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(dealSource, encoding);  //计算字段dealSource的长度 size_of(String)
				length += 4;  //计算字段orderFrom的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ReserveIn, encoding);  //计算字段ReserveIn的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
