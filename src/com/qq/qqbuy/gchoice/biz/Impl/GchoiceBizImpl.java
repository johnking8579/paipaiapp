package com.qq.qqbuy.gchoice.biz.Impl;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.zip.GZIPInputStream;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.client.log.UrlConnProxy;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.common.util.HttpUtil.ProxyManager;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.gchoice.biz.GchoiceBiz;
import com.qq.qqbuy.gchoice.po.GatherChoice;

public class GchoiceBizImpl implements GchoiceBiz {
	
	private static final String GC_SAME_URL ="http://static.paipaiimg.com/act_cms/jujingpin/jjp_list_";
	/**
	 * 获取当前时间的静态聚精品数据
	 */
	@Override
	public GatherChoice getGatherChoice(long l) {
		Log.run.info("getGatherChoice -->>-->>begin");
		GatherChoice gChoice = new GatherChoice();
		//将所传时间转成日期并获取小时用于判断是第几场
		Calendar cld = Calendar.getInstance();
		cld.setTimeInMillis(l);
		int h = cld.get(Calendar.HOUR_OF_DAY);
		int cc = getCc(h);
		Log.run.info("getGatherChoice -->>-->>cc"+GC_SAME_URL+cc+".js");
		String cotext = fromPpms(GC_SAME_URL+cc+".js",true);
		if(cotext!=null&&!"".equals(cotext)){
			gChoice = this.parsmGather(cotext,l);
		}
		//如果上面获取为空则进行时时获取
		if(gChoice==null){
			gChoice= this.getByGatherChoice();
		}
		return gChoice;
	}
	/**
	 * 
	
	 * @Title: getCc 
	
	 * @Description:将日期传入给出当前场次
	
	 * @param @param i
	 * @param @return    设定文件 
	
	 * @return int    返回类型 
	
	 * @throws
	 */
	public int getCc(int i){
		if(i>=1&&i<7){
			return 0;
		}else if(i>=7&&i<9) {
			return 1;
		}else if(i>=9&&i<11) {
			return 2;
		}else if(i>=11&&i<13){
			return 3;
		}else  if(i>=13&&i<15){
			return 4;
		}else  if(i>=15&&i<17){
			return 5;
		}else  if(i>=17&&i<19){
			return 6;
		}else  if(i>=19&&i<21){
			return 7;
		}else  if(i>=21&&i<23){
			return 8;
		}else  if(i>=23&&i<24){
			return 9;
		}else {
			return 10;
		}
	}
	/**
	 * 
	
	 * @Title: fromPpms 
	
	 * @Description:获取数据 
	
	 * @param @param url
	 * @param @param useProxy
	 * @param @return    设定文件 
	
	 * @return String    返回类型 
	
	 * @throws
	 */
	public  String  fromPpms(String url, boolean useProxy) 	{
		Log.run.info("fromPpms -->>-->>begin");
		BufferedInputStream is = null;
		try {
			HttpURLConnection conn;
			if(useProxy)	{
				conn = (HttpURLConnection)new URL(url).openConnection(ProxyManager.getProxy());
			} else	{
				conn = (HttpURLConnection)new URL(url).openConnection();
			}
			if("gzip".equalsIgnoreCase(conn.getContentEncoding()))	{
				is = new BufferedInputStream(new GZIPInputStream(new UrlConnProxy(conn).getInputStream()));
			} else	{
				is = new BufferedInputStream(new UrlConnProxy(conn).getInputStream());
			}
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			int i;
			while((i=is.read()) != -1)	{
				baos.write(i);
			}
		
			String recoresult = new String(baos.toByteArray(), "GBK");
			Log.run.info("fromPpms -->>-->>url:"+url+":"+recoresult);
			return recoresult;
		}catch(IOException e){
			return null;
		}finally{
			Util.closeStream(is);
		}
	}
	/**
	 * 
	
	 * @Title: parsmGather 
	
	 * @Description: 将json转成对象
	
	 * @param @param recoresult
	 * @param @return    设定文件 
	
	 * @return GatherChoice    返回类型 
	
	 * @throws
	 */
	public GatherChoice parsmGather(String recoresult, long l) {
		Log.run.info("parsmGather -->>-->>begin"+recoresult);
		String st = "try{GetListCb(";
		String ed = ");}catch(e){}";
		GatherChoice gc = null;
		// 如果接口返回值 不为空则处理接口返回数据
		if (recoresult != null && !"".equals(recoresult)) {
			int stn = recoresult.indexOf(st);
			int edn = recoresult.indexOf(ed);
			if (stn >= 0 && edn > 0) {
				recoresult = recoresult.substring(stn + st.length(), edn);
				JsonParser jsonParser = new JsonParser();
				// 取出库存里的东西
				JsonElement jsonE1Bl = jsonParser.parse(recoresult);
				JsonElement dataBl2 = jsonE1Bl.getAsJsonObject().get("data");
				if (dataBl2.isJsonArray()
						&& dataBl2.getAsJsonArray().size() > 0
						&& dataBl2.getAsJsonArray().get(0) != null) {
					JsonElement datacontexbl2 = dataBl2.getAsJsonArray().get(0)
							.getAsJsonObject().get("data");
					String AreaStartTime = dataBl2.getAsJsonArray().get(0)
							.getAsJsonObject().get("AreaStartTime")
							.getAsString() == null ? "0" : dataBl2
							.getAsJsonArray().get(0).getAsJsonObject()
							.get("AreaStartTime").getAsString();
					String AreaEndTime = dataBl2.getAsJsonArray().get(0)
							.getAsJsonObject().get("AreaEndTime").getAsString() == null ? "0"
							: dataBl2.getAsJsonArray().get(0).getAsJsonObject()
									.get("AreaEndTime").getAsString();
					//如果当前时间在获取的数据之间则
					if((l/1000)>=Long.valueOf(AreaStartTime)&&(l/1000)<=Long.valueOf(AreaEndTime)){
						if (datacontexbl2 != null && datacontexbl2.isJsonArray()) {
							for (Iterator bliter = datacontexbl2.getAsJsonArray()
									.iterator(); bliter.hasNext();) {
								Object ob = bliter.next();
								if (ob != null && !(ob instanceof JsonNull)) {
									JsonObject obj = (JsonObject) ob;
									gc = new Gson().fromJson(obj.toString(),
											GatherChoice.class);
									// 设置活动的开始结束时间
									gc.setAreaStartTime(AreaStartTime);
									gc.setAreaEndTime(AreaEndTime);
									gc.setUrl("http://app.paipai.com/polyBoutique/index.html");
									gc.setNowTime(String.valueOf(System
											.currentTimeMillis() / 1000));
									gc.setStock(obj.getAsJsonObject()
											.get("orgStock").getAsString());
									if ("85A8DF".equals(gc.getExt3_colormark())) {
										break;
									}
								}
							}
						}
					}
				}
			}
		}
		Log.run.info("parsmGather -->>-->>gc"+gc.toString());
		return gc;

	}
	/**
	 * 
	
	 * @Title: getGatherChoice 
	
	 * @Description: 动态获取聚精品信息
	
	 * @param @return    设定文件 
	
	 * @return GatherChoice    返回类型 
	
	 * @throws
	 */
	public GatherChoice getByGatherChoice(){
		Log.run.info("getByGatherChoice -->>-->>begin");
		GatherChoice gc = new GatherChoice();
		//以下为动态获取
		// 准备好两个cgi请求URL
		String url = "http://act.paipai.com/newjujingpinpc/GetList?infotype=3";
		String url2 = "http://act.paipai.com/newjujingpinpc/GetList?infotype=2";
		String recoresult = "";
		String recoresult2 = "";
		try {
			// 商品纬度的聚精品
			recoresult = HttpUtil.get(url, 10000, 10000, "gb2312", true);
			// 库存纬度的聚精品
			recoresult2 = HttpUtil.get(url2, 10000, 10000, "gb2312", true);
		} catch (Exception e) {
			return gc;
		}
		String st = "try{GetListCb(";
		String ed = ");}catch(e){}";
		
		// 如果接口返回值 不为空则处理接口返回数据
		if (recoresult != null && !"".equals(recoresult) && recoresult2 != null
				&& !"".equals(recoresult2)) {
			//
			int stn = recoresult.indexOf(st);
			int edn = recoresult.indexOf(ed);
			int stn2 = recoresult2.indexOf(st);
			int edn2 = recoresult2.indexOf(ed);
			if (stn >= 0 && edn > 0) {
				recoresult = recoresult.substring(stn + st.length(), edn);
				recoresult2 = recoresult2.substring(stn2 + st.length(), edn2);
				JsonParser jsonParser = new JsonParser();
				 boolean isgetFirst = true;
				//先去找数据中标蓝的数据
				// 取出库存里的东西
				JsonElement jsonE1Bl = jsonParser.parse(recoresult2);
				JsonElement dataBl2 = jsonE1Bl.getAsJsonObject().get("data");
				if(dataBl2.isJsonArray()&&dataBl2.getAsJsonArray().size()>0&&dataBl2.getAsJsonArray().get(0)!=null){
					JsonElement datacontexbl2 = dataBl2.getAsJsonArray().get(0).getAsJsonObject().get("data");
					String AreaStartTime = dataBl2.getAsJsonArray().get(0).getAsJsonObject().get("AreaStartTime").getAsString()==null?"0":dataBl2.getAsJsonArray().get(0).getAsJsonObject().get("AreaStartTime").getAsString();
					String AreaEndTime = dataBl2.getAsJsonArray().get(0).getAsJsonObject().get("AreaEndTime").getAsString()==null?"0":dataBl2.getAsJsonArray().get(0).getAsJsonObject().get("AreaEndTime").getAsString();
					if (datacontexbl2 != null && datacontexbl2.isJsonArray()) {
						 for(Iterator bliter=datacontexbl2.getAsJsonArray().iterator();bliter.hasNext();){
							 Object ob = bliter.next();
							 if(ob!=null&&!(ob instanceof JsonNull)){
						        JsonObject obj=(JsonObject)ob;
						        		gc = new Gson().fromJson(obj.toString(), GatherChoice.class);
						        		//设置活动的开始结束时间
						        		gc.setAreaStartTime(AreaStartTime);
										gc.setAreaEndTime(AreaEndTime);
						        		gc.setUrl("http://app.paipai.com/polyBoutique/index.html");
						        		gc.setNowTime(String.valueOf(System.currentTimeMillis()/1000));
						        		gc.setStock(obj.getAsJsonObject().get("orgStock").getAsString());
						        		if("85A8DF".equals(gc.getExt3_colormark())){
						        		isgetFirst=false;
						        		break;
						        		}
							 }
						   }
						 
							// 先将商品的精品中的值取出来
							JsonElement jsonElement = jsonParser.parse(recoresult);
							JsonElement data = jsonElement.getAsJsonObject().get("data");
							if (data.isJsonArray() && data.getAsJsonArray().size() > 0
									&& data.getAsJsonArray().get(0) != null) {
								JsonElement datacontex = data.getAsJsonArray().get(0).getAsJsonObject().get("data");
								if (datacontex != null && datacontex.isJsonArray()) {
									  for(Iterator iter=datacontex.getAsJsonArray().iterator();iter.hasNext();){
										  	Object ob = iter.next();
											 if(ob!=null&&!(ob instanceof JsonNull)){
										        JsonObject obj=(JsonObject)ob;
										        if(obj!=null){
											        	//设置商品的实际价格
											        	if(obj.getAsJsonObject().get("itemId").getAsString().equals(gc.getItemId())){
														gc.setMarketPrice(obj.getAsJsonObject().get("marketPrice").getAsString());
														break;
											        	}
										        	}
											  }
									       }
			
										}else {
											Log.run.info("获取聚精品商品纬度接口时数据格式出错了！");
										}
							}else {
								Log.run.info("获取聚精品商品纬度接口时数据格式出错了！");
							}
						
					}else {
						Log.run.info("获取聚精品库存纬度接口时数据格式出错了！");
					}
					
				}else {
					Log.run.info("获取聚精品库存纬度接口时数据格式出错了！");
				}
					//如果没有一个标蓝的则
					if(isgetFirst){
								gc = new GatherChoice();
								// 先将商品的精品中的值取出来
								JsonElement jsonElement = jsonParser.parse(recoresult);
								gc.setNowTime(String.valueOf(System.currentTimeMillis()/1000));
								JsonElement data = jsonElement.getAsJsonObject().get("data");
								Log.run.info(data);
								// 将出第一个商品的实际价格85A8DF
								if (data.isJsonArray() && data.getAsJsonArray().size() > 0
										&& data.getAsJsonArray().get(0) != null) {
									JsonElement datacontex = data.getAsJsonArray().get(0).getAsJsonObject().get("data");
									if (datacontex != null && datacontex.isJsonArray()) {
										  for(Iterator iter=datacontex.getAsJsonArray().iterator();iter.hasNext();){
											  	Object ob = iter.next();
												 if(ob!=null&&!(ob instanceof JsonNull)){
											        JsonObject obj=(JsonObject)ob;
										        if(obj!=null){
										        	//设置商品的id以及实际价格
										        	gc.setItemId(obj.getAsJsonObject().get("itemId").getAsString());
										        	//gc.setUrl("http://auction1.paipai.com/"+gc.getItemId());
										        	gc.setUrl("http://app.paipai.com/polyBoutique/index.html");
													gc.setMarketPrice(obj.getAsJsonObject().get("marketPrice").getAsString());
													break;
										        }
											  }
										       }
				
											}else {
												Log.run.info("获取聚精品商品纬度接口时数据格式出错了！");
											}
									// 取出库存里的东西
									JsonElement jsonEl = jsonParser.parse(recoresult2);
									JsonElement data2 = jsonEl.getAsJsonObject().get("data");
									if(data2.isJsonArray()&&data2.getAsJsonArray().size()>0&&data2.getAsJsonArray().get(0)!=null){
										//设置活动的开始结束时间
										String AreaStartTime = data2.getAsJsonArray().get(0).getAsJsonObject().get("AreaStartTime").getAsString()==null?"0":data2.getAsJsonArray().get(0).getAsJsonObject().get("AreaStartTime").getAsString();
										gc.setAreaStartTime(AreaStartTime);
										String AreaEndTime = data2.getAsJsonArray().get(0).getAsJsonObject().get("AreaEndTime").getAsString()==null?"0":data2.getAsJsonArray().get(0).getAsJsonObject().get("AreaEndTime").getAsString();
										gc.setAreaEndTime(AreaEndTime);
										JsonElement datacontex2 = data2.getAsJsonArray().get(0).getAsJsonObject().get("data");
										if (datacontex2 != null && datacontex2.isJsonArray()) {
											 for(Iterator iter=datacontex2.getAsJsonArray().iterator();iter.hasNext();){
													Object ob = iter.next();
													 if(ob!=null&&!(ob instanceof JsonNull)){
												        JsonObject obj=(JsonObject)ob;
											        if(obj!=null){
											        	if(obj.getAsJsonObject().get("itemId").getAsString().equals(gc.getItemId())){
											        		//设置商品名称
											        		gc.setItemName(obj.getAsJsonObject().get("itemName").getAsString());
											        		gc.setPicUrl(obj.getAsJsonObject().get("picUrl").getAsString());
											        		gc.setPicUrl2(obj.getAsJsonObject().get("picUrl2").getAsString());
											        		gc.setPicUrl3(obj.getAsJsonObject().get("picUrl3").getAsString());
											        		gc.setPrice(obj.getAsJsonObject().get("price").getAsString());
											        		gc.setShopId(obj.getAsJsonObject().get("shopId").getAsString());
											        		gc.setStock(obj.getAsJsonObject().get("orgStock").getAsString());
											        		gc.setExt3_colormark(obj.getAsJsonObject().get("ext3_colormark").getAsString());
											        	}
														break;
											        }
												 }
											   }
											
										}else {
											Log.run.info("获取聚精品库存纬度接口时数据格式出错了！");
										}
									}else {
										Log.run.info("获取聚精品库存纬度接口时数据格式出错了！");
									}
								}else {
									Log.run.info("获取聚精品商品纬度接口时数据格式出错了！");
								}
						}
			}else {
				Log.run.info("聚精品商品获取的数据格式不对！");
			}

		}else {
			Log.run.info("聚精品商品获取数据时为空！");
		}
	
		return gc;
	}
	public static void main(String[] args) {
		Date dt = new Date(System.currentTimeMillis());
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		System.out.println(df2.format(dt));
		Calendar cld = Calendar.getInstance();
		cld.setTimeInMillis(System.currentTimeMillis());
		System.out.println(cld.getTime());
		System.out.println(cld.get(Calendar.HOUR_OF_DAY));
		
	}
}
