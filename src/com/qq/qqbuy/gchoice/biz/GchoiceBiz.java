package com.qq.qqbuy.gchoice.biz;

import com.qq.qqbuy.gchoice.po.GatherChoice;

public interface GchoiceBiz {
	/**
	 * 
	
	 * @Title: getGatherChoice 
	
	 * @Description: 获取当前时间的静态聚精品数据 
	
	 * @param @param l
	 * @param @return    设定文件 
	
	 * @return GatherChoice    返回类型 
	
	 * @throws
	 */
	GatherChoice getGatherChoice(long l);

}
