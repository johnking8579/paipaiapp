 package com.qq.qqbuy.thirdparty.openapi;

import com.qq.qqbuy.thirdparty.openapi.impl.OpenApiProxyImpl;

public class OpenApi
 {
   private static OpenApiProxy proxy = new OpenApiProxyImpl();

  public static OpenApiProxy getProxy() {
	  return proxy;
  }
 }

