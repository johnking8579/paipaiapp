package com.qq.qqbuy.thirdparty.openapi;

import java.io.Serializable;

import com.paipai.util.string.StringUtil;
import com.qq.qqbuy.common.util.CoderUtil;

public class OpenApiRequest
  implements Serializable
{
  private static final long serialVersionUID = -8119157767583763096L;
  protected OpenApiArgList argList = new OpenApiArgList();
  
  public OpenApiRequest() {
    this.argList.addParam("format", "xml");
  }

  public OpenApiArgList getArglist() {
    return this.argList;
  }
  
	public static String getVisitkey(long Uin){
		return String.valueOf((Uin<<32)+System.currentTimeMillis());
	}

  public void setUin(long uin)
  {
    this.argList.addParam("uin", Long.valueOf(uin));
  }
  /**
   * 设置sid用于鉴权
   * @param sid
   * @date:2013-6-24
   * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
   * @version 1.0
   */
  public void setSid(String sid)
  {
//		if(!gray.isNotAllowGray()){
//			if(StringUtil.isNotEmpty(sid)){
//				if(StringUtil.isNotEmpty(sid) && sid.contains("%")){
//					this.argList.addParam("sid", CoderUtil.decodeUtf8(sid));//处理被encode过后的sid，此处需还原					
//				}else{
//					this.argList.addParam("sid", sid);
//				}
//			}	
//		}	
  }
  /**
   * 设置lskey用于鉴权
   * @param lskey
   * @date:2013-6-24
   * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
   * @version 1.0
   */
  public void setLskey(String lskey)
  {
//	  if(!gray.isNotAllowGray()){
//		  if(StringUtil.isNotEmpty(lskey)){
//			    this.argList.addParam("lskey", lskey);
//		  } 
//	  }	 	  
  }

  public String toString() {
    return this.argList.toString();
  }
}