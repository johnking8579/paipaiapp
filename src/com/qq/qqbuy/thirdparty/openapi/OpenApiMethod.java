package com.qq.qqbuy.thirdparty.openapi;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.TreeMap;

import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.paipai.component.configcenter.api.app.SvcRoute;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.thirdparty.openapi.impl.ErrorInfo;

public abstract class OpenApiMethod<RequestType extends OpenApiRequest, ResponseType extends OpenApiResponse> {
	protected String serviceUrlSuffix = ".xhtml";
	protected String serviceModelName = "";
	protected String serviceActionName = "";
	protected boolean signRequired = false;
	protected long clientErrorCode = 0;
	protected String clientErrorMessage = null;
	protected Document xmlDoc = null;
	protected OpenApiArgList argList = null;
	
	private static long invokeCount = 0;
	
	public OpenApiMethod() {
		init();
	}
	
	/**
	 * 子类中必须重载本方法,设置serviceModelName和serviceActionName以及signRequired
	 */
	protected abstract void init();
	
	/**
	 * 子类中必须重载本方法,创建相应的Response对象
	 */
	protected abstract ResponseType createResponse();
	
	/**
	 * 子类中必须重载本方法,根据服务端返回的XML内容构造相应的Response对象
	 *
	 * @param xmlDoc 服务侧返回的XML Document对象
	 */
	protected abstract void handleResponse(ResponseType res, long clientErrorCode, final String clientErrorMessage, final Document xmlDoc);
	
	protected void setResponseError(final ResponseType res, long errCode, boolean isClientError) {
		if (res != null) {
			if (isClientError) {
				res.common.clientErrorCode = errCode;
				res.common.clientErrorMessage = ErrorInfo.getErrorMessage(errCode);
			}
			else {
				res.common.serverErrorCode = errCode;
				res.common.serverErrorMessage = ErrorInfo.getErrorMessage(errCode);
			}
		}
	}
	
	private InetSocketAddress getOpenApiUrl(){
		
			invokeCount++;
			InetSocketAddress address = SvcRoute.getSvcAddress("qgo.sys.openapi", (int)invokeCount);
			return address;		
		
	}
	
	public ResponseType invoke(final RequestType req) {
	    
		ResponseType res = null;
		long start = System.currentTimeMillis();
		String host=null;
		String url = null;
		int port =0;
		String servAddr=null;
		if(EnvManager.isLocal()){
			servAddr= "api.paipai.com";
		}else{
			 host = getOpenApiUrl().getHostName();
			 port = getOpenApiUrl().getPort();
			 servAddr = host+":"+port;
		}
				
		try {
			// 准备参数列表
			argList = req.getArglist();

			// 准备调用
			res = createResponse();
			url = "http://" + servAddr + "/" + serviceModelName + "/" + serviceActionName + serviceUrlSuffix + "?charset=utf-8";
			
			// 如果需要鉴权,添加鉴权参数并进行签名
			if (this.signRequired) {
				argList.addParam("token", OpenApiConstant.TOKEN);		
				argList.addParam("spid", OpenApiConstant.SPID);
			}
			
			// 增加时间戳
			//argList.addParam("t", PaipaiUtil.getCurTimeStr());
			
			String paramStr = toQueryString(argList);
			if (this.signRequired) {
				String sign = makeSign();
				paramStr = paramStr + "&sign=" + sign;
			}
			
			// POST到OpenApi
			byte[] recvBuf = doPost(url, paramStr);
			
			// 根据接收到的消息创建Xml文档树
			xmlDoc = buildXmlDoc(recvBuf);
			
			// 根据结果信息构造应答
			handleResponse(res, clientErrorCode, clientErrorMessage, xmlDoc);
			
			// 日志
			if (isDebugEnabled()) {
    			StringBuilder sb = new StringBuilder();
                sb.append("[thread:").append(Thread.currentThread().getId()).append("] ");
                sb.append("http response parsed: ");
                if (res != null) {
            	  if ( res.getErrorCode() != 0) {
                      sb.append("errCode=").append(res.getErrorCode()).append(", errMsg=").append(res.getErrorMessage());
                  } else {
                      sb.append(res.toString());
                  }
                } else {
                	sb.append("null");
                }
                debug(sb.toString());
			}
			return res;
		} catch (JDOMException e) {
			// 异常日志
			setResponseError(res, ErrorInfo.ERRNO_XML_DOM_FAILED, false);
			StringBuilder sb = new StringBuilder();
            sb.append("[thread:").append(Thread.currentThread().getId()).append("] ");
            sb.append("[").append(servAddr).append("]\t");
            if(res != null){
            	sb.append(e.getClass().getSimpleName()).append("\t");
            }
            sb.append(""+req);
			Log.run.warn(sb.toString(),e);
			return res;
		} catch (IOException e) {
			// 异常日志
			setResponseError(res, ErrorInfo.ERRNO_HTTP_IO_FAILED, true);
			StringBuilder sb = new StringBuilder();
            sb.append("[thread:").append(Thread.currentThread().getId()).append("] ");
            if(res != null){
            	sb.append("[").append(this.serviceModelName).append("/").append(this.serviceActionName).append("]\t");
            	sb.append("[").append(servAddr).append("]\t");
            	sb.append(res.getErrorMessage()).append("(").append(e.getClass().getSimpleName()).append(")");
            	sb.append("\tCost ").append(System.currentTimeMillis()-start).append("ms");
            	sb.append("\t");
            }
            sb.append(""+req);
			Log.run.warn(sb.toString(),e);
			return res;
		} finally {
			// 调用结果统计上报
			boolean ok = res != null && !res.needsAlarm() ;
			// 访问日志
			Log.run.info(toAccessLog(servAddr, (res != null && res.isSucceed()) ? 200 : 500, serviceActionName, System
							.currentTimeMillis()
							- start, res != null && res.getErrorCode() == 0 ? "" : res.getErrorMessage(),req,res));			
		}		
	}
	
//	private void writePerformanceLog(TimeCostLogger costLogger) {
//		StringBuilder sb = new StringBuilder();
//		sb.append("[thread:").append(Thread.currentThread().getId()).append("] ");
//		sb.append("invoke_cost\t");
//		sb.append("name:").append(serviceModelName).append(".").append(serviceActionName).append("\t");
//		long temp = costLogger.getTime("total");
//		sb.append("total:").append((temp>0?temp:"")).append(" us\t");
//		temp = costLogger.getTime("buildURL");
//		sb.append("buildURL=").append((temp>0?temp:"")).append(" us\t");
//		temp = costLogger.getTime("doPost");
//		sb.append("doPost=").append((temp>0?temp:"")).append(" us\t");
//		temp = costLogger.getTime("buildXMLDoc");
//		sb.append("buildXMLDoc=").append((temp>0?temp:"")).append(" us\t");
//		temp = costLogger.getTime("handleResponse");
//		sb.append("handleResponse=").append((temp>0?temp:"")).append(" us\t");
//		Log.run.info(sb.toString());
//	}
	
	private byte[] doPost(final String urlStr, final String paramStr) throws IOException, JDOMException {
		HttpURLConnection huc = null;
		byte []buf = new byte[1024];
		ByteArrayOutputStream bos = new ByteArrayOutputStream(2048);
		
		// DEBUG日志
		if (isDebugEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("[thread:").append(Thread.currentThread().getId()).append("] ");
			sb.append("http request post to OpenApi(");
			sb.append("url: ").append(urlStr).append(" | ");
			sb.append("params: ").append(paramStr).append(")");
			debug(sb.toString());
		}
		
		try {
			URL url = new URL(urlStr);
			huc = (HttpURLConnection) url.openConnection();
			huc.setConnectTimeout(OpenApiConstant.HTTP_CONNECT_TIMEOUT);
			huc.setReadTimeout(OpenApiConstant.HTTP_READ_TIMEOUT);
			huc.setRequestMethod("POST");
			huc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			if (!StringUtil.isEmpty(paramStr)) {
				huc.setDoOutput(true);
				OutputStreamWriter wr = new OutputStreamWriter(huc.getOutputStream(), OpenApiConstant.CHARSET);
				wr.write(paramStr);
				wr.flush();
			}
			
			InputStream is = huc.getInputStream();
			int length = -1;
			while((length = is.read(buf))>=0) {
				bos.write(buf, 0, length);
			}
		} finally {
			if (huc != null) {
				huc.disconnect();
			}
		}
		
		buf = bos.toByteArray();
		// DEBUG日志
		if (isDebugEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("[thread:").append(Thread.currentThread().getId()).append("] ");
			int length = (buf == null ? 0 : buf.length);
			sb.append("http response received from OpenApi(" + length + " bytes)");
			debug(sb.toString());
		}
		return buf;
	}
	
	/**
	 * 根据字节数组创建XML文档对象
	 * @param buf 字节数组
	 * @return 创建的XML文档对象
	 * @throws IOException
	 * @throws JDOMException
	 */
	protected Document buildXmlDoc(final byte[] buf) throws IOException, JDOMException {
		// 根据缓冲区创建String对象
		String strDoc = new String(buf, OpenApiConstant.CHARSET);
		
		// 解析xml前对xml内容进行前处理
		strDoc = preprocessXmlContent(strDoc);
		
		// 解析xml
		return new SAXBuilder().build(new StringReader(strDoc));
	}
	
	/**
	 * 在解析xml文档前对xml内容进行预处理,子类中可重载本方法对xml内容进行预处理
	 * @param xmlContent 待处理的xml文档内容
	 * @return 处理后的xml文档内容
	 */
	protected String preprocessXmlContent(final String xmlContent) {
		return xmlContent;
	}
	
	/**
	 * 对xml文档中无法解析的字符进行预处理
	 * @param xmlContent 待处理的xml文档内容
	 * @return 处理后的xml文档内容
	 */
	protected String handleUnsupportedXmlChar(final String xmlContent) {
		// 1. 取xml内容到char数组中
		char[] content = new char[xmlContent.length()];
		xmlContent.getChars(0, xmlContent.length(), content, 0);
		
		// 2. 遍历char数组,替换无法解析的字符
		for (int i = 0; i < content.length; i++) {
			if (content[i] >= 0x00 && content[i] <= 0x08){
				content[i] = '*';
			}
			if (content[i] == 0x0b || content[i] == 0x0c){
				content[i] = '*';
			}
			if (content[i] >= 0x0e && content[i] <= 0x1f){
				content[i] = '*';
			}
		}
		return new String(content);
	}
	
	private String toQueryString(OpenApiArgList argList) throws UnsupportedEncodingException {
		if (null == argList || argList.size() == 0) {
			return null;
		}
		StringBuilder sb = new StringBuilder("");
		
		String paramName = null;
		String paramValue = null;
		Iterator<String> itrName = argList.getParamNames().iterator();
		while ( itrName.hasNext()) {
			paramName = itrName.next();
			paramValue = argList.getParamValue(paramName);
			if (paramValue == null || paramValue.trim().length() == 0) {
				continue;
			}
//			if(!gray.isAllowGray()){
//				if("sid".equals(paramName) || "lskey".equals(paramName)){
//					if(!serviceActionName.contains("ReceiverAddress")){//只对收货地址接口灰度
//						continue;
//					}
//				}
//			}
		
			if (sb.length() > 0) {
				sb.append("&");
			}
			sb.append(URLEncoder.encode(paramName, OpenApiConstant.CHARSET));
			sb.append("=");
			sb.append(URLEncoder.encode(paramValue.trim(), OpenApiConstant.CHARSET));
		}
		return sb.toString();
	}
	
	/**
	 * 
	 * @param host 调用方或被调用方：一般写IP,也可以是其它字符串
	 * @param result 返回码
	 * @param method 方法名
	 * @param costTime 耗时
	 * @param msg 其它信息
	 * @return
	 */
	private static String toAccessLog(String host, int result, String method, long costTime, Object... msg) {
		StringBuilder sb = new StringBuilder();
		sb.append(host).append("\t").append((""+method).replace('\t', '_')).append("\t").append(result).append("\t").append(costTime).append("\t");
		for (Object o : msg) {
			sb.append(("" + o).replaceAll("[\\\r\\\n\\\t]+", "_")).append("\t");
		}
		return sb.toString();
	}
	
	protected static boolean isDebugEnabled() {
		return Log.run.isDebugEnabled();
	}
	
	protected static void debug(final String content) {
		Log.run.debug(content);
	}
	
	private String makeSign() throws UnsupportedEncodingException, IOException {
		// 将需要签名的参数放入TreeMap进行排序
		TreeMap<String, String> signParams = new TreeMap<String, String>();
		signParams.put("charset", "utf-8");
		
		String paramName = null;
		String paramValue = null;
		Iterator<String> itrName = argList.getParamNames().iterator();
		while ( itrName.hasNext()) {
			paramName = itrName.next();
			paramValue = argList.getParamValue(paramName);	
			if (paramValue == null || paramValue.trim().length() == 0) {
				continue;
			}
			signParams.put(paramName, paramValue.trim());
		}
		
		// 将TreeMap中的参数输出到字节数组
		String cmdId = serviceModelName + "." + serviceActionName;
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		byteStream.write(cmdId.getBytes(OpenApiConstant.CHARSET));
		String value=null;
		for (String key : signParams.keySet()) {
			value=signParams.get(key);
			if((value!=null)&&(value.trim()!="")){
				byteStream.write(key.getBytes(OpenApiConstant.CHARSET));
				byteStream.write(value.getBytes(OpenApiConstant.CHARSET));
			}
		}
		// SecKey输出到字节数组
		byteStream.write(OpenApiConstant.SECKEY.getBytes(OpenApiConstant.CHARSET));
		return Util.toMd5(byteStream.toByteArray());
	}
}
