package com.qq.qqbuy.thirdparty.openapi.po;

import com.qq.qqbuy.thirdparty.openapi.OpenApiRequest;


/**
 * 特价抢购下单request
 * @author homerwu
 *
 */
public class AddOrderDealRequest extends OpenApiRequest {
		
	/**
	 * buyerUin:买家QQ号,同时根据买家qq号和时间戳生成visitkey
	 */
	public void setBuyerUin(long buyerUin) {
		this.argList.addParam("buyerUin", buyerUin);
		this.argList.addParam("visitkey", getVisitkey(buyerUin));
	}
	
	/**
	 * buyerName:买家名字
	 */
	public void setBuyerName(String buyerName) {
		this.argList.addParam("buyerName", buyerName);
	}

	/**
	 * sellerUin:卖家QQ号
	 */
	public void setSellerUin(long sellerUin) {
		this.argList.addParam("sellerUin", sellerUin);
	}
	
	/**
	 * itemCode:商品编码
	 */
	public void setItemCode(String itemCode) {
		this.argList.addParam("itemCode", itemCode);
	}
	
	/**
	 * itemNum:商品数量
	 */
	public void setItemNum(long itemNum) {
		this.argList.addParam("itemNum", itemNum);
	}
	
	/**
	 * buyNum:购买数量
	 */
	public void setBuyNum(long buyNum) {
		this.argList.addParam("buyNum", buyNum);
	}
	
	/**
	 * activeType:活动类型
	 */
	public void setActiveType(long activeType) {
		this.argList.addParam("activeType", activeType);
	}

	/**
	 * maxBuyNum:最大购买数量
	 */
	public void setMaxBuyNum(long maxBuyNum) {
		this.argList.addParam("maxBuyNum", maxBuyNum);
	}
	

	/**
	 * uploadTime:商品上架时间
	 */
	public void setUploadTime(String uploadTime) {
		this.argList.addParam("uploadTime", uploadTime);
	}
	
	/**
	 * phone:卖家电话
	 */
	public void setPhone(String phone) {
		this.argList.addParam("phone", phone);
	}

	/**
	 * mobile:买家手机
	 */
	public void setMobile(String mobile) {
		this.argList.addParam("mobile", mobile);
	}

	/**
	 * postCode:买家邮编
	 */
	public void setPostCode(String postCode) {
		this.argList.addParam("postCode", postCode);
	}

	/**
	 * address:买家地址
	 */
	public void setAddress(String address) {
		this.argList.addParam("address", address);
	}

	/**
	 * region:买家区域
	 */
	public void setRegion(long region) {
		this.argList.addParam("region", region);
	}

	/**
	 * addressId:买家地址ID
	 */
	public void setAddressId(long addressId) {
		this.argList.addParam("addressId", addressId);
	}

	/**
	 * property:标志是否有库存
	 */
	public void setProperty(String property) {
		this.argList.addParam("property", property);
	}

	/**
	 * buyerRemark:购买留言
	 */
	public void setBuyerRemark(String buyerRemark) {
		this.argList.addParam("buyerRemark", buyerRemark);
	}

	/**
	 * stockAttr:库存属性
	 */
	public void setStockAttr(String stockAttr) {
		this.argList.addParam("stockAttr", stockAttr);
	}

	/**
	 * price:商品价格,以分为单
	 */
	public void setPrice(long price) {
		this.argList.addParam("price", price);
	}
}