package com.qq.qqbuy.thirdparty.openapi.po;

import com.qq.qqbuy.thirdparty.openapi.OpenApiRequest;

public class GetFreightTemplateRequest extends OpenApiRequest {
    
    /**
     * 设置卖家QQ号
     * @param sellerUin 卖家QQ号
     */
    public void setSellerUin(long sellerUin) {
        this.argList.addParam("sellerUin", sellerUin);
    }
    
    /**
     * 设置运费模板Id
     * @param freightId 运费模板Id
     */
    public void setFreightId(int freightId) {
        this.argList.addParam("freightId", freightId);
    }
}
