package com.qq.qqbuy.thirdparty.openapi.po;

import com.qq.qqbuy.thirdparty.openapi.OpenApiRequest;

/**
 * 查询抢购结果request
 * @author homerwu
 *
 */
public class GetDealRetRequest extends OpenApiRequest {

	/**
	 * queueId:订单队列号
	 */
	public void setQueueId(long queueId) {
		this.argList.addParam("queueId", queueId);
	}

}
