package com.qq.qqbuy.thirdparty.openapi.po;

import com.qq.qqbuy.thirdparty.openapi.OpenApiRequest;

/**
 * 查询商品库存信息request
 * @author homerwu
 *
 */
public class GetComdyStockInfoRequest extends OpenApiRequest {
	
	/**
	 * itemCode:商品编码
	 */
	public void setItemCode(String itemCode) {
		this.argList.addParam("itemCode", itemCode);
	}
}
