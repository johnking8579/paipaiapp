package com.qq.qqbuy.thirdparty.openapi.po;

import java.util.ArrayList;
import java.util.List;

import com.qq.qqbuy.thirdparty.openapi.OpenApiResponse;

/**
 * 查询商品库存信息response
 * 
 * @author homerwu
 * 
 */
@SuppressWarnings("serial")
public class GetComdyStockInfoResponse extends OpenApiResponse
{

    @Override
    public boolean needsAlarm()
    {
        boolean requireAlarm = super.needsAlarm();
        if (requireAlarm)
        {
            // 过滤不需要作为接口调用失败上报的情况
            if (common.clientErrorCode == 0)
            {
                // 注意下面的错误码排序是按照情况出现频率由高到低以提升系统判断效率
                if (common.serverErrorCode == 0xaaa1 // 商品数量不足
                )
                {
                    requireAlarm = false;
                }
            }
        }
        return requireAlarm;
    }

    /** 商品列表 */
    public List<ComdyStockInfo> items = new ArrayList<ComdyStockInfo>();

    public class ComdyStockInfo implements java.io.Serializable
    {
        /**
         * itemCode:商品编码
         */
        public String itemCode = null;

        /**
         * itemName:商品名称
         */
        public String itemName = null;

        /**
         * 商品状态
         */
        public String itemState = "";

        /**
         * stockNum:商品总数
         */
        public long stockNum;

        /**
         * soldNum:已售数量
         */
        public long soldNum;

        /**
         * remainNum:剩余未售商品数量
         */
        public long remainNum;

        /**
         * limitBuyNum:限购数量
         */
        public long limitBuyNum;

        /**
         * uploadTime:上架时间
         */
        public String uploadTime;

        /**
         * price:商品价格
         */
        public long price;

        /**
         * desc:描述
         */
        public String desc;

        /**
         * property:属性值,1:有分库存,0：无分库存
         */
        public String property = null;

        /**
         * extinfo:扩展信息
         */
        public String extInfo = null;

        public List<Stock> stockList = new ArrayList<Stock>();

        public class Stock implements java.io.Serializable
        {

            public String stockId = null;
            public String stockLocalCode = null;
            public String attr = null;
            public long stockNum;
            public long soldNum;
            public long remainNum;
            public long price;
            public String desc = null;

            @Override
            public String toString()
            {
                return "Stock [attr=" + attr + ", desc=" + desc + ", price="
                        + price + ", remainNum=" + remainNum + ", soldNum="
                        + soldNum + ", stockId=" + stockId
                        + ", stockLocalCode=" + stockLocalCode + ", stockNum="
                        + stockNum + "]";
            }
        }

        @Override
        public String toString()
        {
            return "GetComdyStockInfoResponse [desc=" + desc + ", extInfo="
                    + extInfo + ", itemCode=" + itemCode + ", itemName="
                    + itemName + ", limitBuyNum=" + limitBuyNum + ", price="
                    + price + ", property=" + property + ", remainNum="
                    + remainNum + ", soldNum=" + soldNum + ", stockListCount="
                    + stockList.size() + ", stockNum=" + stockNum
                    + ", uploadTime=" + uploadTime + "]";
        }
    }

    @Override
    public String toString()
    {
        return "GetComdyStockInfoResponse [items=" + items + "]";
    }

}
