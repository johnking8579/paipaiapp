package com.qq.qqbuy.thirdparty.openapi.po;

import java.util.ArrayList;

import com.qq.qqbuy.thirdparty.openapi.OpenApiResponse;

public class GetFreightTemplateResponse extends OpenApiResponse {
    
    /**
     * 运费模板Id
     */
    public int freightId = 0;
    
    /**
     * 运费模版名
     */
    public String name = null;
    
    /**
     * 运费模板描述
     */
    public String desc = null;
    
    /**
     * 运费规则列表
     */
    public ArrayList<FreightRule> freightRuleList = new ArrayList<FreightRule>();
    
    /**
     * 运费规则
     */
    public class FreightRule implements java.io.Serializable{
        
        /**
		 * 
		 */
		private static final long serialVersionUID = 638812431668194606L;

		/**
         * 目的地，以全角“、”分割
         */
        public String dest = null;
        
        /**
         * 普通价格
         */
        public long priceNormal = 0;
        
        /**
         * 普通加量价格
         */
        public long priceNormalAdd = 0;
        
        /**
         * 快递价格
         */
        public long priceExpress = 0;
        
        /**
         * 快递加量价格
         */
        public long priceExpressAdd = 0;
        
        /**
         * EMS价格
         */
        public long priceEms = 0;
        
        /**
         * EMS加量价格
         */
        public long priceEmsAdd = 0;
    }
    
    @Override
    public String toString() {
        return "GetFreightTemplateResponse [freightId=" + freightId
                + ", name=" + name + ", desc=" + desc + ", freightRuleCount=" + freightRuleList.size()
                + "]";
    }
}
