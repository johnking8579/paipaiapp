package com.qq.qqbuy.thirdparty.openapi.po;

import com.qq.qqbuy.thirdparty.openapi.OpenApiResponse;

/**
 * 特价抢购下单response
 * @author homerwu
 *
 */
public class AddOrderDealResponse extends OpenApiResponse {

	/**
	 * dealId:订单编码
	 */
	public String queueId;
	
	/**
	 * beginDate:处罚开始时间
	 */
	public String beginDate;
	
	/**
	 * endDate:处罚结束时间
	 */
	public String endDate;

	@Override
    public boolean needsAlarm() {
	    boolean requireAlarm = super.needsAlarm();
	    if (requireAlarm) {
	        // 过滤不需要作为接口调用失败上报的情况
	        if (common.clientErrorCode == 0) {
	            
	            // 注意下面的错误码排序是按照情况出现频率由高到低以提升系统判断效率
	            if (common.serverErrorCode == 0xBBA6 // 商品数量不足
	             || common.serverErrorCode == 0xBBA3 // 50件以下商品,被延时10-15秒
	             || common.serverErrorCode == 0xBBA4 // 商品购买频率检查没有通过
	             || common.serverErrorCode == 0xBBA0 // 在拍下不买黑名单中
	             || common.serverErrorCode == 0xBBA1 // 用户QQ号被加入黑名单
	             || common.serverErrorCode == 0xBBB3 // 买自己的商品
	             || common.serverErrorCode == 8193 //购买限制检查没有通过
	                    ) {
	                requireAlarm = false;
	            }
	        }
	    }
	    return requireAlarm;
    }

    @Override
	public String toString() {
		return "AddOrderDealResponse [" + super.toString()
		        + ", beginDate=" + beginDate + ", endDate=" + endDate
		        + ", queueId=" + queueId + "]";
	}
	
}
