package com.qq.qqbuy.thirdparty.openapi.po;

import java.util.ArrayList;
import java.util.List;

import com.qq.qqbuy.thirdparty.openapi.OpenApiResponse;

public class GetDealDetailResponse extends OpenApiResponse {

	/**
	 * 订单支付ID
	 *
	 * 版本 >= 0
	 */
	public long PayId;
	
	/**
	 * buyerName:买家名称
	 */
	public String buyerName = null;

	/**
	 * buyerUin:买家QQ号
	 */
	public long buyerUin;

	/**
	 * buyerRemark:买家下订单时的备注
	 */
	public String buyerRemark = null;

	/**
	 * dealCode:订单编码
	 */
	public String dealCode = null;

	/**
	 * dealDesc:订单序列号
	 */
	public String dealDesc = null;

	/**
	 * dealDetailLink:订单的详情连接
	 */
	public String dealDetailLink = null;

	/**
	 * dealNoteType:订单备注类型
	 *  RED:红色
	 * YELLOW:黄色
	 * GREEN:绿色
	 * BLUE:蓝色
	 * PINK:粉红色
	 * UN_LABEL:未标注
	 */
	public String dealNoteType = null;

	/**
	 * dealNote:订单的备注内容
	 */
	public String dealNote = null;

	/**
	 * dealState:订单状态
	 */
	public String dealState = null;

	/**
	 * dealStateDesc:订单状态说明
	 */
	public String dealStateDesc = null;

	/**
	 * dealType:订单类型
	 */
	public String dealType = null;

	/**
	 * dealTypeDesc:订单类型描述
	 */
	public String dealTypeDesc = null;

	/**
	 * dealPayType:订单的支付方式
	 */
	public String dealPayType = null;

	/**
	 * dealPayTypeDesc:订单支付方式说明
	 */
	public String dealPayTypeDesc = null;

	/**
	 * dealRateState:订单的评价状态
	 */
	public String dealRateState = null;

	/**
	 * dealRateStateDesc:订单的评价状态说明
	 */
	public String dealRateStateDesc = null;

	/**
	 * createTime:订单的创建时间
	 */
	public String createTime = null;

	/**
	 * dealEndTime:订单结束时间
	 */
	public String dealEndTime = null;

	/**
	 * lastUpdateTime:订单的最后更新时间
	 */
	public String lastUpdateTime = null;

	/**
	 * payTime:买家的付款时间
	 */
	public String payTime = null;

	/**
	 * payReturnTime:打款返回时间
	 */
	public String payReturnTime = null;

	/**
	 * recvfeeReturnTime:退款返回时间
	 */
	public String recvfeeReturnTime = null;

	/**
	 * recvfeeTime:退款时间
	 */
	public String recvfeeTime = null;

	/**
	 * sellerConsignmentTime:	卖家发货时间
	 */
	public String sellerConsignmentTime = null;

	/**
	 * hasInvoice:是否提供发票:0否,1是
	 */
	public long hasInvoice;

	/**
	 * invoiceContent:发票内容
	 */
	public String invoiceContent = null;

	/**
	 * invoiceTitle:发票标题
	 */
	public String invoiceTitle = null;

	/**
	 * tenpayCode:财付通付款单号
	 */
	public String tenpayCode = null;

	/**
	 * transportType:运送类型
	 */
	public String transportType = null;

	/**
	 * transportTypeDesc:运费类型说明
	 */
	public String transportTypeDesc = null;

	/**
	 * whoPayShippingfee:	承担运费方式
	 */
	public long whoPayShippingfee;

	/**
	 * wuliuId:物流id
	 */
	public String wuliuId = null;

	/**
	 * receiverAddress:收货人详细地址
	 */
	public String receiverAddress = null;

	/**
	 * receiverMobile:收货人手机号码
	 */
	public String receiverMobile = null;

	/**
	 * receiverName:收货人姓名
	 */
	public String receiverName = null;

	/**
	 * receiverPhone:收货人电话号码
	 */
	public String receiverPhone = null;

	/**
	 * receiverPostcode:收货人邮编,为空时返回0
	 */
	public String receiverPostcode = null;

	/**
	 * sellerRecvRefund:退款:卖家实收金额
	 */
	public long sellerRecvRefund;

	/**
	 * buyerRecvRefund:退款:买家收到的退款金额
	 */
	public long buyerRecvRefund;

	/**
	 * couponFee:折扣优惠金额
	 */
	public long couponFee;

	/**
	 * dealPayFeePoint:实际积分支付金额
	 */
	public long dealPayFeePoint;

	/**
	 * dealPayFeeTicket:财付券支付金额
	 */
	public long dealPayFeeTicket;

	/**
	 * dealPayFeeTotal:费用合计
	 */
	public long dealPayFeeTotal;

	/**
	 * freight:	支付的运费(单位:分)
	 */
	public long freight;

	/**
	 * shippingfeeCalc:运费合计说明
	 */
	public long shippingfeeCalc;

	/**
	 * totalCash:买家支付现金总额
	 */
	public long totalCash;

	/**
	 * sellerCrm:客服CRM
	 */
	public String sellerCrm = null;

	/**
	 * sellerName:卖家名称
	 */
	public String sellerName = null;

	/**
	 * sellerUin:卖家QQ号
	 */
	public long sellerUin;
	
	/**
	 * 订单属性字段
	 */
	public String propertymask;
	
	/** itemList:订单的商品列表*/
	public List<Item> itemList=new ArrayList<Item>();
	
	public class Item{
		/** itemCode:商品编码*/
		public String itemCode=null;
		
		/** itemCodeHistory:订单的商品快照编码*/
		public String itemCodeHistory=null;
		
		/** itemLocalCode:商家自定义编码*/
		public String itemLocalCode=null;
		
		/** stockLocalCode:商品库存编码*/
		public String stockLocalCode=null;
		
		/** stockAttr:买家下单时选择的库存属性*/
		public String stockAttr=null;
		
		/** itemDetailLink:商品详情的url*/
		public String itemDetailLink=null;
		
		/** itemName:商品名称*/
		public String itemName=null;
		
		/** itemPic80:商品图片的url*/
		public String itemPic80=null;
		
		/** itemRetailPrice:商品的市场价格*/
		public long itemRetailPrice;
		
		/** itemDealPrice:买家下单时的商品价格*/
		public long itemDealPrice;
		
		/** itemAdjustPrice:订单的调整价格:正数为订单加价,负数为订单减价*/
		public long itemAdjustPrice;
		
		/** itemDealCount:购买的数量*/
		public long itemDealCount;
		
		/** account:充值帐号（点卡类商品订单中才有意义）*/
		public String account=null;
		
		/** itemFlag*/
		public String itemFlag=null;
		
		/** refundState:退款状态，有退款时才有值*/
		public String refundState=null;
		
		/** refundStateDesc:退款状态描述，有退款时才有值*/
		public String refundStateDesc=null;
		
		/** availableAction*/
		public String availableAction=null;
		
		/** itemDiscountFee:购买商品的红包值、折扣优惠价。。。*/
		public long itemDiscountFee;
		/** dealSubCode:子订单id。。。*/
		public String dealSubCode = null;
		
		/** itemDealState:子订单状态  **/
		public String itemDealState = "";
		
		/** itemDealStateDesc:子订单状态  描述**/
		public String itemDealStateDesc = "";
		
		
		@Override
		public String toString() {
			return "Item [itemCode=" + itemCode + ", itemCodeHistory="
					+ itemCodeHistory + ", itemLocalCode=" + itemLocalCode
					+ ", stockLocalCode=" + stockLocalCode + ", stockAttr="
					+ stockAttr + ", itemDetailLink=" + itemDetailLink
					+ ", itemName=" + itemName + ", itemPic80=" + itemPic80
					+ ", itemRetailPrice=" + itemRetailPrice
					+ ", itemDealPrice=" + itemDealPrice
					+ ", itemAdjustPrice=" + itemAdjustPrice
					+ ", itemDealCount=" + itemDealCount + ", account="
					+ account + ", itemFlag=" + itemFlag
					+ ", refundState=" + refundState + ", refundStateDesc="
					+ refundStateDesc + ", availableAction=" + availableAction
					+ ",itemDiscountFee=" + itemDiscountFee + ",dealSubCode="
					+ dealSubCode + ",itemDealState=" + itemDealState + ",itemDealStateDesc=" +itemDealStateDesc + "]";
		}
		
	}

	
	public String getBuyerName() {
		return buyerName;
	}


	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}


	public long getBuyerUin() {
		return buyerUin;
	}


	public void setBuyerUin(long buyerUin) {
		this.buyerUin = buyerUin;
	}


	public String getBuyerRemark() {
		return buyerRemark;
	}


	public void setBuyerRemark(String buyerRemark) {
		this.buyerRemark = buyerRemark;
	}


	public String getDealCode() {
		return dealCode;
	}


	public void setDealCode(String dealCode) {
		this.dealCode = dealCode;
	}


	public String getDealDesc() {
		return dealDesc;
	}


	public void setDealDesc(String dealDesc) {
		this.dealDesc = dealDesc;
	}


	public String getDealDetailLink() {
		return dealDetailLink;
	}


	public void setDealDetailLink(String dealDetailLink) {
		this.dealDetailLink = dealDetailLink;
	}


	public String getDealNoteType() {
		return dealNoteType;
	}


	public void setDealNoteType(String dealNoteType) {
		this.dealNoteType = dealNoteType;
	}


	public String getDealNote() {
		return dealNote;
	}


	public void setDealNote(String dealNote) {
		this.dealNote = dealNote;
	}


	public String getDealState() {
		return dealState;
	}


	public void setDealState(String dealState) {
		this.dealState = dealState;
	}


	public String getDealStateDesc() {
		return dealStateDesc;
	}


	public void setDealStateDesc(String dealStateDesc) {
		this.dealStateDesc = dealStateDesc;
	}


	public String getDealType() {
		return dealType;
	}


	public void setDealType(String dealType) {
		this.dealType = dealType;
	}


	public String getDealTypeDesc() {
		return dealTypeDesc;
	}


	public void setDealTypeDesc(String dealTypeDesc) {
		this.dealTypeDesc = dealTypeDesc;
	}


	public String getDealPayType() {
		return dealPayType;
	}


	public void setDealPayType(String dealPayType) {
		this.dealPayType = dealPayType;
	}


	public String getDealPayTypeDesc() {
		return dealPayTypeDesc;
	}


	public void setDealPayTypeDesc(String dealPayTypeDesc) {
		this.dealPayTypeDesc = dealPayTypeDesc;
	}


	public String getDealRateState() {
		return dealRateState;
	}


	public void setDealRateState(String dealRateState) {
		this.dealRateState = dealRateState;
	}


	public String getDealRateStateDesc() {
		return dealRateStateDesc;
	}


	public void setDealRateStateDesc(String dealRateStateDesc) {
		this.dealRateStateDesc = dealRateStateDesc;
	}


	public String getCreateTime() {
		return createTime;
	}


	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}


	public String getDealEndTime() {
		return dealEndTime;
	}


	public void setDealEndTime(String dealEndTime) {
		this.dealEndTime = dealEndTime;
	}


	public String getLastUpdateTime() {
		return lastUpdateTime;
	}


	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}


	public String getPayTime() {
		return payTime;
	}


	public void setPayTime(String payTime) {
		this.payTime = payTime;
	}


	public String getPayReturnTime() {
		return payReturnTime;
	}


	public void setPayReturnTime(String payReturnTime) {
		this.payReturnTime = payReturnTime;
	}


	public String getRecvfeeReturnTime() {
		return recvfeeReturnTime;
	}


	public void setRecvfeeReturnTime(String recvfeeReturnTime) {
		this.recvfeeReturnTime = recvfeeReturnTime;
	}


	public String getRecvfeeTime() {
		return recvfeeTime;
	}


	public void setRecvfeeTime(String recvfeeTime) {
		this.recvfeeTime = recvfeeTime;
	}


	public String getSellerConsignmentTime() {
		return sellerConsignmentTime;
	}


	public void setSellerConsignmentTime(String sellerConsignmentTime) {
		this.sellerConsignmentTime = sellerConsignmentTime;
	}


	public long getHasInvoice() {
		return hasInvoice;
	}


	public void setHasInvoice(long hasInvoice) {
		this.hasInvoice = hasInvoice;
	}


	public String getInvoiceContent() {
		return invoiceContent;
	}


	public void setInvoiceContent(String invoiceContent) {
		this.invoiceContent = invoiceContent;
	}


	public String getInvoiceTitle() {
		return invoiceTitle;
	}


	public void setInvoiceTitle(String invoiceTitle) {
		this.invoiceTitle = invoiceTitle;
	}


	public String getTenpayCode() {
		return tenpayCode;
	}


	public void setTenpayCode(String tenpayCode) {
		this.tenpayCode = tenpayCode;
	}


	public String getTransportType() {
		return transportType;
	}


	public void setTransportType(String transportType) {
		this.transportType = transportType;
	}


	public String getTransportTypeDesc() {
		return transportTypeDesc;
	}


	public void setTransportTypeDesc(String transportTypeDesc) {
		this.transportTypeDesc = transportTypeDesc;
	}


	public long getWhoPayShippingfee() {
		return whoPayShippingfee;
	}


	public void setWhoPayShippingfee(long whoPayShippingfee) {
		this.whoPayShippingfee = whoPayShippingfee;
	}


	public String getWuliuId() {
		return wuliuId;
	}


	public void setWuliuId(String wuliuId) {
		this.wuliuId = wuliuId;
	}


	public String getReceiverAddress() {
		return receiverAddress;
	}


	public void setReceiverAddress(String receiverAddress) {
		this.receiverAddress = receiverAddress;
	}


	public String getReceiverMobile() {
		return receiverMobile;
	}


	public void setReceiverMobile(String receiverMobile) {
		this.receiverMobile = receiverMobile;
	}


	public String getReceiverName() {
		return receiverName;
	}


	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}


	public String getReceiverPhone() {
		return receiverPhone;
	}


	public void setReceiverPhone(String receiverPhone) {
		this.receiverPhone = receiverPhone;
	}


	public String getReceiverPostcode() {
		return receiverPostcode;
	}


	public void setReceiverPostcode(String receiverPostcode) {
		this.receiverPostcode = receiverPostcode;
	}


	public long getSellerRecvRefund() {
		return sellerRecvRefund;
	}


	public void setSellerRecvRefund(long sellerRecvRefund) {
		this.sellerRecvRefund = sellerRecvRefund;
	}


	public long getBuyerRecvRefund() {
		return buyerRecvRefund;
	}


	public void setBuyerRecvRefund(long buyerRecvRefund) {
		this.buyerRecvRefund = buyerRecvRefund;
	}


	public long getCouponFee() {
		return couponFee;
	}


	public void setCouponFee(long couponFee) {
		this.couponFee = couponFee;
	}


	public long getDealPayFeePoint() {
		return dealPayFeePoint;
	}


	public void setDealPayFeePoint(long dealPayFeePoint) {
		this.dealPayFeePoint = dealPayFeePoint;
	}


	public long getDealPayFeeTicket() {
		return dealPayFeeTicket;
	}


	public void setDealPayFeeTicket(long dealPayFeeTicket) {
		this.dealPayFeeTicket = dealPayFeeTicket;
	}


	public long getDealPayFeeTotal() {
		return dealPayFeeTotal;
	}


	public void setDealPayFeeTotal(long dealPayFeeTotal) {
		this.dealPayFeeTotal = dealPayFeeTotal;
	}


	public long getFreight() {
		return freight;
	}


	public void setFreight(long freight) {
		this.freight = freight;
	}


	public long getShippingfeeCalc() {
		return shippingfeeCalc;
	}


	public void setShippingfeeCalc(long shippingfeeCalc) {
		this.shippingfeeCalc = shippingfeeCalc;
	}


	public long getTotalCash() {
		return totalCash;
	}


	public void setTotalCash(long totalCash) {
		this.totalCash = totalCash;
	}


	public String getSellerCrm() {
		return sellerCrm;
	}


	public void setSellerCrm(String sellerCrm) {
		this.sellerCrm = sellerCrm;
	}


	public String getSellerName() {
		return sellerName;
	}


	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}


	public long getSellerUin() {
		return sellerUin;
	}


	public void setSellerUin(long sellerUin) {
		this.sellerUin = sellerUin;
	}


	public List<Item> getItemList() {
		return itemList;
	}


	public void setItemList(List<Item> itemList) {
		this.itemList = itemList;
	}


	@Override
	public String toString() {
		return "GetDealDetailResponse [buyerName=" + buyerName + ", buyerUin="
				+ buyerUin + ", buyerRemark=" + buyerRemark + ", dealCode="
				+ dealCode + ", dealDesc=" + dealDesc + ", dealDetailLink="
				+ dealDetailLink + ", dealNoteType=" + dealNoteType
				+ ", dealNote=" + dealNote + ", dealState=" + dealState
				+ ", dealStateDesc=" + dealStateDesc + ", dealType=" + dealType
				+ ", dealTypeDesc=" + dealTypeDesc + ", dealPayType="
				+ dealPayType + ", dealPayTypeDesc=" + dealPayTypeDesc
				+ ", dealRateState=" + dealRateState + ", dealRateStateDesc="
				+ dealRateStateDesc + ", createTime=" + createTime
				+ ", dealEndTime=" + dealEndTime + ", lastUpdateTime="
				+ lastUpdateTime + ", payTime=" + payTime + ", payReturnTime="
				+ payReturnTime + ", recvfeeReturnTime=" + recvfeeReturnTime
				+ ", recvfeeTime=" + recvfeeTime + ", sellerConsignmentTime="
				+ sellerConsignmentTime + ", hasInvoice=" + hasInvoice
				+ ", invoiceContent=" + invoiceContent + ", invoiceTitle="
				+ invoiceTitle + ", tenpayCode=" + tenpayCode
				+ ", transportType=" + transportType + ", transportTypeDesc="
				+ transportTypeDesc + ", whoPayShippingfee="
				+ whoPayShippingfee + ", wuliuId=" + wuliuId
				+ ", receiverAddress=" + receiverAddress + ", receiverMobile="
				+ receiverMobile + ", receiverName=" + receiverName
				+ ", receiverPhone=" + receiverPhone + ", receiverPostcode="
				+ receiverPostcode + ", sellerRecvRefund=" + sellerRecvRefund
				+ ", buyerRecvRefund=" + buyerRecvRefund + ", couponFee="
				+ couponFee + ", dealPayFeePoint=" + dealPayFeePoint
				+ ", dealPayFeeTicket=" + dealPayFeeTicket
				+ ", dealPayFeeTotal=" + dealPayFeeTotal + ", freight="
				+ freight + ", shippingfeeCalc=" + shippingfeeCalc
				+ ", totalCash=" + totalCash + ", sellerCrm=" + sellerCrm
				+ ", sellerName=" + sellerName + ", sellerUin=" + sellerUin
				+ ", itemListCount=" + itemList.size() + "]";
	}


	public long getPayId() {
		return PayId;
	}


	public void setPayId(long payId) {
		PayId = payId;
	}

	
}
