package com.qq.qqbuy.thirdparty.openapi;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class OpenApiArgList
  implements Serializable
{
  private static final long serialVersionUID = -2925494370950033706L;
  private Map<String, String> params = new HashMap();

  public void addParam(String paramName, String paramValue) {
    this.params.put(paramName, paramValue);
  }

  public void addParam(String paramName, Number paramValue) {
    addParam(paramName, paramValue.toString());
  }

  public String getParamValue(String paramName) {
    return (String)this.params.get(paramName);
  }

  public Set<String> getParamNames() {
    return this.params.keySet();
  }

  public int size() {
    return this.params.size();
  }

  public String toString()
  {
    Set kv = this.params.entrySet();
    StringBuilder sb = new StringBuilder();
    sb.append("params:[");
    Iterator it = kv.iterator();
    while (it.hasNext()) {
      Map.Entry en = (Map.Entry)it.next();
      sb.append(en.getKey());
      sb.append("=");
      sb.append(en.getValue());
      if (it.hasNext()) {
        sb.append(",");
      }
    }
    sb.append("]");
    return sb.toString();
  }
}