package com.qq.qqbuy.thirdparty.openapi;

import com.qq.qqbuy.common.ConfigHelper;

public class OpenApiConstant {

	static{
    	SPID = ConfigHelper.getProperty("openapi.spid");
    	SECKEY = ConfigHelper.getProperty("openapi.seckey");
    	TOKEN = ConfigHelper.getProperty("openapi.token");
	}
	
	//鉴权参数
	public static final String SPID;
	public static final String SECKEY;
	public static final String TOKEN;
	
	 // DEV测试账号鉴权相关数据(QQ号：1480445585)
//  public static final String TOKEN = "91ce3d580f2796c1914cface867d07ae";
//  public static final String SPID = "29230000ea0396c1914caa70d0212391";
//  public static final String SECKEY = "3cqdeka8ocs4a2jykywaoe89p14vfmco";

    // Gamma环境鉴权参数（QQ号:795019792）
//  public static final String SPID = "29230000e9035e58a249d2c2f25c8981";
//  public static final String SECKEY = "5irdfe6asciev5i8gg79nr8dzu0y8sr3";
//  public static final String TOKEN = "100a632f0f27226a5b4cb8ba135d53dd";
	
    // 正式鉴权参数（QQ号:1506484425）
//    public static final String SPID = "29230000ea033e92ae4c8fcfb796de0f";
//    public static final String SECKEY = "qkluccrl6bblagtezgwhgqopi25o3bez";
//    public static final String TOKEN = "c920cb590f273e92ae4cccd766bcf0cf";
	
    public static final String CHARSET = "UTF-8";
    public static final int HTTP_CONNECT_TIMEOUT = 3000;
    public static final int HTTP_READ_TIMEOUT = 3000;
    public static final String VISITKEY = "26367892141848824l";
    
    
    
    
}
