package com.qq.qqbuy.thirdparty.openapi;

import com.qq.qqbuy.thirdparty.openapi.po.AddOrderDealRequest;
import com.qq.qqbuy.thirdparty.openapi.po.AddOrderDealResponse;
import com.qq.qqbuy.thirdparty.openapi.po.GetComdyStockInfoRequest;
import com.qq.qqbuy.thirdparty.openapi.po.GetComdyStockInfoResponse;
import com.qq.qqbuy.thirdparty.openapi.po.GetDealRetRequest;
import com.qq.qqbuy.thirdparty.openapi.po.GetDealRetResponse;
import com.qq.qqbuy.thirdparty.openapi.po.GetFreightTemplateRequest;
import com.qq.qqbuy.thirdparty.openapi.po.GetFreightTemplateResponse;

public interface OpenApiProxy {

    /*-----------------------------------------------------------
     * 商品接口
     *----------------------------------------------------------*/
//    public GetItemResponse getItem(final GetItemRequest req);
    
//    public GetItemMobileDetailResponse getItemMobileDetail(final GetItemMobileDetailRequest req);

//    public GetItemDetailInfoResponse getItemDetailInfo(final GetItemDetailInfoRequest req);

    /*-----------------------------------------------------------
     * 订单接口
     *----------------------------------------------------------*/
//    public OrderDealResponse orderDeal(final OrderDealRequest req);

//    public BuyerSearchDealListResponse buyerSearchDealList(final BuyerSearchDealListRequest req);

//    public GetDealDetailResponse getDealDetail(final GetDealDetailRequest req);

//    public GetDealRefundInfoListResponse getDealRefundInfoList(final GetDealRefundInfoListRequest req);

//    public GetDealRefundDetailInfoResponse getDealRefundDetailInfo(final GetDealRefundDetailInfoRequest req);

//    public SellerSearchDealListResponse sellerSearchDealList(final SellerSearchDealListRequest req);
    
    /**
     * 
     * @Title: buyerCancelDeal
     *  
     * @Description: 取消订单 
     * @param req
     * @return    设定文件 
     * @return BuyerCancelDealResponse    返回类型 
     * @throws
     */
//    public BuyerCancelDealResponse buyerCancelDeal(final BuyerCancelDealRequest req);
    

    /*-----------------------------------------------------------
     * 店铺接口
     *----------------------------------------------------------*/
//    public GetFreightListResponse getFreightList(final GetFreightListRequest req);

    public GetFreightTemplateResponse getFreightTemplate(final GetFreightTemplateRequest req);
    
    /**
     * 
     * @Title: getShopInfo 
     * @Description: 获取卖家店铺信息 
     * @param req
     * @return    设定文件 
     * @return GetShopInfoResponse    返回类型 
     * @throws
     */
//    public GetShopInfoResponse getShopInfo(GetShopInfoRequest req);

    /*-----------------------------------------------------------
     * 评价接口
     *----------------------------------------------------------*/
//    public GetItemEvalListResponse getItemEvalList(final GetItemEvalListRequest req);

    /*-----------------------------------------------------------
     * 留言接口
     *----------------------------------------------------------*/

    /*-----------------------------------------------------------
     * 类目属性接口
     *----------------------------------------------------------*/

    /*-----------------------------------------------------------
     * 今日特价接口
     *----------------------------------------------------------*/
    public GetComdyStockInfoResponse getComdyStockInfo(final GetComdyStockInfoRequest req);

    public AddOrderDealResponse addOrderDeal(final AddOrderDealRequest req);

//    public AddSubscribeItemResponse addSubscribeItem(final AddSubscribeItemRequest req);

//    public GetReminderResponse getReminder(final GetReminderRequest req);

    public GetDealRetResponse getDealRet(final GetDealRetRequest req);

    /*-----------------------------------------------------------
     * 实物团购接口
     *----------------------------------------------------------*/
//    public GetTuanItemList4MobileResponse getTuanItemList4Mobile(final GetTuanItemList4MobileRequest req);

//    public GetGroupBuyItemState4MobileResponse getGroupBuyItemState4Mobile(final GetGroupBuyItemState4MobileRequest req);

    /*-----------------------------------------------------------
     * 用户接口
     *----------------------------------------------------------*/
//    public GetReceiverAddressListResponse getReceiverAddressList(final GetReceiverAddressListRequest req);

//    public AddReceiverAddressResponse addReceiverAddressAddress(final AddReceiverAddressRequest req);

//    public ModifyReceiverAddressResponse modifyReceiverAddress(final ModifyReceiverAddressRequest req);

//    public DeleteReceiverAddressResponse deleteReceiverAddress(final DeleteReceiverAddressRequest req);

//    public GetUserDetailInfoResponse getUserDetailInfo(final GetUserDetailInfoRequest req);

//    public GetUserInfoResponse getUserInfo(final GetUserInfoRequest req);

    /*-----------------------------------------------------------
     * VB2C接口
     *----------------------------------------------------------*/

//    public GetWirelessDealListResponse getWirelessDealList(final GetWirelessDealListRequest req);

    /*-----------------------------------------------------------
     * 营销活动接口
     *----------------------------------------------------------*/

    /*-----------------------------------------------------------
     * 尺码表接口
     *----------------------------------------------------------*/

    /*-----------------------------------------------------------
     * 拍拍红包接口
     *----------------------------------------------------------*/
    /**
     * 查询红包或优惠券列表接口
     * 
     * @see <a href="http://pop.paipai.com/bin/view/Main/getRedPacketList">拍拍OpenAPI在线文档</a>
     * @see RTX: colinjing,ankerdiao
     */
//    public GetRedPacketListResponse getRedPacketList(GetRedPacketListRequest req);

    /**
     * 提供手机直充下单接口
     * 
     * @see <a href="http://pop.paipai.com/bin/view/Main/wirelessMobileSubmit">拍拍OpenAPI在线文档</a>
     * @see RTX: terrellhu,ankerdiao,rickwang
     */
//    public ChargeMobileResponse chargeMobile(ChargeMobileRequest req);

    /**
     * 网游直充下单接口
     * 
     * @see <a href="http://pop.paipai.com/bin/view/Main/wirelessGameSubmit">拍拍OpenAPI在线文档</a>
     * @see RTX: terrellhu,ankerdiao,rickwang
     */
//    public ChargeGameResponse chargeGame(ChargeGameRequest req);

    /**
     * 取网游区服及产品信息接口
     * 
     * @see <a href="http://pop.paipai.com/bin/view/Main/wirelessGetGameProduct">拍拍OpenAPI在线文档</a>
     * @see RTX: terrellhu,ankerdiao,rickwang
     */
//    public GetGameProductResponse getGameProduct(GetGameProductRequest req);

    /**
     * 跟据产品ID取网游商品信息接口
     * 
     * @see <a href="http://pop.paipai.com/bin/view/Main/wirelessGetGameItem">拍拍OpenAPI在线文档</a>
     * @see RTX: terrellhu,ankerdiao,rickwang
     */
//    public GetGameItemResponse getGameItem(GetGameItemRequest req);
    /**
     * 查询买家直充订单详情接口
     * 
     * @see <a href="http://pop.paipai.com/bin/view/Main/getWirelessDealInfo">拍拍OpenAPI在线文档</a>
     * @see RTX: terrellhu,ankerdiao,rickwang
     */
//    public GetVB2CDealDetailResponse getVB2CDealDetail(GetVB2CDealDetailRequest req);
}
