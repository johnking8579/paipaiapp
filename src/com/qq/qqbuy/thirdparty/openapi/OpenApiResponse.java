package com.qq.qqbuy.thirdparty.openapi;

import java.io.Serializable;

public class OpenApiResponse
  implements Serializable
{
  public CommonFields common = new CommonFields();

  public boolean isSucceed()
  {
    return (this.common.clientErrorCode == 0L) && (this.common.serverErrorCode == 0L);
  }

  public boolean needsAlarm()
  {
    return getErrorCode() != 0L;
  }

  public long getErrorCode()
  {
    if (this.common.clientErrorCode != 0L) {
      return this.common.clientErrorCode;
    }
    return this.common.serverErrorCode;
  }

  public String getErrorMessage()
  {
    if (this.common.clientErrorCode != 0L) {
      return this.common.clientErrorMessage;
    }
    return this.common.serverErrorMessage;
  }

  public String toString()
  {
    return "OpenApiResponse [common=" + this.common + ", isSucceed()=" + isSucceed() + ", needsAlarm()=" + needsAlarm() + "]";
  }

  public static class CommonFields
    implements Serializable
  {
    public long clientErrorCode = 0L;

    public String clientErrorMessage = null;

    public long serverErrorCode = 0L;

    public String serverErrorMessage = null;

    public String toString()
    {
      return "CommonFields [clientErrorCode=" + this.clientErrorCode + ", clientErrorMessage=" + this.clientErrorMessage + ", serverErrorCode=" + this.serverErrorCode + ", serverErrorMessage=" + this.serverErrorMessage + "]";
    }
  }
}