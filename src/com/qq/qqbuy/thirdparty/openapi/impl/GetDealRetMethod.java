package com.qq.qqbuy.thirdparty.openapi.impl;

import org.jdom.Document;
import org.jdom.Element;

import com.qq.qqbuy.thirdparty.openapi.OpenApiMethod;
import com.qq.qqbuy.thirdparty.openapi.po.GetDealRetRequest;
import com.qq.qqbuy.thirdparty.openapi.po.GetDealRetResponse;

public class GetDealRetMethod extends OpenApiMethod<GetDealRetRequest, GetDealRetResponse> {
	
	@Override
	protected void init() {
		super.signRequired = false;
		super.serviceModelName = "oneaday";
		super.serviceActionName = "getDealRet";
	}

	@Override
	protected void handleResponse(final GetDealRetResponse res, long clientErrorCode, final String clientErrorMessage, final Document xmlDoc) {
		res.common.clientErrorCode = clientErrorCode;
		res.common.clientErrorMessage = clientErrorMessage;
		if (clientErrorCode != 0) {
			return;
		}
		xml2Response(res, xmlDoc);
	}
	
	private void xml2Response(final GetDealRetResponse res, final Document xml) {
		try {
			if (xml != null) {
				Element elmRoot = xml.getRootElement();
				res.common.serverErrorCode = Long.parseLong(elmRoot.getChildText("errorCode"));
				res.common.serverErrorMessage = elmRoot.getChildText("errorMessage");
				if (res.common.serverErrorCode != 0) {
					return;
				}
				res.pos 		   = Long.parseLong(elmRoot.getChildText("pos"));
				res.dealRet 	   = Long.parseLong(elmRoot.getChildText("dealRet"));
				res.dealCode 	   = elmRoot.getChildText("dealCode");
			} else {
				setResponseError(res, ErrorInfo.ERRNO_XML_IS_NULL, true);
			}
		} catch (Exception e) {
			e.printStackTrace();
			setResponseError(res, ErrorInfo.ERRNO_PARSE_XML_FAILED, true);
		}
	}

	@Override
	protected GetDealRetResponse createResponse() {
		return new GetDealRetResponse();
	}
}
