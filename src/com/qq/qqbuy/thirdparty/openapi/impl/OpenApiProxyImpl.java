package com.qq.qqbuy.thirdparty.openapi.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Iterator;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import com.qq.qqbuy.common.ConfigHelper;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.thirdparty.openapi.OpenApiProxy;
import com.qq.qqbuy.thirdparty.openapi.po.AddOrderDealRequest;
import com.qq.qqbuy.thirdparty.openapi.po.AddOrderDealResponse;
import com.qq.qqbuy.thirdparty.openapi.po.GetComdyStockInfoRequest;
import com.qq.qqbuy.thirdparty.openapi.po.GetComdyStockInfoResponse;
import com.qq.qqbuy.thirdparty.openapi.po.GetDealRetRequest;
import com.qq.qqbuy.thirdparty.openapi.po.GetDealRetResponse;
import com.qq.qqbuy.thirdparty.openapi.po.GetFreightTemplateRequest;
import com.qq.qqbuy.thirdparty.openapi.po.GetFreightTemplateResponse;

/**
 * 注意OpenApiProxyImpl是非Thread-Safe的，实现上要注意Stateless
 * 
 * @author rickwang
 */
public class OpenApiProxyImpl implements OpenApiProxy {

	@Override
	public GetFreightTemplateResponse getFreightTemplate(GetFreightTemplateRequest req) {

		GetFreightTemplateResponse res = new GetFreightTemplateResponse();
		StringBuilder sbLogInfo = new StringBuilder();

		try {
			System.setProperty("sun.net.http.allowRestrictedHeaders", "true");

			// 构造URL
			String freightTemplateAddr = ConfigHelper.getProperty("freightTemplateAddr");
			/*if (StringUtils.isBlank(freightTemplateAddr)) {
				freightTemplateAddr = "172.25.76.194:80,172.25.76.192:80";
			}
			String serverAddr = Util.getNextString(freightTemplateAddr.split(","), "freightTemplateAddr");*/
			/**
			 * 不再使用写固定ip方式，绑定域名
			 */
			String serverAddr = null;
			if (StringUtils.isBlank(freightTemplateAddr))
				serverAddr = "my.paipai.com";
			else
				serverAddr = freightTemplateAddr;
			String urlStr = "http://" + serverAddr + "/cgi-bin/shippingfee/sfdetail";
			urlStr = urlStr + "?sellerUin=" + req.getArglist().getParamValue("sellerUin");
			urlStr = urlStr + "&innerId=" + req.getArglist().getParamValue("freightId");
			Log.run.debug("invoke " + urlStr);
			
			int timeOut = 3000;

			// 日志信息
			sbLogInfo.append("[thread:").append(Thread.currentThread().getId()).append("] ");
			sbLogInfo.append("http request post to OpenApi(");
			sbLogInfo.append("url: ").append(urlStr);
			
			String strDoc = HttpUtil.get(urlStr, timeOut, timeOut, "GBK", true);

			sbLogInfo.append(" response=").append(strDoc);
			Log.run.debug(sbLogInfo.toString());

			// 去掉Json结果前后的JS调用字符串
			String head = "try{funcGetSfTpt(";
			String tail = ")}catch(e){}";
			int index = strDoc.indexOf(head);
			if (index >= 0) {
				strDoc = strDoc.substring(index + head.length());
			}
			if (strDoc.endsWith(tail)) {
				strDoc = strDoc.substring(0, strDoc.length() - tail.length());
			}
			strDoc = strDoc.replace("\r", "$R");
			strDoc = strDoc.replace("\n", "$N");

			// 解析请求结果
			ObjectMapper jsonMapper = new ObjectMapper();
			JsonNode rootNode = jsonMapper.readValue(strDoc, JsonNode.class);
			res.common.serverErrorCode = rootNode.path("errCode").getIntValue();
			res.common.serverErrorMessage = rootNode.path("msg").getTextValue();
			if (res.common.serverErrorCode != 0) { // 服务端错误
				sbLogInfo.append(" errCode=").append(res.common.serverErrorCode);
				sbLogInfo.append(" errMsg=").append(res.common.serverErrorMessage);
				Log.run.error(sbLogInfo.toString());
				return res;
			}
			JsonNode dataNode = rootNode.path("data");
			res.name = dataNode.path("name").getTextValue();
			res.freightId = dataNode.path("innerId").getIntValue();
			res.desc = dataNode.path("desc").getTextValue();
			JsonNode ruleListNode = dataNode.path("sfRule");
			Iterator<JsonNode> itr = ruleListNode.getElements();
			while (itr.hasNext()) {
				JsonNode ruleNode = itr.next();
				GetFreightTemplateResponse.FreightRule rule = res.new FreightRule();

				rule.dest = ruleNode.path("dest").getTextValue();
				rule.priceNormal = (long) (ruleNode.path("priceNormal").getDoubleValue() * 100);
				rule.priceNormalAdd = (long) (ruleNode.path("priceNormalAdd").getDoubleValue() * 100);
				rule.priceExpress = (long) (ruleNode.path("priceExpress").getDoubleValue() * 100);
				rule.priceExpressAdd = (long) (ruleNode.path("priceExpressAdd").getDoubleValue() * 100);
				rule.priceEms = (long) (ruleNode.path("priceEms").getDoubleValue() * 100);
				rule.priceEmsAdd = (long) (ruleNode.path("priceEmsAdd").getDoubleValue() * 100);

				res.freightRuleList.add(rule);
			}

			sbLogInfo.append(" response=").append(res.toString());
			Log.run.debug(sbLogInfo.toString());

			return res;

		} catch (MalformedURLException e) {
			Log.run.error("invoke getFreightTemplate CGI(" + ") failed with exception", e);
			res.common.clientErrorCode = -1;
			res.common.clientErrorMessage = "MalformedURLException";
			return res;
		} catch (IOException e) {
			Log.run.error("invoke getFreightTemplate CGI(" + ") failed with exception", e);
			res.common.serverErrorCode = -1;
			res.common.serverErrorMessage = "IOException";
			return res;
		} 
	}

	// @Override
	// public GetItemResponse getItem(GetItemRequest req) {
	// GetItemMethod method = new GetItemMethod();
	// GetItemResponse res = method.invoke(req);
	// return res;
	// }

	// public OrderDealResponse orderDeal(final OrderDealRequest req) {
	// OrderDealMethod method = new OrderDealMethod();
	// OrderDealResponse res = method.invoke(req);
	// return res;
	// }

	// public ModifyReceiverAddressResponse modifyReceiverAddress(final
	// ModifyReceiverAddressRequest req) {
	// ModifyReceiverAddressMethod method = new ModifyReceiverAddressMethod();
	// ModifyReceiverAddressResponse res = method.invoke(req);
	// return res;
	// }
	//
	// public AddReceiverAddressResponse addReceiverAddressAddress(final
	// AddReceiverAddressRequest req) {
	// AddReceiverAddressMethod method = new AddReceiverAddressMethod();
	// AddReceiverAddressResponse res = method.invoke(req);
	// return res;
	// }
	//
	// public DeleteReceiverAddressResponse deleteReceiverAddress(final
	// DeleteReceiverAddressRequest req) {
	// DeleteReceiverAddressMethod method = new DeleteReceiverAddressMethod();
	// DeleteReceiverAddressResponse res = method.invoke(req);
	// return res;
	// }

	// public GetItemEvalListResponse getItemEvalList(final
	// GetItemEvalListRequest req) {
	// GetItemEvalListMethod method = new GetItemEvalListMethod();
	// GetItemEvalListResponse res = method.invoke(req);
	// return res;
	// }

	// public GetReceiverAddressListResponse getReceiverAddressList(final
	// GetReceiverAddressListRequest req) {
	// GetReceiverAddressListMethod method = new GetReceiverAddressListMethod();
	// GetReceiverAddressListResponse res = method.invoke(req);
	// return res;
	// }

	// @Override
	// public GetItemDetailInfoResponse
	// getItemDetailInfo(GetItemDetailInfoRequest req) {
	// GetItemDetailInfoMethod method = new GetItemDetailInfoMethod();
	// GetItemDetailInfoResponse res = method.invoke(req);
	// return res;
	// }

	@Override
	// public BuyerSearchDealListResponse
	// buyerSearchDealList(BuyerSearchDealListRequest req) {
	// BuyerSearchDealListMethod method = new BuyerSearchDealListMethod();
	// BuyerSearchDealListResponse res = method.invoke(req);
	// return res;
	// }
	public GetComdyStockInfoResponse getComdyStockInfo(final GetComdyStockInfoRequest req) {
		GetComdyStockInfoMethod method = new GetComdyStockInfoMethod();
		GetComdyStockInfoResponse res = method.invoke(req);
		return res;
	}

	public AddOrderDealResponse addOrderDeal(final AddOrderDealRequest req) {
		AddOrderDealMethod method = new AddOrderDealMethod();
		AddOrderDealResponse res = method.invoke(req);
		return res;
	}

	// public GetDealDetailResponse getDealDetail(final GetDealDetailRequest
	// req) {
	// GetDealDetailMethod method = new GetDealDetailMethod();
	// GetDealDetailResponse res = method.invoke(req);
	// return res;
	// }

	// public GetDealRefundInfoListResponse getDealRefundInfoList(final
	// GetDealRefundInfoListRequest req) {
	// GetDealRefundInfoListMethod method = new GetDealRefundInfoListMethod();
	// GetDealRefundInfoListResponse res = method.invoke(req);
	// return res;
	// }
	//
	// public GetDealRefundDetailInfoResponse getDealRefundDetailInfo(final
	// GetDealRefundDetailInfoRequest req) {
	// GetDealRefundDetailInfoMethod method = new
	// GetDealRefundDetailInfoMethod();
	// GetDealRefundDetailInfoResponse res = method.invoke(req);
	// return res;
	// }
	//
	// public AddSubscribeItemResponse addSubscribeItem(final
	// AddSubscribeItemRequest req) {
	// AddSubscribeItemMethod method = new AddSubscribeItemMethod();
	// AddSubscribeItemResponse res = method.invoke(req);
	// return res;
	// }

	// public GetReminderResponse getReminder(final GetReminderRequest req) {
	// GetReminderMethod method = new GetReminderMethod();
	// GetReminderResponse res = method.invoke(req);
	// return res;
	// }

	// public GetUserDetailInfoResponse getUserDetailInfo(final
	// GetUserDetailInfoRequest req) {
	// GetUserDetailInfoMethod method = new GetUserDetailInfoMethod();
	// GetUserDetailInfoResponse res = method.invoke(req);
	// return res;
	// }

	// public GetUserInfoResponse getUserInfo(final GetUserInfoRequest req) {
	// GetUserInfoMethod method = new GetUserInfoMethod();
	// GetUserInfoResponse res = method.invoke(req);
	// return res;
	// }

	@Override
	public GetDealRetResponse getDealRet(GetDealRetRequest req) {
		GetDealRetMethod method = new GetDealRetMethod();
		GetDealRetResponse res = method.invoke(req);
		return res;
	}

	// public GetFreightListResponse getFreightList(final GetFreightListRequest
	// req) {
	// GetFreightListMethod method = new GetFreightListMethod();
	// GetFreightListResponse res = method.invoke(req);
	// return res;
	// }
	//
	// public GetWirelessDealListResponse
	// getWirelessDealList(GetWirelessDealListRequest req) {
	// GetWirelessDealListMethod method = new GetWirelessDealListMethod();
	// GetWirelessDealListResponse res = method.invoke(req);
	// return res;
	// }

	// @Override
	// public SellerSearchDealListResponse
	// sellerSearchDealList(SellerSearchDealListRequest req) {
	// SellerSearchDealListMethod method = new SellerSearchDealListMethod();
	// SellerSearchDealListResponse res = method.invoke(req);
	// return res;
	// }

	// @Override
	// public GetTuanItemList4MobileResponse
	// getTuanItemList4Mobile(GetTuanItemList4MobileRequest req) {
	// GetTuanItemList4MobileMethod method = new GetTuanItemList4MobileMethod();
	// GetTuanItemList4MobileResponse res = method.invoke(req);
	// return res;
	// }
	//
	// @Override
	// public GetGroupBuyItemState4MobileResponse
	// getGroupBuyItemState4Mobile(GetGroupBuyItemState4MobileRequest req) {
	// GetGroupBuyItemState4MobileMethod method = new
	// GetGroupBuyItemState4MobileMethod();
	// GetGroupBuyItemState4MobileResponse res = method.invoke(req);
	// return res;
	// }
	//
	// @Override
	// public GetItemMobileDetailResponse
	// getItemMobileDetail(GetItemMobileDetailRequest req) {
	// GetItemMobileDetailMethod method = new GetItemMobileDetailMethod();
	// GetItemMobileDetailResponse res = method.invoke(req);
	// return res;
	// }

	// @Override
	// public GetRedPacketListResponse getRedPacketList(GetRedPacketListRequest
	// req) {
	// return new GetRedPacketListMethod().invoke(req);
	// }

	// @Override
	// public ChargeMobileResponse chargeMobile(ChargeMobileRequest req) {
	// return new ChargeMobileMethod().invoke(req);
	// }
	//
	// @Override
	// public ChargeGameResponse chargeGame(ChargeGameRequest req) {
	// return new ChargeGameMethod().invoke(req);
	// }
	//
	// @Override
	// public GetGameProductResponse getGameProduct(GetGameProductRequest req) {
	// return new GetGameProductMethod().invoke(req);
	// }
	//
	// @Override
	// public GetGameItemResponse getGameItem(GetGameItemRequest req) {
	// return new GetGameItemMethod().invoke(req);
	// }
	//
	// @Override
	// public GetVB2CDealDetailResponse
	// getVB2CDealDetail(GetVB2CDealDetailRequest req) {
	// return new GetVB2CDealDetailMethod().invoke(req);
	// }

	// @Override
	// public BuyerCancelDealResponse buyerCancelDeal(BuyerCancelDealRequest
	// req)
	// {
	// BuyerCancelDealMethod method = new BuyerCancelDealMethod();
	// BuyerCancelDealResponse res = method.invoke(req);
	// return res;
	// }

	// @Override
	// public GetShopInfoResponse getShopInfo(GetShopInfoRequest req)
	// {
	// GetShopInfoMethod method = new GetShopInfoMethod();
	// GetShopInfoResponse res = method.invoke(req);
	// return res;
	// }
}
