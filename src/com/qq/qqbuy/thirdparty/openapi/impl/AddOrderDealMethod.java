package com.qq.qqbuy.thirdparty.openapi.impl;

import org.jdom.Document;
import org.jdom.Element;

import com.qq.qqbuy.thirdparty.openapi.OpenApiMethod;
import com.qq.qqbuy.thirdparty.openapi.po.AddOrderDealRequest;
import com.qq.qqbuy.thirdparty.openapi.po.AddOrderDealResponse;

/**
 * 特价抢购下单method
 * @author homerwu
 *
 */

public class AddOrderDealMethod extends OpenApiMethod<AddOrderDealRequest, AddOrderDealResponse> {

	@Override
	protected void init() {
		super.signRequired = true;
		super.serviceModelName = "oneaday";
		super.serviceActionName = "addOrderDeal";
	}

	@Override
	protected void handleResponse(final AddOrderDealResponse res, long clientErrorCode, final String clientErrorMessage, final Document xmlDoc) {
		res.common.clientErrorCode = clientErrorCode;
		res.common.clientErrorMessage = clientErrorMessage;
		if (clientErrorCode != 0) {
			return;
		}
		xml2Response(res, xmlDoc);
	}
	
	private void xml2Response(final AddOrderDealResponse res, final Document xml) {
		try {
			if (xml != null) {
				Element elm = xml.getRootElement();
				res.common.serverErrorCode = Long.parseLong(elm.getChildText("errorCode"));
				res.common.serverErrorMessage = elm.getChildText("errorMessage");
				if (res.common.serverErrorCode != 0) {
					return;
				}
				res.queueId = elm.getChildText("queueId");
				if (res.common.serverErrorCode == 0xBBA0) { // 在拍下不买黑名单中
					res.beginDate = elm.getChildText("beginDate");
					res.endDate   = elm.getChildText("endDate");
				}
			} else {
				setResponseError(res, ErrorInfo.ERRNO_XML_IS_NULL, true);
			}
		} catch (Exception e) {
			setResponseError(res, ErrorInfo.ERRNO_PARSE_XML_FAILED, true);
		}
	}

	@Override
	protected AddOrderDealResponse createResponse() {
		return new AddOrderDealResponse();
	}
}
