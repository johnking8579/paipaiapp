package com.qq.qqbuy.thirdparty.openapi.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

public class Util {
	public static boolean isEmpty(final String s) {
		return (s == null | s.length() == 0);
	}
	
	public static long getChildLong(final Element ele, final String nodeName) {	
		long lvalue=Long.parseLong(ele.getChildText(nodeName));
		return lvalue;
	}
	
	public static long getChildLong(Element elm, String nodeName, long defaultValue) {
		try {
			return Long.parseLong(elm.getChildText(nodeName));
		} catch (Exception e) {
			return defaultValue;
		}
	}
	
	public static int getChildInt(final Element ele, final String nodeName) {
		int ivalue=Integer.parseInt(ele.getChildText(nodeName));
		return ivalue;
	}
	
	public static String makeMd5Sum(byte[] srcContent)
	{
		if (srcContent == null) {
			return null;
		}

		String strDes = null;
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(srcContent);
			strDes = bytes2Hex(md5.digest());
			return strDes;
		}
		catch (NoSuchAlgorithmException e) {
			return null;
		}
	}
	
	public static String xml2String(Document doc) throws IOException {
		Format format = Format.getPrettyFormat();
		format.setEncoding(Constant.CHARSET);
		format.setExpandEmptyElements(true);
		XMLOutputter xmlout = new XMLOutputter(format);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		xmlout.output(doc, bos);
		return bos.toString(Constant.CHARSET);
	}

	private static String bytes2Hex(byte[] byteArray) {
		StringBuffer strBuf = new StringBuffer();
		for (int i = 0; i < byteArray.length; ++i) {
			if ((byteArray[i] >= 0) && (byteArray[i] < 16)) {
				strBuf.append("0");
			}
			strBuf.append(Integer.toHexString(byteArray[i] & 0xFF));
		}
		return strBuf.toString();
	}
	
	
    public static Map<String, Long> switchMap = new HashMap<String, Long>();
    public static String getNextString (String[] strings , String id){
        Long count = 0L;
        if(switchMap.containsKey(id)){
            count = switchMap.get(id);
            count++;
            switchMap.put(id, count);
            return strings[(int) (count%strings.length)];
        }else{
            count++;
            switchMap.put(id, count);
            return strings[0];
        }
    }
}
