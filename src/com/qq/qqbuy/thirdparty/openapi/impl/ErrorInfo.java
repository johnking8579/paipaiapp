package com.qq.qqbuy.thirdparty.openapi.impl;

import java.util.HashMap;
import java.util.Map;

public class ErrorInfo {
	
	public static long ERRNO_XML_IS_NULL			= -1;
	public static long ERRNO_PARSE_XML_FAILED 	= -2;
	public static long ERRNO_XML_DOM_FAILED		= -3;
	public static long ERRNO_HTTP_IO_FAILED		= -4;
	
	private static Map<Long, String> errorMap = null;
	
	private static void initMap() {
		if (errorMap == null) {
			synchronized (ErrorInfo.class) {
				errorMap = new HashMap<Long, String>();
				
				errorMap.put(ERRNO_XML_IS_NULL,			"OpenApi返回的XML文档为空");
				errorMap.put(ERRNO_PARSE_XML_FAILED, 	"解析OpenApi返回的XML文档失败");
				errorMap.put(ERRNO_XML_DOM_FAILED, 		"通过OpenApi返回的内容创建DOM文档失败");
				errorMap.put(ERRNO_HTTP_IO_FAILED,		"OpenApi_HTTP_I/O异常");
			}
		}
	}
	
	public static String getErrorMessage(long errorCode) {
		if (errorMap == null) {
			initMap();
		}
		return errorMap.get(errorCode);
	}
}
