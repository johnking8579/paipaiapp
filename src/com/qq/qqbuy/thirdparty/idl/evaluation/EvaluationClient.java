package com.qq.qqbuy.thirdparty.idl.evaluation;

import java.util.List;
import java.util.Vector;

import com.paipai.component.c2cplatform.AsynWebStubException;
import com.paipai.component.c2cplatform.IAsynWebStub;
import com.paipai.lang.uint32_t;
import com.qq.qqbuy.common.exception.ExternalInterfaceException;
import com.qq.qqbuy.deal.po.MakeEvalListPo;
import com.qq.qqbuy.deal.po.MakeEvalPo;
import com.qq.qqbuy.deal.util.DealConstant;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.evaluation.protocol.CreateEvalFromDealIdReq;
import com.qq.qqbuy.thirdparty.idl.evaluation.protocol.CreateEvalFromDealIdResp;
import com.qq.qqbuy.thirdparty.idl.evaluation.protocol.MakeEvalListReq;
import com.qq.qqbuy.thirdparty.idl.evaluation.protocol.MakeEvalListResp;
import com.qq.qqbuy.thirdparty.idl.evaluation.protocol.UserMakeEval;
import com.qq.qqbuy.thirdparty.openapi.impl.Util;

/**
 * 评价
 */
public class EvaluationClient extends SupportIDLBaseClient {
	
	/**
	 * 提交评价前调用, 作用不明
	 * @param buyer
	 * @param seller
	 * @param dealId
	 * @param mk
	 * @param skey
	 */
	public void createBefore(long buyer, long seller, String dealId, String mk, String skey)	{
		CreateEvalFromDealIdReq req = new CreateEvalFromDealIdReq();
		CreateEvalFromDealIdResp resp = new CreateEvalFromDealIdResp();
		req.setDwBuyerUin(buyer);
		req.setDwSellerUin(seller);
		req.setSDealId(dealId);
		req.setSMachineKey(mk);
		req.setSSource(CALL_IDL_SOURCE);
		
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		stub.setOperator(buyer);
		stub.setUin(buyer);
		stub.setSkey(skey.getBytes());
		stub.setMachineKey(mk.getBytes());
		
		invoke(stub, req, resp);
	}
	
	/**
	 * 提交评价, 
	 * !!MakeEvalListReq.tradeId应该为完整的dealId
	 * !!UserMakeEval.dealId应该为完整的子订单号,不是tradeId
	 * !!UserMakeEval.dwDsr的满分=5
	 * 
	 * result=14:评价参数错误
	 * result=213:无可评价的订单
	 * 
	 * UserMakeEval.vecReason的取值:
	 * 1 发货速度慢
	 * 2 联系不到卖家
	 * 3 临时涨价
	 * 4 卖家缺货
	 * 5 卖家发错货
	 * 6 商品与描述不符
	 * 7 虚拟点卡无效
	 * 8 卖家使用不文明语言
	 * 9 货品质量缺陷
	 * 10 卖家拒绝退货
	 * 11 物流送货慢
	 * 12 物流服务差
	 * 255 其它原因
	 * 
	 * @param uin
	 * @param dealId
	 * @param skey
	 * @param mk
	 * @param evals
	 */
	public void makeEval(long uin, String dealId, String skey, String mk, List<UserMakeEval> evals)	{
		MakeEvalListReq req = new MakeEvalListReq();
		MakeEvalListResp resp = new MakeEvalListResp();
		req.setSSource(CALL_IDL_SOURCE);
		req.setSMachineKey(mk);
		req.setSTradeId(dealId);
		req.setVecUserMakeEvalReqList(new Vector<UserMakeEval>(evals));
		
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		stub.setOperator(uin);
		stub.setUin(uin);
		stub.setSkey(skey.getBytes());
		stub.setMachineKey(mk.getBytes());
		
		try {
			int ret = stub.invoke(req, resp);		
			if(resp.getResult() != 0)	{	//注:IDL服务端与客户端已不兼容, 反序列化会失败. 不要判断ret值
				throw new ExternalInterfaceException(ret, resp.getResult());
			}
		} catch (AsynWebStubException e) {
			throw new ExternalInterfaceException(e.getMessage(), e);
		}
	}
}
