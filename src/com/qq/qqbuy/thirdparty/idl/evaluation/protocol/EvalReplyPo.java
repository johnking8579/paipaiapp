//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.evaluation.idl.EvalRecordPo.java

package com.qq.qqbuy.thirdparty.idl.evaluation.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *
 *
 *@date 2013-07-09 10:14:40
 *
 *@since version:0
*/
public class EvalReplyPo  implements ICanSerializeObject
{
	/**
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 版本 >= 0
	 */
	 private long dwUin;

	/**
	 * 版本 >= 0
	 */
	 private long dwFDealId;

	/**
	 * 版本 >= 0
	 */
	 private long dwProperty;

	/**
	 * 版本 >= 0
	 */
	 private long dwCreateTime;

	/**
	 * 版本 >= 0
	 */
	 private long dwLastUpdateTime;

	/**
	 * 版本 >= 0
	 */
	 private long dwBossUpdateTime;

	/**
	 * 版本 >= 0
	 */
	 private String strDealId = "";

	/**
	 * 版本 >= 0
	 */
	 private String strContent = "";

	/**
	 * 版本 >= 0
	 */
	 private short cUin_u;

	/**
	 * 版本 >= 0
	 */
	 private short cFDealId_u;

	/**
	 * 版本 >= 0
	 */
	 private short cProperty_u;

	/**
	 * 版本 >= 0
	 */
	 private short cCreateTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short cLastUpdateTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short cBossUpdateTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short cDealId_u;

	/**
	 * 版本 >= 0
	 */
	 private short cContent_u;

	/**
	 * 
	 *
	 * 版本 >= 1
	 */
	 private long dwId;

	/**
	 * 
	 *
	 * 版本 >= 1
	 */
	 private short cId_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize() - 4);
		bs.pushUInt(version);
		bs.pushUInt(dwUin);
		bs.pushUInt(dwFDealId);
		bs.pushUInt(dwProperty);
		bs.pushUInt(dwCreateTime);
		bs.pushUInt(dwLastUpdateTime);
		bs.pushUInt(dwBossUpdateTime);
		bs.pushString(strDealId);
		bs.pushString(strContent);
		bs.pushUByte(cUin_u);
		bs.pushUByte(cFDealId_u);
		bs.pushUByte(cProperty_u);
		bs.pushUByte(cCreateTime_u);
		bs.pushUByte(cLastUpdateTime_u);
		bs.pushUByte(cBossUpdateTime_u);
		bs.pushUByte(cDealId_u);
		bs.pushUByte(cContent_u);
		if(  this.version >= 1 ){
				bs.pushUInt(dwId);
		}
		if(  this.version >= 1 ){
				bs.pushUByte(cId_u);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		dwUin = bs.popUInt();
		dwFDealId = bs.popUInt();
		dwProperty = bs.popUInt();
		dwCreateTime = bs.popUInt();
		dwLastUpdateTime = bs.popUInt();
		dwBossUpdateTime = bs.popUInt();
		strDealId = bs.popString();
		strContent = bs.popString();
		cUin_u = bs.popUByte();
		cFDealId_u = bs.popUByte();
		cProperty_u = bs.popUByte();
		cCreateTime_u = bs.popUByte();
		cLastUpdateTime_u = bs.popUByte();
		cBossUpdateTime_u = bs.popUByte();
		cDealId_u = bs.popUByte();
		cContent_u = bs.popUByte();
		if(  this.version >= 1 ){
				dwId = bs.popUInt();
		}
		if(  this.version >= 1 ){
				cId_u = bs.popUByte();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwUin value 类型为:long
	 * 
	 */
	public long getDwUin()
	{
		return dwUin;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwUin(long value)
	{
		this.dwUin = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwFDealId value 类型为:long
	 * 
	 */
	public long getDwFDealId()
	{
		return dwFDealId;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwFDealId(long value)
	{
		this.dwFDealId = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwProperty value 类型为:long
	 * 
	 */
	public long getDwProperty()
	{
		return dwProperty;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwProperty(long value)
	{
		this.dwProperty = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwCreateTime value 类型为:long
	 * 
	 */
	public long getDwCreateTime()
	{
		return dwCreateTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwCreateTime(long value)
	{
		this.dwCreateTime = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwLastUpdateTime value 类型为:long
	 * 
	 */
	public long getDwLastUpdateTime()
	{
		return dwLastUpdateTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwLastUpdateTime(long value)
	{
		this.dwLastUpdateTime = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwBossUpdateTime value 类型为:long
	 * 
	 */
	public long getDwBossUpdateTime()
	{
		return dwBossUpdateTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwBossUpdateTime(long value)
	{
		this.dwBossUpdateTime = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strDealId value 类型为:String
	 * 
	 */
	public String getStrDealId()
	{
		return strDealId;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrDealId(String value)
	{
		this.strDealId = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strContent value 类型为:String
	 * 
	 */
	public String getStrContent()
	{
		return strContent;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrContent(String value)
	{
		this.strContent = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cUin_u value 类型为:short
	 * 
	 */
	public short getCUin_u()
	{
		return cUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCUin_u(short value)
	{
		this.cUin_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cFDealId_u value 类型为:short
	 * 
	 */
	public short getCFDealId_u()
	{
		return cFDealId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCFDealId_u(short value)
	{
		this.cFDealId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cProperty_u value 类型为:short
	 * 
	 */
	public short getCProperty_u()
	{
		return cProperty_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCProperty_u(short value)
	{
		this.cProperty_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cCreateTime_u value 类型为:short
	 * 
	 */
	public short getCCreateTime_u()
	{
		return cCreateTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCCreateTime_u(short value)
	{
		this.cCreateTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cLastUpdateTime_u value 类型为:short
	 * 
	 */
	public short getCLastUpdateTime_u()
	{
		return cLastUpdateTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCLastUpdateTime_u(short value)
	{
		this.cLastUpdateTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cBossUpdateTime_u value 类型为:short
	 * 
	 */
	public short getCBossUpdateTime_u()
	{
		return cBossUpdateTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCBossUpdateTime_u(short value)
	{
		this.cBossUpdateTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cDealId_u value 类型为:short
	 * 
	 */
	public short getCDealId_u()
	{
		return cDealId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCDealId_u(short value)
	{
		this.cDealId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cContent_u value 类型为:short
	 * 
	 */
	public short getCContent_u()
	{
		return cContent_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCContent_u(short value)
	{
		this.cContent_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 1
	 * @return dwId value 类型为:long
	 * 
	 */
	public long getDwId()
	{
		return dwId;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwId(long value)
	{
		this.dwId = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 1
	 * @return cId_u value 类型为:short
	 * 
	 */
	public short getCId_u()
	{
		return cId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:short
	 * 
	 */
	public void setCId_u(short value)
	{
		this.cId_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(EvalReplyPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段dwUin的长度 size_of(uint32_t)
				length += 4;  //计算字段dwFDealId的长度 size_of(uint32_t)
				length += 4;  //计算字段dwProperty的长度 size_of(uint32_t)
				length += 4;  //计算字段dwCreateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段dwLastUpdateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段dwBossUpdateTime的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(strDealId);  //计算字段strDealId的长度 size_of(String)
				length += ByteStream.getObjectSize(strContent);  //计算字段strContent的长度 size_of(String)
				length += 1;  //计算字段cUin_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cFDealId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cProperty_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cCreateTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cLastUpdateTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cBossUpdateTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cDealId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cContent_u的长度 size_of(uint8_t)
				if(  this.version >= 1 ){
						length += 4;  //计算字段dwId的长度 size_of(uint32_t)
				}
				if(  this.version >= 1 ){
						length += 1;  //计算字段cId_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本1所包含的字段*******
 *	long version;
 *	long dwUin;
 *	long dwFDealId;
 *	long dwProperty;
 *	long dwCreateTime;
 *	long dwLastUpdateTime;
 *	long dwBossUpdateTime;
 *	String strDealId;
 *	String strContent;
 *	short cUin_u;
 *	short cFDealId_u;
 *	short cProperty_u;
 *	short cCreateTime_u;
 *	short cLastUpdateTime_u;
 *	short cBossUpdateTime_u;
 *	short cDealId_u;
 *	short cContent_u;
 *	long dwId;
 *	short cId_u;
 *****以上是版本1所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
