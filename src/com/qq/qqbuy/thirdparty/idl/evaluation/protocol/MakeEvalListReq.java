 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.evaluation.idl.EvaluationService.java

package com.qq.qqbuy.thirdparty.idl.evaluation.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Vector;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 *
 *
 *@date 2013-07-09 10:14:40
 *
 *@since version:1
*/
public class  MakeEvalListReq implements IServiceObject
{
	/**
	 * 版本 >= 0
	 */
	 private Vector<UserMakeEval> vecUserMakeEvalReqList = new Vector<UserMakeEval>();

	/**
	 * 版本 >= 0
	 */
	 private String sTradeId = "";

	/**
	 * 版本 >= 0
	 */
	 private String sMachineKey = "";

	/**
	 * 版本 >= 0
	 */
	 private String sSource = "";

	/**
	 * 版本 >= 0
	 */
	 private String sInReserve = "";

	/**
	 * 版本 >= 0
	 */
	 private Vector<EvalRecordPo> vecEvalRecordPoList = new Vector<EvalRecordPo>();

	/**
	 * 版本 >= 0
	 */
	 private String sOutReserve = "";


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(vecUserMakeEvalReqList);
		bs.pushString(sTradeId);
		bs.pushString(sMachineKey);
		bs.pushString(sSource);
		bs.pushString(sInReserve);
//		bs.pushObject(vecEvalRecordPoList);
//		bs.pushString(sOutReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		vecUserMakeEvalReqList = (Vector<UserMakeEval>)bs.popVector(UserMakeEval.class);
		sTradeId = bs.popString();
		sMachineKey = bs.popString();
		sSource = bs.popString();
		sInReserve = bs.popString();
//		vecEvalRecordPoList = (Vector<EvalRecordPo>)bs.popVector(EvalRecordPo.class);
//		sOutReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x23101009L;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return vecUserMakeEvalReqList value 类型为:Vector<UserMakeEval>
	 * 
	 */
	public Vector<UserMakeEval> getVecUserMakeEvalReqList()
	{
		return vecUserMakeEvalReqList;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<UserMakeEval>
	 * 
	 */
	public void setVecUserMakeEvalReqList(Vector<UserMakeEval> value)
	{
		if (value != null) {
				this.vecUserMakeEvalReqList = value;
		}else{
				this.vecUserMakeEvalReqList = new Vector<UserMakeEval>();
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sTradeId value 类型为:String
	 * 
	 */
	public String getSTradeId()
	{
		return sTradeId;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSTradeId(String value)
	{
		this.sTradeId = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sMachineKey value 类型为:String
	 * 
	 */
	public String getSMachineKey()
	{
		return sMachineKey;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSMachineKey(String value)
	{
		this.sMachineKey = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sSource value 类型为:String
	 * 
	 */
	public String getSSource()
	{
		return sSource;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSSource(String value)
	{
		this.sSource = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sInReserve value 类型为:String
	 * 
	 */
	public String getSInReserve()
	{
		return sInReserve;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSInReserve(String value)
	{
		this.sInReserve = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return vecEvalRecordPoList value 类型为:Vector<EvalRecordPo>
	 * 
	 */
	public Vector<EvalRecordPo> getVecEvalRecordPoList()
	{
		return vecEvalRecordPoList;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<EvalRecordPo>
	 * 
	 */
	public void setVecEvalRecordPoList(Vector<EvalRecordPo> value)
	{
		if (value != null) {
				this.vecEvalRecordPoList = value;
		}else{
				this.vecEvalRecordPoList = new Vector<EvalRecordPo>();
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sOutReserve value 类型为:String
	 * 
	 */
	public String getSOutReserve()
	{
		return sOutReserve;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSOutReserve(String value)
	{
		this.sOutReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(MakeEvalListReq)
				length += ByteStream.getObjectSize(vecUserMakeEvalReqList);  //计算字段vecUserMakeEvalReqList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(sTradeId);  //计算字段sTradeId的长度 size_of(String)
				length += ByteStream.getObjectSize(sMachineKey);  //计算字段sMachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(sSource);  //计算字段sSource的长度 size_of(String)
				length += ByteStream.getObjectSize(sInReserve);  //计算字段sInReserve的长度 size_of(String)
//				length += ByteStream.getObjectSize(vecEvalRecordPoList);  //计算字段vecEvalRecordPoList的长度 size_of(Vector)
//				length += ByteStream.getObjectSize(sOutReserve);  //计算字段sOutReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
