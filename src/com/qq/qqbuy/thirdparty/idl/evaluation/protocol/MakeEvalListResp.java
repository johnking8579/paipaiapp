 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.evaluation.idl.EvaluationService.java

package com.qq.qqbuy.thirdparty.idl.evaluation.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Vector;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 *
 *
 *@date 2013-07-09 07:41:05
 *
 *@since version:1
*/
public class  MakeEvalListResp implements IServiceObject
{
	public long result;
	/**
	 * 版本 >= 0
	 */
	 private long dwVersion;

	/**
	 * 版本 >= 0
	 */
	 private long dwTotal;

	/**
	 * 版本 >= 0
	 */
	 private Vector<EvalRecordPo> vecEvalRecordPoList = new Vector<EvalRecordPo>();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String outReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushUInt(dwVersion);
		bs.pushUInt(dwTotal);
		bs.pushObject(vecEvalRecordPoList);
		bs.pushString(outReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		dwVersion = bs.popUInt();
		dwTotal = bs.popUInt();
		vecEvalRecordPoList = (Vector<EvalRecordPo>)bs.popVector(EvalRecordPo.class);
		outReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x23108009L;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwVersion value 类型为:long
	 * 
	 */
	public long getDwVersion()
	{
		return dwVersion;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwVersion(long value)
	{
		this.dwVersion = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwTotal value 类型为:long
	 * 
	 */
	public long getDwTotal()
	{
		return dwTotal;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwTotal(long value)
	{
		this.dwTotal = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return vecEvalRecordPoList value 类型为:Vector<EvalRecordPo>
	 * 
	 */
	public Vector<EvalRecordPo> getVecEvalRecordPoList()
	{
		return vecEvalRecordPoList;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<EvalRecordPo>
	 * 
	 */
	public void setVecEvalRecordPoList(Vector<EvalRecordPo> value)
	{
		if (value != null) {
				this.vecEvalRecordPoList = value;
		}else{
				this.vecEvalRecordPoList = new Vector<EvalRecordPo>();
		}
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserved value 类型为:String
	 * 
	 */
	public String getOutReserved()
	{
		return outReserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserved(String value)
	{
		this.outReserved = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(MakeEvalListResp)
				length += 4;  //计算字段dwVersion的长度 size_of(uint32_t)
				length += 4;  //计算字段dwTotal的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(vecEvalRecordPoList);  //计算字段vecEvalRecordPoList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(outReserved);  //计算字段outReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
