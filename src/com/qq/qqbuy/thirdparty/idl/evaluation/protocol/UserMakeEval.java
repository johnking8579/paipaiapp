//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.evaluation.idl.MakeEvalListReq.java

package com.qq.qqbuy.thirdparty.idl.evaluation.protocol;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.uint32_t;
import java.util.Vector;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * 
 * 
 *@date 2013-07-09 10:14:40
 * 
 *@since version:0
 */
public class UserMakeEval implements ICanSerializeObject {
	/**
	 * 版本 >= 0
	 */
	private long version = 1;

	/**
	 * 版本 >= 0
	 */
	private String strDealId = "";

	/**
	 * 版本 >= 0
	 */
	private String strContent = "";

	/**
	 * 版本 >= 0
	 */
	private long dwBuyerUin;

	/**
	 * 版本 >= 0
	 */
	private long dwSellerUin;

	/**
	 * 版本 >= 0
	 */
	private short cRole;

	/**
	 * 版本 >= 0
	 */
	private short cHideBuyerInfo;

	/**
	 * 版本 >= 0
	 */
	private short cPadd2;

	/**
	 * 版本 >= 0
	 */
	private short cEvalLevel;

	/**
	 * 版本 >= 0
	 */
	private short cDealId_u;

	/**
	 * 版本 >= 0
	 */
	private short cBuyerUin_u;

	/**
	 * 版本 >= 0
	 */
	private short cSellerUin_u;

	/**
	 * 版本 >= 0
	 */
	private short cRole_u;

	/**
	 * 版本 >= 0
	 */
	private short cHideBuyerInfo_u;

	/**
	 * 版本 >= 0
	 */
	private short cPadd2_u;

	/**
	 * 版本 >= 0
	 */
	private short cEvalLevel_u;

	/**
	 * 版本 >= 0
	 */
	private short cContent_u;

	/**
	 * 版本 >= 0
	 */
	private Vector<uint32_t> vecReason = new Vector<uint32_t>();

	/**
	 * 
	 * 
	 * 版本 >= 1
	 */
	private long dwDsr1;

	/**
	 * 
	 * 
	 * 版本 >= 1
	 */
	private long dwDsr2;

	/**
	 * 
	 * 
	 * 版本 >= 1
	 */
	private long dwDsr3;

	/**
	 * 
	 * 
	 * 版本 >= 1
	 */
	private short cDsr1_u;

	/**
	 * 
	 * 
	 * 版本 >= 1
	 */
	private short cDsr2_u;

	/**
	 * 
	 * 
	 * 版本 >= 1
	 */
	private short cDsr3_u;

	/**
	 * 
	 * 
	 * 版本 >= 1
	 */
	private long dwSellerLevel;

	/**
	 * 
	 * 
	 * 版本 >= 1
	 */
	private short cSellerLevel_u;

	public int serialize(ByteStream bs) throws Exception {
		bs.pushUInt(getSize() - 4);
		bs.pushUInt(version);
		bs.pushString(strDealId);
		bs.pushString(strContent);
		bs.pushUInt(dwBuyerUin);
		bs.pushUInt(dwSellerUin);
		bs.pushUByte(cRole);
		bs.pushUByte(cHideBuyerInfo);
		bs.pushUByte(cPadd2);
		bs.pushUByte(cEvalLevel);
		bs.pushUByte(cDealId_u);
		bs.pushUByte(cBuyerUin_u);
		bs.pushUByte(cSellerUin_u);
		bs.pushUByte(cRole_u);
		bs.pushUByte(cHideBuyerInfo_u);
		bs.pushUByte(cPadd2_u);
		bs.pushUByte(cEvalLevel_u);
		bs.pushUByte(cContent_u);
		bs.pushObject(vecReason);
		if (this.version >= 1) {
			bs.pushUInt(dwDsr1);
			bs.pushUInt(dwDsr2);
			bs.pushUInt(dwDsr3);
			bs.pushUByte(cDsr1_u);
			bs.pushUByte(cDsr2_u);
			bs.pushUByte(cDsr3_u);
			bs.pushUInt(dwSellerLevel);
			bs.pushUByte(cSellerLevel_u);
		}
		return bs.getWrittenLength();
	}

	public int unSerialize(ByteStream bs) throws Exception {
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
			return 0;
		version = bs.popUInt();
		strDealId = bs.popString();
		strContent = bs.popString();
		dwBuyerUin = bs.popUInt();
		dwSellerUin = bs.popUInt();
		cRole = bs.popUByte();
		cHideBuyerInfo = bs.popUByte();
		cPadd2 = bs.popUByte();
		cEvalLevel = bs.popUByte();
		cDealId_u = bs.popUByte();
		cBuyerUin_u = bs.popUByte();
		cSellerUin_u = bs.popUByte();
		cRole_u = bs.popUByte();
		cHideBuyerInfo_u = bs.popUByte();
		cPadd2_u = bs.popUByte();
		cEvalLevel_u = bs.popUByte();
		cContent_u = bs.popUByte();
		vecReason = (Vector<uint32_t>) bs.popVector(uint32_t.class);
		if (this.version >= 1) {
			dwDsr1 = bs.popUInt();
			dwDsr2 = bs.popUInt();
			dwDsr3 = bs.popUInt();
			cDsr1_u = bs.popUByte();
			cDsr2_u = bs.popUByte();
			cDsr3_u = bs.popUByte();
			dwSellerLevel = bs.popUInt();
			cSellerLevel_u = bs.popUByte();
		}

		/********************** 为了支持多个版本的客户端 ************************/
		int needPopBytes = (int) size - (bs.getReadLength() - startPosPop);
		for (int i = 0; i < needPopBytes; i++)
			bs.popByte();
		/********************** 为了支持多个版本的客户端 ************************/

		return bs.getReadLength();
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setVersion(long value) {
		this.version = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return strDealId value 类型为:String
	 * 
	 */
	public String getStrDealId() {
		return strDealId;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:String
	 * 
	 */
	public void setStrDealId(String value) {
		this.strDealId = value;
		this.cDealId_u = 1;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return strContent value 类型为:String
	 * 
	 */
	public String getStrContent() {
		return strContent;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:String
	 * 
	 */
	public void setStrContent(String value) {
		this.strContent = value;
		this.cContent_u = 1;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return dwBuyerUin value 类型为:long
	 * 
	 */
	public long getDwBuyerUin() {
		return dwBuyerUin;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setDwBuyerUin(long value) {
		this.dwBuyerUin = value;
		this.cBuyerUin_u = 1;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return dwSellerUin value 类型为:long
	 * 
	 */
	public long getDwSellerUin() {
		return dwSellerUin;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setDwSellerUin(long value) {
		this.dwSellerUin = value;
		this.cSellerUin_u = 1;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return cRole value 类型为:short
	 * 
	 */
	public short getCRole() {
		return cRole;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setCRole(short value) {
		this.cRole = value;
		this.cRole_u = 1;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return cHideBuyerInfo value 类型为:short
	 * 
	 */
	public short getCHideBuyerInfo() {
		return cHideBuyerInfo;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setCHideBuyerInfo(short value) {
		this.cHideBuyerInfo = value;
		this.cHideBuyerInfo_u = 1;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return cPadd2 value 类型为:short
	 * 
	 */
	public short getCPadd2() {
		return cPadd2;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setCPadd2(short value) {
		this.cPadd2 = value;
		this.cPadd2_u = 1;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return cEvalLevel value 类型为:short
	 * 
	 */
	public short getCEvalLevel() {
		return cEvalLevel;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setCEvalLevel(short value) {
		this.cEvalLevel = value;
		this.cEvalLevel_u = 1;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return cDealId_u value 类型为:short
	 * 
	 */
	public short getCDealId_u() {
		return cDealId_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setCDealId_u(short value) {
		this.cDealId_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return cBuyerUin_u value 类型为:short
	 * 
	 */
	public short getCBuyerUin_u() {
		return cBuyerUin_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setCBuyerUin_u(short value) {
		this.cBuyerUin_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return cSellerUin_u value 类型为:short
	 * 
	 */
	public short getCSellerUin_u() {
		return cSellerUin_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setCSellerUin_u(short value) {
		this.cSellerUin_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return cRole_u value 类型为:short
	 * 
	 */
	public short getCRole_u() {
		return cRole_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setCRole_u(short value) {
		this.cRole_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return cHideBuyerInfo_u value 类型为:short
	 * 
	 */
	public short getCHideBuyerInfo_u() {
		return cHideBuyerInfo_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setCHideBuyerInfo_u(short value) {
		this.cHideBuyerInfo_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return cPadd2_u value 类型为:short
	 * 
	 */
	public short getCPadd2_u() {
		return cPadd2_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setCPadd2_u(short value) {
		this.cPadd2_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return cEvalLevel_u value 类型为:short
	 * 
	 */
	public short getCEvalLevel_u() {
		return cEvalLevel_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setCEvalLevel_u(short value) {
		this.cEvalLevel_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return cContent_u value 类型为:short
	 * 
	 */
	public short getCContent_u() {
		return cContent_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setCContent_u(short value) {
		this.cContent_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return vecReason value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getVecReason() {
		return vecReason;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:Vector<uint32_t>
	 * 
	 */
	public void setVecReason(Vector<uint32_t> value) {
		if (value != null) {
			this.vecReason = value;
		} else {
			this.vecReason = new Vector<uint32_t>();
		}
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @return dwDsr1 value 类型为:long
	 * 
	 */
	public long getDwDsr1() {
		return dwDsr1;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setDwDsr1(long value) {
		this.dwDsr1 = value;
		this.cDsr1_u = 1;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @return dwDsr2 value 类型为:long
	 * 
	 */
	public long getDwDsr2() {
		return dwDsr2;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setDwDsr2(long value) {
		this.dwDsr2 = value;
		this.cDsr2_u = 1;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @return dwDsr3 value 类型为:long
	 * 
	 */
	public long getDwDsr3() {
		return dwDsr3;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setDwDsr3(long value) {
		this.dwDsr3 = value;
		this.cDsr3_u = 1;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @return cDsr1_u value 类型为:short
	 * 
	 */
	public short getCDsr1_u() {
		return cDsr1_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setCDsr1_u(short value) {
		this.cDsr1_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @return cDsr2_u value 类型为:short
	 * 
	 */
	public short getCDsr2_u() {
		return cDsr2_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setCDsr2_u(short value) {
		this.cDsr2_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @return cDsr3_u value 类型为:short
	 * 
	 */
	public short getCDsr3_u() {
		return cDsr3_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setCDsr3_u(short value) {
		this.cDsr3_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @return dwSellerLevel value 类型为:long
	 * 
	 */
	public long getDwSellerLevel() {
		return dwSellerLevel;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setDwSellerLevel(long value) {
		this.dwSellerLevel = value;
		this.cSellerLevel_u = 1;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @return cSellerLevel_u value 类型为:short
	 * 
	 */
	public short getCSellerLevel_u() {
		return cSellerLevel_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setCSellerLevel_u(short value) {
		this.cSellerLevel_u = value;
	}

	/**
	 * 计算类长度 用于告诉解包者，该类只放了这么长的数据
	 * 
	 */
	protected int getClassSize() {
		int length = getSize() - 4;
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
		return length;
	}

	/**
	 * 计算类长度 这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 * 
	 */
	public int getSize() {
		int length = 4;
		try {
			length = 4; // size_of(UserMakeEval)
			length += 4; // 计算字段version的长度 size_of(uint32_t)
			length += ByteStream.getObjectSize(strDealId); // 计算字段strDealId的长度
															// size_of(String)
			length += 4;
			if(strContent != null)
				length += strContent.getBytes("GBK").length;
			//length += ByteStream.getObjectSize(strContent); // 计算字段strContent的长度
															// size_of(String)
			length += 4; // 计算字段dwBuyerUin的长度 size_of(uint32_t)
			length += 4; // 计算字段dwSellerUin的长度 size_of(uint32_t)
			length += 1; // 计算字段cRole的长度 size_of(uint8_t)
			length += 1; // 计算字段cHideBuyerInfo的长度 size_of(uint8_t)
			length += 1; // 计算字段cPadd2的长度 size_of(uint8_t)
			length += 1; // 计算字段cEvalLevel的长度 size_of(uint8_t)
			length += 1; // 计算字段cDealId_u的长度 size_of(uint8_t)
			length += 1; // 计算字段cBuyerUin_u的长度 size_of(uint8_t)
			length += 1; // 计算字段cSellerUin_u的长度 size_of(uint8_t)
			length += 1; // 计算字段cRole_u的长度 size_of(uint8_t)
			length += 1; // 计算字段cHideBuyerInfo_u的长度 size_of(uint8_t)
			length += 1; // 计算字段cPadd2_u的长度 size_of(uint8_t)
			length += 1; // 计算字段cEvalLevel_u的长度 size_of(uint8_t)
			length += 1; // 计算字段cContent_u的长度 size_of(uint8_t)
			length += ByteStream.getObjectSize(vecReason); // 计算字段vecReason的长度
															// size_of(Vector)
			if (this.version >= 1) {
				length += 4; // 计算字段dwDsr1的长度 size_of(uint32_t)
			}
			if (this.version >= 1) {
				length += 4; // 计算字段dwDsr2的长度 size_of(uint32_t)
			}
			if (this.version >= 1) {
				length += 4; // 计算字段dwDsr3的长度 size_of(uint32_t)
			}
			if (this.version >= 1) {
				length += 1; // 计算字段cDsr1_u的长度 size_of(uint8_t)
			}
			if (this.version >= 1) {
				length += 1; // 计算字段cDsr2_u的长度 size_of(uint8_t)
			}
			if (this.version >= 1) {
				length += 1; // 计算字段cDsr3_u的长度 size_of(uint8_t)
			}
			if (this.version >= 1) {
				length += 4; // 计算字段dwSellerLevel的长度 size_of(uint32_t)
			}
			if (this.version >= 1) {
				length += 1; // 计算字段cSellerLevel_u的长度 size_of(uint8_t)
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return length;
	}

	/**
	 ********************以下信息是每个版本的字段********************
	 * 
	 *****以下是版本1所包含的字段******* long version; String strDealId; String strContent;
	 * long dwBuyerUin; long dwSellerUin; short cRole; short cHideBuyerInfo;
	 * short cPadd2; short cEvalLevel; short cDealId_u; short cBuyerUin_u; short
	 * cSellerUin_u; short cRole_u; short cHideBuyerInfo_u; short cPadd2_u;
	 * short cEvalLevel_u; short cContent_u; Vector<uint32_t> vecReason; long
	 * dwDsr1; long dwDsr2; long dwDsr3; short cDsr1_u; short cDsr2_u; short
	 * cDsr3_u; long dwSellerLevel; short cSellerLevel_u; 以上是版本1所包含的字段*******
	 */

	/**
	 *下面是生成toString()方法 此方法用于调试时打开* 如果要打开此方法，请加入commons-lang-2.4.jar 并导入 import
	 * org.apache.commons.lang.builder.ToStringBuilder; import
	 * org.apache.commons.lang.builder.ToStringStyle;
	 * 
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
