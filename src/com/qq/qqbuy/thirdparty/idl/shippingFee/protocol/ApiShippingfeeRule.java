//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.o2o.thirdparty.idl.shippingfee.ShippingfeeApiAo.java

package com.qq.qqbuy.thirdparty.idl.shippingFee.protocol;


import java.util.Vector;

import com.paipai.lang.uint32_t;
import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *运费模板规则
 *
 *@date 2012-12-28 07:19:31
 *
 *@since version:0
*/
public class ApiShippingfeeRule  implements ICanSerializeObject
{
	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long version = 1;

	/**
	 * 目的地 C2C为到省份 B2C为到市
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint32_t> vecDest = new Vector<uint32_t>();

	/**
	 * 类型 按件或按重量
	 *
	 * 版本 >= 0
	 */
	 private short type;

	/**
	 * 属性
	 *
	 * 版本 >= 0
	 */
	 private long property;

	/**
	 * 运送达到时间
	 *
	 * 版本 >= 0
	 */
	 private long arriveDays;

	/**
	 * 货到付款手续费
	 *
	 * 版本 >= 0
	 */
	 private long onArriFee;

	/**
	 * 平邮初始重量或件数
	 *
	 * 版本 >= 0
	 */
	 private long normInitHvyorCount;

	/**
	 * 平邮初始价格
	 *
	 * 版本 >= 0
	 */
	 private long normInitShipFee;

	/**
	 * 平邮超过的重量或件数
	 *
	 * 版本 >= 0
	 */
	 private long normOverHvyorCount;

	/**
	 * 平邮超过重量或件数时需要的额外增加的费用
	 *
	 * 版本 >= 0
	 */
	 private long normOverShipFee;

	/**
	 * 快递初始重量或件数
	 *
	 * 版本 >= 0
	 */
	 private long expInitHvyorCount;

	/**
	 * 快递初始价格
	 *
	 * 版本 >= 0
	 */
	 private long expInitShipFee;

	/**
	 * 快递超过的重量或件数
	 *
	 * 版本 >= 0
	 */
	 private long expOverHvyorCount;

	/**
	 * 快递超过重量或件数时需要的额外增加的费用
	 *
	 * 版本 >= 0
	 */
	 private long expOverShipFee;

	/**
	 * EMS初始重量或件数
	 *
	 * 版本 >= 0
	 */
	 private long emsInitHvyorCount;

	/**
	 * EMS初始价格
	 *
	 * 版本 >= 0
	 */
	 private long emsInitShipFee;

	/**
	 * EMS超过的重量或件数
	 *
	 * 版本 >= 0
	 */
	 private long emsOverHvyorCount;

	/**
	 * EMS超过重量或件数时需要的额外增加的费用
	 *
	 * 版本 >= 0
	 */
	 private long emsOverShipFee;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 版本 >= 0
	 */
	 private short vecDest_u;

	/**
	 * 版本 >= 0
	 */
	 private short type_u;

	/**
	 * 版本 >= 0
	 */
	 private short property_u;

	/**
	 * 版本 >= 0
	 */
	 private short arriveDays_u;

	/**
	 * 版本 >= 0
	 */
	 private short onArriFee_u;

	/**
	 * 版本 >= 0
	 */
	 private short normInitHvyorCount_u;

	/**
	 * 版本 >= 0
	 */
	 private short normInitShipFee_u;

	/**
	 * 版本 >= 0
	 */
	 private short normOverHvyorCount_u;

	/**
	 * 版本 >= 0
	 */
	 private short normOverShipFee_u;

	/**
	 * 版本 >= 0
	 */
	 private short expInitHvyorCount_u;

	/**
	 * 版本 >= 0
	 */
	 private short expInitShipFee_u;

	/**
	 * 版本 >= 0
	 */
	 private short expOverHvyorCount_u;

	/**
	 * 版本 >= 0
	 */
	 private short expOverShipFee_u;

	/**
	 * 版本 >= 0
	 */
	 private short emsInitHvyorCount_u;

	/**
	 * 版本 >= 0
	 */
	 private short emsInitShipFee_u;

	/**
	 * 版本 >= 0
	 */
	 private short emsOverHvyorCount_u;

	/**
	 * 版本 >= 0
	 */
	 private short emsOverShipFee_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushObject(vecDest);
		bs.pushUByte(type);
		bs.pushUInt(property);
		bs.pushUInt(arriveDays);
		bs.pushUInt(onArriFee);
		bs.pushUInt(normInitHvyorCount);
		bs.pushUInt(normInitShipFee);
		bs.pushUInt(normOverHvyorCount);
		bs.pushUInt(normOverShipFee);
		bs.pushUInt(expInitHvyorCount);
		bs.pushUInt(expInitShipFee);
		bs.pushUInt(expOverHvyorCount);
		bs.pushUInt(expOverShipFee);
		bs.pushUInt(emsInitHvyorCount);
		bs.pushUInt(emsInitShipFee);
		bs.pushUInt(emsOverHvyorCount);
		bs.pushUInt(emsOverShipFee);
		bs.pushUByte(version_u);
		bs.pushUByte(vecDest_u);
		bs.pushUByte(type_u);
		bs.pushUByte(property_u);
		bs.pushUByte(arriveDays_u);
		bs.pushUByte(onArriFee_u);
		bs.pushUByte(normInitHvyorCount_u);
		bs.pushUByte(normInitShipFee_u);
		bs.pushUByte(normOverHvyorCount_u);
		bs.pushUByte(normOverShipFee_u);
		bs.pushUByte(expInitHvyorCount_u);
		bs.pushUByte(expInitShipFee_u);
		bs.pushUByte(expOverHvyorCount_u);
		bs.pushUByte(expOverShipFee_u);
		bs.pushUByte(emsInitHvyorCount_u);
		bs.pushUByte(emsInitShipFee_u);
		bs.pushUByte(emsOverHvyorCount_u);
		bs.pushUByte(emsOverShipFee_u);
		return bs.getWrittenLength();
	}
	
	@SuppressWarnings("unchecked")
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		vecDest = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		type = bs.popUByte();
		property = bs.popUInt();
		arriveDays = bs.popUInt();
		onArriFee = bs.popUInt();
		normInitHvyorCount = bs.popUInt();
		normInitShipFee = bs.popUInt();
		normOverHvyorCount = bs.popUInt();
		normOverShipFee = bs.popUInt();
		expInitHvyorCount = bs.popUInt();
		expInitShipFee = bs.popUInt();
		expOverHvyorCount = bs.popUInt();
		expOverShipFee = bs.popUInt();
		emsInitHvyorCount = bs.popUInt();
		emsInitShipFee = bs.popUInt();
		emsOverHvyorCount = bs.popUInt();
		emsOverShipFee = bs.popUInt();
		version_u = bs.popUByte();
		vecDest_u = bs.popUByte();
		type_u = bs.popUByte();
		property_u = bs.popUByte();
		arriveDays_u = bs.popUByte();
		onArriFee_u = bs.popUByte();
		normInitHvyorCount_u = bs.popUByte();
		normInitShipFee_u = bs.popUByte();
		normOverHvyorCount_u = bs.popUByte();
		normOverShipFee_u = bs.popUByte();
		expInitHvyorCount_u = bs.popUByte();
		expInitShipFee_u = bs.popUByte();
		expOverHvyorCount_u = bs.popUByte();
		expOverShipFee_u = bs.popUByte();
		emsInitHvyorCount_u = bs.popUByte();
		emsInitShipFee_u = bs.popUByte();
		emsOverHvyorCount_u = bs.popUByte();
		emsOverShipFee_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取目的地 C2C为到省份 B2C为到市
	 * 
	 * 此字段的版本 >= 0
	 * @return vecDest value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getVecDest()
	{
		return vecDest;
	}


	/**
	 * 设置目的地 C2C为到省份 B2C为到市
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setVecDest(Vector<uint32_t> value)
	{
		if (value != null) {
				this.vecDest = value;
				this.vecDest_u = 1;
		}
	}


	/**
	 * 获取类型 按件或按重量
	 * 
	 * 此字段的版本 >= 0
	 * @return type value 类型为:short
	 * 
	 */
	public short getType()
	{
		return type;
	}


	/**
	 * 设置类型 按件或按重量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setType(short value)
	{
		this.type = value;
		this.type_u = 1;
	}


	/**
	 * 获取属性
	 * 
	 * 此字段的版本 >= 0
	 * @return property value 类型为:long
	 * 
	 */
	public long getProperty()
	{
		return property;
	}


	/**
	 * 设置属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setProperty(long value)
	{
		this.property = value;
		this.property_u = 1;
	}


	/**
	 * 获取运送达到时间
	 * 
	 * 此字段的版本 >= 0
	 * @return arriveDays value 类型为:long
	 * 
	 */
	public long getArriveDays()
	{
		return arriveDays;
	}


	/**
	 * 设置运送达到时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setArriveDays(long value)
	{
		this.arriveDays = value;
		this.arriveDays_u = 1;
	}


	/**
	 * 获取货到付款手续费
	 * 
	 * 此字段的版本 >= 0
	 * @return onArriFee value 类型为:long
	 * 
	 */
	public long getOnArriFee()
	{
		return onArriFee;
	}


	/**
	 * 设置货到付款手续费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOnArriFee(long value)
	{
		this.onArriFee = value;
		this.onArriFee_u = 1;
	}


	/**
	 * 获取平邮初始重量或件数
	 * 
	 * 此字段的版本 >= 0
	 * @return normInitHvyorCount value 类型为:long
	 * 
	 */
	public long getNormInitHvyorCount()
	{
		return normInitHvyorCount;
	}


	/**
	 * 设置平邮初始重量或件数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNormInitHvyorCount(long value)
	{
		this.normInitHvyorCount = value;
		this.normInitHvyorCount_u = 1;
	}


	/**
	 * 获取平邮初始价格
	 * 
	 * 此字段的版本 >= 0
	 * @return normInitShipFee value 类型为:long
	 * 
	 */
	public long getNormInitShipFee()
	{
		return normInitShipFee;
	}


	/**
	 * 设置平邮初始价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNormInitShipFee(long value)
	{
		this.normInitShipFee = value;
		this.normInitShipFee_u = 1;
	}


	/**
	 * 获取平邮超过的重量或件数
	 * 
	 * 此字段的版本 >= 0
	 * @return normOverHvyorCount value 类型为:long
	 * 
	 */
	public long getNormOverHvyorCount()
	{
		return normOverHvyorCount;
	}


	/**
	 * 设置平邮超过的重量或件数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNormOverHvyorCount(long value)
	{
		this.normOverHvyorCount = value;
		this.normOverHvyorCount_u = 1;
	}


	/**
	 * 获取平邮超过重量或件数时需要的额外增加的费用
	 * 
	 * 此字段的版本 >= 0
	 * @return normOverShipFee value 类型为:long
	 * 
	 */
	public long getNormOverShipFee()
	{
		return normOverShipFee;
	}


	/**
	 * 设置平邮超过重量或件数时需要的额外增加的费用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNormOverShipFee(long value)
	{
		this.normOverShipFee = value;
		this.normOverShipFee_u = 1;
	}


	/**
	 * 获取快递初始重量或件数
	 * 
	 * 此字段的版本 >= 0
	 * @return expInitHvyorCount value 类型为:long
	 * 
	 */
	public long getExpInitHvyorCount()
	{
		return expInitHvyorCount;
	}


	/**
	 * 设置快递初始重量或件数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setExpInitHvyorCount(long value)
	{
		this.expInitHvyorCount = value;
		this.expInitHvyorCount_u = 1;
	}


	/**
	 * 获取快递初始价格
	 * 
	 * 此字段的版本 >= 0
	 * @return expInitShipFee value 类型为:long
	 * 
	 */
	public long getExpInitShipFee()
	{
		return expInitShipFee;
	}


	/**
	 * 设置快递初始价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setExpInitShipFee(long value)
	{
		this.expInitShipFee = value;
		this.expInitShipFee_u = 1;
	}


	/**
	 * 获取快递超过的重量或件数
	 * 
	 * 此字段的版本 >= 0
	 * @return expOverHvyorCount value 类型为:long
	 * 
	 */
	public long getExpOverHvyorCount()
	{
		return expOverHvyorCount;
	}


	/**
	 * 设置快递超过的重量或件数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setExpOverHvyorCount(long value)
	{
		this.expOverHvyorCount = value;
		this.expOverHvyorCount_u = 1;
	}


	/**
	 * 获取快递超过重量或件数时需要的额外增加的费用
	 * 
	 * 此字段的版本 >= 0
	 * @return expOverShipFee value 类型为:long
	 * 
	 */
	public long getExpOverShipFee()
	{
		return expOverShipFee;
	}


	/**
	 * 设置快递超过重量或件数时需要的额外增加的费用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setExpOverShipFee(long value)
	{
		this.expOverShipFee = value;
		this.expOverShipFee_u = 1;
	}


	/**
	 * 获取EMS初始重量或件数
	 * 
	 * 此字段的版本 >= 0
	 * @return emsInitHvyorCount value 类型为:long
	 * 
	 */
	public long getEmsInitHvyorCount()
	{
		return emsInitHvyorCount;
	}


	/**
	 * 设置EMS初始重量或件数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEmsInitHvyorCount(long value)
	{
		this.emsInitHvyorCount = value;
		this.emsInitHvyorCount_u = 1;
	}


	/**
	 * 获取EMS初始价格
	 * 
	 * 此字段的版本 >= 0
	 * @return emsInitShipFee value 类型为:long
	 * 
	 */
	public long getEmsInitShipFee()
	{
		return emsInitShipFee;
	}


	/**
	 * 设置EMS初始价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEmsInitShipFee(long value)
	{
		this.emsInitShipFee = value;
		this.emsInitShipFee_u = 1;
	}


	/**
	 * 获取EMS超过的重量或件数
	 * 
	 * 此字段的版本 >= 0
	 * @return emsOverHvyorCount value 类型为:long
	 * 
	 */
	public long getEmsOverHvyorCount()
	{
		return emsOverHvyorCount;
	}


	/**
	 * 设置EMS超过的重量或件数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEmsOverHvyorCount(long value)
	{
		this.emsOverHvyorCount = value;
		this.emsOverHvyorCount_u = 1;
	}


	/**
	 * 获取EMS超过重量或件数时需要的额外增加的费用
	 * 
	 * 此字段的版本 >= 0
	 * @return emsOverShipFee value 类型为:long
	 * 
	 */
	public long getEmsOverShipFee()
	{
		return emsOverShipFee;
	}


	/**
	 * 设置EMS超过重量或件数时需要的额外增加的费用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEmsOverShipFee(long value)
	{
		this.emsOverShipFee = value;
		this.emsOverShipFee_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return vecDest_u value 类型为:short
	 * 
	 */
	public short getVecDest_u()
	{
		return vecDest_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVecDest_u(short value)
	{
		this.vecDest_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return type_u value 类型为:short
	 * 
	 */
	public short getType_u()
	{
		return type_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setType_u(short value)
	{
		this.type_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return property_u value 类型为:short
	 * 
	 */
	public short getProperty_u()
	{
		return property_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProperty_u(short value)
	{
		this.property_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return arriveDays_u value 类型为:short
	 * 
	 */
	public short getArriveDays_u()
	{
		return arriveDays_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setArriveDays_u(short value)
	{
		this.arriveDays_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return onArriFee_u value 类型为:short
	 * 
	 */
	public short getOnArriFee_u()
	{
		return onArriFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOnArriFee_u(short value)
	{
		this.onArriFee_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return normInitHvyorCount_u value 类型为:short
	 * 
	 */
	public short getNormInitHvyorCount_u()
	{
		return normInitHvyorCount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNormInitHvyorCount_u(short value)
	{
		this.normInitHvyorCount_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return normInitShipFee_u value 类型为:short
	 * 
	 */
	public short getNormInitShipFee_u()
	{
		return normInitShipFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNormInitShipFee_u(short value)
	{
		this.normInitShipFee_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return normOverHvyorCount_u value 类型为:short
	 * 
	 */
	public short getNormOverHvyorCount_u()
	{
		return normOverHvyorCount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNormOverHvyorCount_u(short value)
	{
		this.normOverHvyorCount_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return normOverShipFee_u value 类型为:short
	 * 
	 */
	public short getNormOverShipFee_u()
	{
		return normOverShipFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNormOverShipFee_u(short value)
	{
		this.normOverShipFee_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return expInitHvyorCount_u value 类型为:short
	 * 
	 */
	public short getExpInitHvyorCount_u()
	{
		return expInitHvyorCount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setExpInitHvyorCount_u(short value)
	{
		this.expInitHvyorCount_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return expInitShipFee_u value 类型为:short
	 * 
	 */
	public short getExpInitShipFee_u()
	{
		return expInitShipFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setExpInitShipFee_u(short value)
	{
		this.expInitShipFee_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return expOverHvyorCount_u value 类型为:short
	 * 
	 */
	public short getExpOverHvyorCount_u()
	{
		return expOverHvyorCount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setExpOverHvyorCount_u(short value)
	{
		this.expOverHvyorCount_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return expOverShipFee_u value 类型为:short
	 * 
	 */
	public short getExpOverShipFee_u()
	{
		return expOverShipFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setExpOverShipFee_u(short value)
	{
		this.expOverShipFee_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return emsInitHvyorCount_u value 类型为:short
	 * 
	 */
	public short getEmsInitHvyorCount_u()
	{
		return emsInitHvyorCount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEmsInitHvyorCount_u(short value)
	{
		this.emsInitHvyorCount_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return emsInitShipFee_u value 类型为:short
	 * 
	 */
	public short getEmsInitShipFee_u()
	{
		return emsInitShipFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEmsInitShipFee_u(short value)
	{
		this.emsInitShipFee_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return emsOverHvyorCount_u value 类型为:short
	 * 
	 */
	public short getEmsOverHvyorCount_u()
	{
		return emsOverHvyorCount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEmsOverHvyorCount_u(short value)
	{
		this.emsOverHvyorCount_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return emsOverShipFee_u value 类型为:short
	 * 
	 */
	public short getEmsOverShipFee_u()
	{
		return emsOverShipFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEmsOverShipFee_u(short value)
	{
		this.emsOverShipFee_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ApiShippingfeeRule)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(vecDest);  //计算字段vecDest的长度 size_of(Vector)
				length += 1;  //计算字段type的长度 size_of(uint8_t)
				length += 4;  //计算字段property的长度 size_of(uint32_t)
				length += 4;  //计算字段arriveDays的长度 size_of(uint32_t)
				length += 4;  //计算字段onArriFee的长度 size_of(uint32_t)
				length += 4;  //计算字段normInitHvyorCount的长度 size_of(uint32_t)
				length += 4;  //计算字段normInitShipFee的长度 size_of(uint32_t)
				length += 4;  //计算字段normOverHvyorCount的长度 size_of(uint32_t)
				length += 4;  //计算字段normOverShipFee的长度 size_of(uint32_t)
				length += 4;  //计算字段expInitHvyorCount的长度 size_of(uint32_t)
				length += 4;  //计算字段expInitShipFee的长度 size_of(uint32_t)
				length += 4;  //计算字段expOverHvyorCount的长度 size_of(uint32_t)
				length += 4;  //计算字段expOverShipFee的长度 size_of(uint32_t)
				length += 4;  //计算字段emsInitHvyorCount的长度 size_of(uint32_t)
				length += 4;  //计算字段emsInitShipFee的长度 size_of(uint32_t)
				length += 4;  //计算字段emsOverHvyorCount的长度 size_of(uint32_t)
				length += 4;  //计算字段emsOverShipFee的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段vecDest_u的长度 size_of(uint8_t)
				length += 1;  //计算字段type_u的长度 size_of(uint8_t)
				length += 1;  //计算字段property_u的长度 size_of(uint8_t)
				length += 1;  //计算字段arriveDays_u的长度 size_of(uint8_t)
				length += 1;  //计算字段onArriFee_u的长度 size_of(uint8_t)
				length += 1;  //计算字段normInitHvyorCount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段normInitShipFee_u的长度 size_of(uint8_t)
				length += 1;  //计算字段normOverHvyorCount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段normOverShipFee_u的长度 size_of(uint8_t)
				length += 1;  //计算字段expInitHvyorCount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段expInitShipFee_u的长度 size_of(uint8_t)
				length += 1;  //计算字段expOverHvyorCount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段expOverShipFee_u的长度 size_of(uint8_t)
				length += 1;  //计算字段emsInitHvyorCount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段emsInitShipFee_u的长度 size_of(uint8_t)
				length += 1;  //计算字段emsOverHvyorCount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段emsOverShipFee_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
