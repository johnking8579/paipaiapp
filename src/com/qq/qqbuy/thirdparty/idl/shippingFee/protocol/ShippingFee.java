package com.qq.qqbuy.thirdparty.idl.shippingFee.protocol;

import java.util.Vector;

import com.paipai.lang.uint32_t;
import com.paipai.lang.uint64_t;
import com.paipai.lang.uint8_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;

@HeadApiProtocol(cPlusNamespace = "com.paipai.deal")
public class ShippingFee {

    @Member(desc = "规则信息", cPlusNamespace = "c2cent::bo::shippingfee", isNeedUFlag = false, isSetClassSize = false)
    public class Rule {
        uint8_t cType; // /< 类型
        Vector<uint32_t> vecDest;
        uint32_t dwProperty; // /< 属性
        uint32_t dwPriceNormal; // /< 平邮运费
        uint32_t dwPriceExpress; // /< 快递运费
        uint32_t dwPriceEms; // /< ems运费

        uint32_t dwPriceNormalAdd; // /< 平邮运费增幅
        uint32_t dwPriceExpressAdd; // /< 快递运费增幅
        uint32_t dwPriceEmsAdd; // /< ems运费增幅
    }

    @Member(desc = "运费模板", cPlusNamespace = "c2cent::bo::shippingfee", isNeedUFlag = false, isSetClassSize = false)
    public class TemplateSimpleInfo {
        uint32_t dwId;
        uint32_t dwUin;
        uint8_t cInnerId;
        uint32_t dwProperty;
        String sName;
        String sDesc;
        uint32_t dwCreateTime;
        uint32_t dwLastModifyTime;
    }

    @Member(desc = "运费模板", cPlusNamespace = "c2cent::bo::shippingfee", isNeedUFlag = false, isSetClassSize = false)
    public class TemplateDetailInfo {
        @Field(desc = "运费信息")
        TemplateSimpleInfo oSimple;

        @Field(desc = "规则信息")
        Vector<Rule> rule_vector;
    }

    @ApiProtocol(cmdid = "0x22101803L", desc = "sayHello")
    class GetTemplateDetail {
        class Req {
            @Field(desc = "属主uin ")
            uint32_t uin;

            @Field(desc = "运费模板id")
            uint8_t cInnerId;
        }

        class Resp {
            @Field(desc = "错误信息")
            TemplateDetailInfo detail;
        }
    }
    
    
    @ApiProtocol(cmdid = "0x26301804L", desc = "取消订单")
    class CloseDeal {
        @ApiProtocol(cmdid = "0x26301804L", desc = "取消订单请求")
        class Req {
            @Field(desc = "调用者==>请设置为源文件名")
            String Source;
            @Field(desc = "订单id")
            String DealId;
            @Field(desc = "场景id")
            uint32_t SceneId;
            @Field(desc = "关闭原因")
            uint32_t CloseReason;
            @Field(desc = "子单列表")
            Vector<uint64_t> oTradeIdList;
            @Field(desc = "用户MachineKey")
            String MachineKey;
            @Field(desc = "保留输入字段")
            String ReserveIn;
        }

        @ApiProtocol(cmdid = "0x26308804L", desc = "取消订单返回")
        class Resp {
            @Field(desc = "错误信息")
            String ErrMsg;
        }
    }

}
