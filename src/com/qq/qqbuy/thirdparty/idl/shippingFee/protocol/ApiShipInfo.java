//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.o2o.thirdparty.idl.shippingfee.ShippingfeeApiAo.java

package com.qq.qqbuy.thirdparty.idl.shippingFee.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *运费模板相关信息
 *
 *@date 2012-12-28 07:19:32
 *
 *@since version:0
*/
public class ApiShipInfo  implements ICanSerializeObject
{
	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long version = 0;

	/**
	 * 运费模板id
	 *
	 * 版本 >= 0
	 */
	 private long shipTptId;

	/**
	 * 商品所在地id
	 *
	 * 版本 >= 0
	 */
	 private long cmdyRegId;

	/**
	 * 商品数量
	 *
	 * 版本 >= 0
	 */
	 private long cmdyNum;

	/**
	 * 商品重量
	 *
	 * 版本 >= 0
	 */
	 private long cmdyWeight;

	/**
	 * 平邮费用
	 *
	 * 版本 >= 0
	 */
	 private long normalFee;

	/**
	 * 快递费用
	 *
	 * 版本 >= 0
	 */
	 private long expressFee;

	/**
	 * EMS费用
	 *
	 * 版本 >= 0
	 */
	 private long emsFee;

	/**
	 * 货到付款运费模板类型
	 *
	 * 版本 >= 0
	 */
	 private long codTptType;

	/**
	 * 运费类型属性
	 *
	 * 版本 >= 0
	 */
	 private long property;

	/**
	 * 输出结果是否正常标识
	 *
	 * 版本 >= 0
	 */
	 private long retFlag;

	/**
	 * 卖家uin
	 *
	 * 版本 >= 0
	 */
	 private long sellerUin;

	/**
	 * 保留string
	 *
	 * 版本 >= 0
	 */
	 private String reserve = new String();

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 版本 >= 0
	 */
	 private short shipTptId_u;

	/**
	 * 版本 >= 0
	 */
	 private short cmdyRegId_u;

	/**
	 * 版本 >= 0
	 */
	 private short cmdyNum_u;

	/**
	 * 版本 >= 0
	 */
	 private short cmdyWeight_u;

	/**
	 * 版本 >= 0
	 */
	 private short normalFee_u;

	/**
	 * 版本 >= 0
	 */
	 private short expressFee_u;

	/**
	 * 版本 >= 0
	 */
	 private short emsFee_u;

	/**
	 * 版本 >= 0
	 */
	 private short codTptType_u;

	/**
	 * 版本 >= 0
	 */
	 private short property_u;

	/**
	 * 版本 >= 0
	 */
	 private short retFlag_u;

	/**
	 * 版本 >= 0
	 */
	 private short sellerUin_u;

	/**
	 * 版本 >= 0
	 */
	 private short reserve_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUInt(shipTptId);
		bs.pushUInt(cmdyRegId);
		bs.pushUInt(cmdyNum);
		bs.pushUInt(cmdyWeight);
		bs.pushUInt(normalFee);
		bs.pushUInt(expressFee);
		bs.pushUInt(emsFee);
		bs.pushUInt(codTptType);
		bs.pushUInt(property);
		bs.pushUInt(retFlag);
		bs.pushUInt(sellerUin);
		bs.pushString(reserve);
		bs.pushUByte(version_u);
		bs.pushUByte(shipTptId_u);
		bs.pushUByte(cmdyRegId_u);
		bs.pushUByte(cmdyNum_u);
		bs.pushUByte(cmdyWeight_u);
		bs.pushUByte(normalFee_u);
		bs.pushUByte(expressFee_u);
		bs.pushUByte(emsFee_u);
		bs.pushUByte(codTptType_u);
		bs.pushUByte(property_u);
		bs.pushUByte(retFlag_u);
		bs.pushUByte(sellerUin_u);
		bs.pushUByte(reserve_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		shipTptId = bs.popUInt();
		cmdyRegId = bs.popUInt();
		cmdyNum = bs.popUInt();
		cmdyWeight = bs.popUInt();
		normalFee = bs.popUInt();
		expressFee = bs.popUInt();
		emsFee = bs.popUInt();
		codTptType = bs.popUInt();
		property = bs.popUInt();
		retFlag = bs.popUInt();
		sellerUin = bs.popUInt();
		reserve = bs.popString();
		version_u = bs.popUByte();
		shipTptId_u = bs.popUByte();
		cmdyRegId_u = bs.popUByte();
		cmdyNum_u = bs.popUByte();
		cmdyWeight_u = bs.popUByte();
		normalFee_u = bs.popUByte();
		expressFee_u = bs.popUByte();
		emsFee_u = bs.popUByte();
		codTptType_u = bs.popUByte();
		property_u = bs.popUByte();
		retFlag_u = bs.popUByte();
		sellerUin_u = bs.popUByte();
		reserve_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取运费模板id
	 * 
	 * 此字段的版本 >= 0
	 * @return shipTptId value 类型为:long
	 * 
	 */
	public long getShipTptId()
	{
		return shipTptId;
	}


	/**
	 * 设置运费模板id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setShipTptId(long value)
	{
		this.shipTptId = value;
		this.shipTptId_u = 1;
	}


	/**
	 * 获取商品所在地id
	 * 
	 * 此字段的版本 >= 0
	 * @return cmdyRegId value 类型为:long
	 * 
	 */
	public long getCmdyRegId()
	{
		return cmdyRegId;
	}


	/**
	 * 设置商品所在地id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCmdyRegId(long value)
	{
		this.cmdyRegId = value;
		this.cmdyRegId_u = 1;
	}


	/**
	 * 获取商品数量
	 * 
	 * 此字段的版本 >= 0
	 * @return cmdyNum value 类型为:long
	 * 
	 */
	public long getCmdyNum()
	{
		return cmdyNum;
	}


	/**
	 * 设置商品数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCmdyNum(long value)
	{
		this.cmdyNum = value;
		this.cmdyNum_u = 1;
	}


	/**
	 * 获取商品重量
	 * 
	 * 此字段的版本 >= 0
	 * @return cmdyWeight value 类型为:long
	 * 
	 */
	public long getCmdyWeight()
	{
		return cmdyWeight;
	}


	/**
	 * 设置商品重量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCmdyWeight(long value)
	{
		this.cmdyWeight = value;
		this.cmdyWeight_u = 1;
	}


	/**
	 * 获取平邮费用
	 * 
	 * 此字段的版本 >= 0
	 * @return normalFee value 类型为:long
	 * 
	 */
	public long getNormalFee()
	{
		return normalFee;
	}


	/**
	 * 设置平邮费用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNormalFee(long value)
	{
		this.normalFee = value;
		this.normalFee_u = 1;
	}


	/**
	 * 获取快递费用
	 * 
	 * 此字段的版本 >= 0
	 * @return expressFee value 类型为:long
	 * 
	 */
	public long getExpressFee()
	{
		return expressFee;
	}


	/**
	 * 设置快递费用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setExpressFee(long value)
	{
		this.expressFee = value;
		this.expressFee_u = 1;
	}


	/**
	 * 获取EMS费用
	 * 
	 * 此字段的版本 >= 0
	 * @return emsFee value 类型为:long
	 * 
	 */
	public long getEmsFee()
	{
		return emsFee;
	}


	/**
	 * 设置EMS费用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEmsFee(long value)
	{
		this.emsFee = value;
		this.emsFee_u = 1;
	}


	/**
	 * 获取货到付款运费模板类型
	 * 
	 * 此字段的版本 >= 0
	 * @return codTptType value 类型为:long
	 * 
	 */
	public long getCodTptType()
	{
		return codTptType;
	}


	/**
	 * 设置货到付款运费模板类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCodTptType(long value)
	{
		this.codTptType = value;
		this.codTptType_u = 1;
	}


	/**
	 * 获取运费类型属性
	 * 
	 * 此字段的版本 >= 0
	 * @return property value 类型为:long
	 * 
	 */
	public long getProperty()
	{
		return property;
	}


	/**
	 * 设置运费类型属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setProperty(long value)
	{
		this.property = value;
		this.property_u = 1;
	}


	/**
	 * 获取输出结果是否正常标识
	 * 
	 * 此字段的版本 >= 0
	 * @return retFlag value 类型为:long
	 * 
	 */
	public long getRetFlag()
	{
		return retFlag;
	}


	/**
	 * 设置输出结果是否正常标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRetFlag(long value)
	{
		this.retFlag = value;
		this.retFlag_u = 1;
	}


	/**
	 * 获取卖家uin
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return sellerUin;
	}


	/**
	 * 设置卖家uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.sellerUin = value;
		this.sellerUin_u = 1;
	}


	/**
	 * 获取保留string
	 * 
	 * 此字段的版本 >= 0
	 * @return reserve value 类型为:String
	 * 
	 */
	public String getReserve()
	{
		return reserve;
	}


	/**
	 * 设置保留string
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserve(String value)
	{
		this.reserve = value;
		this.reserve_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return shipTptId_u value 类型为:short
	 * 
	 */
	public short getShipTptId_u()
	{
		return shipTptId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShipTptId_u(short value)
	{
		this.shipTptId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cmdyRegId_u value 类型为:short
	 * 
	 */
	public short getCmdyRegId_u()
	{
		return cmdyRegId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCmdyRegId_u(short value)
	{
		this.cmdyRegId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cmdyNum_u value 类型为:short
	 * 
	 */
	public short getCmdyNum_u()
	{
		return cmdyNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCmdyNum_u(short value)
	{
		this.cmdyNum_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cmdyWeight_u value 类型为:short
	 * 
	 */
	public short getCmdyWeight_u()
	{
		return cmdyWeight_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCmdyWeight_u(short value)
	{
		this.cmdyWeight_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return normalFee_u value 类型为:short
	 * 
	 */
	public short getNormalFee_u()
	{
		return normalFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNormalFee_u(short value)
	{
		this.normalFee_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return expressFee_u value 类型为:short
	 * 
	 */
	public short getExpressFee_u()
	{
		return expressFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setExpressFee_u(short value)
	{
		this.expressFee_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return emsFee_u value 类型为:short
	 * 
	 */
	public short getEmsFee_u()
	{
		return emsFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEmsFee_u(short value)
	{
		this.emsFee_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return codTptType_u value 类型为:short
	 * 
	 */
	public short getCodTptType_u()
	{
		return codTptType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCodTptType_u(short value)
	{
		this.codTptType_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return property_u value 类型为:short
	 * 
	 */
	public short getProperty_u()
	{
		return property_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProperty_u(short value)
	{
		this.property_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return retFlag_u value 类型为:short
	 * 
	 */
	public short getRetFlag_u()
	{
		return retFlag_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRetFlag_u(short value)
	{
		this.retFlag_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerUin_u value 类型为:short
	 * 
	 */
	public short getSellerUin_u()
	{
		return sellerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerUin_u(short value)
	{
		this.sellerUin_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return reserve_u value 类型为:short
	 * 
	 */
	public short getReserve_u()
	{
		return reserve_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserve_u(short value)
	{
		this.reserve_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ApiShipInfo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段shipTptId的长度 size_of(uint32_t)
				length += 4;  //计算字段cmdyRegId的长度 size_of(uint32_t)
				length += 4;  //计算字段cmdyNum的长度 size_of(uint32_t)
				length += 4;  //计算字段cmdyWeight的长度 size_of(uint32_t)
				length += 4;  //计算字段normalFee的长度 size_of(uint32_t)
				length += 4;  //计算字段expressFee的长度 size_of(uint32_t)
				length += 4;  //计算字段emsFee的长度 size_of(uint32_t)
				length += 4;  //计算字段codTptType的长度 size_of(uint32_t)
				length += 4;  //计算字段property的长度 size_of(uint32_t)
				length += 4;  //计算字段retFlag的长度 size_of(uint32_t)
				length += 4;  //计算字段sellerUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(reserve);  //计算字段reserve的长度 size_of(String)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段shipTptId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cmdyRegId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cmdyNum_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cmdyWeight_u的长度 size_of(uint8_t)
				length += 1;  //计算字段normalFee_u的长度 size_of(uint8_t)
				length += 1;  //计算字段expressFee_u的长度 size_of(uint8_t)
				length += 1;  //计算字段emsFee_u的长度 size_of(uint8_t)
				length += 1;  //计算字段codTptType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段property_u的长度 size_of(uint8_t)
				length += 1;  //计算字段retFlag_u的长度 size_of(uint8_t)
				length += 1;  //计算字段sellerUin_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserve_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
