//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.o2o.thirdparty.idl.shippingfee.ShippingfeeApiAo.java

package com.qq.qqbuy.thirdparty.idl.shippingFee.protocol;


import java.util.Vector;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *运费模板相关信息list
 *
 *@date 2012-12-28 07:19:32
 *
 *@since version:0
*/
public class ApiShipInfoList  implements ICanSerializeObject
{
	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long version = 0;

	/**
	 * 运费模板相关信息vector
	 *
	 * 版本 >= 0
	 */
	 private Vector<ApiShipInfo> vecShipInfo = new Vector<ApiShipInfo>();

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 版本 >= 0
	 */
	 private short vecShipInfo_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushObject(vecShipInfo);
		bs.pushUByte(version_u);
		bs.pushUByte(vecShipInfo_u);
		return bs.getWrittenLength();
	}
	
	@SuppressWarnings("unchecked")
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		vecShipInfo = (Vector<ApiShipInfo>)bs.popVector(ApiShipInfo.class);
		version_u = bs.popUByte();
		vecShipInfo_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取运费模板相关信息vector
	 * 
	 * 此字段的版本 >= 0
	 * @return vecShipInfo value 类型为:Vector<ApiShipInfo>
	 * 
	 */
	public Vector<ApiShipInfo> getVecShipInfo()
	{
		return vecShipInfo;
	}


	/**
	 * 设置运费模板相关信息vector
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<ApiShipInfo>
	 * 
	 */
	public void setVecShipInfo(Vector<ApiShipInfo> value)
	{
		if (value != null) {
				this.vecShipInfo = value;
				this.vecShipInfo_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return vecShipInfo_u value 类型为:short
	 * 
	 */
	public short getVecShipInfo_u()
	{
		return vecShipInfo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVecShipInfo_u(short value)
	{
		this.vecShipInfo_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ApiShipInfoList)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(vecShipInfo);  //计算字段vecShipInfo的长度 size_of(Vector)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段vecShipInfo_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
