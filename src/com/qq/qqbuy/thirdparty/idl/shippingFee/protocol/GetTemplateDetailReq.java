 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.shippingFee.protocol;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

/**
 *
 *
 *@date 2012-12-26 02:59::35
 *
 *@since version:1
*/
public class  GetTemplateDetailReq implements IServiceObject
{
	/**
	 * 属主uin 
	 *
	 * 版本 >= 0
	 */
	 private long uin;

	/**
	 * 运费模板id
	 *
	 * 版本 >= 0
	 */
	 private short cInnerId;


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(uin);
		bs.pushUByte(cInnerId);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		uin = bs.popUInt();
		cInnerId = bs.popUByte();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x22101803L;
	}


	/**
	 * 获取属主uin 
	 * 
	 * 此字段的版本 >= 0
	 * @return uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return uin;
	}


	/**
	 * 设置属主uin 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.uin = value;
	}


	/**
	 * 获取运费模板id
	 * 
	 * 此字段的版本 >= 0
	 * @return cInnerId value 类型为:short
	 * 
	 */
	public short getCInnerId()
	{
		return cInnerId;
	}


	/**
	 * 设置运费模板id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCInnerId(short value)
	{
		this.cInnerId = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetTemplateDetailReq)
				length += 4;  //计算字段uin的长度 size_of(uint32_t)
				length += 1;  //计算字段cInnerId的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
