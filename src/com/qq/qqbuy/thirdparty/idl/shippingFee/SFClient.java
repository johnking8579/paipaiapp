package com.qq.qqbuy.thirdparty.idl.shippingFee;


import com.paipai.component.c2cplatform.IAsynWebStub;

import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.shippingFee.protocol.GetTemplateDetailReq;
import com.qq.qqbuy.thirdparty.idl.shippingFee.protocol.GetTemplateDetailResp;

public class SFClient extends SupportIDLBaseClient {

    public static GetTemplateDetailResp getTemplateDetail(int sellerUin, int cInnerId) {
        long start = System.currentTimeMillis();
        GetTemplateDetailReq req = new GetTemplateDetailReq();
        // req.setItemCode(itemCode);
        // req.setMachineKey(getDefaultMachineKey("ItemClient"));
        // req.setSource(CALL_IDL_SOURCE);
        req.setUin(855000369);
        req.setCInnerId((short)15);

        GetTemplateDetailResp resp = new GetTemplateDetailResp();
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setUin(53876675);
        stub.setOperator(53876675);
        stub.setSkey("@qFsFCQNth".getBytes());
        ret = invokePaiPaiIDL(req, resp, stub);

        long timeCost = System.currentTimeMillis() - start;
        return ret == SUCCESS ? resp : null;
    }
    
    public static void main(String[] args){
        SFClient.getTemplateDetail(0, 0);
    }
}
