package com.qq.qqbuy.thirdparty.idl.shippingFee;

import java.util.Vector;

import com.paipai.component.c2cplatform.IAsynWebStub;

import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.shippingFee.protocol.ApiCalcShipTemplateFeeReq;
import com.qq.qqbuy.thirdparty.idl.shippingFee.protocol.ApiCalcShipTemplateFeeResp;
import com.qq.qqbuy.thirdparty.idl.shippingFee.protocol.ApiGetShippingfeeByIdReq;
import com.qq.qqbuy.thirdparty.idl.shippingFee.protocol.ApiGetShippingfeeByIdResp;
import com.qq.qqbuy.thirdparty.idl.shippingFee.protocol.ApiShipInfo;
import com.qq.qqbuy.thirdparty.idl.shippingFee.protocol.ApiShipInfoList;

/**
 * @author winsonwu
 * @Created 2012-12-28
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class ShippingfeeClient extends SupportIDLBaseClient {
	
	protected static final String BETA_IP = "10.6.223.152";
	protected static final int SHIPPINGFEE_PORT = 53101;
	
//	protected static IAsynWebStub getWebStub4Deal() {
//		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
//		if (EnvManager.isIdlBetaEnvCurr()) {
//			stub.setIpAndPort(BETA_IP, SHIPPINGFEE_PORT);
//		} 
//		return stub;
//	}
	
	public static ApiCalcShipTemplateFeeResp calcTemplateFee4SingleItem(long sellerUin, long recvRid, long shipTptId, long codTptId, long cmdyRid, long cmdyNum) {
		long start = System.currentTimeMillis();
		ApiCalcShipTemplateFeeReq req = new ApiCalcShipTemplateFeeReq();
		req.setMachineKey(getDefaultMachineKey("Shippingfee"));
		req.setSource(CALL_IDL_SOURCE);
		req.setUin(sellerUin);
		req.setRecvRegId(recvRid);
		ApiShipInfoList shipList = new ApiShipInfoList();
		Vector<ApiShipInfo> ships = new Vector<ApiShipInfo>();
		
		// 普通运费模版
		if (shipTptId > 0) {
			ApiShipInfo ship = new ApiShipInfo();
			ship.setShipTptId(shipTptId);
			ship.setCmdyRegId(cmdyRid);
			ship.setCmdyNum(cmdyNum);
			ships.add(ship);
		}
		// 货到付款运费模版
		if (codTptId > 0) {
			ApiShipInfo codShip = new ApiShipInfo();
			codShip.setShipTptId(codTptId);
			codShip.setCmdyRegId(cmdyRid);
			codShip.setCmdyNum(cmdyNum);
			ships.add(codShip);
		}
		
		shipList.setVecShipInfo(ships);
		req.setApiShipInfoListIn(shipList);
		
		ApiCalcShipTemplateFeeResp resp = new ApiCalcShipTemplateFeeResp();
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		int ret = invokePaiPaiIDL(req, resp, stub);
		
		long timeCost = System.currentTimeMillis() - start;
        return ret == SUCCESS ? resp : null;
	}
	
	/**
	 * 获取运费模版接口（需要验证登录态）
	 * @param freightId 运费模版ID
	 * @param sellerUin 卖家QQ号
	 * @return
	 */
	public static ApiGetShippingfeeByIdResp getShippingfeeById(short freightId, long sellerUin) {
		long start = System.currentTimeMillis();
		ApiGetShippingfeeByIdReq req = new ApiGetShippingfeeByIdReq();
		req.setMachineKey(getDefaultMachineKey("Shippingfee"));
		req.setSource(CALL_IDL_SOURCE);
		req.setInnerId(freightId);
		req.setUin(sellerUin);
		
		ApiGetShippingfeeByIdResp resp = new ApiGetShippingfeeByIdResp();
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		stub.setOperator(sellerUin);
		int ret = invokePaiPaiIDL(req, resp, stub);
		
		long timeCost = System.currentTimeMillis() - start;
        return ret == SUCCESS ? resp : null;
	}
	
}
