package com.qq.qqbuy.thirdparty.idl.stat;

import java.util.Map;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.paipai.lang.uint32_t;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.exception.ExternalInterfaceException;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.stat.protocol.GetDealStatInfoByUinReq;
import com.qq.qqbuy.thirdparty.idl.stat.protocol.GetDealStatInfoByUinResp;
import com.qq.qqbuy.thirdparty.idl.stat.protocol.GetFavoritesByIdReq;
import com.qq.qqbuy.thirdparty.idl.stat.protocol.GetFavoritesByIdResp;

public class StatClient extends SupportIDLBaseClient {
	
	
	/**
	 * 查询商品或店铺被收藏的次数
	 * @param keyId 店铺ID或商品ID(long值)
	 * @return  查店铺时,key=2
	 */ 
    public static GetFavoritesByIdResp findFavCount(Long keyId){
        GetFavoritesByIdReq req = new GetFavoritesByIdReq();
        GetFavoritesByIdResp resp = new GetFavoritesByIdResp();
        req.setKeyId(keyId);        
        int ret = 0;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setRouteKey(System.currentTimeMillis()) ;
        try {
			ret = stub.invoke(req, resp);
		} catch (Exception e) {
			Log.run.warn("e", e);
			ret =  ERRCODE_CALLIDL_FAIL;  
		}
        return ret == SUCCESS ? resp : null;
    }
    
    /**
     * 通过QQ号获取定单统计信息之请求
     * @param uin
     * @return map key见StatConst.订单统计相关值
     */
    public Map<uint32_t,Integer> getDealStatInfoByUin(long uin)	{
    	GetDealStatInfoByUinReq req = new GetDealStatInfoByUinReq();
    	GetDealStatInfoByUinResp resp = new GetDealStatInfoByUinResp();
    	req.setUin(uin);
    	AsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
    	invoke(stub, req, resp);
    	if(resp.getResult() != 0)
    		throw new ExternalInterfaceException(0, resp.getResult());
    	return resp.getStatData();
    }

	
}
