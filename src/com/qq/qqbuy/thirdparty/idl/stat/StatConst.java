package com.qq.qqbuy.thirdparty.idl.stat;

/**
 * 从c++ stat_define.h复制过来
 * @author JingYing
 * @date 2015年1月14日
 */
public class StatConst {
	
	public static final int
		//------------------------------------------------------------业务领域类型定义("AREATYPE" +  "_" + <键值类型> + "_"+ <领域>)
			AREATYPE_UIN_MSGBRD		= 1,	//留言相关统计
			AREATYPE_UIN_DEAL		= 2,	//订单相关统计
			AREATYPE_UIN_ITEM        =3, //商品相关统计
			AREATYPE_UID_FAVORITES   = 4, // 收藏相关统计
			AREATYPE_UIN_UNP_DEAL    = 5,   // 新统一平台订单相关统计
		
		//---------------------------------------------------------------统计项类型定义（统计项是归属于某一业务领域）
		// 留言统计
			UIN_MSGBRD_QSHOP_BUYER_REPLY				= 1,	//(店铺留言)(买家)被回复的留言数
			UIN_MSGBRD_QSHOP_BUYER_NOTREPLY			= 2,	//(店铺留言)(买家)未被回复的留言数
			UIN_MSGBRD_QSHOP_SELLER_REPLY				= 3,	//(店铺留言)(卖家)回复的留言数
			UIN_MSGBRD_QSHOP_SELLER_NOTREPLY			= 4,	//(店铺留言)(卖家)未回复的留言数

		// 订单统计
			UIN_DEAL_BUYER_CHENGBAO_SERVDATE= 1,	//买家订单在诚保服务期内的个数
			UIN_DEAL_BUYER_WAIT4REFUND		= 2,	//(买家)买家已申请退款，等待卖家响应退款请求 13/8/15
			UIN_DEAL_BUYER_WAIT4CONSIGNMENT	= 3,	//(买家)买家已经付款，等待卖家发货的交易 5
			UIN_DEAL_BUYER_WAIT4PAY			= 4,	//(买家)等待买家付款和买家付款中的交易 3/4
			UIN_DEAL_BUYER_WAIT4PAYCONFIRM	= 5,	//(买家)卖家已发货，等待买家收货的交易 6/17/18
			UIN_DEAL_BUYER_PAY_3M			= 6,	//(买家)下单时间在三个月内的已付款数(直接统计)
			ITEMID_ITEM_PAYCNT_1M 			=7 ,	// 一个月内的付款笔数(包括当天31天 当天实时计算)
			ITEMID_ITEM_PAYMONEY_1M 		=8,	   	// 一个月内的付款金额(包括当天31天 当天实时计算)
			ITEMID_ITEM_PAYCNT_PERIOD 		=9 ,	// 当期的付款笔数(-90天~当天)
			ITEMID_ITEM_PAYCNT_90DAY 		=10 ,	// 当期销售量(编辑后重置)
			UIN_DEAL_SELLER_WAIT4REFUND		= 12,	//(卖家)买家已申请退款，等待卖家响应退款请求 13/8/15
			UIN_DEAL_SELLER_WAIT4CONSIGNMENT= 13,	//(卖家)买家已经付款，等待卖家发货的交易 5
			UIN_DEAL_SELLER_WAIT4PAY		= 14,	//(卖家)等待买家付款和买家付款中的交易 3/4
			UIN_DEAL_SELLER_WAIT4PAYCONFIRM	= 15,	//(卖家)卖家已发货，等待买家收货的交易 6/17/18
			UIN_DEAL_SELLER_PAY_3M			= 16,	//(卖家)下单时间在三个月内的已付款数(直接统计)
			UIN_DEAL_BUYER_IN_REFUND		= 17,	//(买家)申请退款中
			UIN_DEAL_SELLER_IN_REFUND 		= 18,	//(卖家)申请退款中
			UIN_DEAL_BUYER_COD_BEFORE_SIGN  = 19,   // 买家 cod待发货和已发货的 订单数目
			UIN_DEAL_SELLER_CONFIRM_RECV 	= 20,	//(卖家)买家已确认收货，等待系统返款的交易 


		// 商品统计
			UIN_ITEM_ALL = 0, //所有商品
			UIN_ITEM_UPLOAD = 1, //发布商品
			UIN_ITEM_INSTORE = 2, //仓库中
			UIN_ITEM_SELLING = 3, //出售中
			UIN_ITEM_HIS = 4,  //违规商品
			UIN_ITEM_SHOPWINDOW = 5,  //橱窗推荐的商品数
			UIN_ITEM_SHOP_TJ = 6,  //店铺推荐商品数
			UIN_ITEM_ONLINE_TJ = 7,  //全站推荐的商品数
			UIN_ITEM_TIMEDOWN = 8,  //到期下架的
			UIN_ITEM_SOLD = 9,  //售完
			UIN_ITEM_NEW = 10;//上新
}
