 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: test.StatServDao.java

package com.qq.qqbuy.thirdparty.idl.stat.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import java.util.ArrayList;
import com.paipai.lang.uint32_t;
import java.util.List;

/**
 *批量获取新统一平台订单的所有统计信息请求
 *
 *@date 2014-09-03 04:24:58
 *
 *@since version:0
*/
public class  GetBatchUnifyingPlatformDealStatInfoByUinReq extends NetMessage
{
	/**
	 * qq号
	 *
	 * 版本 >= 0
	 */
	 private List<uint32_t> uins = new ArrayList<uint32_t>();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(uins);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		uins = (List<uint32_t>)bs.popList(ArrayList.class,uint32_t.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x10511818L;
	}


	/**
	 * 获取qq号
	 * 
	 * 此字段的版本 >= 0
	 * @return uins value 类型为:List<uint32_t>
	 * 
	 */
	public List<uint32_t> getUins()
	{
		return uins;
	}


	/**
	 * 设置qq号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:List<uint32_t>
	 * 
	 */
	public void setUins(List<uint32_t> value)
	{
		if (value != null) {
				this.uins = value;
		}else{
				this.uins = new ArrayList<uint32_t>();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetBatchUnifyingPlatformDealStatInfoByUinReq)
				length += ByteStream.getObjectSize(uins, null);  //计算字段uins的长度 size_of(List)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetBatchUnifyingPlatformDealStatInfoByUinReq)
				length += ByteStream.getObjectSize(uins, encoding);  //计算字段uins的长度 size_of(List)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
