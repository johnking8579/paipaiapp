 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: test.StatServDao.java

package com.qq.qqbuy.thirdparty.idl.stat.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *据统计server的key，初始化统计server的具体值
 *
 *@date 2014-09-03 04:24:58
 *
 *@since version:0
*/
public class  InitStatCommTmemReq extends NetMessage
{
	/**
	 * keyId
	 *
	 * 版本 >= 0
	 */
	 private long keyId;

	/**
	 * areaType
	 *
	 * 版本 >= 0
	 */
	 private int areaType;

	/**
	 * statType
	 *
	 * 版本 >= 0
	 */
	 private long statType;

	/**
	 * value
	 *
	 * 版本 >= 0
	 */
	 private long value;


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushLong(keyId);
		bs.pushUShort(areaType);
		bs.pushUInt(statType);
		bs.pushUInt(value);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		keyId = bs.popLong();
		areaType = bs.popUShort();
		statType = bs.popUInt();
		value = bs.popUInt();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x10541803L;
	}


	/**
	 * 获取keyId
	 * 
	 * 此字段的版本 >= 0
	 * @return keyId value 类型为:long
	 * 
	 */
	public long getKeyId()
	{
		return keyId;
	}


	/**
	 * 设置keyId
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setKeyId(long value)
	{
		this.keyId = value;
	}


	/**
	 * 获取areaType
	 * 
	 * 此字段的版本 >= 0
	 * @return areaType value 类型为:int
	 * 
	 */
	public int getAreaType()
	{
		return areaType;
	}


	/**
	 * 设置areaType
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setAreaType(int value)
	{
		this.areaType = value;
	}


	/**
	 * 获取statType
	 * 
	 * 此字段的版本 >= 0
	 * @return statType value 类型为:long
	 * 
	 */
	public long getStatType()
	{
		return statType;
	}


	/**
	 * 设置statType
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setStatType(long value)
	{
		this.statType = value;
	}


	/**
	 * 获取value
	 * 
	 * 此字段的版本 >= 0
	 * @return value value 类型为:long
	 * 
	 */
	public long getValue()
	{
		return value;
	}


	/**
	 * 设置value
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setValue(long value)
	{
		this.value = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(InitStatCommTmemReq)
				length += 17;  //计算字段keyId的长度 size_of(uint64_t)
				length += 2;  //计算字段areaType的长度 size_of(uint16_t)
				length += 4;  //计算字段statType的长度 size_of(uint32_t)
				length += 4;  //计算字段value的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(InitStatCommTmemReq)
				length += 17;  //计算字段keyId的长度 size_of(uint64_t)
				length += 2;  //计算字段areaType的长度 size_of(uint16_t)
				length += 4;  //计算字段statType的长度 size_of(uint32_t)
				length += 4;  //计算字段value的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
