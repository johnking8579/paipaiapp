 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: test.StatServDao.java

package com.qq.qqbuy.thirdparty.idl.stat.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *反转Tmem的当前使用的key
 *
 *@date 2014-09-03 04:24:58
 *
 *@since version:0
*/
public class  ReverseTmemKeyReq extends NetMessage
{
	/**
	 * 领域类型
	 *
	 * 版本 >= 0
	 */
	 private int areaType;

	/**
	 * 同步时间
	 *
	 * 版本 >= 0
	 */
	 private long initTime;

	/**
	 * 模得大小
	 *
	 * 版本 >= 0
	 */
	 private long modSize;


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUShort(areaType);
		bs.pushUInt(initTime);
		bs.pushUInt(modSize);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		areaType = bs.popUShort();
		initTime = bs.popUInt();
		modSize = bs.popUInt();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x10541804L;
	}


	/**
	 * 获取领域类型
	 * 
	 * 此字段的版本 >= 0
	 * @return areaType value 类型为:int
	 * 
	 */
	public int getAreaType()
	{
		return areaType;
	}


	/**
	 * 设置领域类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setAreaType(int value)
	{
		this.areaType = value;
	}


	/**
	 * 获取同步时间
	 * 
	 * 此字段的版本 >= 0
	 * @return initTime value 类型为:long
	 * 
	 */
	public long getInitTime()
	{
		return initTime;
	}


	/**
	 * 设置同步时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setInitTime(long value)
	{
		this.initTime = value;
	}


	/**
	 * 获取模得大小
	 * 
	 * 此字段的版本 >= 0
	 * @return modSize value 类型为:long
	 * 
	 */
	public long getModSize()
	{
		return modSize;
	}


	/**
	 * 设置模得大小
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setModSize(long value)
	{
		this.modSize = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(ReverseTmemKeyReq)
				length += 2;  //计算字段areaType的长度 size_of(uint16_t)
				length += 4;  //计算字段initTime的长度 size_of(uint32_t)
				length += 4;  //计算字段modSize的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(ReverseTmemKeyReq)
				length += 2;  //计算字段areaType的长度 size_of(uint16_t)
				length += 4;  //计算字段initTime的长度 size_of(uint32_t)
				length += 4;  //计算字段modSize的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
