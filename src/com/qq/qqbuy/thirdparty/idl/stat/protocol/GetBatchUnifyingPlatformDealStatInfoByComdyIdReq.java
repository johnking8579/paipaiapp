 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: test.StatServDao.java

package com.qq.qqbuy.thirdparty.idl.stat.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import com.paipai.lang.uint64_t;
import java.util.ArrayList;
import java.util.List;

/**
 *批量获取新统一平台订单的所有统计信息请求
 *
 *@date 2014-09-03 04:24:58
 *
 *@since version:0
*/
public class  GetBatchUnifyingPlatformDealStatInfoByComdyIdReq extends NetMessage
{
	/**
	 * 商品ID
	 *
	 * 版本 >= 0
	 */
	 private List<uint64_t> comdyIds = new ArrayList<uint64_t>();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(comdyIds);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		comdyIds = (List<uint64_t>)bs.popList(ArrayList.class,uint64_t.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x10511819L;
	}


	/**
	 * 获取商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return comdyIds value 类型为:List<uint64_t>
	 * 
	 */
	public List<uint64_t> getComdyIds()
	{
		return comdyIds;
	}


	/**
	 * 设置商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:List<uint64_t>
	 * 
	 */
	public void setComdyIds(List<uint64_t> value)
	{
		if (value != null) {
				this.comdyIds = value;
		}else{
				this.comdyIds = new ArrayList<uint64_t>();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetBatchUnifyingPlatformDealStatInfoByComdyIdReq)
				length += ByteStream.getObjectSize(comdyIds, null);  //计算字段comdyIds的长度 size_of(List)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetBatchUnifyingPlatformDealStatInfoByComdyIdReq)
				length += ByteStream.getObjectSize(comdyIds, encoding);  //计算字段comdyIds的长度 size_of(List)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
