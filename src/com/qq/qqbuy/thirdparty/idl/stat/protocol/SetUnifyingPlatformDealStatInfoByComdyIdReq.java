 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: test.StatServDao.java

package com.qq.qqbuy.thirdparty.idl.stat.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import java.util.Map;
import com.paipai.lang.uint32_t;
import java.util.HashMap;

/**
 *通过商品ID和统计类型来设置统计数值之请求
 *
 *@date 2014-09-03 04:24:58
 *
 *@since version:0
*/
public class  SetUnifyingPlatformDealStatInfoByComdyIdReq extends NetMessage
{
	/**
	 * 商品ID
	 *
	 * 版本 >= 0
	 */
	 private long comdyId;

	/**
	 * 要设置统计类型
	 *
	 * 版本 >= 0
	 */
	 private Map<uint32_t,Integer> StatData = new HashMap<uint32_t,Integer>();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushLong(comdyId);
		bs.pushObject(StatData);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		comdyId = bs.popLong();
		StatData = (Map<uint32_t,Integer>)bs.popMap(uint32_t.class,Integer.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x10511817L;
	}


	/**
	 * 获取商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return comdyId value 类型为:long
	 * 
	 */
	public long getComdyId()
	{
		return comdyId;
	}


	/**
	 * 设置商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setComdyId(long value)
	{
		this.comdyId = value;
	}


	/**
	 * 获取要设置统计类型
	 * 
	 * 此字段的版本 >= 0
	 * @return StatData value 类型为:Map<uint32_t,Integer>
	 * 
	 */
	public Map<uint32_t,Integer> getStatData()
	{
		return StatData;
	}


	/**
	 * 设置要设置统计类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<uint32_t,Integer>
	 * 
	 */
	public void setStatData(Map<uint32_t,Integer> value)
	{
		if (value != null) {
				this.StatData = value;
		}else{
				this.StatData = new HashMap<uint32_t,Integer>();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(SetUnifyingPlatformDealStatInfoByComdyIdReq)
				length += 17;  //计算字段comdyId的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(StatData, null);  //计算字段StatData的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(SetUnifyingPlatformDealStatInfoByComdyIdReq)
				length += 17;  //计算字段comdyId的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(StatData, encoding);  //计算字段StatData的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
