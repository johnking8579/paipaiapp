 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: test.StatServDao.java

package com.qq.qqbuy.thirdparty.idl.stat.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *据统计server的key，设置统计server的具体值
 *
 *@date 2014-09-03 04:24:58
 *
 *@since version:0
*/
public class  SetStatCommTmemResp extends NetMessage
{
	/**
	 * 返回最新的统计server的值
	 *
	 * 版本 >= 0
	 */
	 private long newValue;


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushUInt(newValue);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		newValue = bs.popUInt();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x10548802L;
	}


	/**
	 * 获取返回最新的统计server的值
	 * 
	 * 此字段的版本 >= 0
	 * @return newValue value 类型为:long
	 * 
	 */
	public long getNewValue()
	{
		return newValue;
	}


	/**
	 * 设置返回最新的统计server的值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNewValue(long value)
	{
		this.newValue = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SetStatCommTmemResp)
				length += 4;  //计算字段newValue的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(SetStatCommTmemResp)
				length += 4;  //计算字段newValue的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
