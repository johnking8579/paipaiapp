

//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang

package com.qq.qqbuy.thirdparty.idl.stat.protocol;


import com.paipai.util.annotation.Protocol;
import com.paipai.netframework.kernal.NetAction;


import com.paipai.lang.GenericWrapper;/**
 *
 *NetAction类
 *
 */
public class StatServDao  extends NetAction
{
	@Override
	 public int getSnowslideThresold(){
		// 这里设置防雪崩时间，也就是超时时间值
		  return 2;
	 }




	@Protocol(cmdid = 0x10541801L, desc = "根据统计server的key，修改具体的值，有加，减，具体的值在value中指定", export = true)
	 public UpdateStatCommTmemResp UpdateStatCommTmem(UpdateStatCommTmemReq req){
		UpdateStatCommTmemResp resp = new  UpdateStatCommTmemResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x10511817L, desc = "设置新统一平台订单的统计项", export = true)
	 public SetUnifyingPlatformDealStatInfoByComdyIdResp SetUnifyingPlatformDealStatInfoByComdyId(SetUnifyingPlatformDealStatInfoByComdyIdReq req){
		SetUnifyingPlatformDealStatInfoByComdyIdResp resp = new  SetUnifyingPlatformDealStatInfoByComdyIdResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x10541802L, desc = "根据统计server的key，设置统计server的具体值", export = true)
	 public SetStatCommTmemResp SetStatCommTmem(SetStatCommTmemReq req){
		SetStatCommTmemResp resp = new  SetStatCommTmemResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x10511812L, desc = "设置订单的统计项", export = true)
	 public SetDealStatInfoByComdyIdResp SetDealStatInfoByComdyId(SetDealStatInfoByComdyIdReq req){
		SetDealStatInfoByComdyIdResp resp = new  SetDealStatInfoByComdyIdResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x10541804L, desc = "反转Tmem的当前使用的key", export = true)
	 public ReverseTmemKeyResp ReverseTmemKey(ReverseTmemKeyReq req){
		ReverseTmemKeyResp resp = new  ReverseTmemKeyResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x10541805L, desc = "初始化的时候，更新msg消息接口", export = true)
	 public InitUpdateStatCommTmemResp InitUpdateStatCommTmem(InitUpdateStatCommTmemReq req){
		InitUpdateStatCommTmemResp resp = new  InitUpdateStatCommTmemResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x10541803L, desc = "根据统计server的key，初始化统计server的具体值", export = true)
	 public InitStatCommTmemResp InitStatCommTmem(InitStatCommTmemReq req){
		InitStatCommTmemResp resp = new  InitStatCommTmemResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x10511815L, desc = "获取新统一平台订单的所有统计信息", export = true)
	 public GetUnifyingPlatformDealStatInfoByUinResp GetUnifyingPlatformDealStatInfoByUin(GetUnifyingPlatformDealStatInfoByUinReq req){
		GetUnifyingPlatformDealStatInfoByUinResp resp = new  GetUnifyingPlatformDealStatInfoByUinResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x10511816L, desc = "获取新统一平台订单的所有统计信息", export = true)
	 public GetUnifyingPlatformDealStatInfoByComdyIdResp GetUnifyingPlatformDealStatInfoByComdyId(GetUnifyingPlatformDealStatInfoByComdyIdReq req){
		GetUnifyingPlatformDealStatInfoByComdyIdResp resp = new  GetUnifyingPlatformDealStatInfoByComdyIdResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x10501801L, desc = "获取留言的所有统计信息", export = true)
	 public GetMsgBrdStatInfoByUinResp GetMsgBrdStatInfoByUin(GetMsgBrdStatInfoByUinReq req){
		GetMsgBrdStatInfoByUinResp resp = new  GetMsgBrdStatInfoByUinResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x10531801L, desc = "根据商品id或者店铺id获取该商品的收藏统计数", export = true)
	 public GetFavoritesByIdResp GetFavoritesById(GetFavoritesByIdReq req){
		GetFavoritesByIdResp resp = new  GetFavoritesByIdResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x10511801L, desc = "获取订单的所有统计信息", export = true)
	 public GetDealStatInfoByUinResp GetDealStatInfoByUin(GetDealStatInfoByUinReq req){
		GetDealStatInfoByUinResp resp = new  GetDealStatInfoByUinResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x10511811L, desc = "获取订单的所有统计信息", export = true)
	 public GetDealStatInfoByComdyIdResp GetDealStatInfoByComdyId(GetDealStatInfoByComdyIdReq req){
		GetDealStatInfoByComdyIdResp resp = new  GetDealStatInfoByComdyIdResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x10521801L, desc = "获取商品的所有统计信息", export = true)
	 public GetCommodityStatInfoByUinResp GetCommodityStatInfoByUin(GetCommodityStatInfoByUinReq req){
		GetCommodityStatInfoByUinResp resp = new  GetCommodityStatInfoByUinResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x10511818L, desc = "批量获取新统一平台订单的所有统计信息,只适合数据全部在一个set", export = true)
	 public GetBatchUnifyingPlatformDealStatInfoByUinResp GetBatchUnifyingPlatformDealStatInfoByUin(GetBatchUnifyingPlatformDealStatInfoByUinReq req){
		GetBatchUnifyingPlatformDealStatInfoByUinResp resp = new  GetBatchUnifyingPlatformDealStatInfoByUinResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x10511819L, desc = "批量获取新统一平台订单的所有统计信息,只适合数据全部在一个set", export = true)
	 public GetBatchUnifyingPlatformDealStatInfoByComdyIdResp GetBatchUnifyingPlatformDealStatInfoByComdyId(GetBatchUnifyingPlatformDealStatInfoByComdyIdReq req){
		GetBatchUnifyingPlatformDealStatInfoByComdyIdResp resp = new  GetBatchUnifyingPlatformDealStatInfoByComdyIdResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x10511813L, desc = "批量获取订单的所有统计信息,只适合数据全部在一个set", export = true)
	 public GetBatchDealStatInfoByUinResp GetBatchDealStatInfoByUin(GetBatchDealStatInfoByUinReq req){
		GetBatchDealStatInfoByUinResp resp = new  GetBatchDealStatInfoByUinResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x10511814L, desc = "批量获取订单的所有统计信息,只适合数据全部在一个set", export = true)
	 public GetBatchDealStatInfoByComdyIdResp GetBatchDealStatInfoByComdyId(GetBatchDealStatInfoByComdyIdReq req){
		GetBatchDealStatInfoByComdyIdResp resp = new  GetBatchDealStatInfoByComdyIdResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }



}
