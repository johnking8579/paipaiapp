 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: test.StatServDao.java

package com.qq.qqbuy.thirdparty.idl.stat.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *根据商品id或者店铺id获取该商品的收藏统计数的请求参数
 *
 *@date 2014-09-03 04:24:58
 *
 *@since version:0
*/
public class  GetFavoritesByIdReq extends NetMessage
{
	/**
	 * keyId
	 *
	 * 版本 >= 0
	 */
	 private long keyId;


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushLong(keyId);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		keyId = bs.popLong();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x10531801L;
	}


	/**
	 * 获取keyId
	 * 
	 * 此字段的版本 >= 0
	 * @return keyId value 类型为:long
	 * 
	 */
	public long getKeyId()
	{
		return keyId;
	}


	/**
	 * 设置keyId
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setKeyId(long value)
	{
		this.keyId = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetFavoritesByIdReq)
				length += 17;  //计算字段keyId的长度 size_of(uint64_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetFavoritesByIdReq)
				length += 17;  //计算字段keyId的长度 size_of(uint64_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
