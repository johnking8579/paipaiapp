package com.qq.qqbuy.thirdparty.idl.shop;

import java.util.Map;
import java.util.Vector;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.exception.ExternalInterfaceException;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.thirdparty.idl.IDLResult;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.item.protocol.eval.EvalStat;
import com.qq.qqbuy.thirdparty.idl.item.protocol.eval.GetEvalStatReq;
import com.qq.qqbuy.thirdparty.idl.item.protocol.eval.GetEvalStatResp;
import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiGetRecommedComdyListReq;
import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiGetRecommedComdyListResp;
import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiGetShopCategoryListByUinReq;
import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiGetShopCategoryListByUinResp;
import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiGetShopComdyListReq;
import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiGetShopComdyListResp;
import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiGetShopInfoReq;
import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiGetShopInfoResp;
import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiItemFilter;
import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiShopCategory;
import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiShopInfo;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.apiuser.ApiGetUserSimpleProfileReq;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.apiuser.ApiGetUserSimpleProfileResp;


public class ShopClient extends SupportIDLBaseClient
{
    protected static final String CALL_IDL_SOURCE = "mobileLife"; // 用于IDL接口服务端判断是否进行免登陆处理

    /**
     * 
     * @Title: getRecommedComdyList
     * 
     * @Description: 获取店铺推荐信息
     * 
     * @param shopID
     *            店铺id，也就是卖家qq号码
     * 
     * @param machineKey
     *            机器码
     * @param scene
     *            场景id，默认为0，暂时不填
     * 
     * @return 设定文件
     * @return ApiGetRecommedComdyListResp 返回类型
     * @throws
     */
    public static ApiGetRecommedComdyListResp getRecommedComdyList(long shopID,
            String machineKey, long scene)
    {
        long start = System.currentTimeMillis();

        ApiGetRecommedComdyListReq req = new ApiGetRecommedComdyListReq();
        ApiGetRecommedComdyListResp resp = new ApiGetRecommedComdyListResp();
        if (StringUtil.isNotBlank(machineKey))
            req.setMachineKey(machineKey);
        else
            req.setMachineKey(CALL_IDL_SOURCE);
        req.setSource(CALL_IDL_SOURCE);
        req.setShopID(shopID);
        req.setScene(scene);
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setOperator(shopID);
        int ret = -1;
        try
        {
            ret = stub.invoke(req, resp);
        } catch (Exception e)
        {
            Log.run
                    .error(
                            "ShopClient#getRecommedComdyList invokeIDL but find an exception!",
                            e);
        }

        long timeCost = System.currentTimeMillis() - start;
        String msg = resp.getResult() == 0 ? "IDL获取店铺推荐信息成功" : "IDL获取店铺推荐信息失败";
        return ret == SUCCESS ? resp : null;
    }

    /**
     * @Title: getShopCategoryListByUin
     * @Description: 获取店铺分类信息
     * @param shopID 店铺id，也就是卖家qq号码
     * @param machineKey 机器码
     * @param scene 场景id，默认为0，暂时不填
     * @throws
     */
    public static Vector<ApiShopCategory> findShopCategory(long shopID, String machineKey)    {
        ApiGetShopCategoryListByUinReq req = new ApiGetShopCategoryListByUinReq();
        ApiGetShopCategoryListByUinResp resp = new ApiGetShopCategoryListByUinResp();
        if (StringUtil.isNotBlank(machineKey))
            req.setMachineKey(machineKey);
        else
            req.setMachineKey(CALL_IDL_SOURCE);
        req.setSource(CALL_IDL_SOURCE);
        req.setUin(shopID);
        req.setPageSize(500);
        req.setStartIndex(0);
        
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setOperator(shopID);
        invoke(stub, req, resp);;
		if(resp.getResult() != 0)
			throw new ExternalInterfaceException(0, resp.getResult(), resp.getErrmsg());
		return resp.getApiShopCategoryList();
    }

    /**
     * 
     * @Title: getShopComdyList
     * @Description: 获取商品列表信息
     * 
     * @param shopID
     *            卖家号
     * @param machineKey
     *            机器信息
     * @param scene
     *            场景id。默认为0
     * @param startIndex
     *            起始页
     * @param pageSize
     *            每页的大小
     * @param orderType
     *            排序方式 使用说明见
     *            {@link com.paipai.c2c.item.constants.CVItemFilterConstants}
     * @param filterMap
     *            过滤map，使用说明见
     *            {@link com.paipai.c2c.item.constants.CVItemFilterConstants}
     * @return 设定文件
     * @return ApiGetShopComdyListResp 返回类型
     * @deprecated 接口响应速度慢,返回的价格为原价,需要二次计算折扣价.返回的销量为总销量
     * @throws
     */
    public static ApiGetShopComdyListResp getShopComdyList(long shopID,
            String machineKey, long scene, long startIndex, long pageSize,
            long orderType, Map<String, String> filterMap)	{
        ApiGetShopComdyListReq req = new ApiGetShopComdyListReq();
        ApiGetShopComdyListResp resp = new ApiGetShopComdyListResp();
        if (StringUtil.isNotBlank(machineKey))
            req.setMachineKey(machineKey);
        else
            req.setMachineKey(CALL_IDL_SOURCE);
        req.setSource(CALL_IDL_SOURCE);
        req.setScene(scene);
        req.setShopID(shopID);

        ApiItemFilter apiItemFilter = new ApiItemFilter();
        apiItemFilter.setOrderType(orderType);
        apiItemFilter.setPageSize(pageSize);
        apiItemFilter.setSellerUin(shopID);
        apiItemFilter.setStartIndex(startIndex);
        if (filterMap != null && !filterMap.isEmpty())   {
            apiItemFilter.setFilterMap(filterMap);
        }
        req.setApiItemFilter(apiItemFilter);

        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setOperator(shopID);
        invoke(stub, req, resp);
        if(resp.getResult() != 0)
        	throw new ExternalInterfaceException(0, resp.getResult(), resp.getErrMsg());
        return resp;
    }
    
    /**
     * 查询店铺详情
     * @param sellerUin
     * @return 当店铺不存在时, 对象不为空, ApiShopInfo.shopId=0
     */
	public ApiShopInfo getShopInfo(long sellerUin)  {
		ApiGetShopInfoReq req = new ApiGetShopInfoReq();
		req.setSource("mqgo");
		req.setUin(sellerUin);
		ApiGetShopInfoResp resp = new ApiGetShopInfoResp();
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		invoke(stub, req, resp);
		if(resp.getResult() != 0)
			throw new ExternalInterfaceException(0, resp.getResult(), resp.getErrmsg());
		return resp.getShopInfo();
	}
	
	/**
	 * 查用户基本信息
	 */
	public IDLResult<ApiGetUserSimpleProfileResp> getUserSimpleProfile(ApiGetUserSimpleProfileReq req,long uin,String skey)  throws BusinessException{
		ApiGetUserSimpleProfileResp resp = new ApiGetUserSimpleProfileResp();
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		int ret = invokePaiPaiIDL(req, resp, stub);
		
		IDLResult<ApiGetUserSimpleProfileResp> rsp = new IDLResult<ApiGetUserSimpleProfileResp>(resp, ret, resp.getResult());
		return rsp;
	}
	
	/**
	 * 查评价信息
	 */
	public EvalStat getEval(long sellerUin)	{
		GetEvalStatReq req = new GetEvalStatReq();
		GetEvalStatResp resp = new GetEvalStatResp();
		req.setSource("mqgo");
		req.setUin(sellerUin);
		
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		invoke(stub, req, resp);
		if(resp.getResult() != 0)
			throw new ExternalInterfaceException(0, resp.getResult(), resp.getErrMsg());
		return resp.getStatInfo();
	}
	
	/**
	 * 查boss评价信息
	 */
//	public IDLResult<BizStatQueryOneResp> getBossEval(BizStatQueryOneReq req)  throws BusinessException{
//		long start = System.currentTimeMillis();
//		BizStatQueryOneResp resp = new BizStatQueryOneResp();
//		int ret= 0;
//		
//		BossWebStub stub = WebStubFactory.getBossStub();
//		try {
//			stub.invoke(req, resp);
//		} catch (Exception e) {
//			Log.run.warn("boss invoke",e);
//        	ret =  ERRCODE_CALLIDL_FAIL;
//		}
//		
//		IDLResult<BizStatQueryOneResp> rsp = new IDLResult<BizStatQueryOneResp>(resp, ret, resp.getResult());
//		return rsp;
//	}
//	
//	/**
//	 * 查商品信息
//	 */
//	public IDLResult<GetCommodityStatInfoByUinResp> getCommStat(GetCommodityStatInfoByUinReq req)  throws BusinessException{
//		long start = System.currentTimeMillis();
//		GetCommodityStatInfoByUinResp resp = new GetCommodityStatInfoByUinResp();
//		
//		
//		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
//		int ret = invokePaiPaiIDL(req, resp, stub);
//		try {
//			stub.invoke(req, resp);
//		} catch (Exception e) {
//			Log.run.warn("boss invoke",e);
////        	Log.idlRun.warn("Calling invokePaiPaiIDL but find an exception!", e);
//        	ret =  ERRCODE_CALLIDL_FAIL;
//		}
//		
//		IDLResult<GetCommodityStatInfoByUinResp> rsp = new IDLResult<GetCommodityStatInfoByUinResp>(resp, ret, resp.getResult());
//		return rsp;
//	}
	
}
