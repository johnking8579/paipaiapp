 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.shop.ao.idl.ShopApiAo.java

package com.qq.qqbuy.thirdparty.idl.shop.protocol;


import java.util.Vector;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ApiItem;

/**
 *获取商品信息列表返回类
 *
 *@date 2013-03-07 02:28:56
 *
 *@since version:0
*/
public class  ApiGetShopComdyListResp implements IServiceObject
{
	public long result;
	/**
	 * 返回的商品信息列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<ApiItem> ItemList = new Vector<ApiItem>();

	/**
	 * 符合条件的商品总数
	 *
	 * 版本 >= 0
	 */
	 private long foundItemNum;

	/**
	 * 店铺商品总数
	 *
	 * 版本 >= 0
	 */
	 private long totalItemNum;

	/**
	 * 错误信息,用于debug
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 返回保留字
	 *
	 * 版本 >= 0
	 */
	 private String outReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(ItemList);
		bs.pushUInt(foundItemNum);
		bs.pushUInt(totalItemNum);
		bs.pushString(errMsg);
		bs.pushString(outReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		ItemList = (Vector<ApiItem>)bs.popVector(ApiItem.class);
		foundItemNum = bs.popUInt();
		totalItemNum = bs.popUInt();
		errMsg = bs.popString();
		outReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x3014880DL;
	}


	/**
	 * 获取返回的商品信息列表
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemList value 类型为:Vector<ApiItem>
	 * 
	 */
	public Vector<ApiItem> getItemList()
	{
		return ItemList;
	}


	/**
	 * 设置返回的商品信息列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<ApiItem>
	 * 
	 */
	public void setItemList(Vector<ApiItem> value)
	{
		if (value != null) {
				this.ItemList = value;
		}else{
				this.ItemList = new Vector<ApiItem>();
		}
	}


	/**
	 * 获取符合条件的商品总数
	 * 
	 * 此字段的版本 >= 0
	 * @return foundItemNum value 类型为:long
	 * 
	 */
	public long getFoundItemNum()
	{
		return foundItemNum;
	}


	/**
	 * 设置符合条件的商品总数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFoundItemNum(long value)
	{
		this.foundItemNum = value;
	}


	/**
	 * 获取店铺商品总数
	 * 
	 * 此字段的版本 >= 0
	 * @return totalItemNum value 类型为:long
	 * 
	 */
	public long getTotalItemNum()
	{
		return totalItemNum;
	}


	/**
	 * 设置店铺商品总数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalItemNum(long value)
	{
		this.totalItemNum = value;
	}


	/**
	 * 获取错误信息,用于debug
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误信息,用于debug
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	/**
	 * 获取返回保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return outReserve;
	}


	/**
	 * 设置返回保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		this.outReserve = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ApiGetShopComdyListResp)
				length += ByteStream.getObjectSize(ItemList);  //计算字段ItemList的长度 size_of(Vector)
				length += 4;  //计算字段foundItemNum的长度 size_of(uint32_t)
				length += 4;  //计算字段totalItemNum的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserve);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
