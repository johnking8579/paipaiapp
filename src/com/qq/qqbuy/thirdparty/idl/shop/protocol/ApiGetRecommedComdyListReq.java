/*     */ package com.qq.qqbuy.thirdparty.idl.shop.protocol;
/*     */ 
/*     */ import com.paipai.netframework.kernal.NetMessage;
/*     */ import com.paipai.util.io.ByteStream;
/*     */ 
/*     */ public class ApiGetRecommedComdyListReq extends NetMessage
/*     */ {
/*  26 */   private String machineKey = new String();
/*     */ 
/*  33 */   private String source = new String();
/*     */   private long scene;
/*     */   private long shopID;
/*  54 */   private String inReserve = new String();
/*     */ 
/*     */   public int serialize(ByteStream bs)
/*     */     throws Exception
/*     */   {
/*  59 */     bs.pushString(this.machineKey);
/*  60 */     bs.pushString(this.source);
/*  61 */     bs.pushUInt(this.scene);
/*  62 */     bs.pushUInt(this.shopID);
/*  63 */     bs.pushString(this.inReserve);
/*  64 */     return bs.getWrittenLength();
/*     */   }
/*     */ 
/*     */   public int unSerialize(ByteStream bs) throws Exception
/*     */   {
/*  69 */     this.machineKey = bs.popString();
/*  70 */     this.source = bs.popString();
/*  71 */     this.scene = bs.popUInt();
/*  72 */     this.shopID = bs.popUInt();
/*  73 */     this.inReserve = bs.popString();
/*  74 */     return bs.getReadLength();
/*     */   }
/*     */ 
/*     */   public long getCmdId()
/*     */   {
/*  79 */     return 806623238L;
/*     */   }
/*     */ 
/*     */   public String getMachineKey()
/*     */   {
/*  92 */     return this.machineKey;
/*     */   }
/*     */ 
/*     */   public void setMachineKey(String value)
/*     */   {
/* 105 */     if (value != null)
/* 106 */       this.machineKey = value;
/*     */     else
/* 108 */       this.machineKey = new String();
/*     */   }
/*     */ 
/*     */   public String getSource()
/*     */   {
/* 122 */     return this.source;
/*     */   }
/*     */ 
/*     */   public void setSource(String value)
/*     */   {
/* 135 */     if (value != null)
/* 136 */       this.source = value;
/*     */     else
/* 138 */       this.source = new String();
/*     */   }
/*     */ 
/*     */   public long getScene()
/*     */   {
/* 152 */     return this.scene;
/*     */   }
/*     */ 
/*     */   public void setScene(long value)
/*     */   {
/* 165 */     this.scene = value;
/*     */   }
/*     */ 
/*     */   public long getShopID()
/*     */   {
/* 178 */     return this.shopID;
/*     */   }
/*     */ 
/*     */   public void setShopID(long value)
/*     */   {
/* 191 */     this.shopID = value;
/*     */   }
/*     */ 
/*     */   public String getInReserve()
/*     */   {
/* 204 */     return this.inReserve;
/*     */   }
/*     */ 
/*     */   public void setInReserve(String value)
/*     */   {
/* 217 */     if (value != null)
/* 218 */       this.inReserve = value;
/*     */     else
/* 220 */       this.inReserve = new String();
/*     */   }
/*     */ 
/*     */   protected int getClassSize()
/*     */   {
/* 227 */     return getSize() - 4;
/*     */   }
/*     */ 
/*     */   public int getSize()
/*     */   {
/* 233 */     int length = 0;
/*     */     try {
/* 235 */       length = 0;
/* 236 */       length += ByteStream.getObjectSize(this.machineKey);
/* 237 */       length += ByteStream.getObjectSize(this.source);
/* 238 */       length += 4;
/* 239 */       length += 4;
/* 240 */       length += ByteStream.getObjectSize(this.inReserve);
/*     */     } catch (Exception e) {
/* 242 */       e.printStackTrace();
/*     */     }
/* 244 */     return length;
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.shop.ao.protocol.ApiGetRecommedComdyListReq
 * JD-Core Version:    0.6.2
 */