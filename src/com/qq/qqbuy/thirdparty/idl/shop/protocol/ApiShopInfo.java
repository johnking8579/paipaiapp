 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.shop.protocol;


import java.util.Vector;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *店铺信息po
 *
 *@date 2013-08-22 03:13::01
 *
 *@since version:0
*/
public class ApiShopInfo  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long poVersion = 20100531; 

	/**
	 * 店铺id，即qq号
	 *
	 * 版本 >= 0
	 */
	 private long shopID;

	/**
	 * 店主qq号
	 *
	 * 版本 >= 0
	 */
	 private long ownerUin;

	/**
	 * 注册时间
	 *
	 * 版本 >= 0
	 */
	 private long regTime;

	/**
	 * 店铺属性 1代表店铺2.0
	 *
	 * 版本 >= 0
	 */
	 private long property;

	/**
	 * 店名更新次数
	 *
	 * 版本 >= 0
	 */
	 private long nameUpdateTimes;

	/**
	 * 店名更新时间
	 *
	 * 版本 >= 0
	 */
	 private long nameUpdTime;

	/**
	 * 店铺已推荐商品数量
	 *
	 * 版本 >= 0
	 */
	 private long recommendNum;

	/**
	 * 店铺出售中商品数(已废弃)
	 *
	 * 版本 >= 0
	 */
	 private long commCount;

	/**
	 * 店铺自定义类型对多值
	 *
	 * 版本 >= 0
	 */
	 private long categoryIndex;

	/**
	 * 店铺属性标记 已废弃
	 *
	 * 版本 >= 0
	 */
	 private long flag;

	/**
	 * 店铺颜色风格
	 *
	 * 版本 >= 0
	 */
	 private long shopStyle;

	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 店铺最后更新时间
	 *
	 * 版本 >= 0
	 */
	 private long lastUpdTime;

	/**
	 * 自定义分类最后更新时间
	 *
	 * 版本 >= 0
	 */
	 private long cateGoryLastUpdTime;

	/**
	 * 友情链接最后更新时间
	 *
	 * 版本 >= 0
	 */
	 private long friendLinkLastUpdTime;

	/**
	 * 公告最后更新时间
	 *
	 * 版本 >= 0
	 */
	 private long announceLastUpdTime;

	/**
	 * 店铺活动最后更新时间
	 *
	 * 版本 >= 0
	 */
	 private long actionLastUpdTime;

	/**
	 * 店铺允许设置的最大橱窗数
	 *
	 * 版本 >= 0
	 */
	 private long shopWindowNum;

	/**
	 * 店铺审核版本号(给Boss使用)
	 *
	 * 版本 >= 0
	 */
	 private long auditVersion;

	/**
	 * 店铺审核结果位
	 *
	 * 版本 >= 0
	 */
	 private long auditResult1;

	/**
	 * 店铺审核结果位,预留
	 *
	 * 版本 >= 0
	 */
	 private long auditResult2;

	/**
	 * 店铺审核标记位(给Boss使用)
	 *
	 * 版本 >= 0
	 */
	 private long auditFlag1;

	/**
	 * 店铺审核标记位(给Boss使用)
	 *
	 * 版本 >= 0
	 */
	 private long auditFlag2;

	/**
	 * 废弃
	 *
	 * 版本 >= 0
	 */
	 private long customadFlag;

	/**
	 * 废弃
	 *
	 * 版本 >= 0
	 */
	 private long customactFlag;

	/**
	 * 店铺名称
	 *
	 * 版本 >= 0
	 */
	 private String shopName = new String();

	/**
	 * 店铺Logo
	 *
	 * 版本 >= 0
	 */
	 private String mainLogoName = new String();

	/**
	 * 店铺介绍TSS文件名
	 *
	 * 版本 >= 0
	 */
	 private String shopIntroName = new String();

	/**
	 * 店铺招牌TSS文件名
	 *
	 * 版本 >= 0
	 */
	 private String shopSignName = new String();

	/**
	 * 店铺活动区域TSS文件名
	 *
	 * 版本 >= 0
	 */
	 private String actionAreaName = new String();

	/**
	 * 店铺类别
	 *
	 * 版本 >= 0
	 */
	 private String shopType = new String();

	/**
	 * 店铺主营区域
	 *
	 * 版本 >= 0
	 */
	 private String mainArea = new String();

	/**
	 * 自定义html代码存放文件名称
	 *
	 * 版本 >= 0
	 */
	 private String customHtmlName = new String();

	/**
	 * 店铺公告
	 *
	 * 版本 >= 0
	 */
	 private String annouce = new String();

	/**
	 * 店铺描述
	 *
	 * 版本 >= 0
	 */
	 private String description = new String();

	/**
	 * 预留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserve = new String();

	/**
	 * 版本 >= 0
	 */
	 private short poVersion_u;

	/**
	 * 版本 >= 0
	 */
	 private short shopID_u;

	/**
	 * 版本 >= 0
	 */
	 private short ownerUin_u;

	/**
	 * 版本 >= 0
	 */
	 private short regTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short property_u;

	/**
	 * 版本 >= 0
	 */
	 private short nameUpdateTimes_u;

	/**
	 * 版本 >= 0
	 */
	 private short nameUpdTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short recommendNum_u;

	/**
	 * 版本 >= 0
	 */
	 private short commCount_u;

	/**
	 * 版本 >= 0
	 */
	 private short categoryIndex_u;

	/**
	 * 版本 >= 0
	 */
	 private short flag_u;

	/**
	 * 版本 >= 0
	 */
	 private short shopStyle_u;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 版本 >= 0
	 */
	 private short lastUpdTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short cateGoryLastUpdTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short friendLinkLastUpdTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short announceLastUpdTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short actionLastUpdTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short shopWindowNum_u;

	/**
	 * 版本 >= 0
	 */
	 private short auditVersion_u;

	/**
	 * 版本 >= 0
	 */
	 private short auditResult1_u;

	/**
	 * 版本 >= 0
	 */
	 private short auditResult2_u;

	/**
	 * 版本 >= 0
	 */
	 private short auditFlag1_u;

	/**
	 * 版本 >= 0
	 */
	 private short auditFlag2_u;

	/**
	 * 版本 >= 0
	 */
	 private short customadFlag_u;

	/**
	 * 版本 >= 0
	 */
	 private short customactFlag_u;

	/**
	 * 版本 >= 0
	 */
	 private short shopName_u;

	/**
	 * 版本 >= 0
	 */
	 private short mainLogoName_u;

	/**
	 * 版本 >= 0
	 */
	 private short shopIntroName_u;

	/**
	 * 版本 >= 0
	 */
	 private short shopSignName_u;

	/**
	 * 版本 >= 0
	 */
	 private short actionAreaName_u;

	/**
	 * 版本 >= 0
	 */
	 private short shopType_u;

	/**
	 * 版本 >= 0
	 */
	 private short mainArea_u;

	/**
	 * 版本 >= 0
	 */
	 private short customHtmlName_u;

	/**
	 * 版本 >= 0
	 */
	 private short annouce_u;

	/**
	 * 版本 >= 0
	 */
	 private short description_u;

	/**
	 * 版本 >= 0
	 */
	 private short reserve_u;

	/**
	 * 热点信息
	 *
	 * 版本 >= 0
	 */
	 private Vector<String> vecHotKey = new Vector<String>();

	/**
	 * 热点信息_u
	 *
	 * 版本 >= 0
	 */
	 private short vecHotKey_u;

	/**
	 * 店铺最后更新时间
	 *
	 * 版本 >= 0
	 */
	 private long shopTypeLastUpdTime;

	/**
	 * 店铺类型更新次数
	 *
	 * 版本 >= 0
	 */
	 private long shopTypeUpdateTimes;

	/**
	 * 店铺最后更新时间_u
	 *
	 * 版本 >= 0
	 */
	 private short shopTypeLastUpdTime_u;

	/**
	 * 店铺类型更新次数_u
	 *
	 * 版本 >= 0
	 */
	 private short shopTypeUpdateTimes_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(poVersion);
		bs.pushUInt(shopID);
		bs.pushUInt(ownerUin);
		bs.pushUInt(regTime);
		bs.pushUInt(property);
		bs.pushUInt(nameUpdateTimes);
		bs.pushUInt(nameUpdTime);
		bs.pushUInt(recommendNum);
		bs.pushUInt(commCount);
		bs.pushUInt(categoryIndex);
		bs.pushUInt(flag);
		bs.pushUInt(shopStyle);
		bs.pushUInt(version);
		bs.pushUInt(lastUpdTime);
		bs.pushUInt(cateGoryLastUpdTime);
		bs.pushUInt(friendLinkLastUpdTime);
		bs.pushUInt(announceLastUpdTime);
		bs.pushUInt(actionLastUpdTime);
		bs.pushUInt(shopWindowNum);
		bs.pushUInt(auditVersion);
		bs.pushUInt(auditResult1);
		bs.pushUInt(auditResult2);
		bs.pushUInt(auditFlag1);
		bs.pushUInt(auditFlag2);
		bs.pushUInt(customadFlag);
		bs.pushUInt(customactFlag);
		bs.pushString(shopName);
		bs.pushString(mainLogoName);
		bs.pushString(shopIntroName);
		bs.pushString(shopSignName);
		bs.pushString(actionAreaName);
		bs.pushString(shopType);
		bs.pushString(mainArea);
		bs.pushString(customHtmlName);
		bs.pushString(annouce);
		bs.pushString(description);
		bs.pushString(reserve);
		bs.pushUByte(poVersion_u);
		bs.pushUByte(shopID_u);
		bs.pushUByte(ownerUin_u);
		bs.pushUByte(regTime_u);
		bs.pushUByte(property_u);
		bs.pushUByte(nameUpdateTimes_u);
		bs.pushUByte(nameUpdTime_u);
		bs.pushUByte(recommendNum_u);
		bs.pushUByte(commCount_u);
		bs.pushUByte(categoryIndex_u);
		bs.pushUByte(flag_u);
		bs.pushUByte(shopStyle_u);
		bs.pushUByte(version_u);
		bs.pushUByte(lastUpdTime_u);
		bs.pushUByte(cateGoryLastUpdTime_u);
		bs.pushUByte(friendLinkLastUpdTime_u);
		bs.pushUByte(announceLastUpdTime_u);
		bs.pushUByte(actionLastUpdTime_u);
		bs.pushUByte(shopWindowNum_u);
		bs.pushUByte(auditVersion_u);
		bs.pushUByte(auditResult1_u);
		bs.pushUByte(auditResult2_u);
		bs.pushUByte(auditFlag1_u);
		bs.pushUByte(auditFlag2_u);
		bs.pushUByte(customadFlag_u);
		bs.pushUByte(customactFlag_u);
		bs.pushUByte(shopName_u);
		bs.pushUByte(mainLogoName_u);
		bs.pushUByte(shopIntroName_u);
		bs.pushUByte(shopSignName_u);
		bs.pushUByte(actionAreaName_u);
		bs.pushUByte(shopType_u);
		bs.pushUByte(mainArea_u);
		bs.pushUByte(customHtmlName_u);
		bs.pushUByte(annouce_u);
		bs.pushUByte(description_u);
		bs.pushUByte(reserve_u);
		bs.pushObject(vecHotKey);
		bs.pushUByte(vecHotKey_u);
		bs.pushUInt(shopTypeLastUpdTime);
		bs.pushUInt(shopTypeUpdateTimes);
		bs.pushUByte(shopTypeLastUpdTime_u);
		bs.pushUByte(shopTypeUpdateTimes_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		poVersion = bs.popUInt();
		shopID = bs.popUInt();
		ownerUin = bs.popUInt();
		regTime = bs.popUInt();
		property = bs.popUInt();
		nameUpdateTimes = bs.popUInt();
		nameUpdTime = bs.popUInt();
		recommendNum = bs.popUInt();
		commCount = bs.popUInt();
		categoryIndex = bs.popUInt();
		flag = bs.popUInt();
		shopStyle = bs.popUInt();
		version = bs.popUInt();
		lastUpdTime = bs.popUInt();
		cateGoryLastUpdTime = bs.popUInt();
		friendLinkLastUpdTime = bs.popUInt();
		announceLastUpdTime = bs.popUInt();
		actionLastUpdTime = bs.popUInt();
		shopWindowNum = bs.popUInt();
		auditVersion = bs.popUInt();
		auditResult1 = bs.popUInt();
		auditResult2 = bs.popUInt();
		auditFlag1 = bs.popUInt();
		auditFlag2 = bs.popUInt();
		customadFlag = bs.popUInt();
		customactFlag = bs.popUInt();
		shopName = bs.popString();
		mainLogoName = bs.popString();
		shopIntroName = bs.popString();
		shopSignName = bs.popString();
		actionAreaName = bs.popString();
		shopType = bs.popString();
		mainArea = bs.popString();
		customHtmlName = bs.popString();
		annouce = bs.popString();
		description = bs.popString();
		reserve = bs.popString();
		poVersion_u = bs.popUByte();
		shopID_u = bs.popUByte();
		ownerUin_u = bs.popUByte();
		regTime_u = bs.popUByte();
		property_u = bs.popUByte();
		nameUpdateTimes_u = bs.popUByte();
		nameUpdTime_u = bs.popUByte();
		recommendNum_u = bs.popUByte();
		commCount_u = bs.popUByte();
		categoryIndex_u = bs.popUByte();
		flag_u = bs.popUByte();
		shopStyle_u = bs.popUByte();
		version_u = bs.popUByte();
		lastUpdTime_u = bs.popUByte();
		cateGoryLastUpdTime_u = bs.popUByte();
		friendLinkLastUpdTime_u = bs.popUByte();
		announceLastUpdTime_u = bs.popUByte();
		actionLastUpdTime_u = bs.popUByte();
		shopWindowNum_u = bs.popUByte();
		auditVersion_u = bs.popUByte();
		auditResult1_u = bs.popUByte();
		auditResult2_u = bs.popUByte();
		auditFlag1_u = bs.popUByte();
		auditFlag2_u = bs.popUByte();
		customadFlag_u = bs.popUByte();
		customactFlag_u = bs.popUByte();
		shopName_u = bs.popUByte();
		mainLogoName_u = bs.popUByte();
		shopIntroName_u = bs.popUByte();
		shopSignName_u = bs.popUByte();
		actionAreaName_u = bs.popUByte();
		shopType_u = bs.popUByte();
		mainArea_u = bs.popUByte();
		customHtmlName_u = bs.popUByte();
		annouce_u = bs.popUByte();
		description_u = bs.popUByte();
		reserve_u = bs.popUByte();
		vecHotKey = (Vector<String>)bs.popVector(String.class);
		vecHotKey_u = bs.popUByte();
		shopTypeLastUpdTime = bs.popUInt();
		shopTypeUpdateTimes = bs.popUInt();
		shopTypeLastUpdTime_u = bs.popUByte();
		shopTypeUpdateTimes_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return poVersion value 类型为:long
	 * 
	 */
	public long getPoVersion()
	{
		return poVersion;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPoVersion(long value)
	{
		this.poVersion = value;
		this.poVersion_u = 1;
	}


	/**
	 * 获取店铺id，即qq号
	 * 
	 * 此字段的版本 >= 0
	 * @return shopID value 类型为:long
	 * 
	 */
	public long getShopID()
	{
		return shopID;
	}


	/**
	 * 设置店铺id，即qq号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setShopID(long value)
	{
		this.shopID = value;
		this.shopID_u = 1;
	}


	/**
	 * 获取店主qq号
	 * 
	 * 此字段的版本 >= 0
	 * @return ownerUin value 类型为:long
	 * 
	 */
	public long getOwnerUin()
	{
		return ownerUin;
	}


	/**
	 * 设置店主qq号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOwnerUin(long value)
	{
		this.ownerUin = value;
		this.ownerUin_u = 1;
	}


	/**
	 * 获取注册时间
	 * 
	 * 此字段的版本 >= 0
	 * @return regTime value 类型为:long
	 * 
	 */
	public long getRegTime()
	{
		return regTime;
	}


	/**
	 * 设置注册时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRegTime(long value)
	{
		this.regTime = value;
		this.regTime_u = 1;
	}


	/**
	 * 获取店铺属性 1代表店铺2.0
	 * 
	 * 此字段的版本 >= 0
	 * @return property value 类型为:long
	 * 
	 */
	public long getProperty()
	{
		return property;
	}


	/**
	 * 设置店铺属性 1代表店铺2.0
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setProperty(long value)
	{
		this.property = value;
		this.property_u = 1;
	}


	/**
	 * 获取店名更新次数
	 * 
	 * 此字段的版本 >= 0
	 * @return nameUpdateTimes value 类型为:long
	 * 
	 */
	public long getNameUpdateTimes()
	{
		return nameUpdateTimes;
	}


	/**
	 * 设置店名更新次数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNameUpdateTimes(long value)
	{
		this.nameUpdateTimes = value;
		this.nameUpdateTimes_u = 1;
	}


	/**
	 * 获取店名更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return nameUpdTime value 类型为:long
	 * 
	 */
	public long getNameUpdTime()
	{
		return nameUpdTime;
	}


	/**
	 * 设置店名更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNameUpdTime(long value)
	{
		this.nameUpdTime = value;
		this.nameUpdTime_u = 1;
	}


	/**
	 * 获取店铺已推荐商品数量
	 * 
	 * 此字段的版本 >= 0
	 * @return recommendNum value 类型为:long
	 * 
	 */
	public long getRecommendNum()
	{
		return recommendNum;
	}


	/**
	 * 设置店铺已推荐商品数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRecommendNum(long value)
	{
		this.recommendNum = value;
		this.recommendNum_u = 1;
	}


	/**
	 * 获取店铺出售中商品数(已废弃)
	 * 
	 * 此字段的版本 >= 0
	 * @return commCount value 类型为:long
	 * 
	 */
	public long getCommCount()
	{
		return commCount;
	}


	/**
	 * 设置店铺出售中商品数(已废弃)
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCommCount(long value)
	{
		this.commCount = value;
		this.commCount_u = 1;
	}


	/**
	 * 获取店铺自定义类型对多值
	 * 
	 * 此字段的版本 >= 0
	 * @return categoryIndex value 类型为:long
	 * 
	 */
	public long getCategoryIndex()
	{
		return categoryIndex;
	}


	/**
	 * 设置店铺自定义类型对多值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCategoryIndex(long value)
	{
		this.categoryIndex = value;
		this.categoryIndex_u = 1;
	}


	/**
	 * 获取店铺属性标记 已废弃
	 * 
	 * 此字段的版本 >= 0
	 * @return flag value 类型为:long
	 * 
	 */
	public long getFlag()
	{
		return flag;
	}


	/**
	 * 设置店铺属性标记 已废弃
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFlag(long value)
	{
		this.flag = value;
		this.flag_u = 1;
	}


	/**
	 * 获取店铺颜色风格
	 * 
	 * 此字段的版本 >= 0
	 * @return shopStyle value 类型为:long
	 * 
	 */
	public long getShopStyle()
	{
		return shopStyle;
	}


	/**
	 * 设置店铺颜色风格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setShopStyle(long value)
	{
		this.shopStyle = value;
		this.shopStyle_u = 1;
	}


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取店铺最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return lastUpdTime value 类型为:long
	 * 
	 */
	public long getLastUpdTime()
	{
		return lastUpdTime;
	}


	/**
	 * 设置店铺最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastUpdTime(long value)
	{
		this.lastUpdTime = value;
		this.lastUpdTime_u = 1;
	}


	/**
	 * 获取自定义分类最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return cateGoryLastUpdTime value 类型为:long
	 * 
	 */
	public long getCateGoryLastUpdTime()
	{
		return cateGoryLastUpdTime;
	}


	/**
	 * 设置自定义分类最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCateGoryLastUpdTime(long value)
	{
		this.cateGoryLastUpdTime = value;
		this.cateGoryLastUpdTime_u = 1;
	}


	/**
	 * 获取友情链接最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return friendLinkLastUpdTime value 类型为:long
	 * 
	 */
	public long getFriendLinkLastUpdTime()
	{
		return friendLinkLastUpdTime;
	}


	/**
	 * 设置友情链接最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFriendLinkLastUpdTime(long value)
	{
		this.friendLinkLastUpdTime = value;
		this.friendLinkLastUpdTime_u = 1;
	}


	/**
	 * 获取公告最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return announceLastUpdTime value 类型为:long
	 * 
	 */
	public long getAnnounceLastUpdTime()
	{
		return announceLastUpdTime;
	}


	/**
	 * 设置公告最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAnnounceLastUpdTime(long value)
	{
		this.announceLastUpdTime = value;
		this.announceLastUpdTime_u = 1;
	}


	/**
	 * 获取店铺活动最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return actionLastUpdTime value 类型为:long
	 * 
	 */
	public long getActionLastUpdTime()
	{
		return actionLastUpdTime;
	}


	/**
	 * 设置店铺活动最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActionLastUpdTime(long value)
	{
		this.actionLastUpdTime = value;
		this.actionLastUpdTime_u = 1;
	}


	/**
	 * 获取店铺允许设置的最大橱窗数
	 * 
	 * 此字段的版本 >= 0
	 * @return shopWindowNum value 类型为:long
	 * 
	 */
	public long getShopWindowNum()
	{
		return shopWindowNum;
	}


	/**
	 * 设置店铺允许设置的最大橱窗数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setShopWindowNum(long value)
	{
		this.shopWindowNum = value;
		this.shopWindowNum_u = 1;
	}


	/**
	 * 获取店铺审核版本号(给Boss使用)
	 * 
	 * 此字段的版本 >= 0
	 * @return auditVersion value 类型为:long
	 * 
	 */
	public long getAuditVersion()
	{
		return auditVersion;
	}


	/**
	 * 设置店铺审核版本号(给Boss使用)
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAuditVersion(long value)
	{
		this.auditVersion = value;
		this.auditVersion_u = 1;
	}


	/**
	 * 获取店铺审核结果位
	 * 
	 * 此字段的版本 >= 0
	 * @return auditResult1 value 类型为:long
	 * 
	 */
	public long getAuditResult1()
	{
		return auditResult1;
	}


	/**
	 * 设置店铺审核结果位
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAuditResult1(long value)
	{
		this.auditResult1 = value;
		this.auditResult1_u = 1;
	}


	/**
	 * 获取店铺审核结果位,预留
	 * 
	 * 此字段的版本 >= 0
	 * @return auditResult2 value 类型为:long
	 * 
	 */
	public long getAuditResult2()
	{
		return auditResult2;
	}


	/**
	 * 设置店铺审核结果位,预留
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAuditResult2(long value)
	{
		this.auditResult2 = value;
		this.auditResult2_u = 1;
	}


	/**
	 * 获取店铺审核标记位(给Boss使用)
	 * 
	 * 此字段的版本 >= 0
	 * @return auditFlag1 value 类型为:long
	 * 
	 */
	public long getAuditFlag1()
	{
		return auditFlag1;
	}


	/**
	 * 设置店铺审核标记位(给Boss使用)
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAuditFlag1(long value)
	{
		this.auditFlag1 = value;
		this.auditFlag1_u = 1;
	}


	/**
	 * 获取店铺审核标记位(给Boss使用)
	 * 
	 * 此字段的版本 >= 0
	 * @return auditFlag2 value 类型为:long
	 * 
	 */
	public long getAuditFlag2()
	{
		return auditFlag2;
	}


	/**
	 * 设置店铺审核标记位(给Boss使用)
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAuditFlag2(long value)
	{
		this.auditFlag2 = value;
		this.auditFlag2_u = 1;
	}


	/**
	 * 获取废弃
	 * 
	 * 此字段的版本 >= 0
	 * @return customadFlag value 类型为:long
	 * 
	 */
	public long getCustomadFlag()
	{
		return customadFlag;
	}


	/**
	 * 设置废弃
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCustomadFlag(long value)
	{
		this.customadFlag = value;
		this.customadFlag_u = 1;
	}


	/**
	 * 获取废弃
	 * 
	 * 此字段的版本 >= 0
	 * @return customactFlag value 类型为:long
	 * 
	 */
	public long getCustomactFlag()
	{
		return customactFlag;
	}


	/**
	 * 设置废弃
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCustomactFlag(long value)
	{
		this.customactFlag = value;
		this.customactFlag_u = 1;
	}


	/**
	 * 获取店铺名称
	 * 
	 * 此字段的版本 >= 0
	 * @return shopName value 类型为:String
	 * 
	 */
	public String getShopName()
	{
		return shopName;
	}


	/**
	 * 设置店铺名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setShopName(String value)
	{
		if (value != null) {
				this.shopName = value;
				this.shopName_u = 1;
		}
	}


	/**
	 * 获取店铺Logo
	 * 
	 * 此字段的版本 >= 0
	 * @return mainLogoName value 类型为:String
	 * 
	 */
	public String getMainLogoName()
	{
		return mainLogoName;
	}


	/**
	 * 设置店铺Logo
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMainLogoName(String value)
	{
		if (value != null) {
				this.mainLogoName = value;
				this.mainLogoName_u = 1;
		}
	}


	/**
	 * 获取店铺介绍TSS文件名
	 * 
	 * 此字段的版本 >= 0
	 * @return shopIntroName value 类型为:String
	 * 
	 */
	public String getShopIntroName()
	{
		return shopIntroName;
	}


	/**
	 * 设置店铺介绍TSS文件名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setShopIntroName(String value)
	{
		if (value != null) {
				this.shopIntroName = value;
				this.shopIntroName_u = 1;
		}
	}


	/**
	 * 获取店铺招牌TSS文件名
	 * 
	 * 此字段的版本 >= 0
	 * @return shopSignName value 类型为:String
	 * 
	 */
	public String getShopSignName()
	{
		return shopSignName;
	}


	/**
	 * 设置店铺招牌TSS文件名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setShopSignName(String value)
	{
		if (value != null) {
				this.shopSignName = value;
				this.shopSignName_u = 1;
		}
	}


	/**
	 * 获取店铺活动区域TSS文件名
	 * 
	 * 此字段的版本 >= 0
	 * @return actionAreaName value 类型为:String
	 * 
	 */
	public String getActionAreaName()
	{
		return actionAreaName;
	}


	/**
	 * 设置店铺活动区域TSS文件名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setActionAreaName(String value)
	{
		if (value != null) {
				this.actionAreaName = value;
				this.actionAreaName_u = 1;
		}
	}


	/**
	 * 获取店铺类别
	 * 
	 * 此字段的版本 >= 0
	 * @return shopType value 类型为:String
	 * 
	 */
	public String getShopType()
	{
		return shopType;
	}


	/**
	 * 设置店铺类别
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setShopType(String value)
	{
		if (value != null) {
				this.shopType = value;
				this.shopType_u = 1;
		}
	}


	/**
	 * 获取店铺主营区域
	 * 
	 * 此字段的版本 >= 0
	 * @return mainArea value 类型为:String
	 * 
	 */
	public String getMainArea()
	{
		return mainArea;
	}


	/**
	 * 设置店铺主营区域
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMainArea(String value)
	{
		if (value != null) {
				this.mainArea = value;
				this.mainArea_u = 1;
		}
	}


	/**
	 * 获取自定义html代码存放文件名称
	 * 
	 * 此字段的版本 >= 0
	 * @return customHtmlName value 类型为:String
	 * 
	 */
	public String getCustomHtmlName()
	{
		return customHtmlName;
	}


	/**
	 * 设置自定义html代码存放文件名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCustomHtmlName(String value)
	{
		if (value != null) {
				this.customHtmlName = value;
				this.customHtmlName_u = 1;
		}
	}


	/**
	 * 获取店铺公告
	 * 
	 * 此字段的版本 >= 0
	 * @return annouce value 类型为:String
	 * 
	 */
	public String getAnnouce()
	{
		return annouce;
	}


	/**
	 * 设置店铺公告
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAnnouce(String value)
	{
		if (value != null) {
				this.annouce = value;
				this.annouce_u = 1;
		}
	}


	/**
	 * 获取店铺描述
	 * 
	 * 此字段的版本 >= 0
	 * @return description value 类型为:String
	 * 
	 */
	public String getDescription()
	{
		return description;
	}


	/**
	 * 设置店铺描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDescription(String value)
	{
		if (value != null) {
				this.description = value;
				this.description_u = 1;
		}
	}


	/**
	 * 获取预留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserve value 类型为:String
	 * 
	 */
	public String getReserve()
	{
		return reserve;
	}


	/**
	 * 设置预留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserve(String value)
	{
		if (value != null) {
				this.reserve = value;
				this.reserve_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return poVersion_u value 类型为:short
	 * 
	 */
	public short getPoVersion_u()
	{
		return poVersion_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPoVersion_u(short value)
	{
		this.poVersion_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return shopID_u value 类型为:short
	 * 
	 */
	public short getShopID_u()
	{
		return shopID_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopID_u(short value)
	{
		this.shopID_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ownerUin_u value 类型为:short
	 * 
	 */
	public short getOwnerUin_u()
	{
		return ownerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOwnerUin_u(short value)
	{
		this.ownerUin_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return regTime_u value 类型为:short
	 * 
	 */
	public short getRegTime_u()
	{
		return regTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRegTime_u(short value)
	{
		this.regTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return property_u value 类型为:short
	 * 
	 */
	public short getProperty_u()
	{
		return property_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProperty_u(short value)
	{
		this.property_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return nameUpdateTimes_u value 类型为:short
	 * 
	 */
	public short getNameUpdateTimes_u()
	{
		return nameUpdateTimes_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNameUpdateTimes_u(short value)
	{
		this.nameUpdateTimes_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return nameUpdTime_u value 类型为:short
	 * 
	 */
	public short getNameUpdTime_u()
	{
		return nameUpdTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNameUpdTime_u(short value)
	{
		this.nameUpdTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return recommendNum_u value 类型为:short
	 * 
	 */
	public short getRecommendNum_u()
	{
		return recommendNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRecommendNum_u(short value)
	{
		this.recommendNum_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return commCount_u value 类型为:short
	 * 
	 */
	public short getCommCount_u()
	{
		return commCount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCommCount_u(short value)
	{
		this.commCount_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return categoryIndex_u value 类型为:short
	 * 
	 */
	public short getCategoryIndex_u()
	{
		return categoryIndex_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCategoryIndex_u(short value)
	{
		this.categoryIndex_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return flag_u value 类型为:short
	 * 
	 */
	public short getFlag_u()
	{
		return flag_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFlag_u(short value)
	{
		this.flag_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return shopStyle_u value 类型为:short
	 * 
	 */
	public short getShopStyle_u()
	{
		return shopStyle_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopStyle_u(short value)
	{
		this.shopStyle_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return lastUpdTime_u value 类型为:short
	 * 
	 */
	public short getLastUpdTime_u()
	{
		return lastUpdTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastUpdTime_u(short value)
	{
		this.lastUpdTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cateGoryLastUpdTime_u value 类型为:short
	 * 
	 */
	public short getCateGoryLastUpdTime_u()
	{
		return cateGoryLastUpdTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCateGoryLastUpdTime_u(short value)
	{
		this.cateGoryLastUpdTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return friendLinkLastUpdTime_u value 类型为:short
	 * 
	 */
	public short getFriendLinkLastUpdTime_u()
	{
		return friendLinkLastUpdTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFriendLinkLastUpdTime_u(short value)
	{
		this.friendLinkLastUpdTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return announceLastUpdTime_u value 类型为:short
	 * 
	 */
	public short getAnnounceLastUpdTime_u()
	{
		return announceLastUpdTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAnnounceLastUpdTime_u(short value)
	{
		this.announceLastUpdTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return actionLastUpdTime_u value 类型为:short
	 * 
	 */
	public short getActionLastUpdTime_u()
	{
		return actionLastUpdTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setActionLastUpdTime_u(short value)
	{
		this.actionLastUpdTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return shopWindowNum_u value 类型为:short
	 * 
	 */
	public short getShopWindowNum_u()
	{
		return shopWindowNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopWindowNum_u(short value)
	{
		this.shopWindowNum_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return auditVersion_u value 类型为:short
	 * 
	 */
	public short getAuditVersion_u()
	{
		return auditVersion_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAuditVersion_u(short value)
	{
		this.auditVersion_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return auditResult1_u value 类型为:short
	 * 
	 */
	public short getAuditResult1_u()
	{
		return auditResult1_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAuditResult1_u(short value)
	{
		this.auditResult1_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return auditResult2_u value 类型为:short
	 * 
	 */
	public short getAuditResult2_u()
	{
		return auditResult2_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAuditResult2_u(short value)
	{
		this.auditResult2_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return auditFlag1_u value 类型为:short
	 * 
	 */
	public short getAuditFlag1_u()
	{
		return auditFlag1_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAuditFlag1_u(short value)
	{
		this.auditFlag1_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return auditFlag2_u value 类型为:short
	 * 
	 */
	public short getAuditFlag2_u()
	{
		return auditFlag2_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAuditFlag2_u(short value)
	{
		this.auditFlag2_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return customadFlag_u value 类型为:short
	 * 
	 */
	public short getCustomadFlag_u()
	{
		return customadFlag_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCustomadFlag_u(short value)
	{
		this.customadFlag_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return customactFlag_u value 类型为:short
	 * 
	 */
	public short getCustomactFlag_u()
	{
		return customactFlag_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCustomactFlag_u(short value)
	{
		this.customactFlag_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return shopName_u value 类型为:short
	 * 
	 */
	public short getShopName_u()
	{
		return shopName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopName_u(short value)
	{
		this.shopName_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return mainLogoName_u value 类型为:short
	 * 
	 */
	public short getMainLogoName_u()
	{
		return mainLogoName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMainLogoName_u(short value)
	{
		this.mainLogoName_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return shopIntroName_u value 类型为:short
	 * 
	 */
	public short getShopIntroName_u()
	{
		return shopIntroName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopIntroName_u(short value)
	{
		this.shopIntroName_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return shopSignName_u value 类型为:short
	 * 
	 */
	public short getShopSignName_u()
	{
		return shopSignName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopSignName_u(short value)
	{
		this.shopSignName_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return actionAreaName_u value 类型为:short
	 * 
	 */
	public short getActionAreaName_u()
	{
		return actionAreaName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setActionAreaName_u(short value)
	{
		this.actionAreaName_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return shopType_u value 类型为:short
	 * 
	 */
	public short getShopType_u()
	{
		return shopType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopType_u(short value)
	{
		this.shopType_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return mainArea_u value 类型为:short
	 * 
	 */
	public short getMainArea_u()
	{
		return mainArea_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMainArea_u(short value)
	{
		this.mainArea_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return customHtmlName_u value 类型为:short
	 * 
	 */
	public short getCustomHtmlName_u()
	{
		return customHtmlName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCustomHtmlName_u(short value)
	{
		this.customHtmlName_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return annouce_u value 类型为:short
	 * 
	 */
	public short getAnnouce_u()
	{
		return annouce_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAnnouce_u(short value)
	{
		this.annouce_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return description_u value 类型为:short
	 * 
	 */
	public short getDescription_u()
	{
		return description_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDescription_u(short value)
	{
		this.description_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return reserve_u value 类型为:short
	 * 
	 */
	public short getReserve_u()
	{
		return reserve_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserve_u(short value)
	{
		this.reserve_u = value;
	}


	/**
	 * 获取热点信息
	 * 
	 * 此字段的版本 >= 0
	 * @return vecHotKey value 类型为:Vector<String>
	 * 
	 */
	public Vector<String> getVecHotKey()
	{
		return vecHotKey;
	}


	/**
	 * 设置热点信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<String>
	 * 
	 */
	public void setVecHotKey(Vector<String> value)
	{
		if (value != null) {
				this.vecHotKey = value;
				this.vecHotKey_u = 1;
		}
	}


	/**
	 * 获取热点信息_u
	 * 
	 * 此字段的版本 >= 0
	 * @return vecHotKey_u value 类型为:short
	 * 
	 */
	public short getVecHotKey_u()
	{
		return vecHotKey_u;
	}


	/**
	 * 设置热点信息_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVecHotKey_u(short value)
	{
		this.vecHotKey_u = value;
	}


	/**
	 * 获取店铺最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return shopTypeLastUpdTime value 类型为:long
	 * 
	 */
	public long getShopTypeLastUpdTime()
	{
		return shopTypeLastUpdTime;
	}


	/**
	 * 设置店铺最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setShopTypeLastUpdTime(long value)
	{
		this.shopTypeLastUpdTime = value;
		this.shopTypeLastUpdTime_u = 1;
	}


	/**
	 * 获取店铺类型更新次数
	 * 
	 * 此字段的版本 >= 0
	 * @return shopTypeUpdateTimes value 类型为:long
	 * 
	 */
	public long getShopTypeUpdateTimes()
	{
		return shopTypeUpdateTimes;
	}


	/**
	 * 设置店铺类型更新次数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setShopTypeUpdateTimes(long value)
	{
		this.shopTypeUpdateTimes = value;
		this.shopTypeUpdateTimes_u = 1;
	}


	/**
	 * 获取店铺最后更新时间_u
	 * 
	 * 此字段的版本 >= 0
	 * @return shopTypeLastUpdTime_u value 类型为:short
	 * 
	 */
	public short getShopTypeLastUpdTime_u()
	{
		return shopTypeLastUpdTime_u;
	}


	/**
	 * 设置店铺最后更新时间_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopTypeLastUpdTime_u(short value)
	{
		this.shopTypeLastUpdTime_u = value;
	}


	/**
	 * 获取店铺类型更新次数_u
	 * 
	 * 此字段的版本 >= 0
	 * @return shopTypeUpdateTimes_u value 类型为:short
	 * 
	 */
	public short getShopTypeUpdateTimes_u()
	{
		return shopTypeUpdateTimes_u;
	}


	/**
	 * 设置店铺类型更新次数_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopTypeUpdateTimes_u(short value)
	{
		this.shopTypeUpdateTimes_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ApiShopInfo)
				length += 4;  //计算字段poVersion的长度 size_of(uint32_t)
				length += 4;  //计算字段shopID的长度 size_of(uint32_t)
				length += 4;  //计算字段ownerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段regTime的长度 size_of(uint32_t)
				length += 4;  //计算字段property的长度 size_of(uint32_t)
				length += 4;  //计算字段nameUpdateTimes的长度 size_of(uint32_t)
				length += 4;  //计算字段nameUpdTime的长度 size_of(uint32_t)
				length += 4;  //计算字段recommendNum的长度 size_of(uint32_t)
				length += 4;  //计算字段commCount的长度 size_of(uint32_t)
				length += 4;  //计算字段categoryIndex的长度 size_of(uint32_t)
				length += 4;  //计算字段flag的长度 size_of(uint32_t)
				length += 4;  //计算字段shopStyle的长度 size_of(uint32_t)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段lastUpdTime的长度 size_of(uint32_t)
				length += 4;  //计算字段cateGoryLastUpdTime的长度 size_of(uint32_t)
				length += 4;  //计算字段friendLinkLastUpdTime的长度 size_of(uint32_t)
				length += 4;  //计算字段announceLastUpdTime的长度 size_of(uint32_t)
				length += 4;  //计算字段actionLastUpdTime的长度 size_of(uint32_t)
				length += 4;  //计算字段shopWindowNum的长度 size_of(uint32_t)
				length += 4;  //计算字段auditVersion的长度 size_of(uint32_t)
				length += 4;  //计算字段auditResult1的长度 size_of(uint32_t)
				length += 4;  //计算字段auditResult2的长度 size_of(uint32_t)
				length += 4;  //计算字段auditFlag1的长度 size_of(uint32_t)
				length += 4;  //计算字段auditFlag2的长度 size_of(uint32_t)
				length += 4;  //计算字段customadFlag的长度 size_of(uint32_t)
				length += 4;  //计算字段customactFlag的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(shopName);  //计算字段shopName的长度 size_of(String)
				length += ByteStream.getObjectSize(mainLogoName);  //计算字段mainLogoName的长度 size_of(String)
				length += ByteStream.getObjectSize(shopIntroName);  //计算字段shopIntroName的长度 size_of(String)
				length += ByteStream.getObjectSize(shopSignName);  //计算字段shopSignName的长度 size_of(String)
				length += ByteStream.getObjectSize(actionAreaName);  //计算字段actionAreaName的长度 size_of(String)
				length += ByteStream.getObjectSize(shopType);  //计算字段shopType的长度 size_of(String)
				length += ByteStream.getObjectSize(mainArea);  //计算字段mainArea的长度 size_of(String)
				length += ByteStream.getObjectSize(customHtmlName);  //计算字段customHtmlName的长度 size_of(String)
				length += ByteStream.getObjectSize(annouce);  //计算字段annouce的长度 size_of(String)
				length += ByteStream.getObjectSize(description);  //计算字段description的长度 size_of(String)
				length += ByteStream.getObjectSize(reserve);  //计算字段reserve的长度 size_of(String)
				length += 1;  //计算字段poVersion_u的长度 size_of(uint8_t)
				length += 1;  //计算字段shopID_u的长度 size_of(uint8_t)
				length += 1;  //计算字段ownerUin_u的长度 size_of(uint8_t)
				length += 1;  //计算字段regTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段property_u的长度 size_of(uint8_t)
				length += 1;  //计算字段nameUpdateTimes_u的长度 size_of(uint8_t)
				length += 1;  //计算字段nameUpdTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段recommendNum_u的长度 size_of(uint8_t)
				length += 1;  //计算字段commCount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段categoryIndex_u的长度 size_of(uint8_t)
				length += 1;  //计算字段flag_u的长度 size_of(uint8_t)
				length += 1;  //计算字段shopStyle_u的长度 size_of(uint8_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段lastUpdTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cateGoryLastUpdTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段friendLinkLastUpdTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段announceLastUpdTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段actionLastUpdTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段shopWindowNum_u的长度 size_of(uint8_t)
				length += 1;  //计算字段auditVersion_u的长度 size_of(uint8_t)
				length += 1;  //计算字段auditResult1_u的长度 size_of(uint8_t)
				length += 1;  //计算字段auditResult2_u的长度 size_of(uint8_t)
				length += 1;  //计算字段auditFlag1_u的长度 size_of(uint8_t)
				length += 1;  //计算字段auditFlag2_u的长度 size_of(uint8_t)
				length += 1;  //计算字段customadFlag_u的长度 size_of(uint8_t)
				length += 1;  //计算字段customactFlag_u的长度 size_of(uint8_t)
				length += 1;  //计算字段shopName_u的长度 size_of(uint8_t)
				length += 1;  //计算字段mainLogoName_u的长度 size_of(uint8_t)
				length += 1;  //计算字段shopIntroName_u的长度 size_of(uint8_t)
				length += 1;  //计算字段shopSignName_u的长度 size_of(uint8_t)
				length += 1;  //计算字段actionAreaName_u的长度 size_of(uint8_t)
				length += 1;  //计算字段shopType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段mainArea_u的长度 size_of(uint8_t)
				length += 1;  //计算字段customHtmlName_u的长度 size_of(uint8_t)
				length += 1;  //计算字段annouce_u的长度 size_of(uint8_t)
				length += 1;  //计算字段description_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserve_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(vecHotKey);  //计算字段vecHotKey的长度 size_of(Vector)
				length += 1;  //计算字段vecHotKey_u的长度 size_of(uint8_t)
				length += 4;  //计算字段shopTypeLastUpdTime的长度 size_of(uint32_t)
				length += 4;  //计算字段shopTypeUpdateTimes的长度 size_of(uint32_t)
				length += 1;  //计算字段shopTypeLastUpdTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段shopTypeUpdateTimes_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
