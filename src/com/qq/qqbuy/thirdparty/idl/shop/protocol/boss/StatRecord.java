package com.qq.qqbuy.thirdparty.idl.shop.protocol.boss;

import com.paipai.util.io.ByteStream;

public class StatRecord {
	private long uin; // 用户ID
	private short bizType; // 统计项所属业务模块（交易统计1 送礼许愿2）
	private int statType; // 统计项标识
	private int statData; // 统计项数值
	private int latestUpdateTime; // 当前统计项的最后更新时间
	private byte state; // 统计项的状态

	public StatRecord() {
		this.uin = 0;
		this.bizType = 0;
		this.statType = 0;
		this.statData = 0;
		this.latestUpdateTime = 0;
		this.state = 0;
	}

	public int Serialize(ByteStream bs) throws Exception {
		bs.pushUInt(uin);
		bs.pushShort(bizType);
		bs.pushInt(statType);
		bs.pushInt(statData);
		bs.pushInt(latestUpdateTime);
		bs.pushByte(state);
		return bs.getWrittenLength();
	}

	public int UnSerialize(ByteStream bs) throws Exception {
		this.uin = bs.popInt();
		this.bizType = bs.popShort();
		this.statType = bs.popInt();
		this.statData = bs.popInt();
		this.latestUpdateTime = bs.popInt();
		this.state = bs.popByte();
		return bs.getReadLength();
	}

	public long getUin() {
		return uin;
	}

	public void setUin(long uin) {
		this.uin = uin;
	}

	public short getBizType() {
		return bizType;
	}

	public void setBizType(short bizType) {
		this.bizType = bizType;
	}

	public int getStatType() {
		return statType;
	}

	public void setStatType(int statType) {
		this.statType = statType;
	}

	public int getStatData() {
		return statData;
	}

	public void setStatData(int statData) {
		this.statData = statData;
	}

	public int getLatestUpdateTime() {
		return latestUpdateTime;
	}

	public void setLatestUpdateTime(int latestUpdateTime) {
		this.latestUpdateTime = latestUpdateTime;
	}

	public byte getState() {
		return state;
	}

	public void setState(byte state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "StatRecord [bizType=" + bizType + ", latestUpdateTime="
				+ latestUpdateTime + ", statData=" + statData + ", statType="
				+ statType + ", state=" + state + ", uin=" + uin + "]";
	}

}
