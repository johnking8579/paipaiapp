/*     */ package com.qq.qqbuy.thirdparty.idl.shop.protocol;
/*     */ 
/*     */ import com.paipai.netframework.kernal.NetMessage;
/*     */ import com.paipai.util.io.ByteStream;
/*     */ import java.util.Vector;
/*     */ 
/*     */ public class ApiGetShopCategoryListByUinResp extends NetMessage
/*     */ {
/*  27 */   private Vector<ApiShopCategory> apiShopCategoryList = new Vector();
/*     */   private long totalSize;
/*  41 */   private String errmsg = new String();
/*     */ 
/*  48 */   private String outReserve = new String();
/*     */ 
/*     */   public int serialize(ByteStream bs)
/*     */     throws Exception
/*     */   {
/*  54 */     bs.pushUInt(this.result);
/*  55 */     bs.pushObject(this.apiShopCategoryList);
/*  56 */     bs.pushUInt(this.totalSize);
/*  57 */     bs.pushString(this.errmsg);
/*  58 */     bs.pushString(this.outReserve);
/*  59 */     return bs.getWrittenLength();
/*     */   }
/*     */ 
/*     */   public int unSerialize(ByteStream bs)
/*     */     throws Exception
/*     */   {
/*  65 */     this.result = bs.popUInt();
/*  66 */     this.apiShopCategoryList = (Vector<ApiShopCategory>) bs.popVector(ApiShopCategory.class);
/*  67 */     this.totalSize = bs.popUInt();
/*  68 */     this.errmsg = bs.popString();
/*  69 */     this.outReserve = bs.popString();
/*  70 */     return bs.getReadLength();
/*     */   }
/*     */ 
/*     */   public long getCmdId()
/*     */   {
/*  75 */     return 0x20038801L;
/*     */   }
/*     */ 
/*     */   public Vector<ApiShopCategory> getApiShopCategoryList()
/*     */   {
/*  88 */     return this.apiShopCategoryList;
/*     */   }
/*     */ 
/*     */   public void setApiShopCategoryList(Vector<ApiShopCategory> value)
/*     */   {
/* 101 */     if (value != null)
/* 102 */       this.apiShopCategoryList = value;
/*     */     else
/* 104 */       this.apiShopCategoryList = new Vector();
/*     */   }
/*     */ 
/*     */   public long getTotalSize()
/*     */   {
/* 118 */     return this.totalSize;
/*     */   }
/*     */ 
/*     */   public void setTotalSize(long value)
/*     */   {
/* 131 */     this.totalSize = value;
/*     */   }
/*     */ 
/*     */   public String getErrmsg()
/*     */   {
/* 144 */     return this.errmsg;
/*     */   }
/*     */ 
/*     */   public void setErrmsg(String value)
/*     */   {
/* 157 */     if (value != null)
/* 158 */       this.errmsg = value;
/*     */     else
/* 160 */       this.errmsg = new String();
/*     */   }
/*     */ 
/*     */   public String getOutReserve()
/*     */   {
/* 174 */     return this.outReserve;
/*     */   }
/*     */ 
/*     */   public void setOutReserve(String value)
/*     */   {
/* 187 */     if (value != null)
/* 188 */       this.outReserve = value;
/*     */     else
/* 190 */       this.outReserve = new String();
/*     */   }
/*     */ 
/*     */   protected int getClassSize()
/*     */   {
/* 197 */     return getSize() - 4;
/*     */   }
/*     */ 
/*     */   public int getSize()
/*     */   {
/* 203 */     int length = 4;
/*     */     try {
/* 205 */       length = 4;
/* 206 */       length += ByteStream.getObjectSize(this.apiShopCategoryList);
/* 207 */       length += 4;
/* 208 */       length += ByteStream.getObjectSize(this.errmsg);
/* 209 */       length += ByteStream.getObjectSize(this.outReserve);
/*     */     } catch (Exception e) {
/* 211 */       e.printStackTrace();
/*     */     }
/* 213 */     return length;
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.shop.ao.protocol.ApiGetShopCategoryListByUinResp
 * JD-Core Version:    0.6.2
 */