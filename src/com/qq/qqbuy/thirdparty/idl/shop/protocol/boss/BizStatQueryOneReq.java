package com.qq.qqbuy.thirdparty.idl.shop.protocol.boss;

import java.util.ArrayList;
import java.util.List;

import com.paipai.util.io.ByteStream;
import com.qq.qqbuy.thirdparty.idl.boss.IBossServiceObject;

/**
 * @author raywu
 * 
 */
public class BizStatQueryOneReq implements IBossServiceObject {
	// 统计业务定义
	public final static int BIZSTAT_BIZTYPE_DEAL = 1; // 交易相关统计
	public final static int BIZSTAT_BIZTYPE_WISH = 2; // 送礼许愿相关统计
	public final static int BIZSTAT_BIZTYPE_MSGBRD = 3; // 留言相关统计
	public final static int BIZSTAT_BIZTYPE_EVAL = 4; // 评价相关统计
	public final static int BIZSTAT_BIZTYPE_COMM_HIS = 5; // 商品违规计数
	public final static int BIZSTAT_BIZTYPE_CREDIT = 6; // 买卖家信用
	public final static int BIZSTAT_BIZTYPE_PPMSG = 7; // 站内信统计
	public final static int BIZSTAT_BIZTYPE_DWSELLER = 8; // 数据仓库导出的卖家信息
	public final static int BIZSTAT_BIZTYPE_PUNISH = 9; // 处罚状态
	public final static int BIZSTAT_BIZTYPE_SHOP_RECMD = 10; // 店铺推荐数据
	public final static int BIZSTAT_BIZTYPE_QQMAIL = 11; // QQMail邮件订阅分群数据
	public final static int BIZSTAT_BIZTYPE_SELLERGRADE_BASE      = 12;  //卖家成绩表（最近周期） - 基本信息
	public final static int BIZSTAT_BIZTYPE_SELLERGRADE_CLASS     = 13; //卖家成绩表 - 类目信息
	public final static int BIZSTAT_BIZTYPE_SELLERGRADE_BASE1     = 14;  //卖家成绩表（上个周期） - 基本信息
	public final static int BIZSTAT_BIZTYPE_DEAL2		= 15;	//交易相关统计(c2c2.0)
	public final static int BIZSTAT_BIZTYPE_PPVIP		= 16;	// 客服vip系统
	public final static int BIZSTAT_BIZTYPE_EVAL2		= 17;	//评价相关统计2
	public final static int BIZSTAT_BIZTYPE_EVAL3		= 18;	//评价相关统计3(买卖家做出的评价)
	public final static int BIZSTAT_BIZTYPE_EVAL4       = 19;   //评价相关统计4(评价管理页)

	// 统计项定义
	// 交易统计
	public final static int BIZSTAT_STATTYPE_DEAL_BUYER_WAIT4REFUND = 1; // (买家)买家已申请退款，等待卖家响应退款请求
	// 13/8/15
	public final static int BIZSTAT_STATTYPE_DEAL_BUYER_WAIT4CONSIGNMENT = 2; // (买家)买家已经付款，等待卖家发货的交易
	// 5
	public final static int BIZSTAT_STATTYPE_DEAL_BUYER_WAIT4PAY = 3; // (买家)等待买家付款和买家付款中的交易
	// 3/4
	public final static int BIZSTAT_STATTYPE_DEAL_BUYER_WAIT4PAYCONFIRM = 4; // (买家)卖家已发货，等待买家收货的交易
	// 6/17/18
	public final static int BIZSTAT_STATTYPE_DEAL_BUYER_PAY_3M = 5; // (买家)下单时间在三个月内的已付款数(直接统计)
	public final static int BIZSTAT_STATTYPE_DEAL_SELLER_WAIT4REFUND = 11; // (卖家)买家已申请退款，等待卖家响应退款请求
	// 13/8/15
	public final static int BIZSTAT_STATTYPE_DEAL_SELLER_WAIT4CONSIGNMENT = 12; // (卖家)买家已经付款，等待卖家发货的交易
	// 5
	public final static int BIZSTAT_STATTYPE_DEAL_SELLER_WAIT4PAY = 13; // (卖家)等待买家付款和买家付款中的交易
	// 3/4
	public final static int BIZSTAT_STATTYPE_DEAL_SELLER_WAIT4PAYCONFIRM = 14; // (卖家)卖家已发货，等待买家收货的交易
	// 6/17/18
	public final static int BIZSTAT_STATTYPE_DEAL_SELLER_PAY_3M = 15; // (卖家)下单时间在三个月内的已付款数(直接统计)

	// 送礼统计
	public final static int BIZSTAT_STATTYPE_SEND_CNT = 1; // 已送礼数
	public final static int BIZSTAT_STATTYPE_RECV_CNT = 2; // 已收礼数

	// 留言统计
	public final static int BIZSTAT_STATTYPE_COMM_BUYER_REPLY = 1; // (商品留言)(买家)被回复的留言数
	public final static int BIZSTAT_STATTYPE_COMM_BUYER_NOTREPLY = 2; // (商品留言)(买家)未被回复的留言数
	public final static int BIZSTAT_STATTYPE_COMM_SELLER_REPLY = 3; // (商品留言)(卖家)回复的留言数
	public final static int BIZSTAT_STATTYPE_COMM_SELLER_NOTREPLY = 4; // (商品留言)(卖家)未回复的留言数
	public final static int BIZSTAT_STATTYPE_QSHOP_BUYER_REPLY = 11; // (店铺留言)(买家)被回复的留言数
	public final static int BIZSTAT_STATTYPE_QSHOP_BUYER_NOTREPLY = 12; // (店铺留言)(买家)未被回复的留言数
	public final static int BIZSTAT_STATTYPE_QSHOP_SELLER_REPLY = 13; // (店铺留言)(卖家)回复的留言数
	public final static int BIZSTAT_STATTYPE_QSHOP_SELLER_NOTREPLY = 14; // (店铺留言)(卖家)未回复的留言数

	// 评价统计
	public final static int BIZSTAT_STATTYPE_EVAL_BUYER_WAIT_BUYER = 1; // (作为买家)等待买家评价数
	public final static int BIZSTAT_STATTYPE_EVAL_BUYER_WAIT_SELLER = 2; // (作为买家)等待卖家评价数
	public final static int BIZSTAT_STATTYPE_EVAL_SELLER_WAIT_BUYER = 3; // (作为卖家)等待买家评价数
	public final static int BIZSTAT_STATTYPE_EVAL_SELLER_WAIT_SELLER = 4; // (作为卖家)等待卖家评价数

	public final static int BIZSTAT_STATTYPE_EVAL_SELLER_GOOD_EVAL_ALL = 5; // (作为卖家)所有好评数
	public final static int BIZSTAT_STATTYPE_EVAL_SELLER_NORMAL_EVAL_ALL = 6; // (作为卖家)所有中评数
	public final static int BIZSTAT_STATTYPE_EVAL_SELLER_BAD_EVAL_ALL = 7; // (作为卖家)所有差评数
	public final static int BIZSTAT_STATTYPE_EVAL_SELLER_GOOD_EVAL_6M = 8; // (作为卖家)六个月好评数
	public final static int BIZSTAT_STATTYPE_EVAL_SELLER_NORMAL_EVAL_6M = 9; // (作为卖家)六个月中评数
	public final static int BIZSTAT_STATTYPE_EVAL_SELLER_BAD_EVAL_6M = 10; // (作为卖家)六个月差评数
	public final static int BIZSTAT_STATTYPE_EVAL_SELLER_GOOD_EVAL_1M = 11; // (作为卖家)一个月好评数
	public final static int BIZSTAT_STATTYPE_EVAL_SELLER_NORMAL_EVAL_1M = 12; // (作为卖家)一个月中评数
	public final static int BIZSTAT_STATTYPE_EVAL_SELLER_BAD_EVAL_1M = 13; // (作为卖家)一个月差评数
	public final static int BIZSTAT_STATTYPE_EVAL_SELLER_GOOD_EVAL_1W = 14; // (作为卖家)一周好评数
	public final static int BIZSTAT_STATTYPE_EVAL_SELLER_NORMAL_EVAL_1W = 15; // (作为卖家)一周中评数
	public final static int BIZSTAT_STATTYPE_EVAL_SELLER_BAD_EVAL_1W = 16; // (作为卖家)一周差评数

	public final static int BIZSTAT_STATTYPE_EVAL_BUYER_GOOD_EVAL_ALL = 17; // (作为买家)所有好评数
	public final static int BIZSTAT_STATTYPE_EVAL_BUYER_NORMAL_EVAL_ALL = 18; // (作为买家)所有中评数
	public final static int BIZSTAT_STATTYPE_EVAL_BUYER_BAD_EVAL_ALL = 19; // (作为买家)所有差评数
	public final static int BIZSTAT_STATTYPE_EVAL_BUYER_GOOD_EVAL_6M = 20; // (作为买家)六个月好评数
	public final static int BIZSTAT_STATTYPE_EVAL_BUYER_NORMAL_EVAL_6M = 21; // (作为买家)六个月中评数
	public final static int BIZSTAT_STATTYPE_EVAL_BUYER_BAD_EVAL_6M = 22; // (作为买家)六个月差评数
	public final static int BIZSTAT_STATTYPE_EVAL_BUYER_GOOD_EVAL_1M = 23; // (作为买家)一个月好评数
	public final static int BIZSTAT_STATTYPE_EVAL_BUYER_NORMAL_EVAL_1M = 24; // (作为买家)一个月中评数
	public final static int BIZSTAT_STATTYPE_EVAL_BUYER_BAD_EVAL_1M = 25; // (作为买家)一个月差评数
	public final static int BIZSTAT_STATTYPE_EVAL_BUYER_GOOD_EVAL_1W = 26; // (作为买家)一周好评数
	public final static int BIZSTAT_STATTYPE_EVAL_BUYER_NORMAL_EVAL_1W = 27; // (作为买家)一周中评数
	public final static int BIZSTAT_STATTYPE_EVAL_BUYER_BAD_EVAL_1W = 28; // (作为买家)一周差评数

	// ---------------卖家成绩表（最近周期） - 基本信息 12 
	// -- 基本信息
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_SHOPTYPE    		= 1;   //主营类目
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_GUARANTY    		= 2;   //是否诚保
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_BEGINDATE    		= 3;   //统计开始时间
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_ENDDATE    		= 4;   //统计结束时间
	// -- 1、可信度
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_RELIABILITY    	= 5;   //可信度分数
	// -- 2、交易业绩
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_TRADE    			= 6;   //交易业绩分数
	// -- 3、买家满意度
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_SATISFACTION    		= 7;   //买家满意度分数
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_SATISFACTION_VALID    	= 8;   //买家满意度是否无效：0有效，1无效
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_SATISFACTION_LAWSUIT    	= 9;   //投诉笔数
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_SATISFACTION_LAWSUIT_RATE    	= 10;   //投诉率分数
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_SATISFACTION_RATE1  		= 11;   //描述相符
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_SATISFACTION_RATE2    	= 12;   //买卖沟通
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_SATISFACTION_RATE3    	= 13;   //货物运送
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_SATISFACTION_REFUND    	= 14;   //退款次数
	// -- 4、政策遵守度
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_DISCIPLINE    	= 15;   //政策遵守度分数
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_SUPPORT_SHOPTYPE    	= 16;   //主营类目支持卖家成绩表，1支持，0不支持
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_LEVEL_RELIABILITY    	= 17;   //可信度分数级别：1优秀，2良好，3一般，4需提升
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_LEVEL_TRADE  		= 18;   //交易业绩分数级别：1优秀，2良好，3一般，4需提升
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_LEVEL_SATISFACTION    = 19;   //买家满意度分数级别：1优秀，2良好，3一般，4需提升
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_LEVEL_DISCIPLINE    	= 20;   //政策遵守度分数级别：1优秀，2良好，3一般，4需提升

	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_CHANGE_RELIABILITY    = 21;   //可信度分数较上周期变化： 0 未变化 1 上升 2下降
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_CHANGE_TRADE    	= 22;   //交易业绩分数较上周期变化： 0 未变化 1 上升 2下降
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_CHANGE_SATISFACTION   = 23;   //买家满意度分数较上周期变化： 0 未变化 1 上升 2下降
	public final static int BIZSTAT_STATTYPE_SELLERGRADE_BASE_CHANGE_DISCIPLINE    	= 24;   //政策遵守度分数较上周期变化： 0 未变化 1 上升 2下降

	// 商品违规计数
	public final static int BIZSTAT_STATTYPE_COMM_HIS_CNT_3M = 1; // 三个月的违规商品个数(不去重)

	// 买卖家信用
	public final static int BIZSTAT_STATTYPE_BUYERCREDIT = 1; // 买家信用
	public final static int BIZSTAT_STATTYPE_SELLERCREDIT = 2; // 卖家信用

	// 站内信
	public final static int BIZSTAT_STATTYPE_PPMSG_NEWMSG = 1; // 未读站内信数
	public final static int BIZSTAT_STATTYPE_PPMSG_ALLMSG = 2; // 总的站内信数

	// 数据仓库导出的卖家数据
	public final static int BIZSTAT_STATTYPE_DWSELLER_ONLINE_COMMCNT = 1; // 在线商品数
	public final static int BIZSTAT_STATTYPE_DWSELLER_GOOD_EVAL_CNT = 2; // 好评数
	public final static int BIZSTAT_STATTYPE_DWSELLER_NORMAL_EVAL_CNT = 3; // 中评数
	public final static int BIZSTAT_STATTYPE_DWSELLER_BAD_EVAL_CNT = 4; // 差评数
	public final static int BIZSTAT_STATTYPE_DWSELLER_MAJORCLASS_ONLINE_COMMCNT = 5; // 主营类目在线商品数
	public final static int BIZSTAT_STATTYPE_DWSELLER_PUNISHCNT = 6; // 处罚数
	public final static int BIZSTAT_STATTYPE_DWSELLER_LAWSUITCNT = 7; // 投诉数
	public final static int BIZSTAT_STATTYPE_DWSELLER_SHOP_REGTIME = 8; // 开店时间

	// 数据仓库导出的卖家处罚状态 9
	public final static int BIZSTAT_STATTYPE_PUNISH_DONGJIE = 1; // 冻结
	public final static int BIZSTAT_STATTYPE_PUNISH_JINGGAO = 4; // 警告
	public final static int BIZSTAT_STATTYPE_PUNISH_JINZHI = 6; // 禁止
	public final static int BIZSTAT_STATTYPE_PUNISH_CHAKAN = 8; // 查看
	public final static int BIZSTAT_STATTYPE_PUNISH_DONGJIEDIANPU = 14; // 冻结店铺

	// 店铺推荐数据
	public final static int BIZSTAT_STATTYPE_SHOP_RECMD_SHOPTYPE = 1; // 主营类目
	public final static int BIZSTAT_STATTYPE_SHOP_RECMD_PAYCNT_SHOPTYPE = 2; // 30天内主营类目付款笔数
	public final static int BIZSTAT_STATTYPE_SHOP_RECMD_PAYCNT_ALL = 3; // 30天内店铺付款笔数
	public final static int BIZSTAT_STATTYPE_SHOP_RECMD_PAYMONEY_ALL = 4; // 30天内店铺付款金额
	public final static int BIZSTAT_STATTYPE_SHOP_RECMD_LAWSUIT_ALL = 5; // 30天内店铺卖家责任投诉数

	// 邮件订阅数据
	public final static int BIZSTAT_STATTYPE_QQMAIL_SHOP_BOUGHT_BEFORE = 10000005; // 购买过的店铺的最近更新

	// 定义STATTYPE中标识用户的bitmask
	public final static int BIZSTAT_BIZTYPE_DEAL_USRMASK_BUYER = 0X00000000; // 最高位为0
	public final static int BIZSTAT_BIZTYPE_DEAL_USRMASK_SELLER = 0X80000000; // 最高位为1

	// 定义返回值编码
	public final static int BIZSTAT_SUCC = 0; // 操作成功
	public final static int BIZSTAT_ERR_FAIL = 1; // 找不到符合条件的记录、操作失败
	public final static int BIZSTAT_ERR_DISABLED = 2; // 业务被禁用
	public final static int BIZSTAT_ERR_HASHTABLE = 3; // HASHTABLE打开失败
	public final static int BIZSTAT_ERR_LOCK = 4; // 加锁失败
	public final static int BIZSTAT_ERR_PARMS = 5; // 参数错误
	public final static int BIZSTAT_ERR_SYS = 6; // 系统初始化错误
	
	
	

	private long cmdId = 0x000000a8;
	private String cmdDesc = "bizstat_sellergrade";
	private long subCmdId = 0;

	private int bizType; // <业务模块ID
	private List<Long> uins = new ArrayList<Long>(); // <批量查询的号码

	public int Serialize(ByteStream bs) throws Exception {
		bs.pushUShort(bizType);
		bs.pushUInt(uins.size());
		for (Long uin : uins) {
			bs.pushUInt(uin);
		}
		return bs.getWrittenLength();
	}

	public void addUin(long uin) {
		this.uins.add(uin);
	}

	public void setBizType(int bizType) {
		this.bizType = bizType;
	}

	public String getCmdDesc() {
		return cmdDesc;
	}

	public long getSubCmdId() {
		return subCmdId;
	}

	public int UnSerialize(ByteStream bs) throws Exception {
		return bs.getReadLength();
	}

	public long getCmdId() {
		return cmdId;
	}

	public void setHasResult(boolean arg0) {
	}

	public void setCmdDesc(String cmdDesc) {
		this.cmdDesc = cmdDesc;
	}
}
