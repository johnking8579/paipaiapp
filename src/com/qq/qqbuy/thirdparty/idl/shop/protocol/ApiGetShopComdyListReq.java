 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.shop.ao.idl.ShopApiAo.java

package com.qq.qqbuy.thirdparty.idl.shop.protocol;


import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

/**
 *获取商品信息列表请求类
 *
 *@date 2013-03-07 02:28:56
 *
 *@since version:0
*/
public class  ApiGetShopComdyListReq implements IServiceObject
{
	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 来源,必填
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 场景id
	 *
	 * 版本 >= 0
	 */
	 private long scene;

	/**
	 * 店铺id
	 *
	 * 版本 >= 0
	 */
	 private long shopID;

	/**
	 * 过滤器(设置商品搜索条件)
	 *
	 * 版本 >= 0
	 */
	 private ApiItemFilter apiItemFilter = new ApiItemFilter();

	/**
	 * 请求保留字
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(machineKey);
		bs.pushString(source);
		bs.pushUInt(scene);
		bs.pushUInt(shopID);
		bs.pushObject(apiItemFilter);
		bs.pushString(inReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		machineKey = bs.popString();
		source = bs.popString();
		scene = bs.popUInt();
		shopID = bs.popUInt();
		apiItemFilter = (ApiItemFilter)bs.popObject(ApiItemFilter.class);
		inReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x3014180DL;
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取来源,必填
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置来源,必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取场景id
	 * 
	 * 此字段的版本 >= 0
	 * @return scene value 类型为:long
	 * 
	 */
	public long getScene()
	{
		return scene;
	}


	/**
	 * 设置场景id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setScene(long value)
	{
		this.scene = value;
	}


	/**
	 * 获取店铺id
	 * 
	 * 此字段的版本 >= 0
	 * @return shopID value 类型为:long
	 * 
	 */
	public long getShopID()
	{
		return shopID;
	}


	/**
	 * 设置店铺id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setShopID(long value)
	{
		this.shopID = value;
	}


	/**
	 * 获取过滤器(设置商品搜索条件)
	 * 
	 * 此字段的版本 >= 0
	 * @return apiItemFilter value 类型为:ApiItemFilter
	 * 
	 */
	public ApiItemFilter getApiItemFilter()
	{
		return apiItemFilter;
	}


	/**
	 * 设置过滤器(设置商品搜索条件)
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ApiItemFilter
	 * 
	 */
	public void setApiItemFilter(ApiItemFilter value)
	{
		if (value != null) {
				this.apiItemFilter = value;
		}else{
				this.apiItemFilter = new ApiItemFilter();
		}
	}


	/**
	 * 获取请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(ApiGetShopComdyListReq)
				length += ByteStream.getObjectSize(machineKey);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += 4;  //计算字段scene的长度 size_of(uint32_t)
				length += 4;  //计算字段shopID的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(apiItemFilter);  //计算字段apiItemFilter的长度 size_of(ApiItemFilter)
				length += ByteStream.getObjectSize(inReserve);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
