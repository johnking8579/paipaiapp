//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.item.ao.idl.ItemApiAo.java

package com.qq.qqbuy.thirdparty.idl.shop.protocol;


import java.util.HashMap;
import java.util.Map;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *过滤器，详细使用见{@link com.paipai.c2c.item.constants.CVItemFilterConstants}
 *
 *@date 2013-03-07 02:29:22
 *
 *@since version:0
*/
public class ApiItemFilter  implements ICanSerializeObject
{
	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private short version = 1;

	/**
	 * 卖家QQ
	 *
	 * 版本 >= 0
	 */
	 private long sellerUin;

	/**
	 * 每页大小,最多50
	 *
	 * 版本 >= 0
	 */
	 private long pageSize;

	/**
	 * 页号,从0开始
	 *
	 * 版本 >= 0
	 */
	 private long startIndex;

	/**
	 * 排序方式,使用说明见{@link com.paipai.c2c.item.constants.CVItemFilterConstants}
	 *
	 * 版本 >= 0
	 */
	 private long orderType;

	/**
	 * 过滤map，使用说明见{@link com.paipai.c2c.item.constants.CVItemFilterConstants}
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> filterMap = new HashMap<String,String>();

	/**
	 * 过滤map属性,用于是否需要免登录操作等特殊需求
	 *
	 * 版本 >= 0
	 */
	 private long property;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUByte(version);
		bs.pushUInt(sellerUin);
		bs.pushUInt(pageSize);
		bs.pushUInt(startIndex);
		bs.pushUInt(orderType);
		bs.pushObject(filterMap);
		bs.pushUInt(property);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUByte();
		sellerUin = bs.popUInt();
		pageSize = bs.popUInt();
		startIndex = bs.popUInt();
		orderType = bs.popUInt();
		filterMap = (Map<String,String>)bs.popMap(String.class,String.class);
		property = bs.popUInt();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:short
	 * 
	 */
	public short getVersion()
	{
		return version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion(short value)
	{
		this.version = value;
	}


	/**
	 * 获取卖家QQ
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return sellerUin;
	}


	/**
	 * 设置卖家QQ
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.sellerUin = value;
	}


	/**
	 * 获取每页大小,最多50
	 * 
	 * 此字段的版本 >= 0
	 * @return pageSize value 类型为:long
	 * 
	 */
	public long getPageSize()
	{
		return pageSize;
	}


	/**
	 * 设置每页大小,最多50
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPageSize(long value)
	{
		this.pageSize = value;
	}


	/**
	 * 获取页号,从0开始
	 * 
	 * 此字段的版本 >= 0
	 * @return startIndex value 类型为:long
	 * 
	 */
	public long getStartIndex()
	{
		return startIndex;
	}


	/**
	 * 设置页号,从0开始
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setStartIndex(long value)
	{
		this.startIndex = value;
	}


	/**
	 * 获取排序方式,使用说明见{@link com.paipai.c2c.item.constants.CVItemFilterConstants}
	 * 
	 * 此字段的版本 >= 0
	 * @return orderType value 类型为:long
	 * 
	 */
	public long getOrderType()
	{
		return orderType;
	}


	/**
	 * 设置排序方式,使用说明见{@link com.paipai.c2c.item.constants.CVItemFilterConstants}
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOrderType(long value)
	{
		this.orderType = value;
	}


	/**
	 * 获取过滤map，使用说明见{@link com.paipai.c2c.item.constants.CVItemFilterConstants}
	 * 
	 * 此字段的版本 >= 0
	 * @return filterMap value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getFilterMap()
	{
		return filterMap;
	}


	/**
	 * 设置过滤map，使用说明见{@link com.paipai.c2c.item.constants.CVItemFilterConstants}
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setFilterMap(Map<String,String> value)
	{
		if (value != null) {
				this.filterMap = value;
		}else{
				this.filterMap = new HashMap<String,String>();
		}
	}


	/**
	 * 获取过滤map属性,用于是否需要免登录操作等特殊需求
	 * 
	 * 此字段的版本 >= 0
	 * @return property value 类型为:long
	 * 
	 */
	public long getProperty()
	{
		return property;
	}


	/**
	 * 设置过滤map属性,用于是否需要免登录操作等特殊需求
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setProperty(long value)
	{
		this.property = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ApiItemFilter)
				length += 1;  //计算字段version的长度 size_of(uint8_t)
				length += 4;  //计算字段sellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段pageSize的长度 size_of(uint32_t)
				length += 4;  //计算字段startIndex的长度 size_of(uint32_t)
				length += 4;  //计算字段orderType的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(filterMap,"GBK");  //计算字段filterMap的长度 size_of(Map)
				length += 4;  //计算字段property的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
