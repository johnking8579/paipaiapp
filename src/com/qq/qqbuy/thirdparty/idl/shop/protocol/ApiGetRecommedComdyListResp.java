/*     */ package com.qq.qqbuy.thirdparty.idl.shop.protocol;
/*     */ 
/*     */ /*     */ import java.util.Vector;

/*     */ import com.paipai.netframework.kernal.NetMessage;
/*     */ import com.paipai.util.io.ByteStream;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ApiItem;
/*     */ 
/*     */ public class ApiGetRecommedComdyListResp extends NetMessage
/*     */ {
/*  28 */   private Vector<ApiItem> recommedComdyInfoList = new Vector<ApiItem>();
/*     */ 
/*  35 */   private String errmsg = new String();
/*     */ 
/*  42 */   private String outReserve = new String();
/*     */ 
/*     */   public int serialize(ByteStream bs)
/*     */     throws Exception
/*     */   {
/*  48 */     bs.pushUInt(this.result);
/*  49 */     bs.pushObject(this.recommedComdyInfoList);
/*  50 */     bs.pushString(this.errmsg);
/*  51 */     bs.pushString(this.outReserve);
/*  52 */     return bs.getWrittenLength();
/*     */   }
/*     */ 
/*     */   public int unSerialize(ByteStream bs)
/*     */     throws Exception
/*     */   {
/*  58 */     this.result = bs.popUInt();
/*  59 */     this.recommedComdyInfoList = (Vector<ApiItem>) bs.popVector(ApiItem.class);
/*  60 */     this.errmsg = bs.popString();
/*  61 */     this.outReserve = bs.popString();
/*  62 */     return bs.getReadLength();
/*     */   }
/*     */ 
/*     */   public long getCmdId()
/*     */   {
/*  67 */     return 806651910L;
/*     */   }
/*     */ 
/*     */   public Vector<ApiItem> getRecommedComdyInfoList()
/*     */   {
/*  80 */     return this.recommedComdyInfoList;
/*     */   }
/*     */ 
/*     */   public void setRecommedComdyInfoList(Vector<ApiItem> value)
/*     */   {
/*  93 */     if (value != null)
/*  94 */       this.recommedComdyInfoList = value;
/*     */     else
/*  96 */       this.recommedComdyInfoList = new Vector();
/*     */   }
/*     */ 
/*     */   public String getErrmsg()
/*     */   {
/* 110 */     return this.errmsg;
/*     */   }
/*     */ 
/*     */   public void setErrmsg(String value)
/*     */   {
/* 123 */     if (value != null)
/* 124 */       this.errmsg = value;
/*     */     else
/* 126 */       this.errmsg = new String();
/*     */   }
/*     */ 
/*     */   public String getOutReserve()
/*     */   {
/* 140 */     return this.outReserve;
/*     */   }
/*     */ 
/*     */   public void setOutReserve(String value)
/*     */   {
/* 153 */     if (value != null)
/* 154 */       this.outReserve = value;
/*     */     else
/* 156 */       this.outReserve = new String();
/*     */   }
/*     */ 
/*     */   protected int getClassSize()
/*     */   {
/* 163 */     return getSize() - 4;
/*     */   }
/*     */ 
/*     */   public int getSize()
/*     */   {
/* 169 */     int length = 4;
/*     */     try {
/* 171 */       length = 4;
/* 172 */       length += ByteStream.getObjectSize(this.recommedComdyInfoList);
/* 173 */       length += ByteStream.getObjectSize(this.errmsg);
/* 174 */       length += ByteStream.getObjectSize(this.outReserve);
/*     */     } catch (Exception e) {
/* 176 */       e.printStackTrace();
/*     */     }
/* 178 */     return length;
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.shop.ao.protocol.ApiGetRecommedComdyListResp
 * JD-Core Version:    0.6.2
 */