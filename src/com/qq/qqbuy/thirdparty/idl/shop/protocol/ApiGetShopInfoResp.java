 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.shop.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *获取店铺信息返回类
 *
 *@date 2013-08-22 03:13::02
 *
 *@since version:0
*/
public class  ApiGetShopInfoResp implements IServiceObject
{
	public long result;
	/**
	 * 店铺信息
	 *
	 * 版本 >= 0
	 */
	 private ApiShopInfo shopInfo = new ApiShopInfo();

	/**
	 * 错误信息,用于debug
	 *
	 * 版本 >= 0
	 */
	 private String errmsg = new String();

	/**
	 * 返回保留字
	 *
	 * 版本 >= 0
	 */
	 private String outReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(shopInfo);
		bs.pushString(errmsg);
		bs.pushString(outReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		shopInfo = (ApiShopInfo) bs.popObject(ApiShopInfo.class);
		errmsg = bs.popString();
		outReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x30148801L;
	}


	/**
	 * 获取店铺信息
	 * 
	 * 此字段的版本 >= 0
	 * @return shopInfo value 类型为:ApiShopInfo
	 * 
	 */
	public ApiShopInfo getShopInfo()
	{
		return shopInfo;
	}


	/**
	 * 设置店铺信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ApiShopInfo
	 * 
	 */
	public void setShopInfo(ApiShopInfo value)
	{
		if (value != null) {
				this.shopInfo = value;
		}else{
				this.shopInfo = new ApiShopInfo();
		}
	}


	/**
	 * 获取错误信息,用于debug
	 * 
	 * 此字段的版本 >= 0
	 * @return errmsg value 类型为:String
	 * 
	 */
	public String getErrmsg()
	{
		return errmsg;
	}


	/**
	 * 设置错误信息,用于debug
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrmsg(String value)
	{
		if (value != null) {
				this.errmsg = value;
		}else{
				this.errmsg = new String();
		}
	}


	/**
	 * 获取返回保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return outReserve;
	}


	/**
	 * 设置返回保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		if (value != null) {
				this.outReserve = value;
		}else{
				this.outReserve = new String();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ApiGetShopInfoResp)
				length += ByteStream.getObjectSize(shopInfo);  //计算字段shopInfo的长度 size_of(ApiShopInfo)
				length += ByteStream.getObjectSize(errmsg);  //计算字段errmsg的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserve);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
