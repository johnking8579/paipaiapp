package com.qq.qqbuy.thirdparty.idl.selfmarketactive;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.qq.qqbuy.common.Log;

import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.selfmarketactive.protocol.SelfMarketForQueryReq;
import com.qq.qqbuy.thirdparty.idl.selfmarketactive.protocol.SelfMarketForQueryResp;
import com.qq.qqbuy.thirdparty.idl.selfmarketactive.protocol.SelfMarketReqBo;
import com.qq.qqbuy.thirdparty.idl.stat.protocol.GetFavoritesByIdReq;
import com.qq.qqbuy.thirdparty.idl.stat.protocol.GetFavoritesByIdResp;

public class SelfMarketActiveClient extends SupportIDLBaseClient {
	
    public static SelfMarketForQueryResp selfMarketForQuery(Long sellerUin){
        SelfMarketForQueryReq req = new SelfMarketForQueryReq();
        SelfMarketForQueryResp resp = new SelfMarketForQueryResp();
        SelfMarketReqBo selfMarketReqBo = new SelfMarketReqBo() ;
        selfMarketReqBo.setUin(sellerUin) ; 
        req.setSelfMarketReq(selfMarketReqBo);
        int ret = 0;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setRouteKey(System.currentTimeMillis()) ;
        try {
			ret = stub.invoke(req, resp);
		} catch (Exception e) {
			Log.run.warn("e", e);
			ret =  ERRCODE_CALLIDL_FAIL;  
		}
        return ret == SUCCESS ? resp : null;
    }    

	
}
