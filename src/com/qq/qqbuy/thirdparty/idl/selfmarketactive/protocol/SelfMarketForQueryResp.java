 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.SelfMarketActive.ao.idl.SelfMarkContentOrderAo.java

package com.qq.qqbuy.thirdparty.idl.selfmarketactive.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *优惠活动响应
 *
 *@date 2015-01-04 04:14:42
 *
 *@since version:0
*/
public class  SelfMarketForQueryResp implements IServiceObject
{
	public long result;
	/**
	 * 返回结果
	 *
	 * 版本 >= 0
	 */
	 private SelfMarketActiveForOrderRespBo SelfMarketActiveForOrderResp = new SelfMarketActiveForOrderRespBo();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(SelfMarketActiveForOrderResp);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		SelfMarketActiveForOrderResp = (SelfMarketActiveForOrderRespBo)bs.popObject(SelfMarketActiveForOrderRespBo.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x53908801L;
	}


	/**
	 * 获取返回结果
	 * 
	 * 此字段的版本 >= 0
	 * @return SelfMarketActiveForOrderResp value 类型为:SelfMarketActiveForOrderRespBo
	 * 
	 */
	public SelfMarketActiveForOrderRespBo getSelfMarketActiveForOrderResp()
	{
		return SelfMarketActiveForOrderResp;
	}


	/**
	 * 设置返回结果
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:SelfMarketActiveForOrderRespBo
	 * 
	 */
	public void setSelfMarketActiveForOrderResp(SelfMarketActiveForOrderRespBo value)
	{
		if (value != null) {
				this.SelfMarketActiveForOrderResp = value;
		}else{
				this.SelfMarketActiveForOrderResp = new SelfMarketActiveForOrderRespBo();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SelfMarketForQueryResp)
				length += ByteStream.getObjectSize(SelfMarketActiveForOrderResp, null);  //计算字段SelfMarketActiveForOrderResp的长度 size_of(SelfMarketActiveForOrderRespBo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(SelfMarketForQueryResp)
				length += ByteStream.getObjectSize(SelfMarketActiveForOrderResp, encoding);  //计算字段SelfMarketActiveForOrderResp的长度 size_of(SelfMarketActiveForOrderRespBo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
