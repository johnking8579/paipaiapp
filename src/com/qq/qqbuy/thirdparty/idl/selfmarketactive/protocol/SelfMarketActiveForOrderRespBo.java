//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.SelfMarketActive.ao.idl.SelfMarketForQueryResp.java

package com.qq.qqbuy.thirdparty.idl.selfmarketactive.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *响应参数
 *
 *@date 2015-01-04 04:14:42
 *
 *@since version:0
*/
public class SelfMarketActiveForOrderRespBo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private int version = 1;

	/**
	 * QQ号码
	 *
	 * 版本 >= 0
	 */
	 private long Uin;

	/**
	 * 版本 >= 0
	 */
	 private SelfMarketActiveOrderBo active = new SelfMarketActiveOrderBo();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUShort(version);
		bs.pushUInt(Uin);
		bs.pushObject(active);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUShort();
		Uin = bs.popUInt();
		active = (SelfMarketActiveOrderBo) bs.popObject(SelfMarketActiveOrderBo.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取QQ号码
	 * 
	 * 此字段的版本 >= 0
	 * @return Uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return Uin;
	}


	/**
	 * 设置QQ号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.Uin = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return active value 类型为:SelfMarketActiveOrderBo
	 * 
	 */
	public SelfMarketActiveOrderBo getActive()
	{
		return active;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:SelfMarketActiveOrderBo
	 * 
	 */
	public void setActive(SelfMarketActiveOrderBo value)
	{
		if (value != null) {
				this.active = value;
		}else{
				this.active = new SelfMarketActiveOrderBo();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SelfMarketActiveForOrderRespBo)
				length += 2;  //计算字段version的长度 size_of(uint16_t)
				length += 4;  //计算字段Uin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(active, null);  //计算字段active的长度 size_of(SelfMarketActiveOrderBo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(SelfMarketActiveForOrderRespBo)
				length += 2;  //计算字段version的长度 size_of(uint16_t)
				length += 4;  //计算字段Uin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(active, encoding);  //计算字段active的长度 size_of(SelfMarketActiveOrderBo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
