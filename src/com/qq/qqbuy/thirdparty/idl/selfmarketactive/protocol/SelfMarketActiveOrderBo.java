//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.SelfMarketActive.bo.idl.SelfMarketActiveForOrderRespBo.java

package com.qq.qqbuy.thirdparty.idl.selfmarketactive.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *中间过程参数
 *
 *@date 2015-01-04 04:14:42
 *
 *@since version:0
*/
public class SelfMarketActiveOrderBo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private int version = 1;

	/**
	 * 活动id
	 *
	 * 版本 >= 0
	 */
	 private long activeID;

	/**
	 * QQ号码
	 *
	 * 版本 >= 0
	 */
	 private long Uin;

	/**
	 * 活动开始时间
	 *
	 * 版本 >= 0
	 */
	 private String beginTime = new String();

	/**
	 * 活动结束时间
	 *
	 * 版本 >= 0
	 */
	 private String endTime = new String();

	/**
	 * 商品分类
	 *
	 * 版本 >= 0
	 */
	 private String commClassify = new String();

	/**
	 * 活动描述
	 *
	 * 版本 >= 0
	 */
	 private String activeDesc = new String();

	/**
	 * 活动状态
	 *
	 * 版本 >= 0
	 */
	 private long state;

	/**
	 * 是否有被同步到商城
	 *
	 * 版本 >= 0
	 */
	 private long syncFlag;

	/**
	 * 创建标识
	 *
	 * 版本 >= 0
	 */
	 private long createFlag;

	/**
	 * 活动属性
	 *
	 * 版本 >= 0
	 */
	 private long activeProperty;

	/**
	 * 库存属性
	 *
	 * 版本 >= 0
	 */
	 private String property = new String();

	/**
	 * 商品维度请求的优惠列表,如红包等
	 *
	 * 版本 >= 0
	 */
	 private Vector<SelfMarkContentOrderBo> vecContent = new Vector<SelfMarkContentOrderBo>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUShort(version);
		bs.pushUInt(activeID);
		bs.pushUInt(Uin);
		bs.pushString(beginTime);
		bs.pushString(endTime);
		bs.pushString(commClassify);
		bs.pushString(activeDesc);
		bs.pushUInt(state);
		bs.pushUInt(syncFlag);
		bs.pushUInt(createFlag);
		bs.pushUInt(activeProperty);
		bs.pushString(property);
		bs.pushObject(vecContent);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUShort();
		activeID = bs.popUInt();
		Uin = bs.popUInt();
		beginTime = bs.popString();
		endTime = bs.popString();
		commClassify = bs.popString();
		activeDesc = bs.popString();
		state = bs.popUInt();
		syncFlag = bs.popUInt();
		createFlag = bs.popUInt();
		activeProperty = bs.popUInt();
		property = bs.popString();
		vecContent = (Vector<SelfMarkContentOrderBo>)bs.popVector(SelfMarkContentOrderBo.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取活动id
	 * 
	 * 此字段的版本 >= 0
	 * @return activeID value 类型为:long
	 * 
	 */
	public long getActiveID()
	{
		return activeID;
	}


	/**
	 * 设置活动id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActiveID(long value)
	{
		this.activeID = value;
	}


	/**
	 * 获取QQ号码
	 * 
	 * 此字段的版本 >= 0
	 * @return Uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return Uin;
	}


	/**
	 * 设置QQ号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.Uin = value;
	}


	/**
	 * 获取活动开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @return beginTime value 类型为:String
	 * 
	 */
	public String getBeginTime()
	{
		return beginTime;
	}


	/**
	 * 设置活动开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBeginTime(String value)
	{
		this.beginTime = value;
	}


	/**
	 * 获取活动结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return endTime value 类型为:String
	 * 
	 */
	public String getEndTime()
	{
		return endTime;
	}


	/**
	 * 设置活动结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setEndTime(String value)
	{
		this.endTime = value;
	}


	/**
	 * 获取商品分类
	 * 
	 * 此字段的版本 >= 0
	 * @return commClassify value 类型为:String
	 * 
	 */
	public String getCommClassify()
	{
		return commClassify;
	}


	/**
	 * 设置商品分类
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCommClassify(String value)
	{
		this.commClassify = value;
	}


	/**
	 * 获取活动描述
	 * 
	 * 此字段的版本 >= 0
	 * @return activeDesc value 类型为:String
	 * 
	 */
	public String getActiveDesc()
	{
		return activeDesc;
	}


	/**
	 * 设置活动描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setActiveDesc(String value)
	{
		this.activeDesc = value;
	}


	/**
	 * 获取活动状态
	 * 
	 * 此字段的版本 >= 0
	 * @return state value 类型为:long
	 * 
	 */
	public long getState()
	{
		return state;
	}


	/**
	 * 设置活动状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setState(long value)
	{
		this.state = value;
	}


	/**
	 * 获取是否有被同步到商城
	 * 
	 * 此字段的版本 >= 0
	 * @return syncFlag value 类型为:long
	 * 
	 */
	public long getSyncFlag()
	{
		return syncFlag;
	}


	/**
	 * 设置是否有被同步到商城
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSyncFlag(long value)
	{
		this.syncFlag = value;
	}


	/**
	 * 获取创建标识
	 * 
	 * 此字段的版本 >= 0
	 * @return createFlag value 类型为:long
	 * 
	 */
	public long getCreateFlag()
	{
		return createFlag;
	}


	/**
	 * 设置创建标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCreateFlag(long value)
	{
		this.createFlag = value;
	}


	/**
	 * 获取活动属性
	 * 
	 * 此字段的版本 >= 0
	 * @return activeProperty value 类型为:long
	 * 
	 */
	public long getActiveProperty()
	{
		return activeProperty;
	}


	/**
	 * 设置活动属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActiveProperty(long value)
	{
		this.activeProperty = value;
	}


	/**
	 * 获取库存属性
	 * 
	 * 此字段的版本 >= 0
	 * @return property value 类型为:String
	 * 
	 */
	public String getProperty()
	{
		return property;
	}


	/**
	 * 设置库存属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setProperty(String value)
	{
		this.property = value;
	}


	/**
	 * 获取商品维度请求的优惠列表,如红包等
	 * 
	 * 此字段的版本 >= 0
	 * @return vecContent value 类型为:Vector<SelfMarkContentOrderBo>
	 * 
	 */
	public Vector<SelfMarkContentOrderBo> getVecContent()
	{
		return vecContent;
	}


	/**
	 * 设置商品维度请求的优惠列表,如红包等
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<SelfMarkContentOrderBo>
	 * 
	 */
	public void setVecContent(Vector<SelfMarkContentOrderBo> value)
	{
		if (value != null) {
				this.vecContent = value;
		}else{
				this.vecContent = new Vector<SelfMarkContentOrderBo>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SelfMarketActiveOrderBo)
				length += 2;  //计算字段version的长度 size_of(uint16_t)
				length += 4;  //计算字段activeID的长度 size_of(uint32_t)
				length += 4;  //计算字段Uin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(beginTime, null);  //计算字段beginTime的长度 size_of(String)
				length += ByteStream.getObjectSize(endTime, null);  //计算字段endTime的长度 size_of(String)
				length += ByteStream.getObjectSize(commClassify, null);  //计算字段commClassify的长度 size_of(String)
				length += ByteStream.getObjectSize(activeDesc, null);  //计算字段activeDesc的长度 size_of(String)
				length += 4;  //计算字段state的长度 size_of(uint32_t)
				length += 4;  //计算字段syncFlag的长度 size_of(uint32_t)
				length += 4;  //计算字段createFlag的长度 size_of(uint32_t)
				length += 4;  //计算字段activeProperty的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(property, null);  //计算字段property的长度 size_of(String)
				length += ByteStream.getObjectSize(vecContent, null);  //计算字段vecContent的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(SelfMarketActiveOrderBo)
				length += 2;  //计算字段version的长度 size_of(uint16_t)
				length += 4;  //计算字段activeID的长度 size_of(uint32_t)
				length += 4;  //计算字段Uin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(beginTime, encoding);  //计算字段beginTime的长度 size_of(String)
				length += ByteStream.getObjectSize(endTime, encoding);  //计算字段endTime的长度 size_of(String)
				length += ByteStream.getObjectSize(commClassify, encoding);  //计算字段commClassify的长度 size_of(String)
				length += ByteStream.getObjectSize(activeDesc, encoding);  //计算字段activeDesc的长度 size_of(String)
				length += 4;  //计算字段state的长度 size_of(uint32_t)
				length += 4;  //计算字段syncFlag的长度 size_of(uint32_t)
				length += 4;  //计算字段createFlag的长度 size_of(uint32_t)
				length += 4;  //计算字段activeProperty的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(property, encoding);  //计算字段property的长度 size_of(String)
				length += ByteStream.getObjectSize(vecContent, encoding);  //计算字段vecContent的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
