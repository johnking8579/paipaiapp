 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.SelfMarketActive.ao.idl.SelfMarkContentOrderAo.java

package com.qq.qqbuy.thirdparty.idl.selfmarketactive.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *优惠活动请求
 *
 *@date 2015-01-04 04:14:42
 *
 *@since version:0
*/
public class  SelfMarketForQueryReq implements IServiceObject
{
	/**
	 * 请求参数
	 *
	 * 版本 >= 0
	 */
	 private SelfMarketReqBo SelfMarketReq = new SelfMarketReqBo();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(SelfMarketReq);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		SelfMarketReq = (SelfMarketReqBo)bs.popObject(SelfMarketReqBo.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x53901801L;
	}


	/**
	 * 获取请求参数
	 * 
	 * 此字段的版本 >= 0
	 * @return SelfMarketReq value 类型为:SelfMarketReqBo
	 * 
	 */
	public SelfMarketReqBo getSelfMarketReq()
	{
		return SelfMarketReq;
	}


	/**
	 * 设置请求参数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:SelfMarketReqBo
	 * 
	 */
	public void setSelfMarketReq(SelfMarketReqBo value)
	{
		if (value != null) {
				this.SelfMarketReq = value;
		}else{
				this.SelfMarketReq = new SelfMarketReqBo();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(SelfMarketForQueryReq)
				length += ByteStream.getObjectSize(SelfMarketReq, null);  //计算字段SelfMarketReq的长度 size_of(SelfMarketReqBo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(SelfMarketForQueryReq)
				length += ByteStream.getObjectSize(SelfMarketReq, encoding);  //计算字段SelfMarketReq的长度 size_of(SelfMarketReqBo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
