//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.SelfMarketActive.bo.idl.SelfMarketActiveOrderBo.java

package com.qq.qqbuy.thirdparty.idl.selfmarketactive.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *CSelfMarkContentOrder结构体Bo
 *
 *@date 2015-01-04 04:14:42
 *
 *@since version:0
*/
public class SelfMarkContentOrderBo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private int version = 1;

	/**
	 * 内容id
	 *
	 * 版本 >= 0
	 */
	 private long contentID;

	/**
	 * 活动id
	 *
	 * 版本 >= 0
	 */
	 private long activeID;

	/**
	 * QQ号码
	 *
	 * 版本 >= 0
	 */
	 private long Uin;

	/**
	 * 消费标记
	 *
	 * 版本 >= 0
	 */
	 private long costFlag;

	/**
	 * 消费金额
	 *
	 * 版本 >= 0
	 */
	 private long costMoney;

	/**
	 * 优惠标记
	 *
	 * 版本 >= 0
	 */
	 private long favorableFlag;

	/**
	 * 减免金额
	 *
	 * 版本 >= 0
	 */
	 private long freeMoney;

	/**
	 * 折扣率
	 *
	 * 版本 >= 0
	 */
	 private long freeRebate;

	/**
	 * 赠品名称
	 *
	 * 版本 >= 0
	 */
	 private String presentName = new String();

	/**
	 * 赠品地址
	 *
	 * 版本 >= 0
	 */
	 private String presentUrl = new String();

	/**
	 * 换购金额
	 *
	 * 版本 >= 0
	 */
	 private long barterMoney;

	/**
	 * 换购商品名称
	 *
	 * 版本 >= 0
	 */
	 private String barterName = new String();

	/**
	 * 换购商品地址
	 *
	 * 版本 >= 0
	 */
	 private String barterUrl = new String();

	/**
	 * 内容描述
	 *
	 * 版本 >= 0
	 */
	 private String contentDesc = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUShort(version);
		bs.pushUInt(contentID);
		bs.pushUInt(activeID);
		bs.pushUInt(Uin);
		bs.pushUInt(costFlag);
		bs.pushUInt(costMoney);
		bs.pushUInt(favorableFlag);
		bs.pushUInt(freeMoney);
		bs.pushUInt(freeRebate);
		bs.pushString(presentName);
		bs.pushString(presentUrl);
		bs.pushUInt(barterMoney);
		bs.pushString(barterName);
		bs.pushString(barterUrl);
		bs.pushString(contentDesc);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUShort();
		contentID = bs.popUInt();
		activeID = bs.popUInt();
		Uin = bs.popUInt();
		costFlag = bs.popUInt();
		costMoney = bs.popUInt();
		favorableFlag = bs.popUInt();
		freeMoney = bs.popUInt();
		freeRebate = bs.popUInt();
		presentName = bs.popString();
		presentUrl = bs.popString();
		barterMoney = bs.popUInt();
		barterName = bs.popString();
		barterUrl = bs.popString();
		contentDesc = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取内容id
	 * 
	 * 此字段的版本 >= 0
	 * @return contentID value 类型为:long
	 * 
	 */
	public long getContentID()
	{
		return contentID;
	}


	/**
	 * 设置内容id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setContentID(long value)
	{
		this.contentID = value;
	}


	/**
	 * 获取活动id
	 * 
	 * 此字段的版本 >= 0
	 * @return activeID value 类型为:long
	 * 
	 */
	public long getActiveID()
	{
		return activeID;
	}


	/**
	 * 设置活动id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActiveID(long value)
	{
		this.activeID = value;
	}


	/**
	 * 获取QQ号码
	 * 
	 * 此字段的版本 >= 0
	 * @return Uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return Uin;
	}


	/**
	 * 设置QQ号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.Uin = value;
	}


	/**
	 * 获取消费标记
	 * 
	 * 此字段的版本 >= 0
	 * @return costFlag value 类型为:long
	 * 
	 */
	public long getCostFlag()
	{
		return costFlag;
	}


	/**
	 * 设置消费标记
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCostFlag(long value)
	{
		this.costFlag = value;
	}


	/**
	 * 获取消费金额
	 * 
	 * 此字段的版本 >= 0
	 * @return costMoney value 类型为:long
	 * 
	 */
	public long getCostMoney()
	{
		return costMoney;
	}


	/**
	 * 设置消费金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCostMoney(long value)
	{
		this.costMoney = value;
	}


	/**
	 * 获取优惠标记
	 * 
	 * 此字段的版本 >= 0
	 * @return favorableFlag value 类型为:long
	 * 
	 */
	public long getFavorableFlag()
	{
		return favorableFlag;
	}


	/**
	 * 设置优惠标记
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFavorableFlag(long value)
	{
		this.favorableFlag = value;
	}


	/**
	 * 获取减免金额
	 * 
	 * 此字段的版本 >= 0
	 * @return freeMoney value 类型为:long
	 * 
	 */
	public long getFreeMoney()
	{
		return freeMoney;
	}


	/**
	 * 设置减免金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFreeMoney(long value)
	{
		this.freeMoney = value;
	}


	/**
	 * 获取折扣率
	 * 
	 * 此字段的版本 >= 0
	 * @return freeRebate value 类型为:long
	 * 
	 */
	public long getFreeRebate()
	{
		return freeRebate;
	}


	/**
	 * 设置折扣率
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFreeRebate(long value)
	{
		this.freeRebate = value;
	}


	/**
	 * 获取赠品名称
	 * 
	 * 此字段的版本 >= 0
	 * @return presentName value 类型为:String
	 * 
	 */
	public String getPresentName()
	{
		return presentName;
	}


	/**
	 * 设置赠品名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPresentName(String value)
	{
		this.presentName = value;
	}


	/**
	 * 获取赠品地址
	 * 
	 * 此字段的版本 >= 0
	 * @return presentUrl value 类型为:String
	 * 
	 */
	public String getPresentUrl()
	{
		return presentUrl;
	}


	/**
	 * 设置赠品地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPresentUrl(String value)
	{
		this.presentUrl = value;
	}


	/**
	 * 获取换购金额
	 * 
	 * 此字段的版本 >= 0
	 * @return barterMoney value 类型为:long
	 * 
	 */
	public long getBarterMoney()
	{
		return barterMoney;
	}


	/**
	 * 设置换购金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBarterMoney(long value)
	{
		this.barterMoney = value;
	}


	/**
	 * 获取换购商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @return barterName value 类型为:String
	 * 
	 */
	public String getBarterName()
	{
		return barterName;
	}


	/**
	 * 设置换购商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBarterName(String value)
	{
		this.barterName = value;
	}


	/**
	 * 获取换购商品地址
	 * 
	 * 此字段的版本 >= 0
	 * @return barterUrl value 类型为:String
	 * 
	 */
	public String getBarterUrl()
	{
		return barterUrl;
	}


	/**
	 * 设置换购商品地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBarterUrl(String value)
	{
		this.barterUrl = value;
	}


	/**
	 * 获取内容描述
	 * 
	 * 此字段的版本 >= 0
	 * @return contentDesc value 类型为:String
	 * 
	 */
	public String getContentDesc()
	{
		return contentDesc;
	}


	/**
	 * 设置内容描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setContentDesc(String value)
	{
		this.contentDesc = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SelfMarkContentOrderBo)
				length += 2;  //计算字段version的长度 size_of(uint16_t)
				length += 4;  //计算字段contentID的长度 size_of(uint32_t)
				length += 4;  //计算字段activeID的长度 size_of(uint32_t)
				length += 4;  //计算字段Uin的长度 size_of(uint32_t)
				length += 4;  //计算字段costFlag的长度 size_of(uint32_t)
				length += 4;  //计算字段costMoney的长度 size_of(uint32_t)
				length += 4;  //计算字段favorableFlag的长度 size_of(uint32_t)
				length += 4;  //计算字段freeMoney的长度 size_of(uint32_t)
				length += 4;  //计算字段freeRebate的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(presentName, null);  //计算字段presentName的长度 size_of(String)
				length += ByteStream.getObjectSize(presentUrl, null);  //计算字段presentUrl的长度 size_of(String)
				length += 4;  //计算字段barterMoney的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(barterName, null);  //计算字段barterName的长度 size_of(String)
				length += ByteStream.getObjectSize(barterUrl, null);  //计算字段barterUrl的长度 size_of(String)
				length += ByteStream.getObjectSize(contentDesc, null);  //计算字段contentDesc的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(SelfMarkContentOrderBo)
				length += 2;  //计算字段version的长度 size_of(uint16_t)
				length += 4;  //计算字段contentID的长度 size_of(uint32_t)
				length += 4;  //计算字段activeID的长度 size_of(uint32_t)
				length += 4;  //计算字段Uin的长度 size_of(uint32_t)
				length += 4;  //计算字段costFlag的长度 size_of(uint32_t)
				length += 4;  //计算字段costMoney的长度 size_of(uint32_t)
				length += 4;  //计算字段favorableFlag的长度 size_of(uint32_t)
				length += 4;  //计算字段freeMoney的长度 size_of(uint32_t)
				length += 4;  //计算字段freeRebate的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(presentName, encoding);  //计算字段presentName的长度 size_of(String)
				length += ByteStream.getObjectSize(presentUrl, encoding);  //计算字段presentUrl的长度 size_of(String)
				length += 4;  //计算字段barterMoney的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(barterName, encoding);  //计算字段barterName的长度 size_of(String)
				length += ByteStream.getObjectSize(barterUrl, encoding);  //计算字段barterUrl的长度 size_of(String)
				length += ByteStream.getObjectSize(contentDesc, encoding);  //计算字段contentDesc的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
