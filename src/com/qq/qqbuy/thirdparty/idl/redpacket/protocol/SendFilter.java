//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.redpacketapi.java

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *发放红包Filter
 *
 *@date 2014-10-16 05:56:15
 *
 *@since version:0
*/
public class SendFilter  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 业务类型
	 *
	 * 版本 >= 0
	 */
	 private long BusinessType;

	/**
	 * 版本 >= 0
	 */
	 private short BusinessType_u;

	/**
	 * 发放请求列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<SendPacketInfo> PacketListInfo = new Vector<SendPacketInfo>();

	/**
	 * 版本 >= 0
	 */
	 private short PacketListInfo_u;

	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(BusinessType);
		bs.pushUByte(BusinessType_u);
		bs.pushObject(PacketListInfo);
		bs.pushUByte(PacketListInfo_u);
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		BusinessType = bs.popUInt();
		BusinessType_u = bs.popUByte();
		PacketListInfo = (Vector<SendPacketInfo>)bs.popVector(SendPacketInfo.class);
		PacketListInfo_u = bs.popUByte();
		version = bs.popUInt();
		version_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取业务类型
	 * 
	 * 此字段的版本 >= 0
	 * @return BusinessType value 类型为:long
	 * 
	 */
	public long getBusinessType()
	{
		return BusinessType;
	}


	/**
	 * 设置业务类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBusinessType(long value)
	{
		this.BusinessType = value;
		this.BusinessType_u = 1;
	}

	public boolean issetBusinessType()
	{
		return this.BusinessType_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BusinessType_u value 类型为:short
	 * 
	 */
	public short getBusinessType_u()
	{
		return BusinessType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBusinessType_u(short value)
	{
		this.BusinessType_u = value;
	}


	/**
	 * 获取发放请求列表
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketListInfo value 类型为:Vector<SendPacketInfo>
	 * 
	 */
	public Vector<SendPacketInfo> getPacketListInfo()
	{
		return PacketListInfo;
	}


	/**
	 * 设置发放请求列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<SendPacketInfo>
	 * 
	 */
	public void setPacketListInfo(Vector<SendPacketInfo> value)
	{
		if (value != null) {
				this.PacketListInfo = value;
				this.PacketListInfo_u = 1;
		}
	}

	public boolean issetPacketListInfo()
	{
		return this.PacketListInfo_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketListInfo_u value 类型为:short
	 * 
	 */
	public short getPacketListInfo_u()
	{
		return PacketListInfo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketListInfo_u(short value)
	{
		this.PacketListInfo_u = value;
	}


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SendFilter)
				length += 4;  //计算字段BusinessType的长度 size_of(uint32_t)
				length += 1;  //计算字段BusinessType_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(PacketListInfo, null);  //计算字段PacketListInfo的长度 size_of(Vector)
				length += 1;  //计算字段PacketListInfo_u的长度 size_of(uint8_t)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(SendFilter)
				length += 4;  //计算字段BusinessType的长度 size_of(uint32_t)
				length += 1;  //计算字段BusinessType_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(PacketListInfo, encoding);  //计算字段PacketListInfo的长度 size_of(Vector)
				length += 1;  //计算字段PacketListInfo_u的长度 size_of(uint8_t)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
