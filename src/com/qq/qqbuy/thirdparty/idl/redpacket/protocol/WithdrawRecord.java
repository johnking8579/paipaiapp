//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.PacketAndStockList.java

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *红包兑现记录
 *
 *@date 2014-10-16 05:56:15
 *
 *@since version:0
*/
public class WithdrawRecord  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 兑现编号id
	 *
	 * 版本 >= 0
	 */
	 private long WithdrawId;

	/**
	 * 版本 >= 0
	 */
	 private short WithdrawId_u;

	/**
	 * 财付通转账包序列编号id
	 *
	 * 版本 >= 0
	 */
	 private String CftPackageId = new String();

	/**
	 * 版本 >= 0
	 */
	 private short CftPackageId_u;

	/**
	 * 卖家uin
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * 版本 >= 0
	 */
	 private short SellerUin_u;

	/**
	 * 兑现金额
	 *
	 * 版本 >= 0
	 */
	 private long TotalAmount;

	/**
	 * 版本 >= 0
	 */
	 private short TotalAmount_u;

	/**
	 * 兑现财付通时间
	 *
	 * 版本 >= 0
	 */
	 private long WithdrawCftTime;

	/**
	 * 版本 >= 0
	 */
	 private short WithdrawCftTime_u;

	/**
	 * 兑现时间
	 *
	 * 版本 >= 0
	 */
	 private long WithdrawTime;

	/**
	 * 版本 >= 0
	 */
	 private short WithdrawTime_u;

	/**
	 * 申请兑现时间
	 *
	 * 版本 >= 0
	 */
	 private long ApplyTime;

	/**
	 * 版本 >= 0
	 */
	 private short ApplyTime_u;

	/**
	 * 兑现状态
	 *
	 * 版本 >= 0
	 */
	 private long State;

	/**
	 * 版本 >= 0
	 */
	 private short State_u;

	/**
	 * 兑现ip地址
	 *
	 * 版本 >= 0
	 */
	 private String WithdrawIp = new String();

	/**
	 * 版本 >= 0
	 */
	 private short WithdrawIp_u;

	/**
	 * 兑现机器码
	 *
	 * 版本 >= 0
	 */
	 private String WithdrawMachineKey = new String();

	/**
	 * 版本 >= 0
	 */
	 private short WithdrawMachineKey_u;

	/**
	 * 最后更新时间
	 *
	 * 版本 >= 0
	 */
	 private long LastUpdateTime;

	/**
	 * 版本 >= 0
	 */
	 private short LastUpdateTime_u;

	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushLong(WithdrawId);
		bs.pushUByte(WithdrawId_u);
		bs.pushString(CftPackageId);
		bs.pushUByte(CftPackageId_u);
		bs.pushUInt(SellerUin);
		bs.pushUByte(SellerUin_u);
		bs.pushUInt(TotalAmount);
		bs.pushUByte(TotalAmount_u);
		bs.pushUInt(WithdrawCftTime);
		bs.pushUByte(WithdrawCftTime_u);
		bs.pushUInt(WithdrawTime);
		bs.pushUByte(WithdrawTime_u);
		bs.pushUInt(ApplyTime);
		bs.pushUByte(ApplyTime_u);
		bs.pushUInt(State);
		bs.pushUByte(State_u);
		bs.pushString(WithdrawIp);
		bs.pushUByte(WithdrawIp_u);
		bs.pushString(WithdrawMachineKey);
		bs.pushUByte(WithdrawMachineKey_u);
		bs.pushUInt(LastUpdateTime);
		bs.pushUByte(LastUpdateTime_u);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		WithdrawId = bs.popLong();
		WithdrawId_u = bs.popUByte();
		CftPackageId = bs.popString();
		CftPackageId_u = bs.popUByte();
		SellerUin = bs.popUInt();
		SellerUin_u = bs.popUByte();
		TotalAmount = bs.popUInt();
		TotalAmount_u = bs.popUByte();
		WithdrawCftTime = bs.popUInt();
		WithdrawCftTime_u = bs.popUByte();
		WithdrawTime = bs.popUInt();
		WithdrawTime_u = bs.popUByte();
		ApplyTime = bs.popUInt();
		ApplyTime_u = bs.popUByte();
		State = bs.popUInt();
		State_u = bs.popUByte();
		WithdrawIp = bs.popString();
		WithdrawIp_u = bs.popUByte();
		WithdrawMachineKey = bs.popString();
		WithdrawMachineKey_u = bs.popUByte();
		LastUpdateTime = bs.popUInt();
		LastUpdateTime_u = bs.popUByte();
		Version = bs.popUInt();
		Version_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取兑现编号id
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawId value 类型为:long
	 * 
	 */
	public long getWithdrawId()
	{
		return WithdrawId;
	}


	/**
	 * 设置兑现编号id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWithdrawId(long value)
	{
		this.WithdrawId = value;
		this.WithdrawId_u = 1;
	}

	public boolean issetWithdrawId()
	{
		return this.WithdrawId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawId_u value 类型为:short
	 * 
	 */
	public short getWithdrawId_u()
	{
		return WithdrawId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWithdrawId_u(short value)
	{
		this.WithdrawId_u = value;
	}


	/**
	 * 获取财付通转账包序列编号id
	 * 
	 * 此字段的版本 >= 0
	 * @return CftPackageId value 类型为:String
	 * 
	 */
	public String getCftPackageId()
	{
		return CftPackageId;
	}


	/**
	 * 设置财付通转账包序列编号id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCftPackageId(String value)
	{
		this.CftPackageId = value;
		this.CftPackageId_u = 1;
	}

	public boolean issetCftPackageId()
	{
		return this.CftPackageId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return CftPackageId_u value 类型为:short
	 * 
	 */
	public short getCftPackageId_u()
	{
		return CftPackageId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCftPackageId_u(short value)
	{
		this.CftPackageId_u = value;
	}


	/**
	 * 获取卖家uin
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置卖家uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
		this.SellerUin_u = 1;
	}

	public boolean issetSellerUin()
	{
		return this.SellerUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin_u value 类型为:short
	 * 
	 */
	public short getSellerUin_u()
	{
		return SellerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerUin_u(short value)
	{
		this.SellerUin_u = value;
	}


	/**
	 * 获取兑现金额
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalAmount value 类型为:long
	 * 
	 */
	public long getTotalAmount()
	{
		return TotalAmount;
	}


	/**
	 * 设置兑现金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalAmount(long value)
	{
		this.TotalAmount = value;
		this.TotalAmount_u = 1;
	}

	public boolean issetTotalAmount()
	{
		return this.TotalAmount_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalAmount_u value 类型为:short
	 * 
	 */
	public short getTotalAmount_u()
	{
		return TotalAmount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTotalAmount_u(short value)
	{
		this.TotalAmount_u = value;
	}


	/**
	 * 获取兑现财付通时间
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawCftTime value 类型为:long
	 * 
	 */
	public long getWithdrawCftTime()
	{
		return WithdrawCftTime;
	}


	/**
	 * 设置兑现财付通时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWithdrawCftTime(long value)
	{
		this.WithdrawCftTime = value;
		this.WithdrawCftTime_u = 1;
	}

	public boolean issetWithdrawCftTime()
	{
		return this.WithdrawCftTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawCftTime_u value 类型为:short
	 * 
	 */
	public short getWithdrawCftTime_u()
	{
		return WithdrawCftTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWithdrawCftTime_u(short value)
	{
		this.WithdrawCftTime_u = value;
	}


	/**
	 * 获取兑现时间
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawTime value 类型为:long
	 * 
	 */
	public long getWithdrawTime()
	{
		return WithdrawTime;
	}


	/**
	 * 设置兑现时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWithdrawTime(long value)
	{
		this.WithdrawTime = value;
		this.WithdrawTime_u = 1;
	}

	public boolean issetWithdrawTime()
	{
		return this.WithdrawTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawTime_u value 类型为:short
	 * 
	 */
	public short getWithdrawTime_u()
	{
		return WithdrawTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWithdrawTime_u(short value)
	{
		this.WithdrawTime_u = value;
	}


	/**
	 * 获取申请兑现时间
	 * 
	 * 此字段的版本 >= 0
	 * @return ApplyTime value 类型为:long
	 * 
	 */
	public long getApplyTime()
	{
		return ApplyTime;
	}


	/**
	 * 设置申请兑现时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setApplyTime(long value)
	{
		this.ApplyTime = value;
		this.ApplyTime_u = 1;
	}

	public boolean issetApplyTime()
	{
		return this.ApplyTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ApplyTime_u value 类型为:short
	 * 
	 */
	public short getApplyTime_u()
	{
		return ApplyTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setApplyTime_u(short value)
	{
		this.ApplyTime_u = value;
	}


	/**
	 * 获取兑现状态
	 * 
	 * 此字段的版本 >= 0
	 * @return State value 类型为:long
	 * 
	 */
	public long getState()
	{
		return State;
	}


	/**
	 * 设置兑现状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setState(long value)
	{
		this.State = value;
		this.State_u = 1;
	}

	public boolean issetState()
	{
		return this.State_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return State_u value 类型为:short
	 * 
	 */
	public short getState_u()
	{
		return State_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setState_u(short value)
	{
		this.State_u = value;
	}


	/**
	 * 获取兑现ip地址
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawIp value 类型为:String
	 * 
	 */
	public String getWithdrawIp()
	{
		return WithdrawIp;
	}


	/**
	 * 设置兑现ip地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setWithdrawIp(String value)
	{
		this.WithdrawIp = value;
		this.WithdrawIp_u = 1;
	}

	public boolean issetWithdrawIp()
	{
		return this.WithdrawIp_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawIp_u value 类型为:short
	 * 
	 */
	public short getWithdrawIp_u()
	{
		return WithdrawIp_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWithdrawIp_u(short value)
	{
		this.WithdrawIp_u = value;
	}


	/**
	 * 获取兑现机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawMachineKey value 类型为:String
	 * 
	 */
	public String getWithdrawMachineKey()
	{
		return WithdrawMachineKey;
	}


	/**
	 * 设置兑现机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setWithdrawMachineKey(String value)
	{
		this.WithdrawMachineKey = value;
		this.WithdrawMachineKey_u = 1;
	}

	public boolean issetWithdrawMachineKey()
	{
		return this.WithdrawMachineKey_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawMachineKey_u value 类型为:short
	 * 
	 */
	public short getWithdrawMachineKey_u()
	{
		return WithdrawMachineKey_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWithdrawMachineKey_u(short value)
	{
		this.WithdrawMachineKey_u = value;
	}


	/**
	 * 获取最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return LastUpdateTime value 类型为:long
	 * 
	 */
	public long getLastUpdateTime()
	{
		return LastUpdateTime;
	}


	/**
	 * 设置最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastUpdateTime(long value)
	{
		this.LastUpdateTime = value;
		this.LastUpdateTime_u = 1;
	}

	public boolean issetLastUpdateTime()
	{
		return this.LastUpdateTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return LastUpdateTime_u value 类型为:short
	 * 
	 */
	public short getLastUpdateTime_u()
	{
		return LastUpdateTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastUpdateTime_u(short value)
	{
		this.LastUpdateTime_u = value;
	}


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(WithdrawRecord)
				length += 17;  //计算字段WithdrawId的长度 size_of(uint64_t)
				length += 1;  //计算字段WithdrawId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(CftPackageId, null);  //计算字段CftPackageId的长度 size_of(String)
				length += 1;  //计算字段CftPackageId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TotalAmount的长度 size_of(uint32_t)
				length += 1;  //计算字段TotalAmount_u的长度 size_of(uint8_t)
				length += 4;  //计算字段WithdrawCftTime的长度 size_of(uint32_t)
				length += 1;  //计算字段WithdrawCftTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段WithdrawTime的长度 size_of(uint32_t)
				length += 1;  //计算字段WithdrawTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ApplyTime的长度 size_of(uint32_t)
				length += 1;  //计算字段ApplyTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段State的长度 size_of(uint32_t)
				length += 1;  //计算字段State_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(WithdrawIp, null);  //计算字段WithdrawIp的长度 size_of(String)
				length += 1;  //计算字段WithdrawIp_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(WithdrawMachineKey, null);  //计算字段WithdrawMachineKey的长度 size_of(String)
				length += 1;  //计算字段WithdrawMachineKey_u的长度 size_of(uint8_t)
				length += 4;  //计算字段LastUpdateTime的长度 size_of(uint32_t)
				length += 1;  //计算字段LastUpdateTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(WithdrawRecord)
				length += 17;  //计算字段WithdrawId的长度 size_of(uint64_t)
				length += 1;  //计算字段WithdrawId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(CftPackageId, encoding);  //计算字段CftPackageId的长度 size_of(String)
				length += 1;  //计算字段CftPackageId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TotalAmount的长度 size_of(uint32_t)
				length += 1;  //计算字段TotalAmount_u的长度 size_of(uint8_t)
				length += 4;  //计算字段WithdrawCftTime的长度 size_of(uint32_t)
				length += 1;  //计算字段WithdrawCftTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段WithdrawTime的长度 size_of(uint32_t)
				length += 1;  //计算字段WithdrawTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ApplyTime的长度 size_of(uint32_t)
				length += 1;  //计算字段ApplyTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段State的长度 size_of(uint32_t)
				length += 1;  //计算字段State_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(WithdrawIp, encoding);  //计算字段WithdrawIp的长度 size_of(String)
				length += 1;  //计算字段WithdrawIp_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(WithdrawMachineKey, encoding);  //计算字段WithdrawMachineKey的长度 size_of(String)
				length += 1;  //计算字段WithdrawMachineKey_u的长度 size_of(uint8_t)
				length += 4;  //计算字段LastUpdateTime的长度 size_of(uint32_t)
				length += 1;  //计算字段LastUpdateTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
