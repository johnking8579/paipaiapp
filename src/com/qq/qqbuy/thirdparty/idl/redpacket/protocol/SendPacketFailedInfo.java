//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.redpacketapi.java

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import com.paipai.lang.uint64_t;
import java.util.Map;
import com.paipai.lang.uint32_t;
import java.util.HashMap;

/**
 *发送红包Info
 *
 *@date 2014-10-16 05:56:15
 *
 *@since version:0
*/
public class SendPacketFailedInfo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 失败索引
	 *
	 * 版本 >= 0
	 */
	 private Map<uint64_t,uint32_t> SequenceFailedList = new HashMap<uint64_t,uint32_t>();

	/**
	 * 版本 >= 0
	 */
	 private short SequenceFailedList_u;

	/**
	 * 红包批次ID
	 *
	 * 版本 >= 0
	 */
	 private long PacketStockId;

	/**
	 * 版本 >= 0
	 */
	 private short PacketStockId_u;

	/**
	 * 接收买家Uin
	 *
	 * 版本 >= 0
	 */
	 private long BuyerUin;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerUin_u;

	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushObject(SequenceFailedList);
		bs.pushUByte(SequenceFailedList_u);
		bs.pushLong(PacketStockId);
		bs.pushUByte(PacketStockId_u);
		bs.pushUInt(BuyerUin);
		bs.pushUByte(BuyerUin_u);
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		SequenceFailedList = (Map<uint64_t,uint32_t>)bs.popMap(uint64_t.class,uint32_t.class);
		SequenceFailedList_u = bs.popUByte();
		PacketStockId = bs.popLong();
		PacketStockId_u = bs.popUByte();
		BuyerUin = bs.popUInt();
		BuyerUin_u = bs.popUByte();
		version = bs.popUInt();
		version_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取失败索引
	 * 
	 * 此字段的版本 >= 0
	 * @return SequenceFailedList value 类型为:Map<uint64_t,uint32_t>
	 * 
	 */
	public Map<uint64_t,uint32_t> getSequenceFailedList()
	{
		return SequenceFailedList;
	}


	/**
	 * 设置失败索引
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<uint64_t,uint32_t>
	 * 
	 */
	public void setSequenceFailedList(Map<uint64_t,uint32_t> value)
	{
		if (value != null) {
				this.SequenceFailedList = value;
				this.SequenceFailedList_u = 1;
		}
	}

	public boolean issetSequenceFailedList()
	{
		return this.SequenceFailedList_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SequenceFailedList_u value 类型为:short
	 * 
	 */
	public short getSequenceFailedList_u()
	{
		return SequenceFailedList_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSequenceFailedList_u(short value)
	{
		this.SequenceFailedList_u = value;
	}


	/**
	 * 获取红包批次ID
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketStockId value 类型为:long
	 * 
	 */
	public long getPacketStockId()
	{
		return PacketStockId;
	}


	/**
	 * 设置红包批次ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPacketStockId(long value)
	{
		this.PacketStockId = value;
		this.PacketStockId_u = 1;
	}

	public boolean issetPacketStockId()
	{
		return this.PacketStockId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketStockId_u value 类型为:short
	 * 
	 */
	public short getPacketStockId_u()
	{
		return PacketStockId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketStockId_u(short value)
	{
		this.PacketStockId_u = value;
	}


	/**
	 * 获取接收买家Uin
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return BuyerUin;
	}


	/**
	 * 设置接收买家Uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.BuyerUin = value;
		this.BuyerUin_u = 1;
	}

	public boolean issetBuyerUin()
	{
		return this.BuyerUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerUin_u value 类型为:short
	 * 
	 */
	public short getBuyerUin_u()
	{
		return BuyerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerUin_u(short value)
	{
		this.BuyerUin_u = value;
	}


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SendPacketFailedInfo)
				length += ByteStream.getObjectSize(SequenceFailedList, null);  //计算字段SequenceFailedList的长度 size_of(Map)
				length += 1;  //计算字段SequenceFailedList_u的长度 size_of(uint8_t)
				length += 17;  //计算字段PacketStockId的长度 size_of(uint64_t)
				length += 1;  //计算字段PacketStockId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(SendPacketFailedInfo)
				length += ByteStream.getObjectSize(SequenceFailedList, encoding);  //计算字段SequenceFailedList的长度 size_of(Map)
				length += 1;  //计算字段SequenceFailedList_u的长度 size_of(uint8_t)
				length += 17;  //计算字段PacketStockId的长度 size_of(uint64_t)
				length += 1;  //计算字段PacketStockId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
