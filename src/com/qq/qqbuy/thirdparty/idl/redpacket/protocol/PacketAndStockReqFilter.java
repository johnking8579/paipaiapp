//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.GetPackeAndStockRequest.java

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import com.paipai.lang.uint64_t;
import java.util.Vector;
import com.paipai.lang.uint32_t;
import java.util.HashSet;
import java.util.Set;

/**
 *获取红包和批次请求过滤器
 *
 *@date 2014-10-16 05:56:15
 *
 *@since version:0
*/
public class PacketAndStockReqFilter  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 请求信息类型
	 *
	 * 版本 >= 0
	 */
	 private long InfoType;

	/**
	 * 版本 >= 0
	 */
	 private short InfoType_u;

	/**
	 * 红包名称
	 *
	 * 版本 >= 0
	 */
	 private String PacketName = new String();

	/**
	 * 版本 >= 0
	 */
	 private short PacketName_u;

	/**
	 * 红包批次id
	 *
	 * 版本 >= 0
	 */
	 private long PacketStockId;

	/**
	 * 版本 >= 0
	 */
	 private short PacketStockId_u;

	/**
	 * 红包owner
	 *
	 * 版本 >= 0
	 */
	 private long OwnerUin;

	/**
	 * 版本 >= 0
	 */
	 private short OwnerUin_u;

	/**
	 * 红包使用seller
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * 版本 >= 0
	 */
	 private short SellerUin_u;

	/**
	 * 红包状态
	 *
	 * 版本 >= 0
	 */
	 private long State;

	/**
	 * 版本 >= 0
	 */
	 private short State_u;

	/**
	 * 绑定订单id
	 *
	 * 版本 >= 0
	 */
	 private String DealCode = new String();

	/**
	 * 版本 >= 0
	 */
	 private short DealCode_u;

	/**
	 * 兑现编号
	 *
	 * 版本 >= 0
	 */
	 private long WithdrawId;

	/**
	 * 版本 >= 0
	 */
	 private short WithdrawId_u;

	/**
	 * 最大面值
	 *
	 * 版本 >= 0
	 */
	 private long MaxFaceValue;

	/**
	 * 版本 >= 0
	 */
	 private short MaxFaceValue_u;

	/**
	 * 适用店铺
	 *
	 * 版本 >= 0
	 */
	 private Set<uint32_t> ApplicableScope = new HashSet<uint32_t>();

	/**
	 * 版本 >= 0
	 */
	 private short ApplicableScope_u;

	/**
	 * 红包编号列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint64_t> PacketIdList = new Vector<uint64_t>();

	/**
	 * 版本 >= 0
	 */
	 private short PacketIdList_u;

	/**
	 * 红包类型
	 *
	 * 版本 >= 0
	 */
	 private long Type;

	/**
	 * 版本 >= 0
	 */
	 private short Type_u;

	/**
	 * 开始位置
	 *
	 * 版本 >= 0
	 */
	 private long Offset;

	/**
	 * 版本 >= 0
	 */
	 private short Offset_u;

	/**
	 * 获取数量
	 *
	 * 版本 >= 0
	 */
	 private long Limit;

	/**
	 * 版本 >= 0
	 */
	 private short Limit_u;

	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(InfoType);
		bs.pushUByte(InfoType_u);
		bs.pushString(PacketName);
		bs.pushUByte(PacketName_u);
		bs.pushLong(PacketStockId);
		bs.pushUByte(PacketStockId_u);
		bs.pushUInt(OwnerUin);
		bs.pushUByte(OwnerUin_u);
		bs.pushUInt(SellerUin);
		bs.pushUByte(SellerUin_u);
		bs.pushUInt(State);
		bs.pushUByte(State_u);
		bs.pushString(DealCode);
		bs.pushUByte(DealCode_u);
		bs.pushLong(WithdrawId);
		bs.pushUByte(WithdrawId_u);
		bs.pushUInt(MaxFaceValue);
		bs.pushUByte(MaxFaceValue_u);
		bs.pushObject(ApplicableScope);
		bs.pushUByte(ApplicableScope_u);
		bs.pushObject(PacketIdList);
		bs.pushUByte(PacketIdList_u);
		bs.pushUInt(Type);
		bs.pushUByte(Type_u);
		bs.pushUInt(Offset);
		bs.pushUByte(Offset_u);
		bs.pushUInt(Limit);
		bs.pushUByte(Limit_u);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		InfoType = bs.popUInt();
		InfoType_u = bs.popUByte();
		PacketName = bs.popString();
		PacketName_u = bs.popUByte();
		PacketStockId = bs.popLong();
		PacketStockId_u = bs.popUByte();
		OwnerUin = bs.popUInt();
		OwnerUin_u = bs.popUByte();
		SellerUin = bs.popUInt();
		SellerUin_u = bs.popUByte();
		State = bs.popUInt();
		State_u = bs.popUByte();
		DealCode = bs.popString();
		DealCode_u = bs.popUByte();
		WithdrawId = bs.popLong();
		WithdrawId_u = bs.popUByte();
		MaxFaceValue = bs.popUInt();
		MaxFaceValue_u = bs.popUByte();
		ApplicableScope = (Set<uint32_t>)bs.popSet(HashSet.class,uint32_t.class);
		ApplicableScope_u = bs.popUByte();
		PacketIdList = (Vector<uint64_t>)bs.popVector(uint64_t.class);
		PacketIdList_u = bs.popUByte();
		Type = bs.popUInt();
		Type_u = bs.popUByte();
		Offset = bs.popUInt();
		Offset_u = bs.popUByte();
		Limit = bs.popUInt();
		Limit_u = bs.popUByte();
		Version = bs.popUInt();
		Version_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取请求信息类型
	 * 
	 * 此字段的版本 >= 0
	 * @return InfoType value 类型为:long
	 * 
	 */
	public long getInfoType()
	{
		return InfoType;
	}


	/**
	 * 设置请求信息类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setInfoType(long value)
	{
		this.InfoType = value;
		this.InfoType_u = 1;
	}

	public boolean issetInfoType()
	{
		return this.InfoType_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return InfoType_u value 类型为:short
	 * 
	 */
	public short getInfoType_u()
	{
		return InfoType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setInfoType_u(short value)
	{
		this.InfoType_u = value;
	}


	/**
	 * 获取红包名称
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketName value 类型为:String
	 * 
	 */
	public String getPacketName()
	{
		return PacketName;
	}


	/**
	 * 设置红包名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPacketName(String value)
	{
		this.PacketName = value;
		this.PacketName_u = 1;
	}

	public boolean issetPacketName()
	{
		return this.PacketName_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketName_u value 类型为:short
	 * 
	 */
	public short getPacketName_u()
	{
		return PacketName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketName_u(short value)
	{
		this.PacketName_u = value;
	}


	/**
	 * 获取红包批次id
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketStockId value 类型为:long
	 * 
	 */
	public long getPacketStockId()
	{
		return PacketStockId;
	}


	/**
	 * 设置红包批次id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPacketStockId(long value)
	{
		this.PacketStockId = value;
		this.PacketStockId_u = 1;
	}

	public boolean issetPacketStockId()
	{
		return this.PacketStockId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketStockId_u value 类型为:short
	 * 
	 */
	public short getPacketStockId_u()
	{
		return PacketStockId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketStockId_u(short value)
	{
		this.PacketStockId_u = value;
	}


	/**
	 * 获取红包owner
	 * 
	 * 此字段的版本 >= 0
	 * @return OwnerUin value 类型为:long
	 * 
	 */
	public long getOwnerUin()
	{
		return OwnerUin;
	}


	/**
	 * 设置红包owner
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOwnerUin(long value)
	{
		this.OwnerUin = value;
		this.OwnerUin_u = 1;
	}

	public boolean issetOwnerUin()
	{
		return this.OwnerUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return OwnerUin_u value 类型为:short
	 * 
	 */
	public short getOwnerUin_u()
	{
		return OwnerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOwnerUin_u(short value)
	{
		this.OwnerUin_u = value;
	}


	/**
	 * 获取红包使用seller
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置红包使用seller
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
		this.SellerUin_u = 1;
	}

	public boolean issetSellerUin()
	{
		return this.SellerUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin_u value 类型为:short
	 * 
	 */
	public short getSellerUin_u()
	{
		return SellerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerUin_u(short value)
	{
		this.SellerUin_u = value;
	}


	/**
	 * 获取红包状态
	 * 
	 * 此字段的版本 >= 0
	 * @return State value 类型为:long
	 * 
	 */
	public long getState()
	{
		return State;
	}


	/**
	 * 设置红包状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setState(long value)
	{
		this.State = value;
		this.State_u = 1;
	}

	public boolean issetState()
	{
		return this.State_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return State_u value 类型为:short
	 * 
	 */
	public short getState_u()
	{
		return State_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setState_u(short value)
	{
		this.State_u = value;
	}


	/**
	 * 获取绑定订单id
	 * 
	 * 此字段的版本 >= 0
	 * @return DealCode value 类型为:String
	 * 
	 */
	public String getDealCode()
	{
		return DealCode;
	}


	/**
	 * 设置绑定订单id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealCode(String value)
	{
		this.DealCode = value;
		this.DealCode_u = 1;
	}

	public boolean issetDealCode()
	{
		return this.DealCode_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DealCode_u value 类型为:short
	 * 
	 */
	public short getDealCode_u()
	{
		return DealCode_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealCode_u(short value)
	{
		this.DealCode_u = value;
	}


	/**
	 * 获取兑现编号
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawId value 类型为:long
	 * 
	 */
	public long getWithdrawId()
	{
		return WithdrawId;
	}


	/**
	 * 设置兑现编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWithdrawId(long value)
	{
		this.WithdrawId = value;
		this.WithdrawId_u = 1;
	}

	public boolean issetWithdrawId()
	{
		return this.WithdrawId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawId_u value 类型为:short
	 * 
	 */
	public short getWithdrawId_u()
	{
		return WithdrawId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWithdrawId_u(short value)
	{
		this.WithdrawId_u = value;
	}


	/**
	 * 获取最大面值
	 * 
	 * 此字段的版本 >= 0
	 * @return MaxFaceValue value 类型为:long
	 * 
	 */
	public long getMaxFaceValue()
	{
		return MaxFaceValue;
	}


	/**
	 * 设置最大面值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMaxFaceValue(long value)
	{
		this.MaxFaceValue = value;
		this.MaxFaceValue_u = 1;
	}

	public boolean issetMaxFaceValue()
	{
		return this.MaxFaceValue_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return MaxFaceValue_u value 类型为:short
	 * 
	 */
	public short getMaxFaceValue_u()
	{
		return MaxFaceValue_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMaxFaceValue_u(short value)
	{
		this.MaxFaceValue_u = value;
	}


	/**
	 * 获取适用店铺
	 * 
	 * 此字段的版本 >= 0
	 * @return ApplicableScope value 类型为:Set<uint32_t>
	 * 
	 */
	public Set<uint32_t> getApplicableScope()
	{
		return ApplicableScope;
	}


	/**
	 * 设置适用店铺
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Set<uint32_t>
	 * 
	 */
	public void setApplicableScope(Set<uint32_t> value)
	{
		if (value != null) {
				this.ApplicableScope = value;
				this.ApplicableScope_u = 1;
		}
	}

	public boolean issetApplicableScope()
	{
		return this.ApplicableScope_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ApplicableScope_u value 类型为:short
	 * 
	 */
	public short getApplicableScope_u()
	{
		return ApplicableScope_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setApplicableScope_u(short value)
	{
		this.ApplicableScope_u = value;
	}


	/**
	 * 获取红包编号列表
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketIdList value 类型为:Vector<uint64_t>
	 * 
	 */
	public Vector<uint64_t> getPacketIdList()
	{
		return PacketIdList;
	}


	/**
	 * 设置红包编号列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint64_t>
	 * 
	 */
	public void setPacketIdList(Vector<uint64_t> value)
	{
		if (value != null) {
				this.PacketIdList = value;
				this.PacketIdList_u = 1;
		}
	}

	public boolean issetPacketIdList()
	{
		return this.PacketIdList_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketIdList_u value 类型为:short
	 * 
	 */
	public short getPacketIdList_u()
	{
		return PacketIdList_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketIdList_u(short value)
	{
		this.PacketIdList_u = value;
	}


	/**
	 * 获取红包类型
	 * 
	 * 此字段的版本 >= 0
	 * @return Type value 类型为:long
	 * 
	 */
	public long getType()
	{
		return Type;
	}


	/**
	 * 设置红包类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setType(long value)
	{
		this.Type = value;
		this.Type_u = 1;
	}

	public boolean issetType()
	{
		return this.Type_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Type_u value 类型为:short
	 * 
	 */
	public short getType_u()
	{
		return Type_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setType_u(short value)
	{
		this.Type_u = value;
	}


	/**
	 * 获取开始位置
	 * 
	 * 此字段的版本 >= 0
	 * @return Offset value 类型为:long
	 * 
	 */
	public long getOffset()
	{
		return Offset;
	}


	/**
	 * 设置开始位置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOffset(long value)
	{
		this.Offset = value;
		this.Offset_u = 1;
	}

	public boolean issetOffset()
	{
		return this.Offset_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Offset_u value 类型为:short
	 * 
	 */
	public short getOffset_u()
	{
		return Offset_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOffset_u(short value)
	{
		this.Offset_u = value;
	}


	/**
	 * 获取获取数量
	 * 
	 * 此字段的版本 >= 0
	 * @return Limit value 类型为:long
	 * 
	 */
	public long getLimit()
	{
		return Limit;
	}


	/**
	 * 设置获取数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLimit(long value)
	{
		this.Limit = value;
		this.Limit_u = 1;
	}

	public boolean issetLimit()
	{
		return this.Limit_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Limit_u value 类型为:short
	 * 
	 */
	public short getLimit_u()
	{
		return Limit_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLimit_u(short value)
	{
		this.Limit_u = value;
	}


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(PacketAndStockReqFilter)
				length += 4;  //计算字段InfoType的长度 size_of(uint32_t)
				length += 1;  //计算字段InfoType_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(PacketName, null);  //计算字段PacketName的长度 size_of(String)
				length += 1;  //计算字段PacketName_u的长度 size_of(uint8_t)
				length += 17;  //计算字段PacketStockId的长度 size_of(uint64_t)
				length += 1;  //计算字段PacketStockId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段OwnerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段OwnerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段State的长度 size_of(uint32_t)
				length += 1;  //计算字段State_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(DealCode, null);  //计算字段DealCode的长度 size_of(String)
				length += 1;  //计算字段DealCode_u的长度 size_of(uint8_t)
				length += 17;  //计算字段WithdrawId的长度 size_of(uint64_t)
				length += 1;  //计算字段WithdrawId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段MaxFaceValue的长度 size_of(uint32_t)
				length += 1;  //计算字段MaxFaceValue_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ApplicableScope, null);  //计算字段ApplicableScope的长度 size_of(Set)
				length += 1;  //计算字段ApplicableScope_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(PacketIdList, null);  //计算字段PacketIdList的长度 size_of(Vector)
				length += 1;  //计算字段PacketIdList_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Type的长度 size_of(uint32_t)
				length += 1;  //计算字段Type_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Offset的长度 size_of(uint32_t)
				length += 1;  //计算字段Offset_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Limit的长度 size_of(uint32_t)
				length += 1;  //计算字段Limit_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(PacketAndStockReqFilter)
				length += 4;  //计算字段InfoType的长度 size_of(uint32_t)
				length += 1;  //计算字段InfoType_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(PacketName, encoding);  //计算字段PacketName的长度 size_of(String)
				length += 1;  //计算字段PacketName_u的长度 size_of(uint8_t)
				length += 17;  //计算字段PacketStockId的长度 size_of(uint64_t)
				length += 1;  //计算字段PacketStockId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段OwnerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段OwnerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段State的长度 size_of(uint32_t)
				length += 1;  //计算字段State_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(DealCode, encoding);  //计算字段DealCode的长度 size_of(String)
				length += 1;  //计算字段DealCode_u的长度 size_of(uint8_t)
				length += 17;  //计算字段WithdrawId的长度 size_of(uint64_t)
				length += 1;  //计算字段WithdrawId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段MaxFaceValue的长度 size_of(uint32_t)
				length += 1;  //计算字段MaxFaceValue_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ApplicableScope, encoding);  //计算字段ApplicableScope的长度 size_of(Set)
				length += 1;  //计算字段ApplicableScope_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(PacketIdList, encoding);  //计算字段PacketIdList的长度 size_of(Vector)
				length += 1;  //计算字段PacketIdList_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Type的长度 size_of(uint32_t)
				length += 1;  //计算字段Type_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Offset的长度 size_of(uint32_t)
				length += 1;  //计算字段Offset_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Limit的长度 size_of(uint32_t)
				length += 1;  //计算字段Limit_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
