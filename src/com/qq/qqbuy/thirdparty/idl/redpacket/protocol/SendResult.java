//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.SendRedPacketV2Resp.java

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *发送红包Result
 *
 *@date 2014-10-16 05:56:15
 *
 *@since version:0
*/
public class SendResult  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 发放返回列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<SendPacketFailedInfo> PacketFailedList = new Vector<SendPacketFailedInfo>();

	/**
	 * 版本 >= 0
	 */
	 private short PacketFailedList_u;

	/**
	 * 发放失败数
	 *
	 * 版本 >= 0
	 */
	 private long FailedNumber;

	/**
	 * 版本 >= 0
	 */
	 private short FailedNumber_u;

	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushObject(PacketFailedList);
		bs.pushUByte(PacketFailedList_u);
		bs.pushUInt(FailedNumber);
		bs.pushUByte(FailedNumber_u);
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		PacketFailedList = (Vector<SendPacketFailedInfo>)bs.popVector(SendPacketFailedInfo.class);
		PacketFailedList_u = bs.popUByte();
		FailedNumber = bs.popUInt();
		FailedNumber_u = bs.popUByte();
		version = bs.popUInt();
		version_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取发放返回列表
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketFailedList value 类型为:Vector<SendPacketFailedInfo>
	 * 
	 */
	public Vector<SendPacketFailedInfo> getPacketFailedList()
	{
		return PacketFailedList;
	}


	/**
	 * 设置发放返回列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<SendPacketFailedInfo>
	 * 
	 */
	public void setPacketFailedList(Vector<SendPacketFailedInfo> value)
	{
		if (value != null) {
				this.PacketFailedList = value;
				this.PacketFailedList_u = 1;
		}
	}

	public boolean issetPacketFailedList()
	{
		return this.PacketFailedList_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketFailedList_u value 类型为:short
	 * 
	 */
	public short getPacketFailedList_u()
	{
		return PacketFailedList_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketFailedList_u(short value)
	{
		this.PacketFailedList_u = value;
	}


	/**
	 * 获取发放失败数
	 * 
	 * 此字段的版本 >= 0
	 * @return FailedNumber value 类型为:long
	 * 
	 */
	public long getFailedNumber()
	{
		return FailedNumber;
	}


	/**
	 * 设置发放失败数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFailedNumber(long value)
	{
		this.FailedNumber = value;
		this.FailedNumber_u = 1;
	}

	public boolean issetFailedNumber()
	{
		return this.FailedNumber_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return FailedNumber_u value 类型为:short
	 * 
	 */
	public short getFailedNumber_u()
	{
		return FailedNumber_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFailedNumber_u(short value)
	{
		this.FailedNumber_u = value;
	}


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SendResult)
				length += ByteStream.getObjectSize(PacketFailedList, null);  //计算字段PacketFailedList的长度 size_of(Vector)
				length += 1;  //计算字段PacketFailedList_u的长度 size_of(uint8_t)
				length += 4;  //计算字段FailedNumber的长度 size_of(uint32_t)
				length += 1;  //计算字段FailedNumber_u的长度 size_of(uint8_t)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(SendResult)
				length += ByteStream.getObjectSize(PacketFailedList, encoding);  //计算字段PacketFailedList的长度 size_of(Vector)
				length += 1;  //计算字段PacketFailedList_u的长度 size_of(uint8_t)
				length += 4;  //计算字段FailedNumber的长度 size_of(uint32_t)
				length += 1;  //计算字段FailedNumber_u的长度 size_of(uint8_t)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
