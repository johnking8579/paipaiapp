package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.uint32_t;
import java.util.HashSet;
import java.util.Set;

/**
 *红包列表
 *
 *@date 2013-08-20 05:45::21
 *
 *@since version:20130328
*/
public class RedPacket  implements ICanSerializeObject,Comparable<RedPacket>
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20130328; 

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 红包id
	 *
	 * 版本 >= 0
	 */
	 private long PacketId;

	/**
	 * 版本 >= 0
	 */
	 private short PacketId_u;

	/**
	 * 红包名称
	 *
	 * 版本 >= 0
	 */
	 private String PacketName = new String();

	/**
	 * 版本 >= 0
	 */
	 private short PacketName_u;

	/**
	 * 红包类型,红包(1)，店铺代金券(2)
	 *
	 * 版本 >= 0
	 */
	 private long Type;

	/**
	 * 版本 >= 0
	 */
	 private short Type_u;

	/**
	 * 红包批次id
	 *
	 * 版本 >= 0
	 */
	 private long PacketStockId;

	/**
	 * 版本 >= 0
	 */
	 private short PacketStockId_u;

	/**
	 * 所属者
	 *
	 * 版本 >= 0
	 */
	 private long OwnerUin;

	/**
	 * 版本 >= 0
	 */
	 private short OwnerUin_u;

	/**
	 * 面值
	 *
	 * 版本 >= 0
	 */
	 private long PacketPrice;

	/**
	 * 版本 >= 0
	 */
	 private short PacketPrice_u;

	/**
	 * 红包标识
	 *
	 * 版本 >= 0
	 */
	 private long PacketFlag;

	/**
	 * 版本 >= 0
	 */
	 private short PacketFlag_u;

	/**
	 * 对应卖家号
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * 版本 >= 0
	 */
	 private short SellerUin_u;

	/**
	 * 起始时间
	 *
	 * 版本 >= 0
	 */
	 private long BeginTime;

	/**
	 * 版本 >= 0
	 */
	 private short BeginTime_u;

	/**
	 * 结束时间
	 *
	 * 版本 >= 0
	 */
	 private long EndTime;

	/**
	 * 版本 >= 0
	 */
	 private short EndTime_u;

	/**
	 * 领取ip
	 *
	 * 版本 >= 0
	 */
	 private String Ip = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Ip_u;

	/**
	 * 领取时间
	 *
	 * 版本 >= 0
	 */
	 private long RecvTime;

	/**
	 * 版本 >= 0
	 */
	 private short RecvTime_u;

	/**
	 * 使用时间
	 *
	 * 版本 >= 0
	 */
	 private long UseTime;

	/**
	 * 版本 >= 0
	 */
	 private short UseTime_u;

	/**
	 * 红包状态,待使用(100),使用中--绑定和冻结(101)，已使用(102)
	 *
	 * 版本 >= 0
	 */
	 private long State;

	/**
	 * 版本 >= 0
	 */
	 private short State_u;

	/**
	 * 绑定的订单id
	 *
	 * 版本 >= 0
	 */
	 private String DealCode = new String();

	/**
	 * 版本 >= 0
	 */
	 private short DealCode_u;

	/**
	 * 关联url
	 *
	 * 版本 >= 0
	 */
	 private String RelaUrl = new String();

	/**
	 * 版本 >= 0
	 */
	 private short RelaUrl_u;

	/**
	 * 图片url
	 *
	 * 版本 >= 0
	 */
	 private String ImageUrl = new String();

	/**
	 * 版本 >= 0
	 */
	 private short ImageUrl_u;

	/**
	 * 实际抵扣金额
	 *
	 * 版本 >= 0
	 */
	 private long ActualPrice;

	/**
	 * 版本 >= 0
	 */
	 private short ActualPrice_u;

	/**
	 * 兑现编号
	 *
	 * 版本 >= 0
	 */
	 private long WithdrawId;

	/**
	 * 版本 >= 0
	 */
	 private short WithdrawId_u;

	/**
	 * 兑现时间
	 *
	 * 版本 >= 0
	 */
	 private long WithdrawTime;

	/**
	 * 版本 >= 0
	 */
	 private short WithdrawTime_u;

	/**
	 * 适用店铺
	 *
	 * 版本 >= 0
	 */
	 private Set<uint32_t> ApplicableScope = new HashSet<uint32_t>();

	/**
	 * 版本 >= 0
	 */
	 private short ApplicableScope_u;

	/**
	 * 最低消费
	 *
	 * 版本 >= 20130328
	 */
	 private long minimum;

	/**
	 * 
	 *
	 * 版本 >= 20130328
	 */
	 private short minimum_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushUInt(PacketId);
		bs.pushUByte(PacketId_u);
		bs.pushString(PacketName);
		bs.pushUByte(PacketName_u);
		bs.pushUInt(Type);
		bs.pushUByte(Type_u);
		bs.pushLong(PacketStockId);
		bs.pushUByte(PacketStockId_u);
		bs.pushUInt(OwnerUin);
		bs.pushUByte(OwnerUin_u);
		bs.pushUInt(PacketPrice);
		bs.pushUByte(PacketPrice_u);
		bs.pushUInt(PacketFlag);
		bs.pushUByte(PacketFlag_u);
		bs.pushUInt(SellerUin);
		bs.pushUByte(SellerUin_u);
		bs.pushUInt(BeginTime);
		bs.pushUByte(BeginTime_u);
		bs.pushUInt(EndTime);
		bs.pushUByte(EndTime_u);
		bs.pushString(Ip);
		bs.pushUByte(Ip_u);
		bs.pushUInt(RecvTime);
		bs.pushUByte(RecvTime_u);
		bs.pushUInt(UseTime);
		bs.pushUByte(UseTime_u);
		bs.pushUInt(State);
		bs.pushUByte(State_u);
		bs.pushString(DealCode);
		bs.pushUByte(DealCode_u);
		bs.pushString(RelaUrl);
		bs.pushUByte(RelaUrl_u);
		bs.pushString(ImageUrl);
		bs.pushUByte(ImageUrl_u);
		bs.pushUInt(ActualPrice);
		bs.pushUByte(ActualPrice_u);
		bs.pushLong(WithdrawId);
		bs.pushUByte(WithdrawId_u);
		bs.pushUInt(WithdrawTime);
		bs.pushUByte(WithdrawTime_u);
		bs.pushObject(ApplicableScope);
		bs.pushUByte(ApplicableScope_u);
		if(  this.version >= 20130328 ){
				bs.pushUInt(minimum);
		}
		if(  this.version >= 20130328 ){
				bs.pushUByte(minimum_u);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		PacketId = bs.popUInt();
		PacketId_u = bs.popUByte();
		PacketName = bs.popString();
		PacketName_u = bs.popUByte();
		Type = bs.popUInt();
		Type_u = bs.popUByte();
		PacketStockId = bs.popLong();
		PacketStockId_u = bs.popUByte();
		OwnerUin = bs.popUInt();
		OwnerUin_u = bs.popUByte();
		PacketPrice = bs.popUInt();
		PacketPrice_u = bs.popUByte();
		PacketFlag = bs.popUInt();
		PacketFlag_u = bs.popUByte();
		SellerUin = bs.popUInt();
		SellerUin_u = bs.popUByte();
		BeginTime = bs.popUInt();
		BeginTime_u = bs.popUByte();
		EndTime = bs.popUInt();
		EndTime_u = bs.popUByte();
		Ip = bs.popString();
		Ip_u = bs.popUByte();
		RecvTime = bs.popUInt();
		RecvTime_u = bs.popUByte();
		UseTime = bs.popUInt();
		UseTime_u = bs.popUByte();
		State = bs.popUInt();
		State_u = bs.popUByte();
		DealCode = bs.popString();
		DealCode_u = bs.popUByte();
		RelaUrl = bs.popString();
		RelaUrl_u = bs.popUByte();
		ImageUrl = bs.popString();
		ImageUrl_u = bs.popUByte();
		ActualPrice = bs.popUInt();
		ActualPrice_u = bs.popUByte();
		WithdrawId = bs.popLong();
		WithdrawId_u = bs.popUByte();
		WithdrawTime = bs.popUInt();
		WithdrawTime_u = bs.popUByte();
		ApplicableScope = (Set<uint32_t>)bs.popSet(HashSet.class,uint32_t.class);
		ApplicableScope_u = bs.popUByte();
		if(  this.version >= 20130328 ){
				minimum = bs.popUInt();
		}
		if(  this.version >= 20130328 ){
				minimum_u = bs.popUByte();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取红包id
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketId value 类型为:long
	 * 
	 */
	public long getPacketId()
	{
		return PacketId;
	}


	/**
	 * 设置红包id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPacketId(long value)
	{
		this.PacketId = value;
		this.PacketId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketId_u value 类型为:short
	 * 
	 */
	public short getPacketId_u()
	{
		return PacketId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketId_u(short value)
	{
		this.PacketId_u = value;
	}


	/**
	 * 获取红包名称
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketName value 类型为:String
	 * 
	 */
	public String getPacketName()
	{
		return PacketName;
	}


	/**
	 * 设置红包名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPacketName(String value)
	{
		if (value != null) {
				this.PacketName = value;
				this.PacketName_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketName_u value 类型为:short
	 * 
	 */
	public short getPacketName_u()
	{
		return PacketName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketName_u(short value)
	{
		this.PacketName_u = value;
	}


	/**
	 * 获取红包类型,红包(1)，店铺代金券(2)
	 * 
	 * 此字段的版本 >= 0
	 * @return Type value 类型为:long
	 * 
	 */
	public long getType()
	{
		return Type;
	}


	/**
	 * 设置红包类型,红包(1)，店铺代金券(2)
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setType(long value)
	{
		this.Type = value;
		this.Type_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Type_u value 类型为:short
	 * 
	 */
	public short getType_u()
	{
		return Type_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setType_u(short value)
	{
		this.Type_u = value;
	}


	/**
	 * 获取红包批次id
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketStockId value 类型为:long
	 * 
	 */
	public long getPacketStockId()
	{
		return PacketStockId;
	}


	/**
	 * 设置红包批次id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPacketStockId(long value)
	{
		this.PacketStockId = value;
		this.PacketStockId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketStockId_u value 类型为:short
	 * 
	 */
	public short getPacketStockId_u()
	{
		return PacketStockId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketStockId_u(short value)
	{
		this.PacketStockId_u = value;
	}


	/**
	 * 获取所属者
	 * 
	 * 此字段的版本 >= 0
	 * @return OwnerUin value 类型为:long
	 * 
	 */
	public long getOwnerUin()
	{
		return OwnerUin;
	}


	/**
	 * 设置所属者
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOwnerUin(long value)
	{
		this.OwnerUin = value;
		this.OwnerUin_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return OwnerUin_u value 类型为:short
	 * 
	 */
	public short getOwnerUin_u()
	{
		return OwnerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOwnerUin_u(short value)
	{
		this.OwnerUin_u = value;
	}


	/**
	 * 获取面值
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketPrice value 类型为:long
	 * 
	 */
	public long getPacketPrice()
	{
		return PacketPrice;
	}


	/**
	 * 设置面值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPacketPrice(long value)
	{
		this.PacketPrice = value;
		this.PacketPrice_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketPrice_u value 类型为:short
	 * 
	 */
	public short getPacketPrice_u()
	{
		return PacketPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketPrice_u(short value)
	{
		this.PacketPrice_u = value;
	}


	/**
	 * 获取红包标识
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketFlag value 类型为:long
	 * 
	 */
	public long getPacketFlag()
	{
		return PacketFlag;
	}


	/**
	 * 设置红包标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPacketFlag(long value)
	{
		this.PacketFlag = value;
		this.PacketFlag_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketFlag_u value 类型为:short
	 * 
	 */
	public short getPacketFlag_u()
	{
		return PacketFlag_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketFlag_u(short value)
	{
		this.PacketFlag_u = value;
	}


	/**
	 * 获取对应卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置对应卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
		this.SellerUin_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin_u value 类型为:short
	 * 
	 */
	public short getSellerUin_u()
	{
		return SellerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerUin_u(short value)
	{
		this.SellerUin_u = value;
	}


	/**
	 * 获取起始时间
	 * 
	 * 此字段的版本 >= 0
	 * @return BeginTime value 类型为:long
	 * 
	 */
	public long getBeginTime()
	{
		return BeginTime;
	}


	/**
	 * 设置起始时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBeginTime(long value)
	{
		this.BeginTime = value;
		this.BeginTime_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BeginTime_u value 类型为:short
	 * 
	 */
	public short getBeginTime_u()
	{
		return BeginTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBeginTime_u(short value)
	{
		this.BeginTime_u = value;
	}


	/**
	 * 获取结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return EndTime value 类型为:long
	 * 
	 */
	public long getEndTime()
	{
		return EndTime;
	}


	/**
	 * 设置结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEndTime(long value)
	{
		this.EndTime = value;
		this.EndTime_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return EndTime_u value 类型为:short
	 * 
	 */
	public short getEndTime_u()
	{
		return EndTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEndTime_u(short value)
	{
		this.EndTime_u = value;
	}


	/**
	 * 获取领取ip
	 * 
	 * 此字段的版本 >= 0
	 * @return Ip value 类型为:String
	 * 
	 */
	public String getIp()
	{
		return Ip;
	}


	/**
	 * 设置领取ip
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setIp(String value)
	{
		if (value != null) {
				this.Ip = value;
				this.Ip_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Ip_u value 类型为:short
	 * 
	 */
	public short getIp_u()
	{
		return Ip_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIp_u(short value)
	{
		this.Ip_u = value;
	}


	/**
	 * 获取领取时间
	 * 
	 * 此字段的版本 >= 0
	 * @return RecvTime value 类型为:long
	 * 
	 */
	public long getRecvTime()
	{
		return RecvTime;
	}


	/**
	 * 设置领取时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRecvTime(long value)
	{
		this.RecvTime = value;
		this.RecvTime_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return RecvTime_u value 类型为:short
	 * 
	 */
	public short getRecvTime_u()
	{
		return RecvTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRecvTime_u(short value)
	{
		this.RecvTime_u = value;
	}


	/**
	 * 获取使用时间
	 * 
	 * 此字段的版本 >= 0
	 * @return UseTime value 类型为:long
	 * 
	 */
	public long getUseTime()
	{
		return UseTime;
	}


	/**
	 * 设置使用时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUseTime(long value)
	{
		this.UseTime = value;
		this.UseTime_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return UseTime_u value 类型为:short
	 * 
	 */
	public short getUseTime_u()
	{
		return UseTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUseTime_u(short value)
	{
		this.UseTime_u = value;
	}


	/**
	 * 获取红包状态,待使用(100),使用中--绑定和冻结(101)，已使用(102)
	 * 
	 * 此字段的版本 >= 0
	 * @return State value 类型为:long
	 * 
	 */
	public long getState()
	{
		return State;
	}


	/**
	 * 设置红包状态,待使用(100),使用中--绑定和冻结(101)，已使用(102)
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setState(long value)
	{
		this.State = value;
		this.State_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return State_u value 类型为:short
	 * 
	 */
	public short getState_u()
	{
		return State_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setState_u(short value)
	{
		this.State_u = value;
	}


	/**
	 * 获取绑定的订单id
	 * 
	 * 此字段的版本 >= 0
	 * @return DealCode value 类型为:String
	 * 
	 */
	public String getDealCode()
	{
		return DealCode;
	}


	/**
	 * 设置绑定的订单id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealCode(String value)
	{
		if (value != null) {
				this.DealCode = value;
				this.DealCode_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DealCode_u value 类型为:short
	 * 
	 */
	public short getDealCode_u()
	{
		return DealCode_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealCode_u(short value)
	{
		this.DealCode_u = value;
	}


	/**
	 * 获取关联url
	 * 
	 * 此字段的版本 >= 0
	 * @return RelaUrl value 类型为:String
	 * 
	 */
	public String getRelaUrl()
	{
		return RelaUrl;
	}


	/**
	 * 设置关联url
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRelaUrl(String value)
	{
		if (value != null) {
				this.RelaUrl = value;
				this.RelaUrl_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return RelaUrl_u value 类型为:short
	 * 
	 */
	public short getRelaUrl_u()
	{
		return RelaUrl_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRelaUrl_u(short value)
	{
		this.RelaUrl_u = value;
	}


	/**
	 * 获取图片url
	 * 
	 * 此字段的版本 >= 0
	 * @return ImageUrl value 类型为:String
	 * 
	 */
	public String getImageUrl()
	{
		return ImageUrl;
	}


	/**
	 * 设置图片url
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setImageUrl(String value)
	{
		if (value != null) {
				this.ImageUrl = value;
				this.ImageUrl_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ImageUrl_u value 类型为:short
	 * 
	 */
	public short getImageUrl_u()
	{
		return ImageUrl_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setImageUrl_u(short value)
	{
		this.ImageUrl_u = value;
	}


	/**
	 * 获取实际抵扣金额
	 * 
	 * 此字段的版本 >= 0
	 * @return ActualPrice value 类型为:long
	 * 
	 */
	public long getActualPrice()
	{
		return ActualPrice;
	}


	/**
	 * 设置实际抵扣金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActualPrice(long value)
	{
		this.ActualPrice = value;
		this.ActualPrice_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ActualPrice_u value 类型为:short
	 * 
	 */
	public short getActualPrice_u()
	{
		return ActualPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setActualPrice_u(short value)
	{
		this.ActualPrice_u = value;
	}


	/**
	 * 获取兑现编号
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawId value 类型为:long
	 * 
	 */
	public long getWithdrawId()
	{
		return WithdrawId;
	}


	/**
	 * 设置兑现编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWithdrawId(long value)
	{
		this.WithdrawId = value;
		this.WithdrawId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawId_u value 类型为:short
	 * 
	 */
	public short getWithdrawId_u()
	{
		return WithdrawId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWithdrawId_u(short value)
	{
		this.WithdrawId_u = value;
	}


	/**
	 * 获取兑现时间
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawTime value 类型为:long
	 * 
	 */
	public long getWithdrawTime()
	{
		return WithdrawTime;
	}


	/**
	 * 设置兑现时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWithdrawTime(long value)
	{
		this.WithdrawTime = value;
		this.WithdrawTime_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawTime_u value 类型为:short
	 * 
	 */
	public short getWithdrawTime_u()
	{
		return WithdrawTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWithdrawTime_u(short value)
	{
		this.WithdrawTime_u = value;
	}


	/**
	 * 获取适用店铺
	 * 
	 * 此字段的版本 >= 0
	 * @return ApplicableScope value 类型为:Set<uint32_t>
	 * 
	 */
	public Set<uint32_t> getApplicableScope()
	{
		return ApplicableScope;
	}


	/**
	 * 设置适用店铺
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Set<uint32_t>
	 * 
	 */
	public void setApplicableScope(Set<uint32_t> value)
	{
		if (value != null) {
				this.ApplicableScope = value;
				this.ApplicableScope_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ApplicableScope_u value 类型为:short
	 * 
	 */
	public short getApplicableScope_u()
	{
		return ApplicableScope_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setApplicableScope_u(short value)
	{
		this.ApplicableScope_u = value;
	}


	/**
	 * 获取最低消费
	 * 
	 * 此字段的版本 >= 20130328
	 * @return minimum value 类型为:long
	 * 
	 */
	public long getMinimum()
	{
		return minimum;
	}


	/**
	 * 设置最低消费
	 * 
	 * 此字段的版本 >= 20130328
	 * @param  value 类型为:long
	 * 
	 */
	public void setMinimum(long value)
	{
		this.minimum = value;
		this.minimum_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20130328
	 * @return minimum_u value 类型为:short
	 * 
	 */
	public short getMinimum_u()
	{
		return minimum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20130328
	 * @param  value 类型为:short
	 * 
	 */
	public void setMinimum_u(short value)
	{
		this.minimum_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(RedPacket)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PacketId的长度 size_of(uint32_t)
				length += 1;  //计算字段PacketId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(PacketName);  //计算字段PacketName的长度 size_of(String)
				length += 1;  //计算字段PacketName_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Type的长度 size_of(uint32_t)
				length += 1;  //计算字段Type_u的长度 size_of(uint8_t)
				length += 17;  //计算字段PacketStockId的长度 size_of(uint64_t)
				length += 1;  //计算字段PacketStockId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段OwnerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段OwnerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PacketPrice的长度 size_of(uint32_t)
				length += 1;  //计算字段PacketPrice_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PacketFlag的长度 size_of(uint32_t)
				length += 1;  //计算字段PacketFlag_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BeginTime的长度 size_of(uint32_t)
				length += 1;  //计算字段BeginTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段EndTime的长度 size_of(uint32_t)
				length += 1;  //计算字段EndTime_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Ip);  //计算字段Ip的长度 size_of(String)
				length += 1;  //计算字段Ip_u的长度 size_of(uint8_t)
				length += 4;  //计算字段RecvTime的长度 size_of(uint32_t)
				length += 1;  //计算字段RecvTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段UseTime的长度 size_of(uint32_t)
				length += 1;  //计算字段UseTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段State的长度 size_of(uint32_t)
				length += 1;  //计算字段State_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(DealCode);  //计算字段DealCode的长度 size_of(String)
				length += 1;  //计算字段DealCode_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(RelaUrl);  //计算字段RelaUrl的长度 size_of(String)
				length += 1;  //计算字段RelaUrl_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ImageUrl);  //计算字段ImageUrl的长度 size_of(String)
				length += 1;  //计算字段ImageUrl_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ActualPrice的长度 size_of(uint32_t)
				length += 1;  //计算字段ActualPrice_u的长度 size_of(uint8_t)
				length += 17;  //计算字段WithdrawId的长度 size_of(uint64_t)
				length += 1;  //计算字段WithdrawId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段WithdrawTime的长度 size_of(uint32_t)
				length += 1;  //计算字段WithdrawTime_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ApplicableScope);  //计算字段ApplicableScope的长度 size_of(Set)
				length += 1;  //计算字段ApplicableScope_u的长度 size_of(uint8_t)
				if(  this.version >= 20130328 ){
						length += 4;  //计算字段minimum的长度 size_of(uint32_t)
				}
				if(  this.version >= 20130328 ){
						length += 1;  //计算字段minimum_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	@Override
	public int compareTo(RedPacket o) {
		//如果时间比较晚放上面
		if(this.EndTime < o.getEndTime()){
			return -1;
		}
		//如果该对你的时间比较早了，则按领取时间来算排
		if((this.EndTime == o.getEndTime())&&(this.RecvTime > o.getRecvTime())){
			return -1;
		}
		return 1;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本20130328所包含的字段*******
 *	long version;///<版本号
 *	short version_u;
 *	long PacketId;///<红包id
 *	short PacketId_u;
 *	String PacketName;///<红包名称
 *	short PacketName_u;
 *	long Type;///<红包类型,红包(1)，店铺代金券(2)
 *	short Type_u;
 *	long PacketStockId;///<红包批次id
 *	short PacketStockId_u;
 *	long OwnerUin;///<所属者
 *	short OwnerUin_u;
 *	long PacketPrice;///<面值
 *	short PacketPrice_u;
 *	long PacketFlag;///<红包标识
 *	short PacketFlag_u;
 *	long SellerUin;///<对应卖家号
 *	short SellerUin_u;
 *	long BeginTime;///<起始时间
 *	short BeginTime_u;
 *	long EndTime;///<结束时间
 *	short EndTime_u;
 *	String Ip;///<领取ip
 *	short Ip_u;
 *	long RecvTime;///<领取时间
 *	short RecvTime_u;
 *	long UseTime;///<使用时间
 *	short UseTime_u;
 *	long State;///<红包状态,待使用(100),使用中--绑定和冻结(101)，已使用(102)
 *	short State_u;
 *	String DealCode;///<绑定的订单id
 *	short DealCode_u;
 *	String RelaUrl;///<关联url
 *	short RelaUrl_u;
 *	String ImageUrl;///<图片url
 *	short ImageUrl_u;
 *	long ActualPrice;///<实际抵扣金额
 *	short ActualPrice_u;
 *	long WithdrawId;///<兑现编号
 *	short WithdrawId_u;
 *	long WithdrawTime;///<兑现时间
 *	short WithdrawTime_u;
 *	Set<uint32_t> ApplicableScope;///<适用店铺
 *	short ApplicableScope_u;
 *	long minimum;///<最低消费
 *	short minimum_u;
 *****以上是版本20130328所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
