//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.GetPackeAndStockResponse.java

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import com.paipai.lang.uint32_t;
import java.util.HashSet;
import java.util.Set;

/**
 *红包和批次信息
 *
 *@date 2014-10-16 05:56:15
 *
 *@since version:0
*/
public class PacketAndStockList  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 红包编号
	 *
	 * 版本 >= 0
	 */
	 private long PacketId;

	/**
	 * 版本 >= 0
	 */
	 private short PacketId_u;

	/**
	 * 红包批次
	 *
	 * 版本 >= 0
	 */
	 private long PacketStockId;

	/**
	 * 版本 >= 0
	 */
	 private short PacketStockId_u;

	/**
	 * 红包名称
	 *
	 * 版本 >= 0
	 */
	 private String PacketName = new String();

	/**
	 * 版本 >= 0
	 */
	 private short PacketName_u;

	/**
	 * 红包类型
	 *
	 * 版本 >= 0
	 */
	 private long Type;

	/**
	 * 版本 >= 0
	 */
	 private short Type_u;

	/**
	 * 红包owner
	 *
	 * 版本 >= 0
	 */
	 private long OwnerUin;

	/**
	 * 版本 >= 0
	 */
	 private short OwnerUin_u;

	/**
	 * 红包面值
	 *
	 * 版本 >= 0
	 */
	 private long PacketPrice;

	/**
	 * 版本 >= 0
	 */
	 private short PacketPrice_u;

	/**
	 * 红包标识
	 *
	 * 版本 >= 0
	 */
	 private long PacketFlag;

	/**
	 * 版本 >= 0
	 */
	 private short PacketFlag_u;

	/**
	 * 生效日期
	 *
	 * 版本 >= 0
	 */
	 private long BeginTime;

	/**
	 * 版本 >= 0
	 */
	 private short BeginTime_u;

	/**
	 * 失效日期
	 *
	 * 版本 >= 0
	 */
	 private long EndTime;

	/**
	 * 版本 >= 0
	 */
	 private short EndTime_u;

	/**
	 * 领取ip
	 *
	 * 版本 >= 0
	 */
	 private String GetIp = new String();

	/**
	 * 版本 >= 0
	 */
	 private short GetIp_u;

	/**
	 * 领取机器码
	 *
	 * 版本 >= 0
	 */
	 private String GetMachineKey = new String();

	/**
	 * 版本 >= 0
	 */
	 private short GetMachineKey_u;

	/**
	 * 领取时间
	 *
	 * 版本 >= 0
	 */
	 private long RecvTime;

	/**
	 * 版本 >= 0
	 */
	 private short RecvTime_u;

	/**
	 * 使用ip
	 *
	 * 版本 >= 0
	 */
	 private String UseIp = new String();

	/**
	 * 版本 >= 0
	 */
	 private short UseIp_u;

	/**
	 * 使用机器码
	 *
	 * 版本 >= 0
	 */
	 private String UseMachineKey = new String();

	/**
	 * 版本 >= 0
	 */
	 private short UseMachineKey_u;

	/**
	 * 使用时间
	 *
	 * 版本 >= 0
	 */
	 private long UsedTime;

	/**
	 * 版本 >= 0
	 */
	 private short UsedTime_u;

	/**
	 * 兑现编号
	 *
	 * 版本 >= 0
	 */
	 private long WithdrawId;

	/**
	 * 版本 >= 0
	 */
	 private short WithdrawId_u;

	/**
	 * 兑现时间
	 *
	 * 版本 >= 0
	 */
	 private long WithdrawTime;

	/**
	 * 版本 >= 0
	 */
	 private short WithdrawTime_u;

	/**
	 * 兑现财付通时间
	 *
	 * 版本 >= 0
	 */
	 private long WithdrawCftTime;

	/**
	 * 版本 >= 0
	 */
	 private short WithdrawCftTime_u;

	/**
	 * 实际抵扣金额
	 *
	 * 版本 >= 0
	 */
	 private long ActualPrice;

	/**
	 * 版本 >= 0
	 */
	 private short ActualPrice_u;

	/**
	 * 兑现金额
	 *
	 * 版本 >= 0
	 */
	 private long WithdrawAmount;

	/**
	 * 版本 >= 0
	 */
	 private short WithdrawAmount_u;

	/**
	 * 最低消费
	 *
	 * 版本 >= 0
	 */
	 private long Minimum;

	/**
	 * 版本 >= 0
	 */
	 private short Minimum_u;

	/**
	 * 兑现比例
	 *
	 * 版本 >= 0
	 */
	 private long WithdrawRate;

	/**
	 * 版本 >= 0
	 */
	 private short WithdrawRate_u;

	/**
	 * 订单卖家uin
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * 版本 >= 0
	 */
	 private short SellerUin_u;

	/**
	 * 关联订单编号
	 *
	 * 版本 >= 0
	 */
	 private String DealCode = new String();

	/**
	 * 版本 >= 0
	 */
	 private short DealCode_u;

	/**
	 * 关联地址
	 *
	 * 版本 >= 0
	 */
	 private String RelaUrl = new String();

	/**
	 * 版本 >= 0
	 */
	 private short RelaUrl_u;

	/**
	 * 图片地址
	 *
	 * 版本 >= 0
	 */
	 private String ImageUrl = new String();

	/**
	 * 版本 >= 0
	 */
	 private short ImageUrl_u;

	/**
	 * 红包状态
	 *
	 * 版本 >= 0
	 */
	 private long State;

	/**
	 * 版本 >= 0
	 */
	 private short State_u;

	/**
	 * 适用店铺
	 *
	 * 版本 >= 0
	 */
	 private Set<uint32_t> ApplicableScope = new HashSet<uint32_t>();

	/**
	 * 版本 >= 0
	 */
	 private short ApplicableScope_u;

	/**
	 * 最后更新时间
	 *
	 * 版本 >= 0
	 */
	 private long LastUpdateTime;

	/**
	 * 版本 >= 0
	 */
	 private short LastUpdateTime_u;

	/**
	 * 红包批次
	 *
	 * 版本 >= 0
	 */
	 private PacketStock PacketStockInfo = new PacketStock();

	/**
	 * 版本 >= 0
	 */
	 private short PacketStockInfo_u;

	/**
	 * 兑现记录
	 *
	 * 版本 >= 0
	 */
	 private WithdrawRecord WithdrawRecordInfo = new WithdrawRecord();

	/**
	 * 版本 >= 0
	 */
	 private short WithdrawRecordInfo_u;

	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushLong(PacketId);
		bs.pushUByte(PacketId_u);
		bs.pushLong(PacketStockId);
		bs.pushUByte(PacketStockId_u);
		bs.pushString(PacketName);
		bs.pushUByte(PacketName_u);
		bs.pushUInt(Type);
		bs.pushUByte(Type_u);
		bs.pushUInt(OwnerUin);
		bs.pushUByte(OwnerUin_u);
		bs.pushUInt(PacketPrice);
		bs.pushUByte(PacketPrice_u);
		bs.pushUInt(PacketFlag);
		bs.pushUByte(PacketFlag_u);
		bs.pushUInt(BeginTime);
		bs.pushUByte(BeginTime_u);
		bs.pushUInt(EndTime);
		bs.pushUByte(EndTime_u);
		bs.pushString(GetIp);
		bs.pushUByte(GetIp_u);
		bs.pushString(GetMachineKey);
		bs.pushUByte(GetMachineKey_u);
		bs.pushUInt(RecvTime);
		bs.pushUByte(RecvTime_u);
		bs.pushString(UseIp);
		bs.pushUByte(UseIp_u);
		bs.pushString(UseMachineKey);
		bs.pushUByte(UseMachineKey_u);
		bs.pushUInt(UsedTime);
		bs.pushUByte(UsedTime_u);
		bs.pushLong(WithdrawId);
		bs.pushUByte(WithdrawId_u);
		bs.pushUInt(WithdrawTime);
		bs.pushUByte(WithdrawTime_u);
		bs.pushUInt(WithdrawCftTime);
		bs.pushUByte(WithdrawCftTime_u);
		bs.pushUInt(ActualPrice);
		bs.pushUByte(ActualPrice_u);
		bs.pushUInt(WithdrawAmount);
		bs.pushUByte(WithdrawAmount_u);
		bs.pushUInt(Minimum);
		bs.pushUByte(Minimum_u);
		bs.pushUInt(WithdrawRate);
		bs.pushUByte(WithdrawRate_u);
		bs.pushUInt(SellerUin);
		bs.pushUByte(SellerUin_u);
		bs.pushString(DealCode);
		bs.pushUByte(DealCode_u);
		bs.pushString(RelaUrl);
		bs.pushUByte(RelaUrl_u);
		bs.pushString(ImageUrl);
		bs.pushUByte(ImageUrl_u);
		bs.pushUInt(State);
		bs.pushUByte(State_u);
		bs.pushObject(ApplicableScope);
		bs.pushUByte(ApplicableScope_u);
		bs.pushUInt(LastUpdateTime);
		bs.pushUByte(LastUpdateTime_u);
		bs.pushObject(PacketStockInfo);
		bs.pushUByte(PacketStockInfo_u);
		bs.pushObject(WithdrawRecordInfo);
		bs.pushUByte(WithdrawRecordInfo_u);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		PacketId = bs.popLong();
		PacketId_u = bs.popUByte();
		PacketStockId = bs.popLong();
		PacketStockId_u = bs.popUByte();
		PacketName = bs.popString();
		PacketName_u = bs.popUByte();
		Type = bs.popUInt();
		Type_u = bs.popUByte();
		OwnerUin = bs.popUInt();
		OwnerUin_u = bs.popUByte();
		PacketPrice = bs.popUInt();
		PacketPrice_u = bs.popUByte();
		PacketFlag = bs.popUInt();
		PacketFlag_u = bs.popUByte();
		BeginTime = bs.popUInt();
		BeginTime_u = bs.popUByte();
		EndTime = bs.popUInt();
		EndTime_u = bs.popUByte();
		GetIp = bs.popString();
		GetIp_u = bs.popUByte();
		GetMachineKey = bs.popString();
		GetMachineKey_u = bs.popUByte();
		RecvTime = bs.popUInt();
		RecvTime_u = bs.popUByte();
		UseIp = bs.popString();
		UseIp_u = bs.popUByte();
		UseMachineKey = bs.popString();
		UseMachineKey_u = bs.popUByte();
		UsedTime = bs.popUInt();
		UsedTime_u = bs.popUByte();
		WithdrawId = bs.popLong();
		WithdrawId_u = bs.popUByte();
		WithdrawTime = bs.popUInt();
		WithdrawTime_u = bs.popUByte();
		WithdrawCftTime = bs.popUInt();
		WithdrawCftTime_u = bs.popUByte();
		ActualPrice = bs.popUInt();
		ActualPrice_u = bs.popUByte();
		WithdrawAmount = bs.popUInt();
		WithdrawAmount_u = bs.popUByte();
		Minimum = bs.popUInt();
		Minimum_u = bs.popUByte();
		WithdrawRate = bs.popUInt();
		WithdrawRate_u = bs.popUByte();
		SellerUin = bs.popUInt();
		SellerUin_u = bs.popUByte();
		DealCode = bs.popString();
		DealCode_u = bs.popUByte();
		RelaUrl = bs.popString();
		RelaUrl_u = bs.popUByte();
		ImageUrl = bs.popString();
		ImageUrl_u = bs.popUByte();
		State = bs.popUInt();
		State_u = bs.popUByte();
		ApplicableScope = (Set<uint32_t>)bs.popSet(HashSet.class,uint32_t.class);
		ApplicableScope_u = bs.popUByte();
		LastUpdateTime = bs.popUInt();
		LastUpdateTime_u = bs.popUByte();
		PacketStockInfo = (PacketStock) bs.popObject(PacketStock.class);
		PacketStockInfo_u = bs.popUByte();
		WithdrawRecordInfo = (WithdrawRecord) bs.popObject(WithdrawRecord.class);
		WithdrawRecordInfo_u = bs.popUByte();
		Version = bs.popUInt();
		Version_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取红包编号
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketId value 类型为:long
	 * 
	 */
	public long getPacketId()
	{
		return PacketId;
	}


	/**
	 * 设置红包编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPacketId(long value)
	{
		this.PacketId = value;
		this.PacketId_u = 1;
	}

	public boolean issetPacketId()
	{
		return this.PacketId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketId_u value 类型为:short
	 * 
	 */
	public short getPacketId_u()
	{
		return PacketId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketId_u(short value)
	{
		this.PacketId_u = value;
	}


	/**
	 * 获取红包批次
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketStockId value 类型为:long
	 * 
	 */
	public long getPacketStockId()
	{
		return PacketStockId;
	}


	/**
	 * 设置红包批次
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPacketStockId(long value)
	{
		this.PacketStockId = value;
		this.PacketStockId_u = 1;
	}

	public boolean issetPacketStockId()
	{
		return this.PacketStockId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketStockId_u value 类型为:short
	 * 
	 */
	public short getPacketStockId_u()
	{
		return PacketStockId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketStockId_u(short value)
	{
		this.PacketStockId_u = value;
	}


	/**
	 * 获取红包名称
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketName value 类型为:String
	 * 
	 */
	public String getPacketName()
	{
		return PacketName;
	}


	/**
	 * 设置红包名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPacketName(String value)
	{
		this.PacketName = value;
		this.PacketName_u = 1;
	}

	public boolean issetPacketName()
	{
		return this.PacketName_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketName_u value 类型为:short
	 * 
	 */
	public short getPacketName_u()
	{
		return PacketName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketName_u(short value)
	{
		this.PacketName_u = value;
	}


	/**
	 * 获取红包类型
	 * 
	 * 此字段的版本 >= 0
	 * @return Type value 类型为:long
	 * 
	 */
	public long getType()
	{
		return Type;
	}


	/**
	 * 设置红包类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setType(long value)
	{
		this.Type = value;
		this.Type_u = 1;
	}

	public boolean issetType()
	{
		return this.Type_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Type_u value 类型为:short
	 * 
	 */
	public short getType_u()
	{
		return Type_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setType_u(short value)
	{
		this.Type_u = value;
	}


	/**
	 * 获取红包owner
	 * 
	 * 此字段的版本 >= 0
	 * @return OwnerUin value 类型为:long
	 * 
	 */
	public long getOwnerUin()
	{
		return OwnerUin;
	}


	/**
	 * 设置红包owner
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOwnerUin(long value)
	{
		this.OwnerUin = value;
		this.OwnerUin_u = 1;
	}

	public boolean issetOwnerUin()
	{
		return this.OwnerUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return OwnerUin_u value 类型为:short
	 * 
	 */
	public short getOwnerUin_u()
	{
		return OwnerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOwnerUin_u(short value)
	{
		this.OwnerUin_u = value;
	}


	/**
	 * 获取红包面值
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketPrice value 类型为:long
	 * 
	 */
	public long getPacketPrice()
	{
		return PacketPrice;
	}


	/**
	 * 设置红包面值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPacketPrice(long value)
	{
		this.PacketPrice = value;
		this.PacketPrice_u = 1;
	}

	public boolean issetPacketPrice()
	{
		return this.PacketPrice_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketPrice_u value 类型为:short
	 * 
	 */
	public short getPacketPrice_u()
	{
		return PacketPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketPrice_u(short value)
	{
		this.PacketPrice_u = value;
	}


	/**
	 * 获取红包标识
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketFlag value 类型为:long
	 * 
	 */
	public long getPacketFlag()
	{
		return PacketFlag;
	}


	/**
	 * 设置红包标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPacketFlag(long value)
	{
		this.PacketFlag = value;
		this.PacketFlag_u = 1;
	}

	public boolean issetPacketFlag()
	{
		return this.PacketFlag_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketFlag_u value 类型为:short
	 * 
	 */
	public short getPacketFlag_u()
	{
		return PacketFlag_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketFlag_u(short value)
	{
		this.PacketFlag_u = value;
	}


	/**
	 * 获取生效日期
	 * 
	 * 此字段的版本 >= 0
	 * @return BeginTime value 类型为:long
	 * 
	 */
	public long getBeginTime()
	{
		return BeginTime;
	}


	/**
	 * 设置生效日期
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBeginTime(long value)
	{
		this.BeginTime = value;
		this.BeginTime_u = 1;
	}

	public boolean issetBeginTime()
	{
		return this.BeginTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BeginTime_u value 类型为:short
	 * 
	 */
	public short getBeginTime_u()
	{
		return BeginTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBeginTime_u(short value)
	{
		this.BeginTime_u = value;
	}


	/**
	 * 获取失效日期
	 * 
	 * 此字段的版本 >= 0
	 * @return EndTime value 类型为:long
	 * 
	 */
	public long getEndTime()
	{
		return EndTime;
	}


	/**
	 * 设置失效日期
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEndTime(long value)
	{
		this.EndTime = value;
		this.EndTime_u = 1;
	}

	public boolean issetEndTime()
	{
		return this.EndTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return EndTime_u value 类型为:short
	 * 
	 */
	public short getEndTime_u()
	{
		return EndTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEndTime_u(short value)
	{
		this.EndTime_u = value;
	}


	/**
	 * 获取领取ip
	 * 
	 * 此字段的版本 >= 0
	 * @return GetIp value 类型为:String
	 * 
	 */
	public String getGetIp()
	{
		return GetIp;
	}


	/**
	 * 设置领取ip
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setGetIp(String value)
	{
		this.GetIp = value;
		this.GetIp_u = 1;
	}

	public boolean issetGetIp()
	{
		return this.GetIp_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return GetIp_u value 类型为:short
	 * 
	 */
	public short getGetIp_u()
	{
		return GetIp_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setGetIp_u(short value)
	{
		this.GetIp_u = value;
	}


	/**
	 * 获取领取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return GetMachineKey value 类型为:String
	 * 
	 */
	public String getGetMachineKey()
	{
		return GetMachineKey;
	}


	/**
	 * 设置领取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setGetMachineKey(String value)
	{
		this.GetMachineKey = value;
		this.GetMachineKey_u = 1;
	}

	public boolean issetGetMachineKey()
	{
		return this.GetMachineKey_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return GetMachineKey_u value 类型为:short
	 * 
	 */
	public short getGetMachineKey_u()
	{
		return GetMachineKey_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setGetMachineKey_u(short value)
	{
		this.GetMachineKey_u = value;
	}


	/**
	 * 获取领取时间
	 * 
	 * 此字段的版本 >= 0
	 * @return RecvTime value 类型为:long
	 * 
	 */
	public long getRecvTime()
	{
		return RecvTime;
	}


	/**
	 * 设置领取时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRecvTime(long value)
	{
		this.RecvTime = value;
		this.RecvTime_u = 1;
	}

	public boolean issetRecvTime()
	{
		return this.RecvTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return RecvTime_u value 类型为:short
	 * 
	 */
	public short getRecvTime_u()
	{
		return RecvTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRecvTime_u(short value)
	{
		this.RecvTime_u = value;
	}


	/**
	 * 获取使用ip
	 * 
	 * 此字段的版本 >= 0
	 * @return UseIp value 类型为:String
	 * 
	 */
	public String getUseIp()
	{
		return UseIp;
	}


	/**
	 * 设置使用ip
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setUseIp(String value)
	{
		this.UseIp = value;
		this.UseIp_u = 1;
	}

	public boolean issetUseIp()
	{
		return this.UseIp_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return UseIp_u value 类型为:short
	 * 
	 */
	public short getUseIp_u()
	{
		return UseIp_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUseIp_u(short value)
	{
		this.UseIp_u = value;
	}


	/**
	 * 获取使用机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return UseMachineKey value 类型为:String
	 * 
	 */
	public String getUseMachineKey()
	{
		return UseMachineKey;
	}


	/**
	 * 设置使用机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setUseMachineKey(String value)
	{
		this.UseMachineKey = value;
		this.UseMachineKey_u = 1;
	}

	public boolean issetUseMachineKey()
	{
		return this.UseMachineKey_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return UseMachineKey_u value 类型为:short
	 * 
	 */
	public short getUseMachineKey_u()
	{
		return UseMachineKey_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUseMachineKey_u(short value)
	{
		this.UseMachineKey_u = value;
	}


	/**
	 * 获取使用时间
	 * 
	 * 此字段的版本 >= 0
	 * @return UsedTime value 类型为:long
	 * 
	 */
	public long getUsedTime()
	{
		return UsedTime;
	}


	/**
	 * 设置使用时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUsedTime(long value)
	{
		this.UsedTime = value;
		this.UsedTime_u = 1;
	}

	public boolean issetUsedTime()
	{
		return this.UsedTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return UsedTime_u value 类型为:short
	 * 
	 */
	public short getUsedTime_u()
	{
		return UsedTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUsedTime_u(short value)
	{
		this.UsedTime_u = value;
	}


	/**
	 * 获取兑现编号
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawId value 类型为:long
	 * 
	 */
	public long getWithdrawId()
	{
		return WithdrawId;
	}


	/**
	 * 设置兑现编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWithdrawId(long value)
	{
		this.WithdrawId = value;
		this.WithdrawId_u = 1;
	}

	public boolean issetWithdrawId()
	{
		return this.WithdrawId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawId_u value 类型为:short
	 * 
	 */
	public short getWithdrawId_u()
	{
		return WithdrawId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWithdrawId_u(short value)
	{
		this.WithdrawId_u = value;
	}


	/**
	 * 获取兑现时间
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawTime value 类型为:long
	 * 
	 */
	public long getWithdrawTime()
	{
		return WithdrawTime;
	}


	/**
	 * 设置兑现时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWithdrawTime(long value)
	{
		this.WithdrawTime = value;
		this.WithdrawTime_u = 1;
	}

	public boolean issetWithdrawTime()
	{
		return this.WithdrawTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawTime_u value 类型为:short
	 * 
	 */
	public short getWithdrawTime_u()
	{
		return WithdrawTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWithdrawTime_u(short value)
	{
		this.WithdrawTime_u = value;
	}


	/**
	 * 获取兑现财付通时间
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawCftTime value 类型为:long
	 * 
	 */
	public long getWithdrawCftTime()
	{
		return WithdrawCftTime;
	}


	/**
	 * 设置兑现财付通时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWithdrawCftTime(long value)
	{
		this.WithdrawCftTime = value;
		this.WithdrawCftTime_u = 1;
	}

	public boolean issetWithdrawCftTime()
	{
		return this.WithdrawCftTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawCftTime_u value 类型为:short
	 * 
	 */
	public short getWithdrawCftTime_u()
	{
		return WithdrawCftTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWithdrawCftTime_u(short value)
	{
		this.WithdrawCftTime_u = value;
	}


	/**
	 * 获取实际抵扣金额
	 * 
	 * 此字段的版本 >= 0
	 * @return ActualPrice value 类型为:long
	 * 
	 */
	public long getActualPrice()
	{
		return ActualPrice;
	}


	/**
	 * 设置实际抵扣金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActualPrice(long value)
	{
		this.ActualPrice = value;
		this.ActualPrice_u = 1;
	}

	public boolean issetActualPrice()
	{
		return this.ActualPrice_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ActualPrice_u value 类型为:short
	 * 
	 */
	public short getActualPrice_u()
	{
		return ActualPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setActualPrice_u(short value)
	{
		this.ActualPrice_u = value;
	}


	/**
	 * 获取兑现金额
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawAmount value 类型为:long
	 * 
	 */
	public long getWithdrawAmount()
	{
		return WithdrawAmount;
	}


	/**
	 * 设置兑现金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWithdrawAmount(long value)
	{
		this.WithdrawAmount = value;
		this.WithdrawAmount_u = 1;
	}

	public boolean issetWithdrawAmount()
	{
		return this.WithdrawAmount_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawAmount_u value 类型为:short
	 * 
	 */
	public short getWithdrawAmount_u()
	{
		return WithdrawAmount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWithdrawAmount_u(short value)
	{
		this.WithdrawAmount_u = value;
	}


	/**
	 * 获取最低消费
	 * 
	 * 此字段的版本 >= 0
	 * @return Minimum value 类型为:long
	 * 
	 */
	public long getMinimum()
	{
		return Minimum;
	}


	/**
	 * 设置最低消费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMinimum(long value)
	{
		this.Minimum = value;
		this.Minimum_u = 1;
	}

	public boolean issetMinimum()
	{
		return this.Minimum_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Minimum_u value 类型为:short
	 * 
	 */
	public short getMinimum_u()
	{
		return Minimum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMinimum_u(short value)
	{
		this.Minimum_u = value;
	}


	/**
	 * 获取兑现比例
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawRate value 类型为:long
	 * 
	 */
	public long getWithdrawRate()
	{
		return WithdrawRate;
	}


	/**
	 * 设置兑现比例
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWithdrawRate(long value)
	{
		this.WithdrawRate = value;
		this.WithdrawRate_u = 1;
	}

	public boolean issetWithdrawRate()
	{
		return this.WithdrawRate_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawRate_u value 类型为:short
	 * 
	 */
	public short getWithdrawRate_u()
	{
		return WithdrawRate_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWithdrawRate_u(short value)
	{
		this.WithdrawRate_u = value;
	}


	/**
	 * 获取订单卖家uin
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置订单卖家uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
		this.SellerUin_u = 1;
	}

	public boolean issetSellerUin()
	{
		return this.SellerUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin_u value 类型为:short
	 * 
	 */
	public short getSellerUin_u()
	{
		return SellerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerUin_u(short value)
	{
		this.SellerUin_u = value;
	}


	/**
	 * 获取关联订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @return DealCode value 类型为:String
	 * 
	 */
	public String getDealCode()
	{
		return DealCode;
	}


	/**
	 * 设置关联订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealCode(String value)
	{
		this.DealCode = value;
		this.DealCode_u = 1;
	}

	public boolean issetDealCode()
	{
		return this.DealCode_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DealCode_u value 类型为:short
	 * 
	 */
	public short getDealCode_u()
	{
		return DealCode_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealCode_u(short value)
	{
		this.DealCode_u = value;
	}


	/**
	 * 获取关联地址
	 * 
	 * 此字段的版本 >= 0
	 * @return RelaUrl value 类型为:String
	 * 
	 */
	public String getRelaUrl()
	{
		return RelaUrl;
	}


	/**
	 * 设置关联地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRelaUrl(String value)
	{
		this.RelaUrl = value;
		this.RelaUrl_u = 1;
	}

	public boolean issetRelaUrl()
	{
		return this.RelaUrl_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return RelaUrl_u value 类型为:short
	 * 
	 */
	public short getRelaUrl_u()
	{
		return RelaUrl_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRelaUrl_u(short value)
	{
		this.RelaUrl_u = value;
	}


	/**
	 * 获取图片地址
	 * 
	 * 此字段的版本 >= 0
	 * @return ImageUrl value 类型为:String
	 * 
	 */
	public String getImageUrl()
	{
		return ImageUrl;
	}


	/**
	 * 设置图片地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setImageUrl(String value)
	{
		this.ImageUrl = value;
		this.ImageUrl_u = 1;
	}

	public boolean issetImageUrl()
	{
		return this.ImageUrl_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ImageUrl_u value 类型为:short
	 * 
	 */
	public short getImageUrl_u()
	{
		return ImageUrl_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setImageUrl_u(short value)
	{
		this.ImageUrl_u = value;
	}


	/**
	 * 获取红包状态
	 * 
	 * 此字段的版本 >= 0
	 * @return State value 类型为:long
	 * 
	 */
	public long getState()
	{
		return State;
	}


	/**
	 * 设置红包状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setState(long value)
	{
		this.State = value;
		this.State_u = 1;
	}

	public boolean issetState()
	{
		return this.State_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return State_u value 类型为:short
	 * 
	 */
	public short getState_u()
	{
		return State_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setState_u(short value)
	{
		this.State_u = value;
	}


	/**
	 * 获取适用店铺
	 * 
	 * 此字段的版本 >= 0
	 * @return ApplicableScope value 类型为:Set<uint32_t>
	 * 
	 */
	public Set<uint32_t> getApplicableScope()
	{
		return ApplicableScope;
	}


	/**
	 * 设置适用店铺
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Set<uint32_t>
	 * 
	 */
	public void setApplicableScope(Set<uint32_t> value)
	{
		if (value != null) {
				this.ApplicableScope = value;
				this.ApplicableScope_u = 1;
		}
	}

	public boolean issetApplicableScope()
	{
		return this.ApplicableScope_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ApplicableScope_u value 类型为:short
	 * 
	 */
	public short getApplicableScope_u()
	{
		return ApplicableScope_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setApplicableScope_u(short value)
	{
		this.ApplicableScope_u = value;
	}


	/**
	 * 获取最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return LastUpdateTime value 类型为:long
	 * 
	 */
	public long getLastUpdateTime()
	{
		return LastUpdateTime;
	}


	/**
	 * 设置最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastUpdateTime(long value)
	{
		this.LastUpdateTime = value;
		this.LastUpdateTime_u = 1;
	}

	public boolean issetLastUpdateTime()
	{
		return this.LastUpdateTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return LastUpdateTime_u value 类型为:short
	 * 
	 */
	public short getLastUpdateTime_u()
	{
		return LastUpdateTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastUpdateTime_u(short value)
	{
		this.LastUpdateTime_u = value;
	}


	/**
	 * 获取红包批次
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketStockInfo value 类型为:PacketStock
	 * 
	 */
	public PacketStock getPacketStockInfo()
	{
		return PacketStockInfo;
	}


	/**
	 * 设置红包批次
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:PacketStock
	 * 
	 */
	public void setPacketStockInfo(PacketStock value)
	{
		if (value != null) {
				this.PacketStockInfo = value;
				this.PacketStockInfo_u = 1;
		}
	}

	public boolean issetPacketStockInfo()
	{
		return this.PacketStockInfo_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketStockInfo_u value 类型为:short
	 * 
	 */
	public short getPacketStockInfo_u()
	{
		return PacketStockInfo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketStockInfo_u(short value)
	{
		this.PacketStockInfo_u = value;
	}


	/**
	 * 获取兑现记录
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawRecordInfo value 类型为:WithdrawRecord
	 * 
	 */
	public WithdrawRecord getWithdrawRecordInfo()
	{
		return WithdrawRecordInfo;
	}


	/**
	 * 设置兑现记录
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:WithdrawRecord
	 * 
	 */
	public void setWithdrawRecordInfo(WithdrawRecord value)
	{
		if (value != null) {
				this.WithdrawRecordInfo = value;
				this.WithdrawRecordInfo_u = 1;
		}
	}

	public boolean issetWithdrawRecordInfo()
	{
		return this.WithdrawRecordInfo_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawRecordInfo_u value 类型为:short
	 * 
	 */
	public short getWithdrawRecordInfo_u()
	{
		return WithdrawRecordInfo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWithdrawRecordInfo_u(short value)
	{
		this.WithdrawRecordInfo_u = value;
	}


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(PacketAndStockList)
				length += 17;  //计算字段PacketId的长度 size_of(uint64_t)
				length += 1;  //计算字段PacketId_u的长度 size_of(uint8_t)
				length += 17;  //计算字段PacketStockId的长度 size_of(uint64_t)
				length += 1;  //计算字段PacketStockId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(PacketName, null);  //计算字段PacketName的长度 size_of(String)
				length += 1;  //计算字段PacketName_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Type的长度 size_of(uint32_t)
				length += 1;  //计算字段Type_u的长度 size_of(uint8_t)
				length += 4;  //计算字段OwnerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段OwnerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PacketPrice的长度 size_of(uint32_t)
				length += 1;  //计算字段PacketPrice_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PacketFlag的长度 size_of(uint32_t)
				length += 1;  //计算字段PacketFlag_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BeginTime的长度 size_of(uint32_t)
				length += 1;  //计算字段BeginTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段EndTime的长度 size_of(uint32_t)
				length += 1;  //计算字段EndTime_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(GetIp, null);  //计算字段GetIp的长度 size_of(String)
				length += 1;  //计算字段GetIp_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(GetMachineKey, null);  //计算字段GetMachineKey的长度 size_of(String)
				length += 1;  //计算字段GetMachineKey_u的长度 size_of(uint8_t)
				length += 4;  //计算字段RecvTime的长度 size_of(uint32_t)
				length += 1;  //计算字段RecvTime_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(UseIp, null);  //计算字段UseIp的长度 size_of(String)
				length += 1;  //计算字段UseIp_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(UseMachineKey, null);  //计算字段UseMachineKey的长度 size_of(String)
				length += 1;  //计算字段UseMachineKey_u的长度 size_of(uint8_t)
				length += 4;  //计算字段UsedTime的长度 size_of(uint32_t)
				length += 1;  //计算字段UsedTime_u的长度 size_of(uint8_t)
				length += 17;  //计算字段WithdrawId的长度 size_of(uint64_t)
				length += 1;  //计算字段WithdrawId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段WithdrawTime的长度 size_of(uint32_t)
				length += 1;  //计算字段WithdrawTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段WithdrawCftTime的长度 size_of(uint32_t)
				length += 1;  //计算字段WithdrawCftTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ActualPrice的长度 size_of(uint32_t)
				length += 1;  //计算字段ActualPrice_u的长度 size_of(uint8_t)
				length += 4;  //计算字段WithdrawAmount的长度 size_of(uint32_t)
				length += 1;  //计算字段WithdrawAmount_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Minimum的长度 size_of(uint32_t)
				length += 1;  //计算字段Minimum_u的长度 size_of(uint8_t)
				length += 4;  //计算字段WithdrawRate的长度 size_of(uint32_t)
				length += 1;  //计算字段WithdrawRate_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(DealCode, null);  //计算字段DealCode的长度 size_of(String)
				length += 1;  //计算字段DealCode_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(RelaUrl, null);  //计算字段RelaUrl的长度 size_of(String)
				length += 1;  //计算字段RelaUrl_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ImageUrl, null);  //计算字段ImageUrl的长度 size_of(String)
				length += 1;  //计算字段ImageUrl_u的长度 size_of(uint8_t)
				length += 4;  //计算字段State的长度 size_of(uint32_t)
				length += 1;  //计算字段State_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ApplicableScope, null);  //计算字段ApplicableScope的长度 size_of(Set)
				length += 1;  //计算字段ApplicableScope_u的长度 size_of(uint8_t)
				length += 4;  //计算字段LastUpdateTime的长度 size_of(uint32_t)
				length += 1;  //计算字段LastUpdateTime_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(PacketStockInfo, null);  //计算字段PacketStockInfo的长度 size_of(PacketStock)
				length += 1;  //计算字段PacketStockInfo_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(WithdrawRecordInfo, null);  //计算字段WithdrawRecordInfo的长度 size_of(WithdrawRecord)
				length += 1;  //计算字段WithdrawRecordInfo_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(PacketAndStockList)
				length += 17;  //计算字段PacketId的长度 size_of(uint64_t)
				length += 1;  //计算字段PacketId_u的长度 size_of(uint8_t)
				length += 17;  //计算字段PacketStockId的长度 size_of(uint64_t)
				length += 1;  //计算字段PacketStockId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(PacketName, encoding);  //计算字段PacketName的长度 size_of(String)
				length += 1;  //计算字段PacketName_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Type的长度 size_of(uint32_t)
				length += 1;  //计算字段Type_u的长度 size_of(uint8_t)
				length += 4;  //计算字段OwnerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段OwnerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PacketPrice的长度 size_of(uint32_t)
				length += 1;  //计算字段PacketPrice_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PacketFlag的长度 size_of(uint32_t)
				length += 1;  //计算字段PacketFlag_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BeginTime的长度 size_of(uint32_t)
				length += 1;  //计算字段BeginTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段EndTime的长度 size_of(uint32_t)
				length += 1;  //计算字段EndTime_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(GetIp, encoding);  //计算字段GetIp的长度 size_of(String)
				length += 1;  //计算字段GetIp_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(GetMachineKey, encoding);  //计算字段GetMachineKey的长度 size_of(String)
				length += 1;  //计算字段GetMachineKey_u的长度 size_of(uint8_t)
				length += 4;  //计算字段RecvTime的长度 size_of(uint32_t)
				length += 1;  //计算字段RecvTime_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(UseIp, encoding);  //计算字段UseIp的长度 size_of(String)
				length += 1;  //计算字段UseIp_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(UseMachineKey, encoding);  //计算字段UseMachineKey的长度 size_of(String)
				length += 1;  //计算字段UseMachineKey_u的长度 size_of(uint8_t)
				length += 4;  //计算字段UsedTime的长度 size_of(uint32_t)
				length += 1;  //计算字段UsedTime_u的长度 size_of(uint8_t)
				length += 17;  //计算字段WithdrawId的长度 size_of(uint64_t)
				length += 1;  //计算字段WithdrawId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段WithdrawTime的长度 size_of(uint32_t)
				length += 1;  //计算字段WithdrawTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段WithdrawCftTime的长度 size_of(uint32_t)
				length += 1;  //计算字段WithdrawCftTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ActualPrice的长度 size_of(uint32_t)
				length += 1;  //计算字段ActualPrice_u的长度 size_of(uint8_t)
				length += 4;  //计算字段WithdrawAmount的长度 size_of(uint32_t)
				length += 1;  //计算字段WithdrawAmount_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Minimum的长度 size_of(uint32_t)
				length += 1;  //计算字段Minimum_u的长度 size_of(uint8_t)
				length += 4;  //计算字段WithdrawRate的长度 size_of(uint32_t)
				length += 1;  //计算字段WithdrawRate_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(DealCode, encoding);  //计算字段DealCode的长度 size_of(String)
				length += 1;  //计算字段DealCode_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(RelaUrl, encoding);  //计算字段RelaUrl的长度 size_of(String)
				length += 1;  //计算字段RelaUrl_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ImageUrl, encoding);  //计算字段ImageUrl的长度 size_of(String)
				length += 1;  //计算字段ImageUrl_u的长度 size_of(uint8_t)
				length += 4;  //计算字段State的长度 size_of(uint32_t)
				length += 1;  //计算字段State_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ApplicableScope, encoding);  //计算字段ApplicableScope的长度 size_of(Set)
				length += 1;  //计算字段ApplicableScope_u的长度 size_of(uint8_t)
				length += 4;  //计算字段LastUpdateTime的长度 size_of(uint32_t)
				length += 1;  //计算字段LastUpdateTime_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(PacketStockInfo, encoding);  //计算字段PacketStockInfo的长度 size_of(PacketStock)
				length += 1;  //计算字段PacketStockInfo_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(WithdrawRecordInfo, encoding);  //计算字段WithdrawRecordInfo的长度 size_of(WithdrawRecord)
				length += 1;  //计算字段WithdrawRecordInfo_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
