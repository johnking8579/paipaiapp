 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.redpacketapi.java

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *发放红包支持可重录回复
 *
 *@date 2014-10-16 05:56:16
 *
 *@since version:0
*/
public class  SendRedPacketV2Resp extends NetMessage
{
	/**
	 * 发送红包Result
	 *
	 * 版本 >= 0
	 */
	 private SendResult ResultInfo = new SendResult();

	/**
	 * 保留输出字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveOut = new String();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(ResultInfo);
		bs.pushString(ReserveOut);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		ResultInfo = (SendResult) bs.popObject(SendResult.class);
		ReserveOut = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x9111880AL;
	}


	/**
	 * 获取发送红包Result
	 * 
	 * 此字段的版本 >= 0
	 * @return ResultInfo value 类型为:SendResult
	 * 
	 */
	public SendResult getResultInfo()
	{
		return ResultInfo;
	}


	/**
	 * 设置发送红包Result
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:SendResult
	 * 
	 */
	public void setResultInfo(SendResult value)
	{
		if (value != null) {
				this.ResultInfo = value;
		}else{
				this.ResultInfo = new SendResult();
		}
	}


	/**
	 * 获取保留输出字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveOut value 类型为:String
	 * 
	 */
	public String getReserveOut()
	{
		return ReserveOut;
	}


	/**
	 * 设置保留输出字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveOut(String value)
	{
		this.ReserveOut = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SendRedPacketV2Resp)
				length += ByteStream.getObjectSize(ResultInfo, null);  //计算字段ResultInfo的长度 size_of(SendResult)
				length += ByteStream.getObjectSize(ReserveOut, null);  //计算字段ReserveOut的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(SendRedPacketV2Resp)
				length += ByteStream.getObjectSize(ResultInfo, encoding);  //计算字段ResultInfo的长度 size_of(SendResult)
				length += ByteStream.getObjectSize(ReserveOut, encoding);  //计算字段ReserveOut的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
