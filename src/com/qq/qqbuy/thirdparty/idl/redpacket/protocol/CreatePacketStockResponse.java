//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.CreatePacketStockResp.java

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *创建红包批次回复
 *
 *@date 2014-10-16 05:56:15
 *
 *@since version:0
*/
public class CreatePacketStockResponse  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 创建批次者uin
	 *
	 * 版本 >= 0
	 */
	 private long CreatorUin;

	/**
	 * 版本 >= 0
	 */
	 private short CreatorUin_u;

	/**
	 * 生成的红包批次
	 *
	 * 版本 >= 0
	 */
	 private PacketStock RespPacketStock = new PacketStock();

	/**
	 * 版本 >= 0
	 */
	 private short RespPacketStock_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(CreatorUin);
		bs.pushUByte(CreatorUin_u);
		bs.pushObject(RespPacketStock);
		bs.pushUByte(RespPacketStock_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		CreatorUin = bs.popUInt();
		CreatorUin_u = bs.popUByte();
		RespPacketStock = (PacketStock) bs.popObject(PacketStock.class);
		RespPacketStock_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取创建批次者uin
	 * 
	 * 此字段的版本 >= 0
	 * @return CreatorUin value 类型为:long
	 * 
	 */
	public long getCreatorUin()
	{
		return CreatorUin;
	}


	/**
	 * 设置创建批次者uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCreatorUin(long value)
	{
		this.CreatorUin = value;
		this.CreatorUin_u = 1;
	}

	public boolean issetCreatorUin()
	{
		return this.CreatorUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return CreatorUin_u value 类型为:short
	 * 
	 */
	public short getCreatorUin_u()
	{
		return CreatorUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCreatorUin_u(short value)
	{
		this.CreatorUin_u = value;
	}


	/**
	 * 获取生成的红包批次
	 * 
	 * 此字段的版本 >= 0
	 * @return RespPacketStock value 类型为:PacketStock
	 * 
	 */
	public PacketStock getRespPacketStock()
	{
		return RespPacketStock;
	}


	/**
	 * 设置生成的红包批次
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:PacketStock
	 * 
	 */
	public void setRespPacketStock(PacketStock value)
	{
		if (value != null) {
				this.RespPacketStock = value;
				this.RespPacketStock_u = 1;
		}
	}

	public boolean issetRespPacketStock()
	{
		return this.RespPacketStock_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return RespPacketStock_u value 类型为:short
	 * 
	 */
	public short getRespPacketStock_u()
	{
		return RespPacketStock_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRespPacketStock_u(short value)
	{
		this.RespPacketStock_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CreatePacketStockResponse)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段CreatorUin的长度 size_of(uint32_t)
				length += 1;  //计算字段CreatorUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(RespPacketStock, null);  //计算字段RespPacketStock的长度 size_of(PacketStock)
				length += 1;  //计算字段RespPacketStock_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(CreatePacketStockResponse)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段CreatorUin的长度 size_of(uint32_t)
				length += 1;  //计算字段CreatorUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(RespPacketStock, encoding);  //计算字段RespPacketStock的长度 size_of(PacketStock)
				length += 1;  //计算字段RespPacketStock_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
