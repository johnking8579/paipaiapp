//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.redpacket.redpacketapi.java

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import com.paipai.lang.uint64_t;
import com.paipai.lang.uint32_t;
import com.paipai.lang.MultiMap;

/**
 *发放失败的错误信息
 *
 *@date 2015-03-10 02:42:23
 *
 *@since version:0
*/
public class SendFailInfo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 接收者uin
	 *
	 * 版本 >= 0
	 */
	 private long RecvUin;

	/**
	 * 版本 >= 0
	 */
	 private short RecvUin_u;

	/**
	 * 发放失败的错误码列表
	 *
	 * 版本 >= 0
	 */
	 private MultiMap<uint64_t,uint32_t> SendFailItems = new MultiMap<uint64_t,uint32_t>();

	/**
	 * 版本 >= 0
	 */
	 private short SendFailItems_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(RecvUin);
		bs.pushUByte(RecvUin_u);
		bs.pushObject(SendFailItems);
		bs.pushUByte(SendFailItems_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		RecvUin = bs.popUInt();
		RecvUin_u = bs.popUByte();
		SendFailItems = (MultiMap<uint64_t,uint32_t>)bs.popMultiMap(uint64_t.class,uint32_t.class);
		SendFailItems_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取接收者uin
	 * 
	 * 此字段的版本 >= 0
	 * @return RecvUin value 类型为:long
	 * 
	 */
	public long getRecvUin()
	{
		return RecvUin;
	}


	/**
	 * 设置接收者uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRecvUin(long value)
	{
		this.RecvUin = value;
		this.RecvUin_u = 1;
	}

	public boolean issetRecvUin()
	{
		return this.RecvUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return RecvUin_u value 类型为:short
	 * 
	 */
	public short getRecvUin_u()
	{
		return RecvUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRecvUin_u(short value)
	{
		this.RecvUin_u = value;
	}


	/**
	 * 获取发放失败的错误码列表
	 * 
	 * 此字段的版本 >= 0
	 * @return SendFailItems value 类型为:MultiMap<uint64_t,uint32_t>
	 * 
	 */
	public MultiMap<uint64_t,uint32_t> getSendFailItems()
	{
		return SendFailItems;
	}


	/**
	 * 设置发放失败的错误码列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:MultiMap<uint64_t,uint32_t>
	 * 
	 */
	public void setSendFailItems(MultiMap<uint64_t,uint32_t> value)
	{
		if (value != null) {
				this.SendFailItems = value;
				this.SendFailItems_u = 1;
		}
	}

	public boolean issetSendFailItems()
	{
		return this.SendFailItems_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SendFailItems_u value 类型为:short
	 * 
	 */
	public short getSendFailItems_u()
	{
		return SendFailItems_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSendFailItems_u(short value)
	{
		this.SendFailItems_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SendFailInfo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段RecvUin的长度 size_of(uint32_t)
				length += 1;  //计算字段RecvUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(SendFailItems, null);  //计算字段SendFailItems的长度 size_of(MultiMap)
				length += 1;  //计算字段SendFailItems_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(SendFailInfo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段RecvUin的长度 size_of(uint32_t)
				length += 1;  //计算字段RecvUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(SendFailItems, encoding);  //计算字段SendFailItems的长度 size_of(MultiMap)
				length += 1;  //计算字段SendFailItems_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
