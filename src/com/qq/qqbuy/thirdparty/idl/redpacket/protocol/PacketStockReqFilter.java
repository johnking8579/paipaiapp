//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.GetPacketStockRequest.java

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import com.paipai.lang.uint64_t;
import java.util.Vector;

/**
 *红包批次过滤信息
 *
 *@date 2014-10-16 05:56:15
 *
 *@since version:0
*/
public class PacketStockReqFilter  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String ErrMsg = new String();

	/**
	 * 版本 >= 0
	 */
	 private short ErrMsg_u;

	/**
	 * 红包名称
	 *
	 * 版本 >= 0
	 */
	 private String PacketName = new String();

	/**
	 * 版本 >= 0
	 */
	 private short PacketName_u;

	/**
	 * 红包批次状态
	 *
	 * 版本 >= 0
	 */
	 private long State;

	/**
	 * 版本 >= 0
	 */
	 private short State_u;

	/**
	 * 红包批次列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint64_t> PacketStockIdList = new Vector<uint64_t>();

	/**
	 * 版本 >= 0
	 */
	 private short PacketStockIdList_u;

	/**
	 * 红包批次类型
	 *
	 * 版本 >= 0
	 */
	 private long Type;

	/**
	 * 版本 >= 0
	 */
	 private short Type_u;

	/**
	 * 红包批次发行开始时间
	 *
	 * 版本 >= 0
	 */
	 private long BeginIssueTime;

	/**
	 * 版本 >= 0
	 */
	 private short BeginIssueTime_u;

	/**
	 * 红包批次发行结束时间
	 *
	 * 版本 >= 0
	 */
	 private long EndIssueTime;

	/**
	 * 版本 >= 0
	 */
	 private short EndIssueTime_u;

	/**
	 * 开始位置
	 *
	 * 版本 >= 0
	 */
	 private long Offset;

	/**
	 * 版本 >= 0
	 */
	 private short Offset_u;

	/**
	 * 所需数目
	 *
	 * 版本 >= 0
	 */
	 private long Limit;

	/**
	 * 版本 >= 0
	 */
	 private short Limit_u;

	/**
	 * 红包批次uin
	 *
	 * 版本 >= 0
	 */
	 private long Uin;

	/**
	 * 版本 >= 0
	 */
	 private short Uin_u;

	/**
	 * 红包属性
	 *
	 * 版本 >= 0
	 */
	 private long Property;

	/**
	 * 版本 >= 0
	 */
	 private short Property_u;

	/**
	 * 红包批次有效期状态
	 *
	 * 版本 >= 0
	 */
	 private long IndateState;

	/**
	 * 版本 >= 0
	 */
	 private short IndateState_u;

	/**
	 * 红包批次发放状态
	 *
	 * 版本 >= 0
	 */
	 private long SendState;

	/**
	 * 版本 >= 0
	 */
	 private short SendState_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushString(ErrMsg);
		bs.pushUByte(ErrMsg_u);
		bs.pushString(PacketName);
		bs.pushUByte(PacketName_u);
		bs.pushUInt(State);
		bs.pushUByte(State_u);
		bs.pushObject(PacketStockIdList);
		bs.pushUByte(PacketStockIdList_u);
		bs.pushUInt(Type);
		bs.pushUByte(Type_u);
		bs.pushUInt(BeginIssueTime);
		bs.pushUByte(BeginIssueTime_u);
		bs.pushUInt(EndIssueTime);
		bs.pushUByte(EndIssueTime_u);
		bs.pushUInt(Offset);
		bs.pushUByte(Offset_u);
		bs.pushUInt(Limit);
		bs.pushUByte(Limit_u);
		bs.pushUInt(Uin);
		bs.pushUByte(Uin_u);
		bs.pushUInt(Property);
		bs.pushUByte(Property_u);
		bs.pushUInt(IndateState);
		bs.pushUByte(IndateState_u);
		bs.pushUInt(SendState);
		bs.pushUByte(SendState_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		ErrMsg = bs.popString();
		ErrMsg_u = bs.popUByte();
		PacketName = bs.popString();
		PacketName_u = bs.popUByte();
		State = bs.popUInt();
		State_u = bs.popUByte();
		PacketStockIdList = (Vector<uint64_t>)bs.popVector(uint64_t.class);
		PacketStockIdList_u = bs.popUByte();
		Type = bs.popUInt();
		Type_u = bs.popUByte();
		BeginIssueTime = bs.popUInt();
		BeginIssueTime_u = bs.popUByte();
		EndIssueTime = bs.popUInt();
		EndIssueTime_u = bs.popUByte();
		Offset = bs.popUInt();
		Offset_u = bs.popUByte();
		Limit = bs.popUInt();
		Limit_u = bs.popUByte();
		Uin = bs.popUInt();
		Uin_u = bs.popUByte();
		Property = bs.popUInt();
		Property_u = bs.popUByte();
		IndateState = bs.popUInt();
		IndateState_u = bs.popUByte();
		SendState = bs.popUInt();
		SendState_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return ErrMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return ErrMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.ErrMsg = value;
		this.ErrMsg_u = 1;
	}

	public boolean issetErrMsg()
	{
		return this.ErrMsg_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ErrMsg_u value 类型为:short
	 * 
	 */
	public short getErrMsg_u()
	{
		return ErrMsg_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setErrMsg_u(short value)
	{
		this.ErrMsg_u = value;
	}


	/**
	 * 获取红包名称
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketName value 类型为:String
	 * 
	 */
	public String getPacketName()
	{
		return PacketName;
	}


	/**
	 * 设置红包名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPacketName(String value)
	{
		this.PacketName = value;
		this.PacketName_u = 1;
	}

	public boolean issetPacketName()
	{
		return this.PacketName_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketName_u value 类型为:short
	 * 
	 */
	public short getPacketName_u()
	{
		return PacketName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketName_u(short value)
	{
		this.PacketName_u = value;
	}


	/**
	 * 获取红包批次状态
	 * 
	 * 此字段的版本 >= 0
	 * @return State value 类型为:long
	 * 
	 */
	public long getState()
	{
		return State;
	}


	/**
	 * 设置红包批次状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setState(long value)
	{
		this.State = value;
		this.State_u = 1;
	}

	public boolean issetState()
	{
		return this.State_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return State_u value 类型为:short
	 * 
	 */
	public short getState_u()
	{
		return State_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setState_u(short value)
	{
		this.State_u = value;
	}


	/**
	 * 获取红包批次列表
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketStockIdList value 类型为:Vector<uint64_t>
	 * 
	 */
	public Vector<uint64_t> getPacketStockIdList()
	{
		return PacketStockIdList;
	}


	/**
	 * 设置红包批次列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint64_t>
	 * 
	 */
	public void setPacketStockIdList(Vector<uint64_t> value)
	{
		if (value != null) {
				this.PacketStockIdList = value;
				this.PacketStockIdList_u = 1;
		}
	}

	public boolean issetPacketStockIdList()
	{
		return this.PacketStockIdList_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketStockIdList_u value 类型为:short
	 * 
	 */
	public short getPacketStockIdList_u()
	{
		return PacketStockIdList_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketStockIdList_u(short value)
	{
		this.PacketStockIdList_u = value;
	}


	/**
	 * 获取红包批次类型
	 * 
	 * 此字段的版本 >= 0
	 * @return Type value 类型为:long
	 * 
	 */
	public long getType()
	{
		return Type;
	}


	/**
	 * 设置红包批次类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setType(long value)
	{
		this.Type = value;
		this.Type_u = 1;
	}

	public boolean issetType()
	{
		return this.Type_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Type_u value 类型为:short
	 * 
	 */
	public short getType_u()
	{
		return Type_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setType_u(short value)
	{
		this.Type_u = value;
	}


	/**
	 * 获取红包批次发行开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @return BeginIssueTime value 类型为:long
	 * 
	 */
	public long getBeginIssueTime()
	{
		return BeginIssueTime;
	}


	/**
	 * 设置红包批次发行开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBeginIssueTime(long value)
	{
		this.BeginIssueTime = value;
		this.BeginIssueTime_u = 1;
	}

	public boolean issetBeginIssueTime()
	{
		return this.BeginIssueTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BeginIssueTime_u value 类型为:short
	 * 
	 */
	public short getBeginIssueTime_u()
	{
		return BeginIssueTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBeginIssueTime_u(short value)
	{
		this.BeginIssueTime_u = value;
	}


	/**
	 * 获取红包批次发行结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return EndIssueTime value 类型为:long
	 * 
	 */
	public long getEndIssueTime()
	{
		return EndIssueTime;
	}


	/**
	 * 设置红包批次发行结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEndIssueTime(long value)
	{
		this.EndIssueTime = value;
		this.EndIssueTime_u = 1;
	}

	public boolean issetEndIssueTime()
	{
		return this.EndIssueTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return EndIssueTime_u value 类型为:short
	 * 
	 */
	public short getEndIssueTime_u()
	{
		return EndIssueTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEndIssueTime_u(short value)
	{
		this.EndIssueTime_u = value;
	}


	/**
	 * 获取开始位置
	 * 
	 * 此字段的版本 >= 0
	 * @return Offset value 类型为:long
	 * 
	 */
	public long getOffset()
	{
		return Offset;
	}


	/**
	 * 设置开始位置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOffset(long value)
	{
		this.Offset = value;
		this.Offset_u = 1;
	}

	public boolean issetOffset()
	{
		return this.Offset_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Offset_u value 类型为:short
	 * 
	 */
	public short getOffset_u()
	{
		return Offset_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOffset_u(short value)
	{
		this.Offset_u = value;
	}


	/**
	 * 获取所需数目
	 * 
	 * 此字段的版本 >= 0
	 * @return Limit value 类型为:long
	 * 
	 */
	public long getLimit()
	{
		return Limit;
	}


	/**
	 * 设置所需数目
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLimit(long value)
	{
		this.Limit = value;
		this.Limit_u = 1;
	}

	public boolean issetLimit()
	{
		return this.Limit_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Limit_u value 类型为:short
	 * 
	 */
	public short getLimit_u()
	{
		return Limit_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLimit_u(short value)
	{
		this.Limit_u = value;
	}


	/**
	 * 获取红包批次uin
	 * 
	 * 此字段的版本 >= 0
	 * @return Uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return Uin;
	}


	/**
	 * 设置红包批次uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.Uin = value;
		this.Uin_u = 1;
	}

	public boolean issetUin()
	{
		return this.Uin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Uin_u value 类型为:short
	 * 
	 */
	public short getUin_u()
	{
		return Uin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUin_u(short value)
	{
		this.Uin_u = value;
	}


	/**
	 * 获取红包属性
	 * 
	 * 此字段的版本 >= 0
	 * @return Property value 类型为:long
	 * 
	 */
	public long getProperty()
	{
		return Property;
	}


	/**
	 * 设置红包属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setProperty(long value)
	{
		this.Property = value;
		this.Property_u = 1;
	}

	public boolean issetProperty()
	{
		return this.Property_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Property_u value 类型为:short
	 * 
	 */
	public short getProperty_u()
	{
		return Property_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProperty_u(short value)
	{
		this.Property_u = value;
	}


	/**
	 * 获取红包批次有效期状态
	 * 
	 * 此字段的版本 >= 0
	 * @return IndateState value 类型为:long
	 * 
	 */
	public long getIndateState()
	{
		return IndateState;
	}


	/**
	 * 设置红包批次有效期状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setIndateState(long value)
	{
		this.IndateState = value;
		this.IndateState_u = 1;
	}

	public boolean issetIndateState()
	{
		return this.IndateState_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return IndateState_u value 类型为:short
	 * 
	 */
	public short getIndateState_u()
	{
		return IndateState_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIndateState_u(short value)
	{
		this.IndateState_u = value;
	}


	/**
	 * 获取红包批次发放状态
	 * 
	 * 此字段的版本 >= 0
	 * @return SendState value 类型为:long
	 * 
	 */
	public long getSendState()
	{
		return SendState;
	}


	/**
	 * 设置红包批次发放状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSendState(long value)
	{
		this.SendState = value;
		this.SendState_u = 1;
	}

	public boolean issetSendState()
	{
		return this.SendState_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SendState_u value 类型为:short
	 * 
	 */
	public short getSendState_u()
	{
		return SendState_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSendState_u(short value)
	{
		this.SendState_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(PacketStockReqFilter)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ErrMsg, null);  //计算字段ErrMsg的长度 size_of(String)
				length += 1;  //计算字段ErrMsg_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(PacketName, null);  //计算字段PacketName的长度 size_of(String)
				length += 1;  //计算字段PacketName_u的长度 size_of(uint8_t)
				length += 4;  //计算字段State的长度 size_of(uint32_t)
				length += 1;  //计算字段State_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(PacketStockIdList, null);  //计算字段PacketStockIdList的长度 size_of(Vector)
				length += 1;  //计算字段PacketStockIdList_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Type的长度 size_of(uint32_t)
				length += 1;  //计算字段Type_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BeginIssueTime的长度 size_of(uint32_t)
				length += 1;  //计算字段BeginIssueTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段EndIssueTime的长度 size_of(uint32_t)
				length += 1;  //计算字段EndIssueTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Offset的长度 size_of(uint32_t)
				length += 1;  //计算字段Offset_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Limit的长度 size_of(uint32_t)
				length += 1;  //计算字段Limit_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Uin的长度 size_of(uint32_t)
				length += 1;  //计算字段Uin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Property的长度 size_of(uint32_t)
				length += 1;  //计算字段Property_u的长度 size_of(uint8_t)
				length += 4;  //计算字段IndateState的长度 size_of(uint32_t)
				length += 1;  //计算字段IndateState_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SendState的长度 size_of(uint32_t)
				length += 1;  //计算字段SendState_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(PacketStockReqFilter)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ErrMsg, encoding);  //计算字段ErrMsg的长度 size_of(String)
				length += 1;  //计算字段ErrMsg_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(PacketName, encoding);  //计算字段PacketName的长度 size_of(String)
				length += 1;  //计算字段PacketName_u的长度 size_of(uint8_t)
				length += 4;  //计算字段State的长度 size_of(uint32_t)
				length += 1;  //计算字段State_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(PacketStockIdList, encoding);  //计算字段PacketStockIdList的长度 size_of(Vector)
				length += 1;  //计算字段PacketStockIdList_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Type的长度 size_of(uint32_t)
				length += 1;  //计算字段Type_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BeginIssueTime的长度 size_of(uint32_t)
				length += 1;  //计算字段BeginIssueTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段EndIssueTime的长度 size_of(uint32_t)
				length += 1;  //计算字段EndIssueTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Offset的长度 size_of(uint32_t)
				length += 1;  //计算字段Offset_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Limit的长度 size_of(uint32_t)
				length += 1;  //计算字段Limit_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Uin的长度 size_of(uint32_t)
				length += 1;  //计算字段Uin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Property的长度 size_of(uint32_t)
				length += 1;  //计算字段Property_u的长度 size_of(uint8_t)
				length += 4;  //计算字段IndateState的长度 size_of(uint32_t)
				length += 1;  //计算字段IndateState_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SendState的长度 size_of(uint32_t)
				length += 1;  //计算字段SendState_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
