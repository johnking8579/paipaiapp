//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.BindRedPacketRequest.java

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *绑定红包信息
 *
 *@date 2014-10-16 05:56:15
 *
 *@since version:0
*/
public class BindRedPacketInfo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 红包编号
	 *
	 * 版本 >= 0
	 */
	 private long PacketId;

	/**
	 * 版本 >= 0
	 */
	 private short PacketId_u;

	/**
	 * 卖家QQ
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * 版本 >= 0
	 */
	 private short SellerUin_u;

	/**
	 * 订单编号
	 *
	 * 版本 >= 0
	 */
	 private String DealCode = new String();

	/**
	 * 版本 >= 0
	 */
	 private short DealCode_u;

	/**
	 * 实际抵扣金额
	 *
	 * 版本 >= 0
	 */
	 private long ActualPrice;

	/**
	 * 版本 >= 0
	 */
	 private short ActualPrice_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushLong(PacketId);
		bs.pushUByte(PacketId_u);
		bs.pushUInt(SellerUin);
		bs.pushUByte(SellerUin_u);
		bs.pushString(DealCode);
		bs.pushUByte(DealCode_u);
		bs.pushUInt(ActualPrice);
		bs.pushUByte(ActualPrice_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		PacketId = bs.popLong();
		PacketId_u = bs.popUByte();
		SellerUin = bs.popUInt();
		SellerUin_u = bs.popUByte();
		DealCode = bs.popString();
		DealCode_u = bs.popUByte();
		ActualPrice = bs.popUInt();
		ActualPrice_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取红包编号
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketId value 类型为:long
	 * 
	 */
	public long getPacketId()
	{
		return PacketId;
	}


	/**
	 * 设置红包编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPacketId(long value)
	{
		this.PacketId = value;
		this.PacketId_u = 1;
	}

	public boolean issetPacketId()
	{
		return this.PacketId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketId_u value 类型为:short
	 * 
	 */
	public short getPacketId_u()
	{
		return PacketId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketId_u(short value)
	{
		this.PacketId_u = value;
	}


	/**
	 * 获取卖家QQ
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置卖家QQ
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
		this.SellerUin_u = 1;
	}

	public boolean issetSellerUin()
	{
		return this.SellerUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin_u value 类型为:short
	 * 
	 */
	public short getSellerUin_u()
	{
		return SellerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerUin_u(short value)
	{
		this.SellerUin_u = value;
	}


	/**
	 * 获取订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @return DealCode value 类型为:String
	 * 
	 */
	public String getDealCode()
	{
		return DealCode;
	}


	/**
	 * 设置订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealCode(String value)
	{
		this.DealCode = value;
		this.DealCode_u = 1;
	}

	public boolean issetDealCode()
	{
		return this.DealCode_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DealCode_u value 类型为:short
	 * 
	 */
	public short getDealCode_u()
	{
		return DealCode_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealCode_u(short value)
	{
		this.DealCode_u = value;
	}


	/**
	 * 获取实际抵扣金额
	 * 
	 * 此字段的版本 >= 0
	 * @return ActualPrice value 类型为:long
	 * 
	 */
	public long getActualPrice()
	{
		return ActualPrice;
	}


	/**
	 * 设置实际抵扣金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActualPrice(long value)
	{
		this.ActualPrice = value;
		this.ActualPrice_u = 1;
	}

	public boolean issetActualPrice()
	{
		return this.ActualPrice_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ActualPrice_u value 类型为:short
	 * 
	 */
	public short getActualPrice_u()
	{
		return ActualPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setActualPrice_u(short value)
	{
		this.ActualPrice_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(BindRedPacketInfo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段PacketId的长度 size_of(uint64_t)
				length += 1;  //计算字段PacketId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(DealCode, null);  //计算字段DealCode的长度 size_of(String)
				length += 1;  //计算字段DealCode_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ActualPrice的长度 size_of(uint32_t)
				length += 1;  //计算字段ActualPrice_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(BindRedPacketInfo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段PacketId的长度 size_of(uint64_t)
				length += 1;  //计算字段PacketId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(DealCode, encoding);  //计算字段DealCode的长度 size_of(String)
				length += 1;  //计算字段DealCode_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ActualPrice的长度 size_of(uint32_t)
				length += 1;  //计算字段ActualPrice_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
