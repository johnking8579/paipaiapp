 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.redpacketapi.java

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *发放红包支持可重录请求
 *
 *@date 2014-10-16 05:56:16
 *
 *@since version:0
*/
public class  SendRedPacketV2Req extends NetMessage
{
	/**
	 * 请求来源描述
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 场景id
	 *
	 * 版本 >= 0
	 */
	 private long SceneId;

	/**
	 * 请求ip
	 *
	 * 版本 >= 0
	 */
	 private String ReqIp = new String();

	/**
	 * 发放红包Filter
	 *
	 * 版本 >= 0
	 */
	 private SendFilter FilterInfo = new SendFilter();

	/**
	 * 请求MachineKey
	 *
	 * 版本 >= 0
	 */
	 private String MachineKey = new String();

	/**
	 * 保留输入字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveIn = new String();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(Source);
		bs.pushUInt(SceneId);
		bs.pushString(ReqIp);
		bs.pushObject(FilterInfo);
		bs.pushString(MachineKey);
		bs.pushString(ReserveIn);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		Source = bs.popString();
		SceneId = bs.popUInt();
		ReqIp = bs.popString();
		FilterInfo = (SendFilter) bs.popObject(SendFilter.class);
		MachineKey = bs.popString();
		ReserveIn = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x9111180AL;
	}


	/**
	 * 获取请求来源描述
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置请求来源描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取场景id
	 * 
	 * 此字段的版本 >= 0
	 * @return SceneId value 类型为:long
	 * 
	 */
	public long getSceneId()
	{
		return SceneId;
	}


	/**
	 * 设置场景id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSceneId(long value)
	{
		this.SceneId = value;
	}


	/**
	 * 获取请求ip
	 * 
	 * 此字段的版本 >= 0
	 * @return ReqIp value 类型为:String
	 * 
	 */
	public String getReqIp()
	{
		return ReqIp;
	}


	/**
	 * 设置请求ip
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReqIp(String value)
	{
		this.ReqIp = value;
	}


	/**
	 * 获取发放红包Filter
	 * 
	 * 此字段的版本 >= 0
	 * @return FilterInfo value 类型为:SendFilter
	 * 
	 */
	public SendFilter getFilterInfo()
	{
		return FilterInfo;
	}


	/**
	 * 设置发放红包Filter
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:SendFilter
	 * 
	 */
	public void setFilterInfo(SendFilter value)
	{
		if (value != null) {
				this.FilterInfo = value;
		}else{
				this.FilterInfo = new SendFilter();
		}
	}


	/**
	 * 获取请求MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @return MachineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return MachineKey;
	}


	/**
	 * 设置请求MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.MachineKey = value;
	}


	/**
	 * 获取保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveIn value 类型为:String
	 * 
	 */
	public String getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveIn(String value)
	{
		this.ReserveIn = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(SendRedPacketV2Req)
				length += ByteStream.getObjectSize(Source, null);  //计算字段Source的长度 size_of(String)
				length += 4;  //计算字段SceneId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ReqIp, null);  //计算字段ReqIp的长度 size_of(String)
				length += ByteStream.getObjectSize(FilterInfo, null);  //计算字段FilterInfo的长度 size_of(SendFilter)
				length += ByteStream.getObjectSize(MachineKey, null);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveIn, null);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(SendRedPacketV2Req)
				length += ByteStream.getObjectSize(Source, encoding);  //计算字段Source的长度 size_of(String)
				length += 4;  //计算字段SceneId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ReqIp, encoding);  //计算字段ReqIp的长度 size_of(String)
				length += ByteStream.getObjectSize(FilterInfo, encoding);  //计算字段FilterInfo的长度 size_of(SendFilter)
				length += ByteStream.getObjectSize(MachineKey, encoding);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveIn, encoding);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
