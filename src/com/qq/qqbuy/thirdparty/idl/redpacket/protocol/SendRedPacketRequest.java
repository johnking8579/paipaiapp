//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.redpacket.SendRedPacketReq.java

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *发放红包请求
 *
 *@date 2015-03-10 02:42:23
 *
 *@since version:0
*/
public class SendRedPacketRequest  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 发放者uin
	 *
	 * 版本 >= 0
	 */
	 private long SendUin;

	/**
	 * 版本 >= 0
	 */
	 private short SendUin_u;

	/**
	 * 等待发放列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<RedPacketInfo> WaitSendInfo = new Vector<RedPacketInfo>();

	/**
	 * 版本 >= 0
	 */
	 private short WaitSendInfo_u;

	/**
	 * cookies中VisitKey
	 *
	 * 版本 >= 0
	 */
	 private String VisitKey = new String();

	/**
	 * 版本 >= 0
	 */
	 private short VisitKey_u;

	/**
	 * 客户端IP
	 *
	 * 版本 >= 0
	 */
	 private String ClientIP = new String();

	/**
	 * 版本 >= 0
	 */
	 private short ClientIP_u;

	/**
	 * http请求中User-Agent
	 *
	 * 版本 >= 0
	 */
	 private String UserAgent = new String();

	/**
	 * 版本 >= 0
	 */
	 private short UserAgent_u;

	/**
	 * http请求中Referer
	 *
	 * 版本 >= 0
	 */
	 private String Referer = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Referer_u;

	/**
	 * http请求中Url
	 *
	 * 版本 >= 0
	 */
	 private String Url = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Url_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(SendUin);
		bs.pushUByte(SendUin_u);
		bs.pushObject(WaitSendInfo);
		bs.pushUByte(WaitSendInfo_u);
		bs.pushString(VisitKey);
		bs.pushUByte(VisitKey_u);
		bs.pushString(ClientIP);
		bs.pushUByte(ClientIP_u);
		bs.pushString(UserAgent);
		bs.pushUByte(UserAgent_u);
		bs.pushString(Referer);
		bs.pushUByte(Referer_u);
		bs.pushString(Url);
		bs.pushUByte(Url_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		SendUin = bs.popUInt();
		SendUin_u = bs.popUByte();
		WaitSendInfo = (Vector<RedPacketInfo>)bs.popVector(RedPacketInfo.class);
		WaitSendInfo_u = bs.popUByte();
		VisitKey = bs.popString();
		VisitKey_u = bs.popUByte();
		ClientIP = bs.popString();
		ClientIP_u = bs.popUByte();
		UserAgent = bs.popString();
		UserAgent_u = bs.popUByte();
		Referer = bs.popString();
		Referer_u = bs.popUByte();
		Url = bs.popString();
		Url_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取发放者uin
	 * 
	 * 此字段的版本 >= 0
	 * @return SendUin value 类型为:long
	 * 
	 */
	public long getSendUin()
	{
		return SendUin;
	}


	/**
	 * 设置发放者uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSendUin(long value)
	{
		this.SendUin = value;
		this.SendUin_u = 1;
	}

	public boolean issetSendUin()
	{
		return this.SendUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SendUin_u value 类型为:short
	 * 
	 */
	public short getSendUin_u()
	{
		return SendUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSendUin_u(short value)
	{
		this.SendUin_u = value;
	}


	/**
	 * 获取等待发放列表
	 * 
	 * 此字段的版本 >= 0
	 * @return WaitSendInfo value 类型为:Vector<RedPacketInfo>
	 * 
	 */
	public Vector<RedPacketInfo> getWaitSendInfo()
	{
		return WaitSendInfo;
	}


	/**
	 * 设置等待发放列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<RedPacketInfo>
	 * 
	 */
	public void setWaitSendInfo(Vector<RedPacketInfo> value)
	{
		if (value != null) {
				this.WaitSendInfo = value;
				this.WaitSendInfo_u = 1;
		}
	}

	public boolean issetWaitSendInfo()
	{
		return this.WaitSendInfo_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return WaitSendInfo_u value 类型为:short
	 * 
	 */
	public short getWaitSendInfo_u()
	{
		return WaitSendInfo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWaitSendInfo_u(short value)
	{
		this.WaitSendInfo_u = value;
	}


	/**
	 * 获取cookies中VisitKey
	 * 
	 * 此字段的版本 >= 0
	 * @return VisitKey value 类型为:String
	 * 
	 */
	public String getVisitKey()
	{
		return VisitKey;
	}


	/**
	 * 设置cookies中VisitKey
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setVisitKey(String value)
	{
		this.VisitKey = value;
		this.VisitKey_u = 1;
	}

	public boolean issetVisitKey()
	{
		return this.VisitKey_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return VisitKey_u value 类型为:short
	 * 
	 */
	public short getVisitKey_u()
	{
		return VisitKey_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVisitKey_u(short value)
	{
		this.VisitKey_u = value;
	}


	/**
	 * 获取客户端IP
	 * 
	 * 此字段的版本 >= 0
	 * @return ClientIP value 类型为:String
	 * 
	 */
	public String getClientIP()
	{
		return ClientIP;
	}


	/**
	 * 设置客户端IP
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setClientIP(String value)
	{
		this.ClientIP = value;
		this.ClientIP_u = 1;
	}

	public boolean issetClientIP()
	{
		return this.ClientIP_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ClientIP_u value 类型为:short
	 * 
	 */
	public short getClientIP_u()
	{
		return ClientIP_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setClientIP_u(short value)
	{
		this.ClientIP_u = value;
	}


	/**
	 * 获取http请求中User-Agent
	 * 
	 * 此字段的版本 >= 0
	 * @return UserAgent value 类型为:String
	 * 
	 */
	public String getUserAgent()
	{
		return UserAgent;
	}


	/**
	 * 设置http请求中User-Agent
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setUserAgent(String value)
	{
		this.UserAgent = value;
		this.UserAgent_u = 1;
	}

	public boolean issetUserAgent()
	{
		return this.UserAgent_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return UserAgent_u value 类型为:short
	 * 
	 */
	public short getUserAgent_u()
	{
		return UserAgent_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUserAgent_u(short value)
	{
		this.UserAgent_u = value;
	}


	/**
	 * 获取http请求中Referer
	 * 
	 * 此字段的版本 >= 0
	 * @return Referer value 类型为:String
	 * 
	 */
	public String getReferer()
	{
		return Referer;
	}


	/**
	 * 设置http请求中Referer
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReferer(String value)
	{
		this.Referer = value;
		this.Referer_u = 1;
	}

	public boolean issetReferer()
	{
		return this.Referer_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Referer_u value 类型为:short
	 * 
	 */
	public short getReferer_u()
	{
		return Referer_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReferer_u(short value)
	{
		this.Referer_u = value;
	}


	/**
	 * 获取http请求中Url
	 * 
	 * 此字段的版本 >= 0
	 * @return Url value 类型为:String
	 * 
	 */
	public String getUrl()
	{
		return Url;
	}


	/**
	 * 设置http请求中Url
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setUrl(String value)
	{
		this.Url = value;
		this.Url_u = 1;
	}

	public boolean issetUrl()
	{
		return this.Url_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Url_u value 类型为:short
	 * 
	 */
	public short getUrl_u()
	{
		return Url_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUrl_u(short value)
	{
		this.Url_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SendRedPacketRequest)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SendUin的长度 size_of(uint32_t)
				length += 1;  //计算字段SendUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(WaitSendInfo, null);  //计算字段WaitSendInfo的长度 size_of(Vector)
				length += 1;  //计算字段WaitSendInfo_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(VisitKey, null);  //计算字段VisitKey的长度 size_of(String)
				length += 1;  //计算字段VisitKey_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ClientIP, null);  //计算字段ClientIP的长度 size_of(String)
				length += 1;  //计算字段ClientIP_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(UserAgent, null);  //计算字段UserAgent的长度 size_of(String)
				length += 1;  //计算字段UserAgent_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Referer, null);  //计算字段Referer的长度 size_of(String)
				length += 1;  //计算字段Referer_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Url, null);  //计算字段Url的长度 size_of(String)
				length += 1;  //计算字段Url_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(SendRedPacketRequest)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SendUin的长度 size_of(uint32_t)
				length += 1;  //计算字段SendUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(WaitSendInfo, encoding);  //计算字段WaitSendInfo的长度 size_of(Vector)
				length += 1;  //计算字段WaitSendInfo_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(VisitKey, encoding);  //计算字段VisitKey的长度 size_of(String)
				length += 1;  //计算字段VisitKey_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ClientIP, encoding);  //计算字段ClientIP的长度 size_of(String)
				length += 1;  //计算字段ClientIP_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(UserAgent, encoding);  //计算字段UserAgent的长度 size_of(String)
				length += 1;  //计算字段UserAgent_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Referer, encoding);  //计算字段Referer的长度 size_of(String)
				length += 1;  //计算字段Referer_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Url, encoding);  //计算字段Url的长度 size_of(String)
				length += 1;  //计算字段Url_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
