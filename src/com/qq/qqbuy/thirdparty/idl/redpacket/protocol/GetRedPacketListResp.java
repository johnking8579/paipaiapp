 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Vector;

/**
 *获取红包列表返回
 *
 *@date 2013-08-20 05:45::21
 *
 *@since version:0
*/
public class  GetRedPacketListResp implements IServiceObject
{
	public long result;
	/**
	 * 订单列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<RedPacket> RedPacketList = new Vector<RedPacket>();

	/**
	 * 符合条件总数
	 *
	 * 版本 >= 0
	 */
	 private long TotalNum;

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String ErrMsg = new String();

	/**
	 * 保留输出字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveOut = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(RedPacketList);
		bs.pushUInt(TotalNum);
		bs.pushString(ErrMsg);
		bs.pushString(ReserveOut);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		RedPacketList = (Vector<RedPacket>)bs.popVector(RedPacket.class);
		TotalNum = bs.popUInt();
		ErrMsg = bs.popString();
		ReserveOut = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x91118801L;
	}


	/**
	 * 获取订单列表
	 * 
	 * 此字段的版本 >= 0
	 * @return RedPacketList value 类型为:Vector<RedPacket>
	 * 
	 */
	public Vector<RedPacket> getRedPacketList()
	{
		return RedPacketList;
	}


	/**
	 * 设置订单列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<RedPacket>
	 * 
	 */
	public void setRedPacketList(Vector<RedPacket> value)
	{
		if (value != null) {
				this.RedPacketList = value;
		}else{
				this.RedPacketList = new Vector<RedPacket>();
		}
	}


	/**
	 * 获取符合条件总数
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalNum value 类型为:long
	 * 
	 */
	public long getTotalNum()
	{
		return TotalNum;
	}


	/**
	 * 设置符合条件总数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalNum(long value)
	{
		this.TotalNum = value;
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return ErrMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return ErrMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		if (value != null) {
				this.ErrMsg = value;
		}else{
				this.ErrMsg = new String();
		}
	}


	/**
	 * 获取保留输出字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveOut value 类型为:String
	 * 
	 */
	public String getReserveOut()
	{
		return ReserveOut;
	}


	/**
	 * 设置保留输出字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveOut(String value)
	{
		if (value != null) {
				this.ReserveOut = value;
		}else{
				this.ReserveOut = new String();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetRedPacketListResp)
				length += ByteStream.getObjectSize(RedPacketList);  //计算字段RedPacketList的长度 size_of(Vector)
				length += 4;  //计算字段TotalNum的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ErrMsg);  //计算字段ErrMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveOut);  //计算字段ReserveOut的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
