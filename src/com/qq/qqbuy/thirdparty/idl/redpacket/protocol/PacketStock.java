//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.CreatePacketStockRequest.java

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import com.paipai.lang.uint32_t;
import java.util.HashSet;
import java.util.Set;

/**
 *红包批次
 *
 *@date 2014-10-16 05:56:15
 *
 *@since version:0
*/
public class PacketStock  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String ErrMsg = new String();

	/**
	 * 版本 >= 0
	 */
	 private short ErrMsg_u;

	/**
	 * 批次编号
	 *
	 * 版本 >= 0
	 */
	 private long PacketStockId;

	/**
	 * 版本 >= 0
	 */
	 private short PacketStockId_u;

	/**
	 * 红包名称
	 *
	 * 版本 >= 0
	 */
	 private String PacketName = new String();

	/**
	 * 版本 >= 0
	 */
	 private short PacketName_u;

	/**
	 * 红包批次创建者uin
	 *
	 * 版本 >= 0
	 */
	 private long CreatorUin;

	/**
	 * 版本 >= 0
	 */
	 private short CreatorUin_u;

	/**
	 * 发放限额
	 *
	 * 版本 >= 0
	 */
	 private long MaxIssue;

	/**
	 * 版本 >= 0
	 */
	 private short MaxIssue_u;

	/**
	 * 接收限额
	 *
	 * 版本 >= 0
	 */
	 private long MaxHave;

	/**
	 * 版本 >= 0
	 */
	 private short MaxHave_u;

	/**
	 * 批次属性
	 *
	 * 版本 >= 0
	 */
	 private long Property;

	/**
	 * 版本 >= 0
	 */
	 private short Property_u;

	/**
	 * 面值
	 *
	 * 版本 >= 0
	 */
	 private long FaceValue;

	/**
	 * 版本 >= 0
	 */
	 private short FaceValue_u;

	/**
	 * 申请人
	 *
	 * 版本 >= 0
	 */
	 private String ApplicantName = new String();

	/**
	 * 版本 >= 0
	 */
	 private short ApplicantName_u;

	/**
	 * 审核人
	 *
	 * 版本 >= 0
	 */
	 private String ExaminerName = new String();

	/**
	 * 版本 >= 0
	 */
	 private short ExaminerName_u;

	/**
	 * 红包类型
	 *
	 * 版本 >= 0
	 */
	 private long Type;

	/**
	 * 版本 >= 0
	 */
	 private short Type_u;

	/**
	 * 红包标识
	 *
	 * 版本 >= 0
	 */
	 private long PacketFlag;

	/**
	 * 版本 >= 0
	 */
	 private short PacketFlag_u;

	/**
	 * 创建时间
	 *
	 * 版本 >= 0
	 */
	 private long CreateTime;

	/**
	 * 版本 >= 0
	 */
	 private short CreateTime_u;

	/**
	 * 发行时间
	 *
	 * 版本 >= 0
	 */
	 private long IssueTime;

	/**
	 * 版本 >= 0
	 */
	 private short IssueTime_u;

	/**
	 * 红包批次状态
	 *
	 * 版本 >= 0
	 */
	 private long State;

	/**
	 * 版本 >= 0
	 */
	 private short State_u;

	/**
	 * 生效日期
	 *
	 * 版本 >= 0
	 */
	 private long BeginTime;

	/**
	 * 版本 >= 0
	 */
	 private short BeginTime_u;

	/**
	 * 失效日期
	 *
	 * 版本 >= 0
	 */
	 private long EndTime;

	/**
	 * 版本 >= 0
	 */
	 private short EndTime_u;

	/**
	 * 最大有效天数
	 *
	 * 版本 >= 0
	 */
	 private long MaxExpireDays;

	/**
	 * 版本 >= 0
	 */
	 private short MaxExpireDays_u;

	/**
	 * 总使用量
	 *
	 * 版本 >= 0
	 */
	 private long TotalUsed;

	/**
	 * 版本 >= 0
	 */
	 private short TotalUsed_u;

	/**
	 * 总发放量
	 *
	 * 版本 >= 0
	 */
	 private long TotalIssued;

	/**
	 * 版本 >= 0
	 */
	 private short TotalIssued_u;

	/**
	 * 关联地址
	 *
	 * 版本 >= 0
	 */
	 private String RelaUrl = new String();

	/**
	 * 版本 >= 0
	 */
	 private short RelaUrl_u;

	/**
	 * 备注
	 *
	 * 版本 >= 0
	 */
	 private String Remark = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Remark_u;

	/**
	 * 适用店铺
	 *
	 * 版本 >= 0
	 */
	 private Set<uint32_t> ApplicableScope = new HashSet<uint32_t>();

	/**
	 * 版本 >= 0
	 */
	 private short ApplicableScope_u;

	/**
	 * 使用说明
	 *
	 * 版本 >= 0
	 */
	 private String Desc = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Desc_u;

	/**
	 * 图片地址
	 *
	 * 版本 >= 0
	 */
	 private String ImageUrl = new String();

	/**
	 * 版本 >= 0
	 */
	 private short ImageUrl_u;

	/**
	 * 最低消费
	 *
	 * 版本 >= 0
	 */
	 private long Minimum;

	/**
	 * 版本 >= 0
	 */
	 private short Minimum_u;

	/**
	 * 兑现比例
	 *
	 * 版本 >= 0
	 */
	 private long WithdrawRate;

	/**
	 * 版本 >= 0
	 */
	 private short WithdrawRate_u;

	/**
	 * 最后更新时间
	 *
	 * 版本 >= 0
	 */
	 private long LastUpdateTime;

	/**
	 * 版本 >= 0
	 */
	 private short LastUpdateTime_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushString(ErrMsg);
		bs.pushUByte(ErrMsg_u);
		bs.pushLong(PacketStockId);
		bs.pushUByte(PacketStockId_u);
		bs.pushString(PacketName);
		bs.pushUByte(PacketName_u);
		bs.pushUInt(CreatorUin);
		bs.pushUByte(CreatorUin_u);
		bs.pushUInt(MaxIssue);
		bs.pushUByte(MaxIssue_u);
		bs.pushUInt(MaxHave);
		bs.pushUByte(MaxHave_u);
		bs.pushUInt(Property);
		bs.pushUByte(Property_u);
		bs.pushUInt(FaceValue);
		bs.pushUByte(FaceValue_u);
		bs.pushString(ApplicantName);
		bs.pushUByte(ApplicantName_u);
		bs.pushString(ExaminerName);
		bs.pushUByte(ExaminerName_u);
		bs.pushUInt(Type);
		bs.pushUByte(Type_u);
		bs.pushUInt(PacketFlag);
		bs.pushUByte(PacketFlag_u);
		bs.pushUInt(CreateTime);
		bs.pushUByte(CreateTime_u);
		bs.pushUInt(IssueTime);
		bs.pushUByte(IssueTime_u);
		bs.pushUInt(State);
		bs.pushUByte(State_u);
		bs.pushUInt(BeginTime);
		bs.pushUByte(BeginTime_u);
		bs.pushUInt(EndTime);
		bs.pushUByte(EndTime_u);
		bs.pushUInt(MaxExpireDays);
		bs.pushUByte(MaxExpireDays_u);
		bs.pushUInt(TotalUsed);
		bs.pushUByte(TotalUsed_u);
		bs.pushUInt(TotalIssued);
		bs.pushUByte(TotalIssued_u);
		bs.pushString(RelaUrl);
		bs.pushUByte(RelaUrl_u);
		bs.pushString(Remark);
		bs.pushUByte(Remark_u);
		bs.pushObject(ApplicableScope);
		bs.pushUByte(ApplicableScope_u);
		bs.pushString(Desc);
		bs.pushUByte(Desc_u);
		bs.pushString(ImageUrl);
		bs.pushUByte(ImageUrl_u);
		bs.pushUInt(Minimum);
		bs.pushUByte(Minimum_u);
		bs.pushUInt(WithdrawRate);
		bs.pushUByte(WithdrawRate_u);
		bs.pushUInt(LastUpdateTime);
		bs.pushUByte(LastUpdateTime_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		ErrMsg = bs.popString();
		ErrMsg_u = bs.popUByte();
		PacketStockId = bs.popLong();
		PacketStockId_u = bs.popUByte();
		PacketName = bs.popString();
		PacketName_u = bs.popUByte();
		CreatorUin = bs.popUInt();
		CreatorUin_u = bs.popUByte();
		MaxIssue = bs.popUInt();
		MaxIssue_u = bs.popUByte();
		MaxHave = bs.popUInt();
		MaxHave_u = bs.popUByte();
		Property = bs.popUInt();
		Property_u = bs.popUByte();
		FaceValue = bs.popUInt();
		FaceValue_u = bs.popUByte();
		ApplicantName = bs.popString();
		ApplicantName_u = bs.popUByte();
		ExaminerName = bs.popString();
		ExaminerName_u = bs.popUByte();
		Type = bs.popUInt();
		Type_u = bs.popUByte();
		PacketFlag = bs.popUInt();
		PacketFlag_u = bs.popUByte();
		CreateTime = bs.popUInt();
		CreateTime_u = bs.popUByte();
		IssueTime = bs.popUInt();
		IssueTime_u = bs.popUByte();
		State = bs.popUInt();
		State_u = bs.popUByte();
		BeginTime = bs.popUInt();
		BeginTime_u = bs.popUByte();
		EndTime = bs.popUInt();
		EndTime_u = bs.popUByte();
		MaxExpireDays = bs.popUInt();
		MaxExpireDays_u = bs.popUByte();
		TotalUsed = bs.popUInt();
		TotalUsed_u = bs.popUByte();
		TotalIssued = bs.popUInt();
		TotalIssued_u = bs.popUByte();
		RelaUrl = bs.popString();
		RelaUrl_u = bs.popUByte();
		Remark = bs.popString();
		Remark_u = bs.popUByte();
		ApplicableScope = (Set<uint32_t>)bs.popSet(HashSet.class,uint32_t.class);
		ApplicableScope_u = bs.popUByte();
		Desc = bs.popString();
		Desc_u = bs.popUByte();
		ImageUrl = bs.popString();
		ImageUrl_u = bs.popUByte();
		Minimum = bs.popUInt();
		Minimum_u = bs.popUByte();
		WithdrawRate = bs.popUInt();
		WithdrawRate_u = bs.popUByte();
		LastUpdateTime = bs.popUInt();
		LastUpdateTime_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return ErrMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return ErrMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.ErrMsg = value;
		this.ErrMsg_u = 1;
	}

	public boolean issetErrMsg()
	{
		return this.ErrMsg_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ErrMsg_u value 类型为:short
	 * 
	 */
	public short getErrMsg_u()
	{
		return ErrMsg_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setErrMsg_u(short value)
	{
		this.ErrMsg_u = value;
	}


	/**
	 * 获取批次编号
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketStockId value 类型为:long
	 * 
	 */
	public long getPacketStockId()
	{
		return PacketStockId;
	}


	/**
	 * 设置批次编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPacketStockId(long value)
	{
		this.PacketStockId = value;
		this.PacketStockId_u = 1;
	}

	public boolean issetPacketStockId()
	{
		return this.PacketStockId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketStockId_u value 类型为:short
	 * 
	 */
	public short getPacketStockId_u()
	{
		return PacketStockId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketStockId_u(short value)
	{
		this.PacketStockId_u = value;
	}


	/**
	 * 获取红包名称
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketName value 类型为:String
	 * 
	 */
	public String getPacketName()
	{
		return PacketName;
	}


	/**
	 * 设置红包名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPacketName(String value)
	{
		this.PacketName = value;
		this.PacketName_u = 1;
	}

	public boolean issetPacketName()
	{
		return this.PacketName_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketName_u value 类型为:short
	 * 
	 */
	public short getPacketName_u()
	{
		return PacketName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketName_u(short value)
	{
		this.PacketName_u = value;
	}


	/**
	 * 获取红包批次创建者uin
	 * 
	 * 此字段的版本 >= 0
	 * @return CreatorUin value 类型为:long
	 * 
	 */
	public long getCreatorUin()
	{
		return CreatorUin;
	}


	/**
	 * 设置红包批次创建者uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCreatorUin(long value)
	{
		this.CreatorUin = value;
		this.CreatorUin_u = 1;
	}

	public boolean issetCreatorUin()
	{
		return this.CreatorUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return CreatorUin_u value 类型为:short
	 * 
	 */
	public short getCreatorUin_u()
	{
		return CreatorUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCreatorUin_u(short value)
	{
		this.CreatorUin_u = value;
	}


	/**
	 * 获取发放限额
	 * 
	 * 此字段的版本 >= 0
	 * @return MaxIssue value 类型为:long
	 * 
	 */
	public long getMaxIssue()
	{
		return MaxIssue;
	}


	/**
	 * 设置发放限额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMaxIssue(long value)
	{
		this.MaxIssue = value;
		this.MaxIssue_u = 1;
	}

	public boolean issetMaxIssue()
	{
		return this.MaxIssue_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return MaxIssue_u value 类型为:short
	 * 
	 */
	public short getMaxIssue_u()
	{
		return MaxIssue_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMaxIssue_u(short value)
	{
		this.MaxIssue_u = value;
	}


	/**
	 * 获取接收限额
	 * 
	 * 此字段的版本 >= 0
	 * @return MaxHave value 类型为:long
	 * 
	 */
	public long getMaxHave()
	{
		return MaxHave;
	}


	/**
	 * 设置接收限额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMaxHave(long value)
	{
		this.MaxHave = value;
		this.MaxHave_u = 1;
	}

	public boolean issetMaxHave()
	{
		return this.MaxHave_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return MaxHave_u value 类型为:short
	 * 
	 */
	public short getMaxHave_u()
	{
		return MaxHave_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMaxHave_u(short value)
	{
		this.MaxHave_u = value;
	}


	/**
	 * 获取批次属性
	 * 
	 * 此字段的版本 >= 0
	 * @return Property value 类型为:long
	 * 
	 */
	public long getProperty()
	{
		return Property;
	}


	/**
	 * 设置批次属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setProperty(long value)
	{
		this.Property = value;
		this.Property_u = 1;
	}

	public boolean issetProperty()
	{
		return this.Property_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Property_u value 类型为:short
	 * 
	 */
	public short getProperty_u()
	{
		return Property_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProperty_u(short value)
	{
		this.Property_u = value;
	}


	/**
	 * 获取面值
	 * 
	 * 此字段的版本 >= 0
	 * @return FaceValue value 类型为:long
	 * 
	 */
	public long getFaceValue()
	{
		return FaceValue;
	}


	/**
	 * 设置面值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFaceValue(long value)
	{
		this.FaceValue = value;
		this.FaceValue_u = 1;
	}

	public boolean issetFaceValue()
	{
		return this.FaceValue_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return FaceValue_u value 类型为:short
	 * 
	 */
	public short getFaceValue_u()
	{
		return FaceValue_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFaceValue_u(short value)
	{
		this.FaceValue_u = value;
	}


	/**
	 * 获取申请人
	 * 
	 * 此字段的版本 >= 0
	 * @return ApplicantName value 类型为:String
	 * 
	 */
	public String getApplicantName()
	{
		return ApplicantName;
	}


	/**
	 * 设置申请人
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setApplicantName(String value)
	{
		this.ApplicantName = value;
		this.ApplicantName_u = 1;
	}

	public boolean issetApplicantName()
	{
		return this.ApplicantName_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ApplicantName_u value 类型为:short
	 * 
	 */
	public short getApplicantName_u()
	{
		return ApplicantName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setApplicantName_u(short value)
	{
		this.ApplicantName_u = value;
	}


	/**
	 * 获取审核人
	 * 
	 * 此字段的版本 >= 0
	 * @return ExaminerName value 类型为:String
	 * 
	 */
	public String getExaminerName()
	{
		return ExaminerName;
	}


	/**
	 * 设置审核人
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setExaminerName(String value)
	{
		this.ExaminerName = value;
		this.ExaminerName_u = 1;
	}

	public boolean issetExaminerName()
	{
		return this.ExaminerName_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ExaminerName_u value 类型为:short
	 * 
	 */
	public short getExaminerName_u()
	{
		return ExaminerName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setExaminerName_u(short value)
	{
		this.ExaminerName_u = value;
	}


	/**
	 * 获取红包类型
	 * 
	 * 此字段的版本 >= 0
	 * @return Type value 类型为:long
	 * 
	 */
	public long getType()
	{
		return Type;
	}


	/**
	 * 设置红包类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setType(long value)
	{
		this.Type = value;
		this.Type_u = 1;
	}

	public boolean issetType()
	{
		return this.Type_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Type_u value 类型为:short
	 * 
	 */
	public short getType_u()
	{
		return Type_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setType_u(short value)
	{
		this.Type_u = value;
	}


	/**
	 * 获取红包标识
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketFlag value 类型为:long
	 * 
	 */
	public long getPacketFlag()
	{
		return PacketFlag;
	}


	/**
	 * 设置红包标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPacketFlag(long value)
	{
		this.PacketFlag = value;
		this.PacketFlag_u = 1;
	}

	public boolean issetPacketFlag()
	{
		return this.PacketFlag_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketFlag_u value 类型为:short
	 * 
	 */
	public short getPacketFlag_u()
	{
		return PacketFlag_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketFlag_u(short value)
	{
		this.PacketFlag_u = value;
	}


	/**
	 * 获取创建时间
	 * 
	 * 此字段的版本 >= 0
	 * @return CreateTime value 类型为:long
	 * 
	 */
	public long getCreateTime()
	{
		return CreateTime;
	}


	/**
	 * 设置创建时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCreateTime(long value)
	{
		this.CreateTime = value;
		this.CreateTime_u = 1;
	}

	public boolean issetCreateTime()
	{
		return this.CreateTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return CreateTime_u value 类型为:short
	 * 
	 */
	public short getCreateTime_u()
	{
		return CreateTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCreateTime_u(short value)
	{
		this.CreateTime_u = value;
	}


	/**
	 * 获取发行时间
	 * 
	 * 此字段的版本 >= 0
	 * @return IssueTime value 类型为:long
	 * 
	 */
	public long getIssueTime()
	{
		return IssueTime;
	}


	/**
	 * 设置发行时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setIssueTime(long value)
	{
		this.IssueTime = value;
		this.IssueTime_u = 1;
	}

	public boolean issetIssueTime()
	{
		return this.IssueTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return IssueTime_u value 类型为:short
	 * 
	 */
	public short getIssueTime_u()
	{
		return IssueTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIssueTime_u(short value)
	{
		this.IssueTime_u = value;
	}


	/**
	 * 获取红包批次状态
	 * 
	 * 此字段的版本 >= 0
	 * @return State value 类型为:long
	 * 
	 */
	public long getState()
	{
		return State;
	}


	/**
	 * 设置红包批次状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setState(long value)
	{
		this.State = value;
		this.State_u = 1;
	}

	public boolean issetState()
	{
		return this.State_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return State_u value 类型为:short
	 * 
	 */
	public short getState_u()
	{
		return State_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setState_u(short value)
	{
		this.State_u = value;
	}


	/**
	 * 获取生效日期
	 * 
	 * 此字段的版本 >= 0
	 * @return BeginTime value 类型为:long
	 * 
	 */
	public long getBeginTime()
	{
		return BeginTime;
	}


	/**
	 * 设置生效日期
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBeginTime(long value)
	{
		this.BeginTime = value;
		this.BeginTime_u = 1;
	}

	public boolean issetBeginTime()
	{
		return this.BeginTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BeginTime_u value 类型为:short
	 * 
	 */
	public short getBeginTime_u()
	{
		return BeginTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBeginTime_u(short value)
	{
		this.BeginTime_u = value;
	}


	/**
	 * 获取失效日期
	 * 
	 * 此字段的版本 >= 0
	 * @return EndTime value 类型为:long
	 * 
	 */
	public long getEndTime()
	{
		return EndTime;
	}


	/**
	 * 设置失效日期
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEndTime(long value)
	{
		this.EndTime = value;
		this.EndTime_u = 1;
	}

	public boolean issetEndTime()
	{
		return this.EndTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return EndTime_u value 类型为:short
	 * 
	 */
	public short getEndTime_u()
	{
		return EndTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEndTime_u(short value)
	{
		this.EndTime_u = value;
	}


	/**
	 * 获取最大有效天数
	 * 
	 * 此字段的版本 >= 0
	 * @return MaxExpireDays value 类型为:long
	 * 
	 */
	public long getMaxExpireDays()
	{
		return MaxExpireDays;
	}


	/**
	 * 设置最大有效天数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMaxExpireDays(long value)
	{
		this.MaxExpireDays = value;
		this.MaxExpireDays_u = 1;
	}

	public boolean issetMaxExpireDays()
	{
		return this.MaxExpireDays_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return MaxExpireDays_u value 类型为:short
	 * 
	 */
	public short getMaxExpireDays_u()
	{
		return MaxExpireDays_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMaxExpireDays_u(short value)
	{
		this.MaxExpireDays_u = value;
	}


	/**
	 * 获取总使用量
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalUsed value 类型为:long
	 * 
	 */
	public long getTotalUsed()
	{
		return TotalUsed;
	}


	/**
	 * 设置总使用量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalUsed(long value)
	{
		this.TotalUsed = value;
		this.TotalUsed_u = 1;
	}

	public boolean issetTotalUsed()
	{
		return this.TotalUsed_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalUsed_u value 类型为:short
	 * 
	 */
	public short getTotalUsed_u()
	{
		return TotalUsed_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTotalUsed_u(short value)
	{
		this.TotalUsed_u = value;
	}


	/**
	 * 获取总发放量
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalIssued value 类型为:long
	 * 
	 */
	public long getTotalIssued()
	{
		return TotalIssued;
	}


	/**
	 * 设置总发放量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalIssued(long value)
	{
		this.TotalIssued = value;
		this.TotalIssued_u = 1;
	}

	public boolean issetTotalIssued()
	{
		return this.TotalIssued_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalIssued_u value 类型为:short
	 * 
	 */
	public short getTotalIssued_u()
	{
		return TotalIssued_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTotalIssued_u(short value)
	{
		this.TotalIssued_u = value;
	}


	/**
	 * 获取关联地址
	 * 
	 * 此字段的版本 >= 0
	 * @return RelaUrl value 类型为:String
	 * 
	 */
	public String getRelaUrl()
	{
		return RelaUrl;
	}


	/**
	 * 设置关联地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRelaUrl(String value)
	{
		this.RelaUrl = value;
		this.RelaUrl_u = 1;
	}

	public boolean issetRelaUrl()
	{
		return this.RelaUrl_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return RelaUrl_u value 类型为:short
	 * 
	 */
	public short getRelaUrl_u()
	{
		return RelaUrl_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRelaUrl_u(short value)
	{
		this.RelaUrl_u = value;
	}


	/**
	 * 获取备注
	 * 
	 * 此字段的版本 >= 0
	 * @return Remark value 类型为:String
	 * 
	 */
	public String getRemark()
	{
		return Remark;
	}


	/**
	 * 设置备注
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRemark(String value)
	{
		this.Remark = value;
		this.Remark_u = 1;
	}

	public boolean issetRemark()
	{
		return this.Remark_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Remark_u value 类型为:short
	 * 
	 */
	public short getRemark_u()
	{
		return Remark_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRemark_u(short value)
	{
		this.Remark_u = value;
	}


	/**
	 * 获取适用店铺
	 * 
	 * 此字段的版本 >= 0
	 * @return ApplicableScope value 类型为:Set<uint32_t>
	 * 
	 */
	public Set<uint32_t> getApplicableScope()
	{
		return ApplicableScope;
	}


	/**
	 * 设置适用店铺
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Set<uint32_t>
	 * 
	 */
	public void setApplicableScope(Set<uint32_t> value)
	{
		if (value != null) {
				this.ApplicableScope = value;
				this.ApplicableScope_u = 1;
		}
	}

	public boolean issetApplicableScope()
	{
		return this.ApplicableScope_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ApplicableScope_u value 类型为:short
	 * 
	 */
	public short getApplicableScope_u()
	{
		return ApplicableScope_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setApplicableScope_u(short value)
	{
		this.ApplicableScope_u = value;
	}


	/**
	 * 获取使用说明
	 * 
	 * 此字段的版本 >= 0
	 * @return Desc value 类型为:String
	 * 
	 */
	public String getDesc()
	{
		return Desc;
	}


	/**
	 * 设置使用说明
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDesc(String value)
	{
		this.Desc = value;
		this.Desc_u = 1;
	}

	public boolean issetDesc()
	{
		return this.Desc_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Desc_u value 类型为:short
	 * 
	 */
	public short getDesc_u()
	{
		return Desc_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDesc_u(short value)
	{
		this.Desc_u = value;
	}


	/**
	 * 获取图片地址
	 * 
	 * 此字段的版本 >= 0
	 * @return ImageUrl value 类型为:String
	 * 
	 */
	public String getImageUrl()
	{
		return ImageUrl;
	}


	/**
	 * 设置图片地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setImageUrl(String value)
	{
		this.ImageUrl = value;
		this.ImageUrl_u = 1;
	}

	public boolean issetImageUrl()
	{
		return this.ImageUrl_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ImageUrl_u value 类型为:short
	 * 
	 */
	public short getImageUrl_u()
	{
		return ImageUrl_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setImageUrl_u(short value)
	{
		this.ImageUrl_u = value;
	}


	/**
	 * 获取最低消费
	 * 
	 * 此字段的版本 >= 0
	 * @return Minimum value 类型为:long
	 * 
	 */
	public long getMinimum()
	{
		return Minimum;
	}


	/**
	 * 设置最低消费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMinimum(long value)
	{
		this.Minimum = value;
		this.Minimum_u = 1;
	}

	public boolean issetMinimum()
	{
		return this.Minimum_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Minimum_u value 类型为:short
	 * 
	 */
	public short getMinimum_u()
	{
		return Minimum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMinimum_u(short value)
	{
		this.Minimum_u = value;
	}


	/**
	 * 获取兑现比例
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawRate value 类型为:long
	 * 
	 */
	public long getWithdrawRate()
	{
		return WithdrawRate;
	}


	/**
	 * 设置兑现比例
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWithdrawRate(long value)
	{
		this.WithdrawRate = value;
		this.WithdrawRate_u = 1;
	}

	public boolean issetWithdrawRate()
	{
		return this.WithdrawRate_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawRate_u value 类型为:short
	 * 
	 */
	public short getWithdrawRate_u()
	{
		return WithdrawRate_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWithdrawRate_u(short value)
	{
		this.WithdrawRate_u = value;
	}


	/**
	 * 获取最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return LastUpdateTime value 类型为:long
	 * 
	 */
	public long getLastUpdateTime()
	{
		return LastUpdateTime;
	}


	/**
	 * 设置最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastUpdateTime(long value)
	{
		this.LastUpdateTime = value;
		this.LastUpdateTime_u = 1;
	}

	public boolean issetLastUpdateTime()
	{
		return this.LastUpdateTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return LastUpdateTime_u value 类型为:short
	 * 
	 */
	public short getLastUpdateTime_u()
	{
		return LastUpdateTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastUpdateTime_u(short value)
	{
		this.LastUpdateTime_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(PacketStock)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ErrMsg, null);  //计算字段ErrMsg的长度 size_of(String)
				length += 1;  //计算字段ErrMsg_u的长度 size_of(uint8_t)
				length += 17;  //计算字段PacketStockId的长度 size_of(uint64_t)
				length += 1;  //计算字段PacketStockId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(PacketName, null);  //计算字段PacketName的长度 size_of(String)
				length += 1;  //计算字段PacketName_u的长度 size_of(uint8_t)
				length += 4;  //计算字段CreatorUin的长度 size_of(uint32_t)
				length += 1;  //计算字段CreatorUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段MaxIssue的长度 size_of(uint32_t)
				length += 1;  //计算字段MaxIssue_u的长度 size_of(uint8_t)
				length += 4;  //计算字段MaxHave的长度 size_of(uint32_t)
				length += 1;  //计算字段MaxHave_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Property的长度 size_of(uint32_t)
				length += 1;  //计算字段Property_u的长度 size_of(uint8_t)
				length += 4;  //计算字段FaceValue的长度 size_of(uint32_t)
				length += 1;  //计算字段FaceValue_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ApplicantName, null);  //计算字段ApplicantName的长度 size_of(String)
				length += 1;  //计算字段ApplicantName_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ExaminerName, null);  //计算字段ExaminerName的长度 size_of(String)
				length += 1;  //计算字段ExaminerName_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Type的长度 size_of(uint32_t)
				length += 1;  //计算字段Type_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PacketFlag的长度 size_of(uint32_t)
				length += 1;  //计算字段PacketFlag_u的长度 size_of(uint8_t)
				length += 4;  //计算字段CreateTime的长度 size_of(uint32_t)
				length += 1;  //计算字段CreateTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段IssueTime的长度 size_of(uint32_t)
				length += 1;  //计算字段IssueTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段State的长度 size_of(uint32_t)
				length += 1;  //计算字段State_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BeginTime的长度 size_of(uint32_t)
				length += 1;  //计算字段BeginTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段EndTime的长度 size_of(uint32_t)
				length += 1;  //计算字段EndTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段MaxExpireDays的长度 size_of(uint32_t)
				length += 1;  //计算字段MaxExpireDays_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TotalUsed的长度 size_of(uint32_t)
				length += 1;  //计算字段TotalUsed_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TotalIssued的长度 size_of(uint32_t)
				length += 1;  //计算字段TotalIssued_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(RelaUrl, null);  //计算字段RelaUrl的长度 size_of(String)
				length += 1;  //计算字段RelaUrl_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Remark, null);  //计算字段Remark的长度 size_of(String)
				length += 1;  //计算字段Remark_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ApplicableScope, null);  //计算字段ApplicableScope的长度 size_of(Set)
				length += 1;  //计算字段ApplicableScope_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Desc, null);  //计算字段Desc的长度 size_of(String)
				length += 1;  //计算字段Desc_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ImageUrl, null);  //计算字段ImageUrl的长度 size_of(String)
				length += 1;  //计算字段ImageUrl_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Minimum的长度 size_of(uint32_t)
				length += 1;  //计算字段Minimum_u的长度 size_of(uint8_t)
				length += 4;  //计算字段WithdrawRate的长度 size_of(uint32_t)
				length += 1;  //计算字段WithdrawRate_u的长度 size_of(uint8_t)
				length += 4;  //计算字段LastUpdateTime的长度 size_of(uint32_t)
				length += 1;  //计算字段LastUpdateTime_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(PacketStock)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ErrMsg, encoding);  //计算字段ErrMsg的长度 size_of(String)
				length += 1;  //计算字段ErrMsg_u的长度 size_of(uint8_t)
				length += 17;  //计算字段PacketStockId的长度 size_of(uint64_t)
				length += 1;  //计算字段PacketStockId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(PacketName, encoding);  //计算字段PacketName的长度 size_of(String)
				length += 1;  //计算字段PacketName_u的长度 size_of(uint8_t)
				length += 4;  //计算字段CreatorUin的长度 size_of(uint32_t)
				length += 1;  //计算字段CreatorUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段MaxIssue的长度 size_of(uint32_t)
				length += 1;  //计算字段MaxIssue_u的长度 size_of(uint8_t)
				length += 4;  //计算字段MaxHave的长度 size_of(uint32_t)
				length += 1;  //计算字段MaxHave_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Property的长度 size_of(uint32_t)
				length += 1;  //计算字段Property_u的长度 size_of(uint8_t)
				length += 4;  //计算字段FaceValue的长度 size_of(uint32_t)
				length += 1;  //计算字段FaceValue_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ApplicantName, encoding);  //计算字段ApplicantName的长度 size_of(String)
				length += 1;  //计算字段ApplicantName_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ExaminerName, encoding);  //计算字段ExaminerName的长度 size_of(String)
				length += 1;  //计算字段ExaminerName_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Type的长度 size_of(uint32_t)
				length += 1;  //计算字段Type_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PacketFlag的长度 size_of(uint32_t)
				length += 1;  //计算字段PacketFlag_u的长度 size_of(uint8_t)
				length += 4;  //计算字段CreateTime的长度 size_of(uint32_t)
				length += 1;  //计算字段CreateTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段IssueTime的长度 size_of(uint32_t)
				length += 1;  //计算字段IssueTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段State的长度 size_of(uint32_t)
				length += 1;  //计算字段State_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BeginTime的长度 size_of(uint32_t)
				length += 1;  //计算字段BeginTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段EndTime的长度 size_of(uint32_t)
				length += 1;  //计算字段EndTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段MaxExpireDays的长度 size_of(uint32_t)
				length += 1;  //计算字段MaxExpireDays_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TotalUsed的长度 size_of(uint32_t)
				length += 1;  //计算字段TotalUsed_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TotalIssued的长度 size_of(uint32_t)
				length += 1;  //计算字段TotalIssued_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(RelaUrl, encoding);  //计算字段RelaUrl的长度 size_of(String)
				length += 1;  //计算字段RelaUrl_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Remark, encoding);  //计算字段Remark的长度 size_of(String)
				length += 1;  //计算字段Remark_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ApplicableScope, encoding);  //计算字段ApplicableScope的长度 size_of(Set)
				length += 1;  //计算字段ApplicableScope_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Desc, encoding);  //计算字段Desc的长度 size_of(String)
				length += 1;  //计算字段Desc_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ImageUrl, encoding);  //计算字段ImageUrl的长度 size_of(String)
				length += 1;  //计算字段ImageUrl_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Minimum的长度 size_of(uint32_t)
				length += 1;  //计算字段Minimum_u的长度 size_of(uint8_t)
				length += 4;  //计算字段WithdrawRate的长度 size_of(uint32_t)
				length += 1;  //计算字段WithdrawRate_u的长度 size_of(uint8_t)
				length += 4;  //计算字段LastUpdateTime的长度 size_of(uint32_t)
				length += 1;  //计算字段LastUpdateTime_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
