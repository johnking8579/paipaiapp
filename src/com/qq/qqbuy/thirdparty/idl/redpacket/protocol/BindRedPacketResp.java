 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.redpacketapi.java

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *绑定红包回复
 *
 *@date 2014-10-16 05:56:15
 *
 *@since version:0
*/
public class  BindRedPacketResp extends NetMessage
{
	/**
	 * 绑定红包回复
	 *
	 * 版本 >= 0
	 */
	 private BindRedPacketResponse Response = new BindRedPacketResponse();

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String ErrMsg = new String();

	/**
	 * 保留输出字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveOut = new String();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(Response);
		bs.pushString(ErrMsg);
		bs.pushString(ReserveOut);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		Response = (BindRedPacketResponse) bs.popObject(BindRedPacketResponse.class);
		ErrMsg = bs.popString();
		ReserveOut = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x91118807L;
	}


	/**
	 * 获取绑定红包回复
	 * 
	 * 此字段的版本 >= 0
	 * @return Response value 类型为:BindRedPacketResponse
	 * 
	 */
	public BindRedPacketResponse getResponse()
	{
		return Response;
	}


	/**
	 * 设置绑定红包回复
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:BindRedPacketResponse
	 * 
	 */
	public void setResponse(BindRedPacketResponse value)
	{
		if (value != null) {
				this.Response = value;
		}else{
				this.Response = new BindRedPacketResponse();
		}
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return ErrMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return ErrMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.ErrMsg = value;
	}


	/**
	 * 获取保留输出字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveOut value 类型为:String
	 * 
	 */
	public String getReserveOut()
	{
		return ReserveOut;
	}


	/**
	 * 设置保留输出字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveOut(String value)
	{
		this.ReserveOut = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(BindRedPacketResp)
				length += ByteStream.getObjectSize(Response, null);  //计算字段Response的长度 size_of(BindRedPacketResponse)
				length += ByteStream.getObjectSize(ErrMsg, null);  //计算字段ErrMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveOut, null);  //计算字段ReserveOut的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(BindRedPacketResp)
				length += ByteStream.getObjectSize(Response, encoding);  //计算字段Response的长度 size_of(BindRedPacketResponse)
				length += ByteStream.getObjectSize(ErrMsg, encoding);  //计算字段ErrMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveOut, encoding);  //计算字段ReserveOut的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
