package com.qq.qqbuy.thirdparty.idl.redpacket;

/* 
 * @author 		colinjing
 * @version 	1.0
 * @date 		2010.12.22
 */

import java.util.Vector;
import java.util.Set;
import java.util.Map;

import com.paipai.lang.uint64_t;
import com.paipai.lang.uint32_t;
import com.paipai.lang.uint8_t;
import com.paipai.lang.MultiMap;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;

/**
 * 说明：这个是新版的IDL接口，目前只是更新了发券接口对应的文件。
 * @author zhaohuayu
 *
 */
@HeadApiProtocol(cPlusNamespace = "c2cent::ao::redpacketapi",needInit=true )
public class redpacketapi {
	@ApiProtocol(cmdid = "0x91111801L", desc = "获取红包列表" )
	class GetRedPacketList {
		@ApiProtocol(cmdid = "0x91111801L", desc = "获取红包列表")
		class Req{
			@Field(desc = "调用者==>请设置为源文件名")
			String Source;
			@Field(desc = "场景id")
			uint32_t SceneId;
			@Field(desc = "请求filter")
			RedPacketFilter oFilter;	
			@Field(desc = "用户MachineKey")
			String MachineKey;
			@Field(desc = "保留输入字段")
			String ReserveIn;
		}
		@ApiProtocol(cmdid = "0x91118801L", desc = "获取红包列表返回")
		class Resp{
			@Field(desc = "订单列表")
			Vector<RedPacket> RedPacketList;
			@Field(desc = "符合条件总数")
			uint32_t TotalNum;
			@Field(desc = "错误信息")
			String ErrMsg;
			@Field(desc = "保留输出字段")
			String ReserveOut;
		}
	}		
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "请求filter", isSetClassSize = true,isNeedUFlag= true,version=20130328)
	class RedPacketFilter{
		@Field(desc = "版本号", defaultValue="20130328")
		uint32_t  version;
		uint8_t version_u;
		@Field(desc = "红包名称")
		String PacketName;
		uint8_t PacketName_u;
		@Field(desc = "红包批次id") 
		uint64_t PacketStockId;
		uint8_t PacketStockId_u;
		@Field(desc = "所属者") 
		uint32_t OwnerUin;
		uint8_t OwnerUin_u;
		@Field(desc = "对应卖家号") 
		uint32_t SellerUin;
		uint8_t SellerUin_u;
		@Field(desc = "红包状态,待使用(100),使用中--绑定和冻结(101)，已使用(102),过期(2)") 
		uint32_t State;
		uint8_t State_u;
		@Field(desc = "绑定的订单id")
		String DealCode;	
		uint8_t DealCode_u;
		@Field(desc = "兑现编号")
		uint64_t WithdrawId;			
		uint8_t WithdrawId_u;
		@Field(desc = "最大面值")
		uint32_t MaxFaceValue;
		uint8_t MaxFaceValue_u;
		@Field(desc = "适用范围")
		Set<uint32_t>	ApplicableScope;
		uint8_t ApplicableScope_u;
		@Field(desc = "红包id列表") 
		Vector<uint64_t> PacketIdList;	
		uint8_t PacketIdList_u;
		@Field(desc = "红包类型,红包(1)，店铺代金券(2)")
		uint32_t Type;
		uint8_t Type_u;
		@Field(desc = "拉取信息类型，基本信息(0x01),批次(0x02),兑现信息(0x04),详细信息(0x08),代金券购买(0x1)")
		uint32_t Info;
		uint8_t Info_u;
		
		@Field(desc = "分页开始位置, 从0开始", version=20130328)
	    uint32_t offset;
		@Field(version=20130328)
	    uint8_t offset_u;

	    @Field(desc = "分页所需数目, 最大值20", version=20130328)
	    uint32_t limit;
	    @Field(version=20130328)
	    uint8_t limit_u;
	}	
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "红包列表", isSetClassSize = true,isNeedUFlag= true,version=20130328)	
	class RedPacket{
		@Field(desc = "版本号",defaultValue="20130328")
		uint32_t  version;
		uint8_t version_u;
		@Field(desc = "红包id")
		uint32_t PacketId;
		uint8_t PacketId_u;
		@Field(desc = "红包名称")
		String PacketName;
		uint8_t PacketName_u;
		@Field(desc = "红包类型,红包(1)，店铺代金券(2)")
		uint32_t Type;
		uint8_t Type_u;	 
		@Field(desc = "红包批次id") 
		uint64_t PacketStockId;
		uint8_t PacketStockId_u;
		@Field(desc = "所属者") 
		uint32_t OwnerUin;
		uint8_t OwnerUin_u;
		@Field(desc = "面值")
		uint32_t PacketPrice;
		uint8_t PacketPrice_u;
		@Field(desc = "红包标识") 
		uint32_t PacketFlag;
		uint8_t PacketFlag_u;
		@Field(desc = "对应卖家号") 
		uint32_t SellerUin;
		uint8_t SellerUin_u;
		@Field(desc = "起始时间")
		uint32_t BeginTime;
		uint8_t BeginTime_u;
		@Field(desc = "结束时间")
		uint32_t EndTime;
		uint8_t EndTime_u;
		@Field(desc = "领取ip")
		String Ip;
		uint8_t Ip_u;
		@Field(desc = "领取时间")
		uint32_t RecvTime;
		uint8_t RecvTime_u;
		@Field(desc = "使用时间") 
		uint32_t UseTime;
		uint8_t UseTime_u;	
		@Field(desc = "红包状态,待使用(100),使用中--绑定和冻结(101)，已使用(102)") 
		uint32_t State;
		uint8_t State_u;
		@Field(desc = "绑定的订单id")
		String DealCode;	
		uint8_t DealCode_u;
		@Field(desc = "关联url")
		String RelaUrl;	
		uint8_t RelaUrl_u;
		@Field(desc = "图片url")
		String ImageUrl;	
		uint8_t ImageUrl_u; 
		@Field(desc = "实际抵扣金额") 
		uint32_t ActualPrice;
		uint8_t   ActualPrice_u;
		@Field(desc = "兑现编号")
		uint64_t WithdrawId;			
		uint8_t WithdrawId_u;
		@Field(desc = "兑现时间") 
		uint32_t WithdrawTime;
		uint8_t WithdrawTime_u; 
		@Field(desc = "适用店铺")
		Set<uint32_t>	ApplicableScope;
		uint8_t ApplicableScope_u;
		
		@Field(desc = "最低消费", version=20130328)
	    uint32_t minimum;
		@Field(version=20130328)
	    uint8_t minimum_u;
	}
	
	@ApiProtocol(cmdid = "0x91111802L", desc = "发放红包")
	class SendRedPacket{
		@ApiProtocol(cmdid = "0x91111802L", desc = "发放红包请求")
		class Req{
			@Field(desc="请求来源描述")
			String Source;
			@Field(desc="场景id")
			uint32_t SceneId;
			@Field(desc="请求ip")
			String ReqIp;
			@Field(desc="发放红包请求")
			SendRedPacketRequest Request;
			@Field(desc="请求MachineKey")
			String MachineKey;
			@Field(desc="保留输入字段")
			String ReserveIn;
		}
		@ApiProtocol(cmdid = "0x91118802L", desc = "发放红包回复")
		class Resp{
			@Field(desc="发放红包回复")
			SendRedPacketResponse Response;
			@Field(desc="错误信息")
			String ErrMsg;	
			@Field(desc="保留输出字段")
			String ReserveOut;
		}
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "发放红包请求", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class SendRedPacketRequest{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="发放者uin")
		uint32_t SendUin;
		uint8_t SendUin_u;
		@Field(desc="等待发放列表")
		Vector<RedPacketInfo> WaitSendInfo;
		uint8_t WaitSendInfo_u;
		@Field(desc="cookies中VisitKey")
		String VisitKey;
		uint8_t VisitKey_u;
		@Field(desc="客户端IP")
		String ClientIP;
		uint8_t ClientIP_u;
		@Field(desc="http请求中User-Agent")
		String UserAgent;
		uint8_t UserAgent_u;
		@Field(desc="http请求中Referer")
		String Referer;
		uint8_t Referer_u;
		@Field(desc="http请求中Url")
		String Url;
		uint8_t Url_u;
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "等待发放的红包信息", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class RedPacketInfo{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="接收者uin")
		uint32_t RecvUin;
		uint8_t RecvUin_u;
		@Field(desc="等待发放的红包列表")
		Map<uint64_t,uint32_t> WaitSendRedPacket;
		uint8_t WaitSendRedPacket_u;
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "发放红包回复", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class SendRedPacketResponse{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="发送失败列表")
		Vector<SendFailInfo> SendFailedRecords;
		uint8_t SendFailedRecords_u;
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "发放失败的错误信息", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class SendFailInfo{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="接收者uin")
		uint32_t RecvUin;
		uint8_t RecvUin_u;
		@Field(desc="发放失败的错误码列表")
		MultiMap<uint64_t,uint32_t> SendFailItems;
		uint8_t SendFailItems_u;
	}
	
	@ApiProtocol(cmdid = "0x9111180AL", desc = "发放红包支持可重录")
	class SendRedPacketV2{
		@ApiProtocol(cmdid = "0x9111180AL", desc = "发放红包支持可重录请求")
		class Req{
			@Field(desc="请求来源描述")
			String Source;
			@Field(desc="场景id")
			uint32_t SceneId;
			@Field(desc="请求ip")
			String ReqIp;
			@Field(desc = "发放红包Filter")
            SendFilter FilterInfo;
			@Field(desc="请求MachineKey")
			String MachineKey;
			@Field(desc="保留输入字段")
			String ReserveIn;
		}
		@ApiProtocol(cmdid = "0x9111880AL", desc = "发放红包支持可重录回复")
		class Resp{
			@Field(desc = "发送红包Result")
            SendResult ResultInfo;
			@Field(desc="保留输出字段")
			String ReserveOut;
		}
	}
	
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "发放红包Filter", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class SendFilter {
	    @Field(desc = "业务类型")
	    uint32_t BusinessType;
	    uint8_t BusinessType_u;
	    @Field(desc = "发放请求列表")
	    Vector<SendPacketInfo> PacketListInfo;
	    uint8_t PacketListInfo_u;
	    @Field(desc = "版本")
	    uint32_t version;
	    uint8_t version_u;
	}
	
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "发送红包Result", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class SendResult {
	    @Field(desc = "发放返回列表")
	    Vector<SendPacketFailedInfo> PacketFailedList;
	    uint8_t PacketFailedList_u;
	    @Field(desc = "发放失败数")
	    uint32_t FailedNumber;
	    uint8_t FailedNumber_u;
	    @Field(desc = "版本")
	    uint32_t version;
	    uint8_t version_u;
	}
	
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "发送红包Info", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class SendPacketInfo {
	    @Field(desc = "发放唯一索引")
	    Vector<uint64_t> SequenceList;
	    uint8_t SequenceList_u;
	    @Field(desc = "红包批次ID")
	    uint64_t PacketStockId;
	    uint8_t PacketStockId_u;
	    @Field(desc = "接收买家Uin")
	    uint32_t BuyerUin;
	    uint8_t BuyerUin_u;
	    @Field(desc = "版本")
	    uint32_t version;
	    uint8_t version_u;
	}
	
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "发送红包Info", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class SendPacketFailedInfo {
	    @Field(desc = "失败索引")
	    Map<uint64_t,uint32_t> SequenceFailedList;
	    uint8_t SequenceFailedList_u;
	    @Field(desc = "红包批次ID")
	    uint64_t PacketStockId;
	    uint8_t PacketStockId_u;
	    @Field(desc = "接收买家Uin")
	    uint32_t BuyerUin;
	    uint8_t BuyerUin_u;
	    @Field(desc = "版本")
	    uint32_t version;
	    uint8_t version_u;
	}
	
	@ApiProtocol(cmdid = "0x91111803L", desc = "创建红包批次")
	class CreatePacketStock{
		@ApiProtocol(cmdid = "0x91111803L", desc = "创建红包批次请求")
		class Req{
			@Field(desc="请求来源描述")
			String Source;
			@Field(desc="场景id")
			uint32_t SceneId;
			@Field(desc="请求ip")
			String ReqIp;
			@Field(desc="创建红包批次请求")
			CreatePacketStockRequest Request;
			@Field(desc="请求MachineKey")
			String MachineKey;
			@Field(desc="保留输入字段")
			String ReserveIn;
		}
		@ApiProtocol(cmdid = "0x91118803L", desc = "创建红包批次回复")
		class Resp{
			@Field(desc="创建红包批次回复")
			CreatePacketStockResponse Response;
			@Field(desc="错误信息")
			String ErrMsg;
			@Field(desc="保留输出字段")
			String ReserveOut;
		}
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "创建红包批次请求", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class CreatePacketStockRequest{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="创建批次者uin")
		uint32_t CreatorUin;
		uint8_t CreatorUin_u;
		@Field(desc="创建的红包批次")
		PacketStock ReqPacketStock;
		uint8_t ReqPacketStock_u;
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "创建红包批次回复", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class CreatePacketStockResponse{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="创建批次者uin")
		uint32_t CreatorUin;
		uint8_t CreatorUin_u;
		@Field(desc="生成的红包批次")
		PacketStock RespPacketStock;
		uint8_t RespPacketStock_u;
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "红包批次", isSetClassSize = true, isNeedUFlag = true, version = 20141230)
	class PacketStock{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="错误信息")
		String ErrMsg;
		uint8_t ErrMsg_u;
		@Field(desc="批次编号")
		uint64_t PacketStockId;
		uint8_t PacketStockId_u;
		@Field(desc="红包名称")
		String PacketName;
		uint8_t PacketName_u;
		@Field(desc="红包批次创建者uin")
		uint32_t CreatorUin;
		uint8_t CreatorUin_u;
		@Field(desc="发放限额")
		uint32_t MaxIssue;
		uint8_t MaxIssue_u;
		@Field(desc="接收限额")
		uint32_t MaxHave;
		uint8_t MaxHave_u;
		@Field(desc="批次属性")
		uint32_t Property;
		uint8_t Property_u;
		@Field(desc="面值")
		uint32_t FaceValue;
		uint8_t FaceValue_u;
		@Field(desc="申请人")
		String ApplicantName;
		uint8_t ApplicantName_u;
		@Field(desc="审核人")
		String ExaminerName;
		uint8_t ExaminerName_u;
		@Field(desc="红包类型")
		uint32_t Type;
		uint8_t Type_u;
		@Field(desc="红包标识")
		uint32_t PacketFlag;
		uint8_t PacketFlag_u;
		@Field(desc="创建时间")
		uint32_t CreateTime;
		uint8_t CreateTime_u;
		@Field(desc="发行时间")
		uint32_t IssueTime;
		uint8_t IssueTime_u;
		@Field(desc="红包批次状态")
		uint32_t State;
		uint8_t State_u;
		@Field(desc="生效日期")
		uint32_t BeginTime;
		uint8_t BeginTime_u;
		@Field(desc="失效日期")
		uint32_t EndTime;
		uint8_t EndTime_u;
		@Field(desc="最大有效天数")
		uint32_t MaxExpireDays;
		uint8_t MaxExpireDays_u;
		@Field(desc="总使用量")
		uint32_t TotalUsed;
		uint8_t TotalUsed_u;
		@Field(desc="总发放量")
		uint32_t TotalIssued;
		uint8_t TotalIssued_u;
		@Field(desc="关联地址")
		String RelaUrl;
		uint8_t RelaUrl_u;
		@Field(desc="备注")
		String Remark;
		uint8_t Remark_u;
		@Field(desc="适用店铺")
		Set<uint32_t> ApplicableScope;
		uint8_t ApplicableScope_u;
		@Field(desc="使用说明")
		String Desc;
		uint8_t Desc_u;
		@Field(desc="图片地址")
		String ImageUrl;
		uint8_t ImageUrl_u;
		@Field(desc="最低消费")
		uint32_t Minimum;
		uint8_t Minimum_u;
		@Field(desc="兑现比例")
		uint32_t WithdrawRate;
		uint8_t WithdrawRate_u;
		@Field(desc="最后更新时间")
		uint32_t LastUpdateTime;
		uint8_t LastUpdateTime_u;
		
		@Field(desc = "键值对属性,skuactid : 活动id;limit_classs 类目规则 limit_class_name 限制的类目名称")
        Map<String,String> mpProperty;
    	uint8_t mpProperty_u;
	}

	@ApiProtocol(cmdid = "0x91111804L", desc = "获取红包批次")
	class GetPacketStock{
		@ApiProtocol(cmdid = "0x91111804L", desc = "获取红包批次请求")
		class Req{
			@Field(desc="请求来源描述")
			String Source;
			@Field(desc="场景id")
			uint32_t SceneId;
			@Field(desc="请求ip")
			String ReqIp;
			@Field(desc="获取红包批次请求")
			GetPacketStockRequest Request;
			@Field(desc="请求MachineKey")
			String MachineKey;
			@Field(desc="保留输入字段")
			String ReserveIn;
		}
		@ApiProtocol(cmdid = "0x91118804L", desc = "获取红包批次回复")
		class Resp{
			@Field(desc="获取红包批次回复")
			GetPacketStockResponse Response;
			@Field(desc="错误信息")
			String ErrMsg;
			@Field(desc="保留输出字段")
			String ReserveOut;
		}
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "获取红包批次请求", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class GetPacketStockRequest{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="获取批次者uin")
		uint32_t ReqUin;
		uint8_t ReqUin_u;
		@Field(desc="红包批次过滤")
		PacketStockReqFilter ReqFilter;
		uint8_t ReqFilter_u;
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "红包批次过滤信息", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class PacketStockReqFilter{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="错误信息")
		String ErrMsg;
		uint8_t ErrMsg_u;
		@Field(desc="红包名称")
		String PacketName;
		uint8_t PacketName_u;
		@Field(desc="红包批次状态")
		uint32_t State;
		uint8_t State_u;
		@Field(desc="红包批次列表")
		Vector<uint64_t> PacketStockIdList;
		uint8_t PacketStockIdList_u;
		@Field(desc="红包批次类型")
		uint32_t Type;
		uint8_t Type_u;
		@Field(desc="红包批次发行开始时间")
		uint32_t BeginIssueTime;
		uint8_t BeginIssueTime_u;
		@Field(desc="红包批次发行结束时间")
		uint32_t EndIssueTime;
		uint8_t EndIssueTime_u;
		@Field(desc="开始位置")
		uint32_t Offset;
		uint8_t Offset_u;
		@Field(desc="所需数目")
		uint32_t Limit;
		uint8_t Limit_u;
		@Field(desc="红包批次uin")
		uint32_t Uin;
		uint8_t Uin_u;
		@Field(desc="红包属性")
		uint32_t Property;
		uint8_t Property_u;
		@Field(desc="红包批次有效期状态")
		uint32_t IndateState;
		uint8_t IndateState_u;
		@Field(desc="红包批次发放状态")
		uint32_t SendState;
		uint8_t SendState_u;
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "获取红包批次回复", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class GetPacketStockResponse{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="获取批次者uin")
		uint32_t RespUin;
		uint8_t RespUin_u;
		@Field(desc="红包批次列表")
		Vector<PacketStock> RespPacketStock;
		uint8_t RespPacketStock_u;
		@Field(desc="红包批次总数")
		uint32_t PacketStockTotal;
		uint8_t PacketStockTotal_u;
	}

	@ApiProtocol(cmdid = "0x91111805L", desc = "获取红包和批次列表")
	class GetPackeAndStock{
		@ApiProtocol(cmdid = "0x91111805L", desc = "获取红包和批次列表请求")
		class Req{
			@Field(desc="请求来源描述")
			String Source;
			@Field(desc="场景id")
			uint32_t SceneId;
			@Field(desc="请求ip")
			String ReqIp;
			@Field(desc="获取红包和批次信息列表")
			GetPackeAndStockRequest Request;
			@Field(desc="请求Machinekey")
			String MachineKey;
			@Field(desc="保留输入字段")
			String ReserveIn;
		}
		@ApiProtocol(cmdid = "0x91118805L", desc = "获取红包和批次列表回复")
		class Resp{
			@Field(desc="获取红包和批次列表回复")
			GetPackeAndStockResponse Response;
			@Field(desc="错误信息")
			String ErrMsg;
			@Field(desc="保留输出字段")
			String ReserveOut;
		}
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "获取红包和批次列表请求", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class GetPackeAndStockRequest{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="获取者uin")
		uint32_t ReqUin;
		uint8_t ReqUin_u;
		@Field(desc="获取请求过滤器")
		PacketAndStockReqFilter ReqFilter;
		uint8_t ReqFilter_u;
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "获取红包和批次请求过滤器", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class PacketAndStockReqFilter{
		@Field(desc="请求信息类型")
		uint32_t InfoType;
		uint8_t InfoType_u;
		@Field(desc="红包名称")
		String PacketName;
		uint8_t PacketName_u;
		@Field(desc="红包批次id")
		uint64_t PacketStockId;
		uint8_t PacketStockId_u;
		@Field(desc="红包owner")
		uint32_t OwnerUin;
		uint8_t OwnerUin_u;
		@Field(desc="红包使用seller")
		uint32_t SellerUin;
		uint8_t SellerUin_u;
		@Field(desc="红包状态")
		uint32_t State;
		uint8_t State_u;
		@Field(desc="绑定订单id")
		String DealCode;
		uint8_t DealCode_u;
		@Field(desc="兑现编号")
		uint64_t WithdrawId;
		uint8_t WithdrawId_u;
		@Field(desc="最大面值")
		uint32_t MaxFaceValue;
		uint8_t MaxFaceValue_u;
		@Field(desc="适用店铺")
		Set<uint32_t> ApplicableScope;
		uint8_t ApplicableScope_u;
		@Field(desc="红包编号列表")
		Vector<uint64_t> PacketIdList;
		uint8_t PacketIdList_u;
		@Field(desc="红包类型")
		uint32_t Type;
		uint8_t Type_u;
		@Field(desc="开始位置")
		uint32_t Offset;
		uint8_t Offset_u;
		@Field(desc="获取数量")
		uint32_t Limit;
		uint8_t Limit_u;
		@Field(desc="版本")
		uint32_t Version;
		uint8_t Version_u;
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "获取红包和批次列表回复", isSetClassSize = true, isNeedUFlag = true, version = 20130328)
	class GetPackeAndStockResponse{
		@Field(desc="版本",defaultValue="20130328")
		uint32_t version;
		uint8_t version_u;
		@Field(desc="获取者uin")
		uint32_t RespUin;
		uint8_t RespUin_u;
		@Field(desc="红包和批次列表")
		Vector<PacketAndStockList> PacketAndStock;
		uint8_t PacketAndStock_u;
		@Field(desc = "红包总数",version=20130328)
        uint32_t totalNum;
		@Field(version=20130328)
		uint8_t totalNum_u;
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "红包和批次信息", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class PacketAndStockList{
		@Field(desc="红包编号")
		uint64_t PacketId;
		uint8_t PacketId_u;
		@Field(desc="红包批次")
		uint64_t PacketStockId;
		uint8_t PacketStockId_u;
		@Field(desc="红包名称")
		String PacketName;
		uint8_t PacketName_u;
		@Field(desc="红包类型")
		uint32_t Type;
		uint8_t Type_u;
		@Field(desc="红包owner")
		uint32_t OwnerUin;
		uint8_t OwnerUin_u;
		@Field(desc="红包面值")
		uint32_t PacketPrice;
		uint8_t PacketPrice_u;
		@Field(desc="红包标识")
		uint32_t PacketFlag;
		uint8_t PacketFlag_u;
		@Field(desc="生效日期")
		uint32_t BeginTime;
		uint8_t BeginTime_u;
		@Field(desc="失效日期")
		uint32_t EndTime;
		uint8_t EndTime_u;
		@Field(desc="领取ip")
		String GetIp;
		uint8_t GetIp_u;
		@Field(desc="领取机器码")
		String GetMachineKey;
		uint8_t GetMachineKey_u;
		@Field(desc="领取时间")
		uint32_t RecvTime;
		uint8_t RecvTime_u;
		@Field(desc="使用ip")
		String UseIp;
		uint8_t UseIp_u;
		@Field(desc="使用机器码")
		String UseMachineKey;
		uint8_t UseMachineKey_u;
		@Field(desc="使用时间")
		uint32_t UsedTime;
		uint8_t UsedTime_u;
		@Field(desc="兑现编号")
		uint64_t WithdrawId;
		uint8_t WithdrawId_u;
		@Field(desc="兑现时间")
		uint32_t WithdrawTime;
		uint8_t WithdrawTime_u;
		@Field(desc="兑现财付通时间")
		uint32_t WithdrawCftTime;
		uint8_t WithdrawCftTime_u;
		@Field(desc="实际抵扣金额")
		uint32_t ActualPrice;
		uint8_t ActualPrice_u;
		@Field(desc="兑现金额")
		uint32_t WithdrawAmount;
		uint8_t WithdrawAmount_u;
		@Field(desc="最低消费")
		uint32_t Minimum;
		uint8_t Minimum_u;
		@Field(desc="兑现比例")
		uint32_t WithdrawRate;
		uint8_t WithdrawRate_u;
		@Field(desc="订单卖家uin")
		uint32_t SellerUin;
		uint8_t SellerUin_u;
		@Field(desc="关联订单编号")
		String DealCode;
		uint8_t DealCode_u;
		@Field(desc="关联地址")
		String RelaUrl;
		uint8_t RelaUrl_u;
		@Field(desc="图片地址")
		String ImageUrl;
		uint8_t ImageUrl_u;
		@Field(desc="红包状态")
		uint32_t State;
		uint8_t State_u;
		@Field(desc="适用店铺")
		Set<uint32_t> ApplicableScope;
		uint8_t ApplicableScope_u;
		@Field(desc="最后更新时间")
		uint32_t LastUpdateTime;
		uint8_t LastUpdateTime_u;
		@Field(desc="红包批次")
		PacketStock PacketStockInfo;
		uint8_t PacketStockInfo_u;
		@Field(desc="兑现记录")
		WithdrawRecord WithdrawRecordInfo;
		uint8_t WithdrawRecordInfo_u;
		@Field(desc="版本")
		uint32_t Version;
		uint8_t Version_u;
    
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "红包兑现记录", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class WithdrawRecord{
		@Field(desc="兑现编号id")
		uint64_t WithdrawId;
		uint8_t WithdrawId_u;
		@Field(desc="财付通转账包序列编号id")
		String CftPackageId;
		uint8_t CftPackageId_u;
		@Field(desc="卖家uin")
		uint32_t SellerUin;
		uint8_t SellerUin_u;
		@Field(desc="兑现金额")
		uint32_t TotalAmount;
		uint8_t TotalAmount_u;
		@Field(desc="兑现财付通时间")
		uint32_t WithdrawCftTime;
		uint8_t WithdrawCftTime_u;
		@Field(desc="兑现时间")
		uint32_t WithdrawTime;
		uint8_t WithdrawTime_u;
		@Field(desc="申请兑现时间")
		uint32_t ApplyTime;
		uint8_t ApplyTime_u;
		@Field(desc="兑现状态")
		uint32_t State;
		uint8_t State_u;
		@Field(desc="兑现ip地址")
		String WithdrawIp;
		uint8_t WithdrawIp_u;
		@Field(desc="兑现机器码")
		String WithdrawMachineKey;
		uint8_t WithdrawMachineKey_u;
		@Field(desc="最后更新时间")
		uint32_t LastUpdateTime;
		uint8_t LastUpdateTime_u;
		@Field(desc="版本")
		uint32_t Version;
		uint8_t Version_u;
	}
	@ApiProtocol(cmdid = "0x91111806L", desc = "冻结红包")
	class FreezeRedPacket{
		@ApiProtocol(cmdid = "0x91111806L", desc = "冻结红包请求")
		class Req{
			@Field(desc="请求来源描述")
			String Source;
			@Field(desc="场景id")
			uint32_t SceneId;
			@Field(desc="请求ip")
			String ReqIp;
			@Field(desc="冻结红包请求")
			FreezeRedPacketRequest Request;
			@Field(desc="请求MachineKey")
			String MachineKey;
			@Field(desc="保留输入字段")
			String ReserveIn;
		}
		@ApiProtocol(cmdid = "0x91118806L", desc = "冻结红包回复")
		class Resp{
			@Field(desc="冻结红包回复")
			FreezeRedPacketResponse Response;
			@Field(desc="错误信息")
			String ErrMsg;
			@Field(desc="保留输出字段")
			String ReserveOut;
		}
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "冻结红包请求", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class FreezeRedPacketRequest{
		@Field(desc="版本")
		uint32_t Version;
		uint8_t Version_u;
        @Field(desc="冻结红包用户uin")
        uint32_t OwnerUin;
        uint8_t OwnerUin_u;
		@Field(desc="冻结红包列表")
        Map<uint64_t,String> FreezeRedPacketInfo;
        uint8_t FreezeRedPacketInfo_u;
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "冻结红包回复", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class FreezeRedPacketResponse{
		@Field(desc="版本")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="冻结红包错误列表")
        Map<uint64_t,uint32_t> FreezeRedPacketFailed;
        uint8_t FreezeRedPacketFailed_u;
	}
	@ApiProtocol(cmdid = "0x91111807L", desc = "绑定红包")
	class BindRedPacket{
		@ApiProtocol(cmdid = "0x91111807L", desc = "绑定红包请求")
		class Req{
			@Field(desc="请求来源描述")
			String Source;
			@Field(desc="场景id")
			uint32_t SceneId;
			@Field(desc="请求ip")
			String ReqIp;
			@Field(desc="绑定红包请求")
			BindRedPacketRequest Request;
			@Field(desc="请求MachineKey")
			String MachineKey;
			@Field(desc="保留输入字段")
			String ReserveIn;
		}
		@ApiProtocol(cmdid = "0x91118807L", desc = "绑定红包回复")
		class Resp{
			@Field(desc="绑定红包回复")
			BindRedPacketResponse Response;
			@Field(desc="错误信息")
			String ErrMsg;
			@Field(desc="保留输出字段")
			String ReserveOut;
		}
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "绑定红包请求", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class BindRedPacketRequest{
		@Field(desc="版本")
		uint32_t Version;
		uint8_t Version_u;
        @Field(desc="绑定红包用户uin")
        uint32_t OwnerUin;
        uint8_t OwnerUin_u;
		@Field(desc="绑定红包列表")
        Vector<BindRedPacketInfo> RedPacketForBind;
        uint8_t RedPacketForBind_u;
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "绑定红包回复", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class BindRedPacketResponse{
		@Field(desc="版本")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="绑定红包错误列表")
        Map<uint64_t,uint32_t> BindRedPacketFailed;
        uint8_t BindRedPacketFailed_u;
    }
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "绑定红包信息", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class BindRedPacketInfo{
		@Field(desc="版本")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="红包编号")
        uint64_t PacketId;
        uint8_t PacketId_u;
        @Field(desc="卖家QQ")
        uint32_t SellerUin;
        uint8_t SellerUin_u;
        @Field(desc="订单编号")
        String DealCode;
        uint8_t DealCode_u;
        @Field(desc="实际抵扣金额")
        uint32_t ActualPrice;
        uint8_t ActualPrice_u;
    }
	@ApiProtocol(cmdid = "0x91111808L", desc = "确认使用红包")
	class ConfirmRedPacket{
		@ApiProtocol(cmdid = "0x91111808L", desc = "确认使用红包请求")
		class Req{
			@Field(desc="请求来源描述")
			String Source;
			@Field(desc="场景id")
			uint32_t SceneId;
			@Field(desc="请求ip")
			String ReqIp;
			@Field(desc="确认使用红包请求")
			ConfirmRedPacketRequest Request;
			@Field(desc="请求MachineKey")
			String MachineKey;
			@Field(desc="保留输入字段")
			String ReserveIn;
		}
		@ApiProtocol(cmdid = "0x91118808L", desc = "确认使用红包回复")
		class Resp{
			@Field(desc="确认使用红包回复")
			ConfirmRedPacketResponse Response;
			@Field(desc="错误信息")
			String ErrMsg;
			@Field(desc="保留输出字段")
			String ReserveOut;
		}
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "确认使用红包请求", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class ConfirmRedPacketRequest{
		@Field(desc="版本")
		uint32_t Version;
		uint8_t Version_u;
        @Field(desc="确认使用红包用户uin")
        uint32_t OwnerUin;
        uint8_t OwnerUin_u;
		@Field(desc="确认使用红包列表")
        Vector<uint64_t> ConfirmRedPacketInfo;
        uint8_t ConfirmRedPacketInfo_u;
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "确认使用红包回复", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class ConfirmRedPacketResponse{
		@Field(desc="版本")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="确认使用红包错误列表")
        Map<uint64_t,uint32_t> ConfirmRedPacketFailed;
        uint8_t ConfirmRedPacketFailed_u;
    }
	@ApiProtocol(cmdid = "0x91111809L", desc = "退回红包")
	class RefundRedPacket{
		@ApiProtocol(cmdid = "0x91111809L", desc = "退回红包请求")
		class Req{
			@Field(desc="请求来源描述")
			String Source;
			@Field(desc="场景id")
			uint32_t SceneId;
			@Field(desc="请求ip")
			String ReqIp;
			@Field(desc="退回红包请求")
			RefundRedPacketRequest Request;
			@Field(desc="请求MachineKey")
			String MachineKey;
			@Field(desc="保留输入字段")
			String ReserveIn;
		}
		@ApiProtocol(cmdid = "0x91118809L", desc = "退回红包回复")
		class Resp{
			@Field(desc="退回红包回复")
			RefundRedPacketResponse Response;
			@Field(desc="错误信息")
			String ErrMsg;
			@Field(desc="保留输出字段")
			String ReserveOut;
		}
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "退回红包请求", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class RefundRedPacketRequest{
		@Field(desc="版本")
		uint32_t Version;
		uint8_t Version_u;
        @Field(desc="退回红包用户uin")
        uint32_t OwnerUin;
        uint8_t OwnerUin_u;
		@Field(desc="退回红包列表")
        Vector<uint64_t> RefundRedPacketInfo;
        uint8_t RefundRedPacketInfo_u;
	}
	@Member(cPlusNamespace = "c2cent::po::redpacketapi", desc = "退回红包回复", isSetClassSize = true, isNeedUFlag = true, version = 0)
	class RefundRedPacketResponse{
		@Field(desc="版本")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="退回红包错误列表")
        Map<uint64_t,uint32_t> RefundRedPacketFailed;
        uint8_t RefundRedPacketFailed_u;
    }
}
