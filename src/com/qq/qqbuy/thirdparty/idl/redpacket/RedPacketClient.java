package com.qq.qqbuy.thirdparty.idl.redpacket;


import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.paipai.lang.uint32_t;
import com.paipai.lang.uint64_t;
import com.qq.qqbuy.common.Log;

import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.redpacket.protocol.GetRedPacketListReq;
import com.qq.qqbuy.thirdparty.idl.redpacket.protocol.GetRedPacketListResp;
import com.qq.qqbuy.thirdparty.idl.redpacket.protocol.RedPacketFilter;
import com.qq.qqbuy.thirdparty.idl.redpacket.protocol.RedPacketInfo;
import com.qq.qqbuy.thirdparty.idl.redpacket.protocol.SendRedPacketReq;
import com.qq.qqbuy.thirdparty.idl.redpacket.protocol.SendRedPacketRequest;
import com.qq.qqbuy.thirdparty.idl.redpacket.protocol.SendRedPacketResp;

public class RedPacketClient extends SupportIDLBaseClient{

//    public static GetRedPacketListResponse getRedPacketListFromOpenApi(int state, long uin, String mk, int type,String sid,String lskey) {
//        try {
//            
//            long start = System.currentTimeMillis();
//            
//            int ret;
//            GetRedPacketListRequest req = new GetRedPacketListRequest();
//            req.setOwnerUin(uin);
//            req.setUin(uin);
//            req.setType(type);
//            req.setState(state);
//            req.setSid(sid);
//            req.setLskey(lskey);
//            GetRedPacketListResponse resp =  OpenApi.getProxy().getRedPacketList(req);
//            ret = (int) resp.common.clientErrorCode;
//            
//            long timeCost = System.currentTimeMillis() - start;
//            runLog(ret, genRunLogString("OpenApi#getRedPacketList", ret, resp.common.serverErrorCode, resp.common.serverErrorMessage, genParameterStr(
//                    "timeCost", timeCost,"uin",uin, "state", state))); 
//            perfLog(ret, genPerfLogString("IDL#getRedPacketList", ret, resp.common.serverErrorCode, resp.common.serverErrorMessage, timeCost));
////            reportIDLToMc(stub,Module.C_PP_IDL_REDPACKT,"getRedPacketList", timeCost,ret, (int)resp.common.serverErrorCode);
//            
////            JMCMonitor.getInstance().addResult(Module.C_PP_OPENAPI_REDPACKT.getName(), "0",  0,
////                    Module.C_PP_OPENAPI_REDPACKT+".getRedPacketList" ,  timeCost,  !needsAlarm(ret,resp.common.serverErrorCode));//在openAPI层统一上报
//            
//            return ret == SUCCESS ? resp : null;
//            
//        } catch (Exception e) {
//            Log.run.warn("getRedPacketList exp",e);
//            return null;
//        }
//    }
    /**
     * 接口人；陈雄
     * 接口的问题 店铺优惠券找下王小平  代金券找下李彦英
     * @param state
     * @param uin
     * @param mk
     * @return
     */
    public GetRedPacketListResp getRedPacketList(int state, long uin,long type, String skey) {
//        long start = System.currentTimeMillis();
        
        GetRedPacketListReq req = new GetRedPacketListReq();
        GetRedPacketListResp resp = new GetRedPacketListResp();
        
//        req.setMachineKey(mk);
        req.setSceneId(0L);
        req.setSource(CALL_IDL_SOURCE);
        RedPacketFilter filter = new RedPacketFilter();
        req.setOFilter(filter);
        filter.setOwnerUin(uin);
        filter.setState(state);
        filter.setType(type);
        
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
//        if (EnvManager.isLocalEnvCurrent() || EnvManager.isGAMMAEnvCurrent()) {
//			stub.setIpAndPort("10.6.222.233", 53101);// DEV
//			stub.setIpAndPort("10.6.223.152", 53101);// BETA
//		}
        stub.setUin(uin); // 应为setUin被服务端用来做负载均衡，故需要设置。
        stub.setOperator(uin);
//        stub.setSkey(AuthUtil.getSkFromMk(uin, mk).getBytes());
        stub.setSkey(skey.getBytes());
//        stub.setMachineKey(mk.getBytes());
        ret = invokePaiPaiIDL(req, resp,stub);
        
//        long timeCost = System.currentTimeMillis() - start;
        return ret == SUCCESS ? resp : null;
    }
    
    
    /**
	 * 接口人；陈雄 接口的问题 店铺优惠券找下王小平 代金券找下李彦英
	 * @param uin
	 * @param redPackageIdList
	 * @return
	 */
	public static SendRedPacketResp sendRedPacket(long uin,Long redPackageId, int num, String vk, String ip) {
//		long start = System.currentTimeMillis();
		
		SendRedPacketReq req = new SendRedPacketReq();
		SendRedPacketResp resp = new SendRedPacketResp();		
		
		SendRedPacketRequest request = new SendRedPacketRequest() ;
		Vector<RedPacketInfo> vecRedPacketInfo = new Vector<RedPacketInfo>();
		RedPacketInfo redPacketInfo = new RedPacketInfo();
		redPacketInfo.setRecvUin(uin);
		Map<uint64_t, uint32_t> mapWaitSendRedPackage = new HashMap<uint64_t, uint32_t>();
		//mapWaitSendRedPackage.put(new uint64_t(redPackageId), new uint32_t(num));
		mapWaitSendRedPackage.put(new uint64_t(redPackageId), new uint32_t(1));//基本不会遇到发多张券的可能，防止代码或配置粗心造成发多张的情况，写死为1个圈
		redPacketInfo.setWaitSendRedPacket(mapWaitSendRedPackage);
		vecRedPacketInfo.add(redPacketInfo);
		request.setVersion(20150304L);
		request.setSendUin(0);
		request.setWaitSendInfo(vecRedPacketInfo);
		request.setVisitKey(vk) ;
		request.setClientIP(ip) ;
		req.setSceneId(4L);
		req.setSource("wuxian");
		req.setRequest(request) ;
		req.setReserveIn("2091ecf1e35f0e72728cba78507eca4e");
		req.setMachineKey(String.valueOf(System.currentTimeMillis())) ;
		
		int ret = 0;
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		stub.setUin(uin); // 应为setUin被服务端用来做负载均衡，故需要设置。
		stub.setRouteKey(uin);		
		try {
			//stub.setIpAndPort("10.130.0.237", 53101) ;
			ret = stub.invoke(req, resp);
		} catch (Exception e) {
			Log.run.warn("e", e);
			ret = ERRCODE_CALLIDL_FAIL;
		}
//		long timeCost = System.currentTimeMillis() - start;
		return ret == SUCCESS ? resp : null;
	}
}
