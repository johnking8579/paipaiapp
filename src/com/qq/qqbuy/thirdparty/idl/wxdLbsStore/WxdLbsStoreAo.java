package com.qq.qqbuy.thirdparty.idl.wxdLbsStore;

import java.util.Map;
import java.util.Vector;
import java.util.Set;

import com.paipai.lang.uint64_t;
import com.paipai.lang.uint32_t;
import com.paipai.lang.uint8_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;

@HeadApiProtocol(cPlusNamespace="c2cent::ao::wxdlbs", needInit=true, timers="60")
public class WxdLbsStoreAo {
    @Member(cPlusNamespace="c2cent::wxd::po::lbs", desc = "门店信息", isNeedUFlag = true)
    public class LbsStoreInfoPo {
      @Field(desc = "")
      uint32_t Version;
      uint8_t Version_u;
      
      @Field(desc = "用户qq")
      uint64_t Uin;
      uint8_t Uin_u;
      
      @Field(desc = "门店id")
      String StoreId;
      uint8_t StoreId_u;
    
      @Field(desc = "门店地址（国）")
      String Address1;
      uint8_t Address1_u;
      
      @Field(desc = "门店地址（省）")
      String Address2;
      uint8_t Address2_u;
      
      @Field(desc = "门店地址（省id）")
      uint32_t Address2Id;
      uint8_t Address2Id_u;
      
      @Field(desc = "门店地址（市）")
      String Address3;
      uint8_t Address3_u;
      
      @Field(desc = "门店地址（市id）")
      uint32_t Address3Id;
      uint8_t Address3Id_u;
      
      @Field(desc = "门店地址（区）")
      String Address4;
      uint8_t Address4_u;
      
      @Field(desc = "门店地址（区id）")
      uint32_t Address4Id;
      uint8_t Address4Id_u;
      
      @Field(desc = "门店地址（街道）")
      String Address5;
      uint8_t Address5_u;
      
      @Field(desc = "门店地址（街道id）")
      uint32_t Address5Id;
      uint8_t Address5Id_u;
      
      @Field(desc = "门店地址预留")
      String Address6;
      uint8_t Address6_u;
      
      @Field(desc = "门店地址预留")
      uint32_t Address6Id;
      uint8_t Address6Id_u;
      
      @Field(desc = "门店地址预留")
      String Address7;
      uint8_t Address7_u;
      
      @Field(desc = "门店地址预留")
      uint32_t Address7Id;
      uint8_t Address7Id_u;
      
      @Field(desc = "门店详细地址")
      String DetailAddress;
      uint8_t DetailAddress_u;
      
      @Field(desc = "门店坐标（纬）")
      uint64_t Flatitude;
      uint8_t Flatitude_u;
      
      @Field(desc = "门店坐标（经）")
      uint64_t Flongitude;
      uint8_t Flongitude_u;
      
      @Field(desc = "品类字段id")
      uint32_t Category;
      uint8_t Category_u;

      @Field(desc = "门店地址hash")
      String Hash;
      uint8_t Hash_u;
      
      @Field(desc = "门店状态0:正常状态1:删除")
      uint32_t State;
      uint8_t State_u;
      
      @Field(desc = "添加时间")
      uint32_t AddTime;
      uint8_t AddTime_u;
      
      @Field(desc = "更新时间")
      uint32_t UpdateTime;
      uint8_t UpdateTime_u;

      @Field(desc = "扩展字段")
      Map<String, String> Ext;
      uint8_t Ext_u;     
    }
    
    @Member(cPlusNamespace="c2cent::wxd::po::lbs", desc = "坐标", isNeedUFlag = true)
    public class LbsStoreCoordinate {
      @Field(desc = "")
      uint32_t Version;
      uint8_t Version_u;
      
      @Field(desc = "当前位置（纬）")
      uint64_t Latitude;
      uint8_t Latitude_u;
        
      @Field(desc = "当前位置（经）")
      uint64_t Longitude;
      uint8_t Longitude_u;
    }
    
    @Member(cPlusNamespace="c2cent::wxd::po::lbs", desc = "地址级别过滤条件", isNeedUFlag = true)
    public class LbsStoreFilterByAddress {
      @Field(desc = "")
      uint32_t Version;
      uint8_t Version_u;
      
      @Field(desc = "当前位置坐标")
      LbsStoreCoordinate Coordinate;
      uint8_t Coordinate_u;
      
      @Field(desc = "查询地址级别：0：国，1：省，2：市，3：区，4：街道")
      uint32_t AddressLevel;
      uint8_t AddressLevel_u;
      
      @Field(desc = "门店地址（国）")
      String Address1;
      uint8_t Address1_u;
      
      @Field(desc = "门店地址（省）")
      String Address2;
      uint8_t Address2_u;
      
      @Field(desc = "门店地址（省id）")
      uint32_t Address2Id;
      uint8_t Address2Id_u;
      
      @Field(desc = "门店地址（市）")
      String Address3;
      uint8_t Address3_u;
      
      @Field(desc = "门店地址（市id）")
      uint32_t Address3Id;
      uint8_t Address3Id_u;
      
      @Field(desc = "门店地址（区）")
      String Address4;
      uint8_t Address4_u;
      
      @Field(desc = "门店地址（区id）")
      uint32_t Address4Id;
      uint8_t Address4Id_u;
      
      @Field(desc = "门店地址（街道）")
      String Address5;
      uint8_t Address5_u;
      
      @Field(desc = "门店地址（街道id）")
      uint32_t Address5Id;
      uint8_t Address5Id_u;
      
      @Field(desc = "品类字段id")
      uint32_t Category;
      uint8_t Category_u;
      
      @Field(desc = "排序方式:暂不使用")
      uint32_t OrderType;
      uint8_t OrderType_u;
      
      @Field(desc = "从第几页记录开始")
      uint64_t StartPage;
      uint8_t  StartPage_u;
        
      @Field(desc = "每页记录数")
      uint64_t PageNum;
      uint8_t  PageNum_u; 
      
      @Field(desc = "扩展字段")
      Map<String, String> Ext;
      uint8_t Ext_u;
    }
    
    @Member(cPlusNamespace="c2cent::wxd::po::lbs", desc = "附近位置过滤条件", isNeedUFlag = true)
    public class LbsStoreFilterByNear {
      @Field(desc = "")
      uint32_t Version;
      uint8_t Version_u;
      
      @Field(desc = "当前位置坐标")
      LbsStoreCoordinate Coordinate;
      uint8_t Coordinate_u;

      @Field(desc = "门店地址（市id）,分表查询需要")
      uint32_t Address3Id;
      uint8_t Address3Id_u;
      
      @Field(desc = "品类字段id")
      uint32_t Category;
      uint8_t Category_u;
      
      @Field(desc = "排序方式")
      uint32_t OrderType;
      uint8_t OrderType_u;
      
      @Field(desc = "从第几页记录开始")
      uint64_t StartPage;
      uint8_t  StartPage_u;
        
      @Field(desc = "每页记录数")
      uint64_t PageNum;
      uint8_t  PageNum_u; 
      
      @Field(desc = "扩展字段")
      Map<String, String> Ext;
      uint8_t Ext_u;
    }
    
    @Member(cPlusNamespace="c2cent::wxd::po::lbs", desc = "坐标范围内过滤条件", isNeedUFlag = true)
    public class LbsStoreFilterByCoordinate {
      @Field(desc = "")
      uint32_t Version;
      uint8_t Version_u;
      
      @Field(desc = "当前位置坐标")
      Vector<LbsStoreCoordinate> Coordinate;
      uint8_t Coordinate_u;

      @Field(desc = "门店地址（市id）,分表查询需要")
      uint32_t Address3Id;
      uint8_t Address3Id_u;
      
      @Field(desc = "品类字段id")
      uint32_t Category;
      uint8_t Category_u;
      
      @Field(desc = "排序方式")
      uint32_t OrderType;
      uint8_t OrderType_u;
      
      @Field(desc = "从第几页记录开始")
      uint64_t StartPage;
      uint8_t  StartPage_u;
        
      @Field(desc = "每页记录数")
      uint64_t PageNum;
      uint8_t  PageNum_u; 
      
      @Field(desc = "扩展字段")
      Map<String, String> Ext;
      uint8_t Ext_u;
    }
    
    @Member(cPlusNamespace="c2cent::wxd::po::lbs", desc = "QQ或者门店ID过滤条件", isNeedUFlag = true)
    public class LbsStoreFilterByID {
      @Field(desc = "")
      uint32_t Version;
      uint8_t Version_u;
      
      @Field(desc = "用户qq")
      uint64_t Uin;
      uint8_t Uin_u;
      
      @Field(desc = "门店id")
      String StoreId;
      uint8_t StoreId_u;
      
      @Field(desc = "类型：0:QQ号；1:门店id")
      uint8_t Type;
      uint8_t Type_u;
      
      @Field(desc = "排序方式")
      uint32_t OrderType;
      uint8_t OrderType_u;
      
      @Field(desc = "从第几页记录开始")
      uint64_t StartPage;
      uint8_t  StartPage_u;
        
      @Field(desc = "每页记录数")
      uint64_t PageNum;
      uint8_t  PageNum_u; 
      
      @Field(desc = "扩展字段")
      Map<String, String> Ext;
      uint8_t Ext_u;
    }
    
    @Member(cPlusNamespace="c2cent::wxd::po::lbs", desc = "门店信息汇总", isNeedUFlag = true)
    public class LbsStoreInfoAllPo {
      @Field(desc = "")
      uint32_t Version;
      uint8_t Version_u;
      
      @Field(desc = "用户qq")
      uint64_t Uin;
      uint8_t Uin_u;
      
      @Field(desc = "门店id")
      String StoreId;
      uint8_t StoreId_u;
    
      @Field(desc = "门店地址（国）")
      String Address1;
      uint8_t Address1_u;
      
      @Field(desc = "门店地址（省）")
      String Address2;
      uint8_t Address2_u;
      
      @Field(desc = "门店地址（省id）")
      uint32_t Address2Id;
      uint8_t Address2Id_u;
      
      @Field(desc = "门店地址（市）")
      String Address3;
      uint8_t Address3_u;
      
      @Field(desc = "门店地址（市id）")
      uint32_t Address3Id;
      uint8_t Address3Id_u;
      
      @Field(desc = "门店地址（区）")
      String Address4;
      uint8_t Address4_u;
      
      @Field(desc = "门店地址（区id）")
      uint32_t Address4Id;
      uint8_t Address4Id_u;
      
      @Field(desc = "门店地址（街道）")
      String Address5;
      uint8_t Address5_u;
      
      @Field(desc = "门店地址（街道id）")
      uint32_t Address5Id;
      uint8_t Address5Id_u;
      
      @Field(desc = "门店地址预留")
      String Address6;
      uint8_t Address6_u;
      
      @Field(desc = "门店地址预留")
      uint32_t Address6Id;
      uint8_t Address6Id_u;
      
      @Field(desc = "门店地址预留")
      String Address7;
      uint8_t Address7_u;
      
      @Field(desc = "门店地址预留")
      uint32_t Address7Id;
      uint8_t Address7Id_u;
      
      @Field(desc = "门店详细地址")
      String DetailAddress;
      uint8_t DetailAddress_u;
      
      @Field(desc = "门店坐标（纬）")
      uint64_t Flatitude;
      uint8_t Flatitude_u;
      
      @Field(desc = "门店坐标（经）")
      uint64_t Flongitude;
      uint8_t Flongitude_u;
      
      @Field(desc = "品类字段id")
      uint32_t Category;
      uint8_t Category_u;

      @Field(desc = "门店地址hash")
      String Hash;
      uint8_t Hash_u;
      
      @Field(desc = "门店状态0:正常状态1:删除")
      uint32_t State;
      uint8_t State_u;
      
      @Field(desc = "添加时间")
      uint32_t AddTime;
      uint8_t AddTime_u;
      
      @Field(desc = "更新时间")
      uint32_t UpdateTime;
      uint8_t UpdateTime_u;

      @Field(desc = "扩展字段")
      Map<String, String> Ext;
      uint8_t Ext_u;
      
      @Field(desc = "门店名称")
      String   		storeName;
      uint8_t  		storeName_u;
      
      @Field(desc = "主营类目")
      String			productcategory;
	  uint8_t			productcategory_u;
        
      @Field(desc = "联系电话")
      String 			telphone;
      uint8_t 		telphone_u;
    
      @Field(desc = "人均价格")
      uint32_t 		renPrice;
      uint8_t 		renPrice_u;
        
      @Field(desc = "营业时间")
      String 			openTime;
      uint8_t 		openTime_u;
        
      @Field(desc = "卖家推荐信息")
      String			recommend;
      uint8_t 		recommend_u;
        
      @Field(desc = "特色服务")
      String 			specService;
      uint8_t 		specService_u;
        
      @Field(desc = "简介")
      String 			introduction;
      uint8_t 		introduction_u;
    }
    
    @ApiProtocol(cmdid = "0x879e1801L", desc = "新增门店信息")
    class AddStoreInfo {
      @ApiProtocol(cmdid = "0x879e1801L", desc = "请求")
      class Req {
        @Field(desc="用户机器码")
        String machineKey;      	
        	
        @Field(desc="请求来源，不可为空，填写来源文件名")
        String source;

        @Field(desc="场景id,填调用方的cmdid,如果调用方没有命令号,填0")
        uint32_t sceneId;
			
        @Field(desc="用户ID")
        uint64_t sellerUin;
        
        @Field(desc = "门店信息")
        LbsStoreInfoPo storeInfo;
      
        @Field(desc = "保留字段")
        String inReserve;
      }

      @ApiProtocol(cmdid = "0x879e8801L", desc = "响应")
      class Resp {
        @Field(desc = "错误信息")
        String errorMsg;
        
        @Field(desc = "保留字段")
        String outReserve;
      }
    }
    
    @ApiProtocol(cmdid = "0x879e1802L", desc = "修改门店信息")
    class UpdateStoreInfo {
      @ApiProtocol(cmdid = "0x879e1802L", desc = "请求")
      class Req {
        @Field(desc="用户机器码")
        String machineKey;      	
        	
        @Field(desc="请求来源，不可为空，填写来源文件名")
        String source;   
            
        @Field(desc="场景id,填调用方的cmdid,如果调用方没有命令号,填0")
        uint32_t sceneId;
			
        @Field(desc="用户ID")
        uint64_t sellerUin;
        
        @Field(desc = "门店信息")
        LbsStoreInfoPo storeInfo;
        
        @Field(desc = "保留字段")
        String inReserve;
      }

      @ApiProtocol(cmdid = "0x879e8802L", desc = "响应")
      class Resp {
        @Field(desc = "错误信息")
        String errorMsg;
        
        @Field(desc = "保留字段")
        String outReserve;
      }
    }
    
    @ApiProtocol(cmdid = "0x879e1803L", desc = "删除门店信息")
    class DeleteStoreInfo {
      @ApiProtocol(cmdid = "0x879e1803L", desc = "请求")
      class Req {
        @Field(desc="用户机器码")
        String machineKey;      	
        	
        @Field(desc="请求来源，不可为空，填写来源文件名")
        String source;   
            
        @Field(desc="场景id,填调用方的cmdid,如果调用方没有命令号,填0")
        uint32_t sceneId;
			
        @Field(desc="用户ID")
        uint64_t sellerUin;
        
        @Field(desc = "门店ID")
        String storeId;
        
        @Field(desc = "保留字段")
        String inReserve;
      }

      @ApiProtocol(cmdid = "0x879e8803L", desc = "响应")
      class Resp {
        @Field(desc = "错误信息")
        String errorMsg;
        
        @Field(desc = "保留字段")
        String outReserve;
      }
    }
    
    @ApiProtocol(cmdid = "0x879e1804L", desc = "根据地址级别查询门店信息")
    class QueryStoreInfoByAdress {
      @ApiProtocol(cmdid = "0x879e1804L", desc = "请求")
      class Req {
        @Field(desc="用户机器码")
        String machineKey;  	
        	
        @Field(desc="请求来源，不可为空，填写来源文件名")
        String source;
            
        @Field(desc="场景id,填调用方的cmdid,如果调用方没有命令号,填0")
        uint32_t sceneId;
			
        @Field(desc="用户ID")
        uint64_t sellerUin;
        
        @Field(desc = "查询过滤")
        LbsStoreFilterByAddress filter;

        @Field(desc = "保留字段")
        String inReserve;
      }

      @ApiProtocol(cmdid = "0x879e8804L", desc = "响应")
      class Resp {
        @Field(desc = "满足条件的总数")
        uint32_t total;
        
        @Field(desc = "结果")
        Vector<LbsStoreInfoAllPo> storeInfo;
        
        @Field(desc = "错误信息")
        String errorMsg;
        
        @Field(desc = "保留字段")
        String outReserve;
      }
    }
    
    @ApiProtocol(cmdid = "0x879e1805L", desc = "查询附近的门店信息")
    class QueryStoreInfoByNear {
      @ApiProtocol(cmdid = "0x879e1805L", desc = "请求")
      class Req {
        @Field(desc="用户机器码")
        String machineKey;      	
        	
        @Field(desc="请求来源，不可为空，填写来源文件名")
        String source;   
            
        @Field(desc="场景id,填调用方的cmdid,如果调用方没有命令号,填0")
        uint32_t sceneId;
			
        @Field(desc="用户ID")
        uint64_t sellerUin;
        
        @Field(desc = "查询过滤")
        LbsStoreFilterByNear filter;

        @Field(desc = "保留字段")
        String inReserve;
      }
      
      @ApiProtocol(cmdid = "0x879e8805L", desc = "响应")
      class Resp {
        @Field(desc = "满足条件的总数")
        uint32_t Total;
        
        @Field(desc = "结果")
        Vector<LbsStoreInfoAllPo> StoreInfo;
        
        @Field(desc = "错误信息")
        String ErrorMsg;
        
        @Field(desc = "保留字段")
        String outReserve;
      }
    }
    
    @ApiProtocol(cmdid = "0x879e1806L", desc = "查询坐标范围内的门店信息")
    class QueryStoreInfoByCoordinate {
      @ApiProtocol(cmdid = "0x879e1806L", desc = "请求")
      class Req {
        @Field(desc="用户机器码")
        String machineKey;
        	
        @Field(desc="请求来源，不可为空，填写来源文件名")
        String source; 
            
        @Field(desc="场景id,填调用方的cmdid,如果调用方没有命令号,填0")
        uint32_t sceneId;
			
        @Field(desc="用户ID")
        uint64_t sellerUin;
        
        @Field(desc = "查询过滤")
        LbsStoreFilterByCoordinate filter;

        @Field(desc = "保留字段")
        String inReserve;
      }

      @ApiProtocol(cmdid = "0x879e8806L", desc = "响应")
      class Resp {
        @Field(desc = "满足条件的总数")
        uint32_t Total;
        
        @Field(desc = "结果")
        Vector<LbsStoreInfoAllPo> StoreInfo;
        
        @Field(desc = "错误信息")
        String ErrorMsg;
        
        @Field(desc = "保留字段")
        String outReserve;
      }
    }
    
    @ApiProtocol(cmdid = "0x879e1807L", desc = "根据QQ号或门店ID查询门店信息")
    class QueryStoreInfoByStoreId {
      @ApiProtocol(cmdid = "0x879e1807L", desc = "请求")
      class Req {
        @Field(desc="用户机器码")
        String machineKey;	
        	
        @Field(desc="请求来源，不可为空，填写来源文件名")
        String source;   
            
        @Field(desc="场景id,填调用方的cmdid,如果调用方没有命令号,填0")
        uint32_t sceneId;
			
        @Field(desc="用户ID")
        uint64_t sellerUin;
        
        @Field(desc = "查询过滤")
        LbsStoreFilterByID filter;

        @Field(desc = "保留字段")
        String inReserve;
      }

      @ApiProtocol(cmdid = "0x879e8807L", desc = "响应")
      class Resp {
        @Field(desc = "满足条件的总数")
        uint32_t Total;
        
        @Field(desc = "结果")
        Vector<LbsStoreInfoAllPo> StoreInfo;
        
        @Field(desc = "错误信息")
        String ErrorMsg;
        
        @Field(desc = "保留字段")
        String outReserve;
      }
    }
}
