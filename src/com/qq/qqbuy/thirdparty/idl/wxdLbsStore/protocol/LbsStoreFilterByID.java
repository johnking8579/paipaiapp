//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.wxdLbsStore.WxdLbsStoreAo.java

package com.qq.qqbuy.thirdparty.idl.wxdLbsStore.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import java.util.Map;
import java.util.HashMap;

/**
 *QQ或者门店ID过滤条件
 *
 *@date 2015-03-18 05:00:00
 *
 *@since version:0
*/
public class LbsStoreFilterByID  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 用户qq
	 *
	 * 版本 >= 0
	 */
	 private long Uin;

	/**
	 * 版本 >= 0
	 */
	 private short Uin_u;

	/**
	 * 门店id
	 *
	 * 版本 >= 0
	 */
	 private String StoreId = new String();

	/**
	 * 版本 >= 0
	 */
	 private short StoreId_u;

	/**
	 * 类型：0:QQ号；1:门店id
	 *
	 * 版本 >= 0
	 */
	 private short Type;

	/**
	 * 版本 >= 0
	 */
	 private short Type_u;

	/**
	 * 排序方式
	 *
	 * 版本 >= 0
	 */
	 private long OrderType;

	/**
	 * 版本 >= 0
	 */
	 private short OrderType_u;

	/**
	 * 从第几页记录开始
	 *
	 * 版本 >= 0
	 */
	 private long StartPage;

	/**
	 * 版本 >= 0
	 */
	 private short StartPage_u;

	/**
	 * 每页记录数
	 *
	 * 版本 >= 0
	 */
	 private long PageNum;

	/**
	 * 版本 >= 0
	 */
	 private short PageNum_u;

	/**
	 * 扩展字段
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> Ext = new HashMap<String,String>();

	/**
	 * 版本 >= 0
	 */
	 private short Ext_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushLong(Uin);
		bs.pushUByte(Uin_u);
		bs.pushString(StoreId);
		bs.pushUByte(StoreId_u);
		bs.pushUByte(Type);
		bs.pushUByte(Type_u);
		bs.pushUInt(OrderType);
		bs.pushUByte(OrderType_u);
		bs.pushLong(StartPage);
		bs.pushUByte(StartPage_u);
		bs.pushLong(PageNum);
		bs.pushUByte(PageNum_u);
		bs.pushObject(Ext);
		bs.pushUByte(Ext_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		Uin = bs.popLong();
		Uin_u = bs.popUByte();
		StoreId = bs.popString();
		StoreId_u = bs.popUByte();
		Type = bs.popUByte();
		Type_u = bs.popUByte();
		OrderType = bs.popUInt();
		OrderType_u = bs.popUByte();
		StartPage = bs.popLong();
		StartPage_u = bs.popUByte();
		PageNum = bs.popLong();
		PageNum_u = bs.popUByte();
		Ext = (Map<String,String>)bs.popMap(String.class,String.class);
		Ext_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取用户qq
	 * 
	 * 此字段的版本 >= 0
	 * @return Uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return Uin;
	}


	/**
	 * 设置用户qq
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.Uin = value;
		this.Uin_u = 1;
	}

	public boolean issetUin()
	{
		return this.Uin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Uin_u value 类型为:short
	 * 
	 */
	public short getUin_u()
	{
		return Uin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUin_u(short value)
	{
		this.Uin_u = value;
	}


	/**
	 * 获取门店id
	 * 
	 * 此字段的版本 >= 0
	 * @return StoreId value 类型为:String
	 * 
	 */
	public String getStoreId()
	{
		return StoreId;
	}


	/**
	 * 设置门店id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStoreId(String value)
	{
		this.StoreId = value;
		this.StoreId_u = 1;
	}

	public boolean issetStoreId()
	{
		return this.StoreId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return StoreId_u value 类型为:short
	 * 
	 */
	public short getStoreId_u()
	{
		return StoreId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStoreId_u(short value)
	{
		this.StoreId_u = value;
	}


	/**
	 * 获取类型：0:QQ号；1:门店id
	 * 
	 * 此字段的版本 >= 0
	 * @return Type value 类型为:short
	 * 
	 */
	public short getType()
	{
		return Type;
	}


	/**
	 * 设置类型：0:QQ号；1:门店id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setType(short value)
	{
		this.Type = value;
		this.Type_u = 1;
	}

	public boolean issetType()
	{
		return this.Type_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Type_u value 类型为:short
	 * 
	 */
	public short getType_u()
	{
		return Type_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setType_u(short value)
	{
		this.Type_u = value;
	}


	/**
	 * 获取排序方式
	 * 
	 * 此字段的版本 >= 0
	 * @return OrderType value 类型为:long
	 * 
	 */
	public long getOrderType()
	{
		return OrderType;
	}


	/**
	 * 设置排序方式
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOrderType(long value)
	{
		this.OrderType = value;
		this.OrderType_u = 1;
	}

	public boolean issetOrderType()
	{
		return this.OrderType_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return OrderType_u value 类型为:short
	 * 
	 */
	public short getOrderType_u()
	{
		return OrderType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOrderType_u(short value)
	{
		this.OrderType_u = value;
	}


	/**
	 * 获取从第几页记录开始
	 * 
	 * 此字段的版本 >= 0
	 * @return StartPage value 类型为:long
	 * 
	 */
	public long getStartPage()
	{
		return StartPage;
	}


	/**
	 * 设置从第几页记录开始
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setStartPage(long value)
	{
		this.StartPage = value;
		this.StartPage_u = 1;
	}

	public boolean issetStartPage()
	{
		return this.StartPage_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return StartPage_u value 类型为:short
	 * 
	 */
	public short getStartPage_u()
	{
		return StartPage_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStartPage_u(short value)
	{
		this.StartPage_u = value;
	}


	/**
	 * 获取每页记录数
	 * 
	 * 此字段的版本 >= 0
	 * @return PageNum value 类型为:long
	 * 
	 */
	public long getPageNum()
	{
		return PageNum;
	}


	/**
	 * 设置每页记录数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPageNum(long value)
	{
		this.PageNum = value;
		this.PageNum_u = 1;
	}

	public boolean issetPageNum()
	{
		return this.PageNum_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PageNum_u value 类型为:short
	 * 
	 */
	public short getPageNum_u()
	{
		return PageNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPageNum_u(short value)
	{
		this.PageNum_u = value;
	}


	/**
	 * 获取扩展字段
	 * 
	 * 此字段的版本 >= 0
	 * @return Ext value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getExt()
	{
		return Ext;
	}


	/**
	 * 设置扩展字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setExt(Map<String,String> value)
	{
		if (value != null) {
				this.Ext = value;
				this.Ext_u = 1;
		}
	}

	public boolean issetExt()
	{
		return this.Ext_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Ext_u value 类型为:short
	 * 
	 */
	public short getExt_u()
	{
		return Ext_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setExt_u(short value)
	{
		this.Ext_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(LbsStoreFilterByID)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段Uin的长度 size_of(uint64_t)
				length += 1;  //计算字段Uin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(StoreId, null);  //计算字段StoreId的长度 size_of(String)
				length += 1;  //计算字段StoreId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段Type的长度 size_of(uint8_t)
				length += 1;  //计算字段Type_u的长度 size_of(uint8_t)
				length += 4;  //计算字段OrderType的长度 size_of(uint32_t)
				length += 1;  //计算字段OrderType_u的长度 size_of(uint8_t)
				length += 17;  //计算字段StartPage的长度 size_of(uint64_t)
				length += 1;  //计算字段StartPage_u的长度 size_of(uint8_t)
				length += 17;  //计算字段PageNum的长度 size_of(uint64_t)
				length += 1;  //计算字段PageNum_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Ext, null);  //计算字段Ext的长度 size_of(Map)
				length += 1;  //计算字段Ext_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(LbsStoreFilterByID)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段Uin的长度 size_of(uint64_t)
				length += 1;  //计算字段Uin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(StoreId, encoding);  //计算字段StoreId的长度 size_of(String)
				length += 1;  //计算字段StoreId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段Type的长度 size_of(uint8_t)
				length += 1;  //计算字段Type_u的长度 size_of(uint8_t)
				length += 4;  //计算字段OrderType的长度 size_of(uint32_t)
				length += 1;  //计算字段OrderType_u的长度 size_of(uint8_t)
				length += 17;  //计算字段StartPage的长度 size_of(uint64_t)
				length += 1;  //计算字段StartPage_u的长度 size_of(uint8_t)
				length += 17;  //计算字段PageNum的长度 size_of(uint64_t)
				length += 1;  //计算字段PageNum_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Ext, encoding);  //计算字段Ext的长度 size_of(Map)
				length += 1;  //计算字段Ext_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
