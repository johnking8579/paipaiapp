//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.wxdLbsStore.WxdLbsStoreAo.java

package com.qq.qqbuy.thirdparty.idl.wxdLbsStore.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import java.util.Map;
import java.util.HashMap;

/**
 *地址级别过滤条件
 *
 *@date 2015-03-18 05:00:00
 *
 *@since version:0
*/
public class LbsStoreFilterByAddress  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 当前位置坐标
	 *
	 * 版本 >= 0
	 */
	 private LbsStoreCoordinate Coordinate = new LbsStoreCoordinate();

	/**
	 * 版本 >= 0
	 */
	 private short Coordinate_u;

	/**
	 * 查询地址级别：0：国，1：省，2：市，3：区，4：街道
	 *
	 * 版本 >= 0
	 */
	 private long AddressLevel;

	/**
	 * 版本 >= 0
	 */
	 private short AddressLevel_u;

	/**
	 * 门店地址（国）
	 *
	 * 版本 >= 0
	 */
	 private String Address1 = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Address1_u;

	/**
	 * 门店地址（省）
	 *
	 * 版本 >= 0
	 */
	 private String Address2 = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Address2_u;

	/**
	 * 门店地址（省id）
	 *
	 * 版本 >= 0
	 */
	 private long Address2Id;

	/**
	 * 版本 >= 0
	 */
	 private short Address2Id_u;

	/**
	 * 门店地址（市）
	 *
	 * 版本 >= 0
	 */
	 private String Address3 = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Address3_u;

	/**
	 * 门店地址（市id）
	 *
	 * 版本 >= 0
	 */
	 private long Address3Id;

	/**
	 * 版本 >= 0
	 */
	 private short Address3Id_u;

	/**
	 * 门店地址（区）
	 *
	 * 版本 >= 0
	 */
	 private String Address4 = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Address4_u;

	/**
	 * 门店地址（区id）
	 *
	 * 版本 >= 0
	 */
	 private long Address4Id;

	/**
	 * 版本 >= 0
	 */
	 private short Address4Id_u;

	/**
	 * 门店地址（街道）
	 *
	 * 版本 >= 0
	 */
	 private String Address5 = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Address5_u;

	/**
	 * 门店地址（街道id）
	 *
	 * 版本 >= 0
	 */
	 private long Address5Id;

	/**
	 * 版本 >= 0
	 */
	 private short Address5Id_u;

	/**
	 * 品类字段id
	 *
	 * 版本 >= 0
	 */
	 private long Category;

	/**
	 * 版本 >= 0
	 */
	 private short Category_u;

	/**
	 * 排序方式:暂不使用
	 *
	 * 版本 >= 0
	 */
	 private long OrderType;

	/**
	 * 版本 >= 0
	 */
	 private short OrderType_u;

	/**
	 * 从第几页记录开始
	 *
	 * 版本 >= 0
	 */
	 private long StartPage;

	/**
	 * 版本 >= 0
	 */
	 private short StartPage_u;

	/**
	 * 每页记录数
	 *
	 * 版本 >= 0
	 */
	 private long PageNum;

	/**
	 * 版本 >= 0
	 */
	 private short PageNum_u;

	/**
	 * 扩展字段
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> Ext = new HashMap<String,String>();

	/**
	 * 版本 >= 0
	 */
	 private short Ext_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushObject(Coordinate);
		bs.pushUByte(Coordinate_u);
		bs.pushUInt(AddressLevel);
		bs.pushUByte(AddressLevel_u);
		bs.pushString(Address1);
		bs.pushUByte(Address1_u);
		bs.pushString(Address2);
		bs.pushUByte(Address2_u);
		bs.pushUInt(Address2Id);
		bs.pushUByte(Address2Id_u);
		bs.pushString(Address3);
		bs.pushUByte(Address3_u);
		bs.pushUInt(Address3Id);
		bs.pushUByte(Address3Id_u);
		bs.pushString(Address4);
		bs.pushUByte(Address4_u);
		bs.pushUInt(Address4Id);
		bs.pushUByte(Address4Id_u);
		bs.pushString(Address5);
		bs.pushUByte(Address5_u);
		bs.pushUInt(Address5Id);
		bs.pushUByte(Address5Id_u);
		bs.pushUInt(Category);
		bs.pushUByte(Category_u);
		bs.pushUInt(OrderType);
		bs.pushUByte(OrderType_u);
		bs.pushLong(StartPage);
		bs.pushUByte(StartPage_u);
		bs.pushLong(PageNum);
		bs.pushUByte(PageNum_u);
		bs.pushObject(Ext);
		bs.pushUByte(Ext_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		Coordinate = (LbsStoreCoordinate) bs.popObject(LbsStoreCoordinate.class);
		Coordinate_u = bs.popUByte();
		AddressLevel = bs.popUInt();
		AddressLevel_u = bs.popUByte();
		Address1 = bs.popString();
		Address1_u = bs.popUByte();
		Address2 = bs.popString();
		Address2_u = bs.popUByte();
		Address2Id = bs.popUInt();
		Address2Id_u = bs.popUByte();
		Address3 = bs.popString();
		Address3_u = bs.popUByte();
		Address3Id = bs.popUInt();
		Address3Id_u = bs.popUByte();
		Address4 = bs.popString();
		Address4_u = bs.popUByte();
		Address4Id = bs.popUInt();
		Address4Id_u = bs.popUByte();
		Address5 = bs.popString();
		Address5_u = bs.popUByte();
		Address5Id = bs.popUInt();
		Address5Id_u = bs.popUByte();
		Category = bs.popUInt();
		Category_u = bs.popUByte();
		OrderType = bs.popUInt();
		OrderType_u = bs.popUByte();
		StartPage = bs.popLong();
		StartPage_u = bs.popUByte();
		PageNum = bs.popLong();
		PageNum_u = bs.popUByte();
		Ext = (Map<String,String>)bs.popMap(String.class,String.class);
		Ext_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取当前位置坐标
	 * 
	 * 此字段的版本 >= 0
	 * @return Coordinate value 类型为:LbsStoreCoordinate
	 * 
	 */
	public LbsStoreCoordinate getCoordinate()
	{
		return Coordinate;
	}


	/**
	 * 设置当前位置坐标
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:LbsStoreCoordinate
	 * 
	 */
	public void setCoordinate(LbsStoreCoordinate value)
	{
		if (value != null) {
				this.Coordinate = value;
				this.Coordinate_u = 1;
		}
	}

	public boolean issetCoordinate()
	{
		return this.Coordinate_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Coordinate_u value 类型为:short
	 * 
	 */
	public short getCoordinate_u()
	{
		return Coordinate_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCoordinate_u(short value)
	{
		this.Coordinate_u = value;
	}


	/**
	 * 获取查询地址级别：0：国，1：省，2：市，3：区，4：街道
	 * 
	 * 此字段的版本 >= 0
	 * @return AddressLevel value 类型为:long
	 * 
	 */
	public long getAddressLevel()
	{
		return AddressLevel;
	}


	/**
	 * 设置查询地址级别：0：国，1：省，2：市，3：区，4：街道
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddressLevel(long value)
	{
		this.AddressLevel = value;
		this.AddressLevel_u = 1;
	}

	public boolean issetAddressLevel()
	{
		return this.AddressLevel_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return AddressLevel_u value 类型为:short
	 * 
	 */
	public short getAddressLevel_u()
	{
		return AddressLevel_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddressLevel_u(short value)
	{
		this.AddressLevel_u = value;
	}


	/**
	 * 获取门店地址（国）
	 * 
	 * 此字段的版本 >= 0
	 * @return Address1 value 类型为:String
	 * 
	 */
	public String getAddress1()
	{
		return Address1;
	}


	/**
	 * 设置门店地址（国）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAddress1(String value)
	{
		this.Address1 = value;
		this.Address1_u = 1;
	}

	public boolean issetAddress1()
	{
		return this.Address1_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address1_u value 类型为:short
	 * 
	 */
	public short getAddress1_u()
	{
		return Address1_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress1_u(short value)
	{
		this.Address1_u = value;
	}


	/**
	 * 获取门店地址（省）
	 * 
	 * 此字段的版本 >= 0
	 * @return Address2 value 类型为:String
	 * 
	 */
	public String getAddress2()
	{
		return Address2;
	}


	/**
	 * 设置门店地址（省）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAddress2(String value)
	{
		this.Address2 = value;
		this.Address2_u = 1;
	}

	public boolean issetAddress2()
	{
		return this.Address2_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address2_u value 类型为:short
	 * 
	 */
	public short getAddress2_u()
	{
		return Address2_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress2_u(short value)
	{
		this.Address2_u = value;
	}


	/**
	 * 获取门店地址（省id）
	 * 
	 * 此字段的版本 >= 0
	 * @return Address2Id value 类型为:long
	 * 
	 */
	public long getAddress2Id()
	{
		return Address2Id;
	}


	/**
	 * 设置门店地址（省id）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddress2Id(long value)
	{
		this.Address2Id = value;
		this.Address2Id_u = 1;
	}

	public boolean issetAddress2Id()
	{
		return this.Address2Id_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address2Id_u value 类型为:short
	 * 
	 */
	public short getAddress2Id_u()
	{
		return Address2Id_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress2Id_u(short value)
	{
		this.Address2Id_u = value;
	}


	/**
	 * 获取门店地址（市）
	 * 
	 * 此字段的版本 >= 0
	 * @return Address3 value 类型为:String
	 * 
	 */
	public String getAddress3()
	{
		return Address3;
	}


	/**
	 * 设置门店地址（市）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAddress3(String value)
	{
		this.Address3 = value;
		this.Address3_u = 1;
	}

	public boolean issetAddress3()
	{
		return this.Address3_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address3_u value 类型为:short
	 * 
	 */
	public short getAddress3_u()
	{
		return Address3_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress3_u(short value)
	{
		this.Address3_u = value;
	}


	/**
	 * 获取门店地址（市id）
	 * 
	 * 此字段的版本 >= 0
	 * @return Address3Id value 类型为:long
	 * 
	 */
	public long getAddress3Id()
	{
		return Address3Id;
	}


	/**
	 * 设置门店地址（市id）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddress3Id(long value)
	{
		this.Address3Id = value;
		this.Address3Id_u = 1;
	}

	public boolean issetAddress3Id()
	{
		return this.Address3Id_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address3Id_u value 类型为:short
	 * 
	 */
	public short getAddress3Id_u()
	{
		return Address3Id_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress3Id_u(short value)
	{
		this.Address3Id_u = value;
	}


	/**
	 * 获取门店地址（区）
	 * 
	 * 此字段的版本 >= 0
	 * @return Address4 value 类型为:String
	 * 
	 */
	public String getAddress4()
	{
		return Address4;
	}


	/**
	 * 设置门店地址（区）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAddress4(String value)
	{
		this.Address4 = value;
		this.Address4_u = 1;
	}

	public boolean issetAddress4()
	{
		return this.Address4_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address4_u value 类型为:short
	 * 
	 */
	public short getAddress4_u()
	{
		return Address4_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress4_u(short value)
	{
		this.Address4_u = value;
	}


	/**
	 * 获取门店地址（区id）
	 * 
	 * 此字段的版本 >= 0
	 * @return Address4Id value 类型为:long
	 * 
	 */
	public long getAddress4Id()
	{
		return Address4Id;
	}


	/**
	 * 设置门店地址（区id）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddress4Id(long value)
	{
		this.Address4Id = value;
		this.Address4Id_u = 1;
	}

	public boolean issetAddress4Id()
	{
		return this.Address4Id_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address4Id_u value 类型为:short
	 * 
	 */
	public short getAddress4Id_u()
	{
		return Address4Id_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress4Id_u(short value)
	{
		this.Address4Id_u = value;
	}


	/**
	 * 获取门店地址（街道）
	 * 
	 * 此字段的版本 >= 0
	 * @return Address5 value 类型为:String
	 * 
	 */
	public String getAddress5()
	{
		return Address5;
	}


	/**
	 * 设置门店地址（街道）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAddress5(String value)
	{
		this.Address5 = value;
		this.Address5_u = 1;
	}

	public boolean issetAddress5()
	{
		return this.Address5_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address5_u value 类型为:short
	 * 
	 */
	public short getAddress5_u()
	{
		return Address5_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress5_u(short value)
	{
		this.Address5_u = value;
	}


	/**
	 * 获取门店地址（街道id）
	 * 
	 * 此字段的版本 >= 0
	 * @return Address5Id value 类型为:long
	 * 
	 */
	public long getAddress5Id()
	{
		return Address5Id;
	}


	/**
	 * 设置门店地址（街道id）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddress5Id(long value)
	{
		this.Address5Id = value;
		this.Address5Id_u = 1;
	}

	public boolean issetAddress5Id()
	{
		return this.Address5Id_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address5Id_u value 类型为:short
	 * 
	 */
	public short getAddress5Id_u()
	{
		return Address5Id_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress5Id_u(short value)
	{
		this.Address5Id_u = value;
	}


	/**
	 * 获取品类字段id
	 * 
	 * 此字段的版本 >= 0
	 * @return Category value 类型为:long
	 * 
	 */
	public long getCategory()
	{
		return Category;
	}


	/**
	 * 设置品类字段id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCategory(long value)
	{
		this.Category = value;
		this.Category_u = 1;
	}

	public boolean issetCategory()
	{
		return this.Category_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Category_u value 类型为:short
	 * 
	 */
	public short getCategory_u()
	{
		return Category_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCategory_u(short value)
	{
		this.Category_u = value;
	}


	/**
	 * 获取排序方式:暂不使用
	 * 
	 * 此字段的版本 >= 0
	 * @return OrderType value 类型为:long
	 * 
	 */
	public long getOrderType()
	{
		return OrderType;
	}


	/**
	 * 设置排序方式:暂不使用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOrderType(long value)
	{
		this.OrderType = value;
		this.OrderType_u = 1;
	}

	public boolean issetOrderType()
	{
		return this.OrderType_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return OrderType_u value 类型为:short
	 * 
	 */
	public short getOrderType_u()
	{
		return OrderType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOrderType_u(short value)
	{
		this.OrderType_u = value;
	}


	/**
	 * 获取从第几页记录开始
	 * 
	 * 此字段的版本 >= 0
	 * @return StartPage value 类型为:long
	 * 
	 */
	public long getStartPage()
	{
		return StartPage;
	}


	/**
	 * 设置从第几页记录开始
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setStartPage(long value)
	{
		this.StartPage = value;
		this.StartPage_u = 1;
	}

	public boolean issetStartPage()
	{
		return this.StartPage_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return StartPage_u value 类型为:short
	 * 
	 */
	public short getStartPage_u()
	{
		return StartPage_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStartPage_u(short value)
	{
		this.StartPage_u = value;
	}


	/**
	 * 获取每页记录数
	 * 
	 * 此字段的版本 >= 0
	 * @return PageNum value 类型为:long
	 * 
	 */
	public long getPageNum()
	{
		return PageNum;
	}


	/**
	 * 设置每页记录数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPageNum(long value)
	{
		this.PageNum = value;
		this.PageNum_u = 1;
	}

	public boolean issetPageNum()
	{
		return this.PageNum_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PageNum_u value 类型为:short
	 * 
	 */
	public short getPageNum_u()
	{
		return PageNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPageNum_u(short value)
	{
		this.PageNum_u = value;
	}


	/**
	 * 获取扩展字段
	 * 
	 * 此字段的版本 >= 0
	 * @return Ext value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getExt()
	{
		return Ext;
	}


	/**
	 * 设置扩展字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setExt(Map<String,String> value)
	{
		if (value != null) {
				this.Ext = value;
				this.Ext_u = 1;
		}
	}

	public boolean issetExt()
	{
		return this.Ext_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Ext_u value 类型为:short
	 * 
	 */
	public short getExt_u()
	{
		return Ext_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setExt_u(short value)
	{
		this.Ext_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(LbsStoreFilterByAddress)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Coordinate, null);  //计算字段Coordinate的长度 size_of(LbsStoreCoordinate)
				length += 1;  //计算字段Coordinate_u的长度 size_of(uint8_t)
				length += 4;  //计算字段AddressLevel的长度 size_of(uint32_t)
				length += 1;  //计算字段AddressLevel_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address1, null);  //计算字段Address1的长度 size_of(String)
				length += 1;  //计算字段Address1_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address2, null);  //计算字段Address2的长度 size_of(String)
				length += 1;  //计算字段Address2_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Address2Id的长度 size_of(uint32_t)
				length += 1;  //计算字段Address2Id_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address3, null);  //计算字段Address3的长度 size_of(String)
				length += 1;  //计算字段Address3_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Address3Id的长度 size_of(uint32_t)
				length += 1;  //计算字段Address3Id_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address4, null);  //计算字段Address4的长度 size_of(String)
				length += 1;  //计算字段Address4_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Address4Id的长度 size_of(uint32_t)
				length += 1;  //计算字段Address4Id_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address5, null);  //计算字段Address5的长度 size_of(String)
				length += 1;  //计算字段Address5_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Address5Id的长度 size_of(uint32_t)
				length += 1;  //计算字段Address5Id_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Category的长度 size_of(uint32_t)
				length += 1;  //计算字段Category_u的长度 size_of(uint8_t)
				length += 4;  //计算字段OrderType的长度 size_of(uint32_t)
				length += 1;  //计算字段OrderType_u的长度 size_of(uint8_t)
				length += 17;  //计算字段StartPage的长度 size_of(uint64_t)
				length += 1;  //计算字段StartPage_u的长度 size_of(uint8_t)
				length += 17;  //计算字段PageNum的长度 size_of(uint64_t)
				length += 1;  //计算字段PageNum_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Ext, null);  //计算字段Ext的长度 size_of(Map)
				length += 1;  //计算字段Ext_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(LbsStoreFilterByAddress)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Coordinate, encoding);  //计算字段Coordinate的长度 size_of(LbsStoreCoordinate)
				length += 1;  //计算字段Coordinate_u的长度 size_of(uint8_t)
				length += 4;  //计算字段AddressLevel的长度 size_of(uint32_t)
				length += 1;  //计算字段AddressLevel_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address1, encoding);  //计算字段Address1的长度 size_of(String)
				length += 1;  //计算字段Address1_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address2, encoding);  //计算字段Address2的长度 size_of(String)
				length += 1;  //计算字段Address2_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Address2Id的长度 size_of(uint32_t)
				length += 1;  //计算字段Address2Id_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address3, encoding);  //计算字段Address3的长度 size_of(String)
				length += 1;  //计算字段Address3_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Address3Id的长度 size_of(uint32_t)
				length += 1;  //计算字段Address3Id_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address4, encoding);  //计算字段Address4的长度 size_of(String)
				length += 1;  //计算字段Address4_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Address4Id的长度 size_of(uint32_t)
				length += 1;  //计算字段Address4Id_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address5, encoding);  //计算字段Address5的长度 size_of(String)
				length += 1;  //计算字段Address5_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Address5Id的长度 size_of(uint32_t)
				length += 1;  //计算字段Address5Id_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Category的长度 size_of(uint32_t)
				length += 1;  //计算字段Category_u的长度 size_of(uint8_t)
				length += 4;  //计算字段OrderType的长度 size_of(uint32_t)
				length += 1;  //计算字段OrderType_u的长度 size_of(uint8_t)
				length += 17;  //计算字段StartPage的长度 size_of(uint64_t)
				length += 1;  //计算字段StartPage_u的长度 size_of(uint8_t)
				length += 17;  //计算字段PageNum的长度 size_of(uint64_t)
				length += 1;  //计算字段PageNum_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Ext, encoding);  //计算字段Ext的长度 size_of(Map)
				length += 1;  //计算字段Ext_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
