

//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang

package com.qq.qqbuy.thirdparty.idl.wxdLbsStore.protocol;


import com.paipai.util.annotation.Protocol;
import com.paipai.netframework.kernal.NetAction;


import com.paipai.lang.GenericWrapper;/**
 *
 *NetAction类
 *
 */
public class WxdLbsStoreAo  extends NetAction
{
	@Override
	 public int getSnowslideThresold(){
		// 这里设置防雪崩时间，也就是超时时间值
		  return 2;
	 }




	@Protocol(cmdid = 0x879e1802L, desc = "修改门店信息", export = true)
	 public UpdateStoreInfoResp UpdateStoreInfo(UpdateStoreInfoReq req){
		UpdateStoreInfoResp resp = new  UpdateStoreInfoResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x879e1807L, desc = "根据QQ号或门店ID查询门店信息", export = true)
	 public QueryStoreInfoByStoreIdResp QueryStoreInfoByStoreId(QueryStoreInfoByStoreIdReq req){
		QueryStoreInfoByStoreIdResp resp = new  QueryStoreInfoByStoreIdResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x879e1805L, desc = "查询附近的门店信息", export = true)
	 public QueryStoreInfoByNearResp QueryStoreInfoByNear(QueryStoreInfoByNearReq req){
		QueryStoreInfoByNearResp resp = new  QueryStoreInfoByNearResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x879e1806L, desc = "查询坐标范围内的门店信息", export = true)
	 public QueryStoreInfoByCoordinateResp QueryStoreInfoByCoordinate(QueryStoreInfoByCoordinateReq req){
		QueryStoreInfoByCoordinateResp resp = new  QueryStoreInfoByCoordinateResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x879e1804L, desc = "根据地址级别查询门店信息", export = true)
	 public QueryStoreInfoByAdressResp QueryStoreInfoByAdress(QueryStoreInfoByAdressReq req){
		QueryStoreInfoByAdressResp resp = new  QueryStoreInfoByAdressResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x879e1803L, desc = "删除门店信息", export = true)
	 public DeleteStoreInfoResp DeleteStoreInfo(DeleteStoreInfoReq req){
		DeleteStoreInfoResp resp = new  DeleteStoreInfoResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x879e1801L, desc = "新增门店信息", export = true)
	 public AddStoreInfoResp AddStoreInfo(AddStoreInfoReq req){
		AddStoreInfoResp resp = new  AddStoreInfoResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }



}
