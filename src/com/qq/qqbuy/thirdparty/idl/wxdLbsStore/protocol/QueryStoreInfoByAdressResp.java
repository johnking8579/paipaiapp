 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.wxdLbsStore.WxdLbsStoreAo.java

package com.qq.qqbuy.thirdparty.idl.wxdLbsStore.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import java.util.Vector;

/**
 *响应
 *
 *@date 2015-03-18 05:00:00
 *
 *@since version:0
*/
public class  QueryStoreInfoByAdressResp extends NetMessage
{
	/**
	 * 满足条件的总数
	 *
	 * 版本 >= 0
	 */
	 private long total;

	/**
	 * 结果
	 *
	 * 版本 >= 0
	 */
	 private Vector<LbsStoreInfoAllPo> storeInfo = new Vector<LbsStoreInfoAllPo>();

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errorMsg = new String();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String outReserve = new String();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushUInt(total);
		bs.pushObject(storeInfo);
		bs.pushString(errorMsg);
		bs.pushString(outReserve);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		total = bs.popUInt();
		storeInfo = (Vector<LbsStoreInfoAllPo>)bs.popVector(LbsStoreInfoAllPo.class);
		errorMsg = bs.popString();
		outReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x879e8804L;
	}


	/**
	 * 获取满足条件的总数
	 * 
	 * 此字段的版本 >= 0
	 * @return total value 类型为:long
	 * 
	 */
	public long getTotal()
	{
		return total;
	}


	/**
	 * 设置满足条件的总数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotal(long value)
	{
		this.total = value;
	}


	/**
	 * 获取结果
	 * 
	 * 此字段的版本 >= 0
	 * @return storeInfo value 类型为:Vector<LbsStoreInfoAllPo>
	 * 
	 */
	public Vector<LbsStoreInfoAllPo> getStoreInfo()
	{
		return storeInfo;
	}


	/**
	 * 设置结果
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<LbsStoreInfoAllPo>
	 * 
	 */
	public void setStoreInfo(Vector<LbsStoreInfoAllPo> value)
	{
		if (value != null) {
				this.storeInfo = value;
		}else{
				this.storeInfo = new Vector<LbsStoreInfoAllPo>();
		}
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errorMsg value 类型为:String
	 * 
	 */
	public String getErrorMsg()
	{
		return errorMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrorMsg(String value)
	{
		this.errorMsg = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return outReserve;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		this.outReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(QueryStoreInfoByAdressResp)
				length += 4;  //计算字段total的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(storeInfo, null);  //计算字段storeInfo的长度 size_of(Vector)
				length += ByteStream.getObjectSize(errorMsg, null);  //计算字段errorMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserve, null);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(QueryStoreInfoByAdressResp)
				length += 4;  //计算字段total的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(storeInfo, encoding);  //计算字段storeInfo的长度 size_of(Vector)
				length += ByteStream.getObjectSize(errorMsg, encoding);  //计算字段errorMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserve, encoding);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
