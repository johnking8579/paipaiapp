 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *添加商品到购物车请求
 *
 *@date 2013-03-08 09:40:21
 *
 *@since version:0
*/
public class  AddCmdy2CartReq implements IServiceObject
{
	/**
	 * 请求源
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 请求ip
	 *
	 * 版本 >= 0
	 */
	 private String ReqIp = new String();

	/**
	 * 请求MachineKey
	 *
	 * 版本 >= 0
	 */
	 private String MachineKey = new String();

	/**
	 * 商品Id
	 *
	 * 版本 >= 0
	 */
	 private String ItemId = new String();

	/**
	 * 库存属性
	 *
	 * 版本 >= 0
	 */
	 private String StockAttr = new String();

	/**
	 * 数量
	 *
	 * 版本 >= 0
	 */
	 private long Num;

	/**
	 * 是否强制加入购物车
	 *
	 * 版本 >= 0
	 */
	 private boolean bAddForce;

	/**
	 * 保留输入字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveIn = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(Source);
		bs.pushString(ReqIp);
		bs.pushString(MachineKey);
		bs.pushString(ItemId);
		bs.pushString(StockAttr);
		bs.pushUInt(Num);
		bs.pushBoolean(bAddForce);
		bs.pushString(ReserveIn);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		Source = bs.popString();
		ReqIp = bs.popString();
		MachineKey = bs.popString();
		ItemId = bs.popString();
		StockAttr = bs.popString();
		Num = bs.popUInt();
		bAddForce = bs.popBoolean();
		ReserveIn = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x91121802L;
	}


	/**
	 * 获取请求源
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置请求源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取请求ip
	 * 
	 * 此字段的版本 >= 0
	 * @return ReqIp value 类型为:String
	 * 
	 */
	public String getReqIp()
	{
		return ReqIp;
	}


	/**
	 * 设置请求ip
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReqIp(String value)
	{
		this.ReqIp = value;
	}


	/**
	 * 获取请求MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @return MachineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return MachineKey;
	}


	/**
	 * 设置请求MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.MachineKey = value;
	}


	/**
	 * 获取商品Id
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemId value 类型为:String
	 * 
	 */
	public String getItemId()
	{
		return ItemId;
	}


	/**
	 * 设置商品Id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemId(String value)
	{
		this.ItemId = value;
	}


	/**
	 * 获取库存属性
	 * 
	 * 此字段的版本 >= 0
	 * @return StockAttr value 类型为:String
	 * 
	 */
	public String getStockAttr()
	{
		return StockAttr;
	}


	/**
	 * 设置库存属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStockAttr(String value)
	{
		this.StockAttr = value;
	}


	/**
	 * 获取数量
	 * 
	 * 此字段的版本 >= 0
	 * @return Num value 类型为:long
	 * 
	 */
	public long getNum()
	{
		return Num;
	}


	/**
	 * 设置数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNum(long value)
	{
		this.Num = value;
	}


	/**
	 * 获取是否强制加入购物车
	 * 
	 * 此字段的版本 >= 0
	 * @return bAddForce value 类型为:boolean
	 * 
	 */
	public boolean getBAddForce()
	{
		return bAddForce;
	}


	/**
	 * 设置是否强制加入购物车
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:boolean
	 * 
	 */
	public void setBAddForce(boolean value)
	{
		this.bAddForce = value;
	}


	/**
	 * 获取保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveIn value 类型为:String
	 * 
	 */
	public String getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveIn(String value)
	{
		this.ReserveIn = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(AddCmdy2CartReq)
				length += ByteStream.getObjectSize(Source);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(ReqIp);  //计算字段ReqIp的长度 size_of(String)
				length += ByteStream.getObjectSize(MachineKey);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(ItemId);  //计算字段ItemId的长度 size_of(String)
				if(StockAttr == null)
                    length += 4;
                else
                    StockAttr += 4+ StockAttr.getBytes("GBK").length;
				length += 4;  //计算字段Num的长度 size_of(uint32_t)
				length += 1;  //计算字段bAddForce的长度 size_of(boolean)
				length += ByteStream.getObjectSize(ReserveIn);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
