//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;
import com.paipai.lang.MultiMap;

/**
 *购物车下单请求
 *
 *@date 2013-03-08 09:40:21
 *
 *@since version:0
*/
public class MakeOrderRequest  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 序列号
	 *
	 * 版本 >= 0
	 */
	 private long SequenceId;

	/**
	 * 版本 >= 0
	 */
	 private short SequenceId_u;

	/**
	 * 验证码
	 *
	 * 版本 >= 0
	 */
	 private String VerifyCode = new String();

	/**
	 * 版本 >= 0
	 */
	 private short VerifyCode_u;

	/**
	 * Refer
	 *
	 * 版本 >= 0
	 */
	 private String Refer = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Refer_u;

	/**
	 * 收获地址
	 *
	 * 版本 >= 0
	 */
	 private ReceiveAddress RecvAddr = new ReceiveAddress();

	/**
	 * 版本 >= 0
	 */
	 private short RecvAddr_u;

	/**
	 * 下单列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<OrderInformation> OrderInfo = new Vector<OrderInformation>();

	/**
	 * 版本 >= 0
	 */
	 private short OrderInfo_u;

	/**
	 * 扩展信息
	 *
	 * 版本 >= 0
	 */
	 private MultiMap<String,String> ExtInfo = new MultiMap<String,String>();

	/**
	 * 版本 >= 0
	 */
	 private short ExtInfo_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushLong(SequenceId);
		bs.pushUByte(SequenceId_u);
		bs.pushString(VerifyCode);
		bs.pushUByte(VerifyCode_u);
		bs.pushString(Refer);
		bs.pushUByte(Refer_u);
		bs.pushObject(RecvAddr);
		bs.pushUByte(RecvAddr_u);
		bs.pushObject(OrderInfo);
		bs.pushUByte(OrderInfo_u);
		bs.pushObject(ExtInfo);
		bs.pushUByte(ExtInfo_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		SequenceId = bs.popLong();
		SequenceId_u = bs.popUByte();
		VerifyCode = bs.popString();
		VerifyCode_u = bs.popUByte();
		Refer = bs.popString();
		Refer_u = bs.popUByte();
		RecvAddr = (ReceiveAddress) bs.popObject(ReceiveAddress.class);
		RecvAddr_u = bs.popUByte();
		OrderInfo = (Vector<OrderInformation>)bs.popVector(OrderInformation.class);
		OrderInfo_u = bs.popUByte();
		ExtInfo = (MultiMap<String,String>)bs.popMultiMap(String.class,String.class);
		ExtInfo_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取序列号
	 * 
	 * 此字段的版本 >= 0
	 * @return SequenceId value 类型为:long
	 * 
	 */
	public long getSequenceId()
	{
		return SequenceId;
	}


	/**
	 * 设置序列号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSequenceId(long value)
	{
		this.SequenceId = value;
		this.SequenceId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SequenceId_u value 类型为:short
	 * 
	 */
	public short getSequenceId_u()
	{
		return SequenceId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSequenceId_u(short value)
	{
		this.SequenceId_u = value;
	}


	/**
	 * 获取验证码
	 * 
	 * 此字段的版本 >= 0
	 * @return VerifyCode value 类型为:String
	 * 
	 */
	public String getVerifyCode()
	{
		return VerifyCode;
	}


	/**
	 * 设置验证码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setVerifyCode(String value)
	{
		this.VerifyCode = value;
		this.VerifyCode_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return VerifyCode_u value 类型为:short
	 * 
	 */
	public short getVerifyCode_u()
	{
		return VerifyCode_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVerifyCode_u(short value)
	{
		this.VerifyCode_u = value;
	}


	/**
	 * 获取Refer
	 * 
	 * 此字段的版本 >= 0
	 * @return Refer value 类型为:String
	 * 
	 */
	public String getRefer()
	{
		return Refer;
	}


	/**
	 * 设置Refer
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRefer(String value)
	{
		this.Refer = value;
		this.Refer_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Refer_u value 类型为:short
	 * 
	 */
	public short getRefer_u()
	{
		return Refer_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRefer_u(short value)
	{
		this.Refer_u = value;
	}


	/**
	 * 获取收获地址
	 * 
	 * 此字段的版本 >= 0
	 * @return RecvAddr value 类型为:ReceiveAddress
	 * 
	 */
	public ReceiveAddress getRecvAddr()
	{
		return RecvAddr;
	}


	/**
	 * 设置收获地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ReceiveAddress
	 * 
	 */
	public void setRecvAddr(ReceiveAddress value)
	{
		if (value != null) {
				this.RecvAddr = value;
				this.RecvAddr_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return RecvAddr_u value 类型为:short
	 * 
	 */
	public short getRecvAddr_u()
	{
		return RecvAddr_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRecvAddr_u(short value)
	{
		this.RecvAddr_u = value;
	}


	/**
	 * 获取下单列表
	 * 
	 * 此字段的版本 >= 0
	 * @return OrderInfo value 类型为:Vector<OrderInformation>
	 * 
	 */
	public Vector<OrderInformation> getOrderInfo()
	{
		return OrderInfo;
	}


	/**
	 * 设置下单列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<OrderInformation>
	 * 
	 */
	public void setOrderInfo(Vector<OrderInformation> value)
	{
		if (value != null) {
				this.OrderInfo = value;
				this.OrderInfo_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return OrderInfo_u value 类型为:short
	 * 
	 */
	public short getOrderInfo_u()
	{
		return OrderInfo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOrderInfo_u(short value)
	{
		this.OrderInfo_u = value;
	}


	/**
	 * 获取扩展信息
	 * 
	 * 此字段的版本 >= 0
	 * @return ExtInfo value 类型为:MultiMap<String,String>
	 * 
	 */
	public MultiMap<String,String> getExtInfo()
	{
		return ExtInfo;
	}


	/**
	 * 设置扩展信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:MultiMap<String,String>
	 * 
	 */
	public void setExtInfo(MultiMap<String,String> value)
	{
		if (value != null) {
				this.ExtInfo = value;
				this.ExtInfo_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ExtInfo_u value 类型为:short
	 * 
	 */
	public short getExtInfo_u()
	{
		return ExtInfo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setExtInfo_u(short value)
	{
		this.ExtInfo_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(MakeOrderRequest)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段SequenceId的长度 size_of(uint64_t)
				length += 1;  //计算字段SequenceId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(VerifyCode);  //计算字段VerifyCode的长度 size_of(String)
				length += 1;  //计算字段VerifyCode_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Refer);  //计算字段Refer的长度 size_of(String)
				length += 1;  //计算字段Refer_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(RecvAddr);  //计算字段RecvAddr的长度 size_of(ReceiveAddress)
				length += 1;  //计算字段RecvAddr_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(OrderInfo);  //计算字段OrderInfo的长度 size_of(Vector)
				length += 1;  //计算字段OrderInfo_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ExtInfo);  //计算字段ExtInfo的长度 size_of(MultiMap)
				length += 1;  //计算字段ExtInfo_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
