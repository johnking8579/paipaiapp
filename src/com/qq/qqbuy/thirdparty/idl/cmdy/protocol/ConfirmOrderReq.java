 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *购物车确认订单请求
 *
 *@date 2013-03-08 09:40:21
 *
 *@since version:0
*/
public class  ConfirmOrderReq implements IServiceObject
{
	/**
	 * 请求源
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 请求ip
	 *
	 * 版本 >= 0
	 */
	 private String ReqIp = new String();

	/**
	 * 请求MachineKey
	 *
	 * 版本 >= 0
	 */
	 private String MachineKey = new String();

	/**
	 * 请求分单的商品列表
	 *
	 * 版本 >= 0
	 */
	 private CartItemDataList ReqLst = new CartItemDataList();

	/**
	 * 区域ID
	 *
	 * 版本 >= 0
	 */
	 private long RegionId;

	/**
	 * 保留输入字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveIn = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(Source);
		bs.pushString(ReqIp);
		bs.pushString(MachineKey);
		bs.pushObject(ReqLst);
		bs.pushUInt(RegionId);
		bs.pushString(ReserveIn);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		Source = bs.popString();
		ReqIp = bs.popString();
		MachineKey = bs.popString();
		ReqLst = (CartItemDataList) bs.popObject(CartItemDataList.class);
		RegionId = bs.popUInt();
		ReserveIn = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x91121806L;
	}


	/**
	 * 获取请求源
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置请求源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取请求ip
	 * 
	 * 此字段的版本 >= 0
	 * @return ReqIp value 类型为:String
	 * 
	 */
	public String getReqIp()
	{
		return ReqIp;
	}


	/**
	 * 设置请求ip
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReqIp(String value)
	{
		this.ReqIp = value;
	}


	/**
	 * 获取请求MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @return MachineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return MachineKey;
	}


	/**
	 * 设置请求MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.MachineKey = value;
	}


	/**
	 * 获取请求分单的商品列表
	 * 
	 * 此字段的版本 >= 0
	 * @return ReqLst value 类型为:CartItemDataList
	 * 
	 */
	public CartItemDataList getReqLst()
	{
		return ReqLst;
	}


	/**
	 * 设置请求分单的商品列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:CartItemDataList
	 * 
	 */
	public void setReqLst(CartItemDataList value)
	{
		if (value != null) {
				this.ReqLst = value;
		}else{
				this.ReqLst = new CartItemDataList();
		}
	}


	/**
	 * 获取区域ID
	 * 
	 * 此字段的版本 >= 0
	 * @return RegionId value 类型为:long
	 * 
	 */
	public long getRegionId()
	{
		return RegionId;
	}


	/**
	 * 设置区域ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRegionId(long value)
	{
		this.RegionId = value;
	}


	/**
	 * 获取保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveIn value 类型为:String
	 * 
	 */
	public String getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveIn(String value)
	{
		this.ReserveIn = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(ConfirmOrderReq)
				length += ByteStream.getObjectSize(Source);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(ReqIp);  //计算字段ReqIp的长度 size_of(String)
				length += ByteStream.getObjectSize(MachineKey);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(ReqLst);  //计算字段ReqLst的长度 size_of(CartItemDataList)
				length += 4;  //计算字段RegionId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ReserveIn);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
