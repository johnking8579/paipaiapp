//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import com.paipai.lang.uint64_t;
import java.util.Vector;

/**
 *交易信息
 *
 *@date 2013-03-08 09:40:21
 *
 *@since version:0
*/
public class DealInformation  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	private long Version = 4;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 运送方式
	 *
	 * 版本 >= 0
	 */
	 private long TransportType;

	/**
	 * 版本 >= 0
	 */
	 private short TransportType_u;

	/**
	 * 促销规则id
	 *
	 * 版本 >= 0
	 */
	 private long PromotionRuleId;

	/**
	 * 版本 >= 0
	 */
	 private short PromotionRuleId_u;

	/**
	 * 发票类型
	 *
	 * 版本 >= 0
	 */
	 private long InvoiceType;

	/**
	 * 版本 >= 0
	 */
	 private short InvoiceType_u;

	/**
	 * 订单类型
	 *
	 * 版本 >= 0
	 */
	 private long DealType;

	/**
	 * 版本 >= 0
	 */
	 private short DealType_u;

	/**
	 * 指定费用
	 *
	 * 版本 >= 0
	 */
	 private long SpecialFee;

	/**
	 * 版本 >= 0
	 */
	 private short SpecialFee_u;

	/**
	 * 指定费用类型
	 *
	 * 版本 >= 0
	 */
	 private long SpecialFeeType;

	/**
	 * 版本 >= 0
	 */
	 private short SpecialFeeType_u;

	/**
	 * 套餐id
	 *
	 * 版本 >= 0
	 */
	 private String ComboId = new String();

	/**
	 * 版本 >= 0
	 */
	 private short ComboId_u;

	/**
	 * 订单留言
	 *
	 * 版本 >= 0
	 */
	 private String DealNote = new String();

	/**
	 * 版本 >= 0
	 */
	 private short DealNote_u;

	/**
	 * 发票title
	 *
	 * 版本 >= 0
	 */
	 private String InvoiceTitle = new String();

	/**
	 * 版本 >= 0
	 */
	 private short InvoiceTitle_u;

	/**
	 * 优惠券id列表，目前只支持一张优惠券
	 *
	 * 版本 >= 1
	 */
	 private Vector<uint64_t> CouponId = new Vector<uint64_t>();

	/**
	 * 优惠券id列表标识
	 *
	 * 版本 >= 1
	 */
	 private short CouponId_u;

	/**
	 * 包邮卡标识
	 *
	 * 版本 >= 3
	 */
	 private long FreeMailCardId;

	/**
	 * 包邮卡标识
	 *
	 * 版本 >= 3
	 */
	 private short FreeMailCardId_u;

	/**
	 * 代金券标识
	 *
	 * 版本 >= 3
	 */
	 private long onlineShoppingCoupons;

	/**
	 * 代金券标识
	 *
	 * 版本 >= 3
	 */
	 private short onlineShoppingCoupons_u;

	/**
	 * 红包积分
	 *
	 * 版本 >= 4
	 */
	private long RedBagScore;

	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(TransportType);
		bs.pushUByte(TransportType_u);
		bs.pushUInt(PromotionRuleId);
		bs.pushUByte(PromotionRuleId_u);
		bs.pushUInt(InvoiceType);
		bs.pushUByte(InvoiceType_u);
		bs.pushUInt(DealType);
		bs.pushUByte(DealType_u);
		bs.pushUInt(SpecialFee);
		bs.pushUByte(SpecialFee_u);
		bs.pushUInt(SpecialFeeType);
		bs.pushUByte(SpecialFeeType_u);
		bs.pushString(ComboId);
		bs.pushUByte(ComboId_u);
		bs.pushString(DealNote);
		bs.pushUByte(DealNote_u);
		bs.pushString(InvoiceTitle);
		bs.pushUByte(InvoiceTitle_u);
		if(  this.Version >= 1 ){
				bs.pushObject(CouponId);
		}
		if(  this.Version >= 1 ){
				bs.pushUByte(CouponId_u);
		}

		if(  this.Version >= 3 ){
			bs.pushLong(FreeMailCardId);
			bs.pushUByte(FreeMailCardId_u);
			bs.pushLong(onlineShoppingCoupons);
			bs.pushUByte(onlineShoppingCoupons_u);
		}
		if(  this.Version >= 4 ){
			bs.pushUInt(RedBagScore);
		}
		return bs.getWrittenLength();
	}

	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		TransportType = bs.popUInt();
		TransportType_u = bs.popUByte();
		PromotionRuleId = bs.popUInt();
		PromotionRuleId_u = bs.popUByte();
		InvoiceType = bs.popUInt();
		InvoiceType_u = bs.popUByte();
		DealType = bs.popUInt();
		DealType_u = bs.popUByte();
		SpecialFee = bs.popUInt();
		SpecialFee_u = bs.popUByte();
		SpecialFeeType = bs.popUInt();
		SpecialFeeType_u = bs.popUByte();
		ComboId = bs.popString();
		ComboId_u = bs.popUByte();
		DealNote = bs.popString();
		DealNote_u = bs.popUByte();
		InvoiceTitle = bs.popString();
		InvoiceTitle_u = bs.popUByte();
		if(  this.Version >= 1 ){
				CouponId = (Vector<uint64_t>)bs.popVector(uint64_t.class);
		}
		if(  this.Version >= 1 ){
				CouponId_u = bs.popUByte();
		}

		if(  this.Version >= 3 ){
			FreeMailCardId = bs.popLong();
			FreeMailCardId_u = bs.popUByte();
			onlineShoppingCoupons = bs.popLong();
			onlineShoppingCoupons_u = bs.popUByte();
		}
		if(  this.Version >= 4 ){
			RedBagScore = bs.popUInt();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	}


	/**
	 * 获取版本号
	 *
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 *
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 *
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 *
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	/**
	 * 获取
	 *
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 *
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 *
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 *
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取运送方式
	 *
	 * 此字段的版本 >= 0
	 * @return TransportType value 类型为:long
	 *
	 */
	public long getTransportType()
	{
		return TransportType;
	}


	/**
	 * 设置运送方式
	 *
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 *
	 */
	public void setTransportType(long value)
	{
		this.TransportType = value;
		this.TransportType_u = 1;
	}


	/**
	 * 获取
	 *
	 * 此字段的版本 >= 0
	 * @return TransportType_u value 类型为:short
	 *
	 */
	public short getTransportType_u()
	{
		return TransportType_u;
	}


	/**
	 * 设置
	 *
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 *
	 */
	public void setTransportType_u(short value)
	{
		this.TransportType_u = value;
	}


	/**
	 * 获取促销规则id
	 *
	 * 此字段的版本 >= 0
	 * @return PromotionRuleId value 类型为:long
	 *
	 */
	public long getPromotionRuleId()
	{
		return PromotionRuleId;
	}


	/**
	 * 设置促销规则id
	 *
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 *
	 */
	public void setPromotionRuleId(long value)
	{
		this.PromotionRuleId = value;
		this.PromotionRuleId_u = 1;
	}


	/**
	 * 获取
	 *
	 * 此字段的版本 >= 0
	 * @return PromotionRuleId_u value 类型为:short
	 *
	 */
	public short getPromotionRuleId_u()
	{
		return PromotionRuleId_u;
	}


	/**
	 * 设置
	 *
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 *
	 */
	public void setPromotionRuleId_u(short value)
	{
		this.PromotionRuleId_u = value;
	}


	/**
	 * 获取发票类型
	 *
	 * 此字段的版本 >= 0
	 * @return InvoiceType value 类型为:long
	 *
	 */
	public long getInvoiceType()
	{
		return InvoiceType;
	}


	/**
	 * 设置发票类型
	 *
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 *
	 */
	public void setInvoiceType(long value)
	{
		this.InvoiceType = value;
		this.InvoiceType_u = 1;
	}


	/**
	 * 获取
	 *
	 * 此字段的版本 >= 0
	 * @return InvoiceType_u value 类型为:short
	 *
	 */
	public short getInvoiceType_u()
	{
		return InvoiceType_u;
	}


	/**
	 * 设置
	 *
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 *
	 */
	public void setInvoiceType_u(short value)
	{
		this.InvoiceType_u = value;
	}


	/**
	 * 获取订单类型
	 *
	 * 此字段的版本 >= 0
	 * @return DealType value 类型为:long
	 *
	 */
	public long getDealType()
	{
		return DealType;
	}


	/**
	 * 设置订单类型
	 *
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 *
	 */
	public void setDealType(long value)
	{
		this.DealType = value;
		this.DealType_u = 1;
	}


	/**
	 * 获取
	 *
	 * 此字段的版本 >= 0
	 * @return DealType_u value 类型为:short
	 *
	 */
	public short getDealType_u()
	{
		return DealType_u;
	}


	/**
	 * 设置
	 *
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 *
	 */
	public void setDealType_u(short value)
	{
		this.DealType_u = value;
	}


	/**
	 * 获取指定费用
	 *
	 * 此字段的版本 >= 0
	 * @return SpecialFee value 类型为:long
	 *
	 */
	public long getSpecialFee()
	{
		return SpecialFee;
	}


	/**
	 * 设置指定费用
	 *
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 *
	 */
	public void setSpecialFee(long value)
	{
		this.SpecialFee = value;
		this.SpecialFee_u = 1;
	}


	/**
	 * 获取
	 *
	 * 此字段的版本 >= 0
	 * @return SpecialFee_u value 类型为:short
	 *
	 */
	public short getSpecialFee_u()
	{
		return SpecialFee_u;
	}


	/**
	 * 设置
	 *
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 *
	 */
	public void setSpecialFee_u(short value)
	{
		this.SpecialFee_u = value;
	}


	/**
	 * 获取指定费用类型
	 *
	 * 此字段的版本 >= 0
	 * @return SpecialFeeType value 类型为:long
	 *
	 */
	public long getSpecialFeeType()
	{
		return SpecialFeeType;
	}


	/**
	 * 设置指定费用类型
	 *
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 *
	 */
	public void setSpecialFeeType(long value)
	{
		this.SpecialFeeType = value;
		this.SpecialFeeType_u = 1;
	}


	/**
	 * 获取
	 *
	 * 此字段的版本 >= 0
	 * @return SpecialFeeType_u value 类型为:short
	 *
	 */
	public short getSpecialFeeType_u()
	{
		return SpecialFeeType_u;
	}


	/**
	 * 设置
	 *
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 *
	 */
	public void setSpecialFeeType_u(short value)
	{
		this.SpecialFeeType_u = value;
	}


	/**
	 * 获取套餐id
	 *
	 * 此字段的版本 >= 0
	 * @return ComboId value 类型为:String
	 *
	 */
	public String getComboId()
	{
		return ComboId;
	}


	/**
	 * 设置套餐id
	 *
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 *
	 */
	public void setComboId(String value)
	{
		this.ComboId = value;
		this.ComboId_u = 1;
	}


	/**
	 * 获取
	 *
	 * 此字段的版本 >= 0
	 * @return ComboId_u value 类型为:short
	 *
	 */
	public short getComboId_u()
	{
		return ComboId_u;
	}


	/**
	 * 设置
	 *
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 *
	 */
	public void setComboId_u(short value)
	{
		this.ComboId_u = value;
	}


	/**
	 * 获取订单留言
	 *
	 * 此字段的版本 >= 0
	 * @return DealNote value 类型为:String
	 *
	 */
	public String getDealNote()
	{
		return DealNote;
	}


	/**
	 * 设置订单留言
	 *
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 *
	 */
	public void setDealNote(String value)
	{
		this.DealNote = value;
		this.DealNote_u = 1;
	}


	/**
	 * 获取
	 *
	 * 此字段的版本 >= 0
	 * @return DealNote_u value 类型为:short
	 *
	 */
	public short getDealNote_u()
	{
		return DealNote_u;
	}


	/**
	 * 设置
	 *
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 *
	 */
	public void setDealNote_u(short value)
	{
		this.DealNote_u = value;
	}


	/**
	 * 获取发票title
	 *
	 * 此字段的版本 >= 0
	 * @return InvoiceTitle value 类型为:String
	 *
	 */
	public String getInvoiceTitle()
	{
		return InvoiceTitle;
	}


	/**
	 * 设置发票title
	 *
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 *
	 */
	public void setInvoiceTitle(String value)
	{
		this.InvoiceTitle = value;
		this.InvoiceTitle_u = 1;
	}


	/**
	 * 获取
	 *
	 * 此字段的版本 >= 0
	 * @return InvoiceTitle_u value 类型为:short
	 *
	 */
	public short getInvoiceTitle_u()
	{
		return InvoiceTitle_u;
	}


	/**
	 * 设置
	 *
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 *
	 */
	public void setInvoiceTitle_u(short value)
	{
		this.InvoiceTitle_u = value;
	}


	/**
	 * 获取优惠券id列表，目前只支持一张优惠券
	 *
	 * 此字段的版本 >= 1
	 * @return CouponId value 类型为:Vector<uint64_t>
	 *
	 */
	public Vector<uint64_t> getCouponId()
	{
		return CouponId;
	}


	/**
	 * 设置优惠券id列表，目前只支持一张优惠券
	 *
	 * 此字段的版本 >= 1
	 * @param  value 类型为:Vector<uint64_t>
	 *
	 */
	public void setCouponId(Vector<uint64_t> value)
	{
		if (value != null) {
				this.CouponId = value;
				this.CouponId_u = 1;
		}
	}


	/**
	 * 获取优惠券id列表标识
	 *
	 * 此字段的版本 >= 1
	 * @return CouponId_u value 类型为:short
	 *
	 */
	public short getCouponId_u()
	{
		return CouponId_u;
	}


	/**
	 * 设置优惠券id列表标识
	 *
	 * 此字段的版本 >= 1
	 * @param  value 类型为:short
	 *
	 */
	public void setCouponId_u(short value)
	{
		this.CouponId_u = value;
	}


	public long getFreeMailCardId() {
		return FreeMailCardId;
	}

	public void setFreeMailCardId(long freeMailCardId) {
		FreeMailCardId = freeMailCardId;
	}

	public short getFreeMailCardId_u() {
		return FreeMailCardId_u;
	}

	public void setFreeMailCardId_u(short freeMailCardIdU) {
		FreeMailCardId_u = freeMailCardIdU;
	}

	public long getOnlineShoppingCoupons() {
		return onlineShoppingCoupons;
	}

	public void setOnlineShoppingCoupons(long onlineShoppingCoupons) {
		this.onlineShoppingCoupons = onlineShoppingCoupons;
	}

	public short getOnlineShoppingCoupons_u() {
		return onlineShoppingCoupons_u;
	}

	public void setOnlineShoppingCoupons_u(short onlineShoppingCouponsU) {
		onlineShoppingCoupons_u = onlineShoppingCouponsU;
	}

	/**
	 * 获取红包积分
	 *
	 * 此字段的版本 >= 4
	 * @return RedBagScore value 类型为:long
	 *
	 */
	public long getRedBagScore()
	{
		return RedBagScore;
	}


	/**
	 * 设置红包积分
	 *
	 * 此字段的版本 >= 4
	 * @param  value 类型为:long
	 *
	 */
	public void setRedBagScore(long value)
	{
		this.RedBagScore = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(DealInformation)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TransportType的长度 size_of(uint32_t)
				length += 1;  //计算字段TransportType_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PromotionRuleId的长度 size_of(uint32_t)
				length += 1;  //计算字段PromotionRuleId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段InvoiceType的长度 size_of(uint32_t)
				length += 1;  //计算字段InvoiceType_u的长度 size_of(uint8_t)
				length += 4;  //计算字段DealType的长度 size_of(uint32_t)
				length += 1;  //计算字段DealType_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SpecialFee的长度 size_of(uint32_t)
				length += 1;  //计算字段SpecialFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SpecialFeeType的长度 size_of(uint32_t)
				length += 1;  //计算字段SpecialFeeType_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ComboId);  //计算字段ComboId的长度 size_of(String)
				length += 1;  //计算字段ComboId_u的长度 size_of(uint8_t)
//				length += ByteStream.getObjectSize(DealNote);  //计算字段DealNote的长度 size_of(String)
				if(DealNote == null)
                    length += 4;
                else
                    length += 4+ DealNote.getBytes("GBK").length;
				length += 1;  //计算字段DealNote_u的长度 size_of(uint8_t)
//				length += ByteStream.getObjectSize(InvoiceTitle);  //计算字段InvoiceTitle的长度 size_of(String)
				if(InvoiceTitle == null)
                    length += 4;
                else
                    length += 4+ InvoiceTitle.getBytes("GBK").length;
				length += 1;  //计算字段InvoiceTitle_u的长度 size_of(uint8_t)
				if(  this.Version >= 1 ){
						length += ByteStream.getObjectSize(CouponId);  //计算字段CouponId的长度 size_of(Vector)
				}
				if(  this.Version >= 1 ){
						length += 1;  //计算字段CouponId_u的长度 size_of(uint8_t)
				}

				if(  this.Version >= 3 ){
					length += 17;  //计算字段FreeMailCardId的长度 size_of(uint64_t)
					length += 1;  //计算字段FreeMailCardId_u的长度 size_of(uint8_t)
					length += 17;  //计算字段onlineShoppingCoupons的长度 size_of(uint64_t)
					length += 1;  //计算字段onlineShoppingCoupons_u的长度 size_of(uint8_t)
				}
			if(  this.Version >= 4 ){
				length += 4;  //计算字段RedBagScore的长度 size_of(uint32_t)
			}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本1所包含的字段*******
 *	long Version;///<版本号
 *	short Version_u;
 *	long TransportType;///<运送方式
 *	short TransportType_u;
 *	long PromotionRuleId;///<促销规则id
 *	short PromotionRuleId_u;
 *	long InvoiceType;///<发票类型
 *	short InvoiceType_u;
 *	long DealType;///<订单类型
 *	short DealType_u;
 *	long SpecialFee;///<指定费用
 *	short SpecialFee_u;
 *	long SpecialFeeType;///<指定费用类型
 *	short SpecialFeeType_u;
 *	String ComboId;///<套餐id
 *	short ComboId_u;
 *	String DealNote;///<订单留言
 *	short DealNote_u;
 *	String InvoiceTitle;///<发票title
 *	short InvoiceTitle_u;
 *	Vector<uint64_t> CouponId;///<优惠券id列表，目前只支持一张优惠券
 *	short CouponId_u;///<优惠券id列表标识
 *  long FreeMailCardId;///
 *	short FreeMailCardId_u;///
 *	long onlineShoppingCoupons;///
 *	short onlineShoppingCoupons_u;///
 *****以上是版本1所包含的字段*******
 *
 *****以下是版本2所包含的字段*******
 *	long Version;///<版本号
 *	short Version_u;
 *	long TransportType;///<运送方式
 *	short TransportType_u;
 *	long PromotionRuleId;///<促销规则id
 *	short PromotionRuleId_u;
 *	long InvoiceType;///<发票类型
 *	short InvoiceType_u;
 *	long DealType;///<订单类型
 *	short DealType_u;
 *	long SpecialFee;///<指定费用
 *	short SpecialFee_u;
 *	long SpecialFeeType;///<指定费用类型
 *	short SpecialFeeType_u;
 *	String ComboId;///<套餐id
 *	short ComboId_u;
 *	String DealNote;///<订单留言
 *	short DealNote_u;
 *	String InvoiceTitle;///<发票title
 *	short InvoiceTitle_u;
 *	Vector<uint64_t> CouponId;///<优惠券id列表，目前只支持一张优惠券
 *	short CouponId_u;///<优惠券id列表标识
 *	long FreeMailCardId;///<包邮卡ID
 *	short FreeMailCardId_u;///<包邮卡ID标识
 *****以上是版本2所包含的字段*******
 *
 *****以下是版本3所包含的字段*******
 *	long Version;///<版本号
 *	short Version_u;
 *	long TransportType;///<运送方式
 *	short TransportType_u;
 *	long PromotionRuleId;///<促销规则id
 *	short PromotionRuleId_u;
 *	long InvoiceType;///<发票类型
 *	short InvoiceType_u;
 *	long DealType;///<订单类型
 *	short DealType_u;
 *	long SpecialFee;///<指定费用
 *	short SpecialFee_u;
 *	long SpecialFeeType;///<指定费用类型
 *	short SpecialFeeType_u;
 *	String ComboId;///<套餐id
 *	short ComboId_u;
 *	String DealNote;///<订单留言
 *	short DealNote_u;
 *	String InvoiceTitle;///<发票title
 *	short InvoiceTitle_u;
 *	Vector<uint64_t> CouponId;///<优惠券id列表，目前只支持一张优惠券
 *	short CouponId_u;///<优惠券id列表标识
 *	long FreeMailCardId;///<包邮卡ID
 *	short FreeMailCardId_u;///<包邮卡ID标识
 *	long onlineShoppingCoupons;///<网购券id
 *	short onlineShoppingCoupons_u;///<网购券id
 *****以上是版本3所包含的字段*******
 *
 *****以下是版本4所包含的字段*******
 *	long Version;///<版本号
 *	short Version_u;
 *	long TransportType;///<运送方式
 *	short TransportType_u;
 *	long PromotionRuleId;///<促销规则id
 *	short PromotionRuleId_u;
 *	long InvoiceType;///<发票类型
 *	short InvoiceType_u;
 *	long DealType;///<订单类型
 *	short DealType_u;
 *	long SpecialFee;///<指定费用
 *	short SpecialFee_u;
 *	long SpecialFeeType;///<指定费用类型
 *	short SpecialFeeType_u;
 *	String ComboId;///<套餐id
 *	short ComboId_u;
 *	String DealNote;///<订单留言
 *	short DealNote_u;
 *	String InvoiceTitle;///<发票title
 *	short InvoiceTitle_u;
 *	Vector<uint64_t> CouponId;///<优惠券id列表，目前只支持一张优惠券
 *	short CouponId_u;///<优惠券id列表标识
 *	long FreeMailCardId;///<包邮卡ID
 *	short FreeMailCardId_u;///<包邮卡ID标识
 *	long onlineShoppingCoupons;///<网购券id
 *	short onlineShoppingCoupons_u;///<网购券id
 *	long RedBagScore;///<红包积分
 *****以上是版本4所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
