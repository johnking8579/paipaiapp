//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *商品列表
 *
 *@date 2013-03-08 09:40:21
 *
 *@since version:0
*/
public class CartItemDataList  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 支付方式
	 *
	 * 版本 >= 0
	 */
	 private long PayType;

	/**
	 * 版本 >= 0
	 */
	 private short PayType_u;

	/**
	 * 商品列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<CartItemData> DataList = new Vector<CartItemData>();

	/**
	 * 版本 >= 0
	 */
	 private short DataList_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(PayType);
		bs.pushUByte(PayType_u);
		bs.pushObject(DataList);
		bs.pushUByte(DataList_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		PayType = bs.popUInt();
		PayType_u = bs.popUByte();
		DataList = (Vector<CartItemData>)bs.popVector(CartItemData.class);
		DataList_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取支付方式
	 * 
	 * 此字段的版本 >= 0
	 * @return PayType value 类型为:long
	 * 
	 */
	public long getPayType()
	{
		return PayType;
	}


	/**
	 * 设置支付方式
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayType(long value)
	{
		this.PayType = value;
		this.PayType_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PayType_u value 类型为:short
	 * 
	 */
	public short getPayType_u()
	{
		return PayType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPayType_u(short value)
	{
		this.PayType_u = value;
	}


	/**
	 * 获取商品列表
	 * 
	 * 此字段的版本 >= 0
	 * @return DataList value 类型为:Vector<CartItemData>
	 * 
	 */
	public Vector<CartItemData> getDataList()
	{
		return DataList;
	}


	/**
	 * 设置商品列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<CartItemData>
	 * 
	 */
	public void setDataList(Vector<CartItemData> value)
	{
		if (value != null) {
				this.DataList = value;
				this.DataList_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DataList_u value 类型为:short
	 * 
	 */
	public short getDataList_u()
	{
		return DataList_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDataList_u(short value)
	{
		this.DataList_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CartItemDataList)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PayType的长度 size_of(uint32_t)
				length += 1;  //计算字段PayType_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(DataList);  //计算字段DataList的长度 size_of(Vector)
				length += 1;  //计算字段DataList_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
