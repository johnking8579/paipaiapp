//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *订单信息
 *
 *@date 2013-03-08 09:40:21
 *
 *@since version:0
*/
public class DealViewData  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 返回结果
	 *
	 * 版本 >= 0
	 */
	 private long Result;

	/**
	 * 版本 >= 0
	 */
	 private short Result_u;

	/**
	 * 促销活动信息
	 *
	 * 版本 >= 0
	 */
	 private PromotionActive Promotionactive = new PromotionActive();

	/**
	 * 版本 >= 0
	 */
	 private short Promotionactive_u;

	/**
	 * 店铺信息
	 *
	 * 版本 >= 0
	 */
	 private ShopView Shopview = new ShopView();

	/**
	 * 版本 >= 0
	 */
	 private short Shopview_u;

	/**
	 * 运费信息
	 *
	 * 版本 >= 0
	 */
	 private ShipFeeInfo ShipfeeInfo = new ShipFeeInfo();

	/**
	 * 版本 >= 0
	 */
	 private short ShipfeeInfo_u;

	/**
	 * 商品信息列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<CommodityView> CmdyViewList = new Vector<CommodityView>();

	/**
	 * 版本 >= 0
	 */
	 private short CmdyViewList_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(Result);
		bs.pushUByte(Result_u);
		bs.pushObject(Promotionactive);
		bs.pushUByte(Promotionactive_u);
		bs.pushObject(Shopview);
		bs.pushUByte(Shopview_u);
		bs.pushObject(ShipfeeInfo);
		bs.pushUByte(ShipfeeInfo_u);
		bs.pushObject(CmdyViewList);
		bs.pushUByte(CmdyViewList_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		Result = bs.popUInt();
		Result_u = bs.popUByte();
		Promotionactive = (PromotionActive) bs.popObject(PromotionActive.class);
		Promotionactive_u = bs.popUByte();
		Shopview = (ShopView) bs.popObject(ShopView.class);
		Shopview_u = bs.popUByte();
		ShipfeeInfo = (ShipFeeInfo) bs.popObject(ShipFeeInfo.class);
		ShipfeeInfo_u = bs.popUByte();
		CmdyViewList = (Vector<CommodityView>)bs.popVector(CommodityView.class);
		CmdyViewList_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取返回结果
	 * 
	 * 此字段的版本 >= 0
	 * @return Result value 类型为:long
	 * 
	 */
	public long getResult()
	{
		return Result;
	}


	/**
	 * 设置返回结果
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setResult(long value)
	{
		this.Result = value;
		this.Result_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Result_u value 类型为:short
	 * 
	 */
	public short getResult_u()
	{
		return Result_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setResult_u(short value)
	{
		this.Result_u = value;
	}


	/**
	 * 获取促销活动信息
	 * 
	 * 此字段的版本 >= 0
	 * @return Promotionactive value 类型为:PromotionActive
	 * 
	 */
	public PromotionActive getPromotionactive()
	{
		return Promotionactive;
	}


	/**
	 * 设置促销活动信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:PromotionActive
	 * 
	 */
	public void setPromotionactive(PromotionActive value)
	{
		if (value != null) {
				this.Promotionactive = value;
				this.Promotionactive_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Promotionactive_u value 类型为:short
	 * 
	 */
	public short getPromotionactive_u()
	{
		return Promotionactive_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPromotionactive_u(short value)
	{
		this.Promotionactive_u = value;
	}


	/**
	 * 获取店铺信息
	 * 
	 * 此字段的版本 >= 0
	 * @return Shopview value 类型为:ShopView
	 * 
	 */
	public ShopView getShopview()
	{
		return Shopview;
	}


	/**
	 * 设置店铺信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ShopView
	 * 
	 */
	public void setShopview(ShopView value)
	{
		if (value != null) {
				this.Shopview = value;
				this.Shopview_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Shopview_u value 类型为:short
	 * 
	 */
	public short getShopview_u()
	{
		return Shopview_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopview_u(short value)
	{
		this.Shopview_u = value;
	}


	/**
	 * 获取运费信息
	 * 
	 * 此字段的版本 >= 0
	 * @return ShipfeeInfo value 类型为:ShipFeeInfo
	 * 
	 */
	public ShipFeeInfo getShipfeeInfo()
	{
		return ShipfeeInfo;
	}


	/**
	 * 设置运费信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ShipFeeInfo
	 * 
	 */
	public void setShipfeeInfo(ShipFeeInfo value)
	{
		if (value != null) {
				this.ShipfeeInfo = value;
				this.ShipfeeInfo_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ShipfeeInfo_u value 类型为:short
	 * 
	 */
	public short getShipfeeInfo_u()
	{
		return ShipfeeInfo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShipfeeInfo_u(short value)
	{
		this.ShipfeeInfo_u = value;
	}


	/**
	 * 获取商品信息列表
	 * 
	 * 此字段的版本 >= 0
	 * @return CmdyViewList value 类型为:Vector<CommodityView>
	 * 
	 */
	public Vector<CommodityView> getCmdyViewList()
	{
		return CmdyViewList;
	}


	/**
	 * 设置商品信息列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<CommodityView>
	 * 
	 */
	public void setCmdyViewList(Vector<CommodityView> value)
	{
		if (value != null) {
				this.CmdyViewList = value;
				this.CmdyViewList_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return CmdyViewList_u value 类型为:short
	 * 
	 */
	public short getCmdyViewList_u()
	{
		return CmdyViewList_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCmdyViewList_u(short value)
	{
		this.CmdyViewList_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(DealViewData)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Result的长度 size_of(uint32_t)
				length += 1;  //计算字段Result_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Promotionactive);  //计算字段Promotionactive的长度 size_of(PromotionActive)
				length += 1;  //计算字段Promotionactive_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Shopview);  //计算字段Shopview的长度 size_of(ShopView)
				length += 1;  //计算字段Shopview_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ShipfeeInfo);  //计算字段ShipfeeInfo的长度 size_of(ShipFeeInfo)
				length += 1;  //计算字段ShipfeeInfo_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(CmdyViewList);  //计算字段CmdyViewList的长度 size_of(Vector)
				length += 1;  //计算字段CmdyViewList_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
