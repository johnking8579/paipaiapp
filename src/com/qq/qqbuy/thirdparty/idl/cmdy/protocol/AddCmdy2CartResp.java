 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *添加商品到购物车返回
 *
 *@date 2013-03-08 09:40:21
 *
 *@since version:0
*/
public class  AddCmdy2CartResp implements IServiceObject
{
	public long result;
	/**
	 * 购物车中商品总数
	 *
	 * 版本 >= 0
	 */
	 private long TotalNum;

	/**
	 * 购物车中商品总价格
	 *
	 * 版本 >= 0
	 */
	 private long TotalFee;

	/**
	 * 保留输出字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveOut = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushUInt(TotalNum);
		bs.pushUInt(TotalFee);
		bs.pushString(ReserveOut);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		TotalNum = bs.popUInt();
		TotalFee = bs.popUInt();
		ReserveOut = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x91128802L;
	}


	/**
	 * 获取购物车中商品总数
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalNum value 类型为:long
	 * 
	 */
	public long getTotalNum()
	{
		return TotalNum;
	}


	/**
	 * 设置购物车中商品总数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalNum(long value)
	{
		this.TotalNum = value;
	}


	/**
	 * 获取购物车中商品总价格
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalFee value 类型为:long
	 * 
	 */
	public long getTotalFee()
	{
		return TotalFee;
	}


	/**
	 * 设置购物车中商品总价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalFee(long value)
	{
		this.TotalFee = value;
	}


	/**
	 * 获取保留输出字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveOut value 类型为:String
	 * 
	 */
	public String getReserveOut()
	{
		return ReserveOut;
	}


	/**
	 * 设置保留输出字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveOut(String value)
	{
		this.ReserveOut = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(AddCmdy2CartResp)
				length += 4;  //计算字段TotalNum的长度 size_of(uint32_t)
				length += 4;  //计算字段TotalFee的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ReserveOut);  //计算字段ReserveOut的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
