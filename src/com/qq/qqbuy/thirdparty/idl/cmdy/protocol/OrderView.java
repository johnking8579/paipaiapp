//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *订单展示
 *
 *@date 2013-03-08 09:40:21
 *
 *@since version:0
*/
public class OrderView  implements ICanSerializeObject
{
	
	/**
	 * 支付方式
	 */
	private int payType;
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 订单状态
	 *
	 * 版本 >= 0
	 */
	 private long OrderResult;

	/**
	 * 版本 >= 0
	 */
	 private short OrderResult_u;

	/**
	 * 订单总费用
	 *
	 * 版本 >= 0
	 */
	 private long TotalFee;

	/**
	 * 版本 >= 0
	 */
	 private short TotalFee_u;

	/**
	 * 订单id
	 *
	 * 版本 >= 0
	 */
	 private String DealId = new String();

	/**
	 * 版本 >= 0
	 */
	 private short DealId_u;

	/**
	 * 店铺展示
	 *
	 * 版本 >= 0
	 */
	 private ShopView ShopViewInfo = new ShopView();

	/**
	 * 版本 >= 0
	 */
	 private short ShopViewInfo_u;

	/**
	 * 商品展示
	 *
	 * 版本 >= 0
	 */
	 private Vector<CommodityView> CommodityViewInfo = new Vector<CommodityView>();

	/**
	 * 版本 >= 0
	 */
	 private short CommodityViewInfo_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(OrderResult);
		bs.pushUByte(OrderResult_u);
		bs.pushUInt(TotalFee);
		bs.pushUByte(TotalFee_u);
		bs.pushString(DealId);
		bs.pushUByte(DealId_u);
		bs.pushObject(ShopViewInfo);
		bs.pushUByte(ShopViewInfo_u);
		bs.pushObject(CommodityViewInfo);
		bs.pushUByte(CommodityViewInfo_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		OrderResult = bs.popUInt();
		OrderResult_u = bs.popUByte();
		TotalFee = bs.popUInt();
		TotalFee_u = bs.popUByte();
		DealId = bs.popString();
		DealId_u = bs.popUByte();
		ShopViewInfo = (ShopView) bs.popObject(ShopView.class);
		ShopViewInfo_u = bs.popUByte();
		CommodityViewInfo = (Vector<CommodityView>)bs.popVector(CommodityView.class);
		CommodityViewInfo_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取订单状态
	 * 
	 * 此字段的版本 >= 0
	 * @return OrderResult value 类型为:long
	 * 
	 */
	public long getOrderResult()
	{
		return OrderResult;
	}


	/**
	 * 设置订单状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOrderResult(long value)
	{
		this.OrderResult = value;
		this.OrderResult_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return OrderResult_u value 类型为:short
	 * 
	 */
	public short getOrderResult_u()
	{
		return OrderResult_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOrderResult_u(short value)
	{
		this.OrderResult_u = value;
	}


	/**
	 * 获取订单总费用
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalFee value 类型为:long
	 * 
	 */
	public long getTotalFee()
	{
		return TotalFee;
	}


	/**
	 * 设置订单总费用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalFee(long value)
	{
		this.TotalFee = value;
		this.TotalFee_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalFee_u value 类型为:short
	 * 
	 */
	public short getTotalFee_u()
	{
		return TotalFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTotalFee_u(short value)
	{
		this.TotalFee_u = value;
	}


	/**
	 * 获取订单id
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:String
	 * 
	 */
	public String getDealId()
	{
		return DealId;
	}


	/**
	 * 设置订单id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealId(String value)
	{
		this.DealId = value;
		this.DealId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId_u value 类型为:short
	 * 
	 */
	public short getDealId_u()
	{
		return DealId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealId_u(short value)
	{
		this.DealId_u = value;
	}


	/**
	 * 获取店铺展示
	 * 
	 * 此字段的版本 >= 0
	 * @return ShopViewInfo value 类型为:ShopView
	 * 
	 */
	public ShopView getShopViewInfo()
	{
		return ShopViewInfo;
	}


	/**
	 * 设置店铺展示
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ShopView
	 * 
	 */
	public void setShopViewInfo(ShopView value)
	{
		if (value != null) {
				this.ShopViewInfo = value;
				this.ShopViewInfo_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ShopViewInfo_u value 类型为:short
	 * 
	 */
	public short getShopViewInfo_u()
	{
		return ShopViewInfo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopViewInfo_u(short value)
	{
		this.ShopViewInfo_u = value;
	}


	/**
	 * 获取商品展示
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityViewInfo value 类型为:Vector<CommodityView>
	 * 
	 */
	public Vector<CommodityView> getCommodityViewInfo()
	{
		return CommodityViewInfo;
	}


	/**
	 * 设置商品展示
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<CommodityView>
	 * 
	 */
	public void setCommodityViewInfo(Vector<CommodityView> value)
	{
		if (value != null) {
				this.CommodityViewInfo = value;
				this.CommodityViewInfo_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityViewInfo_u value 类型为:short
	 * 
	 */
	public short getCommodityViewInfo_u()
	{
		return CommodityViewInfo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCommodityViewInfo_u(short value)
	{
		this.CommodityViewInfo_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(OrderView)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段OrderResult的长度 size_of(uint32_t)
				length += 1;  //计算字段OrderResult_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TotalFee的长度 size_of(uint32_t)
				length += 1;  //计算字段TotalFee_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(DealId);  //计算字段DealId的长度 size_of(String)
				length += 1;  //计算字段DealId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ShopViewInfo);  //计算字段ShopViewInfo的长度 size_of(ShopView)
				length += 1;  //计算字段ShopViewInfo_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(CommodityViewInfo);  //计算字段CommodityViewInfo的长度 size_of(Vector)
				length += 1;  //计算字段CommodityViewInfo_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	public int getPayType() {
		return payType;
	}

	public void setPayType(int payType) {
		this.payType = payType;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
