//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *购物车商品属性
 *
 *@date 2013-03-08 09:40:21
 *
 *@since version:0
*/
public class ShopCartCmdyAttr  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 商品种类数
	 *
	 * 版本 >= 0
	 */
	 private long TotalKinds;

	/**
	 * 版本 >= 0
	 */
	 private short TotalKinds_u;

	/**
	 * 商品总数量
	 *
	 * 版本 >= 0
	 */
	 private long TotalNum;

	/**
	 * 版本 >= 0
	 */
	 private short TotalNum_u;

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private long ReserveInt;

	/**
	 * 版本 >= 0
	 */
	 private short ReserveInt_u;

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveStr = new String();

	/**
	 * 版本 >= 0
	 */
	 private short ReserveStr_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(TotalKinds);
		bs.pushUByte(TotalKinds_u);
		bs.pushUInt(TotalNum);
		bs.pushUByte(TotalNum_u);
		bs.pushUInt(ReserveInt);
		bs.pushUByte(ReserveInt_u);
		bs.pushString(ReserveStr);
		bs.pushUByte(ReserveStr_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		TotalKinds = bs.popUInt();
		TotalKinds_u = bs.popUByte();
		TotalNum = bs.popUInt();
		TotalNum_u = bs.popUByte();
		ReserveInt = bs.popUInt();
		ReserveInt_u = bs.popUByte();
		ReserveStr = bs.popString();
		ReserveStr_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取商品种类数
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalKinds value 类型为:long
	 * 
	 */
	public long getTotalKinds()
	{
		return TotalKinds;
	}


	/**
	 * 设置商品种类数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalKinds(long value)
	{
		this.TotalKinds = value;
		this.TotalKinds_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalKinds_u value 类型为:short
	 * 
	 */
	public short getTotalKinds_u()
	{
		return TotalKinds_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTotalKinds_u(short value)
	{
		this.TotalKinds_u = value;
	}


	/**
	 * 获取商品总数量
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalNum value 类型为:long
	 * 
	 */
	public long getTotalNum()
	{
		return TotalNum;
	}


	/**
	 * 设置商品总数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalNum(long value)
	{
		this.TotalNum = value;
		this.TotalNum_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalNum_u value 类型为:short
	 * 
	 */
	public short getTotalNum_u()
	{
		return TotalNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTotalNum_u(short value)
	{
		this.TotalNum_u = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveInt value 类型为:long
	 * 
	 */
	public long getReserveInt()
	{
		return ReserveInt;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setReserveInt(long value)
	{
		this.ReserveInt = value;
		this.ReserveInt_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveInt_u value 类型为:short
	 * 
	 */
	public short getReserveInt_u()
	{
		return ReserveInt_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserveInt_u(short value)
	{
		this.ReserveInt_u = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveStr value 类型为:String
	 * 
	 */
	public String getReserveStr()
	{
		return ReserveStr;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveStr(String value)
	{
		this.ReserveStr = value;
		this.ReserveStr_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveStr_u value 类型为:short
	 * 
	 */
	public short getReserveStr_u()
	{
		return ReserveStr_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserveStr_u(short value)
	{
		this.ReserveStr_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ShopCartCmdyAttr)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TotalKinds的长度 size_of(uint32_t)
				length += 1;  //计算字段TotalKinds_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TotalNum的长度 size_of(uint32_t)
				length += 1;  //计算字段TotalNum_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ReserveInt的长度 size_of(uint32_t)
				length += 1;  //计算字段ReserveInt_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ReserveStr);  //计算字段ReserveStr的长度 size_of(String)
				length += 1;  //计算字段ReserveStr_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
