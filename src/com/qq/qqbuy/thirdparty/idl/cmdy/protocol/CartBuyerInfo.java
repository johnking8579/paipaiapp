//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *买家信息
 *
 *@date 2013-03-08 09:40:21
 *
 *@since version:0
*/
public class CartBuyerInfo  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 买家Uin
	 *
	 * 版本 >= 0
	 */
	 private long BuyerUin;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerUin_u;

	/**
	 * 序列号
	 *
	 * 版本 >= 0
	 */
	 private long Seq;

	/**
	 * 版本 >= 0
	 */
	 private short Seq_u;

	/**
	 * 红包列表
	 *
	 * 版本 >= 0
	 */
	 private CartRedPacketPoList RedPacketList = new CartRedPacketPoList();

	/**
	 * 版本 >= 0
	 */
	 private short RedPacketList_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(BuyerUin);
		bs.pushUByte(BuyerUin_u);
		bs.pushUInt(Seq);
		bs.pushUByte(Seq_u);
		bs.pushObject(RedPacketList);
		bs.pushUByte(RedPacketList_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		BuyerUin = bs.popUInt();
		BuyerUin_u = bs.popUByte();
		Seq = bs.popUInt();
		Seq_u = bs.popUByte();
		RedPacketList = (CartRedPacketPoList) bs.popObject(CartRedPacketPoList.class);
		RedPacketList_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取买家Uin
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return BuyerUin;
	}


	/**
	 * 设置买家Uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.BuyerUin = value;
		this.BuyerUin_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerUin_u value 类型为:short
	 * 
	 */
	public short getBuyerUin_u()
	{
		return BuyerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerUin_u(short value)
	{
		this.BuyerUin_u = value;
	}


	/**
	 * 获取序列号
	 * 
	 * 此字段的版本 >= 0
	 * @return Seq value 类型为:long
	 * 
	 */
	public long getSeq()
	{
		return Seq;
	}


	/**
	 * 设置序列号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSeq(long value)
	{
		this.Seq = value;
		this.Seq_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Seq_u value 类型为:short
	 * 
	 */
	public short getSeq_u()
	{
		return Seq_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSeq_u(short value)
	{
		this.Seq_u = value;
	}


	/**
	 * 获取红包列表
	 * 
	 * 此字段的版本 >= 0
	 * @return RedPacketList value 类型为:CartRedPacketPoList
	 * 
	 */
	public CartRedPacketPoList getRedPacketList()
	{
		return RedPacketList;
	}


	/**
	 * 设置红包列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:CartRedPacketPoList
	 * 
	 */
	public void setRedPacketList(CartRedPacketPoList value)
	{
		if (value != null) {
				this.RedPacketList = value;
				this.RedPacketList_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return RedPacketList_u value 类型为:short
	 * 
	 */
	public short getRedPacketList_u()
	{
		return RedPacketList_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRedPacketList_u(short value)
	{
		this.RedPacketList_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CartBuyerInfo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Seq的长度 size_of(uint32_t)
				length += 1;  //计算字段Seq_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(RedPacketList);  //计算字段RedPacketList的长度 size_of(CartRedPacketPoList)
				length += 1;  //计算字段RedPacketList_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
