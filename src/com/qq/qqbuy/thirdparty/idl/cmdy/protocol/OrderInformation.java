//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *下单信息
 *
 *@date 2013-03-08 09:40:21
 *
 *@since version:0
*/
public class OrderInformation  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 交易信息
	 *
	 * 版本 >= 0
	 */
	 private DealInformation DealInfo = new DealInformation();

	/**
	 * 版本 >= 0
	 */
	 private short DealInfo_u;

	/**
	 * 店铺信息
	 *
	 * 版本 >= 0
	 */
	 private ShopInformation ShopInfo = new ShopInformation();

	/**
	 * 版本 >= 0
	 */
	 private short ShopInfo_u;

	/**
	 * 商品信息列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<CommodityInformation> CmdyInfo = new Vector<CommodityInformation>();

	/**
	 * 版本 >= 0
	 */
	 private short CmdyInfo_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushObject(DealInfo);
		bs.pushUByte(DealInfo_u);
		bs.pushObject(ShopInfo);
		bs.pushUByte(ShopInfo_u);
		bs.pushObject(CmdyInfo);
		bs.pushUByte(CmdyInfo_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		DealInfo = (DealInformation) bs.popObject(DealInformation.class);
		DealInfo_u = bs.popUByte();
		ShopInfo = (ShopInformation) bs.popObject(ShopInformation.class);
		ShopInfo_u = bs.popUByte();
		CmdyInfo = (Vector<CommodityInformation>)bs.popVector(CommodityInformation.class);
		CmdyInfo_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取交易信息
	 * 
	 * 此字段的版本 >= 0
	 * @return DealInfo value 类型为:DealInformation
	 * 
	 */
	public DealInformation getDealInfo()
	{
		return DealInfo;
	}


	/**
	 * 设置交易信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:DealInformation
	 * 
	 */
	public void setDealInfo(DealInformation value)
	{
		if (value != null) {
				this.DealInfo = value;
				this.DealInfo_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DealInfo_u value 类型为:short
	 * 
	 */
	public short getDealInfo_u()
	{
		return DealInfo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealInfo_u(short value)
	{
		this.DealInfo_u = value;
	}


	/**
	 * 获取店铺信息
	 * 
	 * 此字段的版本 >= 0
	 * @return ShopInfo value 类型为:ShopInformation
	 * 
	 */
	public ShopInformation getShopInfo()
	{
		return ShopInfo;
	}


	/**
	 * 设置店铺信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ShopInformation
	 * 
	 */
	public void setShopInfo(ShopInformation value)
	{
		if (value != null) {
				this.ShopInfo = value;
				this.ShopInfo_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ShopInfo_u value 类型为:short
	 * 
	 */
	public short getShopInfo_u()
	{
		return ShopInfo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopInfo_u(short value)
	{
		this.ShopInfo_u = value;
	}


	/**
	 * 获取商品信息列表
	 * 
	 * 此字段的版本 >= 0
	 * @return CmdyInfo value 类型为:Vector<CommodityInformation>
	 * 
	 */
	public Vector<CommodityInformation> getCmdyInfo()
	{
		return CmdyInfo;
	}


	/**
	 * 设置商品信息列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<CommodityInformation>
	 * 
	 */
	public void setCmdyInfo(Vector<CommodityInformation> value)
	{
		if (value != null) {
				this.CmdyInfo = value;
				this.CmdyInfo_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return CmdyInfo_u value 类型为:short
	 * 
	 */
	public short getCmdyInfo_u()
	{
		return CmdyInfo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCmdyInfo_u(short value)
	{
		this.CmdyInfo_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(OrderInformation)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(DealInfo);  //计算字段DealInfo的长度 size_of(DealInformation)
				length += 1;  //计算字段DealInfo_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ShopInfo);  //计算字段ShopInfo的长度 size_of(ShopInformation)
				length += 1;  //计算字段ShopInfo_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(CmdyInfo);  //计算字段CmdyInfo的长度 size_of(Vector)
				length += 1;  //计算字段CmdyInfo_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
