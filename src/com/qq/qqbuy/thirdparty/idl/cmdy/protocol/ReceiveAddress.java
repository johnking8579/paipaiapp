//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *收货地址
 *
 *@date 2013-03-08 09:40:21
 *
 *@since version:0
*/
public class ReceiveAddress  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 区域id
	 *
	 * 版本 >= 0
	 */
	 private long RegionId;

	/**
	 * 版本 >= 0
	 */
	 private short RegionId_u;

	/**
	 * 地址id
	 *
	 * 版本 >= 0
	 */
	 private long AddressId;

	/**
	 * 版本 >= 0
	 */
	 private short AddressId_u;

	/**
	 * 姓名
	 *
	 * 版本 >= 0
	 */
	 private String Name = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Name_u;

	/**
	 * 手机
	 *
	 * 版本 >= 0
	 */
	 private String Mobile = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Mobile_u;

	/**
	 * 电话
	 *
	 * 版本 >= 0
	 */
	 private String Phone = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Phone_u;

	/**
	 * 邮编
	 *
	 * 版本 >= 0
	 */
	 private String PostCode = new String();

	/**
	 * 版本 >= 0
	 */
	 private short PostCode_u;

	/**
	 * 地址编码
	 *
	 * 版本 >= 0
	 */
	 private String AddressCode = new String();

	/**
	 * 版本 >= 0
	 */
	 private short AddressCode_u;

	/**
	 * 地址
	 *
	 * 版本 >= 0
	 */
	 private String Address = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Address_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(RegionId);
		bs.pushUByte(RegionId_u);
		bs.pushUInt(AddressId);
		bs.pushUByte(AddressId_u);
		bs.pushString(Name);
		bs.pushUByte(Name_u);
		bs.pushString(Mobile);
		bs.pushUByte(Mobile_u);
		bs.pushString(Phone);
		bs.pushUByte(Phone_u);
		bs.pushString(PostCode);
		bs.pushUByte(PostCode_u);
		bs.pushString(AddressCode);
		bs.pushUByte(AddressCode_u);
		bs.pushString(Address);
		bs.pushUByte(Address_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		RegionId = bs.popUInt();
		RegionId_u = bs.popUByte();
		AddressId = bs.popUInt();
		AddressId_u = bs.popUByte();
		Name = bs.popString();
		Name_u = bs.popUByte();
		Mobile = bs.popString();
		Mobile_u = bs.popUByte();
		Phone = bs.popString();
		Phone_u = bs.popUByte();
		PostCode = bs.popString();
		PostCode_u = bs.popUByte();
		AddressCode = bs.popString();
		AddressCode_u = bs.popUByte();
		Address = bs.popString();
		Address_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取区域id
	 * 
	 * 此字段的版本 >= 0
	 * @return RegionId value 类型为:long
	 * 
	 */
	public long getRegionId()
	{
		return RegionId;
	}


	/**
	 * 设置区域id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRegionId(long value)
	{
		this.RegionId = value;
		this.RegionId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return RegionId_u value 类型为:short
	 * 
	 */
	public short getRegionId_u()
	{
		return RegionId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRegionId_u(short value)
	{
		this.RegionId_u = value;
	}


	/**
	 * 获取地址id
	 * 
	 * 此字段的版本 >= 0
	 * @return AddressId value 类型为:long
	 * 
	 */
	public long getAddressId()
	{
		return AddressId;
	}


	/**
	 * 设置地址id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddressId(long value)
	{
		this.AddressId = value;
		this.AddressId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return AddressId_u value 类型为:short
	 * 
	 */
	public short getAddressId_u()
	{
		return AddressId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddressId_u(short value)
	{
		this.AddressId_u = value;
	}


	/**
	 * 获取姓名
	 * 
	 * 此字段的版本 >= 0
	 * @return Name value 类型为:String
	 * 
	 */
	public String getName()
	{
		return Name;
	}


	/**
	 * 设置姓名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setName(String value)
	{
		this.Name = value;
		this.Name_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Name_u value 类型为:short
	 * 
	 */
	public short getName_u()
	{
		return Name_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setName_u(short value)
	{
		this.Name_u = value;
	}


	/**
	 * 获取手机
	 * 
	 * 此字段的版本 >= 0
	 * @return Mobile value 类型为:String
	 * 
	 */
	public String getMobile()
	{
		return Mobile;
	}


	/**
	 * 设置手机
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMobile(String value)
	{
		this.Mobile = value;
		this.Mobile_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Mobile_u value 类型为:short
	 * 
	 */
	public short getMobile_u()
	{
		return Mobile_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMobile_u(short value)
	{
		this.Mobile_u = value;
	}


	/**
	 * 获取电话
	 * 
	 * 此字段的版本 >= 0
	 * @return Phone value 类型为:String
	 * 
	 */
	public String getPhone()
	{
		return Phone;
	}


	/**
	 * 设置电话
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPhone(String value)
	{
		this.Phone = value;
		this.Phone_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Phone_u value 类型为:short
	 * 
	 */
	public short getPhone_u()
	{
		return Phone_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPhone_u(short value)
	{
		this.Phone_u = value;
	}


	/**
	 * 获取邮编
	 * 
	 * 此字段的版本 >= 0
	 * @return PostCode value 类型为:String
	 * 
	 */
	public String getPostCode()
	{
		return PostCode;
	}


	/**
	 * 设置邮编
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPostCode(String value)
	{
		this.PostCode = value;
		this.PostCode_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PostCode_u value 类型为:short
	 * 
	 */
	public short getPostCode_u()
	{
		return PostCode_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPostCode_u(short value)
	{
		this.PostCode_u = value;
	}


	/**
	 * 获取地址编码
	 * 
	 * 此字段的版本 >= 0
	 * @return AddressCode value 类型为:String
	 * 
	 */
	public String getAddressCode()
	{
		return AddressCode;
	}


	/**
	 * 设置地址编码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAddressCode(String value)
	{
		this.AddressCode = value;
		this.AddressCode_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return AddressCode_u value 类型为:short
	 * 
	 */
	public short getAddressCode_u()
	{
		return AddressCode_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddressCode_u(short value)
	{
		this.AddressCode_u = value;
	}


	/**
	 * 获取地址
	 * 
	 * 此字段的版本 >= 0
	 * @return Address value 类型为:String
	 * 
	 */
	public String getAddress()
	{
		return Address;
	}


	/**
	 * 设置地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAddress(String value)
	{
		this.Address = value;
		this.Address_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address_u value 类型为:short
	 * 
	 */
	public short getAddress_u()
	{
		return Address_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress_u(short value)
	{
		this.Address_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ReceiveAddress)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段RegionId的长度 size_of(uint32_t)
				length += 1;  //计算字段RegionId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段AddressId的长度 size_of(uint32_t)
				length += 1;  //计算字段AddressId_u的长度 size_of(uint8_t)
//			    length += ByteStream.getObjectSize(Name);  //计算字段Name的长度 size_of(String)
//                length += 1;  //计算字段Name_u的长度 size_of(uint8_t)
//                length += ByteStream.getObjectSize(Mobile);  //计算字段Mobile的长度 size_of(String)
//                length += 1;  //计算字段Mobile_u的长度 size_of(uint8_t)
//                length += ByteStream.getObjectSize(Phone);  //计算字段Phone的长度 size_of(String)
//                length += 1;  //计算字段Phone_u的长度 size_of(uint8_t)
//                length += ByteStream.getObjectSize(PostCode);  //计算字段PostCode的长度 size_of(String)
//                length += 1;  //计算字段PostCode_u的长度 size_of(uint8_t)
//                length += ByteStream.getObjectSize(AddressCode);  //计算字段AddressCode的长度 size_of(String)
//                length += 1;  //计算字段AddressCode_u的长度 size_of(uint8_t)
//                length += ByteStream.getObjectSize(Address);  //计算字段Address的长度 size_of(String)
//                length += 1;  //计算字段Address_u的长度 size_of(uint8_t)
                
				if(Name == null)
                    length += 4;
                else
                    length += 4+ Name.getBytes("GBK").length;
				length += 1;  //计算字段Name_u的长度 size_of(uint8_t)
			    if(Mobile == null)
                    length += 4;
                else
                    length += 4+ Mobile.getBytes("GBK").length;
				length += 1;  //计算字段Mobile_u的长度 size_of(uint8_t)
				if(Phone == null)
                    length += 4;
                else
                    length += 4+ Phone.getBytes("GBK").length;
				length += 1;  //计算字段Phone_u的长度 size_of(uint8_t)
			    if(PostCode == null)
                    length += 4;
                else
                    length += 4+ PostCode.getBytes("GBK").length;
				length += 1;  //计算字段PostCode_u的长度 size_of(uint8_t)
			    if(AddressCode == null)
                    length += 4;
                else
                    length += 4+ AddressCode.getBytes("GBK").length;
				length += 1;  //计算字段AddressCode_u的长度 size_of(uint8_t)
				if(Address == null)
                    length += 4;
                else
                    length += 4+ Address.getBytes("GBK").length;
				length += 1;  //计算字段Address_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
