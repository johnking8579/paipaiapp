//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *促销活动信息
 *
 *@date 2013-03-08 09:40:21
 *
 *@since version:0
*/
public class PromotionActive  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * Uin
	 *
	 * 版本 >= 0
	 */
	 private long Uin;

	/**
	 * 版本 >= 0
	 */
	 private short Uin_u;

	/**
	 * 促销活动开始时间
	 *
	 * 版本 >= 0
	 */
	 private long StartTime;

	/**
	 * 版本 >= 0
	 */
	 private short StartTime_u;

	/**
	 * 促销活动结束时间
	 *
	 * 版本 >= 0
	 */
	 private long EndTime;

	/**
	 * 版本 >= 0
	 */
	 private short EndTime_u;

	/**
	 * 促销活动描述
	 *
	 * 版本 >= 0
	 */
	 private String Content = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Content_u;

	/**
	 * 商品类目
	 *
	 * 版本 >= 0
	 */
	 private Vector<String> ShopClass = new Vector<String>();

	/**
	 * 版本 >= 0
	 */
	 private short ShopClass_u;

	/**
	 * 活动规则列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<PromotionRule> RuleList = new Vector<PromotionRule>();

	/**
	 * 版本 >= 0
	 */
	 private short RuleList_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(Uin);
		bs.pushUByte(Uin_u);
		bs.pushUInt(StartTime);
		bs.pushUByte(StartTime_u);
		bs.pushUInt(EndTime);
		bs.pushUByte(EndTime_u);
		bs.pushString(Content);
		bs.pushUByte(Content_u);
		bs.pushObject(ShopClass);
		bs.pushUByte(ShopClass_u);
		bs.pushObject(RuleList);
		bs.pushUByte(RuleList_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		Uin = bs.popUInt();
		Uin_u = bs.popUByte();
		StartTime = bs.popUInt();
		StartTime_u = bs.popUByte();
		EndTime = bs.popUInt();
		EndTime_u = bs.popUByte();
		Content = bs.popString();
		Content_u = bs.popUByte();
		ShopClass = (Vector<String>)bs.popVector(String.class);
		ShopClass_u = bs.popUByte();
		RuleList = (Vector<PromotionRule>)bs.popVector(PromotionRule.class);
		RuleList_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取Uin
	 * 
	 * 此字段的版本 >= 0
	 * @return Uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return Uin;
	}


	/**
	 * 设置Uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.Uin = value;
		this.Uin_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Uin_u value 类型为:short
	 * 
	 */
	public short getUin_u()
	{
		return Uin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUin_u(short value)
	{
		this.Uin_u = value;
	}


	/**
	 * 获取促销活动开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @return StartTime value 类型为:long
	 * 
	 */
	public long getStartTime()
	{
		return StartTime;
	}


	/**
	 * 设置促销活动开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setStartTime(long value)
	{
		this.StartTime = value;
		this.StartTime_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return StartTime_u value 类型为:short
	 * 
	 */
	public short getStartTime_u()
	{
		return StartTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStartTime_u(short value)
	{
		this.StartTime_u = value;
	}


	/**
	 * 获取促销活动结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return EndTime value 类型为:long
	 * 
	 */
	public long getEndTime()
	{
		return EndTime;
	}


	/**
	 * 设置促销活动结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEndTime(long value)
	{
		this.EndTime = value;
		this.EndTime_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return EndTime_u value 类型为:short
	 * 
	 */
	public short getEndTime_u()
	{
		return EndTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEndTime_u(short value)
	{
		this.EndTime_u = value;
	}


	/**
	 * 获取促销活动描述
	 * 
	 * 此字段的版本 >= 0
	 * @return Content value 类型为:String
	 * 
	 */
	public String getContent()
	{
		return Content;
	}


	/**
	 * 设置促销活动描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setContent(String value)
	{
		this.Content = value;
		this.Content_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Content_u value 类型为:short
	 * 
	 */
	public short getContent_u()
	{
		return Content_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setContent_u(short value)
	{
		this.Content_u = value;
	}


	/**
	 * 获取商品类目
	 * 
	 * 此字段的版本 >= 0
	 * @return ShopClass value 类型为:Vector<String>
	 * 
	 */
	public Vector<String> getShopClass()
	{
		return ShopClass;
	}


	/**
	 * 设置商品类目
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<String>
	 * 
	 */
	public void setShopClass(Vector<String> value)
	{
		if (value != null) {
				this.ShopClass = value;
				this.ShopClass_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ShopClass_u value 类型为:short
	 * 
	 */
	public short getShopClass_u()
	{
		return ShopClass_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopClass_u(short value)
	{
		this.ShopClass_u = value;
	}


	/**
	 * 获取活动规则列表
	 * 
	 * 此字段的版本 >= 0
	 * @return RuleList value 类型为:Vector<PromotionRule>
	 * 
	 */
	public Vector<PromotionRule> getRuleList()
	{
		return RuleList;
	}


	/**
	 * 设置活动规则列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<PromotionRule>
	 * 
	 */
	public void setRuleList(Vector<PromotionRule> value)
	{
		if (value != null) {
				this.RuleList = value;
				this.RuleList_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return RuleList_u value 类型为:short
	 * 
	 */
	public short getRuleList_u()
	{
		return RuleList_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRuleList_u(short value)
	{
		this.RuleList_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(PromotionActive)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Uin的长度 size_of(uint32_t)
				length += 1;  //计算字段Uin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段StartTime的长度 size_of(uint32_t)
				length += 1;  //计算字段StartTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段EndTime的长度 size_of(uint32_t)
				length += 1;  //计算字段EndTime_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Content);  //计算字段Content的长度 size_of(String)
				length += 1;  //计算字段Content_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ShopClass);  //计算字段ShopClass的长度 size_of(Vector)
				length += 1;  //计算字段ShopClass_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(RuleList);  //计算字段RuleList的长度 size_of(Vector)
				length += 1;  //计算字段RuleList_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
