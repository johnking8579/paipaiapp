//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *运费信息
 *
 *@date 2013-03-08 09:40:21
 *
 *@since version:0
*/
public class ShipFeeInfo  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 谁支付运费
	 *
	 * 版本 >= 0
	 */
	 private long WhoPayShipFee;

	/**
	 * 版本 >= 0
	 */
	 private short WhoPayShipFee_u;

	/**
	 * 平邮运费
	 *
	 * 版本 >= 0
	 */
	 private long NormalShipFee;

	/**
	 * 版本 >= 0
	 */
	 private short NormalShipFee_u;

	/**
	 * 快递运费
	 *
	 * 版本 >= 0
	 */
	 private long ExpressFee;

	/**
	 * 版本 >= 0
	 */
	 private short ExpressFee_u;

	/**
	 * EMS运费
	 *
	 * 版本 >= 0
	 */
	 private long EmsFee;

	/**
	 * 版本 >= 0
	 */
	 private short EmsFee_u;

	/**
	 * 运送类型
	 *
	 * 版本 >= 0
	 */
	 private long ShipMask;

	/**
	 * 版本 >= 0
	 */
	 private short ShipMask_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(WhoPayShipFee);
		bs.pushUByte(WhoPayShipFee_u);
		bs.pushUInt(NormalShipFee);
		bs.pushUByte(NormalShipFee_u);
		bs.pushUInt(ExpressFee);
		bs.pushUByte(ExpressFee_u);
		bs.pushUInt(EmsFee);
		bs.pushUByte(EmsFee_u);
		bs.pushUInt(ShipMask);
		bs.pushUByte(ShipMask_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		WhoPayShipFee = bs.popUInt();
		WhoPayShipFee_u = bs.popUByte();
		NormalShipFee = bs.popUInt();
		NormalShipFee_u = bs.popUByte();
		ExpressFee = bs.popUInt();
		ExpressFee_u = bs.popUByte();
		EmsFee = bs.popUInt();
		EmsFee_u = bs.popUByte();
		ShipMask = bs.popUInt();
		ShipMask_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取谁支付运费
	 * 
	 * 此字段的版本 >= 0
	 * @return WhoPayShipFee value 类型为:long
	 * 
	 */
	public long getWhoPayShipFee()
	{
		return WhoPayShipFee;
	}


	/**
	 * 设置谁支付运费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWhoPayShipFee(long value)
	{
		this.WhoPayShipFee = value;
		this.WhoPayShipFee_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return WhoPayShipFee_u value 类型为:short
	 * 
	 */
	public short getWhoPayShipFee_u()
	{
		return WhoPayShipFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWhoPayShipFee_u(short value)
	{
		this.WhoPayShipFee_u = value;
	}


	/**
	 * 获取平邮运费
	 * 
	 * 此字段的版本 >= 0
	 * @return NormalShipFee value 类型为:long
	 * 
	 */
	public long getNormalShipFee()
	{
		return NormalShipFee;
	}


	/**
	 * 设置平邮运费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNormalShipFee(long value)
	{
		this.NormalShipFee = value;
		this.NormalShipFee_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return NormalShipFee_u value 类型为:short
	 * 
	 */
	public short getNormalShipFee_u()
	{
		return NormalShipFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNormalShipFee_u(short value)
	{
		this.NormalShipFee_u = value;
	}


	/**
	 * 获取快递运费
	 * 
	 * 此字段的版本 >= 0
	 * @return ExpressFee value 类型为:long
	 * 
	 */
	public long getExpressFee()
	{
		return ExpressFee;
	}


	/**
	 * 设置快递运费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setExpressFee(long value)
	{
		this.ExpressFee = value;
		this.ExpressFee_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ExpressFee_u value 类型为:short
	 * 
	 */
	public short getExpressFee_u()
	{
		return ExpressFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setExpressFee_u(short value)
	{
		this.ExpressFee_u = value;
	}


	/**
	 * 获取EMS运费
	 * 
	 * 此字段的版本 >= 0
	 * @return EmsFee value 类型为:long
	 * 
	 */
	public long getEmsFee()
	{
		return EmsFee;
	}


	/**
	 * 设置EMS运费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEmsFee(long value)
	{
		this.EmsFee = value;
		this.EmsFee_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return EmsFee_u value 类型为:short
	 * 
	 */
	public short getEmsFee_u()
	{
		return EmsFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEmsFee_u(short value)
	{
		this.EmsFee_u = value;
	}


	/**
	 * 获取运送类型
	 * 
	 * 此字段的版本 >= 0
	 * @return ShipMask value 类型为:long
	 * 
	 */
	public long getShipMask()
	{
		return ShipMask;
	}


	/**
	 * 设置运送类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setShipMask(long value)
	{
		this.ShipMask = value;
		this.ShipMask_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ShipMask_u value 类型为:short
	 * 
	 */
	public short getShipMask_u()
	{
		return ShipMask_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShipMask_u(short value)
	{
		this.ShipMask_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ShipFeeInfo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段WhoPayShipFee的长度 size_of(uint32_t)
				length += 1;  //计算字段WhoPayShipFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段NormalShipFee的长度 size_of(uint32_t)
				length += 1;  //计算字段NormalShipFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ExpressFee的长度 size_of(uint32_t)
				length += 1;  //计算字段ExpressFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段EmsFee的长度 size_of(uint32_t)
				length += 1;  //计算字段EmsFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ShipMask的长度 size_of(uint32_t)
				length += 1;  //计算字段ShipMask_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
