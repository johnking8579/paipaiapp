//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *店铺展示
 *
 *@date 2013-03-08 09:40:21
 *
 *@since version:0
*/
public class ShopView  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 卖家uin
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * 版本 >= 0
	 */
	 private short SellerUin_u;

	/**
	 * 店铺属性
	 *
	 * 版本 >= 0
	 */
	 private long ShopProperty;

	/**
	 * 版本 >= 0
	 */
	 private short ShopProperty_u;

	/**
	 * 店铺名称
	 *
	 * 版本 >= 0
	 */
	 private String ShopName = new String();

	/**
	 * 版本 >= 0
	 */
	 private short ShopName_u;

	/**
	 * 卖家昵称
	 *
	 * 版本 >= 0
	 */
	 private String SellerNick = new String();

	/**
	 * 版本 >= 0
	 */
	 private short SellerNick_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(SellerUin);
		bs.pushUByte(SellerUin_u);
		bs.pushUInt(ShopProperty);
		bs.pushUByte(ShopProperty_u);
		bs.pushString(ShopName);
		bs.pushUByte(ShopName_u);
		bs.pushString(SellerNick);
		bs.pushUByte(SellerNick_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		SellerUin = bs.popUInt();
		SellerUin_u = bs.popUByte();
		ShopProperty = bs.popUInt();
		ShopProperty_u = bs.popUByte();
		ShopName = bs.popString();
		ShopName_u = bs.popUByte();
		SellerNick = bs.popString();
		SellerNick_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取卖家uin
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置卖家uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
		this.SellerUin_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin_u value 类型为:short
	 * 
	 */
	public short getSellerUin_u()
	{
		return SellerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerUin_u(short value)
	{
		this.SellerUin_u = value;
	}


	/**
	 * 获取店铺属性
	 * 
	 * 此字段的版本 >= 0
	 * @return ShopProperty value 类型为:long
	 * 
	 */
	public long getShopProperty()
	{
		return ShopProperty;
	}


	/**
	 * 设置店铺属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setShopProperty(long value)
	{
		this.ShopProperty = value;
		this.ShopProperty_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ShopProperty_u value 类型为:short
	 * 
	 */
	public short getShopProperty_u()
	{
		return ShopProperty_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopProperty_u(short value)
	{
		this.ShopProperty_u = value;
	}


	/**
	 * 获取店铺名称
	 * 
	 * 此字段的版本 >= 0
	 * @return ShopName value 类型为:String
	 * 
	 */
	public String getShopName()
	{
		return ShopName;
	}


	/**
	 * 设置店铺名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setShopName(String value)
	{
		this.ShopName = value;
		this.ShopName_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ShopName_u value 类型为:short
	 * 
	 */
	public short getShopName_u()
	{
		return ShopName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopName_u(short value)
	{
		this.ShopName_u = value;
	}


	/**
	 * 获取卖家昵称
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerNick value 类型为:String
	 * 
	 */
	public String getSellerNick()
	{
		return SellerNick;
	}


	/**
	 * 设置卖家昵称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSellerNick(String value)
	{
		this.SellerNick = value;
		this.SellerNick_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerNick_u value 类型为:short
	 * 
	 */
	public short getSellerNick_u()
	{
		return SellerNick_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerNick_u(short value)
	{
		this.SellerNick_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ShopView)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ShopProperty的长度 size_of(uint32_t)
				length += 1;  //计算字段ShopProperty_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ShopName);  //计算字段ShopName的长度 size_of(String)
				length += 1;  //计算字段ShopName_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(SellerNick);  //计算字段SellerNick的长度 size_of(String)
				length += 1;  //计算字段SellerNick_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
