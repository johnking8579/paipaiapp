 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *查看购物车数据返回
 *
 *@date 2013-03-08 09:40:21
 *
 *@since version:0
*/
public class  ViewCartResp implements IServiceObject
{
	public long result;
	/**
	 * 正常商品列表
	 *
	 * 版本 >= 0
	 */
	 private DealViewDataList DealViewLst = new DealViewDataList();

	/**
	 * 异常商品列表
	 *
	 * 版本 >= 0
	 */
	 private DealViewDataList ProblemDealViewLst = new DealViewDataList();

	/**
	 * 保留输出字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveOut = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(DealViewLst);
		bs.pushObject(ProblemDealViewLst);
		bs.pushString(ReserveOut);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		DealViewLst = (DealViewDataList) bs.popObject(DealViewDataList.class);
		ProblemDealViewLst = (DealViewDataList) bs.popObject(DealViewDataList.class);
		ReserveOut = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x91128805L;
	}


	/**
	 * 获取正常商品列表
	 * 
	 * 此字段的版本 >= 0
	 * @return DealViewLst value 类型为:DealViewDataList
	 * 
	 */
	public DealViewDataList getDealViewLst()
	{
		return DealViewLst;
	}


	/**
	 * 设置正常商品列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:DealViewDataList
	 * 
	 */
	public void setDealViewLst(DealViewDataList value)
	{
		if (value != null) {
				this.DealViewLst = value;
		}else{
				this.DealViewLst = new DealViewDataList();
		}
	}


	/**
	 * 获取异常商品列表
	 * 
	 * 此字段的版本 >= 0
	 * @return ProblemDealViewLst value 类型为:DealViewDataList
	 * 
	 */
	public DealViewDataList getProblemDealViewLst()
	{
		return ProblemDealViewLst;
	}


	/**
	 * 设置异常商品列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:DealViewDataList
	 * 
	 */
	public void setProblemDealViewLst(DealViewDataList value)
	{
		if (value != null) {
				this.ProblemDealViewLst = value;
		}else{
				this.ProblemDealViewLst = new DealViewDataList();
		}
	}


	/**
	 * 获取保留输出字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveOut value 类型为:String
	 * 
	 */
	public String getReserveOut()
	{
		return ReserveOut;
	}


	/**
	 * 设置保留输出字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveOut(String value)
	{
		this.ReserveOut = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ViewCartResp)
				length += ByteStream.getObjectSize(DealViewLst);  //计算字段DealViewLst的长度 size_of(DealViewDataList)
				length += ByteStream.getObjectSize(ProblemDealViewLst);  //计算字段ProblemDealViewLst的长度 size_of(DealViewDataList)
				length += ByteStream.getObjectSize(ReserveOut);  //计算字段ReserveOut的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
