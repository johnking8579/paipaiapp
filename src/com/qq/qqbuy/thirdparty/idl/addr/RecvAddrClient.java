/**
 * 
 */
package com.qq.qqbuy.thirdparty.idl.addr;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.thirdparty.idl.IDLResult;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiAddRecvaddrListReq;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiAddRecvaddrListResp;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiDeleteRecvaddrListReq;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiDeleteRecvaddrListResp;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiGetRecvaddrListReq;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiGetRecvaddrListResp;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiGetRecvaddrReq;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiGetRecvaddrResp;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiModifyRecvaddrListReq;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiModifyRecvaddrListResp;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.GetRecvAddrList2Req;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.GetRecvAddrList2Resp;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ModifyDefaultRecvAddrReq;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ModifyDefaultRecvAddrResp;

/**
 * @author oriondeng
 * 
 */
public class RecvAddrClient extends SupportIDLBaseClient {

	/**
	 * 查询地址列表
	 * 
	 * @param uin
	 * @return
	 */
	public IDLResult<ApiGetRecvaddrListResp> getRecvaddrList(ApiGetRecvaddrListReq req, long uin, String skey) throws BusinessException {
		long start = System.currentTimeMillis();
		ApiGetRecvaddrListResp resp = new ApiGetRecvaddrListResp();
		
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiSkeySign(uin, skey, CHARSET_GBK);
		int ret = invokePaiPaiIDL(req, resp, stub);
		
		IDLResult<ApiGetRecvaddrListResp> rsp = new IDLResult<ApiGetRecvaddrListResp>(resp, ret, resp.getResult());
		
		return rsp;
	}
	
	/**
	 * 根据id查地址
	 */
	public IDLResult<ApiGetRecvaddrResp> getRecvaddr(ApiGetRecvaddrReq req,long uin,String skey)  throws BusinessException{
		long start = System.currentTimeMillis();
		ApiGetRecvaddrResp resp = new ApiGetRecvaddrResp();
		
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiSkeySign(uin, skey, CHARSET_GBK);
		int ret = invokePaiPaiIDL(req, resp, stub);
		
		IDLResult<ApiGetRecvaddrResp> rsp = new IDLResult<ApiGetRecvaddrResp>(resp, ret, resp.getResult());
		
		return rsp;
	}
	
	/**
	 * 删除地址
	 */
	public IDLResult<ApiDeleteRecvaddrListResp> deleteRecvaddrList(ApiDeleteRecvaddrListReq req,long uin,String skey)  throws BusinessException{
		long start = System.currentTimeMillis();
		ApiDeleteRecvaddrListResp resp = new ApiDeleteRecvaddrListResp();
		
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiSkeySign(uin, skey, CHARSET_GBK);
		int ret = invokePaiPaiIDL(req, resp, stub);
		
		IDLResult<ApiDeleteRecvaddrListResp> rsp = new IDLResult<ApiDeleteRecvaddrListResp>(resp, ret, resp.getResult());
		
		return rsp;
	}
	
	/**
	 * 添加地址
	 */
	
	public IDLResult<ApiAddRecvaddrListResp> addRecvaddrList(ApiAddRecvaddrListReq req,long uin,String skey) throws BusinessException{
		long start = System.currentTimeMillis();
		ApiAddRecvaddrListResp resp = new ApiAddRecvaddrListResp();
		
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiSkeySign(uin, skey, CHARSET_GBK);
		int ret = invokePaiPaiIDL(req, resp, stub);
		
		IDLResult<ApiAddRecvaddrListResp> rsp = new IDLResult<ApiAddRecvaddrListResp>(resp, ret, resp.getResult());
		return rsp;
	}
	
	/**
	 * 修改地址
	 */
	
	public IDLResult<ApiModifyRecvaddrListResp> modifyRecvaddrList(ApiModifyRecvaddrListReq req,long uin,String skey)  throws BusinessException{
		long start = System.currentTimeMillis();
		ApiModifyRecvaddrListResp resp = new ApiModifyRecvaddrListResp();
		
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiSkeySign(uin, skey, CHARSET_GBK);
		int ret = invokePaiPaiIDL(req, resp, stub);
		
		IDLResult<ApiModifyRecvaddrListResp> rsp = new IDLResult<ApiModifyRecvaddrListResp>(resp, ret, resp.getResult());
		return rsp;
	}
		
	
	/**
	 * 查询地址列表V2
	 * 
	 * @param uin
	 * @return
	 */
	public GetRecvAddrList2Resp getRecvaddrListV2(GetRecvAddrList2Req req, long uin, String skey) throws BusinessException {
		GetRecvAddrList2Resp resp = new GetRecvAddrList2Resp();
		
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiSkeySign(uin, skey, CHARSET_GBK);
		int ret = 0;
		try {
			ret = invokePaiPaiIDL(req, resp, stub);
		} catch (Exception e) {
			Log.run.warn("getRecvaddrListV2 error,ret=" + ret, e);
			e.printStackTrace();
			ret = -1;
		}
		return ret == 0 ? resp : null;
	}
	
	/**
	 * 设置默认地址
	 * 
	 * @param uin
	 * @return
	 */
	public ModifyDefaultRecvAddrResp modifyDefaultRecvAddr(ModifyDefaultRecvAddrReq req, long uin, String skey) throws BusinessException {
		ModifyDefaultRecvAddrResp resp = new ModifyDefaultRecvAddrResp();
		
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiSkeySign(uin, skey, CHARSET_GBK);
		int ret = 0;
		try {
			ret = invokePaiPaiIDL(req, resp, stub);
		} catch (Exception e) {
			Log.run.warn("getRecvaddrListV2 error,ret=" + ret, e);
			e.printStackTrace();
			ret = -1;
		}
		return ret == 0 ? resp : null;
	}
	
	public static void main(String[] args) {
		System.out.println(new ApiAddRecvaddrListResp().getClass().getSimpleName());
	}
}
