
/**
* @file ModifyDefaultRecvAddrResp.java
* @brief ModifyDefaultRecvAddrResp java 文件
* 
* @author 
* @version 1.0
* @date 
* @bug  无
* @warning 无
*/

package com.qq.qqbuy.thirdparty.idl.addr.protocol;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

public class ModifyDefaultRecvAddrResp implements IServiceObject {

    private long dwResult;

    private String strOutReserve; ///<
    //SETTER函数段
    public void setstrOutReserve(String  value)
    {
        this.strOutReserve = value;
    }

    //GETTER函数段
    public String getstrOutReserve()
    {
        return this.strOutReserve;
    }


	public int Serialize(ByteStream bs) throws Exception {
	    bs.pushUInt(this.dwResult);
        if(this.strOutReserve == null)
            bs.pushUInt(0);
        else{
            bs.pushUInt(this.strOutReserve.length());
            bs.pushBytes(this.strOutReserve.getBytes(), this.strOutReserve.length());
        }
		return bs.getWrittenLength();
	}

	public int UnSerialize(ByteStream bs) throws Exception {
	    this.dwResult = bs.popUInt();
        this.strOutReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId() {
		return 0x1216880f;
	}
}

