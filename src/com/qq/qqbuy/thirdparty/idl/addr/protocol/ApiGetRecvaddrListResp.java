 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.o2o.thirdparty.idl.recvaddr.RecvaddrApiAo.java

package com.qq.qqbuy.thirdparty.idl.addr.protocol;


import java.util.Vector;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

/**
 *获取收货地址列表返回类
 *
 *@date 2013-05-09 04:22:53
 *
 *@since version:0
*/
public class  ApiGetRecvaddrListResp implements IServiceObject
{
	public long result;
	/**
	 * 收货地址Po列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<ApiRecvaddr> vecApiRecvaddr = new Vector<ApiRecvaddr>();

	/**
	 * 总数
	 *
	 * 版本 >= 0
	 */
	 private long totalCount;

	/**
	 * 错误信息，用于debug
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 返回保留字
	 *
	 * 版本 >= 0
	 */
	 private String outReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(vecApiRecvaddr);
		bs.pushUInt(totalCount);
		bs.pushString(errMsg);
		bs.pushString(outReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		vecApiRecvaddr = (Vector<ApiRecvaddr>)bs.popVector(ApiRecvaddr.class);
		totalCount = bs.popUInt();
		errMsg = bs.popString();
		outReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x22128802L;
	}


	/**
	 * 获取收货地址Po列表
	 * 
	 * 此字段的版本 >= 0
	 * @return vecApiRecvaddr value 类型为:Vector<ApiRecvaddr>
	 * 
	 */
	public Vector<ApiRecvaddr> getVecApiRecvaddr()
	{
		return vecApiRecvaddr;
	}


	/**
	 * 设置收货地址Po列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<ApiRecvaddr>
	 * 
	 */
	public void setVecApiRecvaddr(Vector<ApiRecvaddr> value)
	{
		if (value != null) {
				this.vecApiRecvaddr = value;
		}else{
				this.vecApiRecvaddr = new Vector<ApiRecvaddr>();
		}
	}


	/**
	 * 获取总数
	 * 
	 * 此字段的版本 >= 0
	 * @return totalCount value 类型为:long
	 * 
	 */
	public long getTotalCount()
	{
		return totalCount;
	}


	/**
	 * 设置总数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalCount(long value)
	{
		this.totalCount = value;
	}


	/**
	 * 获取错误信息，用于debug
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误信息，用于debug
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	/**
	 * 获取返回保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return outReserve;
	}


	/**
	 * 设置返回保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		this.outReserve = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ApiGetRecvaddrListResp)
				length += ByteStream.getObjectSize(vecApiRecvaddr, null);  //计算字段vecApiRecvaddr的长度 size_of(Vector)
				length += 4;  //计算字段totalCount的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(errMsg, null);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserve, null);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(ApiGetRecvaddrListResp)
				length += ByteStream.getObjectSize(vecApiRecvaddr, encoding);  //计算字段vecApiRecvaddr的长度 size_of(Vector)
				length += 4;  //计算字段totalCount的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(errMsg, encoding);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserve, encoding);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
