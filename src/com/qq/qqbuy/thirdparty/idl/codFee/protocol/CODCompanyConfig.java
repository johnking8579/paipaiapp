//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.CodConfig.java

package com.qq.qqbuy.thirdparty.idl.codFee.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import java.util.Vector;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * 获取快递公司信息响应参数
 *
 *@date 2013-08-16 06:07:21
 *
 *@since version:0
*/
public class CODCompanyConfig  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 内部ID
	 *
	 * 版本 >= 0
	 */
	 private long innerCompanyId;

	/**
	 * 版本 >= 0
	 */
	 private short innerCompanyId_u;

	/**
	 * 货到付款费率
	 *
	 * 版本 >= 0
	 */
	 private long feeRate;

	/**
	 * 版本 >= 0
	 */
	 private short feeRate_u;

	/**
	 * 最低揽收金额
	 *
	 * 版本 >= 0
	 */
	 private long lowLimitAmount;

	/**
	 * 版本 >= 0
	 */
	 private short lowLimitAmount_u;

	/**
	 * 最高揽收金额
	 *
	 * 版本 >= 0
	 */
	 private long upLimitAmount;

	/**
	 * 版本 >= 0
	 */
	 private short upLimitAmount_u;

	/**
	 * 最低服务费
	 *
	 * 版本 >= 0
	 */
	 private long lowLimitFee;

	/**
	 * 版本 >= 0
	 */
	 private short lowLimitFee_u;

	/**
	 * 最高服务费
	 *
	 * 版本 >= 0
	 */
	 private long upLimitFee;

	/**
	 * 版本 >= 0
	 */
	 private short upLimitFee_u;

	/**
	 * 财付通分成比例
	 *
	 * 版本 >= 0
	 */
	 private long cftDivideRate;

	/**
	 * 版本 >= 0
	 */
	 private short cftDivideRate_u;

	/**
	 * 拍拍分成比例
	 *
	 * 版本 >= 0
	 */
	 private long paipaiDivideRate;

	/**
	 * 版本 >= 0
	 */
	 private short paipaiDivideRate_u;

	/**
	 * 物流公司分成比例
	 *
	 * 版本 >= 0
	 */
	 private long companyDivideRate;

	/**
	 * 版本 >= 0
	 */
	 private short companyDivideRate_u;

	/**
	 * 运送时间下限
	 *
	 * 版本 >= 0
	 */
	 private long lowLimitDay;

	/**
	 * 版本 >= 0
	 */
	 private short lowLimitDay_u;

	/**
	 * 运送时间上限
	 *
	 * 版本 >= 0
	 */
	 private long upLimitDay;

	/**
	 * 版本 >= 0
	 */
	 private short upLimitDay_u;

	/**
	 * cod标识位
	 *
	 * 版本 >= 0
	 */
	 private long flag;

	/**
	 * 版本 >= 0
	 */
	 private short flag_u;

	/**
	 * 垫资模式
	 *
	 * 版本 >= 0
	 */
	 private long prePayMode;

	/**
	 * 版本 >= 0
	 */
	 private short prePayMode_u;

	/**
	 * 物流公司投递费
	 *
	 * 版本 >= 0
	 */
	 private long deliveryFee;

	/**
	 * 版本 >= 0
	 */
	 private short deliveryFee_u;

	/**
	 * 物流系统中公司id
	 *
	 * 版本 >= 0
	 */
	 private long wuliuCompanyId;

	/**
	 * 版本 >= 0
	 */
	 private short wuliuCompanyId_u;

	/**
	 * 拍拍在财付通垫资spid
	 *
	 * 版本 >= 0
	 */
	 private String paipaiSpid = new String();

	/**
	 * 版本 >= 0
	 */
	 private short paipaiSpid_u;

	/**
	 * 拍拍在财付通非垫资spid
	 *
	 * 版本 >= 0
	 */
	 private String paipaiUnpaySpid = new String();

	/**
	 * 版本 >= 0
	 */
	 private short paipaiUnpaySpid_u;

	/**
	 * 物流公司名称
	 *
	 * 版本 >= 0
	 */
	 private String companyName = new String();

	/**
	 * 版本 >= 0
	 */
	 private short companyName_u;

	/**
	 * 物流公司url
	 *
	 * 版本 >= 0
	 */
	 private String companyUrl = new String();

	/**
	 * 版本 >= 0
	 */
	 private short companyUrl_u;

	/**
	 * 开始时间
	 *
	 * 版本 >= 0
	 */
	 private long startTime;

	/**
	 * 版本 >= 0
	 */
	 private short startTime_u;

	/**
	 * 结束时间
	 *
	 * 版本 >= 0
	 */
	 private long endTime;

	/**
	 * 版本 >= 0
	 */
	 private short endTime_u;

	/**
	 * fetch开始时间
	 *
	 * 版本 >= 0
	 */
	 private long fetchStartTime;

	/**
	 * 版本 >= 0
	 */
	 private short fetchStartTime_u;

	/**
	 * fetch结束时间
	 *
	 * 版本 >= 0
	 */
	 private long fetchEndTime;

	/**
	 * 版本 >= 0
	 */
	 private short fetchEndTime_u;

	/**
	 * 物流公司在cft垫资spid
	 *
	 * 版本 >= 0
	 */
	 private String CompanySpid = new String();

	/**
	 * 版本 >= 0
	 */
	 private short CompanySpid_u;

	/**
	 * 物流公司在cft非垫资spid
	 *
	 * 版本 >= 0
	 */
	 private long CompanyUnpaySpid;

	/**
	 * 版本 >= 0
	 */
	 private short CompanyUnpaySpid_u;

	/**
	 * 历史多费率信息
	 *
	 * 版本 >= 0
	 */
	 private Vector<FeeRateHistor> feeRateHis = new Vector<FeeRateHistor>();

	/**
	 * 版本 >= 0
	 */
	 private short feeRateHis_u;

	/**
	 * 保留参数
	 *
	 * 版本 >= 0
	 */
	 private String reserveIn = new String();

	/**
	 * 版本 >= 0
	 */
	 private short reserveIn_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushUInt(innerCompanyId);
		bs.pushUByte(innerCompanyId_u);
		bs.pushUInt(feeRate);
		bs.pushUByte(feeRate_u);
		bs.pushUInt(lowLimitAmount);
		bs.pushUByte(lowLimitAmount_u);
		bs.pushUInt(upLimitAmount);
		bs.pushUByte(upLimitAmount_u);
		bs.pushUInt(lowLimitFee);
		bs.pushUByte(lowLimitFee_u);
		bs.pushUInt(upLimitFee);
		bs.pushUByte(upLimitFee_u);
		bs.pushUInt(cftDivideRate);
		bs.pushUByte(cftDivideRate_u);
		bs.pushUInt(paipaiDivideRate);
		bs.pushUByte(paipaiDivideRate_u);
		bs.pushUInt(companyDivideRate);
		bs.pushUByte(companyDivideRate_u);
		bs.pushUInt(lowLimitDay);
		bs.pushUByte(lowLimitDay_u);
		bs.pushUInt(upLimitDay);
		bs.pushUByte(upLimitDay_u);
		bs.pushUInt(flag);
		bs.pushUByte(flag_u);
		bs.pushUInt(prePayMode);
		bs.pushUByte(prePayMode_u);
		bs.pushUInt(deliveryFee);
		bs.pushUByte(deliveryFee_u);
		bs.pushUInt(wuliuCompanyId);
		bs.pushUByte(wuliuCompanyId_u);
		bs.pushString(paipaiSpid);
		bs.pushUByte(paipaiSpid_u);
		bs.pushString(paipaiUnpaySpid);
		bs.pushUByte(paipaiUnpaySpid_u);
		bs.pushString(companyName);
		bs.pushUByte(companyName_u);
		bs.pushString(companyUrl);
		bs.pushUByte(companyUrl_u);
		bs.pushLong(startTime);
		bs.pushUByte(startTime_u);
		bs.pushLong(endTime);
		bs.pushUByte(endTime_u);
		bs.pushLong(fetchStartTime);
		bs.pushUByte(fetchStartTime_u);
		bs.pushLong(fetchEndTime);
		bs.pushUByte(fetchEndTime_u);
		bs.pushString(CompanySpid);
		bs.pushUByte(CompanySpid_u);
		bs.pushLong(CompanyUnpaySpid);
		bs.pushUByte(CompanyUnpaySpid_u);
		bs.pushObject(feeRateHis);
		bs.pushUByte(feeRateHis_u);
		bs.pushString(reserveIn);
		bs.pushUByte(reserveIn_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		innerCompanyId = bs.popUInt();
		innerCompanyId_u = bs.popUByte();
		feeRate = bs.popUInt();
		feeRate_u = bs.popUByte();
		lowLimitAmount = bs.popUInt();
		lowLimitAmount_u = bs.popUByte();
		upLimitAmount = bs.popUInt();
		upLimitAmount_u = bs.popUByte();
		lowLimitFee = bs.popUInt();
		lowLimitFee_u = bs.popUByte();
		upLimitFee = bs.popUInt();
		upLimitFee_u = bs.popUByte();
		cftDivideRate = bs.popUInt();
		cftDivideRate_u = bs.popUByte();
		paipaiDivideRate = bs.popUInt();
		paipaiDivideRate_u = bs.popUByte();
		companyDivideRate = bs.popUInt();
		companyDivideRate_u = bs.popUByte();
		lowLimitDay = bs.popUInt();
		lowLimitDay_u = bs.popUByte();
		upLimitDay = bs.popUInt();
		upLimitDay_u = bs.popUByte();
		flag = bs.popUInt();
		flag_u = bs.popUByte();
		prePayMode = bs.popUInt();
		prePayMode_u = bs.popUByte();
		deliveryFee = bs.popUInt();
		deliveryFee_u = bs.popUByte();
		wuliuCompanyId = bs.popUInt();
		wuliuCompanyId_u = bs.popUByte();
		paipaiSpid = bs.popString();
		paipaiSpid_u = bs.popUByte();
		paipaiUnpaySpid = bs.popString();
		paipaiUnpaySpid_u = bs.popUByte();
		companyName = bs.popString();
		companyName_u = bs.popUByte();
		companyUrl = bs.popString();
		companyUrl_u = bs.popUByte();
		startTime = bs.popLong();
		startTime_u = bs.popUByte();
		endTime = bs.popLong();
		endTime_u = bs.popUByte();
		fetchStartTime = bs.popLong();
		fetchStartTime_u = bs.popUByte();
		fetchEndTime = bs.popLong();
		fetchEndTime_u = bs.popUByte();
		CompanySpid = bs.popString();
		CompanySpid_u = bs.popUByte();
		CompanyUnpaySpid = bs.popLong();
		CompanyUnpaySpid_u = bs.popUByte();
		feeRateHis = (Vector<FeeRateHistor>)bs.popVector(FeeRateHistor.class);
		feeRateHis_u = bs.popUByte();
		reserveIn = bs.popString();
		reserveIn_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取内部ID
	 * 
	 * 此字段的版本 >= 0
	 * @return innerCompanyId value 类型为:long
	 * 
	 */
	public long getInnerCompanyId()
	{
		return innerCompanyId;
	}


	/**
	 * 设置内部ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setInnerCompanyId(long value)
	{
		this.innerCompanyId = value;
		this.innerCompanyId_u = 1;
	}


	/**
	 * 获取内部ID
	 * 
	 * 此字段的版本 >= 0
	 * @return innerCompanyId_u value 类型为:short
	 * 
	 */
	public short getInnerCompanyId_u()
	{
		return innerCompanyId_u;
	}


	/**
	 * 设置内部ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setInnerCompanyId_u(short value)
	{
		this.innerCompanyId_u = value;
	}


	/**
	 * 获取货到付款费率
	 * 
	 * 此字段的版本 >= 0
	 * @return feeRate value 类型为:long
	 * 
	 */
	public long getFeeRate()
	{
		return feeRate;
	}


	/**
	 * 设置货到付款费率
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFeeRate(long value)
	{
		this.feeRate = value;
		this.feeRate_u = 1;
	}


	/**
	 * 获取货到付款费率
	 * 
	 * 此字段的版本 >= 0
	 * @return feeRate_u value 类型为:short
	 * 
	 */
	public short getFeeRate_u()
	{
		return feeRate_u;
	}


	/**
	 * 设置货到付款费率
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFeeRate_u(short value)
	{
		this.feeRate_u = value;
	}


	/**
	 * 获取最低揽收金额
	 * 
	 * 此字段的版本 >= 0
	 * @return lowLimitAmount value 类型为:long
	 * 
	 */
	public long getLowLimitAmount()
	{
		return lowLimitAmount;
	}


	/**
	 * 设置最低揽收金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLowLimitAmount(long value)
	{
		this.lowLimitAmount = value;
		this.lowLimitAmount_u = 1;
	}


	/**
	 * 获取最低揽收金额
	 * 
	 * 此字段的版本 >= 0
	 * @return lowLimitAmount_u value 类型为:short
	 * 
	 */
	public short getLowLimitAmount_u()
	{
		return lowLimitAmount_u;
	}


	/**
	 * 设置最低揽收金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLowLimitAmount_u(short value)
	{
		this.lowLimitAmount_u = value;
	}


	/**
	 * 获取最高揽收金额
	 * 
	 * 此字段的版本 >= 0
	 * @return upLimitAmount value 类型为:long
	 * 
	 */
	public long getUpLimitAmount()
	{
		return upLimitAmount;
	}


	/**
	 * 设置最高揽收金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUpLimitAmount(long value)
	{
		this.upLimitAmount = value;
		this.upLimitAmount_u = 1;
	}


	/**
	 * 获取最高揽收金额
	 * 
	 * 此字段的版本 >= 0
	 * @return upLimitAmount_u value 类型为:short
	 * 
	 */
	public short getUpLimitAmount_u()
	{
		return upLimitAmount_u;
	}


	/**
	 * 设置最高揽收金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUpLimitAmount_u(short value)
	{
		this.upLimitAmount_u = value;
	}


	/**
	 * 获取最低服务费
	 * 
	 * 此字段的版本 >= 0
	 * @return lowLimitFee value 类型为:long
	 * 
	 */
	public long getLowLimitFee()
	{
		return lowLimitFee;
	}


	/**
	 * 设置最低服务费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLowLimitFee(long value)
	{
		this.lowLimitFee = value;
		this.lowLimitFee_u = 1;
	}


	/**
	 * 获取最低服务费
	 * 
	 * 此字段的版本 >= 0
	 * @return lowLimitFee_u value 类型为:short
	 * 
	 */
	public short getLowLimitFee_u()
	{
		return lowLimitFee_u;
	}


	/**
	 * 设置最低服务费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLowLimitFee_u(short value)
	{
		this.lowLimitFee_u = value;
	}


	/**
	 * 获取最高服务费
	 * 
	 * 此字段的版本 >= 0
	 * @return upLimitFee value 类型为:long
	 * 
	 */
	public long getUpLimitFee()
	{
		return upLimitFee;
	}


	/**
	 * 设置最高服务费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUpLimitFee(long value)
	{
		this.upLimitFee = value;
		this.upLimitFee_u = 1;
	}


	/**
	 * 获取最高服务费
	 * 
	 * 此字段的版本 >= 0
	 * @return upLimitFee_u value 类型为:short
	 * 
	 */
	public short getUpLimitFee_u()
	{
		return upLimitFee_u;
	}


	/**
	 * 设置最高服务费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUpLimitFee_u(short value)
	{
		this.upLimitFee_u = value;
	}


	/**
	 * 获取财付通分成比例
	 * 
	 * 此字段的版本 >= 0
	 * @return cftDivideRate value 类型为:long
	 * 
	 */
	public long getCftDivideRate()
	{
		return cftDivideRate;
	}


	/**
	 * 设置财付通分成比例
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCftDivideRate(long value)
	{
		this.cftDivideRate = value;
		this.cftDivideRate_u = 1;
	}


	/**
	 * 获取财付通分成比例
	 * 
	 * 此字段的版本 >= 0
	 * @return cftDivideRate_u value 类型为:short
	 * 
	 */
	public short getCftDivideRate_u()
	{
		return cftDivideRate_u;
	}


	/**
	 * 设置财付通分成比例
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCftDivideRate_u(short value)
	{
		this.cftDivideRate_u = value;
	}


	/**
	 * 获取拍拍分成比例
	 * 
	 * 此字段的版本 >= 0
	 * @return paipaiDivideRate value 类型为:long
	 * 
	 */
	public long getPaipaiDivideRate()
	{
		return paipaiDivideRate;
	}


	/**
	 * 设置拍拍分成比例
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPaipaiDivideRate(long value)
	{
		this.paipaiDivideRate = value;
		this.paipaiDivideRate_u = 1;
	}


	/**
	 * 获取拍拍分成比例
	 * 
	 * 此字段的版本 >= 0
	 * @return paipaiDivideRate_u value 类型为:short
	 * 
	 */
	public short getPaipaiDivideRate_u()
	{
		return paipaiDivideRate_u;
	}


	/**
	 * 设置拍拍分成比例
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPaipaiDivideRate_u(short value)
	{
		this.paipaiDivideRate_u = value;
	}


	/**
	 * 获取物流公司分成比例
	 * 
	 * 此字段的版本 >= 0
	 * @return companyDivideRate value 类型为:long
	 * 
	 */
	public long getCompanyDivideRate()
	{
		return companyDivideRate;
	}


	/**
	 * 设置物流公司分成比例
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCompanyDivideRate(long value)
	{
		this.companyDivideRate = value;
		this.companyDivideRate_u = 1;
	}


	/**
	 * 获取物流公司分成比例
	 * 
	 * 此字段的版本 >= 0
	 * @return companyDivideRate_u value 类型为:short
	 * 
	 */
	public short getCompanyDivideRate_u()
	{
		return companyDivideRate_u;
	}


	/**
	 * 设置物流公司分成比例
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCompanyDivideRate_u(short value)
	{
		this.companyDivideRate_u = value;
	}


	/**
	 * 获取运送时间下限
	 * 
	 * 此字段的版本 >= 0
	 * @return lowLimitDay value 类型为:long
	 * 
	 */
	public long getLowLimitDay()
	{
		return lowLimitDay;
	}


	/**
	 * 设置运送时间下限
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLowLimitDay(long value)
	{
		this.lowLimitDay = value;
		this.lowLimitDay_u = 1;
	}


	/**
	 * 获取运送时间下限
	 * 
	 * 此字段的版本 >= 0
	 * @return lowLimitDay_u value 类型为:short
	 * 
	 */
	public short getLowLimitDay_u()
	{
		return lowLimitDay_u;
	}


	/**
	 * 设置运送时间下限
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLowLimitDay_u(short value)
	{
		this.lowLimitDay_u = value;
	}


	/**
	 * 获取运送时间上限
	 * 
	 * 此字段的版本 >= 0
	 * @return upLimitDay value 类型为:long
	 * 
	 */
	public long getUpLimitDay()
	{
		return upLimitDay;
	}


	/**
	 * 设置运送时间上限
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUpLimitDay(long value)
	{
		this.upLimitDay = value;
		this.upLimitDay_u = 1;
	}


	/**
	 * 获取运送时间上限
	 * 
	 * 此字段的版本 >= 0
	 * @return upLimitDay_u value 类型为:short
	 * 
	 */
	public short getUpLimitDay_u()
	{
		return upLimitDay_u;
	}


	/**
	 * 设置运送时间上限
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUpLimitDay_u(short value)
	{
		this.upLimitDay_u = value;
	}


	/**
	 * 获取cod标识位
	 * 
	 * 此字段的版本 >= 0
	 * @return flag value 类型为:long
	 * 
	 */
	public long getFlag()
	{
		return flag;
	}


	/**
	 * 设置cod标识位
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFlag(long value)
	{
		this.flag = value;
		this.flag_u = 1;
	}


	/**
	 * 获取cod标识位
	 * 
	 * 此字段的版本 >= 0
	 * @return flag_u value 类型为:short
	 * 
	 */
	public short getFlag_u()
	{
		return flag_u;
	}


	/**
	 * 设置cod标识位
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFlag_u(short value)
	{
		this.flag_u = value;
	}


	/**
	 * 获取垫资模式
	 * 
	 * 此字段的版本 >= 0
	 * @return prePayMode value 类型为:long
	 * 
	 */
	public long getPrePayMode()
	{
		return prePayMode;
	}


	/**
	 * 设置垫资模式
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPrePayMode(long value)
	{
		this.prePayMode = value;
		this.prePayMode_u = 1;
	}


	/**
	 * 获取垫资模式
	 * 
	 * 此字段的版本 >= 0
	 * @return prePayMode_u value 类型为:short
	 * 
	 */
	public short getPrePayMode_u()
	{
		return prePayMode_u;
	}


	/**
	 * 设置垫资模式
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPrePayMode_u(short value)
	{
		this.prePayMode_u = value;
	}


	/**
	 * 获取物流公司投递费
	 * 
	 * 此字段的版本 >= 0
	 * @return deliveryFee value 类型为:long
	 * 
	 */
	public long getDeliveryFee()
	{
		return deliveryFee;
	}


	/**
	 * 设置物流公司投递费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDeliveryFee(long value)
	{
		this.deliveryFee = value;
		this.deliveryFee_u = 1;
	}


	/**
	 * 获取物流公司投递费
	 * 
	 * 此字段的版本 >= 0
	 * @return deliveryFee_u value 类型为:short
	 * 
	 */
	public short getDeliveryFee_u()
	{
		return deliveryFee_u;
	}


	/**
	 * 设置物流公司投递费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDeliveryFee_u(short value)
	{
		this.deliveryFee_u = value;
	}


	/**
	 * 获取物流系统中公司id
	 * 
	 * 此字段的版本 >= 0
	 * @return wuliuCompanyId value 类型为:long
	 * 
	 */
	public long getWuliuCompanyId()
	{
		return wuliuCompanyId;
	}


	/**
	 * 设置物流系统中公司id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWuliuCompanyId(long value)
	{
		this.wuliuCompanyId = value;
		this.wuliuCompanyId_u = 1;
	}


	/**
	 * 获取物流系统中公司id
	 * 
	 * 此字段的版本 >= 0
	 * @return wuliuCompanyId_u value 类型为:short
	 * 
	 */
	public short getWuliuCompanyId_u()
	{
		return wuliuCompanyId_u;
	}


	/**
	 * 设置物流系统中公司id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWuliuCompanyId_u(short value)
	{
		this.wuliuCompanyId_u = value;
	}


	/**
	 * 获取拍拍在财付通垫资spid
	 * 
	 * 此字段的版本 >= 0
	 * @return paipaiSpid value 类型为:String
	 * 
	 */
	public String getPaipaiSpid()
	{
		return paipaiSpid;
	}


	/**
	 * 设置拍拍在财付通垫资spid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPaipaiSpid(String value)
	{
		this.paipaiSpid = value;
		this.paipaiSpid_u = 1;
	}


	/**
	 * 获取拍拍在财付通垫资spid
	 * 
	 * 此字段的版本 >= 0
	 * @return paipaiSpid_u value 类型为:short
	 * 
	 */
	public short getPaipaiSpid_u()
	{
		return paipaiSpid_u;
	}


	/**
	 * 设置拍拍在财付通垫资spid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPaipaiSpid_u(short value)
	{
		this.paipaiSpid_u = value;
	}


	/**
	 * 获取拍拍在财付通非垫资spid
	 * 
	 * 此字段的版本 >= 0
	 * @return paipaiUnpaySpid value 类型为:String
	 * 
	 */
	public String getPaipaiUnpaySpid()
	{
		return paipaiUnpaySpid;
	}


	/**
	 * 设置拍拍在财付通非垫资spid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPaipaiUnpaySpid(String value)
	{
		this.paipaiUnpaySpid = value;
		this.paipaiUnpaySpid_u = 1;
	}


	/**
	 * 获取拍拍在财付通非垫资spid
	 * 
	 * 此字段的版本 >= 0
	 * @return paipaiUnpaySpid_u value 类型为:short
	 * 
	 */
	public short getPaipaiUnpaySpid_u()
	{
		return paipaiUnpaySpid_u;
	}


	/**
	 * 设置拍拍在财付通非垫资spid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPaipaiUnpaySpid_u(short value)
	{
		this.paipaiUnpaySpid_u = value;
	}


	/**
	 * 获取物流公司名称
	 * 
	 * 此字段的版本 >= 0
	 * @return companyName value 类型为:String
	 * 
	 */
	public String getCompanyName()
	{
		return companyName;
	}


	/**
	 * 设置物流公司名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCompanyName(String value)
	{
		this.companyName = value;
		this.companyName_u = 1;
	}


	/**
	 * 获取物流公司名称
	 * 
	 * 此字段的版本 >= 0
	 * @return companyName_u value 类型为:short
	 * 
	 */
	public short getCompanyName_u()
	{
		return companyName_u;
	}


	/**
	 * 设置物流公司名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCompanyName_u(short value)
	{
		this.companyName_u = value;
	}


	/**
	 * 获取物流公司url
	 * 
	 * 此字段的版本 >= 0
	 * @return companyUrl value 类型为:String
	 * 
	 */
	public String getCompanyUrl()
	{
		return companyUrl;
	}


	/**
	 * 设置物流公司url
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCompanyUrl(String value)
	{
		this.companyUrl = value;
		this.companyUrl_u = 1;
	}


	/**
	 * 获取物流公司url
	 * 
	 * 此字段的版本 >= 0
	 * @return companyUrl_u value 类型为:short
	 * 
	 */
	public short getCompanyUrl_u()
	{
		return companyUrl_u;
	}


	/**
	 * 设置物流公司url
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCompanyUrl_u(short value)
	{
		this.companyUrl_u = value;
	}


	/**
	 * 获取开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @return startTime value 类型为:long
	 * 
	 */
	public long getStartTime()
	{
		return startTime;
	}


	/**
	 * 设置开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setStartTime(long value)
	{
		this.startTime = value;
		this.startTime_u = 1;
	}


	/**
	 * 获取开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @return startTime_u value 类型为:short
	 * 
	 */
	public short getStartTime_u()
	{
		return startTime_u;
	}


	/**
	 * 设置开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStartTime_u(short value)
	{
		this.startTime_u = value;
	}


	/**
	 * 获取结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return endTime value 类型为:long
	 * 
	 */
	public long getEndTime()
	{
		return endTime;
	}


	/**
	 * 设置结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEndTime(long value)
	{
		this.endTime = value;
		this.endTime_u = 1;
	}


	/**
	 * 获取结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return endTime_u value 类型为:short
	 * 
	 */
	public short getEndTime_u()
	{
		return endTime_u;
	}


	/**
	 * 设置结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEndTime_u(short value)
	{
		this.endTime_u = value;
	}


	/**
	 * 获取fetch开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @return fetchStartTime value 类型为:long
	 * 
	 */
	public long getFetchStartTime()
	{
		return fetchStartTime;
	}


	/**
	 * 设置fetch开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFetchStartTime(long value)
	{
		this.fetchStartTime = value;
		this.fetchStartTime_u = 1;
	}


	/**
	 * 获取fetch开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @return fetchStartTime_u value 类型为:short
	 * 
	 */
	public short getFetchStartTime_u()
	{
		return fetchStartTime_u;
	}


	/**
	 * 设置fetch开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFetchStartTime_u(short value)
	{
		this.fetchStartTime_u = value;
	}


	/**
	 * 获取fetch结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return fetchEndTime value 类型为:long
	 * 
	 */
	public long getFetchEndTime()
	{
		return fetchEndTime;
	}


	/**
	 * 设置fetch结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFetchEndTime(long value)
	{
		this.fetchEndTime = value;
		this.fetchEndTime_u = 1;
	}


	/**
	 * 获取fetch结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return fetchEndTime_u value 类型为:short
	 * 
	 */
	public short getFetchEndTime_u()
	{
		return fetchEndTime_u;
	}


	/**
	 * 设置fetch结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFetchEndTime_u(short value)
	{
		this.fetchEndTime_u = value;
	}


	/**
	 * 获取物流公司在cft垫资spid
	 * 
	 * 此字段的版本 >= 0
	 * @return CompanySpid value 类型为:String
	 * 
	 */
	public String getCompanySpid()
	{
		return CompanySpid;
	}


	/**
	 * 设置物流公司在cft垫资spid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCompanySpid(String value)
	{
		this.CompanySpid = value;
		this.CompanySpid_u = 1;
	}


	/**
	 * 获取物流公司在cft垫资spid
	 * 
	 * 此字段的版本 >= 0
	 * @return CompanySpid_u value 类型为:short
	 * 
	 */
	public short getCompanySpid_u()
	{
		return CompanySpid_u;
	}


	/**
	 * 设置物流公司在cft垫资spid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCompanySpid_u(short value)
	{
		this.CompanySpid_u = value;
	}


	/**
	 * 获取物流公司在cft非垫资spid
	 * 
	 * 此字段的版本 >= 0
	 * @return CompanyUnpaySpid value 类型为:long
	 * 
	 */
	public long getCompanyUnpaySpid()
	{
		return CompanyUnpaySpid;
	}


	/**
	 * 设置物流公司在cft非垫资spid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCompanyUnpaySpid(long value)
	{
		this.CompanyUnpaySpid = value;
		this.CompanyUnpaySpid_u = 1;
	}


	/**
	 * 获取物流公司在cft非垫资spid
	 * 
	 * 此字段的版本 >= 0
	 * @return CompanyUnpaySpid_u value 类型为:short
	 * 
	 */
	public short getCompanyUnpaySpid_u()
	{
		return CompanyUnpaySpid_u;
	}


	/**
	 * 设置物流公司在cft非垫资spid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCompanyUnpaySpid_u(short value)
	{
		this.CompanyUnpaySpid_u = value;
	}


	/**
	 * 获取历史多费率信息
	 * 
	 * 此字段的版本 >= 0
	 * @return feeRateHis value 类型为:Vector<FeeRateHistor>
	 * 
	 */
	public Vector<FeeRateHistor> getFeeRateHis()
	{
		return feeRateHis;
	}


	/**
	 * 设置历史多费率信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<FeeRateHistor>
	 * 
	 */
	public void setFeeRateHis(Vector<FeeRateHistor> value)
	{
		if (value != null) {
				this.feeRateHis = value;
				this.feeRateHis_u = 1;
		}
	}


	/**
	 * 获取历史多费率信息
	 * 
	 * 此字段的版本 >= 0
	 * @return feeRateHis_u value 类型为:short
	 * 
	 */
	public short getFeeRateHis_u()
	{
		return feeRateHis_u;
	}


	/**
	 * 设置历史多费率信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFeeRateHis_u(short value)
	{
		this.feeRateHis_u = value;
	}


	/**
	 * 获取保留参数
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveIn value 类型为:String
	 * 
	 */
	public String getReserveIn()
	{
		return reserveIn;
	}


	/**
	 * 设置保留参数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveIn(String value)
	{
		this.reserveIn = value;
		this.reserveIn_u = 1;
	}


	/**
	 * 获取保留参数
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveIn_u value 类型为:short
	 * 
	 */
	public short getReserveIn_u()
	{
		return reserveIn_u;
	}


	/**
	 * 设置保留参数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserveIn_u(short value)
	{
		this.reserveIn_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CODCompanyConfig)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段innerCompanyId的长度 size_of(uint32_t)
				length += 1;  //计算字段innerCompanyId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段feeRate的长度 size_of(uint32_t)
				length += 1;  //计算字段feeRate_u的长度 size_of(uint8_t)
				length += 4;  //计算字段lowLimitAmount的长度 size_of(uint32_t)
				length += 1;  //计算字段lowLimitAmount_u的长度 size_of(uint8_t)
				length += 4;  //计算字段upLimitAmount的长度 size_of(uint32_t)
				length += 1;  //计算字段upLimitAmount_u的长度 size_of(uint8_t)
				length += 4;  //计算字段lowLimitFee的长度 size_of(uint32_t)
				length += 1;  //计算字段lowLimitFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段upLimitFee的长度 size_of(uint32_t)
				length += 1;  //计算字段upLimitFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段cftDivideRate的长度 size_of(uint32_t)
				length += 1;  //计算字段cftDivideRate_u的长度 size_of(uint8_t)
				length += 4;  //计算字段paipaiDivideRate的长度 size_of(uint32_t)
				length += 1;  //计算字段paipaiDivideRate_u的长度 size_of(uint8_t)
				length += 4;  //计算字段companyDivideRate的长度 size_of(uint32_t)
				length += 1;  //计算字段companyDivideRate_u的长度 size_of(uint8_t)
				length += 4;  //计算字段lowLimitDay的长度 size_of(uint32_t)
				length += 1;  //计算字段lowLimitDay_u的长度 size_of(uint8_t)
				length += 4;  //计算字段upLimitDay的长度 size_of(uint32_t)
				length += 1;  //计算字段upLimitDay_u的长度 size_of(uint8_t)
				length += 4;  //计算字段flag的长度 size_of(uint32_t)
				length += 1;  //计算字段flag_u的长度 size_of(uint8_t)
				length += 4;  //计算字段prePayMode的长度 size_of(uint32_t)
				length += 1;  //计算字段prePayMode_u的长度 size_of(uint8_t)
				length += 4;  //计算字段deliveryFee的长度 size_of(uint32_t)
				length += 1;  //计算字段deliveryFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段wuliuCompanyId的长度 size_of(uint32_t)
				length += 1;  //计算字段wuliuCompanyId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(paipaiSpid, null);  //计算字段paipaiSpid的长度 size_of(String)
				length += 1;  //计算字段paipaiSpid_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(paipaiUnpaySpid, null);  //计算字段paipaiUnpaySpid的长度 size_of(String)
				length += 1;  //计算字段paipaiUnpaySpid_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(companyName, null);  //计算字段companyName的长度 size_of(String)
				length += 1;  //计算字段companyName_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(companyUrl, null);  //计算字段companyUrl的长度 size_of(String)
				length += 1;  //计算字段companyUrl_u的长度 size_of(uint8_t)
				length += 17;  //计算字段startTime的长度 size_of(uint64_t)
				length += 1;  //计算字段startTime_u的长度 size_of(uint8_t)
				length += 17;  //计算字段endTime的长度 size_of(uint64_t)
				length += 1;  //计算字段endTime_u的长度 size_of(uint8_t)
				length += 17;  //计算字段fetchStartTime的长度 size_of(uint64_t)
				length += 1;  //计算字段fetchStartTime_u的长度 size_of(uint8_t)
				length += 17;  //计算字段fetchEndTime的长度 size_of(uint64_t)
				length += 1;  //计算字段fetchEndTime_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(CompanySpid, null);  //计算字段CompanySpid的长度 size_of(String)
				length += 1;  //计算字段CompanySpid_u的长度 size_of(uint8_t)
				length += 17;  //计算字段CompanyUnpaySpid的长度 size_of(uint64_t)
				length += 1;  //计算字段CompanyUnpaySpid_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(feeRateHis, null);  //计算字段feeRateHis的长度 size_of(Vector)
				length += 1;  //计算字段feeRateHis_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(reserveIn, null);  //计算字段reserveIn的长度 size_of(String)
				length += 1;  //计算字段reserveIn_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(CODCompanyConfig)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段innerCompanyId的长度 size_of(uint32_t)
				length += 1;  //计算字段innerCompanyId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段feeRate的长度 size_of(uint32_t)
				length += 1;  //计算字段feeRate_u的长度 size_of(uint8_t)
				length += 4;  //计算字段lowLimitAmount的长度 size_of(uint32_t)
				length += 1;  //计算字段lowLimitAmount_u的长度 size_of(uint8_t)
				length += 4;  //计算字段upLimitAmount的长度 size_of(uint32_t)
				length += 1;  //计算字段upLimitAmount_u的长度 size_of(uint8_t)
				length += 4;  //计算字段lowLimitFee的长度 size_of(uint32_t)
				length += 1;  //计算字段lowLimitFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段upLimitFee的长度 size_of(uint32_t)
				length += 1;  //计算字段upLimitFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段cftDivideRate的长度 size_of(uint32_t)
				length += 1;  //计算字段cftDivideRate_u的长度 size_of(uint8_t)
				length += 4;  //计算字段paipaiDivideRate的长度 size_of(uint32_t)
				length += 1;  //计算字段paipaiDivideRate_u的长度 size_of(uint8_t)
				length += 4;  //计算字段companyDivideRate的长度 size_of(uint32_t)
				length += 1;  //计算字段companyDivideRate_u的长度 size_of(uint8_t)
				length += 4;  //计算字段lowLimitDay的长度 size_of(uint32_t)
				length += 1;  //计算字段lowLimitDay_u的长度 size_of(uint8_t)
				length += 4;  //计算字段upLimitDay的长度 size_of(uint32_t)
				length += 1;  //计算字段upLimitDay_u的长度 size_of(uint8_t)
				length += 4;  //计算字段flag的长度 size_of(uint32_t)
				length += 1;  //计算字段flag_u的长度 size_of(uint8_t)
				length += 4;  //计算字段prePayMode的长度 size_of(uint32_t)
				length += 1;  //计算字段prePayMode_u的长度 size_of(uint8_t)
				length += 4;  //计算字段deliveryFee的长度 size_of(uint32_t)
				length += 1;  //计算字段deliveryFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段wuliuCompanyId的长度 size_of(uint32_t)
				length += 1;  //计算字段wuliuCompanyId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(paipaiSpid, encoding);  //计算字段paipaiSpid的长度 size_of(String)
				length += 1;  //计算字段paipaiSpid_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(paipaiUnpaySpid, encoding);  //计算字段paipaiUnpaySpid的长度 size_of(String)
				length += 1;  //计算字段paipaiUnpaySpid_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(companyName, encoding);  //计算字段companyName的长度 size_of(String)
				length += 1;  //计算字段companyName_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(companyUrl, encoding);  //计算字段companyUrl的长度 size_of(String)
				length += 1;  //计算字段companyUrl_u的长度 size_of(uint8_t)
				length += 17;  //计算字段startTime的长度 size_of(uint64_t)
				length += 1;  //计算字段startTime_u的长度 size_of(uint8_t)
				length += 17;  //计算字段endTime的长度 size_of(uint64_t)
				length += 1;  //计算字段endTime_u的长度 size_of(uint8_t)
				length += 17;  //计算字段fetchStartTime的长度 size_of(uint64_t)
				length += 1;  //计算字段fetchStartTime_u的长度 size_of(uint8_t)
				length += 17;  //计算字段fetchEndTime的长度 size_of(uint64_t)
				length += 1;  //计算字段fetchEndTime_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(CompanySpid, encoding);  //计算字段CompanySpid的长度 size_of(String)
				length += 1;  //计算字段CompanySpid_u的长度 size_of(uint8_t)
				length += 17;  //计算字段CompanyUnpaySpid的长度 size_of(uint64_t)
				length += 1;  //计算字段CompanyUnpaySpid_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(feeRateHis, encoding);  //计算字段feeRateHis的长度 size_of(Vector)
				length += 1;  //计算字段feeRateHis_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(reserveIn, encoding);  //计算字段reserveIn的长度 size_of(String)
				length += 1;  //计算字段reserveIn_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
