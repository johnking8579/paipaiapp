//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.CODCompanyConfig.java

package com.qq.qqbuy.thirdparty.idl.codFee.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

/**
 * 历史多费率信息
 *
 *@date 2013-08-16 06:07:21
 *
 *@since version:0
*/
public class FeeRateHistor  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 费率
	 *
	 * 版本 >= 0
	 */
	 private long feeRate;

	/**
	 * 版本 >= 0
	 */
	 private short feeRate_u;

	/**
	 * 最低服务费
	 *
	 * 版本 >= 0
	 */
	 private long lowLimitFee;

	/**
	 * 版本 >= 0
	 */
	 private short lowLimitFee_u;

	/**
	 * 最高服务费
	 *
	 * 版本 >= 0
	 */
	 private long upLimitFee;

	/**
	 * 版本 >= 0
	 */
	 private short upLimitFee_u;

	/**
	 * 开始时间
	 *
	 * 版本 >= 0
	 */
	 private long startTime;

	/**
	 * 版本 >= 0
	 */
	 private short startTime_u;

	/**
	 * 结束时间
	 *
	 * 版本 >= 0
	 */
	 private long endTime;

	/**
	 * 版本 >= 0
	 */
	 private short endTime_u;

	/**
	 * 标识
	 *
	 * 版本 >= 0
	 */
	 private long flag;

	/**
	 * 版本 >= 0
	 */
	 private short flag_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushUInt(feeRate);
		bs.pushUByte(feeRate_u);
		bs.pushUInt(lowLimitFee);
		bs.pushUByte(lowLimitFee_u);
		bs.pushUInt(upLimitFee);
		bs.pushUByte(upLimitFee_u);
		bs.pushUInt(startTime);
		bs.pushUByte(startTime_u);
		bs.pushUInt(endTime);
		bs.pushUByte(endTime_u);
		bs.pushUInt(flag);
		bs.pushUByte(flag_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		feeRate = bs.popUInt();
		feeRate_u = bs.popUByte();
		lowLimitFee = bs.popUInt();
		lowLimitFee_u = bs.popUByte();
		upLimitFee = bs.popUInt();
		upLimitFee_u = bs.popUByte();
		startTime = bs.popUInt();
		startTime_u = bs.popUByte();
		endTime = bs.popUInt();
		endTime_u = bs.popUByte();
		flag = bs.popUInt();
		flag_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取费率
	 * 
	 * 此字段的版本 >= 0
	 * @return feeRate value 类型为:long
	 * 
	 */
	public long getFeeRate()
	{
		return feeRate;
	}


	/**
	 * 设置费率
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFeeRate(long value)
	{
		this.feeRate = value;
		this.feeRate_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return feeRate_u value 类型为:short
	 * 
	 */
	public short getFeeRate_u()
	{
		return feeRate_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFeeRate_u(short value)
	{
		this.feeRate_u = value;
	}


	/**
	 * 获取最低服务费
	 * 
	 * 此字段的版本 >= 0
	 * @return lowLimitFee value 类型为:long
	 * 
	 */
	public long getLowLimitFee()
	{
		return lowLimitFee;
	}


	/**
	 * 设置最低服务费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLowLimitFee(long value)
	{
		this.lowLimitFee = value;
		this.lowLimitFee_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return lowLimitFee_u value 类型为:short
	 * 
	 */
	public short getLowLimitFee_u()
	{
		return lowLimitFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLowLimitFee_u(short value)
	{
		this.lowLimitFee_u = value;
	}


	/**
	 * 获取最高服务费
	 * 
	 * 此字段的版本 >= 0
	 * @return upLimitFee value 类型为:long
	 * 
	 */
	public long getUpLimitFee()
	{
		return upLimitFee;
	}


	/**
	 * 设置最高服务费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUpLimitFee(long value)
	{
		this.upLimitFee = value;
		this.upLimitFee_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return upLimitFee_u value 类型为:short
	 * 
	 */
	public short getUpLimitFee_u()
	{
		return upLimitFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUpLimitFee_u(short value)
	{
		this.upLimitFee_u = value;
	}


	/**
	 * 获取开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @return startTime value 类型为:long
	 * 
	 */
	public long getStartTime()
	{
		return startTime;
	}


	/**
	 * 设置开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setStartTime(long value)
	{
		this.startTime = value;
		this.startTime_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return startTime_u value 类型为:short
	 * 
	 */
	public short getStartTime_u()
	{
		return startTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStartTime_u(short value)
	{
		this.startTime_u = value;
	}


	/**
	 * 获取结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return endTime value 类型为:long
	 * 
	 */
	public long getEndTime()
	{
		return endTime;
	}


	/**
	 * 设置结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEndTime(long value)
	{
		this.endTime = value;
		this.endTime_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return endTime_u value 类型为:short
	 * 
	 */
	public short getEndTime_u()
	{
		return endTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEndTime_u(short value)
	{
		this.endTime_u = value;
	}


	/**
	 * 获取标识
	 * 
	 * 此字段的版本 >= 0
	 * @return flag value 类型为:long
	 * 
	 */
	public long getFlag()
	{
		return flag;
	}


	/**
	 * 设置标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFlag(long value)
	{
		this.flag = value;
		this.flag_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return flag_u value 类型为:short
	 * 
	 */
	public short getFlag_u()
	{
		return flag_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFlag_u(short value)
	{
		this.flag_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(FeeRateHistor)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段feeRate的长度 size_of(uint32_t)
				length += 1;  //计算字段feeRate_u的长度 size_of(uint8_t)
				length += 4;  //计算字段lowLimitFee的长度 size_of(uint32_t)
				length += 1;  //计算字段lowLimitFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段upLimitFee的长度 size_of(uint32_t)
				length += 1;  //计算字段upLimitFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段startTime的长度 size_of(uint32_t)
				length += 1;  //计算字段startTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段endTime的长度 size_of(uint32_t)
				length += 1;  //计算字段endTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段flag的长度 size_of(uint32_t)
				length += 1;  //计算字段flag_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(FeeRateHistor)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段feeRate的长度 size_of(uint32_t)
				length += 1;  //计算字段feeRate_u的长度 size_of(uint8_t)
				length += 4;  //计算字段lowLimitFee的长度 size_of(uint32_t)
				length += 1;  //计算字段lowLimitFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段upLimitFee的长度 size_of(uint32_t)
				length += 1;  //计算字段upLimitFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段startTime的长度 size_of(uint32_t)
				length += 1;  //计算字段startTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段endTime的长度 size_of(uint32_t)
				length += 1;  //计算字段endTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段flag的长度 size_of(uint32_t)
				length += 1;  //计算字段flag_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
