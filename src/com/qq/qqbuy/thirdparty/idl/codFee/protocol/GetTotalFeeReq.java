 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.CodConfig.java

package com.qq.qqbuy.thirdparty.idl.codFee.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 * 获取服务费请求
 *
 *@date 2013-08-16 06:07:21
 *
 *@since version:0
*/
public class  GetTotalFeeReq implements IServiceObject
{
	/**
	 * 请求来源描述
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 请求
	 *
	 * 版本 >= 0
	 */
	 private CODFeeFilter reqData = new CODFeeFilter();

	/**
	 * 保留输入字段
	 *
	 * 版本 >= 0
	 */
	 private String reserveIn = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(source);
		bs.pushString(machineKey);
		bs.pushObject(reqData);
		bs.pushString(reserveIn);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		source = bs.popString();
		machineKey = bs.popString();
		reqData = (CODFeeFilter) bs.popObject(CODFeeFilter.class);
		reserveIn = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x63451802L;
	}


	/**
	 * 获取请求来源描述
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置请求来源描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取请求
	 * 
	 * 此字段的版本 >= 0
	 * @return reqData value 类型为:CODFeeFilter
	 * 
	 */
	public CODFeeFilter getReqData()
	{
		return reqData;
	}


	/**
	 * 设置请求
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:CODFeeFilter
	 * 
	 */
	public void setReqData(CODFeeFilter value)
	{
		if (value != null) {
				this.reqData = value;
		}else{
				this.reqData = new CODFeeFilter();
		}
	}


	/**
	 * 获取保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveIn value 类型为:String
	 * 
	 */
	public String getReserveIn()
	{
		return reserveIn;
	}


	/**
	 * 设置保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveIn(String value)
	{
		this.reserveIn = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetTotalFeeReq)
				length += ByteStream.getObjectSize(source, null);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(machineKey, null);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(reqData, null);  //计算字段reqData的长度 size_of(CODFeeFilter)
				length += ByteStream.getObjectSize(reserveIn, null);  //计算字段reserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetTotalFeeReq)
				length += ByteStream.getObjectSize(source, encoding);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(machineKey, encoding);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(reqData, encoding);  //计算字段reqData的长度 size_of(CODFeeFilter)
				length += ByteStream.getObjectSize(reserveIn, encoding);  //计算字段reserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
