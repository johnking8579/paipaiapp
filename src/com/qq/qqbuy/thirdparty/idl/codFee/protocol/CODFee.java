//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.CodConfig.java

package com.qq.qqbuy.thirdparty.idl.codFee.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

/**
 * 获取服务费响应
 *
 *@date 2013-08-16 06:07:21
 *
 *@since version:0
*/
public class CODFee  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 内部id
	 *
	 * 版本 >= 0
	 */
	 private long innerCompanyId;

	/**
	 * 版本 >= 0
	 */
	 private short innerCompanyId_u;

	/**
	 * countTotalFee
	 *
	 * 版本 >= 0
	 */
	 private long countTotalFee;

	/**
	 * 版本 >= 0
	 */
	 private short countTotalFee_u;

	/**
	 * realTotalFee
	 *
	 * 版本 >= 0
	 */
	 private long realTotalFee;

	/**
	 * 版本 >= 0
	 */
	 private short realTotalFee_u;

	/**
	 * dealAdjustFee
	 *
	 * 版本 >= 0
	 */
	 private long dealAdjustFee;

	/**
	 * 版本 >= 0
	 */
	 private short dealAdjustFee_u;

	/**
	 * cftFee
	 *
	 * 版本 >= 0
	 */
	 private long cftFee;

	/**
	 * 版本 >= 0
	 */
	 private short cftFee_u;

	/**
	 * companyFee
	 *
	 * 版本 >= 0
	 */
	 private long companyFee;

	/**
	 * 版本 >= 0
	 */
	 private short companyFee_u;

	/**
	 * paipaiFee
	 *
	 * 版本 >= 0
	 */
	 private long paipaiFee;

	/**
	 * 版本 >= 0
	 */
	 private short paipaiFee_u;

	/**
	 * prepayMode
	 *
	 * 版本 >= 0
	 */
	 private long prepayMode;

	/**
	 * 版本 >= 0
	 */
	 private short prepayMode_u;

	/**
	 * deliveryFee
	 *
	 * 版本 >= 0
	 */
	 private long deliveryFee;

	/**
	 * 版本 >= 0
	 */
	 private short deliveryFee_u;

	/**
	 * companyReturnFee
	 *
	 * 版本 >= 0
	 */
	 private long companyReturnFee;

	/**
	 * 版本 >= 0
	 */
	 private short companyReturnFee_u;

	/**
	 * 保留参数
	 *
	 * 版本 >= 0
	 */
	 private String reserveIn = new String();

	/**
	 * 版本 >= 0
	 */
	 private short reserveIn_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushUInt(innerCompanyId);
		bs.pushUByte(innerCompanyId_u);
		bs.pushUInt(countTotalFee);
		bs.pushUByte(countTotalFee_u);
		bs.pushUInt(realTotalFee);
		bs.pushUByte(realTotalFee_u);
		bs.pushUInt(dealAdjustFee);
		bs.pushUByte(dealAdjustFee_u);
		bs.pushUInt(cftFee);
		bs.pushUByte(cftFee_u);
		bs.pushUInt(companyFee);
		bs.pushUByte(companyFee_u);
		bs.pushUInt(paipaiFee);
		bs.pushUByte(paipaiFee_u);
		bs.pushUInt(prepayMode);
		bs.pushUByte(prepayMode_u);
		bs.pushUInt(deliveryFee);
		bs.pushUByte(deliveryFee_u);
		bs.pushUInt(companyReturnFee);
		bs.pushUByte(companyReturnFee_u);
		bs.pushString(reserveIn);
		bs.pushUByte(reserveIn_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		innerCompanyId = bs.popUInt();
		innerCompanyId_u = bs.popUByte();
		countTotalFee = bs.popUInt();
		countTotalFee_u = bs.popUByte();
		realTotalFee = bs.popUInt();
		realTotalFee_u = bs.popUByte();
		dealAdjustFee = bs.popUInt();
		dealAdjustFee_u = bs.popUByte();
		cftFee = bs.popUInt();
		cftFee_u = bs.popUByte();
		companyFee = bs.popUInt();
		companyFee_u = bs.popUByte();
		paipaiFee = bs.popUInt();
		paipaiFee_u = bs.popUByte();
		prepayMode = bs.popUInt();
		prepayMode_u = bs.popUByte();
		deliveryFee = bs.popUInt();
		deliveryFee_u = bs.popUByte();
		companyReturnFee = bs.popUInt();
		companyReturnFee_u = bs.popUByte();
		reserveIn = bs.popString();
		reserveIn_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取内部id
	 * 
	 * 此字段的版本 >= 0
	 * @return innerCompanyId value 类型为:long
	 * 
	 */
	public long getInnerCompanyId()
	{
		return innerCompanyId;
	}


	/**
	 * 设置内部id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setInnerCompanyId(long value)
	{
		this.innerCompanyId = value;
		this.innerCompanyId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return innerCompanyId_u value 类型为:short
	 * 
	 */
	public short getInnerCompanyId_u()
	{
		return innerCompanyId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setInnerCompanyId_u(short value)
	{
		this.innerCompanyId_u = value;
	}


	/**
	 * 获取countTotalFee
	 * 
	 * 此字段的版本 >= 0
	 * @return countTotalFee value 类型为:long
	 * 
	 */
	public long getCountTotalFee()
	{
		return countTotalFee;
	}


	/**
	 * 设置countTotalFee
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCountTotalFee(long value)
	{
		this.countTotalFee = value;
		this.countTotalFee_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return countTotalFee_u value 类型为:short
	 * 
	 */
	public short getCountTotalFee_u()
	{
		return countTotalFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCountTotalFee_u(short value)
	{
		this.countTotalFee_u = value;
	}


	/**
	 * 获取realTotalFee
	 * 
	 * 此字段的版本 >= 0
	 * @return realTotalFee value 类型为:long
	 * 
	 */
	public long getRealTotalFee()
	{
		return realTotalFee;
	}


	/**
	 * 设置realTotalFee
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRealTotalFee(long value)
	{
		this.realTotalFee = value;
		this.realTotalFee_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return realTotalFee_u value 类型为:short
	 * 
	 */
	public short getRealTotalFee_u()
	{
		return realTotalFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRealTotalFee_u(short value)
	{
		this.realTotalFee_u = value;
	}


	/**
	 * 获取dealAdjustFee
	 * 
	 * 此字段的版本 >= 0
	 * @return dealAdjustFee value 类型为:long
	 * 
	 */
	public long getDealAdjustFee()
	{
		return dealAdjustFee;
	}


	/**
	 * 设置dealAdjustFee
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealAdjustFee(long value)
	{
		this.dealAdjustFee = value;
		this.dealAdjustFee_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dealAdjustFee_u value 类型为:short
	 * 
	 */
	public short getDealAdjustFee_u()
	{
		return dealAdjustFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealAdjustFee_u(short value)
	{
		this.dealAdjustFee_u = value;
	}


	/**
	 * 获取cftFee
	 * 
	 * 此字段的版本 >= 0
	 * @return cftFee value 类型为:long
	 * 
	 */
	public long getCftFee()
	{
		return cftFee;
	}


	/**
	 * 设置cftFee
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCftFee(long value)
	{
		this.cftFee = value;
		this.cftFee_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cftFee_u value 类型为:short
	 * 
	 */
	public short getCftFee_u()
	{
		return cftFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCftFee_u(short value)
	{
		this.cftFee_u = value;
	}


	/**
	 * 获取companyFee
	 * 
	 * 此字段的版本 >= 0
	 * @return companyFee value 类型为:long
	 * 
	 */
	public long getCompanyFee()
	{
		return companyFee;
	}


	/**
	 * 设置companyFee
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCompanyFee(long value)
	{
		this.companyFee = value;
		this.companyFee_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return companyFee_u value 类型为:short
	 * 
	 */
	public short getCompanyFee_u()
	{
		return companyFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCompanyFee_u(short value)
	{
		this.companyFee_u = value;
	}


	/**
	 * 获取paipaiFee
	 * 
	 * 此字段的版本 >= 0
	 * @return paipaiFee value 类型为:long
	 * 
	 */
	public long getPaipaiFee()
	{
		return paipaiFee;
	}


	/**
	 * 设置paipaiFee
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPaipaiFee(long value)
	{
		this.paipaiFee = value;
		this.paipaiFee_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return paipaiFee_u value 类型为:short
	 * 
	 */
	public short getPaipaiFee_u()
	{
		return paipaiFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPaipaiFee_u(short value)
	{
		this.paipaiFee_u = value;
	}


	/**
	 * 获取prepayMode
	 * 
	 * 此字段的版本 >= 0
	 * @return prepayMode value 类型为:long
	 * 
	 */
	public long getPrepayMode()
	{
		return prepayMode;
	}


	/**
	 * 设置prepayMode
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPrepayMode(long value)
	{
		this.prepayMode = value;
		this.prepayMode_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return prepayMode_u value 类型为:short
	 * 
	 */
	public short getPrepayMode_u()
	{
		return prepayMode_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPrepayMode_u(short value)
	{
		this.prepayMode_u = value;
	}


	/**
	 * 获取deliveryFee
	 * 
	 * 此字段的版本 >= 0
	 * @return deliveryFee value 类型为:long
	 * 
	 */
	public long getDeliveryFee()
	{
		return deliveryFee;
	}


	/**
	 * 设置deliveryFee
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDeliveryFee(long value)
	{
		this.deliveryFee = value;
		this.deliveryFee_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return deliveryFee_u value 类型为:short
	 * 
	 */
	public short getDeliveryFee_u()
	{
		return deliveryFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDeliveryFee_u(short value)
	{
		this.deliveryFee_u = value;
	}


	/**
	 * 获取companyReturnFee
	 * 
	 * 此字段的版本 >= 0
	 * @return companyReturnFee value 类型为:long
	 * 
	 */
	public long getCompanyReturnFee()
	{
		return companyReturnFee;
	}


	/**
	 * 设置companyReturnFee
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCompanyReturnFee(long value)
	{
		this.companyReturnFee = value;
		this.companyReturnFee_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return companyReturnFee_u value 类型为:short
	 * 
	 */
	public short getCompanyReturnFee_u()
	{
		return companyReturnFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCompanyReturnFee_u(short value)
	{
		this.companyReturnFee_u = value;
	}


	/**
	 * 获取保留参数
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveIn value 类型为:String
	 * 
	 */
	public String getReserveIn()
	{
		return reserveIn;
	}


	/**
	 * 设置保留参数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveIn(String value)
	{
		this.reserveIn = value;
		this.reserveIn_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveIn_u value 类型为:short
	 * 
	 */
	public short getReserveIn_u()
	{
		return reserveIn_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserveIn_u(short value)
	{
		this.reserveIn_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CODFee)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段innerCompanyId的长度 size_of(uint32_t)
				length += 1;  //计算字段innerCompanyId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段countTotalFee的长度 size_of(uint32_t)
				length += 1;  //计算字段countTotalFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段realTotalFee的长度 size_of(uint32_t)
				length += 1;  //计算字段realTotalFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dealAdjustFee的长度 size_of(uint32_t)
				length += 1;  //计算字段dealAdjustFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段cftFee的长度 size_of(uint32_t)
				length += 1;  //计算字段cftFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段companyFee的长度 size_of(uint32_t)
				length += 1;  //计算字段companyFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段paipaiFee的长度 size_of(uint32_t)
				length += 1;  //计算字段paipaiFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段prepayMode的长度 size_of(uint32_t)
				length += 1;  //计算字段prepayMode_u的长度 size_of(uint8_t)
				length += 4;  //计算字段deliveryFee的长度 size_of(uint32_t)
				length += 1;  //计算字段deliveryFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段companyReturnFee的长度 size_of(uint32_t)
				length += 1;  //计算字段companyReturnFee_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(reserveIn, null);  //计算字段reserveIn的长度 size_of(String)
				length += 1;  //计算字段reserveIn_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(CODFee)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段innerCompanyId的长度 size_of(uint32_t)
				length += 1;  //计算字段innerCompanyId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段countTotalFee的长度 size_of(uint32_t)
				length += 1;  //计算字段countTotalFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段realTotalFee的长度 size_of(uint32_t)
				length += 1;  //计算字段realTotalFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dealAdjustFee的长度 size_of(uint32_t)
				length += 1;  //计算字段dealAdjustFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段cftFee的长度 size_of(uint32_t)
				length += 1;  //计算字段cftFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段companyFee的长度 size_of(uint32_t)
				length += 1;  //计算字段companyFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段paipaiFee的长度 size_of(uint32_t)
				length += 1;  //计算字段paipaiFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段prepayMode的长度 size_of(uint32_t)
				length += 1;  //计算字段prepayMode_u的长度 size_of(uint8_t)
				length += 4;  //计算字段deliveryFee的长度 size_of(uint32_t)
				length += 1;  //计算字段deliveryFee_u的长度 size_of(uint8_t)
				length += 4;  //计算字段companyReturnFee的长度 size_of(uint32_t)
				length += 1;  //计算字段companyReturnFee_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(reserveIn, encoding);  //计算字段reserveIn的长度 size_of(String)
				length += 1;  //计算字段reserveIn_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
