 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.CodConfig.java

package com.qq.qqbuy.thirdparty.idl.codFee.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 * 获取快递公司货到付款配置响应
 *
 *@date 2013-08-16 06:07:21
 *
 *@since version:0
*/
public class  GetCompanyConfigResp implements IServiceObject
{
	public long result;
	/**
	 * 响应
	 *
	 * 版本 >= 0
	 */
	 private CODCompanyConfig config = new CODCompanyConfig();

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(config);
		bs.pushString(errMsg);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		config = (CODCompanyConfig) bs.popObject(CODCompanyConfig.class);
		errMsg = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x63458801L;
	}


	/**
	 * 获取响应
	 * 
	 * 此字段的版本 >= 0
	 * @return config value 类型为:CODCompanyConfig
	 * 
	 */
	public CODCompanyConfig getConfig()
	{
		return config;
	}


	/**
	 * 设置响应
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:CODCompanyConfig
	 * 
	 */
	public void setConfig(CODCompanyConfig value)
	{
		if (value != null) {
				this.config = value;
		}else{
				this.config = new CODCompanyConfig();
		}
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetCompanyConfigResp)
				length += ByteStream.getObjectSize(config, null);  //计算字段config的长度 size_of(CODCompanyConfig)
				length += ByteStream.getObjectSize(errMsg, null);  //计算字段errMsg的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetCompanyConfigResp)
				length += ByteStream.getObjectSize(config, encoding);  //计算字段config的长度 size_of(CODCompanyConfig)
				length += ByteStream.getObjectSize(errMsg, encoding);  //计算字段errMsg的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
