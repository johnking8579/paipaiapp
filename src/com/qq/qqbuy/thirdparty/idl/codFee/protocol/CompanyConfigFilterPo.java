//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.CodConfig.java

package com.qq.qqbuy.thirdparty.idl.codFee.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

/**
 * 获取快递公司信息请求参数
 *
 *@date 2013-08-16 06:07:21
 *
 *@since version:0
*/
public class CompanyConfigFilterPo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 内部ID
	 *
	 * 版本 >= 0
	 */
	 private long innerCompanyId;

	/**
	 * 版本 >= 0
	 */
	 private short innerCompanyId_u;

	/**
	 * 保留参数
	 *
	 * 版本 >= 0
	 */
	 private String reserveIn = new String();

	/**
	 * 版本 >= 0
	 */
	 private short reserveIn_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushUInt(innerCompanyId);
		bs.pushUByte(innerCompanyId_u);
		bs.pushString(reserveIn);
		bs.pushUByte(reserveIn_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		innerCompanyId = bs.popUInt();
		innerCompanyId_u = bs.popUByte();
		reserveIn = bs.popString();
		reserveIn_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取内部ID
	 * 
	 * 此字段的版本 >= 0
	 * @return innerCompanyId value 类型为:long
	 * 
	 */
	public long getInnerCompanyId()
	{
		return innerCompanyId;
	}


	/**
	 * 设置内部ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setInnerCompanyId(long value)
	{
		this.innerCompanyId = value;
		this.innerCompanyId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return innerCompanyId_u value 类型为:short
	 * 
	 */
	public short getInnerCompanyId_u()
	{
		return innerCompanyId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setInnerCompanyId_u(short value)
	{
		this.innerCompanyId_u = value;
	}


	/**
	 * 获取保留参数
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveIn value 类型为:String
	 * 
	 */
	public String getReserveIn()
	{
		return reserveIn;
	}


	/**
	 * 设置保留参数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveIn(String value)
	{
		this.reserveIn = value;
		this.reserveIn_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveIn_u value 类型为:short
	 * 
	 */
	public short getReserveIn_u()
	{
		return reserveIn_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserveIn_u(short value)
	{
		this.reserveIn_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CompanyConfigFilterPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段innerCompanyId的长度 size_of(uint32_t)
				length += 1;  //计算字段innerCompanyId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(reserveIn, null);  //计算字段reserveIn的长度 size_of(String)
				length += 1;  //计算字段reserveIn_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(CompanyConfigFilterPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段innerCompanyId的长度 size_of(uint32_t)
				length += 1;  //计算字段innerCompanyId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(reserveIn, encoding);  //计算字段reserveIn的长度 size_of(String)
				length += 1;  //计算字段reserveIn_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
