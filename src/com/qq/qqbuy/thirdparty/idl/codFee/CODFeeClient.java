package com.qq.qqbuy.thirdparty.idl.codFee;

import com.paipai.component.c2cplatform.IAsynWebStub;

import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.codFee.protocol.CODFeeFilter;
import com.qq.qqbuy.thirdparty.idl.codFee.protocol.GetTotalFeeReq;
import com.qq.qqbuy.thirdparty.idl.codFee.protocol.GetTotalFeeResp;

public class CODFeeClient extends SupportIDLBaseClient {

	public static GetTotalFeeResp getCodFee(int innerCompanyId, int whoPayCodFee, long dealTotalFee) {
		long start = System.currentTimeMillis();
		GetTotalFeeReq req = new GetTotalFeeReq();
		CODFeeFilter filter = new CODFeeFilter();
		filter.setInnerCompanyId(innerCompanyId);
		filter.setSellerPayCod(whoPayCodFee);
		filter.setDealTotalFee(dealTotalFee);
		req.setReqData(filter);
		
		GetTotalFeeResp resp = new GetTotalFeeResp();
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		int ret = -1;
		ret = invokePaiPaiIDL(req, resp, stub);
        return ret == SUCCESS ? resp : null;
	}
	
	public static void main(String args[]) {
		int innerCompanyId = 1; // 1宅急送  2顺丰
		int whoPayCodFee = 1; // 1卖家承担服务费  0买家承担服务费
		long dealTotalFee = 14900; //
		GetTotalFeeResp resp = getCodFee(innerCompanyId, whoPayCodFee, dealTotalFee);
		System.out.println(resp);
	}
}
