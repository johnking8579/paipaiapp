 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: source.VerifyCodeAo.java

package com.qq.qqbuy.thirdparty.idl.sms.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *judge if has passed the mobile verify process Resp
 *
 *@date 2014-11-17 06:08:50
 *
 *@since version:0
*/
public class  JudgeVerifyCodeResp extends NetMessage
{
	/**
	 * verify result, 0-not define, 1-not pass, 2-passed
	 *
	 * 版本 >= 0
	 */
	 private short verifyResult;

	/**
	 * error message
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * reserve resp
	 *
	 * 版本 >= 0
	 */
	 private String outReserve = new String();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushUByte(verifyResult);
		bs.pushString(errMsg);
		bs.pushString(outReserve);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		verifyResult = bs.popUByte();
		errMsg = bs.popString();
		outReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x10558801L;
	}


	/**
	 * 获取verify result, 0-not define, 1-not pass, 2-passed
	 * 
	 * 此字段的版本 >= 0
	 * @return verifyResult value 类型为:short
	 * 
	 */
	public short getVerifyResult()
	{
		return verifyResult;
	}


	/**
	 * 设置verify result, 0-not define, 1-not pass, 2-passed
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVerifyResult(short value)
	{
		this.verifyResult = value;
	}


	/**
	 * 获取error message
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置error message
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	/**
	 * 获取reserve resp
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return outReserve;
	}


	/**
	 * 设置reserve resp
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		this.outReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(JudgeVerifyCodeResp)
				length += 1;  //计算字段verifyResult的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserve);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
