

//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang

package com.qq.qqbuy.thirdparty.idl.sms.protocol;


import com.paipai.util.annotation.Protocol;
import com.paipai.netframework.kernal.NetAction;


import com.paipai.lang.GenericWrapper;/**
 *
 *NetAction类
 *
 */
public class VerifyCodeAo  extends NetAction
{
	@Override
	 public int getSnowslideThresold(){
		// 这里设置防雪崩时间，也就是超时时间值
		  return 2;
	 }




	@Protocol(cmdid = 0x10551801L, desc = "judge if has passed the mobile verify process", export = true)
	 public JudgeVerifyCodeResp JudgeVerifyCode(JudgeVerifyCodeReq req){
		JudgeVerifyCodeResp resp = new  JudgeVerifyCodeResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x10551802L, desc = "create verify code and send short message", export = true)
	 public CreateVerifyCodeAndSendSmsResp CreateVerifyCodeAndSendSms(CreateVerifyCodeAndSendSmsReq req){
		CreateVerifyCodeAndSendSmsResp resp = new  CreateVerifyCodeAndSendSmsResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }



}
