 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: source.VerifyCodeAo.java

package com.qq.qqbuy.thirdparty.idl.sms.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *judge if has passed the mobile verify process Req
 *
 *@date 2014-11-17 06:08:50
 *
 *@since version:0
*/
public class  JudgeVerifyCodeReq extends NetMessage
{
	/**
	 * macheine key
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * source
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * uin
	 *
	 * 版本 >= 0
	 */
	 private long uin;

	/**
	 * mobile number
	 *
	 * 版本 >= 0
	 */
	 private String mobile = new String();

	/**
	 * verify code, set null if have no value
	 *
	 * 版本 >= 0
	 */
	 private String verifyCode = new String();

	/**
	 * reserve req
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(machineKey);
		bs.pushString(source);
		bs.pushUInt(uin);
		bs.pushString(mobile);
		bs.pushString(verifyCode);
		bs.pushString(inReserve);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		machineKey = bs.popString();
		source = bs.popString();
		uin = bs.popUInt();
		mobile = bs.popString();
		verifyCode = bs.popString();
		inReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x10551801L;
	}


	/**
	 * 获取macheine key
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置macheine key
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取source
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置source
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取uin
	 * 
	 * 此字段的版本 >= 0
	 * @return uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return uin;
	}


	/**
	 * 设置uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.uin = value;
	}


	/**
	 * 获取mobile number
	 * 
	 * 此字段的版本 >= 0
	 * @return mobile value 类型为:String
	 * 
	 */
	public String getMobile()
	{
		return mobile;
	}


	/**
	 * 设置mobile number
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMobile(String value)
	{
		this.mobile = value;
	}


	/**
	 * 获取verify code, set null if have no value
	 * 
	 * 此字段的版本 >= 0
	 * @return verifyCode value 类型为:String
	 * 
	 */
	public String getVerifyCode()
	{
		return verifyCode;
	}


	/**
	 * 设置verify code, set null if have no value
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setVerifyCode(String value)
	{
		this.verifyCode = value;
	}


	/**
	 * 获取reserve req
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置reserve req
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(JudgeVerifyCodeReq)
				length += ByteStream.getObjectSize(machineKey);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += 4;  //计算字段uin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(mobile);  //计算字段mobile的长度 size_of(String)
				length += ByteStream.getObjectSize(verifyCode);  //计算字段verifyCode的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
