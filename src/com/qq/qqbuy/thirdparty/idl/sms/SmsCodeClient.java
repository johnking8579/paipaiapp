package com.qq.qqbuy.thirdparty.idl.sms;

import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.exception.ExternalInterfaceException;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.sms.protocol.CreateVerifyCodeAndSendSmsReq;
import com.qq.qqbuy.thirdparty.idl.sms.protocol.CreateVerifyCodeAndSendSmsResp;
import com.qq.qqbuy.thirdparty.idl.sms.protocol.JudgeVerifyCodeReq;
import com.qq.qqbuy.thirdparty.idl.sms.protocol.JudgeVerifyCodeResp;

public class SmsCodeClient {
	public static final String
		SOURCE_确认收货 = "2_1",
		SOURCE_申请退款 = "2_2";

	/**
	 * response.result取值:
	 * 		1092 代表"the verify code is in valid period, no need create verify code.",频繁调用时会出现.
	 * 		14: 通用错误码,参数失败, 比如号码不对
	 * 
	 * @param uin
	 * @param mobileNo
	 * @param inreserve 1是否总是要验证, 2要验证. ???不明
	 * @param source 2_1代表确认收货, 2_2代表申请退款
	 * @param skey
	 * @return
	 */
	public void send(long uin, String mobileNo, String source, String mk, String skey)	{
		CreateVerifyCodeAndSendSmsReq req = new CreateVerifyCodeAndSendSmsReq();
		CreateVerifyCodeAndSendSmsResp resp = new CreateVerifyCodeAndSendSmsResp();
		
		req.setInReserve("1");
		req.setMobile(mobileNo);
		req.setSource(source);
		req.setUin(uin);
		req.setMachineKey(mk);
		
		AsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		stub.setOperator(uin);
		stub.setSkey(skey.getBytes());
		
		if(EnvManager.isNotIdc())	{
			stub.setIpAndPort("10.136.10.162", 53101);
		}
		
		SupportIDLBaseClient.invoke(stub, req, resp);
		if(resp.getResult() != 0)	{
			throw new ExternalInterfaceException(0, resp.getResult(), resp.getErrMsg());
		}
	}
	
	
	/**
	 * 校验手机验证码, 不需要登录态.
	 * 对IDL端错误码进行封装, 成功时无任何返回值, 失败时抛出各种异常
	 * result=1089, "errMsg":"the verify code is no longer valid.
	 * verifyResult, 0-not define, 1-not pass, 2-passed
	 * @param uin
	 * @param mobileNo
	 * @param inputCode
	 * @param source 2_1代表确认收货, 2_2代表申请退款
	 * @throws ExternalInterfaceException
	 */
	public void judge(long uin, String mobileNo, String inputCode, String source, String mk)	{
		JudgeVerifyCodeReq req = new JudgeVerifyCodeReq();
		JudgeVerifyCodeResp resp = new JudgeVerifyCodeResp();
		req.setInReserve("1");
		req.setMobile(mobileNo);
		req.setSource(source);
		req.setUin(uin);
		req.setVerifyCode(inputCode);
		req.setMachineKey(mk);
		
		AsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		stub.setOperator(uin);
		
		if(EnvManager.isNotIdc())	{
			stub.setIpAndPort("10.136.10.162", 53101);
		}
		
		SupportIDLBaseClient.invoke(stub, req, resp);
		if(resp.getResult() != 0)	
			throw new ExternalInterfaceException(0, resp.getResult(), resp.getErrMsg());
		switch ((int)resp.getVerifyResult()){
			case 0:				//不明白0是什么含义,自定义一个错误码
				throw new ExternalInterfaceException(0, 10890, "验证码未定义");	
			case 1:
				throw new ExternalInterfaceException(0, 10891, "验证码错误");
			case 2:
				break;
			default:
				throw new ExternalInterfaceException(0, resp.getVerifyResult(), resp.getErrMsg());	
		}
	}

}
