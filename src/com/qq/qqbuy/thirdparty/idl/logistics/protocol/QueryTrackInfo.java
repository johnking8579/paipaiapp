

//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang

package com.qq.qqbuy.thirdparty.idl.logistics.protocol;


import com.paipai.util.annotation.Protocol;
import com.paipai.netframework.kernal.NetAction;


import com.paipai.lang.GenericWrapper;/**
 *
 *NetAction类
 *
 */
public class QueryTrackInfo  extends NetAction
{
	@Override
	 public int getSnowslideThresold(){
		// 这里设置防雪崩时间，也就是超时时间值
		  return 2;
	 }




	@Protocol(cmdid = 0x1fb31809L, desc = "查物流跟踪信息", export = true)
	 public RetrieveTrackInfoResp RetrieveTrackInfo(RetrieveTrackInfoReq req){
		RetrieveTrackInfoResp resp = new  RetrieveTrackInfoResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x1fb31802L, desc = "获取查件记录列表 req", export = true)
	 public GetQueryHistoryListResp GetQueryHistoryList(GetQueryHistoryListReq req){
		GetQueryHistoryListResp resp = new  GetQueryHistoryListResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x1fb31803L, desc = "删除一条查件记录", export = true)
	 public DelQueryHistoryResp DelQueryHistory(DelQueryHistoryReq req){
		DelQueryHistoryResp resp = new  DelQueryHistoryResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }



}
