//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.logistics.protocol.QueryTrackInfo.java

package com.qq.qqbuy.thirdparty.idl.logistics.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *物流跟踪信息表DO
 *
 *@date 2014-12-19 11:53:14
 *
 *@since version:0
*/
public class TraceInfoDo  implements ICanSerializeObject
{
	/**
	 * id
	 *
	 * 版本 >= 0
	 */
	 private long Id;

	/**
	 * 运单号
	 *
	 * 版本 >= 0
	 */
	 private String DeliverId = new String();

	/**
	 * 用户帐号,选填，40字符内
	 *
	 * 版本 >= 0
	 */
	 private String UserId = new String();

	/**
	 * 物流公司编码
	 *
	 * 版本 >= 0
	 */
	 private String CompanyCode = new String();

	/**
	 * 更新次数
	 *
	 * 版本 >= 0
	 */
	 private long Count;

	/**
	 * 物流跟踪信息
	 *
	 * 版本 >= 0
	 */
	 private String Trace_info = new String();

	/**
	 * 创建时间
	 *
	 * 版本 >= 0
	 */
	 private String Createtime = new String();

	/**
	 * 更新时间
	 *
	 * 版本 >= 0
	 */
	 private String Updatetime = new String();

	/**
	 * 速递单号
	 *
	 * 版本 >= 0
	 */
	 private long Listid;

	/**
	 * 是否绑定速递单号和运单号
	 *
	 * 版本 >= 0
	 */
	 private long Bind;

	/**
	 * 备注或出错信息
	 *
	 * 版本 >= 0
	 */
	 private String Remark = new String();

	/**
	 * 保留字段1
	 *
	 * 版本 >= 0
	 */
	 private String Standby1 = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushLong(Id);
		bs.pushString(DeliverId);
		bs.pushString(UserId);
		bs.pushString(CompanyCode);
		bs.pushUInt(Count);
		bs.pushString(Trace_info);
		bs.pushString(Createtime);
		bs.pushString(Updatetime);
		bs.pushLong(Listid);
		bs.pushUInt(Bind);
		bs.pushString(Remark);
		bs.pushString(Standby1);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Id = bs.popLong();
		DeliverId = bs.popString();
		UserId = bs.popString();
		CompanyCode = bs.popString();
		Count = bs.popUInt();
		Trace_info = bs.popString();
		Createtime = bs.popString();
		Updatetime = bs.popString();
		Listid = bs.popLong();
		Bind = bs.popUInt();
		Remark = bs.popString();
		Standby1 = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取id
	 * 
	 * 此字段的版本 >= 0
	 * @return Id value 类型为:long
	 * 
	 */
	public long getId()
	{
		return Id;
	}


	/**
	 * 设置id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setId(long value)
	{
		this.Id = value;
	}


	/**
	 * 获取运单号
	 * 
	 * 此字段的版本 >= 0
	 * @return DeliverId value 类型为:String
	 * 
	 */
	public String getDeliverId()
	{
		return DeliverId;
	}


	/**
	 * 设置运单号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDeliverId(String value)
	{
		this.DeliverId = value;
	}


	/**
	 * 获取用户帐号,选填，40字符内
	 * 
	 * 此字段的版本 >= 0
	 * @return UserId value 类型为:String
	 * 
	 */
	public String getUserId()
	{
		return UserId;
	}


	/**
	 * 设置用户帐号,选填，40字符内
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setUserId(String value)
	{
		this.UserId = value;
	}


	/**
	 * 获取物流公司编码
	 * 
	 * 此字段的版本 >= 0
	 * @return CompanyCode value 类型为:String
	 * 
	 */
	public String getCompanyCode()
	{
		return CompanyCode;
	}


	/**
	 * 设置物流公司编码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCompanyCode(String value)
	{
		this.CompanyCode = value;
	}


	/**
	 * 获取更新次数
	 * 
	 * 此字段的版本 >= 0
	 * @return Count value 类型为:long
	 * 
	 */
	public long getCount()
	{
		return Count;
	}


	/**
	 * 设置更新次数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCount(long value)
	{
		this.Count = value;
	}


	/**
	 * 获取物流跟踪信息
	 * 
	 * 此字段的版本 >= 0
	 * @return Trace_info value 类型为:String
	 * 
	 */
	public String getTrace_info()
	{
		return Trace_info;
	}


	/**
	 * 设置物流跟踪信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setTrace_info(String value)
	{
		this.Trace_info = value;
	}


	/**
	 * 获取创建时间
	 * 
	 * 此字段的版本 >= 0
	 * @return Createtime value 类型为:String
	 * 
	 */
	public String getCreatetime()
	{
		return Createtime;
	}


	/**
	 * 设置创建时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCreatetime(String value)
	{
		this.Createtime = value;
	}


	/**
	 * 获取更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return Updatetime value 类型为:String
	 * 
	 */
	public String getUpdatetime()
	{
		return Updatetime;
	}


	/**
	 * 设置更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setUpdatetime(String value)
	{
		this.Updatetime = value;
	}


	/**
	 * 获取速递单号
	 * 
	 * 此字段的版本 >= 0
	 * @return Listid value 类型为:long
	 * 
	 */
	public long getListid()
	{
		return Listid;
	}


	/**
	 * 设置速递单号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setListid(long value)
	{
		this.Listid = value;
	}


	/**
	 * 获取是否绑定速递单号和运单号
	 * 
	 * 此字段的版本 >= 0
	 * @return Bind value 类型为:long
	 * 
	 */
	public long getBind()
	{
		return Bind;
	}


	/**
	 * 设置是否绑定速递单号和运单号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBind(long value)
	{
		this.Bind = value;
	}


	/**
	 * 获取备注或出错信息
	 * 
	 * 此字段的版本 >= 0
	 * @return Remark value 类型为:String
	 * 
	 */
	public String getRemark()
	{
		return Remark;
	}


	/**
	 * 设置备注或出错信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRemark(String value)
	{
		this.Remark = value;
	}


	/**
	 * 获取保留字段1
	 * 
	 * 此字段的版本 >= 0
	 * @return Standby1 value 类型为:String
	 * 
	 */
	public String getStandby1()
	{
		return Standby1;
	}


	/**
	 * 设置保留字段1
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStandby1(String value)
	{
		this.Standby1 = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(TraceInfoDo)
				length += 17;  //计算字段Id的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(DeliverId);  //计算字段DeliverId的长度 size_of(String)
				length += ByteStream.getObjectSize(UserId);  //计算字段UserId的长度 size_of(String)
				length += ByteStream.getObjectSize(CompanyCode);  //计算字段CompanyCode的长度 size_of(String)
				length += 4;  //计算字段Count的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(Trace_info);  //计算字段Trace_info的长度 size_of(String)
				length += ByteStream.getObjectSize(Createtime);  //计算字段Createtime的长度 size_of(String)
				length += ByteStream.getObjectSize(Updatetime);  //计算字段Updatetime的长度 size_of(String)
				length += 17;  //计算字段Listid的长度 size_of(uint64_t)
				length += 4;  //计算字段Bind的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(Remark);  //计算字段Remark的长度 size_of(String)
				length += ByteStream.getObjectSize(Standby1);  //计算字段Standby1的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
