 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.logistics.protocol.QueryTrackInfo.java

package com.qq.qqbuy.thirdparty.idl.logistics.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *获取查件记录列表 req
 *
 *@date 2014-12-19 11:53:14
 *
 *@since version:0
*/
public class  GetQueryHistoryListReq extends NetMessage
{
	/**
	 * 用户帐号,选填，40字符内
	 *
	 * 版本 >= 0
	 */
	 private String UserId = new String();

	/**
	 * page index
	 *
	 * 版本 >= 0
	 */
	 private long PageIndex;

	/**
	 * page size
	 *
	 * 版本 >= 0
	 */
	 private long PageSize;

	/**
	 * 调用者来源
	 *
	 * 版本 >= 0
	 */
	 private short Source;


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(UserId);
		bs.pushUInt(PageIndex);
		bs.pushUInt(PageSize);
		bs.pushUByte(Source);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		UserId = bs.popString();
		PageIndex = bs.popUInt();
		PageSize = bs.popUInt();
		Source = bs.popUByte();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x1fb31802L;
	}


	/**
	 * 获取用户帐号,选填，40字符内
	 * 
	 * 此字段的版本 >= 0
	 * @return UserId value 类型为:String
	 * 
	 */
	public String getUserId()
	{
		return UserId;
	}


	/**
	 * 设置用户帐号,选填，40字符内
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setUserId(String value)
	{
		this.UserId = value;
	}


	/**
	 * 获取page index
	 * 
	 * 此字段的版本 >= 0
	 * @return PageIndex value 类型为:long
	 * 
	 */
	public long getPageIndex()
	{
		return PageIndex;
	}


	/**
	 * 设置page index
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPageIndex(long value)
	{
		this.PageIndex = value;
	}


	/**
	 * 获取page size
	 * 
	 * 此字段的版本 >= 0
	 * @return PageSize value 类型为:long
	 * 
	 */
	public long getPageSize()
	{
		return PageSize;
	}


	/**
	 * 设置page size
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPageSize(long value)
	{
		this.PageSize = value;
	}


	/**
	 * 获取调用者来源
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:short
	 * 
	 */
	public short getSource()
	{
		return Source;
	}


	/**
	 * 设置调用者来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSource(short value)
	{
		this.Source = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetQueryHistoryListReq)
				length += ByteStream.getObjectSize(UserId);  //计算字段UserId的长度 size_of(String)
				length += 4;  //计算字段PageIndex的长度 size_of(uint32_t)
				length += 4;  //计算字段PageSize的长度 size_of(uint32_t)
				length += 1;  //计算字段Source的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
