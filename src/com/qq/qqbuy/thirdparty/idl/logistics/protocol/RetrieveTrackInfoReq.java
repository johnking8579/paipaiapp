 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.logistics.protocol.QueryTrackInfo.java

package com.qq.qqbuy.thirdparty.idl.logistics.protocol;


import com.paipai.netframework.kernal.NetMessage;
import com.paipai.util.io.ByteStream;

/**
 *查物流跟踪信息 req
 *
 *@date 2014-12-19 11:53:14
 *
 *@since version:0
*/
public class  RetrieveTrackInfoReq extends NetMessage
{
	/**
	 * 用户帐号,选填，40字符内
	 *
	 * 版本 >= 0
	 */
	 private String UserId = new String();

	/**
	 * 快递运单号，必填
	 *
	 * 版本 >= 0
	 */
	 private String DeliverId = new String();

	/**
	 * 快递公司编码,必填
	 *
	 * 版本 >= 0
	 */
	 private String CompanyCode = new String();

	/**
	 * 速递订单Id，选填
	 *
	 * 版本 >= 0
	 */
	 private long SudiOrderId;

	/**
	 * 调用者来源，必填。新调用方请联系我们分配id号：用于区分不同平台数据来源0 - all全部平台适用1 - QQ网购2 -拍拍3 -速递4 -微信5 - soso6 - QQ团购7 - 高朋8 - QQ腾爱
	 *
	 * 版本 >= 0
	 */
	 private short Source;

	/**
	 * 调用方提供的业务唯一性识别码。选填，比如网购查询，可使用网购订单id做标识码； 如果前端页面查询，可使用页面唯一用户标识uid
	 *
	 * 版本 >= 0
	 */
	 private String BizId = new String();

	/**
	 * 发货地址，选填，统计使用
	 *
	 * 版本 >= 0
	 */
	 private SimpleAddressBo SenderAddr = new SimpleAddressBo();

	/**
	 * 收货地址，选填，统计使用
	 *
	 * 版本 >= 0
	 */
	 private SimpleAddressBo ReceiverAddr = new SimpleAddressBo();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(UserId);
		bs.pushString(DeliverId);
		bs.pushString(CompanyCode);
		bs.pushLong(SudiOrderId);
		bs.pushUByte(Source);
		bs.pushString(BizId);
		bs.pushObject(SenderAddr);
		bs.pushObject(ReceiverAddr);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		UserId = bs.popString();
		DeliverId = bs.popString();
		CompanyCode = bs.popString();
		SudiOrderId = bs.popLong();
		Source = bs.popUByte();
		BizId = bs.popString();
		SenderAddr = (SimpleAddressBo) bs.popObject(SimpleAddressBo.class);
		ReceiverAddr = (SimpleAddressBo) bs.popObject(SimpleAddressBo.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x1fb31809L;
	}


	/**
	 * 获取用户帐号,选填，40字符内
	 * 
	 * 此字段的版本 >= 0
	 * @return UserId value 类型为:String
	 * 
	 */
	public String getUserId()
	{
		return UserId;
	}


	/**
	 * 设置用户帐号,选填，40字符内
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setUserId(String value)
	{
		this.UserId = value;
	}


	/**
	 * 获取快递运单号，必填
	 * 
	 * 此字段的版本 >= 0
	 * @return DeliverId value 类型为:String
	 * 
	 */
	public String getDeliverId()
	{
		return DeliverId;
	}


	/**
	 * 设置快递运单号，必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDeliverId(String value)
	{
		this.DeliverId = value;
	}


	/**
	 * 获取快递公司编码,必填
	 * 
	 * 此字段的版本 >= 0
	 * @return CompanyCode value 类型为:String
	 * 
	 */
	public String getCompanyCode()
	{
		return CompanyCode;
	}


	/**
	 * 设置快递公司编码,必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCompanyCode(String value)
	{
		this.CompanyCode = value;
	}


	/**
	 * 获取速递订单Id，选填
	 * 
	 * 此字段的版本 >= 0
	 * @return SudiOrderId value 类型为:long
	 * 
	 */
	public long getSudiOrderId()
	{
		return SudiOrderId;
	}


	/**
	 * 设置速递订单Id，选填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSudiOrderId(long value)
	{
		this.SudiOrderId = value;
	}


	/**
	 * 获取调用者来源，必填。新调用方请联系我们分配id号：用于区分不同平台数据来源0 - all全部平台适用1 - QQ网购2 -拍拍3 -速递4 -微信5 - soso6 - QQ团购7 - 高朋8 - QQ腾爱
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:short
	 * 
	 */
	public short getSource()
	{
		return Source;
	}


	/**
	 * 设置调用者来源，必填。新调用方请联系我们分配id号：用于区分不同平台数据来源0 - all全部平台适用1 - QQ网购2 -拍拍3 -速递4 -微信5 - soso6 - QQ团购7 - 高朋8 - QQ腾爱
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSource(short value)
	{
		this.Source = value;
	}


	/**
	 * 获取调用方提供的业务唯一性识别码。选填，比如网购查询，可使用网购订单id做标识码； 如果前端页面查询，可使用页面唯一用户标识uid
	 * 
	 * 此字段的版本 >= 0
	 * @return BizId value 类型为:String
	 * 
	 */
	public String getBizId()
	{
		return BizId;
	}


	/**
	 * 设置调用方提供的业务唯一性识别码。选填，比如网购查询，可使用网购订单id做标识码； 如果前端页面查询，可使用页面唯一用户标识uid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBizId(String value)
	{
		this.BizId = value;
	}


	/**
	 * 获取发货地址，选填，统计使用
	 * 
	 * 此字段的版本 >= 0
	 * @return SenderAddr value 类型为:SimpleAddressBo
	 * 
	 */
	public SimpleAddressBo getSenderAddr()
	{
		return SenderAddr;
	}


	/**
	 * 设置发货地址，选填，统计使用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:SimpleAddressBo
	 * 
	 */
	public void setSenderAddr(SimpleAddressBo value)
	{
		if (value != null) {
				this.SenderAddr = value;
		}else{
				this.SenderAddr = new SimpleAddressBo();
		}
	}


	/**
	 * 获取收货地址，选填，统计使用
	 * 
	 * 此字段的版本 >= 0
	 * @return ReceiverAddr value 类型为:SimpleAddressBo
	 * 
	 */
	public SimpleAddressBo getReceiverAddr()
	{
		return ReceiverAddr;
	}


	/**
	 * 设置收货地址，选填，统计使用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:SimpleAddressBo
	 * 
	 */
	public void setReceiverAddr(SimpleAddressBo value)
	{
		if (value != null) {
				this.ReceiverAddr = value;
		}else{
				this.ReceiverAddr = new SimpleAddressBo();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(RetrieveTrackInfoReq)
				length += ByteStream.getObjectSize(UserId);  //计算字段UserId的长度 size_of(String)
				length += ByteStream.getObjectSize(DeliverId);  //计算字段DeliverId的长度 size_of(String)
				length += ByteStream.getObjectSize(CompanyCode);  //计算字段CompanyCode的长度 size_of(String)
				length += 17;  //计算字段SudiOrderId的长度 size_of(uint64_t)
				length += 1;  //计算字段Source的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(BizId);  //计算字段BizId的长度 size_of(String)
				length += ByteStream.getObjectSize(SenderAddr);  //计算字段SenderAddr的长度 size_of(SimpleAddressBo)
				length += ByteStream.getObjectSize(ReceiverAddr);  //计算字段ReceiverAddr的长度 size_of(SimpleAddressBo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
