 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.logistics.protocol.QueryTrackInfo.java

package com.qq.qqbuy.thirdparty.idl.logistics.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import java.util.Vector;

/**
 *获取查件记录列表 resp
 *
 *@date 2014-12-19 11:53:14
 *
 *@since version:0
*/
public class  GetQueryHistoryListResp extends NetMessage
{
	/**
	 * 查件记录列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<QueryHistoryDo> QueryHistoryDoList = new Vector<QueryHistoryDo>();

	/**
	 * 记录总数，用于前端分页
	 *
	 * 版本 >= 0
	 */
	 private long TotalSize;

	/**
	 * 用于存储debug信息
	 *
	 * 版本 >= 0
	 */
	 private String RetMsg = new String();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(QueryHistoryDoList);
		bs.pushUInt(TotalSize);
		bs.pushString(RetMsg);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		QueryHistoryDoList = (Vector<QueryHistoryDo>)bs.popVector(QueryHistoryDo.class);
		TotalSize = bs.popUInt();
		RetMsg = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x1fb38802L;
	}


	/**
	 * 获取查件记录列表
	 * 
	 * 此字段的版本 >= 0
	 * @return QueryHistoryDoList value 类型为:Vector<QueryHistoryDo>
	 * 
	 */
	public Vector<QueryHistoryDo> getQueryHistoryDoList()
	{
		return QueryHistoryDoList;
	}


	/**
	 * 设置查件记录列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<QueryHistoryDo>
	 * 
	 */
	public void setQueryHistoryDoList(Vector<QueryHistoryDo> value)
	{
		if (value != null) {
				this.QueryHistoryDoList = value;
		}else{
				this.QueryHistoryDoList = new Vector<QueryHistoryDo>();
		}
	}


	/**
	 * 获取记录总数，用于前端分页
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalSize value 类型为:long
	 * 
	 */
	public long getTotalSize()
	{
		return TotalSize;
	}


	/**
	 * 设置记录总数，用于前端分页
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalSize(long value)
	{
		this.TotalSize = value;
	}


	/**
	 * 获取用于存储debug信息
	 * 
	 * 此字段的版本 >= 0
	 * @return RetMsg value 类型为:String
	 * 
	 */
	public String getRetMsg()
	{
		return RetMsg;
	}


	/**
	 * 设置用于存储debug信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRetMsg(String value)
	{
		this.RetMsg = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetQueryHistoryListResp)
				length += ByteStream.getObjectSize(QueryHistoryDoList);  //计算字段QueryHistoryDoList的长度 size_of(Vector)
				length += 4;  //计算字段TotalSize的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(RetMsg);  //计算字段RetMsg的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
