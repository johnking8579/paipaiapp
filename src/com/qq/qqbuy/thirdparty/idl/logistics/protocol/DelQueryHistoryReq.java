 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.logistics.protocol.QueryTrackInfo.java

package com.qq.qqbuy.thirdparty.idl.logistics.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *删除一条查件记录 req
 *
 *@date 2014-12-19 11:53:14
 *
 *@since version:0
*/
public class  DelQueryHistoryReq extends NetMessage
{
	/**
	 * 用户帐号,选填，40字符内
	 *
	 * 版本 >= 0
	 */
	 private String UserId = new String();

	/**
	 * 快递运单号，必填
	 *
	 * 版本 >= 0
	 */
	 private String DeliverId = new String();

	/**
	 * 快递公司编码,必填
	 *
	 * 版本 >= 0
	 */
	 private String CompanyCode = new String();

	/**
	 * 调用者来源
	 *
	 * 版本 >= 0
	 */
	 private short Source;


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(UserId);
		bs.pushString(DeliverId);
		bs.pushString(CompanyCode);
		bs.pushUByte(Source);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		UserId = bs.popString();
		DeliverId = bs.popString();
		CompanyCode = bs.popString();
		Source = bs.popUByte();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x1fb31803L;
	}


	/**
	 * 获取用户帐号,选填，40字符内
	 * 
	 * 此字段的版本 >= 0
	 * @return UserId value 类型为:String
	 * 
	 */
	public String getUserId()
	{
		return UserId;
	}


	/**
	 * 设置用户帐号,选填，40字符内
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setUserId(String value)
	{
		this.UserId = value;
	}


	/**
	 * 获取快递运单号，必填
	 * 
	 * 此字段的版本 >= 0
	 * @return DeliverId value 类型为:String
	 * 
	 */
	public String getDeliverId()
	{
		return DeliverId;
	}


	/**
	 * 设置快递运单号，必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDeliverId(String value)
	{
		this.DeliverId = value;
	}


	/**
	 * 获取快递公司编码,必填
	 * 
	 * 此字段的版本 >= 0
	 * @return CompanyCode value 类型为:String
	 * 
	 */
	public String getCompanyCode()
	{
		return CompanyCode;
	}


	/**
	 * 设置快递公司编码,必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCompanyCode(String value)
	{
		this.CompanyCode = value;
	}


	/**
	 * 获取调用者来源
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:short
	 * 
	 */
	public short getSource()
	{
		return Source;
	}


	/**
	 * 设置调用者来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSource(short value)
	{
		this.Source = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(DelQueryHistoryReq)
				length += ByteStream.getObjectSize(UserId);  //计算字段UserId的长度 size_of(String)
				length += ByteStream.getObjectSize(DeliverId);  //计算字段DeliverId的长度 size_of(String)
				length += ByteStream.getObjectSize(CompanyCode);  //计算字段CompanyCode的长度 size_of(String)
				length += 1;  //计算字段Source的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
