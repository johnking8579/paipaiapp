//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.logistics.protocol.QueryTrackInfo.java

package com.qq.qqbuy.thirdparty.idl.logistics.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *物流跟踪信息表DO
 *
 *@date 2014-12-19 11:53:13
 *
 *@since version:0
*/
public class TrackInfoBo  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 记录ID
	 *
	 * 版本 >= 0
	 */
	 private long Id;

	/**
	 * 运单号
	 *
	 * 版本 >= 0
	 */
	 private String DeliverId = new String();

	/**
	 * 用户帐号,选填，40字符内
	 *
	 * 版本 >= 0
	 */
	 private String UserId = new String();

	/**
	 * 物流公司编码
	 *
	 * 版本 >= 0
	 */
	 private String CompanyCode = new String();

	/**
	 * 更新物流跟踪信息次数
	 *
	 * 版本 >= 0
	 */
	 private long Count;

	/**
	 * 记录创建时间
	 *
	 * 版本 >= 0
	 */
	 private long CreateTime;

	/**
	 * 揽件时间
	 *
	 * 版本 >= 0
	 */
	 private long PickupTime;

	/**
	 * 派件时间
	 *
	 * 版本 >= 0
	 */
	 private long DeliveryTime;

	/**
	 * 签收时间
	 *
	 * 版本 >= 0
	 */
	 private long SignTime;

	/**
	 * 物流跟踪信息更新时间
	 *
	 * 版本 >= 0
	 */
	 private long UpdateTime;

	/**
	 * 备注或失败信息
	 *
	 * 版本 >= 0
	 */
	 private String Remark = new String();

	/**
	 * QQ速递订单ID
	 *
	 * 版本 >= 0
	 */
	 private long SudiOrderId;

	/**
	 * 运单号绑定状态 
	 *
	 * 版本 >= 0
	 */
	 private long Bind;

	/**
	 * 调用者来源
	 *
	 * 版本 >= 0
	 */
	 private short Source;

	/**
	 * 调用者系统中业务Id
	 *
	 * 版本 >= 0
	 */
	 private String BizId = new String();

	/**
	 * 发货地址
	 *
	 * 版本 >= 0
	 */
	 private SimpleAddressBo SenderAddr = new SimpleAddressBo();

	/**
	 * 收货地址
	 *
	 * 版本 >= 0
	 */
	 private SimpleAddressBo ReceiverAddr = new SimpleAddressBo();

	/**
	 * 结构化包裹路由详情
	 *
	 * 版本 >= 0
	 */
	 private Vector<TrackStep> Steps = new Vector<TrackStep>();

	/**
	 * 字符串形式包裹路由详情
	 *
	 * 版本 >= 0
	 */
	 private String StepRemark = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 版本 >= 0
	 */
	 private short Id_u;

	/**
	 * 版本 >= 0
	 */
	 private short DeliverId_u;

	/**
	 * 版本 >= 0
	 */
	 private short Uin_u;

	/**
	 * 版本 >= 0
	 */
	 private short CompanyCode_u;

	/**
	 * 版本 >= 0
	 */
	 private short Count_u;

	/**
	 * 版本 >= 0
	 */
	 private short CreateTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short PickupTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short DeliveryTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short SignTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short UpdateTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short Remark_u;

	/**
	 * 版本 >= 0
	 */
	 private short SudiOrderId_u;

	/**
	 * 版本 >= 0
	 */
	 private short Bind_u;

	/**
	 * 版本 >= 0
	 */
	 private short Source_u;

	/**
	 * 版本 >= 0
	 */
	 private short BizId_u;

	/**
	 * 版本 >= 0
	 */
	 private short SenderAddr_u;

	/**
	 * 版本 >= 0
	 */
	 private short ReceiverAddr_u;

	/**
	 * 版本 >= 0
	 */
	 private short Steps_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushLong(Id);
		bs.pushString(DeliverId);
		bs.pushString(UserId);
		bs.pushString(CompanyCode);
		bs.pushUInt(Count);
		bs.pushUInt(CreateTime);
		bs.pushUInt(PickupTime);
		bs.pushUInt(DeliveryTime);
		bs.pushUInt(SignTime);
		bs.pushUInt(UpdateTime);
		bs.pushString(Remark);
		bs.pushLong(SudiOrderId);
		bs.pushUInt(Bind);
		bs.pushUByte(Source);
		bs.pushString(BizId);
		bs.pushObject(SenderAddr);
		bs.pushObject(ReceiverAddr);
		bs.pushObject(Steps);
		bs.pushString(StepRemark);
		bs.pushUByte(Version_u);
		bs.pushUByte(Id_u);
		bs.pushUByte(DeliverId_u);
		bs.pushUByte(Uin_u);
		bs.pushUByte(CompanyCode_u);
		bs.pushUByte(Count_u);
		bs.pushUByte(CreateTime_u);
		bs.pushUByte(PickupTime_u);
		bs.pushUByte(DeliveryTime_u);
		bs.pushUByte(SignTime_u);
		bs.pushUByte(UpdateTime_u);
		bs.pushUByte(Remark_u);
		bs.pushUByte(SudiOrderId_u);
		bs.pushUByte(Bind_u);
		bs.pushUByte(Source_u);
		bs.pushUByte(BizId_u);
		bs.pushUByte(SenderAddr_u);
		bs.pushUByte(ReceiverAddr_u);
		bs.pushUByte(Steps_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Id = bs.popLong();
		DeliverId = bs.popString();
		UserId = bs.popString();
		CompanyCode = bs.popString();
		Count = bs.popUInt();
		CreateTime = bs.popUInt();
		PickupTime = bs.popUInt();
		DeliveryTime = bs.popUInt();
		SignTime = bs.popUInt();
		UpdateTime = bs.popUInt();
		Remark = bs.popString();
		SudiOrderId = bs.popLong();
		Bind = bs.popUInt();
		Source = bs.popUByte();
		BizId = bs.popString();
		SenderAddr = (SimpleAddressBo) bs.popObject(SimpleAddressBo.class);
		ReceiverAddr = (SimpleAddressBo) bs.popObject(SimpleAddressBo.class);
		Steps = (Vector<TrackStep>)bs.popVector(TrackStep.class);
		StepRemark = bs.popString();
		Version_u = bs.popUByte();
		Id_u = bs.popUByte();
		DeliverId_u = bs.popUByte();
		Uin_u = bs.popUByte();
		CompanyCode_u = bs.popUByte();
		Count_u = bs.popUByte();
		CreateTime_u = bs.popUByte();
		PickupTime_u = bs.popUByte();
		DeliveryTime_u = bs.popUByte();
		SignTime_u = bs.popUByte();
		UpdateTime_u = bs.popUByte();
		Remark_u = bs.popUByte();
		SudiOrderId_u = bs.popUByte();
		Bind_u = bs.popUByte();
		Source_u = bs.popUByte();
		BizId_u = bs.popUByte();
		SenderAddr_u = bs.popUByte();
		ReceiverAddr_u = bs.popUByte();
		Steps_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	/**
	 * 获取记录ID
	 * 
	 * 此字段的版本 >= 0
	 * @return Id value 类型为:long
	 * 
	 */
	public long getId()
	{
		return Id;
	}


	/**
	 * 设置记录ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setId(long value)
	{
		this.Id = value;
		this.Id_u = 1;
	}


	/**
	 * 获取运单号
	 * 
	 * 此字段的版本 >= 0
	 * @return DeliverId value 类型为:String
	 * 
	 */
	public String getDeliverId()
	{
		return DeliverId;
	}


	/**
	 * 设置运单号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDeliverId(String value)
	{
		this.DeliverId = value;
		this.DeliverId_u = 1;
	}


	/**
	 * 获取用户帐号,选填，40字符内
	 * 
	 * 此字段的版本 >= 0
	 * @return UserId value 类型为:String
	 * 
	 */
	public String getUserId()
	{
		return UserId;
	}


	/**
	 * 设置用户帐号,选填，40字符内
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setUserId(String value)
	{
		this.UserId = value;
	}


	/**
	 * 获取物流公司编码
	 * 
	 * 此字段的版本 >= 0
	 * @return CompanyCode value 类型为:String
	 * 
	 */
	public String getCompanyCode()
	{
		return CompanyCode;
	}


	/**
	 * 设置物流公司编码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCompanyCode(String value)
	{
		this.CompanyCode = value;
		this.CompanyCode_u = 1;
	}


	/**
	 * 获取更新物流跟踪信息次数
	 * 
	 * 此字段的版本 >= 0
	 * @return Count value 类型为:long
	 * 
	 */
	public long getCount()
	{
		return Count;
	}


	/**
	 * 设置更新物流跟踪信息次数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCount(long value)
	{
		this.Count = value;
		this.Count_u = 1;
	}


	/**
	 * 获取记录创建时间
	 * 
	 * 此字段的版本 >= 0
	 * @return CreateTime value 类型为:long
	 * 
	 */
	public long getCreateTime()
	{
		return CreateTime;
	}


	/**
	 * 设置记录创建时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCreateTime(long value)
	{
		this.CreateTime = value;
		this.CreateTime_u = 1;
	}


	/**
	 * 获取揽件时间
	 * 
	 * 此字段的版本 >= 0
	 * @return PickupTime value 类型为:long
	 * 
	 */
	public long getPickupTime()
	{
		return PickupTime;
	}


	/**
	 * 设置揽件时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPickupTime(long value)
	{
		this.PickupTime = value;
		this.PickupTime_u = 1;
	}


	/**
	 * 获取派件时间
	 * 
	 * 此字段的版本 >= 0
	 * @return DeliveryTime value 类型为:long
	 * 
	 */
	public long getDeliveryTime()
	{
		return DeliveryTime;
	}


	/**
	 * 设置派件时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDeliveryTime(long value)
	{
		this.DeliveryTime = value;
		this.DeliveryTime_u = 1;
	}


	/**
	 * 获取签收时间
	 * 
	 * 此字段的版本 >= 0
	 * @return SignTime value 类型为:long
	 * 
	 */
	public long getSignTime()
	{
		return SignTime;
	}


	/**
	 * 设置签收时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSignTime(long value)
	{
		this.SignTime = value;
		this.SignTime_u = 1;
	}


	/**
	 * 获取物流跟踪信息更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return UpdateTime value 类型为:long
	 * 
	 */
	public long getUpdateTime()
	{
		return UpdateTime;
	}


	/**
	 * 设置物流跟踪信息更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUpdateTime(long value)
	{
		this.UpdateTime = value;
		this.UpdateTime_u = 1;
	}


	/**
	 * 获取备注或失败信息
	 * 
	 * 此字段的版本 >= 0
	 * @return Remark value 类型为:String
	 * 
	 */
	public String getRemark()
	{
		return Remark;
	}


	/**
	 * 设置备注或失败信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRemark(String value)
	{
		this.Remark = value;
		this.Remark_u = 1;
	}


	/**
	 * 获取QQ速递订单ID
	 * 
	 * 此字段的版本 >= 0
	 * @return SudiOrderId value 类型为:long
	 * 
	 */
	public long getSudiOrderId()
	{
		return SudiOrderId;
	}


	/**
	 * 设置QQ速递订单ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSudiOrderId(long value)
	{
		this.SudiOrderId = value;
		this.SudiOrderId_u = 1;
	}


	/**
	 * 获取运单号绑定状态 
	 * 
	 * 此字段的版本 >= 0
	 * @return Bind value 类型为:long
	 * 
	 */
	public long getBind()
	{
		return Bind;
	}


	/**
	 * 设置运单号绑定状态 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBind(long value)
	{
		this.Bind = value;
		this.Bind_u = 1;
	}


	/**
	 * 获取调用者来源
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:short
	 * 
	 */
	public short getSource()
	{
		return Source;
	}


	/**
	 * 设置调用者来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSource(short value)
	{
		this.Source = value;
		this.Source_u = 1;
	}


	/**
	 * 获取调用者系统中业务Id
	 * 
	 * 此字段的版本 >= 0
	 * @return BizId value 类型为:String
	 * 
	 */
	public String getBizId()
	{
		return BizId;
	}


	/**
	 * 设置调用者系统中业务Id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBizId(String value)
	{
		this.BizId = value;
		this.BizId_u = 1;
	}


	/**
	 * 获取发货地址
	 * 
	 * 此字段的版本 >= 0
	 * @return SenderAddr value 类型为:SimpleAddressBo
	 * 
	 */
	public SimpleAddressBo getSenderAddr()
	{
		return SenderAddr;
	}


	/**
	 * 设置发货地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:SimpleAddressBo
	 * 
	 */
	public void setSenderAddr(SimpleAddressBo value)
	{
		if (value != null) {
				this.SenderAddr = value;
				this.SenderAddr_u = 1;
		}
	}


	/**
	 * 获取收货地址
	 * 
	 * 此字段的版本 >= 0
	 * @return ReceiverAddr value 类型为:SimpleAddressBo
	 * 
	 */
	public SimpleAddressBo getReceiverAddr()
	{
		return ReceiverAddr;
	}


	/**
	 * 设置收货地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:SimpleAddressBo
	 * 
	 */
	public void setReceiverAddr(SimpleAddressBo value)
	{
		if (value != null) {
				this.ReceiverAddr = value;
				this.ReceiverAddr_u = 1;
		}
	}


	/**
	 * 获取结构化包裹路由详情
	 * 
	 * 此字段的版本 >= 0
	 * @return Steps value 类型为:Vector<TrackStep>
	 * 
	 */
	public Vector<TrackStep> getSteps()
	{
		return Steps;
	}


	/**
	 * 设置结构化包裹路由详情
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<TrackStep>
	 * 
	 */
	public void setSteps(Vector<TrackStep> value)
	{
		if (value != null) {
				this.Steps = value;
				this.Steps_u = 1;
		}
	}


	/**
	 * 获取字符串形式包裹路由详情
	 * 
	 * 此字段的版本 >= 0
	 * @return StepRemark value 类型为:String
	 * 
	 */
	public String getStepRemark()
	{
		return StepRemark;
	}


	/**
	 * 设置字符串形式包裹路由详情
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStepRemark(String value)
	{
		this.StepRemark = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Id_u value 类型为:short
	 * 
	 */
	public short getId_u()
	{
		return Id_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setId_u(short value)
	{
		this.Id_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DeliverId_u value 类型为:short
	 * 
	 */
	public short getDeliverId_u()
	{
		return DeliverId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDeliverId_u(short value)
	{
		this.DeliverId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Uin_u value 类型为:short
	 * 
	 */
	public short getUin_u()
	{
		return Uin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUin_u(short value)
	{
		this.Uin_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return CompanyCode_u value 类型为:short
	 * 
	 */
	public short getCompanyCode_u()
	{
		return CompanyCode_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCompanyCode_u(short value)
	{
		this.CompanyCode_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Count_u value 类型为:short
	 * 
	 */
	public short getCount_u()
	{
		return Count_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCount_u(short value)
	{
		this.Count_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return CreateTime_u value 类型为:short
	 * 
	 */
	public short getCreateTime_u()
	{
		return CreateTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCreateTime_u(short value)
	{
		this.CreateTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PickupTime_u value 类型为:short
	 * 
	 */
	public short getPickupTime_u()
	{
		return PickupTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPickupTime_u(short value)
	{
		this.PickupTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DeliveryTime_u value 类型为:short
	 * 
	 */
	public short getDeliveryTime_u()
	{
		return DeliveryTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDeliveryTime_u(short value)
	{
		this.DeliveryTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SignTime_u value 类型为:short
	 * 
	 */
	public short getSignTime_u()
	{
		return SignTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSignTime_u(short value)
	{
		this.SignTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return UpdateTime_u value 类型为:short
	 * 
	 */
	public short getUpdateTime_u()
	{
		return UpdateTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUpdateTime_u(short value)
	{
		this.UpdateTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Remark_u value 类型为:short
	 * 
	 */
	public short getRemark_u()
	{
		return Remark_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRemark_u(short value)
	{
		this.Remark_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SudiOrderId_u value 类型为:short
	 * 
	 */
	public short getSudiOrderId_u()
	{
		return SudiOrderId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSudiOrderId_u(short value)
	{
		this.SudiOrderId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Bind_u value 类型为:short
	 * 
	 */
	public short getBind_u()
	{
		return Bind_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBind_u(short value)
	{
		this.Bind_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Source_u value 类型为:short
	 * 
	 */
	public short getSource_u()
	{
		return Source_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSource_u(short value)
	{
		this.Source_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BizId_u value 类型为:short
	 * 
	 */
	public short getBizId_u()
	{
		return BizId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBizId_u(short value)
	{
		this.BizId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SenderAddr_u value 类型为:short
	 * 
	 */
	public short getSenderAddr_u()
	{
		return SenderAddr_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSenderAddr_u(short value)
	{
		this.SenderAddr_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ReceiverAddr_u value 类型为:short
	 * 
	 */
	public short getReceiverAddr_u()
	{
		return ReceiverAddr_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReceiverAddr_u(short value)
	{
		this.ReceiverAddr_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Steps_u value 类型为:short
	 * 
	 */
	public short getSteps_u()
	{
		return Steps_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSteps_u(short value)
	{
		this.Steps_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(TrackInfoBo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 17;  //计算字段Id的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(DeliverId);  //计算字段DeliverId的长度 size_of(String)
				length += ByteStream.getObjectSize(UserId);  //计算字段UserId的长度 size_of(String)
				length += ByteStream.getObjectSize(CompanyCode);  //计算字段CompanyCode的长度 size_of(String)
				length += 4;  //计算字段Count的长度 size_of(uint32_t)
				length += 4;  //计算字段CreateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段PickupTime的长度 size_of(uint32_t)
				length += 4;  //计算字段DeliveryTime的长度 size_of(uint32_t)
				length += 4;  //计算字段SignTime的长度 size_of(uint32_t)
				length += 4;  //计算字段UpdateTime的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(Remark);  //计算字段Remark的长度 size_of(String)
				length += 17;  //计算字段SudiOrderId的长度 size_of(uint64_t)
				length += 4;  //计算字段Bind的长度 size_of(uint32_t)
				length += 1;  //计算字段Source的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(BizId);  //计算字段BizId的长度 size_of(String)
				length += ByteStream.getObjectSize(SenderAddr);  //计算字段SenderAddr的长度 size_of(SimpleAddressBo)
				length += ByteStream.getObjectSize(ReceiverAddr);  //计算字段ReceiverAddr的长度 size_of(SimpleAddressBo)
				length += ByteStream.getObjectSize(Steps);  //计算字段Steps的长度 size_of(Vector)
				length += ByteStream.getObjectSize(StepRemark);  //计算字段StepRemark的长度 size_of(String)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段Id_u的长度 size_of(uint8_t)
				length += 1;  //计算字段DeliverId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段Uin_u的长度 size_of(uint8_t)
				length += 1;  //计算字段CompanyCode_u的长度 size_of(uint8_t)
				length += 1;  //计算字段Count_u的长度 size_of(uint8_t)
				length += 1;  //计算字段CreateTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段PickupTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段DeliveryTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段SignTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段UpdateTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段Remark_u的长度 size_of(uint8_t)
				length += 1;  //计算字段SudiOrderId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段Bind_u的长度 size_of(uint8_t)
				length += 1;  //计算字段Source_u的长度 size_of(uint8_t)
				length += 1;  //计算字段BizId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段SenderAddr_u的长度 size_of(uint8_t)
				length += 1;  //计算字段ReceiverAddr_u的长度 size_of(uint8_t)
				length += 1;  //计算字段Steps_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
