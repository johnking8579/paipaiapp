//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.logistics.protocol.QueryTrackInfo.java

package com.qq.qqbuy.thirdparty.idl.logistics.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *简单地址Bo
 *
 *@date 2014-12-19 11:53:14
 *
 *@since version:0
*/
public class SimpleAddressBo  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 发货人Id
	 *
	 * 版本 >= 0
	 */
	 private String UserId = new String();

	/**
	 * 发货地址
	 *
	 * 版本 >= 0
	 */
	 private String Address = new String();

	/**
	 * 发货地代码
	 *
	 * 版本 >= 0
	 */
	 private long CityCode;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 版本 >= 0
	 */
	 private short UserId_u;

	/**
	 * 版本 >= 0
	 */
	 private short Address_u;

	/**
	 * 版本 >= 0
	 */
	 private short CityCode_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushString(UserId);
		bs.pushString(Address);
		bs.pushUInt(CityCode);
		bs.pushUByte(Version_u);
		bs.pushUByte(UserId_u);
		bs.pushUByte(Address_u);
		bs.pushUByte(CityCode_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		UserId = bs.popString();
		Address = bs.popString();
		CityCode = bs.popUInt();
		Version_u = bs.popUByte();
		UserId_u = bs.popUByte();
		Address_u = bs.popUByte();
		CityCode_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	/**
	 * 获取发货人Id
	 * 
	 * 此字段的版本 >= 0
	 * @return UserId value 类型为:String
	 * 
	 */
	public String getUserId()
	{
		return UserId;
	}


	/**
	 * 设置发货人Id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setUserId(String value)
	{
		this.UserId = value;
		this.UserId_u = 1;
	}


	/**
	 * 获取发货地址
	 * 
	 * 此字段的版本 >= 0
	 * @return Address value 类型为:String
	 * 
	 */
	public String getAddress()
	{
		return Address;
	}


	/**
	 * 设置发货地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAddress(String value)
	{
		this.Address = value;
		this.Address_u = 1;
	}


	/**
	 * 获取发货地代码
	 * 
	 * 此字段的版本 >= 0
	 * @return CityCode value 类型为:long
	 * 
	 */
	public long getCityCode()
	{
		return CityCode;
	}


	/**
	 * 设置发货地代码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCityCode(long value)
	{
		this.CityCode = value;
		this.CityCode_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return UserId_u value 类型为:short
	 * 
	 */
	public short getUserId_u()
	{
		return UserId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUserId_u(short value)
	{
		this.UserId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address_u value 类型为:short
	 * 
	 */
	public short getAddress_u()
	{
		return Address_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress_u(short value)
	{
		this.Address_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return CityCode_u value 类型为:short
	 * 
	 */
	public short getCityCode_u()
	{
		return CityCode_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCityCode_u(short value)
	{
		this.CityCode_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SimpleAddressBo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(UserId);  //计算字段UserId的长度 size_of(String)
				length += ByteStream.getObjectSize(Address);  //计算字段Address的长度 size_of(String)
				length += 4;  //计算字段CityCode的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段UserId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段Address_u的长度 size_of(uint8_t)
				length += 1;  //计算字段CityCode_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
