//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.pay.ao.idl.GetPayRecommendListResp.java

package com.qq.qqbuy.thirdparty.idl.pay.recommend.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

//import com.paipai.lang.GenericWrapper;

/**
 *支付推荐对象
 *
 *@date 2015-04-24 10:22:18
 *
 *@since version:0
*/
public class PayRecommend  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20150423;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 支付方式
	 *
	 * 版本 >= 0
	 */
	 private long dwPayType;

	/**
	 * 版本 >= 0
	 */
	 private short dwPayType_u;

	/**
	 * 支付描述
	 *
	 * 版本 >= 0
	 */
	 private String strDescription = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strDescription_u;

	/**
	 * 推荐类型
	 *
	 * 版本 >= 20150119
	 */
	 private long dwRecommendType;

	/**
	 * 
	 *
	 * 版本 >= 20150119
	 */
	 private short dwRecommendType_u;

	/**
	 * 推荐信息
	 *
	 * 版本 >= 20150423
	 */
	 private String strPromotion = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strPromotion_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushUInt(dwPayType);
		bs.pushUByte(dwPayType_u);
		bs.pushString(strDescription);
		bs.pushUByte(strDescription_u);
		if(  this.version >= 20150119 ){
				bs.pushUInt(dwRecommendType);
		}
		if(  this.version >= 20150119 ){
				bs.pushUByte(dwRecommendType_u);
		}
		if(  this.version >= 20150423 ){
				bs.pushString(strPromotion);
		}
		bs.pushUByte(strPromotion_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		dwPayType = bs.popUInt();
		dwPayType_u = bs.popUByte();
		strDescription = bs.popString();
		strDescription_u = bs.popUByte();
		if(  this.version >= 20150119 ){
				dwRecommendType = bs.popUInt();
		}
		if(  this.version >= 20150119 ){
				dwRecommendType_u = bs.popUByte();
		}
		if(  this.version >= 20150423 ){
				strPromotion = bs.popString();
		}
		strPromotion_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取Э??汾??
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置Э??汾??
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取??????
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPayType value 类型为:long
	 * 
	 */
	public long getDwPayType()
	{
		return dwPayType;
	}


	/**
	 * 设置??????
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwPayType(long value)
	{
		this.dwPayType = value;
		this.dwPayType_u = 1;
	}

	public boolean issetDwPayType()
	{
		return this.dwPayType_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPayType_u value 类型为:short
	 * 
	 */
	public short getDwPayType_u()
	{
		return dwPayType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwPayType_u(short value)
	{
		this.dwPayType_u = value;
	}


	/**
	 * 获取???????
	 * 
	 * 此字段的版本 >= 0
	 * @return strDescription value 类型为:String
	 * 
	 */
	public String getStrDescription()
	{
		return strDescription;
	}


	/**
	 * 设置???????
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrDescription(String value)
	{
		this.strDescription = value;
		this.strDescription_u = 1;
	}

	public boolean issetStrDescription()
	{
		return this.strDescription_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strDescription_u value 类型为:short
	 * 
	 */
	public short getStrDescription_u()
	{
		return strDescription_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrDescription_u(short value)
	{
		this.strDescription_u = value;
	}


	/**
	 * 获取???????
	 * 
	 * 此字段的版本 >= 20150119
	 * @return dwRecommendType value 类型为:long
	 * 
	 */
	public long getDwRecommendType()
	{
		return dwRecommendType;
	}


	/**
	 * 设置???????
	 * 
	 * 此字段的版本 >= 20150119
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwRecommendType(long value)
	{
		this.dwRecommendType = value;
		this.dwRecommendType_u = 1;
	}

	public boolean issetDwRecommendType()
	{
		return this.dwRecommendType_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20150119
	 * @return dwRecommendType_u value 类型为:short
	 * 
	 */
	public short getDwRecommendType_u()
	{
		return dwRecommendType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20150119
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwRecommendType_u(short value)
	{
		this.dwRecommendType_u = value;
	}


	/**
	 * 获取??????
	 * 
	 * 此字段的版本 >= 20150423
	 * @return strPromotion value 类型为:String
	 * 
	 */
	public String getStrPromotion()
	{
		return strPromotion;
	}


	/**
	 * 设置??????
	 * 
	 * 此字段的版本 >= 20150423
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrPromotion(String value)
	{
		this.strPromotion = value;
		this.strPromotion_u = 1;
	}

	public boolean issetStrPromotion()
	{
		return this.strPromotion_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strPromotion_u value 类型为:short
	 * 
	 */
	public short getStrPromotion_u()
	{
		return strPromotion_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrPromotion_u(short value)
	{
		this.strPromotion_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(PayRecommend)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwPayType的长度 size_of(uint32_t)
				length += 1;  //计算字段dwPayType_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strDescription, null);  //计算字段strDescription的长度 size_of(String)
				length += 1;  //计算字段strDescription_u的长度 size_of(uint8_t)
				if(  this.version >= 20150119 ){
						length += 4;  //计算字段dwRecommendType的长度 size_of(uint32_t)
				}
				if(  this.version >= 20150119 ){
						length += 1;  //计算字段dwRecommendType_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20150423 ){
						length += ByteStream.getObjectSize(strPromotion, null);  //计算字段strPromotion的长度 size_of(String)
				}
				length += 1;  //计算字段strPromotion_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(PayRecommend)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwPayType的长度 size_of(uint32_t)
				length += 1;  //计算字段dwPayType_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strDescription, encoding);  //计算字段strDescription的长度 size_of(String)
				length += 1;  //计算字段strDescription_u的长度 size_of(uint8_t)
				if(  this.version >= 20150119 ){
						length += 4;  //计算字段dwRecommendType的长度 size_of(uint32_t)
				}
				if(  this.version >= 20150119 ){
						length += 1;  //计算字段dwRecommendType_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20150423 ){
						length += ByteStream.getObjectSize(strPromotion, encoding);  //计算字段strPromotion的长度 size_of(String)
				}
				length += 1;  //计算字段strPromotion_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本20150423所包含的字段*******
 *	long version;///<Э??汾??
 *	short version_u;
 *	long dwPayType;///<??????
 *	short dwPayType_u;
 *	String strDescription;///<???????
 *	short strDescription_u;
 *	long dwRecommendType;///<???????
 *	short dwRecommendType_u;
 *	String strPromotion;///<??????
 *	short strPromotion_u;
 *****以上是版本20150423所包含的字段*******
 *
 *****以下是版本20150119所包含的字段*******
 *	long version;///<Э??汾??
 *	short version_u;
 *	long dwPayType;///<??????
 *	short dwPayType_u;
 *	String strDescription;///<???????
 *	short strDescription_u;
 *	long dwRecommendType;///<???????
 *	short dwRecommendType_u;
 *	short strPromotion_u;
 *****以上是版本20150119所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
