package com.qq.qqbuy.thirdparty.idl.pay.recommend;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.pay.recommend.protocol.GetPayRecommendListReq;
import com.qq.qqbuy.thirdparty.idl.pay.recommend.protocol.GetPayRecommendListResp;
import com.qq.qqbuy.thirdparty.idl.pay.recommend.protocol.PayRecommendCondition;

/**
 * 支付路由查询客户端
 */
public class PayRecommendClient extends SupportIDLBaseClient {

    /**
     * 支付场景
     * 1.微信场景
     * 2.手Q场景
     * 3.H5浏览器场景
     * 4.APP场景
     */
    private static long PAY_RECOMMEND_SCENE_APP = 4;

    public static GetPayRecommendListResp initPayTypeInfo(String mk,long uin,long uType,long dealTotalFee,String itemCodes,String sellerIds,String categorys,String supportcods) {

        GetPayRecommendListReq req = new GetPayRecommendListReq();
        req.setMachineKey(mk);
        req.setSource(CALL_IDL_SOURCE);
        PayRecommendCondition condition = req.getPayRecommendCondition();
        /**
         * 设置支付场景模，默认为app场景
         */
        condition.setDwScene(PAY_RECOMMEND_SCENE_APP);
        /**
         * 登录用户uin
         */
        condition.setDdwUin(uin);
        /**
         * 用户类型
         * 1=QQ
         * 2=WX
         * 3=JD
         */
        condition.setDwUType(uType);
        /**
         * 订单总价
         */
        condition.setDwTotalFee(dealTotalFee);
        /**
         * 设置商品Id串,用半角逗号分隔
         */
        condition.setStrCommodityIds(itemCodes);
        /**
         * 所有商品卖家qq号串,用半角逗号分隔(顺序跟commodityids一一对应)
         */
        condition.setStrSellerIds(sellerIds);
        /**
         * 提交订单的商品类目,用半角逗号分隔,例如1,2,1(顺序跟commodityids一一对应)
         * 1：实物
         * 2：虚拟
         * 默认为1
         */
        condition.setStrCategorys(categorys);
        /**
         * 是否支持cod
           1=支持
           2=不支持
           用半角逗号分隔，例如1,2,1(顺序跟commodityids一一对应)
         */
        condition.setStrSupportCods(supportcods);
        req.setPayRecommendCondition(condition);
        GetPayRecommendListResp resp = new GetPayRecommendListResp();
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
//        stub.setIpAndPort("10.136.10.162", 53101);//联调时候用的
        ret = invokePaiPaiIDL(req, resp, stub);
        return ret == SUCCESS ? resp : null;
    }

}
