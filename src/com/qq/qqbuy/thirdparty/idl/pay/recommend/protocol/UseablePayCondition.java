//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.pay.ao.idl.GetUseablePayListReq.java

package com.qq.qqbuy.thirdparty.idl.pay.recommend.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *??????????
 *
 *@date 2015-04-24 10:22:18
 *
 *@since version:0
*/
public class UseablePayCondition  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * Э??汾??
	 *
	 * 版本 >= 0
	 */
	 private long version = 20150303;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * ???UIN
	 *
	 * 版本 >= 0
	 */
	 private long ddwUin;

	/**
	 * 版本 >= 0
	 */
	 private short ddwUin_u;

	/**
	 * ???????
	 *
	 * 版本 >= 0
	 */
	 private long dwScene;

	/**
	 * 版本 >= 0
	 */
	 private short dwScene_u;

	/**
	 * ???????
	 *
	 * 版本 >= 0
	 */
	 private long dwUType;

	/**
	 * 版本 >= 0
	 */
	 private short dwUType_u;

	/**
	 * ??????
	 *
	 * 版本 >= 0
	 */
	 private String strOrderId = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strOrderId_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushLong(ddwUin);
		bs.pushUByte(ddwUin_u);
		bs.pushUInt(dwScene);
		bs.pushUByte(dwScene_u);
		bs.pushUInt(dwUType);
		bs.pushUByte(dwUType_u);
		bs.pushString(strOrderId);
		bs.pushUByte(strOrderId_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		ddwUin = bs.popLong();
		ddwUin_u = bs.popUByte();
		dwScene = bs.popUInt();
		dwScene_u = bs.popUByte();
		dwUType = bs.popUInt();
		dwUType_u = bs.popUByte();
		strOrderId = bs.popString();
		strOrderId_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取Э??汾??
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置Э??汾??
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取???UIN
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwUin value 类型为:long
	 * 
	 */
	public long getDdwUin()
	{
		return ddwUin;
	}


	/**
	 * 设置???UIN
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDdwUin(long value)
	{
		this.ddwUin = value;
		this.ddwUin_u = 1;
	}

	public boolean issetDdwUin()
	{
		return this.ddwUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwUin_u value 类型为:short
	 * 
	 */
	public short getDdwUin_u()
	{
		return ddwUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDdwUin_u(short value)
	{
		this.ddwUin_u = value;
	}


	/**
	 * 获取???????
	 * 
	 * 此字段的版本 >= 0
	 * @return dwScene value 类型为:long
	 * 
	 */
	public long getDwScene()
	{
		return dwScene;
	}


	/**
	 * 设置???????
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwScene(long value)
	{
		this.dwScene = value;
		this.dwScene_u = 1;
	}

	public boolean issetDwScene()
	{
		return this.dwScene_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwScene_u value 类型为:short
	 * 
	 */
	public short getDwScene_u()
	{
		return dwScene_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwScene_u(short value)
	{
		this.dwScene_u = value;
	}


	/**
	 * 获取???????
	 * 
	 * 此字段的版本 >= 0
	 * @return dwUType value 类型为:long
	 * 
	 */
	public long getDwUType()
	{
		return dwUType;
	}


	/**
	 * 设置???????
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwUType(long value)
	{
		this.dwUType = value;
		this.dwUType_u = 1;
	}

	public boolean issetDwUType()
	{
		return this.dwUType_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwUType_u value 类型为:short
	 * 
	 */
	public short getDwUType_u()
	{
		return dwUType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwUType_u(short value)
	{
		this.dwUType_u = value;
	}


	/**
	 * 获取??????
	 * 
	 * 此字段的版本 >= 0
	 * @return strOrderId value 类型为:String
	 * 
	 */
	public String getStrOrderId()
	{
		return strOrderId;
	}


	/**
	 * 设置??????
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrOrderId(String value)
	{
		this.strOrderId = value;
		this.strOrderId_u = 1;
	}

	public boolean issetStrOrderId()
	{
		return this.strOrderId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strOrderId_u value 类型为:short
	 * 
	 */
	public short getStrOrderId_u()
	{
		return strOrderId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrOrderId_u(short value)
	{
		this.strOrderId_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(UseablePayCondition)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段ddwUin的长度 size_of(uint64_t)
				length += 1;  //计算字段ddwUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwScene的长度 size_of(uint32_t)
				length += 1;  //计算字段dwScene_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwUType的长度 size_of(uint32_t)
				length += 1;  //计算字段dwUType_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strOrderId, null);  //计算字段strOrderId的长度 size_of(String)
				length += 1;  //计算字段strOrderId_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(UseablePayCondition)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段ddwUin的长度 size_of(uint64_t)
				length += 1;  //计算字段ddwUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwScene的长度 size_of(uint32_t)
				length += 1;  //计算字段dwScene_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwUType的长度 size_of(uint32_t)
				length += 1;  //计算字段dwUType_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strOrderId, encoding);  //计算字段strOrderId的长度 size_of(String)
				length += 1;  //计算字段strOrderId_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
