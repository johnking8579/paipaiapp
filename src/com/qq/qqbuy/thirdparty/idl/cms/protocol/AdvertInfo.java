 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.cms.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *广告信息
 *
 *@date 2012-06-29 09:45::07
 *
 *@since version:0
*/
public class AdvertInfo  implements ICanSerializeObject
{
	/**
	 * 版本控制的字段
	 *
	 * 版本 >= 0
	 */
	 private int version = 0; 

	/**
	 * 广告ID
	 *
	 * 版本 >= 0
	 */
	 private long adId;

	/**
	 * 广告名称
	 *
	 * 版本 >= 0
	 */
	 private String adName = new String();

	/**
	 * 广告描述
	 *
	 * 版本 >= 0
	 */
	 private String adDese = new String();

	/**
	 * 广告状态：1=正常；2=被删除
	 *
	 * 版本 >= 0
	 */
	 private int state;

	/**
	 * 广告创建时间
	 *
	 * 版本 >= 0
	 */
	 private String createTime = new String();

	/**
	 * 广告最后修改时间
	 *
	 * 版本 >= 0
	 */
	 private String lastModifyTime = new String();

	/**
	 * 广告创建者
	 *
	 * 版本 >= 0
	 */
	 private String creator = new String();

	/**
	 * 广告最后修改者
	 *
	 * 版本 >= 0
	 */
	 private String lastModifyOpr = new String();

	/**
	 * 广告内容
	 *
	 * 版本 >= 0
	 */
	 private String content = new String();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(version);
		bs.pushUInt(adId);
		bs.pushString(adName);
		bs.pushString(adDese);
		bs.pushInt(state);
		bs.pushString(createTime);
		bs.pushString(lastModifyTime);
		bs.pushString(creator);
		bs.pushString(lastModifyOpr);
		bs.pushString(content);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		adId = bs.popUInt();
		adName = bs.popString();
		adDese = bs.popString();
		state = bs.popInt();
		createTime = bs.popString();
		lastModifyTime = bs.popString();
		creator = bs.popString();
		lastModifyOpr = bs.popString();
		content = bs.popString();
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取广告ID
	 * 
	 * 此字段的版本 >= 0
	 * @return adId value 类型为:long
	 * 
	 */
	public long getAdId()
	{
		return adId;
	}


	/**
	 * 设置广告ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAdId(long value)
	{
		this.adId = value;
	}


	/**
	 * 获取广告名称
	 * 
	 * 此字段的版本 >= 0
	 * @return adName value 类型为:String
	 * 
	 */
	public String getAdName()
	{
		return adName;
	}


	/**
	 * 设置广告名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAdName(String value)
	{
		this.adName = value;
	}


	/**
	 * 获取广告描述
	 * 
	 * 此字段的版本 >= 0
	 * @return adDese value 类型为:String
	 * 
	 */
	public String getAdDese()
	{
		return adDese;
	}


	/**
	 * 设置广告描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAdDese(String value)
	{
		this.adDese = value;
	}


	/**
	 * 获取广告状态：1=正常；2=被删除
	 * 
	 * 此字段的版本 >= 0
	 * @return state value 类型为:int
	 * 
	 */
	public int getState()
	{
		return state;
	}


	/**
	 * 设置广告状态：1=正常；2=被删除
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setState(int value)
	{
		this.state = value;
	}


	/**
	 * 获取广告创建时间
	 * 
	 * 此字段的版本 >= 0
	 * @return createTime value 类型为:String
	 * 
	 */
	public String getCreateTime()
	{
		return createTime;
	}


	/**
	 * 设置广告创建时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCreateTime(String value)
	{
		this.createTime = value;
	}


	/**
	 * 获取广告最后修改时间
	 * 
	 * 此字段的版本 >= 0
	 * @return lastModifyTime value 类型为:String
	 * 
	 */
	public String getLastModifyTime()
	{
		return lastModifyTime;
	}


	/**
	 * 设置广告最后修改时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setLastModifyTime(String value)
	{
		this.lastModifyTime = value;
	}


	/**
	 * 获取广告创建者
	 * 
	 * 此字段的版本 >= 0
	 * @return creator value 类型为:String
	 * 
	 */
	public String getCreator()
	{
		return creator;
	}


	/**
	 * 设置广告创建者
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCreator(String value)
	{
		this.creator = value;
	}


	/**
	 * 获取广告最后修改者
	 * 
	 * 此字段的版本 >= 0
	 * @return lastModifyOpr value 类型为:String
	 * 
	 */
	public String getLastModifyOpr()
	{
		return lastModifyOpr;
	}


	/**
	 * 设置广告最后修改者
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setLastModifyOpr(String value)
	{
		this.lastModifyOpr = value;
	}


	/**
	 * 获取广告内容
	 * 
	 * 此字段的版本 >= 0
	 * @return content value 类型为:String
	 * 
	 */
	public String getContent()
	{
		return content;
	}


	/**
	 * 设置广告内容
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setContent(String value)
	{
		this.content = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(AdvertInfo)
				length += 4;  //计算字段version的长度 size_of(int)
				length += 4;  //计算字段adId的长度 size_of(long)
				length += ByteStream.getObjectSize(adName);  //计算字段adName的长度 size_of(String)
				length += ByteStream.getObjectSize(adDese);  //计算字段adDese的长度 size_of(String)
				length += 4;  //计算字段state的长度 size_of(int)
				length += ByteStream.getObjectSize(createTime);  //计算字段createTime的长度 size_of(String)
				length += ByteStream.getObjectSize(lastModifyTime);  //计算字段lastModifyTime的长度 size_of(String)
				length += ByteStream.getObjectSize(creator);  //计算字段creator的长度 size_of(String)
				length += ByteStream.getObjectSize(lastModifyOpr);  //计算字段lastModifyOpr的长度 size_of(String)
				length += ByteStream.getObjectSize(content);  //计算字段content的长度 size_of(String)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
