 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.cms.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *广告策略信息
 *
 *@date 2012-06-29 09:45::07
 *
 *@since version:0
*/
public class AdvertPolicyInfo  implements ICanSerializeObject
{
	/**
	 * 版本控制的字段
	 *
	 * 版本 >= 0
	 */
	 private int version = 0; 

	/**
	 * 广告ID
	 *
	 * 版本 >= 0
	 */
	 private long adId;

	/**
	 * 广告位ID
	 *
	 * 版本 >= 0
	 */
	 private long adPosId;

	/**
	 * 广告上线时间
	 *
	 * 版本 >= 0
	 */
	 private String startTime = new String();

	/**
	 * 广告下线时间
	 *
	 * 版本 >= 0
	 */
	 private String endTime = new String();

	/**
	 * 广告权重，取值[1, 100]
	 *
	 * 版本 >= 0
	 */
	 private int weight;

	/**
	 * 广告在该广告位的关联类目
	 *
	 * 版本 >= 0
	 */
	 private String classId = new String();

	/**
	 * 广告在该广告位的关联关键字
	 *
	 * 版本 >= 0
	 */
	 private String key = new String();

	/**
	 * 广告加入广告位时间
	 *
	 * 版本 >= 0
	 */
	 private String createTime = new String();

	/**
	 * 广告加入广告位的操作者
	 *
	 * 版本 >= 0
	 */
	 private String creator = new String();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(version);
		bs.pushUInt(adId);
		bs.pushUInt(adPosId);
		bs.pushString(startTime);
		bs.pushString(endTime);
		bs.pushInt(weight);
		bs.pushString(classId);
		bs.pushString(key);
		bs.pushString(createTime);
		bs.pushString(creator);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		adId = bs.popUInt();
		adPosId = bs.popUInt();
		startTime = bs.popString();
		endTime = bs.popString();
		weight = bs.popInt();
		classId = bs.popString();
		key = bs.popString();
		createTime = bs.popString();
		creator = bs.popString();
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取广告ID
	 * 
	 * 此字段的版本 >= 0
	 * @return adId value 类型为:long
	 * 
	 */
	public long getAdId()
	{
		return adId;
	}


	/**
	 * 设置广告ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAdId(long value)
	{
		this.adId = value;
	}


	/**
	 * 获取广告位ID
	 * 
	 * 此字段的版本 >= 0
	 * @return adPosId value 类型为:long
	 * 
	 */
	public long getAdPosId()
	{
		return adPosId;
	}


	/**
	 * 设置广告位ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAdPosId(long value)
	{
		this.adPosId = value;
	}


	/**
	 * 获取广告上线时间
	 * 
	 * 此字段的版本 >= 0
	 * @return startTime value 类型为:String
	 * 
	 */
	public String getStartTime()
	{
		return startTime;
	}


	/**
	 * 设置广告上线时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStartTime(String value)
	{
		this.startTime = value;
	}


	/**
	 * 获取广告下线时间
	 * 
	 * 此字段的版本 >= 0
	 * @return endTime value 类型为:String
	 * 
	 */
	public String getEndTime()
	{
		return endTime;
	}


	/**
	 * 设置广告下线时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setEndTime(String value)
	{
		this.endTime = value;
	}


	/**
	 * 获取广告权重，取值[1, 100]
	 * 
	 * 此字段的版本 >= 0
	 * @return weight value 类型为:int
	 * 
	 */
	public int getWeight()
	{
		return weight;
	}


	/**
	 * 设置广告权重，取值[1, 100]
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setWeight(int value)
	{
		this.weight = value;
	}


	/**
	 * 获取广告在该广告位的关联类目
	 * 
	 * 此字段的版本 >= 0
	 * @return classId value 类型为:String
	 * 
	 */
	public String getClassId()
	{
		return classId;
	}


	/**
	 * 设置广告在该广告位的关联类目
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setClassId(String value)
	{
		this.classId = value;
	}


	/**
	 * 获取广告在该广告位的关联关键字
	 * 
	 * 此字段的版本 >= 0
	 * @return key value 类型为:String
	 * 
	 */
	public String getKey()
	{
		return key;
	}


	/**
	 * 设置广告在该广告位的关联关键字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setKey(String value)
	{
		this.key = value;
	}


	/**
	 * 获取广告加入广告位时间
	 * 
	 * 此字段的版本 >= 0
	 * @return createTime value 类型为:String
	 * 
	 */
	public String getCreateTime()
	{
		return createTime;
	}


	/**
	 * 设置广告加入广告位时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCreateTime(String value)
	{
		this.createTime = value;
	}


	/**
	 * 获取广告加入广告位的操作者
	 * 
	 * 此字段的版本 >= 0
	 * @return creator value 类型为:String
	 * 
	 */
	public String getCreator()
	{
		return creator;
	}


	/**
	 * 设置广告加入广告位的操作者
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCreator(String value)
	{
		this.creator = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(AdvertPolicyInfo)
				length += 4;  //计算字段version的长度 size_of(int)
				length += 4;  //计算字段adId的长度 size_of(long)
				length += 4;  //计算字段adPosId的长度 size_of(long)
				length += ByteStream.getObjectSize(startTime);  //计算字段startTime的长度 size_of(String)
				length += ByteStream.getObjectSize(endTime);  //计算字段endTime的长度 size_of(String)
				length += 4;  //计算字段weight的长度 size_of(int)
				length += ByteStream.getObjectSize(classId);  //计算字段classId的长度 size_of(String)
				length += ByteStream.getObjectSize(key);  //计算字段key的长度 size_of(String)
				length += ByteStream.getObjectSize(createTime);  //计算字段createTime的长度 size_of(String)
				length += ByteStream.getObjectSize(creator);  //计算字段creator的长度 size_of(String)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
