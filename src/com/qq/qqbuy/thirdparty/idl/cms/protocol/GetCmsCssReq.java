 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.cms.protocol;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

/**
 *
 *
 *@date 2012-03-03 03:52::02
 *
 *@since version:0
*/
public class  GetCmsCssReq implements IServiceObject
{
	/**
	 * Css文件ID列表，必填 
	 *
	 * 版本 >= 0
	 */
	 private List<String> cssFileIds = new ArrayList<String>();

	/**
	 * 适用版本:1、wap2 2、touch，必填 
	 *
	 * 版本 >= 0
	 */
	 private int type;

	/**
	 * 应用环境类型，非必填默认1 正式-1预览 1正式
	 *
	 * 版本 >= 0
	 */
	 private int envType;

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String inReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(cssFileIds);
		bs.pushInt(type);
		bs.pushInt(envType);
		bs.pushString(inReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		cssFileIds = (List<String>)bs.popList(ArrayList.class,String.class);
		type = bs.popInt();
		envType = bs.popInt();
		inReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xF2051801L;
	}


	/**
	 * 获取Css文件ID列表，必填 
	 * 
	 * 此字段的版本 >= 0
	 * @return cssFileIds value 类型为:List<String>
	 * 
	 */
	public List<String> getCssFileIds()
	{
		return cssFileIds;
	}


	/**
	 * 设置Css文件ID列表，必填 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:List<String>
	 * 
	 */
	public void setCssFileIds(List<String> value)
	{
		if (value != null) {
				this.cssFileIds = value;
		}else{
				this.cssFileIds = new ArrayList<String>();
		}
	}


	/**
	 * 获取适用版本:1、wap2 2、touch，必填 
	 * 
	 * 此字段的版本 >= 0
	 * @return type value 类型为:int
	 * 
	 */
	public int getType()
	{
		return type;
	}


	/**
	 * 设置适用版本:1、wap2 2、touch，必填 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setType(int value)
	{
		this.type = value;
	}


	/**
	 * 获取应用环境类型，非必填默认1 正式-1预览 1正式
	 * 
	 * 此字段的版本 >= 0
	 * @return envType value 类型为:int
	 * 
	 */
	public int getEnvType()
	{
		return envType;
	}


	/**
	 * 设置应用环境类型，非必填默认1 正式-1预览 1正式
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setEnvType(int value)
	{
		this.envType = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserved value 类型为:String
	 * 
	 */
	public String getInReserved()
	{
		return inReserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserved(String value)
	{
		if (value != null) {
				this.inReserved = value;
		}else{
				this.inReserved = new String();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetCmsCssReq)
				length += ByteStream.getObjectSize(cssFileIds);  //计算字段cssFileIds的长度 size_of(List)
				length += 4;  //计算字段type的长度 size_of(int)
				length += 4;  //计算字段envType的长度 size_of(int)
				length += ByteStream.getObjectSize(inReserved);  //计算字段inReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
