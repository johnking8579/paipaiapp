//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.cms.idl.ItemPoolInfo.java

package com.qq.qqbuy.thirdparty.idl.cms.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *商品信息
 *
 *@date 2013-04-23 05:37:00
 *
 *@since version:0
*/
@SuppressWarnings("unchecked")
public class ItemInfo  implements ICanSerializeObject, Comparable
{
	/**
	 * 版本控制的字段
	 *
	 * 版本 >= 0
	 */
	 private int version = 1;

	/**
	 * 商品ID
	 *
	 * 版本 >= 0
	 */
	 private String itemCode = new String();

	/**
	 * 商品名称
	 *
	 * 版本 >= 0
	 */
	 private String itemName = new String();

	/**
	 * 网购价格
	 *
	 * 版本 >= 0
	 */
	 private long wgPrice;

	/**
	 * 市场参考价格
	 *
	 * 版本 >= 0
	 */
	 private long marketPrice;

	/**
	 * 库存状态
	 *
	 * 版本 >= 0
	 */
	 private boolean stockState;

	/**
	 * 商品图片url
	 *
	 * 版本 >= 0
	 */
	 private String imgUrl = new String();

	/**
	 * 优先级
	 *
	 * 版本 >= 0
	 */
	 private int priority;

	/**
	 * 备注
	 *
	 * 版本 >= 0
	 */
	 private String comment = new String();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();

	/**
	 * 活动价
	 *
	 * 版本 >= 1
	 */
	 private long activityPrice;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize() );
		bs.pushInt(version);
		bs.pushString(itemCode);
		bs.pushString(itemName);
		bs.pushUInt(wgPrice);
		bs.pushUInt(marketPrice);
		bs.pushBoolean(stockState);
		bs.pushString(imgUrl);
		bs.pushInt(priority);
		bs.pushString(comment);
		bs.pushString(reserved);
		if(  this.version >= 1 ){
				bs.pushUInt(activityPrice);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		itemCode = bs.popString();
		itemName = bs.popString();
		wgPrice = bs.popUInt();
		marketPrice = bs.popUInt();
		stockState = bs.popBoolean();
		imgUrl = bs.popString();
		priority = bs.popInt();
		comment = bs.popString();
		reserved = bs.popString();
		if(  this.version >= 1 ){
				activityPrice = bs.popUInt();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return itemCode value 类型为:String
	 * 
	 */
	public String getItemCode()
	{
		return itemCode;
	}


	/**
	 * 设置商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemCode(String value)
	{
		this.itemCode = value;
	}


	/**
	 * 获取商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @return itemName value 类型为:String
	 * 
	 */
	public String getItemName()
	{
		return itemName;
	}


	/**
	 * 设置商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemName(String value)
	{
		this.itemName = value;
	}


	/**
	 * 获取网购价格
	 * 
	 * 此字段的版本 >= 0
	 * @return wgPrice value 类型为:long
	 * 
	 */
	public long getWgPrice()
	{
		return wgPrice;
	}


	/**
	 * 设置网购价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWgPrice(long value)
	{
		this.wgPrice = value;
	}


	/**
	 * 获取市场参考价格
	 * 
	 * 此字段的版本 >= 0
	 * @return marketPrice value 类型为:long
	 * 
	 */
	public long getMarketPrice()
	{
		return marketPrice;
	}


	/**
	 * 设置市场参考价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMarketPrice(long value)
	{
		this.marketPrice = value;
	}


	/**
	 * 获取库存状态
	 * 
	 * 此字段的版本 >= 0
	 * @return stockState value 类型为:boolean
	 * 
	 */
	public boolean getStockState()
	{
		return stockState;
	}


	/**
	 * 设置库存状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:boolean
	 * 
	 */
	public void setStockState(boolean value)
	{
		this.stockState = value;
	}


	/**
	 * 获取商品图片url
	 * 
	 * 此字段的版本 >= 0
	 * @return imgUrl value 类型为:String
	 * 
	 */
	public String getImgUrl()
	{
		return imgUrl;
	}


	/**
	 * 设置商品图片url
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setImgUrl(String value)
	{
		this.imgUrl = value;
	}


	/**
	 * 获取优先级
	 * 
	 * 此字段的版本 >= 0
	 * @return priority value 类型为:int
	 * 
	 */
	public int getPriority()
	{
		return priority;
	}


	/**
	 * 设置优先级
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPriority(int value)
	{
		this.priority = value;
	}


	/**
	 * 获取备注
	 * 
	 * 此字段的版本 >= 0
	 * @return comment value 类型为:String
	 * 
	 */
	public String getComment()
	{
		return comment;
	}


	/**
	 * 设置备注
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setComment(String value)
	{
		this.comment = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
	}


	/**
	 * 获取活动价
	 * 
	 * 此字段的版本 >= 1
	 * @return activityPrice value 类型为:long
	 * 
	 */
	public long getActivityPrice()
	{
		return activityPrice;
	}


	/**
	 * 设置活动价
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:long
	 * 
	 */
	public void setActivityPrice(long value)
	{
		this.activityPrice = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemInfo)
				length += 4;  //计算字段version的长度 size_of(int)
				length += ByteStream.getObjectSize(itemCode);  //计算字段itemCode的长度 size_of(String)
				length += ByteStream.getObjectSize(itemName);  //计算字段itemName的长度 size_of(String)
				length += 4;  //计算字段wgPrice的长度 size_of(long)
				length += 4;  //计算字段marketPrice的长度 size_of(long)
				length += 1;  //计算字段stockState的长度 size_of(boolean)
				length += ByteStream.getObjectSize(imgUrl);  //计算字段imgUrl的长度 size_of(String)
				length += 4;  //计算字段priority的长度 size_of(int)
				length += ByteStream.getObjectSize(comment);  //计算字段comment的长度 size_of(String)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
				if(  this.version >= 1 ){
						length += 4;  //计算字段activityPrice的长度 size_of(long)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(ByteStream bs)
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemInfo)
				length += 4;  //计算字段version的长度 size_of(int)
				length += 4 + itemCode.getBytes(bs.getDecodeCharset()).length;  //计算字段itemCode的长度 size_of(String)
				length += 4 + itemName.getBytes(bs.getDecodeCharset()).length;  //计算字段itemName的长度 size_of(String)
				length += 4;  //计算字段wgPrice的长度 size_of(long)
				length += 4;  //计算字段marketPrice的长度 size_of(long)
				length += 1;  //计算字段stockState的长度 size_of(boolean)
				length += 4 + imgUrl.getBytes(bs.getDecodeCharset()).length;  //计算字段imgUrl的长度 size_of(String)
				length += 4;  //计算字段priority的长度 size_of(int)
				length += 4 + comment.getBytes(bs.getDecodeCharset()).length;  //计算字段comment的长度 size_of(String)
				length += 4 + reserved.getBytes(bs.getDecodeCharset()).length;  //计算字段reserved的长度 size_of(String)
				if(  this.version >= 1 ){
						length += 4;  //计算字段activityPrice的长度 size_of(long)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ItemInfo [activityPrice=").append(activityPrice)
				.append(", comment=").append(comment).append(", imgUrl=")
				.append(imgUrl).append(", itemCode=").append(itemCode).append(
						", itemName=").append(itemName)
				.append(", marketPrice=").append(marketPrice).append(
						", priority=").append(priority).append(", reserved=")
				.append(reserved).append(", stockState=").append(stockState)
				.append(", version=").append(version).append(", wgPrice=")
				.append(wgPrice).append("]");
		return builder.toString();
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本1所包含的字段*******
 *	int version;///<版本控制的字段
 *	String itemCode;///<商品ID
 *	String itemName;///<商品名称
 *	long wgPrice;///<网购价格
 *	long marketPrice;///<市场参考价格
 *	boolean stockState;///<库存状态
 *	String imgUrl;///<商品图片url
 *	int priority;///<优先级
 *	String comment;///<备注
 *	String reserved;///<保留字段
 *	long activityPrice;///<活动价
 *****以上是版本1所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	
	@Override
	public int compareTo(Object o) {
		return this.priority - ((ItemInfo) o).getPriority();
	}
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
