 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.cms.protocol;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

/**
 *
 *
 *@date 2012-06-13 11:47::33
 *
 *@since version:0
*/
public class  GetItemPoolResp implements IServiceObject
{
	public long result;
	/**
	 * 错误码
	 *
	 * 版本 >= 0
	 */
	 private int errCode;

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 商品组ID 
	 *
	 * 版本 >= 0
	 */
	 private String poolId = new String();

	/**
	 * 商品组版本号 
	 *
	 * 版本 >= 0
	 */
	 private int poolVersion;

	/**
	 * 类目分类 
	 *
	 * 版本 >= 0
	 */
	 private int categoryId;

	/**
	 * 场景 
	 *
	 * 版本 >= 0
	 */
	 private int scene;

	/**
	 * 商品列表
	 *
	 * 版本 >= 0
	 */
	 private List<ItemInfo> itemList = new ArrayList<ItemInfo>();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String outReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushInt(errCode);
		bs.pushString(errMsg);
		bs.pushString(poolId);
		bs.pushInt(poolVersion);
		bs.pushInt(categoryId);
		bs.pushInt(scene);
		bs.pushObject(itemList);
		bs.pushString(outReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		errCode = bs.popInt();
		errMsg = bs.popString();
		poolId = bs.popString();
		poolVersion = bs.popInt();
		categoryId = bs.popInt();
		scene = bs.popInt();
		itemList = (List<ItemInfo>)bs.popList(ArrayList.class,ItemInfo.class);
		outReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xF2058803L;
	}


	/**
	 * 获取错误码
	 * 
	 * 此字段的版本 >= 0
	 * @return errCode value 类型为:int
	 * 
	 */
	public int getErrCode()
	{
		return errCode;
	}


	/**
	 * 设置错误码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setErrCode(int value)
	{
		this.errCode = value;
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		if (value != null) {
				this.errMsg = value;
		}else{
				this.errMsg = new String();
		}
	}


	/**
	 * 获取商品组ID 
	 * 
	 * 此字段的版本 >= 0
	 * @return poolId value 类型为:String
	 * 
	 */
	public String getPoolId()
	{
		return poolId;
	}


	/**
	 * 设置商品组ID 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPoolId(String value)
	{
		if (value != null) {
				this.poolId = value;
		}else{
				this.poolId = new String();
		}
	}


	/**
	 * 获取商品组版本号 
	 * 
	 * 此字段的版本 >= 0
	 * @return poolVersion value 类型为:int
	 * 
	 */
	public int getPoolVersion()
	{
		return poolVersion;
	}


	/**
	 * 设置商品组版本号 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPoolVersion(int value)
	{
		this.poolVersion = value;
	}


	/**
	 * 获取类目分类 
	 * 
	 * 此字段的版本 >= 0
	 * @return categoryId value 类型为:int
	 * 
	 */
	public int getCategoryId()
	{
		return categoryId;
	}


	/**
	 * 设置类目分类 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setCategoryId(int value)
	{
		this.categoryId = value;
	}


	/**
	 * 获取场景 
	 * 
	 * 此字段的版本 >= 0
	 * @return scene value 类型为:int
	 * 
	 */
	public int getScene()
	{
		return scene;
	}


	/**
	 * 设置场景 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setScene(int value)
	{
		this.scene = value;
	}


	/**
	 * 获取商品列表
	 * 
	 * 此字段的版本 >= 0
	 * @return itemList value 类型为:List<ItemInfo>
	 * 
	 */
	public List<ItemInfo> getItemList()
	{
		return itemList;
	}


	/**
	 * 设置商品列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:List<ItemInfo>
	 * 
	 */
	public void setItemList(List<ItemInfo> value)
	{
		if (value != null) {
				this.itemList = value;
		}else{
				this.itemList = new ArrayList<ItemInfo>();
		}
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserved value 类型为:String
	 * 
	 */
	public String getOutReserved()
	{
		return outReserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserved(String value)
	{
		if (value != null) {
				this.outReserved = value;
		}else{
				this.outReserved = new String();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetItemPoolResp)
				length += 4;  //计算字段errCode的长度 size_of(int)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(poolId);  //计算字段poolId的长度 size_of(String)
				length += 4;  //计算字段poolVersion的长度 size_of(int)
				length += 4;  //计算字段categoryId的长度 size_of(int)
				length += 4;  //计算字段scene的长度 size_of(int)
				length += ByteStream.getObjectSize(itemList);  //计算字段itemList的长度 size_of(List)
				length += ByteStream.getObjectSize(outReserved);  //计算字段outReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
