 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.cms.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *模版信息
 *
 *@date 2012-03-03 03:52::02
 *
 *@since version:0
*/
public class TempInfo  implements ICanSerializeObject
{
	/**
	 * 版本控制的字段
	 *
	 * 版本 >= 0
	 */
	 private int version = 0; 

	/**
	 * 模版ID
	 *
	 * 版本 >= 0
	 */
	 private String tempId = new String();

	/**
	 * 模版名称
	 *
	 * 版本 >= 0
	 */
	 private String tempName = new String();

	/**
	 * 适用版本:1、wap2 2、touch
	 *
	 * 版本 >= 0
	 */
	 private int type;

	/**
	 * 模版内容
	 *
	 * 版本 >= 0
	 */
	 private String content = new String();

	/**
	 * 包含Css文件
	 *
	 * 版本 >= 0
	 */
	 private String cssFiles = new String();

	/**
	 * 所属分类
	 *
	 * 版本 >= 0
	 */
	 private int classId;

	/**
	 * 页面类型
	 *
	 * 版本 >= 0
	 */
	 private int pageTpe;

	/**
	 * 创建时间:YYYY-MM-DD HH:MM:SS
	 *
	 * 版本 >= 0
	 */
	 private String createTime = new String();

	/**
	 * 最后更新时间:YYYY-MM-DD HH:MM:SS
	 *
	 * 版本 >= 0
	 */
	 private String lastMdifyTime = new String();

	/**
	 * 状态
	 *
	 * 版本 >= 0
	 */
	 private int status;

	/**
	 * 备注
	 *
	 * 版本 >= 0
	 */
	 private String comment = new String();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(version);
		bs.pushString(tempId);
		bs.pushString(tempName);
		bs.pushInt(type);
		bs.pushString(content);
		bs.pushString(cssFiles);
		bs.pushInt(classId);
		bs.pushInt(pageTpe);
		bs.pushString(createTime);
		bs.pushString(lastMdifyTime);
		bs.pushInt(status);
		bs.pushString(comment);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		tempId = bs.popString();
		tempName = bs.popString();
		type = bs.popInt();
		content = bs.popString();
		cssFiles = bs.popString();
		classId = bs.popInt();
		pageTpe = bs.popInt();
		createTime = bs.popString();
		lastMdifyTime = bs.popString();
		status = bs.popInt();
		comment = bs.popString();
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取模版ID
	 * 
	 * 此字段的版本 >= 0
	 * @return tempId value 类型为:String
	 * 
	 */
	public String getTempId()
	{
		return tempId;
	}


	/**
	 * 设置模版ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setTempId(String value)
	{
		if (value != null) {
				this.tempId = value;
		}else{
				this.tempId = new String();
		}
	}


	/**
	 * 获取模版名称
	 * 
	 * 此字段的版本 >= 0
	 * @return tempName value 类型为:String
	 * 
	 */
	public String getTempName()
	{
		return tempName;
	}


	/**
	 * 设置模版名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setTempName(String value)
	{
		if (value != null) {
				this.tempName = value;
		}else{
				this.tempName = new String();
		}
	}


	/**
	 * 获取适用版本:1、wap2 2、touch
	 * 
	 * 此字段的版本 >= 0
	 * @return type value 类型为:int
	 * 
	 */
	public int getType()
	{
		return type;
	}


	/**
	 * 设置适用版本:1、wap2 2、touch
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setType(int value)
	{
		this.type = value;
	}


	/**
	 * 获取模版内容
	 * 
	 * 此字段的版本 >= 0
	 * @return content value 类型为:String
	 * 
	 */
	public String getContent()
	{
		return content;
	}


	/**
	 * 设置模版内容
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setContent(String value)
	{
		if (value != null) {
				this.content = value;
		}else{
				this.content = new String();
		}
	}


	/**
	 * 获取包含Css文件
	 * 
	 * 此字段的版本 >= 0
	 * @return cssFiles value 类型为:String
	 * 
	 */
	public String getCssFiles()
	{
		return cssFiles;
	}


	/**
	 * 设置包含Css文件
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCssFiles(String value)
	{
		if (value != null) {
				this.cssFiles = value;
		}else{
				this.cssFiles = new String();
		}
	}


	/**
	 * 获取所属分类
	 * 
	 * 此字段的版本 >= 0
	 * @return classId value 类型为:int
	 * 
	 */
	public int getClassId()
	{
		return classId;
	}


	/**
	 * 设置所属分类
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setClassId(int value)
	{
		this.classId = value;
	}


	/**
	 * 获取页面类型
	 * 
	 * 此字段的版本 >= 0
	 * @return pageTpe value 类型为:int
	 * 
	 */
	public int getPageTpe()
	{
		return pageTpe;
	}


	/**
	 * 设置页面类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPageTpe(int value)
	{
		this.pageTpe = value;
	}


	/**
	 * 获取创建时间:YYYY-MM-DD HH:MM:SS
	 * 
	 * 此字段的版本 >= 0
	 * @return createTime value 类型为:String
	 * 
	 */
	public String getCreateTime()
	{
		return createTime;
	}


	/**
	 * 设置创建时间:YYYY-MM-DD HH:MM:SS
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCreateTime(String value)
	{
		if (value != null) {
				this.createTime = value;
		}else{
				this.createTime = new String();
		}
	}


	/**
	 * 获取最后更新时间:YYYY-MM-DD HH:MM:SS
	 * 
	 * 此字段的版本 >= 0
	 * @return lastMdifyTime value 类型为:String
	 * 
	 */
	public String getLastMdifyTime()
	{
		return lastMdifyTime;
	}


	/**
	 * 设置最后更新时间:YYYY-MM-DD HH:MM:SS
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setLastMdifyTime(String value)
	{
		if (value != null) {
				this.lastMdifyTime = value;
		}else{
				this.lastMdifyTime = new String();
		}
	}


	/**
	 * 获取状态
	 * 
	 * 此字段的版本 >= 0
	 * @return status value 类型为:int
	 * 
	 */
	public int getStatus()
	{
		return status;
	}


	/**
	 * 设置状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setStatus(int value)
	{
		this.status = value;
	}


	/**
	 * 获取备注
	 * 
	 * 此字段的版本 >= 0
	 * @return comment value 类型为:String
	 * 
	 */
	public String getComment()
	{
		return comment;
	}


	/**
	 * 设置备注
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setComment(String value)
	{
		if (value != null) {
				this.comment = value;
		}else{
				this.comment = new String();
		}
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		if (value != null) {
				this.reserved = value;
		}else{
				this.reserved = new String();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(TempInfo)
				length += 4;  //计算字段version的长度 size_of(int)
				length += ByteStream.getObjectSize(tempId);  //计算字段tempId的长度 size_of(String)
				length += ByteStream.getObjectSize(tempName);  //计算字段tempName的长度 size_of(String)
				length += 4;  //计算字段type的长度 size_of(int)
				length += ByteStream.getObjectSize(content);  //计算字段content的长度 size_of(String)
				length += ByteStream.getObjectSize(cssFiles);  //计算字段cssFiles的长度 size_of(String)
				length += 4;  //计算字段classId的长度 size_of(int)
				length += 4;  //计算字段pageTpe的长度 size_of(int)
				length += ByteStream.getObjectSize(createTime);  //计算字段createTime的长度 size_of(String)
				length += ByteStream.getObjectSize(lastMdifyTime);  //计算字段lastMdifyTime的长度 size_of(String)
				length += 4;  //计算字段status的长度 size_of(int)
				length += ByteStream.getObjectSize(comment);  //计算字段comment的长度 size_of(String)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
