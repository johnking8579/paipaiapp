package com.qq.qqbuy.thirdparty.idl.cms;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.BizConstant;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.cms.protocol.GetAdvertReq;
import com.qq.qqbuy.thirdparty.idl.cms.protocol.GetAdvertResp;
import com.qq.qqbuy.thirdparty.idl.cms.protocol.GetCmsCssReq;
import com.qq.qqbuy.thirdparty.idl.cms.protocol.GetCmsCssResp;
import com.qq.qqbuy.thirdparty.idl.cms.protocol.GetCmsTpltReq;
import com.qq.qqbuy.thirdparty.idl.cms.protocol.GetCmsTpltResp;
import com.qq.qqbuy.thirdparty.idl.cms.protocol.GetItemPoolBatchedReq;
import com.qq.qqbuy.thirdparty.idl.cms.protocol.GetItemPoolBatchedResp;
import com.qq.qqbuy.thirdparty.idl.cms.protocol.GetItemPoolReq;
import com.qq.qqbuy.thirdparty.idl.cms.protocol.GetItemPoolResp;
import com.qq.qqbuy.thirdparty.idl.cms.protocol.ItemInfo;

public class PPCmsClient extends SupportIDLBaseClient{
	
	private static final int CMS_PORT = 9022;
	
	public static GetAdvertResp getAdvertInfo(long advertPosId, String itemCode, String key, String searchPath) {
		long start = System.currentTimeMillis();
		GetAdvertResp resp = new GetAdvertResp();
		GetAdvertReq req = new GetAdvertReq();
		req.setAdposId(advertPosId);
		req.setItemCode(itemCode);
		req.setKey(key);
		req.setSearchPath(searchPath);
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
	    int ret = -1;
	    if(EnvManager.isIdc())
	    {
	    	ret = invokePaiPaiIDL(req, resp, stub);
		}
	    else
		{
	    	ret = invokePaiPaiIDL(req, resp, stub,GAMMA_IP_NEW, CMS_PORT);
		}
	    
		return resp;
	}
	
	public static GetAdvertResp getAdvertInfo(long advertPosId, String itemCode, String key, String searchPath, String channelId, int scene) {
		Log.run.debug("IDL#getAdvertInfo begin : " +  advertPosId + "|" + itemCode + "|" + key + "|" + searchPath + "|" + channelId + "|" + scene);
		long start = System.currentTimeMillis();
		GetAdvertResp resp = new GetAdvertResp();
		GetAdvertReq req = new GetAdvertReq();
		req.setAdposId(advertPosId);
		req.setItemCode(itemCode);
		req.setKey(key);
		req.setSearchPath(searchPath);
		req.setInReserved(channelId);//当前的渠道号
		req.setScene(scene);	//场景
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
	    int ret = -1;
	    if(EnvManager.isIdc())
	    {
	        ret = invokePaiPaiIDL(req, resp, stub);
		}
	    else
		{
	    	ret = invokePaiPaiIDL(req, resp, stub,GAMMA_IP_NEW, CMS_PORT);
		}
		return resp;
	}
	
	
    public static GetCmsCssResp getCmsCss(String cssFiles, int type){
    	if(StringUtils.isEmpty(cssFiles)){
    		return null;
    	}
    	Log.run.debug("IDL#getCmsCss begin : " +  cssFiles + "|" + type);
        long start = System.currentTimeMillis();
        GetCmsCssReq req = new GetCmsCssReq();
		List<String> cssFileIds = new ArrayList<String>();
		String[] cssItems = cssFiles.split(",");
		for (String css : cssItems) {
			if(StringUtils.isNotEmpty(css)){
				cssFileIds.add(css);
			}
		}
		req.setCssFileIds(cssFileIds);
		req.setType(type);
		if("true".equals(System.getProperty("pp.cms.env.isPrew"))){
			req.setEnvType(BizConstant.ENV_TYPE_MIX);
		}else{
			req.setEnvType(BizConstant.ENV_TYPE_IDC);
		}
        GetCmsCssResp resp = new GetCmsCssResp();
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
        int ret = -1;
        if(EnvManager.isIdc())
        {
        	ret = invokePaiPaiIDL(req, resp, stub);
		}
        else
        {
			ret = invokePaiPaiIDL(req, resp, stub,GAMMA_IP_NEW, CMS_PORT);
		}
        try 
        {
			Util.decode(resp, BizConstant.OPENAPI_CHARSET);
//			for (CssFileInfo info : resp.getCssFileInfos()) {
//	        	Util.decode(info, BizConstant.OPENAPI_CHARSET);
//			}
		} catch (Exception e) {
			Log.run.error("resp decode error:" + e.getMessage());
		}
        return ret == SUCCESS ? resp : null;
    }

    public static GetCmsTpltResp getCmsTplt(String tempId, int type, String lastModifyTime){
    	if(StringUtils.isEmpty(tempId)){
    		return null;
    	}
    	Log.run.debug("IDL#getCmsTplt begin : " +  tempId + "|" + type + "|" + lastModifyTime);
        long start = System.currentTimeMillis();
        GetCmsTpltReq req = new GetCmsTpltReq();
		GetCmsTpltResp resp = new GetCmsTpltResp();
		req.setTempId(tempId);
		req.setType(type);
		req.setInReserved(lastModifyTime);
		if("true".equals(System.getProperty("pp.cms.env.isPrew"))){
			req.setEnvType(BizConstant.ENV_TYPE_MIX);
		}else{
			req.setEnvType(BizConstant.ENV_TYPE_IDC);
		}
        int ret = -1;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
        if(EnvManager.isIdc()){
        	ret = invokePaiPaiIDL(req, resp, stub);
		}else{
			ret = invokePaiPaiIDL(req, resp, stub,GAMMA_IP, CMS_PORT);
		}
        Log.run.debug("tplt resp=" + resp + ",ret=" + ret);
        try {
			Util.decode(resp, BizConstant.OPENAPI_CHARSET);
			//Util.decode(resp.getTempInfo(), BizConstant.OPENAPI_CHARSET);
		} catch (Exception e) {
			Log.run.error("resp decode error:" + e.getMessage());
		}
        return ret == SUCCESS ? resp : null;
    }
    
    public static GetItemPoolResp getItemPool(String poolId, int itemNum){
    	if(StringUtils.isEmpty(poolId)){
    		return null;
    	}
    	Log.run.debug("IDL#getItemPool begin : " +  poolId + "|" + itemNum);
        long start = System.currentTimeMillis();
        GetItemPoolReq req = new GetItemPoolReq();
        GetItemPoolResp resp = new GetItemPoolResp();
		req.setPoolId(poolId);
        int ret = -1;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
        if(EnvManager.isIdc()){
        	ret = invokePaiPaiIDL(req, resp,stub);
		}else{
			ret = invokePaiPaiIDL(req, resp, stub,GAMMA_IP_NEW, CMS_PORT);
			//ret = invokePaiPaiIDL(req, resp,stub);
		}
        try {
			Util.decode(resp, BizConstant.OPENAPI_CHARSET);
			if(resp != null){
				for (ItemInfo info : resp.getItemList()) {
					Util.decode(info, BizConstant.OPENAPI_CHARSET);
				}
			}
		} catch (Exception e) {
			Log.run.error("resp decode error:" + e.getMessage());
		}
        return ret == SUCCESS ? resp : null;
    }
    
    public static GetItemPoolBatchedResp getItemPoolBatched(Map<String,Integer> poolIds){
    	if(poolIds.size() < 0){
    		return null;
    	}
        long start = System.currentTimeMillis();
        GetItemPoolBatchedReq req = new GetItemPoolBatchedReq();
        GetItemPoolBatchedResp resp = new GetItemPoolBatchedResp();
		req.setPoolIds(poolIds);
        int ret = -1;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
        if(EnvManager.isIdc()){
        	ret = invokePaiPaiIDL(req, resp,stub);
		}else{
			ret = invokePaiPaiIDL(req, resp,stub, GAMMA_IP_NEW, CMS_PORT);
		}
        long timeCost = System.currentTimeMillis() - start;
        return ret == SUCCESS ? resp : null;
    }
    
    public static void main(String args[]) throws UnsupportedEncodingException{
    	System.out.println(URLDecoder.decode("var y=8+1", "utf-8"));
    }
    /**
     * 是否需要上报
     * @param ret
     * @param err
     * @return
     */
    protected  static boolean needsAlarm(int ret, long err){		
    	//appj_qqbuy_cms自定义的错误码  	
    	boolean needAlram=ret!=0 || err!=0;//
    	if(needAlram){
    		if(err==0x202 || err==0x0101){
    			needAlram=false;
    		}
    	}
    	return needAlram;
    }
}
