package com.qq.qqbuy.thirdparty.idl.verifycode;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.paipai.component.c2cplatform.AsynWebStubException;
import com.paipai.component.c2cplatform.IAsynWebStub;
import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.qq.ptlogin2.CmlbAccess;
import com.qq.ptlogin2.PtloginApi;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.verifycode.protocol.ApiLoginPo;
import com.qq.qqbuy.thirdparty.idl.verifycode.protocol.ApiLoginReq;
import com.qq.qqbuy.thirdparty.idl.verifycode.protocol.ApiLoginResp;
import com.qq.qqbuy.thirdparty.idl.verifycode.protocol.CheckLoginReq;
import com.qq.qqbuy.thirdparty.idl.verifycode.protocol.CheckLoginResp;
import com.qq.qqbuy.thirdparty.idl.verifycode.protocol.CheckVerifyCodeReq;
import com.qq.qqbuy.thirdparty.idl.verifycode.protocol.CheckVerifyCodeResp;
import com.qq.qqbuy.thirdparty.idl.verifycode.protocol.GetVerifyCodeReq;
import com.qq.qqbuy.thirdparty.idl.verifycode.protocol.GetVerifyCodeResp;

/**
 * @author winsonwu
 * @Created 2012-2-1
 */
public class VerifyCodeClient extends SupportIDLBaseClient
{
    public static final VerifyCodeClient INSTANCE = new VerifyCodeClient();

    protected Logger logger = LogManager.getLogger();
    
    private final int  appId=17000101;
    
    private final int  cmlbId=5258;
    
    static {
    	if(EnvManager.isJd())	{	//京东下直接加载so文件,注意ptlogin4.so没有其它依赖库.否则要加到java.library.path中,京东默认java.library.path=/usr/local/lib
    		String sopath = "so/ptlogin4.so";
    		System.load(Thread.currentThread().getContextClassLoader().getResource(sopath).getPath());
    	} else	{
	        try {
	     	   System.loadLibrary("ptlogin4");	//依赖系统变量LD_LIBRARY_PATH=/somedir. 对应的工程文件为WebRoot/so/lib*.so
	 		} catch (Throwable e) {		//有可能抛出Error, 不是Exception
	 			Log.run.error("====load=====ptlogin4===error="+e.getMessage(), e);
	 		}
    	}
     }
    
  
    private VerifyCodeClient()    {}
    
    public boolean CheckLoginApp(long uid,String skey,
            String serviceName, int connectTimeout, int timeout)
    {
        Log.run.info("VerifyCodeClient#checkLogin-start: uid:" + uid+" lkey: "+skey);
        // 参数检查
        if (uid == 0 || skey == null || skey == "")
        {
        	Log.run.error(" the argument is not valid: " + uid+" lkey: "+skey);
            return false;
        }
        try {
            PtloginApi api = PtloginApi.getPtloginApi();
			if (api == null)
			{
				Log.run.error("Api init is null:");
			    return false;
			}
			 CmlbAccess cmlb = CmlbAccess.getCmlbAccess(cmlbId);
			Log.run.info("==========newCmlbAccess"+uid+"=========: ");
			if (cmlb ==null)
			{
				Log.run.error("CMLB init is null: ");
			    return false;
			}
			 Log.run.info("==========serviceIp"+uid+"=========: "+cmlb.GetSvrAddr());
			int  ret = api.SessVerifyLowLogin(appId, uid, skey, 0, 0, 0, cmlb.GetSvrAddr(), 5000);
			if (ret != 0)
			{
				Log.run.error("SessVerify  failed: ret=" + ret+" errorMsg: "+api.get_errmsg());
			}else{
				 if (1 == api.get_result()){
					 Log.run.info("===================login check is: "+api.get_errmsg());
					 return true;
				 }else{
					 Log.run.error("SessVerify  failed: " + api.get_result()+" errorMsg: "+api.get_errmsg());
				 }
			}
		} catch (Exception e) {
			Log.run.error("call CmlbAccess or SessVerify is error:"+e.getMessage());
		}
        return false;
    }
    public boolean CheckLogin(CheckLoginReq req, CheckLoginResp resp,
            String serviceName, int connectTimeout, int timeout)
    {
        long startTime = System.currentTimeMillis();
        Log.run.debug("VerifyCodeClient#checkLogin-start: " + req.toString());

        // 参数检查
        if (req == null || req.getCommParm() == null)
        {
            logger.error(getBusinesModule(), " the argument is not valid: "+ req.toString());
            return false;
        }

        // 调用远程服务
        IAsynWebStub cntl = new AsynWebStub();
        cntl.setSvc(serviceName);
        if (req.getCommParm().getUin() > 0)
            cntl.setUin(req.getCommParm().getUin());
        else
            cntl.setUin(startTime);
        cntl.setTimeout(connectTimeout, timeout);
        int retCode = 0;
        try
        {
            retCode = cntl.invoke(req, resp);

        } catch (Exception e)
        {
            logger.error(getBusinesModule(), e);
            retCode = -1;
        }

        long costTime = System.currentTimeMillis() - startTime;

        Log.run.debug("VerifyCodeClient#checkLogin-end: req:" + req.toString()
                + " resp:" + resp.toString() + " startTime:" + startTime
                + " costTime:" + costTime);

        if (resp.getResult() == 0)
        {
            return true;
        }
        return false;
    }

    public boolean CheckVerifyCode(CheckVerifyCodeReq req,
            CheckVerifyCodeResp resp, long appId, String serviceName,
            int connectTimeout, int timeout)
    {
        long startTime = System.currentTimeMillis();
        Log.run.debug("start-- " + req.toString());
        req.setAppId(appId);

        // 参数检查
        if (req == null || req.getCommParm() == null
                || req.getCommParm().getUin() < 10000 || req.getCapType() < 0
                || req.getCode() == null || req.getCode().trim().length() < 4)
        {
            logger.error(getBusinesModule(), " the argument is not valid: "+ req.toString());
            return false;
        }

        // 调用远程服务
        IAsynWebStub cntl = new AsynWebStub();
        cntl.setSvc(serviceName);
        cntl.setUin(req.getCommParm().getUin());
        cntl.setTimeout(connectTimeout, timeout);
        int retCode = 0;
        try
        {
            cntl.invoke(req, resp);

        } catch (Exception e)
        {
            logger.error(getBusinesModule(), e);
            retCode = -1;
        }

        Log.run.debug(resp.toString());
        long costTime = System.currentTimeMillis() - startTime;

        if (resp.getResult() == 0)
        {
            return true;
        }

        return false;
    }

    public boolean GetVerifyCode(GetVerifyCodeReq req, GetVerifyCodeResp resp,
            long appId, String serviceName, int connectTimeout, int timeout)
    {
        long startTime = System.currentTimeMillis();

        Log.run.debug("start-- " + req.toString());
        req.setAppId(appId);
        // 参数检查
        if (req == null || req.getCommParm() == null
                || req.getCommParm().getUin() < 10000 || req.getCapType() < 0
                || req.getPicType() < 0)
        {
            logger.error(getBusinesModule(), " the argument is not valid: "
                            + req.toString());
            return false;
        }

        // 调用远程服务
        IAsynWebStub cntl = new AsynWebStub();
        cntl.setSvc(serviceName);
        cntl.setUin(req.getCommParm().getUin());
        cntl.setTimeout(connectTimeout, timeout);
        int retCode = 0;
        try
        {
            cntl.invoke(req, resp);

        } catch (Exception e)
        {
            logger.error(getBusinesModule(),
                    e);
            retCode = -1;
        }

        Log.run.debug(resp.toString());
        long costTime = System.currentTimeMillis() - startTime;

        if (resp.getResult() == 0)
        {
            return true;
        }
        return false;
    }

    /**
     * 
     * @Title: genSkey
     * @Description: 拿lskey去基础组那边换skey
     * @param uin
     *            用户qq号码
     * @param lskey
     *            弱登陆态
     * @param skey
     *            这边生成的skey
     * @param mk
     *            这边用于生成skey相关的信息
     * 
     * @return 设定文件
     * @return String 返回类型
     * @throws
     */
    public String genSkey(long uin, String authCode, String skey, String mk, int type)
    {
        String retStr = null;
        int ret = -1;
        if (skey != null && authCode != null && mk != null)
        {
            ApiLoginReq req = new ApiLoginReq();
            ApiLoginResp resp = new ApiLoginResp();
            ApiLoginPo po = new ApiLoginPo();
            po.setAuthCode(authCode);
            po.setLoginIntensityType(4L);
            //authType:
            //1:代表是uk里面的lskey，那么authCode就填lskey
            //2:代表通过sid验证，那么authCode就填sid
            po.setAuthType(type);
            po.setUin(uin);
            req.setMachineKey(mk);
            req.setSource("MobileGetSkey");
            req.setLoginFilter(po);
            
            // 2、调用协议
            IAsynWebStub webStubCntl = WebStubFactory.getWebStub4PaiPaiGBK();
            webStubCntl.setOperator(uin);
            webStubCntl.setSkey(skey.getBytes());
            try
            {
                ret = webStubCntl.invoke(req, resp);
                if(ret == 0 && resp != null)
                {
                    retStr = resp.getKey();    
                }
            } catch (AsynWebStubException e)
            {
            }
        }
        return retStr;
    }

    public String getBusinesModule()
    {
//        String packageAndClass = getClass().getName();
//        return LoggerUtil.getBusiModuleInPackage(packageAndClass, logger);
    	return getClass().getName();
    }

    protected boolean needAlarm(int ret, long err)
    {
        if (ret == SUCCESS)
        {
            return false;
        } else
        {
            return true;
        }
    }
}
