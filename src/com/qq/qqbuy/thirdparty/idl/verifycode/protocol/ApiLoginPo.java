//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.b2b2c.user.idl.ApiLoginReq.java

package com.qq.qqbuy.thirdparty.idl.verifycode.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *登录po
 *
 *@date 2014-10-16 02:13:32
 *
 *@since version:0
*/
public class ApiLoginPo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 1;

	/**
	 * QQ号
	 *
	 * 版本 >= 0
	 */
	 private long uin;

	/**
	 * 鉴权字符串，必填。调用方标识已登录的字符串，微信登录可填tiket或token
	 *
	 * 版本 >= 0
	 */
	 private String authCode = new String();

	/**
	 * 鉴权类型，1-lskey 2-无线sid验证 3-微购uin+ticket 4-工号权限系统 5-微信ticket 6-mp session 7-微购通过统一后台登录
	 *
	 * 版本 >= 0
	 */
	 private long authType;

	/**
	 * appid，authType填5、6、7时必填
	 *
	 * 版本 >= 0
	 */
	 private String appid = new String();

	/**
	 * authCode补充字段，用于某些场景必填，比如authType填6时，填pluginid， authType填7时，填微信openid
	 *
	 * 版本 >= 1
	 */
	 private String extraAuthCode = new String();

	/**
	 * 登录强度类型，0--默认，普通登录（skey有效期50分钟） 1--网站弱登录（目前不支持），2--无线弱登录(skey有效期30天)，
	 *
	 * 版本 >= 1
	 */
	 private long loginIntensityType;

	/**
	 * 预留字段
	 *
	 * 版本 >= 1
	 */
	 private String reserve1 = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushLong(uin);
		bs.pushString(authCode);
		bs.pushUInt(authType);
		bs.pushString(appid);
		if(  this.version >= 1 ){
				bs.pushString(extraAuthCode);
		}
		if(  this.version >= 1 ){
				bs.pushUInt(loginIntensityType);
		}
		if(  this.version >= 1 ){
				bs.pushString(reserve1);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		uin = bs.popLong();
		authCode = bs.popString();
		authType = bs.popUInt();
		appid = bs.popString();
		if(  this.version >= 1 ){
				extraAuthCode = bs.popString();
		}
		if(  this.version >= 1 ){
				loginIntensityType = bs.popUInt();
		}
		if(  this.version >= 1 ){
				reserve1 = bs.popString();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return uin;
	}


	/**
	 * 设置QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.uin = value;
	}


	/**
	 * 获取鉴权字符串，必填。调用方标识已登录的字符串，微信登录可填tiket或token
	 * 
	 * 此字段的版本 >= 0
	 * @return authCode value 类型为:String
	 * 
	 */
	public String getAuthCode()
	{
		return authCode;
	}


	/**
	 * 设置鉴权字符串，必填。调用方标识已登录的字符串，微信登录可填tiket或token
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAuthCode(String value)
	{
		this.authCode = value;
	}


	/**
	 * 获取鉴权类型，1-lskey 2-无线sid验证 3-微购uin+ticket 4-工号权限系统 5-微信ticket 6-mp session 7-微购通过统一后台登录
	 * 
	 * 此字段的版本 >= 0
	 * @return authType value 类型为:long
	 * 
	 */
	public long getAuthType()
	{
		return authType;
	}


	/**
	 * 设置鉴权类型，1-lskey 2-无线sid验证 3-微购uin+ticket 4-工号权限系统 5-微信ticket 6-mp session 7-微购通过统一后台登录
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAuthType(long value)
	{
		this.authType = value;
	}


	/**
	 * 获取appid，authType填5、6、7时必填
	 * 
	 * 此字段的版本 >= 0
	 * @return appid value 类型为:String
	 * 
	 */
	public String getAppid()
	{
		return appid;
	}


	/**
	 * 设置appid，authType填5、6、7时必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAppid(String value)
	{
		this.appid = value;
	}


	/**
	 * 获取authCode补充字段，用于某些场景必填，比如authType填6时，填pluginid， authType填7时，填微信openid
	 * 
	 * 此字段的版本 >= 1
	 * @return extraAuthCode value 类型为:String
	 * 
	 */
	public String getExtraAuthCode()
	{
		return extraAuthCode;
	}


	/**
	 * 设置authCode补充字段，用于某些场景必填，比如authType填6时，填pluginid， authType填7时，填微信openid
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:String
	 * 
	 */
	public void setExtraAuthCode(String value)
	{
		this.extraAuthCode = value;
	}


	/**
	 * 获取登录强度类型，0--默认，普通登录（skey有效期50分钟） 1--网站弱登录（目前不支持），2--无线弱登录(skey有效期30天)，
	 * 
	 * 此字段的版本 >= 1
	 * @return loginIntensityType value 类型为:long
	 * 
	 */
	public long getLoginIntensityType()
	{
		return loginIntensityType;
	}


	/**
	 * 设置登录强度类型，0--默认，普通登录（skey有效期50分钟） 1--网站弱登录（目前不支持），2--无线弱登录(skey有效期30天)，
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:long
	 * 
	 */
	public void setLoginIntensityType(long value)
	{
		this.loginIntensityType = value;
	}


	/**
	 * 获取预留字段
	 * 
	 * 此字段的版本 >= 1
	 * @return reserve1 value 类型为:String
	 * 
	 */
	public String getReserve1()
	{
		return reserve1;
	}


	/**
	 * 设置预留字段
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserve1(String value)
	{
		this.reserve1 = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ApiLoginPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 17;  //计算字段uin的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(authCode, null);  //计算字段authCode的长度 size_of(String)
				length += 4;  //计算字段authType的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(appid, null);  //计算字段appid的长度 size_of(String)
				if(  this.version >= 1 ){
						length += ByteStream.getObjectSize(extraAuthCode, null);  //计算字段extraAuthCode的长度 size_of(String)
				}
				if(  this.version >= 1 ){
						length += 4;  //计算字段loginIntensityType的长度 size_of(uint32_t)
				}
				if(  this.version >= 1 ){
						length += ByteStream.getObjectSize(reserve1, null);  //计算字段reserve1的长度 size_of(String)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(ApiLoginPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 17;  //计算字段uin的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(authCode, encoding);  //计算字段authCode的长度 size_of(String)
				length += 4;  //计算字段authType的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(appid, encoding);  //计算字段appid的长度 size_of(String)
				if(  this.version >= 1 ){
						length += ByteStream.getObjectSize(extraAuthCode, encoding);  //计算字段extraAuthCode的长度 size_of(String)
				}
				if(  this.version >= 1 ){
						length += 4;  //计算字段loginIntensityType的长度 size_of(uint32_t)
				}
				if(  this.version >= 1 ){
						length += ByteStream.getObjectSize(reserve1, encoding);  //计算字段reserve1的长度 size_of(String)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本1所包含的字段*******
 *	long version;///<版本号
 *	long uin;///<QQ号
 *	String authCode;///<鉴权字符串，必填。调用方标识已登录的字符串，微信登录可填tiket或token
 *	long authType;///<鉴权类型，1-lskey 2-无线sid验证 3-微购uin+ticket 4-工号权限系统 5-微信ticket 6-mp session 7-微购通过统一后台登录
 *	String appid;///<appid，authType填5、6、7时必填
 *	String extraAuthCode;///<authCode补充字段，用于某些场景必填，比如authType填6时，填pluginid， authType填7时，填微信openid
 *	long loginIntensityType;///<登录强度类型，0--默认，普通登录（skey有效期50分钟） 1--网站弱登录（目前不支持），2--无线弱登录(skey有效期30天)，
 *	String reserve1;///<预留字段
 *****以上是版本1所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
