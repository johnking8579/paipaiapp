

//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang

package com.qq.qqbuy.thirdparty.idl.verifycode.protocol;


import com.paipai.util.annotation.Protocol;
import com.paipai.netframework.kernal.NetAction;


import com.paipai.lang.GenericWrapper;/**
 *
 *NetAction类
 *
 */
public class UserLoginApiAo  extends NetAction
{
	@Override
	 public int getSnowslideThresold(){
		// 这里设置防雪崩时间，也就是超时时间值
		  return 2;
	 }




	@Protocol(cmdid = 0xA0A81801L, desc = "api统一登录接口，返回skey和uid。在校验authcode之前，会先校验rCntlInfo中OperatorKey（由rCntlInfo中OperatorUin+strMachineKey加密得到）", export = true)
	 public ApiLoginResp ApiLogin(ApiLoginReq req){
		ApiLoginResp resp = new  ApiLoginResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }



}
