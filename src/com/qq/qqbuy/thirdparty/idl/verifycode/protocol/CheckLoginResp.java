 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.verifycode.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.netframework.kernal.NetMessage;

/**
 * 检查登陆回复
 *
 *@date 2011-09-29 10:15::58
 *
 *@since version:0
*/
public class  CheckLoginResp extends NetMessage
{
	/**
	 * 返回信息
	 *
	 * 版本 >= 0
	 */
	 private String retMsg = new String();
	 private byte[] a8 = null;
	 private byte[] crtTime = null;


	public byte[] getA8()
    {
        return a8;
    }

    public void setA8(byte[] a8)
    {
        this.a8 = a8;
    }

    public byte[] getCrtTime()
    {
        return crtTime;
    }

    public void setCrtTime(byte[] crtTime)
    {
        this.crtTime = crtTime;
    }

    public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushString(retMsg);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		retMsg = bs.popString();
		int size = (int)bs.popUInt();
		a8 = bs.popBytes(size);
		size = (int)bs.popUInt();
		crtTime = bs.popBytes(size);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x91068803L;
	}


	/**
	 * 获取返回信息
	 * 
	 * 此字段的版本 >= 0
	 * @return retMsg value 类型为:String
	 * 
	 */
	public String getRetMsg()
	{
		return retMsg;
	}


	/**
	 * 设置返回信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRetMsg(String value)
	{
		if (value != null) {
				this.retMsg = value;
		}else{
				this.retMsg = new String();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CheckLoginResp)
				length += ByteStream.getObjectSize(retMsg);  //计算字段retMsg的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return "result=" + result + " retMsg=" + retMsg;
	}
}
