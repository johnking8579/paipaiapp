 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.verifycode.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.netframework.kernal.NetMessage;
import com.qq.qqbuy.common.po.QQBuyServiceComm;

/**
 *检查登陆请求
 *
 *@date 2011-09-29 10:15::57
 *
 *@since version:0
*/
public class  CheckLoginReq extends NetMessage
{
	/**
	 * 包含来源，uin等信息
	 *
	 * 版本 >= 0
	 */
	 private QQBuyServiceComm commParm = new QQBuyServiceComm();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(commParm);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		commParm = (QQBuyServiceComm) bs.popObject(QQBuyServiceComm.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x91061803L;
	}


	/**
	 * 获取包含来源，uin等信息
	 * 
	 * 此字段的版本 >= 0
	 * @return commParm value 类型为:QQBuyServiceComm
	 * 
	 */
	public QQBuyServiceComm getCommParm()
	{
		return commParm;
	}


	/**
	 * 设置包含来源，uin等信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:QQBuyServiceComm
	 * 
	 */
	public void setCommParm(QQBuyServiceComm value)
	{
		if (value != null) {
				this.commParm = value;
		}else{
				this.commParm = new QQBuyServiceComm();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(CheckLoginReq)
				length += ByteStream.getObjectSize(commParm);  //计算字段commParm的长度 size_of(QQBuyServiceComm)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	 public String toString() {
	      return commParm.toString();
	 }
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
