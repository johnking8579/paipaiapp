package com.qq.qqbuy.thirdparty.idl.parseAttrText;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;
import java.util.Map;
import java.util.Vector;

public class COptionBo
  implements ICanSerializeObject
{
  private long attrId;
  private long id;
  private long type;
  private long property;
  private long order;
  private String name = new String();

  CSubAttrOption_t subAttrIds = new CSubAttrOption_t();
  private byte attrId_u;
  private byte id_u;
  private byte type_u;
  private byte property_u;
  private byte order_u;
  private byte name_u;
  private byte subAttrIds_u;

  public int getSize()
  {
    byte[] buffer = new byte[1048576];
    ByteStream bsDummy = new ByteStream(buffer, buffer.length);
    bsDummy.setRealWrite(false);
    try {
      serialize_i(bsDummy);
    }
    catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
    return bsDummy.getWrittenLength();
  }

  public int serialize(ByteStream bs) throws Exception
  {
    bs.pushUInt(getSize());
    serialize_i(bs);
    return bs.getWrittenLength();
  }

  private int serialize_i(ByteStream bs) throws Exception {
    bs.pushUInt(this.attrId);
    bs.pushUInt(this.id);
    bs.pushUInt(this.type);
    bs.pushUInt(this.property);
    bs.pushUInt(this.order);
    bs.pushString(this.name);
    this.subAttrIds.serialize(bs);
    return 0;
  }

  public int unSerialize(ByteStream bs) throws Exception
  {
    bs.popUInt();
    this.attrId = bs.popUInt();
    this.id = bs.popUInt();
    this.type = bs.popUInt();
    this.property = bs.popUInt();
    this.order = bs.popUInt();
    this.name = bs.popString();
    this.subAttrIds.unSerialize(bs);
    return 0;
  }

  public long getAttrId()
  {
    return this.attrId;
  }

  public void setAttrId(long attrId)
  {
    this.attrId = attrId;
    this.attrId_u = 1;
  }

  public boolean isAttrIdSet() {
    return this.attrId_u != 0;
  }

  public long getId()
  {
    return this.id;
  }

  public void setId(long id)
  {
    this.id = id;
    this.id_u = 1;
  }

  public boolean isIdSet() {
    return this.id_u != 0;
  }

  public long getType()
  {
    return this.type;
  }

  public void setType(long type)
  {
    this.type = type;
    this.type_u = 1;
  }

  public boolean isTypeSet() {
    return this.type_u != 0;
  }

  public long getProperty()
  {
    return this.property;
  }

  public void setProperty(long property)
  {
    this.property = property;
    this.property_u = 1;
  }

  public boolean isPropertySet() {
    return this.property_u != 0;
  }

  public long getOrder()
  {
    return this.order;
  }

  public void setOrder(long order)
  {
    this.order = order;
    this.order_u = 1;
  }

  public boolean isOrderSet() {
    return this.order_u != 0;
  }

  public String getName()
  {
    return this.name;
  }

  public void setName(String name)
  {
    if (name == null) {
      throw new NullPointerException();
    }
    this.name = name;
    this.name_u = 1;
  }

  public boolean isNameSet() {
    return this.name_u != 0;
  }

  public Map<Long, Vector<CSubAttrOption>> getSubAttrIds()
  {
    return this.subAttrIds.getSubAttrOptionMap();
  }

  public void setSubAttrIds(Map<Long, Vector<CSubAttrOption>> subAttrIds)
  {
    if (subAttrIds == null) {
      throw new NullPointerException();
    }
    this.subAttrIds.setSubAttrOptionMap(subAttrIds);
    this.subAttrIds_u = 1;
  }

  public boolean isSubAttrIdsSet() {
    return this.subAttrIds_u != 0;
  }
}