package com.qq.qqbuy.thirdparty.idl.parseAttrText;

import java.util.*;

import com.paipai.lang.*;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;
import com.paipai.util.annotation.Field;

/**
 * <code>nca_v3</code>类提供类目服务相关协议的接口描述， 用以生成相应的C++调用代码。
 * 
 * @author Doncheng
 * @version 3.0
 */

@HeadApiProtocol(cPlusNamespace = "b2b2c::nca::dao", needInit = true)
public class NcaDao {
	
	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "错误码")
	enum ERR
	{
		@Field(desc = "服务尚未就绪",	defaultValue="3584")	ERR_NOT_READY,	
		@Field(desc = "品类/导航id不存在",
										defaultValue="3585")	ERR_ID_NOT_EXIST, 
		@Field(desc = "属性检查失败",	defaultValue="3586")	ERR_ATTR_CHECK_FAIL, 
		@Field(desc = "属性串格式错误",	defaultValue="3587")	ERR_ATTR_PATTERN_ERR, 
		@Field(desc = "路径信息错误(不完整)",
										defaultValue="3588")	ERR_PATH_ERR, 
		@Field(desc = "发布属性不止一个",	defaultValue="3589")	ERR_MORE_ISSUE_ATTR, 
		@Field(desc = "属性实体不存在",	defaultValue="3590")	ERR_ATTR_NOT_EXIST, 
		@Field(desc = "名称搜索失败",	defaultValue="3591")	ERR_SEARCH_BY_KEY, 
		@Field(desc = "导航属性不存在",	defaultValue="3592")	ERR_NAVATTR_NOT_EXIST, 
		@Field(desc = "地图id不对",		defaultValue="3593")	ERR_MAPID_ERR, 
		@Field(desc = "功能不可用",		defaultValue="3594")	ERR_NOT_AVAILABLE, 
		@Field(desc = "APIControl错误",	defaultValue="3600")	ERR_INVALID_APICONTROL 
	};
	
	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "品类标记位")
	enum CLASS_PROPERTY
	{
		@Field(desc = "预删除", 			defaultValue="31")		CLASS_PROPERTY_PRE_DELETE,
		@Field(desc = "虚拟类目A", 		defaultValue="30")		CLASS_PROPERTY_VIRTUAL,
		@Field(desc = "半虚拟类目B",		defaultValue="29")		CLASS_PROPERTY_HALF_VIRTUAL,
		@Field(desc = "只能是5188来用", 	defaultValue="28")		CLASS_PROPERTY_5188,
		@Field(desc = "复合类目", 		defaultValue="27")		CLASS_PROPERTY_MULTI_PARENT,
		@Field(desc = "属性类目", 		defaultValue="26")		CLASS_PROPERTY_HAS_ATTR_INFO,
		@Field(desc = "成人用品类目", 	defaultValue="25")		CLASS_PROPERTY_SEX_TOOLS,
		@Field(desc = "自动发货类目", 	defaultValue="24")		CLASS_PROPERTY_AUTO_SEND,
		@Field(desc = "搜索属性屏蔽", 	defaultValue="23")		CLASS_PROPERTY_ATTRIBUTE_HIDDEN, 
		@Field(desc = "折叠", 			defaultValue="22")		CLASS_PROPERTY_NO_EXTRACT,
		@Field(desc = "腾讯相关商品", 	defaultValue="21")		CLASS_PROPERTY_TENCENT_PRODUCTS,
		@Field(desc = "一个类目只能发一个商品类目",
										defaultValue="20")		CLASS_PROPERTY_ONE_ITEM_PER_USER,
		@Field(desc = "拍下不买类目", 	defaultValue="19")		CLASS_PROPERTY_TAKE_WHEN_PAYED, 
		@Field(desc = "女装类目", 		defaultValue="18")		CLASS_PROPERTY_BEAUTY_LADY, 
		@Field(desc = "诚保代充", 		defaultValue="17")		CLASS_PROPERTY_FAST_SEND, 
		@Field(desc = "新手机类目", 		defaultValue="16")		CLASS_PROPERTY_MOBILE, 
		@Field(desc = "QCC 品类", 		defaultValue="12")		CLASS_PROPERTY_QCC, 
		@Field(desc = "二手商品支持诚保",defaultValue="11")		CLASS_PROPERTY_SECOND_SP_CHENGBAO, 
		@Field(desc = "游戏充值类目", 	defaultValue="10")		CLASS_PROPERTY_GAME_RECHARGE, 
		@Field(desc = "手机充值类目", 	defaultValue="9")		CLASS_PROPERTY_MOBIL_RECHARGE, 
		@Field(desc = "QQ充值类目", 		defaultValue="8")		CLASS_PROPERTY_QQ_RECHARGE, 
		@Field(desc = "只有城保或者商家认证用户才能发布",
										defaultValue="7")		CLASS_PROPERTY_ONLY_CHENGBAO_LICENCE,
		@Field(desc = "库存类目标记", 	defaultValue="6")		CLASS_PROPERTY_STOCK, 
		@Field(desc = "需要缴纳保证金类目",
										defaultValue="5")		CLASS_PROPERTY_NEED_PLEDGE,
		@Field(desc = "只允许主营类目为本类目的卖家发布",
										defaultValue="4")		CLASS_PROPERTY_ONLYMAINCLASSIN,
		@Field(desc = "c2c使用", 		defaultValue="3")		CLASS_PROPERTY_C2CCLASS,
		@Field(desc = "会员商城使用", 	defaultValue="2")		CLASS_PROPERTY_VIPMALLCLASS,
		@Field(desc = "spu使用", 		defaultValue="1")		CLASS_PROPERTY_SPUCLASS,
		@Field(desc = "字符串的第一位", 	defaultValue="32")		CLASS_PROPERTY_60W,  
		@Field(desc = "该品类不支持7天免邮包退",
										defaultValue="40")		CLASS_PROPERTY_NOT_7_DAYS_FREE_SECURITY,
	}

	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "属性项标记位")
	enum ATTR_PROPERTY
	{
		@Field(desc = "预删除",				defaultValue="33")	ATTR_PROPERTY_PREDELETE,
		@Field(desc = "选项型",				defaultValue="34")	ATTR_PROPERTY_OPTION,
		@Field(desc = "是否单选",			defaultValue="35")	ATTR_PROPERTY_SINGLE,
		@Field(desc = "关键属性",			defaultValue="36")	ATTR_PROPERTY_SPU_KEY,
		@Field(desc = "销售属性",			defaultValue="37")	ATTR_PROPERTY_SKU_SALE,
		@Field(desc = "一般属性",			defaultValue="38")	ATTR_PROPERTY_SKU_NORMAL,
		@Field(desc = "必填",				defaultValue="39")	ATTR_PROPERTY_MUST,
		@Field(desc = "支持卖家属性项别名",	defaultValue="40")	ATTR_PROPERTY_SELLER_ATTR_ALIAS,
		@Field(desc = "支持卖家属性值别名",	defaultValue="41")	ATTR_PROPERTY_SELLER_OPTION_ALIAS,
		@Field(desc = "卖家属性值别名以备注展现",defaultValue="42")ATTR_PROPERTY_SELLER_OPTION_SHOW_COMMENT,
	}

	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "APIControl的Charset定义")
	enum CHARSET
	{
		@Field(desc = "GBK", 			defaultValue="1")		CHARSET_GBK,
		@Field(desc = "UTF8", 			defaultValue="2")		CHARSET_UTF8,
	};

	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "APIControl的Source定义")
	enum SOURCE
	{
		@Field(desc = "易迅", 			defaultValue="1")		SOURCE_ICSON,
		@Field(desc = "拍拍", 			defaultValue="2")		SOURCE_PAIPAI,
	};

	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "APIControl的Option定义")
	enum OPTION
	{
		// ParseAttrText
		@Field(desc = "易迅自营发布商品",
										defaultValue="0")		OPTION_PARSEATTRTEXT_ICSON,
		@Field(desc = "易迅联营/拍拍发布商品",
										defaultValue="1")		OPTION_PARSEATTRTEXT_PUBLISH,
		@Field(desc = "商祥展现商品", 	defaultValue="2")		OPTION_PARSEATTRTEXT_SHOW,
		@Field(desc = "商祥展现商品，但屏蔽\"系统编号\"这个属性项",
										defaultValue="3")		OPTION_PARSEATTRTEXT_SHOW_20,
	};

	@ApiProtocol(cmdid = "0x536018FFL", desc = "模块可用性测试")
	class AvailableTest_5360
	{
	    class Req
	    {
	        @Field(desc = "打酱油")
	        uint8_t x;
	    }
	    
	    class Resp
	    {
	    }   
	}

	@ApiProtocol(cmdid = "0xA00118FFL", desc = "模块可用性测试")
	class AvailableTest_A001
	{
	    class Req
	    {
	        @Field(desc = "打酱油")
	        uint8_t x;
	    }
	    
	    class Resp
	    {
	    }   
	}
	
	/**
	* 属性值下子属性值对结构体
	* @author Doncheng
	* */
	@Member(cPlusNamespace = "c2cent::bo::nca_v3", desc = "属性值下子属性值对结构体", isSetClassSize = true)
	class SubAttrOption_v3
	{
		@Field(desc = "版本号, version需要小写")
		uint32_t    version;
	
		@Field(desc = "属性项id")
		uint32_t    AttrId;
		@Field(desc = "属性值id")
		uint32_t    OptionId;
		@Field(desc = "值对property")
		uint32_t    Property;
	}

	/**
	* 属性值结构体
	* @author Doncheng
	* */
	@Member(cPlusNamespace = "c2cent::bo::nca_v3", desc = "属性值结构体", isSetClassSize = true)
	class OptionBo_v3
	{
		@Field(desc = "版本号, version需要小写")
		uint32_t    version;
		
		@Field(desc = "属性项id")
		uint32_t    AttrId;
		@Field(desc = "属性值id")
		uint32_t    OptionId;   
		@Field(desc = "类型")
		uint32_t    Type;   
		@Field(desc = "property")
		uint32_t    Property;   
		@Field(desc = "属性值排序")
		uint32_t    Order;  
		@Field(desc = "属性值名称")
		String      Name;
		@Field(desc = "属性值下的子属性值对")
		Map<uint32_t, Vector<SubAttrOption_v3> > SubAttrIds;
	}

	/**
	* 导航属性项结构体
	* @author Doncheng
	* */
	@Member(cPlusNamespace = "c2cent::bo::nca_v3", desc = "导航属性项结构体", isSetClassSize = true)
	class AttrBo_v3
	{
		@Field(desc = "版本号, version需要小写")
		uint32_t    version;
		
		@Field(desc = "属性项id")
		uint32_t    AttrId;
		@Field(desc = "导航id")
		uint32_t    NavId;
		@Field(desc = "属性项名称")
		String      Name;
		@Field(desc = "property")
		uint32_t    Property;
		@Field(desc = "类型")
		uint32_t    Type;		
		@Field(desc = "父属性项id")
		uint32_t    PAttrId;
		@Field(desc = "父属性值id")
		uint32_t    POptionId;
		@Field(desc = "属性项描述")
		String      Desc;		
		@Field(desc = "属性项排序")
		uint32_t    Order;			
		@Field(desc = "属性值集合")
		Vector<OptionBo_v3>   Options;	
	}

	/**
	* 导航简易结构体
	* @author Doncheng
	* */
	@Member(cPlusNamespace = "c2cent::bo::nca_v3", desc = "导航简易结构体", isSetClassSize = true)
	class NavEntry_v3
	{
		@Field(desc = "版本号, version需要小写")
		uint32_t    version;
		
		@Field(desc = "导航id")
		uint32_t    NavId;
		@Field(desc = "地图id")
		uint32_t    MapId;      
		@Field(desc = "父导航id")
		uint32_t    PNavId;	
		@Field(desc = "导航名称")
		String      Name;
		@Field(desc = "导航类型")
		uint32_t    Type;
		@Field(desc = "导航分类")
		uint32_t    Catalog;
		@Field(desc = "备注")
		String      Note;
		@Field(desc = "排序字段")
		uint32_t    Order;	
		@Field(desc = "导航property")
		String      PropertyStr;
		@Field(desc = "搜索条件")
		String      SearchCond;
		@Field(desc = "是否有属性")
		uint32_t    HasAttr;
		
		@Field(desc = "导航预留自定义串1")
		String      CustomStr1;		
		@Field(desc = "导航预留自定义串2")
		String      CustomStr2;				
		@Field(desc = " metaclass type")
		uint32_t    MetaCatalog;			
		@Field(desc = "导航预留自定义整形字段2")
		uint32_t    CustomUint2;
	}

	/**
	* 导航结构体(不带属性部分)
	* @author Doncheng
	* */
	@Member(cPlusNamespace = "c2cent::bo::nca_v3", desc = "导航简易结构体", isSetClassSize = true)
	class NavBo_v3
	{
		@Field(desc = "版本号, version需要小写")
		uint32_t    version;

		@Field(desc = "导航信息")
		NavEntry_v3         NavNode;
	        
		@Field(desc = "导航路径")
		Vector<NavEntry_v3> FullPath;
		@Field(desc = "搜索导航路径")
		Vector<NavEntry_v3> MetaSearchPath;
		@Field(desc = "儿子导航")
		Vector<NavEntry_v3> ChildNode;
	}

	/**
	* 导航结构体(带属性部分)
	* @author Doncheng
	* */
	@Member(cPlusNamespace = "c2cent::bo::nca_v3", desc = "导航简易结构体", isSetClassSize = true)
	class NavBoEx_v3
	{
		@Field(desc = "版本号, version需要小写")
		uint32_t    version;

		@Field(desc = "导航信息")
		NavEntry_v3         NavNode;

		@Field(desc = "导航路径")
		Vector<NavEntry_v3> FullPath;
		@Field(desc = "搜索导航路径")
		Vector<NavEntry_v3> MetaSearchPath;
		@Field(desc = "儿子导航")
		Vector<NavEntry_v3> ChildNode;
	        
		@Field(desc = "直接儿子属性")
		Map<uint32_t, Vector<SubAttrOption_v3> > ChildAttrId;       
		@Field(desc = "属性字典")
		Map<uint32_t, AttrBo_v3> AttrDic;
	}

	/**
	* 排序导航结构体(带属性部分)
	* @author Quentinxu
	* */
	@Member(cPlusNamespace = "c2cent::bo::nca_v3", desc = "排序导航简易结构体", isSetClassSize = true)
	class OrderNavBoEx_v3
	{
		@Field(desc = "版本号, version需要小写")
		uint32_t    version;

		@Field(desc = "导航信息")
		NavEntry_v3         NavNode;

		@Field(desc = "导航路径")
		Vector<NavEntry_v3> FullPath;
		@Field(desc = "搜索导航路径")
		Vector<NavEntry_v3> MetaSearchPath;
		@Field(desc = "儿子导航")
		Vector<NavEntry_v3> ChildNode;

		@Field(desc = "直接儿子属性")
		Vector<Vector<SubAttrOption_v3> > ChildAttrId;      
		@Field(desc = "属性字典")
		Map<uint32_t, AttrBo_v3> AttrDic;
	}

	/**
	* 类目搜索结构体
	* @author Doncheng
	* */
	@Member(cPlusNamespace = "c2cent::bo::nca_v3", desc = "类目搜索结构体", isSetClassSize = true)
	class NavMatchKey_v3
	{
		@Field(desc = "版本号, version需要小写")
		uint32_t    version;

		@Field(desc = "导航id")
		uint32_t    NavId;  
		@Field(desc = "Matchinfo")
		String      MatchInfo;      
		@Field(desc = "PathInfo")
		String      PathInfo;   
		@Field(desc = "MatchType")
		uint32_t    MatchType;
	}

	/**
	* Publish Node 结构体
	* @author Doncheng
	* */
	@Member(cPlusNamespace = "c2cent::bo::nca_v3", desc = "Publish Node 结构体", isSetClassSize = true)
	class PublistNode_v3
	{
		@Field(desc = "版本号, version需要小写")
		uint32_t    version;

		@Field(desc = "导航id")
		uint32_t    NavId;      
		@Field(desc = "属性项id")
		uint32_t    AttrId;     
		@Field(desc = "属性值id")
		uint32_t    OptionId;       
		@Field(desc = "类型:1属性，3类目，4品类")
		uint32_t    Type;       
		@Field(desc = "name")
		String      Name;
		@Field(desc = "property")
		String      PropertyStr;
		@Field(desc = "是否有后继节点")
		uint32_t    AnyChildren;        
		@Field(desc = "sDesc")
		String      Desc;       
	}
	
	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "接口调用控制", isSetClassSize = true)
	class APIControl
	{
		@Field(desc = "版本号, version需要小写", defaultValue = "2")
		uint32_t    version;

		@Field(desc = "入参编码类型，1:GBK，2:UTF-8")
		uint32_t    Charset;

		@Field(desc = "来源，1:易讯，2：拍拍")
		uint32_t    Source;
		
		@Field(desc = "跟Source和具体接口有关的选项，具体看各个接口对于APIControl参数的说明（如果有的话）")
		uint32_t    Option;

		@Field(desc = "回参编码类型，0表示与入参编码类型相同，1:GBK，2:UTF-8", version=1)
		uint32_t    CharsetRsp;

		@Field(desc = "额外数据的业务id", version = 2)
		Set<uint32_t> ExtraId;
	}
	/**
	 * 属性值下子属性值对结构体
	 * 
	 * @author Doncheng
	 */
	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = " 属性值下子属性值对结构体 ", isSetClassSize = true)
	public class SubAttrOptionDdo {
		@Field(desc = " 版本号, version需要小写 ")
		uint8_t version;

		@Field(desc = " 属性项id ")
		uint32_t AttrId;

		@Field(desc = " 属性值id ")
		uint32_t OptionId;

		@Field(desc = " 值对property ")
		uint32_t Property;
	}

	/**
	 * 属性值结构体
	 * 
	 * @author Doncheng
	 */
	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = " 属性值结构体 ", isSetClassSize = true)
	public class OptionDdo {
		@Field(desc = " 版本号, version需要小写", defaultValue = "144")
		uint8_t version;

		@Field(desc = " 属性项id ")
		uint32_t AttrId;

		@Field(desc = " 属性值id ")
		uint32_t OptionId;

		@Field(desc = " 类型 ")
		uint32_t Type;

		@Field(desc = " property ")
		uint32_t Property;

		@Field(desc = " 属性值排序 ")
		uint32_t Order;

		@Field(desc = "属性值名称，按照优先级NameSeller>NameOperator>NameOriginal得到的综合结果")
		String Name;

		@Field(desc = " 属性值下的子属性值对 ")
		Map<uint32_t, Vector<SubAttrOptionDdo>> SubAttrIds;

		@Field(desc = "原始名", version = 142)
		String NameOriginal;

		@Field(desc = "运营加挂时定义的别名", version = 142)
		String NameOperator;

		@Field(desc = "卖家自定义名", version = 142)
		String NameSeller;

		@Field(desc = "对搜索导航有用，属性值备注（文本）", version = 143)
		String TextComment;

		@Field(desc = "对搜索导航有用，属性值备注（图片链接）", version = 143)
		String PictureComment;

		@Field(desc = "对搜索导航有用，属性值备注（知识库链接）", version = 143)
		String DetailComment;

		@Field(desc = "额外数据，key是业务id，value是业务数据", version = 144)
		Map<uint32_t, String> ExtraData;
	}

	/**
	 * 导航属性项结构体
	 * 
	 * @author Doncheng
	 */
	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = " 导航属性项结构体 ", isSetClassSize = true)
	public class AttrDdo {
		@Field(desc = " 版本号, version需要小写 ", defaultValue = "146")
		uint8_t version;

		@Field(desc = " 属性项id ")
		uint32_t AttrId;

		@Field(desc = "导航id")
		uint32_t NavId;

		@Field(desc = " 属性项名称，按照优先级NameSeller>NameOperator>NameOriginal得到的综合结果")
		String Name;

		@Field(desc = " property ")
		uint32_t Property;

		@Field(desc = " 类型 ")
		uint32_t Type;

		@Field(desc = "父属性项id")
		uint32_t PAttrId;

		@Field(desc = "父属性值id")
		uint32_t POptionId;

		@Field(desc = " 属性项描述 ")
		String Desc;

		@Field(desc = " 属性项排序 ")
		uint32_t Order;

		@Field(desc = " 属性值集合 ")
		Vector<OptionDdo> Options;
		
		@Field(desc = " 是否选项，0为否，其余为是 ", version = 140)
		uint32_t IsOptional;
		
		@Field(desc = " 是否文本，0为否，其余为是 ", version = 140)
		uint32_t IsText;
		
		@Field(desc = " 是否单选，0为否，其余为是 ", version = 140)
		uint32_t IsSingle;
		
		@Field(desc = " 是否多选，0为否，其余为是 ", version = 140)
		uint32_t IsMulti;
		
		@Field(desc = " 是否可选，0为否，其余为是 ", version = 140)
		uint32_t IsMust;
		
		@Field(desc = " 是否关键属性，0为否，其余为是 ", version = 140)
		uint32_t IsSpuKey;
		
		@Field(desc = " 是否SPU一般属性，0为否，其余为是 ", version = 140)
		uint32_t IsSpuComm;
		
		@Field(desc = " 是否销售属性，0为否，其余为是 ", version = 140)
		uint32_t IsSkuSale;
		
		@Field(desc = " 是否一般属性，0为否，其余为是 ", version = 140)
		uint32_t IsSkuComm;
		
		@Field(desc = " 是否搜索聚合属性，0为否，其余为是 ", version = 140)
		uint32_t IsSearchJoint;
		
		@Field(desc = " 是否商详隐藏属性，0为否，其余为是 ", version = 140)
		uint32_t IsHideSX;
		
		@Field(desc = " 是否支持卖家对属性项设置别名，0为否，其余为是 ", version = 141)
		uint32_t IsSellerAttrAlias;
		
		@Field(desc = " 是否支持卖家对属性值设置别名，0为否，其余为是 ", version = 141)
		uint32_t IsSellerOptAlias;

		@Field(desc = "原始名", version = 142)
		String NameOriginal;

		@Field(desc = "运营加挂时定义的别名", version = 142)
		String NameOperator;

		@Field(desc = "卖家自定义名", version = 142)
		String NameSeller;
		
		@Field(desc = "对搜索导航有用，属性值展现方式，0:不展示，1:显示图片，2:显示文字，3：显示图片和文字", version = 143)
		uint32_t ShowType;
		
		@Field(desc = "对品类有用，属性项分组id", version = 144)
		uint32_t GroupId;
		
		@Field(desc = "对品类有用，属性项分组名", version = 144)
		String GroupName;
		
		@Field(desc = "对品类有用，属性项分组排序", version = 144)
		uint32_t GroupOrder;

		@Field(desc = "额外数据，key是业务id，value是业务数据", version = 145)
		Map<uint32_t, String> ExtraData;

		@Field(desc = "标记位，具体定义参见ATTR_PROPERTY", bitset=128, version=146)
		BitSet PropertyMask;
	}

	/**
	 * 导航简易结构体
	 * 
	 * @author Doncheng
	 */
	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = " 导航简易结构体 ", isSetClassSize = true)
	public class NavEntryDdo {
		@Field(desc = " 版本号, version需要小写 ", defaultValue = "141")
		uint8_t version;

		@Field(desc = " 导航id ")
		uint32_t NavId;

		@Field(desc = "地图id")
		uint32_t MapId;      

		@Field(desc = " 父导航id ")
		uint32_t PNavId;

		@Field(desc = " 导航名称 ")
		String Name;

		@Field(desc = " 导航类型 ")
		uint32_t Type;

		@Field(desc = " 导航分类 ")
		uint32_t Catalog;

		@Field(desc = " 备注 ")
		String Note;

		@Field(desc = " 排序字段 ")
		uint32_t Order;

		@Field(desc = " 导航property ")
		String PropertyStr;

		@Field(desc = " 搜索条件 ")
		String SearchCond;

		@Field(desc = " 是否有属性 ")
		uint32_t HasAttr;

		@Field(desc = " 导航预留自定义串1 ")
		String CustomStr1;

		@Field(desc = " 导航预留自定义串2 ")
		String CustomStr2;

		@Field(desc = " 导航预留自定义整形字段1 ")
		uint32_t CustomUint1;

		@Field(desc = " 导航预留自定义整形字段2 ")
		uint32_t CustomUint2;
		
		@Field(desc = " 是否预删除，0为否，其余为是 ", version = 140)
		uint32_t IsPreDelete;
		
		@Field(desc = " 是否合作伙伴优先，0为否，其余为是 ", version = 140)
		uint32_t IsCooperatorFirst;

		@Field(desc = " 是否低价优先，0为否，其余为是 ", version = 140)
		uint32_t IsLowPriceFirst;
		
		@Field(desc = " 是否高价优先，0为否，其余为是 ", version = 140)
		uint32_t IsHighPriceFirst;

		@Field(desc = "标记位，具体定义参见CLASS_PROPERTY", bitset=128, version=141)
		BitSet PropertyMask;
	}

	/**
	 * 导航结构体(不带属性部分)
	 * 
	 * @author Doncheng
	 */
	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = " 导航简易结构体 ", isSetClassSize = true)
	public class NavDdo {
		@Field(desc = " 版本号, version需要小写 ")
		uint8_t version;

		@Field(desc = " 导航信息 ")
		NavEntryDdo NavNode;

		@Field(desc = " 导航路径 ")
		Vector<NavEntryDdo> FullPath;

		@Field(desc = " 搜索导航路径 ")
		Vector<NavEntryDdo> MetaSearchPath;

		@Field(desc = " 儿子导航 ")
		Vector<NavEntryDdo> ChildNode;
	}

	/**
	 * 导航结构体(带属性部分)
	 * 
	 * @author Doncheng
	 */
	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = " 导航结构体 ", isSetClassSize = true)
	public class NavExDdo {
		@Field(desc = " 版本号, version需要小写 ", defaultValue = "140")
		uint8_t version;

		@Field(desc = " 导航信息 ")
		NavEntryDdo NavNode;

		@Field(desc = " 导航路径 ")
		Vector<NavEntryDdo> FullPath;

		@Field(desc = " 搜索导航路径 ")
		Vector<NavEntryDdo> MetaSearchPath;

		@Field(desc = " 儿子导航 ")
		Vector<NavEntryDdo> ChildNode;

		@Field(desc = " 直接儿子属性 ")
		Map<uint32_t, Vector<SubAttrOptionDdo>> ChildAttrId;

		@Field(desc = " 属性字典 ")
		Map<uint32_t, AttrDdo> AttrDic;
		
		@Field(desc = "额外数据，key是业务id，value是业务数据", version = 140)
		Map<uint32_t, String> ExtraData;
	}

	/**
	 * 导航结构体适应搜索需求属性字典换成vector排序
	 * 
	 * @author jackyjiang
	 */
	
	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = " 导航结构体 ", isSetClassSize = true)
	public class NavExOrderDdo {
		@Field(desc = " 版本号, version需要小写 ")
		uint8_t version;

		@Field(desc = " 导航信息 ")
		NavEntryDdo NavNode;

		@Field(desc = " 导航路径 ")
		Vector<NavEntryDdo> FullPath;

		@Field(desc = " 搜索导航路径 ")
		Vector<NavEntryDdo> MetaSearchPath;

		@Field(desc = " 儿子导航 ")
		Vector<NavEntryDdo> ChildNode;

		@Field(desc = " 直接儿子属性 ")
		Map<uint32_t, Vector<SubAttrOptionDdo>> ChildAttrId;

		@Field(desc = " 属性字典 ")
		Vector<AttrDdo> AttrDic;
	}

	/**
	 * 类目搜索结构体
	 * 
	 * @author Doncheng
	 */
	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = " 类目搜索结构体 ", isSetClassSize = true)
	public class NavMatchKeyDdo {
		@Field(desc = " 版本号, version需要小写 ")
		uint8_t version;

		@Field(desc = " 导航id ")
		uint32_t NavId;

		@Field(desc = " Matchinfo ")
		String MatchInfo;

		@Field(desc = " PathInfo ")
		String PathInfo;

		@Field(desc = " MatchType ")
		uint32_t MatchType;
	}

	/**
	 * Publish Node 结构体
	 * 
	 * @author Doncheng
	 */
	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = " Publish Node 结构体 ", isSetClassSize = true)
	public class PublistNodeDdo {
		@Field(desc = " 版本号, version需要小写 ")
		uint8_t version;

		@Field(desc = " 导航id ")
		uint32_t NavId;

		@Field(desc = " 属性项id ")
		uint32_t AttrId;

		@Field(desc = " 属性值id ")
		uint32_t OptionId;

		@Field(desc = " 类型:1属性，3类目，4品类 ")
		uint32_t Type;

		@Field(desc = " name ")
		String Name;

		@Field(desc = " property ")
		String PropertyStr;

		@Field(desc = " 是否有后继节点 ")
		uint32_t AnyChildren;

		@Field(desc = " sDesc ")
		String Desc;
	}

//	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "属性项结构", isSetClassSize = true)
//	public class AttrTreeDdo {
//		@Field(desc = "版本号", defaultValue = "0")
//		uint32_t version;
//
//		@Field(desc = "属性项id")
//		uint32_t AttrId;
//		
//		@Field(desc = "属性值信息")
//		Vector<OptionTreeDdo> Option; 
//	}
//
//	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "属性值结构", isSetClassSize = true)
//	public class OptionTreeDdo {
//		@Field(desc = "版本号", defaultValue = "0")
//		uint32_t version;
//
//		@Field(desc = "属性值id")
//		uint32_t OptionId;
//		
//		@Field(desc = "子属性信息")
//		Vector<AttrTreeDdo> Children; 
//	}

//	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "品类信息", isSetClassSize = true)
//	public class MetaDdo {
//		@Field(desc = "版本号", defaultValue = "0")
//		uint32_t version;
//		
//		@Field(desc = "品类id")
//		uint32_t MetaId;
//
//		@Field(desc = "品类名称")
//		String Name;
//
//		@Field(desc = "备注")
//		String Desc;
//		
//		@Field(desc = "标记位，兼容老业务，新服务请用下面Mask")
//		String Property;
//
//		@Field(desc = "标记位", bitset=128)
//		BitSet Mask;
//		
//		@Field(desc = "所有的属性项id和属性值id，是一个有父子关系的树型结构，这个树的每一级都是已经排好序的")
//		Vector<MetaAttrIdDdo> AttrTree;
//
//		@Field(desc = "Tree中只记录了属性项id和属性值id，这些id的具体信息则记录在Attr中。key是属性项id")
//		Map<uint32_t, MetaAttrDdo> Attr;
//
//		@Field(desc = "搜索导航路径，下标为0的表示一级导航，下标最大的表示品类直属的导航")
//		Vector<SearchNavDdo> SearchNavPath;
//
//		@Field(desc = "发布导航路径，下标为0的表示一级导航，下标最大的表示品类直属的导航")
//		Vector<PubNavDdo> PubNavPath;
//
//		@Field(desc = "额外数据，key是业务id，value是业务数据")
//		Map<uint32_t, String> ExtraData;
//	}
//
//	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "品类属性结构", isSetClassSize = true)
//	public class MetaAttrIdDdo {
//		@Field(desc = "版本号", defaultValue = "0")
//		uint32_t version;
//		
//		@Field(desc = "属性项id")
//		uint32_t AttrId;
//
//		@Field(desc = "属性项排序值，正序")
//		uint32_t Order;
//		
//		@Field(desc = "属性值")
//		Vector<MetaOptionIdDdo> Option;
//	}
//
//	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "品类属性结构", isSetClassSize = true)
//	public class MetaOptionIdDdo {
//		@Field(desc = "版本号", defaultValue = "0")
//		uint32_t version;
//			
//		@Field(desc = "属性值id")
//		uint32_t OptionId;
//
//		@Field(desc = "属性值排序值，正序")
//		uint32_t Order;
//
//		@Field(desc = "子属性")
//		Vector<MetaAttrIdDdo> Children;
//	}
//
//	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "品类属性项", isSetClassSize = true)
//	public class MetaAttrDdo {
//		@Field(desc = "版本号", defaultValue = "0")
//		uint32_t version;
//
//		@Field(desc = "属性项id")
//		uint32_t AttrId;
//
//		@Field(desc = "属性项名称，是NameOriginal、NameOperator、NameSeller结合Mask的综合结果")
//		String Name;
//
//		@Field(desc = "原始名")
//		String NameOriginal;
//
//		@Field(desc = "运营加挂时定义的别名")
//		String NameOperator;
//
//		@Field(desc = "卖家定义的别名")
//		String NameSeller;
//
//		@Field(desc = "属性项描述")
//		String Desc;
//
//		@Field(desc = "属性值集合。key是属性值id")
//		Map<uint32_t, MetaOptionDdo> Option;
//		
//		@Field(desc = "标记位，兼容老业务，新服务请用下面Mask")
//		uint32_t Property;
//
//		@Field(desc = "标记位", bitset=128)
//		BitSet Mask;
//
//		@Field(desc = "额外数据，key是业务id，value是业务数据")
//		Map<uint32_t, String> ExtraData;
//	}
//
//	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "品类属性值", isSetClassSize = true)
//	public class MetaOptionDdo {
//		@Field(desc = "版本号", defaultValue = "0")
//		uint32_t version;
//
//		@Field(desc = "属性值id")
//		uint32_t OptionId;
//
//		@Field(desc = "属性项名称，是NameOriginal、NameOperator、NameSeller结合Mask的综合结果")
//		String Name;
//
//		@Field(desc = "原始名")
//		String NameOriginal;
//
//		@Field(desc = "运营加挂时定义的别名")
//		String NameOperator;
//
//		@Field(desc = "卖家定义的别名")
//		String NameSeller;
//		
//		@Field(desc = "标记位，兼容老业务，新服务请用下面Mask")
//		uint32_t Property;
//
//		@Field(desc = "标记位", bitset=128)
//		BitSet Mask;
//
//		@Field(desc = "额外数据，key是业务id，value是业务数据")
//		Map<uint32_t, String> ExtraData;
//	}
//	
//	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "发布导航信息", isSetClassSize = true)
//	public class PubNavDdo {
//		@Field(desc = "版本号", defaultValue = "0")
//		uint32_t version;
//		
//		@Field(desc = "导航id")
//		uint32_t NavId;
//
//		@Field(desc = "导航名称")
//		String Name;
//
//		@Field(desc = "备注")
//		String Desc;
//		
//		@Field(desc = "排序字段")
//		uint32_t Order;
//
//		@Field(desc = "标记位，兼容老服务，新服务请用下面Mask")
//		String Property;
//
//		@Field(desc = "标记位", bitset=128)
//		BitSet Mask;
//
//		@Field(desc = "上级导航，下标为0的表示一级搜索导航，下标最大的表示父导航")
//		Vector<PubNavDdo> Ancestor;
//
//		@Field(desc = "子导航")
//		Vector<PubNavDdo> Children;
//
//		@Field(desc = "是否是品类（即叶子节点），0：不是")
//		uint32_t IsMeta;
//
//		@Field(desc = "IsMeta非0时有效")
//		MetaDdo Meta;
//
//		@Field(desc = "额外数据，key是业务id，value是业务数据")
//		Map<uint32_t, String> ExtraData;
//	}
//	
//	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "搜索导航信息", isSetClassSize = true)
//	public class SearchNavDdo {
//		@Field(desc = "版本号", defaultValue = "0")
//		uint32_t version;
//		
//		@Field(desc = "导航id")
//		uint32_t NavId;
//
//		@Field(desc = "导航名称")
//		String Name;
//
//		@Field(desc = "备注")
//		String Desc;
//		
//		@Field(desc = "频道")
//		uint32_t Catalog;
//
//		@Field(desc = "排序字段")
//		uint32_t Order;
//
//		@Field(desc = "商品范围")
//		String SearchCond;
//
//		@Field(desc = "过滤条件，是一个树型结构，这个树的每一级都是已经排好序的")
//		Vector<SearchNavAttrDdo> Filter;
//
//		@Field(desc = "属性位，兼容老服务，新服务请用下面Mark")
//		String Property;
//
//		@Field(desc = "标记位", bitset=128)
//		BitSet Mask;
//
//		@Field(desc = "上级导航，下标为0的表示一级搜索导航，下标最大的表示父导航")
//		Vector<SearchNavDdo> Ancestor;
//
//		@Field(desc = "子导航")
//		Vector<SearchNavDdo> Children;
//
//		@Field(desc = "额外数据，key是业务id，value是业务数据")
//		Map<uint32_t, String> ExtraData;
//	}
//
//	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "搜索导航过滤条件属性项", isSetClassSize = true)
//	public class SearchNavAttrDdo {
//		@Field(desc = "版本号", defaultValue = "0")
//		uint32_t version;
//
//		@Field(desc = "属性项id")
//		uint32_t AttrId;
//
//		@Field(desc = "属性项名称，是NameOriginal、NameOperator结合Mask的综合结果")
//		String Name;
//
//		@Field(desc = "原始名")
//		String NameOriginal;
//
//		@Field(desc = "运营加挂时定义的别名")
//		String NameOperator;
//
//		@Field(desc = "属性项描述")
//		String Desc;
//
//		@Field(desc = "属性项排序")
//		uint32_t Order;
//
//		@Field(desc = "属性值")
//		Vector<SearchNavOptionDdo> Option;
//		
//		@Field(desc = "标记位，兼容老业务，新服务请用下面Mask")
//		uint32_t Property;
//
//		@Field(desc = "标记位", bitset=128)
//		BitSet Mask;
//
//		@Field(desc = "额外数据，key是业务id，value是业务数据")
//		Map<uint32_t, String> ExtraData;
//	}
//
//	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "搜索导航属性值", isSetClassSize = true)
//	public class SearchNavOptionDdo {
//		@Field(desc = "版本号", defaultValue = "0")
//		uint32_t version;
//
//		@Field(desc = "属性值id")
//		uint32_t OptionId;
//
//		@Field(desc = "属性值排序")
//		uint32_t Order;
//
//		@Field(desc = "属性项名称，是NameOriginal、NameOperator结合Mask的综合结果")
//		String Name;
//
//		@Field(desc = "原始名")
//		String NameOriginal;
//
//		@Field(desc = "运营加挂时定义的别名")
//		String NameOperator;
//		
//		@Field(desc = "标记位，兼容老业务，新服务请用下面Mask")
//		uint32_t Property;
//
//		@Field(desc = "标记位", bitset=128)
//		BitSet Mask;
//
//		@Field(desc = "子属性项")
//		Vector<SearchNavAttrDdo> Children;
//
//		@Field(desc = "额外数据，key是业务id，value是业务数据")
//		Map<uint32_t, String> ExtraData;
//	}

	
	// 其实上面的AttrTreeDdo+OptionTreeDdo的设计更符合情况，只不过生成出来的c++头文件会存在互相引用的情况，
	// 互相引用会导致编译不过。
/*	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "属性项结构", isSetClassSize = true)
	public class AttrTreeDdo {
		@Field(desc = "版本号", defaultValue = "0")
		uint32_t version;

		@Field(desc = "属性项id")
		uint32_t AttrId;
		
		@Field(desc = "属性值id")
		uint32_t OptionId;
		
		@Field(desc = "属性值信息")
		Vector<AttrTreeDdo> Children; 
	}

	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "属性库", isSetClassSize = true)
	public class AttrInfoDdo {
		@Field(desc = "版本号", defaultValue = "0")
		uint32_t version;

		@Field(desc = "属性项id")
		uint32_t AttrId;

		@Field(desc = "属性项名称，是NameOriginal、NameOperator、NameSeller结合Mask综合的结果")
		String Name;

		@Field(desc = "原始名")
		String NameOriginal;

		@Field(desc = "运营加挂时定义的别名")
		String NameOperator;

		@Field(desc = "属性项描述")
		String Desc;

		@Field(desc = "属性项排序")
		uint32_t Order;

		@Field(desc = "属性值集合。key是属性值id")
		Map<uint32_t, OptionInfoDdo> Option;
		
		@Field(desc = "属性位，兼容老服务，新服务请用下面Mark")
		uint32_t Property;

		@Field(desc = "标记位", bitset=64)
		BitSet Mask;
	}

	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "属性库", isSetClassSize = true)
	public class OptionInfoDdo {
		@Field(desc = "版本号", defaultValue = "0")
		uint32_t version;

		@Field(desc = "属性值id")
		uint32_t OptionId;

		@Field(desc = "属性值排序")
		uint32_t Order;

		@Field(desc = "属性项名称，是NameOriginal、NameOperator、NameSeller结合Mask综合的结果")
		String Name;

		@Field(desc = "原始名")
		String NameOriginal;

		@Field(desc = "运营加挂时定义的别名")
		String NameOperator;

		@Field(desc = "卖家自定义名")
		String NameSeller;

		@Field(desc = "属性位")
		uint32_t Property;
	}
	
	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "发布导航信息", isSetClassSize = true)
	public class PubNavDdo {
		@Field(desc = "版本号", defaultValue = "0")
		uint32_t version;
		
		@Field(desc = "导航id")
		uint32_t NavId;

		@Field(desc = "导航名称")
		String Name;

		@Field(desc = "备注")
		String Desc;
		
		@Field(desc = "排序字段")
		uint32_t Order;

		@Field(desc = "属性位，兼容老服务，新服务请用下面Mark")
		String Property;

		@Field(desc = "标记位", bitset=64)
		BitSet Mask;

		@Field(desc = "上级导航，下标为0的表示一级搜索导航，下标最大的表示父导航")
		Vector<PubNavDdo> Ancestor;

		@Field(desc = "子导航")
		Vector<PubNavDdo> Children;

		@Field(desc = "是否是品类（即叶子节点），0：不是")
		uint32_t IsMeta;

		@Field(desc = "IsMeta非0时有效")
		MetaDdo Meta;
	}
	
	@Member(cPlusNamespace = "b2b2c::nca::ddo", desc = "搜索导航信息", isSetClassSize = true)
	public class SearchNavDdo {
		@Field(desc = "版本号", defaultValue = "0")
		uint32_t version;
		
		@Field(desc = "导航id")
		uint32_t NavId;

		@Field(desc = "导航名称")
		String Name;

		@Field(desc = "备注")
		String Desc;
		
		@Field(desc = "频道")
		uint32_t Catalog;

		@Field(desc = "排序字段")
		uint32_t Order;

		@Field(desc = "商品范围")
		String SearchCond;

		@Field(desc = "属性位，兼容老服务，新服务请用下面Mark")
		String Property;

		@Field(desc = "标记位", bitset=64)
		BitSet Mask;

		@Field(desc = "上级导航，下标为0的表示一级搜索导航，下标最大的表示父导航")
		Vector<SearchNavDdo> Ancestor;

		@Field(desc = "子导航")
		Vector<SearchNavDdo> Children;

		@Field(desc = "过滤条件，是一个树型结构，这个树的每一级都是已经排好序的")
		Vector<AttrTreeDdo> AttrDic;

		@Field(desc = "AttrDic中只记录了AttrId和OptionId，这些id的具体信息则记录在AttrInfo中。key是属性项id")
		Map<uint32_t, AttrInfoDdo> AttrInfo;
	}
*/	
/////////////////////////////////////拍拍ncav3相关定义///////////////////

	/**
	 * 获取品类数据(不包含属性)--0x53601801
	 * @author Doncheng
	 * */
	@ApiProtocol(cmdid = "0x53601801L", desc = "获取品类数据(不包含属性)")
	class GetMetaClass
	{
	    @ApiProtocol(cmdid = "0x53601801L", desc = "获取品类数据(不包含属性)请求")
	    class Req
	    {
	        @Field(desc = "Source")
	        String     Source;
	        @Field(desc = "InReserve")
	        String     InReserve;
	        @Field(desc = "品类id")
	        uint32_t   MetaId;
	    }
	    
	    @ApiProtocol(cmdid = "0x53608801L", desc = "获取品类数据(不包含属性)响应")
	    class Resp
	    {
	        @Field(desc = "品类数据")
	        NavBo_v3    Meta;
	        @Field(desc = "OutReserve")
	        String      OutReserve;
	    }   
	}

	/**
	 * 获取品类数据(包含属性)--0x53601802
	 * @author Doncheng
	 * */
	@ApiProtocol(cmdid = "0x53601802L", desc = "获取品类数据(包含属性)")
	class GetMetaClassEx
	{
	    @ApiProtocol(cmdid = "0x53601802L", desc = "获取品类数据(包含属性)请求")
	    class Req
	    {
	        @Field(desc = "Source")
	        String     Source;
	        @Field(desc = "InReserve")
	        String     InReserve;
	        @Field(desc = "品类id")
	        uint32_t   MetaId;
	    }
	    
	    @ApiProtocol(cmdid = "0x53608802L", desc = "获取品类数据(包含属性)响应")
	    class Resp
	    {
	        @Field(desc = "品类数据")
	        NavBoEx_v3    Meta;
	        @Field(desc = "OutReserve")
	        String      OutReserve;
	    }   
	}

	/**
	 * 获取导航数据(不包含属性)--0x53601803
	 * @author Doncheng
	 * */
	@ApiProtocol(cmdid = "0x53601803L", desc = "获取导航数据(不包含属性)")
	class GetNav
	{
	    @ApiProtocol(cmdid = "0x53601803L", desc = "获取导航数据(不包含属性)请求")
	    class Req
	    {
	        @Field(desc = "Source")
	        String     Source;
	        @Field(desc = "InReserve")
	        String     InReserve;
	        @Field(desc = "地图id")
	        uint32_t   MapId;
	        @Field(desc = "导航id")
	        uint32_t   NavId;
	    }
	    
	    @ApiProtocol(cmdid = "0x53608803L", desc = "获取导航数据(不包含属性)响应")
	    class Resp
	    {
	        @Field(desc = "导航数据")
	        NavBo_v3    Nav;
	        @Field(desc = "OutReserve")
	        String     OutReserve;
	    }   
	}

	/**
	 * 获取导航及其儿子导航信息--0x53601804
	 * @author Doncheng
	 * */
	@ApiProtocol(cmdid = "0x53601804L", desc = "获取导航及其儿子导航信息")
	class GetNavEx
	{
	    @ApiProtocol(cmdid = "0x53601804L", desc = "请求")
	    class Req
	    {
	        @Field(desc = "Source")
	        String     Source;
	        @Field(desc = "InReserve")
	        String     InReserve;
	        @Field(desc = "地图id")
	        uint32_t   MapId;
	        @Field(desc = "导航id")
	        uint32_t   NavId;
	    }
	    
	    @ApiProtocol(cmdid = "0x53608804L", desc = "响应")
	    class Resp
	    {
	        @Field(desc = "导航数据")
	        NavBoEx_v3    Nav;
	        @Field(desc = "OutReserve")
	        String     OutReserve;
	    }   
	}

	/**
	 * 获取已经挂载到导航树上的全部品类信息(不包含属性信息)--0x53601805
	 * @author Doncheng
	 * */
	@ApiProtocol(cmdid = "0x53601805L", desc = "获取全部品类数据(不包含属性信息)")
	class GetAllMetaClass
	{
	    @ApiProtocol(cmdid = "0x53601805L", desc = "获取全部品类数据(不包含属性信息)请求")
	    class Req
	    {
	        @Field(desc = "Source")
	        String     Source;
	        @Field(desc = "InReserve")
	        String     InReserve;
	    }
	    
	    @ApiProtocol(cmdid = "0x53608805L", desc = "获取全部品类数据(不包含属性信息)响应")
	    class Resp
	    {
	        @Field(desc = "品类数据")
	        Vector<NavEntry_v3>    Nav;
	        @Field(desc = "OutReserve")
	        String     OutReserve;
	    }   
	}

	/**
	 *  通过分类获取已经挂载到导航树上的品类信息(不包含属性信息)--0x53601806
	 * @author Doncheng
	 * */
	@ApiProtocol(cmdid = "0x53601806L", desc = "通过分类获取品类数据(不包含属性信息)")
	class GetMetaByCatalog
	{
	    @ApiProtocol(cmdid = "0x53601806L", desc = "通过分类获取品类数据(不包含属性信息)请求")
	    class Req
	    {
	        @Field(desc = "Source")
	        String     Source;
	        @Field(desc = "InReserve")
	        String     InReserve;
	        @Field(desc = "分类")
	        uint32_t   Catalog;
	    }
	    
	    @ApiProtocol(cmdid = "0x53608806L", desc = "通过分类获取品类数据(不包含属性信息)响应")
	    class Resp
	    {
	        @Field(desc = "品类数据")
	        Vector<NavEntry_v3>    Nav;
	        @Field(desc = "OutReserve")
	        String     OutReserve;
	    }   
	}

	/**
	 * 获取与路径无关的属性信息--0x53601807
	 * @author Doncheng
	 * */       
	@ApiProtocol(cmdid = "0x53601807L", desc = "获取与路径无关的属性信息")
	class GetStaticAttr
	{
	    @ApiProtocol(cmdid = "0x53601807L", desc = "获取与路径无关的属性信息请求")
	    class Req
	    {
	        @Field(desc = "Source")
	        String     Source;
	        @Field(desc = "InReserve")
	        String     InReserve;
	        @Field(desc = "属性项Id")
	        uint32_t   AttrId;
	        @Field(desc = "是否需要属性值,0:不需要,1：需要")
	        uint32_t   IsNeedOptions;

	    }
	    
	    @ApiProtocol(cmdid = "0x53608807L", desc = "获取与路径无关的属性信息响应")
	    class Resp
	    {
	        @Field(desc = "属性项")
	        AttrBo_v3    Attr;
	        @Field(desc = "OutReserve")
	        String      OutReserve;
	    }   
	}

	/**
	 * 属性结构体编码成属性串--0x53601808
	 * @author Doncheng
	 * */
	@ApiProtocol(cmdid = "0x53601808L", desc = "属性接口编码成属性串")
	class MakeAttrText
	{
	    @ApiProtocol(cmdid = "0x53601808L", desc = "属性接口编码成属性串请求")
	    class Req
	    {
	        @Field(desc = "Source")
	        String     Source;
	        @Field(desc = "InReserve")
	        String     InReserve;
	        @Field(desc = "地图id")
	        uint32_t   MapId;
	        @Field(desc = "导航id")
	        uint32_t   NavId;
	        @Field(desc = "属性结构体列表")
	        Vector<AttrBo_v3>    AttrBoList;
	    }
	    
	    @ApiProtocol(cmdid = "0x53608808L", desc = "属性接口编码成属性串响应")
	    class Resp
	    {
	        @Field(desc = "属性串")
	        String      AttrString;
	        @Field(desc = "OutReserve")
	        String      OutReserve;
	    }   
	}

	/**
	 * 商品属性串解析--0x53601809
	 * @author Doncheng
	 * */
	@ApiProtocol(cmdid = "0x53601809L", desc = "商品属性串解析")
	class ParseAttrText
	{
	    @ApiProtocol(cmdid = "0x53601809L", desc = "商品属性串解析请求")
	    class Req
	    {
	        @Field(desc = "Source")
	        String     Source;
	        @Field(desc = "InReserve")
	        String     InReserve;
	        @Field(desc = "地图id")
	        uint32_t   MapId;
	        @Field(desc = "导航id")
	        uint32_t   NavId;
	        @Field(desc = "属性串")
	        String     AttrString;
	    }
	    
	    @ApiProtocol(cmdid = "0x53608809L", desc = "商品属性串解析响应")
	    class Resp
	    {
	        @Field(desc = "属性")
	        Vector<AttrBo_v3>    AttrBoList;
	        @Field(desc = "OutReserve")
	        String      OutReserve;
	    }   
	}

	/**
	 * 属性结构体赋值--0x5360180a
	 * @author Doncheng
	 * */
	@ApiProtocol(cmdid = "0x5360180aL", desc = "属性结构体赋值")
	class GetAttrText
	{
	    @ApiProtocol(cmdid = "0x5360180aL", desc = "属性结构体赋值请求")
	    class Req
	    {
	        @Field(desc = "Source")
	        String     Source;
	        @Field(desc = "InReserve")
	        String     InReserve;
	        @Field(desc = "地图id")
	        uint32_t   MapId;
	        @Field(desc = "导航id")
	        uint32_t   NavId;
	        @Field(desc = "属性列表")
	        Vector<AttrBo_v3>  InAttrBoList;
	    }
	    
	    @ApiProtocol(cmdid = "0x5360880aL", desc = "属性结构体赋值响应")
	    class Resp
	    {
	        @Field(desc = "属性列表")
	        Vector<AttrBo_v3>  OutAttrBoList;
	        @Field(desc = "OutReserve")
	        String      OutReserve;
	    }
	}

	/**
	 * 校验商品属性串是否合法--0x5360180b
	 * @author Doncheng
	 * */
	@ApiProtocol(cmdid = "0x5360180bL", desc = "校验商品属性串是否合法")
	class IsAttrStrValid
	{
	    @ApiProtocol(cmdid = "0x5360180bL", desc = "校验商品属性串是否合法请求")
	    class Req
	    {
	        @Field(desc = "Source")
	        String     Source;
	        @Field(desc = "InReserve")
	        String     InReserve;
	        @Field(desc = "地图id")
	        uint32_t   MapId;
	        @Field(desc = "导航id")
	        uint32_t   NavId;
	        @Field(desc = "属性串")
	        String     AttrString;
	    }
	    
	    @ApiProtocol(cmdid = "0x5360880bL", desc = "校验商品属性串是否合法响应")
	    class Resp
	    {
	        @Field(desc = "错误消息")
	        String      sReason;
	        @Field(desc = "OutReserve")
	        String      OutReserve;
	    }   
	}

	/**
	 * 类目属性名称搜索--0x5360180c
	 * @author Doncheng
	 * */
	@ApiProtocol(cmdid = "0x5360180cL", desc = "类目属性名称搜索")
	class SearchPubNavByKey
	{
	    @ApiProtocol(cmdid = "0x5360180cL", desc = "类目属性名称搜索请求")
	    class Req
	    {
	        @Field(desc = "Source")
	        String     Source;
	        @Field(desc = "InReserve")
	        String     InReserve;
	        @Field(desc = "是否需要搜索属性,0:不需要,1:需要")
	        uint32_t   NeedAttr;
	        @Field(desc = "关键词")
	        String     Key;
	        @Field(desc = "需要的个数")
	        uint32_t   Count;

	    }
	    
	    @ApiProtocol(cmdid = "0x5360880cL", desc = "类目属性名称搜索响应")
	    class Resp
	    {
	        @Field(desc = "结果集")
	        Vector<NavMatchKey_v3> NavMatchKeyList;
	        @Field(desc = "OutReserve")
	        String      OutReserve;
	    }   
	}

	/**
	 * 获取商品发布及编辑类目属性数据--0x5360180d
	 * @author Doncheng
	 * */
	@ApiProtocol(cmdid = "0x5360180dL", desc = "获取商品发布及编辑类目属性数据")
	class GetPublishInfo
	{
	    @ApiProtocol(cmdid = "0x5360180dL", desc = "获取商品发布及编辑类目属性数据请求")
	    class Req
	    {
	        @Field(desc = "Source")
	        String     Source;
	        @Field(desc = "InReserve")
	        String     InReserve;
	        @Field(desc = "类目id")
	        uint32_t   MetaClassId;
	        @Field(desc = "非空则取一级属性")
	        String   ClassPath;
	        @Field(desc = "属性串")
	        String   AttrPath;
	    }
	    
	    @ApiProtocol(cmdid = "0x5360880dL", desc = "获取商品发布及编辑类目属性数据响应")
	    class Resp
	    {
	        @Field(desc = "导航数据")
	        NavBoEx_v3    Nav;
	        @Field(desc = "OutReserve")
	        String     OutReserve;
	    }   
	}

	/**
	 * 获取发布路径信息--0x5360180e
	 * @author Doncheng
	 * */
	@ApiProtocol(cmdid = "0x5360180eL", desc = "获取发布路径信息")
	class GetPubPath
	{
	    @ApiProtocol(cmdid = "0x5360180eL", desc = "获取发布路径信息请求")
	    class Req
	    {
	        @Field(desc = "Source")
	        String     Source;
	        @Field(desc = "InReserve")
	        String     InReserve;
	        @Field(desc = "导航id")
	        uint32_t   NavId;
	        @Field(desc = "属性项id")
	        uint32_t   AttrId;
	        @Field(desc = "属性值id")
	        uint32_t   OptionId;
	    }
	    
	    @ApiProtocol(cmdid = "0x5360880eL", desc = "获取发布路径信息响应")
	    class Resp
	    {
	        @Field(desc = "发布路径节点数据")
	        Vector<PublistNode_v3>   SubNode;
	        @Field(desc = "OutReserve")
	        String     OutReserve;
	    }   
	}

	/**
	 * 将类目地图一级的导航节点分组，每组有相应业务号，根据业务号查询相应导航节点信息
	 * @author quentinxu
	 * */
	@ApiProtocol(cmdid = "0x5360180fL", desc = "获取目地图一级的导航节点分组信息")
	class GetGroupNav
	{
	    @ApiProtocol(cmdid = "0x5360180fL", desc = "获取目地图一级的导航节点分组信息请求")
	    class Req
	    {
	        @Field(desc = "Source")
	        String     Source;
	        @Field(desc = "InReserve")
	        String     InReserve;
	        @Field(desc = "地图id")
	        uint32_t   MapId;
	        @Field(desc = "业务号,即数据文件中mty标识的数字")
	        uint32_t   GroupId;
	    }
	    
	    @ApiProtocol(cmdid = "0x5360880fL", desc = "获取目地图一级的导航节点分组信息响应")
	    class Resp
	    {
	        @Field(desc = "业务下所有的导航数据")
	        Vector<NavBo_v3>    NavList;
	        @Field(desc = "OutReserve")
	        String     OutReserve;
	    }   
	}

	/**
	 * 获取排序的导航及其儿子导航信息（包括属性字典，都排序）
	 * @author quentinxu
	 * */
	@ApiProtocol(cmdid = "0x53601810L", desc = "获取排序的导航及其儿子导航信息")
	class GetOrderNavEx
	{
	    @ApiProtocol(cmdid = "0x53601810L", desc = "获取排序的导航及其儿子导航信息请求")
	    class Req
	    {
	        @Field(desc = "Source")
	        String     Source;
	        @Field(desc = "InReserve")
	        String     InReserve;
	        @Field(desc = "地图id")
	        uint32_t   MapId;
	        @Field(desc = "导航id")
	        uint32_t   NavId;
	        @Field(desc = "排序方法 1表示按照order字段升序排序")
	        uint32_t   OrderType;
	    }
	    
	    @ApiProtocol(cmdid = "0x53608810L", desc = "获取排序的导航及其儿子导航信息响应")
	    class Resp
	    {
	        @Field(desc = "导航数据")
	        OrderNavBoEx_v3    Nav;
	        @Field(desc = "OutReserve")
	        String     OutReserve;
	    }   
	}
	
    /**
     * 获取导航属性关系里的属性值--0x53601813L
     * 分批获取，避免数据量过大
     * @author 
     */
    @ApiProtocol(cmdid= "0x53601813L", desc = "获取导航属性关系里的属性值")
    class GetNavAttrOp
    {
        @ApiProtocol(cmdid= "0x53601813L", desc = "获取导航属性关系里的属性值，请求")
        class Req {
            @Field(desc = "机器码，必填")
            String machineKey;

            @Field(desc = "调用来源，必填")
            String source;

            @Field(desc = "场景Id，必填")
            uint32_t sceneId;

            @Field(desc =  "地图id，必填" )
            uint32_t mapId;

            @Field(desc =  "品类id，必填" )
            uint32_t metaId;

            @Field(desc = "属性项id")
            uint32_t attrId;

            @Field(desc = "请求保留字")
            String inReserve;
        }

        @ApiProtocol(cmdid =  "0x53608813L" , desc =  " 获取导航属性关系里的属性值,响应 " )
        class Resp {
            @Field(desc = "错误信息")
            String errmsg;

            @Field(desc =  "属性实体里有导航属性关系里所有属性值")
            AttrBo_v3 attr;

            @Field(desc = "输出保留字")
            String outReserve;
        }
    }

	/**
	 * 
	 * 商品详情获取过滤属性--0x53601814L
	 * 用品类id得到面包屑的终点，用入参过滤终点的过滤属性
	 * @author jackyjiang
	 */
	@ApiProtocol(cmdid =  "0x53601814L" , desc =  " 商品详情获取过滤属性 " )
	class GetSearchInfo
	{
		@ApiProtocol(cmdid =  "0x53601814L" , desc =  " 商品详情获取过滤属性,请求 " )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "地图id，必填" )
			uint32_t mapId;

			@Field(desc =  "品类id，必填" )
			uint32_t metaId;
			
			@Field(desc =  "属性串，必填" )
			String  attrIn;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0x53608814L" , desc =  " 商品详情获取过滤属性,响应 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "排序的属性项")
			Vector<AttrBo_v3> attrOut;
			
			@Field(desc = "面包屑")
			Vector<NavEntry_v3> searchPath;
			
			@Field(desc =  "key是面包屑每级的导航id，Value是面包屑每级的兄弟节点列表")
			Map<uint32_t,Vector<NavEntry_v3> > brotherNode;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	/**
	 * 获取导航数据(不包含属性)--0x53601815
	 * @author Doncheng
	 * */
	@ApiProtocol(cmdid = "0x53601815L", desc = "获取导航数据(不包含属性)")
	class GetNavs
	{
		@ApiProtocol(cmdid = "0x53601815L", desc = "获取导航数据(不包含属性)请求")
		class Req
		{
			@Field(desc = "Source")
			String     Source;
			@Field(desc = "InReserve")
			String     InReserve;
			@Field(desc = "地图id")
			uint32_t   MapId;
			@Field(desc = "导航id")
			Vector<uint32_t>   NavId;
		}
		
		@ApiProtocol(cmdid = "0x53608815L", desc = "获取导航数据(不包含属性)响应")
		class Resp
		{
			@Field(desc = "导航数据")
			Vector<NavBo_v3>    Nav;
			@Field(desc = "OutReserve")
			String     OutReserve;
		}	
	}


    /**
     * 属性串格式转换---0x53601816L
     * @auther jackyjiang
     */
	@ApiProtocol(cmdid = "0x53601816L", desc = "属性串格式转换")
	class TransAttrStr
	{
	    @ApiProtocol(cmdid = "0x53601816L", desc = "属性串格式转换")
	    class Req
	    {
	            @Field(desc = "source")
	            String Source;
	
	            @Field(desc = "品类id")
	            uint32_t MetaId;
	
	            @Field(desc = "源属性串，拍拍旧格式")
	            String AttrA;
	
	            @Field(desc = "InReserve")
	                    String  InReserve;
	    }
	
	    @ApiProtocol(cmdid = "0x53608816L", desc = "属性串格式转换")
	    class Resp
	    {
	            @Field(desc = "目标属性串B，拍拍新格式")
	            String AttrB;
	
	            @Field(desc = "目标属性串C，拍拍新格式")
	            String AttrC;
	    }
	}

    /**
     * 属性串格式转换---0x53601817L
     * @auther jackyjiang
     */
	@ApiProtocol(cmdid = "0x53601817L", desc = "属性串格式转换")
	class TransAttrStr2
	{
	    @ApiProtocol(cmdid = "0x53601817L", desc = "属性串格式转换")
	    class Req
	    {
	            @Field(desc = "source")
	            String Source;
	
	            @Field(desc = "品类id")
	            uint32_t MetaId;
	
	            @Field(desc = "目标属性串B，拍拍新格式")
	            String AttrB;
	
	            @Field(desc = "InReserve")
	                    String  InReserve;
	    }
	
	    @ApiProtocol(cmdid = "0x53608817L", desc = "属性串格式转换")
	    class Resp
	    {
	            @Field(desc = "源属性串，拍拍旧格式")
	            String AttrA;
	    }
	}

	
/////////////////////////////////////网购商详专用接口///////////////////
//
//	/**
//	 * 
//	 * 商品详情获取过滤属性--0xA0021801
//	 * 用品类id得到面包屑的终点，用入参过滤终点的过滤属性
//	 * @author jackyjiang
//	 */
//	@ApiProtocol(cmdid =  "0xA0021801L" , desc =  " 商品详情获取过滤属性 " )
//	class GetSearchInfo4SX_WG
//	{
//		@ApiProtocol(cmdid =  "0xA0021801L" , desc =  " 商品详情获取过滤属性,请求 " )
//		class Req {
//			@Field(desc = "机器码，必填")
//			String machineKey;
//			
//			@Field(desc = "调用来源，必填")
//			String source;
//
//			@Field(desc = "场景Id，必填")
//			uint32_t sceneId;
//
//			@Field(desc =  "地图id，必填" )
//			uint32_t mapId;
//
//			@Field(desc =  "品类id，必填" )
//			uint32_t metaId;
//			
//			@Field(desc =  "入参过滤值，key是属性项id，value是值id的集合，必填" )
//			Map<uint32_t, Set<uint32_t>> attrIn;
//			
//			@Field(desc = "请求保留字")
//			String inReserve;
//		}
//
//		@ApiProtocol(cmdid =  "0xA0028801L" , desc =  " 商品详情获取过滤属性,响应 " )
//		class Resp {
//			@Field(desc = "错误信息")
//			String errmsg;
//			
//			@Field(desc =  "排序的属性项" )
//			Vector<AttrDdo> attrOut;
//			
//			@Field(desc =  "key是面包屑每级的导航id，Value是面包屑每级的兄弟节点列表" )
//			Map<uint32_t,Vector<NavEntryDdo> > brotherNode;
//
//			@Field(desc = "输出保留字")
//			String outReserve;
//		}
//	}
//
//	/**
//	 * 商详解析属性串和面包屑--0xA0021810
//	 * 
//	 * @author jackyjiang
//	 */
//	@ApiProtocol(cmdid =  "0xA0021810L" , desc =  " 商详解析属性串和面包屑请求 " )
//	class ParseAttr4SXOnly_WG {
//		@ApiProtocol(cmdid =  "0xA0021810L" , desc =  " 商详解析属性串和面包屑请求 " )
//		class Req {
//			@Field(desc = "机器码，必填")
//			String machineKey;
//			
//			@Field(desc = "调用来源，必填")
//			String source;
//
//			@Field(desc = "场景Id，必填")
//			uint32_t sceneId;
//
//			@Field(desc =  "地图id，必填" )
//			uint32_t mapId;
//
//			@Field(desc =  "品类id，必填" )
//			uint32_t metaId;
//
//			@Field(desc =  "解析销售属性，key是属性项id，值是需要解释的项id的集合，必填" )
//			Map<uint32_t, Vector<uint32_t>> saleAttrsIn;
//
//			@Field(desc =  "解析一般展示属性，传进来是个属性id串,key作为这个串的标志，不限定是什么值，必填" )
//			Map<uint32_t, String> commAttrsIn;
//			
//			@Field(desc = "请求保留字")
//			String inReserve;
//		}
//
//		@ApiProtocol(cmdid =  "0xA0028810L" , desc =  " 商详解析属性串和面包屑请求 " )
//		class Resp {
//			@Field(desc = "错误信息")
//			String errmsg;
//			
//			@Field(desc =  "面包屑" )
//			Vector<NavEntryDdo> fullPath;
//
//			@Field(desc =  "解析销售属性" )
//			Map<uint32_t, AttrDdo> saleAttrsOut;
//
//			@Field(desc =  "解析一般展示属性" )
//			Map<uint32_t, String> commAttrsOut;
//
//			@Field(desc = "输出保留字")
//			String outReserve;
//		}
//	}
//	
//
/////////////////////////////////////网购接口///////////////////

	/** ******************************************************************************************************************* */

	/**
	 * 获取品类数据(不包含属性)--0xA0011801
	 * 
	 * @author Doncheng
	 */
	@ApiProtocol(cmdid = "0xA0011801L", desc = " 获取品类数据(不包含属性) ")
	class GetMetaClass_WG {
		@ApiProtocol(cmdid = "0xA0011801L", desc = " 获取品类数据(不包含属性)请求 ")
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc = "品类Id，必填")
			uint32_t metaId;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xA0018801L", desc = " 获取品类数据(不包含属性)响应 ")
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc = "品类数据")
			NavDdo meta;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	/**
	 * 获取品类数据(包含属性)--0xA0011802
	 * 
	 * @author Doncheng
	 */
	@ApiProtocol(cmdid = "0xA0011802L", desc = " 获取品类数据(包含属性)")
	class GetMetaClassEx_WG {
		@ApiProtocol(cmdid = "0xA0011802L" , desc =  " 获取品类数据(包含属性)请求 " )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "品类id，必填" )
			uint32_t metaId;

			@Field(desc =  "请求保留字" )
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0xA0018802L" , desc =  " 获取品类数据(包含属性)响应 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "品类数据" )
			NavExDdo meta;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	/**
	 * 获取导航数据(不包含属性)--0xA0011803
	 * 
	 * @author Doncheng
	 */
	@ApiProtocol(cmdid =  "0xA0011803L" , desc =  " 获取导航数据(不包含属性) " )
	class GetNav_WG {
		@ApiProtocol(cmdid =  "0xA0011803L" , desc =  " 获取导航数据(不包含属性)请求 " )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "地图id，必填" )
			uint32_t mapId;

			@Field(desc =  "导航id，必填" )
			uint32_t navId;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0xA0018803L" , desc =  " 获取导航数据(不包含属性)响应 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "导航数据" )
			NavDdo nav;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	/**
	 * 获取导航及其儿子导航信息--0xA0011804
	 * 
	 * @author Doncheng
	 */
	@ApiProtocol(cmdid =  "0xA0011804L" , desc =  " 获取导航及其儿子导航信息 " )
	class GetNavEx_WG {
		@ApiProtocol(cmdid =  "0xA0011804L" , desc =  " 请求 " )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "地图id，必填" )
			uint32_t mapId;

			@Field(desc =  "导航id，必填" )
			uint32_t navId;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0xA0018804L" , desc =  " 响应 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "导航数据" )
			NavExDdo nav;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	/**
	 * 获取已经挂载到导航树上的全部品类信息(不包含属性信息)--0xA0011805
	 * 
	 * @author Doncheng
	 */
	@ApiProtocol(cmdid =  "0xA0011805L" , desc =  " 获取全部品类数据(不包含属性信息) " )
	class GetAllMetaClass_WG {
		@ApiProtocol(cmdid =  "0xA0011805L" , desc =  " 获取全部品类数据(不包含属性信息)请求 " )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0xA0018805L" , desc =  " 获取全部品类数据(不包含属性信息)响应 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "品类数据" )
			Vector<NavEntryDdo> nav;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	/**
	 * 通过分类获取已经挂载到导航树上的品类信息(不包含属性信息)--0xA0011806
	 * 
	 * @author Doncheng
	 */
	@ApiProtocol(cmdid =  "0xA0011806L" , desc =  " 通过分类获取品类数据(不包含属性信息) " )
	class GetMetaByCatalog_WG {
		@ApiProtocol(cmdid =  "0xA0011806L" , desc =  " 通过分类获取品类数据(不包含属性信息)请求 " )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "分类，必填" )
			uint32_t catalog;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0xA0018806L" , desc =  " 通过分类获取品类数据(不包含属性信息)响应 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "品类数据" )
			Vector<NavEntryDdo> nav;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	/**
	 * 获取与路径无关的属性信息--0xA0011807
	 * 
	 * @author Doncheng
	 */
	@ApiProtocol(cmdid =  "0xA0011807L" , desc =  " 获取与路径无关的属性信息 " )
	class GetStaticAttr_WG {
		@ApiProtocol(cmdid =  "0xA0011807L" , desc =  " 获取与路径无关的属性信息请求 " )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "属性项Id，必填" )
			uint32_t attrId;

			@Field(desc =  "是否需要属性值，必填;0:不需要,1：需要 " )
			uint32_t isNeedOptions;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0xA0018807L" , desc =  " 获取与路径无关的属性信息响应 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "属性项" )
			AttrDdo attr;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	/**
	 * 属性结构体编码成属性串--0xA0011808
	 * 
	 * @author Doncheng
	 */
	@ApiProtocol(cmdid =  "0xA0011808L" , desc =  " 属性接口编码成属性串 " )
	class MakeAttrText_WG {
		@ApiProtocol(cmdid =  "0xA0011808L" , desc =  " 属性接口编码成属性串请求 " )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "地图id，必填" )
			uint32_t mapId;

			@Field(desc =  "导航id，必填" )
			uint32_t navId;

			@Field(desc =  " 属性结构体列表 " )
			Vector<AttrDdo> attrBoList;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0xA0018808L" , desc =  " 属性接口编码成属性串响应 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "属性串" )
			String attrString;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	/**
	 * 商品属性串解析--0xA0011809
	 * 
	 * @author Doncheng
	 */
	@ApiProtocol(cmdid =  "0xA0011809L" , desc =  " 商品属性串解析 " )
	class ParseAttrText_WG {
		@ApiProtocol(cmdid =  "0xA0011809L" , desc =  " 商品属性串解析请求 " )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "地图id，必填" )
			uint32_t mapId;

			@Field(desc =  "导航id，必填" )
			uint32_t navId;

			@Field(desc =  "属性串，必填" )
			String attrString;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0xA0018809L" , desc =  " 商品属性串解析响应 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "属性" )
			Vector<AttrDdo> attrBoList;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	/**
	 * 属性结构体赋值--0xA001180a
	 * 
	 * @author Doncheng
	 */
	@ApiProtocol(cmdid =  "0xA001180aL" , desc =  " 属性结构体赋值 " )
	class GetAttrText_WG {
		@ApiProtocol(cmdid =  "0xA001180aL" , desc =  " 属性结构体赋值请求 " )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "地图id，必填" )
			uint32_t mapId;

			@Field(desc =  "导航id，必填" )
			uint32_t navId;

			@Field(desc =  "属性列表，必填" )
			Vector<AttrDdo> inAttrBoList;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0xA001880aL" , desc =  " 属性结构体赋值响应 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "属性列表" )
			Vector<AttrDdo> outAttrBoList;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	/**
	 * 校验商品属性串是否合法--0xA001180b
	 * 
	 * @author Doncheng
	 */
	@ApiProtocol(cmdid =  "0xA001180bL" , desc =  " 校验商品属性串是否合法 " )
	class IsAttrStrValid_WG {
		@ApiProtocol(cmdid =  "0xA001180bL" , desc =  " 校验商品属性串是否合法请求 " )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "地图id，必填" )
			uint32_t mapId;

			@Field(desc =  "导航id，必填" )
			uint32_t navId;

			@Field(desc =  "属性串，必填" )
			String attrString;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0xA001880bL" , desc =  " 校验商品属性串是否合法响应 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "错误消息" )
			String reason;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	/**
	 * 类目属性名称搜索--0xA001180c
	 * 
	 * @author Doncheng
	 */
	@ApiProtocol(cmdid =  "0xA001180cL" , desc =  " 类目属性名称搜索 " )
	class SearchPubNavByKey_WG {
		@ApiProtocol(cmdid =  "0xA001180cL" , desc =  " 类目属性名称搜索请求 " )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "是否需要搜索属性，必填;0:不需要,1:需要 " )
			uint32_t needAttr;

			@Field(desc =  "关键词，必填" )
			String key;

			@Field(desc =  "需要的个数，必填" )
			uint32_t count;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0xA001880cL" , desc =  " 类目属性名称搜索响应 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  " 结果集 " )
			Vector<NavMatchKeyDdo> navMatchKeyList;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	/**
	 * 获取商品发布及编辑类目属性数据--0xA001180d
	 * 
	 * @author Doncheng
	 */
	@ApiProtocol(cmdid =  "0xA001180dL" , desc =  " 获取商品发布及编辑类目属性数据 " )
	class GetPublishInfo_WG {
		@ApiProtocol(cmdid =  "0xA001180dL" , desc =  " 获取商品发布及编辑类目属性数据请求 " )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "类目id，必填" )
			uint32_t metaClassId;

			@Field(desc =  "非空则取一级属性，必填" )
			String classPath;

			@Field(desc =  "属性串，必填" )
			String attrPath;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0xA001880dL" , desc =  " 获取商品发布及编辑类目属性数据响应 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "导航数据" )
			NavExDdo nav;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	/**
	 * 获取发布路径信息--0xA001180e
	 * 
	 * @author Doncheng
	 */
	@ApiProtocol(cmdid =  "0xA001180eL" , desc =  " 获取发布路径信息 " )
	class GetPubPath_WG {
		@ApiProtocol(cmdid =  "0xA001180eL" , desc =  " 获取发布路径信息请求 " )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  " 导航id，必填" )
			uint32_t navId;

			@Field(desc =  " 属性项id，必填" )
			uint32_t attrId;

			@Field(desc =  " 属性值id，必填" )
			uint32_t optionId;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0xA001880eL" , desc =  " 获取发布路径信息响应 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "发布路径节点数据" )
			Vector<PublistNodeDdo> subNode;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	/**
	 * 获取路径信息--0xA001180f
	 * 
	 * @author jackyjiang
	 */
	@ApiProtocol(cmdid =  "0xA001180fL" , desc =  " 根据导航ID获取相应路径 " )
	class GetPathByNavId_WG {
		@ApiProtocol(cmdid =  "0xA001180fL" , desc =  " 根据导航ID获取相应路径请求 " )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "地图id，必填" )
			uint32_t mapId;

			@Field(desc =  "导航id，必填" )
			uint32_t navId;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0xA001880fL" , desc =  " 根据导航ID获取相应路径响应 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "导航路径" )
			Vector<NavEntryDdo> fullPath;
			
			@Field(desc =  "子节点集合")
			Vector<NavEntryDdo> childNav;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	/**
	 * 商详解析属性串和面包屑--0xA0011810
	 * 
	 * @author jackyjiang
	 */
	@ApiProtocol(cmdid =  "0xA0011810L" , desc =  " 商详解析属性串和面包屑请求 " )
	class ParseAttr4SX_WG {
		@ApiProtocol(cmdid =  "0xA0011810L" , desc =  " 商详解析属性串和面包屑请求 " )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "地图id，必填" )
			uint32_t mapId;

			@Field(desc =  "品类id，必填" )
			uint32_t metaId;

			@Field(desc =  "解析销售属性，key是属性项id，值是需要解释的项id的集合，必填" )
			Map<uint32_t, Vector<uint32_t>> saleAttrsIn;

			@Field(desc =  "解析一般展示属性，传进来是个属性id串,key作为这个串的标志，不限定是什么值，必填" )
			Map<uint32_t, String> commAttrsIn;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0xA0018810L" , desc =  " 商详解析属性串和面包屑请求 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "面包屑" )
			Vector<NavEntryDdo> fullPath;

			@Field(desc =  "解析销售属性" )
			Map<uint32_t, AttrDdo> saleAttrsOut;

			@Field(desc =  "解析一般展示属性" )
			Map<uint32_t, String> commAttrsOut;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	

	
	/**
	 * 商品发布获取品类属性字典（无属性值）--0xA0011811
	 * 
	 * @author hadeswang
	 */
	@ApiProtocol(cmdid =  "0xA0011811L" , desc =  " 商品发布获取品类属性字典（无属性值） " )
	class GetMetaClassAttrDic_WG {
		@ApiProtocol(cmdid =  "0xA0011811L" , desc =  " 商品发布获取品类属性字典（无属性值）请求 " )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "品类id，必填" )
			uint32_t metaId;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0xA0018811L" , desc =  " 商品发布获取品类属性字典（无属性值）返回 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc = "品类属性字典")
			Map<uint32_t, AttrDdo> attrDic;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	/**
	 * 商品属性串解析（不带校验）-- 0xA0011812
	 * 
	 * @author jackyjiang
	 */
	@ApiProtocol(cmdid =  "0xA0011812L" , desc =  " 商品属性串解析 " )
	class ParseAttrTextNoCheck_WG {
		@ApiProtocol(cmdid =  "0xA0011812L" , desc =  " 商品属性串解析请求 " )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "地图id，必填" )
			uint32_t mapId;

			@Field(desc =  "导航id，必填" )
			uint32_t navId;

			@Field(desc =  "属性串，必填" )
			String attrString;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0xA0018812L" , desc =  " 商品属性串解析响应 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "属性" )
			Vector<AttrDdo> attrBoList;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	/**
	 * 商品属性串解析，多类目批量查（给订单）-- 0xA0011813
	 * 
	 * @author jackyjiang
	 */
	@ApiProtocol(cmdid =  "0xA0011813L" , desc =  " 商品属性串解析 " )
	class ParseAttrTextMultiMeta_WG {
		@ApiProtocol(cmdid =  "0xA0011813L" , desc =  " 商品属性串解析请求 " )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "地图id，必填" )
			uint32_t mapId;

			@Field(desc =  "属性串，必填，外层map的key是一个标志，内层map的key是品类" )
			Map<uint32_t, Map<uint32_t, String> > commAttrsIn;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0xA0018813L" , desc =  " 商品属性串解析响应 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "属性串，必填，map的key是一个标志，string是解析好的属性串" )
			Map<uint32_t, String> commAttrsOut;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	/**
	 * 获取与路径无关的属性信息，指定属性值id--0xA0011814
	 * 
	 * @author jackyjiang
	 */
	@ApiProtocol(cmdid =  "0xA0011814L" , desc =  " 获取与路径无关的属性信息 " )
	class GetStaticAttrOp_WG {
		@ApiProtocol(cmdid =  "0xA0011814L" , desc =  " 获取与路径无关的属性信息请求 " )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "属性项Id，必填" )
			uint32_t attrId;

			@Field(desc =  "指定属性值id" )
			uint32_t OptionId;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0xA0018814L" , desc =  " 获取与路径无关的属性信息响应 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "属性项" )
			AttrDdo attr;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	/**
	 * 适应搜索需求属性字典改为vector排序--0xA0011815
	 * 
	 * @author jackyjiang
	 */
	@ApiProtocol(cmdid =  "0xA0011815L" , desc =  " 适应搜索需求属性字典改为vector排序 " )
	class GetNavExOrder_WG {
		@ApiProtocol(cmdid =  "0xA0011815L" , desc =  " 请求 " )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "地图id，必填" )
			uint32_t mapId;

			@Field(desc =  "导航id，必填" )
			uint32_t navId;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0xA0018815L" , desc =  " 响应 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "导航数据" )
			NavExOrderDdo navOrder;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	/**
	 * 批量获取全路径--0xA0011816
	 * 用于构建区分权限的类目导航树
	 * @author jackyjiang
	 */
	
	@ApiProtocol( cmdid= "0xA0011816L", desc = "批量获取全路径" )
	class GetMultiFullPath_WG
	{
		@ApiProtocol(cmdid= "0xA0011816L", desc = "批量获取全路径，请求")
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "地图id，必填" )
			uint32_t mapId;

			@Field(desc =  "品类id，必填，一次最多300个" )
			Set< uint32_t > metaId;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}
		
		@ApiProtocol(cmdid =  "0xA0018816L" , desc =  " 批量获取全路径,响应 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "key是品类id，Value是对应的全路径" )
			Map< uint32_t,Vector < NavEntryDdo > > fullPath;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	/**
	 * 获取导航属性关系里的属性值--0xA0011817
	 * 分批获取，避免数据量过大
	 * @author jackyjiang
	 */
	@ApiProtocol(cmdid= "0xA0011817L", desc = "获取导航属性关系里的属性值")
	class GetNavAttrOp_WG
	{
		@ApiProtocol(cmdid= "0xA0011817L", desc = "获取导航属性关系里的属性值，请求")
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "地图id，必填" )
			uint32_t mapId;

			@Field(desc =  "品类id，必填" )
			uint32_t metaId;
			
			@Field(desc = "属性项id")
			uint32_t attrId;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}
		
		@ApiProtocol(cmdid =  "0xA0018817L" , desc =  " 获取导航属性关系里的属性值,响应 " )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "属性实体里有导航属性关系里所有属性值" )
			AttrDdo attr;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	/**
	 * 新增移动端搜索地图  
	 * 新接口支持指定搜索地图id获取面包屑  getnavV2  0xA0011818L
	 * @author jackyjiang
	 */
	@ApiProtocol(cmdid = "0xA0011818L",desc = "获取导航节点信息，支持指定搜索地图id获取面包屑")
	class GetMetaV2_WG {
		@ApiProtocol(cmdid =  "0xA0011818L" , desc =  "获取导航节点信息，支持指定搜索地图id获取面包屑" )
		class Req {
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "场景Id，必填")
			uint32_t sceneId;

			@Field(desc =  "发布地图id，必填" )
			uint32_t pubMapId;
			
			@Field(desc =  "搜索地图id，必填" )
			uint32_t searchMapId;

			@Field(desc =  "品类id，必填" )
			uint32_t metaId;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid =  "0xA0018818L" , desc =  "获取导航节点信息，支持指定搜索地图id获取面包屑" )
		class Resp {
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc =  "品类数据" )
			NavDdo meta;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0xA0011819L", desc = "获得易迅的属性字典")
	class TransAttrStr_WG
	{
		class Req
		{
			@Field(desc = "机器码")
			String machineKey;

			@Field(desc = "来源")
			String source;

			@Field(desc =  "品类id，必填" )
			uint32_t metaId;
			
            @Field(desc = "源属性串，拍拍旧格式")
            String AttrA;

			@Field(desc = "保留字段")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;

            @Field(desc = "目标属性串C，网购格式")
            String AttrC;

			@Field(desc = "目标属性串C，网购格式，所有属性项和属性值都解析成字符串")
			String TextAttrC;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0xA001181AL", desc = "将属性串中卖家自定义部分去掉，给搜索专用")
	class TransAttrStr2_WG
	{
		class Req
		{
			@Field(desc = "机器码")
			String machineKey;

			@Field(desc = "调用来源")
			String source;

			@Field(desc =  "品类id，必填" )
			uint32_t metaId;
			
            @Field(desc = "源属性串，格式版本:3")
            String Attr3;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;

            @Field(desc = "目标属性串，格式版本:2")
            String Attr2;

			@Field(desc = "目标属性串，所有属性项和属性值都解析成字符串")
			String TextAttr;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

/////////////////////////////////////统一接口///////////////////
	
	@ApiProtocol(cmdid = "0xA0011820L", desc = " 获取品类数据")
	class GetMeta_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;

			@Field(desc = "品类Id")
			uint32_t MetaId;
			
			@Field(desc = "是否需要填充属性字典")
			uint32_t NeedAttrDic;

			@Field(desc = "属性字典中是否只需要属性项，而不需要属性值")
			uint32_t AttrOnly;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc = "品类数据")
			NavExDdo Meta;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0xA0011821L", desc = " 获取品类数据，批量")
	class GetMetas_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;

			@Field(desc = "品类Id，批量，不填表示获取全部的品类")
			Set<uint32_t> MetaId;
			
			@Field(desc = "是否需要填充属性字典")
			uint32_t NeedAttrDic;

			@Field(desc = "属性字典中是否只需要属性项，而不需要属性值")
			uint32_t AttrOnly;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;

			@Field(desc = "品类数据")
			Map<uint32_t, NavExDdo> Meta;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0xA0011822L", desc = "获取发布导航数据")
	class GetPubNav_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;

			@Field(desc = "导航Id，必填")
			uint32_t NavId;

			@Field(desc = "是否需要填充属性字典。叶子导航是品类，这个时候是有属性的")
			uint32_t NeedAttrDic;

			@Field(desc = "属性字典中是否只需要属性项，而不需要属性值")
			uint32_t AttrOnly;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc = "品类数据")
			NavExDdo Nav;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0xA0011823L", desc = "获取发布导航数据，批量")
	class GetPubNavs_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;
			
			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;

			@Field(desc = "导航Id，批量，必填")
			Set<uint32_t> NavId;

			@Field(desc = "是否需要填充属性字典。叶子导航是品类，这个时候是有属性的")
			uint32_t NeedAttrDic;

			@Field(desc = "属性字典中是否只需要属性项，而不需要属性值")
			uint32_t AttrOnly;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc = "品类数据")
			Map<uint32_t, NavExDdo> Nav;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0xA0011824L", desc = "获取发布导航数据")
	class GetSearchNav_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;

			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;

			@Field(desc = "导航Id，必填")
			uint32_t NavId;

			@Field(desc = "是否需要填充属性字典。叶子导航是品类，这个时候是有属性的")
			uint32_t NeedAttrDic;

			@Field(desc = "属性字典中是否只需要属性项，而不需要属性值")
			uint32_t AttrOnly;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;

			@Field(desc = "品类数据")
			NavExDdo Nav;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0xA0011825L", desc = "获取索搜索导航数据，批量")
	class GetSearchNavs_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;

			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;

			@Field(desc = "导航Id，批量，必填")
			Set<uint32_t> NavId;

			@Field(desc = "是否需要填充属性字典。叶子导航是品类，这个时候是有属性的")
			uint32_t NeedAttrDic;

			@Field(desc = "属性字典中是否只需要属性项，而不需要属性值")
			uint32_t AttrOnly;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;

			@Field(desc = "品类数据")
			Map<uint32_t, NavExDdo> Nav;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0xA0011826L", desc = "解析商品的属性串")
	class ParseAttrText_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;

			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;

			@Field(desc = "品类id，必填")
			uint32_t MetaId;

			@Field(desc = "属性串，必填")
			String AttrStr;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;

			@Field(desc = "解析结果")
			Vector<AttrDdo> Attr;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0xA0011827L", desc = "解析商品的属性串，批量")
	class ParseAttrTexts_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;

			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;

			@Field(desc = "key:品类id，value:属性串们，必填")
			Map<uint32_t, Set<String>> AttrIn;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;

			@Field(desc = "key：品类id；value：又是一个map，其中key是属性串，value是解析结果")
			Map<uint32_t, Map<String, Vector<AttrDdo>>> AttrOut;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0xA0011828L", desc = "获取搜索导航频道下的一级导航")
	class GetSearchTopNavByCatalog_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;

			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;

			@Field(desc = "频道Id，必填")
			uint32_t Catalog;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;

			@Field(desc = "一级导航数据")
			Map<uint32_t, NavExDdo> Nav;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0xA0011829L", desc = "获取搜索导航频道下的一级导航，批量")
	class GetSearchTopNavByCatalogs_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;

			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;

			@Field(desc = "频道Id，必填")
			Set<uint32_t> Catalog;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;

			@Field(desc = "一级导航数据")
			Map<uint32_t, NavExDdo> Nav;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0xA001182CL", desc = "获取品类下的某个属性项的信息")
	class GetMetaAttr_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;

			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;

			@Field(desc = "品类id")
			uint32_t MetaId;
			
			@Field(desc = "属性项id")
			uint32_t AttrId;
			
			@Field(desc = "是否只需要属性项信息，而不需要属性值信息")
			uint32_t AttrOnly;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;

			@Field(desc = "属性项")
			AttrDdo attr;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	@Member(cPlusNamespace = "b2b2c::nca::ddo", isNeedUFlag = true, isNeedTempVersion=true)
	public class Param_GetItemInfo_ALL {
		@Field(desc = "版本号", defaultValue = "0")
		uint32_t version;

		@Field(desc = "品类id")
		uint32_t MetaId;

		@Field(desc = "属性串")
		String AttrStr;

		@Field(desc = "是否需要搜索路径，0表示不需要")
		uint32_t NeedSearchPath;

		@Field(desc = "是否需要搜索路径上每一级的兄弟节点，0表示不需要")
		uint32_t NeedSearchBrotherPath;

		@Field(desc = "是否需要解析属性串，0表示不需要")
		uint32_t NeedAttr;
	}

	@Member(cPlusNamespace = "b2b2c::nca::ddo", isNeedUFlag = true, isNeedTempVersion=true)
	public class Result_GetItemInfo_ALL {
		@Field(desc = "版本号", defaultValue = "0")
		uint32_t version;

		@Field(desc = "搜索路径，下标0表示一级导航，下标最大表示叶子导航")
		Vector<NavEntryDdo> SearchPath;

		@Field(desc =  "搜索路径上每一级的兄弟节点，下标0表示一级导航的兄弟，下标最大表示叶子导航的兄弟")
		Vector<Vector<NavEntryDdo>> SearchBrotherPath;

		@Field(desc = "商品属性串解析出的属性项")
		Vector<AttrDdo> Attr;
	}

	@ApiProtocol(cmdid = "0xA001182DL", desc = "获取商品详情信息")
	class GetItemInfo_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;

			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;

			@Field(desc = "key是商品id，value是Param_GetItemInfo_ALL")
			Map<uint64_t, Param_GetItemInfo_ALL> param;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;

			@Field(desc = "key是商品id，value是Result_GetItemInfo_ALL")
			Map<uint64_t, Result_GetItemInfo_ALL> ret;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0xA001182EL", desc = "获取商品属于哪些搜索导航")
	class GetItemSearchNav_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;

			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;

			@Field(desc = "品类id")
			uint32_t MetaId;

			@Field(desc = "属性串")
			String AttrStr;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;

			@Field(desc = "这个商品属于哪些搜索导航id")
			Set<uint32_t> SearchNavId;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0xA001182FL", desc = "获取属性库的属性信息")
	class GetStaticAttr_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;

			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;

			@Field(desc = "属性项id")
			uint32_t AttrId;

			@Field(desc = "除了属性项信息外，还想获得哪些属性值信息")
			Set<uint32_t> OptionId;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;

			@Field(desc = "属性项和属性值信息")
			AttrDdo Attr;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0xA0011830L", desc = "获取品类上某个属性的子属性")
	class GetMetaChildAttr_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;

			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;

			@Field(desc = "品类id")
			uint32_t MetaId;

			@Field(desc = "父属性项id。如果和POptionId都为0，则表示取品类下所有的一级属性")
			uint32_t PAttrId;

			@Field(desc = "父属性值id")
			uint32_t POptionId;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;

			@Field(desc = "子属性")
			Map<uint32_t, AttrDdo> Attr;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0xA0011831L", desc = "根据名称查询品类")
	class GetMetaByName_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;

			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;

			@Field(desc = "品类名称")
			String Name;

			@Field(desc = "0表示精确查询，1表示模糊查询")
			uint32_t Like;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;

			@Field(desc = "品类")
			Vector<NavEntryDdo> Meta;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0xA0011832L", desc = "看看属性值在哪些搜索导航上作为过滤条件了")
	class GetSearchNavByOption_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;

			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;

			@Field(desc = "属性项id")
			uint32_t AttrId;

			@Field(desc = "属性值id")
			uint32_t OptionId;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;

			@Field(desc = "搜索导航id")
			Vector<uint32_t> NavId;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@Member(cPlusNamespace = "b2b2c::nca::ddo", isNeedUFlag = true, isNeedTempVersion=true)
	public class Result_TransAttrTexts_ALL {
		@Field(desc = "版本号", defaultValue = "0")
		uint32_t version;

        @Field(desc = "目标属性串，格式版本:2")
        String Attr2;

		@Field(desc = "目标属性串，所有属性项和属性值都解析成字符串")
		String TextAttr;
	}

	@ApiProtocol(cmdid = "0xA0011833L", desc = "转换属性串")
	class TransAttrTexts_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;

			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;

			@Field(desc = "key:品类id，value:属性串们")
			Map<uint32_t, Set<String>> AttrIn;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;

			@Field(desc = "key：品类id；value：又是一个map，其中key是属性串，value是Result_TransAttrTexts_ALL")
			Map<uint32_t, Map<String, Result_TransAttrTexts_ALL>> AttrOut;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@Member(cPlusNamespace = "b2b2c::nca::ddo", isNeedUFlag = true, isNeedTempVersion=true)
	public class Param_ParseAttrText2_ALL {
		@Field(desc = "版本号", defaultValue = "0")
		uint32_t version;

		@Field(desc = "品类id")
		uint32_t MetaId;

		@Field(desc = "属性串")
		String Attr;
		
		@Field(desc = "是否计算属性项加挂率，0表示不计算")
		uint32_t NeedAttrPermil;
	}

	@Member(cPlusNamespace = "b2b2c::nca::ddo", isNeedUFlag = true, isNeedTempVersion=true)
	public class Result_ParseAttrText2_ALL {
		@Field(desc = "版本号", defaultValue = "0")
		uint32_t version;

		@Field(desc = "解析结果")
		Vector<AttrDdo> Attr;
		
		@Field(desc = "属性项加挂率（千分比，例如198表示 198‰ 或 19.8%）")
		uint32_t AttrPermil;

		@Field(desc = "必选属性项加挂率（千分比，例如198表示 198‰ 或 19.8%）")
		uint32_t MustAttrPermil;

		@Field(desc = "可选属性项加挂率（千分比，例如198表示 198‰ 或 19.8%）")
		uint32_t OptionalAttrPermil;
	}

	@ApiProtocol(cmdid = "0xA0011835L", desc = "解析属性串")
	class ParseAttrText2_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;

			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;
			
			@Field(desc = "入参")
			Param_ParseAttrText2_ALL param;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;
			
			@Field(desc = "回参")
			Result_ParseAttrText2_ALL ret;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0xA0011836L", desc = "替代GetItemSearchNav_ALL")
	class GetItemSearchNav2_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;

			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;

			@Field(desc = "品类id")
			uint32_t MetaId;

			@Field(desc = "属性串")
			String AttrStr;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;

			@Field(desc = "这个商品属于哪些搜索导航id")
			Set<uint32_t> SearchNavId;
			
			@Field(desc = "属性串")
			String AttrStrValid;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0xA0011837L", desc = "获得全部的品类id")
	class GetAllMetaId_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;

			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;

			@Field(desc = "品类id")
			Set<uint32_t> MetaId;
			
			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0xA0011838L", desc = "获得全部的静态属性项id")
	class GetAllStaticAttrId_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;

			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;

			@Field(desc = "属性项id")
			Set<uint32_t> AttrId;
			
			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0xA0011839L", desc = "获得某个静态属性项下，全部的属性值id")
	class GetAllStaticOptionId_ALL
	{
		class Req
		{
			@Field(desc = "机器码，必填")
			String machineKey;

			@Field(desc = "调用来源，必填")
			String source;

			@Field(desc = "调用控制，业务相关")
			APIControl APIControl;
			
			@Field(desc = "属性项id")
			uint32_t AttrId;

			@Field(desc = "请求保留字")
			String inReserve;
		}

		class Resp
		{
			@Field(desc = "错误信息")
			String errmsg;

			@Field(desc = "属性值id")
			Set<uint32_t> OptionId;
			
			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
}
