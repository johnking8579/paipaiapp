package com.qq.qqbuy.thirdparty.idl.parseAttrText;

import com.paipai.component.c2cplatform.IAsynWebStub;

import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;

public class ParseAttrTextClient extends SupportIDLBaseClient{
    
    public static ParseAttrTextResp parseAttrText(ParseAttrTextReq req) {        
        long start = System.currentTimeMillis();
        ParseAttrTextResp resp = new ParseAttrTextResp();
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        ret = invokePaiPaiIDL(req, resp,stub);

        long timeCost = System.currentTimeMillis() - start;
        return ret == SUCCESS ? resp : null;
    }
    /**
     * 类目属性V3接口
     * @param req
     * @return
     * @date:2013-11-27
     * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
     * @version 1.0
     */
    public static com.qq.qqbuy.thirdparty.idl.parseAttrText.v3.ParseAttrTextResp parseAttrTextV3(
    		com.qq.qqbuy.thirdparty.idl.parseAttrText.v3.ParseAttrTextReq req) {
      
      long start = System.currentTimeMillis();
      com.qq.qqbuy.thirdparty.idl.parseAttrText.v3.ParseAttrTextResp resp = new com.qq.qqbuy.thirdparty.idl.parseAttrText.v3.ParseAttrTextResp();
      int ret;
      IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
      ret = invokePaiPaiIDL(req, resp,stub);

      long timeCost = System.currentTimeMillis() - start;
      return ret == SUCCESS ? resp : null;
  }
    public static void main(String args[]){
    	com.qq.qqbuy.thirdparty.idl.parseAttrText.v3.ParseAttrTextReq req = new com.qq.qqbuy.thirdparty.idl.parseAttrText.v3.ParseAttrTextReq();
    	req.setNavId(248571L);
    	req.setAttrString("version=1,37:faa|4ad:4|4c1:5|371c:1,2,3,4|794d:3|7c45:1|93b5:45^7c45:-");
    	com.qq.qqbuy.thirdparty.idl.parseAttrText.v3.ParseAttrTextResp resp = parseAttrTextV3(req);
    	System.out.println(resp);
    }

}
