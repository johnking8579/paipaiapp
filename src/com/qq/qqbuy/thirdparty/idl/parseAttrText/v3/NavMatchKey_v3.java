//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.weigou.mall.NcaDao.java

package com.qq.qqbuy.thirdparty.idl.parseAttrText.v3;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *类目搜索结构体
 *
 *@date 2013-11-27 05:00:47
 *
 *@since version:0
*/
public class NavMatchKey_v3  implements ICanSerializeObject
{
	/**
	 * 版本号, version需要小写
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 导航id
	 *
	 * 版本 >= 0
	 */
	 private long NavId;

	/**
	 * Matchinfo
	 *
	 * 版本 >= 0
	 */
	 private String MatchInfo = new String();

	/**
	 * PathInfo
	 *
	 * 版本 >= 0
	 */
	 private String PathInfo = new String();

	/**
	 * MatchType
	 *
	 * 版本 >= 0
	 */
	 private long MatchType;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUInt(NavId);
		bs.pushString(MatchInfo);
		bs.pushString(PathInfo);
		bs.pushUInt(MatchType);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		NavId = bs.popUInt();
		MatchInfo = bs.popString();
		PathInfo = bs.popString();
		MatchType = bs.popUInt();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号, version需要小写
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号, version需要小写
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取导航id
	 * 
	 * 此字段的版本 >= 0
	 * @return NavId value 类型为:long
	 * 
	 */
	public long getNavId()
	{
		return NavId;
	}


	/**
	 * 设置导航id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNavId(long value)
	{
		this.NavId = value;
	}


	/**
	 * 获取Matchinfo
	 * 
	 * 此字段的版本 >= 0
	 * @return MatchInfo value 类型为:String
	 * 
	 */
	public String getMatchInfo()
	{
		return MatchInfo;
	}


	/**
	 * 设置Matchinfo
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMatchInfo(String value)
	{
		this.MatchInfo = value;
	}


	/**
	 * 获取PathInfo
	 * 
	 * 此字段的版本 >= 0
	 * @return PathInfo value 类型为:String
	 * 
	 */
	public String getPathInfo()
	{
		return PathInfo;
	}


	/**
	 * 设置PathInfo
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPathInfo(String value)
	{
		this.PathInfo = value;
	}


	/**
	 * 获取MatchType
	 * 
	 * 此字段的版本 >= 0
	 * @return MatchType value 类型为:long
	 * 
	 */
	public long getMatchType()
	{
		return MatchType;
	}


	/**
	 * 设置MatchType
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMatchType(long value)
	{
		this.MatchType = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(NavMatchKey_v3)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段NavId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(MatchInfo);  //计算字段MatchInfo的长度 size_of(String)
				length += ByteStream.getObjectSize(PathInfo);  //计算字段PathInfo的长度 size_of(String)
				length += 4;  //计算字段MatchType的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
