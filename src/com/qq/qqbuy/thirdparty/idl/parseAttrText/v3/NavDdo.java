//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.weigou.mall.NcaDao.java

package com.qq.qqbuy.thirdparty.idl.parseAttrText.v3;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 * 导航简易结构体 
 *
 *@date 2013-11-27 05:00:47
 *
 *@since version:0
*/
public class NavDdo  implements ICanSerializeObject
{
	/**
	 *  版本号, version需要小写 
	 *
	 * 版本 >= 0
	 */
	 private short version;

	/**
	 *  导航信息 
	 *
	 * 版本 >= 0
	 */
	 private NavEntryDdo NavNode = new NavEntryDdo();

	/**
	 *  导航路径 
	 *
	 * 版本 >= 0
	 */
	 private Vector<NavEntryDdo> FullPath = new Vector<NavEntryDdo>();

	/**
	 *  搜索导航路径 
	 *
	 * 版本 >= 0
	 */
	 private Vector<NavEntryDdo> MetaSearchPath = new Vector<NavEntryDdo>();

	/**
	 *  儿子导航 
	 *
	 * 版本 >= 0
	 */
	 private Vector<NavEntryDdo> ChildNode = new Vector<NavEntryDdo>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUByte(version);
		bs.pushObject(NavNode);
		bs.pushObject(FullPath);
		bs.pushObject(MetaSearchPath);
		bs.pushObject(ChildNode);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUByte();
		NavNode = (NavEntryDdo) bs.popObject(NavEntryDdo.class);
		FullPath = (Vector<NavEntryDdo>)bs.popVector(NavEntryDdo.class);
		MetaSearchPath = (Vector<NavEntryDdo>)bs.popVector(NavEntryDdo.class);
		ChildNode = (Vector<NavEntryDdo>)bs.popVector(NavEntryDdo.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取 版本号, version需要小写 
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:short
	 * 
	 */
	public short getVersion()
	{
		return version;
	}


	/**
	 * 设置 版本号, version需要小写 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion(short value)
	{
		this.version = value;
	}


	/**
	 * 获取 导航信息 
	 * 
	 * 此字段的版本 >= 0
	 * @return NavNode value 类型为:NavEntryDdo
	 * 
	 */
	public NavEntryDdo getNavNode()
	{
		return NavNode;
	}


	/**
	 * 设置 导航信息 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:NavEntryDdo
	 * 
	 */
	public void setNavNode(NavEntryDdo value)
	{
		if (value != null) {
				this.NavNode = value;
		}else{
				this.NavNode = new NavEntryDdo();
		}
	}


	/**
	 * 获取 导航路径 
	 * 
	 * 此字段的版本 >= 0
	 * @return FullPath value 类型为:Vector<NavEntryDdo>
	 * 
	 */
	public Vector<NavEntryDdo> getFullPath()
	{
		return FullPath;
	}


	/**
	 * 设置 导航路径 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<NavEntryDdo>
	 * 
	 */
	public void setFullPath(Vector<NavEntryDdo> value)
	{
		if (value != null) {
				this.FullPath = value;
		}else{
				this.FullPath = new Vector<NavEntryDdo>();
		}
	}


	/**
	 * 获取 搜索导航路径 
	 * 
	 * 此字段的版本 >= 0
	 * @return MetaSearchPath value 类型为:Vector<NavEntryDdo>
	 * 
	 */
	public Vector<NavEntryDdo> getMetaSearchPath()
	{
		return MetaSearchPath;
	}


	/**
	 * 设置 搜索导航路径 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<NavEntryDdo>
	 * 
	 */
	public void setMetaSearchPath(Vector<NavEntryDdo> value)
	{
		if (value != null) {
				this.MetaSearchPath = value;
		}else{
				this.MetaSearchPath = new Vector<NavEntryDdo>();
		}
	}


	/**
	 * 获取 儿子导航 
	 * 
	 * 此字段的版本 >= 0
	 * @return ChildNode value 类型为:Vector<NavEntryDdo>
	 * 
	 */
	public Vector<NavEntryDdo> getChildNode()
	{
		return ChildNode;
	}


	/**
	 * 设置 儿子导航 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<NavEntryDdo>
	 * 
	 */
	public void setChildNode(Vector<NavEntryDdo> value)
	{
		if (value != null) {
				this.ChildNode = value;
		}else{
				this.ChildNode = new Vector<NavEntryDdo>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(NavDdo)
				length += 1;  //计算字段version的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(NavNode);  //计算字段NavNode的长度 size_of(NavEntryDdo)
				length += ByteStream.getObjectSize(FullPath);  //计算字段FullPath的长度 size_of(Vector)
				length += ByteStream.getObjectSize(MetaSearchPath);  //计算字段MetaSearchPath的长度 size_of(Vector)
				length += ByteStream.getObjectSize(ChildNode);  //计算字段ChildNode的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
