 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.weigou.mall.NcaDao.java

package com.qq.qqbuy.thirdparty.idl.parseAttrText.v3;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *商品属性串解析请求
 *
 *@date 2013-11-27 05:00:47
 *
 *@since version:0
*/
public class  ParseAttrTextReq implements IServiceObject
{
	/**
	 * Source
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * InReserve
	 *
	 * 版本 >= 0
	 */
	 private String InReserve = new String();

	/**
	 * 地图id
	 *
	 * 版本 >= 0
	 */
	 private long MapId;

	/**
	 * 导航id
	 *
	 * 版本 >= 0
	 */
	 private long NavId;

	/**
	 * 属性串
	 *
	 * 版本 >= 0
	 */
	 private String AttrString = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(Source);
		bs.pushString(InReserve);
		bs.pushUInt(MapId);
		bs.pushUInt(NavId);
		bs.pushString(AttrString);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		Source = bs.popString();
		InReserve = bs.popString();
		MapId = bs.popUInt();
		NavId = bs.popUInt();
		AttrString = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x53601809L;
	}


	/**
	 * 获取Source
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置Source
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取InReserve
	 * 
	 * 此字段的版本 >= 0
	 * @return InReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return InReserve;
	}


	/**
	 * 设置InReserve
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.InReserve = value;
	}


	/**
	 * 获取地图id
	 * 
	 * 此字段的版本 >= 0
	 * @return MapId value 类型为:long
	 * 
	 */
	public long getMapId()
	{
		return MapId;
	}


	/**
	 * 设置地图id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMapId(long value)
	{
		this.MapId = value;
	}


	/**
	 * 获取导航id
	 * 
	 * 此字段的版本 >= 0
	 * @return NavId value 类型为:long
	 * 
	 */
	public long getNavId()
	{
		return NavId;
	}


	/**
	 * 设置导航id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNavId(long value)
	{
		this.NavId = value;
	}


	/**
	 * 获取属性串
	 * 
	 * 此字段的版本 >= 0
	 * @return AttrString value 类型为:String
	 * 
	 */
	public String getAttrString()
	{
		return AttrString;
	}


	/**
	 * 设置属性串
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAttrString(String value)
	{
		this.AttrString = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(ParseAttrTextReq)
				length += ByteStream.getObjectSize(Source);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(InReserve);  //计算字段InReserve的长度 size_of(String)
				length += 4;  //计算字段MapId的长度 size_of(uint32_t)
				length += 4;  //计算字段NavId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(AttrString);  //计算字段AttrString的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
