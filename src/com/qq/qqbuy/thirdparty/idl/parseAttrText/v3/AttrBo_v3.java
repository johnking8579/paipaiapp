//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.weigou.mall.NcaDao.java

package com.qq.qqbuy.thirdparty.idl.parseAttrText.v3;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *导航属性项结构体
 *
 *@date 2013-11-27 05:00:47
 *
 *@since version:0
*/
public class AttrBo_v3  implements ICanSerializeObject
{
	/**
	 * 版本号, version需要小写
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 属性项id
	 *
	 * 版本 >= 0
	 */
	 private long AttrId;

	/**
	 * 导航id
	 *
	 * 版本 >= 0
	 */
	 private long NavId;

	/**
	 * 属性项名称
	 *
	 * 版本 >= 0
	 */
	 private String Name = new String();

	/**
	 * property
	 *
	 * 版本 >= 0
	 */
	 private long Property;

	/**
	 * 类型
	 *
	 * 版本 >= 0
	 */
	 private long Type;

	/**
	 * 父属性项id
	 *
	 * 版本 >= 0
	 */
	 private long PAttrId;

	/**
	 * 父属性值id
	 *
	 * 版本 >= 0
	 */
	 private long POptionId;

	/**
	 * 属性项描述
	 *
	 * 版本 >= 0
	 */
	 private String Desc = new String();

	/**
	 * 属性项排序
	 *
	 * 版本 >= 0
	 */
	 private long Order;

	/**
	 * 属性值集合
	 *
	 * 版本 >= 0
	 */
	 private Vector<OptionBo_v3> Options = new Vector<OptionBo_v3>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUInt(AttrId);
		bs.pushUInt(NavId);
		bs.pushString(Name);
		bs.pushUInt(Property);
		bs.pushUInt(Type);
		bs.pushUInt(PAttrId);
		bs.pushUInt(POptionId);
		bs.pushString(Desc);
		bs.pushUInt(Order);
		bs.pushObject(Options);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		AttrId = bs.popUInt();
		NavId = bs.popUInt();
		Name = bs.popString();
		Property = bs.popUInt();
		Type = bs.popUInt();
		PAttrId = bs.popUInt();
		POptionId = bs.popUInt();
		Desc = bs.popString();
		Order = bs.popUInt();
		Options = (Vector<OptionBo_v3>)bs.popVector(OptionBo_v3.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号, version需要小写
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号, version需要小写
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取属性项id
	 * 
	 * 此字段的版本 >= 0
	 * @return AttrId value 类型为:long
	 * 
	 */
	public long getAttrId()
	{
		return AttrId;
	}


	/**
	 * 设置属性项id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAttrId(long value)
	{
		this.AttrId = value;
	}


	/**
	 * 获取导航id
	 * 
	 * 此字段的版本 >= 0
	 * @return NavId value 类型为:long
	 * 
	 */
	public long getNavId()
	{
		return NavId;
	}


	/**
	 * 设置导航id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNavId(long value)
	{
		this.NavId = value;
	}


	/**
	 * 获取属性项名称
	 * 
	 * 此字段的版本 >= 0
	 * @return Name value 类型为:String
	 * 
	 */
	public String getName()
	{
		return Name;
	}


	/**
	 * 设置属性项名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setName(String value)
	{
		this.Name = value;
	}


	/**
	 * 获取property
	 * 
	 * 此字段的版本 >= 0
	 * @return Property value 类型为:long
	 * 
	 */
	public long getProperty()
	{
		return Property;
	}


	/**
	 * 设置property
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setProperty(long value)
	{
		this.Property = value;
	}


	/**
	 * 获取类型
	 * 
	 * 此字段的版本 >= 0
	 * @return Type value 类型为:long
	 * 
	 */
	public long getType()
	{
		return Type;
	}


	/**
	 * 设置类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setType(long value)
	{
		this.Type = value;
	}


	/**
	 * 获取父属性项id
	 * 
	 * 此字段的版本 >= 0
	 * @return PAttrId value 类型为:long
	 * 
	 */
	public long getPAttrId()
	{
		return PAttrId;
	}


	/**
	 * 设置父属性项id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPAttrId(long value)
	{
		this.PAttrId = value;
	}


	/**
	 * 获取父属性值id
	 * 
	 * 此字段的版本 >= 0
	 * @return POptionId value 类型为:long
	 * 
	 */
	public long getPOptionId()
	{
		return POptionId;
	}


	/**
	 * 设置父属性值id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPOptionId(long value)
	{
		this.POptionId = value;
	}


	/**
	 * 获取属性项描述
	 * 
	 * 此字段的版本 >= 0
	 * @return Desc value 类型为:String
	 * 
	 */
	public String getDesc()
	{
		return Desc;
	}


	/**
	 * 设置属性项描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDesc(String value)
	{
		this.Desc = value;
	}


	/**
	 * 获取属性项排序
	 * 
	 * 此字段的版本 >= 0
	 * @return Order value 类型为:long
	 * 
	 */
	public long getOrder()
	{
		return Order;
	}


	/**
	 * 设置属性项排序
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOrder(long value)
	{
		this.Order = value;
	}


	/**
	 * 获取属性值集合
	 * 
	 * 此字段的版本 >= 0
	 * @return Options value 类型为:Vector<OptionBo_v3>
	 * 
	 */
	public Vector<OptionBo_v3> getOptions()
	{
		return Options;
	}


	/**
	 * 设置属性值集合
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<OptionBo_v3>
	 * 
	 */
	public void setOptions(Vector<OptionBo_v3> value)
	{
		if (value != null) {
				this.Options = value;
		}else{
				this.Options = new Vector<OptionBo_v3>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(AttrBo_v3)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段AttrId的长度 size_of(uint32_t)
				length += 4;  //计算字段NavId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(Name);  //计算字段Name的长度 size_of(String)
				length += 4;  //计算字段Property的长度 size_of(uint32_t)
				length += 4;  //计算字段Type的长度 size_of(uint32_t)
				length += 4;  //计算字段PAttrId的长度 size_of(uint32_t)
				length += 4;  //计算字段POptionId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(Desc);  //计算字段Desc的长度 size_of(String)
				length += 4;  //计算字段Order的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(Options);  //计算字段Options的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
