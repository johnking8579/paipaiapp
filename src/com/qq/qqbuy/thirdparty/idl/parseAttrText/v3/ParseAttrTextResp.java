 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.weigou.mall.NcaDao.java

package com.qq.qqbuy.thirdparty.idl.parseAttrText.v3;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Vector;

/**
 *商品属性串解析响应
 *
 *@date 2013-11-27 05:00:47
 *
 *@since version:0
*/
public class  ParseAttrTextResp implements IServiceObject
{
	public long result;
	/**
	 * 属性
	 *
	 * 版本 >= 0
	 */
	 private Vector<AttrBo_v3> AttrBoList = new Vector<AttrBo_v3>();

	/**
	 * OutReserve
	 *
	 * 版本 >= 0
	 */
	 private String OutReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(AttrBoList);
		bs.pushString(OutReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		AttrBoList = (Vector<AttrBo_v3>)bs.popVector(AttrBo_v3.class);
		OutReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x53608809L;
	}


	/**
	 * 获取属性
	 * 
	 * 此字段的版本 >= 0
	 * @return AttrBoList value 类型为:Vector<AttrBo_v3>
	 * 
	 */
	public Vector<AttrBo_v3> getAttrBoList()
	{
		return AttrBoList;
	}


	/**
	 * 设置属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<AttrBo_v3>
	 * 
	 */
	public void setAttrBoList(Vector<AttrBo_v3> value)
	{
		if (value != null) {
				this.AttrBoList = value;
		}else{
				this.AttrBoList = new Vector<AttrBo_v3>();
		}
	}


	/**
	 * 获取OutReserve
	 * 
	 * 此字段的版本 >= 0
	 * @return OutReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return OutReserve;
	}


	/**
	 * 设置OutReserve
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		this.OutReserve = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ParseAttrTextResp)
				length += ByteStream.getObjectSize(AttrBoList);  //计算字段AttrBoList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(OutReserve);  //计算字段OutReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
