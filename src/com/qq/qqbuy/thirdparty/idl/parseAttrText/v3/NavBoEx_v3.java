//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.weigou.mall.NcaDao.java

package com.qq.qqbuy.thirdparty.idl.parseAttrText.v3;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import com.paipai.lang.GenericWrapper;
import java.util.Map;
import com.paipai.lang.uint32_t;
import java.util.Vector;
import java.util.HashMap;

/**
 *导航简易结构体
 *
 *@date 2013-11-27 05:00:47
 *
 *@since version:0
*/
public class NavBoEx_v3  implements ICanSerializeObject
{
	/**
	 * 版本号, version需要小写
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 导航信息
	 *
	 * 版本 >= 0
	 */
	 private NavEntry_v3 NavNode = new NavEntry_v3();

	/**
	 * 导航路径
	 *
	 * 版本 >= 0
	 */
	 private Vector<NavEntry_v3> FullPath = new Vector<NavEntry_v3>();

	/**
	 * 搜索导航路径
	 *
	 * 版本 >= 0
	 */
	 private Vector<NavEntry_v3> MetaSearchPath = new Vector<NavEntry_v3>();

	/**
	 * 儿子导航
	 *
	 * 版本 >= 0
	 */
	 private Vector<NavEntry_v3> ChildNode = new Vector<NavEntry_v3>();

	/**
	 * 直接儿子属性
	 *
	 * 版本 >= 0
	 */
	 private Map<uint32_t,Vector<SubAttrOption_v3>> ChildAttrId = new HashMap<uint32_t,Vector<SubAttrOption_v3>>();

	/**
	 * 属性字典
	 *
	 * 版本 >= 0
	 */
	 private Map<uint32_t,AttrBo_v3> AttrDic = new HashMap<uint32_t,AttrBo_v3>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushObject(NavNode);
		bs.pushObject(FullPath);
		bs.pushObject(MetaSearchPath);
		bs.pushObject(ChildNode);
		bs.pushObject(ChildAttrId);
		bs.pushObject(AttrDic);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		NavNode = (NavEntry_v3) bs.popObject(NavEntry_v3.class);
		FullPath = (Vector<NavEntry_v3>)bs.popVector(NavEntry_v3.class);
		MetaSearchPath = (Vector<NavEntry_v3>)bs.popVector(NavEntry_v3.class);
		ChildNode = (Vector<NavEntry_v3>)bs.popVector(NavEntry_v3.class);

		// 生成反序列化属性ChildAttrId相应的范型参数包裹对象(包裹了该属性中范型的类型)。 
		GenericWrapper ChildAttrIdPaiPai00 = new GenericWrapper();
		ChildAttrIdPaiPai00.setType(HashMap.class);
		GenericWrapper[] ChildAttrIdPaiPaiArray00= new GenericWrapper[2];
		ChildAttrIdPaiPaiArray00[0] = new GenericWrapper(uint32_t.class);
		ChildAttrIdPaiPaiArray00[1] = new GenericWrapper();
		GenericWrapper ChildAttrIdPaiPai11 = new GenericWrapper();
		ChildAttrIdPaiPai11.setType(Vector.class);
		GenericWrapper[] ChildAttrIdPaiPaiArray11= new GenericWrapper[2];
		ChildAttrIdPaiPaiArray11[0] = new GenericWrapper(SubAttrOption_v3.class);
		ChildAttrIdPaiPai11.setGenericParameters(ChildAttrIdPaiPaiArray11);


		ChildAttrIdPaiPaiArray00[1] = ChildAttrIdPaiPai11;
		ChildAttrIdPaiPai00.setGenericParameters(ChildAttrIdPaiPaiArray00);



		ChildAttrId = (Map<uint32_t,Vector<SubAttrOption_v3>>)bs.popObject(ChildAttrIdPaiPai00);
		AttrDic = (Map<uint32_t,AttrBo_v3>)bs.popMap(uint32_t.class,AttrBo_v3.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号, version需要小写
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号, version需要小写
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取导航信息
	 * 
	 * 此字段的版本 >= 0
	 * @return NavNode value 类型为:NavEntry_v3
	 * 
	 */
	public NavEntry_v3 getNavNode()
	{
		return NavNode;
	}


	/**
	 * 设置导航信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:NavEntry_v3
	 * 
	 */
	public void setNavNode(NavEntry_v3 value)
	{
		if (value != null) {
				this.NavNode = value;
		}else{
				this.NavNode = new NavEntry_v3();
		}
	}


	/**
	 * 获取导航路径
	 * 
	 * 此字段的版本 >= 0
	 * @return FullPath value 类型为:Vector<NavEntry_v3>
	 * 
	 */
	public Vector<NavEntry_v3> getFullPath()
	{
		return FullPath;
	}


	/**
	 * 设置导航路径
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<NavEntry_v3>
	 * 
	 */
	public void setFullPath(Vector<NavEntry_v3> value)
	{
		if (value != null) {
				this.FullPath = value;
		}else{
				this.FullPath = new Vector<NavEntry_v3>();
		}
	}


	/**
	 * 获取搜索导航路径
	 * 
	 * 此字段的版本 >= 0
	 * @return MetaSearchPath value 类型为:Vector<NavEntry_v3>
	 * 
	 */
	public Vector<NavEntry_v3> getMetaSearchPath()
	{
		return MetaSearchPath;
	}


	/**
	 * 设置搜索导航路径
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<NavEntry_v3>
	 * 
	 */
	public void setMetaSearchPath(Vector<NavEntry_v3> value)
	{
		if (value != null) {
				this.MetaSearchPath = value;
		}else{
				this.MetaSearchPath = new Vector<NavEntry_v3>();
		}
	}


	/**
	 * 获取儿子导航
	 * 
	 * 此字段的版本 >= 0
	 * @return ChildNode value 类型为:Vector<NavEntry_v3>
	 * 
	 */
	public Vector<NavEntry_v3> getChildNode()
	{
		return ChildNode;
	}


	/**
	 * 设置儿子导航
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<NavEntry_v3>
	 * 
	 */
	public void setChildNode(Vector<NavEntry_v3> value)
	{
		if (value != null) {
				this.ChildNode = value;
		}else{
				this.ChildNode = new Vector<NavEntry_v3>();
		}
	}


	/**
	 * 获取直接儿子属性
	 * 
	 * 此字段的版本 >= 0
	 * @return ChildAttrId value 类型为:Map<uint32_t,Vector<SubAttrOption_v3>>
	 * 
	 */
	public Map<uint32_t,Vector<SubAttrOption_v3>> getChildAttrId()
	{
		return ChildAttrId;
	}


	/**
	 * 设置直接儿子属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<uint32_t,Vector<SubAttrOption_v3>>
	 * 
	 */
	public void setChildAttrId(Map<uint32_t,Vector<SubAttrOption_v3>> value)
	{
		if (value != null) {
				this.ChildAttrId = value;
		}else{
				this.ChildAttrId = new HashMap<uint32_t,Vector<SubAttrOption_v3>>();
		}
	}


	/**
	 * 获取属性字典
	 * 
	 * 此字段的版本 >= 0
	 * @return AttrDic value 类型为:Map<uint32_t,AttrBo_v3>
	 * 
	 */
	public Map<uint32_t,AttrBo_v3> getAttrDic()
	{
		return AttrDic;
	}


	/**
	 * 设置属性字典
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<uint32_t,AttrBo_v3>
	 * 
	 */
	public void setAttrDic(Map<uint32_t,AttrBo_v3> value)
	{
		if (value != null) {
				this.AttrDic = value;
		}else{
				this.AttrDic = new HashMap<uint32_t,AttrBo_v3>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(NavBoEx_v3)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(NavNode);  //计算字段NavNode的长度 size_of(NavEntry_v3)
				length += ByteStream.getObjectSize(FullPath);  //计算字段FullPath的长度 size_of(Vector)
				length += ByteStream.getObjectSize(MetaSearchPath);  //计算字段MetaSearchPath的长度 size_of(Vector)
				length += ByteStream.getObjectSize(ChildNode);  //计算字段ChildNode的长度 size_of(Vector)
				length += ByteStream.getObjectSize(ChildAttrId);  //计算字段ChildAttrId的长度 size_of(Map)
				length += ByteStream.getObjectSize(AttrDic);  //计算字段AttrDic的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
