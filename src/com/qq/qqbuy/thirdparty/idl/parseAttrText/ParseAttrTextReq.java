package com.qq.qqbuy.thirdparty.idl.parseAttrText;

import java.util.Vector;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

public class ParseAttrTextReq
  implements IServiceObject
{
  private String source;
  private String inReserve;
  private long mapId;
  private long navId;
  private String attrString;
  private Vector<CAttrBo> attrBoList = new Vector();
  private String outReserve;

  public void setSource(String value)
  {
    this.source = value;
  }

  public String getSource()
  {
    return this.source;
  }

  public void setInReserve(String value)
  {
    this.inReserve = value;
  }

  public String getInReserve()
  {
    return this.inReserve;
  }

  public void setMapId(long value)
  {
    this.mapId = value;
  }

  public long getMapId()
  {
    return this.mapId;
  }

  public void setNavId(long value)
  {
    this.navId = value;
  }

  public long getNavId()
  {
    return this.navId;
  }

  public void setAttrString(String value)
  {
    this.attrString = value;
  }

  public String getAttrString()
  {
    return this.attrString;
  }

  public void setAttrBoList(Vector<CAttrBo> value)
  {
    this.attrBoList = value;
  }

  public Vector<CAttrBo> getAttrBoList()
  {
    return this.attrBoList;
  }

  public void setOutReserve(String value)
  {
    this.outReserve = value;
  }

  public String getOutReserve()
  {
    return this.outReserve;
  }

  public int Serialize(ByteStream bs) throws Exception
  {
    bs.pushString(this.source);
    bs.pushString(this.inReserve);
    bs.pushUInt(this.mapId);
    bs.pushUInt(this.navId);
    bs.pushString(this.attrString);
    bs.pushVector(this.attrBoList);
    bs.pushString(this.outReserve);
    return bs.getWrittenLength();
  }

  public int UnSerialize(ByteStream bs) throws Exception
  {
    this.source = bs.popString();
    this.inReserve = bs.popString();
    this.mapId = bs.popUInt();
    this.navId = bs.popUInt();
    this.attrString = bs.popString();
    this.attrBoList = (Vector<CAttrBo>) bs.popVector(CAttrBo.class);
    this.outReserve = bs.popString();
    return bs.getReadLength();
  }

  public long getCmdId() {
    return 1397757956L;
  }
}