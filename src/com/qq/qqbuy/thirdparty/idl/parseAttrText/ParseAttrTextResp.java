package com.qq.qqbuy.thirdparty.idl.parseAttrText;

import java.util.Vector;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

public class ParseAttrTextResp
  implements IServiceObject
{
  private long result;
  private Vector<CAttrBo> attrBoList = new Vector();
  private String outReserve;

  public void setAttrBoList(Vector<CAttrBo> value)
  {
    this.attrBoList = value;
  }

  public Vector<CAttrBo> getAttrBoList()
  {
    return this.attrBoList;
  }

  public void setOutReserve(String value)
  {
    this.outReserve = value;
  }

  public String getOutReserve()
  {
    return this.outReserve;
  }

  public int Serialize(ByteStream bs) throws Exception
  {
    bs.pushUInt(this.result);
    bs.pushVector(this.attrBoList);
    bs.pushString(this.outReserve);
    return bs.getWrittenLength();
  }

  public int UnSerialize(ByteStream bs) throws Exception
  {
    this.result = bs.popUInt();
    this.attrBoList = (Vector<CAttrBo>) bs.popVector(CAttrBo.class);
    this.outReserve = bs.popString();
    return bs.getReadLength();
  }

  public long getCmdId() {
    return 1397786628L;
  }

  public long getResult()
  {
    return this.result;
  }

  public void setResult(long result)
  {
    this.result = result;
  }
}