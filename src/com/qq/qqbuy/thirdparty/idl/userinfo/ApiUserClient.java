package com.qq.qqbuy.thirdparty.idl.userinfo;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.qq.qqbuy.common.exception.ExternalInterfaceException;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.apiuser.APIUserProfile;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.apiuser.ApiGetUserSimpleProfileReq;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.apiuser.ApiGetUserSimpleProfileResp;

/**
 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
 */
public class ApiUserClient extends SupportIDLBaseClient {
	
	 /**
     * 获取详细的用户信息,需鉴权
     * 
     * @param clientIp
     * @return
     */
//    public static ApiGetUserDetailProfileResp getUsetInfo(long uin ,String skey,String machineKey)
//    {
//        ApiGetUserDetailProfileReq req= new ApiGetUserDetailProfileReq();
//        req.setUin(uin);
//        req.setSource("webapp_paipai");
//
//        ApiGetUserDetailProfileResp resp = new ApiGetUserDetailProfileResp();
//
//        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
//        stub.setUin(uin);
//        stub.setOperator(uin);
//        stub.setSkey(skey.getBytes());
//        stub.setMachineKey(machineKey.getBytes());
//
//        int  ret = invokePaiPaiIDL(req, resp);
//        
//        return ret == SUCCESS ? resp : null;
//    }
    
    
    /**
     * 获取简单的用户信息,无需鉴权
     * @param clientIp
     * @return
     */
    public static APIUserProfile getUserSimpleInfo(long uin)	{
        ApiGetUserSimpleProfileReq req= new ApiGetUserSimpleProfileReq();
        ApiGetUserSimpleProfileResp resp = new ApiGetUserSimpleProfileResp();
        req.setUin(uin);
        req.setSource("webapp_paipai");

        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setUin(uin);
        stub.setOperator(uin);
        
        invoke(stub, req, resp);
        if(resp.getResult() != 0)
        	throw new ExternalInterfaceException(0, resp.getResult());
        return resp.getSimpleProfile();
    }

}

