package com.qq.qqbuy.thirdparty.idl.userinfo;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.apache.commons.lang.StringUtils;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.BizConstant;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.util.MD5Coding;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.AddFeedBackReq;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.AddFeedBackResp;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.ApiLoginPo;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.ApiLoginReq;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.ApiLoginResp;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.FeedBack;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.FindBuyerVIPAccountReq;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.FindBuyerVIPAccountResp;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.GetFlagLevel4SingleReq;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.GetFlagLevel4SingleResp;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.GetGenerateSidReq;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.GetGenerateSidResp;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.GetUserInfo0x480Req;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.GetUserInfo0x480Resp;

/**
 * 获取用户资料的IDL接口
 * 
 * @author minghe
 * @Created 2012-2-27
 */
public class UserInfoClient extends SupportIDLBaseClient
{

    private final int SUPERQQ_FLAGTYPE = 11;

    public String getUserInfo(long uin, byte[] a8, byte[] crtTime,
            String clientIp)
    {
        String nickName = "";
        GetUserInfo0x480Req req = new GetUserInfo0x480Req();
        req.setBuyerUin(uin);
        if (a8.length != 4 || crtTime.length != 2)
        {
        	Log.run.error("IDL#getUserInfo a8 or crtTime length wrong [a8.length="
                    + a8.length + ";crtTime.length=" + crtTime.length + "]");
            return nickName;
        }
        byte[] sk = new byte[6];
        for (int i = 0; i < 4; i++)
        {
            sk[i] = a8[i];
        }
        for (int i = 0; i < 2; i++)
        {
            sk[i + 4] = crtTime[i];
        }
        req.setSk(sk);
        req.setClientIp(clientIp);
        GetUserInfo0x480Resp resp = new GetUserInfo0x480Resp();
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
        if (EnvManager.isNotIdc())
        {
            ret = invokePaiPaiIDL(req, resp, stub, GAMMA_IP, 9024);
        } else
        {
            ret = invokePaiPaiIDL(req, resp, stub);
        }
        try
        {
            Util.decode(resp, BizConstant.OPENAPI_CHARSET);
        } catch (Exception e)
        {
        	Log.run.error("resp decode error:" + e.getMessage());
        }
        if (ret == SUCCESS && resp != null && resp.getUserInfo() != null
                && StringUtils.isNotEmpty(resp.getUserInfo().getNickName()))
            try
            {
                nickName = URLDecoder.decode(resp.getUserInfo().getNickName(),
                        "utf-8");
            } catch (UnsupportedEncodingException e)
            {
            	Log.run.error("URLDecoder decode error:" + e.getMessage());
            }
        return nickName;
    }

    /**
     * 获取临时sid
     * 
     * @param clientIp
     * @return
     */
    public String getTempSid(String clientIp)
    {
        GetGenerateSidReq req = new GetGenerateSidReq();
        req.setClientIp(clientIp);
        GetGenerateSidResp resp = new GetGenerateSidResp();

        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
        if (EnvManager.isNotIdc())
        {
            ret = invokePaiPaiIDL(req, resp, stub, GAMMA_IP, 9024);
        } else
        {
            ret = invokePaiPaiIDL(req, resp, stub);
        }
        return (ret == SUCCESS && resp.getErrCode() == SUCCESS) ? resp.getSid()
                : null;
    }

    /**
     * 获取用户的超Q等级
     * 
     * @param uin
     * @return
     */
    public GetFlagLevel4SingleResp getSuperQQLevel(long uin)
    {
        GetFlagLevel4SingleReq req = new GetFlagLevel4SingleReq();
        req.setBuyerUin(uin);
        req.setFlagType(SUPERQQ_FLAGTYPE);
        GetFlagLevel4SingleResp resp = new GetFlagLevel4SingleResp();
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
        if (EnvManager.isNotIdc())
        {
            ret = invokePaiPaiIDL(req, resp, stub, GAMMA_IP, 9024);
        } else
        {
            ret = invokePaiPaiIDL(req, resp, stub);
        }
        return ret == SUCCESS ? resp : null;
    }

    /**
     * 获取用户的彩钻等级
     * 
     * @param uin
     * @return
     */
    public FindBuyerVIPAccountResp findBuyerVIPAccount(long uin)
    {
        FindBuyerVIPAccountReq req = new FindBuyerVIPAccountReq();
        req.setDwBuyerUin(uin);
        FindBuyerVIPAccountResp resp = new FindBuyerVIPAccountResp();
        int ret = -1;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();// GBK
        if (EnvManager.isNotIdc())
        {
            ret = invokePaiPaiIDL(req, resp, stub, GAMMA_IP, 9111);
        } else
        {
            ret = invokePaiPaiIDL(req, resp, stub);
        }
        try
        {
            Util.decode(resp, BizConstant.OPENAPI_CHARSET);
            if(resp.getBuyerVIPAccountInfo()!=null){
                Util.decode(resp.getBuyerVIPAccountInfo(), BizConstant.OPENAPI_CHARSET);
            }
        } catch (Exception e)
        {
        	Log.run.error("resp decode FindBuyerVIPAccountResp error:"
                    + e.getMessage());
        }
        return ret == SUCCESS ? resp : null;
    }
    /**
     * 反馈
     * 
     * @param uin
     * @return
     */
    public AddFeedBackResp addFeedBack(long uin, String content )
    {
        AddFeedBackReq req= new AddFeedBackReq();
        FeedBack feedBack=new FeedBack();
        feedBack.setUin(uin);
        feedBack.setContent(content);
        req.setFeedBack(feedBack);
        AddFeedBackResp resp = new AddFeedBackResp();
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();// GBK
        if (EnvManager.isNotIdc())
        {
            ret = invokePaiPaiIDL(req, resp, stub, GAMMA_IP, 9024);
        } else
        {
            ret = invokePaiPaiIDL(req, resp, stub);
        }
       
        return ret == SUCCESS ? resp : null;
    }
    /**
     * 用sid换取拍拍的skey
     * @param uin QQ号
     * @param sid sid号
     * @return
     */
    public ApiLoginResp loginWithSid(long uin, String sid )
    {
        ApiLoginReq req= new ApiLoginReq();
        
        ApiLoginPo po = new ApiLoginPo();
		po.setUin(uin);
		po.setAuthCode(sid);
		po.setAuthType(2); //2-无线sid验证
		req.setSceneId(0); //场景固定为0
		req.setLoginFilter(po);
		req.setSource(CALL_IDL_SOURCE);
		String mk = getMk(uin + "", System.currentTimeMillis() + "");
		req.setMachineKey(mk);
		String skey = getSk(uin + "", mk);
        ApiLoginResp resp = new ApiLoginResp();
        int ret;
        AsynWebStub stub = (AsynWebStub)WebStubFactory.getWebStub4PaiPaiGBK();// GBK 
        stub.setSkey(skey.getBytes());
        stub.setOperator(uin);
        stub.setUin(uin);
        stub.setClientIP(WebStubFactory.iP2Long("60.176.39.188"));
        ret = invokePaiPaiIDL(req, resp, stub);

        return ret == SUCCESS ? resp : null;
    }
    /**
     * 用lseky换取拍拍的skey
     * @param uin QQ
     * @param lskey lskey
     * @return
     */
    public ApiLoginResp loginWithLskey(long uin, String lskey )
    {
        ApiLoginReq req= new ApiLoginReq();
        
        ApiLoginPo po = new ApiLoginPo();
		po.setUin(uin);
		po.setAuthCode(lskey);
		po.setAuthType(1); //2-无线sid验证
		req.setSceneId(0); //场景固定为0
		req.setLoginFilter(po);
		req.setSource(CALL_IDL_SOURCE);
		String mk = getMk(uin + "", System.currentTimeMillis() + "");
		req.setMachineKey(mk);
		//String skey = getSk(uin + "", mk);
        ApiLoginResp resp = new ApiLoginResp();
        int ret;
        AsynWebStub stub = (AsynWebStub)WebStubFactory.getWebStub4PaiPaiGBK();// GBK 
        stub.setSkey(getSk(uin + "", mk).getBytes());
        stub.setOperator(uin);
        stub.setUin(uin);
        stub.setClientIP(WebStubFactory.iP2Long("60.176.39.188"));

        ret = invokePaiPaiIDL(req, resp, stub);

        return ret == SUCCESS ? resp : null;
    }
    
    private static String getMk(String qq, String key) {
		String mk = "11" + MD5Coding.encode2HexStr((qq + key).getBytes()).toLowerCase().substring(0, 15);
		return mk;
	}

    private static String getSk(String qq, String mk) {
		String sk = "@" + MD5Coding.encode2HexStr((qq + mk).getBytes()).toLowerCase().substring(0, 8) + "@";
		return sk;
	}
    
    public static void main(String args[]){
    
//    	System.out.println(neUserInfoClient.loginWithSid(361369681,"AZuFNUJo6yUCJFeZ-EyMZfEq").getKey());
//    	System.out.println(UserInfoClient.loginWithLskey(151738366,"00030000fbdb63d919381cfc6aeee8dfb9d56396208ca9fac4624910e917fde8b09607d89c679251b92d961a").getKey());

    	UserInfoClient client = new UserInfoClient();
    	FindBuyerVIPAccountResp resp = client.findBuyerVIPAccount(13267853L);
    	System.out.println(resp);
    }
}
