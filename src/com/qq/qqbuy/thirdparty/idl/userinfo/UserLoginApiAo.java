package com.qq.qqbuy.thirdparty.idl.userinfo;

import com.paipai.lang.uint32_t;
import com.paipai.lang.uint64_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;

@HeadApiProtocol(cPlusNamespace = "b2b2c::user::ao", needInit = true, needReset = true)
public class UserLoginApiAo {
	@Member(desc = "登录po", cPlusNamespace = "b2b2c::user::po", isNeedUFlag = false)
	public class ApiLoginPo
	{
		@Field(desc = "版本号", defaultValue = "0")
		uint32_t 	version;
		
		@Field(desc = "QQ号")
		uint64_t	uin;
		
		@Field(desc = "鉴权字符串，必填，调用方标识已登录的字符串")
		String		authCode;
		
		@Field(desc = "鉴权类型，1-lskey 2-无线sid验证 3-微购uin+ticket 4-工号权限系统 5-微信ticket")
		uint32_t	authType;
		
		@Field(desc = "appid，authType填5时必填")
		String 		appid;
	}
	
	@ApiProtocol(cmdid = "0xA0A81801L", desc = "api统一登录接口，返回skey和uid。在校验authcode之前，会先校验rCntlInfo中OperatorKey（由rCntlInfo中OperatorUin+strMachineKey加密得到）")
	class ApiLogin
	{
		@ApiProtocol(cmdid = "0xA0A81801L", desc = "移动端登录请求类")
		class Req{
			@Field(desc = "机器码，必填")
			String machineKey;
	
			@Field(desc = "调用来源，必填")
			String source;
	
			@Field(desc = "场景id，必填")
			uint32_t sceneId;
			
			@Field(desc = "登录filter")
			ApiLoginPo	loginFilter;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}
		@ApiProtocol(cmdid = "0xA0A88801L", desc = "移动端登录响应类")
		class Resp{
			@Field(desc = "用户uid")
			uint64_t	uid;
			
			@Field(desc = "session key")
			String 		key;
			
			@Field(desc = "错误信息")
			String 		errmsg;
			
			@Field(desc = "输出保留字")
			String 		outReserve;
		}
	}
}
