 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.userinfo.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *
 *
 *@date 2012-05-04 07:36::41
 *
 *@since version:0
*/
public class  GetFlagLevel4SingleReq implements IServiceObject
{
	/**
	 * 买家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long buyerUin;

	/**
	 * 图标类型[1] 红钻 0-7级[2] 黄钻 0-7级[3] QQ飞行岛 0-7级[4] 地下城与勇士 0-7级[5] CF穿越火线 0-7级[6] QQ三国 0-7级[7] QQ炫舞 0-7级[8] 自由幻想 0-7级[9] QQ飞车 0-7级[10] 手机腾讯网 0-7级[11] 超级QQ 0-7级[12] 寻仙 0-7级[13] 问问 0-7级[14] 绿钻 0-7级[15] 搜搜网页搜索带等级标志位 0-3级[16] 蓝钻贵族等级标志位 0-7级[17] 黑钻 0-7级[18] 拍拍等级0-7[19] AVA等级控制位 0-4[20] AVA等级图标 0-7[21] 英雄岛等级 0-7[22] 公益图标 1-8[23] 烽火战国0～7[41] QQ封神记0-7[42] 三国英雄传0-7[43] 大明龙权0-7[44] 拍拍彩钻带等级标志位0-7[45]QQ幻想世界等级标志位0-7[46]QQ音速（R2beat）等级标志位0-7[47]QQ西游等级标志位0-7[48]七雄争霸等级标志位0-7[49]QQ宠物粉钻贵族等级标志位0-7[50]3366等级标志位0-7[51]安全达人图标等级标志位0-7[52]电脑管家图标等级标志位0-7[53] 英雄联盟等级图标(0-7)   [54] 英雄杀等级图标(0-7)   [55] 丝路英雄等级图标(0-7)   [56] 超级明星等级图标 (0-7)   [57] WOZ等级图标(0-7)   [58] QQ宝石等级图标(0-7)   [59] QQ宠物企鹅等级图标(0-7)   [60] 洛克王国图标(0-3)[61]体验计划等级图标(0-7)[62] 财付通-VIP等级图标(0-7)[63] QQ仙侠传等级图标(0-7)[64] QQTalk等级图标(0-7)[65] 生化战场等级图标(0-7)[66]楚河汉界等级图标(0-7)[67]《江湖笑》IM合作等级图标(0-7)[68]小白大作战等级图标标(0-6)[69]魔幻大陆等级图标(0-7)[70]微博等级图标标志位(0-7)[71]Q宠大乐斗等级图标标志位(0-7)[72]摩登城市等级图标标志位(0-7)[73]QQ九仙等级图标标志位(0-7)[74]超级明星等级图标(0-7)
	 *
	 * 版本 >= 0
	 */
	 private int flagType;

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String inReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(buyerUin);
		bs.pushInt(flagType);
		bs.pushString(inReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		buyerUin = bs.popUInt();
		flagType = bs.popInt();
		inReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xF2071801L;
	}


	/**
	 * 获取买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return buyerUin;
	}


	/**
	 * 设置买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.buyerUin = value;
	}


	/**
	 * 获取图标类型[1] 红钻 0-7级[2] 黄钻 0-7级[3] QQ飞行岛 0-7级[4] 地下城与勇士 0-7级[5] CF穿越火线 0-7级[6] QQ三国 0-7级[7] QQ炫舞 0-7级[8] 自由幻想 0-7级[9] QQ飞车 0-7级[10] 手机腾讯网 0-7级[11] 超级QQ 0-7级[12] 寻仙 0-7级[13] 问问 0-7级[14] 绿钻 0-7级[15] 搜搜网页搜索带等级标志位 0-3级[16] 蓝钻贵族等级标志位 0-7级[17] 黑钻 0-7级[18] 拍拍等级0-7[19] AVA等级控制位 0-4[20] AVA等级图标 0-7[21] 英雄岛等级 0-7[22] 公益图标 1-8[23] 烽火战国0～7[41] QQ封神记0-7[42] 三国英雄传0-7[43] 大明龙权0-7[44] 拍拍彩钻带等级标志位0-7[45]QQ幻想世界等级标志位0-7[46]QQ音速（R2beat）等级标志位0-7[47]QQ西游等级标志位0-7[48]七雄争霸等级标志位0-7[49]QQ宠物粉钻贵族等级标志位0-7[50]3366等级标志位0-7[51]安全达人图标等级标志位0-7[52]电脑管家图标等级标志位0-7[53] 英雄联盟等级图标(0-7)   [54] 英雄杀等级图标(0-7)   [55] 丝路英雄等级图标(0-7)   [56] 超级明星等级图标 (0-7)   [57] WOZ等级图标(0-7)   [58] QQ宝石等级图标(0-7)   [59] QQ宠物企鹅等级图标(0-7)   [60] 洛克王国图标(0-3)[61]体验计划等级图标(0-7)[62] 财付通-VIP等级图标(0-7)[63] QQ仙侠传等级图标(0-7)[64] QQTalk等级图标(0-7)[65] 生化战场等级图标(0-7)[66]楚河汉界等级图标(0-7)[67]《江湖笑》IM合作等级图标(0-7)[68]小白大作战等级图标标(0-6)[69]魔幻大陆等级图标(0-7)[70]微博等级图标标志位(0-7)[71]Q宠大乐斗等级图标标志位(0-7)[72]摩登城市等级图标标志位(0-7)[73]QQ九仙等级图标标志位(0-7)[74]超级明星等级图标(0-7)
	 * 
	 * 此字段的版本 >= 0
	 * @return flagType value 类型为:int
	 * 
	 */
	public int getFlagType()
	{
		return flagType;
	}


	/**
	 * 设置图标类型[1] 红钻 0-7级[2] 黄钻 0-7级[3] QQ飞行岛 0-7级[4] 地下城与勇士 0-7级[5] CF穿越火线 0-7级[6] QQ三国 0-7级[7] QQ炫舞 0-7级[8] 自由幻想 0-7级[9] QQ飞车 0-7级[10] 手机腾讯网 0-7级[11] 超级QQ 0-7级[12] 寻仙 0-7级[13] 问问 0-7级[14] 绿钻 0-7级[15] 搜搜网页搜索带等级标志位 0-3级[16] 蓝钻贵族等级标志位 0-7级[17] 黑钻 0-7级[18] 拍拍等级0-7[19] AVA等级控制位 0-4[20] AVA等级图标 0-7[21] 英雄岛等级 0-7[22] 公益图标 1-8[23] 烽火战国0～7[41] QQ封神记0-7[42] 三国英雄传0-7[43] 大明龙权0-7[44] 拍拍彩钻带等级标志位0-7[45]QQ幻想世界等级标志位0-7[46]QQ音速（R2beat）等级标志位0-7[47]QQ西游等级标志位0-7[48]七雄争霸等级标志位0-7[49]QQ宠物粉钻贵族等级标志位0-7[50]3366等级标志位0-7[51]安全达人图标等级标志位0-7[52]电脑管家图标等级标志位0-7[53] 英雄联盟等级图标(0-7)   [54] 英雄杀等级图标(0-7)   [55] 丝路英雄等级图标(0-7)   [56] 超级明星等级图标 (0-7)   [57] WOZ等级图标(0-7)   [58] QQ宝石等级图标(0-7)   [59] QQ宠物企鹅等级图标(0-7)   [60] 洛克王国图标(0-3)[61]体验计划等级图标(0-7)[62] 财付通-VIP等级图标(0-7)[63] QQ仙侠传等级图标(0-7)[64] QQTalk等级图标(0-7)[65] 生化战场等级图标(0-7)[66]楚河汉界等级图标(0-7)[67]《江湖笑》IM合作等级图标(0-7)[68]小白大作战等级图标标(0-6)[69]魔幻大陆等级图标(0-7)[70]微博等级图标标志位(0-7)[71]Q宠大乐斗等级图标标志位(0-7)[72]摩登城市等级图标标志位(0-7)[73]QQ九仙等级图标标志位(0-7)[74]超级明星等级图标(0-7)
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setFlagType(int value)
	{
		this.flagType = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserved value 类型为:String
	 * 
	 */
	public String getInReserved()
	{
		return inReserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserved(String value)
	{
		if (value != null) {
				this.inReserved = value;
		}else{
				this.inReserved = new String();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetFlagLevel4SingleReq)
				length += 4;  //计算字段buyerUin的长度 size_of(long)
				length += 4;  //计算字段flagType的长度 size_of(int)
				length += ByteStream.getObjectSize(inReserved);  //计算字段inReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
