 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.userinfo.UserLoginApiAo.java

package com.qq.qqbuy.thirdparty.idl.userinfo.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *移动端登录响应类
 *
 *@date 2013-07-12 11:13:41
 *
 *@since version:0
*/
public class  ApiLoginResp implements IServiceObject
{
	public long result;
	/**
	 * 用户uid
	 *
	 * 版本 >= 0
	 */
	 private long uid;

	/**
	 * session key
	 *
	 * 版本 >= 0
	 */
	 private String key = new String();

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errmsg = new String();

	/**
	 * 输出保留字
	 *
	 * 版本 >= 0
	 */
	 private String outReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushLong(uid);
		bs.pushString(key);
		bs.pushString(errmsg);
		bs.pushString(outReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		uid = bs.popLong();
		key = bs.popString();
		errmsg = bs.popString();
		outReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xA0A88801L;
	}


	/**
	 * 获取用户uid
	 * 
	 * 此字段的版本 >= 0
	 * @return uid value 类型为:long
	 * 
	 */
	public long getUid()
	{
		return uid;
	}


	/**
	 * 设置用户uid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUid(long value)
	{
		this.uid = value;
	}


	/**
	 * 获取session key
	 * 
	 * 此字段的版本 >= 0
	 * @return key value 类型为:String
	 * 
	 */
	public String getKey()
	{
		return key;
	}


	/**
	 * 设置session key
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setKey(String value)
	{
		this.key = value;
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errmsg value 类型为:String
	 * 
	 */
	public String getErrmsg()
	{
		return errmsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrmsg(String value)
	{
		this.errmsg = value;
	}


	/**
	 * 获取输出保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return outReserve;
	}


	/**
	 * 设置输出保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		this.outReserve = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ApiLoginResp)
				length += 17;  //计算字段uid的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(key);  //计算字段key的长度 size_of(String)
				length += ByteStream.getObjectSize(errmsg);  //计算字段errmsg的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserve);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
