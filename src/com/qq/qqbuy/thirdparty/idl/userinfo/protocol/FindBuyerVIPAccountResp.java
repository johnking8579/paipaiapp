 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.user.idl.BuyerVIPAcountInfoService.java

package com.qq.qqbuy.thirdparty.idl.userinfo.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2012-12-10 05:41:52
 *
 *@since version:0
*/
public class  FindBuyerVIPAccountResp implements IServiceObject
{
	public long result;
	/**
	 * 买家彩钻信息
	 *
	 * 版本 >= 0
	 */
	 private QgoBuyerVIPAccountInfo buyerVIPAccountInfo = new QgoBuyerVIPAccountInfo();

	/**
	 * 错误码, 0表示没有错误，其它表示相应的错误
	 *
	 * 版本 >= 0
	 */
	 private int errCode;

	/**
	 * 错误信息, errCode不为0时，本字段表示错误描述信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 备用字段,用于一些紧急临时解决方案
	 *
	 * 版本 >= 0
	 */
	 private String outReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(buyerVIPAccountInfo);
		bs.pushInt(errCode);
		bs.pushString(errMsg);
		bs.pushString(outReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		buyerVIPAccountInfo = (QgoBuyerVIPAccountInfo) bs.popObject(QgoBuyerVIPAccountInfo.class);
		errCode = bs.popInt();
		errMsg = bs.popString();
		outReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80068802L;
	}


	/**
	 * 获取买家彩钻信息
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerVIPAccountInfo value 类型为:QgoBuyerVIPAccountInfo
	 * 
	 */
	public QgoBuyerVIPAccountInfo getBuyerVIPAccountInfo()
	{
		return buyerVIPAccountInfo;
	}


	/**
	 * 设置买家彩钻信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:QgoBuyerVIPAccountInfo
	 * 
	 */
	public void setBuyerVIPAccountInfo(QgoBuyerVIPAccountInfo value)
	{
		if (value != null) {
				this.buyerVIPAccountInfo = value;
		}else{
				this.buyerVIPAccountInfo = new QgoBuyerVIPAccountInfo();
		}
	}


	/**
	 * 获取错误码, 0表示没有错误，其它表示相应的错误
	 * 
	 * 此字段的版本 >= 0
	 * @return errCode value 类型为:int
	 * 
	 */
	public int getErrCode()
	{
		return errCode;
	}


	/**
	 * 设置错误码, 0表示没有错误，其它表示相应的错误
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setErrCode(int value)
	{
		this.errCode = value;
	}


	/**
	 * 获取错误信息, errCode不为0时，本字段表示错误描述信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误信息, errCode不为0时，本字段表示错误描述信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	/**
	 * 获取备用字段,用于一些紧急临时解决方案
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserved value 类型为:String
	 * 
	 */
	public String getOutReserved()
	{
		return outReserved;
	}


	/**
	 * 设置备用字段,用于一些紧急临时解决方案
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserved(String value)
	{
		this.outReserved = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(FindBuyerVIPAccountResp)
				length += ByteStream.getObjectSize(buyerVIPAccountInfo);  //计算字段buyerVIPAccountInfo的长度 size_of(QgoBuyerVIPAccountInfo)
				length += 4;  //计算字段errCode的长度 size_of(int)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserved);  //计算字段outReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
