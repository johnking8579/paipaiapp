//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.user.idl.BuyerVIPAcountInfoService.java

package com.qq.qqbuy.thirdparty.idl.userinfo.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *买家彩钻信息
 *
 *@date 2012-12-10 05:41:52
 *
 *@since version:0
*/
public class QgoBuyerVIPAccountInfo implements ICanSerializeObject
{
	/**
	 * 买家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long dwBuyerUin;

	/**
	 * 账户状态,0:彩钻熄灭状态;1:彩钻点亮状态;2:彩钻未激活，即非彩钻用户态
	 *
	 * 版本 >= 0
	 */
	 private int dwState;

	/**
	 * 账户等级
	 *
	 * 版本 >= 0
	 */
	 private int dwLevel;

	/**
	 * 总成长值
	 *
	 * 版本 >= 0
	 */
	 private String dwTotalFee = new String();

	/**
	 * 月累计成长值
	 *
	 * 版本 >= 0
	 */
	 private String dwMonthFee = new String();

	/**
	 * 超额成长值
	 *
	 * 版本 >= 0
	 */
	 private String dwFeeExceed = new String();

	/**
	 * 最近购买时间
	 *
	 * 版本 >= 0
	 */
	 private String dwLastBuyTime = new String();

	/**
	 * 初始成长值
	 *
	 * 版本 >= 0
	 */
	 private String dwInitFee = new String();

	/**
	 * 初始买家信用值
	 *
	 * 版本 >= 0
	 */
	 private String dwInitEval = new String();

	/**
	 * 初始直充次数
	 *
	 * 版本 >= 0
	 */
	 private String dwInitChargeNum = new String();

	/**
	 * 成长值计算月份
	 *
	 * 版本 >= 0
	 */
	 private String dwSumMonth = new String();

	/**
	 * 用于记录IDL接口的版本
	 *
	 * 版本 >= 0
	 */
	 private int version = 1;

	/**
	 * 备用字段,用于一些紧急临时解决方案
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(dwBuyerUin);
		bs.pushInt(dwState);
		bs.pushInt(dwLevel);
		bs.pushString(dwTotalFee);
		bs.pushString(dwMonthFee);
		bs.pushString(dwFeeExceed);
		bs.pushString(dwLastBuyTime);
		bs.pushString(dwInitFee);
		bs.pushString(dwInitEval);
		bs.pushString(dwInitChargeNum);
		bs.pushString(dwSumMonth);
		bs.pushInt(version);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		dwBuyerUin = bs.popUInt();
		dwState = bs.popInt();
		dwLevel = bs.popInt();
		dwTotalFee = bs.popString();
		dwMonthFee = bs.popString();
		dwFeeExceed = bs.popString();
		dwLastBuyTime = bs.popString();
		dwInitFee = bs.popString();
		dwInitEval = bs.popString();
		dwInitChargeNum = bs.popString();
		dwSumMonth = bs.popString();
		version = bs.popInt();
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return dwBuyerUin value 类型为:long
	 * 
	 */
	public long getDwBuyerUin()
	{
		return dwBuyerUin;
	}


	/**
	 * 设置买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwBuyerUin(long value)
	{
		this.dwBuyerUin = value;
	}


	/**
	 * 获取账户状态,0:彩钻熄灭状态;1:彩钻点亮状态;2:彩钻未激活，即非彩钻用户态
	 * 
	 * 此字段的版本 >= 0
	 * @return dwState value 类型为:int
	 * 
	 */
	public int getDwState()
	{
		return dwState;
	}


	/**
	 * 设置账户状态,0:彩钻熄灭状态;1:彩钻点亮状态;2:彩钻未激活，即非彩钻用户态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setDwState(int value)
	{
		this.dwState = value;
	}


	/**
	 * 获取账户等级
	 * 
	 * 此字段的版本 >= 0
	 * @return dwLevel value 类型为:int
	 * 
	 */
	public int getDwLevel()
	{
		return dwLevel;
	}


	/**
	 * 设置账户等级
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setDwLevel(int value)
	{
		this.dwLevel = value;
	}


	/**
	 * 获取总成长值
	 * 
	 * 此字段的版本 >= 0
	 * @return dwTotalFee value 类型为:String
	 * 
	 */
	public String getDwTotalFee()
	{
		return dwTotalFee;
	}


	/**
	 * 设置总成长值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDwTotalFee(String value)
	{
		this.dwTotalFee = value;
	}


	/**
	 * 获取月累计成长值
	 * 
	 * 此字段的版本 >= 0
	 * @return dwMonthFee value 类型为:String
	 * 
	 */
	public String getDwMonthFee()
	{
		return dwMonthFee;
	}


	/**
	 * 设置月累计成长值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDwMonthFee(String value)
	{
		this.dwMonthFee = value;
	}


	/**
	 * 获取超额成长值
	 * 
	 * 此字段的版本 >= 0
	 * @return dwFeeExceed value 类型为:String
	 * 
	 */
	public String getDwFeeExceed()
	{
		return dwFeeExceed;
	}


	/**
	 * 设置超额成长值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDwFeeExceed(String value)
	{
		this.dwFeeExceed = value;
	}


	/**
	 * 获取最近购买时间
	 * 
	 * 此字段的版本 >= 0
	 * @return dwLastBuyTime value 类型为:String
	 * 
	 */
	public String getDwLastBuyTime()
	{
		return dwLastBuyTime;
	}


	/**
	 * 设置最近购买时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDwLastBuyTime(String value)
	{
		this.dwLastBuyTime = value;
	}


	/**
	 * 获取初始成长值
	 * 
	 * 此字段的版本 >= 0
	 * @return dwInitFee value 类型为:String
	 * 
	 */
	public String getDwInitFee()
	{
		return dwInitFee;
	}


	/**
	 * 设置初始成长值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDwInitFee(String value)
	{
		this.dwInitFee = value;
	}


	/**
	 * 获取初始买家信用值
	 * 
	 * 此字段的版本 >= 0
	 * @return dwInitEval value 类型为:String
	 * 
	 */
	public String getDwInitEval()
	{
		return dwInitEval;
	}


	/**
	 * 设置初始买家信用值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDwInitEval(String value)
	{
		this.dwInitEval = value;
	}


	/**
	 * 获取初始直充次数
	 * 
	 * 此字段的版本 >= 0
	 * @return dwInitChargeNum value 类型为:String
	 * 
	 */
	public String getDwInitChargeNum()
	{
		return dwInitChargeNum;
	}


	/**
	 * 设置初始直充次数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDwInitChargeNum(String value)
	{
		this.dwInitChargeNum = value;
	}


	/**
	 * 获取成长值计算月份
	 * 
	 * 此字段的版本 >= 0
	 * @return dwSumMonth value 类型为:String
	 * 
	 */
	public String getDwSumMonth()
	{
		return dwSumMonth;
	}


	/**
	 * 设置成长值计算月份
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDwSumMonth(String value)
	{
		this.dwSumMonth = value;
	}


	/**
	 * 获取用于记录IDL接口的版本
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置用于记录IDL接口的版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取备用字段,用于一些紧急临时解决方案
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置备用字段,用于一些紧急临时解决方案
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(QgoBuyerVIPAccountInfo)
				length += 4;  //计算字段dwBuyerUin的长度 size_of(long)
				length += 4;  //计算字段dwState的长度 size_of(int)
				length += 4;  //计算字段dwLevel的长度 size_of(int)
				length += ByteStream.getObjectSize(dwTotalFee);  //计算字段dwTotalFee的长度 size_of(String)
				length += ByteStream.getObjectSize(dwMonthFee);  //计算字段dwMonthFee的长度 size_of(String)
				length += ByteStream.getObjectSize(dwFeeExceed);  //计算字段dwFeeExceed的长度 size_of(String)
				length += ByteStream.getObjectSize(dwLastBuyTime);  //计算字段dwLastBuyTime的长度 size_of(String)
				length += ByteStream.getObjectSize(dwInitFee);  //计算字段dwInitFee的长度 size_of(String)
				length += ByteStream.getObjectSize(dwInitEval);  //计算字段dwInitEval的长度 size_of(String)
				length += ByteStream.getObjectSize(dwInitChargeNum);  //计算字段dwInitChargeNum的长度 size_of(String)
				length += ByteStream.getObjectSize(dwSumMonth);  //计算字段dwSumMonth的长度 size_of(String)
				length += 4;  //计算字段version的长度 size_of(int)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
