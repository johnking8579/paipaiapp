 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: bi.userlink.PaipaiUserLinkAO.java

package com.qq.qqbuy.thirdparty.idl.userinfo.protocol;


import java.util.HashMap;
import java.util.Map;

import com.paipai.netframework.kernal.NetMessage;
import com.paipai.util.io.ByteStream;

/**
 *get UserLink req
 *
 *@date 2015-04-22 10:17:29
 *
 *@since version:0
*/
public class  GetUserLinkInfoReq extends NetMessage
{
	/**
	 * 版本号, 必填, 默认值1
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 来源
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 场景ID, 必填, 默认0, 以后扩展
	 *
	 * 版本 >= 0
	 */
	 private long sceneId;

	/**
	 * UserID,必填,用户ID
	 *
	 * 版本 >= 0
	 */
	 private long uin;

	/**
	 * Req number of links
	 *
	 * 版本 >= 0
	 */
	 private long reqnum;

	/**
	 * req reserve
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> reserveIn = new HashMap<String,String>();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(version);
		bs.pushString(source);
		bs.pushLong(sceneId);
		bs.pushLong(uin);
		bs.pushUInt(reqnum);
		bs.pushObject(reserveIn);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		version = bs.popUInt();
		source = bs.popString();
		sceneId = bs.popLong();
		uin = bs.popLong();
		reqnum = bs.popUInt();
		reserveIn = (Map<String,String>)bs.popMap(String.class,String.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x86111801L;
	}


	/**
	 * 获取版本号, 必填, 默认值1
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号, 必填, 默认值1
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取来源
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取场景ID, 必填, 默认0, 以后扩展
	 * 
	 * 此字段的版本 >= 0
	 * @return sceneId value 类型为:long
	 * 
	 */
	public long getSceneId()
	{
		return sceneId;
	}


	/**
	 * 设置场景ID, 必填, 默认0, 以后扩展
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSceneId(long value)
	{
		this.sceneId = value;
	}


	/**
	 * 获取UserID,必填,用户ID
	 * 
	 * 此字段的版本 >= 0
	 * @return uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return uin;
	}


	/**
	 * 设置UserID,必填,用户ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.uin = value;
	}


	/**
	 * 获取Req number of links
	 * 
	 * 此字段的版本 >= 0
	 * @return reqnum value 类型为:long
	 * 
	 */
	public long getReqnum()
	{
		return reqnum;
	}


	/**
	 * 设置Req number of links
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setReqnum(long value)
	{
		this.reqnum = value;
	}


	/**
	 * 获取req reserve
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveIn value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getReserveIn()
	{
		return reserveIn;
	}


	/**
	 * 设置req reserve
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setReserveIn(Map<String,String> value)
	{
		if (value != null) {
				this.reserveIn = value;
		}else{
				this.reserveIn = new HashMap<String,String>();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetUserLinkInfoReq)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(source, null);  //计算字段source的长度 size_of(String)
				length += 17;  //计算字段sceneId的长度 size_of(uint64_t)
				length += 17;  //计算字段uin的长度 size_of(uint64_t)
				length += 4;  //计算字段reqnum的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(reserveIn, null);  //计算字段reserveIn的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetUserLinkInfoReq)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(source, encoding);  //计算字段source的长度 size_of(String)
				length += 17;  //计算字段sceneId的长度 size_of(uint64_t)
				length += 17;  //计算字段uin的长度 size_of(uint64_t)
				length += 4;  //计算字段reqnum的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(reserveIn, encoding);  //计算字段reserveIn的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
