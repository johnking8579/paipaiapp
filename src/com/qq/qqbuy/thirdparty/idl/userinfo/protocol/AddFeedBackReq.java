 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.user.idl.FeedBackService.java

package com.qq.qqbuy.thirdparty.idl.userinfo.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2013-03-04 10:31:20
 *
 *@since version:0
*/
public class  AddFeedBackReq implements IServiceObject
{
	/**
	 * 反馈内容
	 *
	 * 版本 >= 0
	 */
	 private FeedBack feedBack = new FeedBack();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String inReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(feedBack);
		bs.pushString(inReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		feedBack = (FeedBack) bs.popObject(FeedBack.class);
		inReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xF2071804L;
	}


	/**
	 * 获取反馈内容
	 * 
	 * 此字段的版本 >= 0
	 * @return feedBack value 类型为:FeedBack
	 * 
	 */
	public FeedBack getFeedBack()
	{
		return feedBack;
	}


	/**
	 * 设置反馈内容
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:FeedBack
	 * 
	 */
	public void setFeedBack(FeedBack value)
	{
		if (value != null) {
				this.feedBack = value;
		}else{
				this.feedBack = new FeedBack();
		}
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserved value 类型为:String
	 * 
	 */
	public String getInReserved()
	{
		return inReserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserved(String value)
	{
		this.inReserved = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(AddFeedBackReq)
				length += ByteStream.getObjectSize(feedBack);  //计算字段feedBack的长度 size_of(FeedBack)
				length += ByteStream.getObjectSize(inReserved);  //计算字段inReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
