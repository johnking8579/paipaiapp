 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.userinfo.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *用户资料
 *
 *@date 2012-05-04 07:36::41
 *
 *@since version:0
*/
public class UserInfo  implements ICanSerializeObject
{
	/**
	 * 版本控制的字段
	 *
	 * 版本 >= 0
	 */
	 private int version = 0; 

	/**
	 * 买家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long buyerUin;

	/**
	 * 用户昵称
	 *
	 * 版本 >= 0
	 */
	 private String nickName = new String();

	/**
	 * 性别 1==男  2==女 0==未填
	 *
	 * 版本 >= 0
	 */
	 private int sex;

	/**
	 * 真实姓名
	 *
	 * 版本 >= 0
	 */
	 private String realName = new String();

	/**
	 * 手机号码
	 *
	 * 版本 >= 0
	 */
	 private String mobilePhone = new String();

	/**
	 * 地址
	 *
	 * 版本 >= 0
	 */
	 private String adress = new String();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(version);
		bs.pushUInt(buyerUin);
		bs.pushString(nickName);
		bs.pushInt(sex);
		bs.pushString(realName);
		bs.pushString(mobilePhone);
		bs.pushString(adress);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		buyerUin = bs.popUInt();
		nickName = bs.popString();
		sex = bs.popInt();
		realName = bs.popString();
		mobilePhone = bs.popString();
		adress = bs.popString();
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return buyerUin;
	}


	/**
	 * 设置买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.buyerUin = value;
	}


	/**
	 * 获取用户昵称
	 * 
	 * 此字段的版本 >= 0
	 * @return nickName value 类型为:String
	 * 
	 */
	public String getNickName()
	{
		return nickName;
	}


	/**
	 * 设置用户昵称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setNickName(String value)
	{
		if (value != null) {
				this.nickName = value;
		}else{
				this.nickName = new String();
		}
	}


	/**
	 * 获取性别 1==男  2==女 0==未填
	 * 
	 * 此字段的版本 >= 0
	 * @return sex value 类型为:int
	 * 
	 */
	public int getSex()
	{
		return sex;
	}


	/**
	 * 设置性别 1==男  2==女 0==未填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setSex(int value)
	{
		this.sex = value;
	}


	/**
	 * 获取真实姓名
	 * 
	 * 此字段的版本 >= 0
	 * @return realName value 类型为:String
	 * 
	 */
	public String getRealName()
	{
		return realName;
	}


	/**
	 * 设置真实姓名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRealName(String value)
	{
		if (value != null) {
				this.realName = value;
		}else{
				this.realName = new String();
		}
	}


	/**
	 * 获取手机号码
	 * 
	 * 此字段的版本 >= 0
	 * @return mobilePhone value 类型为:String
	 * 
	 */
	public String getMobilePhone()
	{
		return mobilePhone;
	}


	/**
	 * 设置手机号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMobilePhone(String value)
	{
		if (value != null) {
				this.mobilePhone = value;
		}else{
				this.mobilePhone = new String();
		}
	}


	/**
	 * 获取地址
	 * 
	 * 此字段的版本 >= 0
	 * @return adress value 类型为:String
	 * 
	 */
	public String getAdress()
	{
		return adress;
	}


	/**
	 * 设置地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAdress(String value)
	{
		if (value != null) {
				this.adress = value;
		}else{
				this.adress = new String();
		}
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		if (value != null) {
				this.reserved = value;
		}else{
				this.reserved = new String();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(UserInfo)
				length += 4;  //计算字段version的长度 size_of(int)
				length += 4;  //计算字段buyerUin的长度 size_of(long)
				length += ByteStream.getObjectSize(nickName);  //计算字段nickName的长度 size_of(String)
				length += 4;  //计算字段sex的长度 size_of(int)
				length += ByteStream.getObjectSize(realName);  //计算字段realName的长度 size_of(String)
				length += ByteStream.getObjectSize(mobilePhone);  //计算字段mobilePhone的长度 size_of(String)
				length += ByteStream.getObjectSize(adress);  //计算字段adress的长度 size_of(String)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
