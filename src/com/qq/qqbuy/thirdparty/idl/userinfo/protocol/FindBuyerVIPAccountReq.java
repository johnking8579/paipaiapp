 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.user.idl.BuyerVIPAcountInfoService.java

package com.qq.qqbuy.thirdparty.idl.userinfo.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;

/**
 *
 *
 *@date 2012-12-10 05:41:52
 *
 *@since version:0
*/
public class  FindBuyerVIPAccountReq implements IServiceObject
{
	/**
	 * 买家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long dwBuyerUin;

	/**
	 * 备用字段,用于一些紧急临时解决方案
	 *
	 * 版本 >= 0
	 */
	 private String inReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(dwBuyerUin);
		bs.pushString(inReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		dwBuyerUin = bs.popUInt();
		inReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80061802L;
	}


	/**
	 * 获取买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return dwBuyerUin value 类型为:long
	 * 
	 */
	public long getDwBuyerUin()
	{
		return dwBuyerUin;
	}


	/**
	 * 设置买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwBuyerUin(long value)
	{
		this.dwBuyerUin = value;
	}


	/**
	 * 获取备用字段,用于一些紧急临时解决方案
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserved value 类型为:String
	 * 
	 */
	public String getInReserved()
	{
		return inReserved;
	}


	/**
	 * 设置备用字段,用于一些紧急临时解决方案
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserved(String value)
	{
		this.inReserved = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(FindBuyerVIPAccountReq)
				length += 4;  //计算字段dwBuyerUin的长度 size_of(long)
				length += ByteStream.getObjectSize(inReserved);  //计算字段inReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
