 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.userinfo.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

/**
 *
 *
 *@date 2012-05-04 07:36::41
 *
 *@since version:0
*/
public class  GetUserInfo0x480Req implements IServiceObject
{
	/**
	 * 买家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long buyerUin;

	/**
	 * a8(byte[4])+crtTime(byte[2])
	 *
	 * 版本 >= 0
	 */
	 private byte[] sk = new byte[6];

	/**
	 * 用户clientIp
	 *
	 * 版本 >= 0
	 */
	 private String clientIp = new String();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String inReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(buyerUin);
		bs.pushBytes(sk);
		bs.pushString(clientIp);
		bs.pushString(inReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		buyerUin = bs.popUInt();
		sk = bs.popBytes();
		clientIp = bs.popString();
		inReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xF2071802L;
	}


	/**
	 * 获取买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return buyerUin;
	}


	/**
	 * 设置买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.buyerUin = value;
	}


	/**
	 * 获取a8(byte[4])+crtTime(byte[2])
	 * 
	 * 此字段的版本 >= 0
	 * @return sk value 类型为:byte[]
	 * 
	 */
	public byte[] getSk()
	{
		return sk;
	}


	/**
	 * 设置a8(byte[4])+crtTime(byte[2])
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:byte[]
	 * 
	 */
	public void setSk(byte[] value)
	{
		if (value != null) {
				this.sk = value;
		}else{
				this.sk = new byte[6];
		}
	}


	/**
	 * 获取用户clientIp
	 * 
	 * 此字段的版本 >= 0
	 * @return clientIp value 类型为:String
	 * 
	 */
	public String getClientIp()
	{
		return clientIp;
	}


	/**
	 * 设置用户clientIp
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setClientIp(String value)
	{
		if (value != null) {
				this.clientIp = value;
		}else{
				this.clientIp = new String();
		}
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserved value 类型为:String
	 * 
	 */
	public String getInReserved()
	{
		return inReserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserved(String value)
	{
		if (value != null) {
				this.inReserved = value;
		}else{
				this.inReserved = new String();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetUserInfo0x480Req)
				length += 4;  //计算字段buyerUin的长度 size_of(long)
				length += ByteStream.getObjectSize(sk);  //计算字段sk的长度 size_of(byte[])
				length += ByteStream.getObjectSize(clientIp);  //计算字段clientIp的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserved);  //计算字段inReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
