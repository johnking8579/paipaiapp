 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import java.io.Serializable;

import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2011-05-13 09:28::30
 *
 *@since version:1
*/
public class  GetActWinnerInfoReq implements IServiceObject,Serializable
{
	/**
	 * 活动ID
	 *
	 * 版本 >= 0
	 */
	 private long actId;

	/**
	 * 规则类型
	 *
	 * 版本 >= 0
	 */
	 private long ruleType;

	/**
	 * 中奖信息显示页面
	 *
	 * 版本 >= 0
	 */
	 private long pageNo;

	/**
	 * 中奖信息每页记录
	 *
	 * 版本 >= 0
	 */
	 private long pageSize;


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(actId);
		bs.pushUInt(ruleType);
		bs.pushUInt(pageNo);
		bs.pushUInt(pageSize);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		actId = bs.popUInt();
		ruleType = bs.popUInt();
		pageNo = bs.popUInt();
		pageSize = bs.popUInt();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80001814L;
	}


	/**
	 * 获取活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @return actId value 类型为:long
	 * 
	 */
	public long getActId()
	{
		return actId;
	}


	/**
	 * 设置活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActId(long value)
	{
		this.actId = value;
	}


	/**
	 * 获取规则类型
	 * 
	 * 此字段的版本 >= 0
	 * @return ruleType value 类型为:long
	 * 
	 */
	public long getRuleType()
	{
		return ruleType;
	}


	/**
	 * 设置规则类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRuleType(long value)
	{
		this.ruleType = value;
	}


	/**
	 * 获取中奖信息显示页面
	 * 
	 * 此字段的版本 >= 0
	 * @return pageNo value 类型为:long
	 * 
	 */
	public long getPageNo()
	{
		return pageNo;
	}


	/**
	 * 设置中奖信息显示页面
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPageNo(long value)
	{
		this.pageNo = value;
	}


	/**
	 * 获取中奖信息每页记录
	 * 
	 * 此字段的版本 >= 0
	 * @return pageSize value 类型为:long
	 * 
	 */
	public long getPageSize()
	{
		return pageSize;
	}


	/**
	 * 设置中奖信息每页记录
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPageSize(long value)
	{
		this.pageSize = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetActWinnerInfoReq)
				length += 4;  //计算字段actId的长度 size_of(long)
				length += 4;  //计算字段ruleType的长度 size_of(long)
				length += 4;  //计算字段pageNo的长度 size_of(long)
				length += 4;  //计算字段pageSize的长度 size_of(long)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
