 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2011-07-28 02:46::46
 *
 *@since version:0
*/
public class  GetWinnerGainMethodInfoReq implements IServiceObject,Serializable
{
	/**
	 * 活动ID
	 *
	 * 版本 >= 0
	 */
	 private int actId;

	/**
	 * 买家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long buyerUin;

	/**
	 * 规则ID
	 *
	 * 版本 >= 0
	 */
	 private int ruleId;

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String inReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushInt(actId);
		bs.pushUInt(buyerUin);
		bs.pushInt(ruleId);
		bs.pushString(inReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		actId = bs.popInt();
		buyerUin = bs.popUInt();
		ruleId = bs.popInt();
		inReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80001821L;
	}


	/**
	 * 获取活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @return actId value 类型为:int
	 * 
	 */
	public int getActId()
	{
		return actId;
	}


	/**
	 * 设置活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setActId(int value)
	{
		this.actId = value;
	}


	/**
	 * 获取买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return buyerUin;
	}


	/**
	 * 设置买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.buyerUin = value;
	}


	/**
	 * 获取规则ID
	 * 
	 * 此字段的版本 >= 0
	 * @return ruleId value 类型为:int
	 * 
	 */
	public int getRuleId()
	{
		return ruleId;
	}


	/**
	 * 设置规则ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setRuleId(int value)
	{
		this.ruleId = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserved value 类型为:String
	 * 
	 */
	public String getInReserved()
	{
		return encode(inReserved);
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserved(String value)
	{
		if (value != null) {
				this.inReserved = value;
		}else{
				this.inReserved = new String();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetWinnerGainMethodInfoReq)
				length += 4;  //计算字段actId的长度 size_of(int)
				length += 4;  //计算字段buyerUin的长度 size_of(long)
				length += 4;  //计算字段ruleId的长度 size_of(int)
				length += ByteStream.getObjectSize(inReserved);  //计算字段inReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	private String encode(String str){
		try {
			return URLDecoder.decode(str, "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}
	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
