 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *规则简要信息
 *
 *@date 2011-05-13 09:28::30
 *
 *@since version:0
*/
public class ActivityBriefInfo  implements ICanSerializeObject,Serializable
{
	/**
	 * 活动ID
	 *
	 * 版本 >= 0
	 */
	 private long actId;

	/**
	 * 活动标题
	 *
	 * 版本 >= 0
	 */
	 private String actTitle = new String();

	/**
	 * 下单开始时间
	 *
	 * 版本 >= 0
	 */
	 private String orderStartTime = new String();

	/**
	 * 下单结束时间
	 *
	 * 版本 >= 0
	 */
	 private String orderEndTime = new String();

	/**
	 * 是否需要报名
	 *
	 * 版本 >= 0
	 */
	 private String needSignIn = new String();

	/**
	 * 买家是否已参与
	 *
	 * 版本 >= 0
	 */
	 private int isJoined;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(actId);
		bs.pushString(actTitle);
		bs.pushString(orderStartTime);
		bs.pushString(orderEndTime);
		bs.pushString(needSignIn);
		bs.pushInt(isJoined);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		actId = bs.popUInt();
		actTitle = bs.popString();
		orderStartTime = bs.popString();
		orderEndTime = bs.popString();
		needSignIn = bs.popString();
		isJoined = bs.popInt();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @return actId value 类型为:long
	 * 
	 */
	public long getActId()
	{
		return actId;
	}


	/**
	 * 设置活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActId(long value)
	{
		this.actId = value;
	}


	/**
	 * 获取活动标题
	 * 
	 * 此字段的版本 >= 0
	 * @return actTitle value 类型为:String
	 * 
	 */
	public String getActTitle()
	{
		return this.encode(actTitle);
	}


	/**
	 * 设置活动标题
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setActTitle(String value)
	{
		if (value != null) {
				this.actTitle = value;
		}else{
				this.actTitle = new String();
		}
	}


	/**
	 * 获取下单开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @return orderStartTime value 类型为:String
	 * 
	 */
	public String getOrderStartTime()
	{
		return this.encode(orderStartTime);
	}


	/**
	 * 设置下单开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOrderStartTime(String value)
	{
		if (value != null) {
				this.orderStartTime = value;
		}else{
				this.orderStartTime = new String();
		}
	}


	/**
	 * 获取下单结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return orderEndTime value 类型为:String
	 * 
	 */
	public String getOrderEndTime()
	{
		return this.encode(orderEndTime);
	}


	/**
	 * 设置下单结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOrderEndTime(String value)
	{
		if (value != null) {
				this.orderEndTime = value;
		}else{
				this.orderEndTime = new String();
		}
	}


	/**
	 * 获取是否需要报名
	 * 
	 * 此字段的版本 >= 0
	 * @return needSignIn value 类型为:String
	 * 
	 */
	public String getNeedSignIn()
	{
		return this.encode(needSignIn);
	}


	/**
	 * 设置是否需要报名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setNeedSignIn(String value)
	{
		if (value != null) {
				this.needSignIn = value;
		}else{
				this.needSignIn = new String();
		}
	}


	/**
	 * 获取买家是否已参与
	 * 
	 * 此字段的版本 >= 0
	 * @return isJoined value 类型为:int
	 * 
	 */
	public int getIsJoined()
	{
		return isJoined;
	}


	/**
	 * 设置买家是否已参与
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setIsJoined(int value)
	{
		this.isJoined = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ActivityBriefInfo)
				length += 4;  //计算字段actId的长度 size_of(long)
				length += ByteStream.getObjectSize(actTitle);  //计算字段actTitle的长度 size_of(String)
				length += ByteStream.getObjectSize(orderStartTime);  //计算字段orderStartTime的长度 size_of(String)
				length += ByteStream.getObjectSize(orderEndTime);  //计算字段orderEndTime的长度 size_of(String)
				length += ByteStream.getObjectSize(needSignIn);  //计算字段needSignIn的长度 size_of(String)
				length += 4;  //计算字段isJoined的长度 size_of(int)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	
	private String encode(String str){
		try {
			return URLDecoder.decode(str, "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
