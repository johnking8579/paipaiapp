 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *领奖信息
 *
 *@date 2011-07-28 02:46::46
 *
 *@since version:20110718
*/
public class GainInfo  implements ICanSerializeObject,Serializable
{
	/**
	 * 版本控制的字段
	 *
	 * 版本 >= 0
	 */
	 private int version = 20110718; 

	/**
	 * 奖品ID
	 *
	 * 版本 >= 0
	 */
	 private int awardId;

	/**
	 * 奖品名称
	 *
	 * 版本 >= 0
	 */
	 private String awardName = new String();

	/**
	 * 领奖方式
	 *
	 * 版本 >= 0
	 */
	 private int gainMethod;

	/**
	 * 领奖QQ
	 *
	 * 版本 >= 0
	 */
	 private long winnerUin;

	/**
	 * 奖品联系人姓名
	 *
	 * 版本 >= 0
	 */
	 private String winnerName = new String();

	/**
	 * 奖品联系人电话
	 *
	 * 版本 >= 0
	 */
	 private String winnerPhone = new String();

	/**
	 * 奖品邮寄地址
	 *
	 * 版本 >= 0
	 */
	 private String winnerAddr = new String();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(version);
		bs.pushInt(awardId);
		bs.pushString(awardName);
		bs.pushInt(gainMethod);
		bs.pushUInt(winnerUin);
		bs.pushString(winnerName);
		bs.pushString(winnerPhone);
		bs.pushString(winnerAddr);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		awardId = bs.popInt();
		awardName = bs.popString();
		gainMethod = bs.popInt();
		winnerUin = bs.popUInt();
		winnerName = bs.popString();
		winnerPhone = bs.popString();
		winnerAddr = bs.popString();
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取奖品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return awardId value 类型为:int
	 * 
	 */
	public int getAwardId()
	{
		return awardId;
	}


	/**
	 * 设置奖品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setAwardId(int value)
	{
		this.awardId = value;
	}


	/**
	 * 获取奖品名称
	 * 
	 * 此字段的版本 >= 0
	 * @return awardName value 类型为:String
	 * 
	 */
	public String getAwardName()
	{
		return encode(awardName);
	}


	/**
	 * 设置奖品名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAwardName(String value)
	{
		if (value != null) {
				this.awardName = value;
		}else{
				this.awardName = new String();
		}
	}


	/**
	 * 获取领奖方式
	 * 
	 * 此字段的版本 >= 0
	 * @return gainMethod value 类型为:int
	 * 
	 */
	public int getGainMethod()
	{
		return gainMethod;
	}


	/**
	 * 设置领奖方式
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setGainMethod(int value)
	{
		this.gainMethod = value;
	}


	/**
	 * 获取领奖QQ
	 * 
	 * 此字段的版本 >= 0
	 * @return winnerUin value 类型为:long
	 * 
	 */
	public long getWinnerUin()
	{
		return winnerUin;
	}


	/**
	 * 设置领奖QQ
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWinnerUin(long value)
	{
		this.winnerUin = value;
	}


	/**
	 * 获取奖品联系人姓名
	 * 
	 * 此字段的版本 >= 0
	 * @return winnerName value 类型为:String
	 * 
	 */
	public String getWinnerName()
	{
		return encode(winnerName);
	}


	/**
	 * 设置奖品联系人姓名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setWinnerName(String value)
	{
		if (value != null) {
				this.winnerName = value;
		}else{
				this.winnerName = new String();
		}
	}


	/**
	 * 获取奖品联系人电话
	 * 
	 * 此字段的版本 >= 0
	 * @return winnerPhone value 类型为:String
	 * 
	 */
	public String getWinnerPhone()
	{
		return encode(winnerPhone);
	}


	/**
	 * 设置奖品联系人电话
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setWinnerPhone(String value)
	{
		if (value != null) {
				this.winnerPhone = value;
		}else{
				this.winnerPhone = new String();
		}
	}


	/**
	 * 获取奖品邮寄地址
	 * 
	 * 此字段的版本 >= 0
	 * @return winnerAddr value 类型为:String
	 * 
	 */
	public String getWinnerAddr()
	{
		return encode(winnerAddr);
	}


	/**
	 * 设置奖品邮寄地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setWinnerAddr(String value)
	{
		if (value != null) {
				this.winnerAddr = value;
		}else{
				this.winnerAddr = new String();
		}
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return encode(reserved);
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		if (value != null) {
				this.reserved = value;
		}else{
				this.reserved = new String();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GainInfo)
				length += 4;  //计算字段version的长度 size_of(int)
				length += 4;  //计算字段awardId的长度 size_of(int)
				length += ByteStream.getObjectSize(awardName);  //计算字段awardName的长度 size_of(String)
				length += 4;  //计算字段gainMethod的长度 size_of(int)
				length += 4;  //计算字段winnerUin的长度 size_of(long)
				length += ByteStream.getObjectSize(winnerName);  //计算字段winnerName的长度 size_of(String)
				length += ByteStream.getObjectSize(winnerPhone);  //计算字段winnerPhone的长度 size_of(String)
				length += ByteStream.getObjectSize(winnerAddr);  //计算字段winnerAddr的长度 size_of(String)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	private String encode(String str){
		try {
			return URLDecoder.decode(str, "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
