 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *奖品规则V2
 *
 *@date 2011-09-07 08:32::22
 *
 *@since version:20110907
*/
public class ActRuleV2  implements ICanSerializeObject,Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7503321208364148863L;

	/**
	 * 版本控制的字段
	 *
	 * 版本 >= 0
	 */
	 private int version = 20110907; 

	/**
	 * 规则ID
	 *
	 * 版本 >= 0
	 */
	 private long ruleId;

	/**
	 * 活动ID
	 *
	 * 版本 >= 0
	 */
	 private long actId;

	/**
	 * 规则类型
	 *
	 * 版本 >= 0
	 */
	 private int ruleType;

	/**
	 * 订单累计方式
	 *
	 * 版本 >= 0
	 */
	 private int dealCountMethod;

	/**
	 * 最低需达金额
	 *
	 * 版本 >= 0
	 */
	 private long ruleStartFee;

	/**
	 * 每人最多可领取该奖品次数
	 *
	 * 版本 >= 0
	 */
	 private long mostChangeTimes;

	/**
	 * 获得领奖/抽奖次数
	 *
	 * 版本 >= 0
	 */
	 private long gainChanceNum;

	/**
	 * 用户类型
	 *
	 * 版本 >= 0
	 */
	 private int buyerType;

	/**
	 * 规则奖项信息
	 *
	 * 版本 >= 0
	 */
	 private List<RuleAwardConditionV2> ruleAwardConditionInfo = new ArrayList<RuleAwardConditionV2>();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(version);
		bs.pushUInt(ruleId);
		bs.pushUInt(actId);
		bs.pushInt(ruleType);
		bs.pushInt(dealCountMethod);
		bs.pushUInt(ruleStartFee);
		bs.pushUInt(mostChangeTimes);
		bs.pushUInt(gainChanceNum);
		bs.pushInt(buyerType);
		bs.pushObject(ruleAwardConditionInfo);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		ruleId = bs.popUInt();
		actId = bs.popUInt();
		ruleType = bs.popInt();
		dealCountMethod = bs.popInt();
		ruleStartFee = bs.popUInt();
		mostChangeTimes = bs.popUInt();
		gainChanceNum = bs.popUInt();
		buyerType = bs.popInt();
		ruleAwardConditionInfo = (List<RuleAwardConditionV2>)bs.popList(ArrayList.class,RuleAwardConditionV2.class);
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取规则ID
	 * 
	 * 此字段的版本 >= 0
	 * @return ruleId value 类型为:long
	 * 
	 */
	public long getRuleId()
	{
		return ruleId;
	}


	/**
	 * 设置规则ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRuleId(long value)
	{
		this.ruleId = value;
	}


	/**
	 * 获取活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @return actId value 类型为:long
	 * 
	 */
	public long getActId()
	{
		return actId;
	}


	/**
	 * 设置活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActId(long value)
	{
		this.actId = value;
	}


	/**
	 * 获取规则类型
	 * 
	 * 此字段的版本 >= 0
	 * @return ruleType value 类型为:int
	 * 
	 */
	public int getRuleType()
	{
		return ruleType;
	}


	/**
	 * 设置规则类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setRuleType(int value)
	{
		this.ruleType = value;
	}


	/**
	 * 获取订单累计方式
	 * 
	 * 此字段的版本 >= 0
	 * @return dealCountMethod value 类型为:int
	 * 
	 */
	public int getDealCountMethod()
	{
		return dealCountMethod;
	}


	/**
	 * 设置订单累计方式
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setDealCountMethod(int value)
	{
		this.dealCountMethod = value;
	}


	/**
	 * 获取最低需达金额
	 * 
	 * 此字段的版本 >= 0
	 * @return ruleStartFee value 类型为:long
	 * 
	 */
	public long getRuleStartFee()
	{
		return ruleStartFee;
	}


	/**
	 * 设置最低需达金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRuleStartFee(long value)
	{
		this.ruleStartFee = value;
	}


	/**
	 * 获取每人最多可领取该奖品次数
	 * 
	 * 此字段的版本 >= 0
	 * @return mostChangeTimes value 类型为:long
	 * 
	 */
	public long getMostChangeTimes()
	{
		return mostChangeTimes;
	}


	/**
	 * 设置每人最多可领取该奖品次数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMostChangeTimes(long value)
	{
		this.mostChangeTimes = value;
	}


	/**
	 * 获取获得领奖/抽奖次数
	 * 
	 * 此字段的版本 >= 0
	 * @return gainChanceNum value 类型为:long
	 * 
	 */
	public long getGainChanceNum()
	{
		return gainChanceNum;
	}


	/**
	 * 设置获得领奖/抽奖次数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setGainChanceNum(long value)
	{
		this.gainChanceNum = value;
	}


	/**
	 * 获取用户类型
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerType value 类型为:int
	 * 
	 */
	public int getBuyerType()
	{
		return buyerType;
	}


	/**
	 * 设置用户类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setBuyerType(int value)
	{
		this.buyerType = value;
	}


	/**
	 * 获取规则奖项信息
	 * 
	 * 此字段的版本 >= 0
	 * @return ruleAwardConditionInfo value 类型为:List<RuleAwardConditionV2>
	 * 
	 */
	public List<RuleAwardConditionV2> getRuleAwardConditionInfo()
	{
		return ruleAwardConditionInfo;
	}


	/**
	 * 设置规则奖项信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:List<RuleAwardConditionV2>
	 * 
	 */
	public void setRuleAwardConditionInfo(List<RuleAwardConditionV2> value)
	{
		if (value != null) {
				this.ruleAwardConditionInfo = value;
		}else{
				this.ruleAwardConditionInfo = new ArrayList<RuleAwardConditionV2>();
		}
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return this.decode(reserved);
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		if (value != null) {
				this.reserved = value;
		}else{
				this.reserved = new String();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ActRuleV2)
				length += 4;  //计算字段version的长度 size_of(int)
				length += 4;  //计算字段ruleId的长度 size_of(long)
				length += 4;  //计算字段actId的长度 size_of(long)
				length += 4;  //计算字段ruleType的长度 size_of(int)
				length += 4;  //计算字段dealCountMethod的长度 size_of(int)
				length += 4;  //计算字段ruleStartFee的长度 size_of(long)
				length += 4;  //计算字段mostChangeTimes的长度 size_of(long)
				length += 4;  //计算字段gainChanceNum的长度 size_of(long)
				length += 4;  //计算字段buyerType的长度 size_of(int)
				length += ByteStream.getObjectSize(ruleAwardConditionInfo);  //计算字段ruleAwardConditionInfo的长度 size_of(List)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
	private String decode(String str){
		try {
			return URLDecoder.decode(str, "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}
}
