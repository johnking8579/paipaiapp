 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2011-07-28 02:46::46
 *
 *@since version:0
*/
public class  GainAwardV2Resp implements IServiceObject,Serializable
{
	public long result;
	/**
	 * IDL接口的标准返回错误码
	 *
	 * 版本 >= 0
	 */
	 private int errCode;

	/**
	 * IDL接口的标准返回错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 领奖结果
	 *
	 * 版本 >= 0
	 */
	 private int gainResult;

	/**
	 * 结果描述
	 *
	 * 版本 >= 0
	 */
	 private String resultDesc = new String();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String outReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushInt(errCode);
		bs.pushString(errMsg);
		bs.pushInt(gainResult);
		bs.pushString(resultDesc);
		bs.pushString(outReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		errCode = bs.popInt();
		errMsg = bs.popString();
		gainResult = bs.popInt();
		resultDesc = bs.popString();
		outReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80008822L;
	}


	/**
	 * 获取IDL接口的标准返回错误码
	 * 
	 * 此字段的版本 >= 0
	 * @return errCode value 类型为:int
	 * 
	 */
	public int getErrCode()
	{
		return errCode;
	}


	/**
	 * 设置IDL接口的标准返回错误码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setErrCode(int value)
	{
		this.errCode = value;
	}


	/**
	 * 获取IDL接口的标准返回错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return encode(errMsg);
	}


	/**
	 * 设置IDL接口的标准返回错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		if (value != null) {
				this.errMsg = value;
		}else{
				this.errMsg = new String();
		}
	}


	/**
	 * 获取领奖结果
	 * 
	 * 此字段的版本 >= 0
	 * @return gainResult value 类型为:int
	 * 
	 */
	public int getGainResult()
	{
		return gainResult;
	}


	/**
	 * 设置领奖结果
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setGainResult(int value)
	{
		this.gainResult = value;
	}


	/**
	 * 获取结果描述
	 * 
	 * 此字段的版本 >= 0
	 * @return resultDesc value 类型为:String
	 * 
	 */
	public String getResultDesc()
	{
		return encode(resultDesc);
	}


	/**
	 * 设置结果描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setResultDesc(String value)
	{
		if (value != null) {
				this.resultDesc = value;
		}else{
				this.resultDesc = new String();
		}
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserved value 类型为:String
	 * 
	 */
	public String getOutReserved()
	{
		return encode(outReserved);
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserved(String value)
	{
		if (value != null) {
				this.outReserved = value;
		}else{
				this.outReserved = new String();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GainAwardV2Resp)
				length += 4;  //计算字段errCode的长度 size_of(int)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += 4;  //计算字段gainResult的长度 size_of(int)
				length += ByteStream.getObjectSize(resultDesc);  //计算字段resultDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserved);  //计算字段outReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	
	private String encode(String str){
		try {
			return URLDecoder.decode(str, "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
