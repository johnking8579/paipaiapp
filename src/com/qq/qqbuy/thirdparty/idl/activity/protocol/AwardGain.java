 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *奖项领取信息
 *
 *@date 2011-05-19 11:44::00
 *
 *@since version:0
*/
public class AwardGain  implements ICanSerializeObject,Serializable
{
	/**
	 * 中奖规则ID
	 *
	 * 版本 >= 0
	 */
	 private long ruleId;

	/**
	 * 奖品ID
	 *
	 * 版本 >= 0
	 */
	 private long awardId;

	/**
	 * 奖项等级
	 *
	 * 版本 >= 0
	 */
	 private int awardLevel;

	/**
	 * 每级奖项名额
	 *
	 * 版本 >= 0
	 */
	 private int awardLevelQuota;

	/**
	 * 已领取该奖项奖品人数
	 *
	 * 版本 >= 0
	 */
	 private int awardGainCount;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(ruleId);
		bs.pushUInt(awardId);
		bs.pushInt(awardLevel);
		bs.pushInt(awardLevelQuota);
		bs.pushInt(awardGainCount);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		ruleId = bs.popUInt();
		awardId = bs.popUInt();
		awardLevel = bs.popInt();
		awardLevelQuota = bs.popInt();
		awardGainCount = bs.popInt();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取中奖规则ID
	 * 
	 * 此字段的版本 >= 0
	 * @return ruleId value 类型为:long
	 * 
	 */
	public long getRuleId()
	{
		return ruleId;
	}


	/**
	 * 设置中奖规则ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRuleId(long value)
	{
		this.ruleId = value;
	}


	/**
	 * 获取奖品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return awardId value 类型为:long
	 * 
	 */
	public long getAwardId()
	{
		return awardId;
	}


	/**
	 * 设置奖品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAwardId(long value)
	{
		this.awardId = value;
	}


	/**
	 * 获取奖项等级
	 * 
	 * 此字段的版本 >= 0
	 * @return awardLevel value 类型为:int
	 * 
	 */
	public int getAwardLevel()
	{
		return awardLevel;
	}


	/**
	 * 设置奖项等级
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setAwardLevel(int value)
	{
		this.awardLevel = value;
	}


	/**
	 * 获取每级奖项名额
	 * 
	 * 此字段的版本 >= 0
	 * @return awardLevelQuota value 类型为:int
	 * 
	 */
	public int getAwardLevelQuota()
	{
		return awardLevelQuota;
	}


	/**
	 * 设置每级奖项名额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setAwardLevelQuota(int value)
	{
		this.awardLevelQuota = value;
	}


	/**
	 * 获取已领取该奖项奖品人数
	 * 
	 * 此字段的版本 >= 0
	 * @return awardGainCount value 类型为:int
	 * 
	 */
	public int getAwardGainCount()
	{
		return awardGainCount;
	}


	/**
	 * 设置已领取该奖项奖品人数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setAwardGainCount(int value)
	{
		this.awardGainCount = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(AwardGain)
				length += 4;  //计算字段ruleId的长度 size_of(long)
				length += 4;  //计算字段awardId的长度 size_of(long)
				length += 4;  //计算字段awardLevel的长度 size_of(int)
				length += 4;  //计算字段awardLevelQuota的长度 size_of(int)
				length += 4;  //计算字段awardGainCount的长度 size_of(int)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	
	
	private String encode(String str){
		try {
			return URLDecoder.decode(str, "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
