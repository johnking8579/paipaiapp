 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import java.io.Serializable;

import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2011-05-19 09:08::47
 *
 *@since version:1
*/
public class  GetActivityDynaInfoV2Req implements IServiceObject,Serializable
{
	/**
	 * 活动号
	 *
	 * 版本 >= 0
	 */
	 private long actId;


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(actId);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		actId = bs.popUInt();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80001819L;
	}


	/**
	 * 获取活动号
	 * 
	 * 此字段的版本 >= 0
	 * @return actId value 类型为:long
	 * 
	 */
	public long getActId()
	{
		return actId;
	}


	/**
	 * 设置活动号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActId(long value)
	{
		this.actId = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetActivityDynaInfoV2Req)
				length += 4;  //计算字段actId的长度 size_of(long)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
