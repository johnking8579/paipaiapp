 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *买家已参与活动信息
 *
 *@date 2011-05-20 03:06::35
 *
 *@since version:0
*/
public class BuyerJoinedActivity  implements ICanSerializeObject,Serializable
{
	/**
	 * 活动ID
	 *
	 * 版本 >= 0
	 */
	 private long actId;

	/**
	 * 活动标题
	 *
	 * 版本 >= 0
	 */
	 private String actTitle = new String();

	/**
	 * 下单开始时间
	 *
	 * 版本 >= 0
	 */
	 private String orderStartTime = new String();

	/**
	 * 下单结束时间
	 *
	 * 版本 >= 0
	 */
	 private String orderEndTime = new String();

	/**
	 * 领奖开始时间
	 *
	 * 版本 >= 0
	 */
	 private String changeStartTime = new String();

	/**
	 * 领奖结束时间
	 *
	 * 版本 >= 0
	 */
	 private String changeEndTime = new String();

	/**
	 * 抽奖时间
	 *
	 * 版本 >= 0
	 */
	 private String drawAwardTime = new String();

	/**
	 * 累计未领奖订单总金额
	 *
	 * 版本 >= 0
	 */
	 private long ungainTotalFee;

	/**
	 * 合规订单信息
	 *
	 * 版本 >= 0
	 */
	 private List<LegalDeal> legalDealInfo = new ArrayList<LegalDeal>();

	/**
	 * 买家中奖信息
	 *
	 * 版本 >= 0
	 */
	 private List<BuyerWinner> buyerWinnerInfo = new ArrayList<BuyerWinner>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(actId);
		bs.pushString(actTitle);
		bs.pushString(orderStartTime);
		bs.pushString(orderEndTime);
		bs.pushString(changeStartTime);
		bs.pushString(changeEndTime);
		bs.pushString(drawAwardTime);
		bs.pushUInt(ungainTotalFee);
		bs.pushObject(legalDealInfo);
		bs.pushObject(buyerWinnerInfo);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		actId = bs.popUInt();
		actTitle = bs.popString();
		orderStartTime = bs.popString();
		orderEndTime = bs.popString();
		changeStartTime = bs.popString();
		changeEndTime = bs.popString();
		drawAwardTime = bs.popString();
		ungainTotalFee = bs.popUInt();
		legalDealInfo = (List<LegalDeal>)bs.popList(ArrayList.class,LegalDeal.class);
		buyerWinnerInfo = (List<BuyerWinner>)bs.popList(ArrayList.class,BuyerWinner.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @return actId value 类型为:long
	 * 
	 */
	public long getActId()
	{
		return actId;
	}


	/**
	 * 设置活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActId(long value)
	{
		this.actId = value;
	}


	/**
	 * 获取活动标题
	 * 
	 * 此字段的版本 >= 0
	 * @return actTitle value 类型为:String
	 * 
	 */
	public String getActTitle()
	{
		return this.encode(actTitle);
	}


	/**
	 * 设置活动标题
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setActTitle(String value)
	{
		if (value != null) {
				this.actTitle = value;
		}else{
				this.actTitle = new String();
		}
	}


	/**
	 * 获取下单开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @return orderStartTime value 类型为:String
	 * 
	 */
	public String getOrderStartTime()
	{
		return this.encode(orderStartTime);
	}


	/**
	 * 设置下单开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOrderStartTime(String value)
	{
		if (value != null) {
				this.orderStartTime = value;
		}else{
				this.orderStartTime = new String();
		}
	}


	/**
	 * 获取下单结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return orderEndTime value 类型为:String
	 * 
	 */
	public String getOrderEndTime()
	{
		return this.encode(orderEndTime);
	}


	/**
	 * 设置下单结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOrderEndTime(String value)
	{
		if (value != null) {
				this.orderEndTime = value;
		}else{
				this.orderEndTime = new String();
		}
	}


	/**
	 * 获取领奖开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @return changeStartTime value 类型为:String
	 * 
	 */
	public String getChangeStartTime()
	{
		return this.encode(changeStartTime);
	}


	/**
	 * 设置领奖开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setChangeStartTime(String value)
	{
		if (value != null) {
				this.changeStartTime = value;
		}else{
				this.changeStartTime = new String();
		}
	}


	/**
	 * 获取领奖结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return changeEndTime value 类型为:String
	 * 
	 */
	public String getChangeEndTime()
	{
		return this.encode(changeEndTime);
	}


	/**
	 * 设置领奖结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setChangeEndTime(String value)
	{
		if (value != null) {
				this.changeEndTime = value;
		}else{
				this.changeEndTime = new String();
		}
	}


	/**
	 * 获取抽奖时间
	 * 
	 * 此字段的版本 >= 0
	 * @return drawAwardTime value 类型为:String
	 * 
	 */
	public String getDrawAwardTime()
	{
		return this.encode(drawAwardTime);
	}


	/**
	 * 设置抽奖时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDrawAwardTime(String value)
	{
		if (value != null) {
				this.drawAwardTime = value;
		}else{
				this.drawAwardTime = new String();
		}
	}


	/**
	 * 获取累计未领奖订单总金额
	 * 
	 * 此字段的版本 >= 0
	 * @return ungainTotalFee value 类型为:long
	 * 
	 */
	public long getUngainTotalFee()
	{
		return ungainTotalFee;
	}


	/**
	 * 设置累计未领奖订单总金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUngainTotalFee(long value)
	{
		this.ungainTotalFee = value;
	}


	/**
	 * 获取合规订单信息
	 * 
	 * 此字段的版本 >= 0
	 * @return legalDealInfo value 类型为:List<LegalDeal>
	 * 
	 */
	public List<LegalDeal> getLegalDealInfo()
	{
		return legalDealInfo;
	}


	/**
	 * 设置合规订单信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:List<LegalDeal>
	 * 
	 */
	public void setLegalDealInfo(List<LegalDeal> value)
	{
		if (value != null) {
				this.legalDealInfo = value;
		}else{
				this.legalDealInfo = new ArrayList<LegalDeal>();
		}
	}


	/**
	 * 获取买家中奖信息
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerWinnerInfo value 类型为:List<BuyerWinner>
	 * 
	 */
	public List<BuyerWinner> getBuyerWinnerInfo()
	{
		return buyerWinnerInfo;
	}


	/**
	 * 设置买家中奖信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:List<BuyerWinner>
	 * 
	 */
	public void setBuyerWinnerInfo(List<BuyerWinner> value)
	{
		if (value != null) {
				this.buyerWinnerInfo = value;
		}else{
				this.buyerWinnerInfo = new ArrayList<BuyerWinner>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(BuyerJoinedActivity)
				length += 4;  //计算字段actId的长度 size_of(long)
				length += ByteStream.getObjectSize(actTitle);  //计算字段actTitle的长度 size_of(String)
				length += ByteStream.getObjectSize(orderStartTime);  //计算字段orderStartTime的长度 size_of(String)
				length += ByteStream.getObjectSize(orderEndTime);  //计算字段orderEndTime的长度 size_of(String)
				length += ByteStream.getObjectSize(changeStartTime);  //计算字段changeStartTime的长度 size_of(String)
				length += ByteStream.getObjectSize(changeEndTime);  //计算字段changeEndTime的长度 size_of(String)
				length += ByteStream.getObjectSize(drawAwardTime);  //计算字段drawAwardTime的长度 size_of(String)
				length += 4;  //计算字段ungainTotalFee的长度 size_of(long)
				length += ByteStream.getObjectSize(legalDealInfo);  //计算字段legalDealInfo的长度 size_of(List)
				length += ByteStream.getObjectSize(buyerWinnerInfo);  //计算字段buyerWinnerInfo的长度 size_of(List)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	private String encode(String str){
		try {
			return URLDecoder.decode(str, "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
