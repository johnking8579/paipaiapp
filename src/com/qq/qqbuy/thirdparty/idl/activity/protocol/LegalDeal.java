 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import java.io.Serializable;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *合规订单信息
 *
 *@date 2011-05-14 11:27::35
 *
 *@since version:0
*/
public class LegalDeal  implements ICanSerializeObject,Serializable
{
	/**
	 * 奖号
	 *
	 * 版本 >= 0
	 */
	 private long lotteryNo;

	/**
	 * 订单金额
	 *
	 * 版本 >= 0
	 */
	 private long dealFee;

	/**
	 * 是否已领奖
	 *
	 * 版本 >= 0
	 */
	 private long gainStatus;

	/**
	 * 领奖操作号
	 *
	 * 版本 >= 0
	 */
	 private long gainSerialNo;

	/**
	 * 是否已抽奖
	 *
	 * 版本 >= 0
	 */
	 private long drawStatus;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(lotteryNo);
		bs.pushUInt(dealFee);
		bs.pushUInt(gainStatus);
		bs.pushUInt(gainSerialNo);
		bs.pushUInt(drawStatus);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		lotteryNo = bs.popUInt();
		dealFee = bs.popUInt();
		gainStatus = bs.popUInt();
		gainSerialNo = bs.popUInt();
		drawStatus = bs.popUInt();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取奖号
	 * 
	 * 此字段的版本 >= 0
	 * @return lotteryNo value 类型为:long
	 * 
	 */
	public long getLotteryNo()
	{
		return lotteryNo;
	}


	/**
	 * 设置奖号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLotteryNo(long value)
	{
		this.lotteryNo = value;
	}


	/**
	 * 获取订单金额
	 * 
	 * 此字段的版本 >= 0
	 * @return dealFee value 类型为:long
	 * 
	 */
	public long getDealFee()
	{
		return dealFee;
	}


	/**
	 * 设置订单金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealFee(long value)
	{
		this.dealFee = value;
	}


	/**
	 * 获取是否已领奖
	 * 
	 * 此字段的版本 >= 0
	 * @return gainStatus value 类型为:long
	 * 
	 */
	public long getGainStatus()
	{
		return gainStatus;
	}


	/**
	 * 设置是否已领奖
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setGainStatus(long value)
	{
		this.gainStatus = value;
	}


	/**
	 * 获取领奖操作号
	 * 
	 * 此字段的版本 >= 0
	 * @return gainSerialNo value 类型为:long
	 * 
	 */
	public long getGainSerialNo()
	{
		return gainSerialNo;
	}


	/**
	 * 设置领奖操作号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setGainSerialNo(long value)
	{
		this.gainSerialNo = value;
	}


	/**
	 * 获取是否已抽奖
	 * 
	 * 此字段的版本 >= 0
	 * @return drawStatus value 类型为:long
	 * 
	 */
	public long getDrawStatus()
	{
		return drawStatus;
	}


	/**
	 * 设置是否已抽奖
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDrawStatus(long value)
	{
		this.drawStatus = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(LegalDeal)
				length += 4;  //计算字段lotteryNo的长度 size_of(long)
				length += 4;  //计算字段dealFee的长度 size_of(long)
				length += 4;  //计算字段gainStatus的长度 size_of(long)
				length += 4;  //计算字段gainSerialNo的长度 size_of(long)
				length += 4;  //计算字段drawStatus的长度 size_of(long)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
