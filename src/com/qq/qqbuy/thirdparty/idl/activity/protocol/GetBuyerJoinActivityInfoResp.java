 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

/**
 *
 *
 *@date 2011-05-20 03:06::36
 *
 *@since version:1
*/
public class  GetBuyerJoinActivityInfoResp implements IServiceObject,Serializable
{
	public long result;
	/**
	 * IDL接口的标准返回错误码
	 *
	 * 版本 >= 0
	 */
	 private int errorCode;

	/**
	 * IDL接口的标准返回错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 买家已参与活动信息
	 *
	 * 版本 >= 0
	 */
	 private List<BuyerJoinedActivity> buyerJoinedActivityInfo = new ArrayList<BuyerJoinedActivity>();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushInt(errorCode);
		bs.pushString(errMsg);
		bs.pushObject(buyerJoinedActivityInfo);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		errorCode = bs.popInt();
		errMsg = bs.popString();
		buyerJoinedActivityInfo = (List<BuyerJoinedActivity>)bs.popList(ArrayList.class,BuyerJoinedActivity.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80008816L;
	}


	/**
	 * 获取IDL接口的标准返回错误码
	 * 
	 * 此字段的版本 >= 0
	 * @return errorCode value 类型为:int
	 * 
	 */
	public int getErrorCode()
	{
		return errorCode;
	}


	/**
	 * 设置IDL接口的标准返回错误码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setErrorCode(int value)
	{
		this.errorCode = value;
	}


	/**
	 * 获取IDL接口的标准返回错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置IDL接口的标准返回错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		if (value != null) {
				this.errMsg = value;
		}else{
				this.errMsg = new String();
		}
	}


	/**
	 * 获取买家已参与活动信息
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerJoinedActivityInfo value 类型为:List<BuyerJoinedActivity>
	 * 
	 */
	public List<BuyerJoinedActivity> getBuyerJoinedActivityInfo()
	{
		return buyerJoinedActivityInfo;
	}


	/**
	 * 设置买家已参与活动信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:List<BuyerJoinedActivity>
	 * 
	 */
	public void setBuyerJoinedActivityInfo(List<BuyerJoinedActivity> value)
	{
		if (value != null) {
				this.buyerJoinedActivityInfo = value;
		}else{
				this.buyerJoinedActivityInfo = new ArrayList<BuyerJoinedActivity>();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetBuyerJoinActivityInfoResp)
				length += 4;  //计算字段errorCode的长度 size_of(int)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(buyerJoinedActivityInfo);  //计算字段buyerJoinedActivityInfo的长度 size_of(List)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
