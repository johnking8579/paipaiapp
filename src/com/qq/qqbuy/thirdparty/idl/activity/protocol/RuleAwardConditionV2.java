 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *规则奖项信息V2
 *
 *@date 2011-09-07 08:32::22
 *
 *@since version:20110907
*/
public class RuleAwardConditionV2  implements ICanSerializeObject,Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4525378117045139585L;

	/**
	 * 版本控制的字段
	 *
	 * 版本 >= 0
	 */
	 private int version = 20110907; 

	/**
	 * 规则ID
	 *
	 * 版本 >= 0
	 */
	 private long ruleId;

	/**
	 * 奖品ID
	 *
	 * 版本 >= 0
	 */
	 private long awardId;

	/**
	 * 奖品名称
	 *
	 * 版本 >= 0
	 */
	 private String awardName = new String();

	/**
	 * 奖品等级
	 *
	 * 版本 >= 0
	 */
	 private String awardLevel = new String();

	/**
	 * 每级奖品名额
	 *
	 * 版本 >= 0
	 */
	 private String awardLevelQuota = new String();

	/**
	 * 奖品个数
	 *
	 * 版本 >= 0
	 */
	 private String winAwardNum = new String();

	/**
	 * 奖品单位
	 *
	 * 版本 >= 0
	 */
	 private String awardUnit = new String();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(version);
		bs.pushUInt(ruleId);
		bs.pushUInt(awardId);
		bs.pushString(awardName);
		bs.pushString(awardLevel);
		bs.pushString(awardLevelQuota);
		bs.pushString(winAwardNum);
		bs.pushString(awardUnit);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		ruleId = bs.popUInt();
		awardId = bs.popUInt();
		awardName = bs.popString();
		awardLevel = bs.popString();
		awardLevelQuota = bs.popString();
		winAwardNum = bs.popString();
		awardUnit = bs.popString();
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取规则ID
	 * 
	 * 此字段的版本 >= 0
	 * @return ruleId value 类型为:long
	 * 
	 */
	public long getRuleId()
	{
		return ruleId;
	}


	/**
	 * 设置规则ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRuleId(long value)
	{
		this.ruleId = value;
	}


	/**
	 * 获取奖品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return awardId value 类型为:long
	 * 
	 */
	public long getAwardId()
	{
		return awardId;
	}


	/**
	 * 设置奖品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAwardId(long value)
	{
		this.awardId = value;
	}


	/**
	 * 获取奖品名称
	 * 
	 * 此字段的版本 >= 0
	 * @return awardName value 类型为:String
	 * 
	 */
	public String getAwardName()
	{
		return this.decode(awardName);
	}


	/**
	 * 设置奖品名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAwardName(String value)
	{
		if (value != null) {
				this.awardName = value;
		}else{
				this.awardName = new String();
		}
	}


	/**
	 * 获取奖品等级
	 * 
	 * 此字段的版本 >= 0
	 * @return awardLevel value 类型为:String
	 * 
	 */
	public String getAwardLevel()
	{
		return this.decode(awardLevel);
	}


	/**
	 * 设置奖品等级
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAwardLevel(String value)
	{
		if (value != null) {
				this.awardLevel = value;
		}else{
				this.awardLevel = new String();
		}
	}


	/**
	 * 获取每级奖品名额
	 * 
	 * 此字段的版本 >= 0
	 * @return awardLevelQuota value 类型为:String
	 * 
	 */
	public String getAwardLevelQuota()
	{
		return this.decode(awardLevelQuota);
	}


	/**
	 * 设置每级奖品名额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAwardLevelQuota(String value)
	{
		if (value != null) {
				this.awardLevelQuota = value;
		}else{
				this.awardLevelQuota = new String();
		}
	}


	/**
	 * 获取奖品个数
	 * 
	 * 此字段的版本 >= 0
	 * @return winAwardNum value 类型为:String
	 * 
	 */
	public String getWinAwardNum()
	{
		return this.decode(winAwardNum);
	}


	/**
	 * 设置奖品个数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setWinAwardNum(String value)
	{
		if (value != null) {
				this.winAwardNum = value;
		}else{
				this.winAwardNum = new String();
		}
	}


	/**
	 * 获取奖品单位
	 * 
	 * 此字段的版本 >= 0
	 * @return awardUnit value 类型为:String
	 * 
	 */
	public String getAwardUnit()
	{
		return this.decode(awardUnit);
	}


	/**
	 * 设置奖品单位
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAwardUnit(String value)
	{
		if (value != null) {
				this.awardUnit = value;
		}else{
				this.awardUnit = new String();
		}
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return this.decode(reserved);
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		if (value != null) {
				this.reserved = value;
		}else{
				this.reserved = new String();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(RuleAwardConditionV2)
				length += 4;  //计算字段version的长度 size_of(int)
				length += 4;  //计算字段ruleId的长度 size_of(long)
				length += 4;  //计算字段awardId的长度 size_of(long)
				length += ByteStream.getObjectSize(awardName);  //计算字段awardName的长度 size_of(String)
				length += ByteStream.getObjectSize(awardLevel);  //计算字段awardLevel的长度 size_of(String)
				length += ByteStream.getObjectSize(awardLevelQuota);  //计算字段awardLevelQuota的长度 size_of(String)
				length += ByteStream.getObjectSize(winAwardNum);  //计算字段winAwardNum的长度 size_of(String)
				length += ByteStream.getObjectSize(awardUnit);  //计算字段awardUnit的长度 size_of(String)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
	
	private String decode(String str){
		try {
			return URLDecoder.decode(str, "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}
}
