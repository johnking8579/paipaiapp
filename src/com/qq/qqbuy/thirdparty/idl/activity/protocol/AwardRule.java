 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *奖品规则
 *
 *@date 2011-05-12 09:57::26
 *
 *@since version:0
*/
public class AwardRule  implements ICanSerializeObject,Serializable
{
    /**
     * 规则ID
     *
     * 版本 >= 0
     */
     private long ruleId;

    /**
     * 活动ID
     *
     * 版本 >= 0
     */
     private long actId;

    /**
     * 奖品获取方式
     *
     * 版本 >= 0
     */
     private int awardWinMethod;

    /**
     * 订单累计方式
     *
     * 版本 >= 0
     */
     private int dealCountMethod;

    /**
     * 最低需达金额
     *
     * 版本 >= 0
     */
     private long leastReachFee;

    /**
     * 奖品名称
     *
     * 版本 >= 0
     */
     private String awardName = new String();

    /**
     * 奖品等级
     *
     * 版本 >= 0
     */
     private int awardLevel;

    /**
     * 奖品总数量
     *
     * 版本 >= 0
     */
     private long awardTotalNum;

    /**
     * 每人最多可领取该奖品次数
     *
     * 版本 >= 0
     */
     private long mostChangeTimes;

    /**
     * 中奖后获得奖品个数
     *
     * 版本 >= 0
     */
     private long winAwardNum;

    /**
     * 获得领奖/抽奖次数
     *
     * 版本 >= 0
     */
     private long gainChanceNum;



    public int serialize(ByteStream bs) throws Exception
    {
        bs.pushUInt(getClassSize());
        bs.pushUInt(ruleId);
        bs.pushUInt(actId);
        bs.pushInt(awardWinMethod);
        bs.pushInt(dealCountMethod);
        bs.pushUInt(leastReachFee);
        bs.pushString(awardName);
        bs.pushInt(awardLevel);
        bs.pushUInt(awardTotalNum);
        bs.pushUInt(mostChangeTimes);
        bs.pushUInt(winAwardNum);
        bs.pushUInt(gainChanceNum);
        return bs.getWrittenLength();
    }
    
    public int unSerialize(ByteStream bs) throws Exception
    {
        long size = bs.popUInt();
        int startPosPop = bs.getReadLength();
        if (size == 0)
                return 0;
        ruleId = bs.popUInt();
        actId = bs.popUInt();
        awardWinMethod = bs.popInt();
        dealCountMethod = bs.popInt();
        leastReachFee = bs.popUInt();
        awardName = bs.popString();
        awardLevel = bs.popInt();
        awardTotalNum = bs.popUInt();
        mostChangeTimes = bs.popUInt();
        winAwardNum = bs.popUInt();
        gainChanceNum = bs.popUInt();

        /**********************为了支持多个版本的客户端************************/
        int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
        for(int i = 0;i< needPopBytes; i++)
                bs.popByte();
        /**********************为了支持多个版本的客户端************************/

        return bs.getReadLength();
    } 


    /**
     * 获取规则ID
     * 
     * 此字段的版本 >= 0
     * @return ruleId value 类型为:long
     * 
     */
    public long getRuleId()
    {
        return ruleId;
    }


    /**
     * 设置规则ID
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:long
     * 
     */
    public void setRuleId(long value)
    {
        this.ruleId = value;
    }


    /**
     * 获取活动ID
     * 
     * 此字段的版本 >= 0
     * @return actId value 类型为:long
     * 
     */
    public long getActId()
    {
        return actId;
    }


    /**
     * 设置活动ID
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:long
     * 
     */
    public void setActId(long value)
    {
        this.actId = value;
    }


    /**
     * 获取奖品获取方式
     * 
     * 此字段的版本 >= 0
     * @return awardWinMethod value 类型为:int
     * 
     */
    public int getAwardWinMethod()
    {
        return awardWinMethod;
    }


    /**
     * 设置奖品获取方式
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:int
     * 
     */
    public void setAwardWinMethod(int value)
    {
        this.awardWinMethod = value;
    }


    /**
     * 获取订单累计方式
     * 
     * 此字段的版本 >= 0
     * @return dealCountMethod value 类型为:int
     * 
     */
    public int getDealCountMethod()
    {
        return dealCountMethod;
    }


    /**
     * 设置订单累计方式
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:int
     * 
     */
    public void setDealCountMethod(int value)
    {
        this.dealCountMethod = value;
    }


    /**
     * 获取最低需达金额
     * 
     * 此字段的版本 >= 0
     * @return leastReachFee value 类型为:long
     * 
     */
    public long getLeastReachFee()
    {
        return leastReachFee;
    }


    /**
     * 设置最低需达金额
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:long
     * 
     */
    public void setLeastReachFee(long value)
    {
        this.leastReachFee = value;
    }


    /**
     * 获取奖品名称
     * 
     * 此字段的版本 >= 0
     * @return awardName value 类型为:String
     * 
     */
    public String getAwardName()
    {
        return this.encode(awardName);
    }


    /**
     * 设置奖品名称
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:String
     * 
     */
    public void setAwardName(String value)
    {
        if (value != null) {
                this.awardName = value;
        }else{
                this.awardName = new String();
        }
    }


    /**
     * 获取奖品等级
     * 
     * 此字段的版本 >= 0
     * @return awardLevel value 类型为:int
     * 
     */
    public int getAwardLevel()
    {
        return awardLevel;
    }


    /**
     * 设置奖品等级
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:int
     * 
     */
    public void setAwardLevel(int value)
    {
        this.awardLevel = value;
    }


    /**
     * 获取奖品总数量
     * 
     * 此字段的版本 >= 0
     * @return awardTotalNum value 类型为:long
     * 
     */
    public long getAwardTotalNum()
    {
        return awardTotalNum;
    }


    /**
     * 设置奖品总数量
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:long
     * 
     */
    public void setAwardTotalNum(long value)
    {
        this.awardTotalNum = value;
    }


    /**
     * 获取每人最多可领取该奖品次数
     * 
     * 此字段的版本 >= 0
     * @return mostChangeTimes value 类型为:long
     * 
     */
    public long getMostChangeTimes()
    {
        return mostChangeTimes;
    }


    /**
     * 设置每人最多可领取该奖品次数
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:long
     * 
     */
    public void setMostChangeTimes(long value)
    {
        this.mostChangeTimes = value;
    }


    /**
     * 获取中奖后获得奖品个数
     * 
     * 此字段的版本 >= 0
     * @return winAwardNum value 类型为:long
     * 
     */
    public long getWinAwardNum()
    {
        return winAwardNum;
    }


    /**
     * 设置中奖后获得奖品个数
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:long
     * 
     */
    public void setWinAwardNum(long value)
    {
        this.winAwardNum = value;
    }


    /**
     * 获取获得领奖/抽奖次数
     * 
     * 此字段的版本 >= 0
     * @return gainChanceNum value 类型为:long
     * 
     */
    public long getGainChanceNum()
    {
        return gainChanceNum;
    }


    /**
     * 设置获得领奖/抽奖次数
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:long
     * 
     */
    public void setGainChanceNum(long value)
    {
        this.gainChanceNum = value;
    }


    /**
     *   计算类长度
     *   用于告诉解包者，该类只放了这么长的数据
     *  
     */
    protected int getClassSize()
    {
        int length = getSize() - 4;
        try{

        }catch(Exception e){
                e.printStackTrace();
        }
        return length;
    }

    
    /**
     *   计算类长度
     *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
     *  
     */
    public int getSize()
    {
        int length = 4;
        try{
                length = 4;  //size_of(AwardRule)
                length += 4;  //计算字段ruleId的长度 size_of(long)
                length += 4;  //计算字段actId的长度 size_of(long)
                length += 4;  //计算字段awardWinMethod的长度 size_of(int)
                length += 4;  //计算字段dealCountMethod的长度 size_of(int)
                length += 4;  //计算字段leastReachFee的长度 size_of(long)
                length += ByteStream.getObjectSize(awardName);  //计算字段awardName的长度 size_of(String)
                length += 4;  //计算字段awardLevel的长度 size_of(int)
                length += 4;  //计算字段awardTotalNum的长度 size_of(long)
                length += 4;  //计算字段mostChangeTimes的长度 size_of(long)
                length += 4;  //计算字段winAwardNum的长度 size_of(long)
                length += 4;  //计算字段gainChanceNum的长度 size_of(long)
        }catch(Exception e){
                e.printStackTrace();
        }
        return length;
    }

	private String encode(String str){
		try {
			return URLDecoder.decode(str, "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

/**
 ********************以下信息是每个版本的字段********************
 */



    /**
     *下面是生成toString()方法
     此方法用于调试时打开*
     *如果要打开此方法，请加入commons-lang-2.4.jar
     *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
     *      import org.apache.commons.lang.builder.ToStringStyle;
     *
     */
    //@Override
    //public String toString() {
    //  return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
    //}
}
