 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.draw.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 *@date 2012-08-31 12:08::32
 *
 *@since version:0
*/
public class  FillBuyerJoinInfoReq implements IServiceObject
{
	/**
	 * 参与ID,非必填
	 *
	 * 版本 >= 0
	 */
	 private int joinId;

	/**
	 * 活动ID,必填
	 *
	 * 版本 >= 0
	 */
	 private int actId;

	/**
	 * 买家QQ号,必填
	 *
	 * 版本 >= 0
	 */
	 private long buyerUin;

	/**
	 * 补充信息
	 *
	 * 版本 >= 0
	 */
	 private List<ExtendInfo> extendInfos = new ArrayList<ExtendInfo>();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String inReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushInt(joinId);
		bs.pushInt(actId);
		bs.pushUInt(buyerUin);
		bs.pushObject(extendInfos);
		bs.pushString(inReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		joinId = bs.popInt();
		actId = bs.popInt();
		buyerUin = bs.popUInt();
		extendInfos = (List<ExtendInfo>)bs.popList(ArrayList.class,ExtendInfo.class);
		inReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x81011802L;
	}


	/**
	 * 获取参与ID,非必填
	 * 
	 * 此字段的版本 >= 0
	 * @return joinId value 类型为:int
	 * 
	 */
	public int getJoinId()
	{
		return joinId;
	}


	/**
	 * 设置参与ID,非必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setJoinId(int value)
	{
		this.joinId = value;
	}


	/**
	 * 获取活动ID,必填
	 * 
	 * 此字段的版本 >= 0
	 * @return actId value 类型为:int
	 * 
	 */
	public int getActId()
	{
		return actId;
	}


	/**
	 * 设置活动ID,必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setActId(int value)
	{
		this.actId = value;
	}


	/**
	 * 获取买家QQ号,必填
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return buyerUin;
	}


	/**
	 * 设置买家QQ号,必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.buyerUin = value;
	}


	/**
	 * 获取补充信息
	 * 
	 * 此字段的版本 >= 0
	 * @return extendInfos value 类型为:List<ExtendInfo>
	 * 
	 */
	public List<ExtendInfo> getExtendInfos()
	{
		return extendInfos;
	}


	/**
	 * 设置补充信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:List<ExtendInfo>
	 * 
	 */
	public void setExtendInfos(List<ExtendInfo> value)
	{
		if (value != null) {
				this.extendInfos = value;
		}else{
				this.extendInfos = new ArrayList<ExtendInfo>();
		}
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserved value 类型为:String
	 * 
	 */
	public String getInReserved()
	{
		return inReserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserved(String value)
	{
		this.inReserved = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(FillBuyerJoinInfoReq)
				length += 4;  //计算字段joinId的长度 size_of(int)
				length += 4;  //计算字段actId的长度 size_of(int)
				length += 4;  //计算字段buyerUin的长度 size_of(long)
				length += ByteStream.getObjectSize(extendInfos);  //计算字段extendInfos的长度 size_of(List)
				length += ByteStream.getObjectSize(inReserved);  //计算字段inReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
