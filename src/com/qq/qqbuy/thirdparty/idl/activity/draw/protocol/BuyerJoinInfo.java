 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.draw.protocol;


import java.util.Vector;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *参与信息
 *
 *@date 2012-09-04 03:18::50
 *
 *@since version:0
*/
public class BuyerJoinInfo  implements ICanSerializeObject
{
	/**
	 * 版本控制的字段
	 *
	 * 版本 >= 0
	 */
	 private int version = 20120904; 

	/**
	 * 参与号
	 *
	 * 版本 >= 20120904
	 */
	 private int joinId;

	/**
	 * 买家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long buyerUin;

	/**
	 * 参与时间：YYYY-MM-DD HH:MM:SS
	 *
	 * 版本 >= 0
	 */
	 private String joinTime = new String();

	/**
	 * 姓名
	 *
	 * 版本 >= 0
	 */
	 private String name = new String();

	/**
	 * 手机
	 *
	 * 版本 >= 0
	 */
	 private String mobile = new String();

	/**
	 * 地址,可填QQ号
	 *
	 * 版本 >= 0
	 */
	 private String addr = new String();

	/**
	 * 奖品
	 *
	 * 版本 >= 0
	 */
	 private Vector<QgoAward> award = new Vector<QgoAward>();

	/**
	 * 奖品扩展描述
	 *
	 * 版本 >= 0
	 */
	 private String awardDetailInfo = new String();

	/**
	 * 中奖等级
	 *
	 * 版本 >= 20120903
	 */
	 private int level;

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(version);
		if(  this.version >= 20120904 ){
				bs.pushInt(joinId);
		}
		bs.pushUInt(buyerUin);
		bs.pushString(joinTime);
		bs.pushString(name);
		bs.pushString(mobile);
		bs.pushString(addr);
		bs.pushObject(award);
		bs.pushString(awardDetailInfo);
		if(  this.version >= 20120903 ){
				bs.pushInt(level);
		}
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		if(  this.version >= 20120904 ){
				joinId = bs.popInt();
		}
		buyerUin = bs.popUInt();
		joinTime = bs.popString();
		name = bs.popString();
		mobile = bs.popString();
		addr = bs.popString();
		award = (Vector<QgoAward>)bs.popVector(QgoAward.class);
		awardDetailInfo = bs.popString();
		if(  this.version >= 20120903 ){
				level = bs.popInt();
		}
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取参与号
	 * 
	 * 此字段的版本 >= 20120904
	 * @return joinId value 类型为:int
	 * 
	 */
	public int getJoinId()
	{
		return joinId;
	}


	/**
	 * 设置参与号
	 * 
	 * 此字段的版本 >= 20120904
	 * @param  value 类型为:int
	 * 
	 */
	public void setJoinId(int value)
	{
		this.joinId = value;
	}


	/**
	 * 获取买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return buyerUin;
	}


	/**
	 * 设置买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.buyerUin = value;
	}


	/**
	 * 获取参与时间：YYYY-MM-DD HH:MM:SS
	 * 
	 * 此字段的版本 >= 0
	 * @return joinTime value 类型为:String
	 * 
	 */
	public String getJoinTime()
	{
		return joinTime;
	}


	/**
	 * 设置参与时间：YYYY-MM-DD HH:MM:SS
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setJoinTime(String value)
	{
		this.joinTime = value;
	}


	/**
	 * 获取姓名
	 * 
	 * 此字段的版本 >= 0
	 * @return name value 类型为:String
	 * 
	 */
	public String getName()
	{
		return name;
	}


	/**
	 * 设置姓名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setName(String value)
	{
		this.name = value;
	}


	/**
	 * 获取手机
	 * 
	 * 此字段的版本 >= 0
	 * @return mobile value 类型为:String
	 * 
	 */
	public String getMobile()
	{
		return mobile;
	}


	/**
	 * 设置手机
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMobile(String value)
	{
		this.mobile = value;
	}


	/**
	 * 获取地址,可填QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return addr value 类型为:String
	 * 
	 */
	public String getAddr()
	{
		return addr;
	}


	/**
	 * 设置地址,可填QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAddr(String value)
	{
		this.addr = value;
	}


	/**
	 * 获取奖品
	 * 
	 * 此字段的版本 >= 0
	 * @return award value 类型为:Vector<QgoAward>
	 * 
	 */
	public Vector<QgoAward> getAward()
	{
		return award;
	}


	/**
	 * 设置奖品
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<QgoAward>
	 * 
	 */
	public void setAward(Vector<QgoAward> value)
	{
		if (value != null) {
				this.award = value;
		}else{
				this.award = new Vector<QgoAward>();
		}
	}


	/**
	 * 获取奖品扩展描述
	 * 
	 * 此字段的版本 >= 0
	 * @return awardDetailInfo value 类型为:String
	 * 
	 */
	public String getAwardDetailInfo()
	{
		return awardDetailInfo;
	}


	/**
	 * 设置奖品扩展描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAwardDetailInfo(String value)
	{
		this.awardDetailInfo = value;
	}


	/**
	 * 获取中奖等级
	 * 
	 * 此字段的版本 >= 20120903
	 * @return level value 类型为:int
	 * 
	 */
	public int getLevel()
	{
		return level;
	}


	/**
	 * 设置中奖等级
	 * 
	 * 此字段的版本 >= 20120903
	 * @param  value 类型为:int
	 * 
	 */
	public void setLevel(int value)
	{
		this.level = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(BuyerJoinInfo)
				length += 4;  //计算字段version的长度 size_of(int)
				if(  this.version >= 20120904 ){
						length += 4;  //计算字段joinId的长度 size_of(int)
				}
				length += 4;  //计算字段buyerUin的长度 size_of(long)
				length += ByteStream.getObjectSize(joinTime);  //计算字段joinTime的长度 size_of(String)
				length += ByteStream.getObjectSize(name);  //计算字段name的长度 size_of(String)
				length += ByteStream.getObjectSize(mobile);  //计算字段mobile的长度 size_of(String)
				length += ByteStream.getObjectSize(addr);  //计算字段addr的长度 size_of(String)
				length += ByteStream.getObjectSize(award);  //计算字段award的长度 size_of(Vector)
				length += ByteStream.getObjectSize(awardDetailInfo);  //计算字段awardDetailInfo的长度 size_of(String)
				if(  this.version >= 20120903 ){
						length += 4;  //计算字段level的长度 size_of(int)
				}
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	@Override
	public String toString() {
		return "BuyerJoinInfo [addr=" + addr + ", award=" + award
				+ ", awardDetailInfo=" + awardDetailInfo + ", buyerUin="
				+ buyerUin + ", joinId=" + joinId + ", joinTime=" + joinTime
				+ ", level=" + level + ", mobile=" + mobile + ", name=" + name
				+ ", reserved=" + reserved + ", version=" + version + "]";
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本20120904所包含的字段*******
 *	int version;///<版本控制的字段
 *	int joinId;///<参与号
 *	long buyerUin;///<买家QQ号
 *	String joinTime;///<参与时间：YYYY-MM-DD HH:MM:SS
 *	String name;///<姓名
 *	String mobile;///<手机
 *	String addr;///<地址,可填QQ号
 *	Vector<QgoAward> award;///<奖品
 *	String awardDetailInfo;///<奖品扩展描述
 *	int level;///<中奖等级
 *	String reserved;///<保留字段
 *****以上是版本20120904所包含的字段*******
 *
 *****以下是版本20120903所包含的字段*******
 *	int version;///<版本控制的字段
 *	long buyerUin;///<买家QQ号
 *	String joinTime;///<参与时间：YYYY-MM-DD HH:MM:SS
 *	String name;///<姓名
 *	String mobile;///<手机
 *	String addr;///<地址,可填QQ号
 *	Vector<QgoAward> award;///<奖品
 *	String awardDetailInfo;///<奖品扩展描述
 *	int level;///<中奖等级
 *	String reserved;///<保留字段
 *****以上是版本20120903所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */

}
