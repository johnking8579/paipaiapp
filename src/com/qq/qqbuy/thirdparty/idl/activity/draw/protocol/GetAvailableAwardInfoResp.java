 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.activity.idl.QgoActivityService.java

package com.qq.qqbuy.thirdparty.idl.activity.draw.protocol;


import java.util.Vector;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

/**
 *
 *
 *@date 2013-05-16 07:31:58
 *
 *@since version:0
*/
public class  GetAvailableAwardInfoResp implements IServiceObject
{
	public long result;
	/**
	 * 可用奖品信息
	 *
	 * 版本 >= 0
	 */
	 private Vector<QgoAward> award = new Vector<QgoAward>();

	/**
	 * 总交易订单金额
	 *
	 * 版本 >= 0
	 */
	 private long totalDealFee;

	/**
	 * 错误码
	 *
	 * 版本 >= 0
	 */
	 private int errCode;

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String outReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(award);
		bs.pushUInt(totalDealFee);
		bs.pushInt(errCode);
		bs.pushString(errMsg);
		bs.pushString(outReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		award = (Vector<QgoAward>)bs.popVector(QgoAward.class);
		totalDealFee = bs.popUInt();
		errCode = bs.popInt();
		errMsg = bs.popString();
		outReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x81018804L;
	}


	/**
	 * 获取可用奖品信息
	 * 
	 * 此字段的版本 >= 0
	 * @return award value 类型为:Vector<QgoAward>
	 * 
	 */
	public Vector<QgoAward> getAward()
	{
		return award;
	}


	/**
	 * 设置可用奖品信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<QgoAward>
	 * 
	 */
	public void setAward(Vector<QgoAward> value)
	{
		if (value != null) {
				this.award = value;
		}else{
				this.award = new Vector<QgoAward>();
		}
	}


	/**
	 * 获取总交易订单金额
	 * 
	 * 此字段的版本 >= 0
	 * @return totalDealFee value 类型为:long
	 * 
	 */
	public long getTotalDealFee()
	{
		return totalDealFee;
	}


	/**
	 * 设置总交易订单金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalDealFee(long value)
	{
		this.totalDealFee = value;
	}


	/**
	 * 获取错误码
	 * 
	 * 此字段的版本 >= 0
	 * @return errCode value 类型为:int
	 * 
	 */
	public int getErrCode()
	{
		return errCode;
	}


	/**
	 * 设置错误码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setErrCode(int value)
	{
		this.errCode = value;
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserved value 类型为:String
	 * 
	 */
	public String getOutReserved()
	{
		return outReserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserved(String value)
	{
		this.outReserved = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetAvailableAwardInfoResp)
				length += ByteStream.getObjectSize(award);  //计算字段award的长度 size_of(Vector)
				length += 4;  //计算字段totalDealFee的长度 size_of(long)
				length += 4;  //计算字段errCode的长度 size_of(int)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserved);  //计算字段outReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
