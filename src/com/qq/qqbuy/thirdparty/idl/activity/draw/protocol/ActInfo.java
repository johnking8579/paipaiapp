//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.activity.idl.GetActInfoResp.java

package com.qq.qqbuy.thirdparty.idl.activity.draw.protocol;


import java.io.Serializable;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *活动信息
 *
 *@date 2013-05-17 10:12:57
 *
 *@since version:0
*/
public class ActInfo  implements ICanSerializeObject,Serializable
{
	/**
	 * 版本控制的字段
	 *
	 * 版本 >= 0
	 */
	 private int version = 0;

	/**
	 * 活动ID
	 *
	 * 版本 >= 0
	 */
	 private int actId;

	/**
	 * 开始时间
	 *
	 * 版本 >= 0
	 */
	 private long actBeginTime;

	/**
	 * 结束时间
	 *
	 * 版本 >= 0
	 */
	 private long actEndTime;

	/**
	 * 活动类型
	 *
	 * 版本 >= 0
	 */
	 private int actType;

	/**
	 * 参与规则,JSON格式
	 *
	 * 版本 >= 0
	 */
	 private String actRule = new String();

	/**
	 * 描述
	 *
	 * 版本 >= 0
	 */
	 private String comment = new String();

	/**
	 * 活动主图
	 *
	 * 版本 >= 0
	 */
	 private String actBanner = new String();

	/**
	 * 活动名称
	 *
	 * 版本 >= 0
	 */
	 private String actTitle = new String();

	/**
	 * 规则描述
	 *
	 * 版本 >= 0
	 */
	 private String ruleDesc = new String();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(version);
		bs.pushInt(actId);
		bs.pushUInt(actBeginTime);
		bs.pushUInt(actEndTime);
		bs.pushInt(actType);
		bs.pushString(actRule);
		bs.pushString(comment);
		bs.pushString(actBanner);
		bs.pushString(actTitle);
		bs.pushString(ruleDesc);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		actId = bs.popInt();
		actBeginTime = bs.popUInt();
		actEndTime = bs.popUInt();
		actType = bs.popInt();
		actRule = bs.popString();
		comment = bs.popString();
		actBanner = bs.popString();
		actTitle = bs.popString();
		ruleDesc = bs.popString();
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @return actId value 类型为:int
	 * 
	 */
	public int getActId()
	{
		return actId;
	}


	/**
	 * 设置活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setActId(int value)
	{
		this.actId = value;
	}


	/**
	 * 获取开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @return actBeginTime value 类型为:long
	 * 
	 */
	public long getActBeginTime()
	{
		return actBeginTime;
	}


	/**
	 * 设置开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActBeginTime(long value)
	{
		this.actBeginTime = value;
	}


	/**
	 * 获取结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return actEndTime value 类型为:long
	 * 
	 */
	public long getActEndTime()
	{
		return actEndTime;
	}


	/**
	 * 设置结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActEndTime(long value)
	{
		this.actEndTime = value;
	}


	/**
	 * 获取活动类型
	 * 
	 * 此字段的版本 >= 0
	 * @return actType value 类型为:int
	 * 
	 */
	public int getActType()
	{
		return actType;
	}


	/**
	 * 设置活动类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setActType(int value)
	{
		this.actType = value;
	}


	/**
	 * 获取参与规则,JSON格式
	 * 
	 * 此字段的版本 >= 0
	 * @return actRule value 类型为:String
	 * 
	 */
	public String getActRule()
	{
		return actRule;
	}


	/**
	 * 设置参与规则,JSON格式
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setActRule(String value)
	{
		this.actRule = value;
	}


	/**
	 * 获取描述
	 * 
	 * 此字段的版本 >= 0
	 * @return comment value 类型为:String
	 * 
	 */
	public String getComment()
	{
		return comment;
	}


	/**
	 * 设置描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setComment(String value)
	{
		this.comment = value;
	}


	/**
	 * 获取活动主图
	 * 
	 * 此字段的版本 >= 0
	 * @return actBanner value 类型为:String
	 * 
	 */
	public String getActBanner()
	{
		return actBanner;
	}


	/**
	 * 设置活动主图
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setActBanner(String value)
	{
		this.actBanner = value;
	}


	/**
	 * 获取活动名称
	 * 
	 * 此字段的版本 >= 0
	 * @return actTitle value 类型为:String
	 * 
	 */
	public String getActTitle()
	{
		return actTitle;
	}


	/**
	 * 设置活动名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setActTitle(String value)
	{
		this.actTitle = value;
	}


	/**
	 * 获取规则描述
	 * 
	 * 此字段的版本 >= 0
	 * @return ruleDesc value 类型为:String
	 * 
	 */
	public String getRuleDesc()
	{
		return ruleDesc;
	}


	/**
	 * 设置规则描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRuleDesc(String value)
	{
		this.ruleDesc = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ActInfo)
				length += 4;  //计算字段version的长度 size_of(int)
				length += 4;  //计算字段actId的长度 size_of(int)
				length += 4;  //计算字段actBeginTime的长度 size_of(long)
				length += 4;  //计算字段actEndTime的长度 size_of(long)
				length += 4;  //计算字段actType的长度 size_of(int)
				length += ByteStream.getObjectSize(actRule);  //计算字段actRule的长度 size_of(String)
				length += ByteStream.getObjectSize(comment);  //计算字段comment的长度 size_of(String)
				length += ByteStream.getObjectSize(actBanner);  //计算字段actBanner的长度 size_of(String)
				length += ByteStream.getObjectSize(actTitle);  //计算字段actTitle的长度 size_of(String)
				length += ByteStream.getObjectSize(ruleDesc);  //计算字段ruleDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


//	/**
//	 *   计算类长度
//	 *   这个是实现String字符集传入的方法
//	 *  
// 	 */
//	public int getSize(ByteStream bs)
//	{
//		int length = 4;
//		try{
//				length = 4;  //size_of(ActInfo)
//				length += 4;  //计算字段version的长度 size_of(int)
//				length += 4;  //计算字段actId的长度 size_of(int)
//				length += 4;  //计算字段actBeginTime的长度 size_of(long)
//				length += 4;  //计算字段actEndTime的长度 size_of(long)
//				length += 4;  //计算字段actType的长度 size_of(int)
//				length += 4 + actRule.getBytes(bs.getDecodeCharset()).length;  //计算字段actRule的长度 size_of(String)
//				length += 4 + comment.getBytes(bs.getDecodeCharset()).length;  //计算字段comment的长度 size_of(String)
//				length += 4 + actBanner.getBytes(bs.getDecodeCharset()).length;  //计算字段actBanner的长度 size_of(String)
//				length += 4 + actTitle.getBytes(bs.getDecodeCharset()).length;  //计算字段actTitle的长度 size_of(String)
//				length += 4 + ruleDesc.getBytes(bs.getDecodeCharset()).length;  //计算字段ruleDesc的长度 size_of(String)
//				length += 4 + reserved.getBytes(bs.getDecodeCharset()).length;  //计算字段reserved的长度 size_of(String)
//		}catch(Exception e){
//				e.printStackTrace();
//		}
//		return length;
//	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
