//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.activity.idl.QgoActivityService.java

package com.qq.qqbuy.thirdparty.idl.activity.draw.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *奖品信息
 *
 *@date 2013-05-16 07:42:40
 *
 *@since version:0
*/
public class QgoAward  implements ICanSerializeObject
{
	/**
	 * 版本控制的字段
	 *
	 * 版本 >= 0
	 */
	 private int version = 1;

	/**
	 * 信息类型
	 *
	 * 版本 >= 0
	 */
	 private int awardId;

	/**
	 * 奖品名称
	 *
	 * 版本 >= 0
	 */
	 private String awardName = new String();

	/**
	 * 奖品单元
	 *
	 * 版本 >= 0
	 */
	 private String awardUnit = new String();

	/**
	 * 奖品类型
	 *
	 * 版本 >= 0
	 */
	 private int awardType;

	/**
	 * 奖品状态
	 *
	 * 版本 >= 0
	 */
	 private int status;

	/**
	 * 奖品数量
	 *
	 * 版本 >= 0
	 */
	 private int totalNum;

	/**
	 * 奖品已领数量
	 *
	 * 版本 >= 0
	 */
	 private int gainedNum;

	/**
	 * 奖品描述
	 *
	 * 版本 >= 0
	 */
	 private String comment = new String();

	/**
	 * 奖品号码
	 *
	 * 版本 >= 0
	 */
	 private String seqNo = new String();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();

	/**
	 * 奖品图片
	 *
	 * 版本 >= 1
	 */
	 private String awardURL = new String();

	/**
	 * 领取奖品所需最低订单金额
	 *
	 * 版本 >= 1
	 */
	 private long minDealFee;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize() );
		bs.pushInt(version);
		bs.pushInt(awardId);
		bs.pushString(awardName);
		bs.pushString(awardUnit);
		bs.pushInt(awardType);
		bs.pushInt(status);
		bs.pushInt(totalNum);
		bs.pushInt(gainedNum);
		bs.pushString(comment);
		bs.pushString(seqNo);
		bs.pushString(reserved);
		if(  this.version >= 1 ){
				bs.pushString(awardURL);
		}
		if(  this.version >= 1 ){
				bs.pushUInt(minDealFee);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		awardId = bs.popInt();
		awardName = bs.popString();
		awardUnit = bs.popString();
		awardType = bs.popInt();
		status = bs.popInt();
		totalNum = bs.popInt();
		gainedNum = bs.popInt();
		comment = bs.popString();
		seqNo = bs.popString();
		reserved = bs.popString();
		if(  this.version >= 1 ){
				awardURL = bs.popString();
		}
		if(  this.version >= 1 ){
				minDealFee = bs.popUInt();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取信息类型
	 * 
	 * 此字段的版本 >= 0
	 * @return awardId value 类型为:int
	 * 
	 */
	public int getAwardId()
	{
		return awardId;
	}


	/**
	 * 设置信息类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setAwardId(int value)
	{
		this.awardId = value;
	}


	/**
	 * 获取奖品名称
	 * 
	 * 此字段的版本 >= 0
	 * @return awardName value 类型为:String
	 * 
	 */
	public String getAwardName()
	{
		return awardName;
	}


	/**
	 * 设置奖品名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAwardName(String value)
	{
		this.awardName = value;
	}


	/**
	 * 获取奖品单元
	 * 
	 * 此字段的版本 >= 0
	 * @return awardUnit value 类型为:String
	 * 
	 */
	public String getAwardUnit()
	{
		return awardUnit;
	}


	/**
	 * 设置奖品单元
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAwardUnit(String value)
	{
		this.awardUnit = value;
	}


	/**
	 * 获取奖品类型
	 * 
	 * 此字段的版本 >= 0
	 * @return awardType value 类型为:int
	 * 
	 */
	public int getAwardType()
	{
		return awardType;
	}


	/**
	 * 设置奖品类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setAwardType(int value)
	{
		this.awardType = value;
	}


	/**
	 * 获取奖品状态
	 * 
	 * 此字段的版本 >= 0
	 * @return status value 类型为:int
	 * 
	 */
	public int getStatus()
	{
		return status;
	}


	/**
	 * 设置奖品状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setStatus(int value)
	{
		this.status = value;
	}


	/**
	 * 获取奖品数量
	 * 
	 * 此字段的版本 >= 0
	 * @return totalNum value 类型为:int
	 * 
	 */
	public int getTotalNum()
	{
		return totalNum;
	}


	/**
	 * 设置奖品数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setTotalNum(int value)
	{
		this.totalNum = value;
	}


	/**
	 * 获取奖品已领数量
	 * 
	 * 此字段的版本 >= 0
	 * @return gainedNum value 类型为:int
	 * 
	 */
	public int getGainedNum()
	{
		return gainedNum;
	}


	/**
	 * 设置奖品已领数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setGainedNum(int value)
	{
		this.gainedNum = value;
	}


	/**
	 * 获取奖品描述
	 * 
	 * 此字段的版本 >= 0
	 * @return comment value 类型为:String
	 * 
	 */
	public String getComment()
	{
		return comment;
	}


	/**
	 * 设置奖品描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setComment(String value)
	{
		this.comment = value;
	}


	/**
	 * 获取奖品号码
	 * 
	 * 此字段的版本 >= 0
	 * @return seqNo value 类型为:String
	 * 
	 */
	public String getSeqNo()
	{
		return seqNo;
	}


	/**
	 * 设置奖品号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSeqNo(String value)
	{
		this.seqNo = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
	}


	/**
	 * 获取奖品图片
	 * 
	 * 此字段的版本 >= 1
	 * @return awardURL value 类型为:String
	 * 
	 */
	public String getAwardURL()
	{
		return awardURL;
	}


	/**
	 * 设置奖品图片
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:String
	 * 
	 */
	public void setAwardURL(String value)
	{
		this.awardURL = value;
	}


	/**
	 * 获取领取奖品所需最低订单金额
	 * 
	 * 此字段的版本 >= 1
	 * @return minDealFee value 类型为:long
	 * 
	 */
	public long getMinDealFee()
	{
		return minDealFee;
	}


	/**
	 * 设置领取奖品所需最低订单金额
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:long
	 * 
	 */
	public void setMinDealFee(long value)
	{
		this.minDealFee = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(QgoAward)
				length += 4;  //计算字段version的长度 size_of(int)
				length += 4;  //计算字段awardId的长度 size_of(int)
				length += ByteStream.getObjectSize(awardName);  //计算字段awardName的长度 size_of(String)
				length += ByteStream.getObjectSize(awardUnit);  //计算字段awardUnit的长度 size_of(String)
				length += 4;  //计算字段awardType的长度 size_of(int)
				length += 4;  //计算字段status的长度 size_of(int)
				length += 4;  //计算字段totalNum的长度 size_of(int)
				length += 4;  //计算字段gainedNum的长度 size_of(int)
				length += ByteStream.getObjectSize(comment);  //计算字段comment的长度 size_of(String)
				length += ByteStream.getObjectSize(seqNo);  //计算字段seqNo的长度 size_of(String)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
				if(  this.version >= 1 ){
						length += ByteStream.getObjectSize(awardURL);  //计算字段awardURL的长度 size_of(String)
				}
				if(  this.version >= 1 ){
						length += 4;  //计算字段minDealFee的长度 size_of(long)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

//
//	/**
//	 *   计算类长度
//	 *   这个是实现String字符集传入的方法
//	 *  
// 	 */
//	public int getSize(ByteStream bs)
//	{
//		int length = 4;
//		try{
//				length = 4;  //size_of(QgoAward)
//				length += 4;  //计算字段version的长度 size_of(int)
//				length += 4;  //计算字段awardId的长度 size_of(int)
//				length += 4 + awardName.getBytes(bs.getDecodeCharset()).length;  //计算字段awardName的长度 size_of(String)
//				length += 4 + awardUnit.getBytes(bs.getDecodeCharset()).length;  //计算字段awardUnit的长度 size_of(String)
//				length += 4;  //计算字段awardType的长度 size_of(int)
//				length += 4;  //计算字段status的长度 size_of(int)
//				length += 4;  //计算字段totalNum的长度 size_of(int)
//				length += 4;  //计算字段gainedNum的长度 size_of(int)
//				length += 4 + comment.getBytes(bs.getDecodeCharset()).length;  //计算字段comment的长度 size_of(String)
//				length += 4 + seqNo.getBytes(bs.getDecodeCharset()).length;  //计算字段seqNo的长度 size_of(String)
//				length += 4 + reserved.getBytes(bs.getDecodeCharset()).length;  //计算字段reserved的长度 size_of(String)
//				if(  this.version >= 1 ){
//						length += 4 + awardURL.getBytes(bs.getDecodeCharset()).length;  //计算字段awardURL的长度 size_of(String)
//				}
//				if(  this.version >= 1 ){
//						length += 4;  //计算字段minDealFee的长度 size_of(long)
//				}
//		}catch(Exception e){
//				e.printStackTrace();
//		}
//		return length;
//	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本1所包含的字段*******
 *	int version;///<版本控制的字段
 *	int awardId;///<信息类型
 *	String awardName;///<奖品名称
 *	String awardUnit;///<奖品单元
 *	int awardType;///<奖品类型
 *	int status;///<奖品状态
 *	int totalNum;///<奖品数量
 *	int gainedNum;///<奖品已领数量
 *	String comment;///<奖品描述
 *	String seqNo;///<奖品号码
 *	String reserved;///<保留字段
 *	String awardURL;///<奖品图片
 *	long minDealFee;///<领取奖品所需最低订单金额
 *****以上是版本1所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
