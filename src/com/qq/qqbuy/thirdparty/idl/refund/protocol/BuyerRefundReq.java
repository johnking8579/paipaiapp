 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.Refund.ao.idl.RefundAo.java

package com.qq.qqbuy.thirdparty.idl.refund.protocol;


import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *买家要求退款
 *
 *@date 2014-12-01 02:20:40
 *
 *@since version:0
*/
public class  BuyerRefundReq implements IServiceObject
{
	/**
	 * 代码文件名
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 基本事件需要的信息
	 *
	 * 版本 >= 0
	 */
	 private EventParamBasePo Basestu = new EventParamBasePo();

	/**
	 * 标记发货事件参数需要的信息
	 *
	 * 版本 >= 0
	 */
	 private EventParamsRefundPo Refundstu = new EventParamsRefundPo();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(Source);
		bs.pushObject(Basestu);
		bs.pushObject(Refundstu);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		Source = bs.popString();
		Basestu = (EventParamBasePo)bs.popObject(EventParamBasePo.class);
		Refundstu = (EventParamsRefundPo)bs.popObject(EventParamsRefundPo.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x876d1801L;
	}


	/**
	 * 获取代码文件名
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置代码文件名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取基本事件需要的信息
	 * 
	 * 此字段的版本 >= 0
	 * @return Basestu value 类型为:EventParamBasePo
	 * 
	 */
	public EventParamBasePo getBasestu()
	{
		return Basestu;
	}


	/**
	 * 设置基本事件需要的信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:EventParamBasePo
	 * 
	 */
	public void setBasestu(EventParamBasePo value)
	{
		if (value != null) {
				this.Basestu = value;
		}else{
				this.Basestu = new EventParamBasePo();
		}
	}


	/**
	 * 获取标记发货事件参数需要的信息
	 * 
	 * 此字段的版本 >= 0
	 * @return Refundstu value 类型为:EventParamsRefundPo
	 * 
	 */
	public EventParamsRefundPo getRefundstu()
	{
		return Refundstu;
	}


	/**
	 * 设置标记发货事件参数需要的信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:EventParamsRefundPo
	 * 
	 */
	public void setRefundstu(EventParamsRefundPo value)
	{
		if (value != null) {
				this.Refundstu = value;
		}else{
				this.Refundstu = new EventParamsRefundPo();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(BuyerRefundReq)
				length += ByteStream.getObjectSize(Source, null);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(Basestu, null);  //计算字段Basestu的长度 size_of(EventParamBasePo)
				length += ByteStream.getObjectSize(Refundstu, null);  //计算字段Refundstu的长度 size_of(EventParamsRefundPo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(BuyerRefundReq)
				length += ByteStream.getObjectSize(Source, encoding);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(Basestu, encoding);  //计算字段Basestu的长度 size_of(EventParamBasePo)
				length += ByteStream.getObjectSize(Refundstu, encoding);  //计算字段Refundstu的长度 size_of(EventParamsRefundPo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
