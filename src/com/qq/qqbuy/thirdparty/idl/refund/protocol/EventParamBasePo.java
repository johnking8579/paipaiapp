//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.Refund.ao.idl.BuyerRefundReq.java

package com.qq.qqbuy.thirdparty.idl.refund.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

/**
 *基本事件参数Po
 *
 *@date 2014-12-01 02:20:40
 *
 *@since version:0
*/
public class EventParamBasePo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 1;

	/**
	 * 大订单号
	 *
	 * 版本 >= 0
	 */
	 private String DealId = new String();

	/**
	 * 事件号
	 *
	 * 版本 >= 0
	 */
	 private long EventId;

	/**
	 * 源文件文件名
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 买家账号
	 *
	 * 版本 >= 0
	 */
	 private long BuyerUin;

	/**
	 * 卖家账号
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * 子订单号,全额退款传空值即可. 完整单号
	 *
	 * 版本 >= 0
	 */
	 private String TradeId = new String();

	/**
	 * 客户端IP
	 *
	 * 版本 >= 0
	 */
	 private String ClientIP = new String();

	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String MachineKey = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushString(DealId);
		bs.pushUInt(EventId);
		bs.pushString(Source);
		bs.pushUInt(BuyerUin);
		bs.pushUInt(SellerUin);
		bs.pushString(TradeId);
		bs.pushString(ClientIP);
		bs.pushString(MachineKey);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		DealId = bs.popString();
		EventId = bs.popUInt();
		Source = bs.popString();
		BuyerUin = bs.popUInt();
		SellerUin = bs.popUInt();
		TradeId = bs.popString();
		ClientIP = bs.popString();
		MachineKey = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取大订单号
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:String
	 * 
	 */
	public String getDealId()
	{
		return DealId;
	}


	/**
	 * 设置大订单号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealId(String value)
	{
		this.DealId = value;
	}


	/**
	 * 获取事件号
	 * 
	 * 此字段的版本 >= 0
	 * @return EventId value 类型为:long
	 * 
	 */
	public long getEventId()
	{
		return EventId;
	}


	/**
	 * 设置事件号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEventId(long value)
	{
		this.EventId = value;
	}


	/**
	 * 获取源文件文件名
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置源文件文件名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取买家账号
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return BuyerUin;
	}


	/**
	 * 设置买家账号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.BuyerUin = value;
	}


	/**
	 * 获取卖家账号
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置卖家账号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
	}


	/**
	 * 获取子订单号,全额退款传空值即可
	 * 
	 * 此字段的版本 >= 0
	 * @return TradeId value 类型为:String
	 * 
	 */
	public String getTradeId()
	{
		return TradeId;
	}


	/**
	 * 设置子订单号,全额退款传空值即可
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setTradeId(String value)
	{
		this.TradeId = value;
	}


	/**
	 * 获取客户端IP
	 * 
	 * 此字段的版本 >= 0
	 * @return ClientIP value 类型为:String
	 * 
	 */
	public String getClientIP()
	{
		return ClientIP;
	}


	/**
	 * 设置客户端IP
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setClientIP(String value)
	{
		this.ClientIP = value;
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return MachineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return MachineKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.MachineKey = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(EventParamBasePo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(DealId, null);  //计算字段DealId的长度 size_of(String)
				length += 4;  //计算字段EventId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(Source, null);  //计算字段Source的长度 size_of(String)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(TradeId, null);  //计算字段TradeId的长度 size_of(String)
				length += ByteStream.getObjectSize(ClientIP, null);  //计算字段ClientIP的长度 size_of(String)
				length += ByteStream.getObjectSize(MachineKey, null);  //计算字段MachineKey的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(EventParamBasePo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(DealId, encoding);  //计算字段DealId的长度 size_of(String)
				length += 4;  //计算字段EventId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(Source, encoding);  //计算字段Source的长度 size_of(String)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(TradeId, encoding);  //计算字段TradeId的长度 size_of(String)
				length += ByteStream.getObjectSize(ClientIP, encoding);  //计算字段ClientIP的长度 size_of(String)
				length += ByteStream.getObjectSize(MachineKey, encoding);  //计算字段MachineKey的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
