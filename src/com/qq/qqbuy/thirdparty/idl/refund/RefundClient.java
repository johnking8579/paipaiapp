package com.qq.qqbuy.thirdparty.idl.refund;

import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.exception.ExternalInterfaceException;
import com.qq.qqbuy.deal.po.ApplyRefundForm;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.refund.protocol.BuyerRefundReq;
import com.qq.qqbuy.thirdparty.idl.refund.protocol.BuyerRefundResp;
import com.qq.qqbuy.thirdparty.idl.refund.protocol.EventParamBasePo;
import com.qq.qqbuy.thirdparty.idl.refund.protocol.EventParamsRefundPo;

/**
 * 申请退款
 * @author JingYing
 * @date 2014年11月27日
 */
public class RefundClient extends SupportIDLBaseClient {
	
	private final String SOURCE = "mobilelife";//移动电商来源，免登录态
	
	/**
	 * 提交退款协议
	 * @param form
	 * @param clientIp
	 * @param mk
	 * @param sk
	 * @param eventId 见DealConstant.EVENT_*
	 * 
	 */
	public void applyRefund(ApplyRefundForm form, String clientIp, String mk, String sk, int eventId)	{
		BuyerRefundReq req = new BuyerRefundReq();
		BuyerRefundResp resp = new BuyerRefundResp();
		req.setSource(SOURCE);
		
		EventParamBasePo basePo = new EventParamBasePo();
		basePo.setBuyerUin(form.getBuyer());
		basePo.setClientIP(clientIp);
		basePo.setDealId(form.getDealId());
		basePo.setEventId(eventId);
		basePo.setMachineKey(mk);
		basePo.setSellerUin(form.getSeller());
		basePo.setSource(SOURCE);
		basePo.setTradeId(form.getTradeId());
		req.setBasestu(basePo);
		
		EventParamsRefundPo refundPo = new EventParamsRefundPo();
		refundPo.setIsRecvItem(form.getIsRecItem());
		refundPo.setIsReqitem(form.getIsReqItem());
		refundPo.setRefundApplyMsg(form.getRefundApplyMsg());
		refundPo.setRefundReasonType(form.getRefundReasonType());
		refundPo.setRefundToBuyer(form.getRefundToBuyerAmount());
		req.setRefundstu(refundPo);
		
		AsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		stub.setOperator(form.getBuyer());
		stub.setUin(form.getBuyer());
		stub.setSkey(sk.getBytes());
		if(EnvManager.isNotIdc()){
			stub.setIpAndPort("10.136.10.162", 53101);
		}
		
		invoke(stub, req, resp);
		if(resp.getResult() != 0)
			throw new ExternalInterfaceException(0, resp.getResult());
	}

}
