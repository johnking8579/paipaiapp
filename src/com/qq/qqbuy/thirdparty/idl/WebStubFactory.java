package com.qq.qqbuy.thirdparty.idl;

import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.paipai.component.configagent.Configs;
import com.qq.qqbuy.common.client.ao.AoClient;
import com.qq.qqbuy.common.client.log.BossWebstub2Proxy;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.thirdparty.idl.boss.BossWebStub2;

/**
 * 取webstub时统一在此调用, 以便使用AsynWebstubLogProxy
 *
 */
public class WebStubFactory {
	private static final int 
				connectTimeOut = 3000,
				readTimeOut = 3000;

	/**
	 * 调拍拍的IDL，服务方为GBK
	 * @return
	 */
	public static AsynWebStub getWebStub4PaiPaiGBK() {
		return newDefaultStub("GBK");
	}
	
	/**
	 * 调拍拍的IDL，默认服务方为UTF-8
	 * @return
	 */
	public static AsynWebStub getWebStub4PaiPai() {
		AsynWebStub stub = newDefaultStub("UTF-8");
		return stub;
	}

	public static AsynWebStub getWebStub4PaiPaiSkeySign(long uin, String skey, String charsetType) {
		AsynWebStub webStub = newDefaultStub(charsetType);
		webStub.setUin(uin);
		webStub.setOperator(uin);
		webStub.setSkey(skey.getBytes());
		return webStub;
	}

	
	/**
	 * 生成代理AsynWebStub记录日志
	 * @param charset
	 * @return
	 */
	private static AsynWebStub newDefaultStub(String charset) {
		AsynWebStub stub = AoClient.newProxyWebStub();
		stub.setTimeout(connectTimeOut, readTimeOut);
		stub.setUin(System.currentTimeMillis()); // 应为setUin被服务端用来做负载均衡，故需要设置。
		if(!EnvManager.isJd())	{	//京东下不启用,启用后会访问配置中心的qqbuy_paipai_config=10.137.141.93:53111,对应移动电商服务器, 目的不明
			stub.setConfigType(Configs.PAIPAI_CONFIG_TYPE);
		}
		stub.setClientIP(iP2Long(Util.getLocalIp()));
		stub.setStringEncodecharset(charset);
		stub.setStringDecodecharset(charset);
		return stub;
	}
	
	public static BossWebStub2 getBossStub2() {
		BossWebStub2 stub = BossWebstub2Proxy.newProxy();
		stub.setTimeoutMs(connectTimeOut);
		return stub;
	}
	
	
	/**
	 * 
	 * @Title: IP2Long
	 * @Description: 转成long
	 * @param ip
	 * @return long 返回类型
	 * 
	 * @throws
	 */
	public static long iP2Long(String ip) {
		if (null == ip) {
			return -1;
		}
		String[] ipArray = ip.split("[.]");
		if (4 != ipArray.length) {
			return -2;
		}
		long ipLong = 0;
		for (int i = 3; i >= 0; --i) {
			try {
				long section = Long.valueOf(ipArray[i]);
				ipLong = (ipLong << 8) + section;
			} catch (Exception e) {
				return -3;
			}
		}
		return ipLong;
	}
}
