//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.oneday.CachestockAO.java

package com.qq.qqbuy.thirdparty.idl.oneday.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *bo
 *
 *@date 2013-08-19 11:07:36
 *
 *@since version:20100914
*/
public class ComdyStockInfo  implements ICanSerializeObject
{
	/**
	 * fcomdyid
	 *
	 * 版本 >= 0
	 */
	 private String fcomdyid = new String();

	/**
	 * fstocknum
	 *
	 * 版本 >= 0
	 */
	 private long fstocknum;

	/**
	 * fsoldnum
	 *
	 * 版本 >= 0
	 */
	 private long fsoldnum;

	/**
	 * fremainnum
	 *
	 * 版本 >= 0
	 */
	 private long fremainnum;

	/**
	 * fproperty
	 *
	 * 版本 >= 0
	 */
	 private long fproperty;

	/**
	 * fcomdyname
	 *
	 * 版本 >= 0
	 */
	 private String fcomdyname = new String();

	/**
	 * flimitbuynum
	 *
	 * 版本 >= 0
	 */
	 private long flimitbuynum;

	/**
	 * fextinfo
	 *
	 * 版本 >= 0
	 */
	 private String fextinfo = new String();

	/**
	 * fuploadtime
	 *
	 * 版本 >= 0
	 */
	 private long fuploadtime;

	/**
	 * fprice
	 *
	 * 版本 >= 0
	 */
	 private long fprice;

	/**
	 * fdesc
	 *
	 * 版本 >= 0
	 */
	 private String fdesc = new String();

	/**
	 * fmtime
	 *
	 * 版本 >= 0
	 */
	 private String fmtime = new String();

	/**
	 * fselleruin
	 *
	 * 版本 >= 0
	 */
	 private long fselleruin;

	/**
	 * fstockindexList
	 *
	 * 版本 >= 0
	 */
	 private Vector<ComdyStockInfoByStock> fstockindexList = new Vector<ComdyStockInfoByStock>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(fcomdyid);
		bs.pushUInt(fstocknum);
		bs.pushUInt(fsoldnum);
		bs.pushUInt(fremainnum);
		bs.pushUInt(fproperty);
		bs.pushString(fcomdyname);
		bs.pushUInt(flimitbuynum);
		bs.pushString(fextinfo);
		bs.pushUInt(fuploadtime);
		bs.pushUInt(fprice);
		bs.pushString(fdesc);
		bs.pushString(fmtime);
		bs.pushUInt(fselleruin);
		bs.pushObject(fstockindexList);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		fcomdyid = bs.popString();
		fstocknum = bs.popUInt();
		fsoldnum = bs.popUInt();
		fremainnum = bs.popUInt();
		fproperty = bs.popUInt();
		fcomdyname = bs.popString();
		flimitbuynum = bs.popUInt();
		fextinfo = bs.popString();
		fuploadtime = bs.popUInt();
		fprice = bs.popUInt();
		fdesc = bs.popString();
		fmtime = bs.popString();
		fselleruin = bs.popUInt();
		fstockindexList = (Vector<ComdyStockInfoByStock>)bs.popVector(ComdyStockInfoByStock.class);
		return bs.getReadLength();
	} 


	/**
	 * 获取fcomdyid
	 * 
	 * 此字段的版本 >= 0
	 * @return fcomdyid value 类型为:String
	 * 
	 */
	public String getFcomdyid()
	{
		return fcomdyid;
	}


	/**
	 * 设置fcomdyid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setFcomdyid(String value)
	{
		this.fcomdyid = value;
	}


	/**
	 * 获取fstocknum
	 * 
	 * 此字段的版本 >= 0
	 * @return fstocknum value 类型为:long
	 * 
	 */
	public long getFstocknum()
	{
		return fstocknum;
	}


	/**
	 * 设置fstocknum
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFstocknum(long value)
	{
		this.fstocknum = value;
	}


	/**
	 * 获取fsoldnum
	 * 
	 * 此字段的版本 >= 0
	 * @return fsoldnum value 类型为:long
	 * 
	 */
	public long getFsoldnum()
	{
		return fsoldnum;
	}


	/**
	 * 设置fsoldnum
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFsoldnum(long value)
	{
		this.fsoldnum = value;
	}


	/**
	 * 获取fremainnum
	 * 
	 * 此字段的版本 >= 0
	 * @return fremainnum value 类型为:long
	 * 
	 */
	public long getFremainnum()
	{
		return fremainnum;
	}


	/**
	 * 设置fremainnum
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFremainnum(long value)
	{
		this.fremainnum = value;
	}


	/**
	 * 获取fproperty
	 * 
	 * 此字段的版本 >= 0
	 * @return fproperty value 类型为:long
	 * 
	 */
	public long getFproperty()
	{
		return fproperty;
	}


	/**
	 * 设置fproperty
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFproperty(long value)
	{
		this.fproperty = value;
	}


	/**
	 * 获取fcomdyname
	 * 
	 * 此字段的版本 >= 0
	 * @return fcomdyname value 类型为:String
	 * 
	 */
	public String getFcomdyname()
	{
		return fcomdyname;
	}


	/**
	 * 设置fcomdyname
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setFcomdyname(String value)
	{
		this.fcomdyname = value;
	}


	/**
	 * 获取flimitbuynum
	 * 
	 * 此字段的版本 >= 0
	 * @return flimitbuynum value 类型为:long
	 * 
	 */
	public long getFlimitbuynum()
	{
		return flimitbuynum;
	}


	/**
	 * 设置flimitbuynum
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFlimitbuynum(long value)
	{
		this.flimitbuynum = value;
	}


	/**
	 * 获取fextinfo
	 * 
	 * 此字段的版本 >= 0
	 * @return fextinfo value 类型为:String
	 * 
	 */
	public String getFextinfo()
	{
		return fextinfo;
	}


	/**
	 * 设置fextinfo
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setFextinfo(String value)
	{
		this.fextinfo = value;
	}


	/**
	 * 获取fuploadtime
	 * 
	 * 此字段的版本 >= 0
	 * @return fuploadtime value 类型为:long
	 * 
	 */
	public long getFuploadtime()
	{
		return fuploadtime;
	}


	/**
	 * 设置fuploadtime
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFuploadtime(long value)
	{
		this.fuploadtime = value;
	}


	/**
	 * 获取fprice
	 * 
	 * 此字段的版本 >= 0
	 * @return fprice value 类型为:long
	 * 
	 */
	public long getFprice()
	{
		return fprice;
	}


	/**
	 * 设置fprice
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFprice(long value)
	{
		this.fprice = value;
	}


	/**
	 * 获取fdesc
	 * 
	 * 此字段的版本 >= 0
	 * @return fdesc value 类型为:String
	 * 
	 */
	public String getFdesc()
	{
		return fdesc;
	}


	/**
	 * 设置fdesc
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setFdesc(String value)
	{
		this.fdesc = value;
	}


	/**
	 * 获取fmtime
	 * 
	 * 此字段的版本 >= 0
	 * @return fmtime value 类型为:String
	 * 
	 */
	public String getFmtime()
	{
		return fmtime;
	}


	/**
	 * 设置fmtime
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setFmtime(String value)
	{
		this.fmtime = value;
	}


	/**
	 * 获取fselleruin
	 * 
	 * 此字段的版本 >= 0
	 * @return fselleruin value 类型为:long
	 * 
	 */
	public long getFselleruin()
	{
		return fselleruin;
	}


	/**
	 * 设置fselleruin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFselleruin(long value)
	{
		this.fselleruin = value;
	}


	/**
	 * 获取fstockindexList
	 * 
	 * 此字段的版本 >= 0
	 * @return fstockindexList value 类型为:Vector<ComdyStockInfoByStock>
	 * 
	 */
	public Vector<ComdyStockInfoByStock> getFstockindexList()
	{
		return fstockindexList;
	}


	/**
	 * 设置fstockindexList
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<ComdyStockInfoByStock>
	 * 
	 */
	public void setFstockindexList(Vector<ComdyStockInfoByStock> value)
	{
		if (value != null) {
				this.fstockindexList = value;
		}else{
				this.fstockindexList = new Vector<ComdyStockInfoByStock>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(ComdyStockInfo)
				length += ByteStream.getObjectSize(fcomdyid);  //计算字段fcomdyid的长度 size_of(String)
				length += 4;  //计算字段fstocknum的长度 size_of(uint32_t)
				length += 4;  //计算字段fsoldnum的长度 size_of(uint32_t)
				length += 4;  //计算字段fremainnum的长度 size_of(uint32_t)
				length += 4;  //计算字段fproperty的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(fcomdyname);  //计算字段fcomdyname的长度 size_of(String)
				length += 4;  //计算字段flimitbuynum的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(fextinfo);  //计算字段fextinfo的长度 size_of(String)
				length += 4;  //计算字段fuploadtime的长度 size_of(uint32_t)
				length += 4;  //计算字段fprice的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(fdesc);  //计算字段fdesc的长度 size_of(String)
				length += ByteStream.getObjectSize(fmtime);  //计算字段fmtime的长度 size_of(String)
				length += 4;  //计算字段fselleruin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(fstockindexList);  //计算字段fstockindexList的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
