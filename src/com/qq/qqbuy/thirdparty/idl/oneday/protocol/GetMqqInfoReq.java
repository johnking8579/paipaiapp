 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.oneday.OadsmsAo.java

package com.qq.qqbuy.thirdparty.idl.oneday.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *上传请求，主要参数为文件名，和内容
 *
 *@date 2013-08-19 11:10:30
 *
 *@since version:0
*/
public class  GetMqqInfoReq implements IServiceObject
{
	/**
	 * 用户qq号
	 *
	 * 版本 >= 0
	 */
	 private long dwUin;


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(dwUin);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		dwUin = bs.popUInt();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x46071801L;
	}


	/**
	 * 获取用户qq号
	 * 
	 * 此字段的版本 >= 0
	 * @return dwUin value 类型为:long
	 * 
	 */
	public long getDwUin()
	{
		return dwUin;
	}


	/**
	 * 设置用户qq号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwUin(long value)
	{
		this.dwUin = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetMqqInfoReq)
				length += 4;  //计算字段dwUin的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
