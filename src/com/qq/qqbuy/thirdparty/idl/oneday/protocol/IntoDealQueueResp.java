 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.oneday.OadmobileAo.java

package com.qq.qqbuy.thirdparty.idl.oneday.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *抢购回复的数据
 *
 *@date 2013-08-19 11:07:52
 *
 *@since version:0
*/
public class  IntoDealQueueResp implements IServiceObject
{
	public long result;
	/**
	 * 抢购结果
	 *
	 * 版本 >= 0
	 */
	 private DealResult oDealResult = new DealResult();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(oDealResult);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		oDealResult = (DealResult) bs.popObject(DealResult.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x46108801L;
	}


	/**
	 * 获取抢购结果
	 * 
	 * 此字段的版本 >= 0
	 * @return oDealResult value 类型为:DealResult
	 * 
	 */
	public DealResult getODealResult()
	{
		return oDealResult;
	}


	/**
	 * 设置抢购结果
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:DealResult
	 * 
	 */
	public void setODealResult(DealResult value)
	{
		if (value != null) {
				this.oDealResult = value;
		}else{
				this.oDealResult = new DealResult();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(IntoDealQueueResp)
				length += ByteStream.getObjectSize(oDealResult);  //计算字段oDealResult的长度 size_of(DealResult)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
