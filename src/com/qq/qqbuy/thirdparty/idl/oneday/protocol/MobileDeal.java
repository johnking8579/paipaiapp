//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.oneday.OadmobileAo.java

package com.qq.qqbuy.thirdparty.idl.oneday.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *bo
 *
 *@date 2013-08-19 11:07:52
 *
 *@since version:20100917
*/
public class MobileDeal  implements ICanSerializeObject
{
	/**
	 * 买家qq号
	 *
	 * 版本 >= 0
	 */
	 private long dwBuyerUin;

	/**
	 * 卖家qq号
	 *
	 * 版本 >= 0
	 */
	 private long dwSellerUin;

	/**
	 * 买家姓名
	 *
	 * 版本 >= 0
	 */
	 private String sTrueName = new String();

	/**
	 * 手机号
	 *
	 * 版本 >= 0
	 */
	 private String sHandset = new String();

	/**
	 * 座机号
	 *
	 * 版本 >= 0
	 */
	 private String sPhone = new String();

	/**
	 * 邮编
	 *
	 * 版本 >= 0
	 */
	 private String sPostCode = new String();

	/**
	 * 收货地址
	 *
	 * 版本 >= 0
	 */
	 private String sAddress = new String();

	/**
	 * 地区ID
	 *
	 * 版本 >= 0
	 */
	 private long dwRegion;

	/**
	 * 商品ID
	 *
	 * 版本 >= 0
	 */
	 private String sCommodityId = new String();

	/**
	 * 是否有库存
	 *
	 * 版本 >= 0
	 */
	 private long dwProperty;

	/**
	 * 库存属性
	 *
	 * 版本 >= 0
	 */
	 private String sStockAttr = new String();

	/**
	 * 购买留言
	 *
	 * 版本 >= 0
	 */
	 private String sNote = new String();

	/**
	 * 购买数量
	 *
	 * 版本 >= 0
	 */
	 private long dwBuyNum;

	/**
	 * 地址ID
	 *
	 * 版本 >= 0
	 */
	 private long dwAddressId;

	/**
	 * 商品价格
	 *
	 * 版本 >= 0
	 */
	 private long dwPrice;

	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String sMKey = new String();

	/**
	 * 买家ip
	 *
	 * 版本 >= 0
	 */
	 private String sClientIp = new String();

	/**
	 * 商品总数量
	 *
	 * 版本 >= 0
	 */
	 private long dwComdyNum;

	/**
	 * 最大购买数量
	 *
	 * 版本 >= 0
	 */
	 private long dwMaxBuyNum;

	/**
	 * 上架时间
	 *
	 * 版本 >= 0
	 */
	 private long dwUploadTime;

	/**
	 * 商品所属的活动类型
	 *
	 * 版本 >= 0
	 */
	 private long dwActiveType;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(dwBuyerUin);
		bs.pushUInt(dwSellerUin);
		bs.pushString(sTrueName);
		bs.pushString(sHandset);
		bs.pushString(sPhone);
		bs.pushString(sPostCode);
		bs.pushString(sAddress);
		bs.pushUInt(dwRegion);
		bs.pushString(sCommodityId);
		bs.pushUInt(dwProperty);
		bs.pushString(sStockAttr);
		bs.pushString(sNote);
		bs.pushUInt(dwBuyNum);
		bs.pushUInt(dwAddressId);
		bs.pushUInt(dwPrice);
		bs.pushString(sMKey);
		bs.pushString(sClientIp);
		bs.pushUInt(dwComdyNum);
		bs.pushUInt(dwMaxBuyNum);
		bs.pushUInt(dwUploadTime);
		bs.pushUInt(dwActiveType);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		dwBuyerUin = bs.popUInt();
		dwSellerUin = bs.popUInt();
		sTrueName = bs.popString();
		sHandset = bs.popString();
		sPhone = bs.popString();
		sPostCode = bs.popString();
		sAddress = bs.popString();
		dwRegion = bs.popUInt();
		sCommodityId = bs.popString();
		dwProperty = bs.popUInt();
		sStockAttr = bs.popString();
		sNote = bs.popString();
		dwBuyNum = bs.popUInt();
		dwAddressId = bs.popUInt();
		dwPrice = bs.popUInt();
		sMKey = bs.popString();
		sClientIp = bs.popString();
		dwComdyNum = bs.popUInt();
		dwMaxBuyNum = bs.popUInt();
		dwUploadTime = bs.popUInt();
		dwActiveType = bs.popUInt();
		return bs.getReadLength();
	} 


	/**
	 * 获取买家qq号
	 * 
	 * 此字段的版本 >= 0
	 * @return dwBuyerUin value 类型为:long
	 * 
	 */
	public long getDwBuyerUin()
	{
		return dwBuyerUin;
	}


	/**
	 * 设置买家qq号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwBuyerUin(long value)
	{
		this.dwBuyerUin = value;
	}


	/**
	 * 获取卖家qq号
	 * 
	 * 此字段的版本 >= 0
	 * @return dwSellerUin value 类型为:long
	 * 
	 */
	public long getDwSellerUin()
	{
		return dwSellerUin;
	}


	/**
	 * 设置卖家qq号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwSellerUin(long value)
	{
		this.dwSellerUin = value;
	}


	/**
	 * 获取买家姓名
	 * 
	 * 此字段的版本 >= 0
	 * @return sTrueName value 类型为:String
	 * 
	 */
	public String getSTrueName()
	{
		return sTrueName;
	}


	/**
	 * 设置买家姓名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSTrueName(String value)
	{
		this.sTrueName = value;
	}


	/**
	 * 获取手机号
	 * 
	 * 此字段的版本 >= 0
	 * @return sHandset value 类型为:String
	 * 
	 */
	public String getSHandset()
	{
		return sHandset;
	}


	/**
	 * 设置手机号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSHandset(String value)
	{
		this.sHandset = value;
	}


	/**
	 * 获取座机号
	 * 
	 * 此字段的版本 >= 0
	 * @return sPhone value 类型为:String
	 * 
	 */
	public String getSPhone()
	{
		return sPhone;
	}


	/**
	 * 设置座机号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSPhone(String value)
	{
		this.sPhone = value;
	}


	/**
	 * 获取邮编
	 * 
	 * 此字段的版本 >= 0
	 * @return sPostCode value 类型为:String
	 * 
	 */
	public String getSPostCode()
	{
		return sPostCode;
	}


	/**
	 * 设置邮编
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSPostCode(String value)
	{
		this.sPostCode = value;
	}


	/**
	 * 获取收货地址
	 * 
	 * 此字段的版本 >= 0
	 * @return sAddress value 类型为:String
	 * 
	 */
	public String getSAddress()
	{
		return sAddress;
	}


	/**
	 * 设置收货地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSAddress(String value)
	{
		this.sAddress = value;
	}


	/**
	 * 获取地区ID
	 * 
	 * 此字段的版本 >= 0
	 * @return dwRegion value 类型为:long
	 * 
	 */
	public long getDwRegion()
	{
		return dwRegion;
	}


	/**
	 * 设置地区ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwRegion(long value)
	{
		this.dwRegion = value;
	}


	/**
	 * 获取商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return sCommodityId value 类型为:String
	 * 
	 */
	public String getSCommodityId()
	{
		return sCommodityId;
	}


	/**
	 * 设置商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSCommodityId(String value)
	{
		this.sCommodityId = value;
	}


	/**
	 * 获取是否有库存
	 * 
	 * 此字段的版本 >= 0
	 * @return dwProperty value 类型为:long
	 * 
	 */
	public long getDwProperty()
	{
		return dwProperty;
	}


	/**
	 * 设置是否有库存
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwProperty(long value)
	{
		this.dwProperty = value;
	}


	/**
	 * 获取库存属性
	 * 
	 * 此字段的版本 >= 0
	 * @return sStockAttr value 类型为:String
	 * 
	 */
	public String getSStockAttr()
	{
		return sStockAttr;
	}


	/**
	 * 设置库存属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSStockAttr(String value)
	{
		this.sStockAttr = value;
	}


	/**
	 * 获取购买留言
	 * 
	 * 此字段的版本 >= 0
	 * @return sNote value 类型为:String
	 * 
	 */
	public String getSNote()
	{
		return sNote;
	}


	/**
	 * 设置购买留言
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSNote(String value)
	{
		this.sNote = value;
	}


	/**
	 * 获取购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @return dwBuyNum value 类型为:long
	 * 
	 */
	public long getDwBuyNum()
	{
		return dwBuyNum;
	}


	/**
	 * 设置购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwBuyNum(long value)
	{
		this.dwBuyNum = value;
	}


	/**
	 * 获取地址ID
	 * 
	 * 此字段的版本 >= 0
	 * @return dwAddressId value 类型为:long
	 * 
	 */
	public long getDwAddressId()
	{
		return dwAddressId;
	}


	/**
	 * 设置地址ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwAddressId(long value)
	{
		this.dwAddressId = value;
	}


	/**
	 * 获取商品价格
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPrice value 类型为:long
	 * 
	 */
	public long getDwPrice()
	{
		return dwPrice;
	}


	/**
	 * 设置商品价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwPrice(long value)
	{
		this.dwPrice = value;
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return sMKey value 类型为:String
	 * 
	 */
	public String getSMKey()
	{
		return sMKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSMKey(String value)
	{
		this.sMKey = value;
	}


	/**
	 * 获取买家ip
	 * 
	 * 此字段的版本 >= 0
	 * @return sClientIp value 类型为:String
	 * 
	 */
	public String getSClientIp()
	{
		return sClientIp;
	}


	/**
	 * 设置买家ip
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSClientIp(String value)
	{
		this.sClientIp = value;
	}


	/**
	 * 获取商品总数量
	 * 
	 * 此字段的版本 >= 0
	 * @return dwComdyNum value 类型为:long
	 * 
	 */
	public long getDwComdyNum()
	{
		return dwComdyNum;
	}


	/**
	 * 设置商品总数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwComdyNum(long value)
	{
		this.dwComdyNum = value;
	}


	/**
	 * 获取最大购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @return dwMaxBuyNum value 类型为:long
	 * 
	 */
	public long getDwMaxBuyNum()
	{
		return dwMaxBuyNum;
	}


	/**
	 * 设置最大购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwMaxBuyNum(long value)
	{
		this.dwMaxBuyNum = value;
	}


	/**
	 * 获取上架时间
	 * 
	 * 此字段的版本 >= 0
	 * @return dwUploadTime value 类型为:long
	 * 
	 */
	public long getDwUploadTime()
	{
		return dwUploadTime;
	}


	/**
	 * 设置上架时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwUploadTime(long value)
	{
		this.dwUploadTime = value;
	}


	/**
	 * 获取商品所属的活动类型
	 * 
	 * 此字段的版本 >= 0
	 * @return dwActiveType value 类型为:long
	 * 
	 */
	public long getDwActiveType()
	{
		return dwActiveType;
	}


	/**
	 * 设置商品所属的活动类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwActiveType(long value)
	{
		this.dwActiveType = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(MobileDeal)
				length += 4;  //计算字段dwBuyerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段dwSellerUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sTrueName);  //计算字段sTrueName的长度 size_of(String)
				length += ByteStream.getObjectSize(sHandset);  //计算字段sHandset的长度 size_of(String)
				length += ByteStream.getObjectSize(sPhone);  //计算字段sPhone的长度 size_of(String)
				length += ByteStream.getObjectSize(sPostCode);  //计算字段sPostCode的长度 size_of(String)
				length += ByteStream.getObjectSize(sAddress);  //计算字段sAddress的长度 size_of(String)
				length += 4;  //计算字段dwRegion的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sCommodityId);  //计算字段sCommodityId的长度 size_of(String)
				length += 4;  //计算字段dwProperty的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sStockAttr);  //计算字段sStockAttr的长度 size_of(String)
				length += ByteStream.getObjectSize(sNote);  //计算字段sNote的长度 size_of(String)
				length += 4;  //计算字段dwBuyNum的长度 size_of(uint32_t)
				length += 4;  //计算字段dwAddressId的长度 size_of(uint32_t)
				length += 4;  //计算字段dwPrice的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sMKey);  //计算字段sMKey的长度 size_of(String)
				length += ByteStream.getObjectSize(sClientIp);  //计算字段sClientIp的长度 size_of(String)
				length += 4;  //计算字段dwComdyNum的长度 size_of(uint32_t)
				length += 4;  //计算字段dwMaxBuyNum的长度 size_of(uint32_t)
				length += 4;  //计算字段dwUploadTime的长度 size_of(uint32_t)
				length += 4;  //计算字段dwActiveType的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
