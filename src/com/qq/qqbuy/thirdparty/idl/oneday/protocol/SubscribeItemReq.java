 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.oneday.OadsmsAo.java

package com.qq.qqbuy.thirdparty.idl.oneday.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *下载请求，主要参数为文件名，和内容
 *
 *@date 2013-08-19 11:10:30
 *
 *@since version:0
*/
public class  SubscribeItemReq implements IServiceObject
{
	/**
	 * 用户qq号
	 *
	 * 版本 >= 0
	 */
	 private long dwUin;

	/**
	 * 商品ID
	 *
	 * 版本 >= 0
	 */
	 private String sItemId = new String();

	/**
	 * 商品名称
	 *
	 * 版本 >= 0
	 */
	 private String sItemTitle = new String();

	/**
	 * 上架时间
	 *
	 * 版本 >= 0
	 */
	 private long dwUploadTime;

	/**
	 * 提前提醒时间
	 *
	 * 版本 >= 0
	 */
	 private long dwAdvanceTime;

	/**
	 * 商品数量
	 *
	 * 版本 >= 0
	 */
	 private long dwItemNum;


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(dwUin);
		bs.pushString(sItemId);
		bs.pushString(sItemTitle);
		bs.pushUInt(dwUploadTime);
		bs.pushUInt(dwAdvanceTime);
		bs.pushUInt(dwItemNum);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		dwUin = bs.popUInt();
		sItemId = bs.popString();
		sItemTitle = bs.popString();
		dwUploadTime = bs.popUInt();
		dwAdvanceTime = bs.popUInt();
		dwItemNum = bs.popUInt();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x46071802L;
	}


	/**
	 * 获取用户qq号
	 * 
	 * 此字段的版本 >= 0
	 * @return dwUin value 类型为:long
	 * 
	 */
	public long getDwUin()
	{
		return dwUin;
	}


	/**
	 * 设置用户qq号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwUin(long value)
	{
		this.dwUin = value;
	}


	/**
	 * 获取商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return sItemId value 类型为:String
	 * 
	 */
	public String getSItemId()
	{
		return sItemId;
	}


	/**
	 * 设置商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSItemId(String value)
	{
		this.sItemId = value;
	}


	/**
	 * 获取商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @return sItemTitle value 类型为:String
	 * 
	 */
	public String getSItemTitle()
	{
		return sItemTitle;
	}


	/**
	 * 设置商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSItemTitle(String value)
	{
		this.sItemTitle = value;
	}


	/**
	 * 获取上架时间
	 * 
	 * 此字段的版本 >= 0
	 * @return dwUploadTime value 类型为:long
	 * 
	 */
	public long getDwUploadTime()
	{
		return dwUploadTime;
	}


	/**
	 * 设置上架时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwUploadTime(long value)
	{
		this.dwUploadTime = value;
	}


	/**
	 * 获取提前提醒时间
	 * 
	 * 此字段的版本 >= 0
	 * @return dwAdvanceTime value 类型为:long
	 * 
	 */
	public long getDwAdvanceTime()
	{
		return dwAdvanceTime;
	}


	/**
	 * 设置提前提醒时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwAdvanceTime(long value)
	{
		this.dwAdvanceTime = value;
	}


	/**
	 * 获取商品数量
	 * 
	 * 此字段的版本 >= 0
	 * @return dwItemNum value 类型为:long
	 * 
	 */
	public long getDwItemNum()
	{
		return dwItemNum;
	}


	/**
	 * 设置商品数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwItemNum(long value)
	{
		this.dwItemNum = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(SubscribeItemReq)
				length += 4;  //计算字段dwUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sItemId);  //计算字段sItemId的长度 size_of(String)
				length += ByteStream.getObjectSize(sItemTitle);  //计算字段sItemTitle的长度 size_of(String)
				length += 4;  //计算字段dwUploadTime的长度 size_of(uint32_t)
				length += 4;  //计算字段dwAdvanceTime的长度 size_of(uint32_t)
				length += 4;  //计算字段dwItemNum的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
