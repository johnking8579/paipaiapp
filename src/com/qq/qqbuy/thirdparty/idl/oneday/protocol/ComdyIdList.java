//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.oneday.CachestockAO.java

package com.qq.qqbuy.thirdparty.idl.oneday.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *bo
 *
 *@date 2013-08-19 11:07:36
 *
 *@since version:20100914
*/
public class ComdyIdList  implements ICanSerializeObject
{
	/**
	 * m_vecComdyId
	 *
	 * 版本 >= 0
	 */
	 private Vector<String> m_vecComdyId = new Vector<String>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(m_vecComdyId);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		m_vecComdyId = (Vector<String>)bs.popVector(String.class);
		return bs.getReadLength();
	} 


	/**
	 * 获取m_vecComdyId
	 * 
	 * 此字段的版本 >= 0
	 * @return m_vecComdyId value 类型为:Vector<String>
	 * 
	 */
	public Vector<String> getM_vecComdyId()
	{
		return m_vecComdyId;
	}


	/**
	 * 设置m_vecComdyId
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<String>
	 * 
	 */
	public void setM_vecComdyId(Vector<String> value)
	{
		if (value != null) {
				this.m_vecComdyId = value;
		}else{
				this.m_vecComdyId = new Vector<String>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(ComdyIdList)
				length += ByteStream.getObjectSize(m_vecComdyId);  //计算字段m_vecComdyId的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
