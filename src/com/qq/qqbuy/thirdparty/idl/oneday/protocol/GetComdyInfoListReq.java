 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.oneday.CachestockAO.java

package com.qq.qqbuy.thirdparty.idl.oneday.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *下载请求，主要参数为文件名，和内容
 *
 *@date 2013-08-19 11:07:36
 *
 *@since version:0
*/
public class  GetComdyInfoListReq implements IServiceObject
{
	/**
	 * comdyIdList
	 *
	 * 版本 >= 0
	 */
	 private ComdyIdList comdyIdList = new ComdyIdList();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(comdyIdList);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		comdyIdList = (ComdyIdList) bs.popObject(ComdyIdList.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x48001802L;
	}


	/**
	 * 获取comdyIdList
	 * 
	 * 此字段的版本 >= 0
	 * @return comdyIdList value 类型为:ComdyIdList
	 * 
	 */
	public ComdyIdList getComdyIdList()
	{
		return comdyIdList;
	}


	/**
	 * 设置comdyIdList
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ComdyIdList
	 * 
	 */
	public void setComdyIdList(ComdyIdList value)
	{
		if (value != null) {
				this.comdyIdList = value;
		}else{
				this.comdyIdList = new ComdyIdList();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetComdyInfoListReq)
				length += ByteStream.getObjectSize(comdyIdList);  //计算字段comdyIdList的长度 size_of(ComdyIdList)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
