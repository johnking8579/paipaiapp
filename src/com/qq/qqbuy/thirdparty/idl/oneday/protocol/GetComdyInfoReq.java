 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.oneday.CachestockAO.java

package com.qq.qqbuy.thirdparty.idl.oneday.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *商品id
 *
 *@date 2013-08-19 11:07:36
 *
 *@since version:0
*/
public class  GetComdyInfoReq implements IServiceObject
{
	/**
	 * comdyid
	 *
	 * 版本 >= 0
	 */
	 private String comdyid = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(comdyid);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		comdyid = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x48001801L;
	}


	/**
	 * 获取comdyid
	 * 
	 * 此字段的版本 >= 0
	 * @return comdyid value 类型为:String
	 * 
	 */
	public String getComdyid()
	{
		return comdyid;
	}


	/**
	 * 设置comdyid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setComdyid(String value)
	{
		this.comdyid = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetComdyInfoReq)
				length += ByteStream.getObjectSize(comdyid);  //计算字段comdyid的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
