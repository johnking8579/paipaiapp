 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.oneday.CachestockAO.java

package com.qq.qqbuy.thirdparty.idl.oneday.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *返回CComdyStockInfo
 *
 *@date 2013-08-19 11:07:36
 *
 *@since version:0
*/
public class  GetComdyInfoResp implements IServiceObject
{
	public long result;
	/**
	 * comdyStockInfo
	 *
	 * 版本 >= 0
	 */
	 private ComdyStockInfo comdyStockInfo = new ComdyStockInfo();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(comdyStockInfo);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		comdyStockInfo = (ComdyStockInfo) bs.popObject(ComdyStockInfo.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x48008801L;
	}


	/**
	 * 获取comdyStockInfo
	 * 
	 * 此字段的版本 >= 0
	 * @return comdyStockInfo value 类型为:ComdyStockInfo
	 * 
	 */
	public ComdyStockInfo getComdyStockInfo()
	{
		return comdyStockInfo;
	}


	/**
	 * 设置comdyStockInfo
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ComdyStockInfo
	 * 
	 */
	public void setComdyStockInfo(ComdyStockInfo value)
	{
		if (value != null) {
				this.comdyStockInfo = value;
		}else{
				this.comdyStockInfo = new ComdyStockInfo();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetComdyInfoResp)
				length += ByteStream.getObjectSize(comdyStockInfo);  //计算字段comdyStockInfo的长度 size_of(ComdyStockInfo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
