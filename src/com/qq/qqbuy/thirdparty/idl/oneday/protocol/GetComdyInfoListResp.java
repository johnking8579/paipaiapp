 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.oneday.CachestockAO.java

package com.qq.qqbuy.thirdparty.idl.oneday.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *下载回复，主要参数为返回状态
 *
 *@date 2013-08-19 11:07:36
 *
 *@since version:0
*/
public class  GetComdyInfoListResp implements IServiceObject
{
	public long result;
	/**
	 * comdyStockInfoList
	 *
	 * 版本 >= 0
	 */
	 private ComdyStockInfoList comdyStockInfoList = new ComdyStockInfoList();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(comdyStockInfoList);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		comdyStockInfoList = (ComdyStockInfoList) bs.popObject(ComdyStockInfoList.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x48008802L;
	}


	/**
	 * 获取comdyStockInfoList
	 * 
	 * 此字段的版本 >= 0
	 * @return comdyStockInfoList value 类型为:ComdyStockInfoList
	 * 
	 */
	public ComdyStockInfoList getComdyStockInfoList()
	{
		return comdyStockInfoList;
	}


	/**
	 * 设置comdyStockInfoList
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ComdyStockInfoList
	 * 
	 */
	public void setComdyStockInfoList(ComdyStockInfoList value)
	{
		if (value != null) {
				this.comdyStockInfoList = value;
		}else{
				this.comdyStockInfoList = new ComdyStockInfoList();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetComdyInfoListResp)
				length += ByteStream.getObjectSize(comdyStockInfoList);  //计算字段comdyStockInfoList的长度 size_of(ComdyStockInfoList)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
