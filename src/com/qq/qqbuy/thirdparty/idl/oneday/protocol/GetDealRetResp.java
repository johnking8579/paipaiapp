 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.oneday.OadmobileAo.java

package com.qq.qqbuy.thirdparty.idl.oneday.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *抢购回复的数据
 *
 *@date 2013-08-19 11:07:52
 *
 *@since version:0
*/
public class  GetDealRetResp implements IServiceObject
{
	public long result;
	/**
	 * 订单处理状态
	 *
	 * 版本 >= 0
	 */
	 private long dwPos;

	/**
	 * 订单处理结果
	 *
	 * 版本 >= 0
	 */
	 private long dwDealRet;

	/**
	 * 订单ID
	 *
	 * 版本 >= 0
	 */
	 private String sDealId = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushUInt(dwPos);
		bs.pushUInt(dwDealRet);
		bs.pushString(sDealId);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		dwPos = bs.popUInt();
		dwDealRet = bs.popUInt();
		sDealId = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x46108802L;
	}


	/**
	 * 获取订单处理状态
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPos value 类型为:long
	 * 
	 */
	public long getDwPos()
	{
		return dwPos;
	}


	/**
	 * 设置订单处理状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwPos(long value)
	{
		this.dwPos = value;
	}


	/**
	 * 获取订单处理结果
	 * 
	 * 此字段的版本 >= 0
	 * @return dwDealRet value 类型为:long
	 * 
	 */
	public long getDwDealRet()
	{
		return dwDealRet;
	}


	/**
	 * 设置订单处理结果
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwDealRet(long value)
	{
		this.dwDealRet = value;
	}


	/**
	 * 获取订单ID
	 * 
	 * 此字段的版本 >= 0
	 * @return sDealId value 类型为:String
	 * 
	 */
	public String getSDealId()
	{
		return sDealId;
	}


	/**
	 * 设置订单ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSDealId(String value)
	{
		this.sDealId = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetDealRetResp)
				length += 4;  //计算字段dwPos的长度 size_of(uint32_t)
				length += 4;  //计算字段dwDealRet的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sDealId);  //计算字段sDealId的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
