//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.oneday.OadmobileAo.java

package com.qq.qqbuy.thirdparty.idl.oneday.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *bo
 *
 *@date 2013-08-19 11:07:52
 *
 *@since version:20100917
*/
public class DealResult  implements ICanSerializeObject
{
	/**
	 * 抢购结果
	 *
	 * 版本 >= 0
	 */
	 private long dwResult;

	/**
	 * 下单队列id
	 *
	 * 版本 >= 0
	 */
	 private long dwQueueId;

	/**
	 * 拍下不买处罚的开始日期
	 *
	 * 版本 >= 0
	 */
	 private String sBegDate = new String();

	/**
	 * 拍下不买处罚的结束日期
	 *
	 * 版本 >= 0
	 */
	 private String sEndDate = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(dwResult);
		bs.pushUInt(dwQueueId);
		bs.pushString(sBegDate);
		bs.pushString(sEndDate);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		dwResult = bs.popUInt();
		dwQueueId = bs.popUInt();
		sBegDate = bs.popString();
		sEndDate = bs.popString();
		return bs.getReadLength();
	} 


	/**
	 * 获取抢购结果
	 * 
	 * 此字段的版本 >= 0
	 * @return dwResult value 类型为:long
	 * 
	 */
	public long getDwResult()
	{
		return dwResult;
	}


	/**
	 * 设置抢购结果
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwResult(long value)
	{
		this.dwResult = value;
	}


	/**
	 * 获取下单队列id
	 * 
	 * 此字段的版本 >= 0
	 * @return dwQueueId value 类型为:long
	 * 
	 */
	public long getDwQueueId()
	{
		return dwQueueId;
	}


	/**
	 * 设置下单队列id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwQueueId(long value)
	{
		this.dwQueueId = value;
	}


	/**
	 * 获取拍下不买处罚的开始日期
	 * 
	 * 此字段的版本 >= 0
	 * @return sBegDate value 类型为:String
	 * 
	 */
	public String getSBegDate()
	{
		return sBegDate;
	}


	/**
	 * 设置拍下不买处罚的开始日期
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSBegDate(String value)
	{
		this.sBegDate = value;
	}


	/**
	 * 获取拍下不买处罚的结束日期
	 * 
	 * 此字段的版本 >= 0
	 * @return sEndDate value 类型为:String
	 * 
	 */
	public String getSEndDate()
	{
		return sEndDate;
	}


	/**
	 * 设置拍下不买处罚的结束日期
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSEndDate(String value)
	{
		this.sEndDate = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(DealResult)
				length += 4;  //计算字段dwResult的长度 size_of(uint32_t)
				length += 4;  //计算字段dwQueueId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sBegDate);  //计算字段sBegDate的长度 size_of(String)
				length += ByteStream.getObjectSize(sEndDate);  //计算字段sEndDate的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
