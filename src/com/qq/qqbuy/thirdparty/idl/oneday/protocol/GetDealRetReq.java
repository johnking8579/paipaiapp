 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.oneday.OadmobileAo.java

package com.qq.qqbuy.thirdparty.idl.oneday.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *抢购请求参数
 *
 *@date 2013-08-19 11:07:52
 *
 *@since version:0
*/
public class  GetDealRetReq implements IServiceObject
{
	/**
	 * 订单队列索引id
	 *
	 * 版本 >= 0
	 */
	 private long dwIndex;


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(dwIndex);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		dwIndex = bs.popUInt();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x46101802L;
	}


	/**
	 * 获取订单队列索引id
	 * 
	 * 此字段的版本 >= 0
	 * @return dwIndex value 类型为:long
	 * 
	 */
	public long getDwIndex()
	{
		return dwIndex;
	}


	/**
	 * 设置订单队列索引id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwIndex(long value)
	{
		this.dwIndex = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetDealRetReq)
				length += 4;  //计算字段dwIndex的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
