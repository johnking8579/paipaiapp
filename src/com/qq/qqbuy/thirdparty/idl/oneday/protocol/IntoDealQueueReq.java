 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.oneday.OadmobileAo.java

package com.qq.qqbuy.thirdparty.idl.oneday.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *抢购请求参数
 *
 *@date 2013-08-19 11:07:52
 *
 *@since version:0
*/
public class  IntoDealQueueReq implements IServiceObject
{
	/**
	 * 抢购需要的数据
	 *
	 * 版本 >= 0
	 */
	 private MobileDeal oDealInfo = new MobileDeal();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(oDealInfo);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		oDealInfo = (MobileDeal) bs.popObject(MobileDeal.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x46101801L;
	}


	/**
	 * 获取抢购需要的数据
	 * 
	 * 此字段的版本 >= 0
	 * @return oDealInfo value 类型为:MobileDeal
	 * 
	 */
	public MobileDeal getODealInfo()
	{
		return oDealInfo;
	}


	/**
	 * 设置抢购需要的数据
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:MobileDeal
	 * 
	 */
	public void setODealInfo(MobileDeal value)
	{
		if (value != null) {
				this.oDealInfo = value;
		}else{
				this.oDealInfo = new MobileDeal();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(IntoDealQueueReq)
				length += ByteStream.getObjectSize(oDealInfo);  //计算字段oDealInfo的长度 size_of(MobileDeal)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
