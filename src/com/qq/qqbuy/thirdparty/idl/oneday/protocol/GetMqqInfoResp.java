 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.oneday.OadsmsAo.java

package com.qq.qqbuy.thirdparty.idl.oneday.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *上传回复，主要参数为返回状态
 *
 *@date 2013-08-19 11:10:30
 *
 *@since version:0
*/
public class  GetMqqInfoResp implements IServiceObject
{
	public long result;
	/**
	 * 返回的用户订阅状态
	 *
	 * 版本 >= 0
	 */
	 private long dwState;

	/**
	 * 返回的错误码
	 *
	 * 版本 >= 0
	 */
	 private int iErrorCode;


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushUInt(dwState);
		bs.pushInt(iErrorCode);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		dwState = bs.popUInt();
		iErrorCode = bs.popInt();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x46078801L;
	}


	/**
	 * 获取返回的用户订阅状态
	 * 
	 * 此字段的版本 >= 0
	 * @return dwState value 类型为:long
	 * 
	 */
	public long getDwState()
	{
		return dwState;
	}


	/**
	 * 设置返回的用户订阅状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwState(long value)
	{
		this.dwState = value;
	}


	/**
	 * 获取返回的错误码
	 * 
	 * 此字段的版本 >= 0
	 * @return iErrorCode value 类型为:int
	 * 
	 */
	public int getIErrorCode()
	{
		return iErrorCode;
	}


	/**
	 * 设置返回的错误码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setIErrorCode(int value)
	{
		this.iErrorCode = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetMqqInfoResp)
				length += 4;  //计算字段dwState的长度 size_of(uint32_t)
				length += 4;  //计算字段iErrorCode的长度 size_of(int)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
