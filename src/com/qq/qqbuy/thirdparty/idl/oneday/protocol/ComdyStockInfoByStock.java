//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.oneday.CachestockAO.java

package com.qq.qqbuy.thirdparty.idl.oneday.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *bo
 *
 *@date 2013-08-19 11:07:36
 *
 *@since version:20100914
*/
public class ComdyStockInfoByStock  implements ICanSerializeObject
{
	/**
	 * fcomdyid
	 *
	 * 版本 >= 0
	 */
	 private String fcomdyid = new String();

	/**
	 * fstockindex
	 *
	 * 版本 >= 0
	 */
	 private long fstockindex;

	/**
	 * fstocknum
	 *
	 * 版本 >= 0
	 */
	 private long fstocknum;

	/**
	 * fsoldnum
	 *
	 * 版本 >= 0
	 */
	 private long fsoldnum;

	/**
	 * fremainnum
	 *
	 * 版本 >= 0
	 */
	 private long fremainnum;

	/**
	 * fattr
	 *
	 * 版本 >= 0
	 */
	 private String fattr = new String();

	/**
	 * fstockid
	 *
	 * 版本 >= 0
	 */
	 private String fstockid = new String();

	/**
	 * fprice
	 *
	 * 版本 >= 0
	 */
	 private long fprice;

	/**
	 * fdesc
	 *
	 * 版本 >= 0
	 */
	 private String fdesc = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(fcomdyid);
		bs.pushUInt(fstockindex);
		bs.pushUInt(fstocknum);
		bs.pushUInt(fsoldnum);
		bs.pushUInt(fremainnum);
		bs.pushString(fattr);
		bs.pushString(fstockid);
		bs.pushUInt(fprice);
		bs.pushString(fdesc);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		fcomdyid = bs.popString();
		fstockindex = bs.popUInt();
		fstocknum = bs.popUInt();
		fsoldnum = bs.popUInt();
		fremainnum = bs.popUInt();
		fattr = bs.popString();
		fstockid = bs.popString();
		fprice = bs.popUInt();
		fdesc = bs.popString();
		return bs.getReadLength();
	} 


	/**
	 * 获取fcomdyid
	 * 
	 * 此字段的版本 >= 0
	 * @return fcomdyid value 类型为:String
	 * 
	 */
	public String getFcomdyid()
	{
		return fcomdyid;
	}


	/**
	 * 设置fcomdyid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setFcomdyid(String value)
	{
		this.fcomdyid = value;
	}


	/**
	 * 获取fstockindex
	 * 
	 * 此字段的版本 >= 0
	 * @return fstockindex value 类型为:long
	 * 
	 */
	public long getFstockindex()
	{
		return fstockindex;
	}


	/**
	 * 设置fstockindex
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFstockindex(long value)
	{
		this.fstockindex = value;
	}


	/**
	 * 获取fstocknum
	 * 
	 * 此字段的版本 >= 0
	 * @return fstocknum value 类型为:long
	 * 
	 */
	public long getFstocknum()
	{
		return fstocknum;
	}


	/**
	 * 设置fstocknum
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFstocknum(long value)
	{
		this.fstocknum = value;
	}


	/**
	 * 获取fsoldnum
	 * 
	 * 此字段的版本 >= 0
	 * @return fsoldnum value 类型为:long
	 * 
	 */
	public long getFsoldnum()
	{
		return fsoldnum;
	}


	/**
	 * 设置fsoldnum
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFsoldnum(long value)
	{
		this.fsoldnum = value;
	}


	/**
	 * 获取fremainnum
	 * 
	 * 此字段的版本 >= 0
	 * @return fremainnum value 类型为:long
	 * 
	 */
	public long getFremainnum()
	{
		return fremainnum;
	}


	/**
	 * 设置fremainnum
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFremainnum(long value)
	{
		this.fremainnum = value;
	}


	/**
	 * 获取fattr
	 * 
	 * 此字段的版本 >= 0
	 * @return fattr value 类型为:String
	 * 
	 */
	public String getFattr()
	{
		return fattr;
	}


	/**
	 * 设置fattr
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setFattr(String value)
	{
		this.fattr = value;
	}


	/**
	 * 获取fstockid
	 * 
	 * 此字段的版本 >= 0
	 * @return fstockid value 类型为:String
	 * 
	 */
	public String getFstockid()
	{
		return fstockid;
	}


	/**
	 * 设置fstockid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setFstockid(String value)
	{
		this.fstockid = value;
	}


	/**
	 * 获取fprice
	 * 
	 * 此字段的版本 >= 0
	 * @return fprice value 类型为:long
	 * 
	 */
	public long getFprice()
	{
		return fprice;
	}


	/**
	 * 设置fprice
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFprice(long value)
	{
		this.fprice = value;
	}


	/**
	 * 获取fdesc
	 * 
	 * 此字段的版本 >= 0
	 * @return fdesc value 类型为:String
	 * 
	 */
	public String getFdesc()
	{
		return fdesc;
	}


	/**
	 * 设置fdesc
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setFdesc(String value)
	{
		this.fdesc = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(ComdyStockInfoByStock)
				length += ByteStream.getObjectSize(fcomdyid);  //计算字段fcomdyid的长度 size_of(String)
				length += 4;  //计算字段fstockindex的长度 size_of(uint32_t)
				length += 4;  //计算字段fstocknum的长度 size_of(uint32_t)
				length += 4;  //计算字段fsoldnum的长度 size_of(uint32_t)
				length += 4;  //计算字段fremainnum的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(fattr);  //计算字段fattr的长度 size_of(String)
				length += ByteStream.getObjectSize(fstockid);  //计算字段fstockid的长度 size_of(String)
				length += 4;  //计算字段fprice的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(fdesc);  //计算字段fdesc的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
