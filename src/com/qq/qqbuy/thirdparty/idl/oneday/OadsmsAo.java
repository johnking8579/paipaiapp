package com.qq.qqbuy.thirdparty.idl.oneday;

import com.paipai.lang.uint32_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;

public class OadsmsAo
{
  @ApiProtocol(cmdid="0x46071802L", desc="预定特价短信定时提醒")
  class SubscribeItem
  {
    SubscribeItem()
    {
    }

    @ApiProtocol(cmdid="0x46078802L", desc="下载回复，主要参数为返回状态")
    class Resp
    {

      @Field(desc="返回的错误码")
      int iErrorCode;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x46071802L", desc="下载请求，主要参数为文件名，和内容")
    class Req
    {

      @Field(desc="用户qq号")
      uint32_t dwUin;

      @Field(desc="商品ID")
      String sItemId;

      @Field(desc="商品名称")
      String sItemTitle;

      @Field(desc="上架时间")
      uint32_t dwUploadTime;

      @Field(desc="提前提醒时间")
      uint32_t dwAdvanceTime;

      @Field(desc="商品数量")
      uint32_t dwItemNum;

      Req()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x46071801L", desc="用户短信订阅身份查询接口")
  class GetMqqInfo
  {
    GetMqqInfo()
    {
    }

    @ApiProtocol(cmdid="0x46078801L", desc="上传回复，主要参数为返回状态")
    class Resp
    {

      @Field(desc="返回的用户订阅状态")
      uint32_t dwState;

      @Field(desc="返回的错误码")
      int iErrorCode;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x46071801L", desc="上传请求，主要参数为文件名，和内容")
    class Req
    {

      @Field(desc="用户qq号")
      uint32_t dwUin;

      Req()
      {
      }
    }
  }
}

/* Location:           C:\Users\jiaqiangxu\Desktop\lib\lib\com.paipai.mall.api-1.0.0.jar
 * Qualified Name:     oad.ao.oadsms.OadsmsAo
 * JD-Core Version:    0.6.0
 */