package com.qq.qqbuy.thirdparty.idl.oneday;

import com.paipai.lang.uint32_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;
import java.util.Vector;

@HeadApiProtocol(cPlusNamespace="oad::ao::cachestock", needInit=true)
public class CachestockAO
{
  @ApiProtocol(cmdid="0x48001802L", desc="批量查询商品信息")
  class GetComdyInfoList
  {
    GetComdyInfoList()
    {
    }

    @ApiProtocol(cmdid="0x48008802L", desc="下载回复，主要参数为返回状态")
    class Resp
    {

      @Field(desc="comdyStockInfoList")
      CachestockAO.ComdyStockInfoList comdyStockInfoList;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x48001802L", desc="下载请求，主要参数为文件名，和内容")
    class Req
    {

      @Field(desc="comdyIdList")
      CachestockAO.ComdyIdList comdyIdList;

      Req()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x48001801L", desc="查询单个商品信息")
  class GetComdyInfo
  {
    GetComdyInfo()
    {
    }

    @ApiProtocol(cmdid="0x48008801L", desc="返回CComdyStockInfo")
    class Resp
    {

      @Field(desc="comdyStockInfo")
      CachestockAO.ComdyStockInfo comdyStockInfo;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x48001801L", desc="商品id")
    class Req
    {

      @Field(desc="comdyid")
      String comdyid;

      Req()
      {
      }
    }
  }

  @Member(cPlusNamespace="oad::bo::cachestock", desc="bo", isSetClassSize=false, version=20100914)
  class ComdyIdList
  {

    @Field(desc="m_vecComdyId")
    Vector<String> m_vecComdyId;

    ComdyIdList()
    {
    }
  }

  @Member(cPlusNamespace="oad::bo::cachestock", desc="bo", isSetClassSize=false, version=20100914)
  class ComdyStockInfoList
  {

    @Field(desc="m_vecComdyStockInfo")
    Vector<CachestockAO.ComdyStockInfo> m_vecComdyStockInfo;

    ComdyStockInfoList()
    {
    }
  }

  @Member(cPlusNamespace="oad::bo::cachestock", desc="bo", isSetClassSize=false, version=20100914)
  class ComdyStockInfo
  {

    @Field(desc="fcomdyid")
    String fcomdyid;

    @Field(desc="fstocknum")
    uint32_t fstocknum;

    @Field(desc="fsoldnum")
    uint32_t fsoldnum;

    @Field(desc="fremainnum")
    uint32_t fremainnum;

    @Field(desc="fproperty")
    uint32_t fproperty;

    @Field(desc="fcomdyname")
    String fcomdyname;

    @Field(desc="flimitbuynum")
    uint32_t flimitbuynum;

    @Field(desc="fextinfo")
    String fextinfo;

    @Field(desc="fuploadtime")
    uint32_t fuploadtime;

    @Field(desc="fprice")
    uint32_t fprice;

    @Field(desc="fdesc")
    String fdesc;

    @Field(desc="fmtime")
    String fmtime;

    @Field(desc="fselleruin")
    uint32_t fselleruin;

    @Field(desc="fstockindexList")
    Vector<CachestockAO.ComdyStockInfoByStock> fstockindexList;

    ComdyStockInfo()
    {
    }
  }

  @Member(cPlusNamespace="oad::bo::cachestock", desc="bo", isSetClassSize=false, version=20100914)
  class ComdyStockInfoByStock
  {

    @Field(desc="fcomdyid")
    String fcomdyid;

    @Field(desc="fstockindex")
    uint32_t fstockindex;

    @Field(desc="fstocknum")
    uint32_t fstocknum;

    @Field(desc="fsoldnum")
    uint32_t fsoldnum;

    @Field(desc="fremainnum")
    uint32_t fremainnum;

    @Field(desc="fattr")
    String fattr;

    @Field(desc="fstockid")
    String fstockid;

    @Field(desc="fprice")
    uint32_t fprice;

    @Field(desc="fdesc")
    String fdesc;

    ComdyStockInfoByStock()
    {
    }
  }
}

/* Location:           C:\Users\jiaqiangxu\Desktop\lib\lib\com.paipai.mall.api-1.0.0.jar
 * Qualified Name:     oad.ao.cachestock.CachestockAO
 * JD-Core Version:    0.6.0
 */