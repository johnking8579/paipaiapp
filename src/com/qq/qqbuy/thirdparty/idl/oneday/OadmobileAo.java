package com.qq.qqbuy.thirdparty.idl.oneday;

import com.paipai.lang.uint32_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;

@HeadApiProtocol(cPlusNamespace="oad::ao::oadmobile", needInit=true, timers="60")
public class OadmobileAo
{
  @ApiProtocol(cmdid="0x46101802L", desc="特价抢购接口")
  class GetDealRet
  {
    GetDealRet()
    {
    }

    @ApiProtocol(cmdid="0x46108802L", desc="抢购回复的数据")
    class Resp
    {

      @Field(desc="订单处理状态")
      uint32_t dwPos;

      @Field(desc="订单处理结果")
      uint32_t dwDealRet;

      @Field(desc="订单ID")
      String sDealId;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x46101802L", desc="抢购请求参数")
    class Req
    {

      @Field(desc="订单队列索引id")
      uint32_t dwIndex;

      Req()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x46101801L", desc="特价抢购接口")
  class IntoDealQueue
  {
    IntoDealQueue()
    {
    }

    @ApiProtocol(cmdid="0x46108801L", desc="抢购回复的数据")
    class Resp
    {

      @Field(desc="抢购结果")
      OadmobileAo.DealResult oDealResult;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x46101801L", desc="抢购请求参数")
    class Req
    {

      @Field(desc="抢购需要的数据")
      OadmobileAo.MobileDeal oDealInfo;

      Req()
      {
      }
    }
  }

  @Member(cPlusNamespace="oad::bo::oadmobile", desc="bo", isSetClassSize=false, version=20100917)
  class DealResult
  {

    @Field(desc="抢购结果")
    uint32_t dwResult;

    @Field(desc="下单队列id")
    uint32_t dwQueueId;

    @Field(desc="拍下不买处罚的开始日期")
    String sBegDate;

    @Field(desc="拍下不买处罚的结束日期")
    String sEndDate;

    DealResult()
    {
    }
  }

  @Member(cPlusNamespace="oad::bo::oadmobile", desc="bo", isSetClassSize=false, version=20100917)
  class MobileDeal
  {

    @Field(desc="买家qq号")
    uint32_t dwBuyerUin;

    @Field(desc="卖家qq号")
    uint32_t dwSellerUin;

    @Field(desc="买家姓名")
    String sTrueName;

    @Field(desc="手机号")
    String sHandset;

    @Field(desc="座机号")
    String sPhone;

    @Field(desc="邮编")
    String sPostCode;

    @Field(desc="收货地址")
    String sAddress;

    @Field(desc="地区ID")
    uint32_t dwRegion;

    @Field(desc="商品ID")
    String sCommodityId;

    @Field(desc="是否有库存")
    uint32_t dwProperty;

    @Field(desc="库存属性")
    String sStockAttr;

    @Field(desc="购买留言")
    String sNote;

    @Field(desc="购买数量")
    uint32_t dwBuyNum;

    @Field(desc="地址ID")
    uint32_t dwAddressId;

    @Field(desc="商品价格")
    uint32_t dwPrice;

    @Field(desc="机器码")
    String sMKey;

    @Field(desc="买家ip")
    String sClientIp;

    @Field(desc="商品总数量")
    uint32_t dwComdyNum;

    @Field(desc="最大购买数量")
    uint32_t dwMaxBuyNum;

    @Field(desc="上架时间")
    uint32_t dwUploadTime;

    @Field(desc="商品所属的活动类型")
    uint32_t dwActiveType;

    MobileDeal()
    {
    }
  }
}

/* Location:           C:\Users\jiaqiangxu\Desktop\lib\lib\com.paipai.mall.api-1.0.0.jar
 * Qualified Name:     oad.ao.oadmobile.OadmobileAo
 * JD-Core Version:    0.6.0
 */