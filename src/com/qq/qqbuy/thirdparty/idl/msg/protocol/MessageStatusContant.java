 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.msg.protocol;


import java.io.Serializable;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *站内信状态
 *
 *@date 2011-09-28 06:53::23
 *
 *@since version:0
*/
public class MessageStatusContant  implements ICanSerializeObject,Serializable
{
	private static final long serialVersionUID = 973614446354105815L;
	/**
	 * 用于记录IDL接口的版本
	 *
	 * 版本 >= 0
	 */
	 private int version = 20110927; 

	/**
	 * 未读并删除
	 *
	 * 版本 >= 0
	 */
	 private int STATUS_UNREAD_DELETED;

	/**
	 * 已读并删除
	 *
	 * 版本 >= 0
	 */
	 private int STATUS_READ_DELETED;

	/**
	 * 未阅读
	 *
	 * 版本 >= 0
	 */
	 private int STATUS_UNREAD;

	/**
	 * 已阅读
	 *
	 * 版本 >= 0
	 */
	 private int STATUS_READ;

	/**
	 * 备用字段,用于一些紧急临时解决方案
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(version);
		bs.pushInt(STATUS_UNREAD_DELETED);
		bs.pushInt(STATUS_READ_DELETED);
		bs.pushInt(STATUS_UNREAD);
		bs.pushInt(STATUS_READ);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		STATUS_UNREAD_DELETED = bs.popInt();
		STATUS_READ_DELETED = bs.popInt();
		STATUS_UNREAD = bs.popInt();
		STATUS_READ = bs.popInt();
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取用于记录IDL接口的版本
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置用于记录IDL接口的版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取未读并删除
	 * 
	 * 此字段的版本 >= 0
	 * @return STATUS_UNREAD_DELETED value 类型为:int
	 * 
	 */
	public int getSTATUS_UNREAD_DELETED()
	{
		return STATUS_UNREAD_DELETED;
	}


	/**
	 * 设置未读并删除
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setSTATUS_UNREAD_DELETED(int value)
	{
		this.STATUS_UNREAD_DELETED = value;
	}


	/**
	 * 获取已读并删除
	 * 
	 * 此字段的版本 >= 0
	 * @return STATUS_READ_DELETED value 类型为:int
	 * 
	 */
	public int getSTATUS_READ_DELETED()
	{
		return STATUS_READ_DELETED;
	}


	/**
	 * 设置已读并删除
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setSTATUS_READ_DELETED(int value)
	{
		this.STATUS_READ_DELETED = value;
	}


	/**
	 * 获取未阅读
	 * 
	 * 此字段的版本 >= 0
	 * @return STATUS_UNREAD value 类型为:int
	 * 
	 */
	public int getSTATUS_UNREAD()
	{
		return STATUS_UNREAD;
	}


	/**
	 * 设置未阅读
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setSTATUS_UNREAD(int value)
	{
		this.STATUS_UNREAD = value;
	}


	/**
	 * 获取已阅读
	 * 
	 * 此字段的版本 >= 0
	 * @return STATUS_READ value 类型为:int
	 * 
	 */
	public int getSTATUS_READ()
	{
		return STATUS_READ;
	}


	/**
	 * 设置已阅读
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setSTATUS_READ(int value)
	{
		this.STATUS_READ = value;
	}


	/**
	 * 获取备用字段,用于一些紧急临时解决方案
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置备用字段,用于一些紧急临时解决方案
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(MessageStatusContant)
				length += 4;  //计算字段version的长度 size_of(int)
				length += 4;  //计算字段STATUS_UNREAD_DELETED的长度 size_of(int)
				length += 4;  //计算字段STATUS_READ_DELETED的长度 size_of(int)
				length += 4;  //计算字段STATUS_UNREAD的长度 size_of(int)
				length += 4;  //计算字段STATUS_READ的长度 size_of(int)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


}
