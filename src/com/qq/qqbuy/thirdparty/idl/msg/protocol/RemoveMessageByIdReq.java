 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.msg.protocol;


import java.io.Serializable;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *
 *
 *@date 2011-09-28 06:53::23
 *
 *@since version:0
*/
public class  RemoveMessageByIdReq implements IServiceObject,Serializable
{
	private static final long serialVersionUID = 973614446354105815L;
	/**
	 * 消息ID
	 *
	 * 版本 >= 0
	 */
	 private int id;

	/**
	 * 接收者QQ号
	 *
	 * 版本 >= 0
	 */
	 private long toUin;

	/**
	 * 删除状态,-11:未读直接删除,-12:已读直接删除
	 *
	 * 版本 >= 0
	 */
	 private int status;

	/**
	 * 备用字段,用于一些紧急临时解决方案
	 *
	 * 版本 >= 0
	 */
	 private String inReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushInt(id);
		bs.pushUInt(toUin);
		bs.pushInt(status);
		bs.pushString(inReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		id = bs.popInt();
		toUin = bs.popUInt();
		status = bs.popInt();
		inReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80091803L;
	}


	/**
	 * 获取消息ID
	 * 
	 * 此字段的版本 >= 0
	 * @return id value 类型为:int
	 * 
	 */
	public int getId()
	{
		return id;
	}


	/**
	 * 设置消息ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setId(int value)
	{
		this.id = value;
	}


	/**
	 * 获取接收者QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return toUin value 类型为:long
	 * 
	 */
	public long getToUin()
	{
		return toUin;
	}


	/**
	 * 设置接收者QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setToUin(long value)
	{
		this.toUin = value;
	}


	/**
	 * 获取删除状态,-11:未读直接删除,-12:已读直接删除
	 * 
	 * 此字段的版本 >= 0
	 * @return status value 类型为:int
	 * 
	 */
	public int getStatus()
	{
		return status;
	}


	/**
	 * 设置删除状态,-11:未读直接删除,-12:已读直接删除
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setStatus(int value)
	{
		this.status = value;
	}


	/**
	 * 获取备用字段,用于一些紧急临时解决方案
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserved value 类型为:String
	 * 
	 */
	public String getInReserved()
	{
		return inReserved;
	}


	/**
	 * 设置备用字段,用于一些紧急临时解决方案
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserved(String value)
	{
		this.inReserved = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(RemoveMessageByIdReq)
				length += 4;  //计算字段id的长度 size_of(int)
				length += 4;  //计算字段toUin的长度 size_of(long)
				length += 4;  //计算字段status的长度 size_of(int)
				length += ByteStream.getObjectSize(inReserved);  //计算字段inReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

}
