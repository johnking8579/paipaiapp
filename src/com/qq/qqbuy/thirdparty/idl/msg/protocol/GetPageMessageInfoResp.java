package com.qq.qqbuy.thirdparty.idl.msg.protocol;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

/**
 *
 *
 *@date 2011-09-28 06:53::23
 *
 *@since version:0
*/
public class  GetPageMessageInfoResp implements IServiceObject,Serializable
{
	private static final long serialVersionUID = 973614446354105815L;
	public long result;
	/**
	 * 错误码
	 *
	 * 版本 >= 0
	 */
	 private int errCode;

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 消息总记录数
	 *
	 * 版本 >= 0
	 */
	 private int messageCount;

	/**
	 * 消息列表
	 *
	 * 版本 >= 0
	 */
	 private List<InternalMessage> messageList = new ArrayList<InternalMessage>();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushInt(errCode);
		bs.pushString(errMsg);
		bs.pushInt(messageCount);
		bs.pushObject(messageList);
		return bs.getWrittenLength();
	}
	
	@SuppressWarnings("unchecked")
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		errCode = bs.popInt();
		errMsg = bs.popString();
		messageCount = bs.popInt();
		messageList = (List<InternalMessage>)bs.popList(ArrayList.class,InternalMessage.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80098802L;
	}


	/**
	 * 获取错误码
	 * 
	 * 此字段的版本 >= 0
	 * @return errCode value 类型为:int
	 * 
	 */
	public int getErrCode()
	{
		return errCode;
	}


	/**
	 * 设置错误码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setErrCode(int value)
	{
		this.errCode = value;
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	/**
	 * 获取消息总记录数
	 * 
	 * 此字段的版本 >= 0
	 * @return messageCount value 类型为:int
	 * 
	 */
	public int getMessageCount()
	{
		return messageCount;
	}


	/**
	 * 设置消息总记录数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setMessageCount(int value)
	{
		this.messageCount = value;
	}


	/**
	 * 获取消息列表
	 * 
	 * 此字段的版本 >= 0
	 * @return messageList value 类型为:List<InternalMessage>
	 * 
	 */
	public List<InternalMessage> getMessageList()
	{
		return messageList;
	}


	/**
	 * 设置消息列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:List<InternalMessage>
	 * 
	 */
	public void setMessageList(List<InternalMessage> value)
	{
		if (value != null) {
				this.messageList = value;
		}else{
				this.messageList = new ArrayList<InternalMessage>();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetPageMessageInfoResp)
				length += 4;  //计算字段errCode的长度 size_of(int)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += 4;  //计算字段messageCount的长度 size_of(int)
				length += ByteStream.getObjectSize(messageList);  //计算字段messageList的长度 size_of(List)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
