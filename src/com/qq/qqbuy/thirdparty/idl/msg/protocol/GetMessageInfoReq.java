 
package com.qq.qqbuy.thirdparty.idl.msg.protocol;


import java.io.Serializable;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *
 *
 *@date 2011-09-28 06:53::23
 *
 *@since version:0
*/
public class  GetMessageInfoReq implements IServiceObject,Serializable
{
	private static final long serialVersionUID = 973614446354105815L;
	/**
	 * 消息ID
	 *
	 * 版本 >= 0
	 */
	 private int id;

	/**
	 * 接收者QQ号
	 *
	 * 版本 >= 0
	 */
	 private long toUin;


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushInt(id);
		bs.pushUInt(toUin);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		id = bs.popInt();
		toUin = bs.popUInt();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80091801L;
	}


	/**
	 * 获取消息ID
	 * 
	 * 此字段的版本 >= 0
	 * @return id value 类型为:int
	 * 
	 */
	public int getId()
	{
		return id;
	}


	/**
	 * 设置消息ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setId(int value)
	{
		this.id = value;
	}


	/**
	 * 获取接收者QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return toUin value 类型为:long
	 * 
	 */
	public long getToUin()
	{
		return toUin;
	}


	/**
	 * 设置接收者QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setToUin(long value)
	{
		this.toUin = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetMessageInfoReq)
				length += 4;  //计算字段id的长度 size_of(int)
				length += 4;  //计算字段toUin的长度 size_of(long)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
