package com.qq.qqbuy.thirdparty.idl.msg;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.qq.qqbuy.common.constant.BizConstant;

import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.msg.protocol.GetMessageCountReq;
import com.qq.qqbuy.thirdparty.idl.msg.protocol.GetMessageCountResp;
import com.qq.qqbuy.thirdparty.idl.msg.protocol.GetMessageInfoReq;
import com.qq.qqbuy.thirdparty.idl.msg.protocol.GetMessageInfoResp;
import com.qq.qqbuy.thirdparty.idl.msg.protocol.GetPageMessageInfoReq;
import com.qq.qqbuy.thirdparty.idl.msg.protocol.GetPageMessageInfoResp;
import com.qq.qqbuy.thirdparty.idl.msg.protocol.InternalMessage;
import com.qq.qqbuy.thirdparty.idl.msg.protocol.MarkAsReadReq;
import com.qq.qqbuy.thirdparty.idl.msg.protocol.MarkAsReadResp;
import com.qq.qqbuy.thirdparty.idl.msg.protocol.RemoveMessageByIdReq;
import com.qq.qqbuy.thirdparty.idl.msg.protocol.RemoveMessageByIdResp;

/**
 * 站内信
 * @author jiaqiangxu
 *
 */
public class MessasgeClient extends SupportIDLBaseClient {
	private static int MSG_PORT = 9117;

//	public static GetMessageCountResp getMessageCount(long uin) {
//        long start = System.currentTimeMillis();
//        GetMessageCountReq req = new GetMessageCountReq();
//        req.setToUin(uin);
//
//        GetMessageCountResp resp = new GetMessageCountResp();
//        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
//		int ret = invokeWgIDL4Env(req, resp, MSG_PORT, stub);
//        try {      	
//			Util.decode(resp, BizConstant.OPENAPI_CHARSET);	
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        long timeCost = System.currentTimeMillis() - start;
//        return ret == SUCCESS ? resp : null;
//    }
//	
//
//	
//
//	
//	public static GetPageMessageInfoResp getPageMessageInfo(long uin,int pn,int ps,int status) {
//        long start = System.currentTimeMillis();
//        GetPageMessageInfoReq req = new GetPageMessageInfoReq();
//        req.setToUin(uin);
//        req.setPageNo(pn);
//        req.setPageSize(ps);
//        req.setStatus(status);
//        GetPageMessageInfoResp resp = new GetPageMessageInfoResp();
//        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
//		int ret = invokeWgIDL4Env(req, resp, MSG_PORT, stub);
//        try {      	
//			Util.decode(resp, BizConstant.OPENAPI_CHARSET);	
//			if(resp.getMessageList()!=null){
//		  	 for(InternalMessage internalMessage:resp.getMessageList()){
//				 Util.decode(internalMessage, BizConstant.OPENAPI_CHARSET);	
//			  }
//			}
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        return ret == SUCCESS ? resp : null;
//    }
//	
//	public static GetMessageInfoResp getMessageInfo(long uin,int id) {
//        long start = System.currentTimeMillis();
//        GetMessageInfoReq req = new GetMessageInfoReq();
//        req.setToUin(uin);
//        req.setId(id);
//        GetMessageInfoResp resp = new GetMessageInfoResp();
//        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
//		int ret = invokeWgIDL4Env(req, resp, MSG_PORT, stub);
//        try {      	
//			Util.decode(resp, BizConstant.OPENAPI_CHARSET);	
//			Util.decode(resp.getMessage(), BizConstant.OPENAPI_CHARSET);	
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        return ret == SUCCESS ? resp : null;
//    }
//	
//	public static MarkAsReadResp markAsRead(long uin,int id) {
//        long start = System.currentTimeMillis();
//        MarkAsReadReq req = new MarkAsReadReq();
//        req.setToUin(uin);
//        req.setId(id);
//        MarkAsReadResp resp = new MarkAsReadResp();
//        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
//		int ret = invokeWgIDL4Env(req, resp, MSG_PORT, stub);
//        try {      	
//			Util.decode(resp, BizConstant.OPENAPI_CHARSET);	
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//        return ret == SUCCESS ? resp : null;
//    }
//	public static RemoveMessageByIdResp removeMessageById(long uin,int id,int status) {
//        long start = System.currentTimeMillis();
//        RemoveMessageByIdReq req = new RemoveMessageByIdReq();
//        req.setToUin(uin);
//        req.setStatus(status);
//        req.setId(id);
//        RemoveMessageByIdResp resp = new RemoveMessageByIdResp();
//        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
//		int ret = invokeWgIDL4Env(req, resp, MSG_PORT, stub);
//        try {      	
//			Util.decode(resp, BizConstant.OPENAPI_CHARSET);	
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        return ret == SUCCESS ? resp : null;
//    }
	
}
