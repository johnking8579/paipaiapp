package com.qq.qqbuy.thirdparty.idl.deal;

import java.net.InetSocketAddress;
import java.util.Vector;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.paipai.lang.uint64_t;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.exception.ExternalInterfaceException;
import com.qq.qqbuy.thirdparty.idl.IDLResult;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.AddCodDealReq;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.AddCodDealResp;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.CloseDealReq;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.CloseDealResp;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.ConfirmRecvReq;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.ConfirmRecvResp;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.GetAllStateDealCountByUinReq;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.GetAllStateDealCountByUinResp;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.smsConfirmReq;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.smsConfirmResp;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.pccod.OrderConfirmV2Req;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.pccod.OrderConfirmV2Resp;


public class DealClient extends SupportIDLBaseClient
{
	protected final int IDL_ORDERDEAL_FROM_QGO = 4; //wap来源

	protected static final Long DEAL_PROMOTE_ORDER_FROM_APP = 11L ;//app来源

	protected final String QGO_RESERVE = "mobile";//openapi区分方式
	public static final String 
		HTML5 = "scene_on_mobile_h5",
		QGO_SOURCE = "mobileLife",//移动电商来源，免登录态
		QGO_APPORDER = "appOrder",//立即购买-orderfrom（场景券）
		QGO_APPTAKCHEAPORDER = "appTakCheapOrder";//立即购买[拍便宜]-orderfrom（场景券）
	
	
	/**
	 * 手拍货到付款下单
	 * 
	 * @param uin
	 * @return
	 */
	public IDLResult<AddCodDealResp> addCodDeal(AddCodDealReq req) throws BusinessException {
		AddCodDealResp resp = new AddCodDealResp();
		IAsynWebStub stub = null;
	    if (EnvManager.isIdc())
        {
        	InetSocketAddress address = getAddressFromConfig("qgo.sys.deal");
        	stub = WebStubFactory.getWebStub4PaiPai();
        	stub.setIpAndPort(address.getHostName(),address.getPort());
        } else
        {	
        	stub = WebStubFactory.getWebStub4PaiPai();
        	stub.setIpAndPort("172.23.0.58", 9104);
        }
		int ret = invokePaiPaiIDL(req, resp, stub);
		
		IDLResult<AddCodDealResp> rsp = new IDLResult<AddCodDealResp>(resp, ret, resp.getResult());
		return rsp;
	}

	/**
	 * 手拍货到付款短信确认
	 */
	public IDLResult<smsConfirmResp> smsConfirmReq(smsConfirmReq req) throws BusinessException {
		smsConfirmResp resp = new smsConfirmResp();
		IAsynWebStub stub = null;
		if (EnvManager.isIdc())
        {
        	InetSocketAddress address = getAddressFromConfig("qgo.sys.deal");
        	stub = WebStubFactory.getWebStub4PaiPai();
        	stub.setIpAndPort(address.getHostName(),address.getPort());
        } else
        {	
        	stub = WebStubFactory.getWebStub4PaiPai();
        	stub.setIpAndPort("172.23.0.58", 9104);
        }
		int ret = invokePaiPaiIDL(req, resp, stub);
		IDLResult<smsConfirmResp> rsp = new IDLResult<smsConfirmResp>(resp, ret, resp.getResult());
		return rsp;
	}
	
	/**
	 * 关闭订单
	 * 
	 * @param uin
	 * @return
	 */
	public IDLResult<CloseDealResp> closeDeal(CloseDealReq req, long uin, String skey) throws BusinessException {
		CloseDealResp resp = new CloseDealResp();
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiSkeySign(uin, skey, CHARSET_GBK);
		int ret = invokePaiPaiIDL(req, resp, stub);
		IDLResult<CloseDealResp> rsp = new IDLResult<CloseDealResp>(resp, ret, resp.getResult());
		return rsp;
	}
//	
//	/**
//	 * paipai一口价下单
//	 * @param req
//	 * @return
//	 * @throws BusinessException
//	 */
//    public IDLResult<OrderFixupResp> order2PP(OrderFixupReq req) throws BusinessException 
//    {
//    	OrderFixupResp resp = new OrderFixupResp();
//    	req.setSource(QGO_SOURCE);
//		if (EnvManager.isGamma()){
//			req.getOReq().setOrderFrom(DEAL_PROMOTE_ORDER_FROM_APP);
//		}else{
//			req.getOReq().setOrderFrom(IDL_ORDERDEAL_FROM_QGO);
//		}
//		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();  //此处不需要设置特定的ip
//		stub.setOperator(req.getOReq().getUin());
//		int ret = invokePaiPaiIDL(req, resp, stub);
//	    
//		IDLResult<OrderFixupResp> rsp = new IDLResult<OrderFixupResp>(resp, ret, resp.getResult());
//		return rsp;
//    }
    
	/**
	 * 一口价下单/微信支付
	 * 
	 * @param req
	 * @return
	 * @throws BusinessException
	 */
	public IDLResult<OrderConfirmV2Resp> orderWg(OrderConfirmV2Req req, String lskey) throws BusinessException {
		OrderConfirmV2Resp resp = new OrderConfirmV2Resp();
//		req.setSource(QGO_SOURCE);//
		req.setSource(QGO_APPORDER);//
		
//		if (EnvManager.isGamma()){//陈喆上线后全部用DEAL_PROMOTE_ORDER_FROM_APP【其实这里陈喆根本没有用到！！！2015.5.6刘本龙】
			req.getReqData().setOrderFrom(DEAL_PROMOTE_ORDER_FROM_APP);//场景券支持
//		}else{
//			req.getReqData().setOrderFrom(IDL_ORDERDEAL_FROM_QGO);
//		}
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK(); // 此处不需要设置特定的ip
		stub.setOperator(req.getReqData().getUin());
		stub.setSkey(lskey.getBytes());
		stub.setRouteKey(req.getReqData().getUin());
		if(EnvManager.isGamma()){
			stub.setIpAndPort("10.130.0.237",53101);
		}
		int ret = invokePaiPaiIDL(req, resp, stub);

		IDLResult<OrderConfirmV2Resp> rsp = new IDLResult<OrderConfirmV2Resp>(resp, ret, resp.result);
		return rsp;
	}
    
	
	/**
	 * 获取订单不同状态的数量
	 * @param operatorUin 
	 * @param dealType
	 * @return
	 */
    public GetAllStateDealCountByUinResp getDealStateNum(long operatorUin, int dealType)    {
        GetAllStateDealCountByUinReq req = new GetAllStateDealCountByUinReq();
        GetAllStateDealCountByUinResp resp = new GetAllStateDealCountByUinResp();
        req.setUin(operatorUin);
        req.setDealType(dealType);
        
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
        invoke(stub, req, resp);
        if(resp.getResult() != 0)
        	throw new ExternalInterfaceException(0, resp.getResult(), resp.getErrMsg());
        return resp;
    }
    
	
	/**
	 * 确认收货
	 * 重复确认收货时:返回值{"result":14,"ErrMsg":"optuin or deal is invalid! uin:795019790, state:7"}
	 * @param uin
	 * @param dealId
	 * @param tradeIds
	 * @param decryptToken 财付通提供的确认收货token(解密后的), 微信支付时不传或传drawId
	 * @param drawId 打款给卖家用的打款id
	 * @return
	 */
	public void confirmRecvDeal(long uin, String dealId, String[] tradeIds, 
								String decryptToken, String drawId) {
		ConfirmRecvReq req = new ConfirmRecvReq();
		ConfirmRecvResp resp = new ConfirmRecvResp();
		req.setSource(HTML5);	
		req.setDealId(dealId);
		if(tradeIds != null)	{
			Vector<uint64_t> v = new Vector<uint64_t>();
			for (String t : tradeIds) {
				v.add(new uint64_t(t));
			}
			req.setTradeIdList(v);
		}
		if(decryptToken != null){
			req.setToken(decryptToken);
		}
		if(drawId != null)	{
			req.setDrawId(drawId);
		}

		AsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		stub.setOperator(uin);
		stub.setUin(uin);
		
		invoke(stub, req, resp);
		if(resp.getResult() != 0)	{
			if(resp.getResult()==14){
				throw new ExternalInterfaceException(0, resp.getResult(), "请求数据中有异常数据");
			}else {
				throw new ExternalInterfaceException(0, resp.getResult(), resp.getErrMsg());
			}
			
		}
	}

    
}
