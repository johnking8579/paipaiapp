//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.deal.idl.DealService.java

package com.qq.qqbuy.thirdparty.idl.deal.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *订单查询条件
 *
 *@date 2012-12-07 10:51:37
 *
 *@since version:0
*/
public class DealCondition  implements ICanSerializeObject
{
	/**
	 * 订单ID
	 *
	 * 版本 >= 0
	 */
	 private String dealCode = new String();

	/**
	 * 卖家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long sellerUin;

	/**
	 * 买家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long buyerUin;

	/**
	 * 订单列表: 多个dealCode，以分号分隔
	 *
	 * 版本 >= 0
	 */
	 private String multiDealCode = new String();

	/**
	 * 订单起始生成时间,格式yyyy-MM-dd HH:mm:ssdealCreatetTime >= dealCreateTimeStart
	 *
	 * 版本 >= 0
	 */
	 private String dealCreateTimeStart = new String();

	/**
	 * 订单结束生成时间,格式yyyy-MM-dd HH:mm:ssdealCreateTime <= dealCreateTimeEnd
	 *
	 * 版本 >= 0
	 */
	 private String dealCreateTimeEnd = new String();

	/**
	 * 拍拍订单状态
	 *
	 * 版本 >= 0
	 */
	 private int state;

	/**
	 * 货到付款子状态
	 *
	 * 版本 >= 0
	 */
	 private int substate;

	/**
	 * 商品ID
	 *
	 * 版本 >= 0
	 */
	 private String itemCode = new String();

	/**
	 * 买家姓名
	 *
	 * 版本 >= 0
	 */
	 private String recvAddrName = new String();

	/**
	 * 买家手机号
	 *
	 * 版本 >= 0
	 */
	 private String recvAddrMobile = new String();

	/**
	 * 关闭原因
	 *
	 * 版本 >= 0
	 */
	 private int closeReason;

	/**
	 * 拒收原因
	 *
	 * 版本 >= 0
	 */
	 private int refuseReason;

	/**
	 * 标志位, >=0时才作为查询条件
	 *
	 * 版本 >= 0
	 */
	 private int flags = -1;

	/**
	 * 排序字段， 默认为： Fdeal_create_time desc
	 *
	 * 版本 >= 0
	 */
	 private String orderStr = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushString(dealCode);
		bs.pushUInt(sellerUin);
		bs.pushUInt(buyerUin);
		bs.pushString(multiDealCode);
		bs.pushString(dealCreateTimeStart);
		bs.pushString(dealCreateTimeEnd);
		bs.pushInt(state);
		bs.pushInt(substate);
		bs.pushString(itemCode);
		bs.pushString(recvAddrName);
		bs.pushString(recvAddrMobile);
		bs.pushInt(closeReason);
		bs.pushInt(refuseReason);
		bs.pushInt(flags);
		bs.pushString(orderStr);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		dealCode = bs.popString();
		sellerUin = bs.popUInt();
		buyerUin = bs.popUInt();
		multiDealCode = bs.popString();
		dealCreateTimeStart = bs.popString();
		dealCreateTimeEnd = bs.popString();
		state = bs.popInt();
		substate = bs.popInt();
		itemCode = bs.popString();
		recvAddrName = bs.popString();
		recvAddrMobile = bs.popString();
		closeReason = bs.popInt();
		refuseReason = bs.popInt();
		flags = bs.popInt();
		orderStr = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取订单ID
	 * 
	 * 此字段的版本 >= 0
	 * @return dealCode value 类型为:String
	 * 
	 */
	public String getDealCode()
	{
		return dealCode;
	}


	/**
	 * 设置订单ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealCode(String value)
	{
		this.dealCode = value;
	}


	/**
	 * 获取卖家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return sellerUin;
	}


	/**
	 * 设置卖家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.sellerUin = value;
	}


	/**
	 * 获取买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return buyerUin;
	}


	/**
	 * 设置买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.buyerUin = value;
	}


	/**
	 * 获取订单列表: 多个dealCode，以分号分隔
	 * 
	 * 此字段的版本 >= 0
	 * @return multiDealCode value 类型为:String
	 * 
	 */
	public String getMultiDealCode()
	{
		return multiDealCode;
	}


	/**
	 * 设置订单列表: 多个dealCode，以分号分隔
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMultiDealCode(String value)
	{
		this.multiDealCode = value;
	}


	/**
	 * 获取订单起始生成时间,格式yyyy-MM-dd HH:mm:ssdealCreatetTime >= dealCreateTimeStart
	 * 
	 * 此字段的版本 >= 0
	 * @return dealCreateTimeStart value 类型为:String
	 * 
	 */
	public String getDealCreateTimeStart()
	{
		return dealCreateTimeStart;
	}


	/**
	 * 设置订单起始生成时间,格式yyyy-MM-dd HH:mm:ssdealCreatetTime >= dealCreateTimeStart
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealCreateTimeStart(String value)
	{
		this.dealCreateTimeStart = value;
	}


	/**
	 * 获取订单结束生成时间,格式yyyy-MM-dd HH:mm:ssdealCreateTime <= dealCreateTimeEnd
	 * 
	 * 此字段的版本 >= 0
	 * @return dealCreateTimeEnd value 类型为:String
	 * 
	 */
	public String getDealCreateTimeEnd()
	{
		return dealCreateTimeEnd;
	}


	/**
	 * 设置订单结束生成时间,格式yyyy-MM-dd HH:mm:ssdealCreateTime <= dealCreateTimeEnd
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealCreateTimeEnd(String value)
	{
		this.dealCreateTimeEnd = value;
	}


	/**
	 * 获取拍拍订单状态
	 * 
	 * 此字段的版本 >= 0
	 * @return state value 类型为:int
	 * 
	 */
	public int getState()
	{
		return state;
	}


	/**
	 * 设置拍拍订单状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setState(int value)
	{
		this.state = value;
	}


	/**
	 * 获取货到付款子状态
	 * 
	 * 此字段的版本 >= 0
	 * @return substate value 类型为:int
	 * 
	 */
	public int getSubstate()
	{
		return substate;
	}


	/**
	 * 设置货到付款子状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setSubstate(int value)
	{
		this.substate = value;
	}


	/**
	 * 获取商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return itemCode value 类型为:String
	 * 
	 */
	public String getItemCode()
	{
		return itemCode;
	}


	/**
	 * 设置商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemCode(String value)
	{
		this.itemCode = value;
	}


	/**
	 * 获取买家姓名
	 * 
	 * 此字段的版本 >= 0
	 * @return recvAddrName value 类型为:String
	 * 
	 */
	public String getRecvAddrName()
	{
		return recvAddrName;
	}


	/**
	 * 设置买家姓名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvAddrName(String value)
	{
		this.recvAddrName = value;
	}


	/**
	 * 获取买家手机号
	 * 
	 * 此字段的版本 >= 0
	 * @return recvAddrMobile value 类型为:String
	 * 
	 */
	public String getRecvAddrMobile()
	{
		return recvAddrMobile;
	}


	/**
	 * 设置买家手机号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvAddrMobile(String value)
	{
		this.recvAddrMobile = value;
	}


	/**
	 * 获取关闭原因
	 * 
	 * 此字段的版本 >= 0
	 * @return closeReason value 类型为:int
	 * 
	 */
	public int getCloseReason()
	{
		return closeReason;
	}


	/**
	 * 设置关闭原因
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setCloseReason(int value)
	{
		this.closeReason = value;
	}


	/**
	 * 获取拒收原因
	 * 
	 * 此字段的版本 >= 0
	 * @return refuseReason value 类型为:int
	 * 
	 */
	public int getRefuseReason()
	{
		return refuseReason;
	}


	/**
	 * 设置拒收原因
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setRefuseReason(int value)
	{
		this.refuseReason = value;
	}


	/**
	 * 获取标志位, >=0时才作为查询条件
	 * 
	 * 此字段的版本 >= 0
	 * @return flags value 类型为:int
	 * 
	 */
	public int getFlags()
	{
		return flags;
	}


	/**
	 * 设置标志位, >=0时才作为查询条件
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setFlags(int value)
	{
		this.flags = value;
	}


	/**
	 * 获取排序字段， 默认为： Fdeal_create_time desc
	 * 
	 * 此字段的版本 >= 0
	 * @return orderStr value 类型为:String
	 * 
	 */
	public String getOrderStr()
	{
		return orderStr;
	}


	/**
	 * 设置排序字段， 默认为： Fdeal_create_time desc
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOrderStr(String value)
	{
		this.orderStr = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(DealCondition)
				length += ByteStream.getObjectSize(dealCode);  //计算字段dealCode的长度 size_of(String)
				length += 4;  //计算字段sellerUin的长度 size_of(long)
				length += 4;  //计算字段buyerUin的长度 size_of(long)
				length += ByteStream.getObjectSize(multiDealCode);  //计算字段multiDealCode的长度 size_of(String)
				length += ByteStream.getObjectSize(dealCreateTimeStart);  //计算字段dealCreateTimeStart的长度 size_of(String)
				length += ByteStream.getObjectSize(dealCreateTimeEnd);  //计算字段dealCreateTimeEnd的长度 size_of(String)
				length += 4;  //计算字段state的长度 size_of(int)
				length += 4;  //计算字段substate的长度 size_of(int)
				length += ByteStream.getObjectSize(itemCode);  //计算字段itemCode的长度 size_of(String)
				length += ByteStream.getObjectSize(recvAddrName);  //计算字段recvAddrName的长度 size_of(String)
				length += ByteStream.getObjectSize(recvAddrMobile);  //计算字段recvAddrMobile的长度 size_of(String)
				length += 4;  //计算字段closeReason的长度 size_of(int)
				length += 4;  //计算字段refuseReason的长度 size_of(int)
				length += 4;  //计算字段flags的长度 size_of(int)
				length += ByteStream.getObjectSize(orderStr);  //计算字段orderStr的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
