//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.deal.idl.DealService.java

package com.qq.qqbuy.thirdparty.idl.deal.protocol;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *子订单详情
 *
 *@date 2012-12-07 10:51:37
 *
 *@since version:20110816
*/
public class TradeInfo  implements ICanSerializeObject
{
	/**
	 * 版本控制的字段
	 *
	 * 版本 >= 0
	 */
	 private int version = 20110816;

	/**
	 * 订单id
	 *
	 * 版本 >= 0
	 */
	 private long dealId;

	/**
	 * 子订单编号 
	 *
	 * 版本 >= 0
	 */
	 private long tradeId;

	/**
	 * 商品ID 
	 *
	 * 版本 >= 0
	 */
	 private String itemCode = new String();

	/**
	 * 支付方式
	 *
	 * 版本 >= 0
	 */
	 private int payType;

	/**
	 * 大订单状态 
	 *
	 * 版本 >= 0
	 */
	 private int state;

	/**
	 * 大订单子状态 
	 *
	 * 版本 >= 0
	 */
	 private int substate;

	/**
	 * 商品名称
	 *
	 * 版本 >= 0
	 */
	 private String itemName = new String();

	/**
	 * 商品价格
	 *
	 * 版本 >= 0
	 */
	 private int itemPrice;

	/**
	 * 购买数量
	 *
	 * 版本 >= 0
	 */
	 private int dealItemCount;

	/**
	 * 商品图片主图
	 *
	 * 版本 >= 0
	 */
	 private String itemLogo = new String();

	/**
	 * 商品价格调整优惠
	 *
	 * 版本 >= 0
	 */
	 private int itemAdjustPrice;

	/**
	 * 折扣（红包）金额
	 *
	 * 版本 >= 0
	 */
	 private int discountFee;

	/**
	 * 订单详情链接
	 *
	 * 版本 >= 0
	 */
	 private String dealDetailLink = new String();

	/**
	 * req保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(version);
		bs.pushUInt(dealId);
		bs.pushUInt(tradeId);
		bs.pushString(itemCode);
		bs.pushInt(payType);
		bs.pushInt(state);
		bs.pushInt(substate);
		bs.pushString(itemName);
		bs.pushInt(itemPrice);
		bs.pushInt(dealItemCount);
		bs.pushString(itemLogo);
		bs.pushInt(itemAdjustPrice);
		bs.pushInt(discountFee);
		bs.pushString(dealDetailLink);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		dealId = bs.popUInt();
		tradeId = bs.popUInt();
		itemCode = bs.popString();
		payType = bs.popInt();
		state = bs.popInt();
		substate = bs.popInt();
		itemName = bs.popString();
		itemPrice = bs.popInt();
		dealItemCount = bs.popInt();
		itemLogo = bs.popString();
		itemAdjustPrice = bs.popInt();
		discountFee = bs.popInt();
		dealDetailLink = bs.popString();
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取订单id
	 * 
	 * 此字段的版本 >= 0
	 * @return dealId value 类型为:long
	 * 
	 */
	public long getDealId()
	{
		return dealId;
	}


	/**
	 * 设置订单id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealId(long value)
	{
		this.dealId = value;
	}


	/**
	 * 获取子订单编号 
	 * 
	 * 此字段的版本 >= 0
	 * @return tradeId value 类型为:long
	 * 
	 */
	public long getTradeId()
	{
		return tradeId;
	}


	/**
	 * 设置子订单编号 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTradeId(long value)
	{
		this.tradeId = value;
	}


	/**
	 * 获取商品ID 
	 * 
	 * 此字段的版本 >= 0
	 * @return itemCode value 类型为:String
	 * 
	 */
	public String getItemCode()
	{
		return itemCode;
	}


	/**
	 * 设置商品ID 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemCode(String value)
	{
		this.itemCode = value;
	}


	/**
	 * 获取支付方式
	 * 
	 * 此字段的版本 >= 0
	 * @return payType value 类型为:int
	 * 
	 */
	public int getPayType()
	{
		return payType;
	}


	/**
	 * 设置支付方式
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPayType(int value)
	{
		this.payType = value;
	}


	/**
	 * 获取大订单状态 
	 * 
	 * 此字段的版本 >= 0
	 * @return state value 类型为:int
	 * 
	 */
	public int getState()
	{
		return state;
	}


	/**
	 * 设置大订单状态 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setState(int value)
	{
		this.state = value;
	}


	/**
	 * 获取大订单子状态 
	 * 
	 * 此字段的版本 >= 0
	 * @return substate value 类型为:int
	 * 
	 */
	public int getSubstate()
	{
		return substate;
	}


	/**
	 * 设置大订单子状态 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setSubstate(int value)
	{
		this.substate = value;
	}


	/**
	 * 获取商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @return itemName value 类型为:String
	 * 
	 */
	public String getItemName()
	{
		return itemName;
	}


	/**
	 * 设置商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemName(String value)
	{
		this.itemName = value;
	}


	/**
	 * 获取商品价格
	 * 
	 * 此字段的版本 >= 0
	 * @return itemPrice value 类型为:int
	 * 
	 */
	public int getItemPrice()
	{
		return itemPrice;
	}


	/**
	 * 设置商品价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setItemPrice(int value)
	{
		this.itemPrice = value;
	}


	/**
	 * 获取购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @return dealItemCount value 类型为:int
	 * 
	 */
	public int getDealItemCount()
	{
		return dealItemCount;
	}


	/**
	 * 设置购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setDealItemCount(int value)
	{
		this.dealItemCount = value;
	}


	/**
	 * 获取商品图片主图
	 * 
	 * 此字段的版本 >= 0
	 * @return itemLogo value 类型为:String
	 * 
	 */
	public String getItemLogo()
	{
		return itemLogo;
	}


	/**
	 * 设置商品图片主图
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemLogo(String value)
	{
		this.itemLogo = value;
	}


	/**
	 * 获取商品价格调整优惠
	 * 
	 * 此字段的版本 >= 0
	 * @return itemAdjustPrice value 类型为:int
	 * 
	 */
	public int getItemAdjustPrice()
	{
		return itemAdjustPrice;
	}


	/**
	 * 设置商品价格调整优惠
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setItemAdjustPrice(int value)
	{
		this.itemAdjustPrice = value;
	}


	/**
	 * 获取折扣（红包）金额
	 * 
	 * 此字段的版本 >= 0
	 * @return discountFee value 类型为:int
	 * 
	 */
	public int getDiscountFee()
	{
		return discountFee;
	}


	/**
	 * 设置折扣（红包）金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setDiscountFee(int value)
	{
		this.discountFee = value;
	}


	/**
	 * 获取订单详情链接
	 * 
	 * 此字段的版本 >= 0
	 * @return dealDetailLink value 类型为:String
	 * 
	 */
	public String getDealDetailLink()
	{
		return dealDetailLink;
	}


	/**
	 * 设置订单详情链接
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealDetailLink(String value)
	{
		this.dealDetailLink = value;
	}


	/**
	 * 获取req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(TradeInfo)
				length += 4;  //计算字段version的长度 size_of(int)
				length += 4;  //计算字段dealId的长度 size_of(long)
				length += 4;  //计算字段tradeId的长度 size_of(long)
				length += ByteStream.getObjectSize(itemCode);  //计算字段itemCode的长度 size_of(String)
				length += 4;  //计算字段payType的长度 size_of(int)
				length += 4;  //计算字段state的长度 size_of(int)
				length += 4;  //计算字段substate的长度 size_of(int)
				length += ByteStream.getObjectSize(itemName);  //计算字段itemName的长度 size_of(String)
				length += 4;  //计算字段itemPrice的长度 size_of(int)
				length += 4;  //计算字段dealItemCount的长度 size_of(int)
				length += ByteStream.getObjectSize(itemLogo);  //计算字段itemLogo的长度 size_of(String)
				length += 4;  //计算字段itemAdjustPrice的长度 size_of(int)
				length += 4;  //计算字段discountFee的长度 size_of(int)
				length += ByteStream.getObjectSize(dealDetailLink);  //计算字段dealDetailLink的长度 size_of(String)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
