 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.deal.idl.DealService.java

package com.qq.qqbuy.thirdparty.idl.deal.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2012-12-07 10:51:37
 *
 *@since version:1
*/
public class  OrderDealReq implements IServiceObject
{
	/**
	 * 买家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long buyerUin;

	/**
	 * 订单相关信息
	 *
	 * 版本 >= 0
	 */
	 private DealReqInfo dealInfo = new DealReqInfo();

	/**
	 * 下单的单个商品信息
	 *
	 * 版本 >= 0
	 */
	 private ItemReqInfo itemInfo = new ItemReqInfo();

	/**
	 * 邮递相关信息
	 *
	 * 版本 >= 0
	 */
	 private MailReqInfo mailInfo = new MailReqInfo();

	/**
	 * 调用者
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * req保留字段
	 *
	 * 版本 >= 0
	 */
	 private String inReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(buyerUin);
		bs.pushObject(dealInfo);
		bs.pushObject(itemInfo);
		bs.pushObject(mailInfo);
		bs.pushString(source);
		bs.pushString(inReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		buyerUin = bs.popUInt();
		dealInfo = (DealReqInfo) bs.popObject(DealReqInfo.class);
		itemInfo = (ItemReqInfo) bs.popObject(ItemReqInfo.class);
		mailInfo = (MailReqInfo) bs.popObject(MailReqInfo.class);
		source = bs.popString();
		inReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80081803L;
	}


	/**
	 * 获取买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return buyerUin;
	}


	/**
	 * 设置买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.buyerUin = value;
	}


	/**
	 * 获取订单相关信息
	 * 
	 * 此字段的版本 >= 0
	 * @return dealInfo value 类型为:DealReqInfo
	 * 
	 */
	public DealReqInfo getDealInfo()
	{
		return dealInfo;
	}


	/**
	 * 设置订单相关信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:DealReqInfo
	 * 
	 */
	public void setDealInfo(DealReqInfo value)
	{
		if (value != null) {
				this.dealInfo = value;
		}else{
				this.dealInfo = new DealReqInfo();
		}
	}


	/**
	 * 获取下单的单个商品信息
	 * 
	 * 此字段的版本 >= 0
	 * @return itemInfo value 类型为:ItemReqInfo
	 * 
	 */
	public ItemReqInfo getItemInfo()
	{
		return itemInfo;
	}


	/**
	 * 设置下单的单个商品信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ItemReqInfo
	 * 
	 */
	public void setItemInfo(ItemReqInfo value)
	{
		if (value != null) {
				this.itemInfo = value;
		}else{
				this.itemInfo = new ItemReqInfo();
		}
	}


	/**
	 * 获取邮递相关信息
	 * 
	 * 此字段的版本 >= 0
	 * @return mailInfo value 类型为:MailReqInfo
	 * 
	 */
	public MailReqInfo getMailInfo()
	{
		return mailInfo;
	}


	/**
	 * 设置邮递相关信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:MailReqInfo
	 * 
	 */
	public void setMailInfo(MailReqInfo value)
	{
		if (value != null) {
				this.mailInfo = value;
		}else{
				this.mailInfo = new MailReqInfo();
		}
	}


	/**
	 * 获取调用者
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置调用者
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserved value 类型为:String
	 * 
	 */
	public String getInReserved()
	{
		return inReserved;
	}


	/**
	 * 设置req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserved(String value)
	{
		this.inReserved = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(OrderDealReq)
				length += 4;  //计算字段buyerUin的长度 size_of(long)
				length += ByteStream.getObjectSize(dealInfo);  //计算字段dealInfo的长度 size_of(DealReqInfo)
				length += ByteStream.getObjectSize(itemInfo);  //计算字段itemInfo的长度 size_of(ItemReqInfo)
				length += ByteStream.getObjectSize(mailInfo);  //计算字段mailInfo的长度 size_of(MailReqInfo)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserved);  //计算字段inReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
