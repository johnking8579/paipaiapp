 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.deal.idl.DealService.java

package com.qq.qqbuy.thirdparty.idl.deal.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Map;
import java.util.HashMap;

/**
 *
 *
 *@date 2012-12-07 10:51:37
 *
 *@since version:0
*/
public class  SetDealFlagReq implements IServiceObject
{
	/**
	 * 订单编号
	 *
	 * 版本 >= 0
	 */
	 private String dealCode = new String();

	/**
	 * 买家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long buyerUin;

	/**
	 * 设置的方式。0代表直接设置，则flag必须填；1代表采用FlagMap来多个设置
	 *
	 * 版本 >= 0
	 */
	 private int setType = 0;

	/**
	 * 直接设置整个订单属性
	 *
	 * 版本 >= 0
	 */
	 private int flag;

	/**
	 * 对订单属性的单个属性位进行设置的Map，key为flag的Position，取值[1-32]；value为flag的值，取值[0-1]
	 *
	 * 版本 >= 0
	 */
	 private Map<Integer,Integer> flagMap = new HashMap<Integer,Integer>();

	/**
	 * req保留字段
	 *
	 * 版本 >= 0
	 */
	 private String inReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(dealCode);
		bs.pushUInt(buyerUin);
		bs.pushInt(setType);
		bs.pushInt(flag);
		bs.pushObject(flagMap);
		bs.pushString(inReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		dealCode = bs.popString();
		buyerUin = bs.popUInt();
		setType = bs.popInt();
		flag = bs.popInt();
		flagMap = (Map<Integer,Integer>)bs.popMap(Integer.class,Integer.class);
		inReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80081809L;
	}


	/**
	 * 获取订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @return dealCode value 类型为:String
	 * 
	 */
	public String getDealCode()
	{
		return dealCode;
	}


	/**
	 * 设置订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealCode(String value)
	{
		this.dealCode = value;
	}


	/**
	 * 获取买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return buyerUin;
	}


	/**
	 * 设置买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.buyerUin = value;
	}


	/**
	 * 获取设置的方式。0代表直接设置，则flag必须填；1代表采用FlagMap来多个设置
	 * 
	 * 此字段的版本 >= 0
	 * @return setType value 类型为:int
	 * 
	 */
	public int getSetType()
	{
		return setType;
	}


	/**
	 * 设置设置的方式。0代表直接设置，则flag必须填；1代表采用FlagMap来多个设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setSetType(int value)
	{
		this.setType = value;
	}


	/**
	 * 获取直接设置整个订单属性
	 * 
	 * 此字段的版本 >= 0
	 * @return flag value 类型为:int
	 * 
	 */
	public int getFlag()
	{
		return flag;
	}


	/**
	 * 设置直接设置整个订单属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setFlag(int value)
	{
		this.flag = value;
	}


	/**
	 * 获取对订单属性的单个属性位进行设置的Map，key为flag的Position，取值[1-32]；value为flag的值，取值[0-1]
	 * 
	 * 此字段的版本 >= 0
	 * @return flagMap value 类型为:Map<Integer,Integer>
	 * 
	 */
	public Map<Integer,Integer> getFlagMap()
	{
		return flagMap;
	}


	/**
	 * 设置对订单属性的单个属性位进行设置的Map，key为flag的Position，取值[1-32]；value为flag的值，取值[0-1]
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<Integer,Integer>
	 * 
	 */
	public void setFlagMap(Map<Integer,Integer> value)
	{
		if (value != null) {
				this.flagMap = value;
		}else{
				this.flagMap = new HashMap<Integer,Integer>();
		}
	}


	/**
	 * 获取req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserved value 类型为:String
	 * 
	 */
	public String getInReserved()
	{
		return inReserved;
	}


	/**
	 * 设置req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserved(String value)
	{
		this.inReserved = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(SetDealFlagReq)
				length += ByteStream.getObjectSize(dealCode);  //计算字段dealCode的长度 size_of(String)
				length += 4;  //计算字段buyerUin的长度 size_of(long)
				length += 4;  //计算字段setType的长度 size_of(int)
				length += 4;  //计算字段flag的长度 size_of(int)
				length += ByteStream.getObjectSize(flagMap);  //计算字段flagMap的长度 size_of(Map)
				length += ByteStream.getObjectSize(inReserved);  //计算字段inReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
