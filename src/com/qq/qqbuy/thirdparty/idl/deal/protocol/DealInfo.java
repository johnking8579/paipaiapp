//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.deal.idl.DealService.java

package com.qq.qqbuy.thirdparty.idl.deal.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 *订单详情
 *
 *@date 2012-12-07 10:51:37
 *
 *@since version:20110816
*/
public class DealInfo  implements ICanSerializeObject
{
	/**
	 * 版本控制的字段
	 *
	 * 版本 >= 0
	 */
	 private int version = 20110816;

	/**
	 * 订单id
	 *
	 * 版本 >= 0
	 */
	 private String dealCode = new String();

	/**
	 * 订单版本
	 *
	 * 版本 >= 0
	 */
	 private int dealVersion;

	/**
	 * 订单生成时间
	 *
	 * 版本 >= 0
	 */
	 private String dealCreateTime = new String();

	/**
	 * 订单结束时间
	 *
	 * 版本 >= 0
	 */
	 private String dealEndTime = new String();

	/**
	 * 订单最后修改时间
	 *
	 * 版本 >= 0
	 */
	 private String lastModifyTime = new String();

	/**
	 * 订单描述
	 *
	 * 版本 >= 0
	 */
	 private String dealDesc = new String();

	/**
	 * 订单在paipai的状态
	 *
	 * 版本 >= 0
	 */
	 private int state;

	/**
	 * 订单在手机paipai货到付款流程的状态
	 *
	 * 版本 >= 0
	 */
	 private int substate;

	/**
	 * 订单关闭原因
	 *
	 * 版本 >= 0
	 */
	 private int closeReason;

	/**
	 * 订单拒收原因  
	 *
	 * 版本 >= 0
	 */
	 private int refuseReason;

	/**
	 * 短信确认开始时间
	 *
	 * 版本 >= 0
	 */
	 private String smBeginTime = new String();

	/**
	 * 支付方式
	 *
	 * 版本 >= 0
	 */
	 private int payType;

	/**
	 * 卖家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long sellerUin;

	/**
	 * 买家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long buyerUin;

	/**
	 * 买家QQ名称
	 *
	 * 版本 >= 0
	 */
	 private String buyerNickName = new String();

	/**
	 * 买家留言
	 *
	 * 版本 >= 0
	 */
	 private String buyerNote = new String();

	/**
	 * 订单来源信息
	 *
	 * 版本 >= 0
	 */
	 private String referer = new String();

	/**
	 * 订单商品名称列表
	 *
	 * 版本 >= 0
	 */
	 private String itemTitleList = new String();

	/**
	 * 商品编码
	 *
	 * 版本 >= 0
	 */
	 private String itemCode = new String();

	/**
	 * 商品库存属性
	 *
	 * 版本 >= 0
	 */
	 private String stockAttr = new String();

	/**
	 * 购买数量
	 *
	 * 版本 >= 0
	 */
	 private int buyAmount;

	/**
	 * 订单现金支付金额
	 *
	 * 版本 >= 0
	 */
	 private int payFeeCash;

	/**
	 * 订单财富券支付金额
	 *
	 * 版本 >= 0
	 */
	 private int payFeeTicket;

	/**
	 * 订单积分支付金额
	 *
	 * 版本 >= 0
	 */
	 private int payFeeScore;

	/**
	 * 订单运费支付金额
	 *
	 * 版本 >= 0
	 */
	 private int payFeeShipping;

	/**
	 * 货到付款手续费
	 *
	 * 版本 >= 0
	 */
	 private int payFeeCommission;

	/**
	 * 订单优惠金额
	 *
	 * 版本 >= 0
	 */
	 private int couponFee;

	/**
	 * 总支付费用
	 *
	 * 版本 >= 0
	 */
	 private int payFeeTotal;

	/**
	 * 向买家显示的订单总金额
	 *
	 * 版本 >= 0
	 */
	 private int buyerFeeTotal;

	/**
	 * 获得积分额
	 *
	 * 版本 >= 0
	 */
	 private int recvScore;

	/**
	 * 财付通支付ID
	 *
	 * 版本 >= 0
	 */
	 private String dealCftPayId = new String();

	/**
	 * 支付单编号
	 *
	 * 版本 >= 0
	 */
	 private long payId;

	/**
	 * 是否有发票
	 *
	 * 版本 >= 0
	 */
	 private int hasInnovice;

	/**
	 * 发票抬头
	 *
	 * 版本 >= 0
	 */
	 private String dealInnoviceTitle = new String();

	/**
	 * 发票内容
	 *
	 * 版本 >= 0
	 */
	 private String dealInnoviceContent = new String();

	/**
	 * 订单标志位
	 *
	 * 版本 >= 0
	 */
	 private int flags;

	/**
	 * 收货地址编号
	 *
	 * 版本 >= 0
	 */
	 private long recvAddrId;

	/**
	 * 收货人姓名
	 *
	 * 版本 >= 0
	 */
	 private String recvAddrName = new String();

	/**
	 * 收货人地址
	 *
	 * 版本 >= 0
	 */
	 private String recvAddrDetail = new String();

	/**
	 * 收货人邮编
	 *
	 * 版本 >= 0
	 */
	 private String recvAddrPostcode = new String();

	/**
	 * 收货人电话
	 *
	 * 版本 >= 0
	 */
	 private String recvAddrPhone = new String();

	/**
	 * 收货人手机号
	 *
	 * 版本 >= 0
	 */
	 private String recvAddrMobile = new String();

	/**
	 * 物流号
	 *
	 * 版本 >= 0
	 */
	 private long wuliuId;

	/**
	 * 邮递类型
	 *
	 * 版本 >= 0
	 */
	 private int mailType;

	/**
	 * 支付完成时间
	 *
	 * 版本 >= 0
	 */
	 private String payTime = new String();

	/**
	 * 支付返回时间
	 *
	 * 版本 >= 0
	 */
	 private String payReturnTime = new String();

	/**
	 * 打款完成时间
	 *
	 * 版本 >= 0
	 */
	 private String recvFeeTime = new String();

	/**
	 * 打款返回时间
	 *
	 * 版本 >= 0
	 */
	 private String recvFeeReturnTime = new String();

	/**
	 * 子订单列表
	 *
	 * 版本 >= 0
	 */
	 private List<TradeInfo> tradeList = new ArrayList<TradeInfo>();

	/**
	 * req保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(version);
		bs.pushString(dealCode);
		bs.pushInt(dealVersion);
		bs.pushString(dealCreateTime);
		bs.pushString(dealEndTime);
		bs.pushString(lastModifyTime);
		bs.pushString(dealDesc);
		bs.pushInt(state);
		bs.pushInt(substate);
		bs.pushInt(closeReason);
		bs.pushInt(refuseReason);
		bs.pushString(smBeginTime);
		bs.pushInt(payType);
		bs.pushUInt(sellerUin);
		bs.pushUInt(buyerUin);
		bs.pushString(buyerNickName);
		bs.pushString(buyerNote);
		bs.pushString(referer);
		bs.pushString(itemTitleList);
		bs.pushString(itemCode);
		bs.pushString(stockAttr);
		bs.pushInt(buyAmount);
		bs.pushInt(payFeeCash);
		bs.pushInt(payFeeTicket);
		bs.pushInt(payFeeScore);
		bs.pushInt(payFeeShipping);
		bs.pushInt(payFeeCommission);
		bs.pushInt(couponFee);
		bs.pushInt(payFeeTotal);
		bs.pushInt(buyerFeeTotal);
		bs.pushInt(recvScore);
		bs.pushString(dealCftPayId);
		bs.pushUInt(payId);
		bs.pushInt(hasInnovice);
		bs.pushString(dealInnoviceTitle);
		bs.pushString(dealInnoviceContent);
		bs.pushInt(flags);
		bs.pushUInt(recvAddrId);
		bs.pushString(recvAddrName);
		bs.pushString(recvAddrDetail);
		bs.pushString(recvAddrPostcode);
		bs.pushString(recvAddrPhone);
		bs.pushString(recvAddrMobile);
		bs.pushUInt(wuliuId);
		bs.pushInt(mailType);
		bs.pushString(payTime);
		bs.pushString(payReturnTime);
		bs.pushString(recvFeeTime);
		bs.pushString(recvFeeReturnTime);
		bs.pushObject(tradeList);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		dealCode = bs.popString();
		dealVersion = bs.popInt();
		dealCreateTime = bs.popString();
		dealEndTime = bs.popString();
		lastModifyTime = bs.popString();
		dealDesc = bs.popString();
		state = bs.popInt();
		substate = bs.popInt();
		closeReason = bs.popInt();
		refuseReason = bs.popInt();
		smBeginTime = bs.popString();
		payType = bs.popInt();
		sellerUin = bs.popUInt();
		buyerUin = bs.popUInt();
		buyerNickName = bs.popString();
		buyerNote = bs.popString();
		referer = bs.popString();
		itemTitleList = bs.popString();
		itemCode = bs.popString();
		stockAttr = bs.popString();
		buyAmount = bs.popInt();
		payFeeCash = bs.popInt();
		payFeeTicket = bs.popInt();
		payFeeScore = bs.popInt();
		payFeeShipping = bs.popInt();
		payFeeCommission = bs.popInt();
		couponFee = bs.popInt();
		payFeeTotal = bs.popInt();
		buyerFeeTotal = bs.popInt();
		recvScore = bs.popInt();
		dealCftPayId = bs.popString();
		payId = bs.popUInt();
		hasInnovice = bs.popInt();
		dealInnoviceTitle = bs.popString();
		dealInnoviceContent = bs.popString();
		flags = bs.popInt();
		recvAddrId = bs.popUInt();
		recvAddrName = bs.popString();
		recvAddrDetail = bs.popString();
		recvAddrPostcode = bs.popString();
		recvAddrPhone = bs.popString();
		recvAddrMobile = bs.popString();
		wuliuId = bs.popUInt();
		mailType = bs.popInt();
		payTime = bs.popString();
		payReturnTime = bs.popString();
		recvFeeTime = bs.popString();
		recvFeeReturnTime = bs.popString();
		tradeList = (List<TradeInfo>)bs.popList(ArrayList.class,TradeInfo.class);
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取订单id
	 * 
	 * 此字段的版本 >= 0
	 * @return dealCode value 类型为:String
	 * 
	 */
	public String getDealCode()
	{
		return dealCode;
	}


	/**
	 * 设置订单id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealCode(String value)
	{
		this.dealCode = value;
	}


	/**
	 * 获取订单版本
	 * 
	 * 此字段的版本 >= 0
	 * @return dealVersion value 类型为:int
	 * 
	 */
	public int getDealVersion()
	{
		return dealVersion;
	}


	/**
	 * 设置订单版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setDealVersion(int value)
	{
		this.dealVersion = value;
	}


	/**
	 * 获取订单生成时间
	 * 
	 * 此字段的版本 >= 0
	 * @return dealCreateTime value 类型为:String
	 * 
	 */
	public String getDealCreateTime()
	{
		return dealCreateTime;
	}


	/**
	 * 设置订单生成时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealCreateTime(String value)
	{
		this.dealCreateTime = value;
	}


	/**
	 * 获取订单结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return dealEndTime value 类型为:String
	 * 
	 */
	public String getDealEndTime()
	{
		return dealEndTime;
	}


	/**
	 * 设置订单结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealEndTime(String value)
	{
		this.dealEndTime = value;
	}


	/**
	 * 获取订单最后修改时间
	 * 
	 * 此字段的版本 >= 0
	 * @return lastModifyTime value 类型为:String
	 * 
	 */
	public String getLastModifyTime()
	{
		return lastModifyTime;
	}


	/**
	 * 设置订单最后修改时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setLastModifyTime(String value)
	{
		this.lastModifyTime = value;
	}


	/**
	 * 获取订单描述
	 * 
	 * 此字段的版本 >= 0
	 * @return dealDesc value 类型为:String
	 * 
	 */
	public String getDealDesc()
	{
		return dealDesc;
	}


	/**
	 * 设置订单描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealDesc(String value)
	{
		this.dealDesc = value;
	}


	/**
	 * 获取订单在paipai的状态
	 * 
	 * 此字段的版本 >= 0
	 * @return state value 类型为:int
	 * 
	 */
	public int getState()
	{
		return state;
	}


	/**
	 * 设置订单在paipai的状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setState(int value)
	{
		this.state = value;
	}


	/**
	 * 获取订单在手机paipai货到付款流程的状态
	 * 
	 * 此字段的版本 >= 0
	 * @return substate value 类型为:int
	 * 
	 */
	public int getSubstate()
	{
		return substate;
	}


	/**
	 * 设置订单在手机paipai货到付款流程的状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setSubstate(int value)
	{
		this.substate = value;
	}


	/**
	 * 获取订单关闭原因
	 * 
	 * 此字段的版本 >= 0
	 * @return closeReason value 类型为:int
	 * 
	 */
	public int getCloseReason()
	{
		return closeReason;
	}


	/**
	 * 设置订单关闭原因
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setCloseReason(int value)
	{
		this.closeReason = value;
	}


	/**
	 * 获取订单拒收原因  
	 * 
	 * 此字段的版本 >= 0
	 * @return refuseReason value 类型为:int
	 * 
	 */
	public int getRefuseReason()
	{
		return refuseReason;
	}


	/**
	 * 设置订单拒收原因  
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setRefuseReason(int value)
	{
		this.refuseReason = value;
	}


	/**
	 * 获取短信确认开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @return smBeginTime value 类型为:String
	 * 
	 */
	public String getSmBeginTime()
	{
		return smBeginTime;
	}


	/**
	 * 设置短信确认开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSmBeginTime(String value)
	{
		this.smBeginTime = value;
	}


	/**
	 * 获取支付方式
	 * 
	 * 此字段的版本 >= 0
	 * @return payType value 类型为:int
	 * 
	 */
	public int getPayType()
	{
		return payType;
	}


	/**
	 * 设置支付方式
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPayType(int value)
	{
		this.payType = value;
	}


	/**
	 * 获取卖家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return sellerUin;
	}


	/**
	 * 设置卖家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.sellerUin = value;
	}


	/**
	 * 获取买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return buyerUin;
	}


	/**
	 * 设置买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.buyerUin = value;
	}


	/**
	 * 获取买家QQ名称
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerNickName value 类型为:String
	 * 
	 */
	public String getBuyerNickName()
	{
		return buyerNickName;
	}


	/**
	 * 设置买家QQ名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBuyerNickName(String value)
	{
		this.buyerNickName = value;
	}


	/**
	 * 获取买家留言
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerNote value 类型为:String
	 * 
	 */
	public String getBuyerNote()
	{
		return buyerNote;
	}


	/**
	 * 设置买家留言
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBuyerNote(String value)
	{
		this.buyerNote = value;
	}


	/**
	 * 获取订单来源信息
	 * 
	 * 此字段的版本 >= 0
	 * @return referer value 类型为:String
	 * 
	 */
	public String getReferer()
	{
		return referer;
	}


	/**
	 * 设置订单来源信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReferer(String value)
	{
		this.referer = value;
	}


	/**
	 * 获取订单商品名称列表
	 * 
	 * 此字段的版本 >= 0
	 * @return itemTitleList value 类型为:String
	 * 
	 */
	public String getItemTitleList()
	{
		return itemTitleList;
	}


	/**
	 * 设置订单商品名称列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemTitleList(String value)
	{
		this.itemTitleList = value;
	}


	/**
	 * 获取商品编码
	 * 
	 * 此字段的版本 >= 0
	 * @return itemCode value 类型为:String
	 * 
	 */
	public String getItemCode()
	{
		return itemCode;
	}


	/**
	 * 设置商品编码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemCode(String value)
	{
		this.itemCode = value;
	}


	/**
	 * 获取商品库存属性
	 * 
	 * 此字段的版本 >= 0
	 * @return stockAttr value 类型为:String
	 * 
	 */
	public String getStockAttr()
	{
		return stockAttr;
	}


	/**
	 * 设置商品库存属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStockAttr(String value)
	{
		this.stockAttr = value;
	}


	/**
	 * 获取购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @return buyAmount value 类型为:int
	 * 
	 */
	public int getBuyAmount()
	{
		return buyAmount;
	}


	/**
	 * 设置购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setBuyAmount(int value)
	{
		this.buyAmount = value;
	}


	/**
	 * 获取订单现金支付金额
	 * 
	 * 此字段的版本 >= 0
	 * @return payFeeCash value 类型为:int
	 * 
	 */
	public int getPayFeeCash()
	{
		return payFeeCash;
	}


	/**
	 * 设置订单现金支付金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPayFeeCash(int value)
	{
		this.payFeeCash = value;
	}


	/**
	 * 获取订单财富券支付金额
	 * 
	 * 此字段的版本 >= 0
	 * @return payFeeTicket value 类型为:int
	 * 
	 */
	public int getPayFeeTicket()
	{
		return payFeeTicket;
	}


	/**
	 * 设置订单财富券支付金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPayFeeTicket(int value)
	{
		this.payFeeTicket = value;
	}


	/**
	 * 获取订单积分支付金额
	 * 
	 * 此字段的版本 >= 0
	 * @return payFeeScore value 类型为:int
	 * 
	 */
	public int getPayFeeScore()
	{
		return payFeeScore;
	}


	/**
	 * 设置订单积分支付金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPayFeeScore(int value)
	{
		this.payFeeScore = value;
	}


	/**
	 * 获取订单运费支付金额
	 * 
	 * 此字段的版本 >= 0
	 * @return payFeeShipping value 类型为:int
	 * 
	 */
	public int getPayFeeShipping()
	{
		return payFeeShipping;
	}


	/**
	 * 设置订单运费支付金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPayFeeShipping(int value)
	{
		this.payFeeShipping = value;
	}


	/**
	 * 获取货到付款手续费
	 * 
	 * 此字段的版本 >= 0
	 * @return payFeeCommission value 类型为:int
	 * 
	 */
	public int getPayFeeCommission()
	{
		return payFeeCommission;
	}


	/**
	 * 设置货到付款手续费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPayFeeCommission(int value)
	{
		this.payFeeCommission = value;
	}


	/**
	 * 获取订单优惠金额
	 * 
	 * 此字段的版本 >= 0
	 * @return couponFee value 类型为:int
	 * 
	 */
	public int getCouponFee()
	{
		return couponFee;
	}


	/**
	 * 设置订单优惠金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setCouponFee(int value)
	{
		this.couponFee = value;
	}


	/**
	 * 获取总支付费用
	 * 
	 * 此字段的版本 >= 0
	 * @return payFeeTotal value 类型为:int
	 * 
	 */
	public int getPayFeeTotal()
	{
		return payFeeTotal;
	}


	/**
	 * 设置总支付费用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPayFeeTotal(int value)
	{
		this.payFeeTotal = value;
	}


	/**
	 * 获取向买家显示的订单总金额
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerFeeTotal value 类型为:int
	 * 
	 */
	public int getBuyerFeeTotal()
	{
		return buyerFeeTotal;
	}


	/**
	 * 设置向买家显示的订单总金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setBuyerFeeTotal(int value)
	{
		this.buyerFeeTotal = value;
	}


	/**
	 * 获取获得积分额
	 * 
	 * 此字段的版本 >= 0
	 * @return recvScore value 类型为:int
	 * 
	 */
	public int getRecvScore()
	{
		return recvScore;
	}


	/**
	 * 设置获得积分额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setRecvScore(int value)
	{
		this.recvScore = value;
	}


	/**
	 * 获取财付通支付ID
	 * 
	 * 此字段的版本 >= 0
	 * @return dealCftPayId value 类型为:String
	 * 
	 */
	public String getDealCftPayId()
	{
		return dealCftPayId;
	}


	/**
	 * 设置财付通支付ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealCftPayId(String value)
	{
		this.dealCftPayId = value;
	}


	/**
	 * 获取支付单编号
	 * 
	 * 此字段的版本 >= 0
	 * @return payId value 类型为:long
	 * 
	 */
	public long getPayId()
	{
		return payId;
	}


	/**
	 * 设置支付单编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayId(long value)
	{
		this.payId = value;
	}


	/**
	 * 获取是否有发票
	 * 
	 * 此字段的版本 >= 0
	 * @return hasInnovice value 类型为:int
	 * 
	 */
	public int getHasInnovice()
	{
		return hasInnovice;
	}


	/**
	 * 设置是否有发票
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setHasInnovice(int value)
	{
		this.hasInnovice = value;
	}


	/**
	 * 获取发票抬头
	 * 
	 * 此字段的版本 >= 0
	 * @return dealInnoviceTitle value 类型为:String
	 * 
	 */
	public String getDealInnoviceTitle()
	{
		return dealInnoviceTitle;
	}


	/**
	 * 设置发票抬头
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealInnoviceTitle(String value)
	{
		this.dealInnoviceTitle = value;
	}


	/**
	 * 获取发票内容
	 * 
	 * 此字段的版本 >= 0
	 * @return dealInnoviceContent value 类型为:String
	 * 
	 */
	public String getDealInnoviceContent()
	{
		return dealInnoviceContent;
	}


	/**
	 * 设置发票内容
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealInnoviceContent(String value)
	{
		this.dealInnoviceContent = value;
	}


	/**
	 * 获取订单标志位
	 * 
	 * 此字段的版本 >= 0
	 * @return flags value 类型为:int
	 * 
	 */
	public int getFlags()
	{
		return flags;
	}


	/**
	 * 设置订单标志位
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setFlags(int value)
	{
		this.flags = value;
	}


	/**
	 * 获取收货地址编号
	 * 
	 * 此字段的版本 >= 0
	 * @return recvAddrId value 类型为:long
	 * 
	 */
	public long getRecvAddrId()
	{
		return recvAddrId;
	}


	/**
	 * 设置收货地址编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRecvAddrId(long value)
	{
		this.recvAddrId = value;
	}


	/**
	 * 获取收货人姓名
	 * 
	 * 此字段的版本 >= 0
	 * @return recvAddrName value 类型为:String
	 * 
	 */
	public String getRecvAddrName()
	{
		return recvAddrName;
	}


	/**
	 * 设置收货人姓名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvAddrName(String value)
	{
		this.recvAddrName = value;
	}


	/**
	 * 获取收货人地址
	 * 
	 * 此字段的版本 >= 0
	 * @return recvAddrDetail value 类型为:String
	 * 
	 */
	public String getRecvAddrDetail()
	{
		return recvAddrDetail;
	}


	/**
	 * 设置收货人地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvAddrDetail(String value)
	{
		this.recvAddrDetail = value;
	}


	/**
	 * 获取收货人邮编
	 * 
	 * 此字段的版本 >= 0
	 * @return recvAddrPostcode value 类型为:String
	 * 
	 */
	public String getRecvAddrPostcode()
	{
		return recvAddrPostcode;
	}


	/**
	 * 设置收货人邮编
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvAddrPostcode(String value)
	{
		this.recvAddrPostcode = value;
	}


	/**
	 * 获取收货人电话
	 * 
	 * 此字段的版本 >= 0
	 * @return recvAddrPhone value 类型为:String
	 * 
	 */
	public String getRecvAddrPhone()
	{
		return recvAddrPhone;
	}


	/**
	 * 设置收货人电话
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvAddrPhone(String value)
	{
		this.recvAddrPhone = value;
	}


	/**
	 * 获取收货人手机号
	 * 
	 * 此字段的版本 >= 0
	 * @return recvAddrMobile value 类型为:String
	 * 
	 */
	public String getRecvAddrMobile()
	{
		return recvAddrMobile;
	}


	/**
	 * 设置收货人手机号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvAddrMobile(String value)
	{
		this.recvAddrMobile = value;
	}


	/**
	 * 获取物流号
	 * 
	 * 此字段的版本 >= 0
	 * @return wuliuId value 类型为:long
	 * 
	 */
	public long getWuliuId()
	{
		return wuliuId;
	}


	/**
	 * 设置物流号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWuliuId(long value)
	{
		this.wuliuId = value;
	}


	/**
	 * 获取邮递类型
	 * 
	 * 此字段的版本 >= 0
	 * @return mailType value 类型为:int
	 * 
	 */
	public int getMailType()
	{
		return mailType;
	}


	/**
	 * 设置邮递类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setMailType(int value)
	{
		this.mailType = value;
	}


	/**
	 * 获取支付完成时间
	 * 
	 * 此字段的版本 >= 0
	 * @return payTime value 类型为:String
	 * 
	 */
	public String getPayTime()
	{
		return payTime;
	}


	/**
	 * 设置支付完成时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPayTime(String value)
	{
		this.payTime = value;
	}


	/**
	 * 获取支付返回时间
	 * 
	 * 此字段的版本 >= 0
	 * @return payReturnTime value 类型为:String
	 * 
	 */
	public String getPayReturnTime()
	{
		return payReturnTime;
	}


	/**
	 * 设置支付返回时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPayReturnTime(String value)
	{
		this.payReturnTime = value;
	}


	/**
	 * 获取打款完成时间
	 * 
	 * 此字段的版本 >= 0
	 * @return recvFeeTime value 类型为:String
	 * 
	 */
	public String getRecvFeeTime()
	{
		return recvFeeTime;
	}


	/**
	 * 设置打款完成时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvFeeTime(String value)
	{
		this.recvFeeTime = value;
	}


	/**
	 * 获取打款返回时间
	 * 
	 * 此字段的版本 >= 0
	 * @return recvFeeReturnTime value 类型为:String
	 * 
	 */
	public String getRecvFeeReturnTime()
	{
		return recvFeeReturnTime;
	}


	/**
	 * 设置打款返回时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvFeeReturnTime(String value)
	{
		this.recvFeeReturnTime = value;
	}


	/**
	 * 获取子订单列表
	 * 
	 * 此字段的版本 >= 0
	 * @return tradeList value 类型为:List<TradeInfo>
	 * 
	 */
	public List<TradeInfo> getTradeList()
	{
		return tradeList;
	}


	/**
	 * 设置子订单列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:List<TradeInfo>
	 * 
	 */
	public void setTradeList(List<TradeInfo> value)
	{
		if (value != null) {
				this.tradeList = value;
		}else{
				this.tradeList = new ArrayList<TradeInfo>();
		}
	}


	/**
	 * 获取req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(DealInfo)
				length += 4;  //计算字段version的长度 size_of(int)
				length += ByteStream.getObjectSize(dealCode);  //计算字段dealCode的长度 size_of(String)
				length += 4;  //计算字段dealVersion的长度 size_of(int)
				length += ByteStream.getObjectSize(dealCreateTime);  //计算字段dealCreateTime的长度 size_of(String)
				length += ByteStream.getObjectSize(dealEndTime);  //计算字段dealEndTime的长度 size_of(String)
				length += ByteStream.getObjectSize(lastModifyTime);  //计算字段lastModifyTime的长度 size_of(String)
				length += ByteStream.getObjectSize(dealDesc);  //计算字段dealDesc的长度 size_of(String)
				length += 4;  //计算字段state的长度 size_of(int)
				length += 4;  //计算字段substate的长度 size_of(int)
				length += 4;  //计算字段closeReason的长度 size_of(int)
				length += 4;  //计算字段refuseReason的长度 size_of(int)
				length += ByteStream.getObjectSize(smBeginTime);  //计算字段smBeginTime的长度 size_of(String)
				length += 4;  //计算字段payType的长度 size_of(int)
				length += 4;  //计算字段sellerUin的长度 size_of(long)
				length += 4;  //计算字段buyerUin的长度 size_of(long)
				length += ByteStream.getObjectSize(buyerNickName);  //计算字段buyerNickName的长度 size_of(String)
				length += ByteStream.getObjectSize(buyerNote);  //计算字段buyerNote的长度 size_of(String)
				length += ByteStream.getObjectSize(referer);  //计算字段referer的长度 size_of(String)
				length += ByteStream.getObjectSize(itemTitleList);  //计算字段itemTitleList的长度 size_of(String)
				length += ByteStream.getObjectSize(itemCode);  //计算字段itemCode的长度 size_of(String)
				length += ByteStream.getObjectSize(stockAttr);  //计算字段stockAttr的长度 size_of(String)
				length += 4;  //计算字段buyAmount的长度 size_of(int)
				length += 4;  //计算字段payFeeCash的长度 size_of(int)
				length += 4;  //计算字段payFeeTicket的长度 size_of(int)
				length += 4;  //计算字段payFeeScore的长度 size_of(int)
				length += 4;  //计算字段payFeeShipping的长度 size_of(int)
				length += 4;  //计算字段payFeeCommission的长度 size_of(int)
				length += 4;  //计算字段couponFee的长度 size_of(int)
				length += 4;  //计算字段payFeeTotal的长度 size_of(int)
				length += 4;  //计算字段buyerFeeTotal的长度 size_of(int)
				length += 4;  //计算字段recvScore的长度 size_of(int)
				length += ByteStream.getObjectSize(dealCftPayId);  //计算字段dealCftPayId的长度 size_of(String)
				length += 4;  //计算字段payId的长度 size_of(long)
				length += 4;  //计算字段hasInnovice的长度 size_of(int)
				length += ByteStream.getObjectSize(dealInnoviceTitle);  //计算字段dealInnoviceTitle的长度 size_of(String)
				length += ByteStream.getObjectSize(dealInnoviceContent);  //计算字段dealInnoviceContent的长度 size_of(String)
				length += 4;  //计算字段flags的长度 size_of(int)
				length += 4;  //计算字段recvAddrId的长度 size_of(long)
				length += ByteStream.getObjectSize(recvAddrName);  //计算字段recvAddrName的长度 size_of(String)
				length += ByteStream.getObjectSize(recvAddrDetail);  //计算字段recvAddrDetail的长度 size_of(String)
				length += ByteStream.getObjectSize(recvAddrPostcode);  //计算字段recvAddrPostcode的长度 size_of(String)
				length += ByteStream.getObjectSize(recvAddrPhone);  //计算字段recvAddrPhone的长度 size_of(String)
				length += ByteStream.getObjectSize(recvAddrMobile);  //计算字段recvAddrMobile的长度 size_of(String)
				length += 4;  //计算字段wuliuId的长度 size_of(long)
				length += 4;  //计算字段mailType的长度 size_of(int)
				length += ByteStream.getObjectSize(payTime);  //计算字段payTime的长度 size_of(String)
				length += ByteStream.getObjectSize(payReturnTime);  //计算字段payReturnTime的长度 size_of(String)
				length += ByteStream.getObjectSize(recvFeeTime);  //计算字段recvFeeTime的长度 size_of(String)
				length += ByteStream.getObjectSize(recvFeeReturnTime);  //计算字段recvFeeReturnTime的长度 size_of(String)
				length += ByteStream.getObjectSize(tradeList);  //计算字段tradeList的长度 size_of(List)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
