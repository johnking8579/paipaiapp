 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.deal.protocol.pccod;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *一口价下单回复
 *
 *@date 2013-08-19 08:35::47
 *
 *@since version:0
*/
public class  OrderConfirmV2Resp implements IServiceObject
{
	public long result;
	/**
	 * 下单回复
	 *
	 * 版本 >= 0
	 */
	 private COrderFixupRsp RspData = new COrderFixupRsp();

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String ErrMsg = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(RspData);
		bs.pushString(ErrMsg);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		RspData = (COrderFixupRsp) bs.popObject(COrderFixupRsp.class);
		ErrMsg = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x97778003L;
	}


	/**
	 * 获取下单回复
	 * 
	 * 此字段的版本 >= 0
	 * @return RspData value 类型为:COrderFixupRsp
	 * 
	 */
	public COrderFixupRsp getRspData()
	{
		return RspData;
	}


	/**
	 * 设置下单回复
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:COrderFixupRsp
	 * 
	 */
	public void setRspData(COrderFixupRsp value)
	{
		if (value != null) {
				this.RspData = value;
		}else{
				this.RspData = new COrderFixupRsp();
		}
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return ErrMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return ErrMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		if (value != null) {
				this.ErrMsg = value;
		}else{
				this.ErrMsg = new String();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(OrderConfirmV2Resp)
				length += ByteStream.getObjectSize(RspData);  //计算字段RspData的长度 size_of(COrderFixupRsp)
				length += ByteStream.getObjectSize(ErrMsg);  //计算字段ErrMsg的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
