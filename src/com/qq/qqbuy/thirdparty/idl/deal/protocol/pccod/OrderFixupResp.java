 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu,skyzhuang

package com.qq.qqbuy.thirdparty.idl.deal.protocol.pccod;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;
import com.qq.qqbuy.common.Log;


/**
 *一口价确认下单回复
 *
 *@date 2013-01-17 03:33::47
 *
 *@since version:0
*/
public class  OrderFixupResp implements IServiceObject
{
	public long result;
	/**
	 * 确认下单回复
	 *
	 * 版本 >= 0
	 */
	 private COrderFixupRsp ORsp = new COrderFixupRsp();

	/**
	 * 保留输出字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveOut = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(result);
		bs.pushObject(ORsp);
		bs.pushString(ReserveOut);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		result = bs.popUInt();
		ORsp = (COrderFixupRsp) bs.popObject(COrderFixupRsp.class);
		ReserveOut = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x97778002L;
	}


	/**
	 * 获取确认下单回复
	 * 
	 * 此字段的版本 >= 0
	 * @return ORsp value 类型为:COrderFixupRsp
	 * 
	 */
	public COrderFixupRsp getORsp()
	{
		return ORsp;
	}


	/**
	 * 设置确认下单回复
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:COrderFixupRsp
	 * 
	 */
	public void setORsp(COrderFixupRsp value)
	{
		if (value != null) {
				this.ORsp = value;
		}else{
				this.ORsp = new COrderFixupRsp();
		}
	}


	/**
	 * 获取保留输出字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveOut value 类型为:String
	 * 
	 */
	public String getReserveOut()
	{
		return ReserveOut;
	}


	/**
	 * 设置保留输出字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveOut(String value)
	{
		this.ReserveOut = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(OrderFixupResp)
				length += ByteStream.getObjectSize(ORsp);  //计算字段ORsp的长度 size_of(COrderFixupRsp)
				length += ByteStream.getObjectSize(ReserveOut);  //计算字段ReserveOut的长度 size_of(String)
		}catch(Exception e){
			Log.run.warn("size e",e);
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
