 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.deal.idl.DealService.java

package com.qq.qqbuy.thirdparty.idl.deal.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2012-12-07 10:51:37
 *
 *@since version:1
*/
public class  SellerGetDealListByConditionReq implements IServiceObject
{
	/**
	 * 查询QQ号
	 *
	 * 版本 >= 0
	 */
	 private long uin;

	/**
	 * 查询类型：256：货到付款255：在线支付,
	 *
	 * 版本 >= 0
	 */
	 private int payType = 256;

	/**
	 * 查询条件
	 *
	 * 版本 >= 0
	 */
	 private DealCondition condition = new DealCondition();

	/**
	 * 页码
	 *
	 * 版本 >= 0
	 */
	 private int pageIndex = 0;

	/**
	 * 页大小
	 *
	 * 版本 >= 0
	 */
	 private int pageSize = 0;

	/**
	 * 订单形式:1: 只查deal，不关联查对应的trade>=2: 关联查对应的trade,
	 *
	 * 版本 >= 0
	 */
	 private int dealFormat = 1;

	/**
	 * 调用者
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * req保留字段
	 *
	 * 版本 >= 0
	 */
	 private String inReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(uin);
		bs.pushInt(payType);
		bs.pushObject(condition);
		bs.pushInt(pageIndex);
		bs.pushInt(pageSize);
		bs.pushInt(dealFormat);
		bs.pushString(source);
		bs.pushString(inReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		uin = bs.popUInt();
		payType = bs.popInt();
		condition = (DealCondition) bs.popObject(DealCondition.class);
		pageIndex = bs.popInt();
		pageSize = bs.popInt();
		dealFormat = bs.popInt();
		source = bs.popString();
		inReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80081806L;
	}


	/**
	 * 获取查询QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return uin;
	}


	/**
	 * 设置查询QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.uin = value;
	}


	/**
	 * 获取查询类型：256：货到付款255：在线支付,
	 * 
	 * 此字段的版本 >= 0
	 * @return payType value 类型为:int
	 * 
	 */
	public int getPayType()
	{
		return payType;
	}


	/**
	 * 设置查询类型：256：货到付款255：在线支付,
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPayType(int value)
	{
		this.payType = value;
	}


	/**
	 * 获取查询条件
	 * 
	 * 此字段的版本 >= 0
	 * @return condition value 类型为:DealCondition
	 * 
	 */
	public DealCondition getCondition()
	{
		return condition;
	}


	/**
	 * 设置查询条件
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:DealCondition
	 * 
	 */
	public void setCondition(DealCondition value)
	{
		if (value != null) {
				this.condition = value;
		}else{
				this.condition = new DealCondition();
		}
	}


	/**
	 * 获取页码
	 * 
	 * 此字段的版本 >= 0
	 * @return pageIndex value 类型为:int
	 * 
	 */
	public int getPageIndex()
	{
		return pageIndex;
	}


	/**
	 * 设置页码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPageIndex(int value)
	{
		this.pageIndex = value;
	}


	/**
	 * 获取页大小
	 * 
	 * 此字段的版本 >= 0
	 * @return pageSize value 类型为:int
	 * 
	 */
	public int getPageSize()
	{
		return pageSize;
	}


	/**
	 * 设置页大小
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPageSize(int value)
	{
		this.pageSize = value;
	}


	/**
	 * 获取订单形式:1: 只查deal，不关联查对应的trade>=2: 关联查对应的trade,
	 * 
	 * 此字段的版本 >= 0
	 * @return dealFormat value 类型为:int
	 * 
	 */
	public int getDealFormat()
	{
		return dealFormat;
	}


	/**
	 * 设置订单形式:1: 只查deal，不关联查对应的trade>=2: 关联查对应的trade,
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setDealFormat(int value)
	{
		this.dealFormat = value;
	}


	/**
	 * 获取调用者
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置调用者
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserved value 类型为:String
	 * 
	 */
	public String getInReserved()
	{
		return inReserved;
	}


	/**
	 * 设置req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserved(String value)
	{
		this.inReserved = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(SellerGetDealListByConditionReq)
				length += 4;  //计算字段uin的长度 size_of(long)
				length += 4;  //计算字段payType的长度 size_of(int)
				length += ByteStream.getObjectSize(condition);  //计算字段condition的长度 size_of(DealCondition)
				length += 4;  //计算字段pageIndex的长度 size_of(int)
				length += 4;  //计算字段pageSize的长度 size_of(int)
				length += 4;  //计算字段dealFormat的长度 size_of(int)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserved);  //计算字段inReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
