 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.deal.protocol.pccod;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *一口价下单请求
 *
 *@date 2013-08-19 08:35::47
 *
 *@since version:0
*/
public class  OrderConfirmV2Req implements IServiceObject
{
	/**
	 * 请求来源描述
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 场景id
	 *
	 * 版本 >= 0
	 */
	 private long SceneId;

	/**
	 * 下单请求
	 *
	 * 版本 >= 0
	 */
	 private ConfirmV2Req ReqData = new ConfirmV2Req();

	/**
	 * 请求MachineKey
	 *
	 * 版本 >= 0
	 */
	 private String MachineKey = new String();

	/**
	 * 保留输入字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveIn = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(Source);
		bs.pushUInt(SceneId);
		bs.pushObject(ReqData);
		bs.pushString(MachineKey);
		bs.pushString(ReserveIn);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		Source = bs.popString();
		SceneId = bs.popUInt();
		ReqData = (ConfirmV2Req) bs.popObject(ConfirmV2Req.class);
		MachineKey = bs.popString();
		ReserveIn = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x97771003L;
	}


	/**
	 * 获取请求来源描述
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置请求来源描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		if (value != null) {
				this.Source = value;
		}else{
				this.Source = new String();
		}
	}


	/**
	 * 获取场景id
	 * 
	 * 此字段的版本 >= 0
	 * @return SceneId value 类型为:long
	 * 
	 */
	public long getSceneId()
	{
		return SceneId;
	}


	/**
	 * 设置场景id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSceneId(long value)
	{
		this.SceneId = value;
	}


	/**
	 * 获取下单请求
	 * 
	 * 此字段的版本 >= 0
	 * @return ReqData value 类型为:ConfirmV2Req
	 * 
	 */
	public ConfirmV2Req getReqData()
	{
		return ReqData;
	}


	/**
	 * 设置下单请求
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ConfirmV2Req
	 * 
	 */
	public void setReqData(ConfirmV2Req value)
	{
		if (value != null) {
				this.ReqData = value;
		}else{
				this.ReqData = new ConfirmV2Req();
		}
	}


	/**
	 * 获取请求MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @return MachineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return MachineKey;
	}


	/**
	 * 设置请求MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		if (value != null) {
				this.MachineKey = value;
		}else{
				this.MachineKey = new String();
		}
	}


	/**
	 * 获取保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveIn value 类型为:String
	 * 
	 */
	public String getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveIn(String value)
	{
		if (value != null) {
				this.ReserveIn = value;
		}else{
				this.ReserveIn = new String();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(OrderConfirmV2Req)
				length += ByteStream.getObjectSize(Source);  //计算字段Source的长度 size_of(String)
				length += 4;  //计算字段SceneId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ReqData);  //计算字段ReqData的长度 size_of(ConfirmV2Req)
				length += ByteStream.getObjectSize(MachineKey);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveIn);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
