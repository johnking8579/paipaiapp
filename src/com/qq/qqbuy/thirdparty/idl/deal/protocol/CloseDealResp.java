//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.deal.protocol;


import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *取消订单返回
 *
 *@date 2011-03-08 03:31::51
 *
 *@since version:0
*/
public class  CloseDealResp implements IServiceObject
{
    public long result;
    /**
     * 保留输出字段
     *
     * 版本 >= 0
     */
     private String ReserveOut = new String();


    public int Serialize(ByteStream bs) throws Exception
    {

        bs.pushUInt(result);
        bs.pushString(ReserveOut);
        return bs.getWrittenLength();
    }
    
    public int UnSerialize(ByteStream bs) throws Exception
    {

        result = bs.popUInt();
        ReserveOut = bs.popString();
        return bs.getReadLength();
    }

    public long getCmdId()
    {
        return 0x26308804L;
    }


    /**
     * 获取保留输出字段
     * 
     * 此字段的版本 >= 0
     * @return ReserveOut value 类型为:String
     * 
     */
    public String getReserveOut()
    {
        return ReserveOut;
    }


    /**
     * 设置保留输出字段
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:String
     * 
     */
    public void setReserveOut(String value)
    {
        if (value != null) {
                this.ReserveOut = value;
        }else{
                this.ReserveOut = new String();
        }
    }


    public long getResult()
    {
        return  this.result;
    }

    
    public void setResult(long value)
    {
        this.result = value;
    }

    
    protected int getClassSize()
    {
        return  getSize() - 4;
    }

    
    public int getSize()
    {
        int length = 4;
        try{
                length = 4;  //size_of(CloseDealResp)
                length += ByteStream.getObjectSize(ReserveOut);  //计算字段ReserveOut的长度 size_of(String)
        }catch(Exception e){
                e.printStackTrace();
        }
        return length;
    }

    /**
     *下面是生成toString()方法
     此方法用于调试时打开*
     *如果要打开此方法，请加入commons-lang-2.4.jar
     *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
     *      import org.apache.commons.lang.builder.ToStringStyle;
     *
     */
    @Override
    public String toString() {
      return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
    }
}

 
 