 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.deal.idl.DealService.java

package com.qq.qqbuy.thirdparty.idl.deal.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2012-12-07 10:51:37
 *
 *@since version:1
*/
public class  BuyerConfirmCodDealReq implements IServiceObject
{
	/**
	 * 上行短信业务实体
	 *
	 * 版本 >= 0
	 */
	 private ReceiveMsgItem msgItem = new ReceiveMsgItem();

	/**
	 * 操作类型
	 *
	 * 版本 >= 0
	 */
	 private int opType;

	/**
	 * 调用者
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * req保留字段
	 *
	 * 版本 >= 0
	 */
	 private String inReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(msgItem);
		bs.pushInt(opType);
		bs.pushString(source);
		bs.pushString(inReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		msgItem = (ReceiveMsgItem) bs.popObject(ReceiveMsgItem.class);
		opType = bs.popInt();
		source = bs.popString();
		inReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80081821L;
	}


	/**
	 * 获取上行短信业务实体
	 * 
	 * 此字段的版本 >= 0
	 * @return msgItem value 类型为:ReceiveMsgItem
	 * 
	 */
	public ReceiveMsgItem getMsgItem()
	{
		return msgItem;
	}


	/**
	 * 设置上行短信业务实体
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ReceiveMsgItem
	 * 
	 */
	public void setMsgItem(ReceiveMsgItem value)
	{
		if (value != null) {
				this.msgItem = value;
		}else{
				this.msgItem = new ReceiveMsgItem();
		}
	}


	/**
	 * 获取操作类型
	 * 
	 * 此字段的版本 >= 0
	 * @return opType value 类型为:int
	 * 
	 */
	public int getOpType()
	{
		return opType;
	}


	/**
	 * 设置操作类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setOpType(int value)
	{
		this.opType = value;
	}


	/**
	 * 获取调用者
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置调用者
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserved value 类型为:String
	 * 
	 */
	public String getInReserved()
	{
		return inReserved;
	}


	/**
	 * 设置req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserved(String value)
	{
		this.inReserved = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(BuyerConfirmCodDealReq)
				length += ByteStream.getObjectSize(msgItem);  //计算字段msgItem的长度 size_of(ReceiveMsgItem)
				length += 4;  //计算字段opType的长度 size_of(int)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserved);  //计算字段inReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
