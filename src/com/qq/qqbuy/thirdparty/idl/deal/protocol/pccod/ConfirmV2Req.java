//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.deal.protocol.pccod;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import java.util.Map;
import com.paipai.lang.MultiMap;
import java.util.HashMap;

/**
 * 下单请求
 * 
 * @date 2013-08-19 08:35::47
 * 
 * @since version:0
 */
public class ConfirmV2Req implements ICanSerializeObject {
	/**
	 * 版本号
	 * 
	 * 版本 >= 0
	 */
	private long version = 6;

	/**
	 * 买家号
	 * 
	 * 版本 >= 0
	 */
	private long Uin;

	/**
	 * 购买数量
	 * 
	 * 版本 >= 0
	 */
	private long BuyNum;

	/**
	 * 邮递类型
	 * 
	 * 版本 >= 0
	 */
	private long MailType;

	/**
	 * 促销规则id
	 * 
	 * 版本 >= 0
	 */
	private long PromotionId;

	/**
	 * 商品id
	 * 
	 * 版本 >= 0
	 */
	private String ItemId = new String();

	/**
	 * 收货地址
	 * 
	 * 版本 >= 0
	 */
	private String RecvAddr = new String();

	/**
	 * 收货姓名
	 * 
	 * 版本 >= 0
	 */
	private String RecvName = new String();

	/**
	 * 收货手机
	 * 
	 * 版本 >= 0
	 */
	private String RecvMobile = new String();

	/**
	 * 收货电话
	 * 
	 * 版本 >= 0
	 */
	private String RecvPhone = new String();

	/**
	 * 收货邮编
	 * 
	 * 版本 >= 0
	 */
	private String RecvPost = new String();

	/**
	 * 商品类型
	 * 
	 * 版本 >= 0
	 */
	private long CmdyType;

	/**
	 * 商品属性
	 * 
	 * 版本 >= 0
	 */
	private String ItemAttr = new String();

	/**
	 * 是否匿名购买
	 * 
	 * 版本 >= 0
	 */
	private long Anonymous;

	/**
	 * 下单来源
	 * 
	 * 版本 >= 0
	 */
	private long OrderFrom;

	/**
	 * 是否保存虚拟地址
	 * 
	 * 版本 >= 0
	 */
	private short SaveAddr;

	/**
	 * 发票名称
	 * 
	 * 版本 >= 0
	 */
	private String InnoviceTitle = new String();

	/**
	 * 价格类型
	 * 
	 * 版本 >= 0
	 */
	private long PriceType;

	/**
	 * 支付类型
	 * 
	 * 版本 >= 0
	 */
	private long PayType;

	/**
	 * 拍拍设置的cookie(_p),请设置
	 * 
	 * 版本 >= 0
	 */
	private String Refer = new String();

	/**
	 * 买家留言
	 * 
	 * 版本 >= 0
	 */
	private String BuyerNote = new String();

	/**
	 * 红包id
	 * 
	 * 版本 >= 0
	 */
	private long RedPacketId;

	/**
	 * 店铺代金券id
	 * 
	 * 版本 >= 0
	 */
	private long ShopcouponId;

	/**
	 * 地址id
	 * 
	 * 版本 >= 0
	 */
	private long Addressid;

	/**
	 * 区域id
	 * 
	 * 版本 >= 0
	 */
	private long RegionId;

	/**
	 * 游戏名
	 * 
	 * 版本 >= 0
	 */
	private String GameName = new String();

	/**
	 * 所在区服
	 * 
	 * 版本 >= 0
	 */
	private String GameArea = new String();

	/**
	 * 充值类型
	 * 
	 * 版本 >= 0
	 */
	private long GameClass;

	/**
	 * 游戏账号
	 * 
	 * 版本 >= 0
	 */
	private String GameId = new String();

	/**
	 * 手机号码
	 * 
	 * 版本 >= 0
	 */
	private String MobileNum = new String();

	/**
	 * qq号码
	 * 
	 * 版本 >= 0
	 */
	private String QQNum = new String();

	/**
	 * 使用的积分数
	 * 
	 * 版本 >= 0
	 */
	private long PaySocre;

	/**
	 * 颜色
	 * 
	 * 版本 >= 0
	 */
	private String ComdyColor = new String();

	/**
	 * 尺码
	 * 
	 * 版本 >= 0
	 */
	private String ComdySize = new String();

	/**
	 * 扩展信息
	 * 
	 * 版本 >= 0
	 */
	private MultiMap<String, String> ExtInfoMap = new MultiMap<String, String>();

	/**
	 * 版本 >= 0
	 */
	private short OrderFrom_u;

	/**
	 * 版本 >= 0
	 */
	private short Uin_u;

	/**
	 * 版本 >= 0
	 */
	private short BuyNum_u;

	/**
	 * 版本 >= 0
	 */
	private short MailType_u;

	/**
	 * 版本 >= 0
	 */
	private short PriceType_u;

	/**
	 * 版本 >= 0
	 */
	private short PayType_u;

	/**
	 * 版本 >= 0
	 */
	private short RedPacketId_u;

	/**
	 * 版本 >= 0
	 */
	private short ShopcouponId_u;

	/**
	 * 版本 >= 0
	 */
	private short PromotionId_u;

	/**
	 * 版本 >= 0
	 */
	private short CmdyType_u;

	/**
	 * 版本 >= 0
	 */
	private short Anonymous_u;

	/**
	 * 版本 >= 0
	 */
	private short SaveAddr_u;

	/**
	 * 版本 >= 0
	 */
	private short InnoviceTitle_u;

	/**
	 * 版本 >= 0
	 */
	private short Refer_u;

	/**
	 * 版本 >= 0
	 */
	private short ItemId_u;

	/**
	 * 版本 >= 0
	 */
	private short ItemAttr_u;

	/**
	 * 版本 >= 0
	 */
	private short BuyerNote_u;

	/**
	 * 版本 >= 0
	 */
	private short Addressid_u;

	/**
	 * 版本 >= 0
	 */
	private short RegionId_u;

	/**
	 * 版本 >= 0
	 */
	private short GameName_u;

	/**
	 * 版本 >= 0
	 */
	private short GameArea_u;

	/**
	 * 版本 >= 0
	 */
	private short GameClass_u;

	/**
	 * 版本 >= 0
	 */
	private short GameId_u;

	/**
	 * 版本 >= 0
	 */
	private short MobileNum_u;

	/**
	 * 版本 >= 0
	 */
	private short PaySocre_u;

	/**
	 * 版本 >= 0
	 */
	private short ComdyColor_u;

	/**
	 * 版本 >= 0
	 */
	private short ComdySize_u;

	/**
	 * 版本 >= 0
	 */
	private short QQNum_u;

	/**
	 * 版本 >= 0
	 */
	private short ExtInfoMap_u;

	/**
	 * 版本号标识
	 * 
	 * 版本 >= 1
	 */
	private short version_u;

	/**
	 * 收货地址标识
	 * 
	 * 版本 >= 1
	 */
	private short RecvAddr_u;

	/**
	 * 收货姓名标识
	 * 
	 * 版本 >= 1
	 */
	private short RecvName_u;

	/**
	 * 收货手机标识
	 * 
	 * 版本 >= 1
	 */
	private short RecvMobile_u;

	/**
	 * 收货电话标识
	 * 
	 * 版本 >= 1
	 */
	private short RecvPhone_u;

	/**
	 * 收货邮编标识
	 * 
	 * 版本 >= 1
	 */
	private short RecvPost_u;

	/**
	 * 包邮卡ID
	 * 
	 * 版本 >= 2
	 */
	private long FreeMailCardId;

	/**
	 * 包邮卡ID标识
	 * 
	 * 版本 >= 2
	 */
	private short FreeMailCardId_u;

	/**
	 * 商品skuid
	 * 
	 * 版本 >= 3
	 */
	private long ItemSkuId;

	/**
	 * 商品skuid标识
	 * 
	 * 版本 >= 3
	 */
	private short ItemSkuId_u;

	/**
	 * 商品维度扩展信息集合
	 * 
	 * 版本 >= 4
	 */
	private Map<String, String> cmdyReserves = new HashMap<String, String>();

	/**
	 * 商品维度扩展信息集合标识
	 * 
	 * 版本 >= 4
	 */
	private short cmdyReserves_u;

	/**
	 * 网购券id
	 * 
	 * 版本 >= 5
	 */
	private long onlineShoppingCoupons;

	/**
	 * 商品skuid标识
	 * 
	 * 版本 >= 5
	 */
	private short onlineShoppingCoupons_u;

    /**
     * 红包积分
     *
     * 版本 >= 6
     */
    private long RedBagScore;

	public int serialize(ByteStream bs) throws Exception {
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUInt(Uin);
		bs.pushUInt(BuyNum);
		bs.pushUInt(MailType);
		bs.pushUInt(PromotionId);
		bs.pushString(ItemId);
		bs.pushString(RecvAddr);
		bs.pushString(RecvName);
		bs.pushString(RecvMobile);
		bs.pushString(RecvPhone);
		bs.pushString(RecvPost);
		bs.pushUInt(CmdyType);
		bs.pushString(ItemAttr);
		bs.pushUInt(Anonymous);
		bs.pushUInt(OrderFrom);
		bs.pushUByte(SaveAddr);
		bs.pushString(InnoviceTitle);
		bs.pushUInt(PriceType);
		bs.pushUInt(PayType);
		bs.pushString(Refer);
		bs.pushString(BuyerNote);
		bs.pushUInt(RedPacketId);
		bs.pushUInt(ShopcouponId);
		bs.pushUInt(Addressid);
		bs.pushUInt(RegionId);
		bs.pushString(GameName);
		bs.pushString(GameArea);
		bs.pushUInt(GameClass);
		bs.pushString(GameId);
		bs.pushString(MobileNum);
		bs.pushString(QQNum);
		bs.pushUInt(PaySocre);
		bs.pushString(ComdyColor);
		bs.pushString(ComdySize);
		bs.pushObject(ExtInfoMap);
		bs.pushUByte(OrderFrom_u);
		bs.pushUByte(Uin_u);
		bs.pushUByte(BuyNum_u);
		bs.pushUByte(MailType_u);
		bs.pushUByte(PriceType_u);
		bs.pushUByte(PayType_u);
		bs.pushUByte(RedPacketId_u);
		bs.pushUByte(ShopcouponId_u);
		bs.pushUByte(PromotionId_u);
		bs.pushUByte(CmdyType_u);
		bs.pushUByte(Anonymous_u);
		bs.pushUByte(SaveAddr_u);
		bs.pushUByte(InnoviceTitle_u);
		bs.pushUByte(Refer_u);
		bs.pushUByte(ItemId_u);
		bs.pushUByte(ItemAttr_u);
		bs.pushUByte(BuyerNote_u);
		bs.pushUByte(Addressid_u);
		bs.pushUByte(RegionId_u);
		bs.pushUByte(GameName_u);
		bs.pushUByte(GameArea_u);
		bs.pushUByte(GameClass_u);
		bs.pushUByte(GameId_u);
		bs.pushUByte(MobileNum_u);
		bs.pushUByte(PaySocre_u);
		bs.pushUByte(ComdyColor_u);
		bs.pushUByte(ComdySize_u);
		bs.pushUByte(QQNum_u);
		bs.pushUByte(ExtInfoMap_u);
		if (this.version >= 1) {
			bs.pushUByte(version_u);
		}
		if (this.version >= 1) {
			bs.pushUByte(RecvAddr_u);
		}
		if (this.version >= 1) {
			bs.pushUByte(RecvName_u);
		}
		if (this.version >= 1) {
			bs.pushUByte(RecvMobile_u);
		}
		if (this.version >= 1) {
			bs.pushUByte(RecvPhone_u);
		}
		if (this.version >= 1) {
			bs.pushUByte(RecvPost_u);
		}
		if (this.version >= 2) {
			bs.pushLong(FreeMailCardId);
		}
		if (this.version >= 2) {
			bs.pushUByte(FreeMailCardId_u);
		}
		if (this.version >= 3) {
			bs.pushLong(ItemSkuId);
		}
		if (this.version >= 3) {
			bs.pushUByte(ItemSkuId_u);
		}
		if (this.version >= 4) {
			bs.pushObject(cmdyReserves);
		}
		if (this.version >= 4) {
			bs.pushUByte(cmdyReserves_u);
		}
		if (this.version >= 5) {
			bs.pushLong(onlineShoppingCoupons);
		}
		if (this.version >= 5) {
			bs.pushUByte(onlineShoppingCoupons_u);
		}
        if (this.version >= 6){
            bs.pushUInt(RedBagScore);
        }
		return bs.getWrittenLength();
	}

	public int unSerialize(ByteStream bs) throws Exception {
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
			return 0;
		version = bs.popUInt();
		Uin = bs.popUInt();
		BuyNum = bs.popUInt();
		MailType = bs.popUInt();
		PromotionId = bs.popUInt();
		ItemId = bs.popString();
		RecvAddr = bs.popString();
		RecvName = bs.popString();
		RecvMobile = bs.popString();
		RecvPhone = bs.popString();
		RecvPost = bs.popString();
		CmdyType = bs.popUInt();
		ItemAttr = bs.popString();
		Anonymous = bs.popUInt();
		OrderFrom = bs.popUInt();
		SaveAddr = bs.popUByte();
		InnoviceTitle = bs.popString();
		PriceType = bs.popUInt();
		PayType = bs.popUInt();
		Refer = bs.popString();
		BuyerNote = bs.popString();
		RedPacketId = bs.popUInt();
		ShopcouponId = bs.popUInt();
		Addressid = bs.popUInt();
		RegionId = bs.popUInt();
		GameName = bs.popString();
		GameArea = bs.popString();
		GameClass = bs.popUInt();
		GameId = bs.popString();
		MobileNum = bs.popString();
		QQNum = bs.popString();
		PaySocre = bs.popUInt();
		ComdyColor = bs.popString();
		ComdySize = bs.popString();
		ExtInfoMap = (MultiMap<String, String>) bs.popMultiMap(String.class, String.class);
		OrderFrom_u = bs.popUByte();
		Uin_u = bs.popUByte();
		BuyNum_u = bs.popUByte();
		MailType_u = bs.popUByte();
		PriceType_u = bs.popUByte();
		PayType_u = bs.popUByte();
		RedPacketId_u = bs.popUByte();
		ShopcouponId_u = bs.popUByte();
		PromotionId_u = bs.popUByte();
		CmdyType_u = bs.popUByte();
		Anonymous_u = bs.popUByte();
		SaveAddr_u = bs.popUByte();
		InnoviceTitle_u = bs.popUByte();
		Refer_u = bs.popUByte();
		ItemId_u = bs.popUByte();
		ItemAttr_u = bs.popUByte();
		BuyerNote_u = bs.popUByte();
		Addressid_u = bs.popUByte();
		RegionId_u = bs.popUByte();
		GameName_u = bs.popUByte();
		GameArea_u = bs.popUByte();
		GameClass_u = bs.popUByte();
		GameId_u = bs.popUByte();
		MobileNum_u = bs.popUByte();
		PaySocre_u = bs.popUByte();
		ComdyColor_u = bs.popUByte();
		ComdySize_u = bs.popUByte();
		QQNum_u = bs.popUByte();
		ExtInfoMap_u = bs.popUByte();
		if (this.version >= 1) {
			version_u = bs.popUByte();
		}
		if (this.version >= 1) {
			RecvAddr_u = bs.popUByte();
		}
		if (this.version >= 1) {
			RecvName_u = bs.popUByte();
		}
		if (this.version >= 1) {
			RecvMobile_u = bs.popUByte();
		}
		if (this.version >= 1) {
			RecvPhone_u = bs.popUByte();
		}
		if (this.version >= 1) {
			RecvPost_u = bs.popUByte();
		}
		if (this.version >= 2) {
			FreeMailCardId = bs.popLong();
		}
		if (this.version >= 2) {
			FreeMailCardId_u = bs.popUByte();
		}
		if (this.version >= 3) {
			ItemSkuId = bs.popLong();
		}
		if (this.version >= 3) {
			ItemSkuId_u = bs.popUByte();
		}
		if (this.version >= 4) {
			cmdyReserves = (Map<String, String>) bs.popMap(String.class, String.class);
		}
		if (this.version >= 4) {
			cmdyReserves_u = bs.popUByte();
		}
		if (this.version >= 5) {
			onlineShoppingCoupons = bs.popLong();
		}
		if (this.version >= 5) {
			onlineShoppingCoupons_u = bs.popUByte();
		}
        if (this.version >= 6){
            RedBagScore = bs.popUInt();
        }

		/********************** 为了支持多个版本的客户端 ************************/
		int needPopBytes = (int) size - (bs.getReadLength() - startPosPop);
		for (int i = 0; i < needPopBytes; i++)
			bs.popByte();
		/********************** 为了支持多个版本的客户端 ************************/

		return bs.getReadLength();
	}

	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setVersion(long value) {
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion() {
		return this.version_u != 0;
	}

	/**
	 * 获取买家号
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return Uin value 类型为:long
	 * 
	 */
	public long getUin() {
		return Uin;
	}

	/**
	 * 设置买家号
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setUin(long value) {
		this.Uin = value;
		this.Uin_u = 1;
	}

	public boolean issetUin() {
		return this.Uin_u != 0;
	}

	/**
	 * 获取购买数量
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return BuyNum value 类型为:long
	 * 
	 */
	public long getBuyNum() {
		return BuyNum;
	}

	/**
	 * 设置购买数量
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setBuyNum(long value) {
		this.BuyNum = value;
		this.BuyNum_u = 1;
	}

	public boolean issetBuyNum() {
		return this.BuyNum_u != 0;
	}

	/**
	 * 获取邮递类型
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return MailType value 类型为:long
	 * 
	 */
	public long getMailType() {
		return MailType;
	}

	/**
	 * 设置邮递类型
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setMailType(long value) {
		this.MailType = value;
		this.MailType_u = 1;
	}

	public boolean issetMailType() {
		return this.MailType_u != 0;
	}

	/**
	 * 获取促销规则id
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return PromotionId value 类型为:long
	 * 
	 */
	public long getPromotionId() {
		return PromotionId;
	}

	/**
	 * 设置促销规则id
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setPromotionId(long value) {
		this.PromotionId = value;
		this.PromotionId_u = 1;
	}

	public boolean issetPromotionId() {
		return this.PromotionId_u != 0;
	}

	/**
	 * 获取商品id
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return ItemId value 类型为:String
	 * 
	 */
	public String getItemId() {
		return ItemId;
	}

	/**
	 * 设置商品id
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:String
	 * 
	 */
	public void setItemId(String value) {
		this.ItemId = value;
		this.ItemId_u = 1;
	}

	public boolean issetItemId() {
		return this.ItemId_u != 0;
	}

	/**
	 * 获取收货地址
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return RecvAddr value 类型为:String
	 * 
	 */
	public String getRecvAddr() {
		return RecvAddr;
	}

	/**
	 * 设置收货地址
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:String
	 * 
	 */
	public void setRecvAddr(String value) {
		this.RecvAddr = value;
		this.RecvAddr_u = 1;
	}

	public boolean issetRecvAddr() {
		return this.RecvAddr_u != 0;
	}

	/**
	 * 获取收货姓名
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return RecvName value 类型为:String
	 * 
	 */
	public String getRecvName() {
		return RecvName;
	}

	/**
	 * 设置收货姓名
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:String
	 * 
	 */
	public void setRecvName(String value) {
		this.RecvName = value;
		this.RecvName_u = 1;
	}

	public boolean issetRecvName() {
		return this.RecvName_u != 0;
	}

	/**
	 * 获取收货手机
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return RecvMobile value 类型为:String
	 * 
	 */
	public String getRecvMobile() {
		return RecvMobile;
	}

	/**
	 * 设置收货手机
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:String
	 * 
	 */
	public void setRecvMobile(String value) {
		this.RecvMobile = value;
		this.RecvMobile_u = 1;
	}

	public boolean issetRecvMobile() {
		return this.RecvMobile_u != 0;
	}

	/**
	 * 获取收货电话
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return RecvPhone value 类型为:String
	 * 
	 */
	public String getRecvPhone() {
		return RecvPhone;
	}

	/**
	 * 设置收货电话
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:String
	 * 
	 */
	public void setRecvPhone(String value) {
		this.RecvPhone = value;
		this.RecvPhone_u = 1;
	}

	public boolean issetRecvPhone() {
		return this.RecvPhone_u != 0;
	}

	/**
	 * 获取收货邮编
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return RecvPost value 类型为:String
	 * 
	 */
	public String getRecvPost() {
		return RecvPost;
	}

	/**
	 * 设置收货邮编
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:String
	 * 
	 */
	public void setRecvPost(String value) {
		this.RecvPost = value;
		this.RecvPost_u = 1;
	}

	public boolean issetRecvPost() {
		return this.RecvPost_u != 0;
	}

	/**
	 * 获取商品类型
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return CmdyType value 类型为:long
	 * 
	 */
	public long getCmdyType() {
		return CmdyType;
	}

	/**
	 * 设置商品类型
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setCmdyType(long value) {
		this.CmdyType = value;
		this.CmdyType_u = 1;
	}

	public boolean issetCmdyType() {
		return this.CmdyType_u != 0;
	}

	/**
	 * 获取商品属性
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return ItemAttr value 类型为:String
	 * 
	 */
	public String getItemAttr() {
		return ItemAttr;
	}

	/**
	 * 设置商品属性
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:String
	 * 
	 */
	public void setItemAttr(String value) {
		this.ItemAttr = value;
		this.ItemAttr_u = 1;
	}

	public boolean issetItemAttr() {
		return this.ItemAttr_u != 0;
	}

	/**
	 * 获取是否匿名购买
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return Anonymous value 类型为:long
	 * 
	 */
	public long getAnonymous() {
		return Anonymous;
	}

	/**
	 * 设置是否匿名购买
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setAnonymous(long value) {
		this.Anonymous = value;
		this.Anonymous_u = 1;
	}

	public boolean issetAnonymous() {
		return this.Anonymous_u != 0;
	}

	/**
	 * 获取下单来源
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return OrderFrom value 类型为:long
	 * 
	 */
	public long getOrderFrom() {
		return OrderFrom;
	}

	/**
	 * 设置下单来源
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setOrderFrom(long value) {
		this.OrderFrom = value;
		this.OrderFrom_u = 1;
	}

	public boolean issetOrderFrom() {
		return this.OrderFrom_u != 0;
	}

	/**
	 * 获取是否保存虚拟地址
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return SaveAddr value 类型为:short
	 * 
	 */
	public short getSaveAddr() {
		return SaveAddr;
	}

	/**
	 * 设置是否保存虚拟地址
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setSaveAddr(short value) {
		this.SaveAddr = value;
		this.SaveAddr_u = 1;
	}

	public boolean issetSaveAddr() {
		return this.SaveAddr_u != 0;
	}

	/**
	 * 获取发票名称
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return InnoviceTitle value 类型为:String
	 * 
	 */
	public String getInnoviceTitle() {
		return InnoviceTitle;
	}

	/**
	 * 设置发票名称
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:String
	 * 
	 */
	public void setInnoviceTitle(String value) {
		this.InnoviceTitle = value;
		this.InnoviceTitle_u = 1;
	}

	public boolean issetInnoviceTitle() {
		return this.InnoviceTitle_u != 0;
	}

	/**
	 * 获取价格类型
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return PriceType value 类型为:long
	 * 
	 */
	public long getPriceType() {
		return PriceType;
	}

	/**
	 * 设置价格类型
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setPriceType(long value) {
		this.PriceType = value;
		this.PriceType_u = 1;
	}

	public boolean issetPriceType() {
		return this.PriceType_u != 0;
	}

	/**
	 * 获取支付类型
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return PayType value 类型为:long
	 * 
	 */
	public long getPayType() {
		return PayType;
	}

	/**
	 * 设置支付类型
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setPayType(long value) {
		this.PayType = value;
		this.PayType_u = 1;
	}

	public boolean issetPayType() {
		return this.PayType_u != 0;
	}

	/**
	 * 获取拍拍设置的cookie(_p),请设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return Refer value 类型为:String
	 * 
	 */
	public String getRefer() {
		return Refer;
	}

	/**
	 * 设置拍拍设置的cookie(_p),请设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:String
	 * 
	 */
	public void setRefer(String value) {
		this.Refer = value;
		this.Refer_u = 1;
	}

	public boolean issetRefer() {
		return this.Refer_u != 0;
	}

	/**
	 * 获取买家留言
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return BuyerNote value 类型为:String
	 * 
	 */
	public String getBuyerNote() {
		return BuyerNote;
	}

	/**
	 * 设置买家留言
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:String
	 * 
	 */
	public void setBuyerNote(String value) {
		this.BuyerNote = value;
		this.BuyerNote_u = 1;
	}

	public boolean issetBuyerNote() {
		return this.BuyerNote_u != 0;
	}

	/**
	 * 获取红包id
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return RedPacketId value 类型为:long
	 * 
	 */
	public long getRedPacketId() {
		return RedPacketId;
	}

	/**
	 * 设置红包id
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setRedPacketId(long value) {
		this.RedPacketId = value;
		this.RedPacketId_u = 1;
	}

	public boolean issetRedPacketId() {
		return this.RedPacketId_u != 0;
	}

	/**
	 * 获取店铺代金券id
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return ShopcouponId value 类型为:long
	 * 
	 */
	public long getShopcouponId() {
		return ShopcouponId;
	}

	/**
	 * 设置店铺代金券id
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setShopcouponId(long value) {
		this.ShopcouponId = value;
		this.ShopcouponId_u = 1;
	}

	public boolean issetShopcouponId() {
		return this.ShopcouponId_u != 0;
	}

	/**
	 * 获取地址id
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return Addressid value 类型为:long
	 * 
	 */
	public long getAddressid() {
		return Addressid;
	}

	/**
	 * 设置地址id
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setAddressid(long value) {
		this.Addressid = value;
		this.Addressid_u = 1;
	}

	public boolean issetAddressid() {
		return this.Addressid_u != 0;
	}

	/**
	 * 获取区域id
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return RegionId value 类型为:long
	 * 
	 */
	public long getRegionId() {
		return RegionId;
	}

	/**
	 * 设置区域id
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setRegionId(long value) {
		this.RegionId = value;
		this.RegionId_u = 1;
	}

	public boolean issetRegionId() {
		return this.RegionId_u != 0;
	}

	/**
	 * 获取游戏名
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return GameName value 类型为:String
	 * 
	 */
	public String getGameName() {
		return GameName;
	}

	/**
	 * 设置游戏名
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:String
	 * 
	 */
	public void setGameName(String value) {
		this.GameName = value;
		this.GameName_u = 1;
	}

	public boolean issetGameName() {
		return this.GameName_u != 0;
	}

	/**
	 * 获取所在区服
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return GameArea value 类型为:String
	 * 
	 */
	public String getGameArea() {
		return GameArea;
	}

	/**
	 * 设置所在区服
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:String
	 * 
	 */
	public void setGameArea(String value) {
		this.GameArea = value;
		this.GameArea_u = 1;
	}

	public boolean issetGameArea() {
		return this.GameArea_u != 0;
	}

	/**
	 * 获取充值类型
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return GameClass value 类型为:long
	 * 
	 */
	public long getGameClass() {
		return GameClass;
	}

	/**
	 * 设置充值类型
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setGameClass(long value) {
		this.GameClass = value;
		this.GameClass_u = 1;
	}

	public boolean issetGameClass() {
		return this.GameClass_u != 0;
	}

	/**
	 * 获取游戏账号
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return GameId value 类型为:String
	 * 
	 */
	public String getGameId() {
		return GameId;
	}

	/**
	 * 设置游戏账号
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:String
	 * 
	 */
	public void setGameId(String value) {
		this.GameId = value;
		this.GameId_u = 1;
	}

	public boolean issetGameId() {
		return this.GameId_u != 0;
	}

	/**
	 * 获取手机号码
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return MobileNum value 类型为:String
	 * 
	 */
	public String getMobileNum() {
		return MobileNum;
	}

	/**
	 * 设置手机号码
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:String
	 * 
	 */
	public void setMobileNum(String value) {
		this.MobileNum = value;
		this.MobileNum_u = 1;
	}

	public boolean issetMobileNum() {
		return this.MobileNum_u != 0;
	}

	/**
	 * 获取qq号码
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return QQNum value 类型为:String
	 * 
	 */
	public String getQQNum() {
		return QQNum;
	}

	/**
	 * 设置qq号码
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:String
	 * 
	 */
	public void setQQNum(String value) {
		this.QQNum = value;
		this.QQNum_u = 1;
	}

	public boolean issetQQNum() {
		return this.QQNum_u != 0;
	}

	/**
	 * 获取使用的积分数
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return PaySocre value 类型为:long
	 * 
	 */
	public long getPaySocre() {
		return PaySocre;
	}

	/**
	 * 设置使用的积分数
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setPaySocre(long value) {
		this.PaySocre = value;
		this.PaySocre_u = 1;
	}

	public boolean issetPaySocre() {
		return this.PaySocre_u != 0;
	}

	/**
	 * 获取颜色
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return ComdyColor value 类型为:String
	 * 
	 */
	public String getComdyColor() {
		return ComdyColor;
	}

	/**
	 * 设置颜色
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:String
	 * 
	 */
	public void setComdyColor(String value) {
		this.ComdyColor = value;
		this.ComdyColor_u = 1;
	}

	public boolean issetComdyColor() {
		return this.ComdyColor_u != 0;
	}

	/**
	 * 获取尺码
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return ComdySize value 类型为:String
	 * 
	 */
	public String getComdySize() {
		return ComdySize;
	}

	/**
	 * 设置尺码
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:String
	 * 
	 */
	public void setComdySize(String value) {
		this.ComdySize = value;
		this.ComdySize_u = 1;
	}

	public boolean issetComdySize() {
		return this.ComdySize_u != 0;
	}

	/**
	 * 获取扩展信息
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return ExtInfoMap value 类型为:MultiMap<String,String>
	 * 
	 */
	public MultiMap<String, String> getExtInfoMap() {
		return ExtInfoMap;
	}

	/**
	 * 设置扩展信息
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:MultiMap<String,String>
	 * 
	 */
	public void setExtInfoMap(MultiMap<String, String> value) {
		if (value != null) {
			this.ExtInfoMap = value;
			this.ExtInfoMap_u = 1;
		}
	}

	public boolean issetExtInfoMap() {
		return this.ExtInfoMap_u != 0;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return OrderFrom_u value 类型为:short
	 * 
	 */
	public short getOrderFrom_u() {
		return OrderFrom_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setOrderFrom_u(short value) {
		this.OrderFrom_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return Uin_u value 类型为:short
	 * 
	 */
	public short getUin_u() {
		return Uin_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setUin_u(short value) {
		this.Uin_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return BuyNum_u value 类型为:short
	 * 
	 */
	public short getBuyNum_u() {
		return BuyNum_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setBuyNum_u(short value) {
		this.BuyNum_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return MailType_u value 类型为:short
	 * 
	 */
	public short getMailType_u() {
		return MailType_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setMailType_u(short value) {
		this.MailType_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return PriceType_u value 类型为:short
	 * 
	 */
	public short getPriceType_u() {
		return PriceType_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setPriceType_u(short value) {
		this.PriceType_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return PayType_u value 类型为:short
	 * 
	 */
	public short getPayType_u() {
		return PayType_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setPayType_u(short value) {
		this.PayType_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return RedPacketId_u value 类型为:short
	 * 
	 */
	public short getRedPacketId_u() {
		return RedPacketId_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setRedPacketId_u(short value) {
		this.RedPacketId_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return ShopcouponId_u value 类型为:short
	 * 
	 */
	public short getShopcouponId_u() {
		return ShopcouponId_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setShopcouponId_u(short value) {
		this.ShopcouponId_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return PromotionId_u value 类型为:short
	 * 
	 */
	public short getPromotionId_u() {
		return PromotionId_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setPromotionId_u(short value) {
		this.PromotionId_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return CmdyType_u value 类型为:short
	 * 
	 */
	public short getCmdyType_u() {
		return CmdyType_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setCmdyType_u(short value) {
		this.CmdyType_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return Anonymous_u value 类型为:short
	 * 
	 */
	public short getAnonymous_u() {
		return Anonymous_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setAnonymous_u(short value) {
		this.Anonymous_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return SaveAddr_u value 类型为:short
	 * 
	 */
	public short getSaveAddr_u() {
		return SaveAddr_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setSaveAddr_u(short value) {
		this.SaveAddr_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return InnoviceTitle_u value 类型为:short
	 * 
	 */
	public short getInnoviceTitle_u() {
		return InnoviceTitle_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setInnoviceTitle_u(short value) {
		this.InnoviceTitle_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return Refer_u value 类型为:short
	 * 
	 */
	public short getRefer_u() {
		return Refer_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setRefer_u(short value) {
		this.Refer_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return ItemId_u value 类型为:short
	 * 
	 */
	public short getItemId_u() {
		return ItemId_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setItemId_u(short value) {
		this.ItemId_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return ItemAttr_u value 类型为:short
	 * 
	 */
	public short getItemAttr_u() {
		return ItemAttr_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setItemAttr_u(short value) {
		this.ItemAttr_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return BuyerNote_u value 类型为:short
	 * 
	 */
	public short getBuyerNote_u() {
		return BuyerNote_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setBuyerNote_u(short value) {
		this.BuyerNote_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return Addressid_u value 类型为:short
	 * 
	 */
	public short getAddressid_u() {
		return Addressid_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setAddressid_u(short value) {
		this.Addressid_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return RegionId_u value 类型为:short
	 * 
	 */
	public short getRegionId_u() {
		return RegionId_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setRegionId_u(short value) {
		this.RegionId_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return GameName_u value 类型为:short
	 * 
	 */
	public short getGameName_u() {
		return GameName_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setGameName_u(short value) {
		this.GameName_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return GameArea_u value 类型为:short
	 * 
	 */
	public short getGameArea_u() {
		return GameArea_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setGameArea_u(short value) {
		this.GameArea_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return GameClass_u value 类型为:short
	 * 
	 */
	public short getGameClass_u() {
		return GameClass_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setGameClass_u(short value) {
		this.GameClass_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return GameId_u value 类型为:short
	 * 
	 */
	public short getGameId_u() {
		return GameId_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setGameId_u(short value) {
		this.GameId_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return MobileNum_u value 类型为:short
	 * 
	 */
	public short getMobileNum_u() {
		return MobileNum_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setMobileNum_u(short value) {
		this.MobileNum_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return PaySocre_u value 类型为:short
	 * 
	 */
	public short getPaySocre_u() {
		return PaySocre_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setPaySocre_u(short value) {
		this.PaySocre_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return ComdyColor_u value 类型为:short
	 * 
	 */
	public short getComdyColor_u() {
		return ComdyColor_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setComdyColor_u(short value) {
		this.ComdyColor_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return ComdySize_u value 类型为:short
	 * 
	 */
	public short getComdySize_u() {
		return ComdySize_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setComdySize_u(short value) {
		this.ComdySize_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return QQNum_u value 类型为:short
	 * 
	 */
	public short getQQNum_u() {
		return QQNum_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setQQNum_u(short value) {
		this.QQNum_u = value;
	}

	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @return ExtInfoMap_u value 类型为:short
	 * 
	 */
	public short getExtInfoMap_u() {
		return ExtInfoMap_u;
	}

	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setExtInfoMap_u(short value) {
		this.ExtInfoMap_u = value;
	}

	/**
	 * 获取版本号标识
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u() {
		return version_u;
	}

	/**
	 * 设置版本号标识
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setVersion_u(short value) {
		this.version_u = value;
	}

	/**
	 * 获取收货地址标识
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @return RecvAddr_u value 类型为:short
	 * 
	 */
	public short getRecvAddr_u() {
		return RecvAddr_u;
	}

	/**
	 * 设置收货地址标识
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setRecvAddr_u(short value) {
		this.RecvAddr_u = value;
	}

	/**
	 * 获取收货姓名标识
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @return RecvName_u value 类型为:short
	 * 
	 */
	public short getRecvName_u() {
		return RecvName_u;
	}

	/**
	 * 设置收货姓名标识
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setRecvName_u(short value) {
		this.RecvName_u = value;
	}

	/**
	 * 获取收货手机标识
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @return RecvMobile_u value 类型为:short
	 * 
	 */
	public short getRecvMobile_u() {
		return RecvMobile_u;
	}

	/**
	 * 设置收货手机标识
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setRecvMobile_u(short value) {
		this.RecvMobile_u = value;
	}

	/**
	 * 获取收货电话标识
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @return RecvPhone_u value 类型为:short
	 * 
	 */
	public short getRecvPhone_u() {
		return RecvPhone_u;
	}

	/**
	 * 设置收货电话标识
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setRecvPhone_u(short value) {
		this.RecvPhone_u = value;
	}

	/**
	 * 获取收货邮编标识
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @return RecvPost_u value 类型为:short
	 * 
	 */
	public short getRecvPost_u() {
		return RecvPost_u;
	}

	/**
	 * 设置收货邮编标识
	 * 
	 * 此字段的版本 >= 1
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setRecvPost_u(short value) {
		this.RecvPost_u = value;
	}

	/**
	 * 获取包邮卡ID
	 * 
	 * 此字段的版本 >= 2
	 * 
	 * @return FreeMailCardId value 类型为:long
	 * 
	 */
	public long getFreeMailCardId() {
		return FreeMailCardId;
	}

	/**
	 * 设置包邮卡ID
	 * 
	 * 此字段的版本 >= 2
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setFreeMailCardId(long value) {
		this.FreeMailCardId = value;
		this.FreeMailCardId_u = 1;
	}

	public boolean issetFreeMailCardId() {
		return this.FreeMailCardId_u != 0;
	}

	/**
	 * 获取包邮卡ID标识
	 * 
	 * 此字段的版本 >= 2
	 * 
	 * @return FreeMailCardId_u value 类型为:short
	 * 
	 */
	public short getFreeMailCardId_u() {
		return FreeMailCardId_u;
	}

	/**
	 * 设置包邮卡ID标识
	 * 
	 * 此字段的版本 >= 2
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setFreeMailCardId_u(short value) {
		this.FreeMailCardId_u = value;
	}

	/**
	 * 获取商品skuid
	 * 
	 * 此字段的版本 >= 3
	 * 
	 * @return ItemSkuId value 类型为:long
	 * 
	 */
	public long getItemSkuId() {
		return ItemSkuId;
	}

	/**
	 * 设置商品skuid
	 * 
	 * 此字段的版本 >= 3
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setItemSkuId(long value) {
		this.ItemSkuId = value;
		this.ItemSkuId_u = 1;
	}

	public boolean issetItemSkuId() {
		return this.ItemSkuId_u != 0;
	}

	/**
	 * 获取商品skuid标识
	 * 
	 * 此字段的版本 >= 3
	 * 
	 * @return ItemSkuId_u value 类型为:short
	 * 
	 */
	public short getItemSkuId_u() {
		return ItemSkuId_u;
	}

	/**
	 * 设置商品skuid标识
	 * 
	 * 此字段的版本 >= 3
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setItemSkuId_u(short value) {
		this.ItemSkuId_u = value;
	}

	/**
	 * 获取商品维度扩展信息集合
	 * 
	 * 此字段的版本 >= 4
	 * 
	 * @return cmdyReserves value 类型为:Map<String,String>
	 * 
	 */
	public Map<String, String> getCmdyReserves() {
		return cmdyReserves;
	}

	/**
	 * 设置商品维度扩展信息集合
	 * 
	 * 此字段的版本 >= 4
	 * 
	 * @param value
	 *            类型为:Map<String,String>
	 * 
	 */
	public void setCmdyReserves(Map<String, String> value) {
		if (value != null) {
			this.cmdyReserves = value;
			this.cmdyReserves_u = 1;
		}
	}

	public boolean issetCmdyReserves() {
		return this.cmdyReserves_u != 0;
	}

	/**
	 * 获取商品维度扩展信息集合标识
	 * 
	 * 此字段的版本 >= 4
	 * 
	 * @return cmdyReserves_u value 类型为:short
	 * 
	 */
	public short getCmdyReserves_u() {
		return cmdyReserves_u;
	}

	/**
	 * 设置商品维度扩展信息集合标识
	 * 
	 * 此字段的版本 >= 4
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setCmdyReserves_u(short value) {
		this.cmdyReserves_u = value;
	}

	/**
	 * 获取网购券id
	 * 
	 * 此字段的版本 >= 5
	 * 
	 * @return onlineShoppingCoupons value 类型为:long
	 * 
	 */
	public long getOnlineShoppingCoupons() {
		return onlineShoppingCoupons;
	}

	/**
	 * 设置网购券id
	 * 
	 * 此字段的版本 >= 5
	 * 
	 * @param value
	 *            类型为:long
	 * 
	 */
	public void setOnlineShoppingCoupons(long value) {
		this.onlineShoppingCoupons = value;
		this.onlineShoppingCoupons_u = 1;
	}

	public boolean issetOnlineShoppingCoupons() {
		return this.onlineShoppingCoupons_u != 0;
	}

	/**
	 * 获取商品skuid标识
	 * 
	 * 此字段的版本 >= 5
	 * 
	 * @return onlineShoppingCoupons_u value 类型为:short
	 * 
	 */
	public short getOnlineShoppingCoupons_u() {
		return onlineShoppingCoupons_u;
	}

	/**
	 * 设置商品skuid标识
	 * 
	 * 此字段的版本 >= 5
	 * 
	 * @param value
	 *            类型为:short
	 * 
	 */
	public void setOnlineShoppingCoupons_u(short value) {
		this.onlineShoppingCoupons_u = value;
	}

    /**
     * 获取红包积分
     *
     * 此字段的版本 >= 6
     * @return RedBagScore value 类型为:long
     *
     */
    public long getRedBagScore()
    {
        return RedBagScore;
    }

    /**
     * 设置红包积分
     *
     * 此字段的版本 >= 6
     * @param  value 类型为:long
     *
     */
    public void setRedBagScore(long value)
    {
        this.RedBagScore = value;
    }

	/**
	 * 计算类长度 用于告诉解包者，该类只放了这么长的数据
	 * 
	 */
	protected int getClassSize() {
		int length = getSize() - 4;
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
		return length;
	}

	/**
	 * 计算类长度 这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 * 
	 */
	public int getSize() {
		int length = 4;
		try {
			length = 4; // size_of(ConfirmV2Req)
			length += 4; // 计算字段version的长度 size_of(uint32_t)
			length += 4; // 计算字段Uin的长度 size_of(uint32_t)
			length += 4; // 计算字段BuyNum的长度 size_of(uint32_t)
			length += 4; // 计算字段MailType的长度 size_of(uint32_t)
			length += 4; // 计算字段PromotionId的长度 size_of(uint32_t)
			length += ByteStream.getObjectSize(ItemId, null); // 计算字段ItemId的长度
																// size_of(String)
			length += ByteStream.getObjectSize(RecvAddr, null); // 计算字段RecvAddr的长度
																// size_of(String)
			length += ByteStream.getObjectSize(RecvName, null); // 计算字段RecvName的长度
																// size_of(String)
			length += ByteStream.getObjectSize(RecvMobile, null); // 计算字段RecvMobile的长度
																	// size_of(String)
			length += ByteStream.getObjectSize(RecvPhone, null); // 计算字段RecvPhone的长度
																	// size_of(String)
			length += ByteStream.getObjectSize(RecvPost, null); // 计算字段RecvPost的长度
																// size_of(String)
			length += 4; // 计算字段CmdyType的长度 size_of(uint32_t)
			length += ByteStream.getObjectSize(ItemAttr, null); // 计算字段ItemAttr的长度
																// size_of(String)
			length += 4; // 计算字段Anonymous的长度 size_of(uint32_t)
			length += 4; // 计算字段OrderFrom的长度 size_of(uint32_t)
			length += 1; // 计算字段SaveAddr的长度 size_of(uint8_t)
			length += ByteStream.getObjectSize(InnoviceTitle, null); // 计算字段InnoviceTitle的长度
																		// size_of(String)
			length += 4; // 计算字段PriceType的长度 size_of(uint32_t)
			length += 4; // 计算字段PayType的长度 size_of(uint32_t)
			length += ByteStream.getObjectSize(Refer, null); // 计算字段Refer的长度
																// size_of(String)
			length += ByteStream.getObjectSize(BuyerNote, null); // 计算字段BuyerNote的长度
																	// size_of(String)
			length += 4; // 计算字段RedPacketId的长度 size_of(uint32_t)
			length += 4; // 计算字段ShopcouponId的长度 size_of(uint32_t)
			length += 4; // 计算字段Addressid的长度 size_of(uint32_t)
			length += 4; // 计算字段RegionId的长度 size_of(uint32_t)
			length += ByteStream.getObjectSize(GameName, null); // 计算字段GameName的长度
																// size_of(String)
			length += ByteStream.getObjectSize(GameArea, null); // 计算字段GameArea的长度
																// size_of(String)
			length += 4; // 计算字段GameClass的长度 size_of(uint32_t)
			length += ByteStream.getObjectSize(GameId, null); // 计算字段GameId的长度
																// size_of(String)
			length += ByteStream.getObjectSize(MobileNum, null); // 计算字段MobileNum的长度
																	// size_of(String)
			length += ByteStream.getObjectSize(QQNum, null); // 计算字段QQNum的长度
																// size_of(String)
			length += 4; // 计算字段PaySocre的长度 size_of(uint32_t)
			length += ByteStream.getObjectSize(ComdyColor, null); // 计算字段ComdyColor的长度
																	// size_of(String)
			length += ByteStream.getObjectSize(ComdySize, null); // 计算字段ComdySize的长度
																	// size_of(String)
			length += ByteStream.getObjectSize(ExtInfoMap, null); // 计算字段ExtInfoMap的长度
																	// size_of(MultiMap)
			length += 1; // 计算字段OrderFrom_u的长度 size_of(uint8_t)
			length += 1; // 计算字段Uin_u的长度 size_of(uint8_t)
			length += 1; // 计算字段BuyNum_u的长度 size_of(uint8_t)
			length += 1; // 计算字段MailType_u的长度 size_of(uint8_t)
			length += 1; // 计算字段PriceType_u的长度 size_of(uint8_t)
			length += 1; // 计算字段PayType_u的长度 size_of(uint8_t)
			length += 1; // 计算字段RedPacketId_u的长度 size_of(uint8_t)
			length += 1; // 计算字段ShopcouponId_u的长度 size_of(uint8_t)
			length += 1; // 计算字段PromotionId_u的长度 size_of(uint8_t)
			length += 1; // 计算字段CmdyType_u的长度 size_of(uint8_t)
			length += 1; // 计算字段Anonymous_u的长度 size_of(uint8_t)
			length += 1; // 计算字段SaveAddr_u的长度 size_of(uint8_t)
			length += 1; // 计算字段InnoviceTitle_u的长度 size_of(uint8_t)
			length += 1; // 计算字段Refer_u的长度 size_of(uint8_t)
			length += 1; // 计算字段ItemId_u的长度 size_of(uint8_t)
			length += 1; // 计算字段ItemAttr_u的长度 size_of(uint8_t)
			length += 1; // 计算字段BuyerNote_u的长度 size_of(uint8_t)
			length += 1; // 计算字段Addressid_u的长度 size_of(uint8_t)
			length += 1; // 计算字段RegionId_u的长度 size_of(uint8_t)
			length += 1; // 计算字段GameName_u的长度 size_of(uint8_t)
			length += 1; // 计算字段GameArea_u的长度 size_of(uint8_t)
			length += 1; // 计算字段GameClass_u的长度 size_of(uint8_t)
			length += 1; // 计算字段GameId_u的长度 size_of(uint8_t)
			length += 1; // 计算字段MobileNum_u的长度 size_of(uint8_t)
			length += 1; // 计算字段PaySocre_u的长度 size_of(uint8_t)
			length += 1; // 计算字段ComdyColor_u的长度 size_of(uint8_t)
			length += 1; // 计算字段ComdySize_u的长度 size_of(uint8_t)
			length += 1; // 计算字段QQNum_u的长度 size_of(uint8_t)
			length += 1; // 计算字段ExtInfoMap_u的长度 size_of(uint8_t)
			if (this.version >= 1) {
				length += 1; // 计算字段version_u的长度 size_of(uint8_t)
			}
			if (this.version >= 1) {
				length += 1; // 计算字段RecvAddr_u的长度 size_of(uint8_t)
			}
			if (this.version >= 1) {
				length += 1; // 计算字段RecvName_u的长度 size_of(uint8_t)
			}
			if (this.version >= 1) {
				length += 1; // 计算字段RecvMobile_u的长度 size_of(uint8_t)
			}
			if (this.version >= 1) {
				length += 1; // 计算字段RecvPhone_u的长度 size_of(uint8_t)
			}
			if (this.version >= 1) {
				length += 1; // 计算字段RecvPost_u的长度 size_of(uint8_t)
			}
			if (this.version >= 2) {
				length += 17; // 计算字段FreeMailCardId的长度 size_of(uint64_t)
			}
			if (this.version >= 2) {
				length += 1; // 计算字段FreeMailCardId_u的长度 size_of(uint8_t)
			}
			if (this.version >= 3) {
				length += 17; // 计算字段ItemSkuId的长度 size_of(uint64_t)
			}
			if (this.version >= 3) {
				length += 1; // 计算字段ItemSkuId_u的长度 size_of(uint8_t)
			}
			if (this.version >= 4) {
				length += ByteStream.getObjectSize(cmdyReserves, null); // 计算字段cmdyReserves的长度
																		// size_of(Map)
			}
			if (this.version >= 4) {
				length += 1; // 计算字段cmdyReserves_u的长度 size_of(uint8_t)
			}
			if (this.version >= 5) {
				length += 17; // 计算字段onlineShoppingCoupons的长度 size_of(uint64_t)
			}
			if (this.version >= 5) {
				length += 1; // 计算字段onlineShoppingCoupons_u的长度 size_of(uint8_t)
			}
            if (this.version >= 6){
                length += 4;  //计算字段RedBagScore的长度 size_of(uint32_t)
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return length;
	}

	/**
	 * 计算类长度 这个是实现String字符集传入的方法
	 * 
	 */
	public int getSize(String encoding) {
		int length = 4;
		try {
			length = 4; // size_of(ConfirmV2Req)
			length += 4; // 计算字段version的长度 size_of(uint32_t)
			length += 4; // 计算字段Uin的长度 size_of(uint32_t)
			length += 4; // 计算字段BuyNum的长度 size_of(uint32_t)
			length += 4; // 计算字段MailType的长度 size_of(uint32_t)
			length += 4; // 计算字段PromotionId的长度 size_of(uint32_t)
			length += ByteStream.getObjectSize(ItemId, encoding); // 计算字段ItemId的长度
																	// size_of(String)
			length += ByteStream.getObjectSize(RecvAddr, encoding); // 计算字段RecvAddr的长度
																	// size_of(String)
			length += ByteStream.getObjectSize(RecvName, encoding); // 计算字段RecvName的长度
																	// size_of(String)
			length += ByteStream.getObjectSize(RecvMobile, encoding); // 计算字段RecvMobile的长度
																		// size_of(String)
			length += ByteStream.getObjectSize(RecvPhone, encoding); // 计算字段RecvPhone的长度
																		// size_of(String)
			length += ByteStream.getObjectSize(RecvPost, encoding); // 计算字段RecvPost的长度
																	// size_of(String)
			length += 4; // 计算字段CmdyType的长度 size_of(uint32_t)
			length += ByteStream.getObjectSize(ItemAttr, encoding); // 计算字段ItemAttr的长度
																	// size_of(String)
			length += 4; // 计算字段Anonymous的长度 size_of(uint32_t)
			length += 4; // 计算字段OrderFrom的长度 size_of(uint32_t)
			length += 1; // 计算字段SaveAddr的长度 size_of(uint8_t)
			length += ByteStream.getObjectSize(InnoviceTitle, encoding); // 计算字段InnoviceTitle的长度
																			// size_of(String)
			length += 4; // 计算字段PriceType的长度 size_of(uint32_t)
			length += 4; // 计算字段PayType的长度 size_of(uint32_t)
			length += ByteStream.getObjectSize(Refer, encoding); // 计算字段Refer的长度
																	// size_of(String)
			length += ByteStream.getObjectSize(BuyerNote, encoding); // 计算字段BuyerNote的长度
																		// size_of(String)
			length += 4; // 计算字段RedPacketId的长度 size_of(uint32_t)
			length += 4; // 计算字段ShopcouponId的长度 size_of(uint32_t)
			length += 4; // 计算字段Addressid的长度 size_of(uint32_t)
			length += 4; // 计算字段RegionId的长度 size_of(uint32_t)
			length += ByteStream.getObjectSize(GameName, encoding); // 计算字段GameName的长度
																	// size_of(String)
			length += ByteStream.getObjectSize(GameArea, encoding); // 计算字段GameArea的长度
																	// size_of(String)
			length += 4; // 计算字段GameClass的长度 size_of(uint32_t)
			length += ByteStream.getObjectSize(GameId, encoding); // 计算字段GameId的长度
																	// size_of(String)
			length += ByteStream.getObjectSize(MobileNum, encoding); // 计算字段MobileNum的长度
																		// size_of(String)
			length += ByteStream.getObjectSize(QQNum, encoding); // 计算字段QQNum的长度
																	// size_of(String)
			length += 4; // 计算字段PaySocre的长度 size_of(uint32_t)
			length += ByteStream.getObjectSize(ComdyColor, encoding); // 计算字段ComdyColor的长度
																		// size_of(String)
			length += ByteStream.getObjectSize(ComdySize, encoding); // 计算字段ComdySize的长度
																		// size_of(String)
			length += ByteStream.getObjectSize(ExtInfoMap, encoding); // 计算字段ExtInfoMap的长度
																		// size_of(MultiMap)
			length += 1; // 计算字段OrderFrom_u的长度 size_of(uint8_t)
			length += 1; // 计算字段Uin_u的长度 size_of(uint8_t)
			length += 1; // 计算字段BuyNum_u的长度 size_of(uint8_t)
			length += 1; // 计算字段MailType_u的长度 size_of(uint8_t)
			length += 1; // 计算字段PriceType_u的长度 size_of(uint8_t)
			length += 1; // 计算字段PayType_u的长度 size_of(uint8_t)
			length += 1; // 计算字段RedPacketId_u的长度 size_of(uint8_t)
			length += 1; // 计算字段ShopcouponId_u的长度 size_of(uint8_t)
			length += 1; // 计算字段PromotionId_u的长度 size_of(uint8_t)
			length += 1; // 计算字段CmdyType_u的长度 size_of(uint8_t)
			length += 1; // 计算字段Anonymous_u的长度 size_of(uint8_t)
			length += 1; // 计算字段SaveAddr_u的长度 size_of(uint8_t)
			length += 1; // 计算字段InnoviceTitle_u的长度 size_of(uint8_t)
			length += 1; // 计算字段Refer_u的长度 size_of(uint8_t)
			length += 1; // 计算字段ItemId_u的长度 size_of(uint8_t)
			length += 1; // 计算字段ItemAttr_u的长度 size_of(uint8_t)
			length += 1; // 计算字段BuyerNote_u的长度 size_of(uint8_t)
			length += 1; // 计算字段Addressid_u的长度 size_of(uint8_t)
			length += 1; // 计算字段RegionId_u的长度 size_of(uint8_t)
			length += 1; // 计算字段GameName_u的长度 size_of(uint8_t)
			length += 1; // 计算字段GameArea_u的长度 size_of(uint8_t)
			length += 1; // 计算字段GameClass_u的长度 size_of(uint8_t)
			length += 1; // 计算字段GameId_u的长度 size_of(uint8_t)
			length += 1; // 计算字段MobileNum_u的长度 size_of(uint8_t)
			length += 1; // 计算字段PaySocre_u的长度 size_of(uint8_t)
			length += 1; // 计算字段ComdyColor_u的长度 size_of(uint8_t)
			length += 1; // 计算字段ComdySize_u的长度 size_of(uint8_t)
			length += 1; // 计算字段QQNum_u的长度 size_of(uint8_t)
			length += 1; // 计算字段ExtInfoMap_u的长度 size_of(uint8_t)
			if (this.version >= 1) {
				length += 1; // 计算字段version_u的长度 size_of(uint8_t)
			}
			if (this.version >= 1) {
				length += 1; // 计算字段RecvAddr_u的长度 size_of(uint8_t)
			}
			if (this.version >= 1) {
				length += 1; // 计算字段RecvName_u的长度 size_of(uint8_t)
			}
			if (this.version >= 1) {
				length += 1; // 计算字段RecvMobile_u的长度 size_of(uint8_t)
			}
			if (this.version >= 1) {
				length += 1; // 计算字段RecvPhone_u的长度 size_of(uint8_t)
			}
			if (this.version >= 1) {
				length += 1; // 计算字段RecvPost_u的长度 size_of(uint8_t)
			}
			if (this.version >= 2) {
				length += 17; // 计算字段FreeMailCardId的长度 size_of(uint64_t)
			}
			if (this.version >= 2) {
				length += 1; // 计算字段FreeMailCardId_u的长度 size_of(uint8_t)
			}
			if (this.version >= 3) {
				length += 17; // 计算字段ItemSkuId的长度 size_of(uint64_t)
			}
			if (this.version >= 3) {
				length += 1; // 计算字段ItemSkuId_u的长度 size_of(uint8_t)
			}
			if (this.version >= 4) {
				length += ByteStream.getObjectSize(cmdyReserves, encoding); // 计算字段cmdyReserves的长度
																			// size_of(Map)
			}
			if (this.version >= 4) {
				length += 1; // 计算字段cmdyReserves_u的长度 size_of(uint8_t)
			}
			if (this.version >= 5) {
				length += 17; // 计算字段onlineShoppingCoupons的长度 size_of(uint64_t)
			}
			if (this.version >= 5) {
				length += 1; // 计算字段onlineShoppingCoupons_u的长度 size_of(uint8_t)
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return length;
	}

	/**
	 ******************** 以下信息是每个版本的字段********************
	 * 
	 ***** 以下是版本1所包含的字段******* long version;///<版本号 long Uin;///<买家号 long
	 * BuyNum;///<购买数量 long MailType;///<邮递类型 long PromotionId;///<促销规则id String
	 * ItemId;///<商品id String RecvAddr;///<收货地址 String RecvName;///<收货姓名 String
	 * RecvMobile;///<收货手机 String RecvPhone;///<收货电话 String RecvPost;///<收货邮编
	 * long CmdyType;///<商品类型 String ItemAttr;///<商品属性 long Anonymous;///<是否匿名购买
	 * long OrderFrom;///<下单来源 short SaveAddr;///<是否保存虚拟地址 String
	 * InnoviceTitle;///<发票名称 long PriceType;///<价格类型 long PayType;///<支付类型
	 * String Refer;///<拍拍设置的cookie(_p),请设置 String BuyerNote;///<买家留言 long
	 * RedPacketId;///<红包id long ShopcouponId;///<店铺代金券id long
	 * Addressid;///<地址id long RegionId;///<区域id String GameName;///<游戏名 String
	 * GameArea;///<所在区服 long GameClass;///<充值类型 String GameId;///<游戏账号 String
	 * MobileNum;///<手机号码 String QQNum;///<qq号码 long PaySocre;///<使用的积分数 String
	 * ComdyColor;///<颜色 String ComdySize;///<尺码 MultiMap<String,String>
	 * ExtInfoMap;///<扩展信息 short OrderFrom_u; short Uin_u; short BuyNum_u; short
	 * MailType_u; short PriceType_u; short PayType_u; short RedPacketId_u;
	 * short ShopcouponId_u; short PromotionId_u; short CmdyType_u; short
	 * Anonymous_u; short SaveAddr_u; short InnoviceTitle_u; short Refer_u;
	 * short ItemId_u; short ItemAttr_u; short BuyerNote_u; short Addressid_u;
	 * short RegionId_u; short GameName_u; short GameArea_u; short GameClass_u;
	 * short GameId_u; short MobileNum_u; short PaySocre_u; short ComdyColor_u;
	 * short ComdySize_u; short QQNum_u; short ExtInfoMap_u; short
	 * version_u;///<版本号标识 short RecvAddr_u;///<收货地址标识 short
	 * RecvName_u;///<收货姓名标识 short RecvMobile_u;///<收货手机标识 short
	 * RecvPhone_u;///<收货电话标识 short RecvPost_u;///<收货邮编标识 以上是版本1所包含的字段*******
	 * 
	 ***** 以下是版本2所包含的字段******* long version;///<版本号 long Uin;///<买家号 long
	 * BuyNum;///<购买数量 long MailType;///<邮递类型 long PromotionId;///<促销规则id String
	 * ItemId;///<商品id String RecvAddr;///<收货地址 String RecvName;///<收货姓名 String
	 * RecvMobile;///<收货手机 String RecvPhone;///<收货电话 String RecvPost;///<收货邮编
	 * long CmdyType;///<商品类型 String ItemAttr;///<商品属性 long Anonymous;///<是否匿名购买
	 * long OrderFrom;///<下单来源 short SaveAddr;///<是否保存虚拟地址 String
	 * InnoviceTitle;///<发票名称 long PriceType;///<价格类型 long PayType;///<支付类型
	 * String Refer;///<拍拍设置的cookie(_p),请设置 String BuyerNote;///<买家留言 long
	 * RedPacketId;///<红包id long ShopcouponId;///<店铺代金券id long
	 * Addressid;///<地址id long RegionId;///<区域id String GameName;///<游戏名 String
	 * GameArea;///<所在区服 long GameClass;///<充值类型 String GameId;///<游戏账号 String
	 * MobileNum;///<手机号码 String QQNum;///<qq号码 long PaySocre;///<使用的积分数 String
	 * ComdyColor;///<颜色 String ComdySize;///<尺码 MultiMap<String,String>
	 * ExtInfoMap;///<扩展信息 short OrderFrom_u; short Uin_u; short BuyNum_u; short
	 * MailType_u; short PriceType_u; short PayType_u; short RedPacketId_u;
	 * short ShopcouponId_u; short PromotionId_u; short CmdyType_u; short
	 * Anonymous_u; short SaveAddr_u; short InnoviceTitle_u; short Refer_u;
	 * short ItemId_u; short ItemAttr_u; short BuyerNote_u; short Addressid_u;
	 * short RegionId_u; short GameName_u; short GameArea_u; short GameClass_u;
	 * short GameId_u; short MobileNum_u; short PaySocre_u; short ComdyColor_u;
	 * short ComdySize_u; short QQNum_u; short ExtInfoMap_u; short
	 * version_u;///<版本号标识 short RecvAddr_u;///<收货地址标识 short
	 * RecvName_u;///<收货姓名标识 short RecvMobile_u;///<收货手机标识 short
	 * RecvPhone_u;///<收货电话标识 short RecvPost_u;///<收货邮编标识 long
	 * FreeMailCardId;///<包邮卡ID short FreeMailCardId_u;///<包邮卡ID标识
	 * 以上是版本2所包含的字段*******
	 * 
	 ***** 以下是版本3所包含的字段******* long version;///<版本号 long Uin;///<买家号 long
	 * BuyNum;///<购买数量 long MailType;///<邮递类型 long PromotionId;///<促销规则id String
	 * ItemId;///<商品id String RecvAddr;///<收货地址 String RecvName;///<收货姓名 String
	 * RecvMobile;///<收货手机 String RecvPhone;///<收货电话 String RecvPost;///<收货邮编
	 * long CmdyType;///<商品类型 String ItemAttr;///<商品属性 long Anonymous;///<是否匿名购买
	 * long OrderFrom;///<下单来源 short SaveAddr;///<是否保存虚拟地址 String
	 * InnoviceTitle;///<发票名称 long PriceType;///<价格类型 long PayType;///<支付类型
	 * String Refer;///<拍拍设置的cookie(_p),请设置 String BuyerNote;///<买家留言 long
	 * RedPacketId;///<红包id long ShopcouponId;///<店铺代金券id long
	 * Addressid;///<地址id long RegionId;///<区域id String GameName;///<游戏名 String
	 * GameArea;///<所在区服 long GameClass;///<充值类型 String GameId;///<游戏账号 String
	 * MobileNum;///<手机号码 String QQNum;///<qq号码 long PaySocre;///<使用的积分数 String
	 * ComdyColor;///<颜色 String ComdySize;///<尺码 MultiMap<String,String>
	 * ExtInfoMap;///<扩展信息 short OrderFrom_u; short Uin_u; short BuyNum_u; short
	 * MailType_u; short PriceType_u; short PayType_u; short RedPacketId_u;
	 * short ShopcouponId_u; short PromotionId_u; short CmdyType_u; short
	 * Anonymous_u; short SaveAddr_u; short InnoviceTitle_u; short Refer_u;
	 * short ItemId_u; short ItemAttr_u; short BuyerNote_u; short Addressid_u;
	 * short RegionId_u; short GameName_u; short GameArea_u; short GameClass_u;
	 * short GameId_u; short MobileNum_u; short PaySocre_u; short ComdyColor_u;
	 * short ComdySize_u; short QQNum_u; short ExtInfoMap_u; short
	 * version_u;///<版本号标识 short RecvAddr_u;///<收货地址标识 short
	 * RecvName_u;///<收货姓名标识 short RecvMobile_u;///<收货手机标识 short
	 * RecvPhone_u;///<收货电话标识 short RecvPost_u;///<收货邮编标识 long
	 * FreeMailCardId;///<包邮卡ID short FreeMailCardId_u;///<包邮卡ID标识 long
	 * ItemSkuId;///<商品skuid short ItemSkuId_u;///<商品skuid标识 以上是版本3所包含的字段*******
	 * 
	 ***** 以下是版本4所包含的字段******* long version;///<版本号 long Uin;///<买家号 long
	 * BuyNum;///<购买数量 long MailType;///<邮递类型 long PromotionId;///<促销规则id String
	 * ItemId;///<商品id String RecvAddr;///<收货地址 String RecvName;///<收货姓名 String
	 * RecvMobile;///<收货手机 String RecvPhone;///<收货电话 String RecvPost;///<收货邮编
	 * long CmdyType;///<商品类型 String ItemAttr;///<商品属性 long Anonymous;///<是否匿名购买
	 * long OrderFrom;///<下单来源 short SaveAddr;///<是否保存虚拟地址 String
	 * InnoviceTitle;///<发票名称 long PriceType;///<价格类型 long PayType;///<支付类型
	 * String Refer;///<拍拍设置的cookie(_p),请设置 String BuyerNote;///<买家留言 long
	 * RedPacketId;///<红包id long ShopcouponId;///<店铺代金券id long
	 * Addressid;///<地址id long RegionId;///<区域id String GameName;///<游戏名 String
	 * GameArea;///<所在区服 long GameClass;///<充值类型 String GameId;///<游戏账号 String
	 * MobileNum;///<手机号码 String QQNum;///<qq号码 long PaySocre;///<使用的积分数 String
	 * ComdyColor;///<颜色 String ComdySize;///<尺码 MultiMap<String,String>
	 * ExtInfoMap;///<扩展信息 short OrderFrom_u; short Uin_u; short BuyNum_u; short
	 * MailType_u; short PriceType_u; short PayType_u; short RedPacketId_u;
	 * short ShopcouponId_u; short PromotionId_u; short CmdyType_u; short
	 * Anonymous_u; short SaveAddr_u; short InnoviceTitle_u; short Refer_u;
	 * short ItemId_u; short ItemAttr_u; short BuyerNote_u; short Addressid_u;
	 * short RegionId_u; short GameName_u; short GameArea_u; short GameClass_u;
	 * short GameId_u; short MobileNum_u; short PaySocre_u; short ComdyColor_u;
	 * short ComdySize_u; short QQNum_u; short ExtInfoMap_u; short
	 * version_u;///<版本号标识 short RecvAddr_u;///<收货地址标识 short
	 * RecvName_u;///<收货姓名标识 short RecvMobile_u;///<收货手机标识 short
	 * RecvPhone_u;///<收货电话标识 short RecvPost_u;///<收货邮编标识 long
	 * FreeMailCardId;///<包邮卡ID short FreeMailCardId_u;///<包邮卡ID标识 long
	 * ItemSkuId;///<商品skuid short ItemSkuId_u;///<商品skuid标识 Map<String,String>
	 * cmdyReserves;///<商品维度扩展信息集合 short cmdyReserves_u;///<商品维度扩展信息集合标识
	 * 以上是版本4所包含的字段*******
	 * 
	 ***** 以下是版本5所包含的字段******* long version;///<版本号 long Uin;///<买家号 long
	 * BuyNum;///<购买数量 long MailType;///<邮递类型 long PromotionId;///<促销规则id String
	 * ItemId;///<商品id String RecvAddr;///<收货地址 String RecvName;///<收货姓名 String
	 * RecvMobile;///<收货手机 String RecvPhone;///<收货电话 String RecvPost;///<收货邮编
	 * long CmdyType;///<商品类型 String ItemAttr;///<商品属性 long Anonymous;///<是否匿名购买
	 * long OrderFrom;///<下单来源 short SaveAddr;///<是否保存虚拟地址 String
	 * InnoviceTitle;///<发票名称 long PriceType;///<价格类型 long PayType;///<支付类型
	 * String Refer;///<拍拍设置的cookie(_p),请设置 String BuyerNote;///<买家留言 long
	 * RedPacketId;///<红包id long ShopcouponId;///<店铺代金券id long
	 * Addressid;///<地址id long RegionId;///<区域id String GameName;///<游戏名 String
	 * GameArea;///<所在区服 long GameClass;///<充值类型 String GameId;///<游戏账号 String
	 * MobileNum;///<手机号码 String QQNum;///<qq号码 long PaySocre;///<使用的积分数 String
	 * ComdyColor;///<颜色 String ComdySize;///<尺码 MultiMap<String,String>
	 * ExtInfoMap;///<扩展信息 short OrderFrom_u; short Uin_u; short BuyNum_u; short
	 * MailType_u; short PriceType_u; short PayType_u; short RedPacketId_u;
	 * short ShopcouponId_u; short PromotionId_u; short CmdyType_u; short
	 * Anonymous_u; short SaveAddr_u; short InnoviceTitle_u; short Refer_u;
	 * short ItemId_u; short ItemAttr_u; short BuyerNote_u; short Addressid_u;
	 * short RegionId_u; short GameName_u; short GameArea_u; short GameClass_u;
	 * short GameId_u; short MobileNum_u; short PaySocre_u; short ComdyColor_u;
	 * short ComdySize_u; short QQNum_u; short ExtInfoMap_u; short
	 * version_u;///<版本号标识 short RecvAddr_u;///<收货地址标识 short
	 * RecvName_u;///<收货姓名标识 short RecvMobile_u;///<收货手机标识 short
	 * RecvPhone_u;///<收货电话标识 short RecvPost_u;///<收货邮编标识 long
	 * FreeMailCardId;///<包邮卡ID short FreeMailCardId_u;///<包邮卡ID标识 long
	 * ItemSkuId;///<商品skuid short ItemSkuId_u;///<商品skuid标识 Map<String,String>
	 * cmdyReserves;///<商品维度扩展信息集合 short cmdyReserves_u;///<商品维度扩展信息集合标识 long
	 * onlineShoppingCoupons;///<网购券id short
	 * onlineShoppingCoupons_u;///<商品skuid标识 以上是版本5所包含的字段*******
	 */
    /*****以下是版本6所包含的字段********
     * long version;///<版本号
     *	long Uin;///<买家号
     *	long BuyNum;///<购买数量
     *	long MailType;///<邮递类型
     *	long PromotionId;///<促销规则id
     *	String ItemId;///<商品id
     *	String RecvAddr;///<收货地址
     *	String RecvName;///<收货姓名
     *	String RecvMobile;///<收货手机
     *	String RecvPhone;///<收货电话
     *	String RecvPost;///<收货邮编
     *	long CmdyType;///<商品类型
     *	String ItemAttr;///<商品属性
     *	long Anonymous;///<是否匿名购买
     *	long OrderFrom;///<下单来源
     *	short SaveAddr;///<是否保存虚拟地址
     *	String InnoviceTitle;///<发票名称
     *	long PriceType;///<价格类型
     *	long PayType;///<支付类型
     *	String Refer;///<拍拍设置的cookie(_p),请设置
     *	String BuyerNote;///<买家留言
     *	long RedPacketId;///<红包id
     *	long ShopcouponId;///<店铺代金券id
     *	long Addressid;///<地址id
     *	long RegionId;///<区域id
     *	String GameName;///<游戏名
     *	String GameArea;///<所在区服
     *	long GameClass;///<充值类型
     *	String GameId;///<游戏账号
     *	String MobileNum;///<手机号码
     *	String QQNum;///<qq号码
     *	long PaySocre;///<使用的积分数
     *	String ComdyColor;///<颜色
     *	String ComdySize;///<尺码
     *	MultiMap<String,String> ExtInfoMap;///<扩展信息
     *	short OrderFrom_u;
     *	short Uin_u;
     *	short BuyNum_u;
     *	short MailType_u;
     *	short PriceType_u;
     *	short PayType_u;
     *	short RedPacketId_u;
     *	short ShopcouponId_u;
     *	short PromotionId_u;
     *	short CmdyType_u;
     *	short Anonymous_u;
     *	short SaveAddr_u;
     *	short InnoviceTitle_u;
     *	short Refer_u;
     *	short ItemId_u;
     *	short ItemAttr_u;
     *	short BuyerNote_u;
     *	short Addressid_u;
     *	short RegionId_u;
     *	short GameName_u;
     *	short GameArea_u;
     *	short GameClass_u;
     *	short GameId_u;
     *	short MobileNum_u;
     *	short PaySocre_u;
     *	short ComdyColor_u;
     *	short ComdySize_u;
     *	short QQNum_u;
     *	short ExtInfoMap_u;
     *	short version_u;///<版本号标识
     *	short RecvAddr_u;///<收货地址标识
     *	short RecvName_u;///<收货姓名标识
     *	short RecvMobile_u;///<收货手机标识
     *	short RecvPhone_u;///<收货电话标识
     *	short RecvPost_u;///<收货邮编标识
     *	long FreeMailCardId;///<包邮卡ID
     *	short FreeMailCardId_u;///<包邮卡ID标识
     *	long ItemSkuId;///<商品skuid
     *	short ItemSkuId_u;///<商品skuid标识
     *	Map<String,String> cmdyReserves;///<商品维度扩展信息集合
     *	short cmdyReserves_u;///<商品维度扩展信息集合标识
     *	long onlineShoppingCoupons;///<网购券id
     *	short onlineShoppingCoupons_u;///<商品skuid标识
     *	long RedBagScore;///<红包积分
     *****以上是版本6所包含的字段********/
	/**
	 * 下面是生成toString()方法 此方法用于调试时打开* 如果要打开此方法，请加入commons-lang-2.4.jar 并导入 import
	 * org.apache.commons.lang.builder.ToStringBuilder; import
	 * org.apache.commons.lang.builder.ToStringStyle;
	 * 
	 */
	// @Override
	// public String toString() {
	// return
	// ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	// }
}
