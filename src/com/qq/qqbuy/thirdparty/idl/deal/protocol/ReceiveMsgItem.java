//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.deal.idl.DealService.java

package com.qq.qqbuy.thirdparty.idl.deal.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *上行短信业务实体
 *
 *@date 2012-12-07 10:51:37
 *
 *@since version:20110816
*/
public class ReceiveMsgItem  implements ICanSerializeObject
{
	/**
	 * 版本控制的字段
	 *
	 * 版本 >= 0
	 */
	 private int version = 20110816;

	/**
	 * 业务类型
	 *
	 * 版本 >= 0
	 */
	 private int buType;

	/**
	 * 业务流水号(用于区别某手机号的某条上行短信)
	 *
	 * 版本 >= 0
	 */
	 private long buId;

	/**
	 * 手机号
	 *
	 * 版本 >= 0
	 */
	 private String mobile = new String();

	/**
	 * 短信内容
	 *
	 * 版本 >= 0
	 */
	 private String content = new String();

	/**
	 * 手机归属地区
	 *
	 * 版本 >= 0
	 */
	 private String upStation = new String();

	/**
	 * 重复发送时间
	 *
	 * 版本 >= 0
	 */
	 private long createTime;

	/**
	 * req保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(version);
		bs.pushInt(buType);
		bs.pushUInt(buId);
		bs.pushString(mobile);
		bs.pushString(content);
		bs.pushString(upStation);
		bs.pushUInt(createTime);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		buType = bs.popInt();
		buId = bs.popUInt();
		mobile = bs.popString();
		content = bs.popString();
		upStation = bs.popString();
		createTime = bs.popUInt();
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取业务类型
	 * 
	 * 此字段的版本 >= 0
	 * @return buType value 类型为:int
	 * 
	 */
	public int getBuType()
	{
		return buType;
	}


	/**
	 * 设置业务类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setBuType(int value)
	{
		this.buType = value;
	}


	/**
	 * 获取业务流水号(用于区别某手机号的某条上行短信)
	 * 
	 * 此字段的版本 >= 0
	 * @return buId value 类型为:long
	 * 
	 */
	public long getBuId()
	{
		return buId;
	}


	/**
	 * 设置业务流水号(用于区别某手机号的某条上行短信)
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuId(long value)
	{
		this.buId = value;
	}


	/**
	 * 获取手机号
	 * 
	 * 此字段的版本 >= 0
	 * @return mobile value 类型为:String
	 * 
	 */
	public String getMobile()
	{
		return mobile;
	}


	/**
	 * 设置手机号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMobile(String value)
	{
		this.mobile = value;
	}


	/**
	 * 获取短信内容
	 * 
	 * 此字段的版本 >= 0
	 * @return content value 类型为:String
	 * 
	 */
	public String getContent()
	{
		return content;
	}


	/**
	 * 设置短信内容
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setContent(String value)
	{
		this.content = value;
	}


	/**
	 * 获取手机归属地区
	 * 
	 * 此字段的版本 >= 0
	 * @return upStation value 类型为:String
	 * 
	 */
	public String getUpStation()
	{
		return upStation;
	}


	/**
	 * 设置手机归属地区
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setUpStation(String value)
	{
		this.upStation = value;
	}


	/**
	 * 获取重复发送时间
	 * 
	 * 此字段的版本 >= 0
	 * @return createTime value 类型为:long
	 * 
	 */
	public long getCreateTime()
	{
		return createTime;
	}


	/**
	 * 设置重复发送时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCreateTime(long value)
	{
		this.createTime = value;
	}


	/**
	 * 获取req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ReceiveMsgItem)
				length += 4;  //计算字段version的长度 size_of(int)
				length += 4;  //计算字段buType的长度 size_of(int)
				length += 4;  //计算字段buId的长度 size_of(long)
				length += ByteStream.getObjectSize(mobile);  //计算字段mobile的长度 size_of(String)
				length += ByteStream.getObjectSize(content);  //计算字段content的长度 size_of(String)
				length += ByteStream.getObjectSize(upStation);  //计算字段upStation的长度 size_of(String)
				length += 4;  //计算字段createTime的长度 size_of(long)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
