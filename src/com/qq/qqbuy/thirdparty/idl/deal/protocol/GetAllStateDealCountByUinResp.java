 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.deal.idl.DealService.java

package com.qq.qqbuy.thirdparty.idl.deal.protocol;

import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Map;
import java.util.HashMap;

/**
 *
 *
 *@date 2012-12-07 10:51:37
 *
 *@since version:1
*/
public class  GetAllStateDealCountByUinResp implements IServiceObject
{
	public long result;
	/**
	 * 错误码,errCode为0表示调用成功,非0表示错误
	 *
	 * 版本 >= 0
	 */
	 private int errCode;

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 查询QQ号
	 *
	 * 版本 >= 0
	 */
	 private long ruin;

	/**
	 * PAIPAI各状态订单数量map：其中：key从1~99表示paipai订单状态key=3表示买家已付款，等待发货key=4表示等待买家付款key=5表示卖家已发货，等待买家收货
	 *
	 * 版本 >= 0
	 */
	 private Map<Integer,Integer> paipaiDealCount = new HashMap<Integer,Integer>();

	/**
	 * 货到付款各状态订单数,key100~199100：所有货到付款状态101：待买家确认102：待卖家确认103：待卖家发货106：待确认收货107：交易成功108：买家拒收109：交易关闭
	 *
	 * 版本 >= 0
	 */
	 private Map<Integer,Integer> codDealCount = new HashMap<Integer,Integer>();

	/**
	 * 在线付款各状态订单数,key200~299200：所有在线支付状态201：待买家付款202：等待发货203：卖家已发货204：交易成功205：交易关闭
	 *
	 * 版本 >= 0
	 */
	 private Map<Integer,Integer> onlinePayDealCount = new HashMap<Integer,Integer>();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushInt(errCode);
		bs.pushString(errMsg);
		bs.pushUInt(ruin);
		bs.pushObject(paipaiDealCount);
		bs.pushObject(codDealCount);
		bs.pushObject(onlinePayDealCount);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		errCode = bs.popInt();
		errMsg = bs.popString();
		ruin = bs.popUInt();
		paipaiDealCount = (Map<Integer,Integer>)bs.popMap(Integer.class,Integer.class);
		codDealCount = (Map<Integer,Integer>)bs.popMap(Integer.class,Integer.class);
		onlinePayDealCount = (Map<Integer,Integer>)bs.popMap(Integer.class,Integer.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80088801L;
	}


	/**
	 * 获取错误码,errCode为0表示调用成功,非0表示错误
	 * 
	 * 此字段的版本 >= 0
	 * @return errCode value 类型为:int
	 * 
	 */
	public int getErrCode()
	{
		return errCode;
	}


	/**
	 * 设置错误码,errCode为0表示调用成功,非0表示错误
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setErrCode(int value)
	{
		this.errCode = value;
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	/**
	 * 获取查询QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return ruin value 类型为:long
	 * 
	 */
	public long getRuin()
	{
		return ruin;
	}


	/**
	 * 设置查询QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRuin(long value)
	{
		this.ruin = value;
	}


	/**
	 * 获取PAIPAI各状态订单数量map：其中：key从1~99表示paipai订单状态key=3表示买家已付款，等待发货key=4表示等待买家付款key=5表示卖家已发货，等待买家收货
	 * 
	 * 此字段的版本 >= 0
	 * @return paipaiDealCount value 类型为:Map<Integer,Integer>
	 * 
	 */
	public Map<Integer,Integer> getPaipaiDealCount()
	{
		return paipaiDealCount;
	}


	/**
	 * 设置PAIPAI各状态订单数量map：其中：key从1~99表示paipai订单状态key=3表示买家已付款，等待发货key=4表示等待买家付款key=5表示卖家已发货，等待买家收货
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<Integer,Integer>
	 * 
	 */
	public void setPaipaiDealCount(Map<Integer,Integer> value)
	{
		if (value != null) {
				this.paipaiDealCount = value;
		}else{
				this.paipaiDealCount = new HashMap<Integer,Integer>();
		}
	}


	/**
	 * 获取货到付款各状态订单数,key100~199100：所有货到付款状态101：待买家确认102：待卖家确认103：待卖家发货106：待确认收货107：交易成功108：买家拒收109：交易关闭
	 * 
	 * 此字段的版本 >= 0
	 * @return codDealCount value 类型为:Map<Integer,Integer>
	 * 
	 */
	public Map<Integer,Integer> getCodDealCount()
	{
		return codDealCount;
	}


	/**
	 * 设置货到付款各状态订单数,key100~199100：所有货到付款状态101：待买家确认102：待卖家确认103：待卖家发货106：待确认收货107：交易成功108：买家拒收109：交易关闭
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<Integer,Integer>
	 * 
	 */
	public void setCodDealCount(Map<Integer,Integer> value)
	{
		if (value != null) {
				this.codDealCount = value;
		}else{
				this.codDealCount = new HashMap<Integer,Integer>();
		}
	}


	/**
	 * 获取在线付款各状态订单数,key200~299200：所有在线支付状态201：待买家付款202：等待发货203：卖家已发货204：交易成功205：交易关闭
	 * 
	 * 此字段的版本 >= 0
	 * @return onlinePayDealCount value 类型为:Map<Integer,Integer>
	 * 
	 */
	public Map<Integer,Integer> getOnlinePayDealCount()
	{
		return onlinePayDealCount;
	}


	/**
	 * 设置在线付款各状态订单数,key200~299200：所有在线支付状态201：待买家付款202：等待发货203：卖家已发货204：交易成功205：交易关闭
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<Integer,Integer>
	 * 
	 */
	public void setOnlinePayDealCount(Map<Integer,Integer> value)
	{
		if (value != null) {
				this.onlinePayDealCount = value;
		}else{
				this.onlinePayDealCount = new HashMap<Integer,Integer>();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetAllStateDealCountByUinResp)
				length += 4;  //计算字段errCode的长度 size_of(int)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += 4;  //计算字段ruin的长度 size_of(long)
				length += ByteStream.getObjectSize(paipaiDealCount);  //计算字段paipaiDealCount的长度 size_of(Map)
				length += ByteStream.getObjectSize(codDealCount);  //计算字段codDealCount的长度 size_of(Map)
				length += ByteStream.getObjectSize(onlinePayDealCount);  //计算字段onlinePayDealCount的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
