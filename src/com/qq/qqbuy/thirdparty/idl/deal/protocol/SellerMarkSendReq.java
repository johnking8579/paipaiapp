 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.deal.idl.DealService.java

package com.qq.qqbuy.thirdparty.idl.deal.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 *@date 2012-12-07 10:51:37
 *
 *@since version:1
*/
public class  SellerMarkSendReq implements IServiceObject
{
	/**
	 * 卖家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long sellerUin;

	/**
	 * 标记发货的信息列表
	 *
	 * 版本 >= 0
	 */
	 private List<MarkSendInfo> markSendInfos = new ArrayList<MarkSendInfo>();

	/**
	 * req保留字段
	 *
	 * 版本 >= 0
	 */
	 private String inReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(sellerUin);
		bs.pushObject(markSendInfos);
		bs.pushString(inReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		sellerUin = bs.popUInt();
		markSendInfos = (List<MarkSendInfo>)bs.popList(ArrayList.class,MarkSendInfo.class);
		inReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80081812L;
	}


	/**
	 * 获取卖家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return sellerUin;
	}


	/**
	 * 设置卖家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.sellerUin = value;
	}


	/**
	 * 获取标记发货的信息列表
	 * 
	 * 此字段的版本 >= 0
	 * @return markSendInfos value 类型为:List<MarkSendInfo>
	 * 
	 */
	public List<MarkSendInfo> getMarkSendInfos()
	{
		return markSendInfos;
	}


	/**
	 * 设置标记发货的信息列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:List<MarkSendInfo>
	 * 
	 */
	public void setMarkSendInfos(List<MarkSendInfo> value)
	{
		if (value != null) {
				this.markSendInfos = value;
		}else{
				this.markSendInfos = new ArrayList<MarkSendInfo>();
		}
	}


	/**
	 * 获取req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserved value 类型为:String
	 * 
	 */
	public String getInReserved()
	{
		return inReserved;
	}


	/**
	 * 设置req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserved(String value)
	{
		this.inReserved = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(SellerMarkSendReq)
				length += 4;  //计算字段sellerUin的长度 size_of(long)
				length += ByteStream.getObjectSize(markSendInfos);  //计算字段markSendInfos的长度 size_of(List)
				length += ByteStream.getObjectSize(inReserved);  //计算字段inReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
