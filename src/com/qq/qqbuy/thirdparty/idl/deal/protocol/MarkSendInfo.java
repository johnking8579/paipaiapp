//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.deal.idl.DealService.java

package com.qq.qqbuy.thirdparty.idl.deal.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *标记发货请求信息
 *
 *@date 2012-12-07 10:51:37
 *
 *@since version:20110816
*/
public class MarkSendInfo  implements ICanSerializeObject
{
	/**
	 * 版本控制的字段
	 *
	 * 版本 >= 0
	 */
	 private int version = 20110816;

	/**
	 * 订单编码
	 *
	 * 版本 >= 0
	 */
	 private String dealCode = new String();

	/**
	 * 物流公司ID
	 *
	 * 版本 >= 0
	 */
	 private long companyId;

	/**
	 * 订单的物流编号
	 *
	 * 版本 >= 0
	 */
	 private String wuliuCode = new String();

	/**
	 * 标记发货时间，格式：yyyy-MM-dd hh:mm:ss
	 *
	 * 版本 >= 0
	 */
	 private String sendTime = new String();

	/**
	 * 预期收货时间，以天为单位
	 *
	 * 版本 >= 0
	 */
	 private int expectTime;

	/**
	 * req保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(version);
		bs.pushString(dealCode);
		bs.pushUInt(companyId);
		bs.pushString(wuliuCode);
		bs.pushString(sendTime);
		bs.pushInt(expectTime);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		dealCode = bs.popString();
		companyId = bs.popUInt();
		wuliuCode = bs.popString();
		sendTime = bs.popString();
		expectTime = bs.popInt();
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取订单编码
	 * 
	 * 此字段的版本 >= 0
	 * @return dealCode value 类型为:String
	 * 
	 */
	public String getDealCode()
	{
		return dealCode;
	}


	/**
	 * 设置订单编码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealCode(String value)
	{
		this.dealCode = value;
	}


	/**
	 * 获取物流公司ID
	 * 
	 * 此字段的版本 >= 0
	 * @return companyId value 类型为:long
	 * 
	 */
	public long getCompanyId()
	{
		return companyId;
	}


	/**
	 * 设置物流公司ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCompanyId(long value)
	{
		this.companyId = value;
	}


	/**
	 * 获取订单的物流编号
	 * 
	 * 此字段的版本 >= 0
	 * @return wuliuCode value 类型为:String
	 * 
	 */
	public String getWuliuCode()
	{
		return wuliuCode;
	}


	/**
	 * 设置订单的物流编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setWuliuCode(String value)
	{
		this.wuliuCode = value;
	}


	/**
	 * 获取标记发货时间，格式：yyyy-MM-dd hh:mm:ss
	 * 
	 * 此字段的版本 >= 0
	 * @return sendTime value 类型为:String
	 * 
	 */
	public String getSendTime()
	{
		return sendTime;
	}


	/**
	 * 设置标记发货时间，格式：yyyy-MM-dd hh:mm:ss
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSendTime(String value)
	{
		this.sendTime = value;
	}


	/**
	 * 获取预期收货时间，以天为单位
	 * 
	 * 此字段的版本 >= 0
	 * @return expectTime value 类型为:int
	 * 
	 */
	public int getExpectTime()
	{
		return expectTime;
	}


	/**
	 * 设置预期收货时间，以天为单位
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setExpectTime(int value)
	{
		this.expectTime = value;
	}


	/**
	 * 获取req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(MarkSendInfo)
				length += 4;  //计算字段version的长度 size_of(int)
				length += ByteStream.getObjectSize(dealCode);  //计算字段dealCode的长度 size_of(String)
				length += 4;  //计算字段companyId的长度 size_of(long)
				length += ByteStream.getObjectSize(wuliuCode);  //计算字段wuliuCode的长度 size_of(String)
				length += ByteStream.getObjectSize(sendTime);  //计算字段sendTime的长度 size_of(String)
				length += 4;  //计算字段expectTime的长度 size_of(int)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
