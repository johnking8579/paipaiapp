//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.deal.idl.DealService.java

package com.qq.qqbuy.thirdparty.idl.deal.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *下单时订单信息
 *
 *@date 2012-12-07 10:51:37
 *
 *@since version:20110816
*/
public class DealReqInfo  implements ICanSerializeObject
{
	/**
	 * 版本控制的字段
	 *
	 * 版本 >= 0
	 */
	 private int version = 20110816;

	/**
	 * 订单类型[255=在线支付, 256=货到付款]
	 *
	 * 版本 >= 0
	 */
	 private int payType;

	/**
	 * 订单描述
	 *
	 * 版本 >= 0
	 */
	 private String dealDesc = new String();

	/**
	 * 买家附言
	 *
	 * 版本 >= 0
	 */
	 private String buyerNote = new String();

	/**
	 * 标志位
	 *
	 * 版本 >= 0
	 */
	 private long flags;

	/**
	 * 是否匿名 1是0否
	 *
	 * 版本 >= 0
	 */
	 private int anonymous;

	/**
	 * req保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(version);
		bs.pushInt(payType);
		bs.pushString(dealDesc);
		bs.pushString(buyerNote);
		bs.pushUInt(flags);
		bs.pushInt(anonymous);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		payType = bs.popInt();
		dealDesc = bs.popString();
		buyerNote = bs.popString();
		flags = bs.popUInt();
		anonymous = bs.popInt();
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取订单类型[255=在线支付, 256=货到付款]
	 * 
	 * 此字段的版本 >= 0
	 * @return payType value 类型为:int
	 * 
	 */
	public int getPayType()
	{
		return payType;
	}


	/**
	 * 设置订单类型[255=在线支付, 256=货到付款]
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPayType(int value)
	{
		this.payType = value;
	}


	/**
	 * 获取订单描述
	 * 
	 * 此字段的版本 >= 0
	 * @return dealDesc value 类型为:String
	 * 
	 */
	public String getDealDesc()
	{
		return dealDesc;
	}


	/**
	 * 设置订单描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealDesc(String value)
	{
		this.dealDesc = value;
	}


	/**
	 * 获取买家附言
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerNote value 类型为:String
	 * 
	 */
	public String getBuyerNote()
	{
		return buyerNote;
	}


	/**
	 * 设置买家附言
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBuyerNote(String value)
	{
		this.buyerNote = value;
	}


	/**
	 * 获取标志位
	 * 
	 * 此字段的版本 >= 0
	 * @return flags value 类型为:long
	 * 
	 */
	public long getFlags()
	{
		return flags;
	}


	/**
	 * 设置标志位
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFlags(long value)
	{
		this.flags = value;
	}


	/**
	 * 获取是否匿名 1是0否
	 * 
	 * 此字段的版本 >= 0
	 * @return anonymous value 类型为:int
	 * 
	 */
	public int getAnonymous()
	{
		return anonymous;
	}


	/**
	 * 设置是否匿名 1是0否
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setAnonymous(int value)
	{
		this.anonymous = value;
	}


	/**
	 * 获取req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(DealReqInfo)
				length += 4;  //计算字段version的长度 size_of(int)
				length += 4;  //计算字段payType的长度 size_of(int)
				length += ByteStream.getObjectSize(dealDesc);  //计算字段dealDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(buyerNote);  //计算字段buyerNote的长度 size_of(String)
				length += 4;  //计算字段flags的长度 size_of(long)
				length += 4;  //计算字段anonymous的长度 size_of(int)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
