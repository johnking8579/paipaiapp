 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.deal.idl.DealService.java

package com.qq.qqbuy.thirdparty.idl.deal.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Map;
import java.util.HashMap;

/**
 *
 *
 *@date 2012-12-07 10:51:37
 *
 *@since version:1
*/
public class  SellerModifyPriceReq implements IServiceObject
{
	/**
	 * 订单编号
	 *
	 * 版本 >= 0
	 */
	 private String dealCode = new String();

	/**
	 * 关闭者uin
	 *
	 * 版本 >= 0
	 */
	 private long uin;

	/**
	 * 新订单金额
	 *
	 * 版本 >= 0
	 */
	 private int newPrice;

	/**
	 * 新子订单金额,该字段不为空则newPrice字段无效
	 *
	 * 版本 >= 0
	 */
	 private Map<String,Integer> tradePrice = new HashMap<String,Integer>();

	/**
	 * skey
	 *
	 * 版本 >= 0
	 */
	 private String skey = new String();

	/**
	 * 调用者
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * req保留字段
	 *
	 * 版本 >= 0
	 */
	 private String inReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(dealCode);
		bs.pushUInt(uin);
		bs.pushInt(newPrice);
		bs.pushObject(tradePrice);
		bs.pushString(skey);
		bs.pushString(source);
		bs.pushString(inReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		dealCode = bs.popString();
		uin = bs.popUInt();
		newPrice = bs.popInt();
		tradePrice = (Map<String,Integer>)bs.popMap(String.class,Integer.class);
		skey = bs.popString();
		source = bs.popString();
		inReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80081824L;
	}


	/**
	 * 获取订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @return dealCode value 类型为:String
	 * 
	 */
	public String getDealCode()
	{
		return dealCode;
	}


	/**
	 * 设置订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealCode(String value)
	{
		this.dealCode = value;
	}


	/**
	 * 获取关闭者uin
	 * 
	 * 此字段的版本 >= 0
	 * @return uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return uin;
	}


	/**
	 * 设置关闭者uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.uin = value;
	}


	/**
	 * 获取新订单金额
	 * 
	 * 此字段的版本 >= 0
	 * @return newPrice value 类型为:int
	 * 
	 */
	public int getNewPrice()
	{
		return newPrice;
	}


	/**
	 * 设置新订单金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setNewPrice(int value)
	{
		this.newPrice = value;
	}


	/**
	 * 获取新子订单金额,该字段不为空则newPrice字段无效
	 * 
	 * 此字段的版本 >= 0
	 * @return tradePrice value 类型为:Map<String,Integer>
	 * 
	 */
	public Map<String,Integer> getTradePrice()
	{
		return tradePrice;
	}


	/**
	 * 设置新子订单金额,该字段不为空则newPrice字段无效
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,Integer>
	 * 
	 */
	public void setTradePrice(Map<String,Integer> value)
	{
		if (value != null) {
				this.tradePrice = value;
		}else{
				this.tradePrice = new HashMap<String,Integer>();
		}
	}


	/**
	 * 获取skey
	 * 
	 * 此字段的版本 >= 0
	 * @return skey value 类型为:String
	 * 
	 */
	public String getSkey()
	{
		return skey;
	}


	/**
	 * 设置skey
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSkey(String value)
	{
		this.skey = value;
	}


	/**
	 * 获取调用者
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置调用者
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserved value 类型为:String
	 * 
	 */
	public String getInReserved()
	{
		return inReserved;
	}


	/**
	 * 设置req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserved(String value)
	{
		this.inReserved = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(SellerModifyPriceReq)
				length += ByteStream.getObjectSize(dealCode);  //计算字段dealCode的长度 size_of(String)
				length += 4;  //计算字段uin的长度 size_of(long)
				length += 4;  //计算字段newPrice的长度 size_of(int)
				length += ByteStream.getObjectSize(tradePrice);  //计算字段tradePrice的长度 size_of(Map)
				length += ByteStream.getObjectSize(skey);  //计算字段skey的长度 size_of(String)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserved);  //计算字段inReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
