package com.qq.qqbuy.thirdparty.idl.favorite;


import java.util.Vector;

import com.paipai.lang.uint32_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.qq.qqbuy.thirdparty.idl.favorite.FavoritePo.AddFavShopPo;
import com.qq.qqbuy.thirdparty.idl.favorite.FavoritePo.FavFilterPo;
import com.qq.qqbuy.thirdparty.idl.favorite.FavoritePo.FavItemListPo;
import com.qq.qqbuy.thirdparty.idl.favorite.FavoritePo.FavShopFilter;
import com.qq.qqbuy.thirdparty.idl.favorite.FavoritePo.FavShopPo;
import com.qq.qqbuy.thirdparty.idl.favorite.FavoritePo.FavShopListPo;

@HeadApiProtocol(cPlusNamespace = "c2cent::ao::favoriteapi", needInit = true)
class FavoriteApi
{
	@ApiProtocol(cmdid = "0x69091801L", desc = "增加一个店铺到收藏列表中，需要带登录态进行操作")
	class AddFavShop{
		@ApiProtocol(cmdid = "0x69091801L", desc = "增加一个店铺到收藏列表中，需要带登录态进行操作")
		class Req 
		{
			@Field(desc = "机器码(即visitkey)，不能为空")
			String MachineKey;

			@Field(desc = "请求方来源,请填为调用方的代码文件名.不能为空,为空则返回失败")
			String Source;
			
			@Field(desc = "调用方业务id，不能为空,请找基础组申请")
			uint32_t BusinessId;
			
			@Field(desc = "收藏店铺po，具体定义参考FavoritePo文件",cPlusNamespace="AddFavShopPo|c2cent::po::favorite",header="FavShopPo|c2cent/po/favorite/addfavshoppo_po.h")
			AddFavShopPo FavShopPo;
			
			@Field(desc = "请求参数的保留字段")
			String InReserve;
		}

		@ApiProtocol(cmdid = "0x69098801L", desc = "查询商品详细信息返回类")
		class Resp 
		{
			@Field(desc = "错误信息,调试使用")
			String errmsg;

			@Field(desc = "返回保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0x69091802L", desc = "查询指定用户是否收藏了指定店铺")
	class GetFavShopDetail{
		@ApiProtocol(cmdid = "0x69091802L", desc = "查询指定用户是否收藏了指定店铺")
		class Req {
			@Field(desc = "机器码(即visitkey)，不能为空")
			String MachineKey;

			@Field(desc = "请求方来源,请填为调用方的代码文件名.不能为空,为空则返回失败")
			String Source;
			
			@Field(desc = "调用方业务id，不能为空,请找基础组申请")
			uint32_t BusinessId;
			
			@Field(desc = "查询收藏店铺fiter，具体定义参考FavoritePo文件",cPlusNamespace="FavShopPo|c2cent::po::favorite",header="FavShopPo|c2cent/po/favorite/favshopfilter_po.h")
			FavShopFilter FavShopFilter;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0x69098802L", desc = "查询指定用户是否收藏了指定店铺返回结果")
		class Resp {
			@Field(desc = "收藏店铺po信息", cPlusNamespace="FavShopPo|c2cent::po::favorite",header="FavShopPo|c2cent/po/favorite/favshoppo_po.h")
			FavShopPo FavShop;
			
			@Field(desc = "错误信息,调试使用")
			String errmsg;

			@Field(desc = "返回保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0x69091803L", desc = "批量增加商品收藏")
	public class AddFavItemList
	{
		@ApiProtocol(cmdid = "0x69091803L", desc = "批量增加商品收藏请求类")
		class Req
		{
			@Field(desc = "机器码")
			String mechineKey;
			
			@Field(desc = "调用来源")
			String source;
			
			@Field(desc = "请求保留字")
			String inReserve;
			
			@Field(desc = "用户qq号")
			uint32_t uin;
			
			@Field(desc = "收藏的商品信息vector")
			Vector<String> addItemIdList;
		}
		
		@ApiProtocol(cmdid = "0x69098803L", desc = "批量增加商品收藏返回类")
		class Resp
		{	
			@Field(desc = "添加结果")
			Vector<uint32_t> addResult;
			
			@Field(desc = "总的收藏数")
			uint32_t totalCount;
			
			@Field(desc = "错误提示信息")
			String errMsg;
									
			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0x69091804L", desc = "取商品信息列表")
	public class GetFavItemList
	{
		@ApiProtocol(cmdid = "0x69091804L", desc = "取商品信息列表请求类")
		class Req
		{
			@Field(desc = "机器码")
			String mechineKey;
			
			@Field(desc = "调用来源")
			String source;
			
			@Field(desc = "查询filter", cPlusNamespace="FavFilterPo|c2cent::po::favorite",header="FavFilterPo|c2cent/po/favorite/favfilterpo_po.h")
			FavFilterPo favItemFiter;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}
		
		@ApiProtocol(cmdid = "0x69098804L",desc = "取商品信息列表返回类")
		class Resp
		{	
			@Field(desc = "商品信息列表", cPlusNamespace="FavItemListPo|c2cent::po::favorite",header="FavItemListPo|c2cent/po/favorite/favitemlistpo_po.h")
			FavItemListPo favItemList;
			
			@Field(desc = "错误提示信息")
			String errMsg;
									
			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	
	@ApiProtocol(cmdid = "0x69091805L", desc = "删除收藏的商品")
	public class DelFavItemList
	{
		@ApiProtocol(cmdid = "0x69091805L", desc = "删除收藏的商品请求类")
		class Req
		{
			@Field(desc = "机器码")
			String mechineKey;
			
			@Field(desc = "调用来源")
			String source;
			
			@Field(desc = "请求保留字")
			String inReserve;
			
			@Field(desc = "用户qq号")
			uint32_t uin;
			
			@Field(desc = "删除的商品列表")
			Vector<String> delItemIdList;
		}
		
		@ApiProtocol(cmdid = "0x69098805L", desc = "删除收藏的商品返回类")
		class Resp
		{			
			@Field(desc = "错误提示信息")
			String errMsg;
									
			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	
	@ApiProtocol(cmdid = "0x69091806L", desc = "批量删除收藏的店铺")
	public class DelFavShopList
	{
		@ApiProtocol(cmdid = "0x69091806L", desc = "批量删除收藏的店铺请求类")
		class Req
		{
			@Field(desc = "机器码")
			String mechineKey;
			
			@Field(desc = "调用来源")
			String source;
			
			@Field(desc = "请求保留字")
			String inReserve;
			
			@Field(desc = "用户qq号")
			uint32_t uin;
			
			@Field(desc = "删除的店铺列表")
			Vector<uint32_t> delShopIdList;
		}
		
		@ApiProtocol(cmdid = "0x69098806L", desc = "批量删除收藏的店铺返回类")
		class Resp
		{			
			@Field(desc = "错误提示信息")
			String errMsg;
									
			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0x69091807L", desc = "批量获取收藏的店铺")
	public class GetFavShopList
	{
		@ApiProtocol(cmdid = "0x69091807L", desc = "批量获取收藏的店铺请求类")
		class Req
		{
			@Field(desc = "机器码")
			String mechineKey;
			
			@Field(desc = "调用来源")
			String source;
			@Field(desc = "请求保留字")
			String inReserve;
			@Field(desc = "查询filter", cPlusNamespace="FavFilterPo|c2cent::po::favorite",header="FavFilterPo|c2cent/po/favorite/favfilterpo_po.h")
			FavFilterPo favShopFiter;
		
		}
		
		@ApiProtocol(cmdid = "0x69098807L", desc = "批量获取收藏的店铺返回类")
		class Resp
		{	
			@Field(desc = "返回的收藏店铺列表")
			FavShopListPo favShopList;
			@Field(desc = "错误提示信息")
			String errMsg;
									
			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	@ApiProtocol(cmdid = "0x69091808L", desc = "商品是否被收藏")
	public class isFavorited
	{
		@ApiProtocol(cmdid = "0x69091808L", desc = "商品是否被收藏请求类")
		class Req
		{
			@Field(desc = "机器码")
			String mechineKey;
			
			@Field(desc = "调用来源")
			String source;

			@Field(desc = "用户qq号")
			uint32_t uin;

			@Field(desc = "要查询的商品Id")
			String queryItemId;

			@Field(desc = "请求保留字")
			String inReserve;
		}
		@ApiProtocol(cmdid = "0x69098808L", desc = "商品是否被收藏返回类")
		class Resp
		{			
			@Field(desc = "是否被收藏")
			boolean  bFavorited;
			
			@Field(desc = "错误提示信息")
			String errMsg;
									
			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
}
