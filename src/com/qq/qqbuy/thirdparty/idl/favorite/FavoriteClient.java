package com.qq.qqbuy.thirdparty.idl.favorite;

import org.apache.commons.lang.StringUtils;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.PpConstants;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.AddFavItemListReq;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.AddFavItemListResp;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.AddFavShopPo;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.AddFavShopReq;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.AddFavShopResp;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.DelFavItemListReq;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.DelFavItemListResp;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.DelFavShopListReq;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.DelFavShopListResp;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.FavShopFilter;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.GetFavItemListReq;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.GetFavItemListResp;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.GetFavShopDetailReq;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.GetFavShopDetailResp;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.GetFavShopListReq;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.GetFavShopListResp;

public class FavoriteClient extends SupportIDLBaseClient
{

    /**
     * 
     * @Title: getFavShopDetail
     * 
     * @Description: 查询某个店铺的收藏信息
     * 
     * @param req
     * @return 设定文件
     * @return GetFavShopDetailResp 返回类型
     * @throws
     */
    public static GetFavShopDetailResp getFavShopDetail(long uin, long shopId,
            String mk, String skey)
    {
        if (uin < 10000 || shopId < 10000 || StringUtils.isBlank(skey)
                || StringUtils.isBlank(mk))
            return null;
        long start = System.currentTimeMillis();
        GetFavShopDetailReq req = new GetFavShopDetailReq();
        req.setSource(PpConstants.PP_SOURCE);
        req.setBusinessId(PpConstants.PP_BUSINESSID);
        FavShopFilter po = new FavShopFilter();
        po.setShopId(shopId);
        po.setUserId(uin);
        req.setFavShopFilter(po);
        req.setMachineKey(String.valueOf(System.currentTimeMillis()));
        GetFavShopDetailResp resp = new GetFavShopDetailResp();
        resp.result = -1;
        int ret = -1;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setOperator(uin);
        stub.setUin(uin);
        stub.setSkey(skey.getBytes());
        stub.setMachineKey(mk.getBytes());
        ret = invokePaiPaiIDL(req, resp, stub);
        return resp;
    }

    /**
     * 
     * @Title: addFavShop
     * 
     * @Description: 添加店铺到收藏夹
     * 
     * @param req
     * @return 设定文件
     * @return AddFavShopResp 返回类型
     * @throws
     */
    public static AddFavShopResp addFavShop(long uin, long shopId, String mk,
            String skey, String reserve)
    {
        if (uin < 10000 || shopId < 10000 || StringUtils.isBlank(skey)
                || StringUtils.isBlank(mk))
            return null;
        long start = System.currentTimeMillis();
        AddFavShopReq req = new AddFavShopReq();
        req.setSource(PpConstants.PP_SOURCE);
        req.setBusinessId(PpConstants.PP_BUSINESSID);
        if(reserve != null)
        	req.setInReserve(reserve);
        
        AddFavShopPo po = new AddFavShopPo();
        po.setShopId(shopId);
        req.setFavShopPo(po);

        AddFavShopResp resp = new AddFavShopResp();
        resp.result = -1;
        int ret = -1;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setOperator(uin);
        stub.setUin(uin);
        stub.setSkey(skey.getBytes());
        stub.setMachineKey(mk.getBytes());
        ret = invokePaiPaiIDL(req, resp, stub);
        return resp;
    }

    /**
     * 
     * @Title: addFavItemList
     * 
     * @Description: 批量添加到收藏夹
     * 
     * @param req
     * @return 设定文件
     * @return AddFavItemListResp 返回类型
     * @throws
     */
    public static AddFavItemListResp addFavItemList(String[] itemcodes, long buyerUin, String clientIp, String sk, String reserve)
    {
        long start = System.currentTimeMillis();
        
        
        AddFavItemListReq req = new AddFavItemListReq();
        req.setAddItemIdList(Util.toVector(itemcodes));
        req.setMechineKey("0");	//这是visitkey,不是机器码,不要传mk
        req.setSource(CALL_IDL_SOURCE);
        req.setUin(buyerUin);
        if(reserve != null)
        	req.setInReserve(reserve);
        
        AddFavItemListResp resp = new AddFavItemListResp();
        resp.result = -1;
        
        	
        AsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setClientIP(Util.iP2Long(clientIp));
        stub.setOperator(buyerUin);
        stub.setUin(buyerUin);
        stub.setSkey(sk.getBytes());
        int ret = invokePaiPaiIDL(req, resp, stub);
        return resp;
    }

    /**
     * 
     * @Title: addFavItemList
     * 
     * @Description: 批量删除收藏夹
     * 
     * @param req
     * @return 设定文件
     * @return AddFavItemListResp 返回类型
     * @throws
     */
    public static DelFavItemListResp delFavItemList(DelFavItemListReq req)
    {
        long start = System.currentTimeMillis();
        req.setSource(CALL_IDL_SOURCE);

        DelFavItemListResp resp = new DelFavItemListResp();
        resp.result = -1;
        int ret = -1;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setOperator(req.getUin());
        stub.setUin(req.getUin());
        stub.setSkey(req.getInReserve().getBytes());
        ret = invokePaiPaiIDL(req, resp, stub);
        return resp;
    }

    /**
     * 
     * @Title: addFavItemList
     * 接口人：：p_jdzhliang(梁增海)
     * @Description: 查询收藏夹
     * 
     * @param req
     * @return 设定文件
     * @return AddFavItemListResp 返回类型
     * @throws
     */
    public static GetFavItemListResp getFavItemList(GetFavItemListReq req)
    {
        long start = System.currentTimeMillis();
        req.setSource(CALL_IDL_SOURCE);
        GetFavItemListResp resp = new GetFavItemListResp();
        resp.result = -1;
        int ret = -1;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setOperator(req.getFavItemFiter().getUin());
        stub.setUin(req.getFavItemFiter().getUin());
        stub.setSkey(req.getInReserve().getBytes());
        if(EnvManager.isGamma()){
        	Log.run.info("Test for duan cheng!");
			stub.setIpAndPort("10.130.0.237", 53101);
		}
        ret = invokePaiPaiIDL(req, resp, stub);
        return resp;
    }

    /**
     * 
     * @Title: addFavItemList
     * 
     * @Description: 查询店铺收藏夹
     * 
     * @param req
     * @return 设定文件
     * @return AddFavItemListResp 返回类型
     * @throws
     */
    public static GetFavShopListResp getFavShopList(GetFavShopListReq req)
    {
        long start = System.currentTimeMillis();
        req.setSource(CALL_IDL_SOURCE);
        GetFavShopListResp resp = new GetFavShopListResp();
        resp.result = -1;
        int ret = -1;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setOperator(req.getFavShopFiter().getUin());
        stub.setUin(req.getFavShopFiter().getUin());
        stub.setSkey(req.getInReserve().getBytes());
        ret = invokePaiPaiIDL(req, resp, stub);
        return resp;
    }
    
    /**
     * 
     * @Title: addFavItemList
     * 
     * @Description: 批量删除收藏夹
     * 
     * @param req
     * @return 设定文件
     * @return AddFavItemListResp 返回类型
     * @throws
     */
    public static DelFavShopListResp delFavShopList(DelFavShopListReq req)
    {
        long start = System.currentTimeMillis();
        req.setSource(CALL_IDL_SOURCE);

        DelFavShopListResp resp = new DelFavShopListResp();
        resp.result = -1;
        int ret = -1;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setOperator(req.getUin());
        stub.setUin(req.getUin());
        stub.setSkey(req.getInReserve().getBytes());
        stub.setSkey(req.getInReserve().getBytes());
        ret = invokePaiPaiIDL(req, resp, stub);
        return resp;
    }

}
