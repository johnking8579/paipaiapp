 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.favorite.ao.idl.FavoriteApi.java

package com.qq.qqbuy.thirdparty.idl.favorite.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;

import com.paipai.lang.uint32_t;
import java.util.Vector;

/**
 *批量增加商品收藏返回类
 *
 *@date 2012-12-26 03:41:56
 *
 *@since version:0
*/
public class  AddFavItemListResp implements IServiceObject
{
	public long result;
	/**
	 * 添加结果
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint32_t> addResult = new Vector<uint32_t>();

	/**
	 * 总的收藏数
	 *
	 * 版本 >= 0
	 */
	 private long totalCount;

	/**
	 * 错误提示信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 输出保留字
	 *
	 * 版本 >= 0
	 */
	 private String outReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(addResult);
		bs.pushUInt(totalCount);
		bs.pushString(errMsg);
		bs.pushString(outReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		addResult = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		totalCount = bs.popUInt();
		errMsg = bs.popString();
		outReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x69098803L;
	}


	/**
	 * 获取添加结果
	 * 
	 * 此字段的版本 >= 0
	 * @return addResult value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getAddResult()
	{
		return addResult;
	}


	/**
	 * 设置添加结果
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setAddResult(Vector<uint32_t> value)
	{
		if (value != null) {
				this.addResult = value;
		}else{
				this.addResult = new Vector<uint32_t>();
		}
	}


	/**
	 * 获取总的收藏数
	 * 
	 * 此字段的版本 >= 0
	 * @return totalCount value 类型为:long
	 * 
	 */
	public long getTotalCount()
	{
		return totalCount;
	}


	/**
	 * 设置总的收藏数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalCount(long value)
	{
		this.totalCount = value;
	}


	/**
	 * 获取错误提示信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误提示信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	/**
	 * 获取输出保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return outReserve;
	}


	/**
	 * 设置输出保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		this.outReserve = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(AddFavItemListResp)
				length += ByteStream.getObjectSize(addResult);  //计算字段addResult的长度 size_of(Vector)
				length += 4;  //计算字段totalCount的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserve);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

    @Override
    public String toString()
    {
        return "AddFavItemListResp [addResult=" + addResult + ", errMsg="
                + errMsg + ", outReserve=" + outReserve + ", result=" + result
                + ", totalCount=" + totalCount + "]";
    }


}
