 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.favorite.FavoriteApi.java

package com.qq.qqbuy.thirdparty.idl.favorite.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *批量获取收藏的店铺请求类
 *
 *@date 2013-04-11 09:21:12
 *
 *@since version:0
*/
public class  GetFavShopListReq implements IServiceObject
{
	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String mechineKey = new String();

	/**
	 * 调用来源
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 请求保留字
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();

	/**
	 * 查询filter
	 *
	 * 版本 >= 0
	 */
	 private FavFilterPo favShopFiter = new FavFilterPo();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(mechineKey);
		bs.pushString(source);
		bs.pushString(inReserve);
		bs.pushObject(favShopFiter);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		mechineKey = bs.popString();
		source = bs.popString();
		inReserve = bs.popString();
		favShopFiter = (FavFilterPo) bs.popObject(FavFilterPo.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x69091807L;
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return mechineKey value 类型为:String
	 * 
	 */
	public String getMechineKey()
	{
		return mechineKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMechineKey(String value)
	{
		this.mechineKey = value;
	}


	/**
	 * 获取调用来源
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置调用来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	/**
	 * 获取查询filter
	 * 
	 * 此字段的版本 >= 0
	 * @return favShopFiter value 类型为:FavFilterPo
	 * 
	 */
	public FavFilterPo getFavShopFiter()
	{
		return favShopFiter;
	}


	/**
	 * 设置查询filter
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:FavFilterPo
	 * 
	 */
	public void setFavShopFiter(FavFilterPo value)
	{
		if (value != null) {
				this.favShopFiter = value;
		}else{
				this.favShopFiter = new FavFilterPo();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetFavShopListReq)
				length += ByteStream.getObjectSize(mechineKey);  //计算字段mechineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve);  //计算字段inReserve的长度 size_of(String)
				length += ByteStream.getObjectSize(favShopFiter);  //计算字段favShopFiter的长度 size_of(FavFilterPo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
