 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.favorite.ao.idl.FavoriteApi.java

package com.qq.qqbuy.thirdparty.idl.favorite.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Vector;

/**
 *批量增加商品收藏请求类
 *
 *@date 2012-12-26 03:41:56
 *
 *@since version:0
*/
public class  AddFavItemListReq implements IServiceObject
{
	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String mechineKey = new String();

	/**
	 * 调用来源
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 请求保留字
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();

	/**
	 * 用户qq号
	 *
	 * 版本 >= 0
	 */
	 private long uin;

	/**
	 * 收藏的商品信息vector
	 *
	 * 版本 >= 0
	 */
	 private Vector<String> addItemIdList = new Vector<String>();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(mechineKey);
		bs.pushString(source);
		bs.pushString(inReserve);
		bs.pushUInt(uin);
		bs.pushObject(addItemIdList);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		mechineKey = bs.popString();
		source = bs.popString();
		inReserve = bs.popString();
		uin = bs.popUInt();
		addItemIdList = (Vector<String>)bs.popVector(String.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x69091803L;
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return mechineKey value 类型为:String
	 * 
	 */
	public String getMechineKey()
	{
		return mechineKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMechineKey(String value)
	{
		this.mechineKey = value;
	}


	/**
	 * 获取调用来源
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置调用来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	/**
	 * 获取用户qq号
	 * 
	 * 此字段的版本 >= 0
	 * @return uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return uin;
	}


	/**
	 * 设置用户qq号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.uin = value;
	}


	/**
	 * 获取收藏的商品信息vector
	 * 
	 * 此字段的版本 >= 0
	 * @return addItemIdList value 类型为:Vector<String>
	 * 
	 */
	public Vector<String> getAddItemIdList()
	{
		return addItemIdList;
	}


	/**
	 * 设置收藏的商品信息vector
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<String>
	 * 
	 */
	public void setAddItemIdList(Vector<String> value)
	{
		if (value != null) {
				this.addItemIdList = value;
		}else{
				this.addItemIdList = new Vector<String>();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(AddFavItemListReq)
				length += ByteStream.getObjectSize(mechineKey);  //计算字段mechineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve);  //计算字段inReserve的长度 size_of(String)
				length += 4;  //计算字段uin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(addItemIdList);  //计算字段addItemIdList的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

    @Override
    public String toString()
    {
        return "AddFavItemListReq [addItemIdList=" + addItemIdList
                + ", inReserve=" + inReserve + ", mechineKey=" + mechineKey
                + ", source=" + source + ", uin=" + uin + "]";
    }
	
}
