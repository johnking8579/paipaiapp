//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.favorite.ao.idl.FavoritePo.java

package com.qq.qqbuy.thirdparty.idl.favorite.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *返回查询收藏的商品信息
 *
 *@date 2012-12-26 03:41:48
 *
 *@since version:0
*/
public class FavItemPo  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 0;

	/**
	 * 商品Id
	 *
	 * 版本 >= 0
	 */
	 private String itemId = new String();

	/**
	 * 收藏时的价格
	 *
	 * 版本 >= 0
	 */
	 private long oldPrice;

	/**
	 * 当前价格
	 *
	 * 版本 >= 0
	 */
	 private long price;

	/**
	 * 商品名称
	 *
	 * 版本 >= 0
	 */
	 private String itemTitle = new String();

	/**
	 * 商品图片
	 *
	 * 版本 >= 0
	 */
	 private String itemPic = new String();

	/**
	 * 当前状态
	 *
	 * 版本 >= 0
	 */
	 private long state;

	/**
	 * 收藏时间
	 *
	 * 版本 >= 0
	 */
	 private long addTime;

	/**
	 * 属性字段
	 *
	 * 版本 >= 0
	 */
	 private String itemProperty = new String();

	/**
	 * 预留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();

	/**
	 * 版本号_u
	 *
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 商品Id_u
	 *
	 * 版本 >= 0
	 */
	 private short itemId_u;

	/**
	 * 商品最低价格_u
	 *
	 * 版本 >= 0
	 */
	 private short oldPrice_u;

	/**
	 * 当前价格
	 *
	 * 版本 >= 0
	 */
	 private short price_u;

	/**
	 * 商品名称
	 *
	 * 版本 >= 0
	 */
	 private short itemTitle_u;

	/**
	 * 商品图片
	 *
	 * 版本 >= 0
	 */
	 private short itemPic_u;

	/**
	 * 当前状态
	 *
	 * 版本 >= 0
	 */
	 private short state_u;

	/**
	 * 收藏时间_u
	 *
	 * 版本 >= 0
	 */
	 private short addTime_u;

	/**
	 * 预留商品属性_u
	 *
	 * 版本 >= 0
	 */
	 private short itemProperty_u;

	/**
	 * 预留字段_u
	 *
	 * 版本 >= 0
	 */
	 private short reserved_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushString(itemId);
		bs.pushUInt(oldPrice);
		bs.pushUInt(price);
		bs.pushString(itemTitle);
		bs.pushString(itemPic);
		bs.pushUInt(state);
		bs.pushUInt(addTime);
		bs.pushString(itemProperty);
		bs.pushString(reserved);
		bs.pushUByte(version_u);
		bs.pushUByte(itemId_u);
		bs.pushUByte(oldPrice_u);
		bs.pushUByte(price_u);
		bs.pushUByte(itemTitle_u);
		bs.pushUByte(itemPic_u);
		bs.pushUByte(state_u);
		bs.pushUByte(addTime_u);
		bs.pushUByte(itemProperty_u);
		bs.pushUByte(reserved_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		itemId = bs.popString();
		oldPrice = bs.popUInt();
		price = bs.popUInt();
		itemTitle = bs.popString();
		itemPic = bs.popString();
		state = bs.popUInt();
		addTime = bs.popUInt();
		itemProperty = bs.popString();
		reserved = bs.popString();
		version_u = bs.popUByte();
		itemId_u = bs.popUByte();
		oldPrice_u = bs.popUByte();
		price_u = bs.popUByte();
		itemTitle_u = bs.popUByte();
		itemPic_u = bs.popUByte();
		state_u = bs.popUByte();
		addTime_u = bs.popUByte();
		itemProperty_u = bs.popUByte();
		reserved_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取商品Id
	 * 
	 * 此字段的版本 >= 0
	 * @return itemId value 类型为:String
	 * 
	 */
	public String getItemId()
	{
		return itemId;
	}


	/**
	 * 设置商品Id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemId(String value)
	{
		this.itemId = value;
		this.itemId_u = 1;
	}


	/**
	 * 获取收藏时的价格
	 * 
	 * 此字段的版本 >= 0
	 * @return oldPrice value 类型为:long
	 * 
	 */
	public long getOldPrice()
	{
		return oldPrice;
	}


	/**
	 * 设置收藏时的价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOldPrice(long value)
	{
		this.oldPrice = value;
		this.oldPrice_u = 1;
	}


	/**
	 * 获取当前价格
	 * 
	 * 此字段的版本 >= 0
	 * @return price value 类型为:long
	 * 
	 */
	public long getPrice()
	{
		return price;
	}


	/**
	 * 设置当前价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPrice(long value)
	{
		this.price = value;
		this.price_u = 1;
	}


	/**
	 * 获取商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @return itemTitle value 类型为:String
	 * 
	 */
	public String getItemTitle()
	{
		return itemTitle;
	}


	/**
	 * 设置商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemTitle(String value)
	{
		this.itemTitle = value;
		this.itemTitle_u = 1;
	}


	/**
	 * 获取商品图片
	 * 
	 * 此字段的版本 >= 0
	 * @return itemPic value 类型为:String
	 * 
	 */
	public String getItemPic()
	{
		return itemPic;
	}


	/**
	 * 设置商品图片
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemPic(String value)
	{
		this.itemPic = value;
		this.itemPic_u = 1;
	}


	/**
	 * 获取当前状态
	 * 
	 * 此字段的版本 >= 0
	 * @return state value 类型为:long
	 * 
	 */
	public long getState()
	{
		return state;
	}


	/**
	 * 设置当前状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setState(long value)
	{
		this.state = value;
		this.state_u = 1;
	}


	/**
	 * 获取收藏时间
	 * 
	 * 此字段的版本 >= 0
	 * @return addTime value 类型为:long
	 * 
	 */
	public long getAddTime()
	{
		return addTime;
	}


	/**
	 * 设置收藏时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddTime(long value)
	{
		this.addTime = value;
		this.addTime_u = 1;
	}


	/**
	 * 获取属性字段
	 * 
	 * 此字段的版本 >= 0
	 * @return itemProperty value 类型为:String
	 * 
	 */
	public String getItemProperty()
	{
		return itemProperty;
	}


	/**
	 * 设置属性字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemProperty(String value)
	{
		this.itemProperty = value;
		this.itemProperty_u = 1;
	}


	/**
	 * 获取预留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置预留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
		this.reserved_u = 1;
	}


	/**
	 * 获取版本号_u
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置版本号_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取商品Id_u
	 * 
	 * 此字段的版本 >= 0
	 * @return itemId_u value 类型为:short
	 * 
	 */
	public short getItemId_u()
	{
		return itemId_u;
	}


	/**
	 * 设置商品Id_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemId_u(short value)
	{
		this.itemId_u = value;
	}


	/**
	 * 获取商品最低价格_u
	 * 
	 * 此字段的版本 >= 0
	 * @return oldPrice_u value 类型为:short
	 * 
	 */
	public short getOldPrice_u()
	{
		return oldPrice_u;
	}


	/**
	 * 设置商品最低价格_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOldPrice_u(short value)
	{
		this.oldPrice_u = value;
	}


	/**
	 * 获取当前价格
	 * 
	 * 此字段的版本 >= 0
	 * @return price_u value 类型为:short
	 * 
	 */
	public short getPrice_u()
	{
		return price_u;
	}


	/**
	 * 设置当前价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPrice_u(short value)
	{
		this.price_u = value;
	}


	/**
	 * 获取商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @return itemTitle_u value 类型为:short
	 * 
	 */
	public short getItemTitle_u()
	{
		return itemTitle_u;
	}


	/**
	 * 设置商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemTitle_u(short value)
	{
		this.itemTitle_u = value;
	}


	/**
	 * 获取商品图片
	 * 
	 * 此字段的版本 >= 0
	 * @return itemPic_u value 类型为:short
	 * 
	 */
	public short getItemPic_u()
	{
		return itemPic_u;
	}


	/**
	 * 设置商品图片
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemPic_u(short value)
	{
		this.itemPic_u = value;
	}


	/**
	 * 获取当前状态
	 * 
	 * 此字段的版本 >= 0
	 * @return state_u value 类型为:short
	 * 
	 */
	public short getState_u()
	{
		return state_u;
	}


	/**
	 * 设置当前状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setState_u(short value)
	{
		this.state_u = value;
	}


	/**
	 * 获取收藏时间_u
	 * 
	 * 此字段的版本 >= 0
	 * @return addTime_u value 类型为:short
	 * 
	 */
	public short getAddTime_u()
	{
		return addTime_u;
	}


	/**
	 * 设置收藏时间_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddTime_u(short value)
	{
		this.addTime_u = value;
	}


	/**
	 * 获取预留商品属性_u
	 * 
	 * 此字段的版本 >= 0
	 * @return itemProperty_u value 类型为:short
	 * 
	 */
	public short getItemProperty_u()
	{
		return itemProperty_u;
	}


	/**
	 * 设置预留商品属性_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemProperty_u(short value)
	{
		this.itemProperty_u = value;
	}


	/**
	 * 获取预留字段_u
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved_u value 类型为:short
	 * 
	 */
	public short getReserved_u()
	{
		return reserved_u;
	}


	/**
	 * 设置预留字段_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserved_u(short value)
	{
		this.reserved_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(FavItemPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(itemId);  //计算字段itemId的长度 size_of(String)
				length += 4;  //计算字段oldPrice的长度 size_of(uint32_t)
				length += 4;  //计算字段price的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(itemTitle);  //计算字段itemTitle的长度 size_of(String)
				length += ByteStream.getObjectSize(itemPic);  //计算字段itemPic的长度 size_of(String)
				length += 4;  //计算字段state的长度 size_of(uint32_t)
				length += 4;  //计算字段addTime的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(itemProperty);  //计算字段itemProperty的长度 size_of(String)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段itemId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段oldPrice_u的长度 size_of(uint8_t)
				length += 1;  //计算字段price_u的长度 size_of(uint8_t)
				length += 1;  //计算字段itemTitle_u的长度 size_of(uint8_t)
				length += 1;  //计算字段itemPic_u的长度 size_of(uint8_t)
				length += 1;  //计算字段state_u的长度 size_of(uint8_t)
				length += 1;  //计算字段addTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段itemProperty_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserved_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

    @Override
    public String toString()
    {
        return "FavItemPo [addTime=" + addTime + ", addTime_u=" + addTime_u
                + ", itemId=" + itemId + ", itemId_u=" + itemId_u
                + ", itemPic=" + itemPic + ", itemPic_u=" + itemPic_u
                + ", itemProperty=" + itemProperty + ", itemProperty_u="
                + itemProperty_u + ", itemTitle=" + itemTitle
                + ", itemTitle_u=" + itemTitle_u + ", oldPrice=" + oldPrice
                + ", oldPrice_u=" + oldPrice_u + ", price=" + price
                + ", price_u=" + price_u + ", reserved=" + reserved
                + ", reserved_u=" + reserved_u + ", state=" + state
                + ", state_u=" + state_u + ", version=" + version
                + ", version_u=" + version_u + "]";
    }


}
