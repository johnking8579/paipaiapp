 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.favorite.ao.idl.FavoriteApi.java

package com.qq.qqbuy.thirdparty.idl.favorite.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *查询指定用户是否收藏了指定店铺返回结果
 *
 *@date 2012-12-26 03:41:56
 *
 *@since version:0
*/
public class  GetFavShopDetailResp implements IServiceObject
{
	public long result;
	/**
	 * 收藏店铺po信息
	 *
	 * 版本 >= 0
	 */
	 private FavShopPo FavShop = new FavShopPo();

	/**
	 * 错误信息,调试使用
	 *
	 * 版本 >= 0
	 */
	 private String errmsg = new String();

	/**
	 * 返回保留字
	 *
	 * 版本 >= 0
	 */
	 private String outReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(FavShop);
		bs.pushString(errmsg);
		bs.pushString(outReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		FavShop = (FavShopPo) bs.popObject(FavShopPo.class);
		errmsg = bs.popString();
		outReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x69098802L;
	}


	/**
	 * 获取收藏店铺po信息
	 * 
	 * 此字段的版本 >= 0
	 * @return FavShop value 类型为:FavShopPo
	 * 
	 */
	public FavShopPo getFavShop()
	{
		return FavShop;
	}


	/**
	 * 设置收藏店铺po信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:FavShopPo
	 * 
	 */
	public void setFavShop(FavShopPo value)
	{
		if (value != null) {
				this.FavShop = value;
		}else{
				this.FavShop = new FavShopPo();
		}
	}


	/**
	 * 获取错误信息,调试使用
	 * 
	 * 此字段的版本 >= 0
	 * @return errmsg value 类型为:String
	 * 
	 */
	public String getErrmsg()
	{
		return errmsg;
	}


	/**
	 * 设置错误信息,调试使用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrmsg(String value)
	{
		this.errmsg = value;
	}


	/**
	 * 获取返回保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return outReserve;
	}


	/**
	 * 设置返回保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		this.outReserve = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetFavShopDetailResp)
				length += ByteStream.getObjectSize(FavShop);  //计算字段FavShop的长度 size_of(FavShopPo)
				length += ByteStream.getObjectSize(errmsg);  //计算字段errmsg的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserve);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
