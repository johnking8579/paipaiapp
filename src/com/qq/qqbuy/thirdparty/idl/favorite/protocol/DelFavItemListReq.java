 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.favorite.ao.idl.FavoriteApi.java

package com.qq.qqbuy.thirdparty.idl.favorite.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Vector;

/**
 *删除收藏的商品请求类
 *
 *@date 2012-12-26 03:41:56
 *
 *@since version:0
*/
public class  DelFavItemListReq implements IServiceObject
{
	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String mechineKey = new String();

	/**
	 * 调用来源
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 请求保留字
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();

	/**
	 * 用户qq号
	 *
	 * 版本 >= 0
	 */
	 private long uin;

	/**
	 * 删除的商品列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<String> delItemIdList = new Vector<String>();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(mechineKey);
		bs.pushString(source);
		bs.pushString(inReserve);
		bs.pushUInt(uin);
		bs.pushObject(delItemIdList);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		mechineKey = bs.popString();
		source = bs.popString();
		inReserve = bs.popString();
		uin = bs.popUInt();
		delItemIdList = (Vector<String>)bs.popVector(String.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x69091805L;
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return mechineKey value 类型为:String
	 * 
	 */
	public String getMechineKey()
	{
		return mechineKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMechineKey(String value)
	{
		this.mechineKey = value;
	}


	/**
	 * 获取调用来源
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置调用来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	/**
	 * 获取用户qq号
	 * 
	 * 此字段的版本 >= 0
	 * @return uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return uin;
	}


	/**
	 * 设置用户qq号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.uin = value;
	}


	/**
	 * 获取删除的商品列表
	 * 
	 * 此字段的版本 >= 0
	 * @return delItemIdList value 类型为:Vector<String>
	 * 
	 */
	public Vector<String> getDelItemIdList()
	{
		return delItemIdList;
	}


	/**
	 * 设置删除的商品列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<String>
	 * 
	 */
	public void setDelItemIdList(Vector<String> value)
	{
		if (value != null) {
				this.delItemIdList = value;
		}else{
				this.delItemIdList = new Vector<String>();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(DelFavItemListReq)
				length += ByteStream.getObjectSize(mechineKey);  //计算字段mechineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve);  //计算字段inReserve的长度 size_of(String)
				length += 4;  //计算字段uin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(delItemIdList);  //计算字段delItemIdList的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

    @Override
    public String toString()
    {
        return "DelFavItemListReq [delItemIdList=" + delItemIdList
                + ", inReserve=" + inReserve + ", mechineKey=" + mechineKey
                + ", source=" + source + ", uin=" + uin + "]";
    }

	
}
