//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.favorite.ao.idl.FavoritePo.java

package com.qq.qqbuy.thirdparty.idl.favorite.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import java.util.Vector;

/**
 *返回查询收藏的商品信息列表
 *
 *@date 2012-12-26 03:41:48
 *
 *@since version:0
*/
public class FavItemListPo  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 0;

	/**
	 * 商品信息
	 *
	 * 版本 >= 0
	 */
	 private Vector<FavItemPo> favItemList = new Vector<FavItemPo>();

	/**
	 * 总数
	 *
	 * 版本 >= 0
	 */
	 private long total;

	/**
	 * 预留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();

	/**
	 * 版本号_u
	 *
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 商品信息_u
	 *
	 * 版本 >= 0
	 */
	 private short favItemList_u;

	/**
	 * 总数_u
	 *
	 * 版本 >= 0
	 */
	 private short total_u;

	/**
	 * 预留字段_u
	 *
	 * 版本 >= 0
	 */
	 private short reserved_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushObject(favItemList);
		bs.pushUInt(total);
		bs.pushString(reserved);
		bs.pushUByte(version_u);
		bs.pushUByte(favItemList_u);
		bs.pushUByte(total_u);
		bs.pushUByte(reserved_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		favItemList = (Vector<FavItemPo>)bs.popVector(FavItemPo.class);
		total = bs.popUInt();
		reserved = bs.popString();
		version_u = bs.popUByte();
		favItemList_u = bs.popUByte();
		total_u = bs.popUByte();
		reserved_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取商品信息
	 * 
	 * 此字段的版本 >= 0
	 * @return favItemList value 类型为:Vector<FavItemPo>
	 * 
	 */
	public Vector<FavItemPo> getFavItemList()
	{
		return favItemList;
	}


	/**
	 * 设置商品信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<FavItemPo>
	 * 
	 */
	public void setFavItemList(Vector<FavItemPo> value)
	{
		if (value != null) {
				this.favItemList = value;
				this.favItemList_u = 1;
		}
	}


	/**
	 * 获取总数
	 * 
	 * 此字段的版本 >= 0
	 * @return total value 类型为:long
	 * 
	 */
	public long getTotal()
	{
		return total;
	}


	/**
	 * 设置总数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotal(long value)
	{
		this.total = value;
		this.total_u = 1;
	}


	/**
	 * 获取预留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置预留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
		this.reserved_u = 1;
	}


	/**
	 * 获取版本号_u
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置版本号_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取商品信息_u
	 * 
	 * 此字段的版本 >= 0
	 * @return favItemList_u value 类型为:short
	 * 
	 */
	public short getFavItemList_u()
	{
		return favItemList_u;
	}


	/**
	 * 设置商品信息_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFavItemList_u(short value)
	{
		this.favItemList_u = value;
	}


	/**
	 * 获取总数_u
	 * 
	 * 此字段的版本 >= 0
	 * @return total_u value 类型为:short
	 * 
	 */
	public short getTotal_u()
	{
		return total_u;
	}


	/**
	 * 设置总数_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTotal_u(short value)
	{
		this.total_u = value;
	}


	/**
	 * 获取预留字段_u
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved_u value 类型为:short
	 * 
	 */
	public short getReserved_u()
	{
		return reserved_u;
	}


	/**
	 * 设置预留字段_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserved_u(short value)
	{
		this.reserved_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(FavItemListPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(favItemList);  //计算字段favItemList的长度 size_of(Vector)
				length += 4;  //计算字段total的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段favItemList_u的长度 size_of(uint8_t)
				length += 1;  //计算字段total_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserved_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

    @Override
    public String toString()
    {
        return "FavItemListPo [favItemList=" + favItemList + ", favItemList_u="
                + favItemList_u + ", reserved=" + reserved + ", reserved_u="
                + reserved_u + ", total=" + total + ", total_u=" + total_u
                + ", version=" + version + ", version_u=" + version_u + "]";
    }


/**
 ********************以下信息是每个版本的字段********************
 */



}
