//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.favorite.FavoritePo.java

package com.qq.qqbuy.thirdparty.idl.favorite.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import com.paipai.lang.uint32_t;
import java.util.Vector;

/**
 *收藏店铺查询结果
 *
 *@date 2013-04-12 04:39:10
 *
 *@since version:0
*/
public class FavShopPo  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20130411;

	/**
	 * 收藏店铺的用户
	 *
	 * 版本 >= 0
	 */
	 private long UserId;

	/**
	 * 被收藏的店铺的id，即卖家qq号
	 *
	 * 版本 >= 0
	 */
	 private long ShopId;

	/**
	 * 收藏该店铺的时间
	 *
	 * 版本 >= 0
	 */
	 private long AddTime;

	/**
	 * 版本 >= 0
	 */
	 private short UserId_u;

	/**
	 * 版本 >= 0
	 */
	 private short ShopId_u;

	/**
	 * 版本 >= 0
	 */
	 private short AddTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 店铺名称
	 *
	 * 版本 >= 20130411
	 */
	 private String shopName = new String();

	/**
	 * 店铺图片
	 *
	 * 版本 >= 20130411
	 */
	 private String shopLogoPos = new String();

	/**
	 * 店铺卖家昵称
	 *
	 * 版本 >= 20130411
	 */
	 private String sellerNickName = new String();

	/**
	 * 店铺信用
	 *
	 * 版本 >= 20130411
	 */
	 private long shopCredit;

	/**
	 * 店铺好评
	 *
	 * 版本 >= 20130411
	 */
	 private String shopGoodRate = new String();

	/**
	 * 店铺收藏人数
	 *
	 * 版本 >= 20130411
	 */
	 private long favoriteHeat;

	/**
	 * 动态数字
	 *
	 * 版本 >= 20130411
	 */
	 private Vector<uint32_t> dynamicNum = new Vector<uint32_t>();

	/**
	 * tencent 标识
	 *
	 * 版本 >= 20130411
	 */
	 private String sigTencent = new String();

	/**
	 * paipai 标识
	 *
	 * 版本 >= 20130411
	 */
	 private String sigUI = new String();

	/**
	 * 属性字段
	 *
	 * 版本 >= 20130411
	 */
	 private String shopProperty = new String();

	/**
	 * 用户标签
	 *
	 * 版本 >= 20130411
	 */
	 private String label = new String();

	/**
	 * 预留字段
	 *
	 * 版本 >= 20130411
	 */
	 private String reserved = new String();

	/**
	 * 
	 *
	 * 版本 >= 20130411
	 */
	 private short shopName_u;

	/**
	 * 
	 *
	 * 版本 >= 20130411
	 */
	 private short shopLogoPos_u;

	/**
	 * 
	 *
	 * 版本 >= 20130411
	 */
	 private short sellerNickName_u;

	/**
	 * 
	 *
	 * 版本 >= 20130411
	 */
	 private short shopCredit_u;

	/**
	 * 
	 *
	 * 版本 >= 20130411
	 */
	 private short shopGoodRate_u;

	/**
	 * 
	 *
	 * 版本 >= 20130411
	 */
	 private short favoriteHeat_u;

	/**
	 * 
	 *
	 * 版本 >= 20130411
	 */
	 private short dynamicNum_u;

	/**
	 * 
	 *
	 * 版本 >= 20130411
	 */
	 private short sigTencent_u;

	/**
	 * 
	 *
	 * 版本 >= 20130411
	 */
	 private short sigUI_u;

	/**
	 * 
	 *
	 * 版本 >= 20130411
	 */
	 private short shopProperty_u;

	/**
	 * 
	 *
	 * 版本 >= 20130411
	 */
	 private short label_u;

	/**
	 * 
	 *
	 * 版本 >= 20130411
	 */
	 private short reserved_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUInt(UserId);
		bs.pushUInt(ShopId);
		bs.pushUInt(AddTime);
		bs.pushUByte(UserId_u);
		bs.pushUByte(ShopId_u);
		bs.pushUByte(AddTime_u);
		bs.pushUByte(version_u);
		if(  this.version >= 20130411 ){
				bs.pushString(shopName);
		}
		if(  this.version >= 20130411 ){
				bs.pushString(shopLogoPos);
		}
		if(  this.version >= 20130411 ){
				bs.pushString(sellerNickName);
		}
		if(  this.version >= 20130411 ){
				bs.pushUInt(shopCredit);
		}
		if(  this.version >= 20130411 ){
				bs.pushString(shopGoodRate);
		}
		if(  this.version >= 20130411 ){
				bs.pushUInt(favoriteHeat);
		}
		if(  this.version >= 20130411 ){
				bs.pushObject(dynamicNum);
		}
		if(  this.version >= 20130411 ){
				bs.pushString(sigTencent);
		}
		if(  this.version >= 20130411 ){
				bs.pushString(sigUI);
		}
		if(  this.version >= 20130411 ){
				bs.pushString(shopProperty);
		}
		if(  this.version >= 20130411 ){
				bs.pushString(label);
		}
		if(  this.version >= 20130411 ){
				bs.pushString(reserved);
		}
		if(  this.version >= 20130411 ){
				bs.pushUByte(shopName_u);
		}
		if(  this.version >= 20130411 ){
				bs.pushUByte(shopLogoPos_u);
		}
		if(  this.version >= 20130411 ){
				bs.pushUByte(sellerNickName_u);
		}
		if(  this.version >= 20130411 ){
				bs.pushUByte(shopCredit_u);
		}
		if(  this.version >= 20130411 ){
				bs.pushUByte(shopGoodRate_u);
		}
		if(  this.version >= 20130411 ){
				bs.pushUByte(favoriteHeat_u);
		}
		if(  this.version >= 20130411 ){
				bs.pushUByte(dynamicNum_u);
		}
		if(  this.version >= 20130411 ){
				bs.pushUByte(sigTencent_u);
		}
		if(  this.version >= 20130411 ){
				bs.pushUByte(sigUI_u);
		}
		if(  this.version >= 20130411 ){
				bs.pushUByte(shopProperty_u);
		}
		if(  this.version >= 20130411 ){
				bs.pushUByte(label_u);
		}
		if(  this.version >= 20130411 ){
				bs.pushUByte(reserved_u);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		UserId = bs.popUInt();
		ShopId = bs.popUInt();
		AddTime = bs.popUInt();
		UserId_u = bs.popUByte();
		ShopId_u = bs.popUByte();
		AddTime_u = bs.popUByte();
		version_u = bs.popUByte();
		if(  this.version >= 20130411 ){
				shopName = bs.popString();
		}
		if(  this.version >= 20130411 ){
				shopLogoPos = bs.popString();
		}
		if(  this.version >= 20130411 ){
				sellerNickName = bs.popString();
		}
		if(  this.version >= 20130411 ){
				shopCredit = bs.popUInt();
		}
		if(  this.version >= 20130411 ){
				shopGoodRate = bs.popString();
		}
		if(  this.version >= 20130411 ){
				favoriteHeat = bs.popUInt();
		}
		if(  this.version >= 20130411 ){
				dynamicNum = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		}
		if(  this.version >= 20130411 ){
				sigTencent = bs.popString();
		}
		if(  this.version >= 20130411 ){
				sigUI = bs.popString();
		}
		if(  this.version >= 20130411 ){
				shopProperty = bs.popString();
		}
		if(  this.version >= 20130411 ){
				label = bs.popString();
		}
		if(  this.version >= 20130411 ){
				reserved = bs.popString();
		}
		if(  this.version >= 20130411 ){
				shopName_u = bs.popUByte();
		}
		if(  this.version >= 20130411 ){
				shopLogoPos_u = bs.popUByte();
		}
		if(  this.version >= 20130411 ){
				sellerNickName_u = bs.popUByte();
		}
		if(  this.version >= 20130411 ){
				shopCredit_u = bs.popUByte();
		}
		if(  this.version >= 20130411 ){
				shopGoodRate_u = bs.popUByte();
		}
		if(  this.version >= 20130411 ){
				favoriteHeat_u = bs.popUByte();
		}
		if(  this.version >= 20130411 ){
				dynamicNum_u = bs.popUByte();
		}
		if(  this.version >= 20130411 ){
				sigTencent_u = bs.popUByte();
		}
		if(  this.version >= 20130411 ){
				sigUI_u = bs.popUByte();
		}
		if(  this.version >= 20130411 ){
				shopProperty_u = bs.popUByte();
		}
		if(  this.version >= 20130411 ){
				label_u = bs.popUByte();
		}
		if(  this.version >= 20130411 ){
				reserved_u = bs.popUByte();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取收藏店铺的用户
	 * 
	 * 此字段的版本 >= 0
	 * @return UserId value 类型为:long
	 * 
	 */
	public long getUserId()
	{
		return UserId;
	}


	/**
	 * 设置收藏店铺的用户
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUserId(long value)
	{
		this.UserId = value;
		this.UserId_u = 1;
	}


	/**
	 * 获取被收藏的店铺的id，即卖家qq号
	 * 
	 * 此字段的版本 >= 0
	 * @return ShopId value 类型为:long
	 * 
	 */
	public long getShopId()
	{
		return ShopId;
	}


	/**
	 * 设置被收藏的店铺的id，即卖家qq号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setShopId(long value)
	{
		this.ShopId = value;
		this.ShopId_u = 1;
	}


	/**
	 * 获取收藏该店铺的时间
	 * 
	 * 此字段的版本 >= 0
	 * @return AddTime value 类型为:long
	 * 
	 */
	public long getAddTime()
	{
		return AddTime;
	}


	/**
	 * 设置收藏该店铺的时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddTime(long value)
	{
		this.AddTime = value;
		this.AddTime_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return UserId_u value 类型为:short
	 * 
	 */
	public short getUserId_u()
	{
		return UserId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUserId_u(short value)
	{
		this.UserId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ShopId_u value 类型为:short
	 * 
	 */
	public short getShopId_u()
	{
		return ShopId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopId_u(short value)
	{
		this.ShopId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return AddTime_u value 类型为:short
	 * 
	 */
	public short getAddTime_u()
	{
		return AddTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddTime_u(short value)
	{
		this.AddTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取店铺名称
	 * 
	 * 此字段的版本 >= 20130411
	 * @return shopName value 类型为:String
	 * 
	 */
	public String getShopName()
	{
		return shopName;
	}


	/**
	 * 设置店铺名称
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:String
	 * 
	 */
	public void setShopName(String value)
	{
		this.shopName = value;
		this.shopName_u = 1;
	}


	/**
	 * 获取店铺图片
	 * 
	 * 此字段的版本 >= 20130411
	 * @return shopLogoPos value 类型为:String
	 * 
	 */
	public String getShopLogoPos()
	{
		return shopLogoPos;
	}


	/**
	 * 设置店铺图片
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:String
	 * 
	 */
	public void setShopLogoPos(String value)
	{
		this.shopLogoPos = value;
		this.shopLogoPos_u = 1;
	}


	/**
	 * 获取店铺卖家昵称
	 * 
	 * 此字段的版本 >= 20130411
	 * @return sellerNickName value 类型为:String
	 * 
	 */
	public String getSellerNickName()
	{
		return sellerNickName;
	}


	/**
	 * 设置店铺卖家昵称
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:String
	 * 
	 */
	public void setSellerNickName(String value)
	{
		this.sellerNickName = value;
		this.sellerNickName_u = 1;
	}


	/**
	 * 获取店铺信用
	 * 
	 * 此字段的版本 >= 20130411
	 * @return shopCredit value 类型为:long
	 * 
	 */
	public long getShopCredit()
	{
		return shopCredit;
	}


	/**
	 * 设置店铺信用
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:long
	 * 
	 */
	public void setShopCredit(long value)
	{
		this.shopCredit = value;
		this.shopCredit_u = 1;
	}


	/**
	 * 获取店铺好评
	 * 
	 * 此字段的版本 >= 20130411
	 * @return shopGoodRate value 类型为:String
	 * 
	 */
	public String getShopGoodRate()
	{
		return shopGoodRate;
	}


	/**
	 * 设置店铺好评
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:String
	 * 
	 */
	public void setShopGoodRate(String value)
	{
		this.shopGoodRate = value;
		this.shopGoodRate_u = 1;
	}


	/**
	 * 获取店铺收藏人数
	 * 
	 * 此字段的版本 >= 20130411
	 * @return favoriteHeat value 类型为:long
	 * 
	 */
	public long getFavoriteHeat()
	{
		return favoriteHeat;
	}


	/**
	 * 设置店铺收藏人数
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:long
	 * 
	 */
	public void setFavoriteHeat(long value)
	{
		this.favoriteHeat = value;
		this.favoriteHeat_u = 1;
	}


	/**
	 * 获取动态数字
	 * 
	 * 此字段的版本 >= 20130411
	 * @return dynamicNum value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getDynamicNum()
	{
		return dynamicNum;
	}


	/**
	 * 设置动态数字
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setDynamicNum(Vector<uint32_t> value)
	{
		if (value != null) {
				this.dynamicNum = value;
				this.dynamicNum_u = 1;
		}
	}


	/**
	 * 获取tencent 标识
	 * 
	 * 此字段的版本 >= 20130411
	 * @return sigTencent value 类型为:String
	 * 
	 */
	public String getSigTencent()
	{
		return sigTencent;
	}


	/**
	 * 设置tencent 标识
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:String
	 * 
	 */
	public void setSigTencent(String value)
	{
		this.sigTencent = value;
		this.sigTencent_u = 1;
	}


	/**
	 * 获取paipai 标识
	 * 
	 * 此字段的版本 >= 20130411
	 * @return sigUI value 类型为:String
	 * 
	 */
	public String getSigUI()
	{
		return sigUI;
	}


	/**
	 * 设置paipai 标识
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:String
	 * 
	 */
	public void setSigUI(String value)
	{
		this.sigUI = value;
		this.sigUI_u = 1;
	}


	/**
	 * 获取属性字段
	 * 
	 * 此字段的版本 >= 20130411
	 * @return shopProperty value 类型为:String
	 * 
	 */
	public String getShopProperty()
	{
		return shopProperty;
	}


	/**
	 * 设置属性字段
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:String
	 * 
	 */
	public void setShopProperty(String value)
	{
		this.shopProperty = value;
		this.shopProperty_u = 1;
	}


	/**
	 * 获取用户标签
	 * 
	 * 此字段的版本 >= 20130411
	 * @return label value 类型为:String
	 * 
	 */
	public String getLabel()
	{
		return label;
	}


	/**
	 * 设置用户标签
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:String
	 * 
	 */
	public void setLabel(String value)
	{
		this.label = value;
		this.label_u = 1;
	}


	/**
	 * 获取预留字段
	 * 
	 * 此字段的版本 >= 20130411
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置预留字段
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
		this.reserved_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20130411
	 * @return shopName_u value 类型为:short
	 * 
	 */
	public short getShopName_u()
	{
		return shopName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopName_u(short value)
	{
		this.shopName_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20130411
	 * @return shopLogoPos_u value 类型为:short
	 * 
	 */
	public short getShopLogoPos_u()
	{
		return shopLogoPos_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopLogoPos_u(short value)
	{
		this.shopLogoPos_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20130411
	 * @return sellerNickName_u value 类型为:short
	 * 
	 */
	public short getSellerNickName_u()
	{
		return sellerNickName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerNickName_u(short value)
	{
		this.sellerNickName_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20130411
	 * @return shopCredit_u value 类型为:short
	 * 
	 */
	public short getShopCredit_u()
	{
		return shopCredit_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopCredit_u(short value)
	{
		this.shopCredit_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20130411
	 * @return shopGoodRate_u value 类型为:short
	 * 
	 */
	public short getShopGoodRate_u()
	{
		return shopGoodRate_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopGoodRate_u(short value)
	{
		this.shopGoodRate_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20130411
	 * @return favoriteHeat_u value 类型为:short
	 * 
	 */
	public short getFavoriteHeat_u()
	{
		return favoriteHeat_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:short
	 * 
	 */
	public void setFavoriteHeat_u(short value)
	{
		this.favoriteHeat_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20130411
	 * @return dynamicNum_u value 类型为:short
	 * 
	 */
	public short getDynamicNum_u()
	{
		return dynamicNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:short
	 * 
	 */
	public void setDynamicNum_u(short value)
	{
		this.dynamicNum_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20130411
	 * @return sigTencent_u value 类型为:short
	 * 
	 */
	public short getSigTencent_u()
	{
		return sigTencent_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:short
	 * 
	 */
	public void setSigTencent_u(short value)
	{
		this.sigTencent_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20130411
	 * @return sigUI_u value 类型为:short
	 * 
	 */
	public short getSigUI_u()
	{
		return sigUI_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:short
	 * 
	 */
	public void setSigUI_u(short value)
	{
		this.sigUI_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20130411
	 * @return shopProperty_u value 类型为:short
	 * 
	 */
	public short getShopProperty_u()
	{
		return shopProperty_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopProperty_u(short value)
	{
		this.shopProperty_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20130411
	 * @return label_u value 类型为:short
	 * 
	 */
	public short getLabel_u()
	{
		return label_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:short
	 * 
	 */
	public void setLabel_u(short value)
	{
		this.label_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20130411
	 * @return reserved_u value 类型为:short
	 * 
	 */
	public short getReserved_u()
	{
		return reserved_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20130411
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserved_u(short value)
	{
		this.reserved_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(FavShopPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段UserId的长度 size_of(uint32_t)
				length += 4;  //计算字段ShopId的长度 size_of(uint32_t)
				length += 4;  //计算字段AddTime的长度 size_of(uint32_t)
				length += 1;  //计算字段UserId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段ShopId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段AddTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				if(  this.version >= 20130411 ){
						length += ByteStream.getObjectSize(shopName);  //计算字段shopName的长度 size_of(String)
				}
				if(  this.version >= 20130411 ){
						length += ByteStream.getObjectSize(shopLogoPos);  //计算字段shopLogoPos的长度 size_of(String)
				}
				if(  this.version >= 20130411 ){
						length += ByteStream.getObjectSize(sellerNickName);  //计算字段sellerNickName的长度 size_of(String)
				}
				if(  this.version >= 20130411 ){
						length += 4;  //计算字段shopCredit的长度 size_of(uint32_t)
				}
				if(  this.version >= 20130411 ){
						length += ByteStream.getObjectSize(shopGoodRate);  //计算字段shopGoodRate的长度 size_of(String)
				}
				if(  this.version >= 20130411 ){
						length += 4;  //计算字段favoriteHeat的长度 size_of(uint32_t)
				}
				if(  this.version >= 20130411 ){
						length += ByteStream.getObjectSize(dynamicNum);  //计算字段dynamicNum的长度 size_of(Vector)
				}
				if(  this.version >= 20130411 ){
						length += ByteStream.getObjectSize(sigTencent);  //计算字段sigTencent的长度 size_of(String)
				}
				if(  this.version >= 20130411 ){
						length += ByteStream.getObjectSize(sigUI);  //计算字段sigUI的长度 size_of(String)
				}
				if(  this.version >= 20130411 ){
						length += ByteStream.getObjectSize(shopProperty);  //计算字段shopProperty的长度 size_of(String)
				}
				if(  this.version >= 20130411 ){
						length += ByteStream.getObjectSize(label);  //计算字段label的长度 size_of(String)
				}
				if(  this.version >= 20130411 ){
						length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
				}
				if(  this.version >= 20130411 ){
						length += 1;  //计算字段shopName_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20130411 ){
						length += 1;  //计算字段shopLogoPos_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20130411 ){
						length += 1;  //计算字段sellerNickName_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20130411 ){
						length += 1;  //计算字段shopCredit_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20130411 ){
						length += 1;  //计算字段shopGoodRate_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20130411 ){
						length += 1;  //计算字段favoriteHeat_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20130411 ){
						length += 1;  //计算字段dynamicNum_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20130411 ){
						length += 1;  //计算字段sigTencent_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20130411 ){
						length += 1;  //计算字段sigUI_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20130411 ){
						length += 1;  //计算字段shopProperty_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20130411 ){
						length += 1;  //计算字段label_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20130411 ){
						length += 1;  //计算字段reserved_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本20130411所包含的字段*******
 *	long version;///<版本号
 *	long UserId;///<收藏店铺的用户
 *	long ShopId;///<被收藏的店铺的id，即卖家qq号
 *	long AddTime;///<收藏该店铺的时间
 *	short UserId_u;
 *	short ShopId_u;
 *	short AddTime_u;
 *	short version_u;
 *	String shopName;///<店铺名称
 *	String shopLogoPos;///<店铺图片
 *	String sellerNickName;///<店铺卖家昵称
 *	long shopCredit;///<店铺信用
 *	String shopGoodRate;///<店铺好评
 *	long favoriteHeat;///<店铺收藏人数
 *	Vector<uint32_t> dynamicNum;///<动态数字
 *	String sigTencent;///<tencent 标识
 *	String sigUI;///<paipai 标识
 *	String shopProperty;///<属性字段
 *	String label;///<用户标签
 *	String reserved;///<预留字段
 *	short shopName_u;
 *	short shopLogoPos_u;
 *	short sellerNickName_u;
 *	short shopCredit_u;
 *	short shopGoodRate_u;
 *	short favoriteHeat_u;
 *	short dynamicNum_u;
 *	short sigTencent_u;
 *	short sigUI_u;
 *	short shopProperty_u;
 *	short label_u;
 *	short reserved_u;
 *****以上是版本20130411所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
