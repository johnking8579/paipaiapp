 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.favorite.ao.idl.FavoriteApi.java

package com.qq.qqbuy.thirdparty.idl.favorite.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *删除收藏的商品返回类
 *
 *@date 2012-12-26 03:41:56
 *
 *@since version:0
*/
public class  DelFavItemListResp implements IServiceObject
{
	public long result;
	/**
	 * 错误提示信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 输出保留字
	 *
	 * 版本 >= 0
	 */
	 private String outReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushString(errMsg);
		bs.pushString(outReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		errMsg = bs.popString();
		outReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x69098805L;
	}


	/**
	 * 获取错误提示信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误提示信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	/**
	 * 获取输出保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return outReserve;
	}


	/**
	 * 设置输出保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		this.outReserve = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(DelFavItemListResp)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserve);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
