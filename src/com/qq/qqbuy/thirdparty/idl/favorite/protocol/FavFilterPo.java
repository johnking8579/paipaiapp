//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.favorite.ao.idl.FavoritePo.java

package com.qq.qqbuy.thirdparty.idl.favorite.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *收藏查询filter
 *
 *@date 2012-12-26 03:41:48
 *
 *@since version:0
*/
public class FavFilterPo  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 0;

	/**
	 * 用户qq号
	 *
	 * 版本 >= 0
	 */
	 private long uin;

	/**
	 * 页面大小，即请求数量
	 *
	 * 版本 >= 0
	 */
	 private long pageSize;

	/**
	 * 页码
	 *
	 * 版本 >= 0
	 */
	 private long startPage;

	/**
	 * 取收藏信息标志位
	 *
	 * 版本 >= 0
	 */
	 private long option;

	/**
	 * 版本号_u
	 *
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 用户qq号_u
	 *
	 * 版本 >= 0
	 */
	 private short uin_u;

	/**
	 * 页面大小_u
	 *
	 * 版本 >= 0
	 */
	 private short pageSize_u;

	/**
	 * 请求页面_u
	 *
	 * 版本 >= 0
	 */
	 private short startPage_u;

	/**
	 * 取收藏信息标志位
	 *
	 * 版本 >= 0
	 */
	 private short option_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUInt(uin);
		bs.pushUInt(pageSize);
		bs.pushUInt(startPage);
		bs.pushLong(option);
		bs.pushUByte(version_u);
		bs.pushUByte(uin_u);
		bs.pushUByte(pageSize_u);
		bs.pushUByte(startPage_u);
		bs.pushUByte(option_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		uin = bs.popUInt();
		pageSize = bs.popUInt();
		startPage = bs.popUInt();
		option = bs.popLong();
		version_u = bs.popUByte();
		uin_u = bs.popUByte();
		pageSize_u = bs.popUByte();
		startPage_u = bs.popUByte();
		option_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取用户qq号
	 * 
	 * 此字段的版本 >= 0
	 * @return uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return uin;
	}


	/**
	 * 设置用户qq号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.uin = value;
		this.uin_u = 1;
	}


	/**
	 * 获取页面大小，即请求数量
	 * 
	 * 此字段的版本 >= 0
	 * @return pageSize value 类型为:long
	 * 
	 */
	public long getPageSize()
	{
		return pageSize;
	}


	/**
	 * 设置页面大小，即请求数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPageSize(long value)
	{
		this.pageSize = value;
		this.pageSize_u = 1;
	}


	/**
	 * 获取页码
	 * 
	 * 此字段的版本 >= 0
	 * @return startPage value 类型为:long
	 * 
	 */
	public long getStartPage()
	{
		return startPage;
	}


	/**
	 * 设置页码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setStartPage(long value)
	{
		this.startPage = value;
		this.startPage_u = 1;
	}


	/**
	 * 获取取收藏信息标志位
	 * 
	 * 此字段的版本 >= 0
	 * @return option value 类型为:long
	 * 
	 */
	public long getOption()
	{
		return option;
	}


	/**
	 * 设置取收藏信息标志位
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOption(long value)
	{
		this.option = value;
		this.option_u = 1;
	}


	/**
	 * 获取版本号_u
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置版本号_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取用户qq号_u
	 * 
	 * 此字段的版本 >= 0
	 * @return uin_u value 类型为:short
	 * 
	 */
	public short getUin_u()
	{
		return uin_u;
	}


	/**
	 * 设置用户qq号_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUin_u(short value)
	{
		this.uin_u = value;
	}


	/**
	 * 获取页面大小_u
	 * 
	 * 此字段的版本 >= 0
	 * @return pageSize_u value 类型为:short
	 * 
	 */
	public short getPageSize_u()
	{
		return pageSize_u;
	}


	/**
	 * 设置页面大小_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPageSize_u(short value)
	{
		this.pageSize_u = value;
	}


	/**
	 * 获取请求页面_u
	 * 
	 * 此字段的版本 >= 0
	 * @return startPage_u value 类型为:short
	 * 
	 */
	public short getStartPage_u()
	{
		return startPage_u;
	}


	/**
	 * 设置请求页面_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStartPage_u(short value)
	{
		this.startPage_u = value;
	}


	/**
	 * 获取取收藏信息标志位
	 * 
	 * 此字段的版本 >= 0
	 * @return option_u value 类型为:short
	 * 
	 */
	public short getOption_u()
	{
		return option_u;
	}


	/**
	 * 设置取收藏信息标志位
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOption_u(short value)
	{
		this.option_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(FavFilterPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段uin的长度 size_of(uint32_t)
				length += 4;  //计算字段pageSize的长度 size_of(uint32_t)
				length += 4;  //计算字段startPage的长度 size_of(uint32_t)
				length += 17;  //计算字段option的长度 size_of(uint64_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段uin_u的长度 size_of(uint8_t)
				length += 1;  //计算字段pageSize_u的长度 size_of(uint8_t)
				length += 1;  //计算字段startPage_u的长度 size_of(uint8_t)
				length += 1;  //计算字段option_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

    @Override
    public String toString()
    {
        return "FavFilterPo [option=" + option + ", option_u=" + option_u
                + ", pageSize=" + pageSize + ", pageSize_u=" + pageSize_u
                + ", startPage=" + startPage + ", startPage_u=" + startPage_u
                + ", uin=" + uin + ", uin_u=" + uin_u + ", version=" + version
                + ", version_u=" + version_u + "]";
    }

}
