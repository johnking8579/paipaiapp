package com.qq.qqbuy.thirdparty.idl.jdlogin;


import com.paipai.component.c2cplatform.IAsynWebStub;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.PpConstants;
import com.qq.qqbuy.common.util.TokenUtil;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.jdlogin.protocol.*;


public class JdLoginClient extends SupportIDLBaseClient {
	
    /**
     * 
     * @param mk 机器码，必需
     * @param uin 鉴权码，必需，具体请联系joelwzli/silenchen获取
     * @param loginInfoPo 登录信息po, 【注】当前只支持QQ号、微信openid登录
     * @return
     */
    public static UniformLoginResp uniformLogin(String mk, Long uin, LoginInfoPo loginInfoPo, String source ){
        long start = System.currentTimeMillis();
        UniformLoginReq req = new UniformLoginReq();
        UniformLoginResp resp = new UniformLoginResp();
        req.setMachineKey(mk) ;
        req.setSource(source) ;
        req.setSceneId(0l) ;
        req.setOption(0) ;
        req.setAuthCode(TokenUtil.OPEN_SDK_IDL_AUTHCODE) ;
        req.setLoginInfoPo(loginInfoPo) ;
        int ret = 0;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setOperator(uin);
		stub.setRouteKey(uin);
        try {
			ret = stub.invoke(req, resp);
		} catch (Exception e) {			
			Log.run.warn("e",e);
			ret =  ERRCODE_CALLIDL_FAIL;  	
		}
        return ret == SUCCESS ? resp : null;
    }    
    
    /**
     * 
     * @param mk 机器码，必需
     * @param uin 鉴权码，必需，具体请联系joelwzli/silenchen获取
     * @param sk 登录信息po, 【注】当前只支持QQ号、微信openid登录
     * @return
     */
    public static CheckLoginByWgUidResp checkLoginByWgUid(String mk, Long uin, String sk ){
        long start = System.currentTimeMillis();
        CheckLoginByWgUidReq req = new CheckLoginByWgUidReq();
        CheckLoginByWgUidResp resp = new CheckLoginByWgUidResp();
        req.setMachineKey(mk) ;
        req.setSource(PpConstants.PP_SOURCE) ;
        req.setSceneId(0l) ;
        int ret = 0;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setOperator(uin);
		stub.setRouteKey(uin);
        stub.setSkey(sk.getBytes()) ;
		try {
			ret = stub.invoke(req, resp);
		} catch (Exception e) {			
			Log.run.warn("e",e);
			ret =  ERRCODE_CALLIDL_FAIL;  	
		}
        long timeCost = System.currentTimeMillis() - start;
        return ret == SUCCESS ? resp : null;
    }   
    
    /**
     * 
     * @param mk 机器码，必需
     * @param uin 鉴权码，必需，具体请联系joelwzli/silenchen获取
     * @param sk 登录信息po, 【注】当前只支持QQ号、微信openid登录
     * @return
     */
    public static WanggouUserLogoutResp wanggouUserLogout(String mk, Long uin, String sk ){
        long start = System.currentTimeMillis();
        WanggouUserLogoutReq req = new WanggouUserLogoutReq();
        WanggouUserLogoutResp resp = new WanggouUserLogoutResp();
        req.setMachineKey(mk) ;
        req.setSource(PpConstants.PP_SOURCE) ;
        req.setSceneId(0l) ;
        req.setAuthCode(TokenUtil.OPEN_SDK_IDL_AUTHCODE) ;
        int ret = 0;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setOperator(uin);
		stub.setRouteKey(uin);
		stub.setSkey(sk.getBytes()) ;
		try {
			ret = stub.invoke(req, resp);
		} catch (Exception e) {			
			Log.run.warn("e",e);
			ret =  ERRCODE_CALLIDL_FAIL;  	
		}
        return ret == SUCCESS ? resp : null;
    }   

	
}
