package com.qq.qqbuy.thirdparty.idl.jdlogin.protocol;

//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.b2b2c.user.idl.BatchGetUserInfoByWgUidResp.java



import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;

import com.paipai.lang.uint16_t;
import com.paipai.lang.uint32_t;
import com.paipai.lang.uint8_t;
import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

/**
 *买家用户信息Po
 *
 *@date 2015-02-10 02:07:42
 *
 *@since version:0
*/
public class BuyerInfoPo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20141229;

	/**
	 * 网购用户内部id，非QQ号，目前仅支持32位
	 *
	 * 版本 >= 0
	 */
	 private long wgUid;

	/**
	 * 易迅用户内部id，目前仅支持32位
	 *
	 * 版本 >= 0
	 */
	 private long icsonUid;

	/**
	 * 绑定的用户QQ号，目前仅支持32位
	 *
	 * 版本 >= 0
	 */
	 private long qQNumber;

	/**
	 * 用户帐号名，包括易迅注册帐号、非qq帐号的第三方联合登录，如LoginAlipay+openid等字符串帐户名
	 *
	 * 版本 >= 0
	 */
	 private String loginAccount = new String();

	/**
	 * 用户帐号类型，1-QQ号 2-个性化帐号，参见user_comm_define.h中的E_ICSON_USER_ACCOUNT_TYPE
	 *
	 * 版本 >= 0
	 */
	 private short accountType;

	/**
	 * 真实姓名
	 *
	 * 版本 >= 0
	 */
	 private String truename = new String();

	/**
	 * 昵称
	 *
	 * 版本 >= 0
	 */
	 private String nickname = new String();

	/**
	 * 用户属性位BitSet，属性位代表的意义请参见b2b2c_define.h中的USER_PROPERTY
	 *
	 * 版本 >= 0
	 */
	 private BitSet bitProperty = new BitSet();

	/**
	 * 用户属性级别，如彩钻级别，Map中第一个uint32_t表示用户属性位值，第二个uint32_t表示级别值
	 *
	 * 版本 >= 0
	 */
	 private Map<uint32_t,uint32_t> userFlagLevel = new HashMap<uint32_t,uint32_t>();

	/**
	 * 性别, 0-不明 1-男 2-女 参见user_comm_define.h中的E_USER_SEX
	 *
	 * 版本 >= 0
	 */
	 private short sex;

	/**
	 * 年龄
	 *
	 * 版本 >= 0
	 */
	 private short age;

	/**
	 * 手机号，如绑定手机的用户属性位为1，则表示绑定的手机号
	 *
	 * 版本 >= 0
	 */
	 private String mobile = new String();

	/**
	 * 电子邮箱，如绑定邮箱的用户属性位为1，则表示绑定的邮箱
	 *
	 * 版本 >= 0
	 */
	 private String email = new String();

	/**
	 * 固定电话
	 *
	 * 版本 >= 0
	 */
	 private String phone = new String();

	/**
	 * 传真
	 *
	 * 版本 >= 0
	 */
	 private String fax = new String();

	/**
	 * 用户所在地区id
	 *
	 * 版本 >= 0
	 */
	 private long region;

	/**
	 * 邮政编码
	 *
	 * 版本 >= 0
	 */
	 private String postcode = new String();

	/**
	 * 用户详细住址
	 *
	 * 版本 >= 0
	 */
	 private String address = new String();

	/**
	 * 身份证件类型，1-身份证 参见user_comm_define.h中E_USER_IDTYPE
	 *
	 * 版本 >= 0
	 */
	 private short identityType;

	/**
	 * 身份证件号码
	 *
	 * 版本 >= 0
	 */
	 private String identityNum = new String();

	/**
	 * 用户信用值
	 *
	 * 版本 >= 0
	 */
	 private int buyerCredit;

	/**
	 * 用户经验值
	 *
	 * 版本 >= 0
	 */
	 private long experience;

	/**
	 * 财付通账号
	 *
	 * 版本 >= 0
	 */
	 private String cftAccount = new String();

	/**
	 * 登录级别，预留
	 *
	 * 版本 >= 0
	 */
	 private short loginLevel;

	/**
	 * 用户类型，从易迅导入，参见user_comm_define.h中E_USER_TYPE
	 *
	 * 版本 >= 0
	 */
	 private short userType;

	/**
	 * 经销商等级，从易迅导入
	 *
	 * 版本 >= 0
	 */
	 private short retailerLevel;

	/**
	 * 易迅会员等级，从易迅导入
	 *
	 * 版本 >= 0
	 */
	 private short icsonMemberLevel;

	/**
	 * 最后修改时间
	 *
	 * 版本 >= 0
	 */
	 private long lastUpdateTime;

	/**
	 * 用户注册时间
	 *
	 * 版本 >= 0
	 */
	 private long regTime;

	/**
	 * reserve string
	 *
	 * 版本 >= 0
	 */
	 private String reserveStr = new String();

	/**
	 * reserve int
	 *
	 * 版本 >= 0
	 */
	 private long reserveInt;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 版本 >= 0
	 */
	 private short wgUid_u;

	/**
	 * 版本 >= 0
	 */
	 private short icsonUid_u;

	/**
	 * 版本 >= 0
	 */
	 private short qQNumber_u;

	/**
	 * 版本 >= 0
	 */
	 private short loginAccount_u;

	/**
	 * 版本 >= 0
	 */
	 private short accountType_u;

	/**
	 * 版本 >= 0
	 */
	 private short truename_u;

	/**
	 * 版本 >= 0
	 */
	 private short nickname_u;

	/**
	 * 版本 >= 0
	 */
	 private short bitProperty_u;

	/**
	 * 版本 >= 0
	 */
	 private short userFlagLevel_u;

	/**
	 * 版本 >= 0
	 */
	 private short sex_u;

	/**
	 * 版本 >= 0
	 */
	 private short age_u;

	/**
	 * 版本 >= 0
	 */
	 private short mobile_u;

	/**
	 * 版本 >= 0
	 */
	 private short email_u;

	/**
	 * 版本 >= 0
	 */
	 private short phone_u;

	/**
	 * 版本 >= 0
	 */
	 private short fax_u;

	/**
	 * 版本 >= 0
	 */
	 private short region_u;

	/**
	 * 版本 >= 0
	 */
	 private short postcode_u;

	/**
	 * 版本 >= 0
	 */
	 private short address_u;

	/**
	 * 版本 >= 0
	 */
	 private short identityType_u;

	/**
	 * 版本 >= 0
	 */
	 private short identityNum_u;

	/**
	 * 版本 >= 0
	 */
	 private short buyerCredit_u;

	/**
	 * 版本 >= 0
	 */
	 private short experience_u;

	/**
	 * 版本 >= 0
	 */
	 private short cftAccount_u;

	/**
	 * 版本 >= 0
	 */
	 private short loginLevel_u;

	/**
	 * 版本 >= 0
	 */
	 private short userType_u;

	/**
	 * 版本 >= 0
	 */
	 private short retailerLevel_u;

	/**
	 * 版本 >= 0
	 */
	 private short icsonMemberLevel_u;

	/**
	 * 版本 >= 0
	 */
	 private short lastUpdateTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short regTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short reserveStr_u;

	/**
	 * 版本 >= 0
	 */
	 private short reserveInt_u;

	/**
	 * 易迅openid
	 *
	 * 版本 >= 20130314
	 */
	 private String weChatId = new String();

	/**
	 * 
	 *
	 * 版本 >= 20130314
	 */
	 private short weChatId_u;

	/**
	 * 虚拟经验值
	 *
	 * 版本 >= 20130429
	 */
	 private long virtualExpPoints;

	/**
	 * 
	 *
	 * 版本 >= 20130429
	 */
	 private short virtualExpPoints_u;

	/**
	 * 登录密码安全级别
	 *
	 * 版本 >= 20130529
	 */
	 private short passwordSecureLevel;

	/**
	 * 
	 *
	 * 版本 >= 20130529
	 */
	 private short passwordSecureLevel_u;

	/**
	 * 不同来源用户注册时间(微购等), key为来源，value为注册时间。易迅用户注册时间请参考另一个字段regTime
	 *
	 * 版本 >= 20130918
	 */
	 private Map<uint32_t,uint32_t> mapDiffSrcRegTime = new HashMap<uint32_t,uint32_t>();

	/**
	 * 
	 *
	 * 版本 >= 20130918
	 */
	 private short mapDiffSrcRegTime_u;

	/**
	 * 网购openid
	 *
	 * 版本 >= 20131220
	 */
	 private String wanggouOpenid = new String();

	/**
	 * 
	 *
	 * 版本 >= 20131220
	 */
	 private short wanggouOpenid_u;

	/**
	 * 卖家总信用
	 *
	 * 版本 >= 20141229
	 */
	 private int sellerCredit;

	/**
	 * 
	 *
	 * 版本 >= 20141229
	 */
	 private short sellerCredit_u;

	/**
	 * 认证信息
	 *
	 * 版本 >= 20141229
	 */
	 private short authMask;

	/**
	 * 
	 *
	 * 版本 >= 20141229
	 */
	 private short authMask_u;

	/**
	 * 用户属性
	 *
	 * 版本 >= 20141229
	 */
	 private Map<uint16_t,uint8_t> mapProp = new HashMap<uint16_t,uint8_t>();

	/**
	 * 
	 *
	 * 版本 >= 20141229
	 */
	 private short mapProp_u;

	/**
	 * 留言标志位
	 *
	 * 版本 >= 20141229
	 */
	 private int recvMsgMask;

	/**
	 * 
	 *
	 * 版本 >= 20141229
	 */
	 private short recvMsgMask_u;

	/**
	 * 用户信息标志位
	 *
	 * 版本 >= 20141229
	 */
	 private long userInfoMask;

	/**
	 * 
	 *
	 * 版本 >= 20141229
	 */
	 private short userInfoMask_u;

	/**
	 * 省份
	 *
	 * 版本 >= 20141229
	 */
	 private short province;

	/**
	 * 
	 *
	 * 版本 >= 20141229
	 */
	 private short province_u;

	/**
	 * 城市
	 *
	 * 版本 >= 20141229
	 */
	 private int city;

	/**
	 * 
	 *
	 * 版本 >= 20141229
	 */
	 private short city_u;

	/**
	 * 城市
	 *
	 * 版本 >= 20141229
	 */
	 private long country;

	/**
	 * 
	 *
	 * 版本 >= 20141229
	 */
	 private short country_u;

	/**
	 * 肖像图片
	 *
	 * 版本 >= 20141229
	 */
	 private String mainLogoPos = new String();

	/**
	 * 
	 *
	 * 版本 >= 20141229
	 */
	 private short mainLogoPos_u;

	/**
	 * 联系方式
	 *
	 * 版本 >= 20141229
	 */
	 private short commContactType;

	/**
	 * 
	 *
	 * 版本 >= 20141229
	 */
	 private short commContactType_u;

	/**
	 * 最后登录IP
	 *
	 * 版本 >= 20141229
	 */
	 private long lastLoginIp;

	/**
	 * 
	 *
	 * 版本 >= 20141229
	 */
	 private short lastLoginIp_u;

	/**
	 * 最后登录时间
	 *
	 * 版本 >= 20141229
	 */
	 private long lastLoginTime;

	/**
	 * 
	 *
	 * 版本 >= 20141229
	 */
	 private short lastLoginTime_u;

	/**
	 * 登录类型
	 *
	 * 版本 >= 20141229
	 */
	 private short lastLoginType;

	/**
	 * 
	 *
	 * 版本 >= 20141229
	 */
	 private short lastLoginType_u;

	/**
	 * 参考信用
	 *
	 * 版本 >= 20141229
	 */
	 private String referCredit = new String();

	/**
	 * 
	 *
	 * 版本 >= 20141229
	 */
	 private short referCredit_u;

	/**
	 * 卖家虚拟信用
	 *
	 * 版本 >= 20141229
	 */
	 private int virtualCredit;

	/**
	 * 
	 *
	 * 版本 >= 20141229
	 */
	 private short virtualCredit_u;

	/**
	 * 卖家实物信用
	 *
	 * 版本 >= 20141229
	 */
	 private int objectCredit;

	/**
	 * 
	 *
	 * 版本 >= 20141229
	 */
	 private short objectCredit_u;

	/**
	 * 卖家联系方式
	 *
	 * 版本 >= 20141229
	 */
	 private String contacts = new String();

	/**
	 * 
	 *
	 * 版本 >= 20141229
	 */
	 private short contacts_u;

	/**
	 * 多工号权限系统员工所属的卖家号
	 *
	 * 版本 >= 20141229
	 */
	 private long sellerUin;

	/**
	 * 
	 *
	 * 版本 >= 20141229
	 */
	 private short sellerUin_u;

	/**
	 * 店铺类目id[用作店铺类型]
	 *
	 * 版本 >= 20141229
	 */
	 private long shopClassId;

	/**
	 * 
	 *
	 * 版本 >= 20141229
	 */
	 private short shopClassId_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushLong(wgUid);
		bs.pushLong(icsonUid);
		bs.pushLong(qQNumber);
		bs.pushString(loginAccount);
		bs.pushUByte(accountType);
		bs.pushString(truename);
		bs.pushString(nickname);
		bs.pushBitSet(bitProperty);
		bs.pushObject(userFlagLevel);
		bs.pushUByte(sex);
		bs.pushUByte(age);
		bs.pushString(mobile);
		bs.pushString(email);
		bs.pushString(phone);
		bs.pushString(fax);
		bs.pushUInt(region);
		bs.pushString(postcode);
		bs.pushString(address);
		bs.pushUByte(identityType);
		bs.pushString(identityNum);
		bs.pushInt(buyerCredit);
		bs.pushUInt(experience);
		bs.pushString(cftAccount);
		bs.pushUByte(loginLevel);
		bs.pushUByte(userType);
		bs.pushUByte(retailerLevel);
		bs.pushUByte(icsonMemberLevel);
		bs.pushUInt(lastUpdateTime);
		bs.pushUInt(regTime);
		bs.pushString(reserveStr);
		bs.pushUInt(reserveInt);
		bs.pushUByte(version_u);
		bs.pushUByte(wgUid_u);
		bs.pushUByte(icsonUid_u);
		bs.pushUByte(qQNumber_u);
		bs.pushUByte(loginAccount_u);
		bs.pushUByte(accountType_u);
		bs.pushUByte(truename_u);
		bs.pushUByte(nickname_u);
		bs.pushUByte(bitProperty_u);
		bs.pushUByte(userFlagLevel_u);
		bs.pushUByte(sex_u);
		bs.pushUByte(age_u);
		bs.pushUByte(mobile_u);
		bs.pushUByte(email_u);
		bs.pushUByte(phone_u);
		bs.pushUByte(fax_u);
		bs.pushUByte(region_u);
		bs.pushUByte(postcode_u);
		bs.pushUByte(address_u);
		bs.pushUByte(identityType_u);
		bs.pushUByte(identityNum_u);
		bs.pushUByte(buyerCredit_u);
		bs.pushUByte(experience_u);
		bs.pushUByte(cftAccount_u);
		bs.pushUByte(loginLevel_u);
		bs.pushUByte(userType_u);
		bs.pushUByte(retailerLevel_u);
		bs.pushUByte(icsonMemberLevel_u);
		bs.pushUByte(lastUpdateTime_u);
		bs.pushUByte(regTime_u);
		bs.pushUByte(reserveStr_u);
		bs.pushUByte(reserveInt_u);
		if(  this.version >= 20130314 ){
				bs.pushString(weChatId);
		}
		if(  this.version >= 20130314 ){
				bs.pushUByte(weChatId_u);
		}
		if(  this.version >= 20130429 ){
				bs.pushUInt(virtualExpPoints);
		}
		if(  this.version >= 20130429 ){
				bs.pushUByte(virtualExpPoints_u);
		}
		if(  this.version >= 20130529 ){
				bs.pushUByte(passwordSecureLevel);
		}
		if(  this.version >= 20130529 ){
				bs.pushUByte(passwordSecureLevel_u);
		}
		if(  this.version >= 20130918 ){
				bs.pushObject(mapDiffSrcRegTime);
		}
		if(  this.version >= 20130918 ){
				bs.pushUByte(mapDiffSrcRegTime_u);
		}
		if(  this.version >= 20131220 ){
				bs.pushString(wanggouOpenid);
		}
		if(  this.version >= 20131220 ){
				bs.pushUByte(wanggouOpenid_u);
		}
		if(  this.version >= 20141229 ){
				bs.pushInt(sellerCredit);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(sellerCredit_u);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(authMask);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(authMask_u);
		}
		if(  this.version >= 20141229 ){
				bs.pushObject(mapProp);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(mapProp_u);
		}
		if(  this.version >= 20141229 ){
				bs.pushUShort(recvMsgMask);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(recvMsgMask_u);
		}
		if(  this.version >= 20141229 ){
				bs.pushUInt(userInfoMask);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(userInfoMask_u);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(province);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(province_u);
		}
		if(  this.version >= 20141229 ){
				bs.pushUShort(city);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(city_u);
		}
		if(  this.version >= 20141229 ){
				bs.pushUInt(country);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(country_u);
		}
		if(  this.version >= 20141229 ){
				bs.pushString(mainLogoPos);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(mainLogoPos_u);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(commContactType);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(commContactType_u);
		}
		if(  this.version >= 20141229 ){
				bs.pushUInt(lastLoginIp);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(lastLoginIp_u);
		}
		if(  this.version >= 20141229 ){
				bs.pushUInt(lastLoginTime);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(lastLoginTime_u);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(lastLoginType);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(lastLoginType_u);
		}
		if(  this.version >= 20141229 ){
				bs.pushString(referCredit);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(referCredit_u);
		}
		if(  this.version >= 20141229 ){
				bs.pushInt(virtualCredit);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(virtualCredit_u);
		}
		if(  this.version >= 20141229 ){
				bs.pushInt(objectCredit);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(objectCredit_u);
		}
		if(  this.version >= 20141229 ){
				bs.pushString(contacts);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(contacts_u);
		}
		if(  this.version >= 20141229 ){
				bs.pushUInt(sellerUin);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(sellerUin_u);
		}
		if(  this.version >= 20141229 ){
				bs.pushUInt(shopClassId);
		}
		if(  this.version >= 20141229 ){
				bs.pushUByte(shopClassId_u);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		wgUid = bs.popLong();
		icsonUid = bs.popLong();
		qQNumber = bs.popLong();
		loginAccount = bs.popString();
		accountType = bs.popUByte();
		truename = bs.popString();
		nickname = bs.popString();
		bitProperty = bs.popBitSet();
		userFlagLevel = (Map<uint32_t,uint32_t>)bs.popMap(uint32_t.class,uint32_t.class);
		sex = bs.popUByte();
		age = bs.popUByte();
		mobile = bs.popString();
		email = bs.popString();
		phone = bs.popString();
		fax = bs.popString();
		region = bs.popUInt();
		postcode = bs.popString();
		address = bs.popString();
		identityType = bs.popUByte();
		identityNum = bs.popString();
		buyerCredit = bs.popInt();
		experience = bs.popUInt();
		cftAccount = bs.popString();
		loginLevel = bs.popUByte();
		userType = bs.popUByte();
		retailerLevel = bs.popUByte();
		icsonMemberLevel = bs.popUByte();
		lastUpdateTime = bs.popUInt();
		regTime = bs.popUInt();
		reserveStr = bs.popString();
		reserveInt = bs.popUInt();
		version_u = bs.popUByte();
		wgUid_u = bs.popUByte();
		icsonUid_u = bs.popUByte();
		qQNumber_u = bs.popUByte();
		loginAccount_u = bs.popUByte();
		accountType_u = bs.popUByte();
		truename_u = bs.popUByte();
		nickname_u = bs.popUByte();
		bitProperty_u = bs.popUByte();
		userFlagLevel_u = bs.popUByte();
		sex_u = bs.popUByte();
		age_u = bs.popUByte();
		mobile_u = bs.popUByte();
		email_u = bs.popUByte();
		phone_u = bs.popUByte();
		fax_u = bs.popUByte();
		region_u = bs.popUByte();
		postcode_u = bs.popUByte();
		address_u = bs.popUByte();
		identityType_u = bs.popUByte();
		identityNum_u = bs.popUByte();
		buyerCredit_u = bs.popUByte();
		experience_u = bs.popUByte();
		cftAccount_u = bs.popUByte();
		loginLevel_u = bs.popUByte();
		userType_u = bs.popUByte();
		retailerLevel_u = bs.popUByte();
		icsonMemberLevel_u = bs.popUByte();
		lastUpdateTime_u = bs.popUByte();
		regTime_u = bs.popUByte();
		reserveStr_u = bs.popUByte();
		reserveInt_u = bs.popUByte();
		if(  this.version >= 20130314 ){
				weChatId = bs.popString();
		}
		if(  this.version >= 20130314 ){
				weChatId_u = bs.popUByte();
		}
		if(  this.version >= 20130429 ){
				virtualExpPoints = bs.popUInt();
		}
		if(  this.version >= 20130429 ){
				virtualExpPoints_u = bs.popUByte();
		}
		if(  this.version >= 20130529 ){
				passwordSecureLevel = bs.popUByte();
		}
		if(  this.version >= 20130529 ){
				passwordSecureLevel_u = bs.popUByte();
		}
		if(  this.version >= 20130918 ){
				mapDiffSrcRegTime = (Map<uint32_t,uint32_t>)bs.popMap(uint32_t.class,uint32_t.class);
		}
		if(  this.version >= 20130918 ){
				mapDiffSrcRegTime_u = bs.popUByte();
		}
		if(  this.version >= 20131220 ){
				wanggouOpenid = bs.popString();
		}
		if(  this.version >= 20131220 ){
				wanggouOpenid_u = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				sellerCredit = bs.popInt();
		}
		if(  this.version >= 20141229 ){
				sellerCredit_u = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				authMask = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				authMask_u = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				mapProp = (Map<uint16_t,uint8_t>)bs.popMap(uint16_t.class,uint8_t.class);
		}
		if(  this.version >= 20141229 ){
				mapProp_u = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				recvMsgMask = bs.popUShort();
		}
		if(  this.version >= 20141229 ){
				recvMsgMask_u = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				userInfoMask = bs.popUInt();
		}
		if(  this.version >= 20141229 ){
				userInfoMask_u = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				province = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				province_u = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				city = bs.popUShort();
		}
		if(  this.version >= 20141229 ){
				city_u = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				country = bs.popUInt();
		}
		if(  this.version >= 20141229 ){
				country_u = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				mainLogoPos = bs.popString();
		}
		if(  this.version >= 20141229 ){
				mainLogoPos_u = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				commContactType = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				commContactType_u = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				lastLoginIp = bs.popUInt();
		}
		if(  this.version >= 20141229 ){
				lastLoginIp_u = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				lastLoginTime = bs.popUInt();
		}
		if(  this.version >= 20141229 ){
				lastLoginTime_u = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				lastLoginType = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				lastLoginType_u = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				referCredit = bs.popString();
		}
		if(  this.version >= 20141229 ){
				referCredit_u = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				virtualCredit = bs.popInt();
		}
		if(  this.version >= 20141229 ){
				virtualCredit_u = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				objectCredit = bs.popInt();
		}
		if(  this.version >= 20141229 ){
				objectCredit_u = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				contacts = bs.popString();
		}
		if(  this.version >= 20141229 ){
				contacts_u = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				sellerUin = bs.popUInt();
		}
		if(  this.version >= 20141229 ){
				sellerUin_u = bs.popUByte();
		}
		if(  this.version >= 20141229 ){
				shopClassId = bs.popUInt();
		}
		if(  this.version >= 20141229 ){
				shopClassId_u = bs.popUByte();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取网购用户内部id，非QQ号，目前仅支持32位
	 * 
	 * 此字段的版本 >= 0
	 * @return wgUid value 类型为:long
	 * 
	 */
	public long getWgUid()
	{
		return wgUid;
	}


	/**
	 * 设置网购用户内部id，非QQ号，目前仅支持32位
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWgUid(long value)
	{
		this.wgUid = value;
		this.wgUid_u = 1;
	}

	public boolean issetWgUid()
	{
		return this.wgUid_u != 0;
	}
	/**
	 * 获取易迅用户内部id，目前仅支持32位
	 * 
	 * 此字段的版本 >= 0
	 * @return icsonUid value 类型为:long
	 * 
	 */
	public long getIcsonUid()
	{
		return icsonUid;
	}


	/**
	 * 设置易迅用户内部id，目前仅支持32位
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setIcsonUid(long value)
	{
		this.icsonUid = value;
		this.icsonUid_u = 1;
	}

	public boolean issetIcsonUid()
	{
		return this.icsonUid_u != 0;
	}
	/**
	 * 获取绑定的用户QQ号，目前仅支持32位
	 * 
	 * 此字段的版本 >= 0
	 * @return qQNumber value 类型为:long
	 * 
	 */
	public long getQQNumber()
	{
		return qQNumber;
	}


	/**
	 * 设置绑定的用户QQ号，目前仅支持32位
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setQQNumber(long value)
	{
		this.qQNumber = value;
		this.qQNumber_u = 1;
	}

	public boolean issetQQNumber()
	{
		return this.qQNumber_u != 0;
	}
	/**
	 * 获取用户帐号名，包括易迅注册帐号、非qq帐号的第三方联合登录，如LoginAlipay+openid等字符串帐户名
	 * 
	 * 此字段的版本 >= 0
	 * @return loginAccount value 类型为:String
	 * 
	 */
	public String getLoginAccount()
	{
		return loginAccount;
	}


	/**
	 * 设置用户帐号名，包括易迅注册帐号、非qq帐号的第三方联合登录，如LoginAlipay+openid等字符串帐户名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setLoginAccount(String value)
	{
		this.loginAccount = value;
		this.loginAccount_u = 1;
	}

	public boolean issetLoginAccount()
	{
		return this.loginAccount_u != 0;
	}
	/**
	 * 获取用户帐号类型，1-QQ号 2-个性化帐号，参见user_comm_define.h中的E_ICSON_USER_ACCOUNT_TYPE
	 * 
	 * 此字段的版本 >= 0
	 * @return accountType value 类型为:short
	 * 
	 */
	public short getAccountType()
	{
		return accountType;
	}


	/**
	 * 设置用户帐号类型，1-QQ号 2-个性化帐号，参见user_comm_define.h中的E_ICSON_USER_ACCOUNT_TYPE
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAccountType(short value)
	{
		this.accountType = value;
		this.accountType_u = 1;
	}

	public boolean issetAccountType()
	{
		return this.accountType_u != 0;
	}
	/**
	 * 获取真实姓名
	 * 
	 * 此字段的版本 >= 0
	 * @return truename value 类型为:String
	 * 
	 */
	public String getTruename()
	{
		return truename;
	}


	/**
	 * 设置真实姓名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setTruename(String value)
	{
		this.truename = value;
		this.truename_u = 1;
	}

	public boolean issetTruename()
	{
		return this.truename_u != 0;
	}
	/**
	 * 获取昵称
	 * 
	 * 此字段的版本 >= 0
	 * @return nickname value 类型为:String
	 * 
	 */
	public String getNickname()
	{
		return nickname;
	}


	/**
	 * 设置昵称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setNickname(String value)
	{
		this.nickname = value;
		this.nickname_u = 1;
	}

	public boolean issetNickname()
	{
		return this.nickname_u != 0;
	}
	/**
	 * 获取用户属性位BitSet，属性位代表的意义请参见b2b2c_define.h中的USER_PROPERTY
	 * 
	 * 此字段的版本 >= 0
	 * @return bitProperty value 类型为:BitSet
	 * 
	 */
	public BitSet getBitProperty()
	{
		return bitProperty;
	}


	/**
	 * 设置用户属性位BitSet，属性位代表的意义请参见b2b2c_define.h中的USER_PROPERTY
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:BitSet
	 * 
	 */
	public void setBitProperty(BitSet value)
	{
		if (value != null) {
				this.bitProperty = value;
				this.bitProperty_u = 1;
		}
	}

	public boolean issetBitProperty()
	{
		return this.bitProperty_u != 0;
	}
	/**
	 * 获取用户属性级别，如彩钻级别，Map中第一个uint32_t表示用户属性位值，第二个uint32_t表示级别值
	 * 
	 * 此字段的版本 >= 0
	 * @return userFlagLevel value 类型为:Map<uint32_t,uint32_t>
	 * 
	 */
	public Map<uint32_t,uint32_t> getUserFlagLevel()
	{
		return userFlagLevel;
	}


	/**
	 * 设置用户属性级别，如彩钻级别，Map中第一个uint32_t表示用户属性位值，第二个uint32_t表示级别值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<uint32_t,uint32_t>
	 * 
	 */
	public void setUserFlagLevel(Map<uint32_t,uint32_t> value)
	{
		if (value != null) {
				this.userFlagLevel = value;
				this.userFlagLevel_u = 1;
		}
	}

	public boolean issetUserFlagLevel()
	{
		return this.userFlagLevel_u != 0;
	}
	/**
	 * 获取性别, 0-不明 1-男 2-女 参见user_comm_define.h中的E_USER_SEX
	 * 
	 * 此字段的版本 >= 0
	 * @return sex value 类型为:short
	 * 
	 */
	public short getSex()
	{
		return sex;
	}


	/**
	 * 设置性别, 0-不明 1-男 2-女 参见user_comm_define.h中的E_USER_SEX
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSex(short value)
	{
		this.sex = value;
		this.sex_u = 1;
	}

	public boolean issetSex()
	{
		return this.sex_u != 0;
	}
	/**
	 * 获取年龄
	 * 
	 * 此字段的版本 >= 0
	 * @return age value 类型为:short
	 * 
	 */
	public short getAge()
	{
		return age;
	}


	/**
	 * 设置年龄
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAge(short value)
	{
		this.age = value;
		this.age_u = 1;
	}

	public boolean issetAge()
	{
		return this.age_u != 0;
	}
	/**
	 * 获取手机号，如绑定手机的用户属性位为1，则表示绑定的手机号
	 * 
	 * 此字段的版本 >= 0
	 * @return mobile value 类型为:String
	 * 
	 */
	public String getMobile()
	{
		return mobile;
	}


	/**
	 * 设置手机号，如绑定手机的用户属性位为1，则表示绑定的手机号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMobile(String value)
	{
		this.mobile = value;
		this.mobile_u = 1;
	}

	public boolean issetMobile()
	{
		return this.mobile_u != 0;
	}
	/**
	 * 获取电子邮箱，如绑定邮箱的用户属性位为1，则表示绑定的邮箱
	 * 
	 * 此字段的版本 >= 0
	 * @return email value 类型为:String
	 * 
	 */
	public String getEmail()
	{
		return email;
	}


	/**
	 * 设置电子邮箱，如绑定邮箱的用户属性位为1，则表示绑定的邮箱
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setEmail(String value)
	{
		this.email = value;
		this.email_u = 1;
	}

	public boolean issetEmail()
	{
		return this.email_u != 0;
	}
	/**
	 * 获取固定电话
	 * 
	 * 此字段的版本 >= 0
	 * @return phone value 类型为:String
	 * 
	 */
	public String getPhone()
	{
		return phone;
	}


	/**
	 * 设置固定电话
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPhone(String value)
	{
		this.phone = value;
		this.phone_u = 1;
	}

	public boolean issetPhone()
	{
		return this.phone_u != 0;
	}
	/**
	 * 获取传真
	 * 
	 * 此字段的版本 >= 0
	 * @return fax value 类型为:String
	 * 
	 */
	public String getFax()
	{
		return fax;
	}


	/**
	 * 设置传真
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setFax(String value)
	{
		this.fax = value;
		this.fax_u = 1;
	}

	public boolean issetFax()
	{
		return this.fax_u != 0;
	}
	/**
	 * 获取用户所在地区id
	 * 
	 * 此字段的版本 >= 0
	 * @return region value 类型为:long
	 * 
	 */
	public long getRegion()
	{
		return region;
	}


	/**
	 * 设置用户所在地区id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRegion(long value)
	{
		this.region = value;
		this.region_u = 1;
	}

	public boolean issetRegion()
	{
		return this.region_u != 0;
	}
	/**
	 * 获取邮政编码
	 * 
	 * 此字段的版本 >= 0
	 * @return postcode value 类型为:String
	 * 
	 */
	public String getPostcode()
	{
		return postcode;
	}


	/**
	 * 设置邮政编码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPostcode(String value)
	{
		this.postcode = value;
		this.postcode_u = 1;
	}

	public boolean issetPostcode()
	{
		return this.postcode_u != 0;
	}
	/**
	 * 获取用户详细住址
	 * 
	 * 此字段的版本 >= 0
	 * @return address value 类型为:String
	 * 
	 */
	public String getAddress()
	{
		return address;
	}


	/**
	 * 设置用户详细住址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAddress(String value)
	{
		this.address = value;
		this.address_u = 1;
	}

	public boolean issetAddress()
	{
		return this.address_u != 0;
	}
	/**
	 * 获取身份证件类型，1-身份证 参见user_comm_define.h中E_USER_IDTYPE
	 * 
	 * 此字段的版本 >= 0
	 * @return identityType value 类型为:short
	 * 
	 */
	public short getIdentityType()
	{
		return identityType;
	}


	/**
	 * 设置身份证件类型，1-身份证 参见user_comm_define.h中E_USER_IDTYPE
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIdentityType(short value)
	{
		this.identityType = value;
		this.identityType_u = 1;
	}

	public boolean issetIdentityType()
	{
		return this.identityType_u != 0;
	}
	/**
	 * 获取身份证件号码
	 * 
	 * 此字段的版本 >= 0
	 * @return identityNum value 类型为:String
	 * 
	 */
	public String getIdentityNum()
	{
		return identityNum;
	}


	/**
	 * 设置身份证件号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setIdentityNum(String value)
	{
		this.identityNum = value;
		this.identityNum_u = 1;
	}

	public boolean issetIdentityNum()
	{
		return this.identityNum_u != 0;
	}
	/**
	 * 获取用户信用值
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerCredit value 类型为:int
	 * 
	 */
	public int getBuyerCredit()
	{
		return buyerCredit;
	}


	/**
	 * 设置用户信用值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setBuyerCredit(int value)
	{
		this.buyerCredit = value;
		this.buyerCredit_u = 1;
	}

	public boolean issetBuyerCredit()
	{
		return this.buyerCredit_u != 0;
	}
	/**
	 * 获取用户经验值
	 * 
	 * 此字段的版本 >= 0
	 * @return experience value 类型为:long
	 * 
	 */
	public long getExperience()
	{
		return experience;
	}


	/**
	 * 设置用户经验值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setExperience(long value)
	{
		this.experience = value;
		this.experience_u = 1;
	}

	public boolean issetExperience()
	{
		return this.experience_u != 0;
	}
	/**
	 * 获取财付通账号
	 * 
	 * 此字段的版本 >= 0
	 * @return cftAccount value 类型为:String
	 * 
	 */
	public String getCftAccount()
	{
		return cftAccount;
	}


	/**
	 * 设置财付通账号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCftAccount(String value)
	{
		this.cftAccount = value;
		this.cftAccount_u = 1;
	}

	public boolean issetCftAccount()
	{
		return this.cftAccount_u != 0;
	}
	/**
	 * 获取登录级别，预留
	 * 
	 * 此字段的版本 >= 0
	 * @return loginLevel value 类型为:short
	 * 
	 */
	public short getLoginLevel()
	{
		return loginLevel;
	}


	/**
	 * 设置登录级别，预留
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLoginLevel(short value)
	{
		this.loginLevel = value;
		this.loginLevel_u = 1;
	}

	public boolean issetLoginLevel()
	{
		return this.loginLevel_u != 0;
	}
	/**
	 * 获取用户类型，从易迅导入，参见user_comm_define.h中E_USER_TYPE
	 * 
	 * 此字段的版本 >= 0
	 * @return userType value 类型为:short
	 * 
	 */
	public short getUserType()
	{
		return userType;
	}


	/**
	 * 设置用户类型，从易迅导入，参见user_comm_define.h中E_USER_TYPE
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUserType(short value)
	{
		this.userType = value;
		this.userType_u = 1;
	}

	public boolean issetUserType()
	{
		return this.userType_u != 0;
	}
	/**
	 * 获取经销商等级，从易迅导入
	 * 
	 * 此字段的版本 >= 0
	 * @return retailerLevel value 类型为:short
	 * 
	 */
	public short getRetailerLevel()
	{
		return retailerLevel;
	}


	/**
	 * 设置经销商等级，从易迅导入
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRetailerLevel(short value)
	{
		this.retailerLevel = value;
		this.retailerLevel_u = 1;
	}

	public boolean issetRetailerLevel()
	{
		return this.retailerLevel_u != 0;
	}
	/**
	 * 获取易迅会员等级，从易迅导入
	 * 
	 * 此字段的版本 >= 0
	 * @return icsonMemberLevel value 类型为:short
	 * 
	 */
	public short getIcsonMemberLevel()
	{
		return icsonMemberLevel;
	}


	/**
	 * 设置易迅会员等级，从易迅导入
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIcsonMemberLevel(short value)
	{
		this.icsonMemberLevel = value;
		this.icsonMemberLevel_u = 1;
	}

	public boolean issetIcsonMemberLevel()
	{
		return this.icsonMemberLevel_u != 0;
	}
	/**
	 * 获取最后修改时间
	 * 
	 * 此字段的版本 >= 0
	 * @return lastUpdateTime value 类型为:long
	 * 
	 */
	public long getLastUpdateTime()
	{
		return lastUpdateTime;
	}


	/**
	 * 设置最后修改时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastUpdateTime(long value)
	{
		this.lastUpdateTime = value;
		this.lastUpdateTime_u = 1;
	}

	public boolean issetLastUpdateTime()
	{
		return this.lastUpdateTime_u != 0;
	}
	/**
	 * 获取用户注册时间
	 * 
	 * 此字段的版本 >= 0
	 * @return regTime value 类型为:long
	 * 
	 */
	public long getRegTime()
	{
		return regTime;
	}


	/**
	 * 设置用户注册时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRegTime(long value)
	{
		this.regTime = value;
		this.regTime_u = 1;
	}

	public boolean issetRegTime()
	{
		return this.regTime_u != 0;
	}
	/**
	 * 获取reserve string
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveStr value 类型为:String
	 * 
	 */
	public String getReserveStr()
	{
		return reserveStr;
	}


	/**
	 * 设置reserve string
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveStr(String value)
	{
		this.reserveStr = value;
		this.reserveStr_u = 1;
	}

	public boolean issetReserveStr()
	{
		return this.reserveStr_u != 0;
	}
	/**
	 * 获取reserve int
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveInt value 类型为:long
	 * 
	 */
	public long getReserveInt()
	{
		return reserveInt;
	}


	/**
	 * 设置reserve int
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setReserveInt(long value)
	{
		this.reserveInt = value;
		this.reserveInt_u = 1;
	}

	public boolean issetReserveInt()
	{
		return this.reserveInt_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return wgUid_u value 类型为:short
	 * 
	 */
	public short getWgUid_u()
	{
		return wgUid_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWgUid_u(short value)
	{
		this.wgUid_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return icsonUid_u value 类型为:short
	 * 
	 */
	public short getIcsonUid_u()
	{
		return icsonUid_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIcsonUid_u(short value)
	{
		this.icsonUid_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return qQNumber_u value 类型为:short
	 * 
	 */
	public short getQQNumber_u()
	{
		return qQNumber_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setQQNumber_u(short value)
	{
		this.qQNumber_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return loginAccount_u value 类型为:short
	 * 
	 */
	public short getLoginAccount_u()
	{
		return loginAccount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLoginAccount_u(short value)
	{
		this.loginAccount_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return accountType_u value 类型为:short
	 * 
	 */
	public short getAccountType_u()
	{
		return accountType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAccountType_u(short value)
	{
		this.accountType_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return truename_u value 类型为:short
	 * 
	 */
	public short getTruename_u()
	{
		return truename_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTruename_u(short value)
	{
		this.truename_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return nickname_u value 类型为:short
	 * 
	 */
	public short getNickname_u()
	{
		return nickname_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNickname_u(short value)
	{
		this.nickname_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return bitProperty_u value 类型为:short
	 * 
	 */
	public short getBitProperty_u()
	{
		return bitProperty_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBitProperty_u(short value)
	{
		this.bitProperty_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return userFlagLevel_u value 类型为:short
	 * 
	 */
	public short getUserFlagLevel_u()
	{
		return userFlagLevel_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUserFlagLevel_u(short value)
	{
		this.userFlagLevel_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sex_u value 类型为:short
	 * 
	 */
	public short getSex_u()
	{
		return sex_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSex_u(short value)
	{
		this.sex_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return age_u value 类型为:short
	 * 
	 */
	public short getAge_u()
	{
		return age_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAge_u(short value)
	{
		this.age_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return mobile_u value 类型为:short
	 * 
	 */
	public short getMobile_u()
	{
		return mobile_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMobile_u(short value)
	{
		this.mobile_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return email_u value 类型为:short
	 * 
	 */
	public short getEmail_u()
	{
		return email_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEmail_u(short value)
	{
		this.email_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return phone_u value 类型为:short
	 * 
	 */
	public short getPhone_u()
	{
		return phone_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPhone_u(short value)
	{
		this.phone_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return fax_u value 类型为:short
	 * 
	 */
	public short getFax_u()
	{
		return fax_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFax_u(short value)
	{
		this.fax_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return region_u value 类型为:short
	 * 
	 */
	public short getRegion_u()
	{
		return region_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRegion_u(short value)
	{
		this.region_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return postcode_u value 类型为:short
	 * 
	 */
	public short getPostcode_u()
	{
		return postcode_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPostcode_u(short value)
	{
		this.postcode_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return address_u value 类型为:short
	 * 
	 */
	public short getAddress_u()
	{
		return address_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress_u(short value)
	{
		this.address_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return identityType_u value 类型为:short
	 * 
	 */
	public short getIdentityType_u()
	{
		return identityType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIdentityType_u(short value)
	{
		this.identityType_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return identityNum_u value 类型为:short
	 * 
	 */
	public short getIdentityNum_u()
	{
		return identityNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIdentityNum_u(short value)
	{
		this.identityNum_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerCredit_u value 类型为:short
	 * 
	 */
	public short getBuyerCredit_u()
	{
		return buyerCredit_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerCredit_u(short value)
	{
		this.buyerCredit_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return experience_u value 类型为:short
	 * 
	 */
	public short getExperience_u()
	{
		return experience_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setExperience_u(short value)
	{
		this.experience_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cftAccount_u value 类型为:short
	 * 
	 */
	public short getCftAccount_u()
	{
		return cftAccount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCftAccount_u(short value)
	{
		this.cftAccount_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return loginLevel_u value 类型为:short
	 * 
	 */
	public short getLoginLevel_u()
	{
		return loginLevel_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLoginLevel_u(short value)
	{
		this.loginLevel_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return userType_u value 类型为:short
	 * 
	 */
	public short getUserType_u()
	{
		return userType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUserType_u(short value)
	{
		this.userType_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return retailerLevel_u value 类型为:short
	 * 
	 */
	public short getRetailerLevel_u()
	{
		return retailerLevel_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRetailerLevel_u(short value)
	{
		this.retailerLevel_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return icsonMemberLevel_u value 类型为:short
	 * 
	 */
	public short getIcsonMemberLevel_u()
	{
		return icsonMemberLevel_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIcsonMemberLevel_u(short value)
	{
		this.icsonMemberLevel_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return lastUpdateTime_u value 类型为:short
	 * 
	 */
	public short getLastUpdateTime_u()
	{
		return lastUpdateTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastUpdateTime_u(short value)
	{
		this.lastUpdateTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return regTime_u value 类型为:short
	 * 
	 */
	public short getRegTime_u()
	{
		return regTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRegTime_u(short value)
	{
		this.regTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveStr_u value 类型为:short
	 * 
	 */
	public short getReserveStr_u()
	{
		return reserveStr_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserveStr_u(short value)
	{
		this.reserveStr_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveInt_u value 类型为:short
	 * 
	 */
	public short getReserveInt_u()
	{
		return reserveInt_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserveInt_u(short value)
	{
		this.reserveInt_u = value;
	}


	/**
	 * 获取易迅openid
	 * 
	 * 此字段的版本 >= 20130314
	 * @return weChatId value 类型为:String
	 * 
	 */
	public String getWeChatId()
	{
		return weChatId;
	}


	/**
	 * 设置易迅openid
	 * 
	 * 此字段的版本 >= 20130314
	 * @param  value 类型为:String
	 * 
	 */
	public void setWeChatId(String value)
	{
		this.weChatId = value;
		this.weChatId_u = 1;
	}

	public boolean issetWeChatId()
	{
		return this.weChatId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20130314
	 * @return weChatId_u value 类型为:short
	 * 
	 */
	public short getWeChatId_u()
	{
		return weChatId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20130314
	 * @param  value 类型为:short
	 * 
	 */
	public void setWeChatId_u(short value)
	{
		this.weChatId_u = value;
	}


	/**
	 * 获取虚拟经验值
	 * 
	 * 此字段的版本 >= 20130429
	 * @return virtualExpPoints value 类型为:long
	 * 
	 */
	public long getVirtualExpPoints()
	{
		return virtualExpPoints;
	}


	/**
	 * 设置虚拟经验值
	 * 
	 * 此字段的版本 >= 20130429
	 * @param  value 类型为:long
	 * 
	 */
	public void setVirtualExpPoints(long value)
	{
		this.virtualExpPoints = value;
		this.virtualExpPoints_u = 1;
	}

	public boolean issetVirtualExpPoints()
	{
		return this.virtualExpPoints_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20130429
	 * @return virtualExpPoints_u value 类型为:short
	 * 
	 */
	public short getVirtualExpPoints_u()
	{
		return virtualExpPoints_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20130429
	 * @param  value 类型为:short
	 * 
	 */
	public void setVirtualExpPoints_u(short value)
	{
		this.virtualExpPoints_u = value;
	}


	/**
	 * 获取登录密码安全级别
	 * 
	 * 此字段的版本 >= 20130529
	 * @return passwordSecureLevel value 类型为:short
	 * 
	 */
	public short getPasswordSecureLevel()
	{
		return passwordSecureLevel;
	}


	/**
	 * 设置登录密码安全级别
	 * 
	 * 此字段的版本 >= 20130529
	 * @param  value 类型为:short
	 * 
	 */
	public void setPasswordSecureLevel(short value)
	{
		this.passwordSecureLevel = value;
		this.passwordSecureLevel_u = 1;
	}

	public boolean issetPasswordSecureLevel()
	{
		return this.passwordSecureLevel_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20130529
	 * @return passwordSecureLevel_u value 类型为:short
	 * 
	 */
	public short getPasswordSecureLevel_u()
	{
		return passwordSecureLevel_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20130529
	 * @param  value 类型为:short
	 * 
	 */
	public void setPasswordSecureLevel_u(short value)
	{
		this.passwordSecureLevel_u = value;
	}


	/**
	 * 获取不同来源用户注册时间(微购等), key为来源，value为注册时间。易迅用户注册时间请参考另一个字段regTime
	 * 
	 * 此字段的版本 >= 20130918
	 * @return mapDiffSrcRegTime value 类型为:Map<uint32_t,uint32_t>
	 * 
	 */
	public Map<uint32_t,uint32_t> getMapDiffSrcRegTime()
	{
		return mapDiffSrcRegTime;
	}


	/**
	 * 设置不同来源用户注册时间(微购等), key为来源，value为注册时间。易迅用户注册时间请参考另一个字段regTime
	 * 
	 * 此字段的版本 >= 20130918
	 * @param  value 类型为:Map<uint32_t,uint32_t>
	 * 
	 */
	public void setMapDiffSrcRegTime(Map<uint32_t,uint32_t> value)
	{
		if (value != null) {
				this.mapDiffSrcRegTime = value;
				this.mapDiffSrcRegTime_u = 1;
		}
	}

	public boolean issetMapDiffSrcRegTime()
	{
		return this.mapDiffSrcRegTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20130918
	 * @return mapDiffSrcRegTime_u value 类型为:short
	 * 
	 */
	public short getMapDiffSrcRegTime_u()
	{
		return mapDiffSrcRegTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20130918
	 * @param  value 类型为:short
	 * 
	 */
	public void setMapDiffSrcRegTime_u(short value)
	{
		this.mapDiffSrcRegTime_u = value;
	}


	/**
	 * 获取网购openid
	 * 
	 * 此字段的版本 >= 20131220
	 * @return wanggouOpenid value 类型为:String
	 * 
	 */
	public String getWanggouOpenid()
	{
		return wanggouOpenid;
	}


	/**
	 * 设置网购openid
	 * 
	 * 此字段的版本 >= 20131220
	 * @param  value 类型为:String
	 * 
	 */
	public void setWanggouOpenid(String value)
	{
		this.wanggouOpenid = value;
		this.wanggouOpenid_u = 1;
	}

	public boolean issetWanggouOpenid()
	{
		return this.wanggouOpenid_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20131220
	 * @return wanggouOpenid_u value 类型为:short
	 * 
	 */
	public short getWanggouOpenid_u()
	{
		return wanggouOpenid_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20131220
	 * @param  value 类型为:short
	 * 
	 */
	public void setWanggouOpenid_u(short value)
	{
		this.wanggouOpenid_u = value;
	}


	/**
	 * 获取卖家总信用
	 * 
	 * 此字段的版本 >= 20141229
	 * @return sellerCredit value 类型为:int
	 * 
	 */
	public int getSellerCredit()
	{
		return sellerCredit;
	}


	/**
	 * 设置卖家总信用
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:int
	 * 
	 */
	public void setSellerCredit(int value)
	{
		this.sellerCredit = value;
		this.sellerCredit_u = 1;
	}

	public boolean issetSellerCredit()
	{
		return this.sellerCredit_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141229
	 * @return sellerCredit_u value 类型为:short
	 * 
	 */
	public short getSellerCredit_u()
	{
		return sellerCredit_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerCredit_u(short value)
	{
		this.sellerCredit_u = value;
	}


	/**
	 * 获取认证信息
	 * 
	 * 此字段的版本 >= 20141229
	 * @return authMask value 类型为:short
	 * 
	 */
	public short getAuthMask()
	{
		return authMask;
	}


	/**
	 * 设置认证信息
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setAuthMask(short value)
	{
		this.authMask = value;
		this.authMask_u = 1;
	}

	public boolean issetAuthMask()
	{
		return this.authMask_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141229
	 * @return authMask_u value 类型为:short
	 * 
	 */
	public short getAuthMask_u()
	{
		return authMask_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setAuthMask_u(short value)
	{
		this.authMask_u = value;
	}


	/**
	 * 获取用户属性
	 * 
	 * 此字段的版本 >= 20141229
	 * @return mapProp value 类型为:Map<uint16_t,uint8_t>
	 * 
	 */
	public Map<uint16_t,uint8_t> getMapProp()
	{
		return mapProp;
	}


	/**
	 * 设置用户属性
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:Map<uint16_t,uint8_t>
	 * 
	 */
	public void setMapProp(Map<uint16_t,uint8_t> value)
	{
		if (value != null) {
				this.mapProp = value;
				this.mapProp_u = 1;
		}
	}

	public boolean issetMapProp()
	{
		return this.mapProp_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141229
	 * @return mapProp_u value 类型为:short
	 * 
	 */
	public short getMapProp_u()
	{
		return mapProp_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setMapProp_u(short value)
	{
		this.mapProp_u = value;
	}


	/**
	 * 获取留言标志位
	 * 
	 * 此字段的版本 >= 20141229
	 * @return recvMsgMask value 类型为:int
	 * 
	 */
	public int getRecvMsgMask()
	{
		return recvMsgMask;
	}


	/**
	 * 设置留言标志位
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:int
	 * 
	 */
	public void setRecvMsgMask(int value)
	{
		this.recvMsgMask = value;
		this.recvMsgMask_u = 1;
	}

	public boolean issetRecvMsgMask()
	{
		return this.recvMsgMask_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141229
	 * @return recvMsgMask_u value 类型为:short
	 * 
	 */
	public short getRecvMsgMask_u()
	{
		return recvMsgMask_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setRecvMsgMask_u(short value)
	{
		this.recvMsgMask_u = value;
	}


	/**
	 * 获取用户信息标志位
	 * 
	 * 此字段的版本 >= 20141229
	 * @return userInfoMask value 类型为:long
	 * 
	 */
	public long getUserInfoMask()
	{
		return userInfoMask;
	}


	/**
	 * 设置用户信息标志位
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:long
	 * 
	 */
	public void setUserInfoMask(long value)
	{
		this.userInfoMask = value;
		this.userInfoMask_u = 1;
	}

	public boolean issetUserInfoMask()
	{
		return this.userInfoMask_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141229
	 * @return userInfoMask_u value 类型为:short
	 * 
	 */
	public short getUserInfoMask_u()
	{
		return userInfoMask_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setUserInfoMask_u(short value)
	{
		this.userInfoMask_u = value;
	}


	/**
	 * 获取省份
	 * 
	 * 此字段的版本 >= 20141229
	 * @return province value 类型为:short
	 * 
	 */
	public short getProvince()
	{
		return province;
	}


	/**
	 * 设置省份
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setProvince(short value)
	{
		this.province = value;
		this.province_u = 1;
	}

	public boolean issetProvince()
	{
		return this.province_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141229
	 * @return province_u value 类型为:short
	 * 
	 */
	public short getProvince_u()
	{
		return province_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setProvince_u(short value)
	{
		this.province_u = value;
	}


	/**
	 * 获取城市
	 * 
	 * 此字段的版本 >= 20141229
	 * @return city value 类型为:int
	 * 
	 */
	public int getCity()
	{
		return city;
	}


	/**
	 * 设置城市
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:int
	 * 
	 */
	public void setCity(int value)
	{
		this.city = value;
		this.city_u = 1;
	}

	public boolean issetCity()
	{
		return this.city_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141229
	 * @return city_u value 类型为:short
	 * 
	 */
	public short getCity_u()
	{
		return city_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setCity_u(short value)
	{
		this.city_u = value;
	}


	/**
	 * 获取城市
	 * 
	 * 此字段的版本 >= 20141229
	 * @return country value 类型为:long
	 * 
	 */
	public long getCountry()
	{
		return country;
	}


	/**
	 * 设置城市
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:long
	 * 
	 */
	public void setCountry(long value)
	{
		this.country = value;
		this.country_u = 1;
	}

	public boolean issetCountry()
	{
		return this.country_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141229
	 * @return country_u value 类型为:short
	 * 
	 */
	public short getCountry_u()
	{
		return country_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setCountry_u(short value)
	{
		this.country_u = value;
	}


	/**
	 * 获取肖像图片
	 * 
	 * 此字段的版本 >= 20141229
	 * @return mainLogoPos value 类型为:String
	 * 
	 */
	public String getMainLogoPos()
	{
		return mainLogoPos;
	}


	/**
	 * 设置肖像图片
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:String
	 * 
	 */
	public void setMainLogoPos(String value)
	{
		this.mainLogoPos = value;
		this.mainLogoPos_u = 1;
	}

	public boolean issetMainLogoPos()
	{
		return this.mainLogoPos_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141229
	 * @return mainLogoPos_u value 类型为:short
	 * 
	 */
	public short getMainLogoPos_u()
	{
		return mainLogoPos_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setMainLogoPos_u(short value)
	{
		this.mainLogoPos_u = value;
	}


	/**
	 * 获取联系方式
	 * 
	 * 此字段的版本 >= 20141229
	 * @return commContactType value 类型为:short
	 * 
	 */
	public short getCommContactType()
	{
		return commContactType;
	}


	/**
	 * 设置联系方式
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setCommContactType(short value)
	{
		this.commContactType = value;
		this.commContactType_u = 1;
	}

	public boolean issetCommContactType()
	{
		return this.commContactType_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141229
	 * @return commContactType_u value 类型为:short
	 * 
	 */
	public short getCommContactType_u()
	{
		return commContactType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setCommContactType_u(short value)
	{
		this.commContactType_u = value;
	}


	/**
	 * 获取最后登录IP
	 * 
	 * 此字段的版本 >= 20141229
	 * @return lastLoginIp value 类型为:long
	 * 
	 */
	public long getLastLoginIp()
	{
		return lastLoginIp;
	}


	/**
	 * 设置最后登录IP
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastLoginIp(long value)
	{
		this.lastLoginIp = value;
		this.lastLoginIp_u = 1;
	}

	public boolean issetLastLoginIp()
	{
		return this.lastLoginIp_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141229
	 * @return lastLoginIp_u value 类型为:short
	 * 
	 */
	public short getLastLoginIp_u()
	{
		return lastLoginIp_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastLoginIp_u(short value)
	{
		this.lastLoginIp_u = value;
	}


	/**
	 * 获取最后登录时间
	 * 
	 * 此字段的版本 >= 20141229
	 * @return lastLoginTime value 类型为:long
	 * 
	 */
	public long getLastLoginTime()
	{
		return lastLoginTime;
	}


	/**
	 * 设置最后登录时间
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastLoginTime(long value)
	{
		this.lastLoginTime = value;
		this.lastLoginTime_u = 1;
	}

	public boolean issetLastLoginTime()
	{
		return this.lastLoginTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141229
	 * @return lastLoginTime_u value 类型为:short
	 * 
	 */
	public short getLastLoginTime_u()
	{
		return lastLoginTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastLoginTime_u(short value)
	{
		this.lastLoginTime_u = value;
	}


	/**
	 * 获取登录类型
	 * 
	 * 此字段的版本 >= 20141229
	 * @return lastLoginType value 类型为:short
	 * 
	 */
	public short getLastLoginType()
	{
		return lastLoginType;
	}


	/**
	 * 设置登录类型
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastLoginType(short value)
	{
		this.lastLoginType = value;
		this.lastLoginType_u = 1;
	}

	public boolean issetLastLoginType()
	{
		return this.lastLoginType_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141229
	 * @return lastLoginType_u value 类型为:short
	 * 
	 */
	public short getLastLoginType_u()
	{
		return lastLoginType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastLoginType_u(short value)
	{
		this.lastLoginType_u = value;
	}


	/**
	 * 获取参考信用
	 * 
	 * 此字段的版本 >= 20141229
	 * @return referCredit value 类型为:String
	 * 
	 */
	public String getReferCredit()
	{
		return referCredit;
	}


	/**
	 * 设置参考信用
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:String
	 * 
	 */
	public void setReferCredit(String value)
	{
		this.referCredit = value;
		this.referCredit_u = 1;
	}

	public boolean issetReferCredit()
	{
		return this.referCredit_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141229
	 * @return referCredit_u value 类型为:short
	 * 
	 */
	public short getReferCredit_u()
	{
		return referCredit_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setReferCredit_u(short value)
	{
		this.referCredit_u = value;
	}


	/**
	 * 获取卖家虚拟信用
	 * 
	 * 此字段的版本 >= 20141229
	 * @return virtualCredit value 类型为:int
	 * 
	 */
	public int getVirtualCredit()
	{
		return virtualCredit;
	}


	/**
	 * 设置卖家虚拟信用
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:int
	 * 
	 */
	public void setVirtualCredit(int value)
	{
		this.virtualCredit = value;
		this.virtualCredit_u = 1;
	}

	public boolean issetVirtualCredit()
	{
		return this.virtualCredit_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141229
	 * @return virtualCredit_u value 类型为:short
	 * 
	 */
	public short getVirtualCredit_u()
	{
		return virtualCredit_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setVirtualCredit_u(short value)
	{
		this.virtualCredit_u = value;
	}


	/**
	 * 获取卖家实物信用
	 * 
	 * 此字段的版本 >= 20141229
	 * @return objectCredit value 类型为:int
	 * 
	 */
	public int getObjectCredit()
	{
		return objectCredit;
	}


	/**
	 * 设置卖家实物信用
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:int
	 * 
	 */
	public void setObjectCredit(int value)
	{
		this.objectCredit = value;
		this.objectCredit_u = 1;
	}

	public boolean issetObjectCredit()
	{
		return this.objectCredit_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141229
	 * @return objectCredit_u value 类型为:short
	 * 
	 */
	public short getObjectCredit_u()
	{
		return objectCredit_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setObjectCredit_u(short value)
	{
		this.objectCredit_u = value;
	}


	/**
	 * 获取卖家联系方式
	 * 
	 * 此字段的版本 >= 20141229
	 * @return contacts value 类型为:String
	 * 
	 */
	public String getContacts()
	{
		return contacts;
	}


	/**
	 * 设置卖家联系方式
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:String
	 * 
	 */
	public void setContacts(String value)
	{
		this.contacts = value;
		this.contacts_u = 1;
	}

	public boolean issetContacts()
	{
		return this.contacts_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141229
	 * @return contacts_u value 类型为:short
	 * 
	 */
	public short getContacts_u()
	{
		return contacts_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setContacts_u(short value)
	{
		this.contacts_u = value;
	}


	/**
	 * 获取多工号权限系统员工所属的卖家号
	 * 
	 * 此字段的版本 >= 20141229
	 * @return sellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return sellerUin;
	}


	/**
	 * 设置多工号权限系统员工所属的卖家号
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.sellerUin = value;
		this.sellerUin_u = 1;
	}

	public boolean issetSellerUin()
	{
		return this.sellerUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141229
	 * @return sellerUin_u value 类型为:short
	 * 
	 */
	public short getSellerUin_u()
	{
		return sellerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerUin_u(short value)
	{
		this.sellerUin_u = value;
	}


	/**
	 * 获取店铺类目id[用作店铺类型]
	 * 
	 * 此字段的版本 >= 20141229
	 * @return shopClassId value 类型为:long
	 * 
	 */
	public long getShopClassId()
	{
		return shopClassId;
	}


	/**
	 * 设置店铺类目id[用作店铺类型]
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:long
	 * 
	 */
	public void setShopClassId(long value)
	{
		this.shopClassId = value;
		this.shopClassId_u = 1;
	}

	public boolean issetShopClassId()
	{
		return this.shopClassId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20141229
	 * @return shopClassId_u value 类型为:short
	 * 
	 */
	public short getShopClassId_u()
	{
		return shopClassId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20141229
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopClassId_u(short value)
	{
		this.shopClassId_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(BuyerInfoPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 17;  //计算字段wgUid的长度 size_of(uint64_t)
				length += 17;  //计算字段icsonUid的长度 size_of(uint64_t)
				length += 17;  //计算字段qQNumber的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(loginAccount, null);  //计算字段loginAccount的长度 size_of(String)
				length += 1;  //计算字段accountType的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(truename, null);  //计算字段truename的长度 size_of(String)
				length += ByteStream.getObjectSize(nickname, null);  //计算字段nickname的长度 size_of(String)
				length += ByteStream.getObjectSize(bitProperty, null);  //计算字段bitProperty的长度 size_of(BitSet)
				length += ByteStream.getObjectSize(userFlagLevel, null);  //计算字段userFlagLevel的长度 size_of(Map)
				length += 1;  //计算字段sex的长度 size_of(uint8_t)
				length += 1;  //计算字段age的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(mobile, null);  //计算字段mobile的长度 size_of(String)
				length += ByteStream.getObjectSize(email, null);  //计算字段email的长度 size_of(String)
				length += ByteStream.getObjectSize(phone, null);  //计算字段phone的长度 size_of(String)
				length += ByteStream.getObjectSize(fax, null);  //计算字段fax的长度 size_of(String)
				length += 4;  //计算字段region的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(postcode, null);  //计算字段postcode的长度 size_of(String)
				length += ByteStream.getObjectSize(address, null);  //计算字段address的长度 size_of(String)
				length += 1;  //计算字段identityType的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(identityNum, null);  //计算字段identityNum的长度 size_of(String)
				length += 4;  //计算字段buyerCredit的长度 size_of(int)
				length += 4;  //计算字段experience的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(cftAccount, null);  //计算字段cftAccount的长度 size_of(String)
				length += 1;  //计算字段loginLevel的长度 size_of(uint8_t)
				length += 1;  //计算字段userType的长度 size_of(uint8_t)
				length += 1;  //计算字段retailerLevel的长度 size_of(uint8_t)
				length += 1;  //计算字段icsonMemberLevel的长度 size_of(uint8_t)
				length += 4;  //计算字段lastUpdateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段regTime的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(reserveStr, null);  //计算字段reserveStr的长度 size_of(String)
				length += 4;  //计算字段reserveInt的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段wgUid_u的长度 size_of(uint8_t)
				length += 1;  //计算字段icsonUid_u的长度 size_of(uint8_t)
				length += 1;  //计算字段qQNumber_u的长度 size_of(uint8_t)
				length += 1;  //计算字段loginAccount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段accountType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段truename_u的长度 size_of(uint8_t)
				length += 1;  //计算字段nickname_u的长度 size_of(uint8_t)
				length += 1;  //计算字段bitProperty_u的长度 size_of(uint8_t)
				length += 1;  //计算字段userFlagLevel_u的长度 size_of(uint8_t)
				length += 1;  //计算字段sex_u的长度 size_of(uint8_t)
				length += 1;  //计算字段age_u的长度 size_of(uint8_t)
				length += 1;  //计算字段mobile_u的长度 size_of(uint8_t)
				length += 1;  //计算字段email_u的长度 size_of(uint8_t)
				length += 1;  //计算字段phone_u的长度 size_of(uint8_t)
				length += 1;  //计算字段fax_u的长度 size_of(uint8_t)
				length += 1;  //计算字段region_u的长度 size_of(uint8_t)
				length += 1;  //计算字段postcode_u的长度 size_of(uint8_t)
				length += 1;  //计算字段address_u的长度 size_of(uint8_t)
				length += 1;  //计算字段identityType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段identityNum_u的长度 size_of(uint8_t)
				length += 1;  //计算字段buyerCredit_u的长度 size_of(uint8_t)
				length += 1;  //计算字段experience_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cftAccount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段loginLevel_u的长度 size_of(uint8_t)
				length += 1;  //计算字段userType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段retailerLevel_u的长度 size_of(uint8_t)
				length += 1;  //计算字段icsonMemberLevel_u的长度 size_of(uint8_t)
				length += 1;  //计算字段lastUpdateTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段regTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserveStr_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserveInt_u的长度 size_of(uint8_t)
				if(  this.version >= 20130314 ){
						length += ByteStream.getObjectSize(weChatId, null);  //计算字段weChatId的长度 size_of(String)
				}
				if(  this.version >= 20130314 ){
						length += 1;  //计算字段weChatId_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20130429 ){
						length += 4;  //计算字段virtualExpPoints的长度 size_of(uint32_t)
				}
				if(  this.version >= 20130429 ){
						length += 1;  //计算字段virtualExpPoints_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20130529 ){
						length += 1;  //计算字段passwordSecureLevel的长度 size_of(uint8_t)
				}
				if(  this.version >= 20130529 ){
						length += 1;  //计算字段passwordSecureLevel_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20130918 ){
						length += ByteStream.getObjectSize(mapDiffSrcRegTime, null);  //计算字段mapDiffSrcRegTime的长度 size_of(Map)
				}
				if(  this.version >= 20130918 ){
						length += 1;  //计算字段mapDiffSrcRegTime_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20131220 ){
						length += ByteStream.getObjectSize(wanggouOpenid, null);  //计算字段wanggouOpenid的长度 size_of(String)
				}
				if(  this.version >= 20131220 ){
						length += 1;  //计算字段wanggouOpenid_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 4;  //计算字段sellerCredit的长度 size_of(int)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段sellerCredit_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段authMask的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段authMask_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += ByteStream.getObjectSize(mapProp, null);  //计算字段mapProp的长度 size_of(Map)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段mapProp_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 2;  //计算字段recvMsgMask的长度 size_of(uint16_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段recvMsgMask_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 4;  //计算字段userInfoMask的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段userInfoMask_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段province的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段province_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 2;  //计算字段city的长度 size_of(uint16_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段city_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 4;  //计算字段country的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段country_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += ByteStream.getObjectSize(mainLogoPos, null);  //计算字段mainLogoPos的长度 size_of(String)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段mainLogoPos_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段commContactType的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段commContactType_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 4;  //计算字段lastLoginIp的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段lastLoginIp_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 4;  //计算字段lastLoginTime的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段lastLoginTime_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段lastLoginType的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段lastLoginType_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += ByteStream.getObjectSize(referCredit, null);  //计算字段referCredit的长度 size_of(String)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段referCredit_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 4;  //计算字段virtualCredit的长度 size_of(int)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段virtualCredit_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 4;  //计算字段objectCredit的长度 size_of(int)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段objectCredit_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += ByteStream.getObjectSize(contacts, null);  //计算字段contacts的长度 size_of(String)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段contacts_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 4;  //计算字段sellerUin的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段sellerUin_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 4;  //计算字段shopClassId的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段shopClassId_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(BuyerInfoPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 17;  //计算字段wgUid的长度 size_of(uint64_t)
				length += 17;  //计算字段icsonUid的长度 size_of(uint64_t)
				length += 17;  //计算字段qQNumber的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(loginAccount, encoding);  //计算字段loginAccount的长度 size_of(String)
				length += 1;  //计算字段accountType的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(truename, encoding);  //计算字段truename的长度 size_of(String)
				length += ByteStream.getObjectSize(nickname, encoding);  //计算字段nickname的长度 size_of(String)
				length += ByteStream.getObjectSize(bitProperty, encoding);  //计算字段bitProperty的长度 size_of(BitSet)
				length += ByteStream.getObjectSize(userFlagLevel, encoding);  //计算字段userFlagLevel的长度 size_of(Map)
				length += 1;  //计算字段sex的长度 size_of(uint8_t)
				length += 1;  //计算字段age的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(mobile, encoding);  //计算字段mobile的长度 size_of(String)
				length += ByteStream.getObjectSize(email, encoding);  //计算字段email的长度 size_of(String)
				length += ByteStream.getObjectSize(phone, encoding);  //计算字段phone的长度 size_of(String)
				length += ByteStream.getObjectSize(fax, encoding);  //计算字段fax的长度 size_of(String)
				length += 4;  //计算字段region的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(postcode, encoding);  //计算字段postcode的长度 size_of(String)
				length += ByteStream.getObjectSize(address, encoding);  //计算字段address的长度 size_of(String)
				length += 1;  //计算字段identityType的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(identityNum, encoding);  //计算字段identityNum的长度 size_of(String)
				length += 4;  //计算字段buyerCredit的长度 size_of(int)
				length += 4;  //计算字段experience的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(cftAccount, encoding);  //计算字段cftAccount的长度 size_of(String)
				length += 1;  //计算字段loginLevel的长度 size_of(uint8_t)
				length += 1;  //计算字段userType的长度 size_of(uint8_t)
				length += 1;  //计算字段retailerLevel的长度 size_of(uint8_t)
				length += 1;  //计算字段icsonMemberLevel的长度 size_of(uint8_t)
				length += 4;  //计算字段lastUpdateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段regTime的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(reserveStr, encoding);  //计算字段reserveStr的长度 size_of(String)
				length += 4;  //计算字段reserveInt的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段wgUid_u的长度 size_of(uint8_t)
				length += 1;  //计算字段icsonUid_u的长度 size_of(uint8_t)
				length += 1;  //计算字段qQNumber_u的长度 size_of(uint8_t)
				length += 1;  //计算字段loginAccount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段accountType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段truename_u的长度 size_of(uint8_t)
				length += 1;  //计算字段nickname_u的长度 size_of(uint8_t)
				length += 1;  //计算字段bitProperty_u的长度 size_of(uint8_t)
				length += 1;  //计算字段userFlagLevel_u的长度 size_of(uint8_t)
				length += 1;  //计算字段sex_u的长度 size_of(uint8_t)
				length += 1;  //计算字段age_u的长度 size_of(uint8_t)
				length += 1;  //计算字段mobile_u的长度 size_of(uint8_t)
				length += 1;  //计算字段email_u的长度 size_of(uint8_t)
				length += 1;  //计算字段phone_u的长度 size_of(uint8_t)
				length += 1;  //计算字段fax_u的长度 size_of(uint8_t)
				length += 1;  //计算字段region_u的长度 size_of(uint8_t)
				length += 1;  //计算字段postcode_u的长度 size_of(uint8_t)
				length += 1;  //计算字段address_u的长度 size_of(uint8_t)
				length += 1;  //计算字段identityType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段identityNum_u的长度 size_of(uint8_t)
				length += 1;  //计算字段buyerCredit_u的长度 size_of(uint8_t)
				length += 1;  //计算字段experience_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cftAccount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段loginLevel_u的长度 size_of(uint8_t)
				length += 1;  //计算字段userType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段retailerLevel_u的长度 size_of(uint8_t)
				length += 1;  //计算字段icsonMemberLevel_u的长度 size_of(uint8_t)
				length += 1;  //计算字段lastUpdateTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段regTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserveStr_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserveInt_u的长度 size_of(uint8_t)
				if(  this.version >= 20130314 ){
						length += ByteStream.getObjectSize(weChatId, encoding);  //计算字段weChatId的长度 size_of(String)
				}
				if(  this.version >= 20130314 ){
						length += 1;  //计算字段weChatId_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20130429 ){
						length += 4;  //计算字段virtualExpPoints的长度 size_of(uint32_t)
				}
				if(  this.version >= 20130429 ){
						length += 1;  //计算字段virtualExpPoints_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20130529 ){
						length += 1;  //计算字段passwordSecureLevel的长度 size_of(uint8_t)
				}
				if(  this.version >= 20130529 ){
						length += 1;  //计算字段passwordSecureLevel_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20130918 ){
						length += ByteStream.getObjectSize(mapDiffSrcRegTime, encoding);  //计算字段mapDiffSrcRegTime的长度 size_of(Map)
				}
				if(  this.version >= 20130918 ){
						length += 1;  //计算字段mapDiffSrcRegTime_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20131220 ){
						length += ByteStream.getObjectSize(wanggouOpenid, encoding);  //计算字段wanggouOpenid的长度 size_of(String)
				}
				if(  this.version >= 20131220 ){
						length += 1;  //计算字段wanggouOpenid_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 4;  //计算字段sellerCredit的长度 size_of(int)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段sellerCredit_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段authMask的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段authMask_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += ByteStream.getObjectSize(mapProp, encoding);  //计算字段mapProp的长度 size_of(Map)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段mapProp_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 2;  //计算字段recvMsgMask的长度 size_of(uint16_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段recvMsgMask_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 4;  //计算字段userInfoMask的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段userInfoMask_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段province的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段province_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 2;  //计算字段city的长度 size_of(uint16_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段city_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 4;  //计算字段country的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段country_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += ByteStream.getObjectSize(mainLogoPos, encoding);  //计算字段mainLogoPos的长度 size_of(String)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段mainLogoPos_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段commContactType的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段commContactType_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 4;  //计算字段lastLoginIp的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段lastLoginIp_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 4;  //计算字段lastLoginTime的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段lastLoginTime_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段lastLoginType的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段lastLoginType_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += ByteStream.getObjectSize(referCredit, encoding);  //计算字段referCredit的长度 size_of(String)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段referCredit_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 4;  //计算字段virtualCredit的长度 size_of(int)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段virtualCredit_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 4;  //计算字段objectCredit的长度 size_of(int)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段objectCredit_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += ByteStream.getObjectSize(contacts, encoding);  //计算字段contacts的长度 size_of(String)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段contacts_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 4;  //计算字段sellerUin的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段sellerUin_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20141229 ){
						length += 4;  //计算字段shopClassId的长度 size_of(uint32_t)
				}
				if(  this.version >= 20141229 ){
						length += 1;  //计算字段shopClassId_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本20131220所包含的字段*******
 *	long version;///<版本号
 *	long wgUid;///<网购用户内部id，非QQ号，目前仅支持32位
 *	long icsonUid;///<易迅用户内部id，目前仅支持32位
 *	long qQNumber;///<绑定的用户QQ号，目前仅支持32位
 *	String loginAccount;///<用户帐号名，包括易迅注册帐号、非qq帐号的第三方联合登录，如LoginAlipay+openid等字符串帐户名
 *	short accountType;///<用户帐号类型，1-QQ号 2-个性化帐号，参见user_comm_define.h中的E_ICSON_USER_ACCOUNT_TYPE
 *	String truename;///<真实姓名
 *	String nickname;///<昵称
 *	BitSet bitProperty;///<用户属性位BitSet，属性位代表的意义请参见b2b2c_define.h中的USER_PROPERTY
 *	Map<uint32_t,uint32_t> userFlagLevel;///<用户属性级别，如彩钻级别，Map中第一个uint32_t表示用户属性位值，第二个uint32_t表示级别值
 *	short sex;///<性别, 0-不明 1-男 2-女 参见user_comm_define.h中的E_USER_SEX
 *	short age;///<年龄
 *	String mobile;///<手机号，如绑定手机的用户属性位为1，则表示绑定的手机号
 *	String email;///<电子邮箱，如绑定邮箱的用户属性位为1，则表示绑定的邮箱
 *	String phone;///<固定电话
 *	String fax;///<传真
 *	long region;///<用户所在地区id
 *	String postcode;///<邮政编码
 *	String address;///<用户详细住址
 *	short identityType;///<身份证件类型，1-身份证 参见user_comm_define.h中E_USER_IDTYPE
 *	String identityNum;///<身份证件号码
 *	int buyerCredit;///<用户信用值
 *	long experience;///<用户经验值
 *	String cftAccount;///<财付通账号
 *	short loginLevel;///<登录级别，预留
 *	short userType;///<用户类型，从易迅导入，参见user_comm_define.h中E_USER_TYPE
 *	short retailerLevel;///<经销商等级，从易迅导入
 *	short icsonMemberLevel;///<易迅会员等级，从易迅导入
 *	long lastUpdateTime;///<最后修改时间
 *	long regTime;///<用户注册时间
 *	String reserveStr;///<reserve string
 *	long reserveInt;///<reserve int
 *	short version_u;
 *	short wgUid_u;
 *	short icsonUid_u;
 *	short qQNumber_u;
 *	short loginAccount_u;
 *	short accountType_u;
 *	short truename_u;
 *	short nickname_u;
 *	short bitProperty_u;
 *	short userFlagLevel_u;
 *	short sex_u;
 *	short age_u;
 *	short mobile_u;
 *	short email_u;
 *	short phone_u;
 *	short fax_u;
 *	short region_u;
 *	short postcode_u;
 *	short address_u;
 *	short identityType_u;
 *	short identityNum_u;
 *	short buyerCredit_u;
 *	short experience_u;
 *	short cftAccount_u;
 *	short loginLevel_u;
 *	short userType_u;
 *	short retailerLevel_u;
 *	short icsonMemberLevel_u;
 *	short lastUpdateTime_u;
 *	short regTime_u;
 *	short reserveStr_u;
 *	short reserveInt_u;
 *	String weChatId;///<易迅openid
 *	short weChatId_u;
 *	long virtualExpPoints;///<虚拟经验值
 *	short virtualExpPoints_u;
 *	short passwordSecureLevel;///<登录密码安全级别
 *	short passwordSecureLevel_u;
 *	Map<uint32_t,uint32_t> mapDiffSrcRegTime;///<不同来源用户注册时间(微购等), key为来源，value为注册时间。易迅用户注册时间请参考另一个字段regTime
 *	short mapDiffSrcRegTime_u;
 *	String wanggouOpenid;///<网购openid
 *	short wanggouOpenid_u;
 *****以上是版本20131220所包含的字段*******
 *
 *****以下是版本20130429所包含的字段*******
 *	long version;///<版本号
 *	long wgUid;///<网购用户内部id，非QQ号，目前仅支持32位
 *	long icsonUid;///<易迅用户内部id，目前仅支持32位
 *	long qQNumber;///<绑定的用户QQ号，目前仅支持32位
 *	String loginAccount;///<用户帐号名，包括易迅注册帐号、非qq帐号的第三方联合登录，如LoginAlipay+openid等字符串帐户名
 *	short accountType;///<用户帐号类型，1-QQ号 2-个性化帐号，参见user_comm_define.h中的E_ICSON_USER_ACCOUNT_TYPE
 *	String truename;///<真实姓名
 *	String nickname;///<昵称
 *	BitSet bitProperty;///<用户属性位BitSet，属性位代表的意义请参见b2b2c_define.h中的USER_PROPERTY
 *	Map<uint32_t,uint32_t> userFlagLevel;///<用户属性级别，如彩钻级别，Map中第一个uint32_t表示用户属性位值，第二个uint32_t表示级别值
 *	short sex;///<性别, 0-不明 1-男 2-女 参见user_comm_define.h中的E_USER_SEX
 *	short age;///<年龄
 *	String mobile;///<手机号，如绑定手机的用户属性位为1，则表示绑定的手机号
 *	String email;///<电子邮箱，如绑定邮箱的用户属性位为1，则表示绑定的邮箱
 *	String phone;///<固定电话
 *	String fax;///<传真
 *	long region;///<用户所在地区id
 *	String postcode;///<邮政编码
 *	String address;///<用户详细住址
 *	short identityType;///<身份证件类型，1-身份证 参见user_comm_define.h中E_USER_IDTYPE
 *	String identityNum;///<身份证件号码
 *	int buyerCredit;///<用户信用值
 *	long experience;///<用户经验值
 *	String cftAccount;///<财付通账号
 *	short loginLevel;///<登录级别，预留
 *	short userType;///<用户类型，从易迅导入，参见user_comm_define.h中E_USER_TYPE
 *	short retailerLevel;///<经销商等级，从易迅导入
 *	short icsonMemberLevel;///<易迅会员等级，从易迅导入
 *	long lastUpdateTime;///<最后修改时间
 *	long regTime;///<用户注册时间
 *	String reserveStr;///<reserve string
 *	long reserveInt;///<reserve int
 *	short version_u;
 *	short wgUid_u;
 *	short icsonUid_u;
 *	short qQNumber_u;
 *	short loginAccount_u;
 *	short accountType_u;
 *	short truename_u;
 *	short nickname_u;
 *	short bitProperty_u;
 *	short userFlagLevel_u;
 *	short sex_u;
 *	short age_u;
 *	short mobile_u;
 *	short email_u;
 *	short phone_u;
 *	short fax_u;
 *	short region_u;
 *	short postcode_u;
 *	short address_u;
 *	short identityType_u;
 *	short identityNum_u;
 *	short buyerCredit_u;
 *	short experience_u;
 *	short cftAccount_u;
 *	short loginLevel_u;
 *	short userType_u;
 *	short retailerLevel_u;
 *	short icsonMemberLevel_u;
 *	short lastUpdateTime_u;
 *	short regTime_u;
 *	short reserveStr_u;
 *	short reserveInt_u;
 *	String weChatId;///<易迅openid
 *	short weChatId_u;
 *	long virtualExpPoints;///<虚拟经验值
 *	short virtualExpPoints_u;
 *****以上是版本20130429所包含的字段*******
 *
 *****以下是版本20130314所包含的字段*******
 *	long version;///<版本号
 *	long wgUid;///<网购用户内部id，非QQ号，目前仅支持32位
 *	long icsonUid;///<易迅用户内部id，目前仅支持32位
 *	long qQNumber;///<绑定的用户QQ号，目前仅支持32位
 *	String loginAccount;///<用户帐号名，包括易迅注册帐号、非qq帐号的第三方联合登录，如LoginAlipay+openid等字符串帐户名
 *	short accountType;///<用户帐号类型，1-QQ号 2-个性化帐号，参见user_comm_define.h中的E_ICSON_USER_ACCOUNT_TYPE
 *	String truename;///<真实姓名
 *	String nickname;///<昵称
 *	BitSet bitProperty;///<用户属性位BitSet，属性位代表的意义请参见b2b2c_define.h中的USER_PROPERTY
 *	Map<uint32_t,uint32_t> userFlagLevel;///<用户属性级别，如彩钻级别，Map中第一个uint32_t表示用户属性位值，第二个uint32_t表示级别值
 *	short sex;///<性别, 0-不明 1-男 2-女 参见user_comm_define.h中的E_USER_SEX
 *	short age;///<年龄
 *	String mobile;///<手机号，如绑定手机的用户属性位为1，则表示绑定的手机号
 *	String email;///<电子邮箱，如绑定邮箱的用户属性位为1，则表示绑定的邮箱
 *	String phone;///<固定电话
 *	String fax;///<传真
 *	long region;///<用户所在地区id
 *	String postcode;///<邮政编码
 *	String address;///<用户详细住址
 *	short identityType;///<身份证件类型，1-身份证 参见user_comm_define.h中E_USER_IDTYPE
 *	String identityNum;///<身份证件号码
 *	int buyerCredit;///<用户信用值
 *	long experience;///<用户经验值
 *	String cftAccount;///<财付通账号
 *	short loginLevel;///<登录级别，预留
 *	short userType;///<用户类型，从易迅导入，参见user_comm_define.h中E_USER_TYPE
 *	short retailerLevel;///<经销商等级，从易迅导入
 *	short icsonMemberLevel;///<易迅会员等级，从易迅导入
 *	long lastUpdateTime;///<最后修改时间
 *	long regTime;///<用户注册时间
 *	String reserveStr;///<reserve string
 *	long reserveInt;///<reserve int
 *	short version_u;
 *	short wgUid_u;
 *	short icsonUid_u;
 *	short qQNumber_u;
 *	short loginAccount_u;
 *	short accountType_u;
 *	short truename_u;
 *	short nickname_u;
 *	short bitProperty_u;
 *	short userFlagLevel_u;
 *	short sex_u;
 *	short age_u;
 *	short mobile_u;
 *	short email_u;
 *	short phone_u;
 *	short fax_u;
 *	short region_u;
 *	short postcode_u;
 *	short address_u;
 *	short identityType_u;
 *	short identityNum_u;
 *	short buyerCredit_u;
 *	short experience_u;
 *	short cftAccount_u;
 *	short loginLevel_u;
 *	short userType_u;
 *	short retailerLevel_u;
 *	short icsonMemberLevel_u;
 *	short lastUpdateTime_u;
 *	short regTime_u;
 *	short reserveStr_u;
 *	short reserveInt_u;
 *	String weChatId;///<易迅openid
 *	short weChatId_u;
 *****以上是版本20130314所包含的字段*******
 *
 *****以下是版本20141229所包含的字段*******
 *	long version;///<版本号
 *	long wgUid;///<网购用户内部id，非QQ号，目前仅支持32位
 *	long icsonUid;///<易迅用户内部id，目前仅支持32位
 *	long qQNumber;///<绑定的用户QQ号，目前仅支持32位
 *	String loginAccount;///<用户帐号名，包括易迅注册帐号、非qq帐号的第三方联合登录，如LoginAlipay+openid等字符串帐户名
 *	short accountType;///<用户帐号类型，1-QQ号 2-个性化帐号，参见user_comm_define.h中的E_ICSON_USER_ACCOUNT_TYPE
 *	String truename;///<真实姓名
 *	String nickname;///<昵称
 *	BitSet bitProperty;///<用户属性位BitSet，属性位代表的意义请参见b2b2c_define.h中的USER_PROPERTY
 *	Map<uint32_t,uint32_t> userFlagLevel;///<用户属性级别，如彩钻级别，Map中第一个uint32_t表示用户属性位值，第二个uint32_t表示级别值
 *	short sex;///<性别, 0-不明 1-男 2-女 参见user_comm_define.h中的E_USER_SEX
 *	short age;///<年龄
 *	String mobile;///<手机号，如绑定手机的用户属性位为1，则表示绑定的手机号
 *	String email;///<电子邮箱，如绑定邮箱的用户属性位为1，则表示绑定的邮箱
 *	String phone;///<固定电话
 *	String fax;///<传真
 *	long region;///<用户所在地区id
 *	String postcode;///<邮政编码
 *	String address;///<用户详细住址
 *	short identityType;///<身份证件类型，1-身份证 参见user_comm_define.h中E_USER_IDTYPE
 *	String identityNum;///<身份证件号码
 *	int buyerCredit;///<用户信用值
 *	long experience;///<用户经验值
 *	String cftAccount;///<财付通账号
 *	short loginLevel;///<登录级别，预留
 *	short userType;///<用户类型，从易迅导入，参见user_comm_define.h中E_USER_TYPE
 *	short retailerLevel;///<经销商等级，从易迅导入
 *	short icsonMemberLevel;///<易迅会员等级，从易迅导入
 *	long lastUpdateTime;///<最后修改时间
 *	long regTime;///<用户注册时间
 *	String reserveStr;///<reserve string
 *	long reserveInt;///<reserve int
 *	short version_u;
 *	short wgUid_u;
 *	short icsonUid_u;
 *	short qQNumber_u;
 *	short loginAccount_u;
 *	short accountType_u;
 *	short truename_u;
 *	short nickname_u;
 *	short bitProperty_u;
 *	short userFlagLevel_u;
 *	short sex_u;
 *	short age_u;
 *	short mobile_u;
 *	short email_u;
 *	short phone_u;
 *	short fax_u;
 *	short region_u;
 *	short postcode_u;
 *	short address_u;
 *	short identityType_u;
 *	short identityNum_u;
 *	short buyerCredit_u;
 *	short experience_u;
 *	short cftAccount_u;
 *	short loginLevel_u;
 *	short userType_u;
 *	short retailerLevel_u;
 *	short icsonMemberLevel_u;
 *	short lastUpdateTime_u;
 *	short regTime_u;
 *	short reserveStr_u;
 *	short reserveInt_u;
 *	String weChatId;///<易迅openid
 *	short weChatId_u;
 *	long virtualExpPoints;///<虚拟经验值
 *	short virtualExpPoints_u;
 *	short passwordSecureLevel;///<登录密码安全级别
 *	short passwordSecureLevel_u;
 *	Map<uint32_t,uint32_t> mapDiffSrcRegTime;///<不同来源用户注册时间(微购等), key为来源，value为注册时间。易迅用户注册时间请参考另一个字段regTime
 *	short mapDiffSrcRegTime_u;
 *	String wanggouOpenid;///<网购openid
 *	short wanggouOpenid_u;
 *	int sellerCredit;///<卖家总信用
 *	short sellerCredit_u;
 *	short authMask;///<认证信息
 *	short authMask_u;
 *	Map<uint16_t,uint8_t> mapProp;///<用户属性
 *	short mapProp_u;
 *	int recvMsgMask;///<留言标志位
 *	short recvMsgMask_u;
 *	long userInfoMask;///<用户信息标志位
 *	short userInfoMask_u;
 *	short province;///<省份
 *	short province_u;
 *	int city;///<城市
 *	short city_u;
 *	long country;///<城市
 *	short country_u;
 *	String mainLogoPos;///<肖像图片
 *	short mainLogoPos_u;
 *	short commContactType;///<联系方式
 *	short commContactType_u;
 *	long lastLoginIp;///<最后登录IP
 *	short lastLoginIp_u;
 *	long lastLoginTime;///<最后登录时间
 *	short lastLoginTime_u;
 *	short lastLoginType;///<登录类型
 *	short lastLoginType_u;
 *	String referCredit;///<参考信用
 *	short referCredit_u;
 *	int virtualCredit;///<卖家虚拟信用
 *	short virtualCredit_u;
 *	int objectCredit;///<卖家实物信用
 *	short objectCredit_u;
 *	String contacts;///<卖家联系方式
 *	short contacts_u;
 *	long sellerUin;///<多工号权限系统员工所属的卖家号
 *	short sellerUin_u;
 *	long shopClassId;///<店铺类目id[用作店铺类型]
 *	short shopClassId_u;
 *****以上是版本20141229所包含的字段*******
 *
 *****以下是版本20130918所包含的字段*******
 *	long version;///<版本号
 *	long wgUid;///<网购用户内部id，非QQ号，目前仅支持32位
 *	long icsonUid;///<易迅用户内部id，目前仅支持32位
 *	long qQNumber;///<绑定的用户QQ号，目前仅支持32位
 *	String loginAccount;///<用户帐号名，包括易迅注册帐号、非qq帐号的第三方联合登录，如LoginAlipay+openid等字符串帐户名
 *	short accountType;///<用户帐号类型，1-QQ号 2-个性化帐号，参见user_comm_define.h中的E_ICSON_USER_ACCOUNT_TYPE
 *	String truename;///<真实姓名
 *	String nickname;///<昵称
 *	BitSet bitProperty;///<用户属性位BitSet，属性位代表的意义请参见b2b2c_define.h中的USER_PROPERTY
 *	Map<uint32_t,uint32_t> userFlagLevel;///<用户属性级别，如彩钻级别，Map中第一个uint32_t表示用户属性位值，第二个uint32_t表示级别值
 *	short sex;///<性别, 0-不明 1-男 2-女 参见user_comm_define.h中的E_USER_SEX
 *	short age;///<年龄
 *	String mobile;///<手机号，如绑定手机的用户属性位为1，则表示绑定的手机号
 *	String email;///<电子邮箱，如绑定邮箱的用户属性位为1，则表示绑定的邮箱
 *	String phone;///<固定电话
 *	String fax;///<传真
 *	long region;///<用户所在地区id
 *	String postcode;///<邮政编码
 *	String address;///<用户详细住址
 *	short identityType;///<身份证件类型，1-身份证 参见user_comm_define.h中E_USER_IDTYPE
 *	String identityNum;///<身份证件号码
 *	int buyerCredit;///<用户信用值
 *	long experience;///<用户经验值
 *	String cftAccount;///<财付通账号
 *	short loginLevel;///<登录级别，预留
 *	short userType;///<用户类型，从易迅导入，参见user_comm_define.h中E_USER_TYPE
 *	short retailerLevel;///<经销商等级，从易迅导入
 *	short icsonMemberLevel;///<易迅会员等级，从易迅导入
 *	long lastUpdateTime;///<最后修改时间
 *	long regTime;///<用户注册时间
 *	String reserveStr;///<reserve string
 *	long reserveInt;///<reserve int
 *	short version_u;
 *	short wgUid_u;
 *	short icsonUid_u;
 *	short qQNumber_u;
 *	short loginAccount_u;
 *	short accountType_u;
 *	short truename_u;
 *	short nickname_u;
 *	short bitProperty_u;
 *	short userFlagLevel_u;
 *	short sex_u;
 *	short age_u;
 *	short mobile_u;
 *	short email_u;
 *	short phone_u;
 *	short fax_u;
 *	short region_u;
 *	short postcode_u;
 *	short address_u;
 *	short identityType_u;
 *	short identityNum_u;
 *	short buyerCredit_u;
 *	short experience_u;
 *	short cftAccount_u;
 *	short loginLevel_u;
 *	short userType_u;
 *	short retailerLevel_u;
 *	short icsonMemberLevel_u;
 *	short lastUpdateTime_u;
 *	short regTime_u;
 *	short reserveStr_u;
 *	short reserveInt_u;
 *	String weChatId;///<易迅openid
 *	short weChatId_u;
 *	long virtualExpPoints;///<虚拟经验值
 *	short virtualExpPoints_u;
 *	short passwordSecureLevel;///<登录密码安全级别
 *	short passwordSecureLevel_u;
 *	Map<uint32_t,uint32_t> mapDiffSrcRegTime;///<不同来源用户注册时间(微购等), key为来源，value为注册时间。易迅用户注册时间请参考另一个字段regTime
 *	short mapDiffSrcRegTime_u;
 *****以上是版本20130918所包含的字段*******
 *
 *****以下是版本20130529所包含的字段*******
 *	long version;///<版本号
 *	long wgUid;///<网购用户内部id，非QQ号，目前仅支持32位
 *	long icsonUid;///<易迅用户内部id，目前仅支持32位
 *	long qQNumber;///<绑定的用户QQ号，目前仅支持32位
 *	String loginAccount;///<用户帐号名，包括易迅注册帐号、非qq帐号的第三方联合登录，如LoginAlipay+openid等字符串帐户名
 *	short accountType;///<用户帐号类型，1-QQ号 2-个性化帐号，参见user_comm_define.h中的E_ICSON_USER_ACCOUNT_TYPE
 *	String truename;///<真实姓名
 *	String nickname;///<昵称
 *	BitSet bitProperty;///<用户属性位BitSet，属性位代表的意义请参见b2b2c_define.h中的USER_PROPERTY
 *	Map<uint32_t,uint32_t> userFlagLevel;///<用户属性级别，如彩钻级别，Map中第一个uint32_t表示用户属性位值，第二个uint32_t表示级别值
 *	short sex;///<性别, 0-不明 1-男 2-女 参见user_comm_define.h中的E_USER_SEX
 *	short age;///<年龄
 *	String mobile;///<手机号，如绑定手机的用户属性位为1，则表示绑定的手机号
 *	String email;///<电子邮箱，如绑定邮箱的用户属性位为1，则表示绑定的邮箱
 *	String phone;///<固定电话
 *	String fax;///<传真
 *	long region;///<用户所在地区id
 *	String postcode;///<邮政编码
 *	String address;///<用户详细住址
 *	short identityType;///<身份证件类型，1-身份证 参见user_comm_define.h中E_USER_IDTYPE
 *	String identityNum;///<身份证件号码
 *	int buyerCredit;///<用户信用值
 *	long experience;///<用户经验值
 *	String cftAccount;///<财付通账号
 *	short loginLevel;///<登录级别，预留
 *	short userType;///<用户类型，从易迅导入，参见user_comm_define.h中E_USER_TYPE
 *	short retailerLevel;///<经销商等级，从易迅导入
 *	short icsonMemberLevel;///<易迅会员等级，从易迅导入
 *	long lastUpdateTime;///<最后修改时间
 *	long regTime;///<用户注册时间
 *	String reserveStr;///<reserve string
 *	long reserveInt;///<reserve int
 *	short version_u;
 *	short wgUid_u;
 *	short icsonUid_u;
 *	short qQNumber_u;
 *	short loginAccount_u;
 *	short accountType_u;
 *	short truename_u;
 *	short nickname_u;
 *	short bitProperty_u;
 *	short userFlagLevel_u;
 *	short sex_u;
 *	short age_u;
 *	short mobile_u;
 *	short email_u;
 *	short phone_u;
 *	short fax_u;
 *	short region_u;
 *	short postcode_u;
 *	short address_u;
 *	short identityType_u;
 *	short identityNum_u;
 *	short buyerCredit_u;
 *	short experience_u;
 *	short cftAccount_u;
 *	short loginLevel_u;
 *	short userType_u;
 *	short retailerLevel_u;
 *	short icsonMemberLevel_u;
 *	short lastUpdateTime_u;
 *	short regTime_u;
 *	short reserveStr_u;
 *	short reserveInt_u;
 *	String weChatId;///<易迅openid
 *	short weChatId_u;
 *	long virtualExpPoints;///<虚拟经验值
 *	short virtualExpPoints_u;
 *	short passwordSecureLevel;///<登录密码安全级别
 *	short passwordSecureLevel_u;
 *****以上是版本20130529所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
