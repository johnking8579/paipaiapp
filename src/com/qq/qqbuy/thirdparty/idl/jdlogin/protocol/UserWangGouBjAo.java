package com.qq.qqbuy.thirdparty.idl.jdlogin.protocol;



//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang



import com.paipai.util.annotation.Protocol;
import com.paipai.netframework.kernal.NetAction;


import com.paipai.lang.GenericWrapper;/**
 *
 *NetAction类
 *
 */
public class UserWangGouBjAo  extends NetAction
{
	@Override
	 public int getSnowslideThresold(){
		// 这里设置防雪崩时间，也就是超时时间值
		  return 2;
	 }




	@Protocol(cmdid = 0x30081818L, desc = "用户登出，必须设置rcntlinfo中的OperatorUin（统一用户uid）和skey.注：只注销电商自生成私有skey,qqskey注销不使用该接口", export = true)
	 public WanggouUserLogoutResp WanggouUserLogout(WanggouUserLogoutReq req){
		WanggouUserLogoutResp resp = new  WanggouUserLogoutResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x30081819L, desc = "通过统一用户id更新用户信息", export = true)
	 public UpdateUserInfoByWgUidResp UpdateUserInfoByWgUid(UpdateUserInfoByWgUidReq req){
		UpdateUserInfoByWgUidResp resp = new  UpdateUserInfoByWgUidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x30081801L, desc = "网购拍拍用户统一登录", export = true)
	 public UniformLoginResp UniformLogin(UniformLoginReq req){
		UniformLoginResp resp = new  UniformLoginResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x30081812L, desc = "设置微信的默认登陆方式.目前仅支持两种方式：直登和绑登.", export = true)
	 public SetWechatDefaultLoginTypeResp SetWechatDefaultLoginType(SetWechatDefaultLoginTypeReq req){
		SetWechatDefaultLoginTypeResp resp = new  SetWechatDefaultLoginTypeResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x30081810L, desc = "移除微信openid和统一用户id的绑定关系，即解绑", export = true)
	 public RemoveOpenidWithWgUidResp RemoveOpenidWithWgUid(RemoveOpenidWithWgUidReq req){
		RemoveOpenidWithWgUidResp resp = new  RemoveOpenidWithWgUidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x30081805L, desc = "临时接口：支持微客服将已存在的commid和wguid导入到统一用户后台", export = true)
	 public ImportWeigouUserResp ImportWeigouUser(ImportWeigouUserReq req){
		ImportWeigouUserResp resp = new  ImportWeigouUserResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x30081802L, desc = "根据QQ号获取网购用户id, 若统一后台不存在该QQ号账户，则注册一个新用户.需带登录态.", export = true)
	 public GetWgUidByQQResp GetWgUidByQQ(GetWgUidByQQReq req){
		GetWgUidByQQResp resp = new  GetWgUidByQQResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x30081811L, desc = "查询微信默认登陆方式.目前仅支持两种方式：直登和绑登.", export = true)
	 public GetWechatDefaultLoginTypeResp GetWechatDefaultLoginType(GetWechatDefaultLoginTypeReq req){
		GetWechatDefaultLoginTypeResp resp = new  GetWechatDefaultLoginTypeResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x30081817L, desc = "通过统一用户id查询用户QQ号。如果WID对应的用户是非QQ用户，会返回3550错误。如果WID不存在，会返回3508错误", export = true)
	 public GetUserQQNumberByWgUidResp GetUserQQNumberByWgUid(GetUserQQNumberByWgUidReq req){
		GetUserQQNumberByWgUidResp resp = new  GetUserQQNumberByWgUidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x30081813L, desc = "根据QQ号获取用户信息，如果用户不存在则自动导入", export = true)
	 public GetUserInfoOrRegisterIfNotExistByQQResp GetUserInfoOrRegisterIfNotExistByQQ(GetUserInfoOrRegisterIfNotExistByQQReq req){
		GetUserInfoOrRegisterIfNotExistByQQResp resp = new  GetUserInfoOrRegisterIfNotExistByQQResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x30081815L, desc = "根据微信openid获取用户信息，没有会自动产生.", export = true)
	 public GetUserInfoOrRegisterIfNotExistByOpenidResp GetUserInfoOrRegisterIfNotExistByOpenid(GetUserInfoOrRegisterIfNotExistByOpenidReq req){
		GetUserInfoOrRegisterIfNotExistByOpenidResp resp = new  GetUserInfoOrRegisterIfNotExistByOpenidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x30081806L, desc = "根据统一用户id获取用户信息", export = true)
	 public GetUserInfoByWgUidResp GetUserInfoByWgUid(GetUserInfoByWgUidReq req){
		GetUserInfoByWgUidResp resp = new  GetUserInfoByWgUidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x30081807L, desc = "根据QQ号获取用户信息.", export = true)
	 public GetUserInfoByQQResp GetUserInfoByQQ(GetUserInfoByQQReq req){
		GetUserInfoByQQResp resp = new  GetUserInfoByQQResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x30081808L, desc = "根据微信openid获取用户信息.", export = true)
	 public GetUserInfoByOpenidResp GetUserInfoByOpenid(GetUserInfoByOpenidReq req){
		GetUserInfoByOpenidResp resp = new  GetUserInfoByOpenidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x30081814L, desc = "根据绑定帐号获取用户信息.", export = true)
	 public GetUserInfoByBindInfoResp GetUserInfoByBindInfo(GetUserInfoByBindInfoReq req){
		GetUserInfoByBindInfoResp resp = new  GetUserInfoByBindInfoResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x30081803L, desc = "用户验登录，请务必在rcntlinfo中设置wgUid和skey", export = true)
	 public CheckLoginByWgUidResp CheckLoginByWgUid(CheckLoginByWgUidReq req){
		CheckLoginByWgUidResp resp = new  CheckLoginByWgUidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x30081804L, desc = "用户验登录，请务必在rcntlinfo中设置QQ号和QQ skey, 若已登录, 则返回统一后台用户id", export = true)
	 public CheckLoginByQQResp CheckLoginByQQ(CheckLoginByQQReq req){
		CheckLoginByQQResp resp = new  CheckLoginByQQResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x30081816L, desc = "通过统一用户id批量查询用户信息", export = true)
	 public BatchGetUserInfoByWgUidResp BatchGetUserInfoByWgUid(BatchGetUserInfoByWgUidReq req){
		BatchGetUserInfoByWgUidResp resp = new  BatchGetUserInfoByWgUidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x30081809L, desc = "新增微信openid和统一用户id的绑定关系，即绑定辅助帐号", export = true)
	 public AddOpenidWithWgUidResp AddOpenidWithWgUid(AddOpenidWithWgUidReq req){
		AddOpenidWithWgUidResp resp = new  AddOpenidWithWgUidResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }



}
