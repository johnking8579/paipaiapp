 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.opr.ao.idl.MyPaipaiAo.java

package com.qq.qqbuy.thirdparty.idl.interestTags.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import com.qq.qqbuy.thirdparty.idl.interestTags.protocol.MyPaipaiExt;

/**
 *首页我的拍拍推荐位Json数据接口响应参数
 *
 *@date 2014-09-10 06:49:37
 *
 *@since version:0
*/
public class  GetIndexRecommendJsonDataResp extends NetMessage
{
	/**
	 * json格式化数据
	 *
	 * 版本 >= 0
	 */
	 private String jsonData = new String();

	/**
	 * 错误消息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 输出扩展
	 *
	 * 版本 >= 0
	 */
	 private MyPaipaiExt extOut = new MyPaipaiExt();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushString(jsonData);
		bs.pushString(errMsg);
		bs.pushObject(extOut);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		jsonData = bs.popString();
		errMsg = bs.popString();
		extOut = (MyPaipaiExt)bs.popObject(MyPaipaiExt.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x87078001L;
	}


	/**
	 * 获取json格式化数据
	 * 
	 * 此字段的版本 >= 0
	 * @return jsonData value 类型为:String
	 * 
	 */
	public String getJsonData()
	{
		return jsonData;
	}


	/**
	 * 设置json格式化数据
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setJsonData(String value)
	{
		this.jsonData = value;
	}


	/**
	 * 获取错误消息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误消息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	/**
	 * 获取输出扩展
	 * 
	 * 此字段的版本 >= 0
	 * @return extOut value 类型为:MyPaipaiExt
	 * 
	 */
	public MyPaipaiExt getExtOut()
	{
		return extOut;
	}


	/**
	 * 设置输出扩展
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:MyPaipaiExt
	 * 
	 */
	public void setExtOut(MyPaipaiExt value)
	{
		if (value != null) {
				this.extOut = value;
		}else{
				this.extOut = new MyPaipaiExt();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(getIndexRecommendJsonDataResp)
				length += ByteStream.getObjectSize(jsonData, null);  //计算字段jsonData的长度 size_of(String)
				length += ByteStream.getObjectSize(errMsg, null);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(extOut, null);  //计算字段extOut的长度 size_of(MyPaipaiExt)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(getIndexRecommendJsonDataResp)
				length += ByteStream.getObjectSize(jsonData, encoding);  //计算字段jsonData的长度 size_of(String)
				length += ByteStream.getObjectSize(errMsg, encoding);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(extOut, encoding);  //计算字段extOut的长度 size_of(MyPaipaiExt)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
