//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.opr.po.idl.MyPaipaiFilter.java

package com.qq.qqbuy.thirdparty.idl.interestTags.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *用户信息 尽可能的多填
 *
 *@date 2014-09-10 06:49:37
 *
 *@since version:0
*/
public class MyPaipaiUser  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20140508;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 用户UIN
	 *
	 * 版本 >= 0
	 */
	 private long ddwUin;

	/**
	 * 版本 >= 0
	 */
	 private short ddwUin_u;

	/**
	 * 用户UIN 字符串型
	 *
	 * 版本 >= 0
	 */
	 private String strUin = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strUin_u;

	/**
	 * 用户ID
	 *
	 * 版本 >= 0
	 */
	 private long ddwUid;

	/**
	 * 版本 >= 0
	 */
	 private short ddwUid_u;

	/**
	 * 用户ID 字符串型
	 *
	 * 版本 >= 0
	 */
	 private String strUid = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strUid_u;

	/**
	 * VisitKey
	 *
	 * 版本 >= 0
	 */
	 private long ddwVisitKey;

	/**
	 * 版本 >= 0
	 */
	 private short ddwVisitKey_u;

	/**
	 * VisitKey 字符串型
	 *
	 * 版本 >= 0
	 */
	 private String strVisitKey = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strVisitKey_u;

	/**
	 * 客户端IP
	 *
	 * 版本 >= 0
	 */
	 private long dwClientIp;

	/**
	 * 版本 >= 0
	 */
	 private short dwClientIp_u;

	/**
	 * 客户端IP 字符串型
	 *
	 * 版本 >= 0
	 */
	 private String strClientIp = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strClientIp_u;

	/**
	 * 区域ID
	 *
	 * 版本 >= 0
	 */
	 private long dwAreaId;

	/**
	 * 版本 >= 0
	 */
	 private short dwAreaId_u;

	/**
	 * referer
	 *
	 * 版本 >= 0
	 */
	 private String strReferer = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strReferer_u;

	/**
	 * userAgent
	 *
	 * 版本 >= 0
	 */
	 private String strUserAgent = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strUserAgent_u;

	/**
	 * 当前URL
	 *
	 * 版本 >= 0
	 */
	 private String strCurrUrl = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strCurrUrl_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushLong(ddwUin);
		bs.pushUByte(ddwUin_u);
		bs.pushString(strUin);
		bs.pushUByte(strUin_u);
		bs.pushLong(ddwUid);
		bs.pushUByte(ddwUid_u);
		bs.pushString(strUid);
		bs.pushUByte(strUid_u);
		bs.pushLong(ddwVisitKey);
		bs.pushUByte(ddwVisitKey_u);
		bs.pushString(strVisitKey);
		bs.pushUByte(strVisitKey_u);
		bs.pushUInt(dwClientIp);
		bs.pushUByte(dwClientIp_u);
		bs.pushString(strClientIp);
		bs.pushUByte(strClientIp_u);
		bs.pushUInt(dwAreaId);
		bs.pushUByte(dwAreaId_u);
		bs.pushString(strReferer);
		bs.pushUByte(strReferer_u);
		bs.pushString(strUserAgent);
		bs.pushUByte(strUserAgent_u);
		bs.pushString(strCurrUrl);
		bs.pushUByte(strCurrUrl_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		ddwUin = bs.popLong();
		ddwUin_u = bs.popUByte();
		strUin = bs.popString();
		strUin_u = bs.popUByte();
		ddwUid = bs.popLong();
		ddwUid_u = bs.popUByte();
		strUid = bs.popString();
		strUid_u = bs.popUByte();
		ddwVisitKey = bs.popLong();
		ddwVisitKey_u = bs.popUByte();
		strVisitKey = bs.popString();
		strVisitKey_u = bs.popUByte();
		dwClientIp = bs.popUInt();
		dwClientIp_u = bs.popUByte();
		strClientIp = bs.popString();
		strClientIp_u = bs.popUByte();
		dwAreaId = bs.popUInt();
		dwAreaId_u = bs.popUByte();
		strReferer = bs.popString();
		strReferer_u = bs.popUByte();
		strUserAgent = bs.popString();
		strUserAgent_u = bs.popUByte();
		strCurrUrl = bs.popString();
		strCurrUrl_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取用户UIN
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwUin value 类型为:long
	 * 
	 */
	public long getDdwUin()
	{
		return ddwUin;
	}


	/**
	 * 设置用户UIN
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDdwUin(long value)
	{
		this.ddwUin = value;
		this.ddwUin_u = 1;
	}

	public boolean issetDdwUin()
	{
		return this.ddwUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwUin_u value 类型为:short
	 * 
	 */
	public short getDdwUin_u()
	{
		return ddwUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDdwUin_u(short value)
	{
		this.ddwUin_u = value;
	}


	/**
	 * 获取用户UIN 字符串型
	 * 
	 * 此字段的版本 >= 0
	 * @return strUin value 类型为:String
	 * 
	 */
	public String getStrUin()
	{
		return strUin;
	}


	/**
	 * 设置用户UIN 字符串型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrUin(String value)
	{
		this.strUin = value;
		this.strUin_u = 1;
	}

	public boolean issetStrUin()
	{
		return this.strUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strUin_u value 类型为:short
	 * 
	 */
	public short getStrUin_u()
	{
		return strUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrUin_u(short value)
	{
		this.strUin_u = value;
	}


	/**
	 * 获取用户ID
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwUid value 类型为:long
	 * 
	 */
	public long getDdwUid()
	{
		return ddwUid;
	}


	/**
	 * 设置用户ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDdwUid(long value)
	{
		this.ddwUid = value;
		this.ddwUid_u = 1;
	}

	public boolean issetDdwUid()
	{
		return this.ddwUid_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwUid_u value 类型为:short
	 * 
	 */
	public short getDdwUid_u()
	{
		return ddwUid_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDdwUid_u(short value)
	{
		this.ddwUid_u = value;
	}


	/**
	 * 获取用户ID 字符串型
	 * 
	 * 此字段的版本 >= 0
	 * @return strUid value 类型为:String
	 * 
	 */
	public String getStrUid()
	{
		return strUid;
	}


	/**
	 * 设置用户ID 字符串型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrUid(String value)
	{
		this.strUid = value;
		this.strUid_u = 1;
	}

	public boolean issetStrUid()
	{
		return this.strUid_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strUid_u value 类型为:short
	 * 
	 */
	public short getStrUid_u()
	{
		return strUid_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrUid_u(short value)
	{
		this.strUid_u = value;
	}


	/**
	 * 获取VisitKey
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwVisitKey value 类型为:long
	 * 
	 */
	public long getDdwVisitKey()
	{
		return ddwVisitKey;
	}


	/**
	 * 设置VisitKey
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDdwVisitKey(long value)
	{
		this.ddwVisitKey = value;
		this.ddwVisitKey_u = 1;
	}

	public boolean issetDdwVisitKey()
	{
		return this.ddwVisitKey_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwVisitKey_u value 类型为:short
	 * 
	 */
	public short getDdwVisitKey_u()
	{
		return ddwVisitKey_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDdwVisitKey_u(short value)
	{
		this.ddwVisitKey_u = value;
	}


	/**
	 * 获取VisitKey 字符串型
	 * 
	 * 此字段的版本 >= 0
	 * @return strVisitKey value 类型为:String
	 * 
	 */
	public String getStrVisitKey()
	{
		return strVisitKey;
	}


	/**
	 * 设置VisitKey 字符串型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrVisitKey(String value)
	{
		this.strVisitKey = value;
		this.strVisitKey_u = 1;
	}

	public boolean issetStrVisitKey()
	{
		return this.strVisitKey_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strVisitKey_u value 类型为:short
	 * 
	 */
	public short getStrVisitKey_u()
	{
		return strVisitKey_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrVisitKey_u(short value)
	{
		this.strVisitKey_u = value;
	}


	/**
	 * 获取客户端IP
	 * 
	 * 此字段的版本 >= 0
	 * @return dwClientIp value 类型为:long
	 * 
	 */
	public long getDwClientIp()
	{
		return dwClientIp;
	}


	/**
	 * 设置客户端IP
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwClientIp(long value)
	{
		this.dwClientIp = value;
		this.dwClientIp_u = 1;
	}

	public boolean issetDwClientIp()
	{
		return this.dwClientIp_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwClientIp_u value 类型为:short
	 * 
	 */
	public short getDwClientIp_u()
	{
		return dwClientIp_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwClientIp_u(short value)
	{
		this.dwClientIp_u = value;
	}


	/**
	 * 获取客户端IP 字符串型
	 * 
	 * 此字段的版本 >= 0
	 * @return strClientIp value 类型为:String
	 * 
	 */
	public String getStrClientIp()
	{
		return strClientIp;
	}


	/**
	 * 设置客户端IP 字符串型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrClientIp(String value)
	{
		this.strClientIp = value;
		this.strClientIp_u = 1;
	}

	public boolean issetStrClientIp()
	{
		return this.strClientIp_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strClientIp_u value 类型为:short
	 * 
	 */
	public short getStrClientIp_u()
	{
		return strClientIp_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrClientIp_u(short value)
	{
		this.strClientIp_u = value;
	}


	/**
	 * 获取区域ID
	 * 
	 * 此字段的版本 >= 0
	 * @return dwAreaId value 类型为:long
	 * 
	 */
	public long getDwAreaId()
	{
		return dwAreaId;
	}


	/**
	 * 设置区域ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwAreaId(long value)
	{
		this.dwAreaId = value;
		this.dwAreaId_u = 1;
	}

	public boolean issetDwAreaId()
	{
		return this.dwAreaId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwAreaId_u value 类型为:short
	 * 
	 */
	public short getDwAreaId_u()
	{
		return dwAreaId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwAreaId_u(short value)
	{
		this.dwAreaId_u = value;
	}


	/**
	 * 获取referer
	 * 
	 * 此字段的版本 >= 0
	 * @return strReferer value 类型为:String
	 * 
	 */
	public String getStrReferer()
	{
		return strReferer;
	}


	/**
	 * 设置referer
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrReferer(String value)
	{
		this.strReferer = value;
		this.strReferer_u = 1;
	}

	public boolean issetStrReferer()
	{
		return this.strReferer_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strReferer_u value 类型为:short
	 * 
	 */
	public short getStrReferer_u()
	{
		return strReferer_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrReferer_u(short value)
	{
		this.strReferer_u = value;
	}


	/**
	 * 获取userAgent
	 * 
	 * 此字段的版本 >= 0
	 * @return strUserAgent value 类型为:String
	 * 
	 */
	public String getStrUserAgent()
	{
		return strUserAgent;
	}


	/**
	 * 设置userAgent
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrUserAgent(String value)
	{
		this.strUserAgent = value;
		this.strUserAgent_u = 1;
	}

	public boolean issetStrUserAgent()
	{
		return this.strUserAgent_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strUserAgent_u value 类型为:short
	 * 
	 */
	public short getStrUserAgent_u()
	{
		return strUserAgent_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrUserAgent_u(short value)
	{
		this.strUserAgent_u = value;
	}


	/**
	 * 获取当前URL
	 * 
	 * 此字段的版本 >= 0
	 * @return strCurrUrl value 类型为:String
	 * 
	 */
	public String getStrCurrUrl()
	{
		return strCurrUrl;
	}


	/**
	 * 设置当前URL
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrCurrUrl(String value)
	{
		this.strCurrUrl = value;
		this.strCurrUrl_u = 1;
	}

	public boolean issetStrCurrUrl()
	{
		return this.strCurrUrl_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strCurrUrl_u value 类型为:short
	 * 
	 */
	public short getStrCurrUrl_u()
	{
		return strCurrUrl_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrCurrUrl_u(short value)
	{
		this.strCurrUrl_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(MyPaipaiUser)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段ddwUin的长度 size_of(uint64_t)
				length += 1;  //计算字段ddwUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strUin, null);  //计算字段strUin的长度 size_of(String)
				length += 1;  //计算字段strUin_u的长度 size_of(uint8_t)
				length += 17;  //计算字段ddwUid的长度 size_of(uint64_t)
				length += 1;  //计算字段ddwUid_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strUid, null);  //计算字段strUid的长度 size_of(String)
				length += 1;  //计算字段strUid_u的长度 size_of(uint8_t)
				length += 17;  //计算字段ddwVisitKey的长度 size_of(uint64_t)
				length += 1;  //计算字段ddwVisitKey_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strVisitKey, null);  //计算字段strVisitKey的长度 size_of(String)
				length += 1;  //计算字段strVisitKey_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwClientIp的长度 size_of(uint32_t)
				length += 1;  //计算字段dwClientIp_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strClientIp, null);  //计算字段strClientIp的长度 size_of(String)
				length += 1;  //计算字段strClientIp_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwAreaId的长度 size_of(uint32_t)
				length += 1;  //计算字段dwAreaId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strReferer, null);  //计算字段strReferer的长度 size_of(String)
				length += 1;  //计算字段strReferer_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strUserAgent, null);  //计算字段strUserAgent的长度 size_of(String)
				length += 1;  //计算字段strUserAgent_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strCurrUrl, null);  //计算字段strCurrUrl的长度 size_of(String)
				length += 1;  //计算字段strCurrUrl_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(MyPaipaiUser)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段ddwUin的长度 size_of(uint64_t)
				length += 1;  //计算字段ddwUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strUin, encoding);  //计算字段strUin的长度 size_of(String)
				length += 1;  //计算字段strUin_u的长度 size_of(uint8_t)
				length += 17;  //计算字段ddwUid的长度 size_of(uint64_t)
				length += 1;  //计算字段ddwUid_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strUid, encoding);  //计算字段strUid的长度 size_of(String)
				length += 1;  //计算字段strUid_u的长度 size_of(uint8_t)
				length += 17;  //计算字段ddwVisitKey的长度 size_of(uint64_t)
				length += 1;  //计算字段ddwVisitKey_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strVisitKey, encoding);  //计算字段strVisitKey的长度 size_of(String)
				length += 1;  //计算字段strVisitKey_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwClientIp的长度 size_of(uint32_t)
				length += 1;  //计算字段dwClientIp_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strClientIp, encoding);  //计算字段strClientIp的长度 size_of(String)
				length += 1;  //计算字段strClientIp_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwAreaId的长度 size_of(uint32_t)
				length += 1;  //计算字段dwAreaId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strReferer, encoding);  //计算字段strReferer的长度 size_of(String)
				length += 1;  //计算字段strReferer_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strUserAgent, encoding);  //计算字段strUserAgent的长度 size_of(String)
				length += 1;  //计算字段strUserAgent_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strCurrUrl, encoding);  //计算字段strCurrUrl的长度 size_of(String)
				length += 1;  //计算字段strCurrUrl_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
