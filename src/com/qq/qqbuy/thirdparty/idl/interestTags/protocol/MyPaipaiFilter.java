//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.opr.ao.idl.getIndexRecommendJsonDataReq.java

package com.qq.qqbuy.thirdparty.idl.interestTags.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import com.paipai.lang.uint64_t;
import java.util.Map;
import java.util.HashMap;

/**
 *查询过滤器 用作入参
 *
 *@date 2014-09-10 06:49:37
 *
 *@since version:0
*/
public class MyPaipaiFilter  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20140508;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 用户信息
	 *
	 * 版本 >= 0
	 */
	 private MyPaipaiUser userInfo = new MyPaipaiUser();

	/**
	 * 版本 >= 0
	 */
	 private short userInfo_u;

	/**
	 * 运营坑位信息 <内部场景id, 场景数据>
	 *
	 * 版本 >= 0
	 */
	 private Map<uint64_t,SceneData> mapSceneData = new HashMap<uint64_t,SceneData>();

	/**
	 * 版本 >= 0
	 */
	 private short mapSceneData_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushObject(userInfo);
		bs.pushUByte(userInfo_u);
		bs.pushObject(mapSceneData);
		bs.pushUByte(mapSceneData_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		userInfo = (MyPaipaiUser) bs.popObject(MyPaipaiUser.class);
		userInfo_u = bs.popUByte();
		mapSceneData = (Map<uint64_t,SceneData>)bs.popMap(uint64_t.class,SceneData.class);
		mapSceneData_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取用户信息
	 * 
	 * 此字段的版本 >= 0
	 * @return userInfo value 类型为:MyPaipaiUser
	 * 
	 */
	public MyPaipaiUser getUserInfo()
	{
		return userInfo;
	}


	/**
	 * 设置用户信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:MyPaipaiUser
	 * 
	 */
	public void setUserInfo(MyPaipaiUser value)
	{
		if (value != null) {
				this.userInfo = value;
				this.userInfo_u = 1;
		}
	}

	public boolean issetUserInfo()
	{
		return this.userInfo_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return userInfo_u value 类型为:short
	 * 
	 */
	public short getUserInfo_u()
	{
		return userInfo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUserInfo_u(short value)
	{
		this.userInfo_u = value;
	}


	/**
	 * 获取运营坑位信息 <内部场景id, 场景数据>
	 * 
	 * 此字段的版本 >= 0
	 * @return mapSceneData value 类型为:Map<uint64_t,SceneData>
	 * 
	 */
	public Map<uint64_t,SceneData> getMapSceneData()
	{
		return mapSceneData;
	}


	/**
	 * 设置运营坑位信息 <内部场景id, 场景数据>
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<uint64_t,SceneData>
	 * 
	 */
	public void setMapSceneData(Map<uint64_t,SceneData> value)
	{
		if (value != null) {
				this.mapSceneData = value;
				this.mapSceneData_u = 1;
		}
	}

	public boolean issetMapSceneData()
	{
		return this.mapSceneData_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return mapSceneData_u value 类型为:short
	 * 
	 */
	public short getMapSceneData_u()
	{
		return mapSceneData_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMapSceneData_u(short value)
	{
		this.mapSceneData_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(MyPaipaiFilter)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(userInfo, null);  //计算字段userInfo的长度 size_of(MyPaipaiUser)
				length += 1;  //计算字段userInfo_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(mapSceneData, null);  //计算字段mapSceneData的长度 size_of(Map)
				length += 1;  //计算字段mapSceneData_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(MyPaipaiFilter)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(userInfo, encoding);  //计算字段userInfo的长度 size_of(MyPaipaiUser)
				length += 1;  //计算字段userInfo_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(mapSceneData, encoding);  //计算字段mapSceneData的长度 size_of(Map)
				length += 1;  //计算字段mapSceneData_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
