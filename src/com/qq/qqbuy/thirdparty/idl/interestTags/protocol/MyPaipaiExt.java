//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.opr.ao.idl.getIndexRecommendJsonDataReq.java

package com.qq.qqbuy.thirdparty.idl.interestTags.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import java.util.Map;
import com.paipai.lang.uint32_t;
import java.util.HashMap;

/**
 *扩展对象
 *
 *@date 2014-09-10 06:49:37
 *
 *@since version:0
*/
public class MyPaipaiExt  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20140617;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 调试信息
	 *
	 * 版本 >= 20140617
	 */
	 private Map<uint32_t,String> mapDebug = new HashMap<uint32_t,String>();

	/**
	 * 
	 *
	 * 版本 >= 20140617
	 */
	 private short mapDebug_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		if(  this.version >= 20140617 ){
				bs.pushObject(mapDebug);
		}
		if(  this.version >= 20140617 ){
				bs.pushUByte(mapDebug_u);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		if(  this.version >= 20140617 ){
				mapDebug = (Map<uint32_t,String>)bs.popMap(uint32_t.class,String.class);
		}
		if(  this.version >= 20140617 ){
				mapDebug_u = bs.popUByte();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取调试信息
	 * 
	 * 此字段的版本 >= 20140617
	 * @return mapDebug value 类型为:Map<uint32_t,String>
	 * 
	 */
	public Map<uint32_t,String> getMapDebug()
	{
		return mapDebug;
	}


	/**
	 * 设置调试信息
	 * 
	 * 此字段的版本 >= 20140617
	 * @param  value 类型为:Map<uint32_t,String>
	 * 
	 */
	public void setMapDebug(Map<uint32_t,String> value)
	{
		if (value != null) {
				this.mapDebug = value;
				this.mapDebug_u = 1;
		}
	}

	public boolean issetMapDebug()
	{
		return this.mapDebug_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20140617
	 * @return mapDebug_u value 类型为:short
	 * 
	 */
	public short getMapDebug_u()
	{
		return mapDebug_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20140617
	 * @param  value 类型为:short
	 * 
	 */
	public void setMapDebug_u(short value)
	{
		this.mapDebug_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(MyPaipaiExt)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				if(  this.version >= 20140617 ){
						length += ByteStream.getObjectSize(mapDebug, null);  //计算字段mapDebug的长度 size_of(Map)
				}
				if(  this.version >= 20140617 ){
						length += 1;  //计算字段mapDebug_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(MyPaipaiExt)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				if(  this.version >= 20140617 ){
						length += ByteStream.getObjectSize(mapDebug, encoding);  //计算字段mapDebug的长度 size_of(Map)
				}
				if(  this.version >= 20140617 ){
						length += 1;  //计算字段mapDebug_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本20140617所包含的字段*******
 *	long version;///<协议版本号
 *	short version_u;
 *	Map<uint32_t,String> mapDebug;///<调试信息
 *	short mapDebug_u;
 *****以上是版本20140617所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
