//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.opr.po.idl.MyPaipaiFilter.java

package com.qq.qqbuy.thirdparty.idl.interestTags.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import java.util.Map;
import com.paipai.lang.uint32_t;
import java.util.Vector;
import java.util.HashMap;

/**
 *运营坑位 信息 
 *
 *@date 2014-09-10 06:49:37
 *
 *@since version:0
*/
public class SceneData  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20140508;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 内部场景ID 比如COSS广告组ID [IN]
	 *
	 * 版本 >= 0
	 */
	 private long ddwCmd;

	/**
	 * 版本 >= 0
	 */
	 private short ddwCmd_u;

	/**
	 * 调用方ID，BI相关 [IN]
	 *
	 * 版本 >= 0
	 */
	 private long dwBiHi;

	/**
	 * 版本 >= 0
	 */
	 private short dwBiHi_u;

	/**
	 * 调用方业务ID，BI相关 [IN]
	 *
	 * 版本 >= 0
	 */
	 private long dwBiMid;

	/**
	 * 版本 >= 0
	 */
	 private short dwBiMid_u;

	/**
	 * 坑位类型 ： 0 商品  1 素材   2 店铺 [IN]
	 *
	 * 版本 >= 0
	 */
	 private long dwType;

	/**
	 * 版本 >= 0
	 */
	 private short dwType_u;

	/**
	 * 数量 [IN]
	 *
	 * 版本 >= 0
	 */
	 private long dwNum;

	/**
	 * 版本 >= 0
	 */
	 private short dwNum_u;

	/**
	 * 返回码 [OUT]
	 *
	 * 版本 >= 0
	 */
	 private int iStatus;

	/**
	 * 版本 >= 0
	 */
	 private short iStatus_u;

	/**
	 * BI场景ID [OUT]
	 *
	 * 版本 >= 0
	 */
	 private long ddwBiCmd;

	/**
	 * 版本 >= 0
	 */
	 private short ddwBiCmd_u;

	/**
	 * 商品素材信息 [OUT]
	 *
	 * 版本 >= 0
	 */
	 private Vector<ItemIdInfo> vecItemIds = new Vector<ItemIdInfo>();

	/**
	 * 版本 >= 0
	 */
	 private short vecItemIds_u;

	/**
	 * 附加param 填充到URL上 [OUT]
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> mapParamGroup = new HashMap<String,String>();

	/**
	 * 版本 >= 0
	 */
	 private short mapParamGroup_u;

	/**
	 * uiPageNo
	 *
	 * 版本 >= 0
	 */
	 private long dwPageNo;

	/**
	 * 版本 >= 0
	 */
	 private short dwPageNo_u;

	/**
	 * uiPageSize
	 *
	 * 版本 >= 0
	 */
	 private long dwPageSize;

	/**
	 * 版本 >= 0
	 */
	 private short dwPageSize_u;

	/**
	 * reserved 备用 vecReserved[0] 排序种子传入
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint32_t> vecReserved = new Vector<uint32_t>();

	/**
	 * 版本 >= 0
	 */
	 private short vecReserved_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushLong(ddwCmd);
		bs.pushUByte(ddwCmd_u);
		bs.pushUInt(dwBiHi);
		bs.pushUByte(dwBiHi_u);
		bs.pushUInt(dwBiMid);
		bs.pushUByte(dwBiMid_u);
		bs.pushUInt(dwType);
		bs.pushUByte(dwType_u);
		bs.pushUInt(dwNum);
		bs.pushUByte(dwNum_u);
		bs.pushInt(iStatus);
		bs.pushUByte(iStatus_u);
		bs.pushLong(ddwBiCmd);
		bs.pushUByte(ddwBiCmd_u);
		bs.pushObject(vecItemIds);
		bs.pushUByte(vecItemIds_u);
		bs.pushObject(mapParamGroup);
		bs.pushUByte(mapParamGroup_u);
		bs.pushUInt(dwPageNo);
		bs.pushUByte(dwPageNo_u);
		bs.pushUInt(dwPageSize);
		bs.pushUByte(dwPageSize_u);
		bs.pushObject(vecReserved);
		bs.pushUByte(vecReserved_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		ddwCmd = bs.popLong();
		ddwCmd_u = bs.popUByte();
		dwBiHi = bs.popUInt();
		dwBiHi_u = bs.popUByte();
		dwBiMid = bs.popUInt();
		dwBiMid_u = bs.popUByte();
		dwType = bs.popUInt();
		dwType_u = bs.popUByte();
		dwNum = bs.popUInt();
		dwNum_u = bs.popUByte();
		iStatus = bs.popInt();
		iStatus_u = bs.popUByte();
		ddwBiCmd = bs.popLong();
		ddwBiCmd_u = bs.popUByte();
		vecItemIds = (Vector<ItemIdInfo>)bs.popVector(ItemIdInfo.class);
		vecItemIds_u = bs.popUByte();
		mapParamGroup = (Map<String,String>)bs.popMap(String.class,String.class);
		mapParamGroup_u = bs.popUByte();
		dwPageNo = bs.popUInt();
		dwPageNo_u = bs.popUByte();
		dwPageSize = bs.popUInt();
		dwPageSize_u = bs.popUByte();
		vecReserved = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		vecReserved_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取内部场景ID 比如COSS广告组ID [IN]
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwCmd value 类型为:long
	 * 
	 */
	public long getDdwCmd()
	{
		return ddwCmd;
	}


	/**
	 * 设置内部场景ID 比如COSS广告组ID [IN]
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDdwCmd(long value)
	{
		this.ddwCmd = value;
		this.ddwCmd_u = 1;
	}

	public boolean issetDdwCmd()
	{
		return this.ddwCmd_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwCmd_u value 类型为:short
	 * 
	 */
	public short getDdwCmd_u()
	{
		return ddwCmd_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDdwCmd_u(short value)
	{
		this.ddwCmd_u = value;
	}


	/**
	 * 获取调用方ID，BI相关 [IN]
	 * 
	 * 此字段的版本 >= 0
	 * @return dwBiHi value 类型为:long
	 * 
	 */
	public long getDwBiHi()
	{
		return dwBiHi;
	}


	/**
	 * 设置调用方ID，BI相关 [IN]
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwBiHi(long value)
	{
		this.dwBiHi = value;
		this.dwBiHi_u = 1;
	}

	public boolean issetDwBiHi()
	{
		return this.dwBiHi_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwBiHi_u value 类型为:short
	 * 
	 */
	public short getDwBiHi_u()
	{
		return dwBiHi_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwBiHi_u(short value)
	{
		this.dwBiHi_u = value;
	}


	/**
	 * 获取调用方业务ID，BI相关 [IN]
	 * 
	 * 此字段的版本 >= 0
	 * @return dwBiMid value 类型为:long
	 * 
	 */
	public long getDwBiMid()
	{
		return dwBiMid;
	}


	/**
	 * 设置调用方业务ID，BI相关 [IN]
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwBiMid(long value)
	{
		this.dwBiMid = value;
		this.dwBiMid_u = 1;
	}

	public boolean issetDwBiMid()
	{
		return this.dwBiMid_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwBiMid_u value 类型为:short
	 * 
	 */
	public short getDwBiMid_u()
	{
		return dwBiMid_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwBiMid_u(short value)
	{
		this.dwBiMid_u = value;
	}


	/**
	 * 获取坑位类型 ： 0 商品  1 素材   2 店铺 [IN]
	 * 
	 * 此字段的版本 >= 0
	 * @return dwType value 类型为:long
	 * 
	 */
	public long getDwType()
	{
		return dwType;
	}


	/**
	 * 设置坑位类型 ： 0 商品  1 素材   2 店铺 [IN]
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwType(long value)
	{
		this.dwType = value;
		this.dwType_u = 1;
	}

	public boolean issetDwType()
	{
		return this.dwType_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwType_u value 类型为:short
	 * 
	 */
	public short getDwType_u()
	{
		return dwType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwType_u(short value)
	{
		this.dwType_u = value;
	}


	/**
	 * 获取数量 [IN]
	 * 
	 * 此字段的版本 >= 0
	 * @return dwNum value 类型为:long
	 * 
	 */
	public long getDwNum()
	{
		return dwNum;
	}


	/**
	 * 设置数量 [IN]
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwNum(long value)
	{
		this.dwNum = value;
		this.dwNum_u = 1;
	}

	public boolean issetDwNum()
	{
		return this.dwNum_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwNum_u value 类型为:short
	 * 
	 */
	public short getDwNum_u()
	{
		return dwNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwNum_u(short value)
	{
		this.dwNum_u = value;
	}


	/**
	 * 获取返回码 [OUT]
	 * 
	 * 此字段的版本 >= 0
	 * @return iStatus value 类型为:int
	 * 
	 */
	public int getIStatus()
	{
		return iStatus;
	}


	/**
	 * 设置返回码 [OUT]
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setIStatus(int value)
	{
		this.iStatus = value;
		this.iStatus_u = 1;
	}

	public boolean issetIStatus()
	{
		return this.iStatus_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return iStatus_u value 类型为:short
	 * 
	 */
	public short getIStatus_u()
	{
		return iStatus_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIStatus_u(short value)
	{
		this.iStatus_u = value;
	}


	/**
	 * 获取BI场景ID [OUT]
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwBiCmd value 类型为:long
	 * 
	 */
	public long getDdwBiCmd()
	{
		return ddwBiCmd;
	}


	/**
	 * 设置BI场景ID [OUT]
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDdwBiCmd(long value)
	{
		this.ddwBiCmd = value;
		this.ddwBiCmd_u = 1;
	}

	public boolean issetDdwBiCmd()
	{
		return this.ddwBiCmd_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwBiCmd_u value 类型为:short
	 * 
	 */
	public short getDdwBiCmd_u()
	{
		return ddwBiCmd_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDdwBiCmd_u(short value)
	{
		this.ddwBiCmd_u = value;
	}


	/**
	 * 获取商品素材信息 [OUT]
	 * 
	 * 此字段的版本 >= 0
	 * @return vecItemIds value 类型为:Vector<ItemIdInfo>
	 * 
	 */
	public Vector<ItemIdInfo> getVecItemIds()
	{
		return vecItemIds;
	}


	/**
	 * 设置商品素材信息 [OUT]
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<ItemIdInfo>
	 * 
	 */
	public void setVecItemIds(Vector<ItemIdInfo> value)
	{
		if (value != null) {
				this.vecItemIds = value;
				this.vecItemIds_u = 1;
		}
	}

	public boolean issetVecItemIds()
	{
		return this.vecItemIds_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return vecItemIds_u value 类型为:short
	 * 
	 */
	public short getVecItemIds_u()
	{
		return vecItemIds_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVecItemIds_u(short value)
	{
		this.vecItemIds_u = value;
	}


	/**
	 * 获取附加param 填充到URL上 [OUT]
	 * 
	 * 此字段的版本 >= 0
	 * @return mapParamGroup value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getMapParamGroup()
	{
		return mapParamGroup;
	}


	/**
	 * 设置附加param 填充到URL上 [OUT]
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setMapParamGroup(Map<String,String> value)
	{
		if (value != null) {
				this.mapParamGroup = value;
				this.mapParamGroup_u = 1;
		}
	}

	public boolean issetMapParamGroup()
	{
		return this.mapParamGroup_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return mapParamGroup_u value 类型为:short
	 * 
	 */
	public short getMapParamGroup_u()
	{
		return mapParamGroup_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMapParamGroup_u(short value)
	{
		this.mapParamGroup_u = value;
	}


	/**
	 * 获取uiPageNo
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPageNo value 类型为:long
	 * 
	 */
	public long getDwPageNo()
	{
		return dwPageNo;
	}


	/**
	 * 设置uiPageNo
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwPageNo(long value)
	{
		this.dwPageNo = value;
		this.dwPageNo_u = 1;
	}

	public boolean issetDwPageNo()
	{
		return this.dwPageNo_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPageNo_u value 类型为:short
	 * 
	 */
	public short getDwPageNo_u()
	{
		return dwPageNo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwPageNo_u(short value)
	{
		this.dwPageNo_u = value;
	}


	/**
	 * 获取uiPageSize
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPageSize value 类型为:long
	 * 
	 */
	public long getDwPageSize()
	{
		return dwPageSize;
	}


	/**
	 * 设置uiPageSize
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwPageSize(long value)
	{
		this.dwPageSize = value;
		this.dwPageSize_u = 1;
	}

	public boolean issetDwPageSize()
	{
		return this.dwPageSize_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPageSize_u value 类型为:short
	 * 
	 */
	public short getDwPageSize_u()
	{
		return dwPageSize_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwPageSize_u(short value)
	{
		this.dwPageSize_u = value;
	}


	/**
	 * 获取reserved 备用 vecReserved[0] 排序种子传入
	 * 
	 * 此字段的版本 >= 0
	 * @return vecReserved value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getVecReserved()
	{
		return vecReserved;
	}


	/**
	 * 设置reserved 备用 vecReserved[0] 排序种子传入
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setVecReserved(Vector<uint32_t> value)
	{
		if (value != null) {
				this.vecReserved = value;
				this.vecReserved_u = 1;
		}
	}

	public boolean issetVecReserved()
	{
		return this.vecReserved_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return vecReserved_u value 类型为:short
	 * 
	 */
	public short getVecReserved_u()
	{
		return vecReserved_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVecReserved_u(short value)
	{
		this.vecReserved_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SceneData)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段ddwCmd的长度 size_of(uint64_t)
				length += 1;  //计算字段ddwCmd_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwBiHi的长度 size_of(uint32_t)
				length += 1;  //计算字段dwBiHi_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwBiMid的长度 size_of(uint32_t)
				length += 1;  //计算字段dwBiMid_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwType的长度 size_of(uint32_t)
				length += 1;  //计算字段dwType_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwNum的长度 size_of(uint32_t)
				length += 1;  //计算字段dwNum_u的长度 size_of(uint8_t)
				length += 4;  //计算字段iStatus的长度 size_of(int)
				length += 1;  //计算字段iStatus_u的长度 size_of(uint8_t)
				length += 17;  //计算字段ddwBiCmd的长度 size_of(uint64_t)
				length += 1;  //计算字段ddwBiCmd_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(vecItemIds, null);  //计算字段vecItemIds的长度 size_of(Vector)
				length += 1;  //计算字段vecItemIds_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(mapParamGroup, null);  //计算字段mapParamGroup的长度 size_of(Map)
				length += 1;  //计算字段mapParamGroup_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwPageNo的长度 size_of(uint32_t)
				length += 1;  //计算字段dwPageNo_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwPageSize的长度 size_of(uint32_t)
				length += 1;  //计算字段dwPageSize_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(vecReserved, null);  //计算字段vecReserved的长度 size_of(Vector)
				length += 1;  //计算字段vecReserved_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(SceneData)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段ddwCmd的长度 size_of(uint64_t)
				length += 1;  //计算字段ddwCmd_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwBiHi的长度 size_of(uint32_t)
				length += 1;  //计算字段dwBiHi_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwBiMid的长度 size_of(uint32_t)
				length += 1;  //计算字段dwBiMid_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwType的长度 size_of(uint32_t)
				length += 1;  //计算字段dwType_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwNum的长度 size_of(uint32_t)
				length += 1;  //计算字段dwNum_u的长度 size_of(uint8_t)
				length += 4;  //计算字段iStatus的长度 size_of(int)
				length += 1;  //计算字段iStatus_u的长度 size_of(uint8_t)
				length += 17;  //计算字段ddwBiCmd的长度 size_of(uint64_t)
				length += 1;  //计算字段ddwBiCmd_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(vecItemIds, encoding);  //计算字段vecItemIds的长度 size_of(Vector)
				length += 1;  //计算字段vecItemIds_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(mapParamGroup, encoding);  //计算字段mapParamGroup的长度 size_of(Map)
				length += 1;  //计算字段mapParamGroup_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwPageNo的长度 size_of(uint32_t)
				length += 1;  //计算字段dwPageNo_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwPageSize的长度 size_of(uint32_t)
				length += 1;  //计算字段dwPageSize_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(vecReserved, encoding);  //计算字段vecReserved的长度 size_of(Vector)
				length += 1;  //计算字段vecReserved_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
