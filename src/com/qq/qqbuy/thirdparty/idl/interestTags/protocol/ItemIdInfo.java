//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.opr.po.idl.SceneData.java

package com.qq.qqbuy.thirdparty.idl.interestTags.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import com.paipai.lang.uint32_t;
import java.util.Vector;

/**
 *商品/素材 信息 
 *
 *@date 2014-09-10 06:49:37
 *
 *@since version:0
*/
public class ItemIdInfo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 商品/素材/店铺 ID
	 *
	 * 版本 >= 0
	 */
	 private long itemId;

	/**
	 * 版本 >= 0
	 */
	 private short itemId_u;

	/**
	 * 卖家UIN
	 *
	 * 版本 >= 0
	 */
	 private long sellerId;

	/**
	 * 版本 >= 0
	 */
	 private short sellerId_u;

	/**
	 * reserved 备用
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint32_t> vecReserved = new Vector<uint32_t>();

	/**
	 * 版本 >= 0
	 */
	 private short vecReserved_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushLong(itemId);
		bs.pushUByte(itemId_u);
		bs.pushLong(sellerId);
		bs.pushUByte(sellerId_u);
		bs.pushObject(vecReserved);
		bs.pushUByte(vecReserved_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		itemId = bs.popLong();
		itemId_u = bs.popUByte();
		sellerId = bs.popLong();
		sellerId_u = bs.popUByte();
		vecReserved = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		vecReserved_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取商品/素材/店铺 ID
	 * 
	 * 此字段的版本 >= 0
	 * @return itemId value 类型为:long
	 * 
	 */
	public long getItemId()
	{
		return itemId;
	}


	/**
	 * 设置商品/素材/店铺 ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setItemId(long value)
	{
		this.itemId = value;
		this.itemId_u = 1;
	}

	public boolean issetItemId()
	{
		return this.itemId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return itemId_u value 类型为:short
	 * 
	 */
	public short getItemId_u()
	{
		return itemId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemId_u(short value)
	{
		this.itemId_u = value;
	}


	/**
	 * 获取卖家UIN
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerId value 类型为:long
	 * 
	 */
	public long getSellerId()
	{
		return sellerId;
	}


	/**
	 * 设置卖家UIN
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerId(long value)
	{
		this.sellerId = value;
		this.sellerId_u = 1;
	}

	public boolean issetSellerId()
	{
		return this.sellerId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerId_u value 类型为:short
	 * 
	 */
	public short getSellerId_u()
	{
		return sellerId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerId_u(short value)
	{
		this.sellerId_u = value;
	}


	/**
	 * 获取reserved 备用
	 * 
	 * 此字段的版本 >= 0
	 * @return vecReserved value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getVecReserved()
	{
		return vecReserved;
	}


	/**
	 * 设置reserved 备用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setVecReserved(Vector<uint32_t> value)
	{
		if (value != null) {
				this.vecReserved = value;
				this.vecReserved_u = 1;
		}
	}

	public boolean issetVecReserved()
	{
		return this.vecReserved_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return vecReserved_u value 类型为:short
	 * 
	 */
	public short getVecReserved_u()
	{
		return vecReserved_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVecReserved_u(short value)
	{
		this.vecReserved_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemIdInfo)
				length += 17;  //计算字段itemId的长度 size_of(uint64_t)
				length += 1;  //计算字段itemId_u的长度 size_of(uint8_t)
				length += 17;  //计算字段sellerId的长度 size_of(uint64_t)
				length += 1;  //计算字段sellerId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(vecReserved, null);  //计算字段vecReserved的长度 size_of(Vector)
				length += 1;  //计算字段vecReserved_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemIdInfo)
				length += 17;  //计算字段itemId的长度 size_of(uint64_t)
				length += 1;  //计算字段itemId_u的长度 size_of(uint8_t)
				length += 17;  //计算字段sellerId的长度 size_of(uint64_t)
				length += 1;  //计算字段sellerId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(vecReserved, encoding);  //计算字段vecReserved的长度 size_of(Vector)
				length += 1;  //计算字段vecReserved_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
