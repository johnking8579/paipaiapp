 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.opr.ao.idl.MyPaipaiAo.java

package com.qq.qqbuy.thirdparty.idl.interestTags.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import com.qq.qqbuy.thirdparty.idl.interestTags.protocol.MyPaipaiTag;
import java.util.Vector;
import com.qq.qqbuy.thirdparty.idl.interestTags.protocol.MyPaipaiExt;

/**
 *我的拍拍标签管理 设置全局标签信息接口 响应参数
 *
 *@date 2014-09-10 06:49:37
 *
 *@since version:0
*/
public class  SetTagInfoResp extends NetMessage
{
	/**
	 * 错误消息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 标签信息
	 *
	 * 版本 >= 0
	 */
	 private Vector<MyPaipaiTag> tagInfo = new Vector<MyPaipaiTag>();

	/**
	 * 输出扩展
	 *
	 * 版本 >= 0
	 */
	 private MyPaipaiExt extOut = new MyPaipaiExt();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushString(errMsg);
		bs.pushObject(tagInfo);
		bs.pushObject(extOut);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		errMsg = bs.popString();
		tagInfo = (Vector<MyPaipaiTag>)bs.popVector(MyPaipaiTag.class);
		extOut = (MyPaipaiExt)bs.popObject(MyPaipaiExt.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x87078006L;
	}


	/**
	 * 获取错误消息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误消息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	/**
	 * 获取标签信息
	 * 
	 * 此字段的版本 >= 0
	 * @return tagInfo value 类型为:Vector<MyPaipaiTag>
	 * 
	 */
	public Vector<MyPaipaiTag> getTagInfo()
	{
		return tagInfo;
	}


	/**
	 * 设置标签信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<MyPaipaiTag>
	 * 
	 */
	public void setTagInfo(Vector<MyPaipaiTag> value)
	{
		if (value != null) {
				this.tagInfo = value;
		}else{
				this.tagInfo = new Vector<MyPaipaiTag>();
		}
	}


	/**
	 * 获取输出扩展
	 * 
	 * 此字段的版本 >= 0
	 * @return extOut value 类型为:MyPaipaiExt
	 * 
	 */
	public MyPaipaiExt getExtOut()
	{
		return extOut;
	}


	/**
	 * 设置输出扩展
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:MyPaipaiExt
	 * 
	 */
	public void setExtOut(MyPaipaiExt value)
	{
		if (value != null) {
				this.extOut = value;
		}else{
				this.extOut = new MyPaipaiExt();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(setTagInfoResp)
				length += ByteStream.getObjectSize(errMsg, null);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(tagInfo, null);  //计算字段tagInfo的长度 size_of(Vector)
				length += ByteStream.getObjectSize(extOut, null);  //计算字段extOut的长度 size_of(MyPaipaiExt)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(setTagInfoResp)
				length += ByteStream.getObjectSize(errMsg, encoding);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(tagInfo, encoding);  //计算字段tagInfo的长度 size_of(Vector)
				length += ByteStream.getObjectSize(extOut, encoding);  //计算字段extOut的长度 size_of(MyPaipaiExt)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
