//
//
////auto gen by paipai.java.augogen ver 1.0
////auther skyzhuang
//
//package com.qq.qqbuy.thirdparty.idl.interestTags.protocol;
//
//
//import com.paipai.util.annotation.Protocol;
//import com.paipai.netframework.kernal.NetAction;
//
//
//import com.paipai.lang.GenericWrapper;/**
// *
// *NetAction类
// *
// */
//public class MyPaipaiAo  extends NetAction
//{
//	@Override
//	 public int getSnowslideThresold(){
//		// 这里设置防雪崩时间，也就是超时时间值
//		  return 2;
//	 }
//
//
//
//
//	@Protocol(cmdid = 0x87071004L, desc = "我的拍拍标签管理 用户标签设置接口 所设置标签会被过滤在标签池范围内", export = true)
//	 public setUserTagResp setUserTag(setUserTagReq req){
//		setUserTagResp resp = new  setUserTagResp();
//		 //TODO:do request and send resp(处理业务)
//		 return resp;
//	 }
//
//
//	@Protocol(cmdid = 0x87071006L, desc = "我的拍拍标签管理 设置全局标签信息 注意 是set 不是update 会覆盖原有数据", export = true)
//	 public setTagInfoResp setTagInfo(setTagInfoReq req){
//		setTagInfoResp resp = new  setTagInfoResp();
//		 //TODO:do request and send resp(处理业务)
//		 return resp;
//	 }
//
//
//	@Protocol(cmdid = 0x87071003L, desc = "我的拍拍标签管理 用户标签查询接口 含标签ID 商品池ID", export = true)
//	 public getUserTagsResp getUserTags(getUserTagsReq req){
//		getUserTagsResp resp = new  getUserTagsResp();
//		 //TODO:do request and send resp(处理业务)
//		 return resp;
//	 }
//
//
//	@Protocol(cmdid = 0x87071002L, desc = "我的拍拍标签管理 标签信息查询接口", export = true)
//	 public getTagInfoResp getTagInfo(getTagInfoReq req){
//		getTagInfoResp resp = new  getTagInfoResp();
//		 //TODO:do request and send resp(处理业务)
//		 return resp;
//	 }
//
//
//	@Protocol(cmdid = 0x87071005L, desc = "我的拍拍 瀑布流商品信息接口", export = true)
//	 public getItemListJsonDataResp getItemListJsonData(getItemListJsonDataReq req){
//		getItemListJsonDataResp resp = new  getItemListJsonDataResp();
//		 //TODO:do request and send resp(处理业务)
//		 return resp;
//	 }
//
//
//	@Protocol(cmdid = 0x87071001L, desc = "首页我的拍拍推荐位Json数据接口", export = true)
//	 public getIndexRecommendJsonDataResp getIndexRecommendJsonData(getIndexRecommendJsonDataReq req){
//		getIndexRecommendJsonDataResp resp = new  getIndexRecommendJsonDataResp();
//		 //TODO:do request and send resp(处理业务)
//		 return resp;
//	 }
//
//
//
//}
