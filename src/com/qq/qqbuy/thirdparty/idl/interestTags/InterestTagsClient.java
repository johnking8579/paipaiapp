package com.qq.qqbuy.thirdparty.idl.interestTags;

import com.paipai.component.c2cplatform.AsynWebStubException;
import com.paipai.component.c2cplatform.IAsynWebStub;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.interestTags.protocol.GetUserTagsReq;
import com.qq.qqbuy.thirdparty.idl.interestTags.protocol.GetUserTagsResp;
import com.qq.qqbuy.thirdparty.idl.interestTags.protocol.SetUserTagReq;
import com.qq.qqbuy.thirdparty.idl.interestTags.protocol.SetUserTagResp;

public class InterestTagsClient extends SupportIDLBaseClient{

	public static SetUserTagResp setUserTag(SetUserTagReq req){
		long start = System.currentTimeMillis();
       
        SetUserTagResp resp=new SetUserTagResp();
        
        int ret = -1;
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		if(!EnvManager.isIdc()){ //在非IDC环境用这个IDC上的兴趣标签服务做测试,原因： gamma环境下 未部署
			 stub.setIpAndPort("10.213.138.122", 53101);
	        }
		stub.setOperator(req.getUserInfo().getDdwUin());
        stub.setUin(req.getUserInfo().getDdwUin());

		
		try {
			ret=stub.invoke(req, resp);
		} catch (AsynWebStubException e) {
		}
        
        return resp;
	}
	
	public static GetUserTagsResp getUserTag(GetUserTagsReq req)
	{
	
		long start = System.currentTimeMillis();
		GetUserTagsResp resp=new GetUserTagsResp();
		 int ret = -1;
		try {
        	
     		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
     		  if(!EnvManager.isIdc()){ //在非IDC环境用这个IDC上的兴趣标签服务做测试,原因： gamma环境下 未部署
     			 stub.setIpAndPort("10.213.138.122", 53101);
     	        }
     		
     		
     		stub.setOperator(req.getUserInfo().getDdwUin());
             stub.setUin(req.getUserInfo().getDdwUin());
           
     		
     		try {
     			ret=stub.invoke(req, resp);
     		} catch (AsynWebStubException e) {
     			throw new RuntimeException(e);
     		}
     		
		} catch (Exception e) {
			Log.run.debug("InterestTagsClient#getUserTag ret:"+ret+"errCode:"+resp.getResult()+"errMsg:"+resp.getErrMsg());
		}
       
        return resp;
	}
}
