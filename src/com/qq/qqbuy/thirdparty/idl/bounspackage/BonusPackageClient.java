package com.qq.qqbuy.thirdparty.idl.bounspackage;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.PaiPaiConfig;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.bounspackage.protocol.SearchBonusPackagePo;
import com.qq.qqbuy.thirdparty.idl.bounspackage.protocol.SearchBonusPackageReq;
import com.qq.qqbuy.thirdparty.idl.bounspackage.protocol.SearchBonusPackageResp;
import com.qq.qqbuy.thirdparty.idl.bounspackage.protocol.SearchBonusPackagersp;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

/**
 * Created by wanghao5 on 2015/3/10.
 */
public class BonusPackageClient extends SupportIDLBaseClient {

    protected static final String CMDY_CALL_IDL_MACHINEKEY = "mobileLife";
    protected static final String CMDY_CALL_IDL_SOURCE = "mobileLife"; // 用于IDL接口服务端判断是否进行免登陆处理

    protected static final String BONUSPACKAGE_HBDESC_CGI_URL = "http://b.paipai.com/useraccount/getbalance?callback=cb_getHbDesc";

    protected static final String BONUSPACKAGE_HBLIST_CGI_URL = "http://b.paipai.com/useraccount/GetBonusDetail?callback=showList";

    protected static final String BONUSPACKAGE_CGI_HOST = "b.paipai.com";

    /**
     *
     *
     * 个人中心红包信息接口
     * @param wid
     * @param sk
     * @return {
        "balance": 0,//当optype!=1&&当optype!=2时，红包钱数
        "big_bonu_money": 0,
        "big_bonus_totalnum": 0, //当optype=1时，待领取的红包个数
        "card_id": "pBEmIjqOv4BE4KfogrhHWtNE6UtU",
        "code": 0,
        "little_bonus_money": 0, //当optype=2时，即将过期的红包钱数
        "little_bonus_totalnum": 0,
        "openid": "",
        "optype": 3,
        "signature": "4e018915329a29a0f3d9074612ebeab2426b4774",
        "timestamp": 1426147204
        }
     * optype =1时，
     */
    public static String getHbDesc(long wid,String sk){
        String results = "{}";
        try {
            StringBuffer url = new StringBuffer();
            url.append(BONUSPACKAGE_HBDESC_CGI_URL);
            int timeOut = 3000;
            String cookie = "wg_uin=" + wid + ";wg_skey=" + sk;
            String result = HttpUtil.get(url.toString().replace(BONUSPACKAGE_CGI_HOST, PaiPaiConfig.getPaipaiCommonIp()),BONUSPACKAGE_CGI_HOST, timeOut, timeOut, "gbk", false,false,cookie,null);
            String head = "try{cb_getHbDesc(";
            String tail = ");}catch(e){}";
            if(!result.startsWith(head)){
                throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(), "红包信息接口异常");
            }
            int index = result.indexOf(head);
            int end = result.indexOf(tail);
            if(index >= 0 && end > 0){
                result = result.substring(index + head.length(), end);
            }
            ObjectMapper jsonMapper = new ObjectMapper();
            JsonNode rootNode = jsonMapper.readValue(result, JsonNode.class);
            int code = rootNode.path("code").asInt();
            if(code != 0){
                if(code == 17005 || code == 13){
                    results = "{}";
                }else{
                    String errmsg = rootNode.path("errmsg").asText();
                    throw BusinessException.createInstance(code,errmsg);
                }
            }else{
                results = jsonMapper.writeValueAsString(rootNode);
            }
            Log.run.info("getHbDesc#results:" + results);
        } catch (JsonParseException e) {
            Log.run.error("接口返回值转换json对象异常！");
            throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"获取红包信息异常");
        } catch (JsonMappingException e) {
            Log.run.error("json对象字段映射异常！");
            throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"获取红包信息异常");
        } catch (IOException e) {
            Log.run.error("");
            throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"获取红包信息异常");
        }
        return results;
    }

    /**
     * 获取领取的红包列表
     * @param wid
     * @param sk
     * @return
     */
    public static String getBonusDetail(long wid,String sk,int pageindex,int pagesize){
        String results = "{}";
        try {
            StringBuffer url = new StringBuffer();
            url.append(BONUSPACKAGE_HBLIST_CGI_URL).append("&pageindex=").append(pageindex)
                    .append("&pagesize=").append(pagesize);
            int timeOut = 3000;
            String cookie = "wg_uin=" + wid + ";wg_skey=" + sk;
            String result = HttpUtil.get(url.toString().replace(BONUSPACKAGE_CGI_HOST, PaiPaiConfig.getPaipaiCommonIp()),BONUSPACKAGE_CGI_HOST, timeOut, timeOut, "gbk", false,false,cookie,null);
            String head = "try{showList(";
            String tail = ");}catch(e){}";
            if(!result.startsWith(head)){
                throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(), "红包列表接口异常");
            }
            int index = result.indexOf(head);
            int end = result.indexOf(tail);
            if(index >= 0 && end > 0){
                result = result.substring(index + head.length(), end);
            }
            ObjectMapper jsonMapper = new ObjectMapper();
            JsonNode rootNode = jsonMapper.readValue(result, JsonNode.class);
            int code = rootNode.path("code").asInt();
            if(code != 0){
                if(code == 17005 || code == 13){
                    results = "{}";
                }else{
                    String errmsg = rootNode.path("errmsg").asText();
                    throw BusinessException.createInstance(code,errmsg);
                }
            }else{
                results = jsonMapper.writeValueAsString(rootNode);
            }
            Log.run.info("getHbDesc#results:" + results);
        } catch (JsonParseException e) {
            Log.run.error("接口返回值转换json对象异常！");
            throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"获取红包信息异常");
        } catch (JsonMappingException e) {
            Log.run.error("json对象字段映射异常！");
            throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"获取红包信息异常");
        } catch (IOException e) {
            Log.run.error("");
            throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"获取红包信息异常");
        }
        return results;
    }

    /**
     * 获取用户红包列表
     * @param sk 用户登录标识
     * @param po 接口请求参数
     * @return
     */
    public static SearchBonusPackagersp bonusPackageList(String sk,SearchBonusPackagePo po){

        SearchBonusPackageReq req = new SearchBonusPackageReq();
        req.setOSearchBonusPackageReq(po);
        SearchBonusPackageResp resp = new SearchBonusPackageResp();
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setSkey(sk.getBytes());
        int ret = invokePaiPaiIDL(req, resp, stub);
        return ret == SUCCESS ? resp.getOSearchBonusPackageResp() : null;
    }

    public static void main(String[] args) {
        long wid = 419739377L;
        String sk = "zaE8FCAFA7";
        int pageindex = 1;
        int pagesize = 20;
        BonusPackageClient.getHbDesc(wid,sk);
        BonusPackageClient.getBonusDetail(wid,sk,pageindex,pagesize);
    }
}
