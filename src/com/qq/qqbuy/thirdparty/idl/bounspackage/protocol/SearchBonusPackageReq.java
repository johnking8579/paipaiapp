 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: bounspackage.idl.BonusPackagesAo.java

package com.qq.qqbuy.thirdparty.idl.bounspackage.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2015-03-10 04:52:47
 *
 *@since version:0
*/
public class  SearchBonusPackageReq implements IServiceObject
{
	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private SearchBonusPackagePo oSearchBonusPackageReq = new SearchBonusPackagePo();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(oSearchBonusPackageReq);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		oSearchBonusPackageReq = (SearchBonusPackagePo) bs.popObject(SearchBonusPackagePo.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x960c1801L;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return oSearchBonusPackageReq value 类型为:SearchBonusPackagePo
	 * 
	 */
	public SearchBonusPackagePo getOSearchBonusPackageReq()
	{
		return oSearchBonusPackageReq;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:SearchBonusPackagePo
	 * 
	 */
	public void setOSearchBonusPackageReq(SearchBonusPackagePo value)
	{
		if (value != null) {
				this.oSearchBonusPackageReq = value;
		}else{
				this.oSearchBonusPackageReq = new SearchBonusPackagePo();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(SearchBonusPackageReq)
				length += ByteStream.getObjectSize(oSearchBonusPackageReq, null);  //计算字段oSearchBonusPackageReq的长度 size_of(SearchBonusPackagePo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(SearchBonusPackageReq)
				length += ByteStream.getObjectSize(oSearchBonusPackageReq, encoding);  //计算字段oSearchBonusPackageReq的长度 size_of(SearchBonusPackagePo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
