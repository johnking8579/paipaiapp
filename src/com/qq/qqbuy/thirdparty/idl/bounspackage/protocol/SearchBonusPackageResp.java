 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: bounspackage.idl.BonusPackagesAo.java

package com.qq.qqbuy.thirdparty.idl.bounspackage.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2015-03-10 04:52:47
 *
 *@since version:0
*/
public class  SearchBonusPackageResp implements IServiceObject
{
	public long result;
	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private SearchBonusPackagersp oSearchBonusPackageResp = new SearchBonusPackagersp();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(oSearchBonusPackageResp);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		oSearchBonusPackageResp = (SearchBonusPackagersp) bs.popObject(SearchBonusPackagersp.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x960c8801L;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return oSearchBonusPackageResp value 类型为:SearchBonusPackagersp
	 * 
	 */
	public SearchBonusPackagersp getOSearchBonusPackageResp()
	{
		return oSearchBonusPackageResp;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:SearchBonusPackagersp
	 * 
	 */
	public void setOSearchBonusPackageResp(SearchBonusPackagersp value)
	{
		if (value != null) {
				this.oSearchBonusPackageResp = value;
		}else{
				this.oSearchBonusPackageResp = new SearchBonusPackagersp();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SearchBonusPackageResp)
				length += ByteStream.getObjectSize(oSearchBonusPackageResp, null);  //计算字段oSearchBonusPackageResp的长度 size_of(SearchBonusPackagersp)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(SearchBonusPackageResp)
				length += ByteStream.getObjectSize(oSearchBonusPackageResp, encoding);  //计算字段oSearchBonusPackageResp的长度 size_of(SearchBonusPackagersp)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
