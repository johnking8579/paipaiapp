 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.vb2c.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *resp
 *
 *@date 2012-06-01 10:03::40
 *
 *@since version:0
*/
public class  GetGameProductResp implements IServiceObject
{
	public long result;
	/**
	 * 版本 >= 0
	 */
	 private GameSectionServerList gssList = new GameSectionServerList();

	/**
	 * 版本 >= 0
	 */
	 private GameProductList gamePdtList = new GameProductList();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(gssList);
		bs.pushObject(gamePdtList);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		gssList = (GameSectionServerList) bs.popObject(GameSectionServerList.class);
		gamePdtList = (GameProductList) bs.popObject(GameProductList.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x70618805L;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return gssList value 类型为:GameSectionServerList
	 * 
	 */
	public GameSectionServerList getGssList()
	{
		return gssList;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:GameSectionServerList
	 * 
	 */
	public void setGssList(GameSectionServerList value)
	{
		if (value != null) {
				this.gssList = value;
		}else{
				this.gssList = new GameSectionServerList();
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return gamePdtList value 类型为:GameProductList
	 * 
	 */
	public GameProductList getGamePdtList()
	{
		return gamePdtList;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:GameProductList
	 * 
	 */
	public void setGamePdtList(GameProductList value)
	{
		if (value != null) {
				this.gamePdtList = value;
		}else{
				this.gamePdtList = new GameProductList();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetGameProductResp)
				length += ByteStream.getObjectSize(gssList);  //计算字段gssList的长度 size_of(GameSectionServerList)
				length += ByteStream.getObjectSize(gamePdtList);  //计算字段gamePdtList的长度 size_of(GameProductList)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
