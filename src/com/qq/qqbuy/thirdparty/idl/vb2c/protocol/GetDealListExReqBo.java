//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.vb2c.WirelessApi.java

package com.qq.qqbuy.thirdparty.idl.vb2c.protocol;


import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import com.paipai.lang.uint32_t;
import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *订单查询filter
 *
 *@date 2012-12-06 07:39:59
 *
 *@since version:0
*/
public class GetDealListExReqBo  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20101108;

	/**
	 * 买家QQ号码
	 *
	 * 版本 >= 0
	 */
	 private long buyerUin;

	/**
	 * 订单状态，1-等待买家付款 ；2-等待发货； 3-发货中；4-退款中；5-交易成功；6-交易完成，已退款； 7-交易取消
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint32_t> dealStateList = new Vector<uint32_t>();

	/**
	 * 渠道ID
	 *
	 * 版本 >= 0
	 */
	 private long channelId;

	/**
	 * 起始位置，从0开始
	 *
	 * 版本 >= 0
	 */
	 private long start;

	/**
	 * 每页大小，最大为50，超过50，默认为50
	 *
	 * 版本 >= 0
	 */
	 private long pageSize;

	/**
	 * 其他查询条件
	 *
	 *  map["deal_type"] = "1,3"
	 *  
     *  1是话费，3是网游
     *
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> filter = new HashMap<String,String>();

	/**
	 * FLAG字段
	 *
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 版本 >= 0
	 */
	 private short buyerUin_u;

	/**
	 * 版本 >= 0
	 */
	 private short dealStateList_u;

	/**
	 * 版本 >= 0
	 */
	 private short channelId_u;

	/**
	 * 版本 >= 0
	 */
	 private short start_u;

	/**
	 * 版本 >= 0
	 */
	 private short pageSize_u;

	/**
	 * 版本 >= 0
	 */
	 private short filter_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUInt(buyerUin);
		bs.pushObject(dealStateList);
		bs.pushUInt(channelId);
		bs.pushUInt(start);
		bs.pushUInt(pageSize);
		bs.pushObject(filter);
		bs.pushUByte(version_u);
		bs.pushUByte(buyerUin_u);
		bs.pushUByte(dealStateList_u);
		bs.pushUByte(channelId_u);
		bs.pushUByte(start_u);
		bs.pushUByte(pageSize_u);
		bs.pushUByte(filter_u);
		return bs.getWrittenLength();
	}
	
	@SuppressWarnings("unchecked")
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		buyerUin = bs.popUInt();
		dealStateList = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		channelId = bs.popUInt();
		start = bs.popUInt();
		pageSize = bs.popUInt();
		filter = (Map<String,String>)bs.popMap(String.class,String.class);
		version_u = bs.popUByte();
		buyerUin_u = bs.popUByte();
		dealStateList_u = bs.popUByte();
		channelId_u = bs.popUByte();
		start_u = bs.popUByte();
		pageSize_u = bs.popUByte();
		filter_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取买家QQ号码
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return buyerUin;
	}


	/**
	 * 设置买家QQ号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.buyerUin = value;
		this.buyerUin_u = 1;
	}


	/**
	 * 获取订单状态，1-等待买家付款 ；2-等待发货； 3-发货中；4-退款中；5-交易成功；6-交易完成，已退款； 7-交易取消
	 * 
	 * 此字段的版本 >= 0
	 * @return dealStateList value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getDealStateList()
	{
		return dealStateList;
	}


	/**
	 * 设置订单状态，1-等待买家付款 ；2-等待发货； 3-发货中；4-退款中；5-交易成功；6-交易完成，已退款； 7-交易取消
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setDealStateList(Vector<uint32_t> value)
	{
		if (value != null) {
				this.dealStateList = value;
				this.dealStateList_u = 1;
		}
	}


	/**
	 * 获取渠道ID
	 * 
	 * 此字段的版本 >= 0
	 * @return channelId value 类型为:long
	 * 
	 */
	public long getChannelId()
	{
		return channelId;
	}


	/**
	 * 设置渠道ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setChannelId(long value)
	{
		this.channelId = value;
		this.channelId_u = 1;
	}


	/**
	 * 获取起始位置，从0开始
	 * 
	 * 此字段的版本 >= 0
	 * @return start value 类型为:long
	 * 
	 */
	public long getStart()
	{
		return start;
	}


	/**
	 * 设置起始位置，从0开始
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setStart(long value)
	{
		this.start = value;
		this.start_u = 1;
	}


	/**
	 * 获取每页大小，最大为50，超过50，默认为50
	 * 
	 * 此字段的版本 >= 0
	 * @return pageSize value 类型为:long
	 * 
	 */
	public long getPageSize()
	{
		return pageSize;
	}


	/**
	 * 设置每页大小，最大为50，超过50，默认为50
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPageSize(long value)
	{
		this.pageSize = value;
		this.pageSize_u = 1;
	}


	/**
	 * 获取其他查询条件
	 * 
	 * 此字段的版本 >= 0
	 * @return filter value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getFilter()
	{
		return filter;
	}


	/**
	 * 设置其他查询条件
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setFilter(Map<String,String> value)
	{
		if (value != null) {
				this.filter = value;
				this.filter_u = 1;
		}
	}


	/**
	 * 获取FLAG字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置FLAG字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin_u value 类型为:short
	 * 
	 */
	public short getBuyerUin_u()
	{
		return buyerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerUin_u(short value)
	{
		this.buyerUin_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dealStateList_u value 类型为:short
	 * 
	 */
	public short getDealStateList_u()
	{
		return dealStateList_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealStateList_u(short value)
	{
		this.dealStateList_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return channelId_u value 类型为:short
	 * 
	 */
	public short getChannelId_u()
	{
		return channelId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setChannelId_u(short value)
	{
		this.channelId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return start_u value 类型为:short
	 * 
	 */
	public short getStart_u()
	{
		return start_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStart_u(short value)
	{
		this.start_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return pageSize_u value 类型为:short
	 * 
	 */
	public short getPageSize_u()
	{
		return pageSize_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPageSize_u(short value)
	{
		this.pageSize_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return filter_u value 类型为:short
	 * 
	 */
	public short getFilter_u()
	{
		return filter_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFilter_u(short value)
	{
		this.filter_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetDealListExReqBo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段buyerUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(dealStateList);  //计算字段dealStateList的长度 size_of(Vector)
				length += 4;  //计算字段channelId的长度 size_of(uint32_t)
				length += 4;  //计算字段start的长度 size_of(uint32_t)
				length += 4;  //计算字段pageSize的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(filter);  //计算字段filter的长度 size_of(Map)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段buyerUin_u的长度 size_of(uint8_t)
				length += 1;  //计算字段dealStateList_u的长度 size_of(uint8_t)
				length += 1;  //计算字段channelId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段start_u的长度 size_of(uint8_t)
				length += 1;  //计算字段pageSize_u的长度 size_of(uint8_t)
				length += 1;  //计算字段filter_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
