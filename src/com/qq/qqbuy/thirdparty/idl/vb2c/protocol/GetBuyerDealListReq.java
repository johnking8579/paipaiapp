 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.vb2c.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *req
 *
 *@date 2012-06-01 10:03::40
 *
 *@since version:0
*/
public class  GetBuyerDealListReq implements IServiceObject
{
	/**
	 * 来源
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 查询条件
	 *
	 * 版本 >= 0
	 */
	 private QueryFilter queryFilter = new QueryFilter();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(source);
		bs.pushObject(queryFilter);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		source = bs.popString();
		queryFilter = (QueryFilter) bs.popObject(QueryFilter.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x70611801L;
	}


	/**
	 * 获取来源
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取查询条件
	 * 
	 * 此字段的版本 >= 0
	 * @return queryFilter value 类型为:QueryFilter
	 * 
	 */
	public QueryFilter getQueryFilter()
	{
		return queryFilter;
	}


	/**
	 * 设置查询条件
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:QueryFilter
	 * 
	 */
	public void setQueryFilter(QueryFilter value)
	{
		if (value != null) {
				this.queryFilter = value;
		}else{
				this.queryFilter = new QueryFilter();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetBuyerDealListReq)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(queryFilter);  //计算字段queryFilter的长度 size_of(QueryFilter)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
