 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.vb2c.protocol;


import java.util.Vector;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *面值对应网游产品信息
 *
 *@date 2012-06-01 10:03::36
 *
 *@since version:0
*/
public class GameProductByAmount  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20101115; 

	/**
	 * 面值
	 *
	 * 版本 >= 0
	 */
	 private long amount;

	/**
	 * 版本 >= 0
	 */
	 private Vector<GameProductInfo> pdtInfo = new Vector<GameProductInfo>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUInt(amount);
		bs.pushObject(pdtInfo);
		return bs.getWrittenLength();
	}
	
	@SuppressWarnings("unchecked")
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		amount = bs.popUInt();
		pdtInfo = (Vector<GameProductInfo>)bs.popVector(GameProductInfo.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取面值
	 * 
	 * 此字段的版本 >= 0
	 * @return amount value 类型为:long
	 * 
	 */
	public long getAmount()
	{
		return amount;
	}


	/**
	 * 设置面值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAmount(long value)
	{
		this.amount = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return pdtInfo value 类型为:Vector<GameProductInfo>
	 * 
	 */
	public Vector<GameProductInfo> getPdtInfo()
	{
		return pdtInfo;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<GameProductInfo>
	 * 
	 */
	public void setPdtInfo(Vector<GameProductInfo> value)
	{
		if (value != null) {
				this.pdtInfo = value;
		}else{
				this.pdtInfo = new Vector<GameProductInfo>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GameProductByAmount)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段amount的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(pdtInfo);  //计算字段pdtInfo的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
