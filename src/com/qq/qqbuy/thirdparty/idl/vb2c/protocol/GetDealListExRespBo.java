//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.vb2c.WirelessApi.java

package com.qq.qqbuy.thirdparty.idl.vb2c.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *订单查询filter
 *
 *@date 2012-12-06 07:39:59
 *
 *@since version:0
*/
public class GetDealListExRespBo  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20101108;

	/**
	 * 订单总数
	 *
	 * 版本 >= 0
	 */
	 private long totalNum;

	/**
	 * 订单列表
	 *
	 * 版本 >= 0
	 */
	 private DealInfoList dealList = new DealInfoList();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUInt(totalNum);
		bs.pushObject(dealList);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		totalNum = bs.popUInt();
		dealList = (DealInfoList) bs.popObject(DealInfoList.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取订单总数
	 * 
	 * 此字段的版本 >= 0
	 * @return totalNum value 类型为:long
	 * 
	 */
	public long getTotalNum()
	{
		return totalNum;
	}


	/**
	 * 设置订单总数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalNum(long value)
	{
		this.totalNum = value;
	}


	/**
	 * 获取订单列表
	 * 
	 * 此字段的版本 >= 0
	 * @return dealList value 类型为:DealInfoList
	 * 
	 */
	public DealInfoList getDealList()
	{
		return dealList;
	}


	/**
	 * 设置订单列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:DealInfoList
	 * 
	 */
	public void setDealList(DealInfoList value)
	{
		if (value != null) {
				this.dealList = value;
		}else{
				this.dealList = new DealInfoList();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetDealListExRespBo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段totalNum的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(dealList);  //计算字段dealList的长度 size_of(DealInfoList)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
