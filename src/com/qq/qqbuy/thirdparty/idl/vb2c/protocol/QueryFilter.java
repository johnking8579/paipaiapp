 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.vb2c.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *订单查询filter
 *
 *@date 2012-06-01 10:03::34
 *
 *@since version:0
*/
public class QueryFilter  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20101108; 

	/**
	 * 买家QQ号码
	 *
	 * 版本 >= 0
	 */
	 private long buyerUin;

	/**
	 * 订单状态
	 *
	 * 版本 >= 0
	 */
	 private long dealState;

	/**
	 * 起始位置
	 *
	 * 版本 >= 0
	 */
	 private long start;

	/**
	 * 每页大小，最大为50，超过50，默认为50
	 *
	 * 版本 >= 0
	 */
	 private long pageSize;

	/**
	 * 订单总数，第一次取的时候填0
	 *
	 * 版本 >= 0
	 */
	 private long totalNum;

	/**
	 * FLAG字段
	 *
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 版本 >= 0
	 */
	 private short buyerUin_u;

	/**
	 * 版本 >= 0
	 */
	 private short dealState_u;

	/**
	 * 版本 >= 0
	 */
	 private short start_u;

	/**
	 * 版本 >= 0
	 */
	 private short pageSize_u;

	/**
	 * 版本 >= 0
	 */
	 private short totalNum_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUInt(buyerUin);
		bs.pushUInt(dealState);
		bs.pushUInt(start);
		bs.pushUInt(pageSize);
		bs.pushUInt(totalNum);
		bs.pushUByte(version_u);
		bs.pushUByte(buyerUin_u);
		bs.pushUByte(dealState_u);
		bs.pushUByte(start_u);
		bs.pushUByte(pageSize_u);
		bs.pushUByte(totalNum_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		buyerUin = bs.popUInt();
		dealState = bs.popUInt();
		start = bs.popUInt();
		pageSize = bs.popUInt();
		totalNum = bs.popUInt();
		version_u = bs.popUByte();
		buyerUin_u = bs.popUByte();
		dealState_u = bs.popUByte();
		start_u = bs.popUByte();
		pageSize_u = bs.popUByte();
		totalNum_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取买家QQ号码
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return buyerUin;
	}


	/**
	 * 设置买家QQ号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.buyerUin = value;
		this.buyerUin_u = 1;
	}


	/**
	 * 获取订单状态
	 * 
	 * 此字段的版本 >= 0
	 * @return dealState value 类型为:long
	 * 
	 */
	public long getDealState()
	{
		return dealState;
	}


	/**
	 * 设置订单状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealState(long value)
	{
		this.dealState = value;
		this.dealState_u = 1;
	}


	/**
	 * 获取起始位置
	 * 
	 * 此字段的版本 >= 0
	 * @return start value 类型为:long
	 * 
	 */
	public long getStart()
	{
		return start;
	}


	/**
	 * 设置起始位置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setStart(long value)
	{
		this.start = value;
		this.start_u = 1;
	}


	/**
	 * 获取每页大小，最大为50，超过50，默认为50
	 * 
	 * 此字段的版本 >= 0
	 * @return pageSize value 类型为:long
	 * 
	 */
	public long getPageSize()
	{
		return pageSize;
	}


	/**
	 * 设置每页大小，最大为50，超过50，默认为50
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPageSize(long value)
	{
		this.pageSize = value;
		this.pageSize_u = 1;
	}


	/**
	 * 获取订单总数，第一次取的时候填0
	 * 
	 * 此字段的版本 >= 0
	 * @return totalNum value 类型为:long
	 * 
	 */
	public long getTotalNum()
	{
		return totalNum;
	}


	/**
	 * 设置订单总数，第一次取的时候填0
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalNum(long value)
	{
		this.totalNum = value;
		this.totalNum_u = 1;
	}


	/**
	 * 获取FLAG字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置FLAG字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin_u value 类型为:short
	 * 
	 */
	public short getBuyerUin_u()
	{
		return buyerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerUin_u(short value)
	{
		this.buyerUin_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dealState_u value 类型为:short
	 * 
	 */
	public short getDealState_u()
	{
		return dealState_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealState_u(short value)
	{
		this.dealState_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return start_u value 类型为:short
	 * 
	 */
	public short getStart_u()
	{
		return start_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStart_u(short value)
	{
		this.start_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return pageSize_u value 类型为:short
	 * 
	 */
	public short getPageSize_u()
	{
		return pageSize_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPageSize_u(short value)
	{
		this.pageSize_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return totalNum_u value 类型为:short
	 * 
	 */
	public short getTotalNum_u()
	{
		return totalNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTotalNum_u(short value)
	{
		this.totalNum_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(QueryFilter)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段buyerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段dealState的长度 size_of(uint32_t)
				length += 4;  //计算字段start的长度 size_of(uint32_t)
				length += 4;  //计算字段pageSize的长度 size_of(uint32_t)
				length += 4;  //计算字段totalNum的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段buyerUin_u的长度 size_of(uint8_t)
				length += 1;  //计算字段dealState_u的长度 size_of(uint8_t)
				length += 1;  //计算字段start_u的长度 size_of(uint8_t)
				length += 1;  //计算字段pageSize_u的长度 size_of(uint8_t)
				length += 1;  //计算字段totalNum_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
