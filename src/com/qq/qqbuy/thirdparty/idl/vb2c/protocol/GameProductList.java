 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.vb2c.protocol;


import java.util.Vector;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *面值对应网游产品信息列表
 *
 *@date 2012-06-01 10:03::36
 *
 *@since version:0
*/
public class GameProductList  implements ICanSerializeObject
{
	/**
	 * 版本 >= 0
	 */
	 private Vector<GameProductByAmount> gameProductList = new Vector<GameProductByAmount>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushObject(gameProductList);
		return bs.getWrittenLength();
	}
	
	@SuppressWarnings("unchecked")
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		gameProductList = (Vector<GameProductByAmount>)bs.popVector(GameProductByAmount.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return gameProductList value 类型为:Vector<GameProductByAmount>
	 * 
	 */
	public Vector<GameProductByAmount> getGameProductList()
	{
		return gameProductList;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<GameProductByAmount>
	 * 
	 */
	public void setGameProductList(Vector<GameProductByAmount> value)
	{
		if (value != null) {
				this.gameProductList = value;
		}else{
				this.gameProductList = new Vector<GameProductByAmount>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GameProductList)
				length += ByteStream.getObjectSize(gameProductList);  //计算字段gameProductList的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
