 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.vb2c.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *网游下单filter
 *
 *@date 2012-06-01 10:03::36
 *
 *@since version:0
*/
public class GameOrderFilter  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20101115; 

	/**
	 * 买家UIN
	 *
	 * 版本 >= 0
	 */
	 private long buyerUin;

	/**
	 * 商品ID
	 *
	 * 版本 >= 0
	 */
	 private String itemId = new String();

	/**
	 * 购买数量
	 *
	 * 版本 >= 0
	 */
	 private long num;

	/**
	 * 区名
	 *
	 * 版本 >= 0
	 */
	 private String section = new String();

	/**
	 * 区编码
	 *
	 * 版本 >= 0
	 */
	 private String sectionCode = new String();

	/**
	 * 服编码
	 *
	 * 版本 >= 0
	 */
	 private String serverCode = new String();

	/**
	 * 服名
	 *
	 * 版本 >= 0
	 */
	 private String server = new String();

	/**
	 * 网游帐号
	 *
	 * 版本 >= 0
	 */
	 private String account = new String();

	/**
	 * vb2ctag
	 *
	 * 版本 >= 0
	 */
	 private String vb2ctag = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUInt(buyerUin);
		bs.pushString(itemId);
		bs.pushUInt(num);
		bs.pushString(section);
		bs.pushString(sectionCode);
		bs.pushString(serverCode);
		bs.pushString(server);
		bs.pushString(account);
		bs.pushString(vb2ctag);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		buyerUin = bs.popUInt();
		itemId = bs.popString();
		num = bs.popUInt();
		section = bs.popString();
		sectionCode = bs.popString();
		serverCode = bs.popString();
		server = bs.popString();
		account = bs.popString();
		vb2ctag = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取买家UIN
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return buyerUin;
	}


	/**
	 * 设置买家UIN
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.buyerUin = value;
	}


	/**
	 * 获取商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return itemId value 类型为:String
	 * 
	 */
	public String getItemId()
	{
		return itemId;
	}


	/**
	 * 设置商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemId(String value)
	{
		this.itemId = value;
	}


	/**
	 * 获取购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @return num value 类型为:long
	 * 
	 */
	public long getNum()
	{
		return num;
	}


	/**
	 * 设置购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNum(long value)
	{
		this.num = value;
	}


	/**
	 * 获取区名
	 * 
	 * 此字段的版本 >= 0
	 * @return section value 类型为:String
	 * 
	 */
	public String getSection()
	{
		return section;
	}


	/**
	 * 设置区名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSection(String value)
	{
		this.section = value;
	}


	/**
	 * 获取区编码
	 * 
	 * 此字段的版本 >= 0
	 * @return sectionCode value 类型为:String
	 * 
	 */
	public String getSectionCode()
	{
		return sectionCode;
	}


	/**
	 * 设置区编码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSectionCode(String value)
	{
		this.sectionCode = value;
	}


	/**
	 * 获取服编码
	 * 
	 * 此字段的版本 >= 0
	 * @return serverCode value 类型为:String
	 * 
	 */
	public String getServerCode()
	{
		return serverCode;
	}


	/**
	 * 设置服编码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setServerCode(String value)
	{
		this.serverCode = value;
	}


	/**
	 * 获取服名
	 * 
	 * 此字段的版本 >= 0
	 * @return server value 类型为:String
	 * 
	 */
	public String getServer()
	{
		return server;
	}


	/**
	 * 设置服名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setServer(String value)
	{
		this.server = value;
	}


	/**
	 * 获取网游帐号
	 * 
	 * 此字段的版本 >= 0
	 * @return account value 类型为:String
	 * 
	 */
	public String getAccount()
	{
		return account;
	}


	/**
	 * 设置网游帐号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAccount(String value)
	{
		this.account = value;
	}


	/**
	 * 获取vb2ctag
	 * 
	 * 此字段的版本 >= 0
	 * @return vb2ctag value 类型为:String
	 * 
	 */
	public String getVb2ctag()
	{
		return vb2ctag;
	}


	/**
	 * 设置vb2ctag
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setVb2ctag(String value)
	{
		this.vb2ctag = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GameOrderFilter)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段buyerUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(itemId);  //计算字段itemId的长度 size_of(String)
				length += 4;  //计算字段num的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(section);  //计算字段section的长度 size_of(String)
				length += ByteStream.getObjectSize(sectionCode);  //计算字段sectionCode的长度 size_of(String)
				length += ByteStream.getObjectSize(serverCode);  //计算字段serverCode的长度 size_of(String)
				length += ByteStream.getObjectSize(server);  //计算字段server的长度 size_of(String)
				length += ByteStream.getObjectSize(account);  //计算字段account的长度 size_of(String)
				length += ByteStream.getObjectSize(vb2ctag);  //计算字段vb2ctag的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
