 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.vb2c.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *resp
 *
 *@date 2012-06-01 10:03::40
 *
 *@since version:0
*/
public class  GetDealInfoResp implements IServiceObject
{
	public long result;
	/**
	 * 版本 >= 0
	 */
	 private DealInfo dealInfo = new DealInfo();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(dealInfo);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		dealInfo = (DealInfo) bs.popObject(DealInfo.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x70618802L;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dealInfo value 类型为:DealInfo
	 * 
	 */
	public DealInfo getDealInfo()
	{
		return dealInfo;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:DealInfo
	 * 
	 */
	public void setDealInfo(DealInfo value)
	{
		if (value != null) {
				this.dealInfo = value;
		}else{
				this.dealInfo = new DealInfo();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetDealInfoResp)
				length += ByteStream.getObjectSize(dealInfo);  //计算字段dealInfo的长度 size_of(DealInfo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
