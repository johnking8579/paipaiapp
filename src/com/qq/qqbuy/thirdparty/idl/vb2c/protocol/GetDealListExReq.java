 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.vb2c.WirelessApi.java

package com.qq.qqbuy.thirdparty.idl.vb2c.protocol;


import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *req
 *
 *@date 2012-12-06 07:39:59
 *
 *@since version:0
*/
public class  GetDealListExReq implements IServiceObject
{
	/**
	 * 来源
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 来源
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 查询条件
	 *
	 * 版本 >= 0
	 */
	 private GetDealListExReqBo req = new GetDealListExReqBo();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(source);
		bs.pushString(machineKey);
		bs.pushObject(req);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		source = bs.popString();
		machineKey = bs.popString();
		req = (GetDealListExReqBo) bs.popObject(GetDealListExReqBo.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x70611809L;
	}


	/**
	 * 获取来源
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取来源
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取查询条件
	 * 
	 * 此字段的版本 >= 0
	 * @return req value 类型为:GetDealListExReqBo
	 * 
	 */
	public GetDealListExReqBo getReq()
	{
		return req;
	}


	/**
	 * 设置查询条件
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:GetDealListExReqBo
	 * 
	 */
	public void setReq(GetDealListExReqBo value)
	{
		if (value != null) {
				this.req = value;
		}else{
				this.req = new GetDealListExReqBo();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetDealListExReq)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(machineKey);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(req);  //计算字段req的长度 size_of(GetDealListExReqBo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
