 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.vb2c.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *网游产品信息
 *
 *@date 2012-06-01 10:03::36
 *
 *@since version:0
*/
public class GameProductInfo  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20101115; 

	/**
	 * 产品ID
	 *
	 * 版本 >= 0
	 */
	 private String productId = new String();

	/**
	 * 产品名称
	 *
	 * 版本 >= 0
	 */
	 private String productName = new String();

	/**
	 * 产品图片
	 *
	 * 版本 >= 0
	 */
	 private String productPic = new String();

	/**
	 * 充值类型
	 *
	 * 版本 >= 0
	 */
	 private String chargeType = new String();

	/**
	 * 充值速度
	 *
	 * 版本 >= 0
	 */
	 private String chargeAce = new String();

	/**
	 * 可充值数量
	 *
	 * 版本 >= 0
	 */
	 private String chargeNum = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushString(productId);
		bs.pushString(productName);
		bs.pushString(productPic);
		bs.pushString(chargeType);
		bs.pushString(chargeAce);
		bs.pushString(chargeNum);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		productId = bs.popString();
		productName = bs.popString();
		productPic = bs.popString();
		chargeType = bs.popString();
		chargeAce = bs.popString();
		chargeNum = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取产品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return productId value 类型为:String
	 * 
	 */
	public String getProductId()
	{
		return productId;
	}


	/**
	 * 设置产品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setProductId(String value)
	{
		this.productId = value;
	}


	/**
	 * 获取产品名称
	 * 
	 * 此字段的版本 >= 0
	 * @return productName value 类型为:String
	 * 
	 */
	public String getProductName()
	{
		return productName;
	}


	/**
	 * 设置产品名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setProductName(String value)
	{
		this.productName = value;
	}


	/**
	 * 获取产品图片
	 * 
	 * 此字段的版本 >= 0
	 * @return productPic value 类型为:String
	 * 
	 */
	public String getProductPic()
	{
		return productPic;
	}


	/**
	 * 设置产品图片
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setProductPic(String value)
	{
		this.productPic = value;
	}


	/**
	 * 获取充值类型
	 * 
	 * 此字段的版本 >= 0
	 * @return chargeType value 类型为:String
	 * 
	 */
	public String getChargeType()
	{
		return chargeType;
	}


	/**
	 * 设置充值类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setChargeType(String value)
	{
		this.chargeType = value;
	}


	/**
	 * 获取充值速度
	 * 
	 * 此字段的版本 >= 0
	 * @return chargeAce value 类型为:String
	 * 
	 */
	public String getChargeAce()
	{
		return chargeAce;
	}


	/**
	 * 设置充值速度
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setChargeAce(String value)
	{
		this.chargeAce = value;
	}


	/**
	 * 获取可充值数量
	 * 
	 * 此字段的版本 >= 0
	 * @return chargeNum value 类型为:String
	 * 
	 */
	public String getChargeNum()
	{
		return chargeNum;
	}


	/**
	 * 设置可充值数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setChargeNum(String value)
	{
		this.chargeNum = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GameProductInfo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(productId);  //计算字段productId的长度 size_of(String)
				length += ByteStream.getObjectSize(productName);  //计算字段productName的长度 size_of(String)
				length += ByteStream.getObjectSize(productPic);  //计算字段productPic的长度 size_of(String)
				length += ByteStream.getObjectSize(chargeType);  //计算字段chargeType的长度 size_of(String)
				length += ByteStream.getObjectSize(chargeAce);  //计算字段chargeAce的长度 size_of(String)
				length += ByteStream.getObjectSize(chargeNum);  //计算字段chargeNum的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
