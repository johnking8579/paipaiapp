 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.vb2c.protocol;


import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *网游区服信息结构
 *
 *@date 2012-06-01 10:03::35
 *
 *@since version:0
*/
public class GameSectionServer  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20101115; 

	/**
	 * 区名
	 *
	 * 版本 >= 0
	 */
	 private String sectionName = new String();

	/**
	 * 区编码
	 *
	 * 版本 >= 0
	 */
	 private String sectionCode = new String();

	/**
	 * 服名及编码列表
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> serverList = new HashMap<String,String>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushString(sectionName);
		bs.pushString(sectionCode);
		bs.pushObject(serverList);
		return bs.getWrittenLength();
	}
	
	@SuppressWarnings("unchecked")
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		sectionName = bs.popString();
		sectionCode = bs.popString();
		serverList = (Map<String,String>)bs.popMap(String.class,String.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取区名
	 * 
	 * 此字段的版本 >= 0
	 * @return sectionName value 类型为:String
	 * 
	 */
	public String getSectionName()
	{
		return sectionName;
	}


	/**
	 * 设置区名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSectionName(String value)
	{
		this.sectionName = value;
	}


	/**
	 * 获取区编码
	 * 
	 * 此字段的版本 >= 0
	 * @return sectionCode value 类型为:String
	 * 
	 */
	public String getSectionCode()
	{
		return sectionCode;
	}


	/**
	 * 设置区编码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSectionCode(String value)
	{
		this.sectionCode = value;
	}


	/**
	 * 获取服名及编码列表
	 * 
	 * 此字段的版本 >= 0
	 * @return serverList value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getServerList()
	{
		return serverList;
	}


	/**
	 * 设置服名及编码列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setServerList(Map<String,String> value)
	{
		if (value != null) {
				this.serverList = value;
		}else{
				this.serverList = new HashMap<String,String>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GameSectionServer)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sectionName);  //计算字段sectionName的长度 size_of(String)
				length += ByteStream.getObjectSize(sectionCode);  //计算字段sectionCode的长度 size_of(String)
				length += ByteStream.getObjectSize(serverList);  //计算字段serverList的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
