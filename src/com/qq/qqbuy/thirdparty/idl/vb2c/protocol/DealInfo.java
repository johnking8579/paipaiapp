 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.vb2c.protocol;


import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *订单信息
 *
 *@date 2012-06-01 10:03::36
 *
 *@since version:0
*/
public class DealInfo  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20101108; 

	/**
	 * 订单ID
	 *
	 * 版本 >= 0
	 */
	 private String dealId = new String();

	/**
	 * 订单类型，0-手机；1-网游；3-农场道具
	 *
	 * 版本 >= 0
	 */
	 private long dealType;

	/**
	 * 商品名称
	 *
	 * 版本 >= 0
	 */
	 private String itemName = new String();

	/**
	 * 订单金额
	 *
	 * 版本 >= 0
	 */
	 private long payFee;

	/**
	 * 购买数量
	 *
	 * 版本 >= 0
	 */
	 private long num;

	/**
	 * 下单时间
	 *
	 * 版本 >= 0
	 */
	 private long dealGenTime;

	/**
	 * 订单状态，1-等待买家付款 ；2-等待发货； 3-发货中；4-退款中；5-交易成功；6-交易完成，已退款； 7-交易取消
	 *
	 * 版本 >= 0
	 */
	 private long dealState;

	/**
	 * 订单状态描述
	 *
	 * 版本 >= 0
	 */
	 private String dealStateDesc = new String();

	/**
	 * 供应商名称
	 *
	 * 版本 >= 0
	 */
	 private String sellerName = new String();

	/**
	 * 供应商SPID
	 *
	 * 版本 >= 0
	 */
	 private long sellerSpid;

	/**
	 * 客服电话
	 *
	 * 版本 >= 0
	 */
	 private String svrPhone = new String();

	/**
	 * 扩展信息，跟据订单类型不同，有不同的字段
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> extInfo = new HashMap<String,String>();

	/**
	 * FLAG字段
	 *
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 版本 >= 0
	 */
	 private short dealId_u;

	/**
	 * 版本 >= 0
	 */
	 private short dealType_u;

	/**
	 * 版本 >= 0
	 */
	 private short itemName_u;

	/**
	 * 版本 >= 0
	 */
	 private short payFee_u;

	/**
	 * 版本 >= 0
	 */
	 private short num_u;

	/**
	 * 版本 >= 0
	 */
	 private short dealGenTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short dealState_u;

	/**
	 * 版本 >= 0
	 */
	 private short dealStateDesc_u;

	/**
	 * 版本 >= 0
	 */
	 private short sellerName_u;

	/**
	 * 版本 >= 0
	 */
	 private short sellerSpid_u;

	/**
	 * 版本 >= 0
	 */
	 private short svrPhone_u;

	/**
	 * 版本 >= 0
	 */
	 private short extInfo_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushString(dealId);
		bs.pushUInt(dealType);
		bs.pushString(itemName);
		bs.pushUInt(payFee);
		bs.pushUInt(num);
		bs.pushUInt(dealGenTime);
		bs.pushUInt(dealState);
		bs.pushString(dealStateDesc);
		bs.pushString(sellerName);
		bs.pushUInt(sellerSpid);
		bs.pushString(svrPhone);
		bs.pushObject(extInfo);
		bs.pushUByte(version_u);
		bs.pushUByte(dealId_u);
		bs.pushUByte(dealType_u);
		bs.pushUByte(itemName_u);
		bs.pushUByte(payFee_u);
		bs.pushUByte(num_u);
		bs.pushUByte(dealGenTime_u);
		bs.pushUByte(dealState_u);
		bs.pushUByte(dealStateDesc_u);
		bs.pushUByte(sellerName_u);
		bs.pushUByte(sellerSpid_u);
		bs.pushUByte(svrPhone_u);
		bs.pushUByte(extInfo_u);
		return bs.getWrittenLength();
	}
	
	@SuppressWarnings("unchecked")
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		dealId = bs.popString();
		dealType = bs.popUInt();
		itemName = bs.popString();
		payFee = bs.popUInt();
		num = bs.popUInt();
		dealGenTime = bs.popUInt();
		dealState = bs.popUInt();
		dealStateDesc = bs.popString();
		sellerName = bs.popString();
		sellerSpid = bs.popUInt();
		svrPhone = bs.popString();
		extInfo = (Map<String,String>)bs.popMap(String.class,String.class);
		version_u = bs.popUByte();
		dealId_u = bs.popUByte();
		dealType_u = bs.popUByte();
		itemName_u = bs.popUByte();
		payFee_u = bs.popUByte();
		num_u = bs.popUByte();
		dealGenTime_u = bs.popUByte();
		dealState_u = bs.popUByte();
		dealStateDesc_u = bs.popUByte();
		sellerName_u = bs.popUByte();
		sellerSpid_u = bs.popUByte();
		svrPhone_u = bs.popUByte();
		extInfo_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取订单ID
	 * 
	 * 此字段的版本 >= 0
	 * @return dealId value 类型为:String
	 * 
	 */
	public String getDealId()
	{
		return dealId;
	}


	/**
	 * 设置订单ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealId(String value)
	{
		this.dealId = value;
		this.dealId_u = 1;
	}


	/**
	 * 获取订单类型，0-手机；1-网游；3-农场道具
	 * 
	 * 此字段的版本 >= 0
	 * @return dealType value 类型为:long
	 * 
	 */
	public long getDealType()
	{
		return dealType;
	}


	/**
	 * 设置订单类型，0-手机；1-网游；3-农场道具
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealType(long value)
	{
		this.dealType = value;
		this.dealType_u = 1;
	}


	/**
	 * 获取商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @return itemName value 类型为:String
	 * 
	 */
	public String getItemName()
	{
		return itemName;
	}


	/**
	 * 设置商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemName(String value)
	{
		this.itemName = value;
		this.itemName_u = 1;
	}


	/**
	 * 获取订单金额
	 * 
	 * 此字段的版本 >= 0
	 * @return payFee value 类型为:long
	 * 
	 */
	public long getPayFee()
	{
		return payFee;
	}


	/**
	 * 设置订单金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayFee(long value)
	{
		this.payFee = value;
		this.payFee_u = 1;
	}


	/**
	 * 获取购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @return num value 类型为:long
	 * 
	 */
	public long getNum()
	{
		return num;
	}


	/**
	 * 设置购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNum(long value)
	{
		this.num = value;
		this.num_u = 1;
	}


	/**
	 * 获取下单时间
	 * 
	 * 此字段的版本 >= 0
	 * @return dealGenTime value 类型为:long
	 * 
	 */
	public long getDealGenTime()
	{
		return dealGenTime;
	}


	/**
	 * 设置下单时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealGenTime(long value)
	{
		this.dealGenTime = value;
		this.dealGenTime_u = 1;
	}


	/**
	 * 获取订单状态，1-等待买家付款 ；2-等待发货； 3-发货中；4-退款中；5-交易成功；6-交易完成，已退款； 7-交易取消
	 * 
	 * 此字段的版本 >= 0
	 * @return dealState value 类型为:long
	 * 
	 */
	public long getDealState()
	{
		return dealState;
	}


	/**
	 * 设置订单状态，1-等待买家付款 ；2-等待发货； 3-发货中；4-退款中；5-交易成功；6-交易完成，已退款； 7-交易取消
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealState(long value)
	{
		this.dealState = value;
		this.dealState_u = 1;
	}


	/**
	 * 获取订单状态描述
	 * 
	 * 此字段的版本 >= 0
	 * @return dealStateDesc value 类型为:String
	 * 
	 */
	public String getDealStateDesc()
	{
		return dealStateDesc;
	}


	/**
	 * 设置订单状态描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealStateDesc(String value)
	{
		this.dealStateDesc = value;
		this.dealStateDesc_u = 1;
	}


	/**
	 * 获取供应商名称
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerName value 类型为:String
	 * 
	 */
	public String getSellerName()
	{
		return sellerName;
	}


	/**
	 * 设置供应商名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSellerName(String value)
	{
		this.sellerName = value;
		this.sellerName_u = 1;
	}


	/**
	 * 获取供应商SPID
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerSpid value 类型为:long
	 * 
	 */
	public long getSellerSpid()
	{
		return sellerSpid;
	}


	/**
	 * 设置供应商SPID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerSpid(long value)
	{
		this.sellerSpid = value;
		this.sellerSpid_u = 1;
	}


	/**
	 * 获取客服电话
	 * 
	 * 此字段的版本 >= 0
	 * @return svrPhone value 类型为:String
	 * 
	 */
	public String getSvrPhone()
	{
		return svrPhone;
	}


	/**
	 * 设置客服电话
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSvrPhone(String value)
	{
		this.svrPhone = value;
		this.svrPhone_u = 1;
	}


	/**
	 * 获取扩展信息，跟据订单类型不同，有不同的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return extInfo value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getExtInfo()
	{
		return extInfo;
	}


	/**
	 * 设置扩展信息，跟据订单类型不同，有不同的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setExtInfo(Map<String,String> value)
	{
		if (value != null) {
				this.extInfo = value;
				this.extInfo_u = 1;
		}
	}


	/**
	 * 获取FLAG字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置FLAG字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dealId_u value 类型为:short
	 * 
	 */
	public short getDealId_u()
	{
		return dealId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealId_u(short value)
	{
		this.dealId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dealType_u value 类型为:short
	 * 
	 */
	public short getDealType_u()
	{
		return dealType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealType_u(short value)
	{
		this.dealType_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return itemName_u value 类型为:short
	 * 
	 */
	public short getItemName_u()
	{
		return itemName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemName_u(short value)
	{
		this.itemName_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return payFee_u value 类型为:short
	 * 
	 */
	public short getPayFee_u()
	{
		return payFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPayFee_u(short value)
	{
		this.payFee_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return num_u value 类型为:short
	 * 
	 */
	public short getNum_u()
	{
		return num_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNum_u(short value)
	{
		this.num_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dealGenTime_u value 类型为:short
	 * 
	 */
	public short getDealGenTime_u()
	{
		return dealGenTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealGenTime_u(short value)
	{
		this.dealGenTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dealState_u value 类型为:short
	 * 
	 */
	public short getDealState_u()
	{
		return dealState_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealState_u(short value)
	{
		this.dealState_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dealStateDesc_u value 类型为:short
	 * 
	 */
	public short getDealStateDesc_u()
	{
		return dealStateDesc_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealStateDesc_u(short value)
	{
		this.dealStateDesc_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerName_u value 类型为:short
	 * 
	 */
	public short getSellerName_u()
	{
		return sellerName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerName_u(short value)
	{
		this.sellerName_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerSpid_u value 类型为:short
	 * 
	 */
	public short getSellerSpid_u()
	{
		return sellerSpid_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerSpid_u(short value)
	{
		this.sellerSpid_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return svrPhone_u value 类型为:short
	 * 
	 */
	public short getSvrPhone_u()
	{
		return svrPhone_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSvrPhone_u(short value)
	{
		this.svrPhone_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return extInfo_u value 类型为:short
	 * 
	 */
	public short getExtInfo_u()
	{
		return extInfo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setExtInfo_u(short value)
	{
		this.extInfo_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(DealInfo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(dealId);  //计算字段dealId的长度 size_of(String)
				length += 4;  //计算字段dealType的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(itemName);  //计算字段itemName的长度 size_of(String)
				length += 4;  //计算字段payFee的长度 size_of(uint32_t)
				length += 4;  //计算字段num的长度 size_of(uint32_t)
				length += 4;  //计算字段dealGenTime的长度 size_of(uint32_t)
				length += 4;  //计算字段dealState的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(dealStateDesc);  //计算字段dealStateDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(sellerName);  //计算字段sellerName的长度 size_of(String)
				length += 4;  //计算字段sellerSpid的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(svrPhone);  //计算字段svrPhone的长度 size_of(String)
				length += ByteStream.getObjectSize(extInfo);  //计算字段extInfo的长度 size_of(Map)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段dealId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段dealType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段itemName_u的长度 size_of(uint8_t)
				length += 1;  //计算字段payFee_u的长度 size_of(uint8_t)
				length += 1;  //计算字段num_u的长度 size_of(uint8_t)
				length += 1;  //计算字段dealGenTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段dealState_u的长度 size_of(uint8_t)
				length += 1;  //计算字段dealStateDesc_u的长度 size_of(uint8_t)
				length += 1;  //计算字段sellerName_u的长度 size_of(uint8_t)
				length += 1;  //计算字段sellerSpid_u的长度 size_of(uint8_t)
				length += 1;  //计算字段svrPhone_u的长度 size_of(uint8_t)
				length += 1;  //计算字段extInfo_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
