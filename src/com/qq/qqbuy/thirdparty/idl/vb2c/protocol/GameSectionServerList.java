 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.vb2c.protocol;


import java.util.Vector;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *网游区服信息结构列表
 *
 *@date 2012-06-01 10:03::35
 *
 *@since version:0
*/
public class GameSectionServerList  implements ICanSerializeObject
{
	/**
	 * 游戏名
	 *
	 * 版本 >= 0
	 */
	 private String gameName = new String();

	/**
	 * 区服列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<GameSectionServer> gssList = new Vector<GameSectionServer>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushString(gameName);
		bs.pushObject(gssList);
		return bs.getWrittenLength();
	}
	
	@SuppressWarnings("unchecked")
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		gameName = bs.popString();
		gssList = (Vector<GameSectionServer>)bs.popVector(GameSectionServer.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取游戏名
	 * 
	 * 此字段的版本 >= 0
	 * @return gameName value 类型为:String
	 * 
	 */
	public String getGameName()
	{
		return gameName;
	}


	/**
	 * 设置游戏名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setGameName(String value)
	{
		this.gameName = value;
	}


	/**
	 * 获取区服列表
	 * 
	 * 此字段的版本 >= 0
	 * @return gssList value 类型为:Vector<GameSectionServer>
	 * 
	 */
	public Vector<GameSectionServer> getGssList()
	{
		return gssList;
	}


	/**
	 * 设置区服列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<GameSectionServer>
	 * 
	 */
	public void setGssList(Vector<GameSectionServer> value)
	{
		if (value != null) {
				this.gssList = value;
		}else{
				this.gssList = new Vector<GameSectionServer>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GameSectionServerList)
				length += ByteStream.getObjectSize(gameName);  //计算字段gameName的长度 size_of(String)
				length += ByteStream.getObjectSize(gssList);  //计算字段gssList的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
