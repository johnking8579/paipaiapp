 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.vb2c.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *话费下单filter
 *
 *@date 2012-06-01 10:03::35
 *
 *@since version:0
*/
public class MobileOrderFilter  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20111026; 

	/**
	 * 买家UIN
	 *
	 * 版本 >= 0
	 */
	 private long buyerUin;

	/**
	 * 手机号码
	 *
	 * 版本 >= 0
	 */
	 private String mobile = new String();

	/**
	 * 面值
	 *
	 * 版本 >= 0
	 */
	 private long amount;

	/**
	 * vb2ctag
	 *
	 * 版本 >= 0
	 */
	 private String vb2ctag = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUInt(buyerUin);
		bs.pushString(mobile);
		bs.pushUInt(amount);
		bs.pushString(vb2ctag);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		buyerUin = bs.popUInt();
		mobile = bs.popString();
		amount = bs.popUInt();
		vb2ctag = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取买家UIN
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return buyerUin;
	}


	/**
	 * 设置买家UIN
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.buyerUin = value;
	}


	/**
	 * 获取手机号码
	 * 
	 * 此字段的版本 >= 0
	 * @return mobile value 类型为:String
	 * 
	 */
	public String getMobile()
	{
		return mobile;
	}


	/**
	 * 设置手机号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMobile(String value)
	{
		this.mobile = value;
	}


	/**
	 * 获取面值
	 * 
	 * 此字段的版本 >= 0
	 * @return amount value 类型为:long
	 * 
	 */
	public long getAmount()
	{
		return amount;
	}


	/**
	 * 设置面值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAmount(long value)
	{
		this.amount = value;
	}


	/**
	 * 获取vb2ctag
	 * 
	 * 此字段的版本 >= 0
	 * @return vb2ctag value 类型为:String
	 * 
	 */
	public String getVb2ctag()
	{
		return vb2ctag;
	}


	/**
	 * 设置vb2ctag
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setVb2ctag(String value)
	{
		this.vb2ctag = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(MobileOrderFilter)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段buyerUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(mobile);  //计算字段mobile的长度 size_of(String)
				length += 4;  //计算字段amount的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(vb2ctag);  //计算字段vb2ctag的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
