 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.vb2c.WirelessApi.java

package com.qq.qqbuy.thirdparty.idl.vb2c.protocol;


import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *resp
 *
 *@date 2012-12-06 07:39:59
 *
 *@since version:0
*/
public class  GetDealListExResp implements IServiceObject
{
	public long result;
	/**
	 * 查询结果
	 *
	 * 版本 >= 0
	 */
	 private GetDealListExRespBo resp = new GetDealListExRespBo();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(resp);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		resp = (GetDealListExRespBo) bs.popObject(GetDealListExRespBo.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x70618809L;
	}


	/**
	 * 获取查询结果
	 * 
	 * 此字段的版本 >= 0
	 * @return resp value 类型为:GetDealListExRespBo
	 * 
	 */
	public GetDealListExRespBo getResp()
	{
		return resp;
	}


	/**
	 * 设置查询结果
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:GetDealListExRespBo
	 * 
	 */
	public void setResp(GetDealListExRespBo value)
	{
		if (value != null) {
				this.resp = value;
		}else{
				this.resp = new GetDealListExRespBo();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetDealListExResp)
				length += ByteStream.getObjectSize(resp);  //计算字段resp的长度 size_of(GetDealListExRespBo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
