package com.qq.qqbuy.thirdparty.idl.shippingFeeV2;

import com.paipai.component.c2cplatform.IAsynWebStub;

import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.shippingFeeV2.protocol.GetTemplateDetailNoLoginReq;
import com.qq.qqbuy.thirdparty.idl.shippingFeeV2.protocol.GetTemplateDetailNoLoginResp;

public class ShippingfeeV2Client extends SupportIDLBaseClient {

	public static GetTemplateDetailNoLoginResp getShippingTemplateDetail(long uin, short innerId) {
		long start = System.currentTimeMillis();
		GetTemplateDetailNoLoginReq req = new GetTemplateDetailNoLoginReq();
		req.setUin(uin);
		req.setInnerId(innerId);
		
		GetTemplateDetailNoLoginResp resp = new GetTemplateDetailNoLoginResp();
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		int ret = invokePaiPaiIDL(req, resp, stub);
		
		long timeCost = System.currentTimeMillis() - start;
        return ret == SUCCESS ? resp : null;
	}
	
	public static void main(String args[]) {
		long uin = 1715146123;
		short innerId = 101;
		GetTemplateDetailNoLoginResp resp = getShippingTemplateDetail(uin, innerId);
		System.out.println(resp);
	}
}
