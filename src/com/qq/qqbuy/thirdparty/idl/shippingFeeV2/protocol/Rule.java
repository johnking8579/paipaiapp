//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.shippingFeeV2.TemplateDetailInfo.java

package com.qq.qqbuy.thirdparty.idl.shippingFeeV2.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.uint32_t;
import java.util.Vector;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 *
 *
 *@date 2013-08-22 11:20:23
 *
 *@since version:0
*/
public class Rule  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 类型
	 *
	 * 版本 >= 0
	 */
	 private short type;

	/**
	 * 目的地
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint32_t> vecDest = new Vector<uint32_t>();

	/**
	 * 属性
	 *
	 * 版本 >= 0
	 */
	 private long property;

	/**
	 * 平邮运费
	 *
	 * 版本 >= 0
	 */
	 private long priceNormal;

	/**
	 *  快递运费
	 *
	 * 版本 >= 0
	 */
	 private long priceExpress;

	/**
	 * ems运费
	 *
	 * 版本 >= 0
	 */
	 private long priceEms;

	/**
	 * 平邮运费增幅
	 *
	 * 版本 >= 0
	 */
	 private long priceNormalAdd;

	/**
	 * 快递运费增幅
	 *
	 * 版本 >= 0
	 */
	 private long priceExpressAdd;

	/**
	 * ems运费增幅
	 *
	 * 版本 >= 0
	 */
	 private long priceEmsAdd;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUByte(type);
		bs.pushObject(vecDest);
		bs.pushUInt(property);
		bs.pushUInt(priceNormal);
		bs.pushUInt(priceExpress);
		bs.pushUInt(priceEms);
		bs.pushUInt(priceNormalAdd);
		bs.pushUInt(priceExpressAdd);
		bs.pushUInt(priceEmsAdd);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		type = bs.popUByte();
		vecDest = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		property = bs.popUInt();
		priceNormal = bs.popUInt();
		priceExpress = bs.popUInt();
		priceEms = bs.popUInt();
		priceNormalAdd = bs.popUInt();
		priceExpressAdd = bs.popUInt();
		priceEmsAdd = bs.popUInt();
		return bs.getReadLength();
	} 


	/**
	 * 获取类型
	 * 
	 * 此字段的版本 >= 0
	 * @return type value 类型为:short
	 * 
	 */
	public short getType()
	{
		return type;
	}


	/**
	 * 设置类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setType(short value)
	{
		this.type = value;
	}


	/**
	 * 获取目的地
	 * 
	 * 此字段的版本 >= 0
	 * @return vecDest value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getVecDest()
	{
		return vecDest;
	}


	/**
	 * 设置目的地
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setVecDest(Vector<uint32_t> value)
	{
		if (value != null) {
				this.vecDest = value;
		}else{
				this.vecDest = new Vector<uint32_t>();
		}
	}


	/**
	 * 获取属性
	 * 
	 * 此字段的版本 >= 0
	 * @return property value 类型为:long
	 * 
	 */
	public long getProperty()
	{
		return property;
	}


	/**
	 * 设置属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setProperty(long value)
	{
		this.property = value;
	}


	/**
	 * 获取平邮运费
	 * 
	 * 此字段的版本 >= 0
	 * @return priceNormal value 类型为:long
	 * 
	 */
	public long getPriceNormal()
	{
		return priceNormal;
	}


	/**
	 * 设置平邮运费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPriceNormal(long value)
	{
		this.priceNormal = value;
	}


	/**
	 * 获取 快递运费
	 * 
	 * 此字段的版本 >= 0
	 * @return priceExpress value 类型为:long
	 * 
	 */
	public long getPriceExpress()
	{
		return priceExpress;
	}


	/**
	 * 设置 快递运费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPriceExpress(long value)
	{
		this.priceExpress = value;
	}


	/**
	 * 获取ems运费
	 * 
	 * 此字段的版本 >= 0
	 * @return priceEms value 类型为:long
	 * 
	 */
	public long getPriceEms()
	{
		return priceEms;
	}


	/**
	 * 设置ems运费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPriceEms(long value)
	{
		this.priceEms = value;
	}


	/**
	 * 获取平邮运费增幅
	 * 
	 * 此字段的版本 >= 0
	 * @return priceNormalAdd value 类型为:long
	 * 
	 */
	public long getPriceNormalAdd()
	{
		return priceNormalAdd;
	}


	/**
	 * 设置平邮运费增幅
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPriceNormalAdd(long value)
	{
		this.priceNormalAdd = value;
	}


	/**
	 * 获取快递运费增幅
	 * 
	 * 此字段的版本 >= 0
	 * @return priceExpressAdd value 类型为:long
	 * 
	 */
	public long getPriceExpressAdd()
	{
		return priceExpressAdd;
	}


	/**
	 * 设置快递运费增幅
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPriceExpressAdd(long value)
	{
		this.priceExpressAdd = value;
	}


	/**
	 * 获取ems运费增幅
	 * 
	 * 此字段的版本 >= 0
	 * @return priceEmsAdd value 类型为:long
	 * 
	 */
	public long getPriceEmsAdd()
	{
		return priceEmsAdd;
	}


	/**
	 * 设置ems运费增幅
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPriceEmsAdd(long value)
	{
		this.priceEmsAdd = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(Rule)
				length += 1;  //计算字段type的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(vecDest, null);  //计算字段vecDest的长度 size_of(Vector)
				length += 4;  //计算字段property的长度 size_of(uint32_t)
				length += 4;  //计算字段priceNormal的长度 size_of(uint32_t)
				length += 4;  //计算字段priceExpress的长度 size_of(uint32_t)
				length += 4;  //计算字段priceEms的长度 size_of(uint32_t)
				length += 4;  //计算字段priceNormalAdd的长度 size_of(uint32_t)
				length += 4;  //计算字段priceExpressAdd的长度 size_of(uint32_t)
				length += 4;  //计算字段priceEmsAdd的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(Rule)
				length += 1;  //计算字段type的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(vecDest, encoding);  //计算字段vecDest的长度 size_of(Vector)
				length += 4;  //计算字段property的长度 size_of(uint32_t)
				length += 4;  //计算字段priceNormal的长度 size_of(uint32_t)
				length += 4;  //计算字段priceExpress的长度 size_of(uint32_t)
				length += 4;  //计算字段priceEms的长度 size_of(uint32_t)
				length += 4;  //计算字段priceNormalAdd的长度 size_of(uint32_t)
				length += 4;  //计算字段priceExpressAdd的长度 size_of(uint32_t)
				length += 4;  //计算字段priceEmsAdd的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
