//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.shippingFeeV2.GetTemplateDetailNoLoginResp.java

package com.qq.qqbuy.thirdparty.idl.shippingFeeV2.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import java.util.Vector;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 *
 *
 *@date 2013-08-22 11:20:23
 *
 *@since version:0
*/
public class TemplateDetailInfo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本 >= 0
	 */
	 private TemplateSimpleInfo simple = new TemplateSimpleInfo();

	/**
	 * 版本 >= 0
	 */
	 private Vector<Rule> vecRule = new Vector<Rule>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(simple);
		bs.pushObject(vecRule);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		simple = (TemplateSimpleInfo) bs.popObject(TemplateSimpleInfo.class);
		vecRule = (Vector<Rule>)bs.popVector(Rule.class);
		return bs.getReadLength();
	} 


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return simple value 类型为:TemplateSimpleInfo
	 * 
	 */
	public TemplateSimpleInfo getSimple()
	{
		return simple;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:TemplateSimpleInfo
	 * 
	 */
	public void setSimple(TemplateSimpleInfo value)
	{
		if (value != null) {
				this.simple = value;
		}else{
				this.simple = new TemplateSimpleInfo();
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return vecRule value 类型为:Vector<Rule>
	 * 
	 */
	public Vector<Rule> getVecRule()
	{
		return vecRule;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<Rule>
	 * 
	 */
	public void setVecRule(Vector<Rule> value)
	{
		if (value != null) {
				this.vecRule = value;
		}else{
				this.vecRule = new Vector<Rule>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(TemplateDetailInfo)
				length += ByteStream.getObjectSize(simple, null);  //计算字段simple的长度 size_of(TemplateSimpleInfo)
				length += ByteStream.getObjectSize(vecRule, null);  //计算字段vecRule的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(TemplateDetailInfo)
				length += ByteStream.getObjectSize(simple, encoding);  //计算字段simple的长度 size_of(TemplateSimpleInfo)
				length += ByteStream.getObjectSize(vecRule, encoding);  //计算字段vecRule的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
