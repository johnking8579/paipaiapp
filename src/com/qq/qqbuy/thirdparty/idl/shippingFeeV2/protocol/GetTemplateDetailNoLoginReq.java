 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.shippingFeeV2.ShippingfeeService.java

package com.qq.qqbuy.thirdparty.idl.shippingFeeV2.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2013-08-22 11:20:23
 *
 *@since version:1
*/
public class  GetTemplateDetailNoLoginReq implements IServiceObject
{
	/**
	 * 属主uin
	 *
	 * 版本 >= 0
	 */
	 private long uin;

	/**
	 * 运费模板id
	 *
	 * 版本 >= 0
	 */
	 private short innerId;


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(uin);
		bs.pushUByte(innerId);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		uin = bs.popUInt();
		innerId = bs.popUByte();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x22101822L;
	}


	/**
	 * 获取属主uin
	 * 
	 * 此字段的版本 >= 0
	 * @return uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return uin;
	}


	/**
	 * 设置属主uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.uin = value;
	}


	/**
	 * 获取运费模板id
	 * 
	 * 此字段的版本 >= 0
	 * @return innerId value 类型为:short
	 * 
	 */
	public short getInnerId()
	{
		return innerId;
	}


	/**
	 * 设置运费模板id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setInnerId(short value)
	{
		this.innerId = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetTemplateDetailNoLoginReq)
				length += 4;  //计算字段uin的长度 size_of(uint32_t)
				length += 1;  //计算字段innerId的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetTemplateDetailNoLoginReq)
				length += 4;  //计算字段uin的长度 size_of(uint32_t)
				length += 1;  //计算字段innerId的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
