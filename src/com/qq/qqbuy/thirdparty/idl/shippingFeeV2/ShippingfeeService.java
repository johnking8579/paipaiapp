package com.qq.qqbuy.thirdparty.idl.shippingFeeV2;

import java.util.Vector;

import com.paipai.lang.uint32_t;
import com.paipai.lang.uint8_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;

/**
 * @author wamiwen
 * @Created 2013-8-19
 */
@HeadApiProtocol(cPlusNamespace = "c2cent::ao::shippingfee" )
public class ShippingfeeService {
	@Member(cPlusNamespace = "c2cent::bo::shippingfee", desc = "", isSetClassSize = false, version=0)
	class TemplateSimpleInfo {
		uint32_t id;
		uint32_t uin;
		uint8_t innerId;
		uint32_t property;
		String name;
		String desc;
		uint32_t createTime;
		uint32_t lastModifyTime;
	}
	@Member(cPlusNamespace = "c2cent::bo::shippingfee", desc = "", isSetClassSize = false, version=0)
    class Rule {
    	@Field(desc = "类型")
    	uint8_t type; 
        @Field(desc = "目的地")
        Vector<uint32_t> vecDest;
        @Field(desc = "属性")
        uint32_t property;
        @Field(desc = "平邮运费")
        uint32_t priceNormal;
        @Field(desc = " 快递运费")
        uint32_t priceExpress;
        @Field(desc = "ems运费")
        uint32_t priceEms;
        @Field(desc = "平邮运费增幅")
        uint32_t priceNormalAdd;
        @Field(desc = "快递运费增幅")
        uint32_t priceExpressAdd;
        @Field(desc = "ems运费增幅")
        uint32_t priceEmsAdd;
    }
	
	@Member(cPlusNamespace = "c2cent::bo::shippingfee", desc = "", isSetClassSize = false, version=0)
	class TemplateDetailInfo {
		TemplateSimpleInfo simple;
		Vector<Rule> vecRule;
	}
	
	@ApiProtocol(cmdid = "0x22101822L", desc = "免登录获取运费模板详细信息")
    class GetTemplateDetailNoLogin {
        class Req {
            @Field(desc = "属主uin")
            uint32_t uin;
            @Field(desc = "运费模板id")
            uint8_t innerId;
        }

        class Resp {
        	@Field(desc = "运费模板详情")
            TemplateDetailInfo detailInfo;
        }
    }
}
