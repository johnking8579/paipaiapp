package com.qq.qqbuy.thirdparty.idl;

import java.net.InetSocketAddress;

import com.paipai.component.c2cplatform.AsynWebStubException;
import com.paipai.component.c2cplatform.IAsynWebStub;
import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.component.configcenter.api.app.SvcRoute;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.exception.ExternalInterfaceException;

public class SupportIDLBaseClient{

    protected static final int 
    		SUCCESS = 0,
    		ERRCODE_CALLIDL_FAIL = 0x0100;
    
    protected static final String 
		    CALL_IDL_SOURCE = "mqgo",
    		GAMMA_IP = "172.23.0.58",
		    GAMMA_IP_NEW = "10.189.54.163",
		    DEV_IP = "172.25.32.43",
		    CHARSET_GBK = "GBK";
 
    protected static int invokePaiPaiIDL(IServiceObject req, IServiceObject resp,
            IAsynWebStub stub, String ip, int port)
    {
        stub.setIpAndPort(ip, port);
        return invokePaiPaiIDL(req,resp,stub);
    }
    
    protected static int invokePaiPaiIDL(Object req, Object resp, IAsynWebStub stub)
    {
        try {
        	int ret = stub.invoke(req, resp);
        	return ret;
        } catch (Exception e) {
        	Log.run.warn("e",e);
        	return ERRCODE_CALLIDL_FAIL;
        }
    }
    
    /**
     * 封装IDL调用产生异常. 如果ret != 0即抛出ExternalInterfaceException. 如果需要忽略ret值,不要调用此方法
     * 由于response的errMsg在子类中, 调用方仍然需要手动处理response的错误码, 以决定是否在ExternalInterfaceException中包含errMsg
     * @param stub
     * @param req  IServiceObject 或者 NetMessage
     * @param resp IServiceObject 或者 NetMessage
     */
    public static void invoke(IAsynWebStub stub, Object req, Object resp)	{
	    try {
			int ret = stub.invoke(req, resp);
			if(ret != 0)	{
				throw new ExternalInterfaceException(ret, 0);
			}
		} catch (AsynWebStubException e) {
			throw new ExternalInterfaceException(e);
		}
    }
    
    protected static String getDefaultMachineKey(String biz)
    {
        return "qgo_" + biz + "_" + System.currentTimeMillis();
    }
    
    protected static InetSocketAddress getAddressFromConfig(String key){
    	InetSocketAddress address = SvcRoute.getSvcAddress(key, (int)Math.random()*1000);
    	return address;		
    }
}