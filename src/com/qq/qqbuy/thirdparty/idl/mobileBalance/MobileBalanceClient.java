package com.qq.qqbuy.thirdparty.idl.mobileBalance;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.qq.qqbuy.common.Log;

import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.mobileBalance.protocol.QueryBalanceReq;
import com.qq.qqbuy.thirdparty.idl.mobileBalance.protocol.QueryBalanceResp;
import com.qq.qqbuy.thirdparty.idl.mobileBalance.protocol.QueryMobilebalanceAoRequest;

public class MobileBalanceClient extends SupportIDLBaseClient {
	
	protected static final String MOBILE_BALANCE_CALL_IDL_SOURCE = "mobileLife";
	
	public static QueryBalanceResp queryMobileBalance(String mobileNo, long uin) {
		QueryMobilebalanceAoRequest aoReq = new QueryMobilebalanceAoRequest();
		aoReq.setMobile(mobileNo);
		
		QueryBalanceResp resp = new QueryBalanceResp();
		QueryBalanceReq req = new QueryBalanceReq();
		req.setQueryMobilebalanceRequest(aoReq);
		req.setSource(MOBILE_BALANCE_CALL_IDL_SOURCE);
		
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		stub.setOperator(uin);
		stub.setUin(uin);
		stub.setTimeout(10000, 10000);
//		stub.setSkey("@gHGAJDtVw".getBytes());
//		stub.setIpAndPort("10.130.26.108", 53101);
		int ret = -1;
		try {
			ret = stub.invoke(req, resp);
		} catch (Exception e) {
		}
        return ret == SUCCESS ? resp : null;
	}
}
