 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.vb2c.mobilebalance.ao.idl.MobileBalanceAo.java

package com.qq.qqbuy.thirdparty.idl.mobileBalance.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.netframework.kernal.NetMessage;

/**
 *query the balance Req
 *
 *@date 2013-04-23 06:28:07
 *
 *@since version:0
*/
public class  QueryBalanceReq extends NetMessage
{
	/**
	 * source
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * req
	 *
	 * 版本 >= 0
	 */
	 private QueryMobilebalanceAoRequest queryMobilebalanceRequest = new QueryMobilebalanceAoRequest();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(source);
		bs.pushObject(queryMobilebalanceRequest);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		source = bs.popString();
		queryMobilebalanceRequest = (QueryMobilebalanceAoRequest) bs.popObject(QueryMobilebalanceAoRequest.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x70AA1801L;
	}


	/**
	 * 获取source
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置source
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取req
	 * 
	 * 此字段的版本 >= 0
	 * @return queryMobilebalanceRequest value 类型为:QueryMobilebalanceAoRequest
	 * 
	 */
	public QueryMobilebalanceAoRequest getQueryMobilebalanceRequest()
	{
		return queryMobilebalanceRequest;
	}


	/**
	 * 设置req
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:QueryMobilebalanceAoRequest
	 * 
	 */
	public void setQueryMobilebalanceRequest(QueryMobilebalanceAoRequest value)
	{
		if (value != null) {
				this.queryMobilebalanceRequest = value;
		}else{
				this.queryMobilebalanceRequest = new QueryMobilebalanceAoRequest();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(QueryBalanceReq)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(queryMobilebalanceRequest);  //计算字段queryMobilebalanceRequest的长度 size_of(QueryMobilebalanceAoRequest)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
