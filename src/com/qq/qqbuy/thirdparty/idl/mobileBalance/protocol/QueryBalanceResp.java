 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.vb2c.mobilebalance.ao.idl.MobileBalanceAo.java

package com.qq.qqbuy.thirdparty.idl.mobileBalance.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.netframework.kernal.NetMessage;

/**
 *query the balance Resp
 *
 *@date 2013-04-23 06:28:07
 *
 *@since version:0
*/
public class  QueryBalanceResp extends NetMessage
{
	/**
	 * resp
	 *
	 * 版本 >= 0
	 */
	 private QueryMobilebalanceAoRespone queryMobilebalanceRespone = new QueryMobilebalanceAoRespone();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(queryMobilebalanceRespone);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		queryMobilebalanceRespone = (QueryMobilebalanceAoRespone) bs.popObject(QueryMobilebalanceAoRespone.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x70AA8801L;
	}


	/**
	 * 获取resp
	 * 
	 * 此字段的版本 >= 0
	 * @return queryMobilebalanceRespone value 类型为:QueryMobilebalanceAoRespone
	 * 
	 */
	public QueryMobilebalanceAoRespone getQueryMobilebalanceRespone()
	{
		return queryMobilebalanceRespone;
	}


	/**
	 * 设置resp
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:QueryMobilebalanceAoRespone
	 * 
	 */
	public void setQueryMobilebalanceRespone(QueryMobilebalanceAoRespone value)
	{
		if (value != null) {
				this.queryMobilebalanceRespone = value;
		}else{
				this.queryMobilebalanceRespone = new QueryMobilebalanceAoRespone();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(QueryBalanceResp)
				length += ByteStream.getObjectSize(queryMobilebalanceRespone);  //计算字段queryMobilebalanceRespone的长度 size_of(QueryMobilebalanceAoRespone)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
