 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.promote.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import java.util.Vector;

/**
 *订单维度优惠po
 *
 *@date 2013-09-12 07:21::36
 *
 *@since version:1
*/
public class TradeFavor  implements ICanSerializeObject
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private int version = 4; 

	/**
	 * 订单ID
	 *
	 * 版本 >= 0
	 */
	 private long DealId;

	/**
	 * 卖家号码
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * 订单批价价格
	 *
	 * 版本 >= 0
	 */
	 private long TradeFee;

	/**
	 * 订单批价的总优惠
	 *
	 * 版本 >= 0
	 */
	 private long FavorFee;

	/**
	 * 订单批价错误类型
	 *
	 * 版本 >= 0
	 */
	 private long ErrType;

	/**
	 * 订单批价错误信息
	 *
	 * 版本 >= 0
	 */
	 private String ErrMsg = new String();

	/**
	 * 包邮标识
	 *
	 * 版本 >= 0
	 */
	 private long PostStat;

	/**
	 * 商品维度优惠集合
	 *
	 * 版本 >= 0
	 */
	 private Vector<ItemPromoteFavor> ItemPromoteFavor = new Vector<ItemPromoteFavor>();

	/**
	 * 店铺维度优惠集合
	 *
	 * 版本 >= 0
	 */
	 private Vector<DealPromoteFavor> DealPromoteFavor = new Vector<DealPromoteFavor>();

	/**
	 * 商品维度批价结果
	 *
	 * 版本 >= 0
	 */
	 private Vector<ItemPromoteResult> ItemPromoteResultList = new Vector<ItemPromoteResult>();

	/**
	 * 订单维度批价结果
	 *
	 * 版本 >= 0
	 */
	 private Vector<DealPromoteResult> DealPromoteResultList = new Vector<DealPromoteResult>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUShort(version);
		bs.pushLong(DealId);
		bs.pushUInt(SellerUin);
		bs.pushUInt(TradeFee);
		bs.pushUInt(FavorFee);
		bs.pushUInt(ErrType);
		bs.pushString(ErrMsg);
		bs.pushUInt(PostStat);
		bs.pushObject(ItemPromoteFavor);
		bs.pushObject(DealPromoteFavor);
		bs.pushObject(ItemPromoteResultList);
		bs.pushObject(DealPromoteResultList);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUShort();
		DealId = bs.popLong();
		SellerUin = bs.popUInt();
		TradeFee = bs.popUInt();
		FavorFee = bs.popUInt();
		ErrType = bs.popUInt();
		ErrMsg = bs.popString();
		PostStat = bs.popUInt();
		ItemPromoteFavor = (Vector<ItemPromoteFavor>)bs.popVector(ItemPromoteFavor.class);
		DealPromoteFavor = (Vector<DealPromoteFavor>)bs.popVector(DealPromoteFavor.class);
		ItemPromoteResultList = (Vector<ItemPromoteResult>)bs.popVector(ItemPromoteResult.class);
		DealPromoteResultList = (Vector<DealPromoteResult>)bs.popVector(DealPromoteResult.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取订单ID
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:long
	 * 
	 */
	public long getDealId()
	{
		return DealId;
	}


	/**
	 * 设置订单ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealId(long value)
	{
		this.DealId = value;
	}


	/**
	 * 获取卖家号码
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置卖家号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
	}


	/**
	 * 获取订单批价价格
	 * 
	 * 此字段的版本 >= 0
	 * @return TradeFee value 类型为:long
	 * 
	 */
	public long getTradeFee()
	{
		return TradeFee;
	}


	/**
	 * 设置订单批价价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTradeFee(long value)
	{
		this.TradeFee = value;
	}


	/**
	 * 获取订单批价的总优惠
	 * 
	 * 此字段的版本 >= 0
	 * @return FavorFee value 类型为:long
	 * 
	 */
	public long getFavorFee()
	{
		return FavorFee;
	}


	/**
	 * 设置订单批价的总优惠
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFavorFee(long value)
	{
		this.FavorFee = value;
	}


	/**
	 * 获取订单批价错误类型
	 * 
	 * 此字段的版本 >= 0
	 * @return ErrType value 类型为:long
	 * 
	 */
	public long getErrType()
	{
		return ErrType;
	}


	/**
	 * 设置订单批价错误类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setErrType(long value)
	{
		this.ErrType = value;
	}


	/**
	 * 获取订单批价错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return ErrMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return ErrMsg;
	}


	/**
	 * 设置订单批价错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		if (value != null) {
				this.ErrMsg = value;
		}else{
				this.ErrMsg = new String();
		}
	}


	/**
	 * 获取包邮标识
	 * 
	 * 此字段的版本 >= 0
	 * @return PostStat value 类型为:long
	 * 
	 */
	public long getPostStat()
	{
		return PostStat;
	}


	/**
	 * 设置包邮标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPostStat(long value)
	{
		this.PostStat = value;
	}


	/**
	 * 获取商品维度优惠集合
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemPromoteFavor value 类型为:Vector<ItemPromoteFavor>
	 * 
	 */
	public Vector<ItemPromoteFavor> getItemPromoteFavor()
	{
		return ItemPromoteFavor;
	}


	/**
	 * 设置商品维度优惠集合
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<ItemPromoteFavor>
	 * 
	 */
	public void setItemPromoteFavor(Vector<ItemPromoteFavor> value)
	{
		if (value != null) {
				this.ItemPromoteFavor = value;
		}else{
				this.ItemPromoteFavor = new Vector<ItemPromoteFavor>();
		}
	}


	/**
	 * 获取店铺维度优惠集合
	 * 
	 * 此字段的版本 >= 0
	 * @return DealPromoteFavor value 类型为:Vector<DealPromoteFavor>
	 * 
	 */
	public Vector<DealPromoteFavor> getDealPromoteFavor()
	{
		return DealPromoteFavor;
	}


	/**
	 * 设置店铺维度优惠集合
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<DealPromoteFavor>
	 * 
	 */
	public void setDealPromoteFavor(Vector<DealPromoteFavor> value)
	{
		if (value != null) {
				this.DealPromoteFavor = value;
		}else{
				this.DealPromoteFavor = new Vector<DealPromoteFavor>();
		}
	}


	/**
	 * 获取商品维度批价结果
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemPromoteResultList value 类型为:Vector<ItemPromoteResult>
	 * 
	 */
	public Vector<ItemPromoteResult> getItemPromoteResultList()
	{
		return ItemPromoteResultList;
	}


	/**
	 * 设置商品维度批价结果
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<ItemPromoteResult>
	 * 
	 */
	public void setItemPromoteResultList(Vector<ItemPromoteResult> value)
	{
		if (value != null) {
				this.ItemPromoteResultList = value;
		}else{
				this.ItemPromoteResultList = new Vector<ItemPromoteResult>();
		}
	}


	/**
	 * 获取订单维度批价结果
	 * 
	 * 此字段的版本 >= 0
	 * @return DealPromoteResultList value 类型为:Vector<DealPromoteResult>
	 * 
	 */
	public Vector<DealPromoteResult> getDealPromoteResultList()
	{
		return DealPromoteResultList;
	}


	/**
	 * 设置订单维度批价结果
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<DealPromoteResult>
	 * 
	 */
	public void setDealPromoteResultList(Vector<DealPromoteResult> value)
	{
		if (value != null) {
				this.DealPromoteResultList = value;
		}else{
				this.DealPromoteResultList = new Vector<DealPromoteResult>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(TradeFavor)
				length += 2;  //计算字段version的长度 size_of(uint16_t)
				length += 17;  //计算字段DealId的长度 size_of(uint64_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段TradeFee的长度 size_of(uint32_t)
				length += 4;  //计算字段FavorFee的长度 size_of(uint32_t)
				length += 4;  //计算字段ErrType的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ErrMsg);  //计算字段ErrMsg的长度 size_of(String)
				length += 4;  //计算字段PostStat的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ItemPromoteFavor);  //计算字段ItemPromoteFavor的长度 size_of(Vector)
				length += ByteStream.getObjectSize(DealPromoteFavor);  //计算字段DealPromoteFavor的长度 size_of(Vector)
				length += ByteStream.getObjectSize(ItemPromoteResultList);  //计算字段ItemPromoteResultList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(DealPromoteResultList);  //计算字段DealPromoteResultList的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
