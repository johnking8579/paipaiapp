 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.promote.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *批价流水po
 *
 *@date 2013-09-12 07:21::36
 *
 *@since version:1
*/
public class PromoteLog  implements ICanSerializeObject
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private int version = 4; 

	/**
	 * 优惠类型
	 *
	 * 版本 >= 0
	 */
	 private long Type;

	/**
	 * 优惠ID
	 *
	 * 版本 >= 0
	 */
	 private long FavorId;

	/**
	 * 优惠前价格
	 *
	 * 版本 >= 0
	 */
	 private long BeforePrice;

	/**
	 * 优惠后价格
	 *
	 * 版本 >= 0
	 */
	 private long AfterPrice;

	/**
	 * 优惠金额
	 *
	 * 版本 >= 0
	 */
	 private int FavorFee;

	/**
	 * 调价标识
	 *
	 * 版本 >= 0
	 */
	 private long ChangeStat;

	/**
	 * 优惠描述
	 *
	 * 版本 >= 0
	 */
	 private String FavorDesc = new String();

	/**
	 * 商品ID
	 *
	 * 版本 >= 0
	 */
	 private String ItemId = new String();

	/**
	 * 商品库存
	 *
	 * 版本 >= 0
	 */
	 private String Attr = new String();

	/**
	 * 商品促销价
	 *
	 * 版本 >= 0
	 */
	 private long PromotionPrice;

	/**
	 * 商品购买数量
	 *
	 * 版本 >= 0
	 */
	 private long Num;

	/**
	 * 商品skuid
	 *
	 * 版本 >= 0
	 */
	 private long Skuid;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUShort(version);
		bs.pushUInt(Type);
		bs.pushLong(FavorId);
		bs.pushUInt(BeforePrice);
		bs.pushUInt(AfterPrice);
		bs.pushInt(FavorFee);
		bs.pushUInt(ChangeStat);
		bs.pushString(FavorDesc);
		bs.pushString(ItemId);
		bs.pushString(Attr);
		bs.pushUInt(PromotionPrice);
		bs.pushUInt(Num);
		bs.pushLong(Skuid);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUShort();
		Type = bs.popUInt();
		FavorId = bs.popLong();
		BeforePrice = bs.popUInt();
		AfterPrice = bs.popUInt();
		FavorFee = bs.popInt();
		ChangeStat = bs.popUInt();
		FavorDesc = bs.popString();
		ItemId = bs.popString();
		Attr = bs.popString();
		PromotionPrice = bs.popUInt();
		Num = bs.popUInt();
		Skuid = bs.popLong();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取优惠类型
	 * 
	 * 此字段的版本 >= 0
	 * @return Type value 类型为:long
	 * 
	 */
	public long getType()
	{
		return Type;
	}


	/**
	 * 设置优惠类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setType(long value)
	{
		this.Type = value;
	}


	/**
	 * 获取优惠ID
	 * 
	 * 此字段的版本 >= 0
	 * @return FavorId value 类型为:long
	 * 
	 */
	public long getFavorId()
	{
		return FavorId;
	}


	/**
	 * 设置优惠ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFavorId(long value)
	{
		this.FavorId = value;
	}


	/**
	 * 获取优惠前价格
	 * 
	 * 此字段的版本 >= 0
	 * @return BeforePrice value 类型为:long
	 * 
	 */
	public long getBeforePrice()
	{
		return BeforePrice;
	}


	/**
	 * 设置优惠前价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBeforePrice(long value)
	{
		this.BeforePrice = value;
	}


	/**
	 * 获取优惠后价格
	 * 
	 * 此字段的版本 >= 0
	 * @return AfterPrice value 类型为:long
	 * 
	 */
	public long getAfterPrice()
	{
		return AfterPrice;
	}


	/**
	 * 设置优惠后价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAfterPrice(long value)
	{
		this.AfterPrice = value;
	}


	/**
	 * 获取优惠金额
	 * 
	 * 此字段的版本 >= 0
	 * @return FavorFee value 类型为:int
	 * 
	 */
	public int getFavorFee()
	{
		return FavorFee;
	}


	/**
	 * 设置优惠金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setFavorFee(int value)
	{
		this.FavorFee = value;
	}


	/**
	 * 获取调价标识
	 * 
	 * 此字段的版本 >= 0
	 * @return ChangeStat value 类型为:long
	 * 
	 */
	public long getChangeStat()
	{
		return ChangeStat;
	}


	/**
	 * 设置调价标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setChangeStat(long value)
	{
		this.ChangeStat = value;
	}


	/**
	 * 获取优惠描述
	 * 
	 * 此字段的版本 >= 0
	 * @return FavorDesc value 类型为:String
	 * 
	 */
	public String getFavorDesc()
	{
		return FavorDesc;
	}


	/**
	 * 设置优惠描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setFavorDesc(String value)
	{
		if (value != null) {
				this.FavorDesc = value;
		}else{
				this.FavorDesc = new String();
		}
	}


	/**
	 * 获取商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemId value 类型为:String
	 * 
	 */
	public String getItemId()
	{
		return ItemId;
	}


	/**
	 * 设置商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemId(String value)
	{
		if (value != null) {
				this.ItemId = value;
		}else{
				this.ItemId = new String();
		}
	}


	/**
	 * 获取商品库存
	 * 
	 * 此字段的版本 >= 0
	 * @return Attr value 类型为:String
	 * 
	 */
	public String getAttr()
	{
		return Attr;
	}


	/**
	 * 设置商品库存
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAttr(String value)
	{
		if (value != null) {
				this.Attr = value;
		}else{
				this.Attr = new String();
		}
	}


	/**
	 * 获取商品促销价
	 * 
	 * 此字段的版本 >= 0
	 * @return PromotionPrice value 类型为:long
	 * 
	 */
	public long getPromotionPrice()
	{
		return PromotionPrice;
	}


	/**
	 * 设置商品促销价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPromotionPrice(long value)
	{
		this.PromotionPrice = value;
	}


	/**
	 * 获取商品购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @return Num value 类型为:long
	 * 
	 */
	public long getNum()
	{
		return Num;
	}


	/**
	 * 设置商品购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNum(long value)
	{
		this.Num = value;
	}


	/**
	 * 获取商品skuid
	 * 
	 * 此字段的版本 >= 0
	 * @return Skuid value 类型为:long
	 * 
	 */
	public long getSkuid()
	{
		return Skuid;
	}


	/**
	 * 设置商品skuid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSkuid(long value)
	{
		this.Skuid = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(PromoteLog)
				length += 2;  //计算字段version的长度 size_of(uint16_t)
				length += 4;  //计算字段Type的长度 size_of(uint32_t)
				length += 17;  //计算字段FavorId的长度 size_of(uint64_t)
				length += 4;  //计算字段BeforePrice的长度 size_of(uint32_t)
				length += 4;  //计算字段AfterPrice的长度 size_of(uint32_t)
				length += 4;  //计算字段FavorFee的长度 size_of(int)
				length += 4;  //计算字段ChangeStat的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(FavorDesc);  //计算字段FavorDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(ItemId);  //计算字段ItemId的长度 size_of(String)
				length += ByteStream.getObjectSize(Attr);  //计算字段Attr的长度 size_of(String)
				length += 4;  //计算字段PromotionPrice的长度 size_of(uint32_t)
				length += 4;  //计算字段Num的长度 size_of(uint32_t)
				length += 17;  //计算字段Skuid的长度 size_of(uint64_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
