 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.promote.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import java.util.Vector;

/**
 *订单维度优惠结果po
 *
 *@date 2013-09-12 07:21::36
 *
 *@since version:1
*/
public class ItemPromoteResult  implements ICanSerializeObject
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private int version = 4; 

	/**
	 * 商品ID
	 *
	 * 版本 >= 0
	 */
	 private String ItemId = new String();

	/**
	 * 商品批价结果
	 *
	 * 版本 >= 0
	 */
	 private long Result;

	/**
	 * 商品库存属性
	 *
	 * 版本 >= 0
	 */
	 private String Attr = new String();

	/**
	 * 商品总价
	 *
	 * 版本 >= 0
	 */
	 private long TotalMoney;

	/**
	 * 商品总优惠
	 *
	 * 版本 >= 0
	 */
	 private long TotalFavorFee;

	/**
	 * 商品原价
	 *
	 * 版本 >= 0
	 */
	 private long Price;

	/**
	 * 商品促销价
	 *
	 * 版本 >= 0
	 */
	 private long PromotionPrice;

	/**
	 * 商品购买数量
	 *
	 * 版本 >= 0
	 */
	 private long Num;

	/**
	 * 商品单价优惠类型
	 *
	 * 版本 >= 0
	 */
	 private long FavorType;

	/**
	 * 商品单价优惠描述
	 *
	 * 版本 >= 0
	 */
	 private String FavorDesc = new String();

	/**
	 * 商品调价
	 *
	 * 版本 >= 0
	 */
	 private long AdjustFee;

	/**
	 * 商品支持红包的最大面值
	 *
	 * 版本 >= 0
	 */
	 private long AllowFee;

	/**
	 * 商品分摊总的优惠
	 *
	 * 版本 >= 0
	 */
	 private long DivideFee;

	/**
	 * 商品维度优惠列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<FavorEx> FavorExList = new Vector<FavorEx>();

	/**
	 * 商品维度批价流水
	 *
	 * 版本 >= 0
	 */
	 private Vector<PromoteLog> PromoteLogList = new Vector<PromoteLog>();

	/**
	 * 商品市场价
	 *
	 * 版本 >= 0
	 */
	 private long MarketPrice;

	/**
	 * 商品参加大促活动标识
	 *
	 * 版本 >= 0
	 */
	 private long ActiveFlag;

	/**
	 * 商品skuid
	 *
	 * 版本 >= 0
	 */
	 private long Skuid;

	/**
	 * 商品消费红包的总和
	 *
	 * 版本 >= 0
	 */
	 private long RedSum;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUShort(version);
		bs.pushString(ItemId);
		bs.pushUInt(Result);
		bs.pushString(Attr);
		bs.pushUInt(TotalMoney);
		bs.pushUInt(TotalFavorFee);
		bs.pushUInt(Price);
		bs.pushUInt(PromotionPrice);
		bs.pushUInt(Num);
		bs.pushUInt(FavorType);
		bs.pushString(FavorDesc);
		bs.pushUInt(AdjustFee);
		bs.pushUInt(AllowFee);
		bs.pushUInt(DivideFee);
		bs.pushObject(FavorExList);
		bs.pushObject(PromoteLogList);
		bs.pushUInt(MarketPrice);
		bs.pushUInt(ActiveFlag);
		bs.pushLong(Skuid);
		bs.pushUInt(RedSum);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUShort();
		ItemId = bs.popString();
		Result = bs.popUInt();
		Attr = bs.popString();
		TotalMoney = bs.popUInt();
		TotalFavorFee = bs.popUInt();
		Price = bs.popUInt();
		PromotionPrice = bs.popUInt();
		Num = bs.popUInt();
		FavorType = bs.popUInt();
		FavorDesc = bs.popString();
		AdjustFee = bs.popUInt();
		AllowFee = bs.popUInt();
		DivideFee = bs.popUInt();
		FavorExList = (Vector<FavorEx>)bs.popVector(FavorEx.class);
		PromoteLogList = (Vector<PromoteLog>)bs.popVector(PromoteLog.class);
		MarketPrice = bs.popUInt();
		ActiveFlag = bs.popUInt();
		Skuid = bs.popLong();
		RedSum = bs.popUInt();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemId value 类型为:String
	 * 
	 */
	public String getItemId()
	{
		return ItemId;
	}


	/**
	 * 设置商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemId(String value)
	{
		if (value != null) {
				this.ItemId = value;
		}else{
				this.ItemId = new String();
		}
	}


	/**
	 * 获取商品批价结果
	 * 
	 * 此字段的版本 >= 0
	 * @return Result value 类型为:long
	 * 
	 */
	public long getResult()
	{
		return Result;
	}


	/**
	 * 设置商品批价结果
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setResult(long value)
	{
		this.Result = value;
	}


	/**
	 * 获取商品库存属性
	 * 
	 * 此字段的版本 >= 0
	 * @return Attr value 类型为:String
	 * 
	 */
	public String getAttr()
	{
		return Attr;
	}


	/**
	 * 设置商品库存属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAttr(String value)
	{
		if (value != null) {
				this.Attr = value;
		}else{
				this.Attr = new String();
		}
	}


	/**
	 * 获取商品总价
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalMoney value 类型为:long
	 * 
	 */
	public long getTotalMoney()
	{
		return TotalMoney;
	}


	/**
	 * 设置商品总价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalMoney(long value)
	{
		this.TotalMoney = value;
	}


	/**
	 * 获取商品总优惠
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalFavorFee value 类型为:long
	 * 
	 */
	public long getTotalFavorFee()
	{
		return TotalFavorFee;
	}


	/**
	 * 设置商品总优惠
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalFavorFee(long value)
	{
		this.TotalFavorFee = value;
	}


	/**
	 * 获取商品原价
	 * 
	 * 此字段的版本 >= 0
	 * @return Price value 类型为:long
	 * 
	 */
	public long getPrice()
	{
		return Price;
	}


	/**
	 * 设置商品原价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPrice(long value)
	{
		this.Price = value;
	}


	/**
	 * 获取商品促销价
	 * 
	 * 此字段的版本 >= 0
	 * @return PromotionPrice value 类型为:long
	 * 
	 */
	public long getPromotionPrice()
	{
		return PromotionPrice;
	}


	/**
	 * 设置商品促销价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPromotionPrice(long value)
	{
		this.PromotionPrice = value;
	}


	/**
	 * 获取商品购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @return Num value 类型为:long
	 * 
	 */
	public long getNum()
	{
		return Num;
	}


	/**
	 * 设置商品购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNum(long value)
	{
		this.Num = value;
	}


	/**
	 * 获取商品单价优惠类型
	 * 
	 * 此字段的版本 >= 0
	 * @return FavorType value 类型为:long
	 * 
	 */
	public long getFavorType()
	{
		return FavorType;
	}


	/**
	 * 设置商品单价优惠类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFavorType(long value)
	{
		this.FavorType = value;
	}


	/**
	 * 获取商品单价优惠描述
	 * 
	 * 此字段的版本 >= 0
	 * @return FavorDesc value 类型为:String
	 * 
	 */
	public String getFavorDesc()
	{
		return FavorDesc;
	}


	/**
	 * 设置商品单价优惠描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setFavorDesc(String value)
	{
		if (value != null) {
				this.FavorDesc = value;
		}else{
				this.FavorDesc = new String();
		}
	}


	/**
	 * 获取商品调价
	 * 
	 * 此字段的版本 >= 0
	 * @return AdjustFee value 类型为:long
	 * 
	 */
	public long getAdjustFee()
	{
		return AdjustFee;
	}


	/**
	 * 设置商品调价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAdjustFee(long value)
	{
		this.AdjustFee = value;
	}


	/**
	 * 获取商品支持红包的最大面值
	 * 
	 * 此字段的版本 >= 0
	 * @return AllowFee value 类型为:long
	 * 
	 */
	public long getAllowFee()
	{
		return AllowFee;
	}


	/**
	 * 设置商品支持红包的最大面值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAllowFee(long value)
	{
		this.AllowFee = value;
	}


	/**
	 * 获取商品分摊总的优惠
	 * 
	 * 此字段的版本 >= 0
	 * @return DivideFee value 类型为:long
	 * 
	 */
	public long getDivideFee()
	{
		return DivideFee;
	}


	/**
	 * 设置商品分摊总的优惠
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDivideFee(long value)
	{
		this.DivideFee = value;
	}


	/**
	 * 获取商品维度优惠列表
	 * 
	 * 此字段的版本 >= 0
	 * @return FavorExList value 类型为:Vector<FavorEx>
	 * 
	 */
	public Vector<FavorEx> getFavorExList()
	{
		return FavorExList;
	}


	/**
	 * 设置商品维度优惠列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<FavorEx>
	 * 
	 */
	public void setFavorExList(Vector<FavorEx> value)
	{
		if (value != null) {
				this.FavorExList = value;
		}else{
				this.FavorExList = new Vector<FavorEx>();
		}
	}


	/**
	 * 获取商品维度批价流水
	 * 
	 * 此字段的版本 >= 0
	 * @return PromoteLogList value 类型为:Vector<PromoteLog>
	 * 
	 */
	public Vector<PromoteLog> getPromoteLogList()
	{
		return PromoteLogList;
	}


	/**
	 * 设置商品维度批价流水
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<PromoteLog>
	 * 
	 */
	public void setPromoteLogList(Vector<PromoteLog> value)
	{
		if (value != null) {
				this.PromoteLogList = value;
		}else{
				this.PromoteLogList = new Vector<PromoteLog>();
		}
	}


	/**
	 * 获取商品市场价
	 * 
	 * 此字段的版本 >= 0
	 * @return MarketPrice value 类型为:long
	 * 
	 */
	public long getMarketPrice()
	{
		return MarketPrice;
	}


	/**
	 * 设置商品市场价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMarketPrice(long value)
	{
		this.MarketPrice = value;
	}


	/**
	 * 获取商品参加大促活动标识
	 * 
	 * 此字段的版本 >= 0
	 * @return ActiveFlag value 类型为:long
	 * 
	 */
	public long getActiveFlag()
	{
		return ActiveFlag;
	}


	/**
	 * 设置商品参加大促活动标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActiveFlag(long value)
	{
		this.ActiveFlag = value;
	}


	/**
	 * 获取商品skuid
	 * 
	 * 此字段的版本 >= 0
	 * @return Skuid value 类型为:long
	 * 
	 */
	public long getSkuid()
	{
		return Skuid;
	}


	/**
	 * 设置商品skuid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSkuid(long value)
	{
		this.Skuid = value;
	}


	/**
	 * 获取商品消费红包的总和
	 * 
	 * 此字段的版本 >= 0
	 * @return RedSum value 类型为:long
	 * 
	 */
	public long getRedSum()
	{
		return RedSum;
	}


	/**
	 * 设置商品消费红包的总和
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRedSum(long value)
	{
		this.RedSum = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemPromoteResult)
				length += 2;  //计算字段version的长度 size_of(uint16_t)
				length += ByteStream.getObjectSize(ItemId);  //计算字段ItemId的长度 size_of(String)
				length += 4;  //计算字段Result的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(Attr);  //计算字段Attr的长度 size_of(String)
				length += 4;  //计算字段TotalMoney的长度 size_of(uint32_t)
				length += 4;  //计算字段TotalFavorFee的长度 size_of(uint32_t)
				length += 4;  //计算字段Price的长度 size_of(uint32_t)
				length += 4;  //计算字段PromotionPrice的长度 size_of(uint32_t)
				length += 4;  //计算字段Num的长度 size_of(uint32_t)
				length += 4;  //计算字段FavorType的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(FavorDesc);  //计算字段FavorDesc的长度 size_of(String)
				length += 4;  //计算字段AdjustFee的长度 size_of(uint32_t)
				length += 4;  //计算字段AllowFee的长度 size_of(uint32_t)
				length += 4;  //计算字段DivideFee的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(FavorExList);  //计算字段FavorExList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(PromoteLogList);  //计算字段PromoteLogList的长度 size_of(Vector)
				length += 4;  //计算字段MarketPrice的长度 size_of(uint32_t)
				length += 4;  //计算字段ActiveFlag的长度 size_of(uint32_t)
				length += 17;  //计算字段Skuid的长度 size_of(uint64_t)
				length += 4;  //计算字段RedSum的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
