 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.promote.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *促销系统订单批量批价接口 resp
 *
 *@date 2013-09-12 07:21::36
 *
 *@since version:0
*/
public class  DealPromoteSaleResp implements IServiceObject
{
	public long result;
	/**
	 * 订单批量批价结果
	 *
	 * 版本 >= 0
	 */
	 private DealFavor oDealFavor = new DealFavor();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(oDealFavor);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		oDealFavor = (DealFavor) bs.popObject(DealFavor.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x53708801L;
	}


	/**
	 * 获取订单批量批价结果
	 * 
	 * 此字段的版本 >= 0
	 * @return oDealFavor value 类型为:DealFavor
	 * 
	 */
	public DealFavor getODealFavor()
	{
		return oDealFavor;
	}


	/**
	 * 设置订单批量批价结果
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:DealFavor
	 * 
	 */
	public void setODealFavor(DealFavor value)
	{
		if (value != null) {
				this.oDealFavor = value;
		}else{
				this.oDealFavor = new DealFavor();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(DealPromoteSaleResp)
				length += ByteStream.getObjectSize(oDealFavor);  //计算字段oDealFavor的长度 size_of(DealFavor)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
