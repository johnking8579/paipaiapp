 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.promote.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *促销系统订单批量批价接口 req
 *
 *@date 2013-09-12 07:21::36
 *
 *@since version:0
*/
public class  DealPromoteSaleReq implements IServiceObject
{
	/**
	 * 订单批量批价接口请求
	 *
	 * 版本 >= 0
	 */
	 private PromoteFilter oPromoteFilter = new PromoteFilter();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(oPromoteFilter);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		oPromoteFilter = (PromoteFilter) bs.popObject(PromoteFilter.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x53701801L;
	}


	/**
	 * 获取订单批量批价接口请求
	 * 
	 * 此字段的版本 >= 0
	 * @return oPromoteFilter value 类型为:PromoteFilter
	 * 
	 */
	public PromoteFilter getOPromoteFilter()
	{
		return oPromoteFilter;
	}


	/**
	 * 设置订单批量批价接口请求
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:PromoteFilter
	 * 
	 */
	public void setOPromoteFilter(PromoteFilter value)
	{
		if (value != null) {
				this.oPromoteFilter = value;
		}else{
				this.oPromoteFilter = new PromoteFilter();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(DealPromoteSaleReq)
				length += ByteStream.getObjectSize(oPromoteFilter);  //计算字段oPromoteFilter的长度 size_of(PromoteFilter)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
