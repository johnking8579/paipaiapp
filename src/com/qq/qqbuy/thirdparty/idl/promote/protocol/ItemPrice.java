 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.promote.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *商品多价po
 *
 *@date 2013-09-12 07:21::36
 *
 *@since version:1
*/
public class ItemPrice  implements ICanSerializeObject
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private int version = 4; 

	/**
	 * 商品多价类型
	 *
	 * 版本 >= 0
	 */
	 private long Type;

	/**
	 * 多价类型等级
	 *
	 * 版本 >= 0
	 */
	 private long Level;

	/**
	 * 多价类型价格
	 *
	 * 版本 >= 0
	 */
	 private long PromotePrice;

	/**
	 * 类型名称
	 *
	 * 版本 >= 0
	 */
	 private String Name = new String();

	/**
	 * 类型描述
	 *
	 * 版本 >= 0
	 */
	 private String Desc = new String();

	/**
	 * 选中标识
	 *
	 * 版本 >= 0
	 */
	 private long CheckStat;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUShort(version);
		bs.pushUInt(Type);
		bs.pushUInt(Level);
		bs.pushUInt(PromotePrice);
		bs.pushString(Name);
		bs.pushString(Desc);
		bs.pushUInt(CheckStat);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUShort();
		Type = bs.popUInt();
		Level = bs.popUInt();
		PromotePrice = bs.popUInt();
		Name = bs.popString();
		Desc = bs.popString();
		CheckStat = bs.popUInt();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取商品多价类型
	 * 
	 * 此字段的版本 >= 0
	 * @return Type value 类型为:long
	 * 
	 */
	public long getType()
	{
		return Type;
	}


	/**
	 * 设置商品多价类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setType(long value)
	{
		this.Type = value;
	}


	/**
	 * 获取多价类型等级
	 * 
	 * 此字段的版本 >= 0
	 * @return Level value 类型为:long
	 * 
	 */
	public long getLevel()
	{
		return Level;
	}


	/**
	 * 设置多价类型等级
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLevel(long value)
	{
		this.Level = value;
	}


	/**
	 * 获取多价类型价格
	 * 
	 * 此字段的版本 >= 0
	 * @return PromotePrice value 类型为:long
	 * 
	 */
	public long getPromotePrice()
	{
		return PromotePrice;
	}


	/**
	 * 设置多价类型价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPromotePrice(long value)
	{
		this.PromotePrice = value;
	}


	/**
	 * 获取类型名称
	 * 
	 * 此字段的版本 >= 0
	 * @return Name value 类型为:String
	 * 
	 */
	public String getName()
	{
		return Name;
	}


	/**
	 * 设置类型名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setName(String value)
	{
		if (value != null) {
				this.Name = value;
		}else{
				this.Name = new String();
		}
	}


	/**
	 * 获取类型描述
	 * 
	 * 此字段的版本 >= 0
	 * @return Desc value 类型为:String
	 * 
	 */
	public String getDesc()
	{
		return Desc;
	}


	/**
	 * 设置类型描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDesc(String value)
	{
		if (value != null) {
				this.Desc = value;
		}else{
				this.Desc = new String();
		}
	}


	/**
	 * 获取选中标识
	 * 
	 * 此字段的版本 >= 0
	 * @return CheckStat value 类型为:long
	 * 
	 */
	public long getCheckStat()
	{
		return CheckStat;
	}


	/**
	 * 设置选中标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCheckStat(long value)
	{
		this.CheckStat = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemPrice)
				length += 2;  //计算字段version的长度 size_of(uint16_t)
				length += 4;  //计算字段Type的长度 size_of(uint32_t)
				length += 4;  //计算字段Level的长度 size_of(uint32_t)
				length += 4;  //计算字段PromotePrice的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(Name);  //计算字段Name的长度 size_of(String)
				length += ByteStream.getObjectSize(Desc);  //计算字段Desc的长度 size_of(String)
				length += 4;  //计算字段CheckStat的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
