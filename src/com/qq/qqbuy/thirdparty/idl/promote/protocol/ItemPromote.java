 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.promote.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import java.util.Map;
import java.util.Vector;
import java.util.HashMap;

/**
 *请求批价的商品po
 *
 *@date 2013-09-12 07:21::36
 *
 *@since version:1
*/
public class ItemPromote  implements ICanSerializeObject
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private int version = 4; 

	/**
	 * 商品ID
	 *
	 * 版本 >= 0
	 */
	 private String ItemId = new String();

	/**
	 * 商品数量
	 *
	 * 版本 >= 0
	 */
	 private long ItemCnt;

	/**
	 * 商品身份价请求
	 *
	 * 版本 >= 0
	 */
	 private long PriceType;

	/**
	 * 库存属性
	 *
	 * 版本 >= 0
	 */
	 private String Property = new String();

	/**
	 * 商品维度请求的优惠列表,如红包、身份等
	 *
	 * 版本 >= 0
	 */
	 private Vector<FavorFilter> FavorFilterList = new Vector<FavorFilter>();

	/**
	 * 商品skuid
	 *
	 * 版本 >= 0
	 */
	 private long Skuid;

	/**
	 * 商品请求优惠扩展字段
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> ExtInfo = new HashMap<String,String>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUShort(version);
		bs.pushString(ItemId);
		bs.pushUInt(ItemCnt);
		bs.pushUInt(PriceType);
		bs.pushString(Property);
		bs.pushObject(FavorFilterList);
		bs.pushLong(Skuid);
		bs.pushObject(ExtInfo);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUShort();
		ItemId = bs.popString();
		ItemCnt = bs.popUInt();
		PriceType = bs.popUInt();
		Property = bs.popString();
		FavorFilterList = (Vector<FavorFilter>)bs.popVector(FavorFilter.class);
		Skuid = bs.popLong();
		ExtInfo = (Map<String,String>)bs.popMap(String.class,String.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemId value 类型为:String
	 * 
	 */
	public String getItemId()
	{
		return ItemId;
	}


	/**
	 * 设置商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemId(String value)
	{
		if (value != null) {
				this.ItemId = value;
		}else{
				this.ItemId = new String();
		}
	}


	/**
	 * 获取商品数量
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemCnt value 类型为:long
	 * 
	 */
	public long getItemCnt()
	{
		return ItemCnt;
	}


	/**
	 * 设置商品数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setItemCnt(long value)
	{
		this.ItemCnt = value;
	}


	/**
	 * 获取商品身份价请求
	 * 
	 * 此字段的版本 >= 0
	 * @return PriceType value 类型为:long
	 * 
	 */
	public long getPriceType()
	{
		return PriceType;
	}


	/**
	 * 设置商品身份价请求
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPriceType(long value)
	{
		this.PriceType = value;
	}


	/**
	 * 获取库存属性
	 * 
	 * 此字段的版本 >= 0
	 * @return Property value 类型为:String
	 * 
	 */
	public String getProperty()
	{
		return Property;
	}


	/**
	 * 设置库存属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setProperty(String value)
	{
		if (value != null) {
				this.Property = value;
		}else{
				this.Property = new String();
		}
	}


	/**
	 * 获取商品维度请求的优惠列表,如红包、身份等
	 * 
	 * 此字段的版本 >= 0
	 * @return FavorFilterList value 类型为:Vector<FavorFilter>
	 * 
	 */
	public Vector<FavorFilter> getFavorFilterList()
	{
		return FavorFilterList;
	}


	/**
	 * 设置商品维度请求的优惠列表,如红包、身份等
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<FavorFilter>
	 * 
	 */
	public void setFavorFilterList(Vector<FavorFilter> value)
	{
		if (value != null) {
				this.FavorFilterList = value;
		}else{
				this.FavorFilterList = new Vector<FavorFilter>();
		}
	}


	/**
	 * 获取商品skuid
	 * 
	 * 此字段的版本 >= 0
	 * @return Skuid value 类型为:long
	 * 
	 */
	public long getSkuid()
	{
		return Skuid;
	}


	/**
	 * 设置商品skuid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSkuid(long value)
	{
		this.Skuid = value;
	}


	/**
	 * 获取商品请求优惠扩展字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ExtInfo value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getExtInfo()
	{
		return ExtInfo;
	}


	/**
	 * 设置商品请求优惠扩展字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setExtInfo(Map<String,String> value)
	{
		if (value != null) {
				this.ExtInfo = value;
		}else{
				this.ExtInfo = new HashMap<String,String>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemPromote)
				length += 2;  //计算字段version的长度 size_of(uint16_t)
				length += ByteStream.getObjectSize(ItemId);  //计算字段ItemId的长度 size_of(String)
				length += 4;  //计算字段ItemCnt的长度 size_of(uint32_t)
				length += 4;  //计算字段PriceType的长度 size_of(uint32_t)
				length += 4; 
				if(Property != null) {
					length += Property.getBytes("GBK").length;  
				}
//				length += ByteStream.getObjectSize(Property);  //计算字段Property的长度 size_of(String)
				length += ByteStream.getObjectSize(FavorFilterList);  //计算字段FavorFilterList的长度 size_of(Vector)
				length += 17;  //计算字段Skuid的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(ExtInfo);  //计算字段ExtInfo的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
