package com.qq.qqbuy.thirdparty.idl;

import java.util.HashMap;
import java.util.Map;

/**
 * stub.invoke(req,resp)的返回值. 0正常, 其它为各类错误
 * @author JingYing
 * @date 2015年1月6日
 */
public class WebstubErr {
	
	public static final int 
		SUCCESS = 0,
		REQ_IS_NULL = -1,
		REQ_IS_VALID_TYPE = -2,
		RESP_IS_VALID_TYPE = -3,
		SEND = -4,
		READ = -5,
		DEALWITHDATA = -6,
		NET = -7,
		NET_TIMEOUT = -8,
		SERIALIZE_DATA = -9,	//序列化响应体失败. 如果接口不需要resp,即使出现该错误码,接口仍可用.
		OPEN_NET = -71,
		CONNECT_NET = -72,
		SELECT_NET = -73;
	
	public static Map<Integer, String> map = new HashMap<Integer, String>(); 
    static 	{
    	map.put(SUCCESS, "成功");
    	map.put(REQ_IS_NULL, "req为空");
		map.put(REQ_IS_VALID_TYPE, "REQ_IS_VALID_TYPE");
		map.put(RESP_IS_VALID_TYPE, "RESP_IS_VALID_TYPE");
		map.put(SEND, "发送消息体时异常");
		map.put(READ, "读取响应体时异常");
		map.put(DEALWITHDATA, "处理响应体时失败");
		map.put(NET, "NET");
		map.put(NET_TIMEOUT, "NET_TIMEOUT");
		map.put(SERIALIZE_DATA, "序列化响应体失败");
		map.put(OPEN_NET, "OPEN_NET");
		map.put(CONNECT_NET, "CONNECT_NET");
		map.put(SELECT_NET, "SELECT_NET");
    }
}
