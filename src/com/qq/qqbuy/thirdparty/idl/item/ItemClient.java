package com.qq.qqbuy.thirdparty.idl.item;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.BizConstant;
import com.qq.qqbuy.common.constant.PpConstants;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.exception.ExternalInterfaceException;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.item.util.ItemUtil;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.IsFavoritedReq;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.IsFavoritedResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ApiDownLoadDescReq;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ApiDownLoadDescResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ApiGetItemListReq;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ApiGetItemListResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ApiGetItemReq;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ApiGetItemResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.FetchItemInfoReq;
import com.qq.qqbuy.thirdparty.idl.item.protocol.FetchItemInfoResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.GetItemExtInfoReq;
import com.qq.qqbuy.thirdparty.idl.item.protocol.GetItemExtInfoResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.GetItemInfoReq;
import com.qq.qqbuy.thirdparty.idl.item.protocol.GetItemInfoResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.GetTabDetailReq;
import com.qq.qqbuy.thirdparty.idl.item.protocol.GetTabDetailResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.MItemPic;
import com.qq.qqbuy.thirdparty.idl.item.protocol.MStock;
import com.qq.qqbuy.thirdparty.idl.item.protocol.TabData;
import com.qq.qqbuy.thirdparty.idl.item.protocol.eval.ComdyEvalFilter;
import com.qq.qqbuy.thirdparty.idl.item.protocol.eval.GetComdyEvalListReq;
import com.qq.qqbuy.thirdparty.idl.item.protocol.eval.GetComdyEvalListResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.qgo.GetItemReq;
import com.qq.qqbuy.thirdparty.idl.item.protocol.qgo.GetItemResp;
import com.qq.qqbuy.thirdparty.idl.shop.CVItemFilterConstants;
import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiItemFilter;

public class ItemClient extends SupportIDLBaseClient {
	
	public static List<TabData> getTabDetail(long buyerUin, String clientIp, 
									String mk, List<Integer> tabIds) {
		
		GetTabDetailResp resp = new GetTabDetailResp();
		GetTabDetailReq req = new GetTabDetailReq();
		req.setMachineKey(mk);
		req.setSceneId(2);	//分配2
		req.setSource(clientIp);
		req.setTabList(Util.toVector(tabIds));
		req.setToken("0");	//对方不验证,先填0
		
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		stub.setOperator(buyerUin);
		if(!EnvManager.isIdc()){
			stub.setIpAndPort("10.213.139.18", 53101);//IDC地址,可在gamma下调用
		}
		
		invoke(stub, req, resp);
		if(resp.getResult() != 0)
			throw new ExternalInterfaceException(0, resp.getResult(), resp.getErrMsg());
		return resp.getDetailList();
	}
	
	/**
	 * 调用appj_item获取手拍商品信息
	 * @param itemCode 商品信息
	 * @param isMobile 是否取手机拍拍商品信息，默认为false
	 * @param needDetail 是否需要商品详情信息，默认为false
	 * @param needParseAttr 是否需要解析商品属性，默认为false
	 * @return
	 * @date:2013-4-6
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
    public static GetItemResp getItem(String itemCode,boolean isMobile,boolean needDetail,boolean needParseAttr){
        long start = System.currentTimeMillis();
        GetItemReq req = new GetItemReq();
        GetItemResp resp = new GetItemResp();
        req.setItemCode(itemCode);
        req.setIsMobile(isMobile);
        req.setNeedDetail(needDetail);
        req.setNeedParseAttr(needParseAttr);
        req.setMachineKey(getDefaultMachineKey("item"));
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        if(EnvManager.isIdc()){
            ret = invokePaiPaiIDL(req, resp,stub);
        }else{
            ret = invokePaiPaiIDL(req, resp,stub, GAMMA_IP_NEW, 9105);
           // ret = invokePaiPaiIDL(req, resp,stub);
        }
        return ret == SUCCESS ? resp : null;
    }
	
    /**
     * 
     * @Title: fetchItemInfo
     *  
     * @Description: 通过商品id，查询商品信息 
     * @param itemCode 商品id
     * @return    设定文件 
     * @return FetchItemInfoResp    返回类型 
     * @throws
     */
    public static FetchItemInfoResp fetchItemInfo(String itemCode){
        FetchItemInfoReq req = new FetchItemInfoReq();
        FetchItemInfoResp resp = new FetchItemInfoResp();
        req.setItemId(itemCode);
        req.setScene(0L);
        req.setSource(PpConstants.PP_SOURCE);
        req.setBusinessId(PpConstants.PP_BUSINESSID);
        req.setOption(0xFFFF);
        int ret = 0;
        long uin = ItemUtil.getSellerUin(itemCode);
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setUin(uin);
        stub.setOperator(uin);
        ret = invokePaiPaiIDL(req, resp,stub);
        return ret == SUCCESS ? resp : null;
    }
    
	public static GetItemInfoResp getItemInfo(String itemCode) {
        long start = System.currentTimeMillis();
        GetItemInfoReq req = new GetItemInfoReq();
        req.setItemCode(itemCode);
        req.setMachineKey(getDefaultMachineKey("ItemClient"));
        req.setSource(CALL_IDL_SOURCE);

        GetItemInfoResp resp = new GetItemInfoResp();
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        if(EnvManager.isIdc()){
            ret = invokePaiPaiIDL(req, resp,stub);
        }else{
            ret = invokePaiPaiIDL(req, resp,stub, GAMMA_IP_NEW, 9105);
           // ret = invokePaiPaiIDL(req, resp,stub);
        }
        try 
        {
			Util.decode(resp, BizConstant.OPENAPI_CHARSET);
			if(resp.getMItem()!=null 
					&& resp.getMItem().getStockList()!=null){
				for (MStock mstock : resp.getMItem().getStockList()) {
		        	Util.decode(mstock, BizConstant.OPENAPI_CHARSET);
				}	
			}
			
		} catch (Exception e) {
			Log.run.error("resp decode error:" + e.getMessage());
		}
        return ret == SUCCESS ? resp : null;
    }
	
	public static GetItemExtInfoResp getItemExtInfo(String itemCode) {
        long start = System.currentTimeMillis();
        GetItemExtInfoReq req = new GetItemExtInfoReq();
        req.setItemCode(itemCode);
        req.setMachineKey(getDefaultMachineKey("ItemClient"));
        req.setSource(CALL_IDL_SOURCE);

        GetItemExtInfoResp resp = new GetItemExtInfoResp();
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        if(EnvManager.isIdc()){
            ret = invokePaiPaiIDL(req, resp,stub);
        }else{
            ret = invokePaiPaiIDL(req, resp,stub, GAMMA_IP_NEW, 9105);
            //ret = invokePaiPaiIDL(req, resp,stub);
        }
        try {      	
			Util.decode(resp, BizConstant.OPENAPI_CHARSET);
			if(resp.getItemExtInfo()!=null){
				Util.decode(resp.getItemExtInfo(), BizConstant.OPENAPI_CHARSET);
			}
			if(resp.getItemExtInfo().getItemPics()!=null){
				for(MItemPic mitemPic:resp.getItemExtInfo().getItemPics()){
					Util.decode(mitemPic, BizConstant.OPENAPI_CHARSET);
				}
			}		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return ret == SUCCESS ? resp : null;
    }
	
	public static ApiGetItemResp getApiItem(String itemCode){
	    long start = System.currentTimeMillis();
	    ApiGetItemReq req = new ApiGetItemReq();
	    req.setItemId(itemCode);
        req.setMachineKey(getDefaultMachineKey("ItemClient"));
        req.setSource(CALL_IDL_SOURCE);

        ApiGetItemResp resp = new ApiGetItemResp();
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        ret = invokePaiPaiIDL(req, resp,stub);

        return ret == SUCCESS ? resp : null;
	}
	public static GetComdyEvalListResp getItemEval(String itemCode,
			long sellerUin,int pn,int ps,
			int needReply,int needHistory,int evalLevel) {
        long start = System.currentTimeMillis();
        ComdyEvalFilter filter=new ComdyEvalFilter();
        filter.setComdyId(itemCode);
        filter.setSellerUin(sellerUin);
        filter.setPageIndex(pn);
        filter.setPageSize(ps);
        if(needHistory>0){
            filter.setIsHistory(needHistory);
        }
        if(evalLevel>0){
        	filter.setLevel(evalLevel);
        }
        if(needReply>0){
        	filter.setNeedReply(needReply);
        }
       
        GetComdyEvalListReq req = new GetComdyEvalListReq();
        req.setSceneId(CALL_IDL_SOURCE);
        req.setFilter(filter);
        req.setMachineKey(getDefaultMachineKey("ItemClient"));
        req.setSource(CALL_IDL_SOURCE);

        GetComdyEvalListResp resp = new GetComdyEvalListResp();
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        ret = invokePaiPaiIDL(req, resp,stub);             
        return ret == SUCCESS ? resp : null;
    }

	public static ApiDownLoadDescResp getItemDetail(String itemCode,String detailFile){
	    long start = System.currentTimeMillis();
	    ApiDownLoadDescReq req = new ApiDownLoadDescReq();
	    ApiDownLoadDescResp resp = new ApiDownLoadDescResp();
	    req.setDescFileName(detailFile);	    
	    req.setScene(0L);
	    req.setSource("paipai_api_client");

	    int ret;
	    long uin = ItemUtil.getSellerUin(itemCode);
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setUin(uin);
        stub.setOperator(uin);
        ret = invokePaiPaiIDL(req, resp,stub);
        return ret == SUCCESS ? resp : null;
	}
	/**
	 * 传入商品id列表，批量获取商品信息
	 * @param itemCodeList
	 * @return
	 * @date:2013-2-20
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
	public static ApiGetItemListResp getItemList(Vector<String> itemCodeList){
	    long start = System.currentTimeMillis();
	    ApiGetItemListReq req=new ApiGetItemListReq();
		ApiGetItemListResp resp=new ApiGetItemListResp();			    
	    req.setSource("paipai_api_client"); 
	    req.setItemIdList(itemCodeList);
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setUin(80000935);
        stub.setOperator(80000935);
        int ret = invokePaiPaiIDL(req, resp,stub);
        return ret == SUCCESS ? resp : null;
	}
	private static void testItem(Vector<String> itemList) throws Exception{
		 ApiGetItemListReq req=new ApiGetItemListReq();
			ApiGetItemListResp resp=new ApiGetItemListResp();		
			req.setSource("Qgo_itemAction");
			req.setItemIdList(itemList);
			 IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		        stub.setUin(80000935);
		        stub.setOperator(80000935);
			
			Log.run.info("GetItemDetailByIDL,ItemIdList Size is:"+itemList.size());		
			Log.run.info("ApiGetItemList begin,params="+req.getItemIdList());
			 int ret = invokePaiPaiIDL(req, resp,stub);
			resp.getApiItemList();
	}
	public static void main(String args[]) throws Exception{
//	    JsonConfig jsoncfg = new JsonConfig();
//	    String[] excludes =
//        { "size", "empty" };
//	    jsoncfg.registerJsonBeanProcessor(BitSet.class, new BitSetJsonBeanProcessor()); //对BitSet类型的json处理特殊化
//        jsoncfg.setExcludes(excludes);
//	    //可以获取店铺vip价A8C97F33000000000401000023F778F7
//        FetchItemInfoResp resp = fetchItemInfo("3D56D82E00000000040100000B883D06");
//	    String jsonStr = JSONSerializer.toJSON(resp, jsoncfg).toString();
//	    System.out.println(jsonStr);
//	    
//	    
//	    ApiGetItemResp resp2=getApiItem("3B996E2E00000000003B3AAC0746003D");
//	    System.out.println(resp2);
////	    
////	    List<ItemAttrBO> itemAttrBO = ItemHelper.parseAttr("30:321f000000|31:2000000|37:154|2b0:1|6f5:3|2ef4:7|794d:26|7c45:1|88b7:2|8b45:2^7c45:087",
////	            243720);
////	    
////	    jsonStr = JSONSerializer.toJSON(itemAttrBO, jsoncfg).toString();
////        System.out.println("" + jsonStr);
//	    PPItemBiz biz = new PPItemBizImpl();
//	    ApiGetItemResp apiGetItemResp = ItemClient.getApiItem("F9AD7E020000000004010000122A84FB");
//	    jsonStr = JSONSerializer.toJSON(apiGetItemResp, jsoncfg).toString();
//      System.out.println(jsonStr);
//		GetItemInfoResp resp=getItemInfo("3B996E2E00000000003B3B0F07A53C49");
//		System.out.println(resp.getErrCode());
//		 FetchItemInfoResp qgo=fetchItemInfo("7FC8A90500000000040100001E4D80D7");
//		    System.out.println(qgo);
//		    
//		    ApiDownLoadDescResp rsp=  getItemDetail("F14DF632000000000401000010A08166");
//		    System.out.println(rsp.getDescFileContent());
		    
//		    GetItemResp rsp2=  getItem("7FC8A90500000000040100002479668B",true,true,true);
//		    System.out.println(rsp2);
//		    ApiFindItemListResp resp= getQgoItemFromPCBySellerUin(2,3,779000123);
//		    System.out.println(resp);
//		    System.out.println(URLDecoder.decode("UCRpE9ZAcQ62ASMOp%2Bzo1i%2BvPcI0BAl815b04af20201%3D%3D", "UTF-8"));
		String itemCode = "EABD69020000000004010000076E6EA7";
		long sellerUin = 40484330L;
		GetComdyEvalListResp resp  = getItemEval(itemCode, sellerUin, 1, 1, 0, 0, 0);
		System.out.println(resp.getTotalNum());
	}
	/**
     * 查询卖家有手拍标的在售商品
     * @param pn
     * @param ps
     * @param sellerUin
     * @return
     * @throws Exception
     * @date:2013-7-9
     * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
     * @version 1.0
     */
//    public static ApiFindItemListResp getQgoItemFromPCBySellerUin(int pn,int ps,long sellerUin) throws Exception{
//
//        // 构造请求 查出商品个数
//        ApiFindItemListReq req = new ApiFindItemListReq();
//        req.setMachineKey("abc123");
//        req.setSource("webapp_boss_mgmt.com.qq.qgo.portal.seller.action.ItemAction.java");
//        ApiItemFilter filter = new ApiItemFilter();
//        filter.setSellerUin(sellerUin);
//        filter.setPageSize(ps);
//        //这里是从0开始
//        filter.setStartIndex(pn);
//        Map<String, String> filterMap = new HashMap<String, String>();
//        filterMap.put(CVItemFilterConstants.ITEM_FILTER_KEY_PROPERTY, "128");
//        
//        //商品状态
//        //filterMap.put(CVItemFilterConstants.ITEM_FILTER_KEY_STATE,CVItemFilterConstants.ITEM_FILTER_STATE_SELLING);
//  
//        filter.setFilterMap(filterMap);
//        //排序方式 上架时间倒序
//        filter.setOrderType(CVItemFilterConstants.C2C_WW_COMDY_FIND_ORDERY_BY_UP_TIME_DESC);
//        Log.run.debug("filterMap:" + filterMap);
//        req.setItemVFilter(filter);
//
//        // 响应
//        ApiFindItemListResp resp = new ApiFindItemListResp();
//        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
//        stub.setUin(sellerUin);
//        stub.invoke(req, resp);
//        if(resp.getResult() == 0){            
//            Log.run.info("WebStubCntl.invoke[CMD=ApiFindItemList] success, find " + resp.getTotalSize() +" items");
//            return resp;
//
//        }else{
//            Log.run.warn("WebStubCntl.invoke[CMD=ApiFindItemList] failed,result:" + resp.getResult());
//        }   
//        return null;
//    }
    /**
     * 根据QQ以及商品id查出是否被收藏
     * @param itemCode
     * @param qq
     * @return
     */
	public static IsFavoritedResp getIsFavoritedDetail(String itemCode, long buyerUin,String clientIp, String sk, String reserve) {
		IsFavoritedResp resp = new IsFavoritedResp();
		IsFavoritedReq req = new IsFavoritedReq();
		req.setMechineKey("0");
		req.setQueryItemId(itemCode);
		req.setUin(buyerUin);
		req.setSource(CALL_IDL_SOURCE);
		int ret = 0;
        AsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setClientIP(Util.iP2Long(clientIp));
        stub.setOperator(buyerUin);
        stub.setUin(buyerUin);
        stub.setSkey(sk.getBytes());
        ret = invokePaiPaiIDL(req, resp,stub);
        return ret == SUCCESS ? resp : null;
	}
	
}
