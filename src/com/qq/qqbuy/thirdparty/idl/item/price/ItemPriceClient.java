package com.qq.qqbuy.thirdparty.idl.item.price;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.qq.qqbuy.common.exception.ExternalInterfaceException;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.item.price.protocol.GetCommodityListPriceReq;
import com.qq.qqbuy.thirdparty.idl.item.price.protocol.GetCommodityListPriceResp;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * Created by wanghao5 on 2015/4/21.
 */
public class ItemPriceClient extends SupportIDLBaseClient {

    /**
     * app专用请求源
     */
    private static final String APP_ITEM_PRICE_REQUEST_SOURCE = "APP";

    /**
     * 获取商品不同场景的价格
     * @param sceneId
     *        场景标识，默认参数为0，
     *        拍拍原有价格体系：1原价 4彩钻价 5店铺vip价 41特价 43coss活动价 45限时折扣 46大促价 105微店价 106批发价
     *        新增的场景价：44拍便宜 200内购活动价 201普通买手推荐价 202认证买手推荐价 203明星买手推荐价 210APP专项价
     *        220QQ会员价 221EDM推送价 222微信专享价 230团购特价 231闪购特价 232渠道拉新价 233老用户价
     * @param userType
     *        userType：核心参数，代表用户类型，
     *        取值如下：1代表买家，2代表卖家，3代表平台用户，
     *        当前系统只支持1，其他取值会返回系统错误，必填
     * @param buyerUin
     *        买家qq号,如果ddwBuyerIdIn传入的值小于10000，就代表着非登录场景下的价格请求，必填
     * @param sellerUin
     *        卖家qq号
     * @param idList
     *        商品的id列表，不能为空，至少一个商品，
     *        并且所有的商品id必须是系统已经存在的商品id，
     *        传入非法构造的商品id会返回系统错误，也就是被当做攻击行为
     * @return GetCommodityListPriceResp
     */
    public static GetCommodityListPriceResp getItemPrice(long sceneId,long userType,long buyerUin,long sellerUin,Vector<String> idList){

        GetCommodityListPriceReq req = new GetCommodityListPriceReq();
        /**
         * 事务id，代表着本次请求，没有实际的业务含义，用于辅助定位问题，必传参数，
         * 可以这样构造 uint64_t  transactionId = (uint64_t)time(NULL) * (uint64_t)time(NULL);
         */
        req.setTransactionId(System.currentTimeMillis());
        /**
         *请求源
         */
        req.setRequestSource(APP_ITEM_PRICE_REQUEST_SOURCE);
        req.setSceneId(sceneId);
        req.setUserType(userType);
        req.setBuyerIdIn(buyerUin);
        req.setSellerIdIn(sellerUin);
        req.setIdList(idList);
        /**
         * 暂时未启用，其含义是用来区分同一个场景下（APP、H5、PC）下的不同的业务，如商祥和收藏商品可能需要区分
         */
        req.setBusinessType("");
        /**
         * 备用的参数通到，当前未启用
         */
        Map<String,String> paramListIn = new HashMap<String,String>();
        req.setParamListIn(paramListIn);

        GetCommodityListPriceResp resp = new GetCommodityListPriceResp();
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        invoke(stub, req, resp);
        if(resp.getResult() != 0)
            throw new ExternalInterfaceException(0, resp.getResult(), resp.getErrMsg());
        return resp;
    }

    public static void main(String[] args) {
        long time = System.currentTimeMillis();
        System.out.println(time*time);
    }
}
