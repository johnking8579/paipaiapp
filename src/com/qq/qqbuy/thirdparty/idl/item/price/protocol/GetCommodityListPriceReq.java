 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: item.UniformPriceGateway.java

package com.qq.qqbuy.thirdparty.idl.item.price.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import java.util.Map;
import java.util.Vector;
import java.util.HashMap;

/**
 *req
 *
 *@date 2015-04-21 04:22:25
 *
 *@since version:0
*/
public class  GetCommodityListPriceReq extends NetMessage
{
	/**
	 * 事务id，必填参数，一个64位的全局唯一的大数
	 *
	 * 版本 >= 0
	 */
	 private long transactionId;

	/**
	 * 场景id
	 *
	 * 版本 >= 0
	 */
	 private long sceneId;

	/**
	 * 商品ID列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<String> idList = new Vector<String>();

	/**
	 * 业务方商品详情购物流程等
	 *
	 * 版本 >= 0
	 */
	 private String businessType = new String();

	/**
	 * 用户类别 1 代表买家，2 代表卖家，3 代表平台用户
	 *
	 * 版本 >= 0
	 */
	 private long userType;

	/**
	 * 卖家id
	 *
	 * 版本 >= 0
	 */
	 private long sellerIdIn;

	/**
	 * 买家id
	 *
	 * 版本 >= 0
	 */
	 private long buyerIdIn;

	/**
	 * 来源
	 *
	 * 版本 >= 0
	 */
	 private String requestSource = new String();

	/**
	 * 附属参数
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> paramListIn = new HashMap<String,String>();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushLong(transactionId);
		bs.pushUInt(sceneId);
		bs.pushObject(idList);
		bs.pushString(businessType);
		bs.pushUInt(userType);
		bs.pushLong(sellerIdIn);
		bs.pushLong(buyerIdIn);
		bs.pushString(requestSource);
		bs.pushObject(paramListIn);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		transactionId = bs.popLong();
		sceneId = bs.popUInt();
		idList = (Vector<String>)bs.popVector(String.class);
		businessType = bs.popString();
		userType = bs.popUInt();
		sellerIdIn = bs.popLong();
		buyerIdIn = bs.popLong();
		requestSource = bs.popString();
		paramListIn = (Map<String,String>)bs.popMap(String.class,String.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x52301801L;
	}


	/**
	 * 获取事务id，必填参数，一个64位的全局唯一的大数
	 * 
	 * 此字段的版本 >= 0
	 * @return transactionId value 类型为:long
	 * 
	 */
	public long getTransactionId()
	{
		return transactionId;
	}


	/**
	 * 设置事务id，必填参数，一个64位的全局唯一的大数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTransactionId(long value)
	{
		this.transactionId = value;
	}


	/**
	 * 获取场景id
	 * 
	 * 此字段的版本 >= 0
	 * @return sceneId value 类型为:long
	 * 
	 */
	public long getSceneId()
	{
		return sceneId;
	}


	/**
	 * 设置场景id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSceneId(long value)
	{
		this.sceneId = value;
	}


	/**
	 * 获取商品ID列表
	 * 
	 * 此字段的版本 >= 0
	 * @return idList value 类型为:Vector<String>
	 * 
	 */
	public Vector<String> getIdList()
	{
		return idList;
	}


	/**
	 * 设置商品ID列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<String>
	 * 
	 */
	public void setIdList(Vector<String> value)
	{
		if (value != null) {
				this.idList = value;
		}else{
				this.idList = new Vector<String>();
		}
	}


	/**
	 * 获取业务方商品详情购物流程等
	 * 
	 * 此字段的版本 >= 0
	 * @return businessType value 类型为:String
	 * 
	 */
	public String getBusinessType()
	{
		return businessType;
	}


	/**
	 * 设置业务方商品详情购物流程等
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBusinessType(String value)
	{
		this.businessType = value;
	}


	/**
	 * 获取用户类别 1 代表买家，2 代表卖家，3 代表平台用户
	 * 
	 * 此字段的版本 >= 0
	 * @return userType value 类型为:long
	 * 
	 */
	public long getUserType()
	{
		return userType;
	}


	/**
	 * 设置用户类别 1 代表买家，2 代表卖家，3 代表平台用户
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUserType(long value)
	{
		this.userType = value;
	}


	/**
	 * 获取卖家id
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerIdIn value 类型为:long
	 * 
	 */
	public long getSellerIdIn()
	{
		return sellerIdIn;
	}


	/**
	 * 设置卖家id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerIdIn(long value)
	{
		this.sellerIdIn = value;
	}


	/**
	 * 获取买家id
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerIdIn value 类型为:long
	 * 
	 */
	public long getBuyerIdIn()
	{
		return buyerIdIn;
	}


	/**
	 * 设置买家id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerIdIn(long value)
	{
		this.buyerIdIn = value;
	}


	/**
	 * 获取来源
	 * 
	 * 此字段的版本 >= 0
	 * @return requestSource value 类型为:String
	 * 
	 */
	public String getRequestSource()
	{
		return requestSource;
	}


	/**
	 * 设置来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRequestSource(String value)
	{
		this.requestSource = value;
	}


	/**
	 * 获取附属参数
	 * 
	 * 此字段的版本 >= 0
	 * @return paramListIn value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getParamListIn()
	{
		return paramListIn;
	}


	/**
	 * 设置附属参数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setParamListIn(Map<String,String> value)
	{
		if (value != null) {
				this.paramListIn = value;
		}else{
				this.paramListIn = new HashMap<String,String>();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetCommodityListPriceReq)
				length += 17;  //计算字段transactionId的长度 size_of(uint64_t)
				length += 4;  //计算字段sceneId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(idList);  //计算字段idList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(businessType);  //计算字段businessType的长度 size_of(String)
				length += 4;  //计算字段userType的长度 size_of(uint32_t)
				length += 17;  //计算字段sellerIdIn的长度 size_of(uint64_t)
				length += 17;  //计算字段buyerIdIn的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(requestSource);  //计算字段requestSource的长度 size_of(String)
				length += ByteStream.getObjectSize(paramListIn);  //计算字段paramListIn的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
