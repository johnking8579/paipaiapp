 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: item.UniformPriceGateway.java

package com.qq.qqbuy.thirdparty.idl.item.price.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import java.util.Map;
import java.util.Vector;
import java.util.HashMap;

/**
 *resp
 *
 *@date 2015-04-21 04:22:25
 *
 *@since version:0
*/
public class  GetCommodityListPriceResp extends NetMessage
{
	/**
	 * 卖家id
	 *
	 * 版本 >= 0
	 */
	 private long sellerIdOut;

	/**
	 * 买家id
	 *
	 * 版本 >= 0
	 */
	 private long buyerIdOut;

	/**
	 * 库存价格列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<CommodityPrice> commodityPriceList = new Vector<CommodityPrice>();

	/**
	 * 附属结果返回
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> paramListOut = new HashMap<String,String>();

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushLong(sellerIdOut);
		bs.pushLong(buyerIdOut);
		bs.pushObject(commodityPriceList);
		bs.pushObject(paramListOut);
		bs.pushString(errMsg);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		sellerIdOut = bs.popLong();
		buyerIdOut = bs.popLong();
		commodityPriceList = (Vector<CommodityPrice>)bs.popVector(CommodityPrice.class);
		paramListOut = (Map<String,String>)bs.popMap(String.class,String.class);
		errMsg = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x52308801L;
	}


	/**
	 * 获取卖家id
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerIdOut value 类型为:long
	 * 
	 */
	public long getSellerIdOut()
	{
		return sellerIdOut;
	}


	/**
	 * 设置卖家id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerIdOut(long value)
	{
		this.sellerIdOut = value;
	}


	/**
	 * 获取买家id
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerIdOut value 类型为:long
	 * 
	 */
	public long getBuyerIdOut()
	{
		return buyerIdOut;
	}


	/**
	 * 设置买家id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerIdOut(long value)
	{
		this.buyerIdOut = value;
	}


	/**
	 * 获取库存价格列表
	 * 
	 * 此字段的版本 >= 0
	 * @return commodityPriceList value 类型为:Vector<CommodityPrice>
	 * 
	 */
	public Vector<CommodityPrice> getCommodityPriceList()
	{
		return commodityPriceList;
	}


	/**
	 * 设置库存价格列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<CommodityPrice>
	 * 
	 */
	public void setCommodityPriceList(Vector<CommodityPrice> value)
	{
		if (value != null) {
				this.commodityPriceList = value;
		}else{
				this.commodityPriceList = new Vector<CommodityPrice>();
		}
	}


	/**
	 * 获取附属结果返回
	 * 
	 * 此字段的版本 >= 0
	 * @return paramListOut value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getParamListOut()
	{
		return paramListOut;
	}


	/**
	 * 设置附属结果返回
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setParamListOut(Map<String,String> value)
	{
		if (value != null) {
				this.paramListOut = value;
		}else{
				this.paramListOut = new HashMap<String,String>();
		}
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetCommodityListPriceResp)
				length += 17;  //计算字段sellerIdOut的长度 size_of(uint64_t)
				length += 17;  //计算字段buyerIdOut的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(commodityPriceList);  //计算字段commodityPriceList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(paramListOut);  //计算字段paramListOut的长度 size_of(Map)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
