//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: item.UniformPriceGateway.java

package com.qq.qqbuy.thirdparty.idl.item.price.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *商品批价结果结构体 
 *
 *@date 2015-04-21 04:22:25
 *
 *@since version:20150309
*/
public class CommodityPrice  implements ICanSerializeObject
{
	/**
	 * 商品ID
	 *
	 * 版本 >= 0
	 */
	 private String id = new String();

	/**
	 * 库存价格列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<SkuPrice> skuPriceList = new Vector<SkuPrice>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushString(id);
		bs.pushObject(skuPriceList);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		id = bs.popString();
		skuPriceList = (Vector<SkuPrice>)bs.popVector(SkuPrice.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return id value 类型为:String
	 * 
	 */
	public String getId()
	{
		return id;
	}


	/**
	 * 设置商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setId(String value)
	{
		this.id = value;
	}


	/**
	 * 获取库存价格列表
	 * 
	 * 此字段的版本 >= 0
	 * @return skuPriceList value 类型为:Vector<SkuPrice>
	 * 
	 */
	public Vector<SkuPrice> getSkuPriceList()
	{
		return skuPriceList;
	}


	/**
	 * 设置库存价格列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<SkuPrice>
	 * 
	 */
	public void setSkuPriceList(Vector<SkuPrice> value)
	{
		if (value != null) {
				this.skuPriceList = value;
		}else{
				this.skuPriceList = new Vector<SkuPrice>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CommodityPrice)
				length += ByteStream.getObjectSize(id);  //计算字段id的长度 size_of(String)
				length += ByteStream.getObjectSize(skuPriceList);  //计算字段skuPriceList的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
