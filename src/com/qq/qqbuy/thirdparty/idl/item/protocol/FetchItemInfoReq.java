 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.item.ao.idl.ao_ItemInfoApi.java

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *查询商品详细信息请求类
 *
 *@date 2013-02-27 05:00:19
 *
 *@since version:0
*/
public class  FetchItemInfoReq implements IServiceObject
{
	/**
	 * 机器码，不能为空
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 来源,不能为空
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 调用方业务id，预留，不能为空
	 *
	 * 版本 >= 0
	 */
	 private long BusinessId;

	/**
	 * 场景id,使用前必须申请，请联系anthonywei，lightwang申请
	 *
	 * 版本 >= 0
	 */
	 private long scene;

	/**
	 * 选项，此选项决定是否取商品的一些特殊信息,如取商品实时浏览量信息(0x0000000000001000)+商品的当期销售量(0x0000000000001000)+商品基本拍卖(0x0000000000010000)：option=0x0000000000000800 | 0x0000000000001000 | 0x0000000000010000,若使用中有疑问，请联系felixwei，或参考c2c_define.h第395行对option的取值定义。
	 *
	 * 版本 >= 0
	 */
	 private long option;

	/**
	 * 商品id,不能为空
	 *
	 * 版本 >= 0
	 */
	 private String itemId = new String();

	/**
	 * 请求保留字
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(machineKey);
		bs.pushString(source);
		bs.pushUInt(BusinessId);
		bs.pushUInt(scene);
		bs.pushLong(option);
		bs.pushString(itemId);
		bs.pushString(inReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		machineKey = bs.popString();
		source = bs.popString();
		BusinessId = bs.popUInt();
		scene = bs.popUInt();
		option = bs.popLong();
		itemId = bs.popString();
		inReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x69111801L;
	}


	/**
	 * 获取机器码，不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置机器码，不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取来源,不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置来源,不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取调用方业务id，预留，不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @return BusinessId value 类型为:long
	 * 
	 */
	public long getBusinessId()
	{
		return BusinessId;
	}


	/**
	 * 设置调用方业务id，预留，不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBusinessId(long value)
	{
		this.BusinessId = value;
	}


	/**
	 * 获取场景id,使用前必须申请，请联系anthonywei，lightwang申请
	 * 
	 * 此字段的版本 >= 0
	 * @return scene value 类型为:long
	 * 
	 */
	public long getScene()
	{
		return scene;
	}


	/**
	 * 设置场景id,使用前必须申请，请联系anthonywei，lightwang申请
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setScene(long value)
	{
		this.scene = value;
	}


	/**
	 * 获取选项，此选项决定是否取商品的一些特殊信息,如取商品实时浏览量信息(0x0000000000001000)+商品的当期销售量(0x0000000000001000)+商品基本拍卖(0x0000000000010000)：option=0x0000000000000800 | 0x0000000000001000 | 0x0000000000010000,若使用中有疑问，请联系felixwei，或参考c2c_define.h第395行对option的取值定义。
	 * 
	 * 此字段的版本 >= 0
	 * @return option value 类型为:long
	 * 
	 */
	public long getOption()
	{
		return option;
	}


	/**
	 * 设置选项，此选项决定是否取商品的一些特殊信息,如取商品实时浏览量信息(0x0000000000001000)+商品的当期销售量(0x0000000000001000)+商品基本拍卖(0x0000000000010000)：option=0x0000000000000800 | 0x0000000000001000 | 0x0000000000010000,若使用中有疑问，请联系felixwei，或参考c2c_define.h第395行对option的取值定义。
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOption(long value)
	{
		this.option = value;
	}


	/**
	 * 获取商品id,不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @return itemId value 类型为:String
	 * 
	 */
	public String getItemId()
	{
		return itemId;
	}


	/**
	 * 设置商品id,不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemId(String value)
	{
		this.itemId = value;
	}


	/**
	 * 获取请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(FetchItemInfoReq)
				length += ByteStream.getObjectSize(machineKey);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += 4;  //计算字段BusinessId的长度 size_of(uint32_t)
				length += 4;  //计算字段scene的长度 size_of(uint32_t)
				length += 17;  //计算字段option的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(itemId);  //计算字段itemId的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
