//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.EvalIdl.java

package com.qq.qqbuy.thirdparty.idl.item.protocol.eval;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *评价列表请求filter
 *
 *@date 2012-12-17 10:47:50
 *
 *@since version:0
*/
public class EvalListFilter  implements ICanSerializeObject
{
	/**
	 * 订单id
	 *
	 * 版本 >= 0
	 */
	 private String DealId = new String();

	/**
	 * QQ号-
	 *
	 * 版本 >= 0
	 */
	 private long Uin;

	/**
	 * 商品id
	 *
	 * 版本 >= 0
	 */
	 private String ComdyId = new String();

	/**
	 * 页码
	 *
	 * 版本 >= 0
	 */
	 private long PageIndex;

	/**
	 * 每页数量
	 *
	 * 版本 >= 0
	 */
	 private long PageSize;

	/**
	 * 当期时间
	 *
	 * 版本 >= 0
	 */
	 private long ResetTime;

	/**
	 * 是否历史库
	 *
	 * 版本 >= 0
	 */
	 private long IsHistory;

	/**
	 * 是否需要回复
	 *
	 * 版本 >= 0
	 */
	 private long NeedReply;

	/**
	 * 请求者身份
	 *
	 * 版本 >= 0
	 */
	 private long Role;

	/**
	 * 评价等级
	 *
	 * 版本 >= 0
	 */
	 private long Level;

	/**
	 * 是否为收到的评价
	 *
	 * 版本 >= 0
	 */
	 private long IsRecv;

	/**
	 * 时间类型 1,2,3: 6month,1month,1week
	 *
	 * 版本 >= 0
	 */
	 private long TimeType;

	/**
	 * 是否只要有内容的，0否1是
	 *
	 * 版本 >= 0
	 */
	 private long HasContent;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushString(DealId);
		bs.pushUInt(Uin);
		bs.pushString(ComdyId);
		bs.pushUInt(PageIndex);
		bs.pushUInt(PageSize);
		bs.pushUInt(ResetTime);
		bs.pushUInt(IsHistory);
		bs.pushUInt(NeedReply);
		bs.pushUInt(Role);
		bs.pushUInt(Level);
		bs.pushUInt(IsRecv);
		bs.pushUInt(TimeType);
		bs.pushUInt(HasContent);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		DealId = bs.popString();
		Uin = bs.popUInt();
		ComdyId = bs.popString();
		PageIndex = bs.popUInt();
		PageSize = bs.popUInt();
		ResetTime = bs.popUInt();
		IsHistory = bs.popUInt();
		NeedReply = bs.popUInt();
		Role = bs.popUInt();
		Level = bs.popUInt();
		IsRecv = bs.popUInt();
		TimeType = bs.popUInt();
		HasContent = bs.popUInt();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取订单id
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:String
	 * 
	 */
	public String getDealId()
	{
		return DealId;
	}


	/**
	 * 设置订单id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealId(String value)
	{
		this.DealId = value;
	}


	/**
	 * 获取QQ号-
	 * 
	 * 此字段的版本 >= 0
	 * @return Uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return Uin;
	}


	/**
	 * 设置QQ号-
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.Uin = value;
	}


	/**
	 * 获取商品id
	 * 
	 * 此字段的版本 >= 0
	 * @return ComdyId value 类型为:String
	 * 
	 */
	public String getComdyId()
	{
		return ComdyId;
	}


	/**
	 * 设置商品id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setComdyId(String value)
	{
		this.ComdyId = value;
	}


	/**
	 * 获取页码
	 * 
	 * 此字段的版本 >= 0
	 * @return PageIndex value 类型为:long
	 * 
	 */
	public long getPageIndex()
	{
		return PageIndex;
	}


	/**
	 * 设置页码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPageIndex(long value)
	{
		this.PageIndex = value;
	}


	/**
	 * 获取每页数量
	 * 
	 * 此字段的版本 >= 0
	 * @return PageSize value 类型为:long
	 * 
	 */
	public long getPageSize()
	{
		return PageSize;
	}


	/**
	 * 设置每页数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPageSize(long value)
	{
		this.PageSize = value;
	}


	/**
	 * 获取当期时间
	 * 
	 * 此字段的版本 >= 0
	 * @return ResetTime value 类型为:long
	 * 
	 */
	public long getResetTime()
	{
		return ResetTime;
	}


	/**
	 * 设置当期时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setResetTime(long value)
	{
		this.ResetTime = value;
	}


	/**
	 * 获取是否历史库
	 * 
	 * 此字段的版本 >= 0
	 * @return IsHistory value 类型为:long
	 * 
	 */
	public long getIsHistory()
	{
		return IsHistory;
	}


	/**
	 * 设置是否历史库
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setIsHistory(long value)
	{
		this.IsHistory = value;
	}


	/**
	 * 获取是否需要回复
	 * 
	 * 此字段的版本 >= 0
	 * @return NeedReply value 类型为:long
	 * 
	 */
	public long getNeedReply()
	{
		return NeedReply;
	}


	/**
	 * 设置是否需要回复
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNeedReply(long value)
	{
		this.NeedReply = value;
	}


	/**
	 * 获取请求者身份
	 * 
	 * 此字段的版本 >= 0
	 * @return Role value 类型为:long
	 * 
	 */
	public long getRole()
	{
		return Role;
	}


	/**
	 * 设置请求者身份
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRole(long value)
	{
		this.Role = value;
	}


	/**
	 * 获取评价等级
	 * 
	 * 此字段的版本 >= 0
	 * @return Level value 类型为:long
	 * 
	 */
	public long getLevel()
	{
		return Level;
	}


	/**
	 * 设置评价等级
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLevel(long value)
	{
		this.Level = value;
	}


	/**
	 * 获取是否为收到的评价
	 * 
	 * 此字段的版本 >= 0
	 * @return IsRecv value 类型为:long
	 * 
	 */
	public long getIsRecv()
	{
		return IsRecv;
	}


	/**
	 * 设置是否为收到的评价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setIsRecv(long value)
	{
		this.IsRecv = value;
	}


	/**
	 * 获取时间类型 1,2,3: 6month,1month,1week
	 * 
	 * 此字段的版本 >= 0
	 * @return TimeType value 类型为:long
	 * 
	 */
	public long getTimeType()
	{
		return TimeType;
	}


	/**
	 * 设置时间类型 1,2,3: 6month,1month,1week
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTimeType(long value)
	{
		this.TimeType = value;
	}


	/**
	 * 获取是否只要有内容的，0否1是
	 * 
	 * 此字段的版本 >= 0
	 * @return HasContent value 类型为:long
	 * 
	 */
	public long getHasContent()
	{
		return HasContent;
	}


	/**
	 * 设置是否只要有内容的，0否1是
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setHasContent(long value)
	{
		this.HasContent = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(EvalListFilter)
				length += ByteStream.getObjectSize(DealId);  //计算字段DealId的长度 size_of(String)
				length += 4;  //计算字段Uin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ComdyId);  //计算字段ComdyId的长度 size_of(String)
				length += 4;  //计算字段PageIndex的长度 size_of(uint32_t)
				length += 4;  //计算字段PageSize的长度 size_of(uint32_t)
				length += 4;  //计算字段ResetTime的长度 size_of(uint32_t)
				length += 4;  //计算字段IsHistory的长度 size_of(uint32_t)
				length += 4;  //计算字段NeedReply的长度 size_of(uint32_t)
				length += 4;  //计算字段Role的长度 size_of(uint32_t)
				length += 4;  //计算字段Level的长度 size_of(uint32_t)
				length += 4;  //计算字段IsRecv的长度 size_of(uint32_t)
				length += 4;  //计算字段TimeType的长度 size_of(uint32_t)
				length += 4;  //计算字段HasContent的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
