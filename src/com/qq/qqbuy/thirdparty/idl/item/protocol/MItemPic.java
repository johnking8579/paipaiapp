 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *商品图片信息
 *
 *@date 2011-11-15 11:14::15
 *
 *@since version:0
*/
public class MItemPic  implements ICanSerializeObject, java.io.Serializable
{
	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private int version = 0; 

	/**
	 * 图片序号
	 *
	 * 版本 >= 0
	 */
	 private int picIndex;

	/**
	 * 图片URL
	 *
	 * 版本 >= 0
	 */
	 private String picUrl = new String();

	/**
	 * 图片描述
	 *
	 * 版本 >= 0
	 */
	 private String picDesc = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(version);
		bs.pushInt(picIndex);
		bs.pushString(picUrl);
		bs.pushString(picDesc);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		picIndex = bs.popInt();
		picUrl = bs.popString();
		picDesc = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取图片序号
	 * 
	 * 此字段的版本 >= 0
	 * @return picIndex value 类型为:int
	 * 
	 */
	public int getPicIndex()
	{
		return picIndex;
	}


	/**
	 * 设置图片序号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPicIndex(int value)
	{
		this.picIndex = value;
	}


	/**
	 * 获取图片URL
	 * 
	 * 此字段的版本 >= 0
	 * @return picUrl value 类型为:String
	 * 
	 */
	public String getPicUrl()
	{
		return picUrl;
	}


	/**
	 * 设置图片URL
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPicUrl(String value)
	{
		if (value != null) {
				this.picUrl = value;
		}else{
				this.picUrl = new String();
		}
	}


	/**
	 * 获取图片描述
	 * 
	 * 此字段的版本 >= 0
	 * @return picDesc value 类型为:String
	 * 
	 */
	public String getPicDesc()
	{
		return picDesc;
	}


	/**
	 * 设置图片描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPicDesc(String value)
	{
		if (value != null) {
				this.picDesc = value;
		}else{
				this.picDesc = new String();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(MItemPic)
				length += 4;  //计算字段version的长度 size_of(int)
				length += 4;  //计算字段picIndex的长度 size_of(int)
				length += ByteStream.getObjectSize(picUrl);  //计算字段picUrl的长度 size_of(String)
				length += ByteStream.getObjectSize(picDesc);  //计算字段picDesc的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
