 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.uint16_t;
import java.util.Map;
import java.util.Vector;
import com.paipai.lang.MultiMap;
import com.paipai.lang.uint8_t;
import java.util.HashMap;

/**
 *商品Po
 *
 *@date 2012-09-02 10:38::24
 *
 *@since version:0
*/
public class ApiItem  implements ICanSerializeObject
{
	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long version = 20110908; 

	/**
	 * 快照版本号
	 *
	 * 版本 >= 0
	 */
	 private long snapVersion;

	/**
	 * 卖家Uin
	 *
	 * 版本 >= 0
	 */
	 private long qQUin;

	/**
	 * 品类(类目)ID
	 *
	 * 版本 >= 0
	 */
	 private long leafClassId;

	/**
	 * 城市
	 *
	 * 版本 >= 0
	 */
	 private long city;

	/**
	 * 省份
	 *
	 * 版本 >= 0
	 */
	 private long province;

	/**
	 * 国家
	 *
	 * 版本 >= 0
	 */
	 private long country;

	/**
	 * 商品单价
	 *
	 * 版本 >= 0
	 */
	 private long price;

	/**
	 * 商品数量
	 *
	 * 版本 >= 0
	 */
	 private long num;

	/**
	 * 商品新旧程度 值见{@link com.paipai.c2c.item.constants.ItemConstants}
	 *
	 * 版本 >= 0
	 */
	 private long newType;

	/**
	 * 出售方式 值见{@link com.paipai.c2c.item.constants.ItemConstants}
	 *
	 * 版本 >= 0
	 */
	 private long dealType;

	/**
	 * 是否支持财付通
	 *
	 * 版本 >= 0
	 */
	 private long supportPayAgency;

	/**
	 * 店铺推荐
	 *
	 * 版本 >= 0
	 */
	 private long isRecommend;

	/**
	 * 商品状态 值见{@link com.paipai.c2c.item.constants.ItemConstants}
	 *
	 * 版本 >= 0
	 */
	 private long state;

	/**
	 * 商品品牌id
	 *
	 * 版本 >= 0
	 */
	 private long brand;

	/**
	 * 商品规格id
	 *
	 * 版本 >= 0
	 */
	 private long spec;

	/**
	 * 商品生产商id
	 *
	 * 版本 >= 0
	 */
	 private long producer;

	/**
	 * 有效时间
	 *
	 * 版本 >= 0
	 */
	 private long validTime;

	/**
	 * 运费承担方式 值见{@link com.paipai.c2c.item.constants.ItemConstants#TRANSPORT_SELLER_PAY}等
	 *
	 * 版本 >= 0
	 */
	 private long transportPriceType;

	/**
	 * 商品发货方式 值见{@link com.paipai.c2c.item.constants.ItemConstants#SEND_TYPE_MONEY}等
	 *
	 * 版本 >= 0
	 */
	 private long sendType;

	/**
	 * 平邮价格
	 *
	 * 版本 >= 0
	 */
	 private long normalMailPrice;

	/**
	 * 快递价格
	 *
	 * 版本 >= 0
	 */
	 private long expressMailPrice;

	/**
	 * 商品限制购买数量,0表示不限制
	 *
	 * 版本 >= 0
	 */
	 private long buyLimit;

	/**
	 * 商品详情页面颜色主题
	 *
	 * 版本 >= 0
	 */
	 private long customStyleType;

	/**
	 * 商品详情文件大小
	 *
	 * 版本 >= 0
	 */
	 private long detailSize;

	/**
	 * 产品ID
	 *
	 * 版本 >= 0
	 */
	 private long productId;

	/**
	 * 商品重量 预留字段
	 *
	 * 版本 >= 0
	 */
	 private long weight;

	/**
	 * 上级分销商相关,没有使用
	 *
	 * 版本 >= 0
	 */
	 private long uperUin;

	/**
	 * 上级分销商相关,没有使用
	 *
	 * 版本 >= 0
	 */
	 private long uperItemId;

	/**
	 * 重新上架次数
	 *
	 * 版本 >= 0
	 */
	 private long reloadCnt;

	/**
	 * 店铺自定义分类
	 *
	 * 版本 >= 0
	 */
	 private String shopClassId = new String();

	/**
	 * 商品详情文件名
	 *
	 * 版本 >= 0
	 */
	 private String descFilePos = new String();

	/**
	 * 商品ID
	 *
	 * 版本 >= 0
	 */
	 private String itemId = new String();

	/**
	 * 商品快照ID
	 *
	 * 版本 >= 0
	 */
	 private String snapId = new String();

	/**
	 * 卖家昵称
	 *
	 * 版本 >= 0
	 */
	 private String qQNick = new String();

	/**
	 * 卖家店铺ID
	 *
	 * 版本 >= 0
	 */
	 private String shopId = new String();

	/**
	 * 商品名称
	 *
	 * 版本 >= 0
	 */
	 private String title = new String();

	/**
	 * 商品所有属性,使用方法{@link com.paipai.c2c.item.constants.ItemConstants}
	 *
	 * 版本 >= 0
	 */
	 private Map<uint16_t,uint8_t> property = new HashMap<uint16_t,uint8_t>();

	/**
	 * 推荐搭配商品ID列表,多个以|分隔
	 *
	 * 版本 >= 0
	 */
	 private String relatedItems = new String();

	/**
	 * 类目属性串
	 *
	 * 版本 >= 0
	 */
	 private String attrText = new String();

	/**
	 * 商品图片列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<String> logo = new Vector<String>();

	/**
	 * 商品扩展表字段 Key见{@link com.paipai.c2c.item.constants.ItemConstants}
	 *
	 * 版本 >= 0
	 */
	 private MultiMap<String,String> extMap = new MultiMap<String,String>();

	/**
	 * 统计信息，如售出数量等
	 *
	 * 版本 >= 0
	 */
	 private ApiItemStat stat = new ApiItemStat();

	/**
	 * 库存相关
	 *
	 * 版本 >= 0
	 */
	 private Vector<ApiStock> stock = new Vector<ApiStock>();

	/**
	 * 拍卖相关
	 *
	 * 版本 >= 0
	 */
	 private ApiAuction auction = new ApiAuction();

	/**
	 * EMS价格
	 *
	 * 版本 >= 0
	 */
	 private long emsMailPrice;

	/**
	 * 商品被编辑且主图被修改的时间
	 *
	 * 版本 >= 0
	 */
	 private long resetTime;

	/**
	 * 当期销售量
	 *
	 * 版本 >= 0
	 */
	 private long resetPayedNum;

	/**
	 * 商家编码localcode
	 *
	 * 版本 >= 0
	 */
	 private String stockId = new String();

	/**
	 * 保留字
	 *
	 * 版本 >= 0
	 */
	 private String relatedCombos = new String();

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 版本 >= 0
	 */
	 private short snapVersion_u;

	/**
	 * 版本 >= 0
	 */
	 private short qQUin_u;

	/**
	 * 版本 >= 0
	 */
	 private short leafClassId_u;

	/**
	 * 版本 >= 0
	 */
	 private short shopClassId_u;

	/**
	 * 版本 >= 0
	 */
	 private short city_u;

	/**
	 * 版本 >= 0
	 */
	 private short province_u;

	/**
	 * 版本 >= 0
	 */
	 private short country_u;

	/**
	 * 版本 >= 0
	 */
	 private short price_u;

	/**
	 * 版本 >= 0
	 */
	 private short num_u;

	/**
	 * 版本 >= 0
	 */
	 private short newType_u;

	/**
	 * 版本 >= 0
	 */
	 private short dealType_u;

	/**
	 * 版本 >= 0
	 */
	 private short supportPayAgency_u;

	/**
	 * 版本 >= 0
	 */
	 private short isRecommend_u;

	/**
	 * 版本 >= 0
	 */
	 private short state_u;

	/**
	 * 版本 >= 0
	 */
	 private short brand_u;

	/**
	 * 版本 >= 0
	 */
	 private short spec_u;

	/**
	 * 版本 >= 0
	 */
	 private short producer_u;

	/**
	 * 版本 >= 0
	 */
	 private short validTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short transportPriceType_u;

	/**
	 * 版本 >= 0
	 */
	 private short sendType_u;

	/**
	 * 版本 >= 0
	 */
	 private short normalMailPrice_u;

	/**
	 * 版本 >= 0
	 */
	 private short expressMailPrice_u;

	/**
	 * 版本 >= 0
	 */
	 private short buyLimit_u;

	/**
	 * 版本 >= 0
	 */
	 private short customStyleType_u;

	/**
	 * 版本 >= 0
	 */
	 private short detailSize_u;

	/**
	 * 版本 >= 0
	 */
	 private short productId_u;

	/**
	 * 版本 >= 0
	 */
	 private short weight_u;

	/**
	 * 版本 >= 0
	 */
	 private short uperUin_u;

	/**
	 * 版本 >= 0
	 */
	 private short uperItemId_u;

	/**
	 * 版本 >= 0
	 */
	 private short reloadCnt_u;

	/**
	 * 版本 >= 0
	 */
	 private short descFilePos_u;

	/**
	 * 版本 >= 0
	 */
	 private short itemId_u;

	/**
	 * 版本 >= 0
	 */
	 private short snapId_u;

	/**
	 * 版本 >= 0
	 */
	 private short qQNick_u;

	/**
	 * 版本 >= 0
	 */
	 private short shopId_u;

	/**
	 * 版本 >= 0
	 */
	 private short title_u;

	/**
	 * 版本 >= 0
	 */
	 private short property_u;

	/**
	 * 版本 >= 0
	 */
	 private short relatedItems_u;

	/**
	 * 版本 >= 0
	 */
	 private short attrText_u;

	/**
	 * 版本 >= 0
	 */
	 private short logo_u;

	/**
	 * 版本 >= 0
	 */
	 private short extMap_u;

	/**
	 * 版本 >= 0
	 */
	 private short stat_u;

	/**
	 * 版本 >= 0
	 */
	 private short stock_u;

	/**
	 * 版本 >= 0
	 */
	 private short auction_u;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private short emsMailPrice_u;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private short resetTime_u;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private short resetPayedNum_u;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private short stockId_u;

	/**
	 * 版本 >= 0
	 */
	 private short relatedCombos_u;

	/**
	 * 货到付款COD
	 *
	 * 版本 >= 20110908
	 */
	 private long CODId;

	/**
	 * 
	 *
	 * 版本 >= 20110908
	 */
	 private short CODId_u;

	/**
	 * 新的店铺自定义分类
	 *
	 * 版本 >= 20111215
	 */
	 private String shopCategory = new String();

	/**
	 * 
	 *
	 * 版本 >= 20111215
	 */
	 private short shopCategory_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUInt(snapVersion);
		bs.pushUInt(qQUin);
		bs.pushUInt(leafClassId);
		bs.pushUInt(city);
		bs.pushUInt(province);
		bs.pushUInt(country);
		bs.pushUInt(price);
		bs.pushUInt(num);
		bs.pushUInt(newType);
		bs.pushUInt(dealType);
		bs.pushUInt(supportPayAgency);
		bs.pushUInt(isRecommend);
		bs.pushUInt(state);
		bs.pushUInt(brand);
		bs.pushUInt(spec);
		bs.pushUInt(producer);
		bs.pushUInt(validTime);
		bs.pushUInt(transportPriceType);
		bs.pushUInt(sendType);
		bs.pushUInt(normalMailPrice);
		bs.pushUInt(expressMailPrice);
		bs.pushUInt(buyLimit);
		bs.pushUInt(customStyleType);
		bs.pushUInt(detailSize);
		bs.pushUInt(productId);
		bs.pushUInt(weight);
		bs.pushUInt(uperUin);
		bs.pushUInt(uperItemId);
		bs.pushUInt(reloadCnt);
		bs.pushString(shopClassId);
		bs.pushString(descFilePos);
		bs.pushString(itemId);
		bs.pushString(snapId);
		bs.pushString(qQNick);
		bs.pushString(shopId);
		bs.pushString(title);
		bs.pushObject(property);
		bs.pushString(relatedItems);
		bs.pushString(attrText);
		bs.pushObject(logo);
		bs.pushObject(extMap);
		bs.pushObject(stat);
		bs.pushObject(stock);
		bs.pushObject(auction);
		bs.pushUInt(emsMailPrice);
		bs.pushUInt(resetTime);
		bs.pushUInt(resetPayedNum);
		bs.pushString(stockId);
		bs.pushString(relatedCombos);
		bs.pushUByte(version_u);
		bs.pushUByte(snapVersion_u);
		bs.pushUByte(qQUin_u);
		bs.pushUByte(leafClassId_u);
		bs.pushUByte(shopClassId_u);
		bs.pushUByte(city_u);
		bs.pushUByte(province_u);
		bs.pushUByte(country_u);
		bs.pushUByte(price_u);
		bs.pushUByte(num_u);
		bs.pushUByte(newType_u);
		bs.pushUByte(dealType_u);
		bs.pushUByte(supportPayAgency_u);
		bs.pushUByte(isRecommend_u);
		bs.pushUByte(state_u);
		bs.pushUByte(brand_u);
		bs.pushUByte(spec_u);
		bs.pushUByte(producer_u);
		bs.pushUByte(validTime_u);
		bs.pushUByte(transportPriceType_u);
		bs.pushUByte(sendType_u);
		bs.pushUByte(normalMailPrice_u);
		bs.pushUByte(expressMailPrice_u);
		bs.pushUByte(buyLimit_u);
		bs.pushUByte(customStyleType_u);
		bs.pushUByte(detailSize_u);
		bs.pushUByte(productId_u);
		bs.pushUByte(weight_u);
		bs.pushUByte(uperUin_u);
		bs.pushUByte(uperItemId_u);
		bs.pushUByte(reloadCnt_u);
		bs.pushUByte(descFilePos_u);
		bs.pushUByte(itemId_u);
		bs.pushUByte(snapId_u);
		bs.pushUByte(qQNick_u);
		bs.pushUByte(shopId_u);
		bs.pushUByte(title_u);
		bs.pushUByte(property_u);
		bs.pushUByte(relatedItems_u);
		bs.pushUByte(attrText_u);
		bs.pushUByte(logo_u);
		bs.pushUByte(extMap_u);
		bs.pushUByte(stat_u);
		bs.pushUByte(stock_u);
		bs.pushUByte(auction_u);
		bs.pushUByte(emsMailPrice_u);
		bs.pushUByte(resetTime_u);
		bs.pushUByte(resetPayedNum_u);
		bs.pushUByte(stockId_u);
		bs.pushUByte(relatedCombos_u);
		if(  this.version >= 20110908 ){
				bs.pushUInt(CODId);
		}
		if(  this.version >= 20110908 ){
				bs.pushUByte(CODId_u);
		}
		if(  this.version >= 20111215 ){
				bs.pushString(shopCategory);
		}
		if(  this.version >= 20111215 ){
				bs.pushUByte(shopCategory_u);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		snapVersion = bs.popUInt();
		qQUin = bs.popUInt();
		leafClassId = bs.popUInt();
		city = bs.popUInt();
		province = bs.popUInt();
		country = bs.popUInt();
		price = bs.popUInt();
		num = bs.popUInt();
		newType = bs.popUInt();
		dealType = bs.popUInt();
		supportPayAgency = bs.popUInt();
		isRecommend = bs.popUInt();
		state = bs.popUInt();
		brand = bs.popUInt();
		spec = bs.popUInt();
		producer = bs.popUInt();
		validTime = bs.popUInt();
		transportPriceType = bs.popUInt();
		sendType = bs.popUInt();
		normalMailPrice = bs.popUInt();
		expressMailPrice = bs.popUInt();
		buyLimit = bs.popUInt();
		customStyleType = bs.popUInt();
		detailSize = bs.popUInt();
		productId = bs.popUInt();
		weight = bs.popUInt();
		uperUin = bs.popUInt();
		uperItemId = bs.popUInt();
		reloadCnt = bs.popUInt();
		shopClassId = bs.popString();
		descFilePos = bs.popString();
		itemId = bs.popString();
		snapId = bs.popString();
		qQNick = bs.popString();
		shopId = bs.popString();
		title = bs.popString();
		property = (Map<uint16_t,uint8_t>)bs.popMap(uint16_t.class,uint8_t.class);
		relatedItems = bs.popString();
		attrText = bs.popString();
		logo = (Vector<String>)bs.popVector(String.class);
		extMap = (MultiMap<String,String>)bs.popMultiMap(String.class,String.class);
		stat = (ApiItemStat) bs.popObject(ApiItemStat.class);
		stock = (Vector<ApiStock>)bs.popVector(ApiStock.class);
		auction = (ApiAuction) bs.popObject(ApiAuction.class);
		emsMailPrice = bs.popUInt();
		resetTime = bs.popUInt();
		resetPayedNum = bs.popUInt();
		stockId = bs.popString();
		relatedCombos = bs.popString();
		version_u = bs.popUByte();
		snapVersion_u = bs.popUByte();
		qQUin_u = bs.popUByte();
		leafClassId_u = bs.popUByte();
		shopClassId_u = bs.popUByte();
		city_u = bs.popUByte();
		province_u = bs.popUByte();
		country_u = bs.popUByte();
		price_u = bs.popUByte();
		num_u = bs.popUByte();
		newType_u = bs.popUByte();
		dealType_u = bs.popUByte();
		supportPayAgency_u = bs.popUByte();
		isRecommend_u = bs.popUByte();
		state_u = bs.popUByte();
		brand_u = bs.popUByte();
		spec_u = bs.popUByte();
		producer_u = bs.popUByte();
		validTime_u = bs.popUByte();
		transportPriceType_u = bs.popUByte();
		sendType_u = bs.popUByte();
		normalMailPrice_u = bs.popUByte();
		expressMailPrice_u = bs.popUByte();
		buyLimit_u = bs.popUByte();
		customStyleType_u = bs.popUByte();
		detailSize_u = bs.popUByte();
		productId_u = bs.popUByte();
		weight_u = bs.popUByte();
		uperUin_u = bs.popUByte();
		uperItemId_u = bs.popUByte();
		reloadCnt_u = bs.popUByte();
		descFilePos_u = bs.popUByte();
		itemId_u = bs.popUByte();
		snapId_u = bs.popUByte();
		qQNick_u = bs.popUByte();
		shopId_u = bs.popUByte();
		title_u = bs.popUByte();
		property_u = bs.popUByte();
		relatedItems_u = bs.popUByte();
		attrText_u = bs.popUByte();
		logo_u = bs.popUByte();
		extMap_u = bs.popUByte();
		stat_u = bs.popUByte();
		stock_u = bs.popUByte();
		auction_u = bs.popUByte();
		emsMailPrice_u = bs.popUByte();
		resetTime_u = bs.popUByte();
		resetPayedNum_u = bs.popUByte();
		stockId_u = bs.popUByte();
		relatedCombos_u = bs.popUByte();
		if(  this.version >= 20110908 ){
				CODId = bs.popUInt();
		}
		if(  this.version >= 20110908 ){
				CODId_u = bs.popUByte();
		}
		if(  this.version >= 20111215 ){
				shopCategory = bs.popString();
		}
		if(  this.version >= 20111215 ){
				shopCategory_u = bs.popUByte();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取快照版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return snapVersion value 类型为:long
	 * 
	 */
	public long getSnapVersion()
	{
		return snapVersion;
	}


	/**
	 * 设置快照版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSnapVersion(long value)
	{
		this.snapVersion = value;
		this.snapVersion_u = 1;
	}


	/**
	 * 获取卖家Uin
	 * 
	 * 此字段的版本 >= 0
	 * @return qQUin value 类型为:long
	 * 
	 */
	public long getQQUin()
	{
		return qQUin;
	}


	/**
	 * 设置卖家Uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setQQUin(long value)
	{
		this.qQUin = value;
		this.qQUin_u = 1;
	}


	/**
	 * 获取品类(类目)ID
	 * 
	 * 此字段的版本 >= 0
	 * @return leafClassId value 类型为:long
	 * 
	 */
	public long getLeafClassId()
	{
		return leafClassId;
	}


	/**
	 * 设置品类(类目)ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLeafClassId(long value)
	{
		this.leafClassId = value;
		this.leafClassId_u = 1;
	}


	/**
	 * 获取城市
	 * 
	 * 此字段的版本 >= 0
	 * @return city value 类型为:long
	 * 
	 */
	public long getCity()
	{
		return city;
	}


	/**
	 * 设置城市
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCity(long value)
	{
		this.city = value;
		this.city_u = 1;
	}


	/**
	 * 获取省份
	 * 
	 * 此字段的版本 >= 0
	 * @return province value 类型为:long
	 * 
	 */
	public long getProvince()
	{
		return province;
	}


	/**
	 * 设置省份
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setProvince(long value)
	{
		this.province = value;
		this.province_u = 1;
	}


	/**
	 * 获取国家
	 * 
	 * 此字段的版本 >= 0
	 * @return country value 类型为:long
	 * 
	 */
	public long getCountry()
	{
		return country;
	}


	/**
	 * 设置国家
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCountry(long value)
	{
		this.country = value;
		this.country_u = 1;
	}


	/**
	 * 获取商品单价
	 * 
	 * 此字段的版本 >= 0
	 * @return price value 类型为:long
	 * 
	 */
	public long getPrice()
	{
		return price;
	}


	/**
	 * 设置商品单价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPrice(long value)
	{
		this.price = value;
		this.price_u = 1;
	}


	/**
	 * 获取商品数量
	 * 
	 * 此字段的版本 >= 0
	 * @return num value 类型为:long
	 * 
	 */
	public long getNum()
	{
		return num;
	}


	/**
	 * 设置商品数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNum(long value)
	{
		this.num = value;
		this.num_u = 1;
	}


	/**
	 * 获取商品新旧程度 值见{@link com.paipai.c2c.item.constants.ItemConstants}
	 * 
	 * 此字段的版本 >= 0
	 * @return newType value 类型为:long
	 * 
	 */
	public long getNewType()
	{
		return newType;
	}


	/**
	 * 设置商品新旧程度 值见{@link com.paipai.c2c.item.constants.ItemConstants}
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNewType(long value)
	{
		this.newType = value;
		this.newType_u = 1;
	}


	/**
	 * 获取出售方式 值见{@link com.paipai.c2c.item.constants.ItemConstants}
	 * 
	 * 此字段的版本 >= 0
	 * @return dealType value 类型为:long
	 * 
	 */
	public long getDealType()
	{
		return dealType;
	}


	/**
	 * 设置出售方式 值见{@link com.paipai.c2c.item.constants.ItemConstants}
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealType(long value)
	{
		this.dealType = value;
		this.dealType_u = 1;
	}


	/**
	 * 获取是否支持财付通
	 * 
	 * 此字段的版本 >= 0
	 * @return supportPayAgency value 类型为:long
	 * 
	 */
	public long getSupportPayAgency()
	{
		return supportPayAgency;
	}


	/**
	 * 设置是否支持财付通
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSupportPayAgency(long value)
	{
		this.supportPayAgency = value;
		this.supportPayAgency_u = 1;
	}


	/**
	 * 获取店铺推荐
	 * 
	 * 此字段的版本 >= 0
	 * @return isRecommend value 类型为:long
	 * 
	 */
	public long getIsRecommend()
	{
		return isRecommend;
	}


	/**
	 * 设置店铺推荐
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setIsRecommend(long value)
	{
		this.isRecommend = value;
		this.isRecommend_u = 1;
	}


	/**
	 * 获取商品状态 值见{@link com.paipai.c2c.item.constants.ItemConstants}
	 * 
	 * 此字段的版本 >= 0
	 * @return state value 类型为:long
	 * 
	 */
	public long getState()
	{
		return state;
	}


	/**
	 * 设置商品状态 值见{@link com.paipai.c2c.item.constants.ItemConstants}
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setState(long value)
	{
		this.state = value;
		this.state_u = 1;
	}


	/**
	 * 获取商品品牌id
	 * 
	 * 此字段的版本 >= 0
	 * @return brand value 类型为:long
	 * 
	 */
	public long getBrand()
	{
		return brand;
	}


	/**
	 * 设置商品品牌id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBrand(long value)
	{
		this.brand = value;
		this.brand_u = 1;
	}


	/**
	 * 获取商品规格id
	 * 
	 * 此字段的版本 >= 0
	 * @return spec value 类型为:long
	 * 
	 */
	public long getSpec()
	{
		return spec;
	}


	/**
	 * 设置商品规格id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSpec(long value)
	{
		this.spec = value;
		this.spec_u = 1;
	}


	/**
	 * 获取商品生产商id
	 * 
	 * 此字段的版本 >= 0
	 * @return producer value 类型为:long
	 * 
	 */
	public long getProducer()
	{
		return producer;
	}


	/**
	 * 设置商品生产商id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setProducer(long value)
	{
		this.producer = value;
		this.producer_u = 1;
	}


	/**
	 * 获取有效时间
	 * 
	 * 此字段的版本 >= 0
	 * @return validTime value 类型为:long
	 * 
	 */
	public long getValidTime()
	{
		return validTime;
	}


	/**
	 * 设置有效时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setValidTime(long value)
	{
		this.validTime = value;
		this.validTime_u = 1;
	}


	/**
	 * 获取运费承担方式 值见{@link com.paipai.c2c.item.constants.ItemConstants#TRANSPORT_SELLER_PAY}等
	 * 
	 * 此字段的版本 >= 0
	 * @return transportPriceType value 类型为:long
	 * 
	 */
	public long getTransportPriceType()
	{
		return transportPriceType;
	}


	/**
	 * 设置运费承担方式 值见{@link com.paipai.c2c.item.constants.ItemConstants#TRANSPORT_SELLER_PAY}等
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTransportPriceType(long value)
	{
		this.transportPriceType = value;
		this.transportPriceType_u = 1;
	}


	/**
	 * 获取商品发货方式 值见{@link com.paipai.c2c.item.constants.ItemConstants#SEND_TYPE_MONEY}等
	 * 
	 * 此字段的版本 >= 0
	 * @return sendType value 类型为:long
	 * 
	 */
	public long getSendType()
	{
		return sendType;
	}


	/**
	 * 设置商品发货方式 值见{@link com.paipai.c2c.item.constants.ItemConstants#SEND_TYPE_MONEY}等
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSendType(long value)
	{
		this.sendType = value;
		this.sendType_u = 1;
	}


	/**
	 * 获取平邮价格
	 * 
	 * 此字段的版本 >= 0
	 * @return normalMailPrice value 类型为:long
	 * 
	 */
	public long getNormalMailPrice()
	{
		return normalMailPrice;
	}


	/**
	 * 设置平邮价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNormalMailPrice(long value)
	{
		this.normalMailPrice = value;
		this.normalMailPrice_u = 1;
	}


	/**
	 * 获取快递价格
	 * 
	 * 此字段的版本 >= 0
	 * @return expressMailPrice value 类型为:long
	 * 
	 */
	public long getExpressMailPrice()
	{
		return expressMailPrice;
	}


	/**
	 * 设置快递价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setExpressMailPrice(long value)
	{
		this.expressMailPrice = value;
		this.expressMailPrice_u = 1;
	}


	/**
	 * 获取商品限制购买数量,0表示不限制
	 * 
	 * 此字段的版本 >= 0
	 * @return buyLimit value 类型为:long
	 * 
	 */
	public long getBuyLimit()
	{
		return buyLimit;
	}


	/**
	 * 设置商品限制购买数量,0表示不限制
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyLimit(long value)
	{
		this.buyLimit = value;
		this.buyLimit_u = 1;
	}


	/**
	 * 获取商品详情页面颜色主题
	 * 
	 * 此字段的版本 >= 0
	 * @return customStyleType value 类型为:long
	 * 
	 */
	public long getCustomStyleType()
	{
		return customStyleType;
	}


	/**
	 * 设置商品详情页面颜色主题
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCustomStyleType(long value)
	{
		this.customStyleType = value;
		this.customStyleType_u = 1;
	}


	/**
	 * 获取商品详情文件大小
	 * 
	 * 此字段的版本 >= 0
	 * @return detailSize value 类型为:long
	 * 
	 */
	public long getDetailSize()
	{
		return detailSize;
	}


	/**
	 * 设置商品详情文件大小
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDetailSize(long value)
	{
		this.detailSize = value;
		this.detailSize_u = 1;
	}


	/**
	 * 获取产品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return productId value 类型为:long
	 * 
	 */
	public long getProductId()
	{
		return productId;
	}


	/**
	 * 设置产品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setProductId(long value)
	{
		this.productId = value;
		this.productId_u = 1;
	}


	/**
	 * 获取商品重量 预留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return weight value 类型为:long
	 * 
	 */
	public long getWeight()
	{
		return weight;
	}


	/**
	 * 设置商品重量 预留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWeight(long value)
	{
		this.weight = value;
		this.weight_u = 1;
	}


	/**
	 * 获取上级分销商相关,没有使用
	 * 
	 * 此字段的版本 >= 0
	 * @return uperUin value 类型为:long
	 * 
	 */
	public long getUperUin()
	{
		return uperUin;
	}


	/**
	 * 设置上级分销商相关,没有使用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUperUin(long value)
	{
		this.uperUin = value;
		this.uperUin_u = 1;
	}


	/**
	 * 获取上级分销商相关,没有使用
	 * 
	 * 此字段的版本 >= 0
	 * @return uperItemId value 类型为:long
	 * 
	 */
	public long getUperItemId()
	{
		return uperItemId;
	}


	/**
	 * 设置上级分销商相关,没有使用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUperItemId(long value)
	{
		this.uperItemId = value;
		this.uperItemId_u = 1;
	}


	/**
	 * 获取重新上架次数
	 * 
	 * 此字段的版本 >= 0
	 * @return reloadCnt value 类型为:long
	 * 
	 */
	public long getReloadCnt()
	{
		return reloadCnt;
	}


	/**
	 * 设置重新上架次数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setReloadCnt(long value)
	{
		this.reloadCnt = value;
		this.reloadCnt_u = 1;
	}


	/**
	 * 获取店铺自定义分类
	 * 
	 * 此字段的版本 >= 0
	 * @return shopClassId value 类型为:String
	 * 
	 */
	public String getShopClassId()
	{
		return shopClassId;
	}


	/**
	 * 设置店铺自定义分类
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setShopClassId(String value)
	{
		if (value != null) {
				this.shopClassId = value;
				this.shopClassId_u = 1;
		}
	}


	/**
	 * 获取商品详情文件名
	 * 
	 * 此字段的版本 >= 0
	 * @return descFilePos value 类型为:String
	 * 
	 */
	public String getDescFilePos()
	{
		return descFilePos;
	}


	/**
	 * 设置商品详情文件名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDescFilePos(String value)
	{
		if (value != null) {
				this.descFilePos = value;
				this.descFilePos_u = 1;
		}
	}


	/**
	 * 获取商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return itemId value 类型为:String
	 * 
	 */
	public String getItemId()
	{
		return itemId;
	}


	/**
	 * 设置商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemId(String value)
	{
		if (value != null) {
				this.itemId = value;
				this.itemId_u = 1;
		}
	}


	/**
	 * 获取商品快照ID
	 * 
	 * 此字段的版本 >= 0
	 * @return snapId value 类型为:String
	 * 
	 */
	public String getSnapId()
	{
		return snapId;
	}


	/**
	 * 设置商品快照ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSnapId(String value)
	{
		if (value != null) {
				this.snapId = value;
				this.snapId_u = 1;
		}
	}


	/**
	 * 获取卖家昵称
	 * 
	 * 此字段的版本 >= 0
	 * @return qQNick value 类型为:String
	 * 
	 */
	public String getQQNick()
	{
		return qQNick;
	}


	/**
	 * 设置卖家昵称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setQQNick(String value)
	{
		if (value != null) {
				this.qQNick = value;
				this.qQNick_u = 1;
		}
	}


	/**
	 * 获取卖家店铺ID
	 * 
	 * 此字段的版本 >= 0
	 * @return shopId value 类型为:String
	 * 
	 */
	public String getShopId()
	{
		return shopId;
	}


	/**
	 * 设置卖家店铺ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setShopId(String value)
	{
		if (value != null) {
				this.shopId = value;
				this.shopId_u = 1;
		}
	}


	/**
	 * 获取商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @return title value 类型为:String
	 * 
	 */
	public String getTitle()
	{
		return title;
	}


	/**
	 * 设置商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setTitle(String value)
	{
		if (value != null) {
				this.title = value;
				this.title_u = 1;
		}
	}


	/**
	 * 获取商品所有属性,使用方法{@link com.paipai.c2c.item.constants.ItemConstants}
	 * 
	 * 此字段的版本 >= 0
	 * @return property value 类型为:Map<uint16_t,uint8_t>
	 * 
	 */
	public Map<uint16_t,uint8_t> getProperty()
	{
		return property;
	}


	/**
	 * 设置商品所有属性,使用方法{@link com.paipai.c2c.item.constants.ItemConstants}
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<uint16_t,uint8_t>
	 * 
	 */
	public void setProperty(Map<uint16_t,uint8_t> value)
	{
		if (value != null) {
				this.property = value;
				this.property_u = 1;
		}
	}


	/**
	 * 获取推荐搭配商品ID列表,多个以|分隔
	 * 
	 * 此字段的版本 >= 0
	 * @return relatedItems value 类型为:String
	 * 
	 */
	public String getRelatedItems()
	{
		return relatedItems;
	}


	/**
	 * 设置推荐搭配商品ID列表,多个以|分隔
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRelatedItems(String value)
	{
		if (value != null) {
				this.relatedItems = value;
				this.relatedItems_u = 1;
		}
	}


	/**
	 * 获取类目属性串
	 * 
	 * 此字段的版本 >= 0
	 * @return attrText value 类型为:String
	 * 
	 */
	public String getAttrText()
	{
		return attrText;
	}


	/**
	 * 设置类目属性串
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAttrText(String value)
	{
		if (value != null) {
				this.attrText = value;
				this.attrText_u = 1;
		}
	}


	/**
	 * 获取商品图片列表
	 * 
	 * 此字段的版本 >= 0
	 * @return logo value 类型为:Vector<String>
	 * 
	 */
	public Vector<String> getLogo()
	{
		return logo;
	}


	/**
	 * 设置商品图片列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<String>
	 * 
	 */
	public void setLogo(Vector<String> value)
	{
		if (value != null) {
				this.logo = value;
				this.logo_u = 1;
		}
	}


	/**
	 * 获取商品扩展表字段 Key见{@link com.paipai.c2c.item.constants.ItemConstants}
	 * 
	 * 此字段的版本 >= 0
	 * @return extMap value 类型为:MultiMap<String,String>
	 * 
	 */
	public MultiMap<String,String> getExtMap()
	{
		return extMap;
	}


	/**
	 * 设置商品扩展表字段 Key见{@link com.paipai.c2c.item.constants.ItemConstants}
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:MultiMap<String,String>
	 * 
	 */
	public void setExtMap(MultiMap<String,String> value)
	{
		if (value != null) {
				this.extMap = value;
				this.extMap_u = 1;
		}
	}


	/**
	 * 获取统计信息，如售出数量等
	 * 
	 * 此字段的版本 >= 0
	 * @return stat value 类型为:ApiItemStat
	 * 
	 */
	public ApiItemStat getStat()
	{
		return stat;
	}


	/**
	 * 设置统计信息，如售出数量等
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ApiItemStat
	 * 
	 */
	public void setStat(ApiItemStat value)
	{
		if (value != null) {
				this.stat = value;
				this.stat_u = 1;
		}
	}


	/**
	 * 获取库存相关
	 * 
	 * 此字段的版本 >= 0
	 * @return stock value 类型为:Vector<ApiStock>
	 * 
	 */
	public Vector<ApiStock> getStock()
	{
		return stock;
	}


	/**
	 * 设置库存相关
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<ApiStock>
	 * 
	 */
	public void setStock(Vector<ApiStock> value)
	{
		if (value != null) {
				this.stock = value;
				this.stock_u = 1;
		}
	}


	/**
	 * 获取拍卖相关
	 * 
	 * 此字段的版本 >= 0
	 * @return auction value 类型为:ApiAuction
	 * 
	 */
	public ApiAuction getAuction()
	{
		return auction;
	}


	/**
	 * 设置拍卖相关
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ApiAuction
	 * 
	 */
	public void setAuction(ApiAuction value)
	{
		if (value != null) {
				this.auction = value;
				this.auction_u = 1;
		}
	}


	/**
	 * 获取EMS价格
	 * 
	 * 此字段的版本 >= 0
	 * @return emsMailPrice value 类型为:long
	 * 
	 */
	public long getEmsMailPrice()
	{
		return emsMailPrice;
	}


	/**
	 * 设置EMS价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEmsMailPrice(long value)
	{
		this.emsMailPrice = value;
		this.emsMailPrice_u = 1;
	}


	/**
	 * 获取商品被编辑且主图被修改的时间
	 * 
	 * 此字段的版本 >= 0
	 * @return resetTime value 类型为:long
	 * 
	 */
	public long getResetTime()
	{
		return resetTime;
	}


	/**
	 * 设置商品被编辑且主图被修改的时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setResetTime(long value)
	{
		this.resetTime = value;
		this.resetTime_u = 1;
	}


	/**
	 * 获取当期销售量
	 * 
	 * 此字段的版本 >= 0
	 * @return resetPayedNum value 类型为:long
	 * 
	 */
	public long getResetPayedNum()
	{
		return resetPayedNum;
	}


	/**
	 * 设置当期销售量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setResetPayedNum(long value)
	{
		this.resetPayedNum = value;
		this.resetPayedNum_u = 1;
	}


	/**
	 * 获取商家编码localcode
	 * 
	 * 此字段的版本 >= 0
	 * @return stockId value 类型为:String
	 * 
	 */
	public String getStockId()
	{
		return stockId;
	}


	/**
	 * 设置商家编码localcode
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStockId(String value)
	{
		if (value != null) {
				this.stockId = value;
				this.stockId_u = 1;
		}
	}


	/**
	 * 获取保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return relatedCombos value 类型为:String
	 * 
	 */
	public String getRelatedCombos()
	{
		return relatedCombos;
	}


	/**
	 * 设置保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRelatedCombos(String value)
	{
		if (value != null) {
				this.relatedCombos = value;
				this.relatedCombos_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return snapVersion_u value 类型为:short
	 * 
	 */
	public short getSnapVersion_u()
	{
		return snapVersion_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSnapVersion_u(short value)
	{
		this.snapVersion_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return qQUin_u value 类型为:short
	 * 
	 */
	public short getQQUin_u()
	{
		return qQUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setQQUin_u(short value)
	{
		this.qQUin_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return leafClassId_u value 类型为:short
	 * 
	 */
	public short getLeafClassId_u()
	{
		return leafClassId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLeafClassId_u(short value)
	{
		this.leafClassId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return shopClassId_u value 类型为:short
	 * 
	 */
	public short getShopClassId_u()
	{
		return shopClassId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopClassId_u(short value)
	{
		this.shopClassId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return city_u value 类型为:short
	 * 
	 */
	public short getCity_u()
	{
		return city_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCity_u(short value)
	{
		this.city_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return province_u value 类型为:short
	 * 
	 */
	public short getProvince_u()
	{
		return province_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProvince_u(short value)
	{
		this.province_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return country_u value 类型为:short
	 * 
	 */
	public short getCountry_u()
	{
		return country_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCountry_u(short value)
	{
		this.country_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return price_u value 类型为:short
	 * 
	 */
	public short getPrice_u()
	{
		return price_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPrice_u(short value)
	{
		this.price_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return num_u value 类型为:short
	 * 
	 */
	public short getNum_u()
	{
		return num_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNum_u(short value)
	{
		this.num_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return newType_u value 类型为:short
	 * 
	 */
	public short getNewType_u()
	{
		return newType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNewType_u(short value)
	{
		this.newType_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dealType_u value 类型为:short
	 * 
	 */
	public short getDealType_u()
	{
		return dealType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealType_u(short value)
	{
		this.dealType_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return supportPayAgency_u value 类型为:short
	 * 
	 */
	public short getSupportPayAgency_u()
	{
		return supportPayAgency_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSupportPayAgency_u(short value)
	{
		this.supportPayAgency_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return isRecommend_u value 类型为:short
	 * 
	 */
	public short getIsRecommend_u()
	{
		return isRecommend_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIsRecommend_u(short value)
	{
		this.isRecommend_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return state_u value 类型为:short
	 * 
	 */
	public short getState_u()
	{
		return state_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setState_u(short value)
	{
		this.state_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return brand_u value 类型为:short
	 * 
	 */
	public short getBrand_u()
	{
		return brand_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBrand_u(short value)
	{
		this.brand_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return spec_u value 类型为:short
	 * 
	 */
	public short getSpec_u()
	{
		return spec_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSpec_u(short value)
	{
		this.spec_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return producer_u value 类型为:short
	 * 
	 */
	public short getProducer_u()
	{
		return producer_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProducer_u(short value)
	{
		this.producer_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return validTime_u value 类型为:short
	 * 
	 */
	public short getValidTime_u()
	{
		return validTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setValidTime_u(short value)
	{
		this.validTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return transportPriceType_u value 类型为:short
	 * 
	 */
	public short getTransportPriceType_u()
	{
		return transportPriceType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTransportPriceType_u(short value)
	{
		this.transportPriceType_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sendType_u value 类型为:short
	 * 
	 */
	public short getSendType_u()
	{
		return sendType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSendType_u(short value)
	{
		this.sendType_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return normalMailPrice_u value 类型为:short
	 * 
	 */
	public short getNormalMailPrice_u()
	{
		return normalMailPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNormalMailPrice_u(short value)
	{
		this.normalMailPrice_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return expressMailPrice_u value 类型为:short
	 * 
	 */
	public short getExpressMailPrice_u()
	{
		return expressMailPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setExpressMailPrice_u(short value)
	{
		this.expressMailPrice_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return buyLimit_u value 类型为:short
	 * 
	 */
	public short getBuyLimit_u()
	{
		return buyLimit_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyLimit_u(short value)
	{
		this.buyLimit_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return customStyleType_u value 类型为:short
	 * 
	 */
	public short getCustomStyleType_u()
	{
		return customStyleType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCustomStyleType_u(short value)
	{
		this.customStyleType_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return detailSize_u value 类型为:short
	 * 
	 */
	public short getDetailSize_u()
	{
		return detailSize_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDetailSize_u(short value)
	{
		this.detailSize_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return productId_u value 类型为:short
	 * 
	 */
	public short getProductId_u()
	{
		return productId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProductId_u(short value)
	{
		this.productId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return weight_u value 类型为:short
	 * 
	 */
	public short getWeight_u()
	{
		return weight_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWeight_u(short value)
	{
		this.weight_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return uperUin_u value 类型为:short
	 * 
	 */
	public short getUperUin_u()
	{
		return uperUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUperUin_u(short value)
	{
		this.uperUin_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return uperItemId_u value 类型为:short
	 * 
	 */
	public short getUperItemId_u()
	{
		return uperItemId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUperItemId_u(short value)
	{
		this.uperItemId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return reloadCnt_u value 类型为:short
	 * 
	 */
	public short getReloadCnt_u()
	{
		return reloadCnt_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReloadCnt_u(short value)
	{
		this.reloadCnt_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return descFilePos_u value 类型为:short
	 * 
	 */
	public short getDescFilePos_u()
	{
		return descFilePos_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDescFilePos_u(short value)
	{
		this.descFilePos_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return itemId_u value 类型为:short
	 * 
	 */
	public short getItemId_u()
	{
		return itemId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemId_u(short value)
	{
		this.itemId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return snapId_u value 类型为:short
	 * 
	 */
	public short getSnapId_u()
	{
		return snapId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSnapId_u(short value)
	{
		this.snapId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return qQNick_u value 类型为:short
	 * 
	 */
	public short getQQNick_u()
	{
		return qQNick_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setQQNick_u(short value)
	{
		this.qQNick_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return shopId_u value 类型为:short
	 * 
	 */
	public short getShopId_u()
	{
		return shopId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopId_u(short value)
	{
		this.shopId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return title_u value 类型为:short
	 * 
	 */
	public short getTitle_u()
	{
		return title_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTitle_u(short value)
	{
		this.title_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return property_u value 类型为:short
	 * 
	 */
	public short getProperty_u()
	{
		return property_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProperty_u(short value)
	{
		this.property_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return relatedItems_u value 类型为:short
	 * 
	 */
	public short getRelatedItems_u()
	{
		return relatedItems_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRelatedItems_u(short value)
	{
		this.relatedItems_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return attrText_u value 类型为:short
	 * 
	 */
	public short getAttrText_u()
	{
		return attrText_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAttrText_u(short value)
	{
		this.attrText_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return logo_u value 类型为:short
	 * 
	 */
	public short getLogo_u()
	{
		return logo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLogo_u(short value)
	{
		this.logo_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return extMap_u value 类型为:short
	 * 
	 */
	public short getExtMap_u()
	{
		return extMap_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setExtMap_u(short value)
	{
		this.extMap_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return stat_u value 类型为:short
	 * 
	 */
	public short getStat_u()
	{
		return stat_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStat_u(short value)
	{
		this.stat_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return stock_u value 类型为:short
	 * 
	 */
	public short getStock_u()
	{
		return stock_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStock_u(short value)
	{
		this.stock_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return auction_u value 类型为:short
	 * 
	 */
	public short getAuction_u()
	{
		return auction_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAuction_u(short value)
	{
		this.auction_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return emsMailPrice_u value 类型为:short
	 * 
	 */
	public short getEmsMailPrice_u()
	{
		return emsMailPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEmsMailPrice_u(short value)
	{
		this.emsMailPrice_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return resetTime_u value 类型为:short
	 * 
	 */
	public short getResetTime_u()
	{
		return resetTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setResetTime_u(short value)
	{
		this.resetTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return resetPayedNum_u value 类型为:short
	 * 
	 */
	public short getResetPayedNum_u()
	{
		return resetPayedNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setResetPayedNum_u(short value)
	{
		this.resetPayedNum_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return stockId_u value 类型为:short
	 * 
	 */
	public short getStockId_u()
	{
		return stockId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStockId_u(short value)
	{
		this.stockId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return relatedCombos_u value 类型为:short
	 * 
	 */
	public short getRelatedCombos_u()
	{
		return relatedCombos_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRelatedCombos_u(short value)
	{
		this.relatedCombos_u = value;
	}


	/**
	 * 获取货到付款COD
	 * 
	 * 此字段的版本 >= 20110908
	 * @return CODId value 类型为:long
	 * 
	 */
	public long getCODId()
	{
		return CODId;
	}


	/**
	 * 设置货到付款COD
	 * 
	 * 此字段的版本 >= 20110908
	 * @param  value 类型为:long
	 * 
	 */
	public void setCODId(long value)
	{
		this.CODId = value;
		this.CODId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20110908
	 * @return CODId_u value 类型为:short
	 * 
	 */
	public short getCODId_u()
	{
		return CODId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20110908
	 * @param  value 类型为:short
	 * 
	 */
	public void setCODId_u(short value)
	{
		this.CODId_u = value;
	}


	/**
	 * 获取新的店铺自定义分类
	 * 
	 * 此字段的版本 >= 20111215
	 * @return shopCategory value 类型为:String
	 * 
	 */
	public String getShopCategory()
	{
		return shopCategory;
	}


	/**
	 * 设置新的店铺自定义分类
	 * 
	 * 此字段的版本 >= 20111215
	 * @param  value 类型为:String
	 * 
	 */
	public void setShopCategory(String value)
	{
		if (value != null) {
				this.shopCategory = value;
				this.shopCategory_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20111215
	 * @return shopCategory_u value 类型为:short
	 * 
	 */
	public short getShopCategory_u()
	{
		return shopCategory_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20111215
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopCategory_u(short value)
	{
		this.shopCategory_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ApiItem)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段snapVersion的长度 size_of(uint32_t)
				length += 4;  //计算字段qQUin的长度 size_of(uint32_t)
				length += 4;  //计算字段leafClassId的长度 size_of(uint32_t)
				length += 4;  //计算字段city的长度 size_of(uint32_t)
				length += 4;  //计算字段province的长度 size_of(uint32_t)
				length += 4;  //计算字段country的长度 size_of(uint32_t)
				length += 4;  //计算字段price的长度 size_of(uint32_t)
				length += 4;  //计算字段num的长度 size_of(uint32_t)
				length += 4;  //计算字段newType的长度 size_of(uint32_t)
				length += 4;  //计算字段dealType的长度 size_of(uint32_t)
				length += 4;  //计算字段supportPayAgency的长度 size_of(uint32_t)
				length += 4;  //计算字段isRecommend的长度 size_of(uint32_t)
				length += 4;  //计算字段state的长度 size_of(uint32_t)
				length += 4;  //计算字段brand的长度 size_of(uint32_t)
				length += 4;  //计算字段spec的长度 size_of(uint32_t)
				length += 4;  //计算字段producer的长度 size_of(uint32_t)
				length += 4;  //计算字段validTime的长度 size_of(uint32_t)
				length += 4;  //计算字段transportPriceType的长度 size_of(uint32_t)
				length += 4;  //计算字段sendType的长度 size_of(uint32_t)
				length += 4;  //计算字段normalMailPrice的长度 size_of(uint32_t)
				length += 4;  //计算字段expressMailPrice的长度 size_of(uint32_t)
				length += 4;  //计算字段buyLimit的长度 size_of(uint32_t)
				length += 4;  //计算字段customStyleType的长度 size_of(uint32_t)
				length += 4;  //计算字段detailSize的长度 size_of(uint32_t)
				length += 4;  //计算字段productId的长度 size_of(uint32_t)
				length += 4;  //计算字段weight的长度 size_of(uint32_t)
				length += 4;  //计算字段uperUin的长度 size_of(uint32_t)
				length += 4;  //计算字段uperItemId的长度 size_of(uint32_t)
				length += 4;  //计算字段reloadCnt的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(shopClassId);  //计算字段shopClassId的长度 size_of(String)
				length += ByteStream.getObjectSize(descFilePos);  //计算字段descFilePos的长度 size_of(String)
				length += ByteStream.getObjectSize(itemId);  //计算字段itemId的长度 size_of(String)
				length += ByteStream.getObjectSize(snapId);  //计算字段snapId的长度 size_of(String)
				length += ByteStream.getObjectSize(qQNick);  //计算字段qQNick的长度 size_of(String)
				length += ByteStream.getObjectSize(shopId);  //计算字段shopId的长度 size_of(String)
				length += ByteStream.getObjectSize(title);  //计算字段title的长度 size_of(String)
				length += ByteStream.getObjectSize(property);  //计算字段property的长度 size_of(Map)
				length += ByteStream.getObjectSize(relatedItems);  //计算字段relatedItems的长度 size_of(String)
				length += ByteStream.getObjectSize(attrText);  //计算字段attrText的长度 size_of(String)
				length += ByteStream.getObjectSize(logo);  //计算字段logo的长度 size_of(Vector)
				length += ByteStream.getObjectSize(extMap);  //计算字段extMap的长度 size_of(MultiMap)
				length += ByteStream.getObjectSize(stat);  //计算字段stat的长度 size_of(ApiItemStat)
				length += ByteStream.getObjectSize(stock);  //计算字段stock的长度 size_of(Vector)
				length += ByteStream.getObjectSize(auction);  //计算字段auction的长度 size_of(ApiAuction)
				length += 4;  //计算字段emsMailPrice的长度 size_of(uint32_t)
				length += 4;  //计算字段resetTime的长度 size_of(uint32_t)
				length += 4;  //计算字段resetPayedNum的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(stockId);  //计算字段stockId的长度 size_of(String)
				length += ByteStream.getObjectSize(relatedCombos);  //计算字段relatedCombos的长度 size_of(String)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段snapVersion_u的长度 size_of(uint8_t)
				length += 1;  //计算字段qQUin_u的长度 size_of(uint8_t)
				length += 1;  //计算字段leafClassId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段shopClassId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段city_u的长度 size_of(uint8_t)
				length += 1;  //计算字段province_u的长度 size_of(uint8_t)
				length += 1;  //计算字段country_u的长度 size_of(uint8_t)
				length += 1;  //计算字段price_u的长度 size_of(uint8_t)
				length += 1;  //计算字段num_u的长度 size_of(uint8_t)
				length += 1;  //计算字段newType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段dealType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段supportPayAgency_u的长度 size_of(uint8_t)
				length += 1;  //计算字段isRecommend_u的长度 size_of(uint8_t)
				length += 1;  //计算字段state_u的长度 size_of(uint8_t)
				length += 1;  //计算字段brand_u的长度 size_of(uint8_t)
				length += 1;  //计算字段spec_u的长度 size_of(uint8_t)
				length += 1;  //计算字段producer_u的长度 size_of(uint8_t)
				length += 1;  //计算字段validTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段transportPriceType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段sendType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段normalMailPrice_u的长度 size_of(uint8_t)
				length += 1;  //计算字段expressMailPrice_u的长度 size_of(uint8_t)
				length += 1;  //计算字段buyLimit_u的长度 size_of(uint8_t)
				length += 1;  //计算字段customStyleType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段detailSize_u的长度 size_of(uint8_t)
				length += 1;  //计算字段productId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段weight_u的长度 size_of(uint8_t)
				length += 1;  //计算字段uperUin_u的长度 size_of(uint8_t)
				length += 1;  //计算字段uperItemId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reloadCnt_u的长度 size_of(uint8_t)
				length += 1;  //计算字段descFilePos_u的长度 size_of(uint8_t)
				length += 1;  //计算字段itemId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段snapId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段qQNick_u的长度 size_of(uint8_t)
				length += 1;  //计算字段shopId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段title_u的长度 size_of(uint8_t)
				length += 1;  //计算字段property_u的长度 size_of(uint8_t)
				length += 1;  //计算字段relatedItems_u的长度 size_of(uint8_t)
				length += 1;  //计算字段attrText_u的长度 size_of(uint8_t)
				length += 1;  //计算字段logo_u的长度 size_of(uint8_t)
				length += 1;  //计算字段extMap_u的长度 size_of(uint8_t)
				length += 1;  //计算字段stat_u的长度 size_of(uint8_t)
				length += 1;  //计算字段stock_u的长度 size_of(uint8_t)
				length += 1;  //计算字段auction_u的长度 size_of(uint8_t)
				length += 1;  //计算字段emsMailPrice_u的长度 size_of(uint8_t)
				length += 1;  //计算字段resetTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段resetPayedNum_u的长度 size_of(uint8_t)
				length += 1;  //计算字段stockId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段relatedCombos_u的长度 size_of(uint8_t)
				if(  this.version >= 20110908 ){
						length += 4;  //计算字段CODId的长度 size_of(uint32_t)
				}
				if(  this.version >= 20110908 ){
						length += 1;  //计算字段CODId_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20111215 ){
						length += ByteStream.getObjectSize(shopCategory);  //计算字段shopCategory的长度 size_of(String)
				}
				if(  this.version >= 20111215 ){
						length += 1;  //计算字段shopCategory_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本20110908所包含的字段*******
 *	long version;///<版本
 *	long snapVersion;///<快照版本号
 *	long qQUin;///<卖家Uin
 *	long leafClassId;///<品类(类目)ID
 *	long city;///<城市
 *	long province;///<省份
 *	long country;///<国家
 *	long price;///<商品单价
 *	long num;///<商品数量
 *	long newType;///<商品新旧程度 值见{@link com.paipai.c2c.item.constants.ItemConstants}
 *	long dealType;///<出售方式 值见{@link com.paipai.c2c.item.constants.ItemConstants}
 *	long supportPayAgency;///<是否支持财付通
 *	long isRecommend;///<店铺推荐
 *	long state;///<商品状态 值见{@link com.paipai.c2c.item.constants.ItemConstants}
 *	long brand;///<商品品牌id
 *	long spec;///<商品规格id
 *	long producer;///<商品生产商id
 *	long validTime;///<有效时间
 *	long transportPriceType;///<运费承担方式 值见{@link com.paipai.c2c.item.constants.ItemConstants#TRANSPORT_SELLER_PAY}等
 *	long sendType;///<商品发货方式 值见{@link com.paipai.c2c.item.constants.ItemConstants#SEND_TYPE_MONEY}等
 *	long normalMailPrice;///<平邮价格
 *	long expressMailPrice;///<快递价格
 *	long buyLimit;///<商品限制购买数量,0表示不限制
 *	long customStyleType;///<商品详情页面颜色主题
 *	long detailSize;///<商品详情文件大小
 *	long productId;///<产品ID
 *	long weight;///<商品重量 预留字段
 *	long uperUin;///<上级分销商相关,没有使用
 *	long uperItemId;///<上级分销商相关,没有使用
 *	long reloadCnt;///<重新上架次数
 *	String shopClassId;///<店铺自定义分类
 *	String descFilePos;///<商品详情文件名
 *	String itemId;///<商品ID
 *	String snapId;///<商品快照ID
 *	String qQNick;///<卖家昵称
 *	String shopId;///<卖家店铺ID
 *	String title;///<商品名称
 *	Map<uint16_t,uint8_t> property;///<商品所有属性,使用方法{@link com.paipai.c2c.item.constants.ItemConstants}
 *	String relatedItems;///<推荐搭配商品ID列表,多个以|分隔
 *	String attrText;///<类目属性串
 *	Vector<String> logo;///<商品图片列表
 *	MultiMap<String,String> extMap;///<商品扩展表字段 Key见{@link com.paipai.c2c.item.constants.ItemConstants}
 *	ApiItemStat stat;///<统计信息，如售出数量等
 *	Vector<ApiStock> stock;///<库存相关
 *	ApiAuction auction;///<拍卖相关
 *	long emsMailPrice;///<EMS价格
 *	long resetTime;///<商品被编辑且主图被修改的时间
 *	long resetPayedNum;///<当期销售量
 *	String stockId;///<商家编码localcode
 *	String relatedCombos;///<保留字
 *	short version_u;
 *	short snapVersion_u;
 *	short qQUin_u;
 *	short leafClassId_u;
 *	short shopClassId_u;
 *	short city_u;
 *	short province_u;
 *	short country_u;
 *	short price_u;
 *	short num_u;
 *	short newType_u;
 *	short dealType_u;
 *	short supportPayAgency_u;
 *	short isRecommend_u;
 *	short state_u;
 *	short brand_u;
 *	short spec_u;
 *	short producer_u;
 *	short validTime_u;
 *	short transportPriceType_u;
 *	short sendType_u;
 *	short normalMailPrice_u;
 *	short expressMailPrice_u;
 *	short buyLimit_u;
 *	short customStyleType_u;
 *	short detailSize_u;
 *	short productId_u;
 *	short weight_u;
 *	short uperUin_u;
 *	short uperItemId_u;
 *	short reloadCnt_u;
 *	short descFilePos_u;
 *	short itemId_u;
 *	short snapId_u;
 *	short qQNick_u;
 *	short shopId_u;
 *	short title_u;
 *	short property_u;
 *	short relatedItems_u;
 *	short attrText_u;
 *	short logo_u;
 *	short extMap_u;
 *	short stat_u;
 *	short stock_u;
 *	short auction_u;
 *	short emsMailPrice_u;
 *	short resetTime_u;
 *	short resetPayedNum_u;
 *	short stockId_u;
 *	short relatedCombos_u;
 *	long CODId;///<货到付款COD
 *	short CODId_u;
 *****以上是版本20110908所包含的字段*******
 *
 *****以下是版本20111215所包含的字段*******
 *	long version;///<版本
 *	long snapVersion;///<快照版本号
 *	long qQUin;///<卖家Uin
 *	long leafClassId;///<品类(类目)ID
 *	long city;///<城市
 *	long province;///<省份
 *	long country;///<国家
 *	long price;///<商品单价
 *	long num;///<商品数量
 *	long newType;///<商品新旧程度 值见{@link com.paipai.c2c.item.constants.ItemConstants}
 *	long dealType;///<出售方式 值见{@link com.paipai.c2c.item.constants.ItemConstants}
 *	long supportPayAgency;///<是否支持财付通
 *	long isRecommend;///<店铺推荐
 *	long state;///<商品状态 值见{@link com.paipai.c2c.item.constants.ItemConstants}
 *	long brand;///<商品品牌id
 *	long spec;///<商品规格id
 *	long producer;///<商品生产商id
 *	long validTime;///<有效时间
 *	long transportPriceType;///<运费承担方式 值见{@link com.paipai.c2c.item.constants.ItemConstants#TRANSPORT_SELLER_PAY}等
 *	long sendType;///<商品发货方式 值见{@link com.paipai.c2c.item.constants.ItemConstants#SEND_TYPE_MONEY}等
 *	long normalMailPrice;///<平邮价格
 *	long expressMailPrice;///<快递价格
 *	long buyLimit;///<商品限制购买数量,0表示不限制
 *	long customStyleType;///<商品详情页面颜色主题
 *	long detailSize;///<商品详情文件大小
 *	long productId;///<产品ID
 *	long weight;///<商品重量 预留字段
 *	long uperUin;///<上级分销商相关,没有使用
 *	long uperItemId;///<上级分销商相关,没有使用
 *	long reloadCnt;///<重新上架次数
 *	String shopClassId;///<店铺自定义分类
 *	String descFilePos;///<商品详情文件名
 *	String itemId;///<商品ID
 *	String snapId;///<商品快照ID
 *	String qQNick;///<卖家昵称
 *	String shopId;///<卖家店铺ID
 *	String title;///<商品名称
 *	Map<uint16_t,uint8_t> property;///<商品所有属性,使用方法{@link com.paipai.c2c.item.constants.ItemConstants}
 *	String relatedItems;///<推荐搭配商品ID列表,多个以|分隔
 *	String attrText;///<类目属性串
 *	Vector<String> logo;///<商品图片列表
 *	MultiMap<String,String> extMap;///<商品扩展表字段 Key见{@link com.paipai.c2c.item.constants.ItemConstants}
 *	ApiItemStat stat;///<统计信息，如售出数量等
 *	Vector<ApiStock> stock;///<库存相关
 *	ApiAuction auction;///<拍卖相关
 *	long emsMailPrice;///<EMS价格
 *	long resetTime;///<商品被编辑且主图被修改的时间
 *	long resetPayedNum;///<当期销售量
 *	String stockId;///<商家编码localcode
 *	String relatedCombos;///<保留字
 *	short version_u;
 *	short snapVersion_u;
 *	short qQUin_u;
 *	short leafClassId_u;
 *	short shopClassId_u;
 *	short city_u;
 *	short province_u;
 *	short country_u;
 *	short price_u;
 *	short num_u;
 *	short newType_u;
 *	short dealType_u;
 *	short supportPayAgency_u;
 *	short isRecommend_u;
 *	short state_u;
 *	short brand_u;
 *	short spec_u;
 *	short producer_u;
 *	short validTime_u;
 *	short transportPriceType_u;
 *	short sendType_u;
 *	short normalMailPrice_u;
 *	short expressMailPrice_u;
 *	short buyLimit_u;
 *	short customStyleType_u;
 *	short detailSize_u;
 *	short productId_u;
 *	short weight_u;
 *	short uperUin_u;
 *	short uperItemId_u;
 *	short reloadCnt_u;
 *	short descFilePos_u;
 *	short itemId_u;
 *	short snapId_u;
 *	short qQNick_u;
 *	short shopId_u;
 *	short title_u;
 *	short property_u;
 *	short relatedItems_u;
 *	short attrText_u;
 *	short logo_u;
 *	short extMap_u;
 *	short stat_u;
 *	short stock_u;
 *	short auction_u;
 *	short emsMailPrice_u;
 *	short resetTime_u;
 *	short resetPayedNum_u;
 *	short stockId_u;
 *	short relatedCombos_u;
 *	long CODId;///<货到付款COD
 *	short CODId_u;
 *	String shopCategory;///<新的店铺自定义分类
 *	short shopCategory_u;
 *****以上是版本20111215所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
