 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.portal.ao.idl.ReCommendAo.java

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import com.paipai.lang.GenericWrapper;
import java.util.Map;
import com.paipai.lang.uint32_t;
import java.util.Vector;
import java.util.HashMap;

/**
 *关联推荐 公共常规类推荐接口 响应参数
 *
 *@date 2014-09-04 05:12:59
 *
 *@since version:0
*/
public class  getDataResp extends NetMessage
{
	/**
	 * 推荐详情数据 map<业务方内部场景ID, 详情列表>
	 *
	 * 版本 >= 0
	 */
	 private Map<uint32_t,Vector<UnitDetail>> detailData = new HashMap<uint32_t,Vector<UnitDetail>>();

	/**
	 * 错误消息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(detailData);
		bs.pushString(errMsg);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();

		// 生成反序列化属性detailData相应的范型参数包裹对象(包裹了该属性中范型的类型)。 
		GenericWrapper detailDataPaiPai00 = new GenericWrapper();
		detailDataPaiPai00.setType(HashMap.class);
		GenericWrapper[] detailDataPaiPaiArray00= new GenericWrapper[2];
		detailDataPaiPaiArray00[0] = new GenericWrapper(uint32_t.class);
		detailDataPaiPaiArray00[1] = new GenericWrapper();
		GenericWrapper detailDataPaiPai11 = new GenericWrapper();
		detailDataPaiPai11.setType(Vector.class);
		GenericWrapper[] detailDataPaiPaiArray11= new GenericWrapper[2];
		detailDataPaiPaiArray11[0] = new GenericWrapper(UnitDetail.class);
		detailDataPaiPai11.setGenericParameters(detailDataPaiPaiArray11);


		detailDataPaiPaiArray00[1] = detailDataPaiPai11;
		detailDataPaiPai00.setGenericParameters(detailDataPaiPaiArray00);



		detailData = (Map<uint32_t,Vector<UnitDetail>>)bs.popObject(detailDataPaiPai00);
		errMsg = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x875a8001L;
	}


	/**
	 * 获取推荐详情数据 map<业务方内部场景ID, 详情列表>
	 * 
	 * 此字段的版本 >= 0
	 * @return detailData value 类型为:Map<uint32_t,Vector<UnitDetail>>
	 * 
	 */
	public Map<uint32_t,Vector<UnitDetail>> getDetailData()
	{
		return detailData;
	}


	/**
	 * 设置推荐详情数据 map<业务方内部场景ID, 详情列表>
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<uint32_t,Vector<UnitDetail>>
	 * 
	 */
	public void setDetailData(Map<uint32_t,Vector<UnitDetail>> value)
	{
		if (value != null) {
				this.detailData = value;
		}else{
				this.detailData = new HashMap<uint32_t,Vector<UnitDetail>>();
		}
	}


	/**
	 * 获取错误消息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误消息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(getDataResp)
				length += ByteStream.getObjectSize(detailData);  //计算字段detailData的长度 size_of(Map)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
