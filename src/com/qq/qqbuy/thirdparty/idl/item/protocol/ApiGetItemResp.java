 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *获取单个商品信息返回类
 *
 *@date 2012-09-02 10:38::24
 *
 *@since version:0
*/
public class  ApiGetItemResp implements IServiceObject
{
	public long result;
	/**
	 * 商品PO
	 *
	 * 版本 >= 0
	 */
	 private ApiItem apiItem = new ApiItem();

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errmsg = new String();

	/**
	 * 返回保留字
	 *
	 * 版本 >= 0
	 */
	 private String reserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(apiItem);
		bs.pushString(errmsg);
		bs.pushString(reserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		apiItem = (ApiItem) bs.popObject(ApiItem.class);
		errmsg = bs.popString();
		reserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x55068807L;
	}


	/**
	 * 获取商品PO
	 * 
	 * 此字段的版本 >= 0
	 * @return apiItem value 类型为:ApiItem
	 * 
	 */
	public ApiItem getApiItem()
	{
		return apiItem;
	}


	/**
	 * 设置商品PO
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ApiItem
	 * 
	 */
	public void setApiItem(ApiItem value)
	{
		if (value != null) {
				this.apiItem = value;
		}else{
				this.apiItem = new ApiItem();
		}
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errmsg value 类型为:String
	 * 
	 */
	public String getErrmsg()
	{
		return errmsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrmsg(String value)
	{
		if (value != null) {
				this.errmsg = value;
		}else{
				this.errmsg = new String();
		}
	}


	/**
	 * 获取返回保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return reserve value 类型为:String
	 * 
	 */
	public String getReserve()
	{
		return reserve;
	}


	/**
	 * 设置返回保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserve(String value)
	{
		if (value != null) {
				this.reserve = value;
		}else{
				this.reserve = new String();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ApiGetItemResp)
				length += ByteStream.getObjectSize(apiItem);  //计算字段apiItem的长度 size_of(ApiItem)
				length += ByteStream.getObjectSize(errmsg);  //计算字段errmsg的长度 size_of(String)
				length += ByteStream.getObjectSize(reserve);  //计算字段reserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
