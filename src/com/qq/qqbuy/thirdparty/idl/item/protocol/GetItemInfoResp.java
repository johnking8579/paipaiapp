 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *
 *
 *@date 2011-06-09 02:25::11
 *
 *@since version:0
*/
public class  GetItemInfoResp implements IServiceObject, java.io.Serializable
{
	public long result;
	/**
	 * 手机拍拍商品信息
	 *
	 * 版本 >= 0
	 */
	 private MItem mItem = new MItem();

	/**
	 * 拍拍商品信息
	 *
	 * 版本 >= 0
	 */
	 private PPItem ppItem = new PPItem();

	/**
	 * 错误码
	 *
	 * 版本 >= 0
	 */
	 private int errCode;

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(mItem);
		bs.pushObject(ppItem);
		bs.pushInt(errCode);
		bs.pushString(errMsg);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		mItem = (MItem) bs.popObject(MItem.class);
		ppItem = (PPItem) bs.popObject(PPItem.class);
		errCode = bs.popInt();
		errMsg = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80038803L;
	}


	/**
	 * 获取手机拍拍商品信息
	 * 
	 * 此字段的版本 >= 0
	 * @return mItem value 类型为:MItem
	 * 
	 */
	public MItem getMItem()
	{
		return mItem;
	}


	/**
	 * 设置手机拍拍商品信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:MItem
	 * 
	 */
	public void setMItem(MItem value)
	{
		if (value != null) {
				this.mItem = value;
		}else{
				this.mItem = new MItem();
		}
	}


	/**
	 * 获取拍拍商品信息
	 * 
	 * 此字段的版本 >= 0
	 * @return ppItem value 类型为:PPItem
	 * 
	 */
	public PPItem getPpItem()
	{
		return ppItem;
	}


	/**
	 * 设置拍拍商品信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:PPItem
	 * 
	 */
	public void setPpItem(PPItem value)
	{
		if (value != null) {
				this.ppItem = value;
		}else{
				this.ppItem = new PPItem();
		}
	}


	/**
	 * 获取错误码
	 * 
	 * 此字段的版本 >= 0
	 * @return errCode value 类型为:int
	 * 
	 */
	public int getErrCode()
	{
		return errCode;
	}


	/**
	 * 设置错误码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setErrCode(int value)
	{
		this.errCode = value;
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		if (value != null) {
				this.errMsg = value;
		}else{
				this.errMsg = new String();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetItemInfoResp)
				length += ByteStream.getObjectSize(mItem);  //计算字段mItem的长度 size_of(MItem)
				length += ByteStream.getObjectSize(ppItem);  //计算字段ppItem的长度 size_of(PPItem)
				length += 4;  //计算字段errCode的长度 size_of(int)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
