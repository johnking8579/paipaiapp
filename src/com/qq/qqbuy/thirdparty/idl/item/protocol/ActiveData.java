//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.portal.po.idl.MartPo.java

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *活动数据
 *
 *@date 2014-09-04 05:20:14
 *
 *@since version:0
*/
public class ActiveData  implements ICanSerializeObject
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20140807;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 活动ID
	 *
	 * 版本 >= 0
	 */
	 private long dwActiveId;

	/**
	 * 版本 >= 0
	 */
	 private short dwActiveId_u;

	/**
	 * 区域列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<AreaData> vecAreas = new Vector<AreaData>();

	/**
	 * 版本 >= 0
	 */
	 private short vecAreas_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushUInt(dwActiveId);
		bs.pushUByte(dwActiveId_u);
		bs.pushObject(vecAreas);
		bs.pushUByte(vecAreas_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		dwActiveId = bs.popUInt();
		dwActiveId_u = bs.popUByte();
		vecAreas = (Vector<AreaData>)bs.popVector(AreaData.class);
		vecAreas_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @return dwActiveId value 类型为:long
	 * 
	 */
	public long getDwActiveId()
	{
		return dwActiveId;
	}


	/**
	 * 设置活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwActiveId(long value)
	{
		this.dwActiveId = value;
		this.dwActiveId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwActiveId_u value 类型为:short
	 * 
	 */
	public short getDwActiveId_u()
	{
		return dwActiveId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwActiveId_u(short value)
	{
		this.dwActiveId_u = value;
	}


	/**
	 * 获取区域列表
	 * 
	 * 此字段的版本 >= 0
	 * @return vecAreas value 类型为:Vector<AreaData>
	 * 
	 */
	public Vector<AreaData> getVecAreas()
	{
		return vecAreas;
	}


	/**
	 * 设置区域列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<AreaData>
	 * 
	 */
	public void setVecAreas(Vector<AreaData> value)
	{
		if (value != null) {
				this.vecAreas = value;
				this.vecAreas_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return vecAreas_u value 类型为:short
	 * 
	 */
	public short getVecAreas_u()
	{
		return vecAreas_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVecAreas_u(short value)
	{
		this.vecAreas_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ActiveData)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwActiveId的长度 size_of(uint32_t)
				length += 1;  //计算字段dwActiveId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(vecAreas);  //计算字段vecAreas的长度 size_of(Vector)
				length += 1;  //计算字段vecAreas_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
