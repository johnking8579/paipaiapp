//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.item.po.idl.ItemPoV2.java

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *商品库存信息
 *
 *@date 2013-02-27 05:06:02
 *
 *@since version:0
*/
public class ItemStock  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 0;

	/**
	 * 库存id，暂时没有启用，请不要使用
	 *
	 * 版本 >= 0
	 */
	 private long stockId;

	/**
	 * 库存属性串，唯一性
	 *
	 * 版本 >= 0
	 */
	 private String attr = new String();

	/**
	 * 描述信息，纯描述信息
	 *
	 * 版本 >= 0
	 */
	 private String desc = new String();

	/**
	 * 库存编码， 由卖家输入的库存编码
	 *
	 * 版本 >= 0
	 */
	 private String stockCode = new String();

	/**
	 * 库存属性，目前没有使用
	 *
	 * 版本 >= 0
	 */
	 private long property;

	/**
	 * 售出数量，此库存的售出数量
	 *
	 * 版本 >= 0
	 */
	 private long soldNum;

	/**
	 * 库存数量，此库存的剩余数量
	 *
	 * 版本 >= 0
	 */
	 private long num;

	/**
	 * 发布时间， 目前宇编辑时间是一样的都是当前时间
	 *
	 * 版本 >= 0
	 */
	 private long addTime;

	/**
	 * 最后编辑时间，当前时间
	 *
	 * 版本 >= 0
	 */
	 private long lastModifyTime;

	/**
	 * 库存价格单价,单位：分
	 *
	 * 版本 >= 0
	 */
	 private long price;

	/**
	 * 扩展价格，和库存相关
	 *
	 * 版本 >= 0
	 */
	 private ItemPriceExt oPriceExt = new ItemPriceExt();

	/**
	 * strReserved
	 *
	 * 版本 >= 0
	 */
	 private String strReserved1 = new String();

	/**
	 * strReserved
	 *
	 * 版本 >= 0
	 */
	 private String strReserved2 = new String();

	/**
	 * strReserved
	 *
	 * 版本 >= 0
	 */
	 private String strReserved3 = new String();

	/**
	 * dwReserved
	 *
	 * 版本 >= 0
	 */
	 private long dwReserved1;

	/**
	 * dwReserved
	 *
	 * 版本 >= 0
	 */
	 private long dwReserved2;

	/**
	 * dwReserved
	 *
	 * 版本 >= 0
	 */
	 private long dwReserved3;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushLong(stockId);
		bs.pushString(attr);
		bs.pushString(desc);
		bs.pushString(stockCode);
		bs.pushUInt(property);
		bs.pushUInt(soldNum);
		bs.pushUInt(num);
		bs.pushUInt(addTime);
		bs.pushUInt(lastModifyTime);
		bs.pushUInt(price);
		bs.pushObject(oPriceExt);
		bs.pushString(strReserved1);
		bs.pushString(strReserved2);
		bs.pushString(strReserved3);
		bs.pushUInt(dwReserved1);
		bs.pushUInt(dwReserved2);
		bs.pushUInt(dwReserved3);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		stockId = bs.popLong();
		attr = bs.popString();
		desc = bs.popString();
		stockCode = bs.popString();
		property = bs.popUInt();
		soldNum = bs.popUInt();
		num = bs.popUInt();
		addTime = bs.popUInt();
		lastModifyTime = bs.popUInt();
		price = bs.popUInt();
		oPriceExt = (ItemPriceExt) bs.popObject(ItemPriceExt.class);
		strReserved1 = bs.popString();
		strReserved2 = bs.popString();
		strReserved3 = bs.popString();
		dwReserved1 = bs.popUInt();
		dwReserved2 = bs.popUInt();
		dwReserved3 = bs.popUInt();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取库存id，暂时没有启用，请不要使用
	 * 
	 * 此字段的版本 >= 0
	 * @return stockId value 类型为:long
	 * 
	 */
	public long getStockId()
	{
		return stockId;
	}


	/**
	 * 设置库存id，暂时没有启用，请不要使用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setStockId(long value)
	{
		this.stockId = value;
	}


	/**
	 * 获取库存属性串，唯一性
	 * 
	 * 此字段的版本 >= 0
	 * @return attr value 类型为:String
	 * 
	 */
	public String getAttr()
	{
		return attr;
	}


	/**
	 * 设置库存属性串，唯一性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAttr(String value)
	{
		this.attr = value;
	}


	/**
	 * 获取描述信息，纯描述信息
	 * 
	 * 此字段的版本 >= 0
	 * @return desc value 类型为:String
	 * 
	 */
	public String getDesc()
	{
		return desc;
	}


	/**
	 * 设置描述信息，纯描述信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDesc(String value)
	{
		this.desc = value;
	}


	/**
	 * 获取库存编码， 由卖家输入的库存编码
	 * 
	 * 此字段的版本 >= 0
	 * @return stockCode value 类型为:String
	 * 
	 */
	public String getStockCode()
	{
		return stockCode;
	}


	/**
	 * 设置库存编码， 由卖家输入的库存编码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStockCode(String value)
	{
		this.stockCode = value;
	}


	/**
	 * 获取库存属性，目前没有使用
	 * 
	 * 此字段的版本 >= 0
	 * @return property value 类型为:long
	 * 
	 */
	public long getProperty()
	{
		return property;
	}


	/**
	 * 设置库存属性，目前没有使用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setProperty(long value)
	{
		this.property = value;
	}


	/**
	 * 获取售出数量，此库存的售出数量
	 * 
	 * 此字段的版本 >= 0
	 * @return soldNum value 类型为:long
	 * 
	 */
	public long getSoldNum()
	{
		return soldNum;
	}


	/**
	 * 设置售出数量，此库存的售出数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSoldNum(long value)
	{
		this.soldNum = value;
	}


	/**
	 * 获取库存数量，此库存的剩余数量
	 * 
	 * 此字段的版本 >= 0
	 * @return num value 类型为:long
	 * 
	 */
	public long getNum()
	{
		return num;
	}


	/**
	 * 设置库存数量，此库存的剩余数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNum(long value)
	{
		this.num = value;
	}


	/**
	 * 获取发布时间， 目前宇编辑时间是一样的都是当前时间
	 * 
	 * 此字段的版本 >= 0
	 * @return addTime value 类型为:long
	 * 
	 */
	public long getAddTime()
	{
		return addTime;
	}


	/**
	 * 设置发布时间， 目前宇编辑时间是一样的都是当前时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddTime(long value)
	{
		this.addTime = value;
	}


	/**
	 * 获取最后编辑时间，当前时间
	 * 
	 * 此字段的版本 >= 0
	 * @return lastModifyTime value 类型为:long
	 * 
	 */
	public long getLastModifyTime()
	{
		return lastModifyTime;
	}


	/**
	 * 设置最后编辑时间，当前时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastModifyTime(long value)
	{
		this.lastModifyTime = value;
	}


	/**
	 * 获取库存价格单价,单位：分
	 * 
	 * 此字段的版本 >= 0
	 * @return price value 类型为:long
	 * 
	 */
	public long getPrice()
	{
		return price;
	}


	/**
	 * 设置库存价格单价,单位：分
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPrice(long value)
	{
		this.price = value;
	}


	/**
	 * 获取扩展价格，和库存相关
	 * 
	 * 此字段的版本 >= 0
	 * @return oPriceExt value 类型为:ItemPriceExt
	 * 
	 */
	public ItemPriceExt getOPriceExt()
	{
		return oPriceExt;
	}


	/**
	 * 设置扩展价格，和库存相关
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ItemPriceExt
	 * 
	 */
	public void setOPriceExt(ItemPriceExt value)
	{
		if (value != null) {
				this.oPriceExt = value;
		}else{
				this.oPriceExt = new ItemPriceExt();
		}
	}


	/**
	 * 获取strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return strReserved1 value 类型为:String
	 * 
	 */
	public String getStrReserved1()
	{
		return strReserved1;
	}


	/**
	 * 设置strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrReserved1(String value)
	{
		this.strReserved1 = value;
	}


	/**
	 * 获取strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return strReserved2 value 类型为:String
	 * 
	 */
	public String getStrReserved2()
	{
		return strReserved2;
	}


	/**
	 * 设置strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrReserved2(String value)
	{
		this.strReserved2 = value;
	}


	/**
	 * 获取strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return strReserved3 value 类型为:String
	 * 
	 */
	public String getStrReserved3()
	{
		return strReserved3;
	}


	/**
	 * 设置strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrReserved3(String value)
	{
		this.strReserved3 = value;
	}


	/**
	 * 获取dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return dwReserved1 value 类型为:long
	 * 
	 */
	public long getDwReserved1()
	{
		return dwReserved1;
	}


	/**
	 * 设置dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwReserved1(long value)
	{
		this.dwReserved1 = value;
	}


	/**
	 * 获取dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return dwReserved2 value 类型为:long
	 * 
	 */
	public long getDwReserved2()
	{
		return dwReserved2;
	}


	/**
	 * 设置dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwReserved2(long value)
	{
		this.dwReserved2 = value;
	}


	/**
	 * 获取dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return dwReserved3 value 类型为:long
	 * 
	 */
	public long getDwReserved3()
	{
		return dwReserved3;
	}


	/**
	 * 设置dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwReserved3(long value)
	{
		this.dwReserved3 = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemStock)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 17;  //计算字段stockId的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(attr);  //计算字段attr的长度 size_of(String)
				length += ByteStream.getObjectSize(desc);  //计算字段desc的长度 size_of(String)
				length += ByteStream.getObjectSize(stockCode);  //计算字段stockCode的长度 size_of(String)
				length += 4;  //计算字段property的长度 size_of(uint32_t)
				length += 4;  //计算字段soldNum的长度 size_of(uint32_t)
				length += 4;  //计算字段num的长度 size_of(uint32_t)
				length += 4;  //计算字段addTime的长度 size_of(uint32_t)
				length += 4;  //计算字段lastModifyTime的长度 size_of(uint32_t)
				length += 4;  //计算字段price的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(oPriceExt);  //计算字段oPriceExt的长度 size_of(ItemPriceExt)
				length += ByteStream.getObjectSize(strReserved1);  //计算字段strReserved1的长度 size_of(String)
				length += ByteStream.getObjectSize(strReserved2);  //计算字段strReserved2的长度 size_of(String)
				length += ByteStream.getObjectSize(strReserved3);  //计算字段strReserved3的长度 size_of(String)
				length += 4;  //计算字段dwReserved1的长度 size_of(uint32_t)
				length += 4;  //计算字段dwReserved2的长度 size_of(uint32_t)
				length += 4;  //计算字段dwReserved3的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
