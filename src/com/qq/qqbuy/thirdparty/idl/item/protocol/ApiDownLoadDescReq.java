package com.qq.qqbuy.thirdparty.idl.item.protocol;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

public class ApiDownLoadDescReq implements IServiceObject
{
  private String machineKey = new String();

  private String source = new String();
  private long scene;
  private String descFileName = new String();

  private String inReserve = new String();

  public int Serialize(ByteStream bs)
    throws Exception
  {
    bs.pushString(this.machineKey);
    bs.pushString(this.source);
    bs.pushUInt(this.scene);
    bs.pushString(this.descFileName);
    bs.pushString(this.inReserve);
    return bs.getWrittenLength();
  }

  public int UnSerialize(ByteStream bs) throws Exception
  {
    this.machineKey = bs.popString();
    this.source = bs.popString();
    this.scene = bs.popUInt();
    this.descFileName = bs.popString();
    this.inReserve = bs.popString();
    return bs.getReadLength();
  }

  public long getCmdId()
  {
    return 1763448833L;
  }

  public String getMachineKey()
  {
    return this.machineKey;
  }

  public void setMachineKey(String value)
  {
    this.machineKey = value;
  }

  public String getSource()
  {
    return this.source;
  }

  public void setSource(String value)
  {
    this.source = value;
  }

  public long getScene()
  {
    return this.scene;
  }

  public void setScene(long value)
  {
    this.scene = value;
  }

  public String getDescFileName()
  {
    return this.descFileName;
  }

  public void setDescFileName(String value)
  {
    this.descFileName = value;
  }

  public String getInReserve()
  {
    return this.inReserve;
  }

  public void setInReserve(String value)
  {
    this.inReserve = value;
  }

  protected int getClassSize()
  {
    return getSize() - 4;
  }

  public int getSize()
  {
    int length = 0;
    try {
      length = 0;
      length += ByteStream.getObjectSize(this.machineKey);
      length += ByteStream.getObjectSize(this.source);
      length += 4;
      length += ByteStream.getObjectSize(this.descFileName);
      length += ByteStream.getObjectSize(this.inReserve);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return length;
  }

}