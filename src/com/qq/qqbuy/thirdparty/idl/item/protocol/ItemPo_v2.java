//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.item.po.idl.ItemPoV2.java

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *商品Po正式版
 *
 *@date 2013-02-27 05:06:02
 *
 *@since version:0
*/
public class ItemPo_v2  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 1;

	/**
	 * 商品基本信息，包含商品的基本信息(商品名称，所在地，最低价格，可使用的红包等)
	 *
	 * 版本 >= 0
	 */
	 private ItemBase oItemBase = new ItemBase();

	/**
	 * 商品图片信息，包含商品的图片信息(包含商品的主图，5个小图等)
	 *
	 * 版本 >= 0
	 */
	 private ItemImg oItemImg = new ItemImg();

	/**
	 * 商品属性，包含商品的主属性信息
	 *
	 * 版本 >= 0
	 */
	 private ItemProp oItemProp = new ItemProp();

	/**
	 * 商品扩展信息，包含商品的扩展信息（请业务方不要使用其中的字段，基础组会在不通知的情况下修改其中的定义）
	 *
	 * 版本 >= 0
	 */
	 private ItemExt oItemExt = new ItemExt();

	/**
	 * 商品库存，包含商品的库存，其中商品的彩钻价格在库存中统一和库存相关，商品的库存图片也保存在其中
	 *
	 * 版本 >= 0
	 */
	 private ItemStockList oItemStockList = new ItemStockList();

	/**
	 * 商品数量信息，商品的下单，购买等数量统计
	 *
	 * 版本 >= 0
	 */
	 private ItemNum oItemNum = new ItemNum();

	/**
	 * 商品操作时间，商品的各个操作时间，但商品的有效时间在商品基本信息中
	 *
	 * 版本 >= 0
	 */
	 private ItemTime oItemTime = new ItemTime();

	/**
	 * 商品拍卖信息，商品拍卖的相关信息
	 *
	 * 版本 >= 0
	 */
	 private ItemAuction oItemAuction = new ItemAuction();

	/**
	 * 商品扩展价格信息，包括彩钻价格，优惠价格等，只有商品没有库存属性时，才会有效，否则，扩展价格是和库存对应的
	 *
	 * 版本 >= 0
	 */
	 private ItemPriceExt oItemPriceExt = new ItemPriceExt();

	/**
	 * 商品视频信息，现在视频信息拍拍侧只保留QQ视频的一个id，考虑以后会扩展，做成一个结构体
	 *
	 * 版本 >= 0
	 */
	 private ItemVideo oItemVideo = new ItemVideo();

	/**
	 * 扩展结构未使用
	 *
	 * 版本 >= 0
	 */
	 private ItemReserve oItemReserved1 = new ItemReserve();

	/**
	 * 返回码
	 *
	 * 版本 >= 0
	 */
	 private long retCode;

	/**
	 * strReserved1
	 *
	 * 版本 >= 0
	 */
	 private String strReserved1 = new String();

	/**
	 * 库存图片列表
	 *
	 * 版本 >= 1
	 */
	 private ItemStockImgList oItemStockImg = new ItemStockImgList();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushObject(oItemBase);
		bs.pushObject(oItemImg);
		bs.pushObject(oItemProp);
		bs.pushObject(oItemExt);
		bs.pushObject(oItemStockList);
		bs.pushObject(oItemNum);
		bs.pushObject(oItemTime);
		bs.pushObject(oItemAuction);
		bs.pushObject(oItemPriceExt);
		bs.pushObject(oItemVideo);
		bs.pushObject(oItemReserved1);
		bs.pushUInt(retCode);
		bs.pushString(strReserved1);
		if(  this.version >= 1 ){
				bs.pushObject(oItemStockImg);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		oItemBase = (ItemBase) bs.popObject(ItemBase.class);
		oItemImg = (ItemImg) bs.popObject(ItemImg.class);
		oItemProp = (ItemProp) bs.popObject(ItemProp.class);
		oItemExt = (ItemExt) bs.popObject(ItemExt.class);
		oItemStockList = (ItemStockList) bs.popObject(ItemStockList.class);
		oItemNum = (ItemNum) bs.popObject(ItemNum.class);
		oItemTime = (ItemTime) bs.popObject(ItemTime.class);
		oItemAuction = (ItemAuction) bs.popObject(ItemAuction.class);
		oItemPriceExt = (ItemPriceExt) bs.popObject(ItemPriceExt.class);
		oItemVideo = (ItemVideo) bs.popObject(ItemVideo.class);
		oItemReserved1 = (ItemReserve) bs.popObject(ItemReserve.class);
		retCode = bs.popUInt();
		strReserved1 = bs.popString();
		if(  this.version >= 1 ){
				oItemStockImg = (ItemStockImgList) bs.popObject(ItemStockImgList.class);
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取商品基本信息，包含商品的基本信息(商品名称，所在地，最低价格，可使用的红包等)
	 * 
	 * 此字段的版本 >= 0
	 * @return oItemBase value 类型为:ItemBase
	 * 
	 */
	public ItemBase getOItemBase()
	{
		return oItemBase;
	}


	/**
	 * 设置商品基本信息，包含商品的基本信息(商品名称，所在地，最低价格，可使用的红包等)
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ItemBase
	 * 
	 */
	public void setOItemBase(ItemBase value)
	{
		if (value != null) {
				this.oItemBase = value;
		}else{
				this.oItemBase = new ItemBase();
		}
	}


	/**
	 * 获取商品图片信息，包含商品的图片信息(包含商品的主图，5个小图等)
	 * 
	 * 此字段的版本 >= 0
	 * @return oItemImg value 类型为:ItemImg
	 * 
	 */
	public ItemImg getOItemImg()
	{
		return oItemImg;
	}


	/**
	 * 设置商品图片信息，包含商品的图片信息(包含商品的主图，5个小图等)
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ItemImg
	 * 
	 */
	public void setOItemImg(ItemImg value)
	{
		if (value != null) {
				this.oItemImg = value;
		}else{
				this.oItemImg = new ItemImg();
		}
	}


	/**
	 * 获取商品属性，包含商品的主属性信息
	 * 
	 * 此字段的版本 >= 0
	 * @return oItemProp value 类型为:ItemProp
	 * 
	 */
	public ItemProp getOItemProp()
	{
		return oItemProp;
	}


	/**
	 * 设置商品属性，包含商品的主属性信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ItemProp
	 * 
	 */
	public void setOItemProp(ItemProp value)
	{
		if (value != null) {
				this.oItemProp = value;
		}else{
				this.oItemProp = new ItemProp();
		}
	}


	/**
	 * 获取商品扩展信息，包含商品的扩展信息（请业务方不要使用其中的字段，基础组会在不通知的情况下修改其中的定义）
	 * 
	 * 此字段的版本 >= 0
	 * @return oItemExt value 类型为:ItemExt
	 * 
	 */
	public ItemExt getOItemExt()
	{
		return oItemExt;
	}


	/**
	 * 设置商品扩展信息，包含商品的扩展信息（请业务方不要使用其中的字段，基础组会在不通知的情况下修改其中的定义）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ItemExt
	 * 
	 */
	public void setOItemExt(ItemExt value)
	{
		if (value != null) {
				this.oItemExt = value;
		}else{
				this.oItemExt = new ItemExt();
		}
	}


	/**
	 * 获取商品库存，包含商品的库存，其中商品的彩钻价格在库存中统一和库存相关，商品的库存图片也保存在其中
	 * 
	 * 此字段的版本 >= 0
	 * @return oItemStockList value 类型为:ItemStockList
	 * 
	 */
	public ItemStockList getOItemStockList()
	{
		return oItemStockList;
	}


	/**
	 * 设置商品库存，包含商品的库存，其中商品的彩钻价格在库存中统一和库存相关，商品的库存图片也保存在其中
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ItemStockList
	 * 
	 */
	public void setOItemStockList(ItemStockList value)
	{
		if (value != null) {
				this.oItemStockList = value;
		}else{
				this.oItemStockList = new ItemStockList();
		}
	}


	/**
	 * 获取商品数量信息，商品的下单，购买等数量统计
	 * 
	 * 此字段的版本 >= 0
	 * @return oItemNum value 类型为:ItemNum
	 * 
	 */
	public ItemNum getOItemNum()
	{
		return oItemNum;
	}


	/**
	 * 设置商品数量信息，商品的下单，购买等数量统计
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ItemNum
	 * 
	 */
	public void setOItemNum(ItemNum value)
	{
		if (value != null) {
				this.oItemNum = value;
		}else{
				this.oItemNum = new ItemNum();
		}
	}


	/**
	 * 获取商品操作时间，商品的各个操作时间，但商品的有效时间在商品基本信息中
	 * 
	 * 此字段的版本 >= 0
	 * @return oItemTime value 类型为:ItemTime
	 * 
	 */
	public ItemTime getOItemTime()
	{
		return oItemTime;
	}


	/**
	 * 设置商品操作时间，商品的各个操作时间，但商品的有效时间在商品基本信息中
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ItemTime
	 * 
	 */
	public void setOItemTime(ItemTime value)
	{
		if (value != null) {
				this.oItemTime = value;
		}else{
				this.oItemTime = new ItemTime();
		}
	}


	/**
	 * 获取商品拍卖信息，商品拍卖的相关信息
	 * 
	 * 此字段的版本 >= 0
	 * @return oItemAuction value 类型为:ItemAuction
	 * 
	 */
	public ItemAuction getOItemAuction()
	{
		return oItemAuction;
	}


	/**
	 * 设置商品拍卖信息，商品拍卖的相关信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ItemAuction
	 * 
	 */
	public void setOItemAuction(ItemAuction value)
	{
		if (value != null) {
				this.oItemAuction = value;
		}else{
				this.oItemAuction = new ItemAuction();
		}
	}


	/**
	 * 获取商品扩展价格信息，包括彩钻价格，优惠价格等，只有商品没有库存属性时，才会有效，否则，扩展价格是和库存对应的
	 * 
	 * 此字段的版本 >= 0
	 * @return oItemPriceExt value 类型为:ItemPriceExt
	 * 
	 */
	public ItemPriceExt getOItemPriceExt()
	{
		return oItemPriceExt;
	}


	/**
	 * 设置商品扩展价格信息，包括彩钻价格，优惠价格等，只有商品没有库存属性时，才会有效，否则，扩展价格是和库存对应的
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ItemPriceExt
	 * 
	 */
	public void setOItemPriceExt(ItemPriceExt value)
	{
		if (value != null) {
				this.oItemPriceExt = value;
		}else{
				this.oItemPriceExt = new ItemPriceExt();
		}
	}


	/**
	 * 获取商品视频信息，现在视频信息拍拍侧只保留QQ视频的一个id，考虑以后会扩展，做成一个结构体
	 * 
	 * 此字段的版本 >= 0
	 * @return oItemVideo value 类型为:ItemVideo
	 * 
	 */
	public ItemVideo getOItemVideo()
	{
		return oItemVideo;
	}


	/**
	 * 设置商品视频信息，现在视频信息拍拍侧只保留QQ视频的一个id，考虑以后会扩展，做成一个结构体
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ItemVideo
	 * 
	 */
	public void setOItemVideo(ItemVideo value)
	{
		if (value != null) {
				this.oItemVideo = value;
		}else{
				this.oItemVideo = new ItemVideo();
		}
	}


	/**
	 * 获取扩展结构未使用
	 * 
	 * 此字段的版本 >= 0
	 * @return oItemReserved1 value 类型为:ItemReserve
	 * 
	 */
	public ItemReserve getOItemReserved1()
	{
		return oItemReserved1;
	}


	/**
	 * 设置扩展结构未使用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ItemReserve
	 * 
	 */
	public void setOItemReserved1(ItemReserve value)
	{
		if (value != null) {
				this.oItemReserved1 = value;
		}else{
				this.oItemReserved1 = new ItemReserve();
		}
	}


	/**
	 * 获取返回码
	 * 
	 * 此字段的版本 >= 0
	 * @return retCode value 类型为:long
	 * 
	 */
	public long getRetCode()
	{
		return retCode;
	}


	/**
	 * 设置返回码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRetCode(long value)
	{
		this.retCode = value;
	}


	/**
	 * 获取strReserved1
	 * 
	 * 此字段的版本 >= 0
	 * @return strReserved1 value 类型为:String
	 * 
	 */
	public String getStrReserved1()
	{
		return strReserved1;
	}


	/**
	 * 设置strReserved1
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrReserved1(String value)
	{
		this.strReserved1 = value;
	}


	/**
	 * 获取库存图片列表
	 * 
	 * 此字段的版本 >= 1
	 * @return oItemStockImg value 类型为:ItemStockImgList
	 * 
	 */
	public ItemStockImgList getOItemStockImg()
	{
		return oItemStockImg;
	}


	/**
	 * 设置库存图片列表
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:ItemStockImgList
	 * 
	 */
	public void setOItemStockImg(ItemStockImgList value)
	{
		if (value != null) {
				this.oItemStockImg = value;
		}else{
				this.oItemStockImg = new ItemStockImgList();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemPo_v2)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(oItemBase);  //计算字段oItemBase的长度 size_of(ItemBase)
				length += ByteStream.getObjectSize(oItemImg);  //计算字段oItemImg的长度 size_of(ItemImg)
				length += ByteStream.getObjectSize(oItemProp);  //计算字段oItemProp的长度 size_of(ItemProp)
				length += ByteStream.getObjectSize(oItemExt);  //计算字段oItemExt的长度 size_of(ItemExt)
				length += ByteStream.getObjectSize(oItemStockList);  //计算字段oItemStockList的长度 size_of(ItemStockList)
				length += ByteStream.getObjectSize(oItemNum);  //计算字段oItemNum的长度 size_of(ItemNum)
				length += ByteStream.getObjectSize(oItemTime);  //计算字段oItemTime的长度 size_of(ItemTime)
				length += ByteStream.getObjectSize(oItemAuction);  //计算字段oItemAuction的长度 size_of(ItemAuction)
				length += ByteStream.getObjectSize(oItemPriceExt);  //计算字段oItemPriceExt的长度 size_of(ItemPriceExt)
				length += ByteStream.getObjectSize(oItemVideo);  //计算字段oItemVideo的长度 size_of(ItemVideo)
				length += ByteStream.getObjectSize(oItemReserved1);  //计算字段oItemReserved1的长度 size_of(ItemReserve)
				length += 4;  //计算字段retCode的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(strReserved1);  //计算字段strReserved1的长度 size_of(String)
				if(  this.version >= 1 ){
						length += ByteStream.getObjectSize(oItemStockImg);  //计算字段oItemStockImg的长度 size_of(ItemStockImgList)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本1所包含的字段*******
 *	long version;///<版本号
 *	ItemBase oItemBase;///<商品基本信息，包含商品的基本信息(商品名称，所在地，最低价格，可使用的红包等)
 *	ItemImg oItemImg;///<商品图片信息，包含商品的图片信息(包含商品的主图，5个小图等)
 *	ItemProp oItemProp;///<商品属性，包含商品的主属性信息
 *	ItemExt oItemExt;///<商品扩展信息，包含商品的扩展信息（请业务方不要使用其中的字段，基础组会在不通知的情况下修改其中的定义）
 *	ItemStockList oItemStockList;///<商品库存，包含商品的库存，其中商品的彩钻价格在库存中统一和库存相关，商品的库存图片也保存在其中
 *	ItemNum oItemNum;///<商品数量信息，商品的下单，购买等数量统计
 *	ItemTime oItemTime;///<商品操作时间，商品的各个操作时间，但商品的有效时间在商品基本信息中
 *	ItemAuction oItemAuction;///<商品拍卖信息，商品拍卖的相关信息
 *	ItemPriceExt oItemPriceExt;///<商品扩展价格信息，包括彩钻价格，优惠价格等，只有商品没有库存属性时，才会有效，否则，扩展价格是和库存对应的
 *	ItemVideo oItemVideo;///<商品视频信息，现在视频信息拍拍侧只保留QQ视频的一个id，考虑以后会扩展，做成一个结构体
 *	ItemReserve oItemReserved1;///<扩展结构未使用
 *	long retCode;///<返回码
 *	String strReserved1;///<strReserved1
 *	ItemStockImgList oItemStockImg;///<库存图片列表
 *****以上是版本1所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
