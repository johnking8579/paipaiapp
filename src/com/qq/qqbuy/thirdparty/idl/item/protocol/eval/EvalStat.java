//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.EvalIdl.java

package com.qq.qqbuy.thirdparty.idl.item.protocol.eval;


import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *评价统计po
 *
 *@date 2012-12-17 10:47:48
 *
 *@since version:0
*/
public class EvalStat  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 卖家号
	 *
	 * 版本 >= 0
	 */
	 private long UserUin;

	/**
	 * 版本 >= 0
	 */
	 private short UserUin_u;

	/**
	 * (作为买家)一周好评数
	 *
	 * 版本 >= 0
	 */
	 private long BuyerGoodWeek;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerGoodWeek_u;

	/**
	 * (作为买家)一个月好评数
	 *
	 * 版本 >= 0
	 */
	 private long BuyerGoodMonth;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerGoodMonth_u;

	/**
	 * (作为买家)六个月好评数
	 *
	 * 版本 >= 0
	 */
	 private long BuyerGood6Month;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerGood6Month_u;

	/**
	 * (作为买家)好评总数
	 *
	 * 版本 >= 0
	 */
	 private long BuyerGoodAll;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerGoodAll_u;

	/**
	 * (作为买家)一周中评数
	 *
	 * 版本 >= 0
	 */
	 private long BuyerNormalWeek;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerNormalWeek_u;

	/**
	 * (作为买家)一个月中评数
	 *
	 * 版本 >= 0
	 */
	 private long BuyerNormalMonth;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerNormalMonth_u;

	/**
	 * (作为买家)六个月中评数
	 *
	 * 版本 >= 0
	 */
	 private long BuyerNormal6Month;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerNormal6Month_u;

	/**
	 * (作为买家)中评总数
	 *
	 * 版本 >= 0
	 */
	 private long BuyerNormalAll;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerNormalAll_u;

	/**
	 * (作为买家)一周差评数
	 *
	 * 版本 >= 0
	 */
	 private long BuyerBadWeek;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerBadWeek_u;

	/**
	 * (作为买家)一个月差评数
	 *
	 * 版本 >= 0
	 */
	 private long BuyerBadMonth;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerBadMonth_u;

	/**
	 * (作为买家)六个月差评数
	 *
	 * 版本 >= 0
	 */
	 private long BuyerBad6Month;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerBad6Month_u;

	/**
	 * (作为买家)差评总数
	 *
	 * 版本 >= 0
	 */
	 private long BuyerBadAll;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerBadAll_u;

	/**
	 * (作为卖家)一周好评数
	 *
	 * 版本 >= 0
	 */
	 private long SellerGoodWeek;

	/**
	 * 版本 >= 0
	 */
	 private short SellerGoodWeek_u;

	/**
	 * (作为卖家)一个月好评数
	 *
	 * 版本 >= 0
	 */
	 private long SellerGoodMonth;

	/**
	 * 版本 >= 0
	 */
	 private short SellerGoodMonth_u;

	/**
	 * (作为卖家)六个月好评数
	 *
	 * 版本 >= 0
	 */
	 private long SellerGood6Month;

	/**
	 * 版本 >= 0
	 */
	 private short SellerGood6Month_u;

	/**
	 * (作为卖家)好评总数
	 *
	 * 版本 >= 0
	 */
	 private long SellerGoodAll;

	/**
	 * 版本 >= 0
	 */
	 private short SellerGoodAll_u;

	/**
	 * (作为卖家)一周中评数
	 *
	 * 版本 >= 0
	 */
	 private long SellerNormalWeek;

	/**
	 * 版本 >= 0
	 */
	 private short SellerNormalWeek_u;

	/**
	 * (作为卖家)一个月中评数
	 *
	 * 版本 >= 0
	 */
	 private long SellerNormalMonth;

	/**
	 * 版本 >= 0
	 */
	 private short SellerNormalMonth_u;

	/**
	 * (作为卖家)六个月中评数
	 *
	 * 版本 >= 0
	 */
	 private long SellerNormal6Month;

	/**
	 * 版本 >= 0
	 */
	 private short SellerNormal6Month_u;

	/**
	 * (作为卖家)中评总数
	 *
	 * 版本 >= 0
	 */
	 private long SellerNormalAll;

	/**
	 * 版本 >= 0
	 */
	 private short SellerNormalAll_u;

	/**
	 * (作为卖家)一周差评数
	 *
	 * 版本 >= 0
	 */
	 private long SellerBadWeek;

	/**
	 * 版本 >= 0
	 */
	 private short SellerBadWeek_u;

	/**
	 * (作为卖家)一个月差评数
	 *
	 * 版本 >= 0
	 */
	 private long SellerBadMonth;

	/**
	 * 版本 >= 0
	 */
	 private short SellerBadMonth_u;

	/**
	 * (作为卖家)六个月差评数
	 *
	 * 版本 >= 0
	 */
	 private long SellerBad6Month;

	/**
	 * 版本 >= 0
	 */
	 private short SellerBad6Month_u;

	/**
	 * (作为卖家)差评总数
	 *
	 * 版本 >= 0
	 */
	 private long SellerBadAll;

	/**
	 * 版本 >= 0
	 */
	 private short SellerBadAll_u;

	/**
	 * 最后更新时间
	 *
	 * 版本 >= 0
	 */
	 private long LastUpdateTime;

	/**
	 * 版本 >= 0
	 */
	 private short LastUpdateTime_u;

	/**
	 * 卖家信用
	 *
	 * 版本 >= 0
	 */
	 private int SellerCreditAll;

	/**
	 * 版本 >= 0
	 */
	 private short SellerCreditAll_u;

	/**
	 * 卖家信用-实物
	 *
	 * 版本 >= 0
	 */
	 private int SellerCreditNormal;

	/**
	 * 版本 >= 0
	 */
	 private short SellerCreditNormal_u;

	/**
	 * 卖家信用-虚拟
	 *
	 * 版本 >= 0
	 */
	 private int SellerCreditVirtual;

	/**
	 * 版本 >= 0
	 */
	 private short SellerCreditVirtual_u;

	/**
	 * 买家信用
	 *
	 * 版本 >= 0
	 */
	 private int BuyerCreditAll;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerCreditAll_u;

	/**
	 * 买家信用-实物
	 *
	 * 版本 >= 0
	 */
	 private int BuyerCreditNormal;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerCreditNormal_u;

	/**
	 * 买家信用-虚拟
	 *
	 * 版本 >= 0
	 */
	 private int BuyerCreditVirtual;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerCreditVirtual_u;

	/**
	 * Dsr1
	 *
	 * 版本 >= 0
	 */
	 private long Dsr1Total;

	/**
	 * 版本 >= 0
	 */
	 private short Dsr1Total_u;

	/**
	 * Dsr2
	 *
	 * 版本 >= 0
	 */
	 private long Dsr2Total;

	/**
	 * 版本 >= 0
	 */
	 private short Dsr2Total_u;

	/**
	 * Dsr3
	 *
	 * 版本 >= 0
	 */
	 private long Dsr3Total;

	/**
	 * 版本 >= 0
	 */
	 private short Dsr3Total_u;

	/**
	 * 评价总数
	 *
	 * 版本 >= 0
	 */
	 private int EvalNums;

	/**
	 * 版本 >= 0
	 */
	 private short EvalNums_u;

	/**
	 * 卖家等级
	 *
	 * 版本 >= 0
	 */
	 private int SellerLevelTotal;

	/**
	 * 版本 >= 0
	 */
	 private short SellerLevelTotal_u;

	/**
	 * DsrTotal
	 *
	 * 版本 >= 0
	 */
	 private long DsrTotal;

	/**
	 * 版本 >= 0
	 */
	 private short DsrTotal_u;

	/**
	 * (作为卖家)等待评价
	 *
	 * 版本 >= 0
	 */
	 private long WaitForSeller;

	/**
	 * 版本 >= 0
	 */
	 private short WaitForSeller_u;

	/**
	 * (作为买家)等待评价
	 *
	 * 版本 >= 0
	 */
	 private long WaitForBuyer;

	/**
	 * 版本 >= 0
	 */
	 private short WaitForBuyer_u;

	/**
	 * 有内容评价--一周
	 *
	 * 版本 >= 0
	 */
	 private long ContentWeek;

	/**
	 * 版本 >= 0
	 */
	 private short ContentWeek_u;

	/**
	 * 有内容评价--一个月
	 *
	 * 版本 >= 0
	 */
	 private long ContentMon;

	/**
	 * 版本 >= 0
	 */
	 private short ContentMon_u;

	/**
	 * 有内容评价--六个月
	 *
	 * 版本 >= 0
	 */
	 private long Content6mon;

	/**
	 * 版本 >= 0
	 */
	 private short Content6mon_u;

	/**
	 * (作为买家)一周做出好评数
	 *
	 * 版本 >= 0
	 */
	 private long BuyerMakeGoodWeek;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerMakeGoodWeek_u;

	/**
	 * (作为买家)一周做出中评数
	 *
	 * 版本 >= 0
	 */
	 private long BuyerMakeNorWeek;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerMakeNorWeek_u;

	/**
	 * (作为买家)一周做出差评数
	 *
	 * 版本 >= 0
	 */
	 private long BuyerMakeBadWeek;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerMakeBadWeek_u;

	/**
	 * (作为买家)一个月做出的好评数
	 *
	 * 版本 >= 0
	 */
	 private long BuyerMakeGoodMon;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerMakeGoodMon_u;

	/**
	 * (作为买家)一个月做出的中评数
	 *
	 * 版本 >= 0
	 */
	 private long BuyerMakeNorMon;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerMakeNorMon_u;

	/**
	 * (作为买家)一个月做出的差评数
	 *
	 * 版本 >= 0
	 */
	 private long BuyerMakeBadMon;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerMakeBadMon_u;

	/**
	 * (作为买家)六个月做出的好评数
	 *
	 * 版本 >= 0
	 */
	 private long BuyerMakeGood6mon;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerMakeGood6mon_u;

	/**
	 * (作为买家)六个月做出的中评数
	 *
	 * 版本 >= 0
	 */
	 private long BuyerMakeNor6mon;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerMakeNor6mon_u;

	/**
	 * (作为买家)六个月做出的差评数
	 *
	 * 版本 >= 0
	 */
	 private long BuyerMakeBad6mon;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerMakeBad6mon_u;

	/**
	 * (作为卖家)一周做出好评数
	 *
	 * 版本 >= 0
	 */
	 private long SellerMakeGoodWeek;

	/**
	 * 版本 >= 0
	 */
	 private short SellerMakeGoodWeek_u;

	/**
	 * (作为卖家)一周做出中评数
	 *
	 * 版本 >= 0
	 */
	 private long SellerMakeNorWeek;

	/**
	 * 版本 >= 0
	 */
	 private short SellerMakeNorWeek_u;

	/**
	 * (作为卖家)一周做出差评数
	 *
	 * 版本 >= 0
	 */
	 private long SellerMakeBadWeek;

	/**
	 * 版本 >= 0
	 */
	 private short SellerMakeBadWeek_u;

	/**
	 * (作为卖家)一个月做出好评数
	 *
	 * 版本 >= 0
	 */
	 private long SellerMakeGoodMon;

	/**
	 * 版本 >= 0
	 */
	 private short SellerMakeGoodMon_u;

	/**
	 * (作为卖家)一个月做出中评数
	 *
	 * 版本 >= 0
	 */
	 private long SellerMakeNorMon;

	/**
	 * 版本 >= 0
	 */
	 private short SellerMakeNorMon_u;

	/**
	 * (作为卖家)一个月做出差评数
	 *
	 * 版本 >= 0
	 */
	 private long SellerMakeBadMon;

	/**
	 * 版本 >= 0
	 */
	 private short SellerMakeBadMon_u;

	/**
	 * (作为卖家)六个月做出好评数
	 *
	 * 版本 >= 0
	 */
	 private long SellerMakeGood6mon;

	/**
	 * 版本 >= 0
	 */
	 private short SellerMakeGood6mon_u;

	/**
	 * (作为卖家)六个月做出中评数
	 *
	 * 版本 >= 0
	 */
	 private long SellerMakeNor6mon;

	/**
	 * 版本 >= 0
	 */
	 private short SellerMakeNor6mon_u;

	/**
	 * (作为卖家)六个月做出差评数
	 *
	 * 版本 >= 0
	 */
	 private long SellerMakeBad6mon;

	/**
	 * 版本 >= 0
	 */
	 private short SellerMakeBad6mon_u;

	/**
	 * 时间
	 *
	 * 版本 >= 0
	 */
	 private long DateTime;

	/**
	 * 版本 >= 0
	 */
	 private short DateTime_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushUInt(UserUin);
		bs.pushUByte(UserUin_u);
		bs.pushUInt(BuyerGoodWeek);
		bs.pushUByte(BuyerGoodWeek_u);
		bs.pushUInt(BuyerGoodMonth);
		bs.pushUByte(BuyerGoodMonth_u);
		bs.pushUInt(BuyerGood6Month);
		bs.pushUByte(BuyerGood6Month_u);
		bs.pushUInt(BuyerGoodAll);
		bs.pushUByte(BuyerGoodAll_u);
		bs.pushUInt(BuyerNormalWeek);
		bs.pushUByte(BuyerNormalWeek_u);
		bs.pushUInt(BuyerNormalMonth);
		bs.pushUByte(BuyerNormalMonth_u);
		bs.pushUInt(BuyerNormal6Month);
		bs.pushUByte(BuyerNormal6Month_u);
		bs.pushUInt(BuyerNormalAll);
		bs.pushUByte(BuyerNormalAll_u);
		bs.pushUInt(BuyerBadWeek);
		bs.pushUByte(BuyerBadWeek_u);
		bs.pushUInt(BuyerBadMonth);
		bs.pushUByte(BuyerBadMonth_u);
		bs.pushUInt(BuyerBad6Month);
		bs.pushUByte(BuyerBad6Month_u);
		bs.pushUInt(BuyerBadAll);
		bs.pushUByte(BuyerBadAll_u);
		bs.pushUInt(SellerGoodWeek);
		bs.pushUByte(SellerGoodWeek_u);
		bs.pushUInt(SellerGoodMonth);
		bs.pushUByte(SellerGoodMonth_u);
		bs.pushUInt(SellerGood6Month);
		bs.pushUByte(SellerGood6Month_u);
		bs.pushUInt(SellerGoodAll);
		bs.pushUByte(SellerGoodAll_u);
		bs.pushUInt(SellerNormalWeek);
		bs.pushUByte(SellerNormalWeek_u);
		bs.pushUInt(SellerNormalMonth);
		bs.pushUByte(SellerNormalMonth_u);
		bs.pushUInt(SellerNormal6Month);
		bs.pushUByte(SellerNormal6Month_u);
		bs.pushUInt(SellerNormalAll);
		bs.pushUByte(SellerNormalAll_u);
		bs.pushUInt(SellerBadWeek);
		bs.pushUByte(SellerBadWeek_u);
		bs.pushUInt(SellerBadMonth);
		bs.pushUByte(SellerBadMonth_u);
		bs.pushUInt(SellerBad6Month);
		bs.pushUByte(SellerBad6Month_u);
		bs.pushUInt(SellerBadAll);
		bs.pushUByte(SellerBadAll_u);
		bs.pushUInt(LastUpdateTime);
		bs.pushUByte(LastUpdateTime_u);
		bs.pushInt(SellerCreditAll);
		bs.pushUByte(SellerCreditAll_u);
		bs.pushInt(SellerCreditNormal);
		bs.pushUByte(SellerCreditNormal_u);
		bs.pushInt(SellerCreditVirtual);
		bs.pushUByte(SellerCreditVirtual_u);
		bs.pushInt(BuyerCreditAll);
		bs.pushUByte(BuyerCreditAll_u);
		bs.pushInt(BuyerCreditNormal);
		bs.pushUByte(BuyerCreditNormal_u);
		bs.pushInt(BuyerCreditVirtual);
		bs.pushUByte(BuyerCreditVirtual_u);
		bs.pushUInt(Dsr1Total);
		bs.pushUByte(Dsr1Total_u);
		bs.pushUInt(Dsr2Total);
		bs.pushUByte(Dsr2Total_u);
		bs.pushUInt(Dsr3Total);
		bs.pushUByte(Dsr3Total_u);
		bs.pushInt(EvalNums);
		bs.pushUByte(EvalNums_u);
		bs.pushInt(SellerLevelTotal);
		bs.pushUByte(SellerLevelTotal_u);
		bs.pushUInt(DsrTotal);
		bs.pushUByte(DsrTotal_u);
		bs.pushUInt(WaitForSeller);
		bs.pushUByte(WaitForSeller_u);
		bs.pushUInt(WaitForBuyer);
		bs.pushUByte(WaitForBuyer_u);
		bs.pushUInt(ContentWeek);
		bs.pushUByte(ContentWeek_u);
		bs.pushUInt(ContentMon);
		bs.pushUByte(ContentMon_u);
		bs.pushUInt(Content6mon);
		bs.pushUByte(Content6mon_u);
		bs.pushUInt(BuyerMakeGoodWeek);
		bs.pushUByte(BuyerMakeGoodWeek_u);
		bs.pushUInt(BuyerMakeNorWeek);
		bs.pushUByte(BuyerMakeNorWeek_u);
		bs.pushUInt(BuyerMakeBadWeek);
		bs.pushUByte(BuyerMakeBadWeek_u);
		bs.pushUInt(BuyerMakeGoodMon);
		bs.pushUByte(BuyerMakeGoodMon_u);
		bs.pushUInt(BuyerMakeNorMon);
		bs.pushUByte(BuyerMakeNorMon_u);
		bs.pushUInt(BuyerMakeBadMon);
		bs.pushUByte(BuyerMakeBadMon_u);
		bs.pushUInt(BuyerMakeGood6mon);
		bs.pushUByte(BuyerMakeGood6mon_u);
		bs.pushUInt(BuyerMakeNor6mon);
		bs.pushUByte(BuyerMakeNor6mon_u);
		bs.pushUInt(BuyerMakeBad6mon);
		bs.pushUByte(BuyerMakeBad6mon_u);
		bs.pushUInt(SellerMakeGoodWeek);
		bs.pushUByte(SellerMakeGoodWeek_u);
		bs.pushUInt(SellerMakeNorWeek);
		bs.pushUByte(SellerMakeNorWeek_u);
		bs.pushUInt(SellerMakeBadWeek);
		bs.pushUByte(SellerMakeBadWeek_u);
		bs.pushUInt(SellerMakeGoodMon);
		bs.pushUByte(SellerMakeGoodMon_u);
		bs.pushUInt(SellerMakeNorMon);
		bs.pushUByte(SellerMakeNorMon_u);
		bs.pushUInt(SellerMakeBadMon);
		bs.pushUByte(SellerMakeBadMon_u);
		bs.pushUInt(SellerMakeGood6mon);
		bs.pushUByte(SellerMakeGood6mon_u);
		bs.pushUInt(SellerMakeNor6mon);
		bs.pushUByte(SellerMakeNor6mon_u);
		bs.pushUInt(SellerMakeBad6mon);
		bs.pushUByte(SellerMakeBad6mon_u);
		bs.pushUInt(DateTime);
		bs.pushUByte(DateTime_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		UserUin = bs.popUInt();
		UserUin_u = bs.popUByte();
		BuyerGoodWeek = bs.popUInt();
		BuyerGoodWeek_u = bs.popUByte();
		BuyerGoodMonth = bs.popUInt();
		BuyerGoodMonth_u = bs.popUByte();
		BuyerGood6Month = bs.popUInt();
		BuyerGood6Month_u = bs.popUByte();
		BuyerGoodAll = bs.popUInt();
		BuyerGoodAll_u = bs.popUByte();
		BuyerNormalWeek = bs.popUInt();
		BuyerNormalWeek_u = bs.popUByte();
		BuyerNormalMonth = bs.popUInt();
		BuyerNormalMonth_u = bs.popUByte();
		BuyerNormal6Month = bs.popUInt();
		BuyerNormal6Month_u = bs.popUByte();
		BuyerNormalAll = bs.popUInt();
		BuyerNormalAll_u = bs.popUByte();
		BuyerBadWeek = bs.popUInt();
		BuyerBadWeek_u = bs.popUByte();
		BuyerBadMonth = bs.popUInt();
		BuyerBadMonth_u = bs.popUByte();
		BuyerBad6Month = bs.popUInt();
		BuyerBad6Month_u = bs.popUByte();
		BuyerBadAll = bs.popUInt();
		BuyerBadAll_u = bs.popUByte();
		SellerGoodWeek = bs.popUInt();
		SellerGoodWeek_u = bs.popUByte();
		SellerGoodMonth = bs.popUInt();
		SellerGoodMonth_u = bs.popUByte();
		SellerGood6Month = bs.popUInt();
		SellerGood6Month_u = bs.popUByte();
		SellerGoodAll = bs.popUInt();
		SellerGoodAll_u = bs.popUByte();
		SellerNormalWeek = bs.popUInt();
		SellerNormalWeek_u = bs.popUByte();
		SellerNormalMonth = bs.popUInt();
		SellerNormalMonth_u = bs.popUByte();
		SellerNormal6Month = bs.popUInt();
		SellerNormal6Month_u = bs.popUByte();
		SellerNormalAll = bs.popUInt();
		SellerNormalAll_u = bs.popUByte();
		SellerBadWeek = bs.popUInt();
		SellerBadWeek_u = bs.popUByte();
		SellerBadMonth = bs.popUInt();
		SellerBadMonth_u = bs.popUByte();
		SellerBad6Month = bs.popUInt();
		SellerBad6Month_u = bs.popUByte();
		SellerBadAll = bs.popUInt();
		SellerBadAll_u = bs.popUByte();
		LastUpdateTime = bs.popUInt();
		LastUpdateTime_u = bs.popUByte();
		SellerCreditAll = bs.popInt();
		SellerCreditAll_u = bs.popUByte();
		SellerCreditNormal = bs.popInt();
		SellerCreditNormal_u = bs.popUByte();
		SellerCreditVirtual = bs.popInt();
		SellerCreditVirtual_u = bs.popUByte();
		BuyerCreditAll = bs.popInt();
		BuyerCreditAll_u = bs.popUByte();
		BuyerCreditNormal = bs.popInt();
		BuyerCreditNormal_u = bs.popUByte();
		BuyerCreditVirtual = bs.popInt();
		BuyerCreditVirtual_u = bs.popUByte();
		Dsr1Total = bs.popUInt();
		Dsr1Total_u = bs.popUByte();
		Dsr2Total = bs.popUInt();
		Dsr2Total_u = bs.popUByte();
		Dsr3Total = bs.popUInt();
		Dsr3Total_u = bs.popUByte();
		EvalNums = bs.popInt();
		EvalNums_u = bs.popUByte();
		SellerLevelTotal = bs.popInt();
		SellerLevelTotal_u = bs.popUByte();
		DsrTotal = bs.popUInt();
		DsrTotal_u = bs.popUByte();
		WaitForSeller = bs.popUInt();
		WaitForSeller_u = bs.popUByte();
		WaitForBuyer = bs.popUInt();
		WaitForBuyer_u = bs.popUByte();
		ContentWeek = bs.popUInt();
		ContentWeek_u = bs.popUByte();
		ContentMon = bs.popUInt();
		ContentMon_u = bs.popUByte();
		Content6mon = bs.popUInt();
		Content6mon_u = bs.popUByte();
		BuyerMakeGoodWeek = bs.popUInt();
		BuyerMakeGoodWeek_u = bs.popUByte();
		BuyerMakeNorWeek = bs.popUInt();
		BuyerMakeNorWeek_u = bs.popUByte();
		BuyerMakeBadWeek = bs.popUInt();
		BuyerMakeBadWeek_u = bs.popUByte();
		BuyerMakeGoodMon = bs.popUInt();
		BuyerMakeGoodMon_u = bs.popUByte();
		BuyerMakeNorMon = bs.popUInt();
		BuyerMakeNorMon_u = bs.popUByte();
		BuyerMakeBadMon = bs.popUInt();
		BuyerMakeBadMon_u = bs.popUByte();
		BuyerMakeGood6mon = bs.popUInt();
		BuyerMakeGood6mon_u = bs.popUByte();
		BuyerMakeNor6mon = bs.popUInt();
		BuyerMakeNor6mon_u = bs.popUByte();
		BuyerMakeBad6mon = bs.popUInt();
		BuyerMakeBad6mon_u = bs.popUByte();
		SellerMakeGoodWeek = bs.popUInt();
		SellerMakeGoodWeek_u = bs.popUByte();
		SellerMakeNorWeek = bs.popUInt();
		SellerMakeNorWeek_u = bs.popUByte();
		SellerMakeBadWeek = bs.popUInt();
		SellerMakeBadWeek_u = bs.popUByte();
		SellerMakeGoodMon = bs.popUInt();
		SellerMakeGoodMon_u = bs.popUByte();
		SellerMakeNorMon = bs.popUInt();
		SellerMakeNorMon_u = bs.popUByte();
		SellerMakeBadMon = bs.popUInt();
		SellerMakeBadMon_u = bs.popUByte();
		SellerMakeGood6mon = bs.popUInt();
		SellerMakeGood6mon_u = bs.popUByte();
		SellerMakeNor6mon = bs.popUInt();
		SellerMakeNor6mon_u = bs.popUByte();
		SellerMakeBad6mon = bs.popUInt();
		SellerMakeBad6mon_u = bs.popUByte();
		DateTime = bs.popUInt();
		DateTime_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @return UserUin value 类型为:long
	 * 
	 */
	public long getUserUin()
	{
		return UserUin;
	}


	/**
	 * 设置卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUserUin(long value)
	{
		this.UserUin = value;
		this.UserUin_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return UserUin_u value 类型为:short
	 * 
	 */
	public short getUserUin_u()
	{
		return UserUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUserUin_u(short value)
	{
		this.UserUin_u = value;
	}


	/**
	 * 获取(作为买家)一周好评数
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerGoodWeek value 类型为:long
	 * 
	 */
	public long getBuyerGoodWeek()
	{
		return BuyerGoodWeek;
	}


	/**
	 * 设置(作为买家)一周好评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerGoodWeek(long value)
	{
		this.BuyerGoodWeek = value;
		this.BuyerGoodWeek_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerGoodWeek_u value 类型为:short
	 * 
	 */
	public short getBuyerGoodWeek_u()
	{
		return BuyerGoodWeek_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerGoodWeek_u(short value)
	{
		this.BuyerGoodWeek_u = value;
	}


	/**
	 * 获取(作为买家)一个月好评数
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerGoodMonth value 类型为:long
	 * 
	 */
	public long getBuyerGoodMonth()
	{
		return BuyerGoodMonth;
	}


	/**
	 * 设置(作为买家)一个月好评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerGoodMonth(long value)
	{
		this.BuyerGoodMonth = value;
		this.BuyerGoodMonth_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerGoodMonth_u value 类型为:short
	 * 
	 */
	public short getBuyerGoodMonth_u()
	{
		return BuyerGoodMonth_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerGoodMonth_u(short value)
	{
		this.BuyerGoodMonth_u = value;
	}


	/**
	 * 获取(作为买家)六个月好评数
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerGood6Month value 类型为:long
	 * 
	 */
	public long getBuyerGood6Month()
	{
		return BuyerGood6Month;
	}


	/**
	 * 设置(作为买家)六个月好评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerGood6Month(long value)
	{
		this.BuyerGood6Month = value;
		this.BuyerGood6Month_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerGood6Month_u value 类型为:short
	 * 
	 */
	public short getBuyerGood6Month_u()
	{
		return BuyerGood6Month_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerGood6Month_u(short value)
	{
		this.BuyerGood6Month_u = value;
	}


	/**
	 * 获取(作为买家)好评总数
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerGoodAll value 类型为:long
	 * 
	 */
	public long getBuyerGoodAll()
	{
		return BuyerGoodAll;
	}


	/**
	 * 设置(作为买家)好评总数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerGoodAll(long value)
	{
		this.BuyerGoodAll = value;
		this.BuyerGoodAll_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerGoodAll_u value 类型为:short
	 * 
	 */
	public short getBuyerGoodAll_u()
	{
		return BuyerGoodAll_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerGoodAll_u(short value)
	{
		this.BuyerGoodAll_u = value;
	}


	/**
	 * 获取(作为买家)一周中评数
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerNormalWeek value 类型为:long
	 * 
	 */
	public long getBuyerNormalWeek()
	{
		return BuyerNormalWeek;
	}


	/**
	 * 设置(作为买家)一周中评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerNormalWeek(long value)
	{
		this.BuyerNormalWeek = value;
		this.BuyerNormalWeek_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerNormalWeek_u value 类型为:short
	 * 
	 */
	public short getBuyerNormalWeek_u()
	{
		return BuyerNormalWeek_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerNormalWeek_u(short value)
	{
		this.BuyerNormalWeek_u = value;
	}


	/**
	 * 获取(作为买家)一个月中评数
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerNormalMonth value 类型为:long
	 * 
	 */
	public long getBuyerNormalMonth()
	{
		return BuyerNormalMonth;
	}


	/**
	 * 设置(作为买家)一个月中评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerNormalMonth(long value)
	{
		this.BuyerNormalMonth = value;
		this.BuyerNormalMonth_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerNormalMonth_u value 类型为:short
	 * 
	 */
	public short getBuyerNormalMonth_u()
	{
		return BuyerNormalMonth_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerNormalMonth_u(short value)
	{
		this.BuyerNormalMonth_u = value;
	}


	/**
	 * 获取(作为买家)六个月中评数
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerNormal6Month value 类型为:long
	 * 
	 */
	public long getBuyerNormal6Month()
	{
		return BuyerNormal6Month;
	}


	/**
	 * 设置(作为买家)六个月中评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerNormal6Month(long value)
	{
		this.BuyerNormal6Month = value;
		this.BuyerNormal6Month_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerNormal6Month_u value 类型为:short
	 * 
	 */
	public short getBuyerNormal6Month_u()
	{
		return BuyerNormal6Month_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerNormal6Month_u(short value)
	{
		this.BuyerNormal6Month_u = value;
	}


	/**
	 * 获取(作为买家)中评总数
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerNormalAll value 类型为:long
	 * 
	 */
	public long getBuyerNormalAll()
	{
		return BuyerNormalAll;
	}


	/**
	 * 设置(作为买家)中评总数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerNormalAll(long value)
	{
		this.BuyerNormalAll = value;
		this.BuyerNormalAll_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerNormalAll_u value 类型为:short
	 * 
	 */
	public short getBuyerNormalAll_u()
	{
		return BuyerNormalAll_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerNormalAll_u(short value)
	{
		this.BuyerNormalAll_u = value;
	}


	/**
	 * 获取(作为买家)一周差评数
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerBadWeek value 类型为:long
	 * 
	 */
	public long getBuyerBadWeek()
	{
		return BuyerBadWeek;
	}


	/**
	 * 设置(作为买家)一周差评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerBadWeek(long value)
	{
		this.BuyerBadWeek = value;
		this.BuyerBadWeek_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerBadWeek_u value 类型为:short
	 * 
	 */
	public short getBuyerBadWeek_u()
	{
		return BuyerBadWeek_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerBadWeek_u(short value)
	{
		this.BuyerBadWeek_u = value;
	}


	/**
	 * 获取(作为买家)一个月差评数
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerBadMonth value 类型为:long
	 * 
	 */
	public long getBuyerBadMonth()
	{
		return BuyerBadMonth;
	}


	/**
	 * 设置(作为买家)一个月差评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerBadMonth(long value)
	{
		this.BuyerBadMonth = value;
		this.BuyerBadMonth_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerBadMonth_u value 类型为:short
	 * 
	 */
	public short getBuyerBadMonth_u()
	{
		return BuyerBadMonth_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerBadMonth_u(short value)
	{
		this.BuyerBadMonth_u = value;
	}


	/**
	 * 获取(作为买家)六个月差评数
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerBad6Month value 类型为:long
	 * 
	 */
	public long getBuyerBad6Month()
	{
		return BuyerBad6Month;
	}


	/**
	 * 设置(作为买家)六个月差评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerBad6Month(long value)
	{
		this.BuyerBad6Month = value;
		this.BuyerBad6Month_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerBad6Month_u value 类型为:short
	 * 
	 */
	public short getBuyerBad6Month_u()
	{
		return BuyerBad6Month_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerBad6Month_u(short value)
	{
		this.BuyerBad6Month_u = value;
	}


	/**
	 * 获取(作为买家)差评总数
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerBadAll value 类型为:long
	 * 
	 */
	public long getBuyerBadAll()
	{
		return BuyerBadAll;
	}


	/**
	 * 设置(作为买家)差评总数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerBadAll(long value)
	{
		this.BuyerBadAll = value;
		this.BuyerBadAll_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerBadAll_u value 类型为:short
	 * 
	 */
	public short getBuyerBadAll_u()
	{
		return BuyerBadAll_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerBadAll_u(short value)
	{
		this.BuyerBadAll_u = value;
	}


	/**
	 * 获取(作为卖家)一周好评数
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerGoodWeek value 类型为:long
	 * 
	 */
	public long getSellerGoodWeek()
	{
		return SellerGoodWeek;
	}


	/**
	 * 设置(作为卖家)一周好评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerGoodWeek(long value)
	{
		this.SellerGoodWeek = value;
		this.SellerGoodWeek_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerGoodWeek_u value 类型为:short
	 * 
	 */
	public short getSellerGoodWeek_u()
	{
		return SellerGoodWeek_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerGoodWeek_u(short value)
	{
		this.SellerGoodWeek_u = value;
	}


	/**
	 * 获取(作为卖家)一个月好评数
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerGoodMonth value 类型为:long
	 * 
	 */
	public long getSellerGoodMonth()
	{
		return SellerGoodMonth;
	}


	/**
	 * 设置(作为卖家)一个月好评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerGoodMonth(long value)
	{
		this.SellerGoodMonth = value;
		this.SellerGoodMonth_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerGoodMonth_u value 类型为:short
	 * 
	 */
	public short getSellerGoodMonth_u()
	{
		return SellerGoodMonth_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerGoodMonth_u(short value)
	{
		this.SellerGoodMonth_u = value;
	}


	/**
	 * 获取(作为卖家)六个月好评数
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerGood6Month value 类型为:long
	 * 
	 */
	public long getSellerGood6Month()
	{
		return SellerGood6Month;
	}


	/**
	 * 设置(作为卖家)六个月好评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerGood6Month(long value)
	{
		this.SellerGood6Month = value;
		this.SellerGood6Month_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerGood6Month_u value 类型为:short
	 * 
	 */
	public short getSellerGood6Month_u()
	{
		return SellerGood6Month_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerGood6Month_u(short value)
	{
		this.SellerGood6Month_u = value;
	}


	/**
	 * 获取(作为卖家)好评总数
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerGoodAll value 类型为:long
	 * 
	 */
	public long getSellerGoodAll()
	{
		return SellerGoodAll;
	}


	/**
	 * 设置(作为卖家)好评总数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerGoodAll(long value)
	{
		this.SellerGoodAll = value;
		this.SellerGoodAll_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerGoodAll_u value 类型为:short
	 * 
	 */
	public short getSellerGoodAll_u()
	{
		return SellerGoodAll_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerGoodAll_u(short value)
	{
		this.SellerGoodAll_u = value;
	}


	/**
	 * 获取(作为卖家)一周中评数
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerNormalWeek value 类型为:long
	 * 
	 */
	public long getSellerNormalWeek()
	{
		return SellerNormalWeek;
	}


	/**
	 * 设置(作为卖家)一周中评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerNormalWeek(long value)
	{
		this.SellerNormalWeek = value;
		this.SellerNormalWeek_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerNormalWeek_u value 类型为:short
	 * 
	 */
	public short getSellerNormalWeek_u()
	{
		return SellerNormalWeek_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerNormalWeek_u(short value)
	{
		this.SellerNormalWeek_u = value;
	}


	/**
	 * 获取(作为卖家)一个月中评数
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerNormalMonth value 类型为:long
	 * 
	 */
	public long getSellerNormalMonth()
	{
		return SellerNormalMonth;
	}


	/**
	 * 设置(作为卖家)一个月中评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerNormalMonth(long value)
	{
		this.SellerNormalMonth = value;
		this.SellerNormalMonth_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerNormalMonth_u value 类型为:short
	 * 
	 */
	public short getSellerNormalMonth_u()
	{
		return SellerNormalMonth_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerNormalMonth_u(short value)
	{
		this.SellerNormalMonth_u = value;
	}


	/**
	 * 获取(作为卖家)六个月中评数
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerNormal6Month value 类型为:long
	 * 
	 */
	public long getSellerNormal6Month()
	{
		return SellerNormal6Month;
	}


	/**
	 * 设置(作为卖家)六个月中评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerNormal6Month(long value)
	{
		this.SellerNormal6Month = value;
		this.SellerNormal6Month_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerNormal6Month_u value 类型为:short
	 * 
	 */
	public short getSellerNormal6Month_u()
	{
		return SellerNormal6Month_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerNormal6Month_u(short value)
	{
		this.SellerNormal6Month_u = value;
	}


	/**
	 * 获取(作为卖家)中评总数
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerNormalAll value 类型为:long
	 * 
	 */
	public long getSellerNormalAll()
	{
		return SellerNormalAll;
	}


	/**
	 * 设置(作为卖家)中评总数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerNormalAll(long value)
	{
		this.SellerNormalAll = value;
		this.SellerNormalAll_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerNormalAll_u value 类型为:short
	 * 
	 */
	public short getSellerNormalAll_u()
	{
		return SellerNormalAll_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerNormalAll_u(short value)
	{
		this.SellerNormalAll_u = value;
	}


	/**
	 * 获取(作为卖家)一周差评数
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerBadWeek value 类型为:long
	 * 
	 */
	public long getSellerBadWeek()
	{
		return SellerBadWeek;
	}


	/**
	 * 设置(作为卖家)一周差评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerBadWeek(long value)
	{
		this.SellerBadWeek = value;
		this.SellerBadWeek_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerBadWeek_u value 类型为:short
	 * 
	 */
	public short getSellerBadWeek_u()
	{
		return SellerBadWeek_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerBadWeek_u(short value)
	{
		this.SellerBadWeek_u = value;
	}


	/**
	 * 获取(作为卖家)一个月差评数
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerBadMonth value 类型为:long
	 * 
	 */
	public long getSellerBadMonth()
	{
		return SellerBadMonth;
	}


	/**
	 * 设置(作为卖家)一个月差评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerBadMonth(long value)
	{
		this.SellerBadMonth = value;
		this.SellerBadMonth_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerBadMonth_u value 类型为:short
	 * 
	 */
	public short getSellerBadMonth_u()
	{
		return SellerBadMonth_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerBadMonth_u(short value)
	{
		this.SellerBadMonth_u = value;
	}


	/**
	 * 获取(作为卖家)六个月差评数
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerBad6Month value 类型为:long
	 * 
	 */
	public long getSellerBad6Month()
	{
		return SellerBad6Month;
	}


	/**
	 * 设置(作为卖家)六个月差评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerBad6Month(long value)
	{
		this.SellerBad6Month = value;
		this.SellerBad6Month_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerBad6Month_u value 类型为:short
	 * 
	 */
	public short getSellerBad6Month_u()
	{
		return SellerBad6Month_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerBad6Month_u(short value)
	{
		this.SellerBad6Month_u = value;
	}


	/**
	 * 获取(作为卖家)差评总数
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerBadAll value 类型为:long
	 * 
	 */
	public long getSellerBadAll()
	{
		return SellerBadAll;
	}


	/**
	 * 设置(作为卖家)差评总数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerBadAll(long value)
	{
		this.SellerBadAll = value;
		this.SellerBadAll_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerBadAll_u value 类型为:short
	 * 
	 */
	public short getSellerBadAll_u()
	{
		return SellerBadAll_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerBadAll_u(short value)
	{
		this.SellerBadAll_u = value;
	}


	/**
	 * 获取最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return LastUpdateTime value 类型为:long
	 * 
	 */
	public long getLastUpdateTime()
	{
		return LastUpdateTime;
	}


	/**
	 * 设置最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastUpdateTime(long value)
	{
		this.LastUpdateTime = value;
		this.LastUpdateTime_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return LastUpdateTime_u value 类型为:short
	 * 
	 */
	public short getLastUpdateTime_u()
	{
		return LastUpdateTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastUpdateTime_u(short value)
	{
		this.LastUpdateTime_u = value;
	}


	/**
	 * 获取卖家信用
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerCreditAll value 类型为:int
	 * 
	 */
	public int getSellerCreditAll()
	{
		return SellerCreditAll;
	}


	/**
	 * 设置卖家信用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setSellerCreditAll(int value)
	{
		this.SellerCreditAll = value;
		this.SellerCreditAll_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerCreditAll_u value 类型为:short
	 * 
	 */
	public short getSellerCreditAll_u()
	{
		return SellerCreditAll_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerCreditAll_u(short value)
	{
		this.SellerCreditAll_u = value;
	}


	/**
	 * 获取卖家信用-实物
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerCreditNormal value 类型为:int
	 * 
	 */
	public int getSellerCreditNormal()
	{
		return SellerCreditNormal;
	}


	/**
	 * 设置卖家信用-实物
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setSellerCreditNormal(int value)
	{
		this.SellerCreditNormal = value;
		this.SellerCreditNormal_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerCreditNormal_u value 类型为:short
	 * 
	 */
	public short getSellerCreditNormal_u()
	{
		return SellerCreditNormal_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerCreditNormal_u(short value)
	{
		this.SellerCreditNormal_u = value;
	}


	/**
	 * 获取卖家信用-虚拟
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerCreditVirtual value 类型为:int
	 * 
	 */
	public int getSellerCreditVirtual()
	{
		return SellerCreditVirtual;
	}


	/**
	 * 设置卖家信用-虚拟
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setSellerCreditVirtual(int value)
	{
		this.SellerCreditVirtual = value;
		this.SellerCreditVirtual_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerCreditVirtual_u value 类型为:short
	 * 
	 */
	public short getSellerCreditVirtual_u()
	{
		return SellerCreditVirtual_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerCreditVirtual_u(short value)
	{
		this.SellerCreditVirtual_u = value;
	}


	/**
	 * 获取买家信用
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerCreditAll value 类型为:int
	 * 
	 */
	public int getBuyerCreditAll()
	{
		return BuyerCreditAll;
	}


	/**
	 * 设置买家信用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setBuyerCreditAll(int value)
	{
		this.BuyerCreditAll = value;
		this.BuyerCreditAll_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerCreditAll_u value 类型为:short
	 * 
	 */
	public short getBuyerCreditAll_u()
	{
		return BuyerCreditAll_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerCreditAll_u(short value)
	{
		this.BuyerCreditAll_u = value;
	}


	/**
	 * 获取买家信用-实物
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerCreditNormal value 类型为:int
	 * 
	 */
	public int getBuyerCreditNormal()
	{
		return BuyerCreditNormal;
	}


	/**
	 * 设置买家信用-实物
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setBuyerCreditNormal(int value)
	{
		this.BuyerCreditNormal = value;
		this.BuyerCreditNormal_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerCreditNormal_u value 类型为:short
	 * 
	 */
	public short getBuyerCreditNormal_u()
	{
		return BuyerCreditNormal_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerCreditNormal_u(short value)
	{
		this.BuyerCreditNormal_u = value;
	}


	/**
	 * 获取买家信用-虚拟
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerCreditVirtual value 类型为:int
	 * 
	 */
	public int getBuyerCreditVirtual()
	{
		return BuyerCreditVirtual;
	}


	/**
	 * 设置买家信用-虚拟
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setBuyerCreditVirtual(int value)
	{
		this.BuyerCreditVirtual = value;
		this.BuyerCreditVirtual_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerCreditVirtual_u value 类型为:short
	 * 
	 */
	public short getBuyerCreditVirtual_u()
	{
		return BuyerCreditVirtual_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerCreditVirtual_u(short value)
	{
		this.BuyerCreditVirtual_u = value;
	}


	/**
	 * 获取Dsr1
	 * 
	 * 此字段的版本 >= 0
	 * @return Dsr1Total value 类型为:long
	 * 
	 */
	public long getDsr1Total()
	{
		return Dsr1Total;
	}


	/**
	 * 设置Dsr1
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDsr1Total(long value)
	{
		this.Dsr1Total = value;
		this.Dsr1Total_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Dsr1Total_u value 类型为:short
	 * 
	 */
	public short getDsr1Total_u()
	{
		return Dsr1Total_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDsr1Total_u(short value)
	{
		this.Dsr1Total_u = value;
	}


	/**
	 * 获取Dsr2
	 * 
	 * 此字段的版本 >= 0
	 * @return Dsr2Total value 类型为:long
	 * 
	 */
	public long getDsr2Total()
	{
		return Dsr2Total;
	}


	/**
	 * 设置Dsr2
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDsr2Total(long value)
	{
		this.Dsr2Total = value;
		this.Dsr2Total_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Dsr2Total_u value 类型为:short
	 * 
	 */
	public short getDsr2Total_u()
	{
		return Dsr2Total_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDsr2Total_u(short value)
	{
		this.Dsr2Total_u = value;
	}


	/**
	 * 获取Dsr3
	 * 
	 * 此字段的版本 >= 0
	 * @return Dsr3Total value 类型为:long
	 * 
	 */
	public long getDsr3Total()
	{
		return Dsr3Total;
	}


	/**
	 * 设置Dsr3
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDsr3Total(long value)
	{
		this.Dsr3Total = value;
		this.Dsr3Total_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Dsr3Total_u value 类型为:short
	 * 
	 */
	public short getDsr3Total_u()
	{
		return Dsr3Total_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDsr3Total_u(short value)
	{
		this.Dsr3Total_u = value;
	}


	/**
	 * 获取评价总数
	 * 
	 * 此字段的版本 >= 0
	 * @return EvalNums value 类型为:int
	 * 
	 */
	public int getEvalNums()
	{
		return EvalNums;
	}


	/**
	 * 设置评价总数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setEvalNums(int value)
	{
		this.EvalNums = value;
		this.EvalNums_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return EvalNums_u value 类型为:short
	 * 
	 */
	public short getEvalNums_u()
	{
		return EvalNums_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEvalNums_u(short value)
	{
		this.EvalNums_u = value;
	}


	/**
	 * 获取卖家等级
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerLevelTotal value 类型为:int
	 * 
	 */
	public int getSellerLevelTotal()
	{
		return SellerLevelTotal;
	}


	/**
	 * 设置卖家等级
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setSellerLevelTotal(int value)
	{
		this.SellerLevelTotal = value;
		this.SellerLevelTotal_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerLevelTotal_u value 类型为:short
	 * 
	 */
	public short getSellerLevelTotal_u()
	{
		return SellerLevelTotal_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerLevelTotal_u(short value)
	{
		this.SellerLevelTotal_u = value;
	}


	/**
	 * 获取DsrTotal
	 * 
	 * 此字段的版本 >= 0
	 * @return DsrTotal value 类型为:long
	 * 
	 */
	public long getDsrTotal()
	{
		return DsrTotal;
	}


	/**
	 * 设置DsrTotal
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDsrTotal(long value)
	{
		this.DsrTotal = value;
		this.DsrTotal_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DsrTotal_u value 类型为:short
	 * 
	 */
	public short getDsrTotal_u()
	{
		return DsrTotal_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDsrTotal_u(short value)
	{
		this.DsrTotal_u = value;
	}


	/**
	 * 获取(作为卖家)等待评价
	 * 
	 * 此字段的版本 >= 0
	 * @return WaitForSeller value 类型为:long
	 * 
	 */
	public long getWaitForSeller()
	{
		return WaitForSeller;
	}


	/**
	 * 设置(作为卖家)等待评价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWaitForSeller(long value)
	{
		this.WaitForSeller = value;
		this.WaitForSeller_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return WaitForSeller_u value 类型为:short
	 * 
	 */
	public short getWaitForSeller_u()
	{
		return WaitForSeller_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWaitForSeller_u(short value)
	{
		this.WaitForSeller_u = value;
	}


	/**
	 * 获取(作为买家)等待评价
	 * 
	 * 此字段的版本 >= 0
	 * @return WaitForBuyer value 类型为:long
	 * 
	 */
	public long getWaitForBuyer()
	{
		return WaitForBuyer;
	}


	/**
	 * 设置(作为买家)等待评价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWaitForBuyer(long value)
	{
		this.WaitForBuyer = value;
		this.WaitForBuyer_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return WaitForBuyer_u value 类型为:short
	 * 
	 */
	public short getWaitForBuyer_u()
	{
		return WaitForBuyer_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWaitForBuyer_u(short value)
	{
		this.WaitForBuyer_u = value;
	}


	/**
	 * 获取有内容评价--一周
	 * 
	 * 此字段的版本 >= 0
	 * @return ContentWeek value 类型为:long
	 * 
	 */
	public long getContentWeek()
	{
		return ContentWeek;
	}


	/**
	 * 设置有内容评价--一周
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setContentWeek(long value)
	{
		this.ContentWeek = value;
		this.ContentWeek_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ContentWeek_u value 类型为:short
	 * 
	 */
	public short getContentWeek_u()
	{
		return ContentWeek_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setContentWeek_u(short value)
	{
		this.ContentWeek_u = value;
	}


	/**
	 * 获取有内容评价--一个月
	 * 
	 * 此字段的版本 >= 0
	 * @return ContentMon value 类型为:long
	 * 
	 */
	public long getContentMon()
	{
		return ContentMon;
	}


	/**
	 * 设置有内容评价--一个月
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setContentMon(long value)
	{
		this.ContentMon = value;
		this.ContentMon_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ContentMon_u value 类型为:short
	 * 
	 */
	public short getContentMon_u()
	{
		return ContentMon_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setContentMon_u(short value)
	{
		this.ContentMon_u = value;
	}


	/**
	 * 获取有内容评价--六个月
	 * 
	 * 此字段的版本 >= 0
	 * @return Content6mon value 类型为:long
	 * 
	 */
	public long getContent6mon()
	{
		return Content6mon;
	}


	/**
	 * 设置有内容评价--六个月
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setContent6mon(long value)
	{
		this.Content6mon = value;
		this.Content6mon_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Content6mon_u value 类型为:short
	 * 
	 */
	public short getContent6mon_u()
	{
		return Content6mon_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setContent6mon_u(short value)
	{
		this.Content6mon_u = value;
	}


	/**
	 * 获取(作为买家)一周做出好评数
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerMakeGoodWeek value 类型为:long
	 * 
	 */
	public long getBuyerMakeGoodWeek()
	{
		return BuyerMakeGoodWeek;
	}


	/**
	 * 设置(作为买家)一周做出好评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerMakeGoodWeek(long value)
	{
		this.BuyerMakeGoodWeek = value;
		this.BuyerMakeGoodWeek_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerMakeGoodWeek_u value 类型为:short
	 * 
	 */
	public short getBuyerMakeGoodWeek_u()
	{
		return BuyerMakeGoodWeek_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerMakeGoodWeek_u(short value)
	{
		this.BuyerMakeGoodWeek_u = value;
	}


	/**
	 * 获取(作为买家)一周做出中评数
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerMakeNorWeek value 类型为:long
	 * 
	 */
	public long getBuyerMakeNorWeek()
	{
		return BuyerMakeNorWeek;
	}


	/**
	 * 设置(作为买家)一周做出中评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerMakeNorWeek(long value)
	{
		this.BuyerMakeNorWeek = value;
		this.BuyerMakeNorWeek_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerMakeNorWeek_u value 类型为:short
	 * 
	 */
	public short getBuyerMakeNorWeek_u()
	{
		return BuyerMakeNorWeek_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerMakeNorWeek_u(short value)
	{
		this.BuyerMakeNorWeek_u = value;
	}


	/**
	 * 获取(作为买家)一周做出差评数
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerMakeBadWeek value 类型为:long
	 * 
	 */
	public long getBuyerMakeBadWeek()
	{
		return BuyerMakeBadWeek;
	}


	/**
	 * 设置(作为买家)一周做出差评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerMakeBadWeek(long value)
	{
		this.BuyerMakeBadWeek = value;
		this.BuyerMakeBadWeek_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerMakeBadWeek_u value 类型为:short
	 * 
	 */
	public short getBuyerMakeBadWeek_u()
	{
		return BuyerMakeBadWeek_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerMakeBadWeek_u(short value)
	{
		this.BuyerMakeBadWeek_u = value;
	}


	/**
	 * 获取(作为买家)一个月做出的好评数
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerMakeGoodMon value 类型为:long
	 * 
	 */
	public long getBuyerMakeGoodMon()
	{
		return BuyerMakeGoodMon;
	}


	/**
	 * 设置(作为买家)一个月做出的好评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerMakeGoodMon(long value)
	{
		this.BuyerMakeGoodMon = value;
		this.BuyerMakeGoodMon_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerMakeGoodMon_u value 类型为:short
	 * 
	 */
	public short getBuyerMakeGoodMon_u()
	{
		return BuyerMakeGoodMon_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerMakeGoodMon_u(short value)
	{
		this.BuyerMakeGoodMon_u = value;
	}


	/**
	 * 获取(作为买家)一个月做出的中评数
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerMakeNorMon value 类型为:long
	 * 
	 */
	public long getBuyerMakeNorMon()
	{
		return BuyerMakeNorMon;
	}


	/**
	 * 设置(作为买家)一个月做出的中评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerMakeNorMon(long value)
	{
		this.BuyerMakeNorMon = value;
		this.BuyerMakeNorMon_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerMakeNorMon_u value 类型为:short
	 * 
	 */
	public short getBuyerMakeNorMon_u()
	{
		return BuyerMakeNorMon_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerMakeNorMon_u(short value)
	{
		this.BuyerMakeNorMon_u = value;
	}


	/**
	 * 获取(作为买家)一个月做出的差评数
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerMakeBadMon value 类型为:long
	 * 
	 */
	public long getBuyerMakeBadMon()
	{
		return BuyerMakeBadMon;
	}


	/**
	 * 设置(作为买家)一个月做出的差评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerMakeBadMon(long value)
	{
		this.BuyerMakeBadMon = value;
		this.BuyerMakeBadMon_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerMakeBadMon_u value 类型为:short
	 * 
	 */
	public short getBuyerMakeBadMon_u()
	{
		return BuyerMakeBadMon_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerMakeBadMon_u(short value)
	{
		this.BuyerMakeBadMon_u = value;
	}


	/**
	 * 获取(作为买家)六个月做出的好评数
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerMakeGood6mon value 类型为:long
	 * 
	 */
	public long getBuyerMakeGood6mon()
	{
		return BuyerMakeGood6mon;
	}


	/**
	 * 设置(作为买家)六个月做出的好评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerMakeGood6mon(long value)
	{
		this.BuyerMakeGood6mon = value;
		this.BuyerMakeGood6mon_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerMakeGood6mon_u value 类型为:short
	 * 
	 */
	public short getBuyerMakeGood6mon_u()
	{
		return BuyerMakeGood6mon_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerMakeGood6mon_u(short value)
	{
		this.BuyerMakeGood6mon_u = value;
	}


	/**
	 * 获取(作为买家)六个月做出的中评数
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerMakeNor6mon value 类型为:long
	 * 
	 */
	public long getBuyerMakeNor6mon()
	{
		return BuyerMakeNor6mon;
	}


	/**
	 * 设置(作为买家)六个月做出的中评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerMakeNor6mon(long value)
	{
		this.BuyerMakeNor6mon = value;
		this.BuyerMakeNor6mon_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerMakeNor6mon_u value 类型为:short
	 * 
	 */
	public short getBuyerMakeNor6mon_u()
	{
		return BuyerMakeNor6mon_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerMakeNor6mon_u(short value)
	{
		this.BuyerMakeNor6mon_u = value;
	}


	/**
	 * 获取(作为买家)六个月做出的差评数
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerMakeBad6mon value 类型为:long
	 * 
	 */
	public long getBuyerMakeBad6mon()
	{
		return BuyerMakeBad6mon;
	}


	/**
	 * 设置(作为买家)六个月做出的差评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerMakeBad6mon(long value)
	{
		this.BuyerMakeBad6mon = value;
		this.BuyerMakeBad6mon_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerMakeBad6mon_u value 类型为:short
	 * 
	 */
	public short getBuyerMakeBad6mon_u()
	{
		return BuyerMakeBad6mon_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerMakeBad6mon_u(short value)
	{
		this.BuyerMakeBad6mon_u = value;
	}


	/**
	 * 获取(作为卖家)一周做出好评数
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerMakeGoodWeek value 类型为:long
	 * 
	 */
	public long getSellerMakeGoodWeek()
	{
		return SellerMakeGoodWeek;
	}


	/**
	 * 设置(作为卖家)一周做出好评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerMakeGoodWeek(long value)
	{
		this.SellerMakeGoodWeek = value;
		this.SellerMakeGoodWeek_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerMakeGoodWeek_u value 类型为:short
	 * 
	 */
	public short getSellerMakeGoodWeek_u()
	{
		return SellerMakeGoodWeek_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerMakeGoodWeek_u(short value)
	{
		this.SellerMakeGoodWeek_u = value;
	}


	/**
	 * 获取(作为卖家)一周做出中评数
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerMakeNorWeek value 类型为:long
	 * 
	 */
	public long getSellerMakeNorWeek()
	{
		return SellerMakeNorWeek;
	}


	/**
	 * 设置(作为卖家)一周做出中评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerMakeNorWeek(long value)
	{
		this.SellerMakeNorWeek = value;
		this.SellerMakeNorWeek_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerMakeNorWeek_u value 类型为:short
	 * 
	 */
	public short getSellerMakeNorWeek_u()
	{
		return SellerMakeNorWeek_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerMakeNorWeek_u(short value)
	{
		this.SellerMakeNorWeek_u = value;
	}


	/**
	 * 获取(作为卖家)一周做出差评数
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerMakeBadWeek value 类型为:long
	 * 
	 */
	public long getSellerMakeBadWeek()
	{
		return SellerMakeBadWeek;
	}


	/**
	 * 设置(作为卖家)一周做出差评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerMakeBadWeek(long value)
	{
		this.SellerMakeBadWeek = value;
		this.SellerMakeBadWeek_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerMakeBadWeek_u value 类型为:short
	 * 
	 */
	public short getSellerMakeBadWeek_u()
	{
		return SellerMakeBadWeek_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerMakeBadWeek_u(short value)
	{
		this.SellerMakeBadWeek_u = value;
	}


	/**
	 * 获取(作为卖家)一个月做出好评数
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerMakeGoodMon value 类型为:long
	 * 
	 */
	public long getSellerMakeGoodMon()
	{
		return SellerMakeGoodMon;
	}


	/**
	 * 设置(作为卖家)一个月做出好评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerMakeGoodMon(long value)
	{
		this.SellerMakeGoodMon = value;
		this.SellerMakeGoodMon_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerMakeGoodMon_u value 类型为:short
	 * 
	 */
	public short getSellerMakeGoodMon_u()
	{
		return SellerMakeGoodMon_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerMakeGoodMon_u(short value)
	{
		this.SellerMakeGoodMon_u = value;
	}


	/**
	 * 获取(作为卖家)一个月做出中评数
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerMakeNorMon value 类型为:long
	 * 
	 */
	public long getSellerMakeNorMon()
	{
		return SellerMakeNorMon;
	}


	/**
	 * 设置(作为卖家)一个月做出中评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerMakeNorMon(long value)
	{
		this.SellerMakeNorMon = value;
		this.SellerMakeNorMon_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerMakeNorMon_u value 类型为:short
	 * 
	 */
	public short getSellerMakeNorMon_u()
	{
		return SellerMakeNorMon_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerMakeNorMon_u(short value)
	{
		this.SellerMakeNorMon_u = value;
	}


	/**
	 * 获取(作为卖家)一个月做出差评数
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerMakeBadMon value 类型为:long
	 * 
	 */
	public long getSellerMakeBadMon()
	{
		return SellerMakeBadMon;
	}


	/**
	 * 设置(作为卖家)一个月做出差评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerMakeBadMon(long value)
	{
		this.SellerMakeBadMon = value;
		this.SellerMakeBadMon_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerMakeBadMon_u value 类型为:short
	 * 
	 */
	public short getSellerMakeBadMon_u()
	{
		return SellerMakeBadMon_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerMakeBadMon_u(short value)
	{
		this.SellerMakeBadMon_u = value;
	}


	/**
	 * 获取(作为卖家)六个月做出好评数
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerMakeGood6mon value 类型为:long
	 * 
	 */
	public long getSellerMakeGood6mon()
	{
		return SellerMakeGood6mon;
	}


	/**
	 * 设置(作为卖家)六个月做出好评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerMakeGood6mon(long value)
	{
		this.SellerMakeGood6mon = value;
		this.SellerMakeGood6mon_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerMakeGood6mon_u value 类型为:short
	 * 
	 */
	public short getSellerMakeGood6mon_u()
	{
		return SellerMakeGood6mon_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerMakeGood6mon_u(short value)
	{
		this.SellerMakeGood6mon_u = value;
	}


	/**
	 * 获取(作为卖家)六个月做出中评数
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerMakeNor6mon value 类型为:long
	 * 
	 */
	public long getSellerMakeNor6mon()
	{
		return SellerMakeNor6mon;
	}


	/**
	 * 设置(作为卖家)六个月做出中评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerMakeNor6mon(long value)
	{
		this.SellerMakeNor6mon = value;
		this.SellerMakeNor6mon_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerMakeNor6mon_u value 类型为:short
	 * 
	 */
	public short getSellerMakeNor6mon_u()
	{
		return SellerMakeNor6mon_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerMakeNor6mon_u(short value)
	{
		this.SellerMakeNor6mon_u = value;
	}


	/**
	 * 获取(作为卖家)六个月做出差评数
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerMakeBad6mon value 类型为:long
	 * 
	 */
	public long getSellerMakeBad6mon()
	{
		return SellerMakeBad6mon;
	}


	/**
	 * 设置(作为卖家)六个月做出差评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerMakeBad6mon(long value)
	{
		this.SellerMakeBad6mon = value;
		this.SellerMakeBad6mon_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerMakeBad6mon_u value 类型为:short
	 * 
	 */
	public short getSellerMakeBad6mon_u()
	{
		return SellerMakeBad6mon_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerMakeBad6mon_u(short value)
	{
		this.SellerMakeBad6mon_u = value;
	}


	/**
	 * 获取时间
	 * 
	 * 此字段的版本 >= 0
	 * @return DateTime value 类型为:long
	 * 
	 */
	public long getDateTime()
	{
		return DateTime;
	}


	/**
	 * 设置时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDateTime(long value)
	{
		this.DateTime = value;
		this.DateTime_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DateTime_u value 类型为:short
	 * 
	 */
	public short getDateTime_u()
	{
		return DateTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDateTime_u(short value)
	{
		this.DateTime_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(EvalStat)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段UserUin的长度 size_of(uint32_t)
				length += 1;  //计算字段UserUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerGoodWeek的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerGoodWeek_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerGoodMonth的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerGoodMonth_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerGood6Month的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerGood6Month_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerGoodAll的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerGoodAll_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerNormalWeek的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerNormalWeek_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerNormalMonth的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerNormalMonth_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerNormal6Month的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerNormal6Month_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerNormalAll的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerNormalAll_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerBadWeek的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerBadWeek_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerBadMonth的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerBadMonth_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerBad6Month的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerBad6Month_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerBadAll的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerBadAll_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerGoodWeek的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerGoodWeek_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerGoodMonth的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerGoodMonth_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerGood6Month的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerGood6Month_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerGoodAll的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerGoodAll_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerNormalWeek的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerNormalWeek_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerNormalMonth的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerNormalMonth_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerNormal6Month的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerNormal6Month_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerNormalAll的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerNormalAll_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerBadWeek的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerBadWeek_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerBadMonth的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerBadMonth_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerBad6Month的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerBad6Month_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerBadAll的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerBadAll_u的长度 size_of(uint8_t)
				length += 4;  //计算字段LastUpdateTime的长度 size_of(uint32_t)
				length += 1;  //计算字段LastUpdateTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerCreditAll的长度 size_of(int)
				length += 1;  //计算字段SellerCreditAll_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerCreditNormal的长度 size_of(int)
				length += 1;  //计算字段SellerCreditNormal_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerCreditVirtual的长度 size_of(int)
				length += 1;  //计算字段SellerCreditVirtual_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerCreditAll的长度 size_of(int)
				length += 1;  //计算字段BuyerCreditAll_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerCreditNormal的长度 size_of(int)
				length += 1;  //计算字段BuyerCreditNormal_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerCreditVirtual的长度 size_of(int)
				length += 1;  //计算字段BuyerCreditVirtual_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Dsr1Total的长度 size_of(uint32_t)
				length += 1;  //计算字段Dsr1Total_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Dsr2Total的长度 size_of(uint32_t)
				length += 1;  //计算字段Dsr2Total_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Dsr3Total的长度 size_of(uint32_t)
				length += 1;  //计算字段Dsr3Total_u的长度 size_of(uint8_t)
				length += 4;  //计算字段EvalNums的长度 size_of(int)
				length += 1;  //计算字段EvalNums_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerLevelTotal的长度 size_of(int)
				length += 1;  //计算字段SellerLevelTotal_u的长度 size_of(uint8_t)
				length += 4;  //计算字段DsrTotal的长度 size_of(uint32_t)
				length += 1;  //计算字段DsrTotal_u的长度 size_of(uint8_t)
				length += 4;  //计算字段WaitForSeller的长度 size_of(uint32_t)
				length += 1;  //计算字段WaitForSeller_u的长度 size_of(uint8_t)
				length += 4;  //计算字段WaitForBuyer的长度 size_of(uint32_t)
				length += 1;  //计算字段WaitForBuyer_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ContentWeek的长度 size_of(uint32_t)
				length += 1;  //计算字段ContentWeek_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ContentMon的长度 size_of(uint32_t)
				length += 1;  //计算字段ContentMon_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Content6mon的长度 size_of(uint32_t)
				length += 1;  //计算字段Content6mon_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerMakeGoodWeek的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerMakeGoodWeek_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerMakeNorWeek的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerMakeNorWeek_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerMakeBadWeek的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerMakeBadWeek_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerMakeGoodMon的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerMakeGoodMon_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerMakeNorMon的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerMakeNorMon_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerMakeBadMon的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerMakeBadMon_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerMakeGood6mon的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerMakeGood6mon_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerMakeNor6mon的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerMakeNor6mon_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerMakeBad6mon的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerMakeBad6mon_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerMakeGoodWeek的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerMakeGoodWeek_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerMakeNorWeek的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerMakeNorWeek_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerMakeBadWeek的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerMakeBadWeek_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerMakeGoodMon的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerMakeGoodMon_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerMakeNorMon的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerMakeNorMon_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerMakeBadMon的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerMakeBadMon_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerMakeGood6mon的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerMakeGood6mon_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerMakeNor6mon的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerMakeNor6mon_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerMakeBad6mon的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerMakeBad6mon_u的长度 size_of(uint8_t)
				length += 4;  //计算字段DateTime的长度 size_of(uint32_t)
				length += 1;  //计算字段DateTime_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
