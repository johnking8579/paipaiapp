 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import java.util.Vector;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *
 *
 *@date 2011-06-08 04:38::32
 *
 *@since version:1
*/
public class MStock  implements ICanSerializeObject
{
	/**
	 * 商品库存id
	 *
	 * 版本 >= 0
	 */
	 private String stockId = new String();

	/**
	 * 商品库存价格
	 *
	 * 版本 >= 0
	 */
	 private int stockPrice;

	/**
	 * 商品的库存属性串
	 *
	 * 版本 >= 0
	 */
	 private String stockAttr = new String();

	/**
	 * 商品的该库存对应的销售数量
	 *
	 * 版本 >= 0
	 */
	 private int soldCount;

	/**
	 * 商品的库存数量
	 *
	 * 版本 >= 0
	 */
	 private int stockCount;

	/**
	 * 商品1~6级彩钻价格
	 *
	 * 版本 >= 0
	 */
	 private Vector<Integer> discountPrice = new Vector<Integer>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(stockId);
		bs.pushInt(stockPrice);
		bs.pushString(stockAttr);
		bs.pushInt(soldCount);
		bs.pushInt(stockCount);
		bs.pushObject(discountPrice);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		stockId = bs.popString();
		stockPrice = bs.popInt();
		stockAttr = bs.popString();
		soldCount = bs.popInt();
		stockCount = bs.popInt();
		discountPrice = (Vector<Integer>)bs.popVector(Integer.class);
		return bs.getReadLength();
	} 


	/**
	 * 获取商品库存id
	 * 
	 * 此字段的版本 >= 0
	 * @return stockId value 类型为:String
	 * 
	 */
	public String getStockId()
	{
		return stockId;
	}


	/**
	 * 设置商品库存id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStockId(String value)
	{
		if (value != null) {
				this.stockId = value;
		}else{
				this.stockId = new String();
		}
	}


	/**
	 * 获取商品库存价格
	 * 
	 * 此字段的版本 >= 0
	 * @return stockPrice value 类型为:int
	 * 
	 */
	public int getStockPrice()
	{
		return stockPrice;
	}


	/**
	 * 设置商品库存价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setStockPrice(int value)
	{
		this.stockPrice = value;
	}


	/**
	 * 获取商品的库存属性串
	 * 
	 * 此字段的版本 >= 0
	 * @return stockAttr value 类型为:String
	 * 
	 */
	public String getStockAttr()
	{
		return stockAttr;
	}


	/**
	 * 设置商品的库存属性串
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStockAttr(String value)
	{
		if (value != null) {
				this.stockAttr = value;
		}else{
				this.stockAttr = new String();
		}
	}


	/**
	 * 获取商品的该库存对应的销售数量
	 * 
	 * 此字段的版本 >= 0
	 * @return soldCount value 类型为:int
	 * 
	 */
	public int getSoldCount()
	{
		return soldCount;
	}


	/**
	 * 设置商品的该库存对应的销售数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setSoldCount(int value)
	{
		this.soldCount = value;
	}


	/**
	 * 获取商品的库存数量
	 * 
	 * 此字段的版本 >= 0
	 * @return stockCount value 类型为:int
	 * 
	 */
	public int getStockCount()
	{
		return stockCount;
	}


	/**
	 * 设置商品的库存数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setStockCount(int value)
	{
		this.stockCount = value;
	}


	/**
	 * 获取商品1~6级彩钻价格
	 * 
	 * 此字段的版本 >= 0
	 * @return discountPrice value 类型为:Vector<Integer>
	 * 
	 */
	public Vector<Integer> getDiscountPrice()
	{
		return discountPrice;
	}


	/**
	 * 设置商品1~6级彩钻价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<Integer>
	 * 
	 */
	public void setDiscountPrice(Vector<Integer> value)
	{
		if (value != null) {
				this.discountPrice = value;
		}else{
				this.discountPrice = new Vector<Integer>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(MStock)
				length += ByteStream.getObjectSize(stockId);  //计算字段stockId的长度 size_of(String)
				length += 4;  //计算字段stockPrice的长度 size_of(int)
				length += ByteStream.getObjectSize(stockAttr);  //计算字段stockAttr的长度 size_of(String)
				length += 4;  //计算字段soldCount的长度 size_of(int)
				length += 4;  //计算字段stockCount的长度 size_of(int)
				length += ByteStream.getObjectSize(discountPrice);  //计算字段discountPrice的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
