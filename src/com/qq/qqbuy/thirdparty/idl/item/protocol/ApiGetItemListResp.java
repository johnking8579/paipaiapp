/*jadclipse*/// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.

package com.qq.qqbuy.thirdparty.idl.item.protocol;

import java.util.Vector;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

// Referenced classes of package com.paipai.c2c.api.item.ao.protocol:
//            ApiItem

public class ApiGetItemListResp implements IServiceObject {
	public long result;

	public ApiGetItemListResp() {
		apiItemList = new Vector();
		errmsg = new String();
		reserve = new String();
	}

	public int Serialize(ByteStream bs) throws Exception {
		bs.pushUInt(result);
		bs.pushObject(apiItemList);
		bs.pushString(errmsg);
		bs.pushString(reserve);
		return bs.getWrittenLength();
	}

	public int UnSerialize(ByteStream bs) throws Exception {
		result = bs.popUInt();
		apiItemList = bs.popVector(com.qq.qqbuy.thirdparty.idl.item.protocol.ApiItem.class);
		errmsg = bs.popString();
		reserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId() {
		return 1426491400L;
	}

	public Vector getApiItemList() {
		return apiItemList;
	}

	public void setApiItemList(Vector value) {
		if (value != null)
			apiItemList = value;
		else
			apiItemList = new Vector();
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String value) {
		if (value != null)
			errmsg = value;
		else
			errmsg = new String();
	}

	public String getReserve() {
		return reserve;
	}

	public void setReserve(String value) {
		if (value != null)
			reserve = value;
		else
			reserve = new String();
	}

	protected int getClassSize() {
		return getSize() - 4;
	}

	public int getSize() {
		int length = 4;
		try {
			length = 4;
			length += ByteStream.getObjectSize(apiItemList);
			length += ByteStream.getObjectSize(errmsg);
			length += ByteStream.getObjectSize(reserve);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return length;
	}

	private Vector apiItemList;
	private String errmsg;
	private String reserve;

	public long getResult() {
		return result;
	}

	public void setResult(long result) {
		this.result = result;
	}
}


/*
	DECOMPILATION REPORT

	Decompiled from: E:\workspace1\webapp_boss_mgmt\WebRoot\WEB-INF\lib\com.paipai.c2c.api-2.0.0.jar
	Total time: 279 ms
	Jad reported messages/errors:
	Exit status: 0
	Caught exceptions:
*/