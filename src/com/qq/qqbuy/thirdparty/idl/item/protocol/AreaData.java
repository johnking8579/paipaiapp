//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.portal.po.idl.MartPo.java

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *区域数据
 *
 *@date 2014-09-04 05:20:14
 *
 *@since version:0
*/
public class AreaData  implements ICanSerializeObject
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20140807;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 区域ID
	 *
	 * 版本 >= 0
	 */
	 private long dwAreaId;

	/**
	 * 版本 >= 0
	 */
	 private short dwAreaId_u;

	/**
	 * 商品池
	 *
	 * 版本 >= 0
	 */
	 private long dwPoolId;

	/**
	 * 版本 >= 0
	 */
	 private short dwPoolId_u;

	/**
	 * beginTime
	 *
	 * 版本 >= 0
	 */
	 private long dwBeginTime;

	/**
	 * 版本 >= 0
	 */
	 private short dwBeginTime_u;

	/**
	 * endTime
	 *
	 * 版本 >= 0
	 */
	 private long dwEndTime;

	/**
	 * 版本 >= 0
	 */
	 private short dwEndTime_u;

	/**
	 * 区域名称
	 *
	 * 版本 >= 0
	 */
	 private String strAreaName = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strAreaName_u;

	/**
	 * 商品列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<TabData> vecTabs = new Vector<TabData>();

	/**
	 * 版本 >= 0
	 */
	 private short vecTabs_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushUInt(dwAreaId);
		bs.pushUByte(dwAreaId_u);
		bs.pushUInt(dwPoolId);
		bs.pushUByte(dwPoolId_u);
		bs.pushUInt(dwBeginTime);
		bs.pushUByte(dwBeginTime_u);
		bs.pushUInt(dwEndTime);
		bs.pushUByte(dwEndTime_u);
		bs.pushString(strAreaName);
		bs.pushUByte(strAreaName_u);
		bs.pushObject(vecTabs);
		bs.pushUByte(vecTabs_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		dwAreaId = bs.popUInt();
		dwAreaId_u = bs.popUByte();
		dwPoolId = bs.popUInt();
		dwPoolId_u = bs.popUByte();
		dwBeginTime = bs.popUInt();
		dwBeginTime_u = bs.popUByte();
		dwEndTime = bs.popUInt();
		dwEndTime_u = bs.popUByte();
		strAreaName = bs.popString();
		strAreaName_u = bs.popUByte();
		vecTabs = (Vector<TabData>)bs.popVector(TabData.class);
		vecTabs_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取区域ID
	 * 
	 * 此字段的版本 >= 0
	 * @return dwAreaId value 类型为:long
	 * 
	 */
	public long getDwAreaId()
	{
		return dwAreaId;
	}


	/**
	 * 设置区域ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwAreaId(long value)
	{
		this.dwAreaId = value;
		this.dwAreaId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwAreaId_u value 类型为:short
	 * 
	 */
	public short getDwAreaId_u()
	{
		return dwAreaId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwAreaId_u(short value)
	{
		this.dwAreaId_u = value;
	}


	/**
	 * 获取商品池
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPoolId value 类型为:long
	 * 
	 */
	public long getDwPoolId()
	{
		return dwPoolId;
	}


	/**
	 * 设置商品池
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwPoolId(long value)
	{
		this.dwPoolId = value;
		this.dwPoolId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPoolId_u value 类型为:short
	 * 
	 */
	public short getDwPoolId_u()
	{
		return dwPoolId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwPoolId_u(short value)
	{
		this.dwPoolId_u = value;
	}


	/**
	 * 获取beginTime
	 * 
	 * 此字段的版本 >= 0
	 * @return dwBeginTime value 类型为:long
	 * 
	 */
	public long getDwBeginTime()
	{
		return dwBeginTime;
	}


	/**
	 * 设置beginTime
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwBeginTime(long value)
	{
		this.dwBeginTime = value;
		this.dwBeginTime_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwBeginTime_u value 类型为:short
	 * 
	 */
	public short getDwBeginTime_u()
	{
		return dwBeginTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwBeginTime_u(short value)
	{
		this.dwBeginTime_u = value;
	}


	/**
	 * 获取endTime
	 * 
	 * 此字段的版本 >= 0
	 * @return dwEndTime value 类型为:long
	 * 
	 */
	public long getDwEndTime()
	{
		return dwEndTime;
	}


	/**
	 * 设置endTime
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwEndTime(long value)
	{
		this.dwEndTime = value;
		this.dwEndTime_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwEndTime_u value 类型为:short
	 * 
	 */
	public short getDwEndTime_u()
	{
		return dwEndTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwEndTime_u(short value)
	{
		this.dwEndTime_u = value;
	}


	/**
	 * 获取区域名称
	 * 
	 * 此字段的版本 >= 0
	 * @return strAreaName value 类型为:String
	 * 
	 */
	public String getStrAreaName()
	{
		return strAreaName;
	}


	/**
	 * 设置区域名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrAreaName(String value)
	{
		this.strAreaName = value;
		this.strAreaName_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strAreaName_u value 类型为:short
	 * 
	 */
	public short getStrAreaName_u()
	{
		return strAreaName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrAreaName_u(short value)
	{
		this.strAreaName_u = value;
	}


	/**
	 * 获取商品列表
	 * 
	 * 此字段的版本 >= 0
	 * @return vecTabs value 类型为:Vector<TabData>
	 * 
	 */
	public Vector<TabData> getVecTabs()
	{
		return vecTabs;
	}


	/**
	 * 设置商品列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<TabData>
	 * 
	 */
	public void setVecTabs(Vector<TabData> value)
	{
		if (value != null) {
				this.vecTabs = value;
				this.vecTabs_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return vecTabs_u value 类型为:short
	 * 
	 */
	public short getVecTabs_u()
	{
		return vecTabs_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVecTabs_u(short value)
	{
		this.vecTabs_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(AreaData)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwAreaId的长度 size_of(uint32_t)
				length += 1;  //计算字段dwAreaId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwPoolId的长度 size_of(uint32_t)
				length += 1;  //计算字段dwPoolId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwBeginTime的长度 size_of(uint32_t)
				length += 1;  //计算字段dwBeginTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwEndTime的长度 size_of(uint32_t)
				length += 1;  //计算字段dwEndTime_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strAreaName);  //计算字段strAreaName的长度 size_of(String)
				length += 1;  //计算字段strAreaName_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(vecTabs);  //计算字段vecTabs的长度 size_of(Vector)
				length += 1;  //计算字段vecTabs_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
