 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.item.idl.ItemAo.java

package com.qq.qqbuy.thirdparty.idl.item.protocol.qgo;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2013-04-08 02:57:15
 *
 *@since version:0
*/
public class  GetItemReq implements IServiceObject
{
	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 来源,不能为空
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 商品编码,不能为空
	 *
	 * 版本 >= 0
	 */
	 private String itemCode = new String();

	/**
	 * 是否需要解析商品属性,默认为false
	 *
	 * 版本 >= 0
	 */
	 private boolean needParseAttr;

	/**
	 * 是否需要详情描述,默认为false
	 *
	 * 版本 >= 0
	 */
	 private boolean needDetail;

	/**
	 * 是否需要查询手拍商品信息,默认为false
	 *
	 * 版本 >= 0
	 */
	 private boolean isMobile;

	/**
	 * 请求保留字 
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(machineKey);
		bs.pushString(source);
		bs.pushString(itemCode);
		bs.pushBoolean(needParseAttr);
		bs.pushBoolean(needDetail);
		bs.pushBoolean(isMobile);
		bs.pushString(inReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		machineKey = bs.popString();
		source = bs.popString();
		itemCode = bs.popString();
		needParseAttr = bs.popBoolean();
		needDetail = bs.popBoolean();
		isMobile = bs.popBoolean();
		inReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80031805L;
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取来源,不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置来源,不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取商品编码,不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @return itemCode value 类型为:String
	 * 
	 */
	public String getItemCode()
	{
		return itemCode;
	}


	/**
	 * 设置商品编码,不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemCode(String value)
	{
		this.itemCode = value;
	}


	/**
	 * 获取是否需要解析商品属性,默认为false
	 * 
	 * 此字段的版本 >= 0
	 * @return needParseAttr value 类型为:boolean
	 * 
	 */
	public boolean getNeedParseAttr()
	{
		return needParseAttr;
	}


	/**
	 * 设置是否需要解析商品属性,默认为false
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:boolean
	 * 
	 */
	public void setNeedParseAttr(boolean value)
	{
		this.needParseAttr = value;
	}


	/**
	 * 获取是否需要详情描述,默认为false
	 * 
	 * 此字段的版本 >= 0
	 * @return needDetail value 类型为:boolean
	 * 
	 */
	public boolean getNeedDetail()
	{
		return needDetail;
	}


	/**
	 * 设置是否需要详情描述,默认为false
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:boolean
	 * 
	 */
	public void setNeedDetail(boolean value)
	{
		this.needDetail = value;
	}


	/**
	 * 获取是否需要查询手拍商品信息,默认为false
	 * 
	 * 此字段的版本 >= 0
	 * @return isMobile value 类型为:boolean
	 * 
	 */
	public boolean getIsMobile()
	{
		return isMobile;
	}


	/**
	 * 设置是否需要查询手拍商品信息,默认为false
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:boolean
	 * 
	 */
	public void setIsMobile(boolean value)
	{
		this.isMobile = value;
	}


	/**
	 * 获取请求保留字 
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置请求保留字 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetItemReq)
				length += ByteStream.getObjectSize(machineKey);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(itemCode);  //计算字段itemCode的长度 size_of(String)
				length += 1;  //计算字段needParseAttr的长度 size_of(boolean)
				length += 1;  //计算字段needDetail的长度 size_of(boolean)
				length += 1;  //计算字段isMobile的长度 size_of(boolean)
				length += ByteStream.getObjectSize(inReserve);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
