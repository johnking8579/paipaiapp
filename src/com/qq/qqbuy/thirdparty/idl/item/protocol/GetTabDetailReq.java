 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.portal.ao.idl.MartAo.java

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import com.paipai.lang.uint32_t;
import java.util.Vector;

/**
 *卖场快车商品详细信息接口请求
 *
 *@date 2014-09-04 05:10:45
 *
 *@since version:0
*/
public class GetTabDetailReq extends NetMessage
{
	/**
	 * 来源，传客户端IP或者__FILE__，必须
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 传用户的VisitKey，必须
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 场景ID，必须 请向接口提供方获取
	 *
	 * 版本 >= 0
	 */
	 private long sceneId;

	/**
	 * 鉴权Token
	 *
	 * 版本 >= 0
	 */
	 private String token = new String();

	/**
	 * tabid list
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint32_t> tabList = new Vector<uint32_t>();

	/**
	 * ext 扩展信息
	 *
	 * 版本 >= 0
	 */
	 private ExtObj extIn = new ExtObj();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(source);
		bs.pushString(machineKey);
		bs.pushUInt(sceneId);
		bs.pushString(token);
		bs.pushObject(tabList);
		bs.pushObject(extIn);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		source = bs.popString();
		machineKey = bs.popString();
		sceneId = bs.popUInt();
		token = bs.popString();
		tabList = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		extIn = (ExtObj)bs.popObject(ExtObj.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x87481002L;
	}


	/**
	 * 获取来源，传客户端IP或者__FILE__，必须
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置来源，传客户端IP或者__FILE__，必须
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取传用户的VisitKey，必须
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置传用户的VisitKey，必须
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取场景ID，必须 请向接口提供方获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sceneId value 类型为:long
	 * 
	 */
	public long getSceneId()
	{
		return sceneId;
	}


	/**
	 * 设置场景ID，必须 请向接口提供方获取
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSceneId(long value)
	{
		this.sceneId = value;
	}


	/**
	 * 获取鉴权Token
	 * 
	 * 此字段的版本 >= 0
	 * @return token value 类型为:String
	 * 
	 */
	public String getToken()
	{
		return token;
	}


	/**
	 * 设置鉴权Token
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setToken(String value)
	{
		this.token = value;
	}


	/**
	 * 获取tabid list
	 * 
	 * 此字段的版本 >= 0
	 * @return tabList value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getTabList()
	{
		return tabList;
	}


	/**
	 * 设置tabid list
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setTabList(Vector<uint32_t> value)
	{
		if (value != null) {
				this.tabList = value;
		}else{
				this.tabList = new Vector<uint32_t>();
		}
	}


	/**
	 * 获取ext 扩展信息
	 * 
	 * 此字段的版本 >= 0
	 * @return extIn value 类型为:ExtObj
	 * 
	 */
	public ExtObj getExtIn()
	{
		return extIn;
	}


	/**
	 * 设置ext 扩展信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ExtObj
	 * 
	 */
	public void setExtIn(ExtObj value)
	{
		if (value != null) {
				this.extIn = value;
		}else{
				this.extIn = new ExtObj();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(getTabDetailReq)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(machineKey);  //计算字段machineKey的长度 size_of(String)
				length += 4;  //计算字段sceneId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(token);  //计算字段token的长度 size_of(String)
				length += ByteStream.getObjectSize(tabList);  //计算字段tabList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(extIn);  //计算字段extIn的长度 size_of(ExtObj)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
