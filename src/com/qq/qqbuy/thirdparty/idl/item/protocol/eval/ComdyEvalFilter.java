//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.EvalIdl.java

package com.qq.qqbuy.thirdparty.idl.item.protocol.eval;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *商品评价过滤器
 *
 *@date 2012-12-17 10:47:50
 *
 *@since version:0
*/
public class ComdyEvalFilter  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 卖家QQ号-
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * 版本 >= 0
	 */
	 private short SellerUin_u;

	/**
	 * 商品id--
	 *
	 * 版本 >= 0
	 */
	 private String ComdyId = new String();

	/**
	 * 版本 >= 0
	 */
	 private short ComdyId_u;

	/**
	 * 页码
	 *
	 * 版本 >= 0
	 */
	 private long PageIndex;

	/**
	 * 版本 >= 0
	 */
	 private short PageIndex_u;

	/**
	 * 每页数量
	 *
	 * 版本 >= 0
	 */
	 private long PageSize;

	/**
	 * 版本 >= 0
	 */
	 private short PageSize_u;

	/**
	 * 是否历史库
	 *
	 * 版本 >= 0
	 */
	 private long IsHistory;

	/**
	 * 版本 >= 0
	 */
	 private short IsHistory_u;

	/**
	 * 是否需要回复
	 *
	 * 版本 >= 0
	 */
	 private long NeedReply;

	/**
	 * 版本 >= 0
	 */
	 private short NeedReply_u;

	/**
	 * 评价等级
	 *
	 * 版本 >= 0
	 */
	 private long Level;

	/**
	 * 版本 >= 0
	 */
	 private short Level_u;

	/**
	 * 排序类型
	 *
	 * 版本 >= 0
	 */
	 private long OrderType;

	/**
	 * 版本 >= 0
	 */
	 private short OrderType_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(SellerUin);
		bs.pushUByte(SellerUin_u);
		bs.pushString(ComdyId);
		bs.pushUByte(ComdyId_u);
		bs.pushUInt(PageIndex);
		bs.pushUByte(PageIndex_u);
		bs.pushUInt(PageSize);
		bs.pushUByte(PageSize_u);
		bs.pushUInt(IsHistory);
		bs.pushUByte(IsHistory_u);
		bs.pushUInt(NeedReply);
		bs.pushUByte(NeedReply_u);
		bs.pushUInt(Level);
		bs.pushUByte(Level_u);
		bs.pushUInt(OrderType);
		bs.pushUByte(OrderType_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		SellerUin = bs.popUInt();
		SellerUin_u = bs.popUByte();
		ComdyId = bs.popString();
		ComdyId_u = bs.popUByte();
		PageIndex = bs.popUInt();
		PageIndex_u = bs.popUByte();
		PageSize = bs.popUInt();
		PageSize_u = bs.popUByte();
		IsHistory = bs.popUInt();
		IsHistory_u = bs.popUByte();
		NeedReply = bs.popUInt();
		NeedReply_u = bs.popUByte();
		Level = bs.popUInt();
		Level_u = bs.popUByte();
		OrderType = bs.popUInt();
		OrderType_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取卖家QQ号-
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置卖家QQ号-
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
		this.SellerUin_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin_u value 类型为:short
	 * 
	 */
	public short getSellerUin_u()
	{
		return SellerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerUin_u(short value)
	{
		this.SellerUin_u = value;
	}


	/**
	 * 获取商品id--
	 * 
	 * 此字段的版本 >= 0
	 * @return ComdyId value 类型为:String
	 * 
	 */
	public String getComdyId()
	{
		return ComdyId;
	}


	/**
	 * 设置商品id--
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setComdyId(String value)
	{
		this.ComdyId = value;
		this.ComdyId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ComdyId_u value 类型为:short
	 * 
	 */
	public short getComdyId_u()
	{
		return ComdyId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setComdyId_u(short value)
	{
		this.ComdyId_u = value;
	}


	/**
	 * 获取页码
	 * 
	 * 此字段的版本 >= 0
	 * @return PageIndex value 类型为:long
	 * 
	 */
	public long getPageIndex()
	{
		return PageIndex;
	}


	/**
	 * 设置页码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPageIndex(long value)
	{
		this.PageIndex = value;
		this.PageIndex_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PageIndex_u value 类型为:short
	 * 
	 */
	public short getPageIndex_u()
	{
		return PageIndex_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPageIndex_u(short value)
	{
		this.PageIndex_u = value;
	}


	/**
	 * 获取每页数量
	 * 
	 * 此字段的版本 >= 0
	 * @return PageSize value 类型为:long
	 * 
	 */
	public long getPageSize()
	{
		return PageSize;
	}


	/**
	 * 设置每页数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPageSize(long value)
	{
		this.PageSize = value;
		this.PageSize_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PageSize_u value 类型为:short
	 * 
	 */
	public short getPageSize_u()
	{
		return PageSize_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPageSize_u(short value)
	{
		this.PageSize_u = value;
	}


	/**
	 * 获取是否历史库
	 * 
	 * 此字段的版本 >= 0
	 * @return IsHistory value 类型为:long
	 * 
	 */
	public long getIsHistory()
	{
		return IsHistory;
	}


	/**
	 * 设置是否历史库
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setIsHistory(long value)
	{
		this.IsHistory = value;
		this.IsHistory_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return IsHistory_u value 类型为:short
	 * 
	 */
	public short getIsHistory_u()
	{
		return IsHistory_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIsHistory_u(short value)
	{
		this.IsHistory_u = value;
	}


	/**
	 * 获取是否需要回复
	 * 
	 * 此字段的版本 >= 0
	 * @return NeedReply value 类型为:long
	 * 
	 */
	public long getNeedReply()
	{
		return NeedReply;
	}


	/**
	 * 设置是否需要回复
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNeedReply(long value)
	{
		this.NeedReply = value;
		this.NeedReply_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return NeedReply_u value 类型为:short
	 * 
	 */
	public short getNeedReply_u()
	{
		return NeedReply_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNeedReply_u(short value)
	{
		this.NeedReply_u = value;
	}


	/**
	 * 获取评价等级
	 * 
	 * 此字段的版本 >= 0
	 * @return Level value 类型为:long
	 * 
	 */
	public long getLevel()
	{
		return Level;
	}


	/**
	 * 设置评价等级
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLevel(long value)
	{
		this.Level = value;
		this.Level_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Level_u value 类型为:short
	 * 
	 */
	public short getLevel_u()
	{
		return Level_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLevel_u(short value)
	{
		this.Level_u = value;
	}


	/**
	 * 获取排序类型
	 * 
	 * 此字段的版本 >= 0
	 * @return OrderType value 类型为:long
	 * 
	 */
	public long getOrderType()
	{
		return OrderType;
	}


	/**
	 * 设置排序类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOrderType(long value)
	{
		this.OrderType = value;
		this.OrderType_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return OrderType_u value 类型为:short
	 * 
	 */
	public short getOrderType_u()
	{
		return OrderType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOrderType_u(short value)
	{
		this.OrderType_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ComdyEvalFilter)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ComdyId);  //计算字段ComdyId的长度 size_of(String)
				length += 1;  //计算字段ComdyId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PageIndex的长度 size_of(uint32_t)
				length += 1;  //计算字段PageIndex_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PageSize的长度 size_of(uint32_t)
				length += 1;  //计算字段PageSize_u的长度 size_of(uint8_t)
				length += 4;  //计算字段IsHistory的长度 size_of(uint32_t)
				length += 1;  //计算字段IsHistory_u的长度 size_of(uint8_t)
				length += 4;  //计算字段NeedReply的长度 size_of(uint32_t)
				length += 1;  //计算字段NeedReply_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Level的长度 size_of(uint32_t)
				length += 1;  //计算字段Level_u的长度 size_of(uint8_t)
				length += 4;  //计算字段OrderType的长度 size_of(uint32_t)
				length += 1;  //计算字段OrderType_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
