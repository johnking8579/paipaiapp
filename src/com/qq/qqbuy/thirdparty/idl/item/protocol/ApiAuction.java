 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *商品拍卖信息
 *
 *@date 2012-09-02 10:38::24
 *
 *@since version:0
*/
public class ApiAuction  implements ICanSerializeObject
{
	/**
	 * 商品ID
	 *
	 * 版本 >= 0
	 */
	 private String itemId = new String();

	/**
	 * 商品库存ID
	 *
	 * 版本 >= 0
	 */
	 private String stockId = new String();

	/**
	 * 卖家QQ
	 *
	 * 版本 >= 0
	 */
	 private long sellerUin;

	/**
	 * 类型
	 *
	 * 版本 >= 0
	 */
	 private long type;

	/**
	 * 加价幅度
	 *
	 * 版本 >= 0
	 */
	 private long priceAddStep;

	/**
	 * 加价方式
	 *
	 * 版本 >= 0
	 */
	 private long priceAddType;

	/**
	 * 抵押金生效金额
	 *
	 * 版本 >= 0
	 */
	 private long plegeStart;

	/**
	 * 抵押金
	 *
	 * 版本 >= 0
	 */
	 private long plegeMoney;

	/**
	 * 版本 >= 0
	 */
	 private short itemId_u;

	/**
	 * 版本 >= 0
	 */
	 private short sellerUin_u;

	/**
	 * 版本 >= 0
	 */
	 private short stockId_u;

	/**
	 * 版本 >= 0
	 */
	 private short type_u;

	/**
	 * 版本 >= 0
	 */
	 private short priceAddStep_u;

	/**
	 * 版本 >= 0
	 */
	 private short priceAddType_u;

	/**
	 * 版本 >= 0
	 */
	 private short plegeStart_u;

	/**
	 * 版本 >= 0
	 */
	 private short plegeMoney_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushString(itemId);
		bs.pushString(stockId);
		bs.pushUInt(sellerUin);
		bs.pushUInt(type);
		bs.pushUInt(priceAddStep);
		bs.pushUInt(priceAddType);
		bs.pushUInt(plegeStart);
		bs.pushUInt(plegeMoney);
		bs.pushUByte(itemId_u);
		bs.pushUByte(sellerUin_u);
		bs.pushUByte(stockId_u);
		bs.pushUByte(type_u);
		bs.pushUByte(priceAddStep_u);
		bs.pushUByte(priceAddType_u);
		bs.pushUByte(plegeStart_u);
		bs.pushUByte(plegeMoney_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		itemId = bs.popString();
		stockId = bs.popString();
		sellerUin = bs.popUInt();
		type = bs.popUInt();
		priceAddStep = bs.popUInt();
		priceAddType = bs.popUInt();
		plegeStart = bs.popUInt();
		plegeMoney = bs.popUInt();
		itemId_u = bs.popUByte();
		sellerUin_u = bs.popUByte();
		stockId_u = bs.popUByte();
		type_u = bs.popUByte();
		priceAddStep_u = bs.popUByte();
		priceAddType_u = bs.popUByte();
		plegeStart_u = bs.popUByte();
		plegeMoney_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return itemId value 类型为:String
	 * 
	 */
	public String getItemId()
	{
		return itemId;
	}


	/**
	 * 设置商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemId(String value)
	{
		if (value != null) {
				this.itemId = value;
				this.itemId_u = 1;
		}
	}


	/**
	 * 获取商品库存ID
	 * 
	 * 此字段的版本 >= 0
	 * @return stockId value 类型为:String
	 * 
	 */
	public String getStockId()
	{
		return stockId;
	}


	/**
	 * 设置商品库存ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStockId(String value)
	{
		if (value != null) {
				this.stockId = value;
				this.stockId_u = 1;
		}
	}


	/**
	 * 获取卖家QQ
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return sellerUin;
	}


	/**
	 * 设置卖家QQ
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.sellerUin = value;
		this.sellerUin_u = 1;
	}


	/**
	 * 获取类型
	 * 
	 * 此字段的版本 >= 0
	 * @return type value 类型为:long
	 * 
	 */
	public long getType()
	{
		return type;
	}


	/**
	 * 设置类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setType(long value)
	{
		this.type = value;
		this.type_u = 1;
	}


	/**
	 * 获取加价幅度
	 * 
	 * 此字段的版本 >= 0
	 * @return priceAddStep value 类型为:long
	 * 
	 */
	public long getPriceAddStep()
	{
		return priceAddStep;
	}


	/**
	 * 设置加价幅度
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPriceAddStep(long value)
	{
		this.priceAddStep = value;
		this.priceAddStep_u = 1;
	}


	/**
	 * 获取加价方式
	 * 
	 * 此字段的版本 >= 0
	 * @return priceAddType value 类型为:long
	 * 
	 */
	public long getPriceAddType()
	{
		return priceAddType;
	}


	/**
	 * 设置加价方式
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPriceAddType(long value)
	{
		this.priceAddType = value;
		this.priceAddType_u = 1;
	}


	/**
	 * 获取抵押金生效金额
	 * 
	 * 此字段的版本 >= 0
	 * @return plegeStart value 类型为:long
	 * 
	 */
	public long getPlegeStart()
	{
		return plegeStart;
	}


	/**
	 * 设置抵押金生效金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPlegeStart(long value)
	{
		this.plegeStart = value;
		this.plegeStart_u = 1;
	}


	/**
	 * 获取抵押金
	 * 
	 * 此字段的版本 >= 0
	 * @return plegeMoney value 类型为:long
	 * 
	 */
	public long getPlegeMoney()
	{
		return plegeMoney;
	}


	/**
	 * 设置抵押金
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPlegeMoney(long value)
	{
		this.plegeMoney = value;
		this.plegeMoney_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return itemId_u value 类型为:short
	 * 
	 */
	public short getItemId_u()
	{
		return itemId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemId_u(short value)
	{
		this.itemId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerUin_u value 类型为:short
	 * 
	 */
	public short getSellerUin_u()
	{
		return sellerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerUin_u(short value)
	{
		this.sellerUin_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return stockId_u value 类型为:short
	 * 
	 */
	public short getStockId_u()
	{
		return stockId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStockId_u(short value)
	{
		this.stockId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return type_u value 类型为:short
	 * 
	 */
	public short getType_u()
	{
		return type_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setType_u(short value)
	{
		this.type_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return priceAddStep_u value 类型为:short
	 * 
	 */
	public short getPriceAddStep_u()
	{
		return priceAddStep_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPriceAddStep_u(short value)
	{
		this.priceAddStep_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return priceAddType_u value 类型为:short
	 * 
	 */
	public short getPriceAddType_u()
	{
		return priceAddType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPriceAddType_u(short value)
	{
		this.priceAddType_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return plegeStart_u value 类型为:short
	 * 
	 */
	public short getPlegeStart_u()
	{
		return plegeStart_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPlegeStart_u(short value)
	{
		this.plegeStart_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return plegeMoney_u value 类型为:short
	 * 
	 */
	public short getPlegeMoney_u()
	{
		return plegeMoney_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPlegeMoney_u(short value)
	{
		this.plegeMoney_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ApiAuction)
				length += ByteStream.getObjectSize(itemId);  //计算字段itemId的长度 size_of(String)
				length += ByteStream.getObjectSize(stockId);  //计算字段stockId的长度 size_of(String)
				length += 4;  //计算字段sellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段type的长度 size_of(uint32_t)
				length += 4;  //计算字段priceAddStep的长度 size_of(uint32_t)
				length += 4;  //计算字段priceAddType的长度 size_of(uint32_t)
				length += 4;  //计算字段plegeStart的长度 size_of(uint32_t)
				length += 4;  //计算字段plegeMoney的长度 size_of(uint32_t)
				length += 1;  //计算字段itemId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段sellerUin_u的长度 size_of(uint8_t)
				length += 1;  //计算字段stockId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段type_u的长度 size_of(uint8_t)
				length += 1;  //计算字段priceAddStep_u的长度 size_of(uint8_t)
				length += 1;  //计算字段priceAddType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段plegeStart_u的长度 size_of(uint8_t)
				length += 1;  //计算字段plegeMoney_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
