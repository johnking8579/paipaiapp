//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.item.po.idl.ItemPoV2.java

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *商品操作时间
 *
 *@date 2013-02-27 05:06:02
 *
 *@since version:0
*/
public class ItemTime  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 0;

	/**
	 * 商品第一次发布时间
	 *
	 * 版本 >= 0
	 */
	 private long addTime;

	/**
	 * 商品编辑时间
	 *
	 * 版本 >= 0
	 */
	 private long editTime;

	/**
	 * 商品最后上架时间，此时间有可能大于当前时间，表明这个商品是定时上架商品
	 *
	 * 版本 >= 0
	 */
	 private long upTime;

	/**
	 * 商品最后下架时间
	 *
	 * 版本 >= 0
	 */
	 private long downloadTime;

	/**
	 * 最后付款时间
	 *
	 * 版本 >= 0
	 */
	 private long lastPayTime;

	/**
	 * 最后下单时间
	 *
	 * 版本 >= 0
	 */
	 private long lastBuyTime;

	/**
	 * 商品的有效时间，指商品在架状态的有效时间
	 *
	 * 版本 >= 0
	 */
	 private long validTime;

	/**
	 * 最后变动时间，上架，下架，编辑，下单，付款都会更新这个时间
	 *
	 * 版本 >= 0
	 */
	 private long lastModifiedTime;

	/**
	 * strReserved
	 *
	 * 版本 >= 0
	 */
	 private String strReserved1 = new String();

	/**
	 * strReserved
	 *
	 * 版本 >= 0
	 */
	 private String strReserved2 = new String();

	/**
	 * strReserved
	 *
	 * 版本 >= 0
	 */
	 private String strReserved3 = new String();

	/**
	 * 商品销量清除时间
	 *
	 * 版本 >= 0
	 */
	 private long resetTime;

	/**
	 * dwReserved
	 *
	 * 版本 >= 0
	 */
	 private long dwReserved2;

	/**
	 * dwReserved
	 *
	 * 版本 >= 0
	 */
	 private long dwReserved3;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUInt(addTime);
		bs.pushUInt(editTime);
		bs.pushUInt(upTime);
		bs.pushUInt(downloadTime);
		bs.pushUInt(lastPayTime);
		bs.pushUInt(lastBuyTime);
		bs.pushUInt(validTime);
		bs.pushUInt(lastModifiedTime);
		bs.pushString(strReserved1);
		bs.pushString(strReserved2);
		bs.pushString(strReserved3);
		bs.pushUInt(resetTime);
		bs.pushUInt(dwReserved2);
		bs.pushUInt(dwReserved3);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		addTime = bs.popUInt();
		editTime = bs.popUInt();
		upTime = bs.popUInt();
		downloadTime = bs.popUInt();
		lastPayTime = bs.popUInt();
		lastBuyTime = bs.popUInt();
		validTime = bs.popUInt();
		lastModifiedTime = bs.popUInt();
		strReserved1 = bs.popString();
		strReserved2 = bs.popString();
		strReserved3 = bs.popString();
		resetTime = bs.popUInt();
		dwReserved2 = bs.popUInt();
		dwReserved3 = bs.popUInt();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取商品第一次发布时间
	 * 
	 * 此字段的版本 >= 0
	 * @return addTime value 类型为:long
	 * 
	 */
	public long getAddTime()
	{
		return addTime;
	}


	/**
	 * 设置商品第一次发布时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddTime(long value)
	{
		this.addTime = value;
	}


	/**
	 * 获取商品编辑时间
	 * 
	 * 此字段的版本 >= 0
	 * @return editTime value 类型为:long
	 * 
	 */
	public long getEditTime()
	{
		return editTime;
	}


	/**
	 * 设置商品编辑时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEditTime(long value)
	{
		this.editTime = value;
	}


	/**
	 * 获取商品最后上架时间，此时间有可能大于当前时间，表明这个商品是定时上架商品
	 * 
	 * 此字段的版本 >= 0
	 * @return upTime value 类型为:long
	 * 
	 */
	public long getUpTime()
	{
		return upTime;
	}


	/**
	 * 设置商品最后上架时间，此时间有可能大于当前时间，表明这个商品是定时上架商品
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUpTime(long value)
	{
		this.upTime = value;
	}


	/**
	 * 获取商品最后下架时间
	 * 
	 * 此字段的版本 >= 0
	 * @return downloadTime value 类型为:long
	 * 
	 */
	public long getDownloadTime()
	{
		return downloadTime;
	}


	/**
	 * 设置商品最后下架时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDownloadTime(long value)
	{
		this.downloadTime = value;
	}


	/**
	 * 获取最后付款时间
	 * 
	 * 此字段的版本 >= 0
	 * @return lastPayTime value 类型为:long
	 * 
	 */
	public long getLastPayTime()
	{
		return lastPayTime;
	}


	/**
	 * 设置最后付款时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastPayTime(long value)
	{
		this.lastPayTime = value;
	}


	/**
	 * 获取最后下单时间
	 * 
	 * 此字段的版本 >= 0
	 * @return lastBuyTime value 类型为:long
	 * 
	 */
	public long getLastBuyTime()
	{
		return lastBuyTime;
	}


	/**
	 * 设置最后下单时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastBuyTime(long value)
	{
		this.lastBuyTime = value;
	}


	/**
	 * 获取商品的有效时间，指商品在架状态的有效时间
	 * 
	 * 此字段的版本 >= 0
	 * @return validTime value 类型为:long
	 * 
	 */
	public long getValidTime()
	{
		return validTime;
	}


	/**
	 * 设置商品的有效时间，指商品在架状态的有效时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setValidTime(long value)
	{
		this.validTime = value;
	}


	/**
	 * 获取最后变动时间，上架，下架，编辑，下单，付款都会更新这个时间
	 * 
	 * 此字段的版本 >= 0
	 * @return lastModifiedTime value 类型为:long
	 * 
	 */
	public long getLastModifiedTime()
	{
		return lastModifiedTime;
	}


	/**
	 * 设置最后变动时间，上架，下架，编辑，下单，付款都会更新这个时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastModifiedTime(long value)
	{
		this.lastModifiedTime = value;
	}


	/**
	 * 获取strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return strReserved1 value 类型为:String
	 * 
	 */
	public String getStrReserved1()
	{
		return strReserved1;
	}


	/**
	 * 设置strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrReserved1(String value)
	{
		this.strReserved1 = value;
	}


	/**
	 * 获取strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return strReserved2 value 类型为:String
	 * 
	 */
	public String getStrReserved2()
	{
		return strReserved2;
	}


	/**
	 * 设置strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrReserved2(String value)
	{
		this.strReserved2 = value;
	}


	/**
	 * 获取strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return strReserved3 value 类型为:String
	 * 
	 */
	public String getStrReserved3()
	{
		return strReserved3;
	}


	/**
	 * 设置strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrReserved3(String value)
	{
		this.strReserved3 = value;
	}


	/**
	 * 获取商品销量清除时间
	 * 
	 * 此字段的版本 >= 0
	 * @return resetTime value 类型为:long
	 * 
	 */
	public long getResetTime()
	{
		return resetTime;
	}


	/**
	 * 设置商品销量清除时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setResetTime(long value)
	{
		this.resetTime = value;
	}


	/**
	 * 获取dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return dwReserved2 value 类型为:long
	 * 
	 */
	public long getDwReserved2()
	{
		return dwReserved2;
	}


	/**
	 * 设置dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwReserved2(long value)
	{
		this.dwReserved2 = value;
	}


	/**
	 * 获取dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return dwReserved3 value 类型为:long
	 * 
	 */
	public long getDwReserved3()
	{
		return dwReserved3;
	}


	/**
	 * 设置dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwReserved3(long value)
	{
		this.dwReserved3 = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemTime)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段addTime的长度 size_of(uint32_t)
				length += 4;  //计算字段editTime的长度 size_of(uint32_t)
				length += 4;  //计算字段upTime的长度 size_of(uint32_t)
				length += 4;  //计算字段downloadTime的长度 size_of(uint32_t)
				length += 4;  //计算字段lastPayTime的长度 size_of(uint32_t)
				length += 4;  //计算字段lastBuyTime的长度 size_of(uint32_t)
				length += 4;  //计算字段validTime的长度 size_of(uint32_t)
				length += 4;  //计算字段lastModifiedTime的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(strReserved1);  //计算字段strReserved1的长度 size_of(String)
				length += ByteStream.getObjectSize(strReserved2);  //计算字段strReserved2的长度 size_of(String)
				length += ByteStream.getObjectSize(strReserved3);  //计算字段strReserved3的长度 size_of(String)
				length += 4;  //计算字段resetTime的长度 size_of(uint32_t)
				length += 4;  //计算字段dwReserved2的长度 size_of(uint32_t)
				length += 4;  //计算字段dwReserved3的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
