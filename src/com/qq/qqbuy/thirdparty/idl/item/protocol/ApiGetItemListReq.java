/*jadclipse*/// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.

package com.qq.qqbuy.thirdparty.idl.item.protocol;

import java.util.Vector;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

public class ApiGetItemListReq implements IServiceObject {

	public ApiGetItemListReq() {
		machineKey = new String();
		source = new String();
		itemIdList = new Vector();
		inReserve = new String();
	}

	public int Serialize(ByteStream bs) throws Exception {
		bs.pushString(machineKey);
		bs.pushString(source);
		bs.pushUInt(scene);
		bs.pushObject(itemIdList);
		bs.pushString(inReserve);
		return bs.getWrittenLength();
	}

	public int UnSerialize(ByteStream bs) throws Exception {
		machineKey = bs.popString();
		source = bs.popString();
		scene = bs.popUInt();
		itemIdList = bs.popVector(String.class);
		inReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId() {
		return 1426462728L;
	}

	public String getMachineKey() {
		return machineKey;
	}

	public void setMachineKey(String value) {
		if (value != null)
			machineKey = value;
		else
			machineKey = new String();
	}

	public String getSource() {
		return source;
	}

	public void setSource(String value) {
		if (value != null)
			source = value;
		else
			source = new String();
	}

	public long getScene() {
		return scene;
	}

	public void setScene(long value) {
		scene = value;
	}

	public Vector getItemIdList() {
		return itemIdList;
	}

	public void setItemIdList(Vector value) {
		if (value != null)
			itemIdList = value;
		else
			itemIdList = new Vector();
	}

	public String getInReserve() {
		return inReserve;
	}

	public void setInReserve(String value) {
		if (value != null)
			inReserve = value;
		else
			inReserve = new String();
	}

	protected int getClassSize() {
		return getSize() - 4;
	}

	public int getSize() {
		int length = 0;
		try {
			length = 0;
			length += ByteStream.getObjectSize(machineKey);
			length += ByteStream.getObjectSize(source);
			length = (length += 4) + ByteStream.getObjectSize(itemIdList);
			length += ByteStream.getObjectSize(inReserve);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return length;
	}

	private String machineKey;
	private String source;
	private long scene;
	private Vector itemIdList;
	private String inReserve;
	
}


/*
	DECOMPILATION REPORT

	Decompiled from: E:\workspace1\webapp_boss_mgmt\WebRoot\WEB-INF\lib\com.paipai.c2c.api-2.0.0.jar
	Total time: 22 ms
	Jad reported messages/errors:
	Exit status: 0
	Caught exceptions:
*/