//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.EvalIdl.java

package com.qq.qqbuy.thirdparty.idl.item.protocol.eval;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *做评信息结构
 *
 *@date 2012-12-17 10:47:48
 *
 *@since version:0
*/
public class MakeEvalRequest  implements ICanSerializeObject
{
	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 子单id
	 *
	 * 版本 >= 0
	 */
	 private String TradeId = new String();

	/**
	 * 做评角色 1,2: 买,卖
	 *
	 * 版本 >= 0
	 */
	 private long Role;

	/**
	 * 评价等级,1,2,3：bad,normal,good
	 *
	 * 版本 >= 0
	 */
	 private long Level;

	/**
	 * 评价内容
	 *
	 * 版本 >= 0
	 */
	 private String Content = new String();

	/**
	 * dsr1 买家侧做评时必填
	 *
	 * 版本 >= 0
	 */
	 private long Dsr1;

	/**
	 * dsr2 买家侧做评时必填
	 *
	 * 版本 >= 0
	 */
	 private long Dsr2;

	/**
	 * dsr3 买家侧做评时必填
	 *
	 * 版本 >= 0
	 */
	 private long Dsr3;

	/**
	 * 保留输入字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveIn = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushString(TradeId);
		bs.pushUInt(Role);
		bs.pushUInt(Level);
		bs.pushString(Content);
		bs.pushUInt(Dsr1);
		bs.pushUInt(Dsr2);
		bs.pushUInt(Dsr3);
		bs.pushString(ReserveIn);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		TradeId = bs.popString();
		Role = bs.popUInt();
		Level = bs.popUInt();
		Content = bs.popString();
		Dsr1 = bs.popUInt();
		Dsr2 = bs.popUInt();
		Dsr3 = bs.popUInt();
		ReserveIn = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
	}


	/**
	 * 获取子单id
	 * 
	 * 此字段的版本 >= 0
	 * @return TradeId value 类型为:String
	 * 
	 */
	public String getTradeId()
	{
		return TradeId;
	}


	/**
	 * 设置子单id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setTradeId(String value)
	{
		this.TradeId = value;
	}


	/**
	 * 获取做评角色 1,2: 买,卖
	 * 
	 * 此字段的版本 >= 0
	 * @return Role value 类型为:long
	 * 
	 */
	public long getRole()
	{
		return Role;
	}


	/**
	 * 设置做评角色 1,2: 买,卖
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRole(long value)
	{
		this.Role = value;
	}


	/**
	 * 获取评价等级,1,2,3：bad,normal,good
	 * 
	 * 此字段的版本 >= 0
	 * @return Level value 类型为:long
	 * 
	 */
	public long getLevel()
	{
		return Level;
	}


	/**
	 * 设置评价等级,1,2,3：bad,normal,good
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLevel(long value)
	{
		this.Level = value;
	}


	/**
	 * 获取评价内容
	 * 
	 * 此字段的版本 >= 0
	 * @return Content value 类型为:String
	 * 
	 */
	public String getContent()
	{
		return Content;
	}


	/**
	 * 设置评价内容
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setContent(String value)
	{
		this.Content = value;
	}


	/**
	 * 获取dsr1 买家侧做评时必填
	 * 
	 * 此字段的版本 >= 0
	 * @return Dsr1 value 类型为:long
	 * 
	 */
	public long getDsr1()
	{
		return Dsr1;
	}


	/**
	 * 设置dsr1 买家侧做评时必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDsr1(long value)
	{
		this.Dsr1 = value;
	}


	/**
	 * 获取dsr2 买家侧做评时必填
	 * 
	 * 此字段的版本 >= 0
	 * @return Dsr2 value 类型为:long
	 * 
	 */
	public long getDsr2()
	{
		return Dsr2;
	}


	/**
	 * 设置dsr2 买家侧做评时必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDsr2(long value)
	{
		this.Dsr2 = value;
	}


	/**
	 * 获取dsr3 买家侧做评时必填
	 * 
	 * 此字段的版本 >= 0
	 * @return Dsr3 value 类型为:long
	 * 
	 */
	public long getDsr3()
	{
		return Dsr3;
	}


	/**
	 * 设置dsr3 买家侧做评时必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDsr3(long value)
	{
		this.Dsr3 = value;
	}


	/**
	 * 获取保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveIn value 类型为:String
	 * 
	 */
	public String getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveIn(String value)
	{
		this.ReserveIn = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(MakeEvalRequest)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(TradeId);  //计算字段TradeId的长度 size_of(String)
				length += 4;  //计算字段Role的长度 size_of(uint32_t)
				length += 4;  //计算字段Level的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(Content);  //计算字段Content的长度 size_of(String)
				length += 4;  //计算字段Dsr1的长度 size_of(uint32_t)
				length += 4;  //计算字段Dsr2的长度 size_of(uint32_t)
				length += 4;  //计算字段Dsr3的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ReserveIn);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
