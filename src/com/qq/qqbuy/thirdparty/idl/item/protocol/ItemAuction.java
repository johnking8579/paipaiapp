//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.item.po.idl.ItemPoV2.java

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *商品拍卖信息
 *
 *@date 2013-02-27 05:06:02
 *
 *@since version:0
*/
public class ItemAuction  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 0;

	/**
	 * 加价幅度，每次出价时的加价幅度
	 *
	 * 版本 >= 0
	 */
	 private long priceAddStep;

	/**
	 * 加价方式，【用户自定义加价/系统自定义加价】两种
	 *
	 * 版本 >= 0
	 */
	 private long priceAddType;

	/**
	 * 抵押金生效金额，一些商品一开始起拍的时候不需要交抵押金，但是当商品的价格被拍到一定的金额时，就必须缴纳抵押金才能继续拍卖了
	 *
	 * 版本 >= 0
	 */
	 private long plegeStart;

	/**
	 * 抵押金，需要缴纳抵押金才能进行拍卖的时候，缴纳的抵押金金额
	 *
	 * 版本 >= 0
	 */
	 private long plegeMoney;

	/**
	 * 出价次数，总的商品出价次数
	 *
	 * 版本 >= 0
	 */
	 private long fixupCount;

	/**
	 * 当前最新价格，商品最新的出价价格
	 *
	 * 版本 >= 0
	 */
	 private long currAuctionPrice;

	/**
	 * strReserved
	 *
	 * 版本 >= 0
	 */
	 private String strReserved1 = new String();

	/**
	 * strReserved
	 *
	 * 版本 >= 0
	 */
	 private String strReserved2 = new String();

	/**
	 * strReserved
	 *
	 * 版本 >= 0
	 */
	 private String strReserved3 = new String();

	/**
	 * dwReserved
	 *
	 * 版本 >= 0
	 */
	 private long dwReserved1;

	/**
	 * dwReserved
	 *
	 * 版本 >= 0
	 */
	 private long dwReserved2;

	/**
	 * dwReserved
	 *
	 * 版本 >= 0
	 */
	 private long dwReserved3;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUInt(priceAddStep);
		bs.pushUInt(priceAddType);
		bs.pushUInt(plegeStart);
		bs.pushUInt(plegeMoney);
		bs.pushUInt(fixupCount);
		bs.pushUInt(currAuctionPrice);
		bs.pushString(strReserved1);
		bs.pushString(strReserved2);
		bs.pushString(strReserved3);
		bs.pushUInt(dwReserved1);
		bs.pushUInt(dwReserved2);
		bs.pushUInt(dwReserved3);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		priceAddStep = bs.popUInt();
		priceAddType = bs.popUInt();
		plegeStart = bs.popUInt();
		plegeMoney = bs.popUInt();
		fixupCount = bs.popUInt();
		currAuctionPrice = bs.popUInt();
		strReserved1 = bs.popString();
		strReserved2 = bs.popString();
		strReserved3 = bs.popString();
		dwReserved1 = bs.popUInt();
		dwReserved2 = bs.popUInt();
		dwReserved3 = bs.popUInt();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取加价幅度，每次出价时的加价幅度
	 * 
	 * 此字段的版本 >= 0
	 * @return priceAddStep value 类型为:long
	 * 
	 */
	public long getPriceAddStep()
	{
		return priceAddStep;
	}


	/**
	 * 设置加价幅度，每次出价时的加价幅度
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPriceAddStep(long value)
	{
		this.priceAddStep = value;
	}


	/**
	 * 获取加价方式，【用户自定义加价/系统自定义加价】两种
	 * 
	 * 此字段的版本 >= 0
	 * @return priceAddType value 类型为:long
	 * 
	 */
	public long getPriceAddType()
	{
		return priceAddType;
	}


	/**
	 * 设置加价方式，【用户自定义加价/系统自定义加价】两种
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPriceAddType(long value)
	{
		this.priceAddType = value;
	}


	/**
	 * 获取抵押金生效金额，一些商品一开始起拍的时候不需要交抵押金，但是当商品的价格被拍到一定的金额时，就必须缴纳抵押金才能继续拍卖了
	 * 
	 * 此字段的版本 >= 0
	 * @return plegeStart value 类型为:long
	 * 
	 */
	public long getPlegeStart()
	{
		return plegeStart;
	}


	/**
	 * 设置抵押金生效金额，一些商品一开始起拍的时候不需要交抵押金，但是当商品的价格被拍到一定的金额时，就必须缴纳抵押金才能继续拍卖了
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPlegeStart(long value)
	{
		this.plegeStart = value;
	}


	/**
	 * 获取抵押金，需要缴纳抵押金才能进行拍卖的时候，缴纳的抵押金金额
	 * 
	 * 此字段的版本 >= 0
	 * @return plegeMoney value 类型为:long
	 * 
	 */
	public long getPlegeMoney()
	{
		return plegeMoney;
	}


	/**
	 * 设置抵押金，需要缴纳抵押金才能进行拍卖的时候，缴纳的抵押金金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPlegeMoney(long value)
	{
		this.plegeMoney = value;
	}


	/**
	 * 获取出价次数，总的商品出价次数
	 * 
	 * 此字段的版本 >= 0
	 * @return fixupCount value 类型为:long
	 * 
	 */
	public long getFixupCount()
	{
		return fixupCount;
	}


	/**
	 * 设置出价次数，总的商品出价次数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFixupCount(long value)
	{
		this.fixupCount = value;
	}


	/**
	 * 获取当前最新价格，商品最新的出价价格
	 * 
	 * 此字段的版本 >= 0
	 * @return currAuctionPrice value 类型为:long
	 * 
	 */
	public long getCurrAuctionPrice()
	{
		return currAuctionPrice;
	}


	/**
	 * 设置当前最新价格，商品最新的出价价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCurrAuctionPrice(long value)
	{
		this.currAuctionPrice = value;
	}


	/**
	 * 获取strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return strReserved1 value 类型为:String
	 * 
	 */
	public String getStrReserved1()
	{
		return strReserved1;
	}


	/**
	 * 设置strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrReserved1(String value)
	{
		this.strReserved1 = value;
	}


	/**
	 * 获取strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return strReserved2 value 类型为:String
	 * 
	 */
	public String getStrReserved2()
	{
		return strReserved2;
	}


	/**
	 * 设置strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrReserved2(String value)
	{
		this.strReserved2 = value;
	}


	/**
	 * 获取strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return strReserved3 value 类型为:String
	 * 
	 */
	public String getStrReserved3()
	{
		return strReserved3;
	}


	/**
	 * 设置strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrReserved3(String value)
	{
		this.strReserved3 = value;
	}


	/**
	 * 获取dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return dwReserved1 value 类型为:long
	 * 
	 */
	public long getDwReserved1()
	{
		return dwReserved1;
	}


	/**
	 * 设置dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwReserved1(long value)
	{
		this.dwReserved1 = value;
	}


	/**
	 * 获取dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return dwReserved2 value 类型为:long
	 * 
	 */
	public long getDwReserved2()
	{
		return dwReserved2;
	}


	/**
	 * 设置dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwReserved2(long value)
	{
		this.dwReserved2 = value;
	}


	/**
	 * 获取dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return dwReserved3 value 类型为:long
	 * 
	 */
	public long getDwReserved3()
	{
		return dwReserved3;
	}


	/**
	 * 设置dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwReserved3(long value)
	{
		this.dwReserved3 = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemAuction)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段priceAddStep的长度 size_of(uint32_t)
				length += 4;  //计算字段priceAddType的长度 size_of(uint32_t)
				length += 4;  //计算字段plegeStart的长度 size_of(uint32_t)
				length += 4;  //计算字段plegeMoney的长度 size_of(uint32_t)
				length += 4;  //计算字段fixupCount的长度 size_of(uint32_t)
				length += 4;  //计算字段currAuctionPrice的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(strReserved1);  //计算字段strReserved1的长度 size_of(String)
				length += ByteStream.getObjectSize(strReserved2);  //计算字段strReserved2的长度 size_of(String)
				length += ByteStream.getObjectSize(strReserved3);  //计算字段strReserved3的长度 size_of(String)
				length += 4;  //计算字段dwReserved1的长度 size_of(uint32_t)
				length += 4;  //计算字段dwReserved2的长度 size_of(uint32_t)
				length += 4;  //计算字段dwReserved3的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
