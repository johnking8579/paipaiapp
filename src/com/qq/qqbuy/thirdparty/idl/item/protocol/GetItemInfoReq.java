 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2011-06-09 02:25::11
 *
 *@since version:0
*/
public class  GetItemInfoReq implements IServiceObject, java.io.Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4160485426734857935L;

	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 来源,不能为空
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 商品编码,不能为空
	 *
	 * 版本 >= 0
	 */
	 private String itemCode = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(machineKey);
		bs.pushString(source);
		bs.pushString(itemCode);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		machineKey = bs.popString();
		source = bs.popString();
		itemCode = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80031803L;
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		if (value != null) {
				this.machineKey = value;
		}else{
				this.machineKey = new String();
		}
	}


	/**
	 * 获取来源,不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置来源,不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		if (value != null) {
				this.source = value;
		}else{
				this.source = new String();
		}
	}


	/**
	 * 获取商品编码,不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @return itemCode value 类型为:String
	 * 
	 */
	public String getItemCode()
	{
		return itemCode;
	}


	/**
	 * 设置商品编码,不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemCode(String value)
	{
		if (value != null) {
				this.itemCode = value;
		}else{
				this.itemCode = new String();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetItemInfoReq)
				length += ByteStream.getObjectSize(machineKey);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(itemCode);  //计算字段itemCode的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
