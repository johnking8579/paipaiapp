 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.EvalIdl.java

package com.qq.qqbuy.thirdparty.idl.item.protocol.eval;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *做评返回
 *
 *@date 2012-12-17 10:47:50
 *
 *@since version:0
*/
public class  MakeEvalResp implements IServiceObject
{
	public long result;
	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String ErrMsg = new String();

	/**
	 * 保留输出字段
	 *
	 * 版本 >= 0
	 */
	 private String ResveOut = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushString(ErrMsg);
		bs.pushString(ResveOut);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		ErrMsg = bs.popString();
		ResveOut = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x23158805L;
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return ErrMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return ErrMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.ErrMsg = value;
	}


	/**
	 * 获取保留输出字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ResveOut value 类型为:String
	 * 
	 */
	public String getResveOut()
	{
		return ResveOut;
	}


	/**
	 * 设置保留输出字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setResveOut(String value)
	{
		this.ResveOut = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(MakeEvalResp)
				length += ByteStream.getObjectSize(ErrMsg);  //计算字段ErrMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(ResveOut);  //计算字段ResveOut的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
