//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.item.po.idl.ItemPoV2.java

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import java.util.Map;
import com.paipai.lang.uint32_t;
import com.paipai.lang.MultiMap;
import java.util.HashMap;

/**
 *商品扩展信息（请外部团队不要使用该结构中的任何内容，基础组有权在未通知大家的前提下修改其中的定义）
 *
 *@date 2013-02-27 05:06:02
 *
 *@since version:0
*/
public class ItemExt  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 1;

	/**
	 * 扩展属性和值
	 *
	 * 版本 >= 0
	 */
	 private Map<uint32_t,String> mapExt = new HashMap<uint32_t,String>();

	/**
	 * 新的扩展属性和值，请不要再用mapExt
	 *
	 * 版本 >= 1
	 */
	 private MultiMap<uint32_t,String> MultiMapExt = new MultiMap<uint32_t,String>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushObject(mapExt);
		if(  this.version >= 1 ){
				bs.pushObject(MultiMapExt);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		mapExt = (Map<uint32_t,String>)bs.popMap(uint32_t.class,String.class);
		if(  this.version >= 1 ){
				MultiMapExt = (MultiMap<uint32_t,String>)bs.popMultiMap(uint32_t.class,String.class);
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取扩展属性和值
	 * 
	 * 此字段的版本 >= 0
	 * @return mapExt value 类型为:Map<uint32_t,String>
	 * 
	 */
	public Map<uint32_t,String> getMapExt()
	{
		return mapExt;
	}


	/**
	 * 设置扩展属性和值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<uint32_t,String>
	 * 
	 */
	public void setMapExt(Map<uint32_t,String> value)
	{
		if (value != null) {
				this.mapExt = value;
		}else{
				this.mapExt = new HashMap<uint32_t,String>();
		}
	}


	/**
	 * 获取新的扩展属性和值，请不要再用mapExt
	 * 
	 * 此字段的版本 >= 1
	 * @return MultiMapExt value 类型为:MultiMap<uint32_t,String>
	 * 
	 */
	public MultiMap<uint32_t,String> getMultiMapExt()
	{
		return MultiMapExt;
	}


	/**
	 * 设置新的扩展属性和值，请不要再用mapExt
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:MultiMap<uint32_t,String>
	 * 
	 */
	public void setMultiMapExt(MultiMap<uint32_t,String> value)
	{
		if (value != null) {
				this.MultiMapExt = value;
		}else{
				this.MultiMapExt = new MultiMap<uint32_t,String>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemExt)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(mapExt);  //计算字段mapExt的长度 size_of(Map)
				if(  this.version >= 1 ){
						length += ByteStream.getObjectSize(MultiMapExt);  //计算字段MultiMapExt的长度 size_of(MultiMap)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本1所包含的字段*******
 *	long version;///<版本号
 *	Map<uint32_t,String> mapExt;///<扩展属性和值
 *	MultiMap<uint32_t,String> MultiMapExt;///<新的扩展属性和值，请不要再用mapExt
 *****以上是版本1所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
