 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *
 *
 *@date 2011-06-08 04:38::32
 *
 *@since version:1
*/
public class PPFreight  implements ICanSerializeObject
{
	/**
	 * 首件运费
	 *
	 * 版本 >= 0
	 */
	 private int priceNormal;

	/**
	 * 每增加一见的运费
	 *
	 * 版本 >= 0
	 */
	 private int priceNormalAdd;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushInt(priceNormal);
		bs.pushInt(priceNormalAdd);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		priceNormal = bs.popInt();
		priceNormalAdd = bs.popInt();
		return bs.getReadLength();
	} 


	/**
	 * 获取首件运费
	 * 
	 * 此字段的版本 >= 0
	 * @return priceNormal value 类型为:int
	 * 
	 */
	public int getPriceNormal()
	{
		return priceNormal;
	}


	/**
	 * 设置首件运费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPriceNormal(int value)
	{
		this.priceNormal = value;
	}


	/**
	 * 获取每增加一见的运费
	 * 
	 * 此字段的版本 >= 0
	 * @return priceNormalAdd value 类型为:int
	 * 
	 */
	public int getPriceNormalAdd()
	{
		return priceNormalAdd;
	}


	/**
	 * 设置每增加一见的运费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPriceNormalAdd(int value)
	{
		this.priceNormalAdd = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(PPFreight)
				length += 4;  //计算字段priceNormal的长度 size_of(int)
				length += 4;  //计算字段priceNormalAdd的长度 size_of(int)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
