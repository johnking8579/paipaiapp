 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.portal.ao.idl.MartAo.java

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import java.util.Vector;

/**
 *卖场快车活动曝光接口响应参数
 *
 *@date 2014-09-04 05:10:45
 *
 *@since version:0
*/
public class  getActiveItemListResp extends NetMessage
{
	/**
	 * 活动数据
	 *
	 * 版本 >= 0
	 */
	 private Vector<ActiveData> activeList = new Vector<ActiveData>();

	/**
	 * json格式化数据
	 *
	 * 版本 >= 0
	 */
	 private String jsonData = new String();

	/**
	 * 错误消息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(activeList);
		bs.pushString(jsonData);
		bs.pushString(errMsg);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		activeList = (Vector<ActiveData>)bs.popVector(ActiveData.class);
		jsonData = bs.popString();
		errMsg = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x87488001L;
	}


	/**
	 * 获取活动数据
	 * 
	 * 此字段的版本 >= 0
	 * @return activeList value 类型为:Vector<ActiveData>
	 * 
	 */
	public Vector<ActiveData> getActiveList()
	{
		return activeList;
	}


	/**
	 * 设置活动数据
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<ActiveData>
	 * 
	 */
	public void setActiveList(Vector<ActiveData> value)
	{
		if (value != null) {
				this.activeList = value;
		}else{
				this.activeList = new Vector<ActiveData>();
		}
	}


	/**
	 * 获取json格式化数据
	 * 
	 * 此字段的版本 >= 0
	 * @return jsonData value 类型为:String
	 * 
	 */
	public String getJsonData()
	{
		return jsonData;
	}


	/**
	 * 设置json格式化数据
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setJsonData(String value)
	{
		this.jsonData = value;
	}


	/**
	 * 获取错误消息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误消息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(getActiveItemListResp)
				length += ByteStream.getObjectSize(activeList);  //计算字段activeList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(jsonData);  //计算字段jsonData的长度 size_of(String)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
