//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.portal.bo.idl.PortalBo.java

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *用户侧的信息 尽可能的多填
 *
 *@date 2014-09-04 05:19:57
 *
 *@since version:0
*/
public class UserInfo  implements ICanSerializeObject
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20140807;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 用户UIN
	 *
	 * 版本 >= 0
	 */
	 private long ddwUin;

	/**
	 * 版本 >= 0
	 */
	 private short ddwUin_u;

	/**
	 * VisitKey 字符串型
	 *
	 * 版本 >= 0
	 */
	 private String strVisitKey = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strVisitKey_u;

	/**
	 * 客户端IP 字符串型
	 *
	 * 版本 >= 0
	 */
	 private String strClientIp = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strClientIp_u;

	/**
	 * referer
	 *
	 * 版本 >= 0
	 */
	 private String strReferer = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strReferer_u;

	/**
	 * userAgent
	 *
	 * 版本 >= 0
	 */
	 private String strUserAgent = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strUserAgent_u;

	/**
	 * 当前URL
	 *
	 * 版本 >= 0
	 */
	 private String strCurrUrl = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strCurrUrl_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushLong(ddwUin);
		bs.pushUByte(ddwUin_u);
		bs.pushString(strVisitKey);
		bs.pushUByte(strVisitKey_u);
		bs.pushString(strClientIp);
		bs.pushUByte(strClientIp_u);
		bs.pushString(strReferer);
		bs.pushUByte(strReferer_u);
		bs.pushString(strUserAgent);
		bs.pushUByte(strUserAgent_u);
		bs.pushString(strCurrUrl);
		bs.pushUByte(strCurrUrl_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		ddwUin = bs.popLong();
		ddwUin_u = bs.popUByte();
		strVisitKey = bs.popString();
		strVisitKey_u = bs.popUByte();
		strClientIp = bs.popString();
		strClientIp_u = bs.popUByte();
		strReferer = bs.popString();
		strReferer_u = bs.popUByte();
		strUserAgent = bs.popString();
		strUserAgent_u = bs.popUByte();
		strCurrUrl = bs.popString();
		strCurrUrl_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取用户UIN
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwUin value 类型为:long
	 * 
	 */
	public long getDdwUin()
	{
		return ddwUin;
	}


	/**
	 * 设置用户UIN
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDdwUin(long value)
	{
		this.ddwUin = value;
		this.ddwUin_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwUin_u value 类型为:short
	 * 
	 */
	public short getDdwUin_u()
	{
		return ddwUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDdwUin_u(short value)
	{
		this.ddwUin_u = value;
	}


	/**
	 * 获取VisitKey 字符串型
	 * 
	 * 此字段的版本 >= 0
	 * @return strVisitKey value 类型为:String
	 * 
	 */
	public String getStrVisitKey()
	{
		return strVisitKey;
	}


	/**
	 * 设置VisitKey 字符串型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrVisitKey(String value)
	{
		this.strVisitKey = value;
		this.strVisitKey_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strVisitKey_u value 类型为:short
	 * 
	 */
	public short getStrVisitKey_u()
	{
		return strVisitKey_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrVisitKey_u(short value)
	{
		this.strVisitKey_u = value;
	}


	/**
	 * 获取客户端IP 字符串型
	 * 
	 * 此字段的版本 >= 0
	 * @return strClientIp value 类型为:String
	 * 
	 */
	public String getStrClientIp()
	{
		return strClientIp;
	}


	/**
	 * 设置客户端IP 字符串型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrClientIp(String value)
	{
		this.strClientIp = value;
		this.strClientIp_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strClientIp_u value 类型为:short
	 * 
	 */
	public short getStrClientIp_u()
	{
		return strClientIp_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrClientIp_u(short value)
	{
		this.strClientIp_u = value;
	}


	/**
	 * 获取referer
	 * 
	 * 此字段的版本 >= 0
	 * @return strReferer value 类型为:String
	 * 
	 */
	public String getStrReferer()
	{
		return strReferer;
	}


	/**
	 * 设置referer
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrReferer(String value)
	{
		this.strReferer = value;
		this.strReferer_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strReferer_u value 类型为:short
	 * 
	 */
	public short getStrReferer_u()
	{
		return strReferer_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrReferer_u(short value)
	{
		this.strReferer_u = value;
	}


	/**
	 * 获取userAgent
	 * 
	 * 此字段的版本 >= 0
	 * @return strUserAgent value 类型为:String
	 * 
	 */
	public String getStrUserAgent()
	{
		return strUserAgent;
	}


	/**
	 * 设置userAgent
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrUserAgent(String value)
	{
		this.strUserAgent = value;
		this.strUserAgent_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strUserAgent_u value 类型为:short
	 * 
	 */
	public short getStrUserAgent_u()
	{
		return strUserAgent_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrUserAgent_u(short value)
	{
		this.strUserAgent_u = value;
	}


	/**
	 * 获取当前URL
	 * 
	 * 此字段的版本 >= 0
	 * @return strCurrUrl value 类型为:String
	 * 
	 */
	public String getStrCurrUrl()
	{
		return strCurrUrl;
	}


	/**
	 * 设置当前URL
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrCurrUrl(String value)
	{
		this.strCurrUrl = value;
		this.strCurrUrl_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strCurrUrl_u value 类型为:short
	 * 
	 */
	public short getStrCurrUrl_u()
	{
		return strCurrUrl_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrCurrUrl_u(short value)
	{
		this.strCurrUrl_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(UserInfo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段ddwUin的长度 size_of(uint64_t)
				length += 1;  //计算字段ddwUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strVisitKey);  //计算字段strVisitKey的长度 size_of(String)
				length += 1;  //计算字段strVisitKey_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strClientIp);  //计算字段strClientIp的长度 size_of(String)
				length += 1;  //计算字段strClientIp_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strReferer);  //计算字段strReferer的长度 size_of(String)
				length += 1;  //计算字段strReferer_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strUserAgent);  //计算字段strUserAgent的长度 size_of(String)
				length += 1;  //计算字段strUserAgent_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strCurrUrl);  //计算字段strCurrUrl的长度 size_of(String)
				length += 1;  //计算字段strCurrUrl_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
