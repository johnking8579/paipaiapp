 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import java.util.Vector;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *paipai商品
 *
 *@date 2011-06-08 04:38::32
 *
 *@since version:0
*/
public class PPItem  implements ICanSerializeObject
{
	/**
	 * 商品价格
	 *
	 * 版本 >= 0
	 */
	 private int itemPrice;

	/**
	 * 商品的市场价格
	 *
	 * 版本 >= 0
	 */
	 private int marketPrice;

	/**
	 * 商品的邮寄费用
	 *
	 * 版本 >= 0
	 */
	 private int mailFee;

	/**
	 * 商品彩钻折扣
	 *
	 * 版本 >= 0
	 */
	 private Vector<Integer> discount = new Vector<Integer>();

	/**
	 * 运费模版id
	 *
	 * 版本 >= 0
	 */
	 private int freightId;

	/**
	 * 运费模版
	 *
	 * 版本 >= 0
	 */
	 private PPFreight freight = new PPFreight();

	/**
	 * 库存信息
	 *
	 * 版本 >= 0
	 */
	 private Vector<PPStock> stockList = new Vector<PPStock>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(itemPrice);
		bs.pushInt(marketPrice);
		bs.pushInt(mailFee);
		bs.pushObject(discount);
		bs.pushInt(freightId);
		bs.pushObject(freight);
		bs.pushObject(stockList);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		itemPrice = bs.popInt();
		marketPrice = bs.popInt();
		mailFee = bs.popInt();
		discount = (Vector<Integer>)bs.popVector(Integer.class);
		freightId = bs.popInt();
		freight = (PPFreight) bs.popObject(PPFreight.class);
		stockList = (Vector<PPStock>)bs.popVector(PPStock.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取商品价格
	 * 
	 * 此字段的版本 >= 0
	 * @return itemPrice value 类型为:int
	 * 
	 */
	public int getItemPrice()
	{
		return itemPrice;
	}


	/**
	 * 设置商品价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setItemPrice(int value)
	{
		this.itemPrice = value;
	}


	/**
	 * 获取商品的市场价格
	 * 
	 * 此字段的版本 >= 0
	 * @return marketPrice value 类型为:int
	 * 
	 */
	public int getMarketPrice()
	{
		return marketPrice;
	}


	/**
	 * 设置商品的市场价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setMarketPrice(int value)
	{
		this.marketPrice = value;
	}


	/**
	 * 获取商品的邮寄费用
	 * 
	 * 此字段的版本 >= 0
	 * @return mailFee value 类型为:int
	 * 
	 */
	public int getMailFee()
	{
		return mailFee;
	}


	/**
	 * 设置商品的邮寄费用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setMailFee(int value)
	{
		this.mailFee = value;
	}


	/**
	 * 获取商品彩钻折扣
	 * 
	 * 此字段的版本 >= 0
	 * @return discount value 类型为:Vector<Integer>
	 * 
	 */
	public Vector<Integer> getDiscount()
	{
		return discount;
	}


	/**
	 * 设置商品彩钻折扣
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<Integer>
	 * 
	 */
	public void setDiscount(Vector<Integer> value)
	{
		if (value != null) {
				this.discount = value;
		}else{
				this.discount = new Vector<Integer>();
		}
	}


	/**
	 * 获取运费模版id
	 * 
	 * 此字段的版本 >= 0
	 * @return freightId value 类型为:int
	 * 
	 */
	public int getFreightId()
	{
		return freightId;
	}


	/**
	 * 设置运费模版id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setFreightId(int value)
	{
		this.freightId = value;
	}


	/**
	 * 获取运费模版
	 * 
	 * 此字段的版本 >= 0
	 * @return freight value 类型为:PPFreight
	 * 
	 */
	public PPFreight getFreight()
	{
		return freight;
	}


	/**
	 * 设置运费模版
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:PPFreight
	 * 
	 */
	public void setFreight(PPFreight value)
	{
		if (value != null) {
				this.freight = value;
		}else{
				this.freight = new PPFreight();
		}
	}


	/**
	 * 获取库存信息
	 * 
	 * 此字段的版本 >= 0
	 * @return stockList value 类型为:Vector<PPStock>
	 * 
	 */
	public Vector<PPStock> getStockList()
	{
		return stockList;
	}


	/**
	 * 设置库存信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<PPStock>
	 * 
	 */
	public void setStockList(Vector<PPStock> value)
	{
		if (value != null) {
				this.stockList = value;
		}else{
				this.stockList = new Vector<PPStock>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(PPItem)
				length += 4;  //计算字段itemPrice的长度 size_of(int)
				length += 4;  //计算字段marketPrice的长度 size_of(int)
				length += 4;  //计算字段mailFee的长度 size_of(int)
				length += ByteStream.getObjectSize(discount);  //计算字段discount的长度 size_of(Vector)
				length += 4;  //计算字段freightId的长度 size_of(int)
				length += ByteStream.getObjectSize(freight);  //计算字段freight的长度 size_of(PPFreight)
				length += ByteStream.getObjectSize(stockList);  //计算字段stockList的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
