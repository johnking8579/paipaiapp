 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *商品库存信息
 *
 *@date 2012-09-02 10:38::24
 *
 *@since version:0
*/
public class ApiStock  implements ICanSerializeObject
{
	/**
	 * 商品ID
	 *
	 * 版本 >= 0
	 */
	 private String itemId = new String();

	/**
	 * 发布来源
	 *
	 * 版本 >= 0
	 */
	 private String attr = new String();

	/**
	 * 描述信息
	 *
	 * 版本 >= 0
	 */
	 private String desc = new String();

	/**
	 * 库存ID
	 *
	 * 版本 >= 0
	 */
	 private String stockId = new String();

	/**
	 * 库存hash值
	 *
	 * 版本 >= 0
	 */
	 private long attrHash;

	/**
	 * 库存属性
	 *
	 * 版本 >= 0
	 */
	 private long property;

	/**
	 * 卖家Uin
	 *
	 * 版本 >= 0
	 */
	 private long sellerUin;

	/**
	 * 售出数量
	 *
	 * 版本 >= 0
	 */
	 private long soldNum;

	/**
	 * 单价,单位：分
	 *
	 * 版本 >= 0
	 */
	 private long price;

	/**
	 * 库存数量
	 *
	 * 版本 >= 0
	 */
	 private long num;

	/**
	 * 发布时间
	 *
	 * 版本 >= 0
	 */
	 private long addTime;

	/**
	 * 最后编辑时间
	 *
	 * 版本 >= 0
	 */
	 private long lastModifyTime;

	/**
	 * 版本 >= 0
	 */
	 private long index;

	/**
	 * 版本 >= 0
	 */
	 private short itemId_u;

	/**
	 * 版本 >= 0
	 */
	 private short attr_u;

	/**
	 * 版本 >= 0
	 */
	 private short desc_u;

	/**
	 * 版本 >= 0
	 */
	 private short stockId_u;

	/**
	 * 版本 >= 0
	 */
	 private short attrHash_u;

	/**
	 * 版本 >= 0
	 */
	 private short property_u;

	/**
	 * 版本 >= 0
	 */
	 private short sellerUin_u;

	/**
	 * 版本 >= 0
	 */
	 private short soldNum_u;

	/**
	 * 版本 >= 0
	 */
	 private short price_u;

	/**
	 * 版本 >= 0
	 */
	 private short num_u;

	/**
	 * 版本 >= 0
	 */
	 private short addTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short lastModifyTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short index_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushString(itemId);
		bs.pushString(attr);
		bs.pushString(desc);
		bs.pushString(stockId);
		bs.pushUInt(attrHash);
		bs.pushUInt(property);
		bs.pushUInt(sellerUin);
		bs.pushUInt(soldNum);
		bs.pushUInt(price);
		bs.pushUInt(num);
		bs.pushUInt(addTime);
		bs.pushUInt(lastModifyTime);
		bs.pushUInt(index);
		bs.pushUByte(itemId_u);
		bs.pushUByte(attr_u);
		bs.pushUByte(desc_u);
		bs.pushUByte(stockId_u);
		bs.pushUByte(attrHash_u);
		bs.pushUByte(property_u);
		bs.pushUByte(sellerUin_u);
		bs.pushUByte(soldNum_u);
		bs.pushUByte(price_u);
		bs.pushUByte(num_u);
		bs.pushUByte(addTime_u);
		bs.pushUByte(lastModifyTime_u);
		bs.pushUByte(index_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		itemId = bs.popString();
		attr = bs.popString();
		desc = bs.popString();
		stockId = bs.popString();
		attrHash = bs.popUInt();
		property = bs.popUInt();
		sellerUin = bs.popUInt();
		soldNum = bs.popUInt();
		price = bs.popUInt();
		num = bs.popUInt();
		addTime = bs.popUInt();
		lastModifyTime = bs.popUInt();
		index = bs.popUInt();
		itemId_u = bs.popUByte();
		attr_u = bs.popUByte();
		desc_u = bs.popUByte();
		stockId_u = bs.popUByte();
		attrHash_u = bs.popUByte();
		property_u = bs.popUByte();
		sellerUin_u = bs.popUByte();
		soldNum_u = bs.popUByte();
		price_u = bs.popUByte();
		num_u = bs.popUByte();
		addTime_u = bs.popUByte();
		lastModifyTime_u = bs.popUByte();
		index_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return itemId value 类型为:String
	 * 
	 */
	public String getItemId()
	{
		return itemId;
	}


	/**
	 * 设置商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemId(String value)
	{
		if (value != null) {
				this.itemId = value;
				this.itemId_u = 1;
		}
	}


	/**
	 * 获取发布来源
	 * 
	 * 此字段的版本 >= 0
	 * @return attr value 类型为:String
	 * 
	 */
	public String getAttr()
	{
		return attr;
	}


	/**
	 * 设置发布来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAttr(String value)
	{
		if (value != null) {
				this.attr = value;
				this.attr_u = 1;
		}
	}


	/**
	 * 获取描述信息
	 * 
	 * 此字段的版本 >= 0
	 * @return desc value 类型为:String
	 * 
	 */
	public String getDesc()
	{
		return desc;
	}


	/**
	 * 设置描述信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDesc(String value)
	{
		if (value != null) {
				this.desc = value;
				this.desc_u = 1;
		}
	}


	/**
	 * 获取库存ID
	 * 
	 * 此字段的版本 >= 0
	 * @return stockId value 类型为:String
	 * 
	 */
	public String getStockId()
	{
		return stockId;
	}


	/**
	 * 设置库存ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStockId(String value)
	{
		if (value != null) {
				this.stockId = value;
				this.stockId_u = 1;
		}
	}


	/**
	 * 获取库存hash值
	 * 
	 * 此字段的版本 >= 0
	 * @return attrHash value 类型为:long
	 * 
	 */
	public long getAttrHash()
	{
		return attrHash;
	}


	/**
	 * 设置库存hash值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAttrHash(long value)
	{
		this.attrHash = value;
		this.attrHash_u = 1;
	}


	/**
	 * 获取库存属性
	 * 
	 * 此字段的版本 >= 0
	 * @return property value 类型为:long
	 * 
	 */
	public long getProperty()
	{
		return property;
	}


	/**
	 * 设置库存属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setProperty(long value)
	{
		this.property = value;
		this.property_u = 1;
	}


	/**
	 * 获取卖家Uin
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return sellerUin;
	}


	/**
	 * 设置卖家Uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.sellerUin = value;
		this.sellerUin_u = 1;
	}


	/**
	 * 获取售出数量
	 * 
	 * 此字段的版本 >= 0
	 * @return soldNum value 类型为:long
	 * 
	 */
	public long getSoldNum()
	{
		return soldNum;
	}


	/**
	 * 设置售出数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSoldNum(long value)
	{
		this.soldNum = value;
		this.soldNum_u = 1;
	}


	/**
	 * 获取单价,单位：分
	 * 
	 * 此字段的版本 >= 0
	 * @return price value 类型为:long
	 * 
	 */
	public long getPrice()
	{
		return price;
	}


	/**
	 * 设置单价,单位：分
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPrice(long value)
	{
		this.price = value;
		this.price_u = 1;
	}


	/**
	 * 获取库存数量
	 * 
	 * 此字段的版本 >= 0
	 * @return num value 类型为:long
	 * 
	 */
	public long getNum()
	{
		return num;
	}


	/**
	 * 设置库存数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNum(long value)
	{
		this.num = value;
		this.num_u = 1;
	}


	/**
	 * 获取发布时间
	 * 
	 * 此字段的版本 >= 0
	 * @return addTime value 类型为:long
	 * 
	 */
	public long getAddTime()
	{
		return addTime;
	}


	/**
	 * 设置发布时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddTime(long value)
	{
		this.addTime = value;
		this.addTime_u = 1;
	}


	/**
	 * 获取最后编辑时间
	 * 
	 * 此字段的版本 >= 0
	 * @return lastModifyTime value 类型为:long
	 * 
	 */
	public long getLastModifyTime()
	{
		return lastModifyTime;
	}


	/**
	 * 设置最后编辑时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastModifyTime(long value)
	{
		this.lastModifyTime = value;
		this.lastModifyTime_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return index value 类型为:long
	 * 
	 */
	public long getIndex()
	{
		return index;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setIndex(long value)
	{
		this.index = value;
		this.index_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return itemId_u value 类型为:short
	 * 
	 */
	public short getItemId_u()
	{
		return itemId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemId_u(short value)
	{
		this.itemId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return attr_u value 类型为:short
	 * 
	 */
	public short getAttr_u()
	{
		return attr_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAttr_u(short value)
	{
		this.attr_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return desc_u value 类型为:short
	 * 
	 */
	public short getDesc_u()
	{
		return desc_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDesc_u(short value)
	{
		this.desc_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return stockId_u value 类型为:short
	 * 
	 */
	public short getStockId_u()
	{
		return stockId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStockId_u(short value)
	{
		this.stockId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return attrHash_u value 类型为:short
	 * 
	 */
	public short getAttrHash_u()
	{
		return attrHash_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAttrHash_u(short value)
	{
		this.attrHash_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return property_u value 类型为:short
	 * 
	 */
	public short getProperty_u()
	{
		return property_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProperty_u(short value)
	{
		this.property_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerUin_u value 类型为:short
	 * 
	 */
	public short getSellerUin_u()
	{
		return sellerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerUin_u(short value)
	{
		this.sellerUin_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return soldNum_u value 类型为:short
	 * 
	 */
	public short getSoldNum_u()
	{
		return soldNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSoldNum_u(short value)
	{
		this.soldNum_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return price_u value 类型为:short
	 * 
	 */
	public short getPrice_u()
	{
		return price_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPrice_u(short value)
	{
		this.price_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return num_u value 类型为:short
	 * 
	 */
	public short getNum_u()
	{
		return num_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNum_u(short value)
	{
		this.num_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return addTime_u value 类型为:short
	 * 
	 */
	public short getAddTime_u()
	{
		return addTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddTime_u(short value)
	{
		this.addTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return lastModifyTime_u value 类型为:short
	 * 
	 */
	public short getLastModifyTime_u()
	{
		return lastModifyTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastModifyTime_u(short value)
	{
		this.lastModifyTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return index_u value 类型为:short
	 * 
	 */
	public short getIndex_u()
	{
		return index_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIndex_u(short value)
	{
		this.index_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ApiStock)
				length += ByteStream.getObjectSize(itemId);  //计算字段itemId的长度 size_of(String)
				length += ByteStream.getObjectSize(attr);  //计算字段attr的长度 size_of(String)
				length += ByteStream.getObjectSize(desc);  //计算字段desc的长度 size_of(String)
				length += ByteStream.getObjectSize(stockId);  //计算字段stockId的长度 size_of(String)
				length += 4;  //计算字段attrHash的长度 size_of(uint32_t)
				length += 4;  //计算字段property的长度 size_of(uint32_t)
				length += 4;  //计算字段sellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段soldNum的长度 size_of(uint32_t)
				length += 4;  //计算字段price的长度 size_of(uint32_t)
				length += 4;  //计算字段num的长度 size_of(uint32_t)
				length += 4;  //计算字段addTime的长度 size_of(uint32_t)
				length += 4;  //计算字段lastModifyTime的长度 size_of(uint32_t)
				length += 4;  //计算字段index的长度 size_of(uint32_t)
				length += 1;  //计算字段itemId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段attr_u的长度 size_of(uint8_t)
				length += 1;  //计算字段desc_u的长度 size_of(uint8_t)
				length += 1;  //计算字段stockId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段attrHash_u的长度 size_of(uint8_t)
				length += 1;  //计算字段property_u的长度 size_of(uint8_t)
				length += 1;  //计算字段sellerUin_u的长度 size_of(uint8_t)
				length += 1;  //计算字段soldNum_u的长度 size_of(uint8_t)
				length += 1;  //计算字段price_u的长度 size_of(uint8_t)
				length += 1;  //计算字段num_u的长度 size_of(uint8_t)
				length += 1;  //计算字段addTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段lastModifyTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段index_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
