//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.portal.bo.idl.PortalBo.java

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.Map;
import com.paipai.lang.uint32_t;
import java.util.Vector;
import java.util.HashMap;

/**
 *卖场快车活动请求信息
 *
 *@date 2014-09-04 05:19:57
 *
 *@since version:0
*/
public class MartActive  implements ICanSerializeObject
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20140807;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 活动ID
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint32_t> vecActiveIds = new Vector<uint32_t>();

	/**
	 * 版本 >= 0
	 */
	 private short vecActiveIds_u;

	/**
	 * 指定各商品池展示的商品数量 格式<poolid, num> 不为空时候仅展示AreaCtrl中指定的区域和数量 为空时候暂时活动下所有区域 商品数以Pcs字段为准
	 *
	 * 版本 >= 0
	 */
	 private Map<uint32_t,uint32_t> mapAreaCtrl = new HashMap<uint32_t,uint32_t>();

	/**
	 * 版本 >= 0
	 */
	 private short mapAreaCtrl_u;

	/**
	 * 当未指定AreaCtrl时候有效 全部区域统一数量限制 值范围为1-128
	 *
	 * 版本 >= 0
	 */
	 private long dwPcs = 6;

	/**
	 * 版本 >= 0
	 */
	 private short dwPcs_u;

	/**
	 * 是否非智能排序 1 管理后台配置排序规则 0 使用BI排序的结果 
	 *
	 * 版本 >= 0
	 */
	 private long dwSortType = 0;

	/**
	 * 版本 >= 0
	 */
	 private short dwSortType_u;

	/**
	 * 是否需要拉取商品优质评论 默认不拉取 一次请求的商品数过多时 谨慎开启
	 *
	 * 版本 >= 0
	 */
	 private long dwNeedEval = 0;

	/**
	 * 版本 >= 0
	 */
	 private short dwNeedEval_u;

	/**
	 * Bi 场景高位值  默认不填
	 *
	 * 版本 >= 0
	 */
	 private long dwBiHid;

	/**
	 * 版本 >= 0
	 */
	 private short dwBiHid_u;

	/**
	 * Bi 场景高位值  默认不填
	 *
	 * 版本 >= 0
	 */
	 private long dwBiMid;

	/**
	 * 版本 >= 0
	 */
	 private short dwBiMid_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushObject(vecActiveIds);
		bs.pushUByte(vecActiveIds_u);
		bs.pushObject(mapAreaCtrl);
		bs.pushUByte(mapAreaCtrl_u);
		bs.pushUInt(dwPcs);
		bs.pushUByte(dwPcs_u);
		bs.pushUInt(dwSortType);
		bs.pushUByte(dwSortType_u);
		bs.pushUInt(dwNeedEval);
		bs.pushUByte(dwNeedEval_u);
		bs.pushUInt(dwBiHid);
		bs.pushUByte(dwBiHid_u);
		bs.pushUInt(dwBiMid);
		bs.pushUByte(dwBiMid_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		vecActiveIds = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		vecActiveIds_u = bs.popUByte();
		mapAreaCtrl = (Map<uint32_t,uint32_t>)bs.popMap(uint32_t.class,uint32_t.class);
		mapAreaCtrl_u = bs.popUByte();
		dwPcs = bs.popUInt();
		dwPcs_u = bs.popUByte();
		dwSortType = bs.popUInt();
		dwSortType_u = bs.popUByte();
		dwNeedEval = bs.popUInt();
		dwNeedEval_u = bs.popUByte();
		dwBiHid = bs.popUInt();
		dwBiHid_u = bs.popUByte();
		dwBiMid = bs.popUInt();
		dwBiMid_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @return vecActiveIds value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getVecActiveIds()
	{
		return vecActiveIds;
	}


	/**
	 * 设置活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setVecActiveIds(Vector<uint32_t> value)
	{
		if (value != null) {
				this.vecActiveIds = value;
				this.vecActiveIds_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return vecActiveIds_u value 类型为:short
	 * 
	 */
	public short getVecActiveIds_u()
	{
		return vecActiveIds_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVecActiveIds_u(short value)
	{
		this.vecActiveIds_u = value;
	}


	/**
	 * 获取指定各商品池展示的商品数量 格式<poolid, num> 不为空时候仅展示AreaCtrl中指定的区域和数量 为空时候暂时活动下所有区域 商品数以Pcs字段为准
	 * 
	 * 此字段的版本 >= 0
	 * @return mapAreaCtrl value 类型为:Map<uint32_t,uint32_t>
	 * 
	 */
	public Map<uint32_t,uint32_t> getMapAreaCtrl()
	{
		return mapAreaCtrl;
	}


	/**
	 * 设置指定各商品池展示的商品数量 格式<poolid, num> 不为空时候仅展示AreaCtrl中指定的区域和数量 为空时候暂时活动下所有区域 商品数以Pcs字段为准
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<uint32_t,uint32_t>
	 * 
	 */
	public void setMapAreaCtrl(Map<uint32_t,uint32_t> value)
	{
		if (value != null) {
				this.mapAreaCtrl = value;
				this.mapAreaCtrl_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return mapAreaCtrl_u value 类型为:short
	 * 
	 */
	public short getMapAreaCtrl_u()
	{
		return mapAreaCtrl_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMapAreaCtrl_u(short value)
	{
		this.mapAreaCtrl_u = value;
	}


	/**
	 * 获取当未指定AreaCtrl时候有效 全部区域统一数量限制 值范围为1-128
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPcs value 类型为:long
	 * 
	 */
	public long getDwPcs()
	{
		return dwPcs;
	}


	/**
	 * 设置当未指定AreaCtrl时候有效 全部区域统一数量限制 值范围为1-128
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwPcs(long value)
	{
		this.dwPcs = value;
		this.dwPcs_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPcs_u value 类型为:short
	 * 
	 */
	public short getDwPcs_u()
	{
		return dwPcs_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwPcs_u(short value)
	{
		this.dwPcs_u = value;
	}


	/**
	 * 获取是否非智能排序 1 管理后台配置排序规则 0 使用BI排序的结果 
	 * 
	 * 此字段的版本 >= 0
	 * @return dwSortType value 类型为:long
	 * 
	 */
	public long getDwSortType()
	{
		return dwSortType;
	}


	/**
	 * 设置是否非智能排序 1 管理后台配置排序规则 0 使用BI排序的结果 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwSortType(long value)
	{
		this.dwSortType = value;
		this.dwSortType_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwSortType_u value 类型为:short
	 * 
	 */
	public short getDwSortType_u()
	{
		return dwSortType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwSortType_u(short value)
	{
		this.dwSortType_u = value;
	}


	/**
	 * 获取是否需要拉取商品优质评论 默认不拉取 一次请求的商品数过多时 谨慎开启
	 * 
	 * 此字段的版本 >= 0
	 * @return dwNeedEval value 类型为:long
	 * 
	 */
	public long getDwNeedEval()
	{
		return dwNeedEval;
	}


	/**
	 * 设置是否需要拉取商品优质评论 默认不拉取 一次请求的商品数过多时 谨慎开启
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwNeedEval(long value)
	{
		this.dwNeedEval = value;
		this.dwNeedEval_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwNeedEval_u value 类型为:short
	 * 
	 */
	public short getDwNeedEval_u()
	{
		return dwNeedEval_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwNeedEval_u(short value)
	{
		this.dwNeedEval_u = value;
	}


	/**
	 * 获取Bi 场景高位值  默认不填
	 * 
	 * 此字段的版本 >= 0
	 * @return dwBiHid value 类型为:long
	 * 
	 */
	public long getDwBiHid()
	{
		return dwBiHid;
	}


	/**
	 * 设置Bi 场景高位值  默认不填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwBiHid(long value)
	{
		this.dwBiHid = value;
		this.dwBiHid_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwBiHid_u value 类型为:short
	 * 
	 */
	public short getDwBiHid_u()
	{
		return dwBiHid_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwBiHid_u(short value)
	{
		this.dwBiHid_u = value;
	}


	/**
	 * 获取Bi 场景高位值  默认不填
	 * 
	 * 此字段的版本 >= 0
	 * @return dwBiMid value 类型为:long
	 * 
	 */
	public long getDwBiMid()
	{
		return dwBiMid;
	}


	/**
	 * 设置Bi 场景高位值  默认不填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwBiMid(long value)
	{
		this.dwBiMid = value;
		this.dwBiMid_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwBiMid_u value 类型为:short
	 * 
	 */
	public short getDwBiMid_u()
	{
		return dwBiMid_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwBiMid_u(short value)
	{
		this.dwBiMid_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(MartActive)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(vecActiveIds);  //计算字段vecActiveIds的长度 size_of(Vector)
				length += 1;  //计算字段vecActiveIds_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(mapAreaCtrl);  //计算字段mapAreaCtrl的长度 size_of(Map)
				length += 1;  //计算字段mapAreaCtrl_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwPcs的长度 size_of(uint32_t)
				length += 1;  //计算字段dwPcs_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwSortType的长度 size_of(uint32_t)
				length += 1;  //计算字段dwSortType_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwNeedEval的长度 size_of(uint32_t)
				length += 1;  //计算字段dwNeedEval_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwBiHid的长度 size_of(uint32_t)
				length += 1;  //计算字段dwBiHid_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwBiMid的长度 size_of(uint32_t)
				length += 1;  //计算字段dwBiMid_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
