//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.item.po.idl.ItemPoV2.java

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *商品视频信息
 *
 *@date 2013-02-27 05:06:02
 *
 *@since version:0
*/
public class ItemVideo  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 0;

	/**
	 * 视频连接地址，目前只有一个视频连接
	 *
	 * 版本 >= 0
	 */
	 private String videoLinker = new String();

	/**
	 * strReserved
	 *
	 * 版本 >= 0
	 */
	 private String strReserved1 = new String();

	/**
	 * strReserved
	 *
	 * 版本 >= 0
	 */
	 private String strReserved2 = new String();

	/**
	 * strReserved
	 *
	 * 版本 >= 0
	 */
	 private String strReserved3 = new String();

	/**
	 * dwReserved
	 *
	 * 版本 >= 0
	 */
	 private long dwReserved1;

	/**
	 * dwReserved
	 *
	 * 版本 >= 0
	 */
	 private long dwReserved2;

	/**
	 * dwReserved
	 *
	 * 版本 >= 0
	 */
	 private long dwReserved3;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushString(videoLinker);
		bs.pushString(strReserved1);
		bs.pushString(strReserved2);
		bs.pushString(strReserved3);
		bs.pushUInt(dwReserved1);
		bs.pushUInt(dwReserved2);
		bs.pushUInt(dwReserved3);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		videoLinker = bs.popString();
		strReserved1 = bs.popString();
		strReserved2 = bs.popString();
		strReserved3 = bs.popString();
		dwReserved1 = bs.popUInt();
		dwReserved2 = bs.popUInt();
		dwReserved3 = bs.popUInt();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取视频连接地址，目前只有一个视频连接
	 * 
	 * 此字段的版本 >= 0
	 * @return videoLinker value 类型为:String
	 * 
	 */
	public String getVideoLinker()
	{
		return videoLinker;
	}


	/**
	 * 设置视频连接地址，目前只有一个视频连接
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setVideoLinker(String value)
	{
		this.videoLinker = value;
	}


	/**
	 * 获取strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return strReserved1 value 类型为:String
	 * 
	 */
	public String getStrReserved1()
	{
		return strReserved1;
	}


	/**
	 * 设置strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrReserved1(String value)
	{
		this.strReserved1 = value;
	}


	/**
	 * 获取strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return strReserved2 value 类型为:String
	 * 
	 */
	public String getStrReserved2()
	{
		return strReserved2;
	}


	/**
	 * 设置strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrReserved2(String value)
	{
		this.strReserved2 = value;
	}


	/**
	 * 获取strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return strReserved3 value 类型为:String
	 * 
	 */
	public String getStrReserved3()
	{
		return strReserved3;
	}


	/**
	 * 设置strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrReserved3(String value)
	{
		this.strReserved3 = value;
	}


	/**
	 * 获取dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return dwReserved1 value 类型为:long
	 * 
	 */
	public long getDwReserved1()
	{
		return dwReserved1;
	}


	/**
	 * 设置dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwReserved1(long value)
	{
		this.dwReserved1 = value;
	}


	/**
	 * 获取dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return dwReserved2 value 类型为:long
	 * 
	 */
	public long getDwReserved2()
	{
		return dwReserved2;
	}


	/**
	 * 设置dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwReserved2(long value)
	{
		this.dwReserved2 = value;
	}


	/**
	 * 获取dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return dwReserved3 value 类型为:long
	 * 
	 */
	public long getDwReserved3()
	{
		return dwReserved3;
	}


	/**
	 * 设置dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwReserved3(long value)
	{
		this.dwReserved3 = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemVideo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(videoLinker);  //计算字段videoLinker的长度 size_of(String)
				length += ByteStream.getObjectSize(strReserved1);  //计算字段strReserved1的长度 size_of(String)
				length += ByteStream.getObjectSize(strReserved2);  //计算字段strReserved2的长度 size_of(String)
				length += ByteStream.getObjectSize(strReserved3);  //计算字段strReserved3的长度 size_of(String)
				length += 4;  //计算字段dwReserved1的长度 size_of(uint32_t)
				length += 4;  //计算字段dwReserved2的长度 size_of(uint32_t)
				length += 4;  //计算字段dwReserved3的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
