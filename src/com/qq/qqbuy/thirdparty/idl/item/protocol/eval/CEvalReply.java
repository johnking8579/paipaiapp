//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.EvalIdl.java

package com.qq.qqbuy.thirdparty.idl.item.protocol.eval;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *评价回复po
 *
 *@date 2012-12-17 10:47:50
 *
 *@since version:0
*/
public class CEvalReply  implements ICanSerializeObject
{
	/**
	 * 回复者QQ号
	 *
	 * 版本 >= 0
	 */
	 private long Uin;

	/**
	 * 订单id-int型
	 *
	 * 版本 >= 0
	 */
	 private long FDealId;

	/**
	 * 创建时间
	 *
	 * 版本 >= 0
	 */
	 private long CreateTime;

	/**
	 * 订单id-str型
	 *
	 * 版本 >= 0
	 */
	 private String DealId = new String();

	/**
	 * 内容
	 *
	 * 版本 >= 0
	 */
	 private String Content = new String();

	/**
	 * id
	 *
	 * 版本 >= 0
	 */
	 private long Id;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Uin);
		bs.pushUInt(FDealId);
		bs.pushUInt(CreateTime);
		bs.pushString(DealId);
		bs.pushString(Content);
		bs.pushUInt(Id);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Uin = bs.popUInt();
		FDealId = bs.popUInt();
		CreateTime = bs.popUInt();
		DealId = bs.popString();
		Content = bs.popString();
		Id = bs.popUInt();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取回复者QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return Uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return Uin;
	}


	/**
	 * 设置回复者QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.Uin = value;
	}


	/**
	 * 获取订单id-int型
	 * 
	 * 此字段的版本 >= 0
	 * @return FDealId value 类型为:long
	 * 
	 */
	public long getFDealId()
	{
		return FDealId;
	}


	/**
	 * 设置订单id-int型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFDealId(long value)
	{
		this.FDealId = value;
	}


	/**
	 * 获取创建时间
	 * 
	 * 此字段的版本 >= 0
	 * @return CreateTime value 类型为:long
	 * 
	 */
	public long getCreateTime()
	{
		return CreateTime;
	}


	/**
	 * 设置创建时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCreateTime(long value)
	{
		this.CreateTime = value;
	}


	/**
	 * 获取订单id-str型
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:String
	 * 
	 */
	public String getDealId()
	{
		return DealId;
	}


	/**
	 * 设置订单id-str型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealId(String value)
	{
		this.DealId = value;
	}


	/**
	 * 获取内容
	 * 
	 * 此字段的版本 >= 0
	 * @return Content value 类型为:String
	 * 
	 */
	public String getContent()
	{
		return Content;
	}


	/**
	 * 设置内容
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setContent(String value)
	{
		this.Content = value;
	}


	/**
	 * 获取id
	 * 
	 * 此字段的版本 >= 0
	 * @return Id value 类型为:long
	 * 
	 */
	public long getId()
	{
		return Id;
	}


	/**
	 * 设置id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setId(long value)
	{
		this.Id = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CEvalReply)
				length += 4;  //计算字段Uin的长度 size_of(uint32_t)
				length += 4;  //计算字段FDealId的长度 size_of(uint32_t)
				length += 4;  //计算字段CreateTime的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(DealId);  //计算字段DealId的长度 size_of(String)
				length += ByteStream.getObjectSize(Content);  //计算字段Content的长度 size_of(String)
				length += 4;  //计算字段Id的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
