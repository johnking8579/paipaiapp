//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.item.po.idl.ItemPoV2.java

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import java.util.Vector;

/**
 *商品图片
 *
 *@date 2013-02-27 05:06:02
 *
 *@since version:0
*/
public class ItemImg  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 0;

	/**
	 * 图片Vector，注：第0张是主图，最多5张图，内容为图片的完整链接，但是如果需要不同的分辨率的图片，请自行拼装
	 *
	 * 版本 >= 0
	 */
	 private Vector<String> vecImg = new Vector<String>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushObject(vecImg);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		vecImg = (Vector<String>)bs.popVector(String.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取图片Vector，注：第0张是主图，最多5张图，内容为图片的完整链接，但是如果需要不同的分辨率的图片，请自行拼装
	 * 
	 * 此字段的版本 >= 0
	 * @return vecImg value 类型为:Vector<String>
	 * 
	 */
	public Vector<String> getVecImg()
	{
		return vecImg;
	}


	/**
	 * 设置图片Vector，注：第0张是主图，最多5张图，内容为图片的完整链接，但是如果需要不同的分辨率的图片，请自行拼装
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<String>
	 * 
	 */
	public void setVecImg(Vector<String> value)
	{
		if (value != null) {
				this.vecImg = value;
		}else{
				this.vecImg = new Vector<String>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemImg)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(vecImg);  //计算字段vecImg的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
