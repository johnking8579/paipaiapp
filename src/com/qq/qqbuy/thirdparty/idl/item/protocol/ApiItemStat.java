 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *商品统计信息
 *
 *@date 2012-09-02 10:38::24
 *
 *@since version:0
*/
public class ApiItemStat  implements ICanSerializeObject
{
	/**
	 * 商品第一次发布时间
	 *
	 * 版本 >= 0
	 */
	 private long addTime;

	/**
	 * 商品编辑时间
	 *
	 * 版本 >= 0
	 */
	 private long editTime;

	/**
	 * 商品最后发布时间
	 *
	 * 版本 >= 0
	 */
	 private long uploadTime;

	/**
	 * 商品最后下架时间
	 *
	 * 版本 >= 0
	 */
	 private long downloadTime;

	/**
	 * 商品好评数
	 *
	 * 版本 >= 0
	 */
	 private long goodRateNum;

	/**
	 * 付款数
	 *
	 * 版本 >= 0
	 */
	 private long payNum;

	/**
	 * 全部付款数
	 *
	 * 版本 >= 0
	 */
	 private long totalPayNum;

	/**
	 * 付款次数
	 *
	 * 版本 >= 0
	 */
	 private long payCount;

	/**
	 * 总的付款次数
	 *
	 * 版本 >= 0
	 */
	 private long totalPayCount;

	/**
	 * 最后付款时间
	 *
	 * 版本 >= 0
	 */
	 private long lastPayTime;

	/**
	 * 下单数
	 *
	 * 版本 >= 0
	 */
	 private long buyNum;

	/**
	 * 下单总数
	 *
	 * 版本 >= 0
	 */
	 private long toalBuyNum;

	/**
	 * 下单次数
	 *
	 * 版本 >= 0
	 */
	 private long buyCount;

	/**
	 * 总的下单次数
	 *
	 * 版本 >= 0
	 */
	 private long totalBuyCount;

	/**
	 * 最后下单时间
	 *
	 * 版本 >= 0
	 */
	 private long lastBuyTime;

	/**
	 * 发布时ip地址
	 *
	 * 版本 >= 0
	 */
	 private long addIp;

	/**
	 * 编辑时ip地址
	 *
	 * 版本 >= 0
	 */
	 private long editIp;

	/**
	 * 浏览次数
	 *
	 * 版本 >= 0
	 */
	 private long visitCount;

	/**
	 * 市场价
	 *
	 * 版本 >= 0
	 */
	 private long marketPrice;

	/**
	 * 最后编辑时间
	 *
	 * 版本 >= 0
	 */
	 private long lastModified;

	/**
	 * 发布来源
	 *
	 * 版本 >= 0
	 */
	 private String addSource = new String();

	/**
	 * 编辑来源
	 *
	 * 版本 >= 0
	 */
	 private String editSource = new String();

	/**
	 * 版本 >= 0
	 */
	 private short addTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short editTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short uploadTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short downloadTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short goodRateNum_u;

	/**
	 * 版本 >= 0
	 */
	 private short payNum_u;

	/**
	 * 版本 >= 0
	 */
	 private short totalPayNum_u;

	/**
	 * 版本 >= 0
	 */
	 private short payCount_u;

	/**
	 * 版本 >= 0
	 */
	 private short totalPayCount_u;

	/**
	 * 版本 >= 0
	 */
	 private short lastPayTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short buyNum_u;

	/**
	 * 版本 >= 0
	 */
	 private short toalBuyNum_u;

	/**
	 * 版本 >= 0
	 */
	 private short buyCount_u;

	/**
	 * 版本 >= 0
	 */
	 private short totalBuyCount_u;

	/**
	 * 版本 >= 0
	 */
	 private short lastBuyTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short addIp_u;

	/**
	 * 版本 >= 0
	 */
	 private short editIp_u;

	/**
	 * 版本 >= 0
	 */
	 private short visitCount_u;

	/**
	 * 版本 >= 0
	 */
	 private short marketPrice_u;

	/**
	 * 版本 >= 0
	 */
	 private short lastModified_u;

	/**
	 * 版本 >= 0
	 */
	 private short addSource_u;

	/**
	 * 版本 >= 0
	 */
	 private short editSource_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(addTime);
		bs.pushUInt(editTime);
		bs.pushUInt(uploadTime);
		bs.pushUInt(downloadTime);
		bs.pushUInt(goodRateNum);
		bs.pushUInt(payNum);
		bs.pushUInt(totalPayNum);
		bs.pushUInt(payCount);
		bs.pushUInt(totalPayCount);
		bs.pushUInt(lastPayTime);
		bs.pushUInt(buyNum);
		bs.pushUInt(toalBuyNum);
		bs.pushUInt(buyCount);
		bs.pushUInt(totalBuyCount);
		bs.pushUInt(lastBuyTime);
		bs.pushUInt(addIp);
		bs.pushUInt(editIp);
		bs.pushUInt(visitCount);
		bs.pushUInt(marketPrice);
		bs.pushUInt(lastModified);
		bs.pushString(addSource);
		bs.pushString(editSource);
		bs.pushUByte(addTime_u);
		bs.pushUByte(editTime_u);
		bs.pushUByte(uploadTime_u);
		bs.pushUByte(downloadTime_u);
		bs.pushUByte(goodRateNum_u);
		bs.pushUByte(payNum_u);
		bs.pushUByte(totalPayNum_u);
		bs.pushUByte(payCount_u);
		bs.pushUByte(totalPayCount_u);
		bs.pushUByte(lastPayTime_u);
		bs.pushUByte(buyNum_u);
		bs.pushUByte(toalBuyNum_u);
		bs.pushUByte(buyCount_u);
		bs.pushUByte(totalBuyCount_u);
		bs.pushUByte(lastBuyTime_u);
		bs.pushUByte(addIp_u);
		bs.pushUByte(editIp_u);
		bs.pushUByte(visitCount_u);
		bs.pushUByte(marketPrice_u);
		bs.pushUByte(lastModified_u);
		bs.pushUByte(addSource_u);
		bs.pushUByte(editSource_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		addTime = bs.popUInt();
		editTime = bs.popUInt();
		uploadTime = bs.popUInt();
		downloadTime = bs.popUInt();
		goodRateNum = bs.popUInt();
		payNum = bs.popUInt();
		totalPayNum = bs.popUInt();
		payCount = bs.popUInt();
		totalPayCount = bs.popUInt();
		lastPayTime = bs.popUInt();
		buyNum = bs.popUInt();
		toalBuyNum = bs.popUInt();
		buyCount = bs.popUInt();
		totalBuyCount = bs.popUInt();
		lastBuyTime = bs.popUInt();
		addIp = bs.popUInt();
		editIp = bs.popUInt();
		visitCount = bs.popUInt();
		marketPrice = bs.popUInt();
		lastModified = bs.popUInt();
		addSource = bs.popString();
		editSource = bs.popString();
		addTime_u = bs.popUByte();
		editTime_u = bs.popUByte();
		uploadTime_u = bs.popUByte();
		downloadTime_u = bs.popUByte();
		goodRateNum_u = bs.popUByte();
		payNum_u = bs.popUByte();
		totalPayNum_u = bs.popUByte();
		payCount_u = bs.popUByte();
		totalPayCount_u = bs.popUByte();
		lastPayTime_u = bs.popUByte();
		buyNum_u = bs.popUByte();
		toalBuyNum_u = bs.popUByte();
		buyCount_u = bs.popUByte();
		totalBuyCount_u = bs.popUByte();
		lastBuyTime_u = bs.popUByte();
		addIp_u = bs.popUByte();
		editIp_u = bs.popUByte();
		visitCount_u = bs.popUByte();
		marketPrice_u = bs.popUByte();
		lastModified_u = bs.popUByte();
		addSource_u = bs.popUByte();
		editSource_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取商品第一次发布时间
	 * 
	 * 此字段的版本 >= 0
	 * @return addTime value 类型为:long
	 * 
	 */
	public long getAddTime()
	{
		return addTime;
	}


	/**
	 * 设置商品第一次发布时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddTime(long value)
	{
		this.addTime = value;
		this.addTime_u = 1;
	}


	/**
	 * 获取商品编辑时间
	 * 
	 * 此字段的版本 >= 0
	 * @return editTime value 类型为:long
	 * 
	 */
	public long getEditTime()
	{
		return editTime;
	}


	/**
	 * 设置商品编辑时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEditTime(long value)
	{
		this.editTime = value;
		this.editTime_u = 1;
	}


	/**
	 * 获取商品最后发布时间
	 * 
	 * 此字段的版本 >= 0
	 * @return uploadTime value 类型为:long
	 * 
	 */
	public long getUploadTime()
	{
		return uploadTime;
	}


	/**
	 * 设置商品最后发布时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUploadTime(long value)
	{
		this.uploadTime = value;
		this.uploadTime_u = 1;
	}


	/**
	 * 获取商品最后下架时间
	 * 
	 * 此字段的版本 >= 0
	 * @return downloadTime value 类型为:long
	 * 
	 */
	public long getDownloadTime()
	{
		return downloadTime;
	}


	/**
	 * 设置商品最后下架时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDownloadTime(long value)
	{
		this.downloadTime = value;
		this.downloadTime_u = 1;
	}


	/**
	 * 获取商品好评数
	 * 
	 * 此字段的版本 >= 0
	 * @return goodRateNum value 类型为:long
	 * 
	 */
	public long getGoodRateNum()
	{
		return goodRateNum;
	}


	/**
	 * 设置商品好评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setGoodRateNum(long value)
	{
		this.goodRateNum = value;
		this.goodRateNum_u = 1;
	}


	/**
	 * 获取付款数
	 * 
	 * 此字段的版本 >= 0
	 * @return payNum value 类型为:long
	 * 
	 */
	public long getPayNum()
	{
		return payNum;
	}


	/**
	 * 设置付款数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayNum(long value)
	{
		this.payNum = value;
		this.payNum_u = 1;
	}


	/**
	 * 获取全部付款数
	 * 
	 * 此字段的版本 >= 0
	 * @return totalPayNum value 类型为:long
	 * 
	 */
	public long getTotalPayNum()
	{
		return totalPayNum;
	}


	/**
	 * 设置全部付款数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalPayNum(long value)
	{
		this.totalPayNum = value;
		this.totalPayNum_u = 1;
	}


	/**
	 * 获取付款次数
	 * 
	 * 此字段的版本 >= 0
	 * @return payCount value 类型为:long
	 * 
	 */
	public long getPayCount()
	{
		return payCount;
	}


	/**
	 * 设置付款次数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayCount(long value)
	{
		this.payCount = value;
		this.payCount_u = 1;
	}


	/**
	 * 获取总的付款次数
	 * 
	 * 此字段的版本 >= 0
	 * @return totalPayCount value 类型为:long
	 * 
	 */
	public long getTotalPayCount()
	{
		return totalPayCount;
	}


	/**
	 * 设置总的付款次数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalPayCount(long value)
	{
		this.totalPayCount = value;
		this.totalPayCount_u = 1;
	}


	/**
	 * 获取最后付款时间
	 * 
	 * 此字段的版本 >= 0
	 * @return lastPayTime value 类型为:long
	 * 
	 */
	public long getLastPayTime()
	{
		return lastPayTime;
	}


	/**
	 * 设置最后付款时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastPayTime(long value)
	{
		this.lastPayTime = value;
		this.lastPayTime_u = 1;
	}


	/**
	 * 获取下单数
	 * 
	 * 此字段的版本 >= 0
	 * @return buyNum value 类型为:long
	 * 
	 */
	public long getBuyNum()
	{
		return buyNum;
	}


	/**
	 * 设置下单数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyNum(long value)
	{
		this.buyNum = value;
		this.buyNum_u = 1;
	}


	/**
	 * 获取下单总数
	 * 
	 * 此字段的版本 >= 0
	 * @return toalBuyNum value 类型为:long
	 * 
	 */
	public long getToalBuyNum()
	{
		return toalBuyNum;
	}


	/**
	 * 设置下单总数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setToalBuyNum(long value)
	{
		this.toalBuyNum = value;
		this.toalBuyNum_u = 1;
	}


	/**
	 * 获取下单次数
	 * 
	 * 此字段的版本 >= 0
	 * @return buyCount value 类型为:long
	 * 
	 */
	public long getBuyCount()
	{
		return buyCount;
	}


	/**
	 * 设置下单次数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyCount(long value)
	{
		this.buyCount = value;
		this.buyCount_u = 1;
	}


	/**
	 * 获取总的下单次数
	 * 
	 * 此字段的版本 >= 0
	 * @return totalBuyCount value 类型为:long
	 * 
	 */
	public long getTotalBuyCount()
	{
		return totalBuyCount;
	}


	/**
	 * 设置总的下单次数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalBuyCount(long value)
	{
		this.totalBuyCount = value;
		this.totalBuyCount_u = 1;
	}


	/**
	 * 获取最后下单时间
	 * 
	 * 此字段的版本 >= 0
	 * @return lastBuyTime value 类型为:long
	 * 
	 */
	public long getLastBuyTime()
	{
		return lastBuyTime;
	}


	/**
	 * 设置最后下单时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastBuyTime(long value)
	{
		this.lastBuyTime = value;
		this.lastBuyTime_u = 1;
	}


	/**
	 * 获取发布时ip地址
	 * 
	 * 此字段的版本 >= 0
	 * @return addIp value 类型为:long
	 * 
	 */
	public long getAddIp()
	{
		return addIp;
	}


	/**
	 * 设置发布时ip地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddIp(long value)
	{
		this.addIp = value;
		this.addIp_u = 1;
	}


	/**
	 * 获取编辑时ip地址
	 * 
	 * 此字段的版本 >= 0
	 * @return editIp value 类型为:long
	 * 
	 */
	public long getEditIp()
	{
		return editIp;
	}


	/**
	 * 设置编辑时ip地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEditIp(long value)
	{
		this.editIp = value;
		this.editIp_u = 1;
	}


	/**
	 * 获取浏览次数
	 * 
	 * 此字段的版本 >= 0
	 * @return visitCount value 类型为:long
	 * 
	 */
	public long getVisitCount()
	{
		return visitCount;
	}


	/**
	 * 设置浏览次数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVisitCount(long value)
	{
		this.visitCount = value;
		this.visitCount_u = 1;
	}


	/**
	 * 获取市场价
	 * 
	 * 此字段的版本 >= 0
	 * @return marketPrice value 类型为:long
	 * 
	 */
	public long getMarketPrice()
	{
		return marketPrice;
	}


	/**
	 * 设置市场价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMarketPrice(long value)
	{
		this.marketPrice = value;
		this.marketPrice_u = 1;
	}


	/**
	 * 获取最后编辑时间
	 * 
	 * 此字段的版本 >= 0
	 * @return lastModified value 类型为:long
	 * 
	 */
	public long getLastModified()
	{
		return lastModified;
	}


	/**
	 * 设置最后编辑时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastModified(long value)
	{
		this.lastModified = value;
		this.lastModified_u = 1;
	}


	/**
	 * 获取发布来源
	 * 
	 * 此字段的版本 >= 0
	 * @return addSource value 类型为:String
	 * 
	 */
	public String getAddSource()
	{
		return addSource;
	}


	/**
	 * 设置发布来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAddSource(String value)
	{
		if (value != null) {
				this.addSource = value;
				this.addSource_u = 1;
		}
	}


	/**
	 * 获取编辑来源
	 * 
	 * 此字段的版本 >= 0
	 * @return editSource value 类型为:String
	 * 
	 */
	public String getEditSource()
	{
		return editSource;
	}


	/**
	 * 设置编辑来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setEditSource(String value)
	{
		if (value != null) {
				this.editSource = value;
				this.editSource_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return addTime_u value 类型为:short
	 * 
	 */
	public short getAddTime_u()
	{
		return addTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddTime_u(short value)
	{
		this.addTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return editTime_u value 类型为:short
	 * 
	 */
	public short getEditTime_u()
	{
		return editTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEditTime_u(short value)
	{
		this.editTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return uploadTime_u value 类型为:short
	 * 
	 */
	public short getUploadTime_u()
	{
		return uploadTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUploadTime_u(short value)
	{
		this.uploadTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return downloadTime_u value 类型为:short
	 * 
	 */
	public short getDownloadTime_u()
	{
		return downloadTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDownloadTime_u(short value)
	{
		this.downloadTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return goodRateNum_u value 类型为:short
	 * 
	 */
	public short getGoodRateNum_u()
	{
		return goodRateNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setGoodRateNum_u(short value)
	{
		this.goodRateNum_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return payNum_u value 类型为:short
	 * 
	 */
	public short getPayNum_u()
	{
		return payNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPayNum_u(short value)
	{
		this.payNum_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return totalPayNum_u value 类型为:short
	 * 
	 */
	public short getTotalPayNum_u()
	{
		return totalPayNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTotalPayNum_u(short value)
	{
		this.totalPayNum_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return payCount_u value 类型为:short
	 * 
	 */
	public short getPayCount_u()
	{
		return payCount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPayCount_u(short value)
	{
		this.payCount_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return totalPayCount_u value 类型为:short
	 * 
	 */
	public short getTotalPayCount_u()
	{
		return totalPayCount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTotalPayCount_u(short value)
	{
		this.totalPayCount_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return lastPayTime_u value 类型为:short
	 * 
	 */
	public short getLastPayTime_u()
	{
		return lastPayTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastPayTime_u(short value)
	{
		this.lastPayTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return buyNum_u value 类型为:short
	 * 
	 */
	public short getBuyNum_u()
	{
		return buyNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyNum_u(short value)
	{
		this.buyNum_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return toalBuyNum_u value 类型为:short
	 * 
	 */
	public short getToalBuyNum_u()
	{
		return toalBuyNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setToalBuyNum_u(short value)
	{
		this.toalBuyNum_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return buyCount_u value 类型为:short
	 * 
	 */
	public short getBuyCount_u()
	{
		return buyCount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyCount_u(short value)
	{
		this.buyCount_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return totalBuyCount_u value 类型为:short
	 * 
	 */
	public short getTotalBuyCount_u()
	{
		return totalBuyCount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTotalBuyCount_u(short value)
	{
		this.totalBuyCount_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return lastBuyTime_u value 类型为:short
	 * 
	 */
	public short getLastBuyTime_u()
	{
		return lastBuyTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastBuyTime_u(short value)
	{
		this.lastBuyTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return addIp_u value 类型为:short
	 * 
	 */
	public short getAddIp_u()
	{
		return addIp_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddIp_u(short value)
	{
		this.addIp_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return editIp_u value 类型为:short
	 * 
	 */
	public short getEditIp_u()
	{
		return editIp_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEditIp_u(short value)
	{
		this.editIp_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return visitCount_u value 类型为:short
	 * 
	 */
	public short getVisitCount_u()
	{
		return visitCount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVisitCount_u(short value)
	{
		this.visitCount_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return marketPrice_u value 类型为:short
	 * 
	 */
	public short getMarketPrice_u()
	{
		return marketPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMarketPrice_u(short value)
	{
		this.marketPrice_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return lastModified_u value 类型为:short
	 * 
	 */
	public short getLastModified_u()
	{
		return lastModified_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastModified_u(short value)
	{
		this.lastModified_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return addSource_u value 类型为:short
	 * 
	 */
	public short getAddSource_u()
	{
		return addSource_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddSource_u(short value)
	{
		this.addSource_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return editSource_u value 类型为:short
	 * 
	 */
	public short getEditSource_u()
	{
		return editSource_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEditSource_u(short value)
	{
		this.editSource_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ApiItemStat)
				length += 4;  //计算字段addTime的长度 size_of(uint32_t)
				length += 4;  //计算字段editTime的长度 size_of(uint32_t)
				length += 4;  //计算字段uploadTime的长度 size_of(uint32_t)
				length += 4;  //计算字段downloadTime的长度 size_of(uint32_t)
				length += 4;  //计算字段goodRateNum的长度 size_of(uint32_t)
				length += 4;  //计算字段payNum的长度 size_of(uint32_t)
				length += 4;  //计算字段totalPayNum的长度 size_of(uint32_t)
				length += 4;  //计算字段payCount的长度 size_of(uint32_t)
				length += 4;  //计算字段totalPayCount的长度 size_of(uint32_t)
				length += 4;  //计算字段lastPayTime的长度 size_of(uint32_t)
				length += 4;  //计算字段buyNum的长度 size_of(uint32_t)
				length += 4;  //计算字段toalBuyNum的长度 size_of(uint32_t)
				length += 4;  //计算字段buyCount的长度 size_of(uint32_t)
				length += 4;  //计算字段totalBuyCount的长度 size_of(uint32_t)
				length += 4;  //计算字段lastBuyTime的长度 size_of(uint32_t)
				length += 4;  //计算字段addIp的长度 size_of(uint32_t)
				length += 4;  //计算字段editIp的长度 size_of(uint32_t)
				length += 4;  //计算字段visitCount的长度 size_of(uint32_t)
				length += 4;  //计算字段marketPrice的长度 size_of(uint32_t)
				length += 4;  //计算字段lastModified的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(addSource);  //计算字段addSource的长度 size_of(String)
				length += ByteStream.getObjectSize(editSource);  //计算字段editSource的长度 size_of(String)
				length += 1;  //计算字段addTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段editTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段uploadTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段downloadTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段goodRateNum_u的长度 size_of(uint8_t)
				length += 1;  //计算字段payNum_u的长度 size_of(uint8_t)
				length += 1;  //计算字段totalPayNum_u的长度 size_of(uint8_t)
				length += 1;  //计算字段payCount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段totalPayCount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段lastPayTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段buyNum_u的长度 size_of(uint8_t)
				length += 1;  //计算字段toalBuyNum_u的长度 size_of(uint8_t)
				length += 1;  //计算字段buyCount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段totalBuyCount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段lastBuyTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段addIp_u的长度 size_of(uint8_t)
				length += 1;  //计算字段editIp_u的长度 size_of(uint8_t)
				length += 1;  //计算字段visitCount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段marketPrice_u的长度 size_of(uint8_t)
				length += 1;  //计算字段lastModified_u的长度 size_of(uint8_t)
				length += 1;  //计算字段addSource_u的长度 size_of(uint8_t)
				length += 1;  //计算字段editSource_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
