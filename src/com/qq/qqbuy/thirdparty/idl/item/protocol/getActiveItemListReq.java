 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.portal.ao.idl.MartAo.java

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *卖场快车活动曝光接口请求参数
 *
 *@date 2014-09-04 05:10:45
 *
 *@since version:0
*/
public class  getActiveItemListReq extends NetMessage
{
	/**
	 * 来源，传客户端IP或者__FILE__，必须
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 传用户的VisitKey，必须
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 场景ID，必须
	 *
	 * 版本 >= 0
	 */
	 private long sceneId;

	/**
	 * 鉴权Token
	 *
	 * 版本 >= 0
	 */
	 private String token = new String();

	/**
	 * 用户信息
	 *
	 * 版本 >= 0
	 */
	 private UserInfo userInfo = new UserInfo();

	/**
	 * 活动信息
	 *
	 * 版本 >= 0
	 */
	 private MartActive actives = new MartActive();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(source);
		bs.pushString(machineKey);
		bs.pushUInt(sceneId);
		bs.pushString(token);
		bs.pushObject(userInfo);
		bs.pushObject(actives);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		source = bs.popString();
		machineKey = bs.popString();
		sceneId = bs.popUInt();
		token = bs.popString();
		userInfo = (UserInfo)bs.popObject(UserInfo.class);
		actives = (MartActive)bs.popObject(MartActive.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x87481001L;
	}


	/**
	 * 获取来源，传客户端IP或者__FILE__，必须
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置来源，传客户端IP或者__FILE__，必须
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取传用户的VisitKey，必须
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置传用户的VisitKey，必须
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取场景ID，必须
	 * 
	 * 此字段的版本 >= 0
	 * @return sceneId value 类型为:long
	 * 
	 */
	public long getSceneId()
	{
		return sceneId;
	}


	/**
	 * 设置场景ID，必须
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSceneId(long value)
	{
		this.sceneId = value;
	}


	/**
	 * 获取鉴权Token
	 * 
	 * 此字段的版本 >= 0
	 * @return token value 类型为:String
	 * 
	 */
	public String getToken()
	{
		return token;
	}


	/**
	 * 设置鉴权Token
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setToken(String value)
	{
		this.token = value;
	}


	/**
	 * 获取用户信息
	 * 
	 * 此字段的版本 >= 0
	 * @return userInfo value 类型为:UserInfo
	 * 
	 */
	public UserInfo getUserInfo()
	{
		return userInfo;
	}


	/**
	 * 设置用户信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:UserInfo
	 * 
	 */
	public void setUserInfo(UserInfo value)
	{
		if (value != null) {
				this.userInfo = value;
		}else{
				this.userInfo = new UserInfo();
		}
	}


	/**
	 * 获取活动信息
	 * 
	 * 此字段的版本 >= 0
	 * @return actives value 类型为:MartActive
	 * 
	 */
	public MartActive getActives()
	{
		return actives;
	}


	/**
	 * 设置活动信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:MartActive
	 * 
	 */
	public void setActives(MartActive value)
	{
		if (value != null) {
				this.actives = value;
		}else{
				this.actives = new MartActive();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(getActiveItemListReq)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(machineKey);  //计算字段machineKey的长度 size_of(String)
				length += 4;  //计算字段sceneId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(token);  //计算字段token的长度 size_of(String)
				length += ByteStream.getObjectSize(userInfo);  //计算字段userInfo的长度 size_of(UserInfo)
				length += ByteStream.getObjectSize(actives);  //计算字段actives的长度 size_of(MartActive)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
