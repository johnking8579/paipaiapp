 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import java.util.Vector;

/**
 *商品信息
 *
 *@date 2011-11-15 11:14::15
 *
 *@since version:0
*/
public class MItemExtInfo  implements ICanSerializeObject, java.io.Serializable
{
	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private int version = 0; 

	/**
	 * 商品名称
	 *
	 * 版本 >= 0
	 */
	 private String itemName = new String();

	/**
	 * 关联商品
	 *
	 * 版本 >= 0
	 */
	 private String raleItems = new String();

	/**
	 * 商品图片
	 *
	 * 版本 >= 0
	 */
	 private Vector<MItemPic> itemPics = new Vector<MItemPic>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(version);
		bs.pushString(itemName);
		bs.pushString(raleItems);
		bs.pushObject(itemPics);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		itemName = bs.popString();
		raleItems = bs.popString();
		itemPics = (Vector<MItemPic>)bs.popVector(MItemPic.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @return itemName value 类型为:String
	 * 
	 */
	public String getItemName()
	{
		return itemName;
	}


	/**
	 * 设置商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemName(String value)
	{
		if (value != null) {
				this.itemName = value;
		}else{
				this.itemName = new String();
		}
	}


	/**
	 * 获取关联商品
	 * 
	 * 此字段的版本 >= 0
	 * @return raleItems value 类型为:String
	 * 
	 */
	public String getRaleItems()
	{
		return raleItems;
	}


	/**
	 * 设置关联商品
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRaleItems(String value)
	{
		if (value != null) {
				this.raleItems = value;
		}else{
				this.raleItems = new String();
		}
	}


	/**
	 * 获取商品图片
	 * 
	 * 此字段的版本 >= 0
	 * @return itemPics value 类型为:Vector<MItemPic>
	 * 
	 */
	public Vector<MItemPic> getItemPics()
	{
		return itemPics;
	}


	/**
	 * 设置商品图片
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<MItemPic>
	 * 
	 */
	public void setItemPics(Vector<MItemPic> value)
	{
		if (value != null) {
				this.itemPics = value;
		}else{
				this.itemPics = new Vector<MItemPic>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(MItemExtInfo)
				length += 4;  //计算字段version的长度 size_of(int)
				length += ByteStream.getObjectSize(itemName);  //计算字段itemName的长度 size_of(String)
				length += ByteStream.getObjectSize(raleItems);  //计算字段raleItems的长度 size_of(String)
				length += ByteStream.getObjectSize(itemPics);  //计算字段itemPics的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
