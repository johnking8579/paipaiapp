//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.EvalIdl.java

package com.qq.qqbuy.thirdparty.idl.item.protocol.eval;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *商品dsr
 *
 *@date 2012-12-17 10:47:50
 *
 *@since version:0
*/
public class ComdyStat  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 商品id
	 *
	 * 版本 >= 0
	 */
	 private long ComdyId;

	/**
	 * 版本 >= 0
	 */
	 private short ComdyId_u;

	/**
	 * 卖家号
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * 版本 >= 0
	 */
	 private short SellerUin_u;

	/**
	 * 好评数
	 *
	 * 版本 >= 0
	 */
	 private long GoodNum;

	/**
	 * 版本 >= 0
	 */
	 private short GoodNum_u;

	/**
	 * 中评数
	 *
	 * 版本 >= 0
	 */
	 private long NormalNum;

	/**
	 * 版本 >= 0
	 */
	 private short NormalNum_u;

	/**
	 * 差评数
	 *
	 * 版本 >= 0
	 */
	 private long BadNum;

	/**
	 * 版本 >= 0
	 */
	 private short BadNum_u;

	/**
	 * 有内容评价数
	 *
	 * 版本 >= 0
	 */
	 private long ContentNum;

	/**
	 * 版本 >= 0
	 */
	 private short ContentNum_u;

	/**
	 * 最后更新时间
	 *
	 * 版本 >= 0
	 */
	 private long LastUpdateTime;

	/**
	 * 版本 >= 0
	 */
	 private short LastUpdateTime_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(ComdyId);
		bs.pushUByte(ComdyId_u);
		bs.pushUInt(SellerUin);
		bs.pushUByte(SellerUin_u);
		bs.pushUInt(GoodNum);
		bs.pushUByte(GoodNum_u);
		bs.pushUInt(NormalNum);
		bs.pushUByte(NormalNum_u);
		bs.pushUInt(BadNum);
		bs.pushUByte(BadNum_u);
		bs.pushUInt(ContentNum);
		bs.pushUByte(ContentNum_u);
		bs.pushUInt(LastUpdateTime);
		bs.pushUByte(LastUpdateTime_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		ComdyId = bs.popUInt();
		ComdyId_u = bs.popUByte();
		SellerUin = bs.popUInt();
		SellerUin_u = bs.popUByte();
		GoodNum = bs.popUInt();
		GoodNum_u = bs.popUByte();
		NormalNum = bs.popUInt();
		NormalNum_u = bs.popUByte();
		BadNum = bs.popUInt();
		BadNum_u = bs.popUByte();
		ContentNum = bs.popUInt();
		ContentNum_u = bs.popUByte();
		LastUpdateTime = bs.popUInt();
		LastUpdateTime_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取商品id
	 * 
	 * 此字段的版本 >= 0
	 * @return ComdyId value 类型为:long
	 * 
	 */
	public long getComdyId()
	{
		return ComdyId;
	}


	/**
	 * 设置商品id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setComdyId(long value)
	{
		this.ComdyId = value;
		this.ComdyId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ComdyId_u value 类型为:short
	 * 
	 */
	public short getComdyId_u()
	{
		return ComdyId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setComdyId_u(short value)
	{
		this.ComdyId_u = value;
	}


	/**
	 * 获取卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
		this.SellerUin_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin_u value 类型为:short
	 * 
	 */
	public short getSellerUin_u()
	{
		return SellerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerUin_u(short value)
	{
		this.SellerUin_u = value;
	}


	/**
	 * 获取好评数
	 * 
	 * 此字段的版本 >= 0
	 * @return GoodNum value 类型为:long
	 * 
	 */
	public long getGoodNum()
	{
		return GoodNum;
	}


	/**
	 * 设置好评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setGoodNum(long value)
	{
		this.GoodNum = value;
		this.GoodNum_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return GoodNum_u value 类型为:short
	 * 
	 */
	public short getGoodNum_u()
	{
		return GoodNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setGoodNum_u(short value)
	{
		this.GoodNum_u = value;
	}


	/**
	 * 获取中评数
	 * 
	 * 此字段的版本 >= 0
	 * @return NormalNum value 类型为:long
	 * 
	 */
	public long getNormalNum()
	{
		return NormalNum;
	}


	/**
	 * 设置中评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNormalNum(long value)
	{
		this.NormalNum = value;
		this.NormalNum_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return NormalNum_u value 类型为:short
	 * 
	 */
	public short getNormalNum_u()
	{
		return NormalNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNormalNum_u(short value)
	{
		this.NormalNum_u = value;
	}


	/**
	 * 获取差评数
	 * 
	 * 此字段的版本 >= 0
	 * @return BadNum value 类型为:long
	 * 
	 */
	public long getBadNum()
	{
		return BadNum;
	}


	/**
	 * 设置差评数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBadNum(long value)
	{
		this.BadNum = value;
		this.BadNum_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BadNum_u value 类型为:short
	 * 
	 */
	public short getBadNum_u()
	{
		return BadNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBadNum_u(short value)
	{
		this.BadNum_u = value;
	}


	/**
	 * 获取有内容评价数
	 * 
	 * 此字段的版本 >= 0
	 * @return ContentNum value 类型为:long
	 * 
	 */
	public long getContentNum()
	{
		return ContentNum;
	}


	/**
	 * 设置有内容评价数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setContentNum(long value)
	{
		this.ContentNum = value;
		this.ContentNum_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ContentNum_u value 类型为:short
	 * 
	 */
	public short getContentNum_u()
	{
		return ContentNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setContentNum_u(short value)
	{
		this.ContentNum_u = value;
	}


	/**
	 * 获取最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return LastUpdateTime value 类型为:long
	 * 
	 */
	public long getLastUpdateTime()
	{
		return LastUpdateTime;
	}


	/**
	 * 设置最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastUpdateTime(long value)
	{
		this.LastUpdateTime = value;
		this.LastUpdateTime_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return LastUpdateTime_u value 类型为:short
	 * 
	 */
	public short getLastUpdateTime_u()
	{
		return LastUpdateTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastUpdateTime_u(short value)
	{
		this.LastUpdateTime_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ComdyStat)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ComdyId的长度 size_of(uint32_t)
				length += 1;  //计算字段ComdyId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段GoodNum的长度 size_of(uint32_t)
				length += 1;  //计算字段GoodNum_u的长度 size_of(uint8_t)
				length += 4;  //计算字段NormalNum的长度 size_of(uint32_t)
				length += 1;  //计算字段NormalNum_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BadNum的长度 size_of(uint32_t)
				length += 1;  //计算字段BadNum_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ContentNum的长度 size_of(uint32_t)
				length += 1;  //计算字段ContentNum_u的长度 size_of(uint8_t)
				length += 4;  //计算字段LastUpdateTime的长度 size_of(uint32_t)
				length += 1;  //计算字段LastUpdateTime_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
