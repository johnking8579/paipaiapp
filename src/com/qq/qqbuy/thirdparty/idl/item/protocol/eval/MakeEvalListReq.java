 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.EvalIdl.java

package com.qq.qqbuy.thirdparty.idl.item.protocol.eval;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Vector;

/**
 *做评请求
 *
 *@date 2012-12-17 10:47:50
 *
 *@since version:0
*/
public class  MakeEvalListReq implements IServiceObject
{
	/**
	 * 调用者==>请设置为源文件名
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 待做评列表，大订单列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<String> DealIdList = new Vector<String>();

	/**
	 * 评价等级
	 *
	 * 版本 >= 0
	 */
	 private long Level;

	/**
	 * 评价内容
	 *
	 * 版本 >= 0
	 */
	 private String Content = new String();

	/**
	 * sceneid
	 *
	 * 版本 >= 0
	 */
	 private String SceneId = new String();

	/**
	 * 用户MachineKey
	 *
	 * 版本 >= 0
	 */
	 private String MachineKey = new String();

	/**
	 * 保留输入字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveIn = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(Source);
		bs.pushObject(DealIdList);
		bs.pushUInt(Level);
		bs.pushString(Content);
		bs.pushString(SceneId);
		bs.pushString(MachineKey);
		bs.pushString(ReserveIn);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		Source = bs.popString();
		DealIdList = (Vector<String>)bs.popVector(String.class);
		Level = bs.popUInt();
		Content = bs.popString();
		SceneId = bs.popString();
		MachineKey = bs.popString();
		ReserveIn = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x23151801L;
	}


	/**
	 * 获取调用者==>请设置为源文件名
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置调用者==>请设置为源文件名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取待做评列表，大订单列表
	 * 
	 * 此字段的版本 >= 0
	 * @return DealIdList value 类型为:Vector<String>
	 * 
	 */
	public Vector<String> getDealIdList()
	{
		return DealIdList;
	}


	/**
	 * 设置待做评列表，大订单列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<String>
	 * 
	 */
	public void setDealIdList(Vector<String> value)
	{
		if (value != null) {
				this.DealIdList = value;
		}else{
				this.DealIdList = new Vector<String>();
		}
	}


	/**
	 * 获取评价等级
	 * 
	 * 此字段的版本 >= 0
	 * @return Level value 类型为:long
	 * 
	 */
	public long getLevel()
	{
		return Level;
	}


	/**
	 * 设置评价等级
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLevel(long value)
	{
		this.Level = value;
	}


	/**
	 * 获取评价内容
	 * 
	 * 此字段的版本 >= 0
	 * @return Content value 类型为:String
	 * 
	 */
	public String getContent()
	{
		return Content;
	}


	/**
	 * 设置评价内容
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setContent(String value)
	{
		this.Content = value;
	}


	/**
	 * 获取sceneid
	 * 
	 * 此字段的版本 >= 0
	 * @return SceneId value 类型为:String
	 * 
	 */
	public String getSceneId()
	{
		return SceneId;
	}


	/**
	 * 设置sceneid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSceneId(String value)
	{
		this.SceneId = value;
	}


	/**
	 * 获取用户MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @return MachineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return MachineKey;
	}


	/**
	 * 设置用户MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.MachineKey = value;
	}


	/**
	 * 获取保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveIn value 类型为:String
	 * 
	 */
	public String getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveIn(String value)
	{
		this.ReserveIn = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(MakeEvalListReq)
				length += ByteStream.getObjectSize(Source);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(DealIdList);  //计算字段DealIdList的长度 size_of(Vector)
				length += 4;  //计算字段Level的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(Content);  //计算字段Content的长度 size_of(String)
				length += ByteStream.getObjectSize(SceneId);  //计算字段SceneId的长度 size_of(String)
				length += ByteStream.getObjectSize(MachineKey);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveIn);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
