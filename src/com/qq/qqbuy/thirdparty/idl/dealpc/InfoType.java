package com.qq.qqbuy.thirdparty.idl.dealpc;

/**
 *
	enum DaoTradeInfoType_E
	{
		DAO_TRADE_INFO_DEAL = 0x01,//大订单表
		DAO_TRADE_INFO_TRADE_ALL = 0x02,//全部子订单
		DAO_TRADE_INFO_TRADE_SINGLE = 0x04,//单个子订单
		DAO_TRADE_INFO_TRADE_EXT = 0x08,//子订单扩展信息
		DAO_TRADE_INFO_ACCESSORY = 0x10,//配件表
		DAO_TRADE_INFO_ACCOUNT_ALL = 0x20,//支付单表全部
		DAO_TRADE_INFO_ACCOUNT_SINGLE = 0x40,//支付单表单条
		DAO_TRADE_INFO_SHIPPING = 0x80,//物流表
		DAO_TRADE_INFO_REFUND = 0x100,//退款表
		DAO_TRADE_INFO_REFUND_EXT = 0x200,//退款扩展信息
		DAO_TRADE_INFO_ACTIONLOG = 0x400,//流水信息
		DAO_TRADE_INFO_COD_PAY = 0x800,//货到付款支付信息(单条)
		DAO_TRADE_INFO_COD_PAY_ALL = 0x1000,//货到付款支付信息(全部)
	}
	业务调用方需要根据实际需要设置，尽量不要获取不需要的信息。如流水信息大多数业务都不需要。
	如果要取大订单和所有子订单信息，则dwInfoType = DAO_TRADE_INFO_DEAL | DAO_TRADE_INFO_TRADE_ALL

 * 
 * @author JingYing
 * @date 2015年1月5日
 */
public class InfoType {

	public static final int 
			DEAL = 0x01,// 大订单表
			TRADE_ALL = 0x02,// 全部子订单
			TRADE_SINGLE = 0x04,// 单个子订单
			TRADE_EXT = 0x08,// 子订单扩展信息
			ACCESSORY = 0x10,// 配件表
			ACCOUNT_ALL = 0x20,// 支付单表全部
			ACCOUNT_SINGLE = 0x40,// 支付单表单条
			SHIPPING = 0x80,// 物流表
			REFUND = 0x100,// 退款表
			REFUND_EXT = 0x200,// 退款扩展信息
			ACTIONLOG = 0x400,// 流水信息
			COD_PAY = 0x800,// 货到付款支付信息(单条)
			COD_PAY_ALL = 0x1000;// 货到付款支付信息(全部)
	
	/**
	 * 或运算. 
	 * @param condition 来自InfoType的公共成员变量
	 * @return
	 */
	public static int or(int...condition)	{
		int or = condition[0];
		for(int i=1; i<condition.length; i++)	{
			or = or | condition[i];
		}
		return or;
	}
	
	
	public static void main(String[] args) {
		System.out.println(InfoType.or(InfoType.DEAL, InfoType.TRADE_ALL, InfoType.REFUND));
		System.out.println(1|2|0x100);
	}
}
