//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.deelpc.protocol.DealIdl.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *物流信息
 *
 *@date 2015-03-04 11:46:40
 *
 *@since version:0
*/
public class CSendGoods  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 收货时间
	 *
	 * 版本 >= 0
	 */
	 private long RecvDays;

	/**
	 * 发货时间
	 *
	 * 版本 >= 0
	 */
	 private long SendTime;

	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * cardinfo
	 *
	 * 版本 >= 0
	 */
	 private Vector<String> vecCardInfo = new Vector<String>();

	/**
	 * 发货描述
	 *
	 * 版本 >= 0
	 */
	 private String WuliuDesc = new String();

	/**
	 * 物流公司
	 *
	 * 版本 >= 0
	 */
	 private String WuliuCompany = new String();

	/**
	 * 物流id
	 *
	 * 版本 >= 0
	 */
	 private String WuliuCode = new String();

	/**
	 * 取货地
	 *
	 * 版本 >= 0
	 */
	 private String GetItemAddr = new String();

	/**
	 * 物流方式
	 *
	 * 版本 >= 0
	 */
	 private String WuliuType = new String();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String Reserve = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(RecvDays);
		bs.pushUInt(SendTime);
		bs.pushUInt(version);
		bs.pushObject(vecCardInfo);
		bs.pushString(WuliuDesc);
		bs.pushString(WuliuCompany);
		bs.pushString(WuliuCode);
		bs.pushString(GetItemAddr);
		bs.pushString(WuliuType);
		bs.pushString(Reserve);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		RecvDays = bs.popUInt();
		SendTime = bs.popUInt();
		version = bs.popUInt();
		vecCardInfo = (Vector<String>)bs.popVector(String.class);
		WuliuDesc = bs.popString();
		WuliuCompany = bs.popString();
		WuliuCode = bs.popString();
		GetItemAddr = bs.popString();
		WuliuType = bs.popString();
		Reserve = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取收货时间
	 * 
	 * 此字段的版本 >= 0
	 * @return RecvDays value 类型为:long
	 * 
	 */
	public long getRecvDays()
	{
		return RecvDays;
	}


	/**
	 * 设置收货时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRecvDays(long value)
	{
		this.RecvDays = value;
	}


	/**
	 * 获取发货时间
	 * 
	 * 此字段的版本 >= 0
	 * @return SendTime value 类型为:long
	 * 
	 */
	public long getSendTime()
	{
		return SendTime;
	}


	/**
	 * 设置发货时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSendTime(long value)
	{
		this.SendTime = value;
	}


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取cardinfo
	 * 
	 * 此字段的版本 >= 0
	 * @return vecCardInfo value 类型为:Vector<String>
	 * 
	 */
	public Vector<String> getVecCardInfo()
	{
		return vecCardInfo;
	}


	/**
	 * 设置cardinfo
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<String>
	 * 
	 */
	public void setVecCardInfo(Vector<String> value)
	{
		if (value != null) {
				this.vecCardInfo = value;
		}else{
				this.vecCardInfo = new Vector<String>();
		}
	}


	/**
	 * 获取发货描述
	 * 
	 * 此字段的版本 >= 0
	 * @return WuliuDesc value 类型为:String
	 * 
	 */
	public String getWuliuDesc()
	{
		return WuliuDesc;
	}


	/**
	 * 设置发货描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setWuliuDesc(String value)
	{
		this.WuliuDesc = value;
	}


	/**
	 * 获取物流公司
	 * 
	 * 此字段的版本 >= 0
	 * @return WuliuCompany value 类型为:String
	 * 
	 */
	public String getWuliuCompany()
	{
		return WuliuCompany;
	}


	/**
	 * 设置物流公司
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setWuliuCompany(String value)
	{
		this.WuliuCompany = value;
	}


	/**
	 * 获取物流id
	 * 
	 * 此字段的版本 >= 0
	 * @return WuliuCode value 类型为:String
	 * 
	 */
	public String getWuliuCode()
	{
		return WuliuCode;
	}


	/**
	 * 设置物流id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setWuliuCode(String value)
	{
		this.WuliuCode = value;
	}


	/**
	 * 获取取货地
	 * 
	 * 此字段的版本 >= 0
	 * @return GetItemAddr value 类型为:String
	 * 
	 */
	public String getGetItemAddr()
	{
		return GetItemAddr;
	}


	/**
	 * 设置取货地
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setGetItemAddr(String value)
	{
		this.GetItemAddr = value;
	}


	/**
	 * 获取物流方式
	 * 
	 * 此字段的版本 >= 0
	 * @return WuliuType value 类型为:String
	 * 
	 */
	public String getWuliuType()
	{
		return WuliuType;
	}


	/**
	 * 设置物流方式
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setWuliuType(String value)
	{
		this.WuliuType = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return Reserve value 类型为:String
	 * 
	 */
	public String getReserve()
	{
		return Reserve;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserve(String value)
	{
		this.Reserve = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CSendGoods)
				length += 4;  //计算字段RecvDays的长度 size_of(uint32_t)
				length += 4;  //计算字段SendTime的长度 size_of(uint32_t)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(vecCardInfo, null);  //计算字段vecCardInfo的长度 size_of(Vector)
				length += ByteStream.getObjectSize(WuliuDesc, null);  //计算字段WuliuDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(WuliuCompany, null);  //计算字段WuliuCompany的长度 size_of(String)
				length += ByteStream.getObjectSize(WuliuCode, null);  //计算字段WuliuCode的长度 size_of(String)
				length += ByteStream.getObjectSize(GetItemAddr, null);  //计算字段GetItemAddr的长度 size_of(String)
				length += ByteStream.getObjectSize(WuliuType, null);  //计算字段WuliuType的长度 size_of(String)
				length += ByteStream.getObjectSize(Reserve, null);  //计算字段Reserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(CSendGoods)
				length += 4;  //计算字段RecvDays的长度 size_of(uint32_t)
				length += 4;  //计算字段SendTime的长度 size_of(uint32_t)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(vecCardInfo, encoding);  //计算字段vecCardInfo的长度 size_of(Vector)
				length += ByteStream.getObjectSize(WuliuDesc, encoding);  //计算字段WuliuDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(WuliuCompany, encoding);  //计算字段WuliuCompany的长度 size_of(String)
				length += ByteStream.getObjectSize(WuliuCode, encoding);  //计算字段WuliuCode的长度 size_of(String)
				length += ByteStream.getObjectSize(GetItemAddr, encoding);  //计算字段GetItemAddr的长度 size_of(String)
				length += ByteStream.getObjectSize(WuliuType, encoding);  //计算字段WuliuType的长度 size_of(String)
				length += ByteStream.getObjectSize(Reserve, encoding);  //计算字段Reserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
