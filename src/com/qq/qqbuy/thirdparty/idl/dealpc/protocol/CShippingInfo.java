//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.deelpc.protocol.CDealInfo.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *物流信息
 *
 *@date 2015-03-04 11:46:41
 *
 *@since version:0
*/
public class CShippingInfo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 订单编号
	 *
	 * 版本 >= 0
	 */
	 private long DealId;

	/**
	 * 物流单生成时间
	 *
	 * 版本 >= 0
	 */
	 private long WuliuGenTime;

	/**
	 * 物流状态
	 *
	 * 版本 >= 0
	 */
	 private long WuliuState;

	/**
	 * 到货时间
	 *
	 * 版本 >= 0
	 */
	 private long RecvTime;

	/**
	 * 预计到货时间
	 *
	 * 版本 >= 0
	 */
	 private long ExpectArrivalTime;

	/**
	 * 间
	 *
	 * 版本 >= 0
	 */
	 private long SendTime;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 最后更新时间
	 *
	 * 版本 >= 0
	 */
	 private long LastUpdateTime;

	/**
	 * 物流公司类型
	 *
	 * 版本 >= 0
	 */
	 private long WuliuCompanyId;

	/**
	 * 收货地址_姓名
	 *
	 * 版本 >= 0
	 */
	 private String ReceiveName = new String();

	/**
	 * 收货地址_地址
	 *
	 * 版本 >= 0
	 */
	 private String ReceiveAddr = new String();

	/**
	 * 收货地址_地址编码
	 *
	 * 版本 >= 0
	 */
	 private String ReceiveAddrCode = new String();

	/**
	 * 收货地址_邮编
	 *
	 * 版本 >= 0
	 */
	 private String ReceivePostcode = new String();

	/**
	 * 收货地息_电话
	 *
	 * 版本 >= 0
	 */
	 private String ReceiveTel = new String();

	/**
	 * 收货地址_手机
	 *
	 * 版本 >= 0
	 */
	 private String ReceiveMobile = new String();

	/**
	 * 物流方式
	 *
	 * 版本 >= 0
	 */
	 private String WuliuType = new String();

	/**
	 * 物流公司
	 *
	 * 版本 >= 0
	 */
	 private String WuliuCompany = new String();

	/**
	 * 物流运单号
	 *
	 * 版本 >= 0
	 */
	 private String WuliuCode = new String();

	/**
	 * 取货地
	 *
	 * 版本 >= 0
	 */
	 private String GetItemAddr = new String();

	/**
	 * 物流描述
	 *
	 * 版本 >= 0
	 */
	 private String WuliuDesc = new String();

	/**
	 * 发货目的地
	 *
	 * 版本 >= 0
	 */
	 private String SendToAddr = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushLong(DealId);
		bs.pushUInt(WuliuGenTime);
		bs.pushUInt(WuliuState);
		bs.pushUInt(RecvTime);
		bs.pushUInt(ExpectArrivalTime);
		bs.pushUInt(SendTime);
		bs.pushUInt(version);
		bs.pushUInt(LastUpdateTime);
		bs.pushUInt(WuliuCompanyId);
		bs.pushString(ReceiveName);
		bs.pushString(ReceiveAddr);
		bs.pushString(ReceiveAddrCode);
		bs.pushString(ReceivePostcode);
		bs.pushString(ReceiveTel);
		bs.pushString(ReceiveMobile);
		bs.pushString(WuliuType);
		bs.pushString(WuliuCompany);
		bs.pushString(WuliuCode);
		bs.pushString(GetItemAddr);
		bs.pushString(WuliuDesc);
		bs.pushString(SendToAddr);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		DealId = bs.popLong();
		WuliuGenTime = bs.popUInt();
		WuliuState = bs.popUInt();
		RecvTime = bs.popUInt();
		ExpectArrivalTime = bs.popUInt();
		SendTime = bs.popUInt();
		version = bs.popUInt();
		LastUpdateTime = bs.popUInt();
		WuliuCompanyId = bs.popUInt();
		ReceiveName = bs.popString();
		ReceiveAddr = bs.popString();
		ReceiveAddrCode = bs.popString();
		ReceivePostcode = bs.popString();
		ReceiveTel = bs.popString();
		ReceiveMobile = bs.popString();
		WuliuType = bs.popString();
		WuliuCompany = bs.popString();
		WuliuCode = bs.popString();
		GetItemAddr = bs.popString();
		WuliuDesc = bs.popString();
		SendToAddr = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:long
	 * 
	 */
	public long getDealId()
	{
		return DealId;
	}


	/**
	 * 设置订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealId(long value)
	{
		this.DealId = value;
	}


	/**
	 * 获取物流单生成时间
	 * 
	 * 此字段的版本 >= 0
	 * @return WuliuGenTime value 类型为:long
	 * 
	 */
	public long getWuliuGenTime()
	{
		return WuliuGenTime;
	}


	/**
	 * 设置物流单生成时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWuliuGenTime(long value)
	{
		this.WuliuGenTime = value;
	}


	/**
	 * 获取物流状态
	 * 
	 * 此字段的版本 >= 0
	 * @return WuliuState value 类型为:long
	 * 
	 */
	public long getWuliuState()
	{
		return WuliuState;
	}


	/**
	 * 设置物流状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWuliuState(long value)
	{
		this.WuliuState = value;
	}


	/**
	 * 获取到货时间
	 * 
	 * 此字段的版本 >= 0
	 * @return RecvTime value 类型为:long
	 * 
	 */
	public long getRecvTime()
	{
		return RecvTime;
	}


	/**
	 * 设置到货时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRecvTime(long value)
	{
		this.RecvTime = value;
	}


	/**
	 * 获取预计到货时间
	 * 
	 * 此字段的版本 >= 0
	 * @return ExpectArrivalTime value 类型为:long
	 * 
	 */
	public long getExpectArrivalTime()
	{
		return ExpectArrivalTime;
	}


	/**
	 * 设置预计到货时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setExpectArrivalTime(long value)
	{
		this.ExpectArrivalTime = value;
	}


	/**
	 * 获取间
	 * 
	 * 此字段的版本 >= 0
	 * @return SendTime value 类型为:long
	 * 
	 */
	public long getSendTime()
	{
		return SendTime;
	}


	/**
	 * 设置间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSendTime(long value)
	{
		this.SendTime = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return LastUpdateTime value 类型为:long
	 * 
	 */
	public long getLastUpdateTime()
	{
		return LastUpdateTime;
	}


	/**
	 * 设置最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastUpdateTime(long value)
	{
		this.LastUpdateTime = value;
	}


	/**
	 * 获取物流公司类型
	 * 
	 * 此字段的版本 >= 0
	 * @return WuliuCompanyId value 类型为:long
	 * 
	 */
	public long getWuliuCompanyId()
	{
		return WuliuCompanyId;
	}


	/**
	 * 设置物流公司类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWuliuCompanyId(long value)
	{
		this.WuliuCompanyId = value;
	}


	/**
	 * 获取收货地址_姓名
	 * 
	 * 此字段的版本 >= 0
	 * @return ReceiveName value 类型为:String
	 * 
	 */
	public String getReceiveName()
	{
		return ReceiveName;
	}


	/**
	 * 设置收货地址_姓名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReceiveName(String value)
	{
		this.ReceiveName = value;
	}


	/**
	 * 获取收货地址_地址
	 * 
	 * 此字段的版本 >= 0
	 * @return ReceiveAddr value 类型为:String
	 * 
	 */
	public String getReceiveAddr()
	{
		return ReceiveAddr;
	}


	/**
	 * 设置收货地址_地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReceiveAddr(String value)
	{
		this.ReceiveAddr = value;
	}


	/**
	 * 获取收货地址_地址编码
	 * 
	 * 此字段的版本 >= 0
	 * @return ReceiveAddrCode value 类型为:String
	 * 
	 */
	public String getReceiveAddrCode()
	{
		return ReceiveAddrCode;
	}


	/**
	 * 设置收货地址_地址编码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReceiveAddrCode(String value)
	{
		this.ReceiveAddrCode = value;
	}


	/**
	 * 获取收货地址_邮编
	 * 
	 * 此字段的版本 >= 0
	 * @return ReceivePostcode value 类型为:String
	 * 
	 */
	public String getReceivePostcode()
	{
		return ReceivePostcode;
	}


	/**
	 * 设置收货地址_邮编
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReceivePostcode(String value)
	{
		this.ReceivePostcode = value;
	}


	/**
	 * 获取收货地息_电话
	 * 
	 * 此字段的版本 >= 0
	 * @return ReceiveTel value 类型为:String
	 * 
	 */
	public String getReceiveTel()
	{
		return ReceiveTel;
	}


	/**
	 * 设置收货地息_电话
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReceiveTel(String value)
	{
		this.ReceiveTel = value;
	}


	/**
	 * 获取收货地址_手机
	 * 
	 * 此字段的版本 >= 0
	 * @return ReceiveMobile value 类型为:String
	 * 
	 */
	public String getReceiveMobile()
	{
		return ReceiveMobile;
	}


	/**
	 * 设置收货地址_手机
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReceiveMobile(String value)
	{
		this.ReceiveMobile = value;
	}


	/**
	 * 获取物流方式
	 * 
	 * 此字段的版本 >= 0
	 * @return WuliuType value 类型为:String
	 * 
	 */
	public String getWuliuType()
	{
		return WuliuType;
	}


	/**
	 * 设置物流方式
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setWuliuType(String value)
	{
		this.WuliuType = value;
	}


	/**
	 * 获取物流公司
	 * 
	 * 此字段的版本 >= 0
	 * @return WuliuCompany value 类型为:String
	 * 
	 */
	public String getWuliuCompany()
	{
		return WuliuCompany;
	}


	/**
	 * 设置物流公司
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setWuliuCompany(String value)
	{
		this.WuliuCompany = value;
	}


	/**
	 * 获取物流运单号
	 * 
	 * 此字段的版本 >= 0
	 * @return WuliuCode value 类型为:String
	 * 
	 */
	public String getWuliuCode()
	{
		return WuliuCode;
	}


	/**
	 * 设置物流运单号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setWuliuCode(String value)
	{
		this.WuliuCode = value;
	}


	/**
	 * 获取取货地
	 * 
	 * 此字段的版本 >= 0
	 * @return GetItemAddr value 类型为:String
	 * 
	 */
	public String getGetItemAddr()
	{
		return GetItemAddr;
	}


	/**
	 * 设置取货地
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setGetItemAddr(String value)
	{
		this.GetItemAddr = value;
	}


	/**
	 * 获取物流描述
	 * 
	 * 此字段的版本 >= 0
	 * @return WuliuDesc value 类型为:String
	 * 
	 */
	public String getWuliuDesc()
	{
		return WuliuDesc;
	}


	/**
	 * 设置物流描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setWuliuDesc(String value)
	{
		this.WuliuDesc = value;
	}


	/**
	 * 获取发货目的地
	 * 
	 * 此字段的版本 >= 0
	 * @return SendToAddr value 类型为:String
	 * 
	 */
	public String getSendToAddr()
	{
		return SendToAddr;
	}


	/**
	 * 设置发货目的地
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSendToAddr(String value)
	{
		this.SendToAddr = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CShippingInfo)
				length += 17;  //计算字段DealId的长度 size_of(uint64_t)
				length += 4;  //计算字段WuliuGenTime的长度 size_of(uint32_t)
				length += 4;  //计算字段WuliuState的长度 size_of(uint32_t)
				length += 4;  //计算字段RecvTime的长度 size_of(uint32_t)
				length += 4;  //计算字段ExpectArrivalTime的长度 size_of(uint32_t)
				length += 4;  //计算字段SendTime的长度 size_of(uint32_t)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段LastUpdateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段WuliuCompanyId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ReceiveName, null);  //计算字段ReceiveName的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceiveAddr, null);  //计算字段ReceiveAddr的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceiveAddrCode, null);  //计算字段ReceiveAddrCode的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceivePostcode, null);  //计算字段ReceivePostcode的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceiveTel, null);  //计算字段ReceiveTel的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceiveMobile, null);  //计算字段ReceiveMobile的长度 size_of(String)
				length += ByteStream.getObjectSize(WuliuType, null);  //计算字段WuliuType的长度 size_of(String)
				length += ByteStream.getObjectSize(WuliuCompany, null);  //计算字段WuliuCompany的长度 size_of(String)
				length += ByteStream.getObjectSize(WuliuCode, null);  //计算字段WuliuCode的长度 size_of(String)
				length += ByteStream.getObjectSize(GetItemAddr, null);  //计算字段GetItemAddr的长度 size_of(String)
				length += ByteStream.getObjectSize(WuliuDesc, null);  //计算字段WuliuDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(SendToAddr, null);  //计算字段SendToAddr的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(CShippingInfo)
				length += 17;  //计算字段DealId的长度 size_of(uint64_t)
				length += 4;  //计算字段WuliuGenTime的长度 size_of(uint32_t)
				length += 4;  //计算字段WuliuState的长度 size_of(uint32_t)
				length += 4;  //计算字段RecvTime的长度 size_of(uint32_t)
				length += 4;  //计算字段ExpectArrivalTime的长度 size_of(uint32_t)
				length += 4;  //计算字段SendTime的长度 size_of(uint32_t)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段LastUpdateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段WuliuCompanyId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ReceiveName, encoding);  //计算字段ReceiveName的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceiveAddr, encoding);  //计算字段ReceiveAddr的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceiveAddrCode, encoding);  //计算字段ReceiveAddrCode的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceivePostcode, encoding);  //计算字段ReceivePostcode的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceiveTel, encoding);  //计算字段ReceiveTel的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceiveMobile, encoding);  //计算字段ReceiveMobile的长度 size_of(String)
				length += ByteStream.getObjectSize(WuliuType, encoding);  //计算字段WuliuType的长度 size_of(String)
				length += ByteStream.getObjectSize(WuliuCompany, encoding);  //计算字段WuliuCompany的长度 size_of(String)
				length += ByteStream.getObjectSize(WuliuCode, encoding);  //计算字段WuliuCode的长度 size_of(String)
				length += ByteStream.getObjectSize(GetItemAddr, encoding);  //计算字段GetItemAddr的长度 size_of(String)
				length += ByteStream.getObjectSize(WuliuDesc, encoding);  //计算字段WuliuDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(SendToAddr, encoding);  //计算字段SendToAddr的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
