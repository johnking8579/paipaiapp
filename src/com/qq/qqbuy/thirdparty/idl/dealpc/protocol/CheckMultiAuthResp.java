 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.EmployeeAo.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import com.paipai.lang.uint32_t;
import java.util.Vector;

/**
 *判断用户是否有场景vector所需的权限返回类
 *
 *@date 2014-12-16 05:53:03
 *
 *@since version:0
*/
public class  CheckMultiAuthResp implements IServiceObject
{
	public long result;
	/**
	 * 对应场景id是否鉴权通过的vector，与场景id vector一一对应。0表示鉴权未通过；1表示鉴权通过
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint32_t> passFlag = new Vector<uint32_t>();

	/**
	 * 卖家qq号
	 *
	 * 版本 >= 0
	 */
	 private long sellerUin;

	/**
	 * 输出保留字
	 *
	 * 版本 >= 0
	 */
	 private String outReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(passFlag);
		bs.pushUInt(sellerUin);
		bs.pushString(outReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		passFlag = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		sellerUin = bs.popUInt();
		outReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xf1928807L;
	}


	/**
	 * 获取对应场景id是否鉴权通过的vector，与场景id vector一一对应。0表示鉴权未通过；1表示鉴权通过
	 * 
	 * 此字段的版本 >= 0
	 * @return passFlag value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getPassFlag()
	{
		return passFlag;
	}


	/**
	 * 设置对应场景id是否鉴权通过的vector，与场景id vector一一对应。0表示鉴权未通过；1表示鉴权通过
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setPassFlag(Vector<uint32_t> value)
	{
		if (value != null) {
				this.passFlag = value;
		}else{
				this.passFlag = new Vector<uint32_t>();
		}
	}


	/**
	 * 获取卖家qq号
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return sellerUin;
	}


	/**
	 * 设置卖家qq号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.sellerUin = value;
	}


	/**
	 * 获取输出保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return outReserve;
	}


	/**
	 * 设置输出保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		this.outReserve = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CheckMultiAuthResp)
				length += ByteStream.getObjectSize(passFlag, null);  //计算字段passFlag的长度 size_of(Vector)
				length += 4;  //计算字段sellerUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(outReserve, null);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(CheckMultiAuthResp)
				length += ByteStream.getObjectSize(passFlag, encoding);  //计算字段passFlag的长度 size_of(Vector)
				length += 4;  //计算字段sellerUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(outReserve, encoding);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
