 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.deelpc.protocol.DealIdl.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import java.util.Vector;

/**
 *卖家改价
 *
 *@date 2015-03-04 11:46:41
 *
 *@since version:0
*/
public class  SellerModifyPriceReq extends NetMessage
{
	/**
	 * 调用者==>请设置为源文件名
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 订单id
	 *
	 * 版本 >= 0
	 */
	 private String DealId = new String();

	/**
	 * 场景id
	 *
	 * 版本 >= 0
	 */
	 private long SceneId;

	/**
	 * 邮费
	 *
	 * 版本 >= 0
	 */
	 private long ShippingFee;

	/**
	 * 子单信息
	 *
	 * 版本 >= 0
	 */
	 private Vector<CTradePrice> vecTradeList = new Vector<CTradePrice>();

	/**
	 * 用户MachineKey
	 *
	 * 版本 >= 0
	 */
	 private String MachineKey = new String();

	/**
	 * 保留输入字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveIn = new String();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(Source);
		bs.pushString(DealId);
		bs.pushUInt(SceneId);
		bs.pushUInt(ShippingFee);
		bs.pushObject(vecTradeList);
		bs.pushString(MachineKey);
		bs.pushString(ReserveIn);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		Source = bs.popString();
		DealId = bs.popString();
		SceneId = bs.popUInt();
		ShippingFee = bs.popUInt();
		vecTradeList = (Vector<CTradePrice>)bs.popVector(CTradePrice.class);
		MachineKey = bs.popString();
		ReserveIn = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x26301808L;
	}


	/**
	 * 获取调用者==>请设置为源文件名
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置调用者==>请设置为源文件名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取订单id
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:String
	 * 
	 */
	public String getDealId()
	{
		return DealId;
	}


	/**
	 * 设置订单id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealId(String value)
	{
		this.DealId = value;
	}


	/**
	 * 获取场景id
	 * 
	 * 此字段的版本 >= 0
	 * @return SceneId value 类型为:long
	 * 
	 */
	public long getSceneId()
	{
		return SceneId;
	}


	/**
	 * 设置场景id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSceneId(long value)
	{
		this.SceneId = value;
	}


	/**
	 * 获取邮费
	 * 
	 * 此字段的版本 >= 0
	 * @return ShippingFee value 类型为:long
	 * 
	 */
	public long getShippingFee()
	{
		return ShippingFee;
	}


	/**
	 * 设置邮费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setShippingFee(long value)
	{
		this.ShippingFee = value;
	}


	/**
	 * 获取子单信息
	 * 
	 * 此字段的版本 >= 0
	 * @return vecTradeList value 类型为:Vector<CTradePrice>
	 * 
	 */
	public Vector<CTradePrice> getVecTradeList()
	{
		return vecTradeList;
	}


	/**
	 * 设置子单信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<CTradePrice>
	 * 
	 */
	public void setVecTradeList(Vector<CTradePrice> value)
	{
		if (value != null) {
				this.vecTradeList = value;
		}else{
				this.vecTradeList = new Vector<CTradePrice>();
		}
	}


	/**
	 * 获取用户MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @return MachineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return MachineKey;
	}


	/**
	 * 设置用户MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.MachineKey = value;
	}


	/**
	 * 获取保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveIn value 类型为:String
	 * 
	 */
	public String getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveIn(String value)
	{
		this.ReserveIn = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(SellerModifyPriceReq)
				length += ByteStream.getObjectSize(Source, null);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(DealId, null);  //计算字段DealId的长度 size_of(String)
				length += 4;  //计算字段SceneId的长度 size_of(uint32_t)
				length += 4;  //计算字段ShippingFee的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(vecTradeList, null);  //计算字段vecTradeList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(MachineKey, null);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveIn, null);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(SellerModifyPriceReq)
				length += ByteStream.getObjectSize(Source, encoding);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(DealId, encoding);  //计算字段DealId的长度 size_of(String)
				length += 4;  //计算字段SceneId的长度 size_of(uint32_t)
				length += 4;  //计算字段ShippingFee的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(vecTradeList, encoding);  //计算字段vecTradeList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(MachineKey, encoding);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveIn, encoding);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
