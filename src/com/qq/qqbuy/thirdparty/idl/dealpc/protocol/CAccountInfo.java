//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.deelpc.protocol.DealIdl.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *支付信息
 *
 *@date 2015-03-04 11:46:41
 *
 *@since version:0
*/
public class CAccountInfo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 订单id
	 *
	 * 版本 >= 0
	 */
	 private long DealId;

	/**
	 * 支付单编号
	 *
	 * 版本 >= 0
	 */
	 private long PayId;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 订单生成时间
	 *
	 * 版本 >= 0
	 */
	 private long DealCreateTime;

	/**
	 * 订单类型
	 *
	 * 版本 >= 0
	 */
	 private long DealType;

	/**
	 * 卖家号
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * 买家好
	 *
	 * 版本 >= 0
	 */
	 private long BuyerUin;

	/**
	 * 商品总价
	 *
	 * 版本 >= 0
	 */
	 private long SumFee;

	/**
	 * 运费
	 *
	 * 版本 >= 0
	 */
	 private long MailFee;

	/**
	 * 退款:买家退款金额
	 *
	 * 版本 >= 0
	 */
	 private long BuyerRecvRefund;

	/**
	 * 退款:卖家实收金额
	 *
	 * 版本 >= 0
	 */
	 private long SellerRecvRefund;

	/**
	 * 打款完成时间
	 *
	 * 版本 >= 0
	 */
	 private long RecvfeeTime;

	/**
	 * 打款返回时间
	 *
	 * 版本 >= 0
	 */
	 private long RecvfeeReturnTime;

	/**
	 * 支付现金
	 *
	 * 版本 >= 0
	 */
	 private long PayFeeCash;

	/**
	 * 支付现金卷
	 *
	 * 版本 >= 0
	 */
	 private long PayFeeTicket;

	/**
	 * 支付其它金额
	 *
	 * 版本 >= 0
	 */
	 private long PayFeeEtc;

	/**
	 * 支付手续费
	 *
	 * 版本 >= 0
	 */
	 private long PayHandleFee;

	/**
	 * 支付折扣卷
	 *
	 * 版本 >= 0
	 */
	 private long PayFeeVfee;

	/**
	 * 支付使用积分
	 *
	 * 版本 >= 0
	 */
	 private long PayPoint;

	/**
	 * 支付完成时间
	 *
	 * 版本 >= 0
	 */
	 private long PayTime;

	/**
	 * 支付返回时间
	 *
	 * 版本 >= 0
	 */
	 private long PayReturnTime;

	/**
	 * 最后更新时间
	 *
	 * 版本 >= 0
	 */
	 private long LastUpdateTime;

	/**
	 * 银行类型
	 *
	 * 版本 >= 0
	 */
	 private long BankType;

	/**
	 * 支付单状态
	 *
	 * 版本 >= 0
	 */
	 private long PayState;

	/**
	 * 商家名称
	 *
	 * 版本 >= 0
	 */
	 private String SellerName = new String();

	/**
	 * 买家名称
	 *
	 * 版本 >= 0
	 */
	 private String BuyerName = new String();

	/**
	 * 商品标题列表
	 *
	 * 版本 >= 0
	 */
	 private String ItemTitleList = new String();

	/**
	 * 订单支付ID
	 *
	 * 版本 >= 0
	 */
	 private String DealCftPayid = new String();

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private String Reserve = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushLong(DealId);
		bs.pushLong(PayId);
		bs.pushUInt(version);
		bs.pushUInt(DealCreateTime);
		bs.pushUInt(DealType);
		bs.pushUInt(SellerUin);
		bs.pushUInt(BuyerUin);
		bs.pushUInt(SumFee);
		bs.pushUInt(MailFee);
		bs.pushUInt(BuyerRecvRefund);
		bs.pushUInt(SellerRecvRefund);
		bs.pushUInt(RecvfeeTime);
		bs.pushUInt(RecvfeeReturnTime);
		bs.pushUInt(PayFeeCash);
		bs.pushUInt(PayFeeTicket);
		bs.pushUInt(PayFeeEtc);
		bs.pushUInt(PayHandleFee);
		bs.pushUInt(PayFeeVfee);
		bs.pushUInt(PayPoint);
		bs.pushUInt(PayTime);
		bs.pushUInt(PayReturnTime);
		bs.pushUInt(LastUpdateTime);
		bs.pushUInt(BankType);
		bs.pushUInt(PayState);
		bs.pushString(SellerName);
		bs.pushString(BuyerName);
		bs.pushString(ItemTitleList);
		bs.pushString(DealCftPayid);
		bs.pushString(Reserve);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		DealId = bs.popLong();
		PayId = bs.popLong();
		version = bs.popUInt();
		DealCreateTime = bs.popUInt();
		DealType = bs.popUInt();
		SellerUin = bs.popUInt();
		BuyerUin = bs.popUInt();
		SumFee = bs.popUInt();
		MailFee = bs.popUInt();
		BuyerRecvRefund = bs.popUInt();
		SellerRecvRefund = bs.popUInt();
		RecvfeeTime = bs.popUInt();
		RecvfeeReturnTime = bs.popUInt();
		PayFeeCash = bs.popUInt();
		PayFeeTicket = bs.popUInt();
		PayFeeEtc = bs.popUInt();
		PayHandleFee = bs.popUInt();
		PayFeeVfee = bs.popUInt();
		PayPoint = bs.popUInt();
		PayTime = bs.popUInt();
		PayReturnTime = bs.popUInt();
		LastUpdateTime = bs.popUInt();
		BankType = bs.popUInt();
		PayState = bs.popUInt();
		SellerName = bs.popString();
		BuyerName = bs.popString();
		ItemTitleList = bs.popString();
		DealCftPayid = bs.popString();
		Reserve = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取订单id
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:long
	 * 
	 */
	public long getDealId()
	{
		return DealId;
	}


	/**
	 * 设置订单id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealId(long value)
	{
		this.DealId = value;
	}


	/**
	 * 获取支付单编号
	 * 
	 * 此字段的版本 >= 0
	 * @return PayId value 类型为:long
	 * 
	 */
	public long getPayId()
	{
		return PayId;
	}


	/**
	 * 设置支付单编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayId(long value)
	{
		this.PayId = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取订单生成时间
	 * 
	 * 此字段的版本 >= 0
	 * @return DealCreateTime value 类型为:long
	 * 
	 */
	public long getDealCreateTime()
	{
		return DealCreateTime;
	}


	/**
	 * 设置订单生成时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealCreateTime(long value)
	{
		this.DealCreateTime = value;
	}


	/**
	 * 获取订单类型
	 * 
	 * 此字段的版本 >= 0
	 * @return DealType value 类型为:long
	 * 
	 */
	public long getDealType()
	{
		return DealType;
	}


	/**
	 * 设置订单类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealType(long value)
	{
		this.DealType = value;
	}


	/**
	 * 获取卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
	}


	/**
	 * 获取买家好
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return BuyerUin;
	}


	/**
	 * 设置买家好
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.BuyerUin = value;
	}


	/**
	 * 获取商品总价
	 * 
	 * 此字段的版本 >= 0
	 * @return SumFee value 类型为:long
	 * 
	 */
	public long getSumFee()
	{
		return SumFee;
	}


	/**
	 * 设置商品总价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSumFee(long value)
	{
		this.SumFee = value;
	}


	/**
	 * 获取运费
	 * 
	 * 此字段的版本 >= 0
	 * @return MailFee value 类型为:long
	 * 
	 */
	public long getMailFee()
	{
		return MailFee;
	}


	/**
	 * 设置运费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMailFee(long value)
	{
		this.MailFee = value;
	}


	/**
	 * 获取退款:买家退款金额
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerRecvRefund value 类型为:long
	 * 
	 */
	public long getBuyerRecvRefund()
	{
		return BuyerRecvRefund;
	}


	/**
	 * 设置退款:买家退款金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerRecvRefund(long value)
	{
		this.BuyerRecvRefund = value;
	}


	/**
	 * 获取退款:卖家实收金额
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerRecvRefund value 类型为:long
	 * 
	 */
	public long getSellerRecvRefund()
	{
		return SellerRecvRefund;
	}


	/**
	 * 设置退款:卖家实收金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerRecvRefund(long value)
	{
		this.SellerRecvRefund = value;
	}


	/**
	 * 获取打款完成时间
	 * 
	 * 此字段的版本 >= 0
	 * @return RecvfeeTime value 类型为:long
	 * 
	 */
	public long getRecvfeeTime()
	{
		return RecvfeeTime;
	}


	/**
	 * 设置打款完成时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRecvfeeTime(long value)
	{
		this.RecvfeeTime = value;
	}


	/**
	 * 获取打款返回时间
	 * 
	 * 此字段的版本 >= 0
	 * @return RecvfeeReturnTime value 类型为:long
	 * 
	 */
	public long getRecvfeeReturnTime()
	{
		return RecvfeeReturnTime;
	}


	/**
	 * 设置打款返回时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRecvfeeReturnTime(long value)
	{
		this.RecvfeeReturnTime = value;
	}


	/**
	 * 获取支付现金
	 * 
	 * 此字段的版本 >= 0
	 * @return PayFeeCash value 类型为:long
	 * 
	 */
	public long getPayFeeCash()
	{
		return PayFeeCash;
	}


	/**
	 * 设置支付现金
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayFeeCash(long value)
	{
		this.PayFeeCash = value;
	}


	/**
	 * 获取支付现金卷
	 * 
	 * 此字段的版本 >= 0
	 * @return PayFeeTicket value 类型为:long
	 * 
	 */
	public long getPayFeeTicket()
	{
		return PayFeeTicket;
	}


	/**
	 * 设置支付现金卷
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayFeeTicket(long value)
	{
		this.PayFeeTicket = value;
	}


	/**
	 * 获取支付其它金额
	 * 
	 * 此字段的版本 >= 0
	 * @return PayFeeEtc value 类型为:long
	 * 
	 */
	public long getPayFeeEtc()
	{
		return PayFeeEtc;
	}


	/**
	 * 设置支付其它金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayFeeEtc(long value)
	{
		this.PayFeeEtc = value;
	}


	/**
	 * 获取支付手续费
	 * 
	 * 此字段的版本 >= 0
	 * @return PayHandleFee value 类型为:long
	 * 
	 */
	public long getPayHandleFee()
	{
		return PayHandleFee;
	}


	/**
	 * 设置支付手续费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayHandleFee(long value)
	{
		this.PayHandleFee = value;
	}


	/**
	 * 获取支付折扣卷
	 * 
	 * 此字段的版本 >= 0
	 * @return PayFeeVfee value 类型为:long
	 * 
	 */
	public long getPayFeeVfee()
	{
		return PayFeeVfee;
	}


	/**
	 * 设置支付折扣卷
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayFeeVfee(long value)
	{
		this.PayFeeVfee = value;
	}


	/**
	 * 获取支付使用积分
	 * 
	 * 此字段的版本 >= 0
	 * @return PayPoint value 类型为:long
	 * 
	 */
	public long getPayPoint()
	{
		return PayPoint;
	}


	/**
	 * 设置支付使用积分
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayPoint(long value)
	{
		this.PayPoint = value;
	}


	/**
	 * 获取支付完成时间
	 * 
	 * 此字段的版本 >= 0
	 * @return PayTime value 类型为:long
	 * 
	 */
	public long getPayTime()
	{
		return PayTime;
	}


	/**
	 * 设置支付完成时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayTime(long value)
	{
		this.PayTime = value;
	}


	/**
	 * 获取支付返回时间
	 * 
	 * 此字段的版本 >= 0
	 * @return PayReturnTime value 类型为:long
	 * 
	 */
	public long getPayReturnTime()
	{
		return PayReturnTime;
	}


	/**
	 * 设置支付返回时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayReturnTime(long value)
	{
		this.PayReturnTime = value;
	}


	/**
	 * 获取最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return LastUpdateTime value 类型为:long
	 * 
	 */
	public long getLastUpdateTime()
	{
		return LastUpdateTime;
	}


	/**
	 * 设置最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastUpdateTime(long value)
	{
		this.LastUpdateTime = value;
	}


	/**
	 * 获取银行类型
	 * 
	 * 此字段的版本 >= 0
	 * @return BankType value 类型为:long
	 * 
	 */
	public long getBankType()
	{
		return BankType;
	}


	/**
	 * 设置银行类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBankType(long value)
	{
		this.BankType = value;
	}


	/**
	 * 获取支付单状态
	 * 
	 * 此字段的版本 >= 0
	 * @return PayState value 类型为:long
	 * 
	 */
	public long getPayState()
	{
		return PayState;
	}


	/**
	 * 设置支付单状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayState(long value)
	{
		this.PayState = value;
	}


	/**
	 * 获取商家名称
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerName value 类型为:String
	 * 
	 */
	public String getSellerName()
	{
		return SellerName;
	}


	/**
	 * 设置商家名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSellerName(String value)
	{
		this.SellerName = value;
	}


	/**
	 * 获取买家名称
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerName value 类型为:String
	 * 
	 */
	public String getBuyerName()
	{
		return BuyerName;
	}


	/**
	 * 设置买家名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBuyerName(String value)
	{
		this.BuyerName = value;
	}


	/**
	 * 获取商品标题列表
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemTitleList value 类型为:String
	 * 
	 */
	public String getItemTitleList()
	{
		return ItemTitleList;
	}


	/**
	 * 设置商品标题列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemTitleList(String value)
	{
		this.ItemTitleList = value;
	}


	/**
	 * 获取订单支付ID
	 * 
	 * 此字段的版本 >= 0
	 * @return DealCftPayid value 类型为:String
	 * 
	 */
	public String getDealCftPayid()
	{
		return DealCftPayid;
	}


	/**
	 * 设置订单支付ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealCftPayid(String value)
	{
		this.DealCftPayid = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Reserve value 类型为:String
	 * 
	 */
	public String getReserve()
	{
		return Reserve;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserve(String value)
	{
		this.Reserve = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CAccountInfo)
				length += 17;  //计算字段DealId的长度 size_of(uint64_t)
				length += 17;  //计算字段PayId的长度 size_of(uint64_t)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段DealCreateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段DealType的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段SumFee的长度 size_of(uint32_t)
				length += 4;  //计算字段MailFee的长度 size_of(uint32_t)
				length += 4;  //计算字段BuyerRecvRefund的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerRecvRefund的长度 size_of(uint32_t)
				length += 4;  //计算字段RecvfeeTime的长度 size_of(uint32_t)
				length += 4;  //计算字段RecvfeeReturnTime的长度 size_of(uint32_t)
				length += 4;  //计算字段PayFeeCash的长度 size_of(uint32_t)
				length += 4;  //计算字段PayFeeTicket的长度 size_of(uint32_t)
				length += 4;  //计算字段PayFeeEtc的长度 size_of(uint32_t)
				length += 4;  //计算字段PayHandleFee的长度 size_of(uint32_t)
				length += 4;  //计算字段PayFeeVfee的长度 size_of(uint32_t)
				length += 4;  //计算字段PayPoint的长度 size_of(uint32_t)
				length += 4;  //计算字段PayTime的长度 size_of(uint32_t)
				length += 4;  //计算字段PayReturnTime的长度 size_of(uint32_t)
				length += 4;  //计算字段LastUpdateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段BankType的长度 size_of(uint32_t)
				length += 4;  //计算字段PayState的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(SellerName, null);  //计算字段SellerName的长度 size_of(String)
				length += ByteStream.getObjectSize(BuyerName, null);  //计算字段BuyerName的长度 size_of(String)
				length += ByteStream.getObjectSize(ItemTitleList, null);  //计算字段ItemTitleList的长度 size_of(String)
				length += ByteStream.getObjectSize(DealCftPayid, null);  //计算字段DealCftPayid的长度 size_of(String)
				length += ByteStream.getObjectSize(Reserve, null);  //计算字段Reserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(CAccountInfo)
				length += 17;  //计算字段DealId的长度 size_of(uint64_t)
				length += 17;  //计算字段PayId的长度 size_of(uint64_t)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段DealCreateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段DealType的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段SumFee的长度 size_of(uint32_t)
				length += 4;  //计算字段MailFee的长度 size_of(uint32_t)
				length += 4;  //计算字段BuyerRecvRefund的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerRecvRefund的长度 size_of(uint32_t)
				length += 4;  //计算字段RecvfeeTime的长度 size_of(uint32_t)
				length += 4;  //计算字段RecvfeeReturnTime的长度 size_of(uint32_t)
				length += 4;  //计算字段PayFeeCash的长度 size_of(uint32_t)
				length += 4;  //计算字段PayFeeTicket的长度 size_of(uint32_t)
				length += 4;  //计算字段PayFeeEtc的长度 size_of(uint32_t)
				length += 4;  //计算字段PayHandleFee的长度 size_of(uint32_t)
				length += 4;  //计算字段PayFeeVfee的长度 size_of(uint32_t)
				length += 4;  //计算字段PayPoint的长度 size_of(uint32_t)
				length += 4;  //计算字段PayTime的长度 size_of(uint32_t)
				length += 4;  //计算字段PayReturnTime的长度 size_of(uint32_t)
				length += 4;  //计算字段LastUpdateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段BankType的长度 size_of(uint32_t)
				length += 4;  //计算字段PayState的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(SellerName, encoding);  //计算字段SellerName的长度 size_of(String)
				length += ByteStream.getObjectSize(BuyerName, encoding);  //计算字段BuyerName的长度 size_of(String)
				length += ByteStream.getObjectSize(ItemTitleList, encoding);  //计算字段ItemTitleList的长度 size_of(String)
				length += ByteStream.getObjectSize(DealCftPayid, encoding);  //计算字段DealCftPayid的长度 size_of(String)
				length += ByteStream.getObjectSize(Reserve, encoding);  //计算字段Reserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
