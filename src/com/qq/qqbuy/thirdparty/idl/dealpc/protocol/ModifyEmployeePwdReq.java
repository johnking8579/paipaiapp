 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.EmployeeAo.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *修改员工密码请求类
 *
 *@date 2014-12-16 05:53:03
 *
 *@since version:0
*/
public class  ModifyEmployeePwdReq implements IServiceObject
{
	/**
	 * 卖家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long sellerUin;

	/**
	 * 员工账号名
	 *
	 * 版本 >= 0
	 */
	 private String accountName = new String();

	/**
	 * 员工新密码
	 *
	 * 版本 >= 0
	 */
	 private String newPwd = new String();

	/**
	 * 员工交易密码
	 *
	 * 版本 >= 0
	 */
	 private String employeeC2cPwd = new String();

	/**
	 * 操作者交易密码
	 *
	 * 版本 >= 0
	 */
	 private String opC2cPwd = new String();

	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 调用来源
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 请求保留字
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(sellerUin);
		bs.pushString(accountName);
		bs.pushString(newPwd);
		bs.pushString(employeeC2cPwd);
		bs.pushString(opC2cPwd);
		bs.pushString(machineKey);
		bs.pushString(source);
		bs.pushString(inReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		sellerUin = bs.popUInt();
		accountName = bs.popString();
		newPwd = bs.popString();
		employeeC2cPwd = bs.popString();
		opC2cPwd = bs.popString();
		machineKey = bs.popString();
		source = bs.popString();
		inReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xf1911804L;
	}


	/**
	 * 获取卖家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return sellerUin;
	}


	/**
	 * 设置卖家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.sellerUin = value;
	}


	/**
	 * 获取员工账号名
	 * 
	 * 此字段的版本 >= 0
	 * @return accountName value 类型为:String
	 * 
	 */
	public String getAccountName()
	{
		return accountName;
	}


	/**
	 * 设置员工账号名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAccountName(String value)
	{
		this.accountName = value;
	}


	/**
	 * 获取员工新密码
	 * 
	 * 此字段的版本 >= 0
	 * @return newPwd value 类型为:String
	 * 
	 */
	public String getNewPwd()
	{
		return newPwd;
	}


	/**
	 * 设置员工新密码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setNewPwd(String value)
	{
		this.newPwd = value;
	}


	/**
	 * 获取员工交易密码
	 * 
	 * 此字段的版本 >= 0
	 * @return employeeC2cPwd value 类型为:String
	 * 
	 */
	public String getEmployeeC2cPwd()
	{
		return employeeC2cPwd;
	}


	/**
	 * 设置员工交易密码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setEmployeeC2cPwd(String value)
	{
		this.employeeC2cPwd = value;
	}


	/**
	 * 获取操作者交易密码
	 * 
	 * 此字段的版本 >= 0
	 * @return opC2cPwd value 类型为:String
	 * 
	 */
	public String getOpC2cPwd()
	{
		return opC2cPwd;
	}


	/**
	 * 设置操作者交易密码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOpC2cPwd(String value)
	{
		this.opC2cPwd = value;
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取调用来源
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置调用来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(ModifyEmployeePwdReq)
				length += 4;  //计算字段sellerUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(accountName, null);  //计算字段accountName的长度 size_of(String)
				length += ByteStream.getObjectSize(newPwd, null);  //计算字段newPwd的长度 size_of(String)
				length += ByteStream.getObjectSize(employeeC2cPwd, null);  //计算字段employeeC2cPwd的长度 size_of(String)
				length += ByteStream.getObjectSize(opC2cPwd, null);  //计算字段opC2cPwd的长度 size_of(String)
				length += ByteStream.getObjectSize(machineKey, null);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, null);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve, null);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(ModifyEmployeePwdReq)
				length += 4;  //计算字段sellerUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(accountName, encoding);  //计算字段accountName的长度 size_of(String)
				length += ByteStream.getObjectSize(newPwd, encoding);  //计算字段newPwd的长度 size_of(String)
				length += ByteStream.getObjectSize(employeeC2cPwd, encoding);  //计算字段employeeC2cPwd的长度 size_of(String)
				length += ByteStream.getObjectSize(opC2cPwd, encoding);  //计算字段opC2cPwd的长度 size_of(String)
				length += ByteStream.getObjectSize(machineKey, encoding);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, encoding);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve, encoding);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
