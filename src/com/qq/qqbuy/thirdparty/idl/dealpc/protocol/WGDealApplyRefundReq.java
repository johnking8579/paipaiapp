 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.deelpc.protocol.DealIdl.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *微购订单：发货前买家申请退款、修改退款协议
 *
 *@date 2015-03-04 11:46:41
 *
 *@since version:0
*/
public class  WGDealApplyRefundReq extends NetMessage
{
	/**
	 * 调用者==>请设置为源文件名
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 订单id
	 *
	 * 版本 >= 0
	 */
	 private String DealId = new String();

	/**
	 * 操作类型：1,申请退款 2, 修改退款协议 
	 *
	 * 版本 >= 0
	 */
	 private long Optype;

	/**
	 * 退款原因
	 *
	 * 版本 >= 0
	 */
	 private long RefundReason;

	/**
	 * 退款原因描述
	 *
	 * 版本 >= 0
	 */
	 private String RefundReasonDesc = new String();

	/**
	 * 用户MachineKey
	 *
	 * 版本 >= 0
	 */
	 private String MachineKey = new String();

	/**
	 * 保留输入字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveIn = new String();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(Source);
		bs.pushString(DealId);
		bs.pushUInt(Optype);
		bs.pushUInt(RefundReason);
		bs.pushString(RefundReasonDesc);
		bs.pushString(MachineKey);
		bs.pushString(ReserveIn);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		Source = bs.popString();
		DealId = bs.popString();
		Optype = bs.popUInt();
		RefundReason = bs.popUInt();
		RefundReasonDesc = bs.popString();
		MachineKey = bs.popString();
		ReserveIn = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x2630181AL;
	}


	/**
	 * 获取调用者==>请设置为源文件名
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置调用者==>请设置为源文件名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取订单id
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:String
	 * 
	 */
	public String getDealId()
	{
		return DealId;
	}


	/**
	 * 设置订单id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealId(String value)
	{
		this.DealId = value;
	}


	/**
	 * 获取操作类型：1,申请退款 2, 修改退款协议 
	 * 
	 * 此字段的版本 >= 0
	 * @return Optype value 类型为:long
	 * 
	 */
	public long getOptype()
	{
		return Optype;
	}


	/**
	 * 设置操作类型：1,申请退款 2, 修改退款协议 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOptype(long value)
	{
		this.Optype = value;
	}


	/**
	 * 获取退款原因
	 * 
	 * 此字段的版本 >= 0
	 * @return RefundReason value 类型为:long
	 * 
	 */
	public long getRefundReason()
	{
		return RefundReason;
	}


	/**
	 * 设置退款原因
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRefundReason(long value)
	{
		this.RefundReason = value;
	}


	/**
	 * 获取退款原因描述
	 * 
	 * 此字段的版本 >= 0
	 * @return RefundReasonDesc value 类型为:String
	 * 
	 */
	public String getRefundReasonDesc()
	{
		return RefundReasonDesc;
	}


	/**
	 * 设置退款原因描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRefundReasonDesc(String value)
	{
		this.RefundReasonDesc = value;
	}


	/**
	 * 获取用户MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @return MachineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return MachineKey;
	}


	/**
	 * 设置用户MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.MachineKey = value;
	}


	/**
	 * 获取保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveIn value 类型为:String
	 * 
	 */
	public String getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveIn(String value)
	{
		this.ReserveIn = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(WGDealApplyRefundReq)
				length += ByteStream.getObjectSize(Source, null);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(DealId, null);  //计算字段DealId的长度 size_of(String)
				length += 4;  //计算字段Optype的长度 size_of(uint32_t)
				length += 4;  //计算字段RefundReason的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(RefundReasonDesc, null);  //计算字段RefundReasonDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(MachineKey, null);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveIn, null);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(WGDealApplyRefundReq)
				length += ByteStream.getObjectSize(Source, encoding);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(DealId, encoding);  //计算字段DealId的长度 size_of(String)
				length += 4;  //计算字段Optype的长度 size_of(uint32_t)
				length += 4;  //计算字段RefundReason的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(RefundReasonDesc, encoding);  //计算字段RefundReasonDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(MachineKey, encoding);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveIn, encoding);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
