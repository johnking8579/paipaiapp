 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.deelpc.protocol.DealIdl.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import com.paipai.lang.uint64_t;
import java.util.Vector;

/**
 *付款后关闭微购订单
 *
 *@date 2015-03-04 11:46:41
 *
 *@since version:0
*/
public class  CloseWGDealPayReq extends NetMessage
{
	/**
	 * 调用者==>请设置为源文件名
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 订单id
	 *
	 * 版本 >= 0
	 */
	 private String DealId = new String();

	/**
	 * 操作者Uin
	 *
	 * 版本 >= 0
	 */
	 private long OpUin;

	/**
	 * 操作者类型
	 *
	 * 版本 >= 0
	 */
	 private long OpType;

	/**
	 * 关闭原因
	 *
	 * 版本 >= 0
	 */
	 private long CloseReason;

	/**
	 * 关闭原因描述
	 *
	 * 版本 >= 0
	 */
	 private String CloseReasonDesc = new String();

	/**
	 * 子单列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint64_t> TradeIdList = new Vector<uint64_t>();

	/**
	 * 用户MachineKey
	 *
	 * 版本 >= 0
	 */
	 private String MachineKey = new String();

	/**
	 * 保留输入字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveIn = new String();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(Source);
		bs.pushString(DealId);
		bs.pushUInt(OpUin);
		bs.pushUInt(OpType);
		bs.pushUInt(CloseReason);
		bs.pushString(CloseReasonDesc);
		bs.pushObject(TradeIdList);
		bs.pushString(MachineKey);
		bs.pushString(ReserveIn);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		Source = bs.popString();
		DealId = bs.popString();
		OpUin = bs.popUInt();
		OpType = bs.popUInt();
		CloseReason = bs.popUInt();
		CloseReasonDesc = bs.popString();
		TradeIdList = (Vector<uint64_t>)bs.popVector(uint64_t.class);
		MachineKey = bs.popString();
		ReserveIn = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x26301815L;
	}


	/**
	 * 获取调用者==>请设置为源文件名
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置调用者==>请设置为源文件名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取订单id
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:String
	 * 
	 */
	public String getDealId()
	{
		return DealId;
	}


	/**
	 * 设置订单id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealId(String value)
	{
		this.DealId = value;
	}


	/**
	 * 获取操作者Uin
	 * 
	 * 此字段的版本 >= 0
	 * @return OpUin value 类型为:long
	 * 
	 */
	public long getOpUin()
	{
		return OpUin;
	}


	/**
	 * 设置操作者Uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOpUin(long value)
	{
		this.OpUin = value;
	}


	/**
	 * 获取操作者类型
	 * 
	 * 此字段的版本 >= 0
	 * @return OpType value 类型为:long
	 * 
	 */
	public long getOpType()
	{
		return OpType;
	}


	/**
	 * 设置操作者类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOpType(long value)
	{
		this.OpType = value;
	}


	/**
	 * 获取关闭原因
	 * 
	 * 此字段的版本 >= 0
	 * @return CloseReason value 类型为:long
	 * 
	 */
	public long getCloseReason()
	{
		return CloseReason;
	}


	/**
	 * 设置关闭原因
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCloseReason(long value)
	{
		this.CloseReason = value;
	}


	/**
	 * 获取关闭原因描述
	 * 
	 * 此字段的版本 >= 0
	 * @return CloseReasonDesc value 类型为:String
	 * 
	 */
	public String getCloseReasonDesc()
	{
		return CloseReasonDesc;
	}


	/**
	 * 设置关闭原因描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCloseReasonDesc(String value)
	{
		this.CloseReasonDesc = value;
	}


	/**
	 * 获取子单列表
	 * 
	 * 此字段的版本 >= 0
	 * @return TradeIdList value 类型为:Vector<uint64_t>
	 * 
	 */
	public Vector<uint64_t> getTradeIdList()
	{
		return TradeIdList;
	}


	/**
	 * 设置子单列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint64_t>
	 * 
	 */
	public void setTradeIdList(Vector<uint64_t> value)
	{
		if (value != null) {
				this.TradeIdList = value;
		}else{
				this.TradeIdList = new Vector<uint64_t>();
		}
	}


	/**
	 * 获取用户MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @return MachineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return MachineKey;
	}


	/**
	 * 设置用户MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.MachineKey = value;
	}


	/**
	 * 获取保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveIn value 类型为:String
	 * 
	 */
	public String getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveIn(String value)
	{
		this.ReserveIn = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(CloseWGDealPayReq)
				length += ByteStream.getObjectSize(Source, null);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(DealId, null);  //计算字段DealId的长度 size_of(String)
				length += 4;  //计算字段OpUin的长度 size_of(uint32_t)
				length += 4;  //计算字段OpType的长度 size_of(uint32_t)
				length += 4;  //计算字段CloseReason的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(CloseReasonDesc, null);  //计算字段CloseReasonDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(TradeIdList, null);  //计算字段TradeIdList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(MachineKey, null);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveIn, null);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(CloseWGDealPayReq)
				length += ByteStream.getObjectSize(Source, encoding);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(DealId, encoding);  //计算字段DealId的长度 size_of(String)
				length += 4;  //计算字段OpUin的长度 size_of(uint32_t)
				length += 4;  //计算字段OpType的长度 size_of(uint32_t)
				length += 4;  //计算字段CloseReason的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(CloseReasonDesc, encoding);  //计算字段CloseReasonDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(TradeIdList, encoding);  //计算字段TradeIdList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(MachineKey, encoding);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveIn, encoding);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
