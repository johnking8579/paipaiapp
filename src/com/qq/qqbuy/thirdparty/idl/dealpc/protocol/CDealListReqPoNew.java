//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.deelpc.protocol.DealIdl.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *订单列表请求filter
 *
 *@date 2015-03-04 11:46:40
 *
 *@since version:0
*/
public class CDealListReqPoNew  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 4;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 卖家号
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * 版本 >= 0
	 */
	 private short SellerUin_u;

	/**
	 * 买家号
	 *
	 * 版本 >= 0
	 */
	 private long BuyerUin;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerUin_u;

	/**
	 * 收货人
	 *
	 * 版本 >= 0
	 */
	 private String RecvName = new String();

	/**
	 * 版本 >= 0
	 */
	 private short RecvName_u;

	/**
	 * 需要拉取的订单信息类型，请参照文档设置对应值，不要过多拉取无用信息，影响大家的性能
	 *
	 * 版本 >= 0
	 */
	 private long InfoType;

	/**
	 * 版本 >= 0
	 */
	 private short InfoType_u;

	/**
	 * 时间类型
	 *
	 * 版本 >= 0
	 */
	 private long TimeType;

	/**
	 * 版本 >= 0
	 */
	 private short TimeType_u;

	/**
	 * 排序类型 0升序 1降序
	 *
	 * 版本 >= 0
	 */
	 private long TimeOrder;

	/**
	 * 版本 >= 0
	 */
	 private short TimeOrder_u;

	/**
	 * 备注类型1-5
	 *
	 * 版本 >= 0
	 */
	 private long NoteType;

	/**
	 * 版本 >= 0
	 */
	 private short NoteType_u;

	/**
	 * 起始时间
	 *
	 * 版本 >= 0
	 */
	 private long TimeStart;

	/**
	 * 版本 >= 0
	 */
	 private short TimeStart_u;

	/**
	 * 结束时间
	 *
	 * 版本 >= 0
	 */
	 private long TimeEnd;

	/**
	 * 版本 >= 0
	 */
	 private short TimeEnd_u;

	/**
	 * 页码，第一页请填 1
	 *
	 * 版本 >= 0
	 */
	 private long PageIndex;

	/**
	 * 版本 >= 0
	 */
	 private short PageIndex_u;

	/**
	 * 每页数量，请填写1-20, 默认为20
	 *
	 * 版本 >= 0
	 */
	 private long PageSize;

	/**
	 * 版本 >= 0
	 */
	 private short PageSize_u;

	/**
	 * 订单状态
	 *
	 * 版本 >= 0
	 */
	 private long DealState;

	/**
	 * 版本 >= 0
	 */
	 private short DealState_u;

	/**
	 * 评价状态101-103
	 *
	 * 版本 >= 0
	 */
	 private long RateState;

	/**
	 * 版本 >= 0
	 */
	 private short RateState_u;

	/**
	 * 商品名称
	 *
	 * 版本 >= 0
	 */
	 private String ItemTitle = new String();

	/**
	 * 版本 >= 0
	 */
	 private short ItemTitle_u;

	/**
	 * 商品id
	 *
	 * 版本 >= 0
	 */
	 private String ItemId = new String();

	/**
	 * 版本 >= 0
	 */
	 private short ItemId_u;

	/**
	 * 历史订单标记 0 false 1 true
	 *
	 * 版本 >= 0
	 */
	 private long HistoryFlag;

	/**
	 * 版本 >= 0
	 */
	 private short HistoryFlag_u;

	/**
	 * 历史库分set
	 *
	 * 版本 >= 2
	 */
	 private long SetX = 0;

	/**
	 * 历史库分set
	 *
	 * 版本 >= 2
	 */
	 private short SetX_u = 0;

	/**
	 * 订单类型,0-所有,1-当期销售,4-店铺街
	 *
	 * 版本 >= 3
	 */
	 private long DealType = 0;

	/**
	 * 订单类型uflag
	 *
	 * 版本 >= 3
	 */
	 private short DealType_u = 0;

	/**
	 * 支付类型（只适用于当前库，不能查历史库） ,0或不设-所有,1-线上支付,2-货到付款
	 *
	 * 版本 >= 4
	 */
	 private long DealPayType = 0;

	/**
	 * 支付类型uflag
	 *
	 * 版本 >= 4
	 */
	 private short DealPayType_u = 0;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushUInt(SellerUin);
		bs.pushUByte(SellerUin_u);
		bs.pushUInt(BuyerUin);
		bs.pushUByte(BuyerUin_u);
		bs.pushString(RecvName);
		bs.pushUByte(RecvName_u);
		bs.pushUInt(InfoType);
		bs.pushUByte(InfoType_u);
		bs.pushUInt(TimeType);
		bs.pushUByte(TimeType_u);
		bs.pushUInt(TimeOrder);
		bs.pushUByte(TimeOrder_u);
		bs.pushUInt(NoteType);
		bs.pushUByte(NoteType_u);
		bs.pushUInt(TimeStart);
		bs.pushUByte(TimeStart_u);
		bs.pushUInt(TimeEnd);
		bs.pushUByte(TimeEnd_u);
		bs.pushUInt(PageIndex);
		bs.pushUByte(PageIndex_u);
		bs.pushUInt(PageSize);
		bs.pushUByte(PageSize_u);
		bs.pushUInt(DealState);
		bs.pushUByte(DealState_u);
		bs.pushUInt(RateState);
		bs.pushUByte(RateState_u);
		bs.pushString(ItemTitle);
		bs.pushUByte(ItemTitle_u);
		bs.pushString(ItemId);
		bs.pushUByte(ItemId_u);
		bs.pushUInt(HistoryFlag);
		bs.pushUByte(HistoryFlag_u);
		if(  this.version >= 2 ){
				bs.pushUInt(SetX);
		}
		if(  this.version >= 2 ){
				bs.pushUByte(SetX_u);
		}
		if(  this.version >= 3 ){
				bs.pushUInt(DealType);
		}
		if(  this.version >= 3 ){
				bs.pushUByte(DealType_u);
		}
		if(  this.version >= 4 ){
				bs.pushUInt(DealPayType);
		}
		if(  this.version >= 4 ){
				bs.pushUByte(DealPayType_u);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		SellerUin = bs.popUInt();
		SellerUin_u = bs.popUByte();
		BuyerUin = bs.popUInt();
		BuyerUin_u = bs.popUByte();
		RecvName = bs.popString();
		RecvName_u = bs.popUByte();
		InfoType = bs.popUInt();
		InfoType_u = bs.popUByte();
		TimeType = bs.popUInt();
		TimeType_u = bs.popUByte();
		TimeOrder = bs.popUInt();
		TimeOrder_u = bs.popUByte();
		NoteType = bs.popUInt();
		NoteType_u = bs.popUByte();
		TimeStart = bs.popUInt();
		TimeStart_u = bs.popUByte();
		TimeEnd = bs.popUInt();
		TimeEnd_u = bs.popUByte();
		PageIndex = bs.popUInt();
		PageIndex_u = bs.popUByte();
		PageSize = bs.popUInt();
		PageSize_u = bs.popUByte();
		DealState = bs.popUInt();
		DealState_u = bs.popUByte();
		RateState = bs.popUInt();
		RateState_u = bs.popUByte();
		ItemTitle = bs.popString();
		ItemTitle_u = bs.popUByte();
		ItemId = bs.popString();
		ItemId_u = bs.popUByte();
		HistoryFlag = bs.popUInt();
		HistoryFlag_u = bs.popUByte();
		if(  this.version >= 2 ){
				SetX = bs.popUInt();
		}
		if(  this.version >= 2 ){
				SetX_u = bs.popUByte();
		}
		if(  this.version >= 3 ){
				DealType = bs.popUInt();
		}
		if(  this.version >= 3 ){
				DealType_u = bs.popUByte();
		}
		if(  this.version >= 4 ){
				DealPayType = bs.popUInt();
		}
		if(  this.version >= 4 ){
				DealPayType_u = bs.popUByte();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
		this.SellerUin_u = 1;
	}

	public boolean issetSellerUin()
	{
		return this.SellerUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin_u value 类型为:short
	 * 
	 */
	public short getSellerUin_u()
	{
		return SellerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerUin_u(short value)
	{
		this.SellerUin_u = value;
	}


	/**
	 * 获取买家号
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return BuyerUin;
	}


	/**
	 * 设置买家号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.BuyerUin = value;
		this.BuyerUin_u = 1;
	}

	public boolean issetBuyerUin()
	{
		return this.BuyerUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerUin_u value 类型为:short
	 * 
	 */
	public short getBuyerUin_u()
	{
		return BuyerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerUin_u(short value)
	{
		this.BuyerUin_u = value;
	}


	/**
	 * 获取收货人
	 * 
	 * 此字段的版本 >= 0
	 * @return RecvName value 类型为:String
	 * 
	 */
	public String getRecvName()
	{
		return RecvName;
	}


	/**
	 * 设置收货人
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvName(String value)
	{
		this.RecvName = value;
		this.RecvName_u = 1;
	}

	public boolean issetRecvName()
	{
		return this.RecvName_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return RecvName_u value 类型为:short
	 * 
	 */
	public short getRecvName_u()
	{
		return RecvName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRecvName_u(short value)
	{
		this.RecvName_u = value;
	}


	/**
	 * 获取需要拉取的订单信息类型，请参照文档设置对应值，不要过多拉取无用信息，影响大家的性能
	 * 
	 * 此字段的版本 >= 0
	 * @return InfoType value 类型为:long
	 * 
	 */
	public long getInfoType()
	{
		return InfoType;
	}


	/**
	 * 设置需要拉取的订单信息类型，请参照文档设置对应值，不要过多拉取无用信息，影响大家的性能
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setInfoType(long value)
	{
		this.InfoType = value;
		this.InfoType_u = 1;
	}

	public boolean issetInfoType()
	{
		return this.InfoType_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return InfoType_u value 类型为:short
	 * 
	 */
	public short getInfoType_u()
	{
		return InfoType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setInfoType_u(short value)
	{
		this.InfoType_u = value;
	}


	/**
	 * 获取时间类型
	 * 
	 * 此字段的版本 >= 0
	 * @return TimeType value 类型为:long
	 * 
	 */
	public long getTimeType()
	{
		return TimeType;
	}


	/**
	 * 设置时间类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTimeType(long value)
	{
		this.TimeType = value;
		this.TimeType_u = 1;
	}

	public boolean issetTimeType()
	{
		return this.TimeType_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return TimeType_u value 类型为:short
	 * 
	 */
	public short getTimeType_u()
	{
		return TimeType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTimeType_u(short value)
	{
		this.TimeType_u = value;
	}


	/**
	 * 获取排序类型 0升序 1降序
	 * 
	 * 此字段的版本 >= 0
	 * @return TimeOrder value 类型为:long
	 * 
	 */
	public long getTimeOrder()
	{
		return TimeOrder;
	}


	/**
	 * 设置排序类型 0升序 1降序
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTimeOrder(long value)
	{
		this.TimeOrder = value;
		this.TimeOrder_u = 1;
	}

	public boolean issetTimeOrder()
	{
		return this.TimeOrder_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return TimeOrder_u value 类型为:short
	 * 
	 */
	public short getTimeOrder_u()
	{
		return TimeOrder_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTimeOrder_u(short value)
	{
		this.TimeOrder_u = value;
	}


	/**
	 * 获取备注类型1-5
	 * 
	 * 此字段的版本 >= 0
	 * @return NoteType value 类型为:long
	 * 
	 */
	public long getNoteType()
	{
		return NoteType;
	}


	/**
	 * 设置备注类型1-5
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNoteType(long value)
	{
		this.NoteType = value;
		this.NoteType_u = 1;
	}

	public boolean issetNoteType()
	{
		return this.NoteType_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return NoteType_u value 类型为:short
	 * 
	 */
	public short getNoteType_u()
	{
		return NoteType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNoteType_u(short value)
	{
		this.NoteType_u = value;
	}


	/**
	 * 获取起始时间
	 * 
	 * 此字段的版本 >= 0
	 * @return TimeStart value 类型为:long
	 * 
	 */
	public long getTimeStart()
	{
		return TimeStart;
	}


	/**
	 * 设置起始时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTimeStart(long value)
	{
		this.TimeStart = value;
		this.TimeStart_u = 1;
	}

	public boolean issetTimeStart()
	{
		return this.TimeStart_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return TimeStart_u value 类型为:short
	 * 
	 */
	public short getTimeStart_u()
	{
		return TimeStart_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTimeStart_u(short value)
	{
		this.TimeStart_u = value;
	}


	/**
	 * 获取结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return TimeEnd value 类型为:long
	 * 
	 */
	public long getTimeEnd()
	{
		return TimeEnd;
	}


	/**
	 * 设置结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTimeEnd(long value)
	{
		this.TimeEnd = value;
		this.TimeEnd_u = 1;
	}

	public boolean issetTimeEnd()
	{
		return this.TimeEnd_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return TimeEnd_u value 类型为:short
	 * 
	 */
	public short getTimeEnd_u()
	{
		return TimeEnd_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTimeEnd_u(short value)
	{
		this.TimeEnd_u = value;
	}


	/**
	 * 获取页码，第一页请填 1
	 * 
	 * 此字段的版本 >= 0
	 * @return PageIndex value 类型为:long
	 * 
	 */
	public long getPageIndex()
	{
		return PageIndex;
	}


	/**
	 * 设置页码，第一页请填 1
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPageIndex(long value)
	{
		this.PageIndex = value;
		this.PageIndex_u = 1;
	}

	public boolean issetPageIndex()
	{
		return this.PageIndex_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PageIndex_u value 类型为:short
	 * 
	 */
	public short getPageIndex_u()
	{
		return PageIndex_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPageIndex_u(short value)
	{
		this.PageIndex_u = value;
	}


	/**
	 * 获取每页数量，请填写1-20, 默认为20
	 * 
	 * 此字段的版本 >= 0
	 * @return PageSize value 类型为:long
	 * 
	 */
	public long getPageSize()
	{
		return PageSize;
	}


	/**
	 * 设置每页数量，请填写1-20, 默认为20
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPageSize(long value)
	{
		this.PageSize = value;
		this.PageSize_u = 1;
	}

	public boolean issetPageSize()
	{
		return this.PageSize_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PageSize_u value 类型为:short
	 * 
	 */
	public short getPageSize_u()
	{
		return PageSize_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPageSize_u(short value)
	{
		this.PageSize_u = value;
	}


	/**
	 * 获取订单状态
	 * 
	 * 此字段的版本 >= 0
	 * @return DealState value 类型为:long
	 * 
	 */
	public long getDealState()
	{
		return DealState;
	}


	/**
	 * 设置订单状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealState(long value)
	{
		this.DealState = value;
		this.DealState_u = 1;
	}

	public boolean issetDealState()
	{
		return this.DealState_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DealState_u value 类型为:short
	 * 
	 */
	public short getDealState_u()
	{
		return DealState_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealState_u(short value)
	{
		this.DealState_u = value;
	}


	/**
	 * 获取评价状态101-103
	 * 
	 * 此字段的版本 >= 0
	 * @return RateState value 类型为:long
	 * 
	 */
	public long getRateState()
	{
		return RateState;
	}


	/**
	 * 设置评价状态101-103
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRateState(long value)
	{
		this.RateState = value;
		this.RateState_u = 1;
	}

	public boolean issetRateState()
	{
		return this.RateState_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return RateState_u value 类型为:short
	 * 
	 */
	public short getRateState_u()
	{
		return RateState_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRateState_u(short value)
	{
		this.RateState_u = value;
	}


	/**
	 * 获取商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemTitle value 类型为:String
	 * 
	 */
	public String getItemTitle()
	{
		return ItemTitle;
	}


	/**
	 * 设置商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemTitle(String value)
	{
		this.ItemTitle = value;
		this.ItemTitle_u = 1;
	}

	public boolean issetItemTitle()
	{
		return this.ItemTitle_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemTitle_u value 类型为:short
	 * 
	 */
	public short getItemTitle_u()
	{
		return ItemTitle_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemTitle_u(short value)
	{
		this.ItemTitle_u = value;
	}


	/**
	 * 获取商品id
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemId value 类型为:String
	 * 
	 */
	public String getItemId()
	{
		return ItemId;
	}


	/**
	 * 设置商品id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemId(String value)
	{
		this.ItemId = value;
		this.ItemId_u = 1;
	}

	public boolean issetItemId()
	{
		return this.ItemId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemId_u value 类型为:short
	 * 
	 */
	public short getItemId_u()
	{
		return ItemId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemId_u(short value)
	{
		this.ItemId_u = value;
	}


	/**
	 * 获取历史订单标记 0 false 1 true
	 * 
	 * 此字段的版本 >= 0
	 * @return HistoryFlag value 类型为:long
	 * 
	 */
	public long getHistoryFlag()
	{
		return HistoryFlag;
	}


	/**
	 * 设置历史订单标记 0 false 1 true
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setHistoryFlag(long value)
	{
		this.HistoryFlag = value;
		this.HistoryFlag_u = 1;
	}

	public boolean issetHistoryFlag()
	{
		return this.HistoryFlag_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return HistoryFlag_u value 类型为:short
	 * 
	 */
	public short getHistoryFlag_u()
	{
		return HistoryFlag_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setHistoryFlag_u(short value)
	{
		this.HistoryFlag_u = value;
	}


	/**
	 * 获取历史库分set
	 * 
	 * 此字段的版本 >= 2
	 * @return SetX value 类型为:long
	 * 
	 */
	public long getSetX()
	{
		return SetX;
	}


	/**
	 * 设置历史库分set
	 * 
	 * 此字段的版本 >= 2
	 * @param  value 类型为:long
	 * 
	 */
	public void setSetX(long value)
	{
		this.SetX = value;
		this.SetX_u = 1;
	}

	public boolean issetSetX()
	{
		return this.SetX_u != 0;
	}
	/**
	 * 获取历史库分set
	 * 
	 * 此字段的版本 >= 2
	 * @return SetX_u value 类型为:short
	 * 
	 */
	public short getSetX_u()
	{
		return SetX_u;
	}


	/**
	 * 设置历史库分set
	 * 
	 * 此字段的版本 >= 2
	 * @param  value 类型为:short
	 * 
	 */
	public void setSetX_u(short value)
	{
		this.SetX_u = value;
	}


	/**
	 * 获取订单类型,0-所有,1-当期销售,4-店铺街
	 * 
	 * 此字段的版本 >= 3
	 * @return DealType value 类型为:long
	 * 
	 */
	public long getDealType()
	{
		return DealType;
	}


	/**
	 * 设置订单类型,0-所有,1-当期销售,4-店铺街
	 * 
	 * 此字段的版本 >= 3
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealType(long value)
	{
		this.DealType = value;
		this.DealType_u = 1;
	}

	public boolean issetDealType()
	{
		return this.DealType_u != 0;
	}
	/**
	 * 获取订单类型uflag
	 * 
	 * 此字段的版本 >= 3
	 * @return DealType_u value 类型为:short
	 * 
	 */
	public short getDealType_u()
	{
		return DealType_u;
	}


	/**
	 * 设置订单类型uflag
	 * 
	 * 此字段的版本 >= 3
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealType_u(short value)
	{
		this.DealType_u = value;
	}


	/**
	 * 获取支付类型（只适用于当前库，不能查历史库） ,0或不设-所有,1-线上支付,2-货到付款
	 * 
	 * 此字段的版本 >= 4
	 * @return DealPayType value 类型为:long
	 * 
	 */
	public long getDealPayType()
	{
		return DealPayType;
	}


	/**
	 * 设置支付类型（只适用于当前库，不能查历史库） ,0或不设-所有,1-线上支付,2-货到付款
	 * 
	 * 此字段的版本 >= 4
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealPayType(long value)
	{
		this.DealPayType = value;
		this.DealPayType_u = 1;
	}

	public boolean issetDealPayType()
	{
		return this.DealPayType_u != 0;
	}
	/**
	 * 获取支付类型uflag
	 * 
	 * 此字段的版本 >= 4
	 * @return DealPayType_u value 类型为:short
	 * 
	 */
	public short getDealPayType_u()
	{
		return DealPayType_u;
	}


	/**
	 * 设置支付类型uflag
	 * 
	 * 此字段的版本 >= 4
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealPayType_u(short value)
	{
		this.DealPayType_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CDealListReqPoNew)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(RecvName, null);  //计算字段RecvName的长度 size_of(String)
				length += 1;  //计算字段RecvName_u的长度 size_of(uint8_t)
				length += 4;  //计算字段InfoType的长度 size_of(uint32_t)
				length += 1;  //计算字段InfoType_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TimeType的长度 size_of(uint32_t)
				length += 1;  //计算字段TimeType_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TimeOrder的长度 size_of(uint32_t)
				length += 1;  //计算字段TimeOrder_u的长度 size_of(uint8_t)
				length += 4;  //计算字段NoteType的长度 size_of(uint32_t)
				length += 1;  //计算字段NoteType_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TimeStart的长度 size_of(uint32_t)
				length += 1;  //计算字段TimeStart_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TimeEnd的长度 size_of(uint32_t)
				length += 1;  //计算字段TimeEnd_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PageIndex的长度 size_of(uint32_t)
				length += 1;  //计算字段PageIndex_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PageSize的长度 size_of(uint32_t)
				length += 1;  //计算字段PageSize_u的长度 size_of(uint8_t)
				length += 4;  //计算字段DealState的长度 size_of(uint32_t)
				length += 1;  //计算字段DealState_u的长度 size_of(uint8_t)
				length += 4;  //计算字段RateState的长度 size_of(uint32_t)
				length += 1;  //计算字段RateState_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ItemTitle, null);  //计算字段ItemTitle的长度 size_of(String)
				length += 1;  //计算字段ItemTitle_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ItemId, null);  //计算字段ItemId的长度 size_of(String)
				length += 1;  //计算字段ItemId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段HistoryFlag的长度 size_of(uint32_t)
				length += 1;  //计算字段HistoryFlag_u的长度 size_of(uint8_t)
				if(  this.version >= 2 ){
						length += 4;  //计算字段SetX的长度 size_of(uint32_t)
				}
				if(  this.version >= 2 ){
						length += 1;  //计算字段SetX_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 3 ){
						length += 4;  //计算字段DealType的长度 size_of(uint32_t)
				}
				if(  this.version >= 3 ){
						length += 1;  //计算字段DealType_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 4 ){
						length += 4;  //计算字段DealPayType的长度 size_of(uint32_t)
				}
				if(  this.version >= 4 ){
						length += 1;  //计算字段DealPayType_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(CDealListReqPoNew)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(RecvName, encoding);  //计算字段RecvName的长度 size_of(String)
				length += 1;  //计算字段RecvName_u的长度 size_of(uint8_t)
				length += 4;  //计算字段InfoType的长度 size_of(uint32_t)
				length += 1;  //计算字段InfoType_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TimeType的长度 size_of(uint32_t)
				length += 1;  //计算字段TimeType_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TimeOrder的长度 size_of(uint32_t)
				length += 1;  //计算字段TimeOrder_u的长度 size_of(uint8_t)
				length += 4;  //计算字段NoteType的长度 size_of(uint32_t)
				length += 1;  //计算字段NoteType_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TimeStart的长度 size_of(uint32_t)
				length += 1;  //计算字段TimeStart_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TimeEnd的长度 size_of(uint32_t)
				length += 1;  //计算字段TimeEnd_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PageIndex的长度 size_of(uint32_t)
				length += 1;  //计算字段PageIndex_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PageSize的长度 size_of(uint32_t)
				length += 1;  //计算字段PageSize_u的长度 size_of(uint8_t)
				length += 4;  //计算字段DealState的长度 size_of(uint32_t)
				length += 1;  //计算字段DealState_u的长度 size_of(uint8_t)
				length += 4;  //计算字段RateState的长度 size_of(uint32_t)
				length += 1;  //计算字段RateState_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ItemTitle, encoding);  //计算字段ItemTitle的长度 size_of(String)
				length += 1;  //计算字段ItemTitle_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ItemId, encoding);  //计算字段ItemId的长度 size_of(String)
				length += 1;  //计算字段ItemId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段HistoryFlag的长度 size_of(uint32_t)
				length += 1;  //计算字段HistoryFlag_u的长度 size_of(uint8_t)
				if(  this.version >= 2 ){
						length += 4;  //计算字段SetX的长度 size_of(uint32_t)
				}
				if(  this.version >= 2 ){
						length += 1;  //计算字段SetX_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 3 ){
						length += 4;  //计算字段DealType的长度 size_of(uint32_t)
				}
				if(  this.version >= 3 ){
						length += 1;  //计算字段DealType_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 4 ){
						length += 4;  //计算字段DealPayType的长度 size_of(uint32_t)
				}
				if(  this.version >= 4 ){
						length += 1;  //计算字段DealPayType_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本2所包含的字段*******
 *	long version;///<版本号
 *	short version_u;
 *	long SellerUin;///<卖家号
 *	short SellerUin_u;
 *	long BuyerUin;///<买家号
 *	short BuyerUin_u;
 *	String RecvName;///<收货人
 *	short RecvName_u;
 *	long InfoType;///<需要拉取的订单信息类型，请参照文档设置对应值，不要过多拉取无用信息，影响大家的性能
 *	short InfoType_u;
 *	long TimeType;///<时间类型
 *	short TimeType_u;
 *	long TimeOrder;///<排序类型 0升序 1降序
 *	short TimeOrder_u;
 *	long NoteType;///<备注类型1-5
 *	short NoteType_u;
 *	long TimeStart;///<起始时间
 *	short TimeStart_u;
 *	long TimeEnd;///<结束时间
 *	short TimeEnd_u;
 *	long PageIndex;///<页码，第一页请填 1
 *	short PageIndex_u;
 *	long PageSize;///<每页数量，请填写1-20, 默认为20
 *	short PageSize_u;
 *	long DealState;///<订单状态
 *	short DealState_u;
 *	long RateState;///<评价状态101-103
 *	short RateState_u;
 *	String ItemTitle;///<商品名称
 *	short ItemTitle_u;
 *	String ItemId;///<商品id
 *	short ItemId_u;
 *	long HistoryFlag;///<历史订单标记 0 false 1 true
 *	short HistoryFlag_u;
 *	long SetX;///<历史库分set
 *	short SetX_u;///<历史库分set
 *****以上是版本2所包含的字段*******
 *
 *****以下是版本3所包含的字段*******
 *	long version;///<版本号
 *	short version_u;
 *	long SellerUin;///<卖家号
 *	short SellerUin_u;
 *	long BuyerUin;///<买家号
 *	short BuyerUin_u;
 *	String RecvName;///<收货人
 *	short RecvName_u;
 *	long InfoType;///<需要拉取的订单信息类型，请参照文档设置对应值，不要过多拉取无用信息，影响大家的性能
 *	short InfoType_u;
 *	long TimeType;///<时间类型
 *	short TimeType_u;
 *	long TimeOrder;///<排序类型 0升序 1降序
 *	short TimeOrder_u;
 *	long NoteType;///<备注类型1-5
 *	short NoteType_u;
 *	long TimeStart;///<起始时间
 *	short TimeStart_u;
 *	long TimeEnd;///<结束时间
 *	short TimeEnd_u;
 *	long PageIndex;///<页码，第一页请填 1
 *	short PageIndex_u;
 *	long PageSize;///<每页数量，请填写1-20, 默认为20
 *	short PageSize_u;
 *	long DealState;///<订单状态
 *	short DealState_u;
 *	long RateState;///<评价状态101-103
 *	short RateState_u;
 *	String ItemTitle;///<商品名称
 *	short ItemTitle_u;
 *	String ItemId;///<商品id
 *	short ItemId_u;
 *	long HistoryFlag;///<历史订单标记 0 false 1 true
 *	short HistoryFlag_u;
 *	long SetX;///<历史库分set
 *	short SetX_u;///<历史库分set
 *	long DealType;///<订单类型,0-所有,1-当期销售,4-店铺街
 *	short DealType_u;///<订单类型uflag
 *****以上是版本3所包含的字段*******
 *
 *****以下是版本4所包含的字段*******
 *	long version;///<版本号
 *	short version_u;
 *	long SellerUin;///<卖家号
 *	short SellerUin_u;
 *	long BuyerUin;///<买家号
 *	short BuyerUin_u;
 *	String RecvName;///<收货人
 *	short RecvName_u;
 *	long InfoType;///<需要拉取的订单信息类型，请参照文档设置对应值，不要过多拉取无用信息，影响大家的性能
 *	short InfoType_u;
 *	long TimeType;///<时间类型
 *	short TimeType_u;
 *	long TimeOrder;///<排序类型 0升序 1降序
 *	short TimeOrder_u;
 *	long NoteType;///<备注类型1-5
 *	short NoteType_u;
 *	long TimeStart;///<起始时间
 *	short TimeStart_u;
 *	long TimeEnd;///<结束时间
 *	short TimeEnd_u;
 *	long PageIndex;///<页码，第一页请填 1
 *	short PageIndex_u;
 *	long PageSize;///<每页数量，请填写1-20, 默认为20
 *	short PageSize_u;
 *	long DealState;///<订单状态
 *	short DealState_u;
 *	long RateState;///<评价状态101-103
 *	short RateState_u;
 *	String ItemTitle;///<商品名称
 *	short ItemTitle_u;
 *	String ItemId;///<商品id
 *	short ItemId_u;
 *	long HistoryFlag;///<历史订单标记 0 false 1 true
 *	short HistoryFlag_u;
 *	long SetX;///<历史库分set
 *	short SetX_u;///<历史库分set
 *	long DealType;///<订单类型,0-所有,1-当期销售,4-店铺街
 *	short DealType_u;///<订单类型uflag
 *	long DealPayType;///<支付类型（只适用于当前库，不能查历史库） ,0或不设-所有,1-线上支付,2-货到付款
 *	short DealPayType_u;///<支付类型uflag
 *****以上是版本4所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
