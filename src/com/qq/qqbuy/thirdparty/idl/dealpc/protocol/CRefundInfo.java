//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.deelpc.protocol.CTradoInfo.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *退款信息
 *
 *@date 2015-03-04 11:46:41
 *
 *@since version:0
*/
public class CRefundInfo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 子订单编号
	 *
	 * 版本 >= 0
	 */
	 private long TradeId;

	/**
	 * 订单编号
	 *
	 * 版本 >= 0
	 */
	 private long DealId;

	/**
	 * 退款协议ID
	 *
	 * 版本 >= 0
	 */
	 private long TradeRefundId;

	/**
	 * 退款申请时间
	 *
	 * 版本 >= 0
	 */
	 private long RefundReqTime;

	/**
	 * 状态
	 *
	 * 版本 >= 0
	 */
	 private long RefundState;

	/**
	 * 预状态
	 *
	 * 版本 >= 0
	 */
	 private long PreRefundState;

	/**
	 * 货物状态（买家选的“是否收到货”,0:没有收到货，1.已经收到货）
	 *
	 * 版本 >= 0
	 */
	 private long RefundItemState;

	/**
	 * 是否需要退货，1需要退货，0不需要退货，
	 *
	 * 版本 >= 0
	 */
	 private long RefundReqitemFlag;

	/**
	 * 退还买家数量
	 *
	 * 版本 >= 0
	 */
	 private long RefundToBuyerNum;

	/**
	 * 申请退还买家金额
	 *
	 * 版本 >= 0
	 */
	 private long RefundToBuyer;

	/**
	 * 支付给卖家金额
	 *
	 * 版本 >= 0
	 */
	 private long RefundToSeller;

	/**
	 * 退款原因类型
	 *
	 * 版本 >= 0
	 */
	 private long RefundReasonType;

	/**
	 * 卖家同意退货时间
	 *
	 * 版本 >= 0
	 */
	 private long SellerAgreeGivebackTime;

	/**
	 * 买家发送退货时间
	 *
	 * 版本 >= 0
	 */
	 private long BuyerConsignmentTime;

	/**
	 * 退款结束时间
	 *
	 * 版本 >= 0
	 */
	 private long RefundEndTime;

	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long LastUpdateTime;

	/**
	 * 子单属性位
	 *
	 * 版本 >= 0
	 */
	 private long TradePropertymask;

	/**
	 * 卖家号
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * 订单创建时间
	 *
	 * 版本 >= 0
	 */
	 private long DealCreateTime;

	/**
	 * 卖家拒绝时间
	 *
	 * 版本 >= 0
	 */
	 private long SellerRefuseTime;

	/**
	 * 超时标志
	 *
	 * 版本 >= 0
	 */
	 private long TimeoutItemFlag;

	/**
	 * 退款原因描述
	 *
	 * 版本 >= 0
	 */
	 private String RefundReasonDesc = new String();

	/**
	 * 买家发送退货物流信息
	 *
	 * 版本 >= 0
	 */
	 private String BuyerConsignmentWuliu = new String();

	/**
	 * 买家发送退货描述
	 *
	 * 版本 >= 0
	 */
	 private String BuyerConsignmentDesc = new String();

	/**
	 * 卖家退货地址
	 *
	 * 版本 >= 0
	 */
	 private String SellerRefundAddr = new String();

	/**
	 * 卖家同意退款附言
	 *
	 * 版本 >= 0
	 */
	 private String SellerAgreeMsg = new String();

	/**
	 * 卖家同意退货附言
	 *
	 * 版本 >= 0
	 */
	 private String SellerAgreeItemMsg = new String();

	/**
	 * 子订单超时商品标识
	 *
	 * 版本 >= 0
	 */
	 private String Reserve = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushLong(TradeId);
		bs.pushLong(DealId);
		bs.pushLong(TradeRefundId);
		bs.pushUInt(RefundReqTime);
		bs.pushUInt(RefundState);
		bs.pushUInt(PreRefundState);
		bs.pushUInt(RefundItemState);
		bs.pushUInt(RefundReqitemFlag);
		bs.pushUInt(RefundToBuyerNum);
		bs.pushUInt(RefundToBuyer);
		bs.pushUInt(RefundToSeller);
		bs.pushUInt(RefundReasonType);
		bs.pushUInt(SellerAgreeGivebackTime);
		bs.pushUInt(BuyerConsignmentTime);
		bs.pushUInt(RefundEndTime);
		bs.pushUInt(version);
		bs.pushUInt(LastUpdateTime);
		bs.pushUInt(TradePropertymask);
		bs.pushUInt(SellerUin);
		bs.pushUInt(DealCreateTime);
		bs.pushUInt(SellerRefuseTime);
		bs.pushUInt(TimeoutItemFlag);
		bs.pushString(RefundReasonDesc);
		bs.pushString(BuyerConsignmentWuliu);
		bs.pushString(BuyerConsignmentDesc);
		bs.pushString(SellerRefundAddr);
		bs.pushString(SellerAgreeMsg);
		bs.pushString(SellerAgreeItemMsg);
		bs.pushString(Reserve);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		TradeId = bs.popLong();
		DealId = bs.popLong();
		TradeRefundId = bs.popLong();
		RefundReqTime = bs.popUInt();
		RefundState = bs.popUInt();
		PreRefundState = bs.popUInt();
		RefundItemState = bs.popUInt();
		RefundReqitemFlag = bs.popUInt();
		RefundToBuyerNum = bs.popUInt();
		RefundToBuyer = bs.popUInt();
		RefundToSeller = bs.popUInt();
		RefundReasonType = bs.popUInt();
		SellerAgreeGivebackTime = bs.popUInt();
		BuyerConsignmentTime = bs.popUInt();
		RefundEndTime = bs.popUInt();
		version = bs.popUInt();
		LastUpdateTime = bs.popUInt();
		TradePropertymask = bs.popUInt();
		SellerUin = bs.popUInt();
		DealCreateTime = bs.popUInt();
		SellerRefuseTime = bs.popUInt();
		TimeoutItemFlag = bs.popUInt();
		RefundReasonDesc = bs.popString();
		BuyerConsignmentWuliu = bs.popString();
		BuyerConsignmentDesc = bs.popString();
		SellerRefundAddr = bs.popString();
		SellerAgreeMsg = bs.popString();
		SellerAgreeItemMsg = bs.popString();
		Reserve = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取子订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @return TradeId value 类型为:long
	 * 
	 */
	public long getTradeId()
	{
		return TradeId;
	}


	/**
	 * 设置子订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTradeId(long value)
	{
		this.TradeId = value;
	}


	/**
	 * 获取订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:long
	 * 
	 */
	public long getDealId()
	{
		return DealId;
	}


	/**
	 * 设置订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealId(long value)
	{
		this.DealId = value;
	}


	/**
	 * 获取退款协议ID
	 * 
	 * 此字段的版本 >= 0
	 * @return TradeRefundId value 类型为:long
	 * 
	 */
	public long getTradeRefundId()
	{
		return TradeRefundId;
	}


	/**
	 * 设置退款协议ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTradeRefundId(long value)
	{
		this.TradeRefundId = value;
	}


	/**
	 * 获取退款申请时间
	 * 
	 * 此字段的版本 >= 0
	 * @return RefundReqTime value 类型为:long
	 * 
	 */
	public long getRefundReqTime()
	{
		return RefundReqTime;
	}


	/**
	 * 设置退款申请时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRefundReqTime(long value)
	{
		this.RefundReqTime = value;
	}


	/**
	 * 获取状态
	 * 
	 * 此字段的版本 >= 0
	 * @return RefundState value 类型为:long
	 * 
	 */
	public long getRefundState()
	{
		return RefundState;
	}


	/**
	 * 设置状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRefundState(long value)
	{
		this.RefundState = value;
	}


	/**
	 * 获取预状态
	 * 
	 * 此字段的版本 >= 0
	 * @return PreRefundState value 类型为:long
	 * 
	 */
	public long getPreRefundState()
	{
		return PreRefundState;
	}


	/**
	 * 设置预状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPreRefundState(long value)
	{
		this.PreRefundState = value;
	}


	/**
	 * 获取货物状态（买家选的“是否收到货”,0:没有收到货，1.已经收到货）
	 * 
	 * 此字段的版本 >= 0
	 * @return RefundItemState value 类型为:long
	 * 
	 */
	public long getRefundItemState()
	{
		return RefundItemState;
	}


	/**
	 * 设置货物状态（买家选的“是否收到货”,0:没有收到货，1.已经收到货）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRefundItemState(long value)
	{
		this.RefundItemState = value;
	}


	/**
	 * 获取是否需要退货，1需要退货，0不需要退货，
	 * 
	 * 此字段的版本 >= 0
	 * @return RefundReqitemFlag value 类型为:long
	 * 
	 */
	public long getRefundReqitemFlag()
	{
		return RefundReqitemFlag;
	}


	/**
	 * 设置是否需要退货，1需要退货，0不需要退货，
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRefundReqitemFlag(long value)
	{
		this.RefundReqitemFlag = value;
	}


	/**
	 * 获取退还买家数量
	 * 
	 * 此字段的版本 >= 0
	 * @return RefundToBuyerNum value 类型为:long
	 * 
	 */
	public long getRefundToBuyerNum()
	{
		return RefundToBuyerNum;
	}


	/**
	 * 设置退还买家数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRefundToBuyerNum(long value)
	{
		this.RefundToBuyerNum = value;
	}


	/**
	 * 获取申请退还买家金额
	 * 
	 * 此字段的版本 >= 0
	 * @return RefundToBuyer value 类型为:long
	 * 
	 */
	public long getRefundToBuyer()
	{
		return RefundToBuyer;
	}


	/**
	 * 设置申请退还买家金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRefundToBuyer(long value)
	{
		this.RefundToBuyer = value;
	}


	/**
	 * 获取支付给卖家金额
	 * 
	 * 此字段的版本 >= 0
	 * @return RefundToSeller value 类型为:long
	 * 
	 */
	public long getRefundToSeller()
	{
		return RefundToSeller;
	}


	/**
	 * 设置支付给卖家金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRefundToSeller(long value)
	{
		this.RefundToSeller = value;
	}


	/**
	 * 获取退款原因类型
	 * 
	 * 此字段的版本 >= 0
	 * @return RefundReasonType value 类型为:long
	 * 
	 */
	public long getRefundReasonType()
	{
		return RefundReasonType;
	}


	/**
	 * 设置退款原因类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRefundReasonType(long value)
	{
		this.RefundReasonType = value;
	}


	/**
	 * 获取卖家同意退货时间
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerAgreeGivebackTime value 类型为:long
	 * 
	 */
	public long getSellerAgreeGivebackTime()
	{
		return SellerAgreeGivebackTime;
	}


	/**
	 * 设置卖家同意退货时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerAgreeGivebackTime(long value)
	{
		this.SellerAgreeGivebackTime = value;
	}


	/**
	 * 获取买家发送退货时间
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerConsignmentTime value 类型为:long
	 * 
	 */
	public long getBuyerConsignmentTime()
	{
		return BuyerConsignmentTime;
	}


	/**
	 * 设置买家发送退货时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerConsignmentTime(long value)
	{
		this.BuyerConsignmentTime = value;
	}


	/**
	 * 获取退款结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return RefundEndTime value 类型为:long
	 * 
	 */
	public long getRefundEndTime()
	{
		return RefundEndTime;
	}


	/**
	 * 设置退款结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRefundEndTime(long value)
	{
		this.RefundEndTime = value;
	}


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return LastUpdateTime value 类型为:long
	 * 
	 */
	public long getLastUpdateTime()
	{
		return LastUpdateTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastUpdateTime(long value)
	{
		this.LastUpdateTime = value;
	}


	/**
	 * 获取子单属性位
	 * 
	 * 此字段的版本 >= 0
	 * @return TradePropertymask value 类型为:long
	 * 
	 */
	public long getTradePropertymask()
	{
		return TradePropertymask;
	}


	/**
	 * 设置子单属性位
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTradePropertymask(long value)
	{
		this.TradePropertymask = value;
	}


	/**
	 * 获取卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
	}


	/**
	 * 获取订单创建时间
	 * 
	 * 此字段的版本 >= 0
	 * @return DealCreateTime value 类型为:long
	 * 
	 */
	public long getDealCreateTime()
	{
		return DealCreateTime;
	}


	/**
	 * 设置订单创建时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealCreateTime(long value)
	{
		this.DealCreateTime = value;
	}


	/**
	 * 获取卖家拒绝时间
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerRefuseTime value 类型为:long
	 * 
	 */
	public long getSellerRefuseTime()
	{
		return SellerRefuseTime;
	}


	/**
	 * 设置卖家拒绝时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerRefuseTime(long value)
	{
		this.SellerRefuseTime = value;
	}


	/**
	 * 获取超时标志
	 * 
	 * 此字段的版本 >= 0
	 * @return TimeoutItemFlag value 类型为:long
	 * 
	 */
	public long getTimeoutItemFlag()
	{
		return TimeoutItemFlag;
	}


	/**
	 * 设置超时标志
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTimeoutItemFlag(long value)
	{
		this.TimeoutItemFlag = value;
	}


	/**
	 * 获取退款原因描述
	 * 
	 * 此字段的版本 >= 0
	 * @return RefundReasonDesc value 类型为:String
	 * 
	 */
	public String getRefundReasonDesc()
	{
		return RefundReasonDesc;
	}


	/**
	 * 设置退款原因描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRefundReasonDesc(String value)
	{
		this.RefundReasonDesc = value;
	}


	/**
	 * 获取买家发送退货物流信息
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerConsignmentWuliu value 类型为:String
	 * 
	 */
	public String getBuyerConsignmentWuliu()
	{
		return BuyerConsignmentWuliu;
	}


	/**
	 * 设置买家发送退货物流信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBuyerConsignmentWuliu(String value)
	{
		this.BuyerConsignmentWuliu = value;
	}


	/**
	 * 获取买家发送退货描述
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerConsignmentDesc value 类型为:String
	 * 
	 */
	public String getBuyerConsignmentDesc()
	{
		return BuyerConsignmentDesc;
	}


	/**
	 * 设置买家发送退货描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBuyerConsignmentDesc(String value)
	{
		this.BuyerConsignmentDesc = value;
	}


	/**
	 * 获取卖家退货地址
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerRefundAddr value 类型为:String
	 * 
	 */
	public String getSellerRefundAddr()
	{
		return SellerRefundAddr;
	}


	/**
	 * 设置卖家退货地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSellerRefundAddr(String value)
	{
		this.SellerRefundAddr = value;
	}


	/**
	 * 获取卖家同意退款附言
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerAgreeMsg value 类型为:String
	 * 
	 */
	public String getSellerAgreeMsg()
	{
		return SellerAgreeMsg;
	}


	/**
	 * 设置卖家同意退款附言
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSellerAgreeMsg(String value)
	{
		this.SellerAgreeMsg = value;
	}


	/**
	 * 获取卖家同意退货附言
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerAgreeItemMsg value 类型为:String
	 * 
	 */
	public String getSellerAgreeItemMsg()
	{
		return SellerAgreeItemMsg;
	}


	/**
	 * 设置卖家同意退货附言
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSellerAgreeItemMsg(String value)
	{
		this.SellerAgreeItemMsg = value;
	}


	/**
	 * 获取子订单超时商品标识
	 * 
	 * 此字段的版本 >= 0
	 * @return Reserve value 类型为:String
	 * 
	 */
	public String getReserve()
	{
		return Reserve;
	}


	/**
	 * 设置子订单超时商品标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserve(String value)
	{
		this.Reserve = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CRefundInfo)
				length += 17;  //计算字段TradeId的长度 size_of(uint64_t)
				length += 17;  //计算字段DealId的长度 size_of(uint64_t)
				length += 17;  //计算字段TradeRefundId的长度 size_of(uint64_t)
				length += 4;  //计算字段RefundReqTime的长度 size_of(uint32_t)
				length += 4;  //计算字段RefundState的长度 size_of(uint32_t)
				length += 4;  //计算字段PreRefundState的长度 size_of(uint32_t)
				length += 4;  //计算字段RefundItemState的长度 size_of(uint32_t)
				length += 4;  //计算字段RefundReqitemFlag的长度 size_of(uint32_t)
				length += 4;  //计算字段RefundToBuyerNum的长度 size_of(uint32_t)
				length += 4;  //计算字段RefundToBuyer的长度 size_of(uint32_t)
				length += 4;  //计算字段RefundToSeller的长度 size_of(uint32_t)
				length += 4;  //计算字段RefundReasonType的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerAgreeGivebackTime的长度 size_of(uint32_t)
				length += 4;  //计算字段BuyerConsignmentTime的长度 size_of(uint32_t)
				length += 4;  //计算字段RefundEndTime的长度 size_of(uint32_t)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段LastUpdateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段TradePropertymask的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段DealCreateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerRefuseTime的长度 size_of(uint32_t)
				length += 4;  //计算字段TimeoutItemFlag的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(RefundReasonDesc, null);  //计算字段RefundReasonDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(BuyerConsignmentWuliu, null);  //计算字段BuyerConsignmentWuliu的长度 size_of(String)
				length += ByteStream.getObjectSize(BuyerConsignmentDesc, null);  //计算字段BuyerConsignmentDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(SellerRefundAddr, null);  //计算字段SellerRefundAddr的长度 size_of(String)
				length += ByteStream.getObjectSize(SellerAgreeMsg, null);  //计算字段SellerAgreeMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(SellerAgreeItemMsg, null);  //计算字段SellerAgreeItemMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(Reserve, null);  //计算字段Reserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(CRefundInfo)
				length += 17;  //计算字段TradeId的长度 size_of(uint64_t)
				length += 17;  //计算字段DealId的长度 size_of(uint64_t)
				length += 17;  //计算字段TradeRefundId的长度 size_of(uint64_t)
				length += 4;  //计算字段RefundReqTime的长度 size_of(uint32_t)
				length += 4;  //计算字段RefundState的长度 size_of(uint32_t)
				length += 4;  //计算字段PreRefundState的长度 size_of(uint32_t)
				length += 4;  //计算字段RefundItemState的长度 size_of(uint32_t)
				length += 4;  //计算字段RefundReqitemFlag的长度 size_of(uint32_t)
				length += 4;  //计算字段RefundToBuyerNum的长度 size_of(uint32_t)
				length += 4;  //计算字段RefundToBuyer的长度 size_of(uint32_t)
				length += 4;  //计算字段RefundToSeller的长度 size_of(uint32_t)
				length += 4;  //计算字段RefundReasonType的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerAgreeGivebackTime的长度 size_of(uint32_t)
				length += 4;  //计算字段BuyerConsignmentTime的长度 size_of(uint32_t)
				length += 4;  //计算字段RefundEndTime的长度 size_of(uint32_t)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段LastUpdateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段TradePropertymask的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段DealCreateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerRefuseTime的长度 size_of(uint32_t)
				length += 4;  //计算字段TimeoutItemFlag的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(RefundReasonDesc, encoding);  //计算字段RefundReasonDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(BuyerConsignmentWuliu, encoding);  //计算字段BuyerConsignmentWuliu的长度 size_of(String)
				length += ByteStream.getObjectSize(BuyerConsignmentDesc, encoding);  //计算字段BuyerConsignmentDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(SellerRefundAddr, encoding);  //计算字段SellerRefundAddr的长度 size_of(String)
				length += ByteStream.getObjectSize(SellerAgreeMsg, encoding);  //计算字段SellerAgreeMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(SellerAgreeItemMsg, encoding);  //计算字段SellerAgreeItemMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(Reserve, encoding);  //计算字段Reserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
