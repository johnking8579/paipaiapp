//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.deelpc.protocol.DealIdl.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *退款信息
 *
 *@date 2015-03-04 11:46:41
 *
 *@since version:0
*/
public class CActionLog  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 订单操作者类别
	 *
	 * 版本 >= 0
	 */
	 private short OperatorType;

	/**
	 * 操作类型
	 *
	 * 版本 >= 0
	 */
	 private short OperationType;

	/**
	 * 操作前订单状态	
	 *
	 * 版本 >= 0
	 */
	 private short FromState;

	/**
	 * 操作后订单状态
	 *
	 * 版本 >= 0
	 */
	 private short ToState;

	/**
	 * 订单流水编号
	 *
	 * 版本 >= 0
	 */
	 private long DealLogId;

	/**
	 * 订单编号
	 *
	 * 版本 >= 0
	 */
	 private long DealId;

	/**
	 * 子订单编号
	 *
	 * 版本 >= 0
	 */
	 private long TradeId;

	/**
	 * 	版本号	
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 订单生成时间(备用)
	 *
	 * 版本 >= 0
	 */
	 private long DealCreateTime;

	/**
	 * 操作时间
	 *
	 * 版本 >= 0
	 */
	 private long OperateTime;

	/**
	 * 动作描述
	 *
	 * 版本 >= 0
	 */
	 private String OperationDesc = new String();

	/**
	 * 用户IP
	 *
	 * 版本 >= 0
	 */
	 private String UserIp = new String();

	/**
	 * 订单状态变更备注
	 *
	 * 版本 >= 0
	 */
	 private String UserRemark = new String();

	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String UserMachineKey = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUByte(OperatorType);
		bs.pushUByte(OperationType);
		bs.pushUByte(FromState);
		bs.pushUByte(ToState);
		bs.pushLong(DealLogId);
		bs.pushLong(DealId);
		bs.pushLong(TradeId);
		bs.pushUInt(version);
		bs.pushUInt(DealCreateTime);
		bs.pushUInt(OperateTime);
		bs.pushString(OperationDesc);
		bs.pushString(UserIp);
		bs.pushString(UserRemark);
		bs.pushString(UserMachineKey);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		OperatorType = bs.popUByte();
		OperationType = bs.popUByte();
		FromState = bs.popUByte();
		ToState = bs.popUByte();
		DealLogId = bs.popLong();
		DealId = bs.popLong();
		TradeId = bs.popLong();
		version = bs.popUInt();
		DealCreateTime = bs.popUInt();
		OperateTime = bs.popUInt();
		OperationDesc = bs.popString();
		UserIp = bs.popString();
		UserRemark = bs.popString();
		UserMachineKey = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取订单操作者类别
	 * 
	 * 此字段的版本 >= 0
	 * @return OperatorType value 类型为:short
	 * 
	 */
	public short getOperatorType()
	{
		return OperatorType;
	}


	/**
	 * 设置订单操作者类别
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOperatorType(short value)
	{
		this.OperatorType = value;
	}


	/**
	 * 获取操作类型
	 * 
	 * 此字段的版本 >= 0
	 * @return OperationType value 类型为:short
	 * 
	 */
	public short getOperationType()
	{
		return OperationType;
	}


	/**
	 * 设置操作类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOperationType(short value)
	{
		this.OperationType = value;
	}


	/**
	 * 获取操作前订单状态	
	 * 
	 * 此字段的版本 >= 0
	 * @return FromState value 类型为:short
	 * 
	 */
	public short getFromState()
	{
		return FromState;
	}


	/**
	 * 设置操作前订单状态	
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFromState(short value)
	{
		this.FromState = value;
	}


	/**
	 * 获取操作后订单状态
	 * 
	 * 此字段的版本 >= 0
	 * @return ToState value 类型为:short
	 * 
	 */
	public short getToState()
	{
		return ToState;
	}


	/**
	 * 设置操作后订单状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setToState(short value)
	{
		this.ToState = value;
	}


	/**
	 * 获取订单流水编号
	 * 
	 * 此字段的版本 >= 0
	 * @return DealLogId value 类型为:long
	 * 
	 */
	public long getDealLogId()
	{
		return DealLogId;
	}


	/**
	 * 设置订单流水编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealLogId(long value)
	{
		this.DealLogId = value;
	}


	/**
	 * 获取订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:long
	 * 
	 */
	public long getDealId()
	{
		return DealId;
	}


	/**
	 * 设置订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealId(long value)
	{
		this.DealId = value;
	}


	/**
	 * 获取子订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @return TradeId value 类型为:long
	 * 
	 */
	public long getTradeId()
	{
		return TradeId;
	}


	/**
	 * 设置子订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTradeId(long value)
	{
		this.TradeId = value;
	}


	/**
	 * 获取	版本号	
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置	版本号	
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取订单生成时间(备用)
	 * 
	 * 此字段的版本 >= 0
	 * @return DealCreateTime value 类型为:long
	 * 
	 */
	public long getDealCreateTime()
	{
		return DealCreateTime;
	}


	/**
	 * 设置订单生成时间(备用)
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealCreateTime(long value)
	{
		this.DealCreateTime = value;
	}


	/**
	 * 获取操作时间
	 * 
	 * 此字段的版本 >= 0
	 * @return OperateTime value 类型为:long
	 * 
	 */
	public long getOperateTime()
	{
		return OperateTime;
	}


	/**
	 * 设置操作时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOperateTime(long value)
	{
		this.OperateTime = value;
	}


	/**
	 * 获取动作描述
	 * 
	 * 此字段的版本 >= 0
	 * @return OperationDesc value 类型为:String
	 * 
	 */
	public String getOperationDesc()
	{
		return OperationDesc;
	}


	/**
	 * 设置动作描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOperationDesc(String value)
	{
		this.OperationDesc = value;
	}


	/**
	 * 获取用户IP
	 * 
	 * 此字段的版本 >= 0
	 * @return UserIp value 类型为:String
	 * 
	 */
	public String getUserIp()
	{
		return UserIp;
	}


	/**
	 * 设置用户IP
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setUserIp(String value)
	{
		this.UserIp = value;
	}


	/**
	 * 获取订单状态变更备注
	 * 
	 * 此字段的版本 >= 0
	 * @return UserRemark value 类型为:String
	 * 
	 */
	public String getUserRemark()
	{
		return UserRemark;
	}


	/**
	 * 设置订单状态变更备注
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setUserRemark(String value)
	{
		this.UserRemark = value;
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return UserMachineKey value 类型为:String
	 * 
	 */
	public String getUserMachineKey()
	{
		return UserMachineKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setUserMachineKey(String value)
	{
		this.UserMachineKey = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CActionLog)
				length += 1;  //计算字段OperatorType的长度 size_of(uint8_t)
				length += 1;  //计算字段OperationType的长度 size_of(uint8_t)
				length += 1;  //计算字段FromState的长度 size_of(uint8_t)
				length += 1;  //计算字段ToState的长度 size_of(uint8_t)
				length += 17;  //计算字段DealLogId的长度 size_of(uint64_t)
				length += 17;  //计算字段DealId的长度 size_of(uint64_t)
				length += 17;  //计算字段TradeId的长度 size_of(uint64_t)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段DealCreateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段OperateTime的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(OperationDesc, null);  //计算字段OperationDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(UserIp, null);  //计算字段UserIp的长度 size_of(String)
				length += ByteStream.getObjectSize(UserRemark, null);  //计算字段UserRemark的长度 size_of(String)
				length += ByteStream.getObjectSize(UserMachineKey, null);  //计算字段UserMachineKey的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(CActionLog)
				length += 1;  //计算字段OperatorType的长度 size_of(uint8_t)
				length += 1;  //计算字段OperationType的长度 size_of(uint8_t)
				length += 1;  //计算字段FromState的长度 size_of(uint8_t)
				length += 1;  //计算字段ToState的长度 size_of(uint8_t)
				length += 17;  //计算字段DealLogId的长度 size_of(uint64_t)
				length += 17;  //计算字段DealId的长度 size_of(uint64_t)
				length += 17;  //计算字段TradeId的长度 size_of(uint64_t)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段DealCreateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段OperateTime的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(OperationDesc, encoding);  //计算字段OperationDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(UserIp, encoding);  //计算字段UserIp的长度 size_of(String)
				length += ByteStream.getObjectSize(UserRemark, encoding);  //计算字段UserRemark的长度 size_of(String)
				length += ByteStream.getObjectSize(UserMachineKey, encoding);  //计算字段UserMachineKey的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
