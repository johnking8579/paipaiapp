 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.EmployeeAo.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Map;
import com.paipai.lang.uint32_t;
import java.util.HashMap;

/**
 *查询员工时候具有某种权限返回类
 *
 *@date 2014-12-16 05:53:03
 *
 *@since version:0
*/
public class  CheckAuthListResp implements IServiceObject
{
	public long result;
	/**
	 * 查询结果
	 *
	 * 版本 >= 0
	 */
	 private Map<uint32_t,uint32_t> passFlag = new HashMap<uint32_t,uint32_t>();

	/**
	 * 错误提示
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 输出保留字
	 *
	 * 版本 >= 0
	 */
	 private String outReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(passFlag);
		bs.pushString(errMsg);
		bs.pushString(outReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		passFlag = (Map<uint32_t,uint32_t>)bs.popMap(uint32_t.class,uint32_t.class);
		errMsg = bs.popString();
		outReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xf1928816L;
	}


	/**
	 * 获取查询结果
	 * 
	 * 此字段的版本 >= 0
	 * @return passFlag value 类型为:Map<uint32_t,uint32_t>
	 * 
	 */
	public Map<uint32_t,uint32_t> getPassFlag()
	{
		return passFlag;
	}


	/**
	 * 设置查询结果
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<uint32_t,uint32_t>
	 * 
	 */
	public void setPassFlag(Map<uint32_t,uint32_t> value)
	{
		if (value != null) {
				this.passFlag = value;
		}else{
				this.passFlag = new HashMap<uint32_t,uint32_t>();
		}
	}


	/**
	 * 获取错误提示
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误提示
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	/**
	 * 获取输出保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return outReserve;
	}


	/**
	 * 设置输出保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		this.outReserve = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CheckAuthListResp)
				length += ByteStream.getObjectSize(passFlag, null);  //计算字段passFlag的长度 size_of(Map)
				length += ByteStream.getObjectSize(errMsg, null);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserve, null);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(CheckAuthListResp)
				length += ByteStream.getObjectSize(passFlag, encoding);  //计算字段passFlag的长度 size_of(Map)
				length += ByteStream.getObjectSize(errMsg, encoding);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserve, encoding);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
