 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.deelpc.protocol.DealIdl.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import com.paipai.lang.uint64_t;
import java.util.Vector;

/**
 *获取订单drawid
 *
 *@date 2015-03-04 11:46:41
 *
 *@since version:0
*/
public class  GetDealDrawIdReq extends NetMessage
{
	/**
	 * 调用者==>请设置为源文件名
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 拍拍单号
	 *
	 * 版本 >= 0
	 */
	 private String DealId = new String();

	/**
	 * cft单号
	 *
	 * 版本 >= 0
	 */
	 private String PayId = new String();

	/**
	 * 卖家Uin
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * daawid类型，确认收货设113，退款设112
	 *
	 * 版本 >= 0
	 */
	 private long GenType;

	/**
	 * 要生成drawid的子单id列表，即要确认收货的子单id列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint64_t> TradeIdList = new Vector<uint64_t>();

	/**
	 * 订单信息
	 *
	 * 版本 >= 0
	 */
	 private String MachineKey = new String();

	/**
	 * 保留输入字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveIn = new String();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(Source);
		bs.pushString(DealId);
		bs.pushString(PayId);
		bs.pushUInt(SellerUin);
		bs.pushUInt(GenType);
		bs.pushObject(TradeIdList);
		bs.pushString(MachineKey);
		bs.pushString(ReserveIn);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		Source = bs.popString();
		DealId = bs.popString();
		PayId = bs.popString();
		SellerUin = bs.popUInt();
		GenType = bs.popUInt();
		TradeIdList = (Vector<uint64_t>)bs.popVector(uint64_t.class);
		MachineKey = bs.popString();
		ReserveIn = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x2630180dL;
	}


	/**
	 * 获取调用者==>请设置为源文件名
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置调用者==>请设置为源文件名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取拍拍单号
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:String
	 * 
	 */
	public String getDealId()
	{
		return DealId;
	}


	/**
	 * 设置拍拍单号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealId(String value)
	{
		this.DealId = value;
	}


	/**
	 * 获取cft单号
	 * 
	 * 此字段的版本 >= 0
	 * @return PayId value 类型为:String
	 * 
	 */
	public String getPayId()
	{
		return PayId;
	}


	/**
	 * 设置cft单号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPayId(String value)
	{
		this.PayId = value;
	}


	/**
	 * 获取卖家Uin
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置卖家Uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
	}


	/**
	 * 获取daawid类型，确认收货设113，退款设112
	 * 
	 * 此字段的版本 >= 0
	 * @return GenType value 类型为:long
	 * 
	 */
	public long getGenType()
	{
		return GenType;
	}


	/**
	 * 设置daawid类型，确认收货设113，退款设112
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setGenType(long value)
	{
		this.GenType = value;
	}


	/**
	 * 获取要生成drawid的子单id列表，即要确认收货的子单id列表
	 * 
	 * 此字段的版本 >= 0
	 * @return TradeIdList value 类型为:Vector<uint64_t>
	 * 
	 */
	public Vector<uint64_t> getTradeIdList()
	{
		return TradeIdList;
	}


	/**
	 * 设置要生成drawid的子单id列表，即要确认收货的子单id列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint64_t>
	 * 
	 */
	public void setTradeIdList(Vector<uint64_t> value)
	{
		if (value != null) {
				this.TradeIdList = value;
		}else{
				this.TradeIdList = new Vector<uint64_t>();
		}
	}


	/**
	 * 获取订单信息
	 * 
	 * 此字段的版本 >= 0
	 * @return MachineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return MachineKey;
	}


	/**
	 * 设置订单信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.MachineKey = value;
	}


	/**
	 * 获取保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveIn value 类型为:String
	 * 
	 */
	public String getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveIn(String value)
	{
		this.ReserveIn = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetDealDrawIdReq)
				length += ByteStream.getObjectSize(Source, null);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(DealId, null);  //计算字段DealId的长度 size_of(String)
				length += ByteStream.getObjectSize(PayId, null);  //计算字段PayId的长度 size_of(String)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段GenType的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(TradeIdList, null);  //计算字段TradeIdList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(MachineKey, null);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveIn, null);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetDealDrawIdReq)
				length += ByteStream.getObjectSize(Source, encoding);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(DealId, encoding);  //计算字段DealId的长度 size_of(String)
				length += ByteStream.getObjectSize(PayId, encoding);  //计算字段PayId的长度 size_of(String)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段GenType的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(TradeIdList, encoding);  //计算字段TradeIdList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(MachineKey, encoding);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveIn, encoding);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
