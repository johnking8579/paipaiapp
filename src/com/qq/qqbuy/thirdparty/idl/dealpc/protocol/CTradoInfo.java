//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.deelpc.protocol.CDealInfo.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *子订单info
 *
 *@date 2015-03-04 11:46:41
 *
 *@since version:0
*/
public class CTradoInfo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 退款单信息表
	 *
	 * 版本 >= 0
	 */
	 private Vector<CRefundInfo> RefundList = new Vector<CRefundInfo>();

	/**
	 * 订单商品状态
	 *
	 * 版本 >= 0
	 */
	 private short DealItemState;

	/**
	 * 支付方式
	 *
	 * 版本 >= 0
	 */
	 private short DealPayType;

	/**
	 * 订单状态
	 *
	 * 版本 >= 0
	 */
	 private short DealState;

	/**
	 * 是否自动发货商品
	 *
	 * 版本 >= 0
	 */
	 private short IsAutoSendItem;

	/**
	 * 商品快照版本
	 *
	 * 版本 >= 0
	 */
	 private short ItemSnapversion;

	/**
	 * 商品类型
	 *
	 * 版本 >= 0
	 */
	 private short ItemType;

	/**
	 * 订单号
	 *
	 * 版本 >= 0
	 */
	 private long DealId;

	/**
	 * 商品id
	 *
	 * 版本 >= 0
	 */
	 private long ItemId;

	/**
	 * 商品库存编号
	 *
	 * 版本 >= 0
	 */
	 private long ItemStockId;

	/**
	 * 运费模板编号
	 *
	 * 版本 >= 0
	 */
	 private long ShippingfeeTemplateId;

	/**
	 * 子单号
	 *
	 * 版本 >= 0
	 */
	 private long TradeId;

	/**
	 * 退款协议ID
	 *
	 * 版本 >= 0
	 */
	 private long TradeRefundId;

	/**
	 * 买家号
	 *
	 * 版本 >= 0
	 */
	 private long BuyerUin;

	/**
	 * 关闭原因
	 *
	 * 版本 >= 0
	 */
	 private long CloseReason;

	/**
	 * 关闭时间
	 *
	 * 版本 >= 0
	 */
	 private long CloseTime;

	/**
	 * 生成时间
	 *
	 * 版本 >= 0
	 */
	 private long DealCreateTime;

	/**
	 * 购买数量
	 *
	 * 版本 >= 0
	 */
	 private long DealItemCount;

	/**
	 * 实际销售数目
	 *
	 * 版本 >= 0
	 */
	 private long DealItemNum;

	/**
	 * 积分
	 *
	 * 版本 >= 0
	 */
	 private long DealItemScore;

	/**
	 * 退款投诉状态
	 *
	 * 版本 >= 0
	 */
	 private long DealRefundState;

	/**
	 * 卖家延迟收货时间
	 *
	 * 版本 >= 0
	 */
	 private long DelayRecvtime;

	/**
	 * 商品类目
	 *
	 * 版本 >= 0
	 */
	 private long ItemClass;

	/**
	 * 商品成本价
	 *
	 * 版本 >= 0
	 */
	 private long ItemCostPrice;

	/**
	 * 商品原价
	 *
	 * 版本 >= 0
	 */
	 private long ItemOriginalPrice;

	/**
	 * 商品价格
	 *
	 * 版本 >= 0
	 */
	 private long ItemPrice;

	/**
	 * 商品重置时间
	 *
	 * 版本 >= 0
	 */
	 private long ItemResetTime;

	/**
	 * 商品运费
	 *
	 * 版本 >= 0
	 */
	 private long ItemShippingFee;

	/**
	 * 最后更新时间
	 *
	 * 版本 >= 0
	 */
	 private long LastUpdateTime;

	/**
	 * 卖家标记缺货时间
	 *
	 * 版本 >= 0
	 */
	 private long MarkNostockTime;

	/**
	 * 支付完成时间
	 *
	 * 版本 >= 0
	 */
	 private long PayTime;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long PreTradeState;

	/**
	 * 打款返回时间
	 *
	 * 版本 >= 0
	 */
	 private long RecvfeeReturnTime;

	/**
	 * 打款完成时间
	 *
	 * 版本 >= 0
	 */
	 private long RecvfeeTime;

	/**
	 * 麦基发货时间
	 *
	 * 版本 >= 0
	 */
	 private long SellerConsignmentTime;

	/**
	 * 卖家uin
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * 子单属性位
	 *
	 * 版本 >= 0
	 */
	 private long TradePropertymask;

	/**
	 * 子单退款状态
	 *
	 * 版本 >= 0
	 */
	 private long TradeRefundState;

	/**
	 * 子单状态
	 *
	 * 版本 >= 0
	 */
	 private long TradeState;

	/**
	 * 子单类型
	 *
	 * 版本 >= 0
	 */
	 private long TradeType;

	/**
	 * 超时商品标识
	 *
	 * 版本 >= 0
	 */
	 private long TimeoutItemFlag;

	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 14;

	/**
	 * 折扣（红包）金额
	 *
	 * 版本 >= 0
	 */
	 private long DiscountFee;

	/**
	 * 买家姓名
	 *
	 * 版本 >= 0
	 */
	 private String BuyerName = new String();

	/**
	 * 买家留言
	 *
	 * 版本 >= 0
	 */
	 private String BuyerRemark = new String();

	/**
	 * 关闭原因描述
	 *
	 * 版本 >= 0
	 */
	 private String CloseReasonDesc = new String();

	/**
	 * 商品id
	 *
	 * 版本 >= 0
	 */
	 private String DisItemId = new String();

	/**
	 * 子单id
	 *
	 * 版本 >= 0
	 */
	 private String DisTradeId = new String();

	/**
	 * 扩展信息
	 *
	 * 版本 >= 0
	 */
	 private String ExtInfo = new String();

	/**
	 * 商品标配说明
	 *
	 * 版本 >= 0
	 */
	 private String ItemAccessoryDesc = new String();

	/**
	 * 商品销售属性选项组合值
	 *
	 * 版本 >= 0
	 */
	 private String ItemAttrOptionValue = new String();

	/**
	 * 商品图片主图
	 *
	 * 版本 >= 0
	 */
	 private String ItemLogo = new String();

	/**
	 * 商品名称
	 *
	 * 版本 >= 0
	 */
	 private String ItemName = new String();

	/**
	 * 商品促销说明
	 *
	 * 版本 >= 0
	 */
	 private String ItemPromotionDesc = new String();

	/**
	 * 套餐信息
	 *
	 * 版本 >= 0
	 */
	 private String PackageInfo = new String();

	/**
	 * 产品编码
	 *
	 * 版本 >= 0
	 */
	 private String ProductCode = new String();

	/**
	 * 送送所需天数
	 *
	 * 版本 >= 0
	 */
	 private String ReceiveDays = new String();

	/**
	 * 商家名称
	 *
	 * 版本 >= 0
	 */
	 private String SellerName = new String();

	/**
	 * 卖家备注
	 *
	 * 版本 >= 0
	 */
	 private String SellerRemark = new String();

	/**
	 * 运费模板说明
	 *
	 * 版本 >= 0
	 */
	 private String ShippingfeeDesc = new String();

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private String Reserve = new String();

	/**
	 * 商品本地编码
	 *
	 * 版本 >= 0
	 */
	 private String ItemLocalCode = new String();

	/**
	 * 库存本地编码
	 *
	 * 版本 >= 0
	 */
	 private String StockLocalCode = new String();

	/**
	 * 商品价格调整优惠
	 *
	 * 版本 >= 0
	 */
	 private int ItemAdjustPrice;

	/**
	 * 子单属性2
	 *
	 * 版本 >= 11
	 */
	 private long TradePropertyMask2 = 0;

	/**
	 * 买家金额
	 *
	 * 版本 >= 16
	 */
	 private long BuyerFee;

	/**
	 * 卖家金额
	 *
	 * 版本 >= 16
	 */
	 private long SellerFee;

	/**
	 * DrawID
	 *
	 * 版本 >= 16
	 */
	 private String DrawId = new String();

	/**
	 * Token
	 *
	 * 版本 >= 16
	 */
	 private String Token = new String();

	/**
	 * (新)商品快照版本
	 *
	 * 版本 >= 14
	 */
	 private long ItemVersion;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushObject(RefundList);
		bs.pushUByte(DealItemState);
		bs.pushUByte(DealPayType);
		bs.pushUByte(DealState);
		bs.pushUByte(IsAutoSendItem);
		bs.pushUByte(ItemSnapversion);
		bs.pushUByte(ItemType);
		bs.pushLong(DealId);
		bs.pushLong(ItemId);
		bs.pushLong(ItemStockId);
		bs.pushLong(ShippingfeeTemplateId);
		bs.pushLong(TradeId);
		bs.pushLong(TradeRefundId);
		bs.pushUInt(BuyerUin);
		bs.pushUInt(CloseReason);
		bs.pushUInt(CloseTime);
		bs.pushUInt(DealCreateTime);
		bs.pushUInt(DealItemCount);
		bs.pushUInt(DealItemNum);
		bs.pushUInt(DealItemScore);
		bs.pushUInt(DealRefundState);
		bs.pushUInt(DelayRecvtime);
		bs.pushUInt(ItemClass);
		bs.pushUInt(ItemCostPrice);
		bs.pushUInt(ItemOriginalPrice);
		bs.pushUInt(ItemPrice);
		bs.pushUInt(ItemResetTime);
		bs.pushUInt(ItemShippingFee);
		bs.pushUInt(LastUpdateTime);
		bs.pushUInt(MarkNostockTime);
		bs.pushUInt(PayTime);
		bs.pushUInt(PreTradeState);
		bs.pushUInt(RecvfeeReturnTime);
		bs.pushUInt(RecvfeeTime);
		bs.pushUInt(SellerConsignmentTime);
		bs.pushUInt(SellerUin);
		bs.pushUInt(TradePropertymask);
		bs.pushUInt(TradeRefundState);
		bs.pushUInt(TradeState);
		bs.pushUInt(TradeType);
		bs.pushUInt(TimeoutItemFlag);
		bs.pushUInt(version);
		bs.pushUInt(DiscountFee);
		bs.pushString(BuyerName);
		bs.pushString(BuyerRemark);
		bs.pushString(CloseReasonDesc);
		bs.pushString(DisItemId);
		bs.pushString(DisTradeId);
		bs.pushString(ExtInfo);
		bs.pushString(ItemAccessoryDesc);
		bs.pushString(ItemAttrOptionValue);
		bs.pushString(ItemLogo);
		bs.pushString(ItemName);
		bs.pushString(ItemPromotionDesc);
		bs.pushString(PackageInfo);
		bs.pushString(ProductCode);
		bs.pushString(ReceiveDays);
		bs.pushString(SellerName);
		bs.pushString(SellerRemark);
		bs.pushString(ShippingfeeDesc);
		bs.pushString(Reserve);
		bs.pushString(ItemLocalCode);
		bs.pushString(StockLocalCode);
		bs.pushInt(ItemAdjustPrice);
		if(  this.version >= 11 ){
				bs.pushUInt(TradePropertyMask2);
		}
		if(  this.version >= 16 ){
				bs.pushUInt(BuyerFee);
		}
		if(  this.version >= 16 ){
				bs.pushUInt(SellerFee);
		}
		if(  this.version >= 16 ){
				bs.pushString(DrawId);
		}
		if(  this.version >= 16 ){
				bs.pushString(Token);
		}
		if(  this.version >= 14 ){
				bs.pushUInt(ItemVersion);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		RefundList = (Vector<CRefundInfo>)bs.popVector(CRefundInfo.class);
		DealItemState = bs.popUByte();
		DealPayType = bs.popUByte();
		DealState = bs.popUByte();
		IsAutoSendItem = bs.popUByte();
		ItemSnapversion = bs.popUByte();
		ItemType = bs.popUByte();
		DealId = bs.popLong();
		ItemId = bs.popLong();
		ItemStockId = bs.popLong();
		ShippingfeeTemplateId = bs.popLong();
		TradeId = bs.popLong();
		TradeRefundId = bs.popLong();
		BuyerUin = bs.popUInt();
		CloseReason = bs.popUInt();
		CloseTime = bs.popUInt();
		DealCreateTime = bs.popUInt();
		DealItemCount = bs.popUInt();
		DealItemNum = bs.popUInt();
		DealItemScore = bs.popUInt();
		DealRefundState = bs.popUInt();
		DelayRecvtime = bs.popUInt();
		ItemClass = bs.popUInt();
		ItemCostPrice = bs.popUInt();
		ItemOriginalPrice = bs.popUInt();
		ItemPrice = bs.popUInt();
		ItemResetTime = bs.popUInt();
		ItemShippingFee = bs.popUInt();
		LastUpdateTime = bs.popUInt();
		MarkNostockTime = bs.popUInt();
		PayTime = bs.popUInt();
		PreTradeState = bs.popUInt();
		RecvfeeReturnTime = bs.popUInt();
		RecvfeeTime = bs.popUInt();
		SellerConsignmentTime = bs.popUInt();
		SellerUin = bs.popUInt();
		TradePropertymask = bs.popUInt();
		TradeRefundState = bs.popUInt();
		TradeState = bs.popUInt();
		TradeType = bs.popUInt();
		TimeoutItemFlag = bs.popUInt();
		version = bs.popUInt();
		DiscountFee = bs.popUInt();
		BuyerName = bs.popString();
		BuyerRemark = bs.popString();
		CloseReasonDesc = bs.popString();
		DisItemId = bs.popString();
		DisTradeId = bs.popString();
		ExtInfo = bs.popString();
		ItemAccessoryDesc = bs.popString();
		ItemAttrOptionValue = bs.popString();
		ItemLogo = bs.popString();
		ItemName = bs.popString();
		ItemPromotionDesc = bs.popString();
		PackageInfo = bs.popString();
		ProductCode = bs.popString();
		ReceiveDays = bs.popString();
		SellerName = bs.popString();
		SellerRemark = bs.popString();
		ShippingfeeDesc = bs.popString();
		Reserve = bs.popString();
		ItemLocalCode = bs.popString();
		StockLocalCode = bs.popString();
		ItemAdjustPrice = bs.popInt();
		if(  this.version >= 11 ){
				TradePropertyMask2 = bs.popUInt();
		}
		if(  this.version >= 16 ){
				BuyerFee = bs.popUInt();
		}
		if(  this.version >= 16 ){
				SellerFee = bs.popUInt();
		}
		if(  this.version >= 16 ){
				DrawId = bs.popString();
		}
		if(  this.version >= 16 ){
				Token = bs.popString();
		}
		if(  this.version >= 14 ){
				ItemVersion = bs.popUInt();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取退款单信息表
	 * 
	 * 此字段的版本 >= 0
	 * @return RefundList value 类型为:Vector<CRefundInfo>
	 * 
	 */
	public Vector<CRefundInfo> getRefundList()
	{
		return RefundList;
	}


	/**
	 * 设置退款单信息表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<CRefundInfo>
	 * 
	 */
	public void setRefundList(Vector<CRefundInfo> value)
	{
		if (value != null) {
				this.RefundList = value;
		}else{
				this.RefundList = new Vector<CRefundInfo>();
		}
	}


	/**
	 * 获取订单商品状态
	 * 
	 * 此字段的版本 >= 0
	 * @return DealItemState value 类型为:short
	 * 
	 */
	public short getDealItemState()
	{
		return DealItemState;
	}


	/**
	 * 设置订单商品状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealItemState(short value)
	{
		this.DealItemState = value;
	}


	/**
	 * 获取支付方式
	 * 
	 * 此字段的版本 >= 0
	 * @return DealPayType value 类型为:short
	 * 
	 */
	public short getDealPayType()
	{
		return DealPayType;
	}


	/**
	 * 设置支付方式
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealPayType(short value)
	{
		this.DealPayType = value;
	}


	/**
	 * 获取订单状态
	 * 
	 * 此字段的版本 >= 0
	 * @return DealState value 类型为:short
	 * 
	 */
	public short getDealState()
	{
		return DealState;
	}


	/**
	 * 设置订单状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealState(short value)
	{
		this.DealState = value;
	}


	/**
	 * 获取是否自动发货商品
	 * 
	 * 此字段的版本 >= 0
	 * @return IsAutoSendItem value 类型为:short
	 * 
	 */
	public short getIsAutoSendItem()
	{
		return IsAutoSendItem;
	}


	/**
	 * 设置是否自动发货商品
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIsAutoSendItem(short value)
	{
		this.IsAutoSendItem = value;
	}


	/**
	 * 获取商品快照版本
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemSnapversion value 类型为:short
	 * 
	 */
	public short getItemSnapversion()
	{
		return ItemSnapversion;
	}


	/**
	 * 设置商品快照版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemSnapversion(short value)
	{
		this.ItemSnapversion = value;
	}


	/**
	 * 获取商品类型
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemType value 类型为:short
	 * 
	 */
	public short getItemType()
	{
		return ItemType;
	}


	/**
	 * 设置商品类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemType(short value)
	{
		this.ItemType = value;
	}


	/**
	 * 获取订单号
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:long
	 * 
	 */
	public long getDealId()
	{
		return DealId;
	}


	/**
	 * 设置订单号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealId(long value)
	{
		this.DealId = value;
	}


	/**
	 * 获取商品id
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemId value 类型为:long
	 * 
	 */
	public long getItemId()
	{
		return ItemId;
	}


	/**
	 * 设置商品id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setItemId(long value)
	{
		this.ItemId = value;
	}


	/**
	 * 获取商品库存编号
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemStockId value 类型为:long
	 * 
	 */
	public long getItemStockId()
	{
		return ItemStockId;
	}


	/**
	 * 设置商品库存编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setItemStockId(long value)
	{
		this.ItemStockId = value;
	}


	/**
	 * 获取运费模板编号
	 * 
	 * 此字段的版本 >= 0
	 * @return ShippingfeeTemplateId value 类型为:long
	 * 
	 */
	public long getShippingfeeTemplateId()
	{
		return ShippingfeeTemplateId;
	}


	/**
	 * 设置运费模板编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setShippingfeeTemplateId(long value)
	{
		this.ShippingfeeTemplateId = value;
	}


	/**
	 * 获取子单号
	 * 
	 * 此字段的版本 >= 0
	 * @return TradeId value 类型为:long
	 * 
	 */
	public long getTradeId()
	{
		return TradeId;
	}


	/**
	 * 设置子单号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTradeId(long value)
	{
		this.TradeId = value;
	}


	/**
	 * 获取退款协议ID
	 * 
	 * 此字段的版本 >= 0
	 * @return TradeRefundId value 类型为:long
	 * 
	 */
	public long getTradeRefundId()
	{
		return TradeRefundId;
	}


	/**
	 * 设置退款协议ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTradeRefundId(long value)
	{
		this.TradeRefundId = value;
	}


	/**
	 * 获取买家号
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return BuyerUin;
	}


	/**
	 * 设置买家号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.BuyerUin = value;
	}


	/**
	 * 获取关闭原因
	 * 
	 * 此字段的版本 >= 0
	 * @return CloseReason value 类型为:long
	 * 
	 */
	public long getCloseReason()
	{
		return CloseReason;
	}


	/**
	 * 设置关闭原因
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCloseReason(long value)
	{
		this.CloseReason = value;
	}


	/**
	 * 获取关闭时间
	 * 
	 * 此字段的版本 >= 0
	 * @return CloseTime value 类型为:long
	 * 
	 */
	public long getCloseTime()
	{
		return CloseTime;
	}


	/**
	 * 设置关闭时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCloseTime(long value)
	{
		this.CloseTime = value;
	}


	/**
	 * 获取生成时间
	 * 
	 * 此字段的版本 >= 0
	 * @return DealCreateTime value 类型为:long
	 * 
	 */
	public long getDealCreateTime()
	{
		return DealCreateTime;
	}


	/**
	 * 设置生成时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealCreateTime(long value)
	{
		this.DealCreateTime = value;
	}


	/**
	 * 获取购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @return DealItemCount value 类型为:long
	 * 
	 */
	public long getDealItemCount()
	{
		return DealItemCount;
	}


	/**
	 * 设置购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealItemCount(long value)
	{
		this.DealItemCount = value;
	}


	/**
	 * 获取实际销售数目
	 * 
	 * 此字段的版本 >= 0
	 * @return DealItemNum value 类型为:long
	 * 
	 */
	public long getDealItemNum()
	{
		return DealItemNum;
	}


	/**
	 * 设置实际销售数目
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealItemNum(long value)
	{
		this.DealItemNum = value;
	}


	/**
	 * 获取积分
	 * 
	 * 此字段的版本 >= 0
	 * @return DealItemScore value 类型为:long
	 * 
	 */
	public long getDealItemScore()
	{
		return DealItemScore;
	}


	/**
	 * 设置积分
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealItemScore(long value)
	{
		this.DealItemScore = value;
	}


	/**
	 * 获取退款投诉状态
	 * 
	 * 此字段的版本 >= 0
	 * @return DealRefundState value 类型为:long
	 * 
	 */
	public long getDealRefundState()
	{
		return DealRefundState;
	}


	/**
	 * 设置退款投诉状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealRefundState(long value)
	{
		this.DealRefundState = value;
	}


	/**
	 * 获取卖家延迟收货时间
	 * 
	 * 此字段的版本 >= 0
	 * @return DelayRecvtime value 类型为:long
	 * 
	 */
	public long getDelayRecvtime()
	{
		return DelayRecvtime;
	}


	/**
	 * 设置卖家延迟收货时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDelayRecvtime(long value)
	{
		this.DelayRecvtime = value;
	}


	/**
	 * 获取商品类目
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemClass value 类型为:long
	 * 
	 */
	public long getItemClass()
	{
		return ItemClass;
	}


	/**
	 * 设置商品类目
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setItemClass(long value)
	{
		this.ItemClass = value;
	}


	/**
	 * 获取商品成本价
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemCostPrice value 类型为:long
	 * 
	 */
	public long getItemCostPrice()
	{
		return ItemCostPrice;
	}


	/**
	 * 设置商品成本价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setItemCostPrice(long value)
	{
		this.ItemCostPrice = value;
	}


	/**
	 * 获取商品原价
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemOriginalPrice value 类型为:long
	 * 
	 */
	public long getItemOriginalPrice()
	{
		return ItemOriginalPrice;
	}


	/**
	 * 设置商品原价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setItemOriginalPrice(long value)
	{
		this.ItemOriginalPrice = value;
	}


	/**
	 * 获取商品价格
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemPrice value 类型为:long
	 * 
	 */
	public long getItemPrice()
	{
		return ItemPrice;
	}


	/**
	 * 设置商品价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setItemPrice(long value)
	{
		this.ItemPrice = value;
	}


	/**
	 * 获取商品重置时间
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemResetTime value 类型为:long
	 * 
	 */
	public long getItemResetTime()
	{
		return ItemResetTime;
	}


	/**
	 * 设置商品重置时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setItemResetTime(long value)
	{
		this.ItemResetTime = value;
	}


	/**
	 * 获取商品运费
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemShippingFee value 类型为:long
	 * 
	 */
	public long getItemShippingFee()
	{
		return ItemShippingFee;
	}


	/**
	 * 设置商品运费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setItemShippingFee(long value)
	{
		this.ItemShippingFee = value;
	}


	/**
	 * 获取最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return LastUpdateTime value 类型为:long
	 * 
	 */
	public long getLastUpdateTime()
	{
		return LastUpdateTime;
	}


	/**
	 * 设置最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastUpdateTime(long value)
	{
		this.LastUpdateTime = value;
	}


	/**
	 * 获取卖家标记缺货时间
	 * 
	 * 此字段的版本 >= 0
	 * @return MarkNostockTime value 类型为:long
	 * 
	 */
	public long getMarkNostockTime()
	{
		return MarkNostockTime;
	}


	/**
	 * 设置卖家标记缺货时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMarkNostockTime(long value)
	{
		this.MarkNostockTime = value;
	}


	/**
	 * 获取支付完成时间
	 * 
	 * 此字段的版本 >= 0
	 * @return PayTime value 类型为:long
	 * 
	 */
	public long getPayTime()
	{
		return PayTime;
	}


	/**
	 * 设置支付完成时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayTime(long value)
	{
		this.PayTime = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PreTradeState value 类型为:long
	 * 
	 */
	public long getPreTradeState()
	{
		return PreTradeState;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPreTradeState(long value)
	{
		this.PreTradeState = value;
	}


	/**
	 * 获取打款返回时间
	 * 
	 * 此字段的版本 >= 0
	 * @return RecvfeeReturnTime value 类型为:long
	 * 
	 */
	public long getRecvfeeReturnTime()
	{
		return RecvfeeReturnTime;
	}


	/**
	 * 设置打款返回时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRecvfeeReturnTime(long value)
	{
		this.RecvfeeReturnTime = value;
	}


	/**
	 * 获取打款完成时间
	 * 
	 * 此字段的版本 >= 0
	 * @return RecvfeeTime value 类型为:long
	 * 
	 */
	public long getRecvfeeTime()
	{
		return RecvfeeTime;
	}


	/**
	 * 设置打款完成时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRecvfeeTime(long value)
	{
		this.RecvfeeTime = value;
	}


	/**
	 * 获取麦基发货时间
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerConsignmentTime value 类型为:long
	 * 
	 */
	public long getSellerConsignmentTime()
	{
		return SellerConsignmentTime;
	}


	/**
	 * 设置麦基发货时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerConsignmentTime(long value)
	{
		this.SellerConsignmentTime = value;
	}


	/**
	 * 获取卖家uin
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置卖家uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
	}


	/**
	 * 获取子单属性位
	 * 
	 * 此字段的版本 >= 0
	 * @return TradePropertymask value 类型为:long
	 * 
	 */
	public long getTradePropertymask()
	{
		return TradePropertymask;
	}


	/**
	 * 设置子单属性位
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTradePropertymask(long value)
	{
		this.TradePropertymask = value;
	}


	/**
	 * 获取子单退款状态
	 * 
	 * 此字段的版本 >= 0
	 * @return TradeRefundState value 类型为:long
	 * 
	 */
	public long getTradeRefundState()
	{
		return TradeRefundState;
	}


	/**
	 * 设置子单退款状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTradeRefundState(long value)
	{
		this.TradeRefundState = value;
	}


	/**
	 * 获取子单状态
	 * 
	 * 此字段的版本 >= 0
	 * @return TradeState value 类型为:long
	 * 
	 */
	public long getTradeState()
	{
		return TradeState;
	}


	/**
	 * 设置子单状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTradeState(long value)
	{
		this.TradeState = value;
	}


	/**
	 * 获取子单类型
	 * 
	 * 此字段的版本 >= 0
	 * @return TradeType value 类型为:long
	 * 
	 */
	public long getTradeType()
	{
		return TradeType;
	}


	/**
	 * 设置子单类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTradeType(long value)
	{
		this.TradeType = value;
	}


	/**
	 * 获取超时商品标识
	 * 
	 * 此字段的版本 >= 0
	 * @return TimeoutItemFlag value 类型为:long
	 * 
	 */
	public long getTimeoutItemFlag()
	{
		return TimeoutItemFlag;
	}


	/**
	 * 设置超时商品标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTimeoutItemFlag(long value)
	{
		this.TimeoutItemFlag = value;
	}


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取折扣（红包）金额
	 * 
	 * 此字段的版本 >= 0
	 * @return DiscountFee value 类型为:long
	 * 
	 */
	public long getDiscountFee()
	{
		return DiscountFee;
	}


	/**
	 * 设置折扣（红包）金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDiscountFee(long value)
	{
		this.DiscountFee = value;
	}


	/**
	 * 获取买家姓名
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerName value 类型为:String
	 * 
	 */
	public String getBuyerName()
	{
		return BuyerName;
	}


	/**
	 * 设置买家姓名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBuyerName(String value)
	{
		this.BuyerName = value;
	}


	/**
	 * 获取买家留言
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerRemark value 类型为:String
	 * 
	 */
	public String getBuyerRemark()
	{
		return BuyerRemark;
	}


	/**
	 * 设置买家留言
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBuyerRemark(String value)
	{
		this.BuyerRemark = value;
	}


	/**
	 * 获取关闭原因描述
	 * 
	 * 此字段的版本 >= 0
	 * @return CloseReasonDesc value 类型为:String
	 * 
	 */
	public String getCloseReasonDesc()
	{
		return CloseReasonDesc;
	}


	/**
	 * 设置关闭原因描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCloseReasonDesc(String value)
	{
		this.CloseReasonDesc = value;
	}


	/**
	 * 获取商品id
	 * 
	 * 此字段的版本 >= 0
	 * @return DisItemId value 类型为:String
	 * 
	 */
	public String getDisItemId()
	{
		return DisItemId;
	}


	/**
	 * 设置商品id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDisItemId(String value)
	{
		this.DisItemId = value;
	}


	/**
	 * 获取子单id
	 * 
	 * 此字段的版本 >= 0
	 * @return DisTradeId value 类型为:String
	 * 
	 */
	public String getDisTradeId()
	{
		return DisTradeId;
	}


	/**
	 * 设置子单id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDisTradeId(String value)
	{
		this.DisTradeId = value;
	}


	/**
	 * 获取扩展信息
	 * 格式:800=1&822=ee7052b1665785fe4f478a1b68ca49fc07caad06
	 * 此字段的版本 >= 0
	 * @return ExtInfo value 类型为:String
	 * 
	 */
	public String getExtInfo()
	{
		return ExtInfo;
	}


	/**
	 * 设置扩展信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setExtInfo(String value)
	{
		this.ExtInfo = value;
	}


	/**
	 * 获取商品标配说明
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemAccessoryDesc value 类型为:String
	 * 
	 */
	public String getItemAccessoryDesc()
	{
		return ItemAccessoryDesc;
	}


	/**
	 * 设置商品标配说明
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemAccessoryDesc(String value)
	{
		this.ItemAccessoryDesc = value;
	}


	/**
	 * 获取商品销售属性选项组合值
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemAttrOptionValue value 类型为:String
	 * 
	 */
	public String getItemAttrOptionValue()
	{
		return ItemAttrOptionValue;
	}


	/**
	 * 设置商品销售属性选项组合值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemAttrOptionValue(String value)
	{
		this.ItemAttrOptionValue = value;
	}


	/**
	 * 获取商品图片主图
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemLogo value 类型为:String
	 * 
	 */
	public String getItemLogo()
	{
		return ItemLogo;
	}


	/**
	 * 设置商品图片主图
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemLogo(String value)
	{
		this.ItemLogo = value;
	}


	/**
	 * 获取商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemName value 类型为:String
	 * 
	 */
	public String getItemName()
	{
		return ItemName;
	}


	/**
	 * 设置商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemName(String value)
	{
		this.ItemName = value;
	}


	/**
	 * 获取商品促销说明
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemPromotionDesc value 类型为:String
	 * 
	 */
	public String getItemPromotionDesc()
	{
		return ItemPromotionDesc;
	}


	/**
	 * 设置商品促销说明
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemPromotionDesc(String value)
	{
		this.ItemPromotionDesc = value;
	}


	/**
	 * 获取套餐信息
	 * 
	 * 此字段的版本 >= 0
	 * @return PackageInfo value 类型为:String
	 * 
	 */
	public String getPackageInfo()
	{
		return PackageInfo;
	}


	/**
	 * 设置套餐信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPackageInfo(String value)
	{
		this.PackageInfo = value;
	}


	/**
	 * 获取产品编码
	 * 
	 * 此字段的版本 >= 0
	 * @return ProductCode value 类型为:String
	 * 
	 */
	public String getProductCode()
	{
		return ProductCode;
	}


	/**
	 * 设置产品编码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setProductCode(String value)
	{
		this.ProductCode = value;
	}


	/**
	 * 获取送送所需天数
	 * 
	 * 此字段的版本 >= 0
	 * @return ReceiveDays value 类型为:String
	 * 
	 */
	public String getReceiveDays()
	{
		return ReceiveDays;
	}


	/**
	 * 设置送送所需天数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReceiveDays(String value)
	{
		this.ReceiveDays = value;
	}


	/**
	 * 获取商家名称
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerName value 类型为:String
	 * 
	 */
	public String getSellerName()
	{
		return SellerName;
	}


	/**
	 * 设置商家名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSellerName(String value)
	{
		this.SellerName = value;
	}


	/**
	 * 获取卖家备注
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerRemark value 类型为:String
	 * 
	 */
	public String getSellerRemark()
	{
		return SellerRemark;
	}


	/**
	 * 设置卖家备注
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSellerRemark(String value)
	{
		this.SellerRemark = value;
	}


	/**
	 * 获取运费模板说明
	 * 
	 * 此字段的版本 >= 0
	 * @return ShippingfeeDesc value 类型为:String
	 * 
	 */
	public String getShippingfeeDesc()
	{
		return ShippingfeeDesc;
	}


	/**
	 * 设置运费模板说明
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setShippingfeeDesc(String value)
	{
		this.ShippingfeeDesc = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Reserve value 类型为:String
	 * 
	 */
	public String getReserve()
	{
		return Reserve;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserve(String value)
	{
		this.Reserve = value;
	}


	/**
	 * 获取商品本地编码
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemLocalCode value 类型为:String
	 * 
	 */
	public String getItemLocalCode()
	{
		return ItemLocalCode;
	}


	/**
	 * 设置商品本地编码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemLocalCode(String value)
	{
		this.ItemLocalCode = value;
	}


	/**
	 * 获取库存本地编码
	 * 
	 * 此字段的版本 >= 0
	 * @return StockLocalCode value 类型为:String
	 * 
	 */
	public String getStockLocalCode()
	{
		return StockLocalCode;
	}


	/**
	 * 设置库存本地编码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStockLocalCode(String value)
	{
		this.StockLocalCode = value;
	}


	/**
	 * 获取商品价格调整优惠
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemAdjustPrice value 类型为:int
	 * 
	 */
	public int getItemAdjustPrice()
	{
		return ItemAdjustPrice;
	}


	/**
	 * 设置商品价格调整优惠
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setItemAdjustPrice(int value)
	{
		this.ItemAdjustPrice = value;
	}


	/**
	 * 获取子单属性2
	 * 
	 * 此字段的版本 >= 11
	 * @return TradePropertyMask2 value 类型为:long
	 * 
	 */
	public long getTradePropertyMask2()
	{
		return TradePropertyMask2;
	}


	/**
	 * 设置子单属性2
	 * 
	 * 此字段的版本 >= 11
	 * @param  value 类型为:long
	 * 
	 */
	public void setTradePropertyMask2(long value)
	{
		this.TradePropertyMask2 = value;
	}


	/**
	 * 获取买家金额
	 * 
	 * 此字段的版本 >= 16
	 * @return BuyerFee value 类型为:long
	 * 
	 */
	public long getBuyerFee()
	{
		return BuyerFee;
	}


	/**
	 * 设置买家金额
	 * 
	 * 此字段的版本 >= 16
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerFee(long value)
	{
		this.BuyerFee = value;
	}


	/**
	 * 获取卖家金额
	 * 
	 * 此字段的版本 >= 16
	 * @return SellerFee value 类型为:long
	 * 
	 */
	public long getSellerFee()
	{
		return SellerFee;
	}


	/**
	 * 设置卖家金额
	 * 
	 * 此字段的版本 >= 16
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerFee(long value)
	{
		this.SellerFee = value;
	}


	/**
	 * 获取DrawID
	 * 
	 * 此字段的版本 >= 16
	 * @return DrawId value 类型为:String
	 * 
	 */
	public String getDrawId()
	{
		return DrawId;
	}


	/**
	 * 设置DrawID
	 * 
	 * 此字段的版本 >= 16
	 * @param  value 类型为:String
	 * 
	 */
	public void setDrawId(String value)
	{
		this.DrawId = value;
	}


	/**
	 * 获取Token
	 * 
	 * 此字段的版本 >= 16
	 * @return Token value 类型为:String
	 * 
	 */
	public String getToken()
	{
		return Token;
	}


	/**
	 * 设置Token
	 * 
	 * 此字段的版本 >= 16
	 * @param  value 类型为:String
	 * 
	 */
	public void setToken(String value)
	{
		this.Token = value;
	}


	/**
	 * 获取(新)商品快照版本
	 * 
	 * 此字段的版本 >= 14
	 * @return ItemVersion value 类型为:long
	 * 
	 */
	public long getItemVersion()
	{
		return ItemVersion;
	}


	/**
	 * 设置(新)商品快照版本
	 * 
	 * 此字段的版本 >= 14
	 * @param  value 类型为:long
	 * 
	 */
	public void setItemVersion(long value)
	{
		this.ItemVersion = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CTradoInfo)
				length += ByteStream.getObjectSize(RefundList, null);  //计算字段RefundList的长度 size_of(Vector)
				length += 1;  //计算字段DealItemState的长度 size_of(uint8_t)
				length += 1;  //计算字段DealPayType的长度 size_of(uint8_t)
				length += 1;  //计算字段DealState的长度 size_of(uint8_t)
				length += 1;  //计算字段IsAutoSendItem的长度 size_of(uint8_t)
				length += 1;  //计算字段ItemSnapversion的长度 size_of(uint8_t)
				length += 1;  //计算字段ItemType的长度 size_of(uint8_t)
				length += 17;  //计算字段DealId的长度 size_of(uint64_t)
				length += 17;  //计算字段ItemId的长度 size_of(uint64_t)
				length += 17;  //计算字段ItemStockId的长度 size_of(uint64_t)
				length += 17;  //计算字段ShippingfeeTemplateId的长度 size_of(uint64_t)
				length += 17;  //计算字段TradeId的长度 size_of(uint64_t)
				length += 17;  //计算字段TradeRefundId的长度 size_of(uint64_t)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段CloseReason的长度 size_of(uint32_t)
				length += 4;  //计算字段CloseTime的长度 size_of(uint32_t)
				length += 4;  //计算字段DealCreateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段DealItemCount的长度 size_of(uint32_t)
				length += 4;  //计算字段DealItemNum的长度 size_of(uint32_t)
				length += 4;  //计算字段DealItemScore的长度 size_of(uint32_t)
				length += 4;  //计算字段DealRefundState的长度 size_of(uint32_t)
				length += 4;  //计算字段DelayRecvtime的长度 size_of(uint32_t)
				length += 4;  //计算字段ItemClass的长度 size_of(uint32_t)
				length += 4;  //计算字段ItemCostPrice的长度 size_of(uint32_t)
				length += 4;  //计算字段ItemOriginalPrice的长度 size_of(uint32_t)
				length += 4;  //计算字段ItemPrice的长度 size_of(uint32_t)
				length += 4;  //计算字段ItemResetTime的长度 size_of(uint32_t)
				length += 4;  //计算字段ItemShippingFee的长度 size_of(uint32_t)
				length += 4;  //计算字段LastUpdateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段MarkNostockTime的长度 size_of(uint32_t)
				length += 4;  //计算字段PayTime的长度 size_of(uint32_t)
				length += 4;  //计算字段PreTradeState的长度 size_of(uint32_t)
				length += 4;  //计算字段RecvfeeReturnTime的长度 size_of(uint32_t)
				length += 4;  //计算字段RecvfeeTime的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerConsignmentTime的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段TradePropertymask的长度 size_of(uint32_t)
				length += 4;  //计算字段TradeRefundState的长度 size_of(uint32_t)
				length += 4;  //计算字段TradeState的长度 size_of(uint32_t)
				length += 4;  //计算字段TradeType的长度 size_of(uint32_t)
				length += 4;  //计算字段TimeoutItemFlag的长度 size_of(uint32_t)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段DiscountFee的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(BuyerName, null);  //计算字段BuyerName的长度 size_of(String)
				length += ByteStream.getObjectSize(BuyerRemark, null);  //计算字段BuyerRemark的长度 size_of(String)
				length += ByteStream.getObjectSize(CloseReasonDesc, null);  //计算字段CloseReasonDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(DisItemId, null);  //计算字段DisItemId的长度 size_of(String)
				length += ByteStream.getObjectSize(DisTradeId, null);  //计算字段DisTradeId的长度 size_of(String)
				length += ByteStream.getObjectSize(ExtInfo, null);  //计算字段ExtInfo的长度 size_of(String)
				length += ByteStream.getObjectSize(ItemAccessoryDesc, null);  //计算字段ItemAccessoryDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(ItemAttrOptionValue, null);  //计算字段ItemAttrOptionValue的长度 size_of(String)
				length += ByteStream.getObjectSize(ItemLogo, null);  //计算字段ItemLogo的长度 size_of(String)
				length += ByteStream.getObjectSize(ItemName, null);  //计算字段ItemName的长度 size_of(String)
				length += ByteStream.getObjectSize(ItemPromotionDesc, null);  //计算字段ItemPromotionDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(PackageInfo, null);  //计算字段PackageInfo的长度 size_of(String)
				length += ByteStream.getObjectSize(ProductCode, null);  //计算字段ProductCode的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceiveDays, null);  //计算字段ReceiveDays的长度 size_of(String)
				length += ByteStream.getObjectSize(SellerName, null);  //计算字段SellerName的长度 size_of(String)
				length += ByteStream.getObjectSize(SellerRemark, null);  //计算字段SellerRemark的长度 size_of(String)
				length += ByteStream.getObjectSize(ShippingfeeDesc, null);  //计算字段ShippingfeeDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(Reserve, null);  //计算字段Reserve的长度 size_of(String)
				length += ByteStream.getObjectSize(ItemLocalCode, null);  //计算字段ItemLocalCode的长度 size_of(String)
				length += ByteStream.getObjectSize(StockLocalCode, null);  //计算字段StockLocalCode的长度 size_of(String)
				length += 4;  //计算字段ItemAdjustPrice的长度 size_of(int)
				if(  this.version >= 11 ){
						length += 4;  //计算字段TradePropertyMask2的长度 size_of(uint32_t)
				}
				if(  this.version >= 16 ){
						length += 4;  //计算字段BuyerFee的长度 size_of(uint32_t)
				}
				if(  this.version >= 16 ){
						length += 4;  //计算字段SellerFee的长度 size_of(uint32_t)
				}
				if(  this.version >= 16 ){
						length += ByteStream.getObjectSize(DrawId, null);  //计算字段DrawId的长度 size_of(String)
				}
				if(  this.version >= 16 ){
						length += ByteStream.getObjectSize(Token, null);  //计算字段Token的长度 size_of(String)
				}
				if(  this.version >= 14 ){
						length += 4;  //计算字段ItemVersion的长度 size_of(uint32_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(CTradoInfo)
				length += ByteStream.getObjectSize(RefundList, encoding);  //计算字段RefundList的长度 size_of(Vector)
				length += 1;  //计算字段DealItemState的长度 size_of(uint8_t)
				length += 1;  //计算字段DealPayType的长度 size_of(uint8_t)
				length += 1;  //计算字段DealState的长度 size_of(uint8_t)
				length += 1;  //计算字段IsAutoSendItem的长度 size_of(uint8_t)
				length += 1;  //计算字段ItemSnapversion的长度 size_of(uint8_t)
				length += 1;  //计算字段ItemType的长度 size_of(uint8_t)
				length += 17;  //计算字段DealId的长度 size_of(uint64_t)
				length += 17;  //计算字段ItemId的长度 size_of(uint64_t)
				length += 17;  //计算字段ItemStockId的长度 size_of(uint64_t)
				length += 17;  //计算字段ShippingfeeTemplateId的长度 size_of(uint64_t)
				length += 17;  //计算字段TradeId的长度 size_of(uint64_t)
				length += 17;  //计算字段TradeRefundId的长度 size_of(uint64_t)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段CloseReason的长度 size_of(uint32_t)
				length += 4;  //计算字段CloseTime的长度 size_of(uint32_t)
				length += 4;  //计算字段DealCreateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段DealItemCount的长度 size_of(uint32_t)
				length += 4;  //计算字段DealItemNum的长度 size_of(uint32_t)
				length += 4;  //计算字段DealItemScore的长度 size_of(uint32_t)
				length += 4;  //计算字段DealRefundState的长度 size_of(uint32_t)
				length += 4;  //计算字段DelayRecvtime的长度 size_of(uint32_t)
				length += 4;  //计算字段ItemClass的长度 size_of(uint32_t)
				length += 4;  //计算字段ItemCostPrice的长度 size_of(uint32_t)
				length += 4;  //计算字段ItemOriginalPrice的长度 size_of(uint32_t)
				length += 4;  //计算字段ItemPrice的长度 size_of(uint32_t)
				length += 4;  //计算字段ItemResetTime的长度 size_of(uint32_t)
				length += 4;  //计算字段ItemShippingFee的长度 size_of(uint32_t)
				length += 4;  //计算字段LastUpdateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段MarkNostockTime的长度 size_of(uint32_t)
				length += 4;  //计算字段PayTime的长度 size_of(uint32_t)
				length += 4;  //计算字段PreTradeState的长度 size_of(uint32_t)
				length += 4;  //计算字段RecvfeeReturnTime的长度 size_of(uint32_t)
				length += 4;  //计算字段RecvfeeTime的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerConsignmentTime的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段TradePropertymask的长度 size_of(uint32_t)
				length += 4;  //计算字段TradeRefundState的长度 size_of(uint32_t)
				length += 4;  //计算字段TradeState的长度 size_of(uint32_t)
				length += 4;  //计算字段TradeType的长度 size_of(uint32_t)
				length += 4;  //计算字段TimeoutItemFlag的长度 size_of(uint32_t)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段DiscountFee的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(BuyerName, encoding);  //计算字段BuyerName的长度 size_of(String)
				length += ByteStream.getObjectSize(BuyerRemark, encoding);  //计算字段BuyerRemark的长度 size_of(String)
				length += ByteStream.getObjectSize(CloseReasonDesc, encoding);  //计算字段CloseReasonDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(DisItemId, encoding);  //计算字段DisItemId的长度 size_of(String)
				length += ByteStream.getObjectSize(DisTradeId, encoding);  //计算字段DisTradeId的长度 size_of(String)
				length += ByteStream.getObjectSize(ExtInfo, encoding);  //计算字段ExtInfo的长度 size_of(String)
				length += ByteStream.getObjectSize(ItemAccessoryDesc, encoding);  //计算字段ItemAccessoryDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(ItemAttrOptionValue, encoding);  //计算字段ItemAttrOptionValue的长度 size_of(String)
				length += ByteStream.getObjectSize(ItemLogo, encoding);  //计算字段ItemLogo的长度 size_of(String)
				length += ByteStream.getObjectSize(ItemName, encoding);  //计算字段ItemName的长度 size_of(String)
				length += ByteStream.getObjectSize(ItemPromotionDesc, encoding);  //计算字段ItemPromotionDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(PackageInfo, encoding);  //计算字段PackageInfo的长度 size_of(String)
				length += ByteStream.getObjectSize(ProductCode, encoding);  //计算字段ProductCode的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceiveDays, encoding);  //计算字段ReceiveDays的长度 size_of(String)
				length += ByteStream.getObjectSize(SellerName, encoding);  //计算字段SellerName的长度 size_of(String)
				length += ByteStream.getObjectSize(SellerRemark, encoding);  //计算字段SellerRemark的长度 size_of(String)
				length += ByteStream.getObjectSize(ShippingfeeDesc, encoding);  //计算字段ShippingfeeDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(Reserve, encoding);  //计算字段Reserve的长度 size_of(String)
				length += ByteStream.getObjectSize(ItemLocalCode, encoding);  //计算字段ItemLocalCode的长度 size_of(String)
				length += ByteStream.getObjectSize(StockLocalCode, encoding);  //计算字段StockLocalCode的长度 size_of(String)
				length += 4;  //计算字段ItemAdjustPrice的长度 size_of(int)
				if(  this.version >= 11 ){
						length += 4;  //计算字段TradePropertyMask2的长度 size_of(uint32_t)
				}
				if(  this.version >= 16 ){
						length += 4;  //计算字段BuyerFee的长度 size_of(uint32_t)
				}
				if(  this.version >= 16 ){
						length += 4;  //计算字段SellerFee的长度 size_of(uint32_t)
				}
				if(  this.version >= 16 ){
						length += ByteStream.getObjectSize(DrawId, encoding);  //计算字段DrawId的长度 size_of(String)
				}
				if(  this.version >= 16 ){
						length += ByteStream.getObjectSize(Token, encoding);  //计算字段Token的长度 size_of(String)
				}
				if(  this.version >= 14 ){
						length += 4;  //计算字段ItemVersion的长度 size_of(uint32_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本16所包含的字段*******
 *	Vector<CRefundInfo> RefundList;///<退款单信息表
 *	short DealItemState;///<订单商品状态
 *	short DealPayType;///<支付方式
 *	short DealState;///<订单状态
 *	short IsAutoSendItem;///<是否自动发货商品
 *	short ItemSnapversion;///<商品快照版本
 *	short ItemType;///<商品类型
 *	long DealId;///<订单号
 *	long ItemId;///<商品id
 *	long ItemStockId;///<商品库存编号
 *	long ShippingfeeTemplateId;///<运费模板编号
 *	long TradeId;///<子单号
 *	long TradeRefundId;///<退款协议ID
 *	long BuyerUin;///<买家号
 *	long CloseReason;///<关闭原因
 *	long CloseTime;///<关闭时间
 *	long DealCreateTime;///<生成时间
 *	long DealItemCount;///<购买数量
 *	long DealItemNum;///<实际销售数目
 *	long DealItemScore;///<积分
 *	long DealRefundState;///<退款投诉状态
 *	long DelayRecvtime;///<卖家延迟收货时间
 *	long ItemClass;///<商品类目
 *	long ItemCostPrice;///<商品成本价
 *	long ItemOriginalPrice;///<商品原价
 *	long ItemPrice;///<商品价格
 *	long ItemResetTime;///<商品重置时间
 *	long ItemShippingFee;///<商品运费
 *	long LastUpdateTime;///<最后更新时间
 *	long MarkNostockTime;///<卖家标记缺货时间
 *	long PayTime;///<支付完成时间
 *	long PreTradeState;
 *	long RecvfeeReturnTime;///<打款返回时间
 *	long RecvfeeTime;///<打款完成时间
 *	long SellerConsignmentTime;///<麦基发货时间
 *	long SellerUin;///<卖家uin
 *	long TradePropertymask;///<子单属性位
 *	long TradeRefundState;///<子单退款状态
 *	long TradeState;///<子单状态
 *	long TradeType;///<子单类型
 *	long TimeoutItemFlag;///<超时商品标识
 *	long version;///<版本号
 *	long DiscountFee;///<折扣（红包）金额
 *	String BuyerName;///<买家姓名
 *	String BuyerRemark;///<买家留言
 *	String CloseReasonDesc;///<关闭原因描述
 *	String DisItemId;///<商品id
 *	String DisTradeId;///<子单id
 *	String ExtInfo;///<扩展信息
 *	String ItemAccessoryDesc;///<商品标配说明
 *	String ItemAttrOptionValue;///<商品销售属性选项组合值
 *	String ItemLogo;///<商品图片主图
 *	String ItemName;///<商品名称
 *	String ItemPromotionDesc;///<商品促销说明
 *	String PackageInfo;///<套餐信息
 *	String ProductCode;///<产品编码
 *	String ReceiveDays;///<送送所需天数
 *	String SellerName;///<商家名称
 *	String SellerRemark;///<卖家备注
 *	String ShippingfeeDesc;///<运费模板说明
 *	String Reserve;
 *	String ItemLocalCode;///<商品本地编码
 *	String StockLocalCode;///<库存本地编码
 *	int ItemAdjustPrice;///<商品价格调整优惠
 *	long TradePropertyMask2;///<子单属性2
 *	long BuyerFee;///<买家金额
 *	long SellerFee;///<卖家金额
 *	String DrawId;///<DrawID
 *	String Token;///<Token
 *	long ItemVersion;///<(新)商品快照版本
 *****以上是版本16所包含的字段*******
 *
 *****以下是版本11所包含的字段*******
 *	Vector<CRefundInfo> RefundList;///<退款单信息表
 *	short DealItemState;///<订单商品状态
 *	short DealPayType;///<支付方式
 *	short DealState;///<订单状态
 *	short IsAutoSendItem;///<是否自动发货商品
 *	short ItemSnapversion;///<商品快照版本
 *	short ItemType;///<商品类型
 *	long DealId;///<订单号
 *	long ItemId;///<商品id
 *	long ItemStockId;///<商品库存编号
 *	long ShippingfeeTemplateId;///<运费模板编号
 *	long TradeId;///<子单号
 *	long TradeRefundId;///<退款协议ID
 *	long BuyerUin;///<买家号
 *	long CloseReason;///<关闭原因
 *	long CloseTime;///<关闭时间
 *	long DealCreateTime;///<生成时间
 *	long DealItemCount;///<购买数量
 *	long DealItemNum;///<实际销售数目
 *	long DealItemScore;///<积分
 *	long DealRefundState;///<退款投诉状态
 *	long DelayRecvtime;///<卖家延迟收货时间
 *	long ItemClass;///<商品类目
 *	long ItemCostPrice;///<商品成本价
 *	long ItemOriginalPrice;///<商品原价
 *	long ItemPrice;///<商品价格
 *	long ItemResetTime;///<商品重置时间
 *	long ItemShippingFee;///<商品运费
 *	long LastUpdateTime;///<最后更新时间
 *	long MarkNostockTime;///<卖家标记缺货时间
 *	long PayTime;///<支付完成时间
 *	long PreTradeState;
 *	long RecvfeeReturnTime;///<打款返回时间
 *	long RecvfeeTime;///<打款完成时间
 *	long SellerConsignmentTime;///<麦基发货时间
 *	long SellerUin;///<卖家uin
 *	long TradePropertymask;///<子单属性位
 *	long TradeRefundState;///<子单退款状态
 *	long TradeState;///<子单状态
 *	long TradeType;///<子单类型
 *	long TimeoutItemFlag;///<超时商品标识
 *	long version;///<版本号
 *	long DiscountFee;///<折扣（红包）金额
 *	String BuyerName;///<买家姓名
 *	String BuyerRemark;///<买家留言
 *	String CloseReasonDesc;///<关闭原因描述
 *	String DisItemId;///<商品id
 *	String DisTradeId;///<子单id
 *	String ExtInfo;///<扩展信息
 *	String ItemAccessoryDesc;///<商品标配说明
 *	String ItemAttrOptionValue;///<商品销售属性选项组合值
 *	String ItemLogo;///<商品图片主图
 *	String ItemName;///<商品名称
 *	String ItemPromotionDesc;///<商品促销说明
 *	String PackageInfo;///<套餐信息
 *	String ProductCode;///<产品编码
 *	String ReceiveDays;///<送送所需天数
 *	String SellerName;///<商家名称
 *	String SellerRemark;///<卖家备注
 *	String ShippingfeeDesc;///<运费模板说明
 *	String Reserve;
 *	String ItemLocalCode;///<商品本地编码
 *	String StockLocalCode;///<库存本地编码
 *	int ItemAdjustPrice;///<商品价格调整优惠
 *	long TradePropertyMask2;///<子单属性2
 *****以上是版本11所包含的字段*******
 *
 *****以下是版本14所包含的字段*******
 *	Vector<CRefundInfo> RefundList;///<退款单信息表
 *	short DealItemState;///<订单商品状态
 *	short DealPayType;///<支付方式
 *	short DealState;///<订单状态
 *	short IsAutoSendItem;///<是否自动发货商品
 *	short ItemSnapversion;///<商品快照版本
 *	short ItemType;///<商品类型
 *	long DealId;///<订单号
 *	long ItemId;///<商品id
 *	long ItemStockId;///<商品库存编号
 *	long ShippingfeeTemplateId;///<运费模板编号
 *	long TradeId;///<子单号
 *	long TradeRefundId;///<退款协议ID
 *	long BuyerUin;///<买家号
 *	long CloseReason;///<关闭原因
 *	long CloseTime;///<关闭时间
 *	long DealCreateTime;///<生成时间
 *	long DealItemCount;///<购买数量
 *	long DealItemNum;///<实际销售数目
 *	long DealItemScore;///<积分
 *	long DealRefundState;///<退款投诉状态
 *	long DelayRecvtime;///<卖家延迟收货时间
 *	long ItemClass;///<商品类目
 *	long ItemCostPrice;///<商品成本价
 *	long ItemOriginalPrice;///<商品原价
 *	long ItemPrice;///<商品价格
 *	long ItemResetTime;///<商品重置时间
 *	long ItemShippingFee;///<商品运费
 *	long LastUpdateTime;///<最后更新时间
 *	long MarkNostockTime;///<卖家标记缺货时间
 *	long PayTime;///<支付完成时间
 *	long PreTradeState;
 *	long RecvfeeReturnTime;///<打款返回时间
 *	long RecvfeeTime;///<打款完成时间
 *	long SellerConsignmentTime;///<麦基发货时间
 *	long SellerUin;///<卖家uin
 *	long TradePropertymask;///<子单属性位
 *	long TradeRefundState;///<子单退款状态
 *	long TradeState;///<子单状态
 *	long TradeType;///<子单类型
 *	long TimeoutItemFlag;///<超时商品标识
 *	long version;///<版本号
 *	long DiscountFee;///<折扣（红包）金额
 *	String BuyerName;///<买家姓名
 *	String BuyerRemark;///<买家留言
 *	String CloseReasonDesc;///<关闭原因描述
 *	String DisItemId;///<商品id
 *	String DisTradeId;///<子单id
 *	String ExtInfo;///<扩展信息
 *	String ItemAccessoryDesc;///<商品标配说明
 *	String ItemAttrOptionValue;///<商品销售属性选项组合值
 *	String ItemLogo;///<商品图片主图
 *	String ItemName;///<商品名称
 *	String ItemPromotionDesc;///<商品促销说明
 *	String PackageInfo;///<套餐信息
 *	String ProductCode;///<产品编码
 *	String ReceiveDays;///<送送所需天数
 *	String SellerName;///<商家名称
 *	String SellerRemark;///<卖家备注
 *	String ShippingfeeDesc;///<运费模板说明
 *	String Reserve;
 *	String ItemLocalCode;///<商品本地编码
 *	String StockLocalCode;///<库存本地编码
 *	int ItemAdjustPrice;///<商品价格调整优惠
 *	long TradePropertyMask2;///<子单属性2
 *	long ItemVersion;///<(新)商品快照版本
 *****以上是版本14所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
