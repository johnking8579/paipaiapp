//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.AddSceneReq.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *场景信息
 *
 *@date 2014-12-16 05:53:02
 *
 *@since version:0
*/
public class SceneInfoPo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private short version = 0;

	/**
	 * 场景内部id
	 *
	 * 版本 >= 0
	 */
	 private long id;

	/**
	 * 场景描述
	 *
	 * 版本 >= 0
	 */
	 private String desc = new String();

	/**
	 * 场景类型，当前有两种：隐藏、非隐藏
	 *
	 * 版本 >= 0
	 */
	 private short type;

	/**
	 * 场景权限
	 *
	 * 版本 >= 0
	 */
	 private String auth = new String();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserve = new String();

	/**
	 * 版本_u
	 *
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 场景内部id_u
	 *
	 * 版本 >= 0
	 */
	 private short id_u;

	/**
	 * 场景描述_u
	 *
	 * 版本 >= 0
	 */
	 private short desc_u;

	/**
	 * 场景类型_u
	 *
	 * 版本 >= 0
	 */
	 private short type_u;

	/**
	 * 场景权限_u
	 *
	 * 版本 >= 0
	 */
	 private short auth_u;

	/**
	 * 保留字段_u
	 *
	 * 版本 >= 0
	 */
	 private short reserve_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUByte(version);
		bs.pushUInt(id);
		bs.pushString(desc);
		bs.pushUByte(type);
		bs.pushString(auth);
		bs.pushString(reserve);
		bs.pushUByte(version_u);
		bs.pushUByte(id_u);
		bs.pushUByte(desc_u);
		bs.pushUByte(type_u);
		bs.pushUByte(auth_u);
		bs.pushUByte(reserve_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUByte();
		id = bs.popUInt();
		desc = bs.popString();
		type = bs.popUByte();
		auth = bs.popString();
		reserve = bs.popString();
		version_u = bs.popUByte();
		id_u = bs.popUByte();
		desc_u = bs.popUByte();
		type_u = bs.popUByte();
		auth_u = bs.popUByte();
		reserve_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:short
	 * 
	 */
	public short getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion(short value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取场景内部id
	 * 
	 * 此字段的版本 >= 0
	 * @return id value 类型为:long
	 * 
	 */
	public long getId()
	{
		return id;
	}


	/**
	 * 设置场景内部id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setId(long value)
	{
		this.id = value;
		this.id_u = 1;
	}

	public boolean issetId()
	{
		return this.id_u != 0;
	}
	/**
	 * 获取场景描述
	 * 
	 * 此字段的版本 >= 0
	 * @return desc value 类型为:String
	 * 
	 */
	public String getDesc()
	{
		return desc;
	}


	/**
	 * 设置场景描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDesc(String value)
	{
		this.desc = value;
		this.desc_u = 1;
	}

	public boolean issetDesc()
	{
		return this.desc_u != 0;
	}
	/**
	 * 获取场景类型，当前有两种：隐藏、非隐藏
	 * 
	 * 此字段的版本 >= 0
	 * @return type value 类型为:short
	 * 
	 */
	public short getType()
	{
		return type;
	}


	/**
	 * 设置场景类型，当前有两种：隐藏、非隐藏
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setType(short value)
	{
		this.type = value;
		this.type_u = 1;
	}

	public boolean issetType()
	{
		return this.type_u != 0;
	}
	/**
	 * 获取场景权限
	 * 
	 * 此字段的版本 >= 0
	 * @return auth value 类型为:String
	 * 
	 */
	public String getAuth()
	{
		return auth;
	}


	/**
	 * 设置场景权限
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAuth(String value)
	{
		this.auth = value;
		this.auth_u = 1;
	}

	public boolean issetAuth()
	{
		return this.auth_u != 0;
	}
	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserve value 类型为:String
	 * 
	 */
	public String getReserve()
	{
		return reserve;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserve(String value)
	{
		this.reserve = value;
		this.reserve_u = 1;
	}

	public boolean issetReserve()
	{
		return this.reserve_u != 0;
	}
	/**
	 * 获取版本_u
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置版本_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取场景内部id_u
	 * 
	 * 此字段的版本 >= 0
	 * @return id_u value 类型为:short
	 * 
	 */
	public short getId_u()
	{
		return id_u;
	}


	/**
	 * 设置场景内部id_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setId_u(short value)
	{
		this.id_u = value;
	}


	/**
	 * 获取场景描述_u
	 * 
	 * 此字段的版本 >= 0
	 * @return desc_u value 类型为:short
	 * 
	 */
	public short getDesc_u()
	{
		return desc_u;
	}


	/**
	 * 设置场景描述_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDesc_u(short value)
	{
		this.desc_u = value;
	}


	/**
	 * 获取场景类型_u
	 * 
	 * 此字段的版本 >= 0
	 * @return type_u value 类型为:short
	 * 
	 */
	public short getType_u()
	{
		return type_u;
	}


	/**
	 * 设置场景类型_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setType_u(short value)
	{
		this.type_u = value;
	}


	/**
	 * 获取场景权限_u
	 * 
	 * 此字段的版本 >= 0
	 * @return auth_u value 类型为:short
	 * 
	 */
	public short getAuth_u()
	{
		return auth_u;
	}


	/**
	 * 设置场景权限_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAuth_u(short value)
	{
		this.auth_u = value;
	}


	/**
	 * 获取保留字段_u
	 * 
	 * 此字段的版本 >= 0
	 * @return reserve_u value 类型为:short
	 * 
	 */
	public short getReserve_u()
	{
		return reserve_u;
	}


	/**
	 * 设置保留字段_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserve_u(short value)
	{
		this.reserve_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SceneInfoPo)
				length += 1;  //计算字段version的长度 size_of(uint8_t)
				length += 4;  //计算字段id的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(desc, null);  //计算字段desc的长度 size_of(String)
				length += 1;  //计算字段type的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(auth, null);  //计算字段auth的长度 size_of(String)
				length += ByteStream.getObjectSize(reserve, null);  //计算字段reserve的长度 size_of(String)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段id_u的长度 size_of(uint8_t)
				length += 1;  //计算字段desc_u的长度 size_of(uint8_t)
				length += 1;  //计算字段type_u的长度 size_of(uint8_t)
				length += 1;  //计算字段auth_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserve_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(SceneInfoPo)
				length += 1;  //计算字段version的长度 size_of(uint8_t)
				length += 4;  //计算字段id的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(desc, encoding);  //计算字段desc的长度 size_of(String)
				length += 1;  //计算字段type的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(auth, encoding);  //计算字段auth的长度 size_of(String)
				length += ByteStream.getObjectSize(reserve, encoding);  //计算字段reserve的长度 size_of(String)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段id_u的长度 size_of(uint8_t)
				length += 1;  //计算字段desc_u的长度 size_of(uint8_t)
				length += 1;  //计算字段type_u的长度 size_of(uint8_t)
				length += 1;  //计算字段auth_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserve_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
