 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.deelpc.protocol.DealIdl.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import java.util.Vector;

/**
 *获取订单信息列表new请求
 *
 *@date 2015-03-04 11:46:41
 *
 *@since version:0
*/
public class  GetDealInfoList2Req extends NetMessage
{
	/**
	 * 调用者==>请设置为源文件名
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 场景id
	 *
	 * 版本 >= 0
	 */
	 private long SceneId;

	/**
	 * 大订单列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<String> oDealIdList = new Vector<String>();

	/**
	 * 请求filter
	 *
	 * 版本 >= 0
	 */
	 private CDealListReqPoNew oFilter = new CDealListReqPoNew();

	/**
	 * 用户MachineKey
	 *
	 * 版本 >= 0
	 */
	 private String MachineKey = new String();

	/**
	 * 保留输入字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveIn = new String();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(Source);
		bs.pushUInt(SceneId);
		bs.pushObject(oDealIdList);
		bs.pushObject(oFilter);
		bs.pushString(MachineKey);
		bs.pushString(ReserveIn);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		Source = bs.popString();
		SceneId = bs.popUInt();
		oDealIdList = (Vector<String>)bs.popVector(String.class);
		oFilter = (CDealListReqPoNew) bs.popObject(CDealListReqPoNew.class);
		MachineKey = bs.popString();
		ReserveIn = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x2630180bL;
	}


	/**
	 * 获取调用者==>请设置为源文件名
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置调用者==>请设置为源文件名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取场景id
	 * 
	 * 此字段的版本 >= 0
	 * @return SceneId value 类型为:long
	 * 
	 */
	public long getSceneId()
	{
		return SceneId;
	}


	/**
	 * 设置场景id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSceneId(long value)
	{
		this.SceneId = value;
	}


	/**
	 * 获取大订单列表
	 * 
	 * 此字段的版本 >= 0
	 * @return oDealIdList value 类型为:Vector<String>
	 * 
	 */
	public Vector<String> getODealIdList()
	{
		return oDealIdList;
	}


	/**
	 * 设置大订单列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<String>
	 * 
	 */
	public void setODealIdList(Vector<String> value)
	{
		if (value != null) {
				this.oDealIdList = value;
		}else{
				this.oDealIdList = new Vector<String>();
		}
	}


	/**
	 * 获取请求filter
	 * 
	 * 此字段的版本 >= 0
	 * @return oFilter value 类型为:CDealListReqPoNew
	 * 
	 */
	public CDealListReqPoNew getOFilter()
	{
		return oFilter;
	}


	/**
	 * 设置请求filter
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:CDealListReqPoNew
	 * 
	 */
	public void setOFilter(CDealListReqPoNew value)
	{
		if (value != null) {
				this.oFilter = value;
		}else{
				this.oFilter = new CDealListReqPoNew();
		}
	}


	/**
	 * 获取用户MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @return MachineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return MachineKey;
	}


	/**
	 * 设置用户MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.MachineKey = value;
	}


	/**
	 * 获取保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveIn value 类型为:String
	 * 
	 */
	public String getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveIn(String value)
	{
		this.ReserveIn = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetDealInfoList2Req)
				length += ByteStream.getObjectSize(Source, null);  //计算字段Source的长度 size_of(String)
				length += 4;  //计算字段SceneId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(oDealIdList, null);  //计算字段oDealIdList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(oFilter, null);  //计算字段oFilter的长度 size_of(CDealListReqPoNew)
				length += ByteStream.getObjectSize(MachineKey, null);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveIn, null);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetDealInfoList2Req)
				length += ByteStream.getObjectSize(Source, encoding);  //计算字段Source的长度 size_of(String)
				length += 4;  //计算字段SceneId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(oDealIdList, encoding);  //计算字段oDealIdList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(oFilter, encoding);  //计算字段oFilter的长度 size_of(CDealListReqPoNew)
				length += ByteStream.getObjectSize(MachineKey, encoding);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveIn, encoding);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
