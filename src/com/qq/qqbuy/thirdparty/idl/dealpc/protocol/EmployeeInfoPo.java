package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *员工信息
 *
 *@date 2014-12-16 05:53:02
 *
 *@since version:0
*/
public class EmployeeInfoPo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private short version = 0;

	/**
	 * 卖家qq号
	 *
	 * 版本 >= 0
	 */
	 private long sellerUin;

	/**
	 * 员工qq号
	 *
	 * 版本 >= 0
	 */
	 private long employeeUin;

	/**
	 * 员工账号
	 *
	 * 版本 >= 0
	 */
	 private String account = new String();

	/**
	 * 员工昵称
	 *
	 * 版本 >= 0
	 */
	 private String nickName = new String();

	/**
	 * 员工姓名
	 *
	 * 版本 >= 0
	 */
	 private String name = new String();

	/**
	 * 员工角色
	 *
	 * 版本 >= 0
	 */
	 private String role = new String();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private long reserveInt;

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserve = new String();

	/**
	 * 版本_u
	 *
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 卖家qq号_u
	 *
	 * 版本 >= 0
	 */
	 private short sellerUin_u;

	/**
	 * 员工qq号_u
	 *
	 * 版本 >= 0
	 */
	 private short employeeUin_u;

	/**
	 * 员工账号_u
	 *
	 * 版本 >= 0
	 */
	 private short account_u;

	/**
	 * 员工昵称_u
	 *
	 * 版本 >= 0
	 */
	 private short nickName_u;

	/**
	 * 员工姓名_u
	 *
	 * 版本 >= 0
	 */
	 private short name_u;

	/**
	 * 员工角色_u
	 *
	 * 版本 >= 0
	 */
	 private short role_u;

	/**
	 * 保留字段_u
	 *
	 * 版本 >= 0
	 */
	 private short reserveInt_u;

	/**
	 * 保留字段_u
	 *
	 * 版本 >= 0
	 */
	 private short reserve_u;
	 /**
	  * 客户状态0：不在线1：离开2：在线
	  */
	 private int status;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUByte(version);
		bs.pushUInt(sellerUin);
		bs.pushUInt(employeeUin);
		bs.pushString(account);
		bs.pushString(nickName);
		bs.pushString(name);
		bs.pushString(role);
		bs.pushUInt(reserveInt);
		bs.pushString(reserve);
		bs.pushUByte(version_u);
		bs.pushUByte(sellerUin_u);
		bs.pushUByte(employeeUin_u);
		bs.pushUByte(account_u);
		bs.pushUByte(nickName_u);
		bs.pushUByte(name_u);
		bs.pushUByte(role_u);
		bs.pushUByte(reserveInt_u);
		bs.pushUByte(reserve_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUByte();
		sellerUin = bs.popUInt();
		employeeUin = bs.popUInt();
		account = bs.popString();
		nickName = bs.popString();
		name = bs.popString();
		role = bs.popString();
		reserveInt = bs.popUInt();
		reserve = bs.popString();
		version_u = bs.popUByte();
		sellerUin_u = bs.popUByte();
		employeeUin_u = bs.popUByte();
		account_u = bs.popUByte();
		nickName_u = bs.popUByte();
		name_u = bs.popUByte();
		role_u = bs.popUByte();
		reserveInt_u = bs.popUByte();
		reserve_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:short
	 * 
	 */
	public short getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion(short value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取卖家qq号
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return sellerUin;
	}


	/**
	 * 设置卖家qq号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.sellerUin = value;
		this.sellerUin_u = 1;
	}

	public boolean issetSellerUin()
	{
		return this.sellerUin_u != 0;
	}
	/**
	 * 获取员工qq号
	 * 
	 * 此字段的版本 >= 0
	 * @return employeeUin value 类型为:long
	 * 
	 */
	public long getEmployeeUin()
	{
		return employeeUin;
	}


	/**
	 * 设置员工qq号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEmployeeUin(long value)
	{
		this.employeeUin = value;
		this.employeeUin_u = 1;
	}

	public boolean issetEmployeeUin()
	{
		return this.employeeUin_u != 0;
	}
	/**
	 * 获取员工账号
	 * 
	 * 此字段的版本 >= 0
	 * @return account value 类型为:String
	 * 
	 */
	public String getAccount()
	{
		return account;
	}


	/**
	 * 设置员工账号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAccount(String value)
	{
		this.account = value;
		this.account_u = 1;
	}

	public boolean issetAccount()
	{
		return this.account_u != 0;
	}
	/**
	 * 获取员工昵称
	 * 
	 * 此字段的版本 >= 0
	 * @return nickName value 类型为:String
	 * 
	 */
	public String getNickName()
	{
		return nickName;
	}


	/**
	 * 设置员工昵称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setNickName(String value)
	{
		this.nickName = value;
		this.nickName_u = 1;
	}

	public boolean issetNickName()
	{
		return this.nickName_u != 0;
	}
	/**
	 * 获取员工姓名
	 * 
	 * 此字段的版本 >= 0
	 * @return name value 类型为:String
	 * 
	 */
	public String getName()
	{
		return name;
	}


	/**
	 * 设置员工姓名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setName(String value)
	{
		this.name = value;
		this.name_u = 1;
	}

	public boolean issetName()
	{
		return this.name_u != 0;
	}
	/**
	 * 获取员工角色
	 * 
	 * 此字段的版本 >= 0
	 * @return role value 类型为:String
	 * 
	 */
	public String getRole()
	{
		return role;
	}


	/**
	 * 设置员工角色
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRole(String value)
	{
		this.role = value;
		this.role_u = 1;
	}

	public boolean issetRole()
	{
		return this.role_u != 0;
	}
	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveInt value 类型为:long
	 * 
	 */
	public long getReserveInt()
	{
		return reserveInt;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setReserveInt(long value)
	{
		this.reserveInt = value;
		this.reserveInt_u = 1;
	}

	public boolean issetReserveInt()
	{
		return this.reserveInt_u != 0;
	}
	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserve value 类型为:String
	 * 
	 */
	public String getReserve()
	{
		return reserve;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserve(String value)
	{
		this.reserve = value;
		this.reserve_u = 1;
	}

	public boolean issetReserve()
	{
		return this.reserve_u != 0;
	}
	/**
	 * 获取版本_u
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置版本_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取卖家qq号_u
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerUin_u value 类型为:short
	 * 
	 */
	public short getSellerUin_u()
	{
		return sellerUin_u;
	}


	/**
	 * 设置卖家qq号_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerUin_u(short value)
	{
		this.sellerUin_u = value;
	}


	/**
	 * 获取员工qq号_u
	 * 
	 * 此字段的版本 >= 0
	 * @return employeeUin_u value 类型为:short
	 * 
	 */
	public short getEmployeeUin_u()
	{
		return employeeUin_u;
	}


	/**
	 * 设置员工qq号_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEmployeeUin_u(short value)
	{
		this.employeeUin_u = value;
	}


	/**
	 * 获取员工账号_u
	 * 
	 * 此字段的版本 >= 0
	 * @return account_u value 类型为:short
	 * 
	 */
	public short getAccount_u()
	{
		return account_u;
	}


	/**
	 * 设置员工账号_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAccount_u(short value)
	{
		this.account_u = value;
	}


	/**
	 * 获取员工昵称_u
	 * 
	 * 此字段的版本 >= 0
	 * @return nickName_u value 类型为:short
	 * 
	 */
	public short getNickName_u()
	{
		return nickName_u;
	}


	/**
	 * 设置员工昵称_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNickName_u(short value)
	{
		this.nickName_u = value;
	}


	/**
	 * 获取员工姓名_u
	 * 
	 * 此字段的版本 >= 0
	 * @return name_u value 类型为:short
	 * 
	 */
	public short getName_u()
	{
		return name_u;
	}


	/**
	 * 设置员工姓名_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setName_u(short value)
	{
		this.name_u = value;
	}


	/**
	 * 获取员工角色_u
	 * 
	 * 此字段的版本 >= 0
	 * @return role_u value 类型为:short
	 * 
	 */
	public short getRole_u()
	{
		return role_u;
	}


	/**
	 * 设置员工角色_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRole_u(short value)
	{
		this.role_u = value;
	}


	/**
	 * 获取保留字段_u
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveInt_u value 类型为:short
	 * 
	 */
	public short getReserveInt_u()
	{
		return reserveInt_u;
	}


	/**
	 * 设置保留字段_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserveInt_u(short value)
	{
		this.reserveInt_u = value;
	}


	/**
	 * 获取保留字段_u
	 * 
	 * 此字段的版本 >= 0
	 * @return reserve_u value 类型为:short
	 * 
	 */
	public short getReserve_u()
	{
		return reserve_u;
	}


	/**
	 * 设置保留字段_u
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserve_u(short value)
	{
		this.reserve_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(EmployeeInfoPo)
				length += 1;  //计算字段version的长度 size_of(uint8_t)
				length += 4;  //计算字段sellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段employeeUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(account, null);  //计算字段account的长度 size_of(String)
				length += ByteStream.getObjectSize(nickName, null);  //计算字段nickName的长度 size_of(String)
				length += ByteStream.getObjectSize(name, null);  //计算字段name的长度 size_of(String)
				length += ByteStream.getObjectSize(role, null);  //计算字段role的长度 size_of(String)
				length += 4;  //计算字段reserveInt的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(reserve, null);  //计算字段reserve的长度 size_of(String)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段sellerUin_u的长度 size_of(uint8_t)
				length += 1;  //计算字段employeeUin_u的长度 size_of(uint8_t)
				length += 1;  //计算字段account_u的长度 size_of(uint8_t)
				length += 1;  //计算字段nickName_u的长度 size_of(uint8_t)
				length += 1;  //计算字段name_u的长度 size_of(uint8_t)
				length += 1;  //计算字段role_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserveInt_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserve_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(EmployeeInfoPo)
				length += 1;  //计算字段version的长度 size_of(uint8_t)
				length += 4;  //计算字段sellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段employeeUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(account, encoding);  //计算字段account的长度 size_of(String)
				length += ByteStream.getObjectSize(nickName, encoding);  //计算字段nickName的长度 size_of(String)
				length += ByteStream.getObjectSize(name, encoding);  //计算字段name的长度 size_of(String)
				length += ByteStream.getObjectSize(role, encoding);  //计算字段role的长度 size_of(String)
				length += 4;  //计算字段reserveInt的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(reserve, encoding);  //计算字段reserve的长度 size_of(String)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段sellerUin_u的长度 size_of(uint8_t)
				length += 1;  //计算字段employeeUin_u的长度 size_of(uint8_t)
				length += 1;  //计算字段account_u的长度 size_of(uint8_t)
				length += 1;  //计算字段nickName_u的长度 size_of(uint8_t)
				length += 1;  //计算字段name_u的长度 size_of(uint8_t)
				length += 1;  //计算字段role_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserveInt_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserve_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
