//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.deelpc.protocol.DealIdl.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *订单列表请求filter
 *
 *@date 2015-03-04 11:46:40
 *
 *@since version:0
*/
public class CDealListReqPo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 卖家号
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * 版本 >= 0
	 */
	 private short SellerUin_u;

	/**
	 * 买家号
	 *
	 * 版本 >= 0
	 */
	 private long BuyerUin;

	/**
	 * 版本 >= 0
	 */
	 private short BuyerUin_u;

	/**
	 * 需要拉取的订单信息类型，请参照文档设置对应值，不要过多拉取无用信息，影响大家的性能
	 *
	 * 版本 >= 0
	 */
	 private long InfoType;

	/**
	 * 版本 >= 0
	 */
	 private short InfoType_u;

	/**
	 * 排序类型 0升序 1降序（订单生成时间）
	 *
	 * 版本 >= 0
	 */
	 private long TimeOrder;

	/**
	 * 版本 >= 0
	 */
	 private short TimeOrder_u;

	/**
	 * 起始时间
	 *
	 * 版本 >= 0
	 */
	 private long TimeStart;

	/**
	 * 版本 >= 0
	 */
	 private short TimeStart_u;

	/**
	 * 结束时间
	 *
	 * 版本 >= 0
	 */
	 private long TimeEnd;

	/**
	 * 版本 >= 0
	 */
	 private short TimeEnd_u;

	/**
	 * 页码，第一页请填 1
	 *
	 * 版本 >= 0
	 */
	 private long PageIndex;

	/**
	 * 版本 >= 0
	 */
	 private short PageIndex_u;

	/**
	 * 每页数量，请填写1-20, 默认为20
	 *
	 * 版本 >= 0
	 */
	 private long PageSize;

	/**
	 * 版本 >= 0
	 */
	 private short PageSize_u;

	/**
	 * 订单状态
	 *
	 * 版本 >= 0
	 */
	 private long DealState;

	/**
	 * 版本 >= 0
	 */
	 private short DealState_u;

	/**
	 * 商品id
	 *
	 * 版本 >= 0
	 */
	 private String ItemId = new String();

	/**
	 * 版本 >= 0
	 */
	 private short ItemId_u;

	/**
	 * 历史订单标记 0 false 1 true
	 *
	 * 版本 >= 0
	 */
	 private long HistoryFlag;

	/**
	 * 版本 >= 0
	 */
	 private short HistoryFlag_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(SellerUin);
		bs.pushUByte(SellerUin_u);
		bs.pushUInt(BuyerUin);
		bs.pushUByte(BuyerUin_u);
		bs.pushUInt(InfoType);
		bs.pushUByte(InfoType_u);
		bs.pushUInt(TimeOrder);
		bs.pushUByte(TimeOrder_u);
		bs.pushUInt(TimeStart);
		bs.pushUByte(TimeStart_u);
		bs.pushUInt(TimeEnd);
		bs.pushUByte(TimeEnd_u);
		bs.pushUInt(PageIndex);
		bs.pushUByte(PageIndex_u);
		bs.pushUInt(PageSize);
		bs.pushUByte(PageSize_u);
		bs.pushUInt(DealState);
		bs.pushUByte(DealState_u);
		bs.pushString(ItemId);
		bs.pushUByte(ItemId_u);
		bs.pushUInt(HistoryFlag);
		bs.pushUByte(HistoryFlag_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		SellerUin = bs.popUInt();
		SellerUin_u = bs.popUByte();
		BuyerUin = bs.popUInt();
		BuyerUin_u = bs.popUByte();
		InfoType = bs.popUInt();
		InfoType_u = bs.popUByte();
		TimeOrder = bs.popUInt();
		TimeOrder_u = bs.popUByte();
		TimeStart = bs.popUInt();
		TimeStart_u = bs.popUByte();
		TimeEnd = bs.popUInt();
		TimeEnd_u = bs.popUByte();
		PageIndex = bs.popUInt();
		PageIndex_u = bs.popUByte();
		PageSize = bs.popUInt();
		PageSize_u = bs.popUByte();
		DealState = bs.popUInt();
		DealState_u = bs.popUByte();
		ItemId = bs.popString();
		ItemId_u = bs.popUByte();
		HistoryFlag = bs.popUInt();
		HistoryFlag_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
		this.SellerUin_u = 1;
	}

	public boolean issetSellerUin()
	{
		return this.SellerUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin_u value 类型为:short
	 * 
	 */
	public short getSellerUin_u()
	{
		return SellerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerUin_u(short value)
	{
		this.SellerUin_u = value;
	}


	/**
	 * 获取买家号
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return BuyerUin;
	}


	/**
	 * 设置买家号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.BuyerUin = value;
		this.BuyerUin_u = 1;
	}

	public boolean issetBuyerUin()
	{
		return this.BuyerUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerUin_u value 类型为:short
	 * 
	 */
	public short getBuyerUin_u()
	{
		return BuyerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerUin_u(short value)
	{
		this.BuyerUin_u = value;
	}


	/**
	 * 获取需要拉取的订单信息类型，请参照文档设置对应值，不要过多拉取无用信息，影响大家的性能
	 * 
	 * 此字段的版本 >= 0
	 * @return InfoType value 类型为:long
	 * 
	 */
	public long getInfoType()
	{
		return InfoType;
	}


	/**
	 * 设置需要拉取的订单信息类型，请参照文档设置对应值，不要过多拉取无用信息，影响大家的性能
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setInfoType(long value)
	{
		this.InfoType = value;
		this.InfoType_u = 1;
	}

	public boolean issetInfoType()
	{
		return this.InfoType_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return InfoType_u value 类型为:short
	 * 
	 */
	public short getInfoType_u()
	{
		return InfoType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setInfoType_u(short value)
	{
		this.InfoType_u = value;
	}


	/**
	 * 获取排序类型 0升序 1降序（订单生成时间）
	 * 
	 * 此字段的版本 >= 0
	 * @return TimeOrder value 类型为:long
	 * 
	 */
	public long getTimeOrder()
	{
		return TimeOrder;
	}


	/**
	 * 设置排序类型 0升序 1降序（订单生成时间）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTimeOrder(long value)
	{
		this.TimeOrder = value;
		this.TimeOrder_u = 1;
	}

	public boolean issetTimeOrder()
	{
		return this.TimeOrder_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return TimeOrder_u value 类型为:short
	 * 
	 */
	public short getTimeOrder_u()
	{
		return TimeOrder_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTimeOrder_u(short value)
	{
		this.TimeOrder_u = value;
	}


	/**
	 * 获取起始时间
	 * 
	 * 此字段的版本 >= 0
	 * @return TimeStart value 类型为:long
	 * 
	 */
	public long getTimeStart()
	{
		return TimeStart;
	}


	/**
	 * 设置起始时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTimeStart(long value)
	{
		this.TimeStart = value;
		this.TimeStart_u = 1;
	}

	public boolean issetTimeStart()
	{
		return this.TimeStart_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return TimeStart_u value 类型为:short
	 * 
	 */
	public short getTimeStart_u()
	{
		return TimeStart_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTimeStart_u(short value)
	{
		this.TimeStart_u = value;
	}


	/**
	 * 获取结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return TimeEnd value 类型为:long
	 * 
	 */
	public long getTimeEnd()
	{
		return TimeEnd;
	}


	/**
	 * 设置结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTimeEnd(long value)
	{
		this.TimeEnd = value;
		this.TimeEnd_u = 1;
	}

	public boolean issetTimeEnd()
	{
		return this.TimeEnd_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return TimeEnd_u value 类型为:short
	 * 
	 */
	public short getTimeEnd_u()
	{
		return TimeEnd_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTimeEnd_u(short value)
	{
		this.TimeEnd_u = value;
	}


	/**
	 * 获取页码，第一页请填 1
	 * 
	 * 此字段的版本 >= 0
	 * @return PageIndex value 类型为:long
	 * 
	 */
	public long getPageIndex()
	{
		return PageIndex;
	}


	/**
	 * 设置页码，第一页请填 1
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPageIndex(long value)
	{
		this.PageIndex = value;
		this.PageIndex_u = 1;
	}

	public boolean issetPageIndex()
	{
		return this.PageIndex_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PageIndex_u value 类型为:short
	 * 
	 */
	public short getPageIndex_u()
	{
		return PageIndex_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPageIndex_u(short value)
	{
		this.PageIndex_u = value;
	}


	/**
	 * 获取每页数量，请填写1-20, 默认为20
	 * 
	 * 此字段的版本 >= 0
	 * @return PageSize value 类型为:long
	 * 
	 */
	public long getPageSize()
	{
		return PageSize;
	}


	/**
	 * 设置每页数量，请填写1-20, 默认为20
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPageSize(long value)
	{
		this.PageSize = value;
		this.PageSize_u = 1;
	}

	public boolean issetPageSize()
	{
		return this.PageSize_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PageSize_u value 类型为:short
	 * 
	 */
	public short getPageSize_u()
	{
		return PageSize_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPageSize_u(short value)
	{
		this.PageSize_u = value;
	}


	/**
	 * 获取订单状态
	 * 
	 * 此字段的版本 >= 0
	 * @return DealState value 类型为:long
	 * 
	 */
	public long getDealState()
	{
		return DealState;
	}


	/**
	 * 设置订单状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealState(long value)
	{
		this.DealState = value;
		this.DealState_u = 1;
	}

	public boolean issetDealState()
	{
		return this.DealState_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DealState_u value 类型为:short
	 * 
	 */
	public short getDealState_u()
	{
		return DealState_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealState_u(short value)
	{
		this.DealState_u = value;
	}


	/**
	 * 获取商品id
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemId value 类型为:String
	 * 
	 */
	public String getItemId()
	{
		return ItemId;
	}


	/**
	 * 设置商品id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemId(String value)
	{
		this.ItemId = value;
		this.ItemId_u = 1;
	}

	public boolean issetItemId()
	{
		return this.ItemId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemId_u value 类型为:short
	 * 
	 */
	public short getItemId_u()
	{
		return ItemId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemId_u(short value)
	{
		this.ItemId_u = value;
	}


	/**
	 * 获取历史订单标记 0 false 1 true
	 * 
	 * 此字段的版本 >= 0
	 * @return HistoryFlag value 类型为:long
	 * 
	 */
	public long getHistoryFlag()
	{
		return HistoryFlag;
	}


	/**
	 * 设置历史订单标记 0 false 1 true
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setHistoryFlag(long value)
	{
		this.HistoryFlag = value;
		this.HistoryFlag_u = 1;
	}

	public boolean issetHistoryFlag()
	{
		return this.HistoryFlag_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return HistoryFlag_u value 类型为:short
	 * 
	 */
	public short getHistoryFlag_u()
	{
		return HistoryFlag_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setHistoryFlag_u(short value)
	{
		this.HistoryFlag_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CDealListReqPo)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段InfoType的长度 size_of(uint32_t)
				length += 1;  //计算字段InfoType_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TimeOrder的长度 size_of(uint32_t)
				length += 1;  //计算字段TimeOrder_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TimeStart的长度 size_of(uint32_t)
				length += 1;  //计算字段TimeStart_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TimeEnd的长度 size_of(uint32_t)
				length += 1;  //计算字段TimeEnd_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PageIndex的长度 size_of(uint32_t)
				length += 1;  //计算字段PageIndex_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PageSize的长度 size_of(uint32_t)
				length += 1;  //计算字段PageSize_u的长度 size_of(uint8_t)
				length += 4;  //计算字段DealState的长度 size_of(uint32_t)
				length += 1;  //计算字段DealState_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ItemId, null);  //计算字段ItemId的长度 size_of(String)
				length += 1;  //计算字段ItemId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段HistoryFlag的长度 size_of(uint32_t)
				length += 1;  //计算字段HistoryFlag_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(CDealListReqPo)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段InfoType的长度 size_of(uint32_t)
				length += 1;  //计算字段InfoType_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TimeOrder的长度 size_of(uint32_t)
				length += 1;  //计算字段TimeOrder_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TimeStart的长度 size_of(uint32_t)
				length += 1;  //计算字段TimeStart_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TimeEnd的长度 size_of(uint32_t)
				length += 1;  //计算字段TimeEnd_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PageIndex的长度 size_of(uint32_t)
				length += 1;  //计算字段PageIndex_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PageSize的长度 size_of(uint32_t)
				length += 1;  //计算字段PageSize_u的长度 size_of(uint8_t)
				length += 4;  //计算字段DealState的长度 size_of(uint32_t)
				length += 1;  //计算字段DealState_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ItemId, encoding);  //计算字段ItemId的长度 size_of(String)
				length += 1;  //计算字段ItemId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段HistoryFlag的长度 size_of(uint32_t)
				length += 1;  //计算字段HistoryFlag_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
