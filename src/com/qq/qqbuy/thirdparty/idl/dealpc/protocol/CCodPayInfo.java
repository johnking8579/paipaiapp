//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.deelpc.protocol.DealIdl.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *COD单支付信息
 *
 *@date 2015-03-04 11:46:41
 *
 *@since version:0
*/
public class CCodPayInfo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 订单生成时间
	 *
	 * 版本 >= 0
	 */
	 private long DealCreateTime;

	/**
	 * 订单类型
	 *
	 * 版本 >= 0
	 */
	 private long DealType;

	/**
	 * 卖家号
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * 买家号
	 *
	 * 版本 >= 0
	 */
	 private long BuyerUin;

	/**
	 * 商品总价
	 *
	 * 版本 >= 0
	 */
	 private long SumFee;

	/**
	 * 运费
	 *
	 * 版本 >= 0
	 */
	 private long MailFee;

	/**
	 * 打款完成时间
	 *
	 * 版本 >= 0
	 */
	 private long RecvfeeTime;

	/**
	 * 打款返回时间
	 *
	 * 版本 >= 0
	 */
	 private long RecvfeeReturnTime;

	/**
	 * 支付手续费
	 *
	 * 版本 >= 0
	 */
	 private int PayHandleFee;

	/**
	 * 最后更新时间
	 *
	 * 版本 >= 0
	 */
	 private long LastUpdateTime;

	/**
	 * 支付单状态
	 *
	 * 版本 >= 0
	 */
	 private long PayState;

	/**
	 * 商品金额，不含运费
	 *
	 * 版本 >= 0
	 */
	 private long ItemFee;

	/**
	 * 拍拍手续费
	 *
	 * 版本 >= 0
	 */
	 private long CodPaipaiCommission;

	/**
	 * 物流手续费
	 *
	 * 版本 >= 0
	 */
	 private long CodWuliuCommission;

	/**
	 * 财付通手续费
	 *
	 * 版本 >= 0
	 */
	 private long CodCftCommission;

	/**
	 * 总手续费
	 *
	 * 版本 >= 0
	 */
	 private long CodTotalCommission;

	/**
	 * 物流打款金额
	 *
	 * 版本 >= 0
	 */
	 private long CodWuliuPayFee;

	/**
	 * 卖家收款金额
	 *
	 * 版本 >= 0
	 */
	 private long CodSellerRecvFee;

	/**
	 * 财付通货到付款单生成时间
	 *
	 * 版本 >= 0
	 */
	 private long CodCftGenTime;

	/**
	 * 物流公司签收时间
	 *
	 * 版本 >= 0
	 */
	 private long CodSignTime;

	/**
	 * 拍拍签收返回时间
	 *
	 * 版本 >= 0
	 */
	 private long CodSignReturnTime;

	/**
	 * 订单id
	 *
	 * 版本 >= 0
	 */
	 private long DealId;

	/**
	 * 支付单编号
	 *
	 * 版本 >= 0
	 */
	 private long PayId;

	/**
	 * 商家名称
	 *
	 * 版本 >= 0
	 */
	 private String SellerName = new String();

	/**
	 * 买家名称
	 *
	 * 版本 >= 0
	 */
	 private String BuyerName = new String();

	/**
	 * 商品标题列表
	 *
	 * 版本 >= 0
	 */
	 private String ItemTitleList = new String();

	/**
	 * 订单支付ID
	 *
	 * 版本 >= 0
	 */
	 private String DealCftPayid = new String();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String Reserve = new String();

	/**
	 * 物流商户号
	 *
	 * 版本 >= 0
	 */
	 private String CodWuliuSpid = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUInt(DealCreateTime);
		bs.pushUInt(DealType);
		bs.pushUInt(SellerUin);
		bs.pushUInt(BuyerUin);
		bs.pushUInt(SumFee);
		bs.pushUInt(MailFee);
		bs.pushUInt(RecvfeeTime);
		bs.pushUInt(RecvfeeReturnTime);
		bs.pushInt(PayHandleFee);
		bs.pushUInt(LastUpdateTime);
		bs.pushUInt(PayState);
		bs.pushUInt(ItemFee);
		bs.pushUInt(CodPaipaiCommission);
		bs.pushUInt(CodWuliuCommission);
		bs.pushUInt(CodCftCommission);
		bs.pushUInt(CodTotalCommission);
		bs.pushUInt(CodWuliuPayFee);
		bs.pushUInt(CodSellerRecvFee);
		bs.pushUInt(CodCftGenTime);
		bs.pushUInt(CodSignTime);
		bs.pushUInt(CodSignReturnTime);
		bs.pushLong(DealId);
		bs.pushLong(PayId);
		bs.pushString(SellerName);
		bs.pushString(BuyerName);
		bs.pushString(ItemTitleList);
		bs.pushString(DealCftPayid);
		bs.pushString(Reserve);
		bs.pushString(CodWuliuSpid);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		DealCreateTime = bs.popUInt();
		DealType = bs.popUInt();
		SellerUin = bs.popUInt();
		BuyerUin = bs.popUInt();
		SumFee = bs.popUInt();
		MailFee = bs.popUInt();
		RecvfeeTime = bs.popUInt();
		RecvfeeReturnTime = bs.popUInt();
		PayHandleFee = bs.popInt();
		LastUpdateTime = bs.popUInt();
		PayState = bs.popUInt();
		ItemFee = bs.popUInt();
		CodPaipaiCommission = bs.popUInt();
		CodWuliuCommission = bs.popUInt();
		CodCftCommission = bs.popUInt();
		CodTotalCommission = bs.popUInt();
		CodWuliuPayFee = bs.popUInt();
		CodSellerRecvFee = bs.popUInt();
		CodCftGenTime = bs.popUInt();
		CodSignTime = bs.popUInt();
		CodSignReturnTime = bs.popUInt();
		DealId = bs.popLong();
		PayId = bs.popLong();
		SellerName = bs.popString();
		BuyerName = bs.popString();
		ItemTitleList = bs.popString();
		DealCftPayid = bs.popString();
		Reserve = bs.popString();
		CodWuliuSpid = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取订单生成时间
	 * 
	 * 此字段的版本 >= 0
	 * @return DealCreateTime value 类型为:long
	 * 
	 */
	public long getDealCreateTime()
	{
		return DealCreateTime;
	}


	/**
	 * 设置订单生成时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealCreateTime(long value)
	{
		this.DealCreateTime = value;
	}


	/**
	 * 获取订单类型
	 * 
	 * 此字段的版本 >= 0
	 * @return DealType value 类型为:long
	 * 
	 */
	public long getDealType()
	{
		return DealType;
	}


	/**
	 * 设置订单类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealType(long value)
	{
		this.DealType = value;
	}


	/**
	 * 获取卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
	}


	/**
	 * 获取买家号
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return BuyerUin;
	}


	/**
	 * 设置买家号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.BuyerUin = value;
	}


	/**
	 * 获取商品总价
	 * 
	 * 此字段的版本 >= 0
	 * @return SumFee value 类型为:long
	 * 
	 */
	public long getSumFee()
	{
		return SumFee;
	}


	/**
	 * 设置商品总价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSumFee(long value)
	{
		this.SumFee = value;
	}


	/**
	 * 获取运费
	 * 
	 * 此字段的版本 >= 0
	 * @return MailFee value 类型为:long
	 * 
	 */
	public long getMailFee()
	{
		return MailFee;
	}


	/**
	 * 设置运费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMailFee(long value)
	{
		this.MailFee = value;
	}


	/**
	 * 获取打款完成时间
	 * 
	 * 此字段的版本 >= 0
	 * @return RecvfeeTime value 类型为:long
	 * 
	 */
	public long getRecvfeeTime()
	{
		return RecvfeeTime;
	}


	/**
	 * 设置打款完成时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRecvfeeTime(long value)
	{
		this.RecvfeeTime = value;
	}


	/**
	 * 获取打款返回时间
	 * 
	 * 此字段的版本 >= 0
	 * @return RecvfeeReturnTime value 类型为:long
	 * 
	 */
	public long getRecvfeeReturnTime()
	{
		return RecvfeeReturnTime;
	}


	/**
	 * 设置打款返回时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRecvfeeReturnTime(long value)
	{
		this.RecvfeeReturnTime = value;
	}


	/**
	 * 获取支付手续费
	 * 
	 * 此字段的版本 >= 0
	 * @return PayHandleFee value 类型为:int
	 * 
	 */
	public int getPayHandleFee()
	{
		return PayHandleFee;
	}


	/**
	 * 设置支付手续费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPayHandleFee(int value)
	{
		this.PayHandleFee = value;
	}


	/**
	 * 获取最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return LastUpdateTime value 类型为:long
	 * 
	 */
	public long getLastUpdateTime()
	{
		return LastUpdateTime;
	}


	/**
	 * 设置最后更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastUpdateTime(long value)
	{
		this.LastUpdateTime = value;
	}


	/**
	 * 获取支付单状态
	 * 
	 * 此字段的版本 >= 0
	 * @return PayState value 类型为:long
	 * 
	 */
	public long getPayState()
	{
		return PayState;
	}


	/**
	 * 设置支付单状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayState(long value)
	{
		this.PayState = value;
	}


	/**
	 * 获取商品金额，不含运费
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemFee value 类型为:long
	 * 
	 */
	public long getItemFee()
	{
		return ItemFee;
	}


	/**
	 * 设置商品金额，不含运费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setItemFee(long value)
	{
		this.ItemFee = value;
	}


	/**
	 * 获取拍拍手续费
	 * 
	 * 此字段的版本 >= 0
	 * @return CodPaipaiCommission value 类型为:long
	 * 
	 */
	public long getCodPaipaiCommission()
	{
		return CodPaipaiCommission;
	}


	/**
	 * 设置拍拍手续费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCodPaipaiCommission(long value)
	{
		this.CodPaipaiCommission = value;
	}


	/**
	 * 获取物流手续费
	 * 
	 * 此字段的版本 >= 0
	 * @return CodWuliuCommission value 类型为:long
	 * 
	 */
	public long getCodWuliuCommission()
	{
		return CodWuliuCommission;
	}


	/**
	 * 设置物流手续费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCodWuliuCommission(long value)
	{
		this.CodWuliuCommission = value;
	}


	/**
	 * 获取财付通手续费
	 * 
	 * 此字段的版本 >= 0
	 * @return CodCftCommission value 类型为:long
	 * 
	 */
	public long getCodCftCommission()
	{
		return CodCftCommission;
	}


	/**
	 * 设置财付通手续费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCodCftCommission(long value)
	{
		this.CodCftCommission = value;
	}


	/**
	 * 获取总手续费
	 * 
	 * 此字段的版本 >= 0
	 * @return CodTotalCommission value 类型为:long
	 * 
	 */
	public long getCodTotalCommission()
	{
		return CodTotalCommission;
	}


	/**
	 * 设置总手续费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCodTotalCommission(long value)
	{
		this.CodTotalCommission = value;
	}


	/**
	 * 获取物流打款金额
	 * 
	 * 此字段的版本 >= 0
	 * @return CodWuliuPayFee value 类型为:long
	 * 
	 */
	public long getCodWuliuPayFee()
	{
		return CodWuliuPayFee;
	}


	/**
	 * 设置物流打款金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCodWuliuPayFee(long value)
	{
		this.CodWuliuPayFee = value;
	}


	/**
	 * 获取卖家收款金额
	 * 
	 * 此字段的版本 >= 0
	 * @return CodSellerRecvFee value 类型为:long
	 * 
	 */
	public long getCodSellerRecvFee()
	{
		return CodSellerRecvFee;
	}


	/**
	 * 设置卖家收款金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCodSellerRecvFee(long value)
	{
		this.CodSellerRecvFee = value;
	}


	/**
	 * 获取财付通货到付款单生成时间
	 * 
	 * 此字段的版本 >= 0
	 * @return CodCftGenTime value 类型为:long
	 * 
	 */
	public long getCodCftGenTime()
	{
		return CodCftGenTime;
	}


	/**
	 * 设置财付通货到付款单生成时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCodCftGenTime(long value)
	{
		this.CodCftGenTime = value;
	}


	/**
	 * 获取物流公司签收时间
	 * 
	 * 此字段的版本 >= 0
	 * @return CodSignTime value 类型为:long
	 * 
	 */
	public long getCodSignTime()
	{
		return CodSignTime;
	}


	/**
	 * 设置物流公司签收时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCodSignTime(long value)
	{
		this.CodSignTime = value;
	}


	/**
	 * 获取拍拍签收返回时间
	 * 
	 * 此字段的版本 >= 0
	 * @return CodSignReturnTime value 类型为:long
	 * 
	 */
	public long getCodSignReturnTime()
	{
		return CodSignReturnTime;
	}


	/**
	 * 设置拍拍签收返回时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCodSignReturnTime(long value)
	{
		this.CodSignReturnTime = value;
	}


	/**
	 * 获取订单id
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:long
	 * 
	 */
	public long getDealId()
	{
		return DealId;
	}


	/**
	 * 设置订单id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealId(long value)
	{
		this.DealId = value;
	}


	/**
	 * 获取支付单编号
	 * 
	 * 此字段的版本 >= 0
	 * @return PayId value 类型为:long
	 * 
	 */
	public long getPayId()
	{
		return PayId;
	}


	/**
	 * 设置支付单编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayId(long value)
	{
		this.PayId = value;
	}


	/**
	 * 获取商家名称
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerName value 类型为:String
	 * 
	 */
	public String getSellerName()
	{
		return SellerName;
	}


	/**
	 * 设置商家名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSellerName(String value)
	{
		this.SellerName = value;
	}


	/**
	 * 获取买家名称
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerName value 类型为:String
	 * 
	 */
	public String getBuyerName()
	{
		return BuyerName;
	}


	/**
	 * 设置买家名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBuyerName(String value)
	{
		this.BuyerName = value;
	}


	/**
	 * 获取商品标题列表
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemTitleList value 类型为:String
	 * 
	 */
	public String getItemTitleList()
	{
		return ItemTitleList;
	}


	/**
	 * 设置商品标题列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemTitleList(String value)
	{
		this.ItemTitleList = value;
	}


	/**
	 * 获取订单支付ID
	 * 
	 * 此字段的版本 >= 0
	 * @return DealCftPayid value 类型为:String
	 * 
	 */
	public String getDealCftPayid()
	{
		return DealCftPayid;
	}


	/**
	 * 设置订单支付ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealCftPayid(String value)
	{
		this.DealCftPayid = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return Reserve value 类型为:String
	 * 
	 */
	public String getReserve()
	{
		return Reserve;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserve(String value)
	{
		this.Reserve = value;
	}


	/**
	 * 获取物流商户号
	 * 
	 * 此字段的版本 >= 0
	 * @return CodWuliuSpid value 类型为:String
	 * 
	 */
	public String getCodWuliuSpid()
	{
		return CodWuliuSpid;
	}


	/**
	 * 设置物流商户号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCodWuliuSpid(String value)
	{
		this.CodWuliuSpid = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CCodPayInfo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段DealCreateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段DealType的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段SumFee的长度 size_of(uint32_t)
				length += 4;  //计算字段MailFee的长度 size_of(uint32_t)
				length += 4;  //计算字段RecvfeeTime的长度 size_of(uint32_t)
				length += 4;  //计算字段RecvfeeReturnTime的长度 size_of(uint32_t)
				length += 4;  //计算字段PayHandleFee的长度 size_of(int)
				length += 4;  //计算字段LastUpdateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段PayState的长度 size_of(uint32_t)
				length += 4;  //计算字段ItemFee的长度 size_of(uint32_t)
				length += 4;  //计算字段CodPaipaiCommission的长度 size_of(uint32_t)
				length += 4;  //计算字段CodWuliuCommission的长度 size_of(uint32_t)
				length += 4;  //计算字段CodCftCommission的长度 size_of(uint32_t)
				length += 4;  //计算字段CodTotalCommission的长度 size_of(uint32_t)
				length += 4;  //计算字段CodWuliuPayFee的长度 size_of(uint32_t)
				length += 4;  //计算字段CodSellerRecvFee的长度 size_of(uint32_t)
				length += 4;  //计算字段CodCftGenTime的长度 size_of(uint32_t)
				length += 4;  //计算字段CodSignTime的长度 size_of(uint32_t)
				length += 4;  //计算字段CodSignReturnTime的长度 size_of(uint32_t)
				length += 17;  //计算字段DealId的长度 size_of(uint64_t)
				length += 17;  //计算字段PayId的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(SellerName, null);  //计算字段SellerName的长度 size_of(String)
				length += ByteStream.getObjectSize(BuyerName, null);  //计算字段BuyerName的长度 size_of(String)
				length += ByteStream.getObjectSize(ItemTitleList, null);  //计算字段ItemTitleList的长度 size_of(String)
				length += ByteStream.getObjectSize(DealCftPayid, null);  //计算字段DealCftPayid的长度 size_of(String)
				length += ByteStream.getObjectSize(Reserve, null);  //计算字段Reserve的长度 size_of(String)
				length += ByteStream.getObjectSize(CodWuliuSpid, null);  //计算字段CodWuliuSpid的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(CCodPayInfo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段DealCreateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段DealType的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段SumFee的长度 size_of(uint32_t)
				length += 4;  //计算字段MailFee的长度 size_of(uint32_t)
				length += 4;  //计算字段RecvfeeTime的长度 size_of(uint32_t)
				length += 4;  //计算字段RecvfeeReturnTime的长度 size_of(uint32_t)
				length += 4;  //计算字段PayHandleFee的长度 size_of(int)
				length += 4;  //计算字段LastUpdateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段PayState的长度 size_of(uint32_t)
				length += 4;  //计算字段ItemFee的长度 size_of(uint32_t)
				length += 4;  //计算字段CodPaipaiCommission的长度 size_of(uint32_t)
				length += 4;  //计算字段CodWuliuCommission的长度 size_of(uint32_t)
				length += 4;  //计算字段CodCftCommission的长度 size_of(uint32_t)
				length += 4;  //计算字段CodTotalCommission的长度 size_of(uint32_t)
				length += 4;  //计算字段CodWuliuPayFee的长度 size_of(uint32_t)
				length += 4;  //计算字段CodSellerRecvFee的长度 size_of(uint32_t)
				length += 4;  //计算字段CodCftGenTime的长度 size_of(uint32_t)
				length += 4;  //计算字段CodSignTime的长度 size_of(uint32_t)
				length += 4;  //计算字段CodSignReturnTime的长度 size_of(uint32_t)
				length += 17;  //计算字段DealId的长度 size_of(uint64_t)
				length += 17;  //计算字段PayId的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(SellerName, encoding);  //计算字段SellerName的长度 size_of(String)
				length += ByteStream.getObjectSize(BuyerName, encoding);  //计算字段BuyerName的长度 size_of(String)
				length += ByteStream.getObjectSize(ItemTitleList, encoding);  //计算字段ItemTitleList的长度 size_of(String)
				length += ByteStream.getObjectSize(DealCftPayid, encoding);  //计算字段DealCftPayid的长度 size_of(String)
				length += ByteStream.getObjectSize(Reserve, encoding);  //计算字段Reserve的长度 size_of(String)
				length += ByteStream.getObjectSize(CodWuliuSpid, encoding);  //计算字段CodWuliuSpid的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
