 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.deelpc.protocol.DealIdl.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *获取订单信息
 *
 *@date 2015-03-04 11:46:41
 *
 *@since version:0
*/
public class  GetDealInfoResp extends NetMessage
{
	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String ErrMsg = new String();

	/**
	 * 返回订单信息
	 *
	 * 版本 >= 0
	 */
	 private CDealInfo oDealInfo = new CDealInfo();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushString(ErrMsg);
		bs.pushObject(oDealInfo);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		ErrMsg = bs.popString();
		oDealInfo = (CDealInfo) bs.popObject(CDealInfo.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x2630880aL;
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return ErrMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return ErrMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.ErrMsg = value;
	}


	/**
	 * 获取返回订单信息
	 * 
	 * 此字段的版本 >= 0
	 * @return oDealInfo value 类型为:CDealInfo
	 * 
	 */
	public CDealInfo getODealInfo()
	{
		return oDealInfo;
	}


	/**
	 * 设置返回订单信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:CDealInfo
	 * 
	 */
	public void setODealInfo(CDealInfo value)
	{
		if (value != null) {
				this.oDealInfo = value;
		}else{
				this.oDealInfo = new CDealInfo();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetDealInfoResp)
				length += ByteStream.getObjectSize(ErrMsg, null);  //计算字段ErrMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(oDealInfo, null);  //计算字段oDealInfo的长度 size_of(CDealInfo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetDealInfoResp)
				length += ByteStream.getObjectSize(ErrMsg, encoding);  //计算字段ErrMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(oDealInfo, encoding);  //计算字段oDealInfo的长度 size_of(CDealInfo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
