//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.deelpc.protocol.DealIdl.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *大订单info
 *
 *@date 2015-03-04 11:46:41
 *
 *@since version:0
*/
public class CDealInfo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 承担运费方式,1卖家承担运费，2买家承担运费，3无需运费，0x0a支持运费模板的边界值
	 *
	 * 版本 >= 0
	 */
	 private short WhoPayShippingfee;

	/**
	 * 订单类型, 所有类型 1--一口价 SELL_TYPE_BIN 2--单件拍卖，3--b2c订单
	 *
	 * 版本 >= 0
	 */
	 private short DealType;

	/**
	 * 订单状态
	 *
	 * 版本 >= 0
	 */
	 private short DealState;

	/**
	 * preState
	 *
	 * 版本 >= 0
	 */
	 private short PreDealState;

	/**
	 * 支付方式 0未定义 1支付中介 2未使用财付通付款(货到付款) 3分期付款 4移动积分
	 *
	 * 版本 >= 0
	 */
	 private short DealPayType;

	/**
	 * id
	 *
	 * 版本 >= 0
	 */
	 private long DealId;

	/**
	 * 订单序列
	 *
	 * 版本 >= 0
	 */
	 private long GenTimestamp;

	/**
	 * 物流id
	 *
	 * 版本 >= 0
	 */
	 private long WuliuId;

	/**
	 * 商品ID签名
	 *
	 * 版本 >= 0
	 */
	 private long ItemlistMd5;

	/**
	 * 订单支付ID
	 *
	 * 版本 >= 0
	 */
	 private long PayId;

	/**
	 * 折扣优惠金额
	 *
	 * 版本 >= 0
	 */
	 private int CouponFee;

	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 卖家号
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * 买家号
	 *
	 * 版本 >= 0
	 */
	 private long BuyerUin;

	/**
	 * 邮费
	 *
	 * 版本 >= 0
	 */
	 private long DealPayFeeShipping;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long PreDealPayFeeTotal;

	/**
	 * 费用合计
	 *
	 * 版本 >= 0
	 */
	 private long DealPayFeeTotal;

	/**
	 * 订单生成时间
	 *
	 * 版本 >= 0
	 */
	 private long DealCreateTime;

	/**
	 * 客服CRM
	 *
	 * 版本 >= 0
	 */
	 private long SellerCrm;

	/**
	 * 邮递类型 1快递，2平邮，3EMS
	 *
	 * 版本 >= 0
	 */
	 private long MailType;

	/**
	 * 退款:买家退款金额
	 *
	 * 版本 >= 0
	 */
	 private long BuyerRecvRefund;

	/**
	 * 退款:卖家实收金额
	 *
	 * 版本 >= 0
	 */
	 private long SellerRecvRefund;

	/**
	 * 评价状态
	 *
	 * 版本 >= 0
	 */
	 private long DealRateState;

	/**
	 * lastmodify
	 *
	 * 版本 >= 0
	 */
	 private long LastUpdateTime;

	/**
	 * 现金支付金额
	 *
	 * 版本 >= 0
	 */
	 private long DealPayFeeCash;

	/**
	 * 财付券支付金额
	 *
	 * 版本 >= 0
	 */
	 private long DealPayFeeTicket;

	/**
	 * 积分支付金额
	 *
	 * 版本 >= 0
	 */
	 private long DealPayFeeScore;

	/**
	 * 支付完成时间
	 *
	 * 版本 >= 0
	 */
	 private long PayTime;

	/**
	 * 支付返回时间
	 *
	 * 版本 >= 0
	 */
	 private long PayReturnTime;

	/**
	 * 卖家发货时间
	 *
	 * 版本 >= 0
	 */
	 private long SellerConsignmentTime;

	/**
	 * 订单结束时间
	 *
	 * 版本 >= 0
	 */
	 private long DealEndTime;

	/**
	 * 打款完成时间
	 *
	 * 版本 >= 0
	 */
	 private long RecvfeeTime;

	/**
	 * 打款返回时间
	 *
	 * 版本 >= 0
	 */
	 private long RecvfeeReturnTime;

	/**
	 * 备注类型
	 *
	 * 版本 >= 0
	 */
	 private long DealNoteType;

	/**
	 * 退款状态
	 *
	 * 版本 >= 0
	 */
	 private long DealRefundState;

	/**
	 * 大单属性位
	 *
	 * 版本 >= 0
	 */
	 private long Propertymask;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long DealRecvScore;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long DataVersion;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long BuyerDataVersion;

	/**
	 * 卖家昵称
	 *
	 * 版本 >= 0
	 */
	 private String SellerName = new String();

	/**
	 * 买家昵称
	 *
	 * 版本 >= 0
	 */
	 private String BuyerName = new String();

	/**
	 * 订单描述
	 *
	 * 版本 >= 0
	 */
	 private String DealDesc = new String();

	/**
	 * 收货人姓名
	 *
	 * 版本 >= 0
	 */
	 private String ReceiveName = new String();

	/**
	 * 地址
	 *
	 * 版本 >= 0
	 */
	 private String ReceiveAddr = new String();

	/**
	 * 邮编
	 *
	 * 版本 >= 0
	 */
	 private String ReceivePostcode = new String();

	/**
	 * 电话
	 *
	 * 版本 >= 0
	 */
	 private String ReceiveTel = new String();

	/**
	 * 手机
	 *
	 * 版本 >= 0
	 */
	 private String strReceiveMobile = new String();

	/**
	 * 发票抬头
	 *
	 * 版本 >= 0
	 */
	 private String DealInvoiceTitle = new String();

	/**
	 * 发票内容
	 *
	 * 版本 >= 0
	 */
	 private String DealInvoiceContent = new String();

	/**
	 * 运费合计说明
	 *
	 * 版本 >= 0
	 */
	 private String ShippingfeeCalc = new String();

	/**
	 * 商品标题列表
	 *
	 * 版本 >= 0
	 */
	 private String ItemTitleList = new String();

	/**
	 * 促销信息
	 *
	 * 版本 >= 0
	 */
	 private String ComboInfo = new String();

	/**
	 * 收货地址_地址编码
	 *
	 * 版本 >= 0
	 */
	 private String ReceiveAddrCode = new String();

	/**
	 * 订单来源信息
	 *
	 * 版本 >= 0
	 */
	 private String Referer = new String();

	/**
	 * 订单支付ID
	 *
	 * 版本 >= 0
	 */
	 private String DealCftPayid = new String();

	/**
	 * 订单备注
	 *
	 * 版本 >= 0
	 */
	 private String DealNote = new String();

	/**
	 * 卖家昵称
	 *
	 * 版本 >= 0
	 */
	 private String SellerNick = new String();

	/**
	 * 买家昵称
	 *
	 * 版本 >= 0
	 */
	 private String BuyerNick = new String();

	/**
	 * string订单id
	 *
	 * 版本 >= 0
	 */
	 private String DisDealId = new String();

	/**
	 * 购买留言
	 *
	 * 版本 >= 0
	 */
	 private String BuyerBuyRemark = new String();

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private String ImportId = new String();

	/**
	 * 子单列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<CTradoInfo> TradeList = new Vector<CTradoInfo>();

	/**
	 * 支付单列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<CAccountInfo> AccountList = new Vector<CAccountInfo>();

	/**
	 * 物流信息
	 *
	 * 版本 >= 0
	 */
	 private CShippingInfo ShippingInfo = new CShippingInfo();

	/**
	 * 版本 >= 0
	 */
	 private Vector<CActionLog> ActionLogList = new Vector<CActionLog>();

	/**
	 * 货到付款手续费
	 *
	 * 版本 >= 11
	 */
	 private int DealPayFeeCommission = 0;

	/**
	 * COD支付单列表信息
	 *
	 * 版本 >= 12
	 */
	 private Vector<CCodPayInfo> CodPayList = new Vector<CCodPayInfo>();

	/**
	 * 物流Code
	 *
	 * 版本 >= 15
	 */
	 private String WDealCode = new String();

	/**
	 * 收银台
	 *
	 * 版本 >= 15
	 */
	 private String JdPin = new String();

	/**
	 * 大单属性位2
	 *
	 * 版本 >= 15
	 */
	 private long Propertymask2;

	/**
	 * 大单属性位3
	 *
	 * 版本 >= 15
	 */
	 private long Propertymask3;

	/**
	 * 订单来源
	 *
	 * 版本 >= 15
	 */
	 private long Dealsource;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUByte(WhoPayShippingfee);
		bs.pushUByte(DealType);
		bs.pushUByte(DealState);
		bs.pushUByte(PreDealState);
		bs.pushUByte(DealPayType);
		bs.pushLong(DealId);
		bs.pushLong(GenTimestamp);
		bs.pushLong(WuliuId);
		bs.pushLong(ItemlistMd5);
		bs.pushLong(PayId);
		bs.pushInt(CouponFee);
		bs.pushUInt(version);
		bs.pushUInt(SellerUin);
		bs.pushUInt(BuyerUin);
		bs.pushUInt(DealPayFeeShipping);
		bs.pushUInt(PreDealPayFeeTotal);
		bs.pushUInt(DealPayFeeTotal);
		bs.pushUInt(DealCreateTime);
		bs.pushUInt(SellerCrm);
		bs.pushUInt(MailType);
		bs.pushUInt(BuyerRecvRefund);
		bs.pushUInt(SellerRecvRefund);
		bs.pushUInt(DealRateState);
		bs.pushUInt(LastUpdateTime);
		bs.pushUInt(DealPayFeeCash);
		bs.pushUInt(DealPayFeeTicket);
		bs.pushUInt(DealPayFeeScore);
		bs.pushUInt(PayTime);
		bs.pushUInt(PayReturnTime);
		bs.pushUInt(SellerConsignmentTime);
		bs.pushUInt(DealEndTime);
		bs.pushUInt(RecvfeeTime);
		bs.pushUInt(RecvfeeReturnTime);
		bs.pushUInt(DealNoteType);
		bs.pushUInt(DealRefundState);
		bs.pushUInt(Propertymask);
		bs.pushUInt(DealRecvScore);
		bs.pushUInt(DataVersion);
		bs.pushUInt(BuyerDataVersion);
		bs.pushString(SellerName);
		bs.pushString(BuyerName);
		bs.pushString(DealDesc);
		bs.pushString(ReceiveName);
		bs.pushString(ReceiveAddr);
		bs.pushString(ReceivePostcode);
		bs.pushString(ReceiveTel);
		bs.pushString(strReceiveMobile);
		bs.pushString(DealInvoiceTitle);
		bs.pushString(DealInvoiceContent);
		bs.pushString(ShippingfeeCalc);
		bs.pushString(ItemTitleList);
		bs.pushString(ComboInfo);
		bs.pushString(ReceiveAddrCode);
		bs.pushString(Referer);
		bs.pushString(DealCftPayid);
		bs.pushString(DealNote);
		bs.pushString(SellerNick);
		bs.pushString(BuyerNick);
		bs.pushString(DisDealId);
		bs.pushString(BuyerBuyRemark);
		bs.pushString(ImportId);
		bs.pushObject(TradeList);
		bs.pushObject(AccountList);
		bs.pushObject(ShippingInfo);
		bs.pushObject(ActionLogList);
		if(  this.version >= 11 ){
				bs.pushInt(DealPayFeeCommission);
		}
		if(  this.version >= 12 ){
				bs.pushObject(CodPayList);
		}
		if(  this.version >= 15 ){
				bs.pushString(WDealCode);
		}
		if(  this.version >= 15 ){
				bs.pushString(JdPin);
		}
		if(  this.version >= 15 ){
				bs.pushLong(Propertymask2);
		}
		if(  this.version >= 15 ){
				bs.pushLong(Propertymask3);
		}
		if(  this.version >= 15 ){
				bs.pushUInt(Dealsource);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		WhoPayShippingfee = bs.popUByte();
		DealType = bs.popUByte();
		DealState = bs.popUByte();
		PreDealState = bs.popUByte();
		DealPayType = bs.popUByte();
		DealId = bs.popLong();
		GenTimestamp = bs.popLong();
		WuliuId = bs.popLong();
		ItemlistMd5 = bs.popLong();
		PayId = bs.popLong();
		CouponFee = bs.popInt();
		version = bs.popUInt();
		SellerUin = bs.popUInt();
		BuyerUin = bs.popUInt();
		DealPayFeeShipping = bs.popUInt();
		PreDealPayFeeTotal = bs.popUInt();
		DealPayFeeTotal = bs.popUInt();
		DealCreateTime = bs.popUInt();
		SellerCrm = bs.popUInt();
		MailType = bs.popUInt();
		BuyerRecvRefund = bs.popUInt();
		SellerRecvRefund = bs.popUInt();
		DealRateState = bs.popUInt();
		LastUpdateTime = bs.popUInt();
		DealPayFeeCash = bs.popUInt();
		DealPayFeeTicket = bs.popUInt();
		DealPayFeeScore = bs.popUInt();
		PayTime = bs.popUInt();
		PayReturnTime = bs.popUInt();
		SellerConsignmentTime = bs.popUInt();
		DealEndTime = bs.popUInt();
		RecvfeeTime = bs.popUInt();
		RecvfeeReturnTime = bs.popUInt();
		DealNoteType = bs.popUInt();
		DealRefundState = bs.popUInt();
		Propertymask = bs.popUInt();
		DealRecvScore = bs.popUInt();
		DataVersion = bs.popUInt();
		BuyerDataVersion = bs.popUInt();
		SellerName = bs.popString();
		BuyerName = bs.popString();
		DealDesc = bs.popString();
		ReceiveName = bs.popString();
		ReceiveAddr = bs.popString();
		ReceivePostcode = bs.popString();
		ReceiveTel = bs.popString();
		strReceiveMobile = bs.popString();
		DealInvoiceTitle = bs.popString();
		DealInvoiceContent = bs.popString();
		ShippingfeeCalc = bs.popString();
		ItemTitleList = bs.popString();
		ComboInfo = bs.popString();
		ReceiveAddrCode = bs.popString();
		Referer = bs.popString();
		DealCftPayid = bs.popString();
		DealNote = bs.popString();
		SellerNick = bs.popString();
		BuyerNick = bs.popString();
		DisDealId = bs.popString();
		BuyerBuyRemark = bs.popString();
		ImportId = bs.popString();
		TradeList = (Vector<CTradoInfo>)bs.popVector(CTradoInfo.class);
		AccountList = (Vector<CAccountInfo>)bs.popVector(CAccountInfo.class);
		ShippingInfo = (CShippingInfo) bs.popObject(CShippingInfo.class);
		ActionLogList = (Vector<CActionLog>)bs.popVector(CActionLog.class);
		if(  this.version >= 11 ){
				DealPayFeeCommission = bs.popInt();
		}
		if(  this.version >= 12 ){
				CodPayList = (Vector<CCodPayInfo>)bs.popVector(CCodPayInfo.class);
		}
		if(  this.version >= 15 ){
				WDealCode = bs.popString();
		}
		if(  this.version >= 15 ){
				JdPin = bs.popString();
		}
		if(  this.version >= 15 ){
				Propertymask2 = bs.popLong();
		}
		if(  this.version >= 15 ){
				Propertymask3 = bs.popLong();
		}
		if(  this.version >= 15 ){
				Dealsource = bs.popUInt();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取承担运费方式,1卖家承担运费，2买家承担运费，3无需运费，0x0a支持运费模板的边界值
	 * 
	 * 此字段的版本 >= 0
	 * @return WhoPayShippingfee value 类型为:short
	 * 
	 */
	public short getWhoPayShippingfee()
	{
		return WhoPayShippingfee;
	}


	/**
	 * 设置承担运费方式,1卖家承担运费，2买家承担运费，3无需运费，0x0a支持运费模板的边界值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWhoPayShippingfee(short value)
	{
		this.WhoPayShippingfee = value;
	}


	/**
	 * 获取订单类型, 所有类型 1--一口价 SELL_TYPE_BIN 2--单件拍卖，3--b2c订单
	 * 
	 * 此字段的版本 >= 0
	 * @return DealType value 类型为:short
	 * 
	 */
	public short getDealType()
	{
		return DealType;
	}


	/**
	 * 设置订单类型, 所有类型 1--一口价 SELL_TYPE_BIN 2--单件拍卖，3--b2c订单
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealType(short value)
	{
		this.DealType = value;
	}


	/**
	 * 获取订单状态
	 * 
	 * 此字段的版本 >= 0
	 * @return DealState value 类型为:short
	 * 
	 */
	public short getDealState()
	{
		return DealState;
	}


	/**
	 * 设置订单状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealState(short value)
	{
		this.DealState = value;
	}


	/**
	 * 获取preState
	 * 
	 * 此字段的版本 >= 0
	 * @return PreDealState value 类型为:short
	 * 
	 */
	public short getPreDealState()
	{
		return PreDealState;
	}


	/**
	 * 设置preState
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPreDealState(short value)
	{
		this.PreDealState = value;
	}


	/**
	 * 获取支付方式 0未定义 1支付中介 2未使用财付通付款(货到付款) 3分期付款 4移动积分
	 * 
	 * 此字段的版本 >= 0
	 * @return DealPayType value 类型为:short
	 * 
	 */
	public short getDealPayType()
	{
		return DealPayType;
	}


	/**
	 * 设置支付方式 0未定义 1支付中介 2未使用财付通付款(货到付款) 3分期付款 4移动积分
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealPayType(short value)
	{
		this.DealPayType = value;
	}


	/**
	 * 获取id
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:long
	 * 
	 */
	public long getDealId()
	{
		return DealId;
	}


	/**
	 * 设置id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealId(long value)
	{
		this.DealId = value;
	}


	/**
	 * 获取订单序列
	 * 
	 * 此字段的版本 >= 0
	 * @return GenTimestamp value 类型为:long
	 * 
	 */
	public long getGenTimestamp()
	{
		return GenTimestamp;
	}


	/**
	 * 设置订单序列
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setGenTimestamp(long value)
	{
		this.GenTimestamp = value;
	}


	/**
	 * 获取物流id
	 * 
	 * 此字段的版本 >= 0
	 * @return WuliuId value 类型为:long
	 * 
	 */
	public long getWuliuId()
	{
		return WuliuId;
	}


	/**
	 * 设置物流id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWuliuId(long value)
	{
		this.WuliuId = value;
	}


	/**
	 * 获取商品ID签名
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemlistMd5 value 类型为:long
	 * 
	 */
	public long getItemlistMd5()
	{
		return ItemlistMd5;
	}


	/**
	 * 设置商品ID签名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setItemlistMd5(long value)
	{
		this.ItemlistMd5 = value;
	}


	/**
	 * 获取订单支付ID
	 * 
	 * 此字段的版本 >= 0
	 * @return PayId value 类型为:long
	 * 
	 */
	public long getPayId()
	{
		return PayId;
	}


	/**
	 * 设置订单支付ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayId(long value)
	{
		this.PayId = value;
	}


	/**
	 * 获取折扣优惠金额
	 * 
	 * 此字段的版本 >= 0
	 * @return CouponFee value 类型为:int
	 * 
	 */
	public int getCouponFee()
	{
		return CouponFee;
	}


	/**
	 * 设置折扣优惠金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setCouponFee(int value)
	{
		this.CouponFee = value;
	}


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
	}


	/**
	 * 获取买家号
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return BuyerUin;
	}


	/**
	 * 设置买家号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.BuyerUin = value;
	}


	/**
	 * 获取邮费
	 * 
	 * 此字段的版本 >= 0
	 * @return DealPayFeeShipping value 类型为:long
	 * 
	 */
	public long getDealPayFeeShipping()
	{
		return DealPayFeeShipping;
	}


	/**
	 * 设置邮费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealPayFeeShipping(long value)
	{
		this.DealPayFeeShipping = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PreDealPayFeeTotal value 类型为:long
	 * 
	 */
	public long getPreDealPayFeeTotal()
	{
		return PreDealPayFeeTotal;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPreDealPayFeeTotal(long value)
	{
		this.PreDealPayFeeTotal = value;
	}


	/**
	 * 获取费用合计
	 * 
	 * 此字段的版本 >= 0
	 * @return DealPayFeeTotal value 类型为:long
	 * 
	 */
	public long getDealPayFeeTotal()
	{
		return DealPayFeeTotal;
	}


	/**
	 * 设置费用合计
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealPayFeeTotal(long value)
	{
		this.DealPayFeeTotal = value;
	}


	/**
	 * 获取订单生成时间
	 * 
	 * 此字段的版本 >= 0
	 * @return DealCreateTime value 类型为:long
	 * 
	 */
	public long getDealCreateTime()
	{
		return DealCreateTime;
	}


	/**
	 * 设置订单生成时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealCreateTime(long value)
	{
		this.DealCreateTime = value;
	}


	/**
	 * 获取客服CRM
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerCrm value 类型为:long
	 * 
	 */
	public long getSellerCrm()
	{
		return SellerCrm;
	}


	/**
	 * 设置客服CRM
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerCrm(long value)
	{
		this.SellerCrm = value;
	}


	/**
	 * 获取邮递类型 1快递，2平邮，3EMS
	 * 
	 * 此字段的版本 >= 0
	 * @return MailType value 类型为:long
	 * 
	 */
	public long getMailType()
	{
		return MailType;
	}


	/**
	 * 设置邮递类型 1快递，2平邮，3EMS
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMailType(long value)
	{
		this.MailType = value;
	}


	/**
	 * 获取退款:买家退款金额
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerRecvRefund value 类型为:long
	 * 
	 */
	public long getBuyerRecvRefund()
	{
		return BuyerRecvRefund;
	}


	/**
	 * 设置退款:买家退款金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerRecvRefund(long value)
	{
		this.BuyerRecvRefund = value;
	}


	/**
	 * 获取退款:卖家实收金额
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerRecvRefund value 类型为:long
	 * 
	 */
	public long getSellerRecvRefund()
	{
		return SellerRecvRefund;
	}


	/**
	 * 设置退款:卖家实收金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerRecvRefund(long value)
	{
		this.SellerRecvRefund = value;
	}


	/**
	 * 获取评价状态
	 * 
	 * 此字段的版本 >= 0
	 * @return DealRateState value 类型为:long
	 * 
	 */
	public long getDealRateState()
	{
		return DealRateState;
	}


	/**
	 * 设置评价状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealRateState(long value)
	{
		this.DealRateState = value;
	}


	/**
	 * 获取lastmodify
	 * 
	 * 此字段的版本 >= 0
	 * @return LastUpdateTime value 类型为:long
	 * 
	 */
	public long getLastUpdateTime()
	{
		return LastUpdateTime;
	}


	/**
	 * 设置lastmodify
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastUpdateTime(long value)
	{
		this.LastUpdateTime = value;
	}


	/**
	 * 获取现金支付金额
	 * 
	 * 此字段的版本 >= 0
	 * @return DealPayFeeCash value 类型为:long
	 * 
	 */
	public long getDealPayFeeCash()
	{
		return DealPayFeeCash;
	}


	/**
	 * 设置现金支付金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealPayFeeCash(long value)
	{
		this.DealPayFeeCash = value;
	}


	/**
	 * 获取财付券支付金额
	 * 
	 * 此字段的版本 >= 0
	 * @return DealPayFeeTicket value 类型为:long
	 * 
	 */
	public long getDealPayFeeTicket()
	{
		return DealPayFeeTicket;
	}


	/**
	 * 设置财付券支付金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealPayFeeTicket(long value)
	{
		this.DealPayFeeTicket = value;
	}


	/**
	 * 获取积分支付金额
	 * 
	 * 此字段的版本 >= 0
	 * @return DealPayFeeScore value 类型为:long
	 * 
	 */
	public long getDealPayFeeScore()
	{
		return DealPayFeeScore;
	}


	/**
	 * 设置积分支付金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealPayFeeScore(long value)
	{
		this.DealPayFeeScore = value;
	}


	/**
	 * 获取支付完成时间
	 * 
	 * 此字段的版本 >= 0
	 * @return PayTime value 类型为:long
	 * 
	 */
	public long getPayTime()
	{
		return PayTime;
	}


	/**
	 * 设置支付完成时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayTime(long value)
	{
		this.PayTime = value;
	}


	/**
	 * 获取支付返回时间
	 * 
	 * 此字段的版本 >= 0
	 * @return PayReturnTime value 类型为:long
	 * 
	 */
	public long getPayReturnTime()
	{
		return PayReturnTime;
	}


	/**
	 * 设置支付返回时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayReturnTime(long value)
	{
		this.PayReturnTime = value;
	}


	/**
	 * 获取卖家发货时间
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerConsignmentTime value 类型为:long
	 * 
	 */
	public long getSellerConsignmentTime()
	{
		return SellerConsignmentTime;
	}


	/**
	 * 设置卖家发货时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerConsignmentTime(long value)
	{
		this.SellerConsignmentTime = value;
	}


	/**
	 * 获取订单结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return DealEndTime value 类型为:long
	 * 
	 */
	public long getDealEndTime()
	{
		return DealEndTime;
	}


	/**
	 * 设置订单结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealEndTime(long value)
	{
		this.DealEndTime = value;
	}


	/**
	 * 获取打款完成时间
	 * 
	 * 此字段的版本 >= 0
	 * @return RecvfeeTime value 类型为:long
	 * 
	 */
	public long getRecvfeeTime()
	{
		return RecvfeeTime;
	}


	/**
	 * 设置打款完成时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRecvfeeTime(long value)
	{
		this.RecvfeeTime = value;
	}


	/**
	 * 获取打款返回时间
	 * 
	 * 此字段的版本 >= 0
	 * @return RecvfeeReturnTime value 类型为:long
	 * 
	 */
	public long getRecvfeeReturnTime()
	{
		return RecvfeeReturnTime;
	}


	/**
	 * 设置打款返回时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRecvfeeReturnTime(long value)
	{
		this.RecvfeeReturnTime = value;
	}


	/**
	 * 获取备注类型
	 * 
	 * 此字段的版本 >= 0
	 * @return DealNoteType value 类型为:long
	 * 
	 */
	public long getDealNoteType()
	{
		return DealNoteType;
	}


	/**
	 * 设置备注类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealNoteType(long value)
	{
		this.DealNoteType = value;
	}


	/**
	 * 获取退款状态
	 * 
	 * 此字段的版本 >= 0
	 * @return DealRefundState value 类型为:long
	 * 
	 */
	public long getDealRefundState()
	{
		return DealRefundState;
	}


	/**
	 * 设置退款状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealRefundState(long value)
	{
		this.DealRefundState = value;
	}


	/**
	 * 获取大单属性位
	 * 
	 * 此字段的版本 >= 0
	 * @return Propertymask value 类型为:long
	 * 
	 */
	public long getPropertymask()
	{
		return Propertymask;
	}


	/**
	 * 设置大单属性位
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPropertymask(long value)
	{
		this.Propertymask = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DealRecvScore value 类型为:long
	 * 
	 */
	public long getDealRecvScore()
	{
		return DealRecvScore;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealRecvScore(long value)
	{
		this.DealRecvScore = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DataVersion value 类型为:long
	 * 
	 */
	public long getDataVersion()
	{
		return DataVersion;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDataVersion(long value)
	{
		this.DataVersion = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerDataVersion value 类型为:long
	 * 
	 */
	public long getBuyerDataVersion()
	{
		return BuyerDataVersion;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerDataVersion(long value)
	{
		this.BuyerDataVersion = value;
	}


	/**
	 * 获取卖家昵称
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerName value 类型为:String
	 * 
	 */
	public String getSellerName()
	{
		return SellerName;
	}


	/**
	 * 设置卖家昵称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSellerName(String value)
	{
		this.SellerName = value;
	}


	/**
	 * 获取买家昵称
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerName value 类型为:String
	 * 
	 */
	public String getBuyerName()
	{
		return BuyerName;
	}


	/**
	 * 设置买家昵称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBuyerName(String value)
	{
		this.BuyerName = value;
	}


	/**
	 * 获取订单描述
	 * 
	 * 此字段的版本 >= 0
	 * @return DealDesc value 类型为:String
	 * 
	 */
	public String getDealDesc()
	{
		return DealDesc;
	}


	/**
	 * 设置订单描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealDesc(String value)
	{
		this.DealDesc = value;
	}


	/**
	 * 获取收货人姓名
	 * 
	 * 此字段的版本 >= 0
	 * @return ReceiveName value 类型为:String
	 * 
	 */
	public String getReceiveName()
	{
		return ReceiveName;
	}


	/**
	 * 设置收货人姓名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReceiveName(String value)
	{
		this.ReceiveName = value;
	}


	/**
	 * 获取地址
	 * 
	 * 此字段的版本 >= 0
	 * @return ReceiveAddr value 类型为:String
	 * 
	 */
	public String getReceiveAddr()
	{
		return ReceiveAddr;
	}


	/**
	 * 设置地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReceiveAddr(String value)
	{
		this.ReceiveAddr = value;
	}


	/**
	 * 获取邮编
	 * 
	 * 此字段的版本 >= 0
	 * @return ReceivePostcode value 类型为:String
	 * 
	 */
	public String getReceivePostcode()
	{
		return ReceivePostcode;
	}


	/**
	 * 设置邮编
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReceivePostcode(String value)
	{
		this.ReceivePostcode = value;
	}


	/**
	 * 获取电话
	 * 
	 * 此字段的版本 >= 0
	 * @return ReceiveTel value 类型为:String
	 * 
	 */
	public String getReceiveTel()
	{
		return ReceiveTel;
	}


	/**
	 * 设置电话
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReceiveTel(String value)
	{
		this.ReceiveTel = value;
	}


	/**
	 * 获取手机
	 * 
	 * 此字段的版本 >= 0
	 * @return strReceiveMobile value 类型为:String
	 * 
	 */
	public String getStrReceiveMobile()
	{
		return strReceiveMobile;
	}


	/**
	 * 设置手机
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrReceiveMobile(String value)
	{
		this.strReceiveMobile = value;
	}


	/**
	 * 获取发票抬头
	 * 
	 * 此字段的版本 >= 0
	 * @return DealInvoiceTitle value 类型为:String
	 * 
	 */
	public String getDealInvoiceTitle()
	{
		return DealInvoiceTitle;
	}


	/**
	 * 设置发票抬头
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealInvoiceTitle(String value)
	{
		this.DealInvoiceTitle = value;
	}


	/**
	 * 获取发票内容
	 * 
	 * 此字段的版本 >= 0
	 * @return DealInvoiceContent value 类型为:String
	 * 
	 */
	public String getDealInvoiceContent()
	{
		return DealInvoiceContent;
	}


	/**
	 * 设置发票内容
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealInvoiceContent(String value)
	{
		this.DealInvoiceContent = value;
	}


	/**
	 * 获取运费合计说明
	 * 
	 * 此字段的版本 >= 0
	 * @return ShippingfeeCalc value 类型为:String
	 * 
	 */
	public String getShippingfeeCalc()
	{
		return ShippingfeeCalc;
	}


	/**
	 * 设置运费合计说明
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setShippingfeeCalc(String value)
	{
		this.ShippingfeeCalc = value;
	}


	/**
	 * 获取商品标题列表
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemTitleList value 类型为:String
	 * 
	 */
	public String getItemTitleList()
	{
		return ItemTitleList;
	}


	/**
	 * 设置商品标题列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemTitleList(String value)
	{
		this.ItemTitleList = value;
	}


	/**
	 * 获取促销信息
	 * 
	 * 此字段的版本 >= 0
	 * @return ComboInfo value 类型为:String
	 * 
	 */
	public String getComboInfo()
	{
		return ComboInfo;
	}


	/**
	 * 设置促销信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setComboInfo(String value)
	{
		this.ComboInfo = value;
	}


	/**
	 * 获取收货地址_地址编码
	 * 
	 * 此字段的版本 >= 0
	 * @return ReceiveAddrCode value 类型为:String
	 * 
	 */
	public String getReceiveAddrCode()
	{
		return ReceiveAddrCode;
	}


	/**
	 * 设置收货地址_地址编码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReceiveAddrCode(String value)
	{
		this.ReceiveAddrCode = value;
	}


	/**
	 * 获取订单来源信息
	 * 
	 * 此字段的版本 >= 0
	 * @return Referer value 类型为:String
	 * 
	 */
	public String getReferer()
	{
		return Referer;
	}


	/**
	 * 设置订单来源信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReferer(String value)
	{
		this.Referer = value;
	}


	/**
	 * 获取订单支付ID
	 * 
	 * 此字段的版本 >= 0
	 * @return DealCftPayid value 类型为:String
	 * 
	 */
	public String getDealCftPayid()
	{
		return DealCftPayid;
	}


	/**
	 * 设置订单支付ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealCftPayid(String value)
	{
		this.DealCftPayid = value;
	}


	/**
	 * 获取订单备注
	 * 
	 * 此字段的版本 >= 0
	 * @return DealNote value 类型为:String
	 * 
	 */
	public String getDealNote()
	{
		return DealNote;
	}


	/**
	 * 设置订单备注
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealNote(String value)
	{
		this.DealNote = value;
	}


	/**
	 * 获取卖家昵称
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerNick value 类型为:String
	 * 
	 */
	public String getSellerNick()
	{
		return SellerNick;
	}


	/**
	 * 设置卖家昵称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSellerNick(String value)
	{
		this.SellerNick = value;
	}


	/**
	 * 获取买家昵称
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerNick value 类型为:String
	 * 
	 */
	public String getBuyerNick()
	{
		return BuyerNick;
	}


	/**
	 * 设置买家昵称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBuyerNick(String value)
	{
		this.BuyerNick = value;
	}


	/**
	 * 获取string订单id
	 * 
	 * 此字段的版本 >= 0
	 * @return DisDealId value 类型为:String
	 * 
	 */
	public String getDisDealId()
	{
		return DisDealId;
	}


	/**
	 * 设置string订单id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDisDealId(String value)
	{
		this.DisDealId = value;
	}


	/**
	 * 获取购买留言
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerBuyRemark value 类型为:String
	 * 
	 */
	public String getBuyerBuyRemark()
	{
		return BuyerBuyRemark;
	}


	/**
	 * 设置购买留言
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBuyerBuyRemark(String value)
	{
		this.BuyerBuyRemark = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ImportId value 类型为:String
	 * 
	 */
	public String getImportId()
	{
		return ImportId;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setImportId(String value)
	{
		this.ImportId = value;
	}


	/**
	 * 获取子单列表
	 * 
	 * 此字段的版本 >= 0
	 * @return TradeList value 类型为:Vector<CTradoInfo>
	 * 
	 */
	public Vector<CTradoInfo> getTradeList()
	{
		return TradeList;
	}


	/**
	 * 设置子单列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<CTradoInfo>
	 * 
	 */
	public void setTradeList(Vector<CTradoInfo> value)
	{
		if (value != null) {
				this.TradeList = value;
		}else{
				this.TradeList = new Vector<CTradoInfo>();
		}
	}


	/**
	 * 获取支付单列表
	 * 
	 * 此字段的版本 >= 0
	 * @return AccountList value 类型为:Vector<CAccountInfo>
	 * 
	 */
	public Vector<CAccountInfo> getAccountList()
	{
		return AccountList;
	}


	/**
	 * 设置支付单列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<CAccountInfo>
	 * 
	 */
	public void setAccountList(Vector<CAccountInfo> value)
	{
		if (value != null) {
				this.AccountList = value;
		}else{
				this.AccountList = new Vector<CAccountInfo>();
		}
	}


	/**
	 * 获取物流信息
	 * 
	 * 此字段的版本 >= 0
	 * @return ShippingInfo value 类型为:CShippingInfo
	 * 
	 */
	public CShippingInfo getShippingInfo()
	{
		return ShippingInfo;
	}


	/**
	 * 设置物流信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:CShippingInfo
	 * 
	 */
	public void setShippingInfo(CShippingInfo value)
	{
		if (value != null) {
				this.ShippingInfo = value;
		}else{
				this.ShippingInfo = new CShippingInfo();
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ActionLogList value 类型为:Vector<CActionLog>
	 * 
	 */
	public Vector<CActionLog> getActionLogList()
	{
		return ActionLogList;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<CActionLog>
	 * 
	 */
	public void setActionLogList(Vector<CActionLog> value)
	{
		if (value != null) {
				this.ActionLogList = value;
		}else{
				this.ActionLogList = new Vector<CActionLog>();
		}
	}


	/**
	 * 获取货到付款手续费
	 * 
	 * 此字段的版本 >= 11
	 * @return DealPayFeeCommission value 类型为:int
	 * 
	 */
	public int getDealPayFeeCommission()
	{
		return DealPayFeeCommission;
	}


	/**
	 * 设置货到付款手续费
	 * 
	 * 此字段的版本 >= 11
	 * @param  value 类型为:int
	 * 
	 */
	public void setDealPayFeeCommission(int value)
	{
		this.DealPayFeeCommission = value;
	}


	/**
	 * 获取COD支付单列表信息
	 * 
	 * 此字段的版本 >= 12
	 * @return CodPayList value 类型为:Vector<CCodPayInfo>
	 * 
	 */
	public Vector<CCodPayInfo> getCodPayList()
	{
		return CodPayList;
	}


	/**
	 * 设置COD支付单列表信息
	 * 
	 * 此字段的版本 >= 12
	 * @param  value 类型为:Vector<CCodPayInfo>
	 * 
	 */
	public void setCodPayList(Vector<CCodPayInfo> value)
	{
		if (value != null) {
				this.CodPayList = value;
		}else{
				this.CodPayList = new Vector<CCodPayInfo>();
		}
	}


	/**
	 * 获取物流Code
	 * 
	 * 此字段的版本 >= 15
	 * @return WDealCode value 类型为:String
	 * 
	 */
	public String getWDealCode()
	{
		return WDealCode;
	}


	/**
	 * 设置物流Code
	 * 
	 * 此字段的版本 >= 15
	 * @param  value 类型为:String
	 * 
	 */
	public void setWDealCode(String value)
	{
		this.WDealCode = value;
	}


	/**
	 * 获取收银台
	 * 
	 * 此字段的版本 >= 15
	 * @return JdPin value 类型为:String
	 * 
	 */
	public String getJdPin()
	{
		return JdPin;
	}


	/**
	 * 设置收银台
	 * 
	 * 此字段的版本 >= 15
	 * @param  value 类型为:String
	 * 
	 */
	public void setJdPin(String value)
	{
		this.JdPin = value;
	}


	/**
	 * 获取大单属性位2
	 * 
	 * 此字段的版本 >= 15
	 * @return Propertymask2 value 类型为:long
	 * 
	 */
	public long getPropertymask2()
	{
		return Propertymask2;
	}


	/**
	 * 设置大单属性位2
	 * 
	 * 此字段的版本 >= 15
	 * @param  value 类型为:long
	 * 
	 */
	public void setPropertymask2(long value)
	{
		this.Propertymask2 = value;
	}


	/**
	 * 获取大单属性位3
	 * 
	 * 此字段的版本 >= 15
	 * @return Propertymask3 value 类型为:long
	 * 
	 */
	public long getPropertymask3()
	{
		return Propertymask3;
	}


	/**
	 * 设置大单属性位3
	 * 
	 * 此字段的版本 >= 15
	 * @param  value 类型为:long
	 * 
	 */
	public void setPropertymask3(long value)
	{
		this.Propertymask3 = value;
	}


	/**
	 * 获取订单来源
	 * 
	 * 此字段的版本 >= 15
	 * @return Dealsource value 类型为:long
	 * 
	 */
	public long getDealsource()
	{
		return Dealsource;
	}


	/**
	 * 设置订单来源
	 * 
	 * 此字段的版本 >= 15
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealsource(long value)
	{
		this.Dealsource = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CDealInfo)
				length += 1;  //计算字段WhoPayShippingfee的长度 size_of(uint8_t)
				length += 1;  //计算字段DealType的长度 size_of(uint8_t)
				length += 1;  //计算字段DealState的长度 size_of(uint8_t)
				length += 1;  //计算字段PreDealState的长度 size_of(uint8_t)
				length += 1;  //计算字段DealPayType的长度 size_of(uint8_t)
				length += 17;  //计算字段DealId的长度 size_of(uint64_t)
				length += 17;  //计算字段GenTimestamp的长度 size_of(uint64_t)
				length += 17;  //计算字段WuliuId的长度 size_of(uint64_t)
				length += 17;  //计算字段ItemlistMd5的长度 size_of(uint64_t)
				length += 17;  //计算字段PayId的长度 size_of(uint64_t)
				length += 4;  //计算字段CouponFee的长度 size_of(int)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段DealPayFeeShipping的长度 size_of(uint32_t)
				length += 4;  //计算字段PreDealPayFeeTotal的长度 size_of(uint32_t)
				length += 4;  //计算字段DealPayFeeTotal的长度 size_of(uint32_t)
				length += 4;  //计算字段DealCreateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerCrm的长度 size_of(uint32_t)
				length += 4;  //计算字段MailType的长度 size_of(uint32_t)
				length += 4;  //计算字段BuyerRecvRefund的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerRecvRefund的长度 size_of(uint32_t)
				length += 4;  //计算字段DealRateState的长度 size_of(uint32_t)
				length += 4;  //计算字段LastUpdateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段DealPayFeeCash的长度 size_of(uint32_t)
				length += 4;  //计算字段DealPayFeeTicket的长度 size_of(uint32_t)
				length += 4;  //计算字段DealPayFeeScore的长度 size_of(uint32_t)
				length += 4;  //计算字段PayTime的长度 size_of(uint32_t)
				length += 4;  //计算字段PayReturnTime的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerConsignmentTime的长度 size_of(uint32_t)
				length += 4;  //计算字段DealEndTime的长度 size_of(uint32_t)
				length += 4;  //计算字段RecvfeeTime的长度 size_of(uint32_t)
				length += 4;  //计算字段RecvfeeReturnTime的长度 size_of(uint32_t)
				length += 4;  //计算字段DealNoteType的长度 size_of(uint32_t)
				length += 4;  //计算字段DealRefundState的长度 size_of(uint32_t)
				length += 4;  //计算字段Propertymask的长度 size_of(uint32_t)
				length += 4;  //计算字段DealRecvScore的长度 size_of(uint32_t)
				length += 4;  //计算字段DataVersion的长度 size_of(uint32_t)
				length += 4;  //计算字段BuyerDataVersion的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(SellerName, null);  //计算字段SellerName的长度 size_of(String)
				length += ByteStream.getObjectSize(BuyerName, null);  //计算字段BuyerName的长度 size_of(String)
				length += ByteStream.getObjectSize(DealDesc, null);  //计算字段DealDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceiveName, null);  //计算字段ReceiveName的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceiveAddr, null);  //计算字段ReceiveAddr的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceivePostcode, null);  //计算字段ReceivePostcode的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceiveTel, null);  //计算字段ReceiveTel的长度 size_of(String)
				length += ByteStream.getObjectSize(strReceiveMobile, null);  //计算字段strReceiveMobile的长度 size_of(String)
				length += ByteStream.getObjectSize(DealInvoiceTitle, null);  //计算字段DealInvoiceTitle的长度 size_of(String)
				length += ByteStream.getObjectSize(DealInvoiceContent, null);  //计算字段DealInvoiceContent的长度 size_of(String)
				length += ByteStream.getObjectSize(ShippingfeeCalc, null);  //计算字段ShippingfeeCalc的长度 size_of(String)
				length += ByteStream.getObjectSize(ItemTitleList, null);  //计算字段ItemTitleList的长度 size_of(String)
				length += ByteStream.getObjectSize(ComboInfo, null);  //计算字段ComboInfo的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceiveAddrCode, null);  //计算字段ReceiveAddrCode的长度 size_of(String)
				length += ByteStream.getObjectSize(Referer, null);  //计算字段Referer的长度 size_of(String)
				length += ByteStream.getObjectSize(DealCftPayid, null);  //计算字段DealCftPayid的长度 size_of(String)
				length += ByteStream.getObjectSize(DealNote, null);  //计算字段DealNote的长度 size_of(String)
				length += ByteStream.getObjectSize(SellerNick, null);  //计算字段SellerNick的长度 size_of(String)
				length += ByteStream.getObjectSize(BuyerNick, null);  //计算字段BuyerNick的长度 size_of(String)
				length += ByteStream.getObjectSize(DisDealId, null);  //计算字段DisDealId的长度 size_of(String)
				length += ByteStream.getObjectSize(BuyerBuyRemark, null);  //计算字段BuyerBuyRemark的长度 size_of(String)
				length += ByteStream.getObjectSize(ImportId, null);  //计算字段ImportId的长度 size_of(String)
				length += ByteStream.getObjectSize(TradeList, null);  //计算字段TradeList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(AccountList, null);  //计算字段AccountList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(ShippingInfo, null);  //计算字段ShippingInfo的长度 size_of(CShippingInfo)
				length += ByteStream.getObjectSize(ActionLogList, null);  //计算字段ActionLogList的长度 size_of(Vector)
				if(  this.version >= 11 ){
						length += 4;  //计算字段DealPayFeeCommission的长度 size_of(int)
				}
				if(  this.version >= 12 ){
						length += ByteStream.getObjectSize(CodPayList, null);  //计算字段CodPayList的长度 size_of(Vector)
				}
				if(  this.version >= 15 ){
						length += ByteStream.getObjectSize(WDealCode, null);  //计算字段WDealCode的长度 size_of(String)
				}
				if(  this.version >= 15 ){
						length += ByteStream.getObjectSize(JdPin, null);  //计算字段JdPin的长度 size_of(String)
				}
				if(  this.version >= 15 ){
						length += 17;  //计算字段Propertymask2的长度 size_of(uint64_t)
				}
				if(  this.version >= 15 ){
						length += 17;  //计算字段Propertymask3的长度 size_of(uint64_t)
				}
				if(  this.version >= 15 ){
						length += 4;  //计算字段Dealsource的长度 size_of(uint32_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(CDealInfo)
				length += 1;  //计算字段WhoPayShippingfee的长度 size_of(uint8_t)
				length += 1;  //计算字段DealType的长度 size_of(uint8_t)
				length += 1;  //计算字段DealState的长度 size_of(uint8_t)
				length += 1;  //计算字段PreDealState的长度 size_of(uint8_t)
				length += 1;  //计算字段DealPayType的长度 size_of(uint8_t)
				length += 17;  //计算字段DealId的长度 size_of(uint64_t)
				length += 17;  //计算字段GenTimestamp的长度 size_of(uint64_t)
				length += 17;  //计算字段WuliuId的长度 size_of(uint64_t)
				length += 17;  //计算字段ItemlistMd5的长度 size_of(uint64_t)
				length += 17;  //计算字段PayId的长度 size_of(uint64_t)
				length += 4;  //计算字段CouponFee的长度 size_of(int)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段DealPayFeeShipping的长度 size_of(uint32_t)
				length += 4;  //计算字段PreDealPayFeeTotal的长度 size_of(uint32_t)
				length += 4;  //计算字段DealPayFeeTotal的长度 size_of(uint32_t)
				length += 4;  //计算字段DealCreateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerCrm的长度 size_of(uint32_t)
				length += 4;  //计算字段MailType的长度 size_of(uint32_t)
				length += 4;  //计算字段BuyerRecvRefund的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerRecvRefund的长度 size_of(uint32_t)
				length += 4;  //计算字段DealRateState的长度 size_of(uint32_t)
				length += 4;  //计算字段LastUpdateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段DealPayFeeCash的长度 size_of(uint32_t)
				length += 4;  //计算字段DealPayFeeTicket的长度 size_of(uint32_t)
				length += 4;  //计算字段DealPayFeeScore的长度 size_of(uint32_t)
				length += 4;  //计算字段PayTime的长度 size_of(uint32_t)
				length += 4;  //计算字段PayReturnTime的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerConsignmentTime的长度 size_of(uint32_t)
				length += 4;  //计算字段DealEndTime的长度 size_of(uint32_t)
				length += 4;  //计算字段RecvfeeTime的长度 size_of(uint32_t)
				length += 4;  //计算字段RecvfeeReturnTime的长度 size_of(uint32_t)
				length += 4;  //计算字段DealNoteType的长度 size_of(uint32_t)
				length += 4;  //计算字段DealRefundState的长度 size_of(uint32_t)
				length += 4;  //计算字段Propertymask的长度 size_of(uint32_t)
				length += 4;  //计算字段DealRecvScore的长度 size_of(uint32_t)
				length += 4;  //计算字段DataVersion的长度 size_of(uint32_t)
				length += 4;  //计算字段BuyerDataVersion的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(SellerName, encoding);  //计算字段SellerName的长度 size_of(String)
				length += ByteStream.getObjectSize(BuyerName, encoding);  //计算字段BuyerName的长度 size_of(String)
				length += ByteStream.getObjectSize(DealDesc, encoding);  //计算字段DealDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceiveName, encoding);  //计算字段ReceiveName的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceiveAddr, encoding);  //计算字段ReceiveAddr的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceivePostcode, encoding);  //计算字段ReceivePostcode的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceiveTel, encoding);  //计算字段ReceiveTel的长度 size_of(String)
				length += ByteStream.getObjectSize(strReceiveMobile, encoding);  //计算字段strReceiveMobile的长度 size_of(String)
				length += ByteStream.getObjectSize(DealInvoiceTitle, encoding);  //计算字段DealInvoiceTitle的长度 size_of(String)
				length += ByteStream.getObjectSize(DealInvoiceContent, encoding);  //计算字段DealInvoiceContent的长度 size_of(String)
				length += ByteStream.getObjectSize(ShippingfeeCalc, encoding);  //计算字段ShippingfeeCalc的长度 size_of(String)
				length += ByteStream.getObjectSize(ItemTitleList, encoding);  //计算字段ItemTitleList的长度 size_of(String)
				length += ByteStream.getObjectSize(ComboInfo, encoding);  //计算字段ComboInfo的长度 size_of(String)
				length += ByteStream.getObjectSize(ReceiveAddrCode, encoding);  //计算字段ReceiveAddrCode的长度 size_of(String)
				length += ByteStream.getObjectSize(Referer, encoding);  //计算字段Referer的长度 size_of(String)
				length += ByteStream.getObjectSize(DealCftPayid, encoding);  //计算字段DealCftPayid的长度 size_of(String)
				length += ByteStream.getObjectSize(DealNote, encoding);  //计算字段DealNote的长度 size_of(String)
				length += ByteStream.getObjectSize(SellerNick, encoding);  //计算字段SellerNick的长度 size_of(String)
				length += ByteStream.getObjectSize(BuyerNick, encoding);  //计算字段BuyerNick的长度 size_of(String)
				length += ByteStream.getObjectSize(DisDealId, encoding);  //计算字段DisDealId的长度 size_of(String)
				length += ByteStream.getObjectSize(BuyerBuyRemark, encoding);  //计算字段BuyerBuyRemark的长度 size_of(String)
				length += ByteStream.getObjectSize(ImportId, encoding);  //计算字段ImportId的长度 size_of(String)
				length += ByteStream.getObjectSize(TradeList, encoding);  //计算字段TradeList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(AccountList, encoding);  //计算字段AccountList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(ShippingInfo, encoding);  //计算字段ShippingInfo的长度 size_of(CShippingInfo)
				length += ByteStream.getObjectSize(ActionLogList, encoding);  //计算字段ActionLogList的长度 size_of(Vector)
				if(  this.version >= 11 ){
						length += 4;  //计算字段DealPayFeeCommission的长度 size_of(int)
				}
				if(  this.version >= 12 ){
						length += ByteStream.getObjectSize(CodPayList, encoding);  //计算字段CodPayList的长度 size_of(Vector)
				}
				if(  this.version >= 15 ){
						length += ByteStream.getObjectSize(WDealCode, encoding);  //计算字段WDealCode的长度 size_of(String)
				}
				if(  this.version >= 15 ){
						length += ByteStream.getObjectSize(JdPin, encoding);  //计算字段JdPin的长度 size_of(String)
				}
				if(  this.version >= 15 ){
						length += 17;  //计算字段Propertymask2的长度 size_of(uint64_t)
				}
				if(  this.version >= 15 ){
						length += 17;  //计算字段Propertymask3的长度 size_of(uint64_t)
				}
				if(  this.version >= 15 ){
						length += 4;  //计算字段Dealsource的长度 size_of(uint32_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本11所包含的字段*******
 *	short WhoPayShippingfee;///<承担运费方式,1卖家承担运费，2买家承担运费，3无需运费，0x0a支持运费模板的边界值
 *	short DealType;///<订单类型, 所有类型 1--一口价 SELL_TYPE_BIN 2--单件拍卖，3--b2c订单
 *	short DealState;///<订单状态
 *	short PreDealState;///<preState
 *	short DealPayType;///<支付方式 0未定义 1支付中介 2未使用财付通付款(货到付款) 3分期付款 4移动积分
 *	long DealId;///<id
 *	long GenTimestamp;///<订单序列
 *	long WuliuId;///<物流id
 *	long ItemlistMd5;///<商品ID签名
 *	long PayId;///<订单支付ID
 *	int CouponFee;///<折扣优惠金额
 *	long version;///<版本号
 *	long SellerUin;///<卖家号
 *	long BuyerUin;///<买家号
 *	long DealPayFeeShipping;///<邮费
 *	long PreDealPayFeeTotal;
 *	long DealPayFeeTotal;///<费用合计
 *	long DealCreateTime;///<订单生成时间
 *	long SellerCrm;///<客服CRM
 *	long MailType;///<邮递类型 1快递，2平邮，3EMS
 *	long BuyerRecvRefund;///<退款:买家退款金额
 *	long SellerRecvRefund;///<退款:卖家实收金额
 *	long DealRateState;///<评价状态
 *	long LastUpdateTime;///<lastmodify
 *	long DealPayFeeCash;///<现金支付金额
 *	long DealPayFeeTicket;///<财付券支付金额
 *	long DealPayFeeScore;///<积分支付金额
 *	long PayTime;///<支付完成时间
 *	long PayReturnTime;///<支付返回时间
 *	long SellerConsignmentTime;///<卖家发货时间
 *	long DealEndTime;///<订单结束时间
 *	long RecvfeeTime;///<打款完成时间
 *	long RecvfeeReturnTime;///<打款返回时间
 *	long DealNoteType;///<备注类型
 *	long DealRefundState;///<退款状态
 *	long Propertymask;///<大单属性位
 *	long DealRecvScore;
 *	long DataVersion;
 *	long BuyerDataVersion;
 *	String SellerName;///<卖家昵称
 *	String BuyerName;///<买家昵称
 *	String DealDesc;///<订单描述
 *	String ReceiveName;///<收货人姓名
 *	String ReceiveAddr;///<地址
 *	String ReceivePostcode;///<邮编
 *	String ReceiveTel;///<电话
 *	String strReceiveMobile;///<手机
 *	String DealInvoiceTitle;///<发票抬头
 *	String DealInvoiceContent;///<发票内容
 *	String ShippingfeeCalc;///<运费合计说明
 *	String ItemTitleList;///<商品标题列表
 *	String ComboInfo;///<促销信息
 *	String ReceiveAddrCode;///<收货地址_地址编码
 *	String Referer;///<订单来源信息
 *	String DealCftPayid;///<订单支付ID
 *	String DealNote;///<订单备注
 *	String SellerNick;///<卖家昵称
 *	String BuyerNick;///<买家昵称
 *	String DisDealId;///<string订单id
 *	String BuyerBuyRemark;///<购买留言
 *	String ImportId;
 *	Vector<CTradoInfo> TradeList;///<子单列表
 *	Vector<CAccountInfo> AccountList;///<支付单列表
 *	CShippingInfo ShippingInfo;///<物流信息
 *	Vector<CActionLog> ActionLogList;
 *	int DealPayFeeCommission;///<货到付款手续费
 *****以上是版本11所包含的字段*******
 *
 *****以下是版本12所包含的字段*******
 *	short WhoPayShippingfee;///<承担运费方式,1卖家承担运费，2买家承担运费，3无需运费，0x0a支持运费模板的边界值
 *	short DealType;///<订单类型, 所有类型 1--一口价 SELL_TYPE_BIN 2--单件拍卖，3--b2c订单
 *	short DealState;///<订单状态
 *	short PreDealState;///<preState
 *	short DealPayType;///<支付方式 0未定义 1支付中介 2未使用财付通付款(货到付款) 3分期付款 4移动积分
 *	long DealId;///<id
 *	long GenTimestamp;///<订单序列
 *	long WuliuId;///<物流id
 *	long ItemlistMd5;///<商品ID签名
 *	long PayId;///<订单支付ID
 *	int CouponFee;///<折扣优惠金额
 *	long version;///<版本号
 *	long SellerUin;///<卖家号
 *	long BuyerUin;///<买家号
 *	long DealPayFeeShipping;///<邮费
 *	long PreDealPayFeeTotal;
 *	long DealPayFeeTotal;///<费用合计
 *	long DealCreateTime;///<订单生成时间
 *	long SellerCrm;///<客服CRM
 *	long MailType;///<邮递类型 1快递，2平邮，3EMS
 *	long BuyerRecvRefund;///<退款:买家退款金额
 *	long SellerRecvRefund;///<退款:卖家实收金额
 *	long DealRateState;///<评价状态
 *	long LastUpdateTime;///<lastmodify
 *	long DealPayFeeCash;///<现金支付金额
 *	long DealPayFeeTicket;///<财付券支付金额
 *	long DealPayFeeScore;///<积分支付金额
 *	long PayTime;///<支付完成时间
 *	long PayReturnTime;///<支付返回时间
 *	long SellerConsignmentTime;///<卖家发货时间
 *	long DealEndTime;///<订单结束时间
 *	long RecvfeeTime;///<打款完成时间
 *	long RecvfeeReturnTime;///<打款返回时间
 *	long DealNoteType;///<备注类型
 *	long DealRefundState;///<退款状态
 *	long Propertymask;///<大单属性位
 *	long DealRecvScore;
 *	long DataVersion;
 *	long BuyerDataVersion;
 *	String SellerName;///<卖家昵称
 *	String BuyerName;///<买家昵称
 *	String DealDesc;///<订单描述
 *	String ReceiveName;///<收货人姓名
 *	String ReceiveAddr;///<地址
 *	String ReceivePostcode;///<邮编
 *	String ReceiveTel;///<电话
 *	String strReceiveMobile;///<手机
 *	String DealInvoiceTitle;///<发票抬头
 *	String DealInvoiceContent;///<发票内容
 *	String ShippingfeeCalc;///<运费合计说明
 *	String ItemTitleList;///<商品标题列表
 *	String ComboInfo;///<促销信息
 *	String ReceiveAddrCode;///<收货地址_地址编码
 *	String Referer;///<订单来源信息
 *	String DealCftPayid;///<订单支付ID
 *	String DealNote;///<订单备注
 *	String SellerNick;///<卖家昵称
 *	String BuyerNick;///<买家昵称
 *	String DisDealId;///<string订单id
 *	String BuyerBuyRemark;///<购买留言
 *	String ImportId;
 *	Vector<CTradoInfo> TradeList;///<子单列表
 *	Vector<CAccountInfo> AccountList;///<支付单列表
 *	CShippingInfo ShippingInfo;///<物流信息
 *	Vector<CActionLog> ActionLogList;
 *	int DealPayFeeCommission;///<货到付款手续费
 *	Vector<CCodPayInfo> CodPayList;///<COD支付单列表信息
 *****以上是版本12所包含的字段*******
 *
 *****以下是版本15所包含的字段*******
 *	short WhoPayShippingfee;///<承担运费方式,1卖家承担运费，2买家承担运费，3无需运费，0x0a支持运费模板的边界值
 *	short DealType;///<订单类型, 所有类型 1--一口价 SELL_TYPE_BIN 2--单件拍卖，3--b2c订单
 *	short DealState;///<订单状态
 *	short PreDealState;///<preState
 *	short DealPayType;///<支付方式 0未定义 1支付中介 2未使用财付通付款(货到付款) 3分期付款 4移动积分
 *	long DealId;///<id
 *	long GenTimestamp;///<订单序列
 *	long WuliuId;///<物流id
 *	long ItemlistMd5;///<商品ID签名
 *	long PayId;///<订单支付ID
 *	int CouponFee;///<折扣优惠金额
 *	long version;///<版本号
 *	long SellerUin;///<卖家号
 *	long BuyerUin;///<买家号
 *	long DealPayFeeShipping;///<邮费
 *	long PreDealPayFeeTotal;
 *	long DealPayFeeTotal;///<费用合计
 *	long DealCreateTime;///<订单生成时间
 *	long SellerCrm;///<客服CRM
 *	long MailType;///<邮递类型 1快递，2平邮，3EMS
 *	long BuyerRecvRefund;///<退款:买家退款金额
 *	long SellerRecvRefund;///<退款:卖家实收金额
 *	long DealRateState;///<评价状态
 *	long LastUpdateTime;///<lastmodify
 *	long DealPayFeeCash;///<现金支付金额
 *	long DealPayFeeTicket;///<财付券支付金额
 *	long DealPayFeeScore;///<积分支付金额
 *	long PayTime;///<支付完成时间
 *	long PayReturnTime;///<支付返回时间
 *	long SellerConsignmentTime;///<卖家发货时间
 *	long DealEndTime;///<订单结束时间
 *	long RecvfeeTime;///<打款完成时间
 *	long RecvfeeReturnTime;///<打款返回时间
 *	long DealNoteType;///<备注类型
 *	long DealRefundState;///<退款状态
 *	long Propertymask;///<大单属性位
 *	long DealRecvScore;
 *	long DataVersion;
 *	long BuyerDataVersion;
 *	String SellerName;///<卖家昵称
 *	String BuyerName;///<买家昵称
 *	String DealDesc;///<订单描述
 *	String ReceiveName;///<收货人姓名
 *	String ReceiveAddr;///<地址
 *	String ReceivePostcode;///<邮编
 *	String ReceiveTel;///<电话
 *	String strReceiveMobile;///<手机
 *	String DealInvoiceTitle;///<发票抬头
 *	String DealInvoiceContent;///<发票内容
 *	String ShippingfeeCalc;///<运费合计说明
 *	String ItemTitleList;///<商品标题列表
 *	String ComboInfo;///<促销信息
 *	String ReceiveAddrCode;///<收货地址_地址编码
 *	String Referer;///<订单来源信息
 *	String DealCftPayid;///<订单支付ID
 *	String DealNote;///<订单备注
 *	String SellerNick;///<卖家昵称
 *	String BuyerNick;///<买家昵称
 *	String DisDealId;///<string订单id
 *	String BuyerBuyRemark;///<购买留言
 *	String ImportId;
 *	Vector<CTradoInfo> TradeList;///<子单列表
 *	Vector<CAccountInfo> AccountList;///<支付单列表
 *	CShippingInfo ShippingInfo;///<物流信息
 *	Vector<CActionLog> ActionLogList;
 *	int DealPayFeeCommission;///<货到付款手续费
 *	Vector<CCodPayInfo> CodPayList;///<COD支付单列表信息
 *	String WDealCode;///<物流Code
 *	String JdPin;///<收银台
 *	long Propertymask2;///<大单属性位2
 *	long Propertymask3;///<大单属性位3
 *	long Dealsource;///<订单来源
 *****以上是版本15所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
