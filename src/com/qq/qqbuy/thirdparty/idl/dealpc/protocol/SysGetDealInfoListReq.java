 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.deelpc.protocol.DealIdl.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import java.util.Vector;

/**
 *获取订单信息列表
 *
 *@date 2015-03-04 11:46:41
 *
 *@since version:0
*/
public class  SysGetDealInfoListReq extends NetMessage
{
	/**
	 * 调用者==>请设置为源文件名
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 大订单列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<String> DealIdList = new Vector<String>();

	/**
	 * 请求filter
	 *
	 * 版本 >= 0
	 */
	 private CDealListReqPo Filter = new CDealListReqPo();

	/**
	 * 用户MachineKey
	 *
	 * 版本 >= 0
	 */
	 private String MachineKey = new String();

	/**
	 * 保留输入字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveIn = new String();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(Source);
		bs.pushObject(DealIdList);
		bs.pushObject(Filter);
		bs.pushString(MachineKey);
		bs.pushString(ReserveIn);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		Source = bs.popString();
		DealIdList = (Vector<String>)bs.popVector(String.class);
		Filter = (CDealListReqPo) bs.popObject(CDealListReqPo.class);
		MachineKey = bs.popString();
		ReserveIn = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x26301801L;
	}


	/**
	 * 获取调用者==>请设置为源文件名
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置调用者==>请设置为源文件名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取大订单列表
	 * 
	 * 此字段的版本 >= 0
	 * @return DealIdList value 类型为:Vector<String>
	 * 
	 */
	public Vector<String> getDealIdList()
	{
		return DealIdList;
	}


	/**
	 * 设置大订单列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<String>
	 * 
	 */
	public void setDealIdList(Vector<String> value)
	{
		if (value != null) {
				this.DealIdList = value;
		}else{
				this.DealIdList = new Vector<String>();
		}
	}


	/**
	 * 获取请求filter
	 * 
	 * 此字段的版本 >= 0
	 * @return Filter value 类型为:CDealListReqPo
	 * 
	 */
	public CDealListReqPo getFilter()
	{
		return Filter;
	}


	/**
	 * 设置请求filter
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:CDealListReqPo
	 * 
	 */
	public void setFilter(CDealListReqPo value)
	{
		if (value != null) {
				this.Filter = value;
		}else{
				this.Filter = new CDealListReqPo();
		}
	}


	/**
	 * 获取用户MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @return MachineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return MachineKey;
	}


	/**
	 * 设置用户MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.MachineKey = value;
	}


	/**
	 * 获取保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveIn value 类型为:String
	 * 
	 */
	public String getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveIn(String value)
	{
		this.ReserveIn = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(SysGetDealInfoListReq)
				length += ByteStream.getObjectSize(Source, null);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(DealIdList, null);  //计算字段DealIdList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(Filter, null);  //计算字段Filter的长度 size_of(CDealListReqPo)
				length += ByteStream.getObjectSize(MachineKey, null);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveIn, null);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(SysGetDealInfoListReq)
				length += ByteStream.getObjectSize(Source, encoding);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(DealIdList, encoding);  //计算字段DealIdList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(Filter, encoding);  //计算字段Filter的长度 size_of(CDealListReqPo)
				length += ByteStream.getObjectSize(MachineKey, encoding);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveIn, encoding);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
