 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.deelpc.protocol.DealIdl.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *创建订单
 *
 *@date 2015-03-04 11:46:41
 *
 *@since version:0
*/
public class  CreateDealReq extends NetMessage
{
	/**
	 * 调用者==>请设置为源文件名
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 用户MachineKey
	 *
	 * 版本 >= 0
	 */
	 private String MachineKey = new String();

	/**
	 * 创建订单需要的信息
	 *
	 * 版本 >= 0
	 */
	 private CDealInfo oDealInfoIn = new CDealInfo();

	/**
	 * 加密签名
	 *
	 * 版本 >= 0
	 */
	 private String Signature = new String();

	/**
	 * 保留输入字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveIn = new String();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(Source);
		bs.pushString(MachineKey);
		bs.pushObject(oDealInfoIn);
		bs.pushString(Signature);
		bs.pushString(ReserveIn);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		Source = bs.popString();
		MachineKey = bs.popString();
		oDealInfoIn = (CDealInfo) bs.popObject(CDealInfo.class);
		Signature = bs.popString();
		ReserveIn = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x26301810L;
	}


	/**
	 * 获取调用者==>请设置为源文件名
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置调用者==>请设置为源文件名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取用户MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @return MachineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return MachineKey;
	}


	/**
	 * 设置用户MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.MachineKey = value;
	}


	/**
	 * 获取创建订单需要的信息
	 * 
	 * 此字段的版本 >= 0
	 * @return oDealInfoIn value 类型为:CDealInfo
	 * 
	 */
	public CDealInfo getODealInfoIn()
	{
		return oDealInfoIn;
	}


	/**
	 * 设置创建订单需要的信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:CDealInfo
	 * 
	 */
	public void setODealInfoIn(CDealInfo value)
	{
		if (value != null) {
				this.oDealInfoIn = value;
		}else{
				this.oDealInfoIn = new CDealInfo();
		}
	}


	/**
	 * 获取加密签名
	 * 
	 * 此字段的版本 >= 0
	 * @return Signature value 类型为:String
	 * 
	 */
	public String getSignature()
	{
		return Signature;
	}


	/**
	 * 设置加密签名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSignature(String value)
	{
		this.Signature = value;
	}


	/**
	 * 获取保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveIn value 类型为:String
	 * 
	 */
	public String getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveIn(String value)
	{
		this.ReserveIn = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(CreateDealReq)
				length += ByteStream.getObjectSize(Source, null);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(MachineKey, null);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(oDealInfoIn, null);  //计算字段oDealInfoIn的长度 size_of(CDealInfo)
				length += ByteStream.getObjectSize(Signature, null);  //计算字段Signature的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveIn, null);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(CreateDealReq)
				length += ByteStream.getObjectSize(Source, encoding);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(MachineKey, encoding);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(oDealInfoIn, encoding);  //计算字段oDealInfoIn的长度 size_of(CDealInfo)
				length += ByteStream.getObjectSize(Signature, encoding);  //计算字段Signature的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveIn, encoding);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
