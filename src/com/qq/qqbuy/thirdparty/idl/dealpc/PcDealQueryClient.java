package com.qq.qqbuy.thirdparty.idl.dealpc;

import java.net.InetSocketAddress;
import java.util.Vector;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.Pager;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.exception.ExternalInterfaceException;
import com.qq.qqbuy.deal.util.DealUtil;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CDealInfo;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CDealListReqPoNew;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.GetDealInfoList2Req;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.GetDealInfoList2Resp;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.GetEmployeeListReq;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.GetEmployeeListResp;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.SysGetDealInfoReq;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.SysGetDealInfoResp;

public class PcDealQueryClient extends SupportIDLBaseClient
{
	/**
	 *     
	 * @param uin
	 * @param dealList
	 * @param dealState
	 * 1)单独的查询状态:
	DealState = 0,  //所有状态
	DealState = 1,  //待付款
	DealState = 2, //已付款，待发货
	DealState = 3, //已发货
	DealState = 4, //交易成功
	DealState = 5, //交易关闭
	DealState = 6, //交易暂停
	DealState = 7, //款项处理中(确认收货)
	DealState = 8, //款项处理中(有退款)
	DealState = 9, //配货中
	DealState = 41, // 货到付款等待发货
	DealState = 42, // 货到付款已发货
	DealState = 43, // 货到付款已签收
	DealState = 44, // 货到付款拒签
	DealState = 45, // 货到付款成功
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
    public static GetDealInfoList2Resp getDealList(long uin,  long dealState,
    		long pageIndex, long pageSize) 
    {
        GetDealInfoList2Req req = new GetDealInfoList2Req();
        req.setSource("WebPPDealQueryClient");
        req.setSceneId(21); //不用鉴权
        CDealListReqPoNew po = new CDealListReqPoNew();
        po.setBuyerUin(uin);
        po.setDealState(dealState);
        po.setInfoType(3); //携带订单列表
        po.setPageIndex(pageIndex);
        po.setPageSize(pageSize);
        req.setOFilter(po);
        GetDealInfoList2Resp res = new GetDealInfoList2Resp();
        int ret = -1;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setOperator(uin);
        stub.setUin(uin);
        //如果组合查询，指定IP查询(当前组合查只发布在部分64位机器上)
		if(dealState == 700)
		{
			InetSocketAddress address = getAddressFromConfig("qgo.sys.dealQuery");
			stub.setIpAndPort(address.getHostName(), address.getPort());
		}
        ret = invokePaiPaiIDL(req, res, stub);
        return ret == SUCCESS ? res : null;
    }
    
    /**
     * 
     * @Title: getDealList
     *
     * @Description: 根据订单状态，评价状态返回订单列表
     * @param @param uin
     * @param @param dealState 订单状态
     * @param @param rateState 订单评价状态
     *  100需买家评价，101需卖家评价，102买家已评，103卖家已评
     * @param @param pageIndex
     * @param @param pageSize
     * @param short historyFlag : 是否查询历史数据    1查询历史数据（三个月前的）,其他都为三个月内的 
     * @param @return  设定文件
     * @return GetDealInfoList2Resp  返回类型
     * @throws
     */
    public static Pager<CDealInfo> getDealList(long buyerUin, long dealState, int rateState,
    		int pageIndex, int pageSize, int infoType, int historyFlag) 
    {
        GetDealInfoList2Req req = new GetDealInfoList2Req();
        GetDealInfoList2Resp resp = new GetDealInfoList2Resp();
        req.setSource("WebPPDealQueryClient");
        req.setSceneId(21); //不用鉴权
        
        CDealListReqPoNew po = new CDealListReqPoNew();
        po.setBuyerUin(buyerUin);
        po.setDealState(dealState);
        if (DealUtil.checkDealRateState(rateState)) {
        	po.setRateState(rateState); //订单评价状态
		}
        if(1 == historyFlag){ //查询三个月前的数据
        	po.setHistoryFlag_u((short)historyFlag);
        	po.setHistoryFlag(historyFlag); //这个起到查询历史的效果
        }
        
        po.setInfoType(infoType);
//      po.setTimeOrder(1); //0升序 1降序
        po.setPageIndex(pageIndex);
        po.setPageSize(pageSize);
        req.setOFilter(po);
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setOperator(buyerUin);
        stub.setUin(buyerUin);
        //如果组合查询，指定IP查询(当前组合查只发布在部分64位机器上)
		if(dealState == 700)
		{
			InetSocketAddress address = getAddressFromConfig("qgo.sys.dealQuery");
			stub.setIpAndPort(address.getHostName(), address.getPort());
		}
        if(1 == historyFlag && !EnvManager.isIdc()){ //在非IDC环境用这个订单列表提供的服务,原因： gamma环境下订单历史服务 未部署
    		stub.setIpAndPort("10.128.35.17", 53101); //线上的一个ip
        }
        invoke(stub, req, resp);
        if(resp.getResult() != 0)
        	throw new ExternalInterfaceException(0, resp.getResult(), resp.getErrMsg());
        return new Pager<CDealInfo>(resp.getODealInfoList(), new Long(resp.getTotalNum()).intValue());
    }
    
    /**
     * 
     * @Title: getDealStateNum
     *
     * @Description: 返回买家各状态下订单数量
     * @param @param uin
     * @param @param dealState
     * @param @param rateState
     * @param @return  设定文件
     * @return GetDealInfoList2Resp  返回类型
     * @deprecated  使用StatClient.getDealStatInfoByUin()和PcDealQueryClient.getDealList()代替
     * @throws
     */
    public static GetDealInfoList2Resp getDealStateNum(long uin, long dealState, long rateState) {
        GetDealInfoList2Req req = new GetDealInfoList2Req();
        req.setSource("WebPPDealQueryClient");
        req.setSceneId(21); //不用鉴权
        CDealListReqPoNew po = new CDealListReqPoNew();
        po.setBuyerUin(uin);
        po.setDealState(dealState);
        if (DealUtil.checkDealRateState((int)rateState)) {
        	po.setRateState(rateState); //订单评价状态
		}
        po.setInfoType(1); //不携带订单列表
        po.setPageIndex(1);
        po.setPageSize(1);
        req.setOFilter(po);
        GetDealInfoList2Resp res = new GetDealInfoList2Resp();
        int ret = -1;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setOperator(uin);
        stub.setUin(uin);
        //如果组合查询，指定IP查询(当前组合查只发布在部分64位机器上)
		if(dealState == 700)
		{
			InetSocketAddress address = getAddressFromConfig("qgo.sys.dealQuery");
			stub.setIpAndPort(address.getHostName(), address.getPort());
		}
		
		ret = invokePaiPaiIDL(req, res, stub);
        return ret == SUCCESS ? res : null;
    }
    
    /**
     * 买家查订单列表，传21场景ID时不用鉴权。PC侧下单时先往卖家库下单的，该接口数据存放在买家订单库中，故该接口数据不是实时的
     * 下单后由系统查询订单不要使用此接口，因数据有一定同步延迟
     * @param buyerUin
     * @param dealId
     * @param listItem
     * @return
     * @date:2013-7-25
     * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
     * @version 1.0
     */
    public static GetDealInfoList2Resp queryByDealId(long buyerUin, String dealId,int listItem) 
    {
		Vector<String> dealList = new Vector<String>();
		dealList.add(dealId); 
		GetDealInfoList2Req req = new GetDealInfoList2Req();
		req.setODealIdList(dealList);  //指定列表
		req.setSource("WebPPDealQueryClient");
		req.setSceneId(21); //不需要鉴权
		CDealListReqPoNew po = new CDealListReqPoNew();
		po.setBuyerUin(buyerUin);
		po.setDealState(0); //所以订单
		po.setInfoType(listItem == 1 ? 3 : 1);  //返回订单商品列表
		req.setOFilter(po);

		GetDealInfoList2Resp res = new GetDealInfoList2Resp();

		int ret = -1;
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		stub.setUin(buyerUin);
		stub.setOperator(buyerUin);

		ret = invokePaiPaiIDL(req, res,stub);
        return ret == SUCCESS ? res : null;
	}
    
    /**
     * 买家查订单列表，传21场景ID时不用鉴权。PC侧下单时先往卖家库下单的，该接口数据存放在买家订单库中，故该接口数据不是实时的
     * 下单后由系统查询订单不要使用此接口，因数据有一定同步延迟
     * @param buyerUin
     * @param dealId
     * @param listItem
     * @return
     * @date:2013-7-25
     * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
     * @version 1.0
     */
    public static GetDealInfoList2Resp queryByDealId(long buyerUin, String dealId, int listItem, short historyFlag) 
    {
		Vector<String> dealList = new Vector<String>();
		dealList.add(dealId); 
		GetDealInfoList2Req req = new GetDealInfoList2Req();
		req.setODealIdList(dealList);  //指定列表
		req.setSource("WebPPDealQueryClient");
		req.setSceneId(21); //不需要鉴权
		CDealListReqPoNew po = new CDealListReqPoNew();
		po.setBuyerUin(buyerUin);
		po.setDealState(0); //所以订单
		po.setInfoType(listItem==1?3:1);  //返回订单商品列表
		if(1 == historyFlag){ //查询三个月前的数据
            po.setHistoryFlag_u(historyFlag);
            po.setHistoryFlag(historyFlag); //这个起到查询历史的效果
        }
		req.setOFilter(po);

		GetDealInfoList2Resp res = new GetDealInfoList2Resp();
		int ret = -1;
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		stub.setUin(buyerUin);
		stub.setOperator(buyerUin);

		ret = invokePaiPaiIDL(req, res,stub);
        return ret == SUCCESS ? res : null;
	}

    /**
     * 重载方法
     * 用于查询物流信息，使用卖家订单ID进行查询
     * @param historyFlag 是否是三月之前的订单
     */
    public static GetDealInfoList2Resp queryDealWithWuli(String dealId, short historyFlag) {
        long seller = Long.parseLong(dealId.split("-")[0]);
        
        GetDealInfoList2Req req = new GetDealInfoList2Req();
        Vector<String> dealList = new Vector<String>();
        dealList.add(dealId); 
        req.setODealIdList(dealList);  //指定列表
        req.setSource("WebPPDealQueryClient");
        req.setSceneId(21); //不需要鉴权
        CDealListReqPoNew po = new CDealListReqPoNew();
        po.setSellerUin(seller);
        if(1 == historyFlag){ //查询三个月前的数据
            po.setHistoryFlag_u(historyFlag);
            po.setHistoryFlag(historyFlag); //这个起到查询历史的效果
        }
        po.setInfoType(InfoType.or(InfoType.DEAL, InfoType.TRADE_ALL, InfoType.SHIPPING));	//返回物流表
        req.setOFilter(po);

        GetDealInfoList2Resp res = new GetDealInfoList2Resp();
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setUin(seller);
        stub.setOperator(seller);
        invokePaiPaiIDL(req, res,stub);   
        return res;
    }
    
    /**
     * 系统查订单，适用于买家下完单后系统查询订单详情，该接口的数据是实时的
     * @param dealId
     * @param isHistory 是否历史订单
     * @param infoType 见InfoType.*
     * @return tradeId+dealId不存在时,返回null,不向外抛异常
     */
    public static CDealInfo sysGetDealInfo(String dealId, boolean isHistory, int infoType, String mk) {
    	SysGetDealInfoReq req = new SysGetDealInfoReq();
    	SysGetDealInfoResp resp = new SysGetDealInfoResp();
    	req.setDealId(dealId);
    	req.setSource("WebPPDealQueryClient");
    	req.setInfoType(infoType);
    	req.setIsHistory(isHistory);
    	
		AsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		if(mk != null)
			stub.setMachineKey(mk.getBytes());	//控制调用频率
		if(dealId != null)	{
			String[] arr = dealId.split("-");
			if(arr.length == 3)	{
				stub.setRouteKey(Long.parseLong(arr[0]));	//用卖家号路由
			}
		}
		invoke(stub, req, resp);
		if(resp.getResult() == 255)	{	//TODO 暂定255时代表dealId+isHistory不存在
			return null;
		} else if(resp.getResult() != 0)	{
			throw new ExternalInterfaceException(0, resp.getResult());
		}
		return resp.getODealInfo();
    }
    
    /**
     * 
     * @Title: queryEmployee 
     * @Description: 调用idl请求卖家员工列表
     * @param @param req
     * @param @return    设定文件 
     * @return GetDealInfoList2Resp    返回类型 
     * @throws
     */
	public static GetEmployeeListResp queryEmployee(GetEmployeeListReq req,String sk,long qq,String clientip) {
		GetEmployeeListResp res = new GetEmployeeListResp();
		AsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();	
		stub.setUin(qq);
        stub.setOperator(qq);
        stub.setSkey(sk.getBytes());
        //stub.setClientIP(Util.iP2Long(clientip));
        stub.setClientIP(0);
        stub.setMachineKey(req.getMachineKey().getBytes());
        stub.setRouteKey(qq);
        //stub.setIpAndPort("10.213.142.101",53101);
		int ret = invokePaiPaiIDL(req, res,stub);  
		Log.run.info("PcDealQueryClient:GetEmployeeListResp-->ret:"+ret);
        return ret == SUCCESS ? res : null;
	}
	
}
