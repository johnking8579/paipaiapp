package com.qq.qqbuy.thirdparty.idl.boss.protocol;


import com.paipai.lang.uint32_t;
import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;



public class CActiveRecmdInfo implements ICanSerializeObject
{
	public CActiveRecmdInfo()
	{
		
	}
	public uint32_t 	dwPageID = new uint32_t(0); 		
	public uint32_t 	dwDomainID = new uint32_t(0); 
	public uint32_t 	dwState = new uint32_t(0); 			//推荐状态
	public uint32_t	dwStartTime = new uint32_t(0); 		//推荐开始时间
	public uint32_t 	dwEndTime = new uint32_t(0); 			//推荐位结束时间
	public String sRecmdDesc = new String(); 			//推荐位描述
	public String sRecmdUrl = new String(); 			//推荐位链接
	public uint32_t 	dwSelfActiveID = new uint32_t(0); 		//自主营销活动ID
	
	public int serialize(ByteStream bs) throws Exception
	{
		 bs.pushUInt32_t(dwPageID);
         bs.pushUInt32_t(dwDomainID);
         bs.pushUInt32_t(dwState);
         bs.pushUInt32_t(dwStartTime);
         bs.pushUInt32_t(dwEndTime);
         bs.pushString(sRecmdDesc);
         bs.pushString(sRecmdUrl);
         bs.pushUInt32_t(dwSelfActiveID);
         
         return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		this.dwPageID = bs.popUint32_t();
        this.dwDomainID = bs.popUint32_t();
        this.dwState = bs.popUint32_t();
        this.dwStartTime = bs.popUint32_t();
        this.dwEndTime = bs.popUint32_t();
        this.sRecmdDesc = bs.popString("GBK");
        this.sRecmdUrl = bs.popString("GBK");
        this.dwSelfActiveID = bs.popUint32_t();
        
 
		return bs.getReadLength();
	}

	
	
	public uint32_t getDwPageID() 
	{
		return dwPageID;
	}

	public void setDwPageID(uint32_t dwPageID) 
	{
		this.dwPageID = dwPageID;
	}

	public uint32_t getDwDomainID() 
	{
		return dwDomainID;
	}

	public void setDwDomainID(uint32_t dwDomainID) 
	{
		this.dwDomainID = dwDomainID;
	}

	public uint32_t getDwState() 
	{
		return dwState;
	}

	public void setDwState(uint32_t dwState) 
	{
		this.dwState = dwState;
	}

	public uint32_t getDwStartTime() 
	{
		return dwStartTime;
	}

	public void setDwStartTime(uint32_t dwStartTime) 
	{
		this.dwStartTime = dwStartTime;
	}

	public uint32_t getDwEndTime()
{
		return dwEndTime;
	}

	public void setDwEndTime(uint32_t dwEndTime)
	{
		this.dwEndTime = dwEndTime;
	}

	public String getsRecmdDesc() 
	{
		return sRecmdDesc;
	}

	public void setsRecmdDesc(String sRecmdDesc) 
	{
		this.sRecmdDesc = sRecmdDesc;
	}

	public String getsRecmdUrl() 
	{
		return sRecmdUrl;
	}

	public void setsRecmdUrl(String sRecmdUrl) 
	{
		this.sRecmdUrl = sRecmdUrl;
	}

	public uint32_t getDwSelfActiveID() 
	{
		return dwSelfActiveID;
	}

	public void setDwSelfActiveID(uint32_t dwSelfActiveID) 
	{
		this.dwSelfActiveID = dwSelfActiveID;
	}

	@Override
	public int getSize() 
	{
		
		return 0;
	}

    @Override
    public String toString()
    {
        return "CActiveRecmdInfo [dwDomainID=" + dwDomainID + ", dwEndTime="
                + dwEndTime + ", dwPageID=" + dwPageID + ", dwSelfActiveID="
                + dwSelfActiveID + ", dwStartTime=" + dwStartTime
                + ", dwState=" + dwState + ", sRecmdDesc=" + sRecmdDesc
                + ", sRecmdUrl=" + sRecmdUrl + "]";
    }
	

}