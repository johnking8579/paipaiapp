package com.qq.qqbuy.thirdparty.idl.boss.protocol;


import com.paipai.lang.uint32_t;
import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;



public class CBoSlfMktMljContent implements ICanSerializeObject
{
	public CBoSlfMktMljContent()
	{
		
	}
	public     uint32_t     dwVersion = new uint32_t(1);
	public     uint32_t     dwContentID = new uint32_t(0);       //内容ID              
	public     uint32_t     dwActiveID = new uint32_t(0);        //活动ID            
	public     uint32_t     dwUin = new uint32_t(0);             //QQ号码                  
	public     uint32_t     dwInvalidFlag = new uint32_t(0);     //无效标记              
	public     uint32_t     dwCostFlag = new uint32_t(0);        //消费标记               
	public     uint32_t     dwCostMoney = new uint32_t(0);       //消费金额/笔数        
	public     uint32_t     dwFavorableFlag = new uint32_t(0);   //优惠标记            
	public     uint32_t     dwFreeMoney = new uint32_t(0);       //减免金额    分        
	public     uint32_t     dwFreeRebate = new uint32_t(0);      //折扣率    百分比         
	public     String       sPresentName = new String();      //赠品名称        VARCHAR2(255) 
	public     uint32_t     dwPresentType = new uint32_t(0);     //赠品类型
	public     String       sPresentID = new String();        //赠品ID        VARCHAR2(255)           
	public     String       sPresentUrl = new String();       //赠品图片地址    VARCHAR2(255)     
	public     uint32_t     dwBarterMoney = new uint32_t(0);     //换购金额    分         
	public     String       sBarterName = new String();       //换购商品名称    VARCHAR2(255) 
	public     uint32_t     dwBarterType = new uint32_t(0);      //换购商品类型
	public     String       sBarterID = new String();         //换购商品ID    VARCHAR2(255)       
	public     String       sBarterUrl = new String();        //换购商品图片地址    VARCHAR2(255)
	public     uint32_t     dwFacePerson = new uint32_t(0);      //面向人群
	public     uint32_t     dwBatchID = new uint32_t(0);         //店铺优惠券批次号
	public     uint32_t     dwFaceValue = new uint32_t(0);       //店铺优惠券面值
	
	public int serialize(ByteStream bs) throws Exception
	{
		 bs.pushUInt32_t(dwVersion);
         bs.pushUInt32_t(dwContentID);
         bs.pushUInt32_t(dwActiveID);
         bs.pushUInt32_t(dwUin);
         bs.pushUInt32_t(dwInvalidFlag);
         bs.pushUInt32_t(dwCostFlag);
         bs.pushUInt32_t(dwCostMoney);
         bs.pushUInt32_t(dwFavorableFlag);
         bs.pushUInt32_t(dwFreeMoney);
         bs.pushUInt32_t(dwFreeRebate);
         bs.pushString(sPresentName);
         bs.pushUInt32_t(dwPresentType);
         bs.pushString(sPresentID);
         bs.pushString(sPresentUrl);
         bs.pushUInt32_t(dwBarterMoney);
         bs.pushString(sBarterName);
         bs.pushUInt32_t(dwBarterType);
         bs.pushString(sBarterID);
         bs.pushString(sBarterUrl);
         bs.pushUInt32_t(dwFacePerson);
         bs.pushUInt32_t(dwBatchID);
         bs.pushUInt32_t(dwFaceValue);
         return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		this.dwVersion = bs.popUint32_t();
        this.dwContentID = bs.popUint32_t();
        this.dwActiveID = bs.popUint32_t();
        this.dwUin = bs.popUint32_t();
        this.dwInvalidFlag = bs.popUint32_t();
        this.dwCostFlag = bs.popUint32_t();
        this.dwCostMoney = bs.popUint32_t();
        this.dwFavorableFlag = bs.popUint32_t();
        this.dwFreeMoney = bs.popUint32_t();
        this.dwFreeRebate = bs.popUint32_t();
        this.sPresentName = bs.popString("GBK");
        this.dwPresentType = bs.popUint32_t();
        this.sPresentID = bs.popString();
        this.sPresentUrl = bs.popString();
        this.dwBarterMoney = bs.popUint32_t();
        this.sBarterName = bs.popString("GBK");
        this.dwBarterType = bs.popUint32_t();
        this.sBarterID = bs.popString();
        this.sBarterUrl = bs.popString();
        this.dwFacePerson = bs.popUint32_t();
        this.dwBatchID = bs.popUint32_t();
        this.dwFaceValue = bs.popUint32_t();
        
 
		return bs.getReadLength();
	}

	public uint32_t getDwVersion()
	{
		return dwVersion;
	}

	public void setDwVersion(uint32_t dwVersion) 
	{
		this.dwVersion = dwVersion;
	}

	public uint32_t getDwContentID()
	{
		return dwContentID;
	}

	public void setDwContentID(uint32_t dwContentID) 
	{
		this.dwContentID = dwContentID;
	}

	public uint32_t getDwActiveID() 
	{
		return dwActiveID;
	}

	public void setDwActiveID(uint32_t dwActiveID)
	{
		this.dwActiveID = dwActiveID;
	}

	public uint32_t getDwUin() 
	{
		return dwUin;
	}

	public void setDwUin(uint32_t dwUin) 
	{
		this.dwUin = dwUin;
	}

	public uint32_t getDwInvalidFlag() 
	{
		return dwInvalidFlag;
	}

	public void setDwInvalidFlag(uint32_t dwInvalidFlag)
	{
		this.dwInvalidFlag = dwInvalidFlag;
	}

	public uint32_t getDwCostFlag()
	{
		return dwCostFlag;
	}

	public void setDwCostFlag(uint32_t dwCostFlag) 
	{
		this.dwCostFlag = dwCostFlag;
	}

	public uint32_t getDwCostMoney() 
	{
		return dwCostMoney;
	}

	public void setDwCostMoney(uint32_t dwCostMoney) 
	{
		this.dwCostMoney = dwCostMoney;
	}

	public uint32_t getDwFavorableFlag() 
	{
		return dwFavorableFlag;
	}

	public void setDwFavorableFlag(uint32_t dwFavorableFlag)
	{
		this.dwFavorableFlag = dwFavorableFlag;
	}

	public uint32_t getDwFreeMoney() 
	{
		return dwFreeMoney;
	}

	public void setDwFreeMoney(uint32_t dwFreeMoney) 
	{
		this.dwFreeMoney = dwFreeMoney;
	}

	public uint32_t getDwFreeRebate() {
		return dwFreeRebate;
	}

	public void setDwFreeRebate(uint32_t dwFreeRebate) 
	{
		this.dwFreeRebate = dwFreeRebate;
	}

	public String getsPresentName() 
	{
		return sPresentName;
	}

	public void setsPresentName(String sPresentName) 
	{
		this.sPresentName = sPresentName;
	}

	public uint32_t getDwPresentType() 
	{
		return dwPresentType;
	}

	public void setDwPresentType(uint32_t dwPresentType)
	{
		this.dwPresentType = dwPresentType;
	}

	public String getsPresentID()
	{
		return sPresentID;
	}

	public void setsPresentID(String sPresentID) 
	{
		this.sPresentID = sPresentID;
	}

	public String getsPresentUrl()
	{
		return sPresentUrl;
	}

	public void setsPresentUrl(String sPresentUrl)
	{
		this.sPresentUrl = sPresentUrl;
	}

	public uint32_t getDwBarterMoney() 
	{
		return dwBarterMoney;
	}

	public void setDwBarterMoney(uint32_t dwBarterMoney) 
	{
		this.dwBarterMoney = dwBarterMoney;
	}

	public String getsBarterName()
	{
		return sBarterName;
	}

	public void setsBarterName(String sBarterName) 
	{
		this.sBarterName = sBarterName;
	}

	public uint32_t getDwBarterType() 
	{
		return dwBarterType;
	}

	public void setDwBarterType(uint32_t dwBarterType) 
	{
		this.dwBarterType = dwBarterType;
	}

	public String getsBarterID() 
	{
		return sBarterID;
	}

	public void setsBarterID(String sBarterID) 
	{
		this.sBarterID = sBarterID;
	}

	public String getsBarterUrl() 
	{
		return sBarterUrl;
	}

	public void setsBarterUrl(String sBarterUrl)
	{
		this.sBarterUrl = sBarterUrl;
	}

	public uint32_t getDwFacePerson() 
	{
		return dwFacePerson;
	}

	public void setDwFacePerson(uint32_t dwFacePerson)
	{
		this.dwFacePerson = dwFacePerson;
	}

	public uint32_t getDwBatchID() {
		return dwBatchID;
	}

	public void setDwBatchID(uint32_t dwBatchID) 
	{
		this.dwBatchID = dwBatchID;
	}

	public uint32_t getDwFaceValue()
	{
		return dwFaceValue;
	}

	public void setDwFaceValue(uint32_t dwFaceValue)
	{
		this.dwFaceValue = dwFaceValue;
	}

	public int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	
	@Override
	public int getSize() 
	{
		int length = 4;
		try{
				length = 4;  //size_of(CBoSlfMktMljContent)
				length += 4;  //计算字段dwVersion的长度 size_of(uint32_t)
				length += 4;  //计算字段dwContentID的长度 size_of(uint32_t)
				length += 4;  //计算字段dwActiveID的长度 size_of(uint32_t)
				length += 4;  //计算字段dwUin的长度 size_of(uint32_t)
				length += 4;  //计算字段dwInvalidFlag的长度 size_of(uint32_t)
				length += 4;  //计算字段dwCostFlag的长度 size_of(uint32_t)
				length += 4;  //计算字段dwCostMoney的长度 size_of(uint32_t)
				length += 4;  //计算字段dwFavorableFlag的长度 size_of(uint32_t)
				length += 4;  //计算字段dwFreeMoney的长度 size_of(uint32_t)
				length += 4;  //计算字段dwFreeRebate的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sPresentName);  //计算字段sPresentName的长度 size_of(String)
				length += 4;  //计算字段dwPresentType的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sPresentID);  //计算字段sPresentID的长度 size_of(String)
				length += ByteStream.getObjectSize(sPresentUrl);  //计算字段sPresentUrl的长度 size_of(String)
				length += 4;  //计算字段dwBarterMoney的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sBarterName);  //计算字段sBarterName的长度 size_of(String)
				length += 4;  //计算字段dwBarterType的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sBarterID);  //计算字段sBarterID的长度 size_of(String)
				length += ByteStream.getObjectSize(sBarterUrl);  //计算字段sBarterUrl的长度 size_of(String)
				length += 4;  //计算字段dwFavorableFlag的长度 size_of(dwFacePerson)
				length += 4;  //计算字段dwFreeMoney的长度 size_of(dwBatchID)
				length += 4;  //计算字段dwFreeRebate的长度 size_of(dwFaceValue)
				
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

    @Override
    public String toString()
    {
        return "CBoSlfMktMljContent [dwActiveID=" + dwActiveID
                + ", dwBarterMoney=" + dwBarterMoney + ", dwBarterType="
                + dwBarterType + ", dwBatchID=" + dwBatchID + ", dwContentID="
                + dwContentID + ", dwCostFlag=" + dwCostFlag + ", dwCostMoney="
                + dwCostMoney + ", dwFacePerson=" + dwFacePerson
                + ", dwFaceValue=" + dwFaceValue + ", dwFavorableFlag="
                + dwFavorableFlag + ", dwFreeMoney=" + dwFreeMoney
                + ", dwFreeRebate=" + dwFreeRebate + ", dwInvalidFlag="
                + dwInvalidFlag + ", dwPresentType=" + dwPresentType
                + ", dwUin=" + dwUin + ", dwVersion=" + dwVersion
                + ", sBarterID=" + sBarterID + ", sBarterName=" + sBarterName
                + ", sBarterUrl=" + sBarterUrl + ", sPresentID=" + sPresentID
                + ", sPresentName=" + sPresentName + ", sPresentUrl="
                + sPresentUrl + "]";
    }
}