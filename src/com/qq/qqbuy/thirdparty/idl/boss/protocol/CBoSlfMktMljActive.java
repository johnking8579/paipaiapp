package com.qq.qqbuy.thirdparty.idl.boss.protocol;

import java.util.Vector;



import com.paipai.lang.uint32_t;
import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


public class CBoSlfMktMljActive  implements ICanSerializeObject
{
	public CBoSlfMktMljActive()
	{
		
	}
	public     uint32_t     dwVersion = new uint32_t(1);
	public     uint32_t     dwActiveID = new uint32_t(0);     //活动ID    NUMBER(11) 
	public     uint32_t     dwUin = new uint32_t(0);          //QQ号码    NUMBER(11)
	public     String       sNickName = new String();      //昵称
	public     uint32_t     dwUserCredit = new uint32_t(0);   //用户信用
	public     String       sShopName = new String();      //店铺名称
	public     String       sPicUrl = new String();        //卖家上传的图片链接地址    VARCHAR2(255)
	public     uint32_t     dwBeginTime = new uint32_t(0);    //活动开始时间    
	public     uint32_t     dwEndTime = new uint32_t(0);      //活动结束时间    
	public     String       sCommClassify = new String();  //商品分类    VARCHAR2(255)
	public     String       sActiveDesc = new String();    //活动描述    VARCHAR2(255)
	public     uint32_t     dwState = new uint32_t(0);        //活动状态    NUMBER(11)
	public     uint32_t     dwSyncFlag = new uint32_t(0);     //是否有被同步到商城    NUMBER(11)
	public     uint32_t     dwCreateFlag = new uint32_t(0);   //创建标识    NUMBER(11)
	public     uint32_t     dwCreateTime = new uint32_t(0);   //创建时间    DATE
	public     uint32_t     dwUpdateTime = new uint32_t(0);   //最近更新时间
	public     uint32_t     dwUserProperty = new uint32_t(0); //用户属性
	public     uint32_t     dwActiveProperty = new uint32_t(0); //活动属性
	public     uint32_t     dwCommClass = new uint32_t(0);    //卖家注册的主营类目
	public     String       sDisableReason = new String(); //运营强制清除活动时，填写的理由
	public     uint32_t     dwActualEndTime = new uint32_t(0);//活动实际结束时间        
	public     uint32_t     dwFacePerson = new uint32_t(0);   //面向人群	
	public Vector<CBoSlfMktMljContent> vContents = new Vector<CBoSlfMktMljContent>();		//内容列表	

	@Override
	public int serialize(ByteStream bs) throws Exception
	{
		 bs.pushUInt32_t(dwVersion);
         bs.pushUInt32_t(dwActiveID);
         bs.pushUInt32_t(dwUin);
         bs.pushString(sNickName);
         bs.pushUInt32_t(dwUserCredit);
         bs.pushString(sShopName);
         bs.pushString(sPicUrl);
         bs.pushUInt32_t(dwBeginTime);
         bs.pushUInt32_t(dwEndTime);
         bs.pushString(sCommClassify);
         bs.pushString(sActiveDesc);
         bs.pushUInt32_t(dwState);
         bs.pushUInt32_t(dwSyncFlag);
         bs.pushUInt32_t(dwCreateFlag);
         bs.pushUInt32_t(dwCreateTime);
         bs.pushUInt32_t(dwUpdateTime);
         bs.pushUInt32_t(dwUserProperty);
         bs.pushUInt32_t(dwActiveProperty);
         bs.pushUInt32_t(dwCommClass);
         bs.pushString(sDisableReason);
         bs.pushUInt32_t(dwActualEndTime);
         bs.pushUInt32_t(dwFacePerson);
         bs.pushVector(vContents);
         return bs.getWrittenLength();
	}
	
	@Override
	public int unSerialize(ByteStream bs) throws Exception
	{
		this.dwVersion = bs.popUint32_t();
        this.dwActiveID = bs.popUint32_t();
        this.dwUin = bs.popUint32_t();
        this.sNickName = bs.popString("GBK");
        this.dwUserCredit = bs.popUint32_t();
        this.sShopName = bs.popString("GBK");
        this.sPicUrl = bs.popString();
        this.dwBeginTime = bs.popUint32_t();
        this.dwEndTime = bs.popUint32_t();
        this.sCommClassify = bs.popString("GBK");
        this.sActiveDesc = bs.popString("GBK");
        this.dwState = bs.popUint32_t();
        this.dwSyncFlag = bs.popUint32_t();
        this.dwCreateFlag = bs.popUint32_t();
        this.dwCreateTime = bs.popUint32_t();
        this.dwUpdateTime = bs.popUint32_t();
        this.dwUserProperty = bs.popUint32_t();
        this.dwActiveProperty = bs.popUint32_t();
        this.dwCommClass = bs.popUint32_t();
        this.sDisableReason = bs.popString("GBK");
        this.dwActualEndTime = bs.popUint32_t();
        this.dwFacePerson = bs.popUint32_t();
        this.vContents = (Vector<CBoSlfMktMljContent>) bs.popVector(CBoSlfMktMljContent.class);
        
 
		return bs.getReadLength();
	}
	
	public int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	@Override
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CBoSlfMktMljActive)
				length += 4;  //计算字段dwVersion的长度 size_of(uint32_t)
				length += 4;  //计算字段dwActiveID的长度 size_of(uint32_t)
				length += 4;  //计算字段dwUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sNickName);  //计算字段sNickName的长度 size_of(String)				
				length += 4;  //计算字段dwUserCredit的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sShopName);  //计算字段sShopName的长度 size_of(String)	
				length += ByteStream.getObjectSize(sPicUrl);  //计算字段sPicUrl的长度 size_of(String)	
				length += 4;  //计算字段dwBeginTime的长度 size_of(uint32_t)
				length += 4;  //计算字段dwEndTime的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sCommClassify);  //计算字段sCommClassify的长度 size_of(String)	
				length += ByteStream.getObjectSize(sActiveDesc);  //计算字段sActiveDesc的长度 size_of(String)	
				length += 4;  //计算字段dwState的长度 size_of(uint32_t)
				length += 4;  //计算字段dwSyncFlag的长度 size_of(uint32_t)
				length += 4;  //计算字段dwCreateFlag的长度 size_of(uint32_t)
				length += 4;  //计算字段dwCreateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段dwUpdateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段dwUserProperty的长度 size_of(uint32_t)
				length += 4;  //计算字段dwActiveProperty的长度 size_of(uint32_t)
				length += 4;  //计算字段dwCommClass的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sDisableReason);  //计算字段sDisableReason的长度 size_of(String)
				length += 4;  //计算字段dwActualEndTime的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(vContents);  //计算字段vContents的长度 size_of(Vector)
			
				
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}



	public uint32_t getDwVersion() 
	{
		return dwVersion;
	}

	public void setDwVersion(uint32_t dwVersion) 
	{
		this.dwVersion = dwVersion;
	}

	public uint32_t getDwActiveID() 
	{
		return dwActiveID;
	}

	public void setDwActiveID(uint32_t dwActiveID)
	{
		this.dwActiveID = dwActiveID;
	}

	public uint32_t getDwUin() 
	{
		return dwUin;
	}

	public void setDwUin(uint32_t dwUin) 
	{
		this.dwUin = dwUin;
	}

	public String getsNickName() 
	{
		return sNickName;
	}

	public void setsNickName(String sNickName) 
	{
		this.sNickName = sNickName;
	}

	public uint32_t getDwUserCredit() 
	{
		return dwUserCredit;
	}

	public void setDwUserCredit(uint32_t dwUserCredit) 
	{
		this.dwUserCredit = dwUserCredit;
	}

	public String getsShopName()
	{
		return sShopName;
	}

	public void setsShopName(String sShopName) 
	{
		this.sShopName = sShopName;
	}

	public String getsPicUrl() 
	{
		return sPicUrl;
	}

	public void setsPicUrl(String sPicUrl) 
	{
		this.sPicUrl = sPicUrl;
	}

	public uint32_t getDwBeginTime() 
	{
		return dwBeginTime;
	}

	public void setDwBeginTime(uint32_t dwBeginTime)
	{
		this.dwBeginTime = dwBeginTime;
	}

	public uint32_t getDwEndTime()
	{
		return dwEndTime;
	}

	public void setDwEndTime(uint32_t dwEndTime)
	{
		this.dwEndTime = dwEndTime;
	}

	public String getsCommClassify() 
	{
		return sCommClassify;
	}

	public void setsCommClassify(String sCommClassify)
	{
		this.sCommClassify = sCommClassify;
	}

	public String getsActiveDesc()
	{
		return sActiveDesc;
	}

	public void setsActiveDesc(String sActiveDesc)
	{
		this.sActiveDesc = sActiveDesc;
	}

	public uint32_t getDwState() 
	{
		return dwState;
	}

	public void setDwState(uint32_t dwState) 
	{
		this.dwState = dwState;
	}

	public uint32_t getDwSyncFlag()
	{
		return dwSyncFlag;
	}

	public void setDwSyncFlag(uint32_t dwSyncFlag) 
	{
		this.dwSyncFlag = dwSyncFlag;
	}

	public uint32_t getDwCreateFlag() 
	{
		return dwCreateFlag;
	}

	public void setDwCreateFlag(uint32_t dwCreateFlag) 
	{
		this.dwCreateFlag = dwCreateFlag;
	}

	public uint32_t getDwCreateTime() 
	{
		return dwCreateTime;
	}

	public void setDwCreateTime(uint32_t dwCreateTime) 
	{
		this.dwCreateTime = dwCreateTime;
	}

	public uint32_t getDwUpdateTime() 
	{
		return dwUpdateTime;
	}

	public void setDwUpdateTime(uint32_t dwUpdateTime) 
	{
		this.dwUpdateTime = dwUpdateTime;
	}

	public uint32_t getDwUserProperty() 
	{
		return dwUserProperty;
	}

	public void setDwUserProperty(uint32_t dwUserProperty) 
	{
		this.dwUserProperty = dwUserProperty;
	}

	public uint32_t getDwActiveProperty() 
	{
		return dwActiveProperty;
	}

	public void setDwActiveProperty(uint32_t dwActiveProperty) 
	{
		this.dwActiveProperty = dwActiveProperty;
	}

	public uint32_t getDwCommClass() 
	{
		return dwCommClass;
	}

	public void setDwCommClass(uint32_t dwCommClass) 
	{
		this.dwCommClass = dwCommClass;
	}

	public String getsDisableReason() 
	{
		return sDisableReason;
	}

	public void setsDisableReason(String sDisableReason) 
	{
		this.sDisableReason = sDisableReason;
	}

	public uint32_t getDwActualEndTime()
	{
		return dwActualEndTime;
	}

	public void setDwActualEndTime(uint32_t dwActualEndTime) 
	{
		this.dwActualEndTime = dwActualEndTime;
	}

	public uint32_t getDwFacePerson()
	{
		return dwFacePerson;
	}

	public void setDwFacePerson(uint32_t dwFacePerson)
	{
		this.dwFacePerson = dwFacePerson;
	}

	public Vector<CBoSlfMktMljContent> getvContents() 
	{
		return vContents;
	}

	public void setvContents(Vector<CBoSlfMktMljContent> vContents) 
	{
		this.vContents = vContents;
	}

    @Override
    public String toString()
    {
        return "CBoSlfMktMljActive [dwActiveID=" + dwActiveID
                + ", dwActiveProperty=" + dwActiveProperty
                + ", dwActualEndTime=" + dwActualEndTime + ", dwBeginTime="
                + dwBeginTime + ", dwCommClass=" + dwCommClass
                + ", dwCreateFlag=" + dwCreateFlag + ", dwCreateTime="
                + dwCreateTime + ", dwEndTime=" + dwEndTime + ", dwFacePerson="
                + dwFacePerson + ", dwState=" + dwState + ", dwSyncFlag="
                + dwSyncFlag + ", dwUin=" + dwUin + ", dwUpdateTime="
                + dwUpdateTime + ", dwUserCredit=" + dwUserCredit
                + ", dwUserProperty=" + dwUserProperty + ", dwVersion="
                + dwVersion + ", sActiveDesc=" + sActiveDesc
                + ", sCommClassify=" + sCommClassify + ", sDisableReason="
                + sDisableReason + ", sNickName=" + sNickName + ", sPicUrl="
                + sPicUrl + ", sShopName=" + sShopName + ", vContents="
                + vContents + "]";
    }


}