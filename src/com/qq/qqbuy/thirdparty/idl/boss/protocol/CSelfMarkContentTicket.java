package com.qq.qqbuy.thirdparty.idl.boss.protocol;


import com.paipai.lang.uint32_t;
import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;



public class CSelfMarkContentTicket implements ICanSerializeObject
{
	public CSelfMarkContentTicket()
	{
		
	}
	public     uint32_t     dwContentID = new uint32_t(0);
	public     uint32_t     dwBatchID = new uint32_t(0);       //内容ID              
	public     uint32_t     dwFaceValue = new uint32_t(0);        //活动ID            
	
	public int serialize(ByteStream bs) throws Exception
	{
		 bs.pushUInt32_t(dwContentID);
         bs.pushUInt32_t(dwBatchID);
         bs.pushUInt32_t(dwFaceValue);
        
         return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
        this.dwContentID = bs.popUint32_t();
        this.dwBatchID = bs.popUint32_t();
        this.dwFaceValue = bs.popUint32_t();       
 
		return bs.getReadLength();
	}

	
	
	public uint32_t getDwContentID()
{
		return dwContentID;
	}

	public void setDwContentID(uint32_t dwContentID) 
	{
		this.dwContentID = dwContentID;
	}

	public uint32_t getDwBatchID() 
	{
		return dwBatchID;
	}

	public void setDwBatchID(uint32_t dwBatchID)
	{
		this.dwBatchID = dwBatchID;
	}

	public uint32_t getDwFaceValue() 
	{
		return dwFaceValue;
	}

	public void setDwFaceValue(uint32_t dwFaceValue) 
	{
		this.dwFaceValue = dwFaceValue;
	}

	@Override
	public int getSize() 
	{
		return 0;
	}

    @Override
    public String toString()
    {
        return "CSelfMarkContentTicket [dwBatchID=" + dwBatchID
                + ", dwContentID=" + dwContentID + ", dwFaceValue="
                + dwFaceValue + "]";
    }
	
}