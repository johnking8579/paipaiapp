//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: gen.DataserverBatch.java

package com.qq.qqbuy.thirdparty.idl.boss.protocol;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *dataserver协议同步请求结构
 *
 *@date 2012-06-15 02:49:01
 *
 *@since version:0
*/
public class DataserverData  implements ICanSerializeObject
{
	/**
	 * 版本 >= 0
	 */
	 private long uiVersion;

	/**
	 * 版本 >= 0
	 */
	 private long uiTime;

	/**
	 * 卖家QQ
	 *
	 * 版本 >= 0
	 */
	 private long uiQQ;

	/**
	 * 业务名，例如：cps 
	 *
	 * 版本 >= 0
	 */
	 private String strBusiness = new String();

	/**
	 * 业务类型
	 *
	 * 版本 >= 0
	 */
	 private String strType = new String();

	/**
	 * 数据内容
	 *
	 * 版本 >= 0
	 */
	 private String strData = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		int length = bs.getWrittenLength();   
		bs.pushUInt(0);    
		bs.pushUInt(uiVersion);
		bs.pushUInt(uiTime);
		bs.pushUInt(uiQQ);
		bs.pushString(strBusiness);
		bs.pushString(strType);
		bs.pushString(strData);
		int endlength = bs.getWrittenLength();  
		bs.skip(-(endlength - length));      
		bs.pushUInt(endlength - length - 4);    
		bs.skip(endlength - length - 4);  
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		int len = (int) bs.popUInt(); 
		int iReadLen = bs.getReadLength(); 
		uiVersion = bs.popUInt();
		uiTime = bs.popUInt();
		uiQQ = bs.popUInt();
		strBusiness = bs.popString();
		strType = bs.popString();
		strData = bs.popString();
		int iReadLen2 = bs.getReadLength();
		int skip = len - (iReadLen2 - iReadLen);
		bs.skip(skip);
		return bs.getReadLength();
	} 


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return uiVersion value 类型为:long
	 * 
	 */
	public long getUiVersion()
	{
		return uiVersion;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUiVersion(long value)
	{
		this.uiVersion = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return uiTime value 类型为:long
	 * 
	 */
	public long getUiTime()
	{
		return uiTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUiTime(long value)
	{
		this.uiTime = value;
	}


	/**
	 * 获取卖家QQ
	 * 
	 * 此字段的版本 >= 0
	 * @return uiQQ value 类型为:long
	 * 
	 */
	public long getUiQQ()
	{
		return uiQQ;
	}


	/**
	 * 设置卖家QQ
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUiQQ(long value)
	{
		this.uiQQ = value;
	}


	/**
	 * 获取业务名，例如：cps 
	 * 
	 * 此字段的版本 >= 0
	 * @return strBusiness value 类型为:String
	 * 
	 */
	public String getStrBusiness()
	{
		return strBusiness;
	}


	/**
	 * 设置业务名，例如：cps 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrBusiness(String value)
	{
		this.strBusiness = value;
	}


	/**
	 * 获取业务类型
	 * 
	 * 此字段的版本 >= 0
	 * @return strType value 类型为:String
	 * 
	 */
	public String getStrType()
	{
		return strType;
	}


	/**
	 * 设置业务类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrType(String value)
	{
		this.strType = value;
	}


	/**
	 * 获取数据内容
	 * 
	 * 此字段的版本 >= 0
	 * @return strData value 类型为:String
	 * 
	 */
	public String getStrData()
	{
		return strData;
	}


	/**
	 * 设置数据内容
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrData(String value)
	{
		this.strData = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(DataserverData)
				length += 4;  //计算字段uiVersion的长度 size_of(uint32_t)
				length += 4;  //计算字段uiTime的长度 size_of(uint32_t)
				length += 4;  //计算字段uiQQ的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(strBusiness);  //计算字段strBusiness的长度 size_of(String)
				length += ByteStream.getObjectSize(strType);  //计算字段strType的长度 size_of(String)
				length += ByteStream.getObjectSize(strData);  //计算字段strData的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
