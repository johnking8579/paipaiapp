package com.qq.qqbuy.thirdparty.idl.boss.protocol;


import com.paipai.lang.uint32_t;
import com.paipai.util.io.ByteStream;
import com.qq.qqbuy.thirdparty.idl.boss.IBossServiceObject;



public class CSelfMarketCopyResp implements IBossServiceObject
{
	private long cmdId;
    private long subCmdId;
    
    private int sendResult = -1;
    private uint32_t uin = new uint32_t();
	private CBoSlfMktMljActive oBoSlfMktMljActive = new CBoSlfMktMljActive();
    
	@Override
	public int Serialize(ByteStream arg0) throws Exception
	{
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public int UnSerialize(ByteStream bs) throws Exception
	{
		this.cmdId = bs.popInt();
        this.sendResult = bs.popInt();
        this.uin = bs.popUint32_t();
        this.oBoSlfMktMljActive = (CBoSlfMktMljActive) bs.popObject(CBoSlfMktMljActive.class);
 
		return bs.getReadLength();
	}

	@Override
	public long getCmdId()
	{
		return cmdId;
	}

	public long getSubCmdId()
	{
		return subCmdId;
	}

	public void setSubCmdId(long subCmdId)
	{
		this.subCmdId = subCmdId;
	}

	public void setCmdId(long cmdId)
	{
		this.cmdId = cmdId;
	}

	@Override
	public String getCmdDesc()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setHasResult(boolean arg0)
	{
		// TODO Auto-generated method stub
		
	}

	
	

	public CBoSlfMktMljActive getoBoSlfMktMljActive() 
	{
		return oBoSlfMktMljActive;
	}


	public void setoBoSlfMktMljActive(CBoSlfMktMljActive oBoSlfMktMljActive) 
	{
		this.oBoSlfMktMljActive = oBoSlfMktMljActive;
	}


	public uint32_t getUin()
	{
		return uin;
	}

	public void setUin(uint32_t uin) 
	{
		this.uin = uin;
	}

	public int getSendResult() 
	{
		return sendResult;
	}

	public void setSendResult(int sendResult) 
	{
		this.sendResult = sendResult;
	}
}
