package com.qq.qqbuy.thirdparty.idl.boss.protocol;

import java.util.Vector;

import com.paipai.lang.uint32_t;
import com.qq.qqbuy.thirdparty.idl.boss.BossWebStub2;

public class CSelfMarket
{    
    
    //优惠类型
    public static final int Boss_SlefMarket_Favor_None          = 0;    //无
    public static final int Boss_SlefMarket_Favor_FreeMoney     = 1;    //减金额
    public static final int Boss_SlefMarket_Favor_Rebate        = 2;    //折扣
    public static final int Boss_SlefMarket_Favor_Present       = 4;    //是否赠送
    public static final int Boss_SlefMarket_Favor_Barter        = 8;    //是否换购
    public static final int Boss_SlefMarket_Favor_Express       = 16;   //是否快递
    public static final int Boss_SlefMarket_Favor_Ticket        = 32;   //是否送店铺优惠券
    

    //消费标记
    public static final int Boss_SlefMarket_Cost_Num            = 0;    //买多少件就优惠
    public static final int Boss_SlefMarket_Cost_Money          = 1;    //消费多少元才优惠



    //活动状态
    public static final int Boss_SelfMarket_State_NotStart      = 0;        //未开始
    public static final int Boss_SelfMarket_State_Started       = 1;        //已开始
    public static final int Boss_SelfMarket_State_Closed        = 2;        //已结束
    public static final int Boss_SelfMarket_State_Clear         = 3;        //活动被运营人员清空
    public static final int Boss_SelfMarket_State_UserCancel    = 4;        //用户取消 
    

    //推广状态
    public static final int Boss_SelfMarket_Recmd_State_Wait    = 0;        //待审核
    public static final int Boss_SelfMarket_Recmd_State_Audited = 1;        //已审核通过，等待推荐
    public static final int Boss_SelfMarket_Recmd_State_Recmding= 2;        //推荐中
    public static final int Boss_SelfMarket_Recmd_State_Recmded = 3;        //推荐结束（已取消标记）
    public static final int Boss_SelfMarket_Recmd_State_Refuse  = 4;        //已拒绝


    //外部返回码定义
    public static final int Boss_SelfMarket_Return_HaveActive =  0;   //有活动
    public static final int Boss_SelfMarket_Return_Err        = -1;   //错误
    public static final int Boss_SelfMarket_Return_NoActive   = 2;   //没有活动
    public static final int Boss_SelfMarket_Return_StateErr   = 3;   //活动状态错误
    public static final int Boss_SelfMarket_Return_InfoErr    = 4;   //活动信息错误

    
    
    public int Cancel(CSelfMarketCancelReq oSelfMarketCancelReq, CSelfMarketCommonResp oSelfMarketCommonResp) throws Exception
    {
        if (oSelfMarketCancelReq == null || oSelfMarketCommonResp == null)
        {
            return -1;
        }
        try
        {
            BossWebStub2 stub = new BossWebStub2();
            stub.invoke(oSelfMarketCancelReq, oSelfMarketCommonResp);
        }catch(Exception e) 
        {
            e.printStackTrace();
            
            return -2;
        }
        
        
        return oSelfMarketCommonResp.getSendResult();
    }
    
    public int QueryValid(CSelfMarketQueryValidReq oSelfMarketQueryValidReq, CSelfMarketQueryValidResp oSelfMarketQueryValidResp) throws Exception
    {
        if (oSelfMarketQueryValidReq == null || oSelfMarketQueryValidResp == null)
        {
            return -1;
        }
        try
        {
            BossWebStub2 stub = new BossWebStub2();
            stub.invoke(oSelfMarketQueryValidReq, oSelfMarketQueryValidResp);
        }catch(Exception e) 
        {
            e.printStackTrace();
            
            return -2;
        }
        
        if(oSelfMarketQueryValidResp.getSendResult() == 0)
        {
            if(oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getDwActiveID().getValue() > 0)
            {
                return 0;
            }
            else
            {
                return 1;
            }
            
        }
        
        return oSelfMarketQueryValidResp.getSendResult();
    }
    
    public int QueryHis(CSelfMarketQueryHisReq oSelfMarketQueryHisReq, CSelfMarketQueryHisResp oSelfMarketQueryHisResp) throws Exception
    {
        if (oSelfMarketQueryHisReq == null || oSelfMarketQueryHisResp == null)
        {
            return -1;
        }
        try
        {
            BossWebStub2 stub = new BossWebStub2();
            stub.invoke(oSelfMarketQueryHisReq, oSelfMarketQueryHisResp);
        }catch(Exception e) 
        {
            e.printStackTrace();
            
            return -2;
        }
        
        return oSelfMarketQueryHisResp.getSendResult();
    }
    
    public int New(CSelfMarketCommitReq oSelfMarketCommitReq , CSelfMarketCommonResp oSelfMarketCommitResp) throws Exception
    {
        if (oSelfMarketCommitReq == null || oSelfMarketCommitResp == null)
        {
            return -1;
        }       
        try
        {
            BossWebStub2 stub = new BossWebStub2();
            
            CSelfMarketCheckNewReq oSelfMarketCheckNewReq = new  CSelfMarketCheckNewReq();
            oSelfMarketCheckNewReq.setUin(oSelfMarketCommitReq.getUin());
            CSelfMarketCommonResp oSelfMarketCheckNewResp = new CSelfMarketCommonResp();
            
            
            stub.invoke(oSelfMarketCheckNewReq, oSelfMarketCheckNewResp);
            if (oSelfMarketCheckNewResp.getSendResult() != 0 )  
            {
                return oSelfMarketCheckNewResp.getSendResult() ;
            }
            
            
            oSelfMarketCommitReq.setType(0);
            stub.invoke(oSelfMarketCommitReq, oSelfMarketCommitResp);
        }catch(Exception e) 
        {
            e.printStackTrace();
            
            return -3;
        }
        
        return oSelfMarketCommitResp.getSendResult();
    }
    
    
    public int Modify(CSelfMarketCommitReq oSelfMarketCommitReq , CSelfMarketCommonResp oSelfMarketCommitResp) throws Exception
    {
        if (oSelfMarketCommitReq == null || oSelfMarketCommitResp == null)
        {
            return -1;
        }
        try
        {
            BossWebStub2 stub = new BossWebStub2();
            
            CSelfMarketCheckModifyReq oSelfMarketCheckModifyReq = new  CSelfMarketCheckModifyReq();
            oSelfMarketCheckModifyReq.setUin(oSelfMarketCommitReq.getUin());
            oSelfMarketCheckModifyReq.setActiveID(oSelfMarketCommitReq.getoBoSlfMktMljActive().getDwActiveID());
            
            CSelfMarketCheckModifyResp oSelfMarketCheckModifyResp = new CSelfMarketCheckModifyResp();
            
            
            stub.invoke(oSelfMarketCheckModifyReq, oSelfMarketCheckModifyResp);
            if (oSelfMarketCheckModifyResp.getSendResult() != 0)
            {
                return oSelfMarketCheckModifyResp.getSendResult();
            }
            
            oSelfMarketCommitReq.setType(1);
            stub.invoke(oSelfMarketCommitReq, oSelfMarketCommitResp);
        }catch(Exception e) 
        {
            e.printStackTrace();
            
            return -2;
        }
        
        return oSelfMarketCommitResp.getSendResult();
    }
    
    public int Copy(CSelfMarketCopyReq oSelfMarketCopyReq , CSelfMarketCopyResp oSelfMarketCopyResp) throws Exception
    {
        if (oSelfMarketCopyReq == null || oSelfMarketCopyResp == null)
        {
            return -1;
        }
        try
        {
            BossWebStub2 stub = new BossWebStub2();
            stub.invoke(oSelfMarketCopyReq, oSelfMarketCopyResp);
            
        }catch(Exception e) 
        {
            e.printStackTrace();
            
            return -2;
        }
        
        return oSelfMarketCopyResp.getSendResult();
    }
    
    
    
    
    public static void main(String[] args) throws Exception 
    {
        CSelfMarket oSelfMarket = new CSelfMarket();
        uint32_t uin = new uint32_t(382232782);
        int iRet = 0;
         
        // 新建活动
        
        CSelfMarketCommitReq oSelfMarketNewReq = new CSelfMarketCommitReq();
        oSelfMarketNewReq.setUin(uin);
        
        CBoSlfMktMljActive oBoSlfMktMljActive = new CBoSlfMktMljActive();
        oBoSlfMktMljActive.setDwActiveID(new uint32_t(0));
        oBoSlfMktMljActive.setDwBeginTime(new uint32_t(1359732311));
        oBoSlfMktMljActive.setDwEndTime(new uint32_t(1359882311));
        oBoSlfMktMljActive.setsActiveDesc("99999999999");
        oBoSlfMktMljActive.setDwUin(uin);
        

        
        CBoSlfMktMljContent oBoSlfMktMljContent = new CBoSlfMktMljContent();
        oBoSlfMktMljContent.setDwUin(uin);
        oBoSlfMktMljContent.setDwCostFlag(new uint32_t(1));
        oBoSlfMktMljContent.setDwCostMoney(new uint32_t(100));
        
        int dwFavorableFlag = 0;
        dwFavorableFlag |= CSelfMarket.Boss_SlefMarket_Favor_Express;
        oBoSlfMktMljContent.setDwFavorableFlag(new uint32_t(dwFavorableFlag));

        
        Vector<CBoSlfMktMljContent> vContents = new Vector<CBoSlfMktMljContent>();
        vContents.add(oBoSlfMktMljContent);
        
        oBoSlfMktMljActive.setvContents(vContents);
        
        oSelfMarketNewReq.setoBoSlfMktMljActive(oBoSlfMktMljActive);
        
        CSelfMarketCommonResp oSelfMarketNewResp = new CSelfMarketCommonResp();
        
        iRet = oSelfMarket.New(oSelfMarketNewReq, oSelfMarketNewResp);
        System.out.println("New ret:"+ iRet) ;
        if(iRet == 0)
        {
            if(oSelfMarketNewResp.getResult() == 0)
            {
                System.out.println("oSelfMarketModifyReqResp.getUin():"+ oSelfMarketNewResp.getUin());
            }
            else
            {
                System.out.println("New Error");    
            }
                        
        }
        else
        {
            System.out.println("New Error");
        }
        
        
        
        // 查询当前有效活动
        CSelfMarketQueryValidReq oSelfMarketQueryValidReq = new CSelfMarketQueryValidReq();
        oSelfMarketQueryValidReq.setUin(uin);
        
        CSelfMarketQueryValidResp oSelfMarketQueryValidResp = new CSelfMarketQueryValidResp();  
        
        iRet = oSelfMarket.QueryValid(oSelfMarketQueryValidReq, oSelfMarketQueryValidResp);
        System.out.println("QueryValid ret:"+ iRet) ;
        if(iRet == 0)
        {
            System.out.println("oSelfMarketQueryValidResp.getUin():"+ oSelfMarketQueryValidResp.getUin()) ;
            System.out.println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getsActiveDesc():"+ oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getsActiveDesc()) ;
            System.out.println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getDwActiveID():"+ oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getDwActiveID()) ;
            System.out.println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getsNickName():"+ oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getsNickName()) ;
            System.out.println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getDwBeginTime(:"+ oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getDwBeginTime()) ;
            System.out.println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getDwUin():"+ oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getDwUin()) ;
            int size =  oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getvContents().size();
            System.out.println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getvContents().size(:"+ size) ;
            if(size > 0)
            {
                System.out.println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getvContents().get(0).getDwActiveID():"+ oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getvContents().get(0).getDwActiveID()) ;
                System.out.println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getvContents().get(0).getDwContentID():"+ oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getvContents().get(0).getDwContentID()) ;
                System.out.println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getvContents().get(0).getDwCostFlag():"+ oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getvContents().get(0).getDwCostFlag()) ;
                System.out.println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getvContents().get(0).getDwCostMoney():"+ oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getvContents().get(0).getDwCostMoney()) ;
                System.out.println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getvContents().get(0).getDwUin():"+ oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getvContents().get(0).getDwUin()) ;
                System.out.println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getvContents().get(0).getDwFavorableFlag():"+ oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getvContents().get(0).getDwFavorableFlag()) ;

            }
            
        }
        else
        {
            if(iRet > 0)
            {
                System.out.println("No Valid Acitive");
            }
            else
            {
                System.out.println("QueryValid Error");
            }
        }
        

        
        
        
        // 修改活动
        CSelfMarketCommitReq oSelfMarketModifyReq = new CSelfMarketCommitReq();
        CSelfMarketCommonResp oSelfMarketModifyReqResp = new CSelfMarketCommonResp();
        
        oSelfMarketModifyReq.setUin(uin);
        CBoSlfMktMljActive oCBoSlfMktMljActive = oSelfMarketQueryValidResp.getoCBoSlfMktMljActive();
        oCBoSlfMktMljActive.setsActiveDesc("kkkkkk");
        oSelfMarketModifyReq.setoBoSlfMktMljActive(oCBoSlfMktMljActive);
        
        iRet = oSelfMarket.Modify(oSelfMarketModifyReq, oSelfMarketModifyReqResp);
        System.out.println("Modify ret:"+ iRet);
        if(iRet == 0)
        {
            if(oSelfMarketModifyReqResp.getResult() == 0)
            {
                System.out.println("oSelfMarketModifyReqResp.getUin():"+ oSelfMarketModifyReqResp.getUin());
            }
            else
            {
                System.out.println("Modify Error"); 
            }
                        
        }
        else
        {
            System.out.println("Modify Error");
        }
         
        // 取消活动
        CSelfMarketCancelReq oSelfMarketCancelReq = new CSelfMarketCancelReq();
        CSelfMarketCommonResp oSelfMarketCancelResp = new CSelfMarketCommonResp();
        
        iRet = oSelfMarket.Cancel(oSelfMarketCancelReq, oSelfMarketCancelResp);
        System.out.println("Modify ret:"+ iRet);
        if(iRet == 0)
        {
            if(oSelfMarketModifyReqResp.getResult() == 0)
            {
                System.out.println("oSelfMarketCancelResp():"+ oSelfMarketModifyReqResp.getUin());
            }
            else
            {
                System.out.println("Cancel Error"); 
            }
                        
        }
        else
        {
            System.out.println("Cancel Error");
        }
        
        
        // 查询历史活动
        CSelfMarketQueryHisReq oSelfMarketQueryHisReq = new CSelfMarketQueryHisReq();
        oSelfMarketQueryHisReq.setUin(uin);
        oSelfMarketQueryHisReq.setDwPageNo(new uint32_t(1));
        oSelfMarketQueryHisReq.setDwPageNum(new uint32_t(100));

        
        CSelfMarketQueryHisResp oSelfMarketQueryHisResp = new CSelfMarketQueryHisResp();  
        
        iRet = oSelfMarket.QueryHis(oSelfMarketQueryHisReq, oSelfMarketQueryHisResp);
        System.out.println("QueryHis ret:"+ iRet) ;
        if(iRet == 0)
        {
            System.out.println("oSelfMarketQueryHisResp total:"+ oSelfMarketQueryHisResp.getDwTotal()) ;
        }
        else
        {
            System.out.println("QueryHis Error");
        }
    }
}