package com.qq.qqbuy.thirdparty.idl.boss.protocol;



import com.paipai.lang.uint32_t;
import com.paipai.util.io.ByteStream;
import com.qq.qqbuy.thirdparty.idl.boss.IBossServiceObject;




public class CSelfMarketQueryHisReq implements IBossServiceObject
{
	private long cmdId = 0x00030007;
    private String cmdDesc = "BossSelfMarkManage";
    private long subCmdId = 0;
    
    
    private uint32_t uin = new uint32_t();
    private uint32_t 	dwPageNo = new uint32_t();		//活动分页,页码
    private uint32_t 	dwPageNum = new uint32_t();		//每页数据量

    
	@Override
	public int Serialize(ByteStream bs) throws Exception
	{
        bs.pushUInt32_t(uin);
        bs.pushUInt32_t(dwPageNo);
        bs.pushUInt32_t(dwPageNum);
        return bs.getWrittenLength();
	}
    
	@Override
	public String getCmdDesc()
	{
		return cmdDesc;
	}
	@Override
	public long getSubCmdId()
	{
		return subCmdId;
	}
	
	@Override
	public void setHasResult(boolean arg0)
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public int UnSerialize(ByteStream arg0) throws Exception
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public long getCmdId()
	{
		// TODO Auto-generated method stub
		return cmdId;
	}

	
	public void setCmdId(long cmdId)
	{
		this.cmdId = cmdId;
	}

	public void setCmdDesc(String cmdDesc)
	{
		this.cmdDesc = cmdDesc;
	}

	public void setSubCmdId(long subCmdId)
	{
		this.subCmdId = subCmdId;
	}

	public uint32_t getUin() 
	{
		return uin;
	}

	public void setUin(uint32_t uin) 
	{
		this.uin = uin;
	}

	public uint32_t getDwPageNo() 
	{
		return dwPageNo;
	}

	public void setDwPageNo(uint32_t dwPageNo) 
	{
		this.dwPageNo = dwPageNo;
	}

	public uint32_t getDwPageNum() 
	{
		return dwPageNum;
	}

	public void setDwPageNum(uint32_t dwPageNum) 
	{
		this.dwPageNum = dwPageNum;
	}

}