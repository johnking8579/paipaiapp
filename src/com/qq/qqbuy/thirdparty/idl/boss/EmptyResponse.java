package com.qq.qqbuy.thirdparty.idl.boss;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

public class EmptyResponse implements IServiceObject {
	private long cmdId;

	public EmptyResponse(long cmdId) {
		this.cmdId = cmdId;
	}

	public int Serialize(ByteStream bs) throws Exception {
		return 0;
	}

	public int UnSerialize(ByteStream bs) throws Exception {
		return 0;
	}

	public long getCmdId() {
		return this.cmdId;
	}
}