package com.qq.qqbuy.thirdparty.idl.boss;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.component.configcenter.api.set.SvcRoute;
import com.paipai.util.io.ByteStream;
import com.qq.qqbuy.common.util.Util;

/**
 * 从paipai-boss.jar里反编译
 * @author JingYing
 * @date 2014年12月28日
 */
public class BossWebStub2 {
	private static final int PKG_LENGTH_SIZE = 4;
	private static final int BUFFER_LEN = 1048576;
	private int timeoutMs = 3000;
	private String peerIp;
	private int peerPort;
	private boolean isPeerIpPortSet = false;
	private int bufferLen;
	private String srcName;
	private int setId = -1;
	private String setName;
	private long routeKey = 0L;

	public BossWebStub2() {
		this(0, null, -1);
	}

	public BossWebStub2(int setId) {
		this(0, null, setId);
	}

	public BossWebStub2(String srcName) {
		this(0, srcName, -1);
	}

	public BossWebStub2(String srcName, String setName, long routeKey) {
		this(0, srcName, setName, routeKey);
	}

	public BossWebStub2(String srcName, int setId) {
		this(0, srcName, setId);
	}

	public BossWebStub2(int bufferLen, String srcName) {
		this(0, srcName, -1);
	}

	public BossWebStub2(int bufferLen, String srcName, int setId) {
		this.srcName = srcName;
		this.bufferLen = (bufferLen > 0 ? bufferLen : 1048576);
		this.setId = setId;
	}

	public BossWebStub2(int bufferLen, String srcName, String setName,
			long routeKey) {
		this.srcName = srcName;
		this.bufferLen = (bufferLen > 0 ? bufferLen : 1048576);
		this.setName = setName;
		this.routeKey = routeKey;
	}

	public void setPeerIPPort(String ip, int port) {
		this.peerIp = ip;
		this.peerPort = port;
		this.isPeerIpPortSet = true;
	}

	public void resetPeerIPPort() {
		this.isPeerIpPortSet = false;
	}

	public void invoke(IBossServiceObject req, IBossServiceObject resp)
			throws Exception {
		byte[] pkgLengthBuffer = new byte[4];
		ByteStream pkgLengthBS = new ByteStream();
		pkgLengthBS.resetBuffer(pkgLengthBuffer, 4);

		byte[] pkgBuffer = new byte[this.bufferLen];
		ByteStream pkgBS = new ByteStream();
		pkgBS.resetBuffer(pkgBuffer, this.bufferLen);

		BossPkgHead2 bossPkgHead2 = new BossPkgHead2(0L, this.srcName);
		bossPkgHead2.setCmdId(req.getCmdId());
		bossPkgHead2.Serialize(pkgBS);

		req.Serialize(pkgBS);

		pkgLengthBS.pushUInt(pkgBS.getWrittenLength() + 4);

		sendAndRecv(req.getCmdDesc(), pkgLengthBuffer, pkgLengthBS, pkgBuffer,
				pkgBS);

		resp.UnSerialize(pkgBS);
	}

	public int invoke(String bossCmd, IServiceObject req) throws Exception {
		return invoke(bossCmd, req, new EmptyResponse(req.getCmdId()), 1);
	}

	public int invoke(String bossCmd, IServiceObject req, IServiceObject resp)
			throws Exception {
		return invoke(bossCmd, req, resp, 1);
	}

	public int invoke(String bossCmd, IServiceObject req, int version)
			throws Exception {
		return invoke(bossCmd, req, new EmptyResponse(req.getCmdId()), version);
	}

	public int invoke(String bossCmd, IServiceObject req, IServiceObject resp,
			int version) throws Exception {
		byte[] pkgLengthBuffer = new byte[4];
		ByteStream pkgLengthBS = new ByteStream();
		pkgLengthBS.resetBuffer(pkgLengthBuffer, 4);

		byte[] pkgBuffer = new byte[this.bufferLen];
		ByteStream pkgBS = new ByteStream();
		pkgBS.resetBuffer(pkgBuffer, this.bufferLen);

		String srcName = this.srcName;
		if (this.setName != null)
			srcName = this.srcName + "#" + bossCmd + "#" + this.setName + "#"
					+ this.routeKey;
		else {
			srcName = this.srcName + "#" + bossCmd + "##";
		}
		BossPkgHead2 bossPkgHead2 = new BossPkgHead2(version, srcName);
		bossPkgHead2.setCmdId(req.getCmdId());
		bossPkgHead2.Serialize(pkgBS);

		req.Serialize(pkgBS);

		pkgLengthBS.pushUInt(pkgBS.getWrittenLength() + 4);

		int size = sendAndRecv(bossCmd, pkgLengthBuffer, pkgLengthBS,
				pkgBuffer, pkgBS);

		bossPkgHead2.UnSerialize(pkgBS);
		if ((bossPkgHead2.getCmdId() & 0x7FFFFFFF) != req.getCmdId()) {
			throw new Exception("Error Response[sourceCmd=" + req.getCmdId()
					+ ",resultCmd=" + bossPkgHead2.getCmdId() + "]");
		}

		int ret = 0;
		long iRespVersion = bossPkgHead2.getSubCmdId();

		if ((req.getCmdId() & 0x10000000) != 0L) {
			ret = (int) iRespVersion;
		} else {
			if ((iRespVersion > 0L) && (size - pkgBS.getReadLength() >= 4)) {
				ret = (int) pkgBS.popUInt();
			}
			if ((pkgBS.getReadLength() < size) && (resp != null)) {
				resp.UnSerialize(pkgBS);
			}
		}
		return ret;
	}

	private int sendAndRecv(String bossCmd, byte[] pkgLengthBuffer,
			ByteStream pkgLengthBS, byte[] pkgBuffer, ByteStream pkgBS)
			throws Exception {
		Socket socket = null;
		BufferedOutputStream os = null;
		DataInputStream is = null;
		InetSocketAddress addr = null;
		try {
			socket = new Socket();
			socket.setSoTimeout(this.timeoutMs);
			addr = getRemoteAddress(bossCmd);
			socket.connect(addr, this.timeoutMs);
			os = new BufferedOutputStream(socket.getOutputStream());
			os.write(pkgLengthBuffer);
			os.write(pkgBuffer, 0, pkgBS.getWrittenLength());
			os.flush();

			is = new DataInputStream(socket.getInputStream());
			int read = is.read(pkgLengthBuffer, 0, 4);
			if (read < 4) {
				throw new Exception(
						"Read Pakcage Length Failed:Error Response Package Format");
			}
			pkgLengthBS.resetBuffer(pkgLengthBuffer, 4);
			int pkgLength = (int) pkgLengthBS.popUInt();

			int hasrecv = 0;
			int offset = 0;
			int haslen = pkgLength - 4;
			while (haslen > 0) {
				hasrecv = is.read(pkgBuffer, offset, haslen);
				haslen -= hasrecv;
				offset += hasrecv;
			}
			pkgBS.resetBuffer(pkgBuffer, pkgLength - 4);
			return pkgLength - 4;
		} catch (Exception e) {
			String errorMessage = null;
			if (addr == null)
				errorMessage = "Get Host Config Failed : " + bossCmd;
			else if (this.isPeerIpPortSet)
				errorMessage = "Connect Host Failed:"
						+ addr.getAddress().getHostAddress() + ":"
						+ addr.getPort();
			else {
				errorMessage = "Connect Host Failed:CmdDesc[" + bossCmd
						+ "]ipconfig[" + addr.getAddress().getHostAddress()
						+ ":" + addr.getPort() + "]";
			}
			throw new Exception(errorMessage, e);
		} finally {
			try {
				if (socket != null)
					socket.close();
				if (os != null)
					os.close();
				if (is != null)
					is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private InetSocketAddress getRemoteAddress(String bossCmd) {
		InetSocketAddress addr;
		if (!this.isPeerIpPortSet) {
			if (this.setId >= 0) {
				addr = SvcRoute.getSvcAddressBySet(bossCmd, this.setId, 0);
			} else {
				if (this.setName != null) {
					int setId = SvcRoute.GetSetIdBySvcAndIp(this.setName,
							Util.getLocalIp());
					if (setId < 0) {
						setId = 0;
					}
					addr = SvcRoute.getSvcAddressBySet(bossCmd, setId, 0);
				} else {
					addr = SvcRoute.getSvcAddressBySet(bossCmd, 0, 0);
				}
			}

			if (addr != null)
				setPeerIPPort(addr.getAddress().getHostAddress(),
						addr.getPort());
		} else {
			addr = new InetSocketAddress(this.peerIp, this.peerPort);
			this.isPeerIpPortSet = false;
		}
		return addr;
	}

	public int getTimeoutMs() {
		return this.timeoutMs;
	}

	public void setTimeoutMs(int timeoutMs) {
		this.timeoutMs = timeoutMs;
	}
}
