package com.qq.qqbuy.thirdparty.idl.boss;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;
import com.qq.qqbuy.common.util.Util;

public class BossPkgHead2
  implements IServiceObject
{
  private long cmdId;
  private long subCmdId = 2L;
  private long seqNo;
  private String ip;
  private String srcName;
  public static final int iPkgHeadLength = 12;

  public BossPkgHead2(long dwVersion, String srcName)
  {
    this.subCmdId = dwVersion;
    this.srcName = srcName;
    this.ip = Util.getLocalIp();
    this.seqNo = System.currentTimeMillis();
    if ((this.subCmdId > 0L) && (srcName == null))
      this.srcName = getSrcName();
  }

  public BossPkgHead2()
  {
    this(null);
  }

  public BossPkgHead2(String sSrcName) {
    this(1L, sSrcName);
  }

  public int Serialize(ByteStream bs)
    throws Exception
  {
    bs.pushUInt(this.cmdId);
    bs.pushUInt(this.subCmdId);
    if (this.subCmdId > 0L) {
      bs.pushUInt(this.seqNo);
      bs.pushString(this.ip);
      bs.pushString(this.srcName);
    }
    return bs.getWrittenLength();
  }

  public int UnSerialize(ByteStream bs)
    throws Exception
  {
    this.cmdId = bs.popUInt();
    this.subCmdId = bs.popUInt();
    if (this.subCmdId > 0L) {
      this.seqNo = bs.popUInt();
      this.ip = bs.popString();
      this.srcName = bs.popString();
    }

    return bs.getReadLength();
  }

  public long getCmdId()
  {
    return this.cmdId;
  }

  public long getSubCmdId() {
    return this.subCmdId;
  }

  public void setSubCmdId(long subCmdId) {
    this.subCmdId = subCmdId;
  }

  public void setCmdId(long cmdId) {
    this.cmdId = cmdId;
  }

  public static String getSrcName() {
    Exception e = new Exception();
    StackTraceElement[] traces = e.getStackTrace();
    String srcName = null;
    for (int i = traces.length - 1; i >= 0; i--) {
      srcName = getSrcName(traces[i]);
      if (srcName != null) {
        return srcName;
      }
    }

    return "boss";
  }

  public static String getSrcName(StackTraceElement trace) {
    String className = trace.getClassName();
    if (!className.startsWith("com.paipai.boss")) {
      return null;
    }
    int index = className.lastIndexOf('.');
    if (index > 0) {
      className = className.substring(index + 1, className.length());
    }

    return "java:" + className + "." + trace.getMethodName();
  }
}
