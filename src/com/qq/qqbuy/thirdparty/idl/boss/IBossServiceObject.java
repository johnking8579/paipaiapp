package com.qq.qqbuy.thirdparty.idl.boss;

import com.paipai.component.c2cplatform.IServiceObject;

public interface IBossServiceObject extends IServiceObject
{
  String getCmdDesc();

  long getSubCmdId();

  void setHasResult(boolean paramBoolean);
}
