package com.qq.qqbuy.thirdparty.http.search;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.qq.qqbuy.common.Pager;
import com.qq.qqbuy.common.client.http.HttpClient;
import com.qq.qqbuy.common.client.http.HttpResp;
import com.qq.qqbuy.common.client.http.ReqHead;
import com.qq.qqbuy.common.client.log.OnlyTimeLogParser;
import com.qq.qqbuy.common.exception.ExternalInterfaceException;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.thirdparty.http.search.po.SearchCondition;
import com.qq.qqbuy.thirdparty.http.search.po.SearchResult;
import com.qq.qqbuy.thirdparty.http.search.po.ShopItem;

/**
 * 商品搜索, 店内商品搜索
 */
public class SearchClient  {
	
	public SearchResult search(SearchCondition condition, long wid,int versionCode)  {
		return search4Filter(condition, wid, null,versionCode);
	}
	
	public SearchResult search4Filter(SearchCondition condition, long wid, String[] fields,int versionCode)  {
		String cookie = null ;
		if (wid != 0) 	cookie = "uin=" + wid;
		String charset = condition.getSysParam().getCharset();
		if(Util.isEmpty(charset))		charset = "gbk";
		
		try {
			String param = Util.paramMapToString(condition.toQueryMap());
			String searchRet = this.connectToSSE1(param, charset, cookie);
			SearchResult ret = SearchResultSerializer.parseResult(searchRet, condition.getSysParam().getCallback(), fields,versionCode);
			if (ret.getRetCode() != SearchResult.SUCCESS_RET_CODE) {
				throw new ExternalInterfaceException(0, ret.getRetCode(), ret.getMsg());
			}
			return ret;
		} catch (IOException e) {
			throw new ExternalInterfaceException(e);
		}
		
	}
	
	/**
	 * 搜索店内商品. 示例: http://sse1.paipai.com/comm_json?dtype=json&charset=utf-8&KeyWord=%E4%BA%94%E9%87%91&shop=329786348.49&pageNum=1&PageSize=2
	 * @param param
	 */
	public Pager<ShopItem> searchShopItem(ShopItemSearchParam param)	{
		String resp = null;
		try {
			resp = connectToSSE1(param.serialize(), "utf-8", null);
		} catch (IOException e) {
			throw new ExternalInterfaceException(e);
		}
		JsonObject json = null;
		try {
			json = new JsonParser().parse(resp).getAsJsonObject();
		} catch (Exception e) {
			throw new ExternalInterfaceException(e);
		}
		int retCode = json.get("retCode").getAsInt(); 
		if(retCode != 0)	{
			throw new ExternalInterfaceException(0, retCode, json.get("msg").getAsString());
		}
		
		JsonObject data = json.get("data").getAsJsonObject();
		List<ShopItem> items = new ArrayList<ShopItem>();
		for(JsonElement je : data.get("list").getAsJsonArray())	{
			JsonObject item = je.getAsJsonObject();
			items.add(ShopItem.parseFromJson(item));
		}
		return new Pager<ShopItem>(items, data.get("totalNum").getAsInt(), param.getPageNum(), param.getPageSize());
	}
	
	private String connectToSSE1(String param, String charset, String cookie) throws IOException{
		ReqHead reqHead = new ReqHead().setReferer("app.paipai.com").setCookie(cookie);
		HttpResp resp = new HttpClient().get("http://sse1.paipai.com/comm_json?" +  param, reqHead, new OnlyTimeLogParser());
		return new String(resp.getResponse(), charset);
	}
}
