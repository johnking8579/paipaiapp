package com.qq.qqbuy.thirdparty.http.search;

import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.search.util.SearchConstant;
import com.qq.qqbuy.thirdparty.http.search.po.SearchBizParams;
import com.qq.qqbuy.thirdparty.http.search.po.SearchCondition;
import com.qq.qqbuy.thirdparty.http.search.po.SearchSysParams;

/**
 * @author winsonwu
 * @Created 2012-5-29
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class SearchConditionFactory {
    
    public static SearchCondition createSearchCondition(String keyWord, String path, int pn, int ps, 
    		int orderStyle,long beginPrice,long endPrice,int em,int showQQvip,String addr,int freeMail ,int supportCod,int degree,int reightInsurance) {
    	SearchCondition condition = new SearchCondition();
		
		SearchSysParams sysParams = condition.getSysParam();
		sysParams.setCharset(SearchConstant.SEARCH_CHARSET_GBK);
		sysParams.setDtag("");
		sysParams.setDtype(SearchConstant.SEARCH_DTYPE_JSON);
		sysParams.setShowPath(true);
		sysParams.setCluster(true);

		SearchBizParams bizParams = condition.getBizParam();
		if (!StringUtil.isEmpty(keyWord)) {
			bizParams.setKeyword(keyWord);
		}
		bizParams.setPageSize(ps);
		bizParams.setPageNum(pn);
		bizParams.setOrderStyle(orderStyle);
		bizParams.setPath(path);
		bizParams.setBeginPrice(beginPrice);
		bizParams.setEndPrice(endPrice);
		bizParams.setAc(1);
		bizParams.setEarmark(em);
		bizParams.setAddr(addr);//地区
		/*if (VersionType.VERSION_IOS.equals(type) || showQQvip == 1) {
			bizParams.addProperty(SearchConstant.PROP_QQVIP);   // IOS版本只搜索QQ商城的商品
		}else{
			bizParams.addProperty(SearchConstant.PROP_PAIPAI_AND_WANGGOU);   // 其他版本只搜网购和拍拍的商品
		}*/
		bizParams.addProperty(SearchConstant.PROP_PAIPAI_AND_WANGGOU);
		if(freeMail == 1){
			bizParams.addProperty(SearchConstant.PROP_SHIP_FREE);//免邮
		}
		/**
		 * 搜索优化添加运费险
		 */
		if(reightInsurance == 1){
			bizParams.addProperty(SearchConstant.PROP_REIGHT_INSURANCE);//免邮
		}
		if(supportCod!=0){
    		condition.getBizParam().setPaytype(2);
    	}
		/**
		 * 搜索优化新增二手
		 */
		condition.getBizParam().setDegree(degree);
		return condition;
    }
    public static SearchCondition createSearchConditionForWap(String keyWord, String path, int pn, int ps, int orderStyle, int itemType,int searchType  ){
    	SearchCondition condition= createSearchCondition(keyWord,path,pn,ps,orderStyle,0,0,1,0,null,0,0,1,0);
    	if(searchType==1){
    		condition.getBizParam().addProperty(SearchConstant.PROP_QGO);//手拍搜索
    	}
    	if(itemType==1){
    		condition.getBizParam().addProperty(SearchConstant.PROP_COLORFUL_DIAMOND);//彩钻
    	}
    	if(itemType==2){
    		condition.getBizParam().addProperty(SearchConstant.PROP_RED_PACKET);//红包
    	}
		return condition;
    }
    /**
     * 搜索PC货到付款
     * @param keyWord
     * @param path
     * @param pn
     * @param ps
     * @param orderStyle
     * @param itemType
     * @param searchType
     * @param cod
     * @return
     * @date:2013-4-15
     * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
     * @version 1.0
     */
    public static SearchCondition createSearchCODForWap(String keyWord, String path, int pn, int ps, int orderStyle, int itemType,int searchType,int cod){
    	SearchCondition condition= createSearchConditionForWap(keyWord,path,pn,ps,orderStyle,itemType,searchType);
    	if(cod!=0){
    		condition.getBizParam().setPaytype(cod);
    	}
		return condition;
    }
 
}
