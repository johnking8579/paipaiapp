package com.qq.qqbuy.thirdparty.http.search.po;

import java.io.Serializable;

/**
 * 结果类别信息
 * @author winsonwu
 * @Created 2012-3-13
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class SearchResultClass implements Serializable {
    
	private static final long serialVersionUID = 1347939452406473239L;

	/**
     * 类别 ID
     */
    private String classId;

    /**
     * 类目数量
     */
    private String count;

	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

}
