package com.qq.qqbuy.thirdparty.http.search.po;

import java.io.Serializable;

/**
 * 结果聚类信息
 */
public class SearchResultCluster implements Serializable {

	private static final long serialVersionUID = -4202187905600779143L;

	/**
	 * 属性 ID
	 */
	private String attrId;

	/**
	 * 聚类 ID
	 */
	private String clusterId;

	/**
	 * 聚类名称
	 */
	private String clusterName;

	/**
	 * 聚路地址
	 */
	private String clusterUrl;

	/**
	 * 聚类数量
	 */
	private String clusterCount;

	public String getAttrId() {
		return attrId;
	}

	public void setAttrId(String attrId) {
		this.attrId = attrId;
	}

	public String getClusterId() {
		return clusterId;
	}

	public void setClusterId(String clusterId) {
		this.clusterId = clusterId;
	}

	public String getClusterName() {
		return clusterName;
	}

	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}

	public String getClusterUrl() {
		return clusterUrl;
	}

	public void setClusterUrl(String clusterUrl) {
		this.clusterUrl = clusterUrl;
	}

	public String getClusterCount() {
		return clusterCount;
	}

	public void setClusterCount(String clusterCount) {
		this.clusterCount = clusterCount;
	}

}