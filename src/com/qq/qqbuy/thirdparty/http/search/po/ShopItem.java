package com.qq.qqbuy.thirdparty.http.search.po;

import java.io.Serializable;
import java.util.Map.Entry;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * 从sse1.paipai.com的店内商品搜索结果中查出的商品列表
 * @author JingYing
 * @date 2014年12月9日
 */
public class ShopItem implements Serializable {

	/**
	 * 从json中解析
	 * @param json
	 * @return
	 */
	public static ShopItem parseFromJson(JsonObject json) {
		ShopItem s = new ShopItem();
		boolean b = false;
		for (Entry<String, JsonElement> e : json.entrySet()) {
			StringBuilder method = new StringBuilder("set")
					.append(e.getKey().substring(0, 1).toUpperCase());
			if (e.getKey().length() > 1) {
				method.append(e.getKey().substring(1, e.getKey().length()));
			}
			try {
				ShopItem.class.getMethod(method.toString(), String.class)
								.invoke(s, e.getValue().getAsString());
				b = true;
			} catch (Exception e1) {
				System.out.println(e1.getMessage());
			}
		}
		return b ? s : null;
	}

	private ShopItem() {	}

	private String img, img160, imgL, imgLL, title, titleH, link, shopUrl,
			commId, leafClassId, nickName, saleinfo, salePromotionInfo, spuid,
			qq, qQStatus, credit, star, commProperty, express, express2,
			mail_Price, express_Price, ems_Price, price, marketPrice,
			active_Price, discountInfo, addr, saleNum, comments, favNum,
			degree, saleType, officialStatus, userValidStatus, legendOneStatus,
			legendTwoStatus, legendThreeStatus, legendFourStatus,
			legendFiveStatus, cashDeliveryStatus, vouchersStatus,
			autoShipStatus, validClothStatus, propertyIDs, qQVipPrice,
			spromoteStoreStatus, spromoteBeginTime, spromoteEndTime,
			isImportComm, itemProperty;
	
	/**
	 * CGI接口中的价格单位是元,需要转成分
	 * @return
	 */
	public String getPrice() {
		try {
			return (Float.parseFloat(price) * 100) + "";
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return price;
		}
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getImg160() {
		return img160;
	}

	public void setImg160(String img160) {
		this.img160 = img160;
	}

	public String getImgL() {
		return imgL;
	}

	public void setImgL(String imgL) {
		this.imgL = imgL;
	}

	public String getImgLL() {
		return imgLL;
	}

	public void setImgLL(String imgLL) {
		this.imgLL = imgLL;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleH() {
		return titleH;
	}

	public void setTitleH(String titleH) {
		this.titleH = titleH;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getShopUrl() {
		return shopUrl;
	}

	public void setShopUrl(String shopUrl) {
		this.shopUrl = shopUrl;
	}

	public String getCommId() {
		return commId;
	}

	public void setCommId(String commId) {
		this.commId = commId;
	}

	public String getLeafClassId() {
		return leafClassId;
	}

	public void setLeafClassId(String leafClassId) {
		this.leafClassId = leafClassId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getSaleinfo() {
		return saleinfo;
	}

	public void setSaleinfo(String saleinfo) {
		this.saleinfo = saleinfo;
	}

	public String getSalePromotionInfo() {
		return salePromotionInfo;
	}

	public void setSalePromotionInfo(String salePromotionInfo) {
		this.salePromotionInfo = salePromotionInfo;
	}

	public String getSpuid() {
		return spuid;
	}

	public void setSpuid(String spuid) {
		this.spuid = spuid;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getQQStatus() {
		return qQStatus;
	}

	public void setQQStatus(String qQStatus) {
		this.qQStatus = qQStatus;
	}

	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

	public String getStar() {
		return star;
	}

	public void setStar(String star) {
		this.star = star;
	}

	public String getCommProperty() {
		return commProperty;
	}

	public void setCommProperty(String commProperty) {
		this.commProperty = commProperty;
	}

	public String getExpress() {
		return express;
	}

	public void setExpress(String express) {
		this.express = express;
	}

	public String getExpress2() {
		return express2;
	}

	public void setExpress2(String express2) {
		this.express2 = express2;
	}

	public String getMail_Price() {
		return mail_Price;
	}

	public void setMail_Price(String mail_Price) {
		this.mail_Price = mail_Price;
	}

	public String getExpress_Price() {
		return express_Price;
	}

	public void setExpress_Price(String express_Price) {
		this.express_Price = express_Price;
	}

	public String getEms_Price() {
		return ems_Price;
	}

	public void setEms_Price(String ems_Price) {
		this.ems_Price = ems_Price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(String marketPrice) {
		this.marketPrice = marketPrice;
	}

	public String getActive_Price() {
		return active_Price;
	}

	public void setActive_Price(String active_Price) {
		this.active_Price = active_Price;
	}

	public String getDiscountInfo() {
		return discountInfo;
	}

	public void setDiscountInfo(String discountInfo) {
		this.discountInfo = discountInfo;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getSaleNum() {
		return saleNum;
	}

	public void setSaleNum(String saleNum) {
		this.saleNum = saleNum;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getFavNum() {
		return favNum;
	}

	public void setFavNum(String favNum) {
		this.favNum = favNum;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String getSaleType() {
		return saleType;
	}

	public void setSaleType(String saleType) {
		this.saleType = saleType;
	}

	public String getOfficialStatus() {
		return officialStatus;
	}

	public void setOfficialStatus(String officialStatus) {
		this.officialStatus = officialStatus;
	}

	public String getUserValidStatus() {
		return userValidStatus;
	}

	public void setUserValidStatus(String userValidStatus) {
		this.userValidStatus = userValidStatus;
	}

	public String getLegendOneStatus() {
		return legendOneStatus;
	}

	public void setLegendOneStatus(String legendOneStatus) {
		this.legendOneStatus = legendOneStatus;
	}

	public String getLegendTwoStatus() {
		return legendTwoStatus;
	}

	public void setLegendTwoStatus(String legendTwoStatus) {
		this.legendTwoStatus = legendTwoStatus;
	}

	public String getLegendThreeStatus() {
		return legendThreeStatus;
	}

	public void setLegendThreeStatus(String legendThreeStatus) {
		this.legendThreeStatus = legendThreeStatus;
	}

	public String getLegendFourStatus() {
		return legendFourStatus;
	}

	public void setLegendFourStatus(String legendFourStatus) {
		this.legendFourStatus = legendFourStatus;
	}

	public String getLegendFiveStatus() {
		return legendFiveStatus;
	}

	public void setLegendFiveStatus(String legendFiveStatus) {
		this.legendFiveStatus = legendFiveStatus;
	}

	public String getCashDeliveryStatus() {
		return cashDeliveryStatus;
	}

	public void setCashDeliveryStatus(String cashDeliveryStatus) {
		this.cashDeliveryStatus = cashDeliveryStatus;
	}

	public String getVouchersStatus() {
		return vouchersStatus;
	}

	public void setVouchersStatus(String vouchersStatus) {
		this.vouchersStatus = vouchersStatus;
	}

	public String getAutoShipStatus() {
		return autoShipStatus;
	}

	public void setAutoShipStatus(String autoShipStatus) {
		this.autoShipStatus = autoShipStatus;
	}

	public String getValidClothStatus() {
		return validClothStatus;
	}

	public void setValidClothStatus(String validClothStatus) {
		this.validClothStatus = validClothStatus;
	}

	public String getPropertyIDs() {
		return propertyIDs;
	}

	public void setPropertyIDs(String propertyIDs) {
		this.propertyIDs = propertyIDs;
	}

	public String getQQVipPrice() {
		return qQVipPrice;
	}

	public void setQQVipPrice(String qQVipPrice) {
		this.qQVipPrice = qQVipPrice;
	}

	public String getSpromoteStoreStatus() {
		return spromoteStoreStatus;
	}

	public void setSpromoteStoreStatus(String spromoteStoreStatus) {
		this.spromoteStoreStatus = spromoteStoreStatus;
	}

	public String getSpromoteBeginTime() {
		return spromoteBeginTime;
	}

	public void setSpromoteBeginTime(String spromoteBeginTime) {
		this.spromoteBeginTime = spromoteBeginTime;
	}

	public String getSpromoteEndTime() {
		return spromoteEndTime;
	}

	public void setSpromoteEndTime(String spromoteEndTime) {
		this.spromoteEndTime = spromoteEndTime;
	}

	public String getIsImportComm() {
		return isImportComm;
	}

	public void setIsImportComm(String isImportComm) {
		this.isImportComm = isImportComm;
	}

	public String getItemProperty() {
		return itemProperty;
	}

	public void setItemProperty(String itemProperty) {
		this.itemProperty = itemProperty;
	}
}
