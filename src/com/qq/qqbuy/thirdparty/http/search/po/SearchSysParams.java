package com.qq.qqbuy.thirdparty.http.search.po;

import java.util.HashMap;
import java.util.Map;

import com.qq.qqbuy.common.util.StringUtil;

/**
 * Description: 搜索系统参数
 * Date: 11-10-31
 * Time: 下午2:41
 *
 * @author <a href="mailto:huangfengjing@gmail.com">Ivan</a>
 * @version 1.0
 */
public class SearchSysParams  {

	/**
     * 回调函数，当类型为 JSONP 时必选
     */
    private String callback;

    /**
     * 返回内容的字符集
     */
    private String charset = "gbk";

    /**
     * 返回类型，搜索默认为 JSONP，在本系统中设置默认为 JSON
     */
    private String dtype = "json";

    /**
     * 请求标识
     */
    private String dtag;
    
    /**
     * 是否输出品类信息
     */
    private boolean isShowClass = false;

    /**
     * 是否输出路径
     */
    private boolean isShowPath = true;

    /**
     * 是否输出聚类信息
     */
    private boolean isCluster = true;
    /**
     * 搜索来源
     */
    private int sf = 2003;
    
    private String qgo = System.currentTimeMillis()+"";

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public String getDtype() {
        return dtype;
    }

    public void setDtype(String dtype) {
        this.dtype = dtype;
    }

    public String getDtag() {
        return dtag;
    }

    public void setDtag(String dtag) {
        this.dtag = dtag;
    }

    public boolean isShowPath() {
        return isShowPath;
    }

    public void setShowPath(boolean showPath) {
        isShowPath = showPath;
    }

    public boolean isCluster() {
        return isCluster;
    }

    public void setCluster(boolean cluster) {
        isCluster = cluster;
    }
    
    public boolean isShowClass() {
		return isShowClass;
	}

	public void setShowClass(boolean isShowClass) {
		this.isShowClass = isShowClass;
	}

	public Map<String, String> validate() {
		Map<String, String> result = new HashMap<String, String>();
		return result;
	}
    
    public Map<String, String> toQueryMap() {
    	Map<String, String> map = new HashMap<String, String>();
		map.put("sf", ""+sf);
		map.put("qgo", qgo);
    	if (!StringUtil.isEmpty(callback)) {
    		map.put("callback", callback);
    	}
    	if (!StringUtil.isEmpty(charset)) {
    		map.put("charset",	charset);
    	}
    	if (!StringUtil.isEmpty(dtype)) {
    		map.put("dtype", dtype); 
    	}
    	if (!StringUtil.isEmpty(dtag)) { 
    		map.put("dtag", dtag);
    	}
    	if (!isShowClass) {
    		map.put("ShowClass", "1");
    	}
    	if (isCluster) {
    		map.put("cluster", "1");
    	}
    	return map;
    }
}
