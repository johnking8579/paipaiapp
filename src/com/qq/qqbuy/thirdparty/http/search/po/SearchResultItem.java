package com.qq.qqbuy.thirdparty.http.search.po;

import java.io.Serializable;

/**
 * 返回的商品
 * 
 * @author winsonwu
 * @Created 2012-3-13 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class SearchResultItem implements Serializable {

	private static final long serialVersionUID = -1896331614181549629L;

	/**
	 * 商品图片链接80 x 80
	 */
	private String img;
	
	/**
	 * 商品图片链接160 x 160
	 */
	private String img160;

	/**
	 * 商品大图链接200 x 200
	 */
	private String imgL;

	/**
	 * 商品超大图链接300 x 300
	 */
	private String imgLL;

	/**
	 * 商品标题
	 */
	private String title;

	private String titleH;
	
	/**
	 * 商品详情链接（拍拍WEB端链接，移动电商中不要直接使用，可用于验证）
	 */
	private String link;

	/**
	 * 商品ID
	 */
	private String commId;
	
	private String leafClassId;
	
	/**
	 * 卖家昵称(店铺名)
	 */
	private String nickName;
	
	private String saleInfo;
	
	private String salePromotionInfo;

	private String spuId;
	
	/**
	 * 卖家QQ号
	 */
	private String qq;
	
	private String qqStatus;
	
	/**
	 * 卖家信用值
	 */
	private long credit;
	
	private String star;
	
	private String commProperty;
	
	/**
	 * 邮递费用，注意可能为文字“免邮费”或者以元为单位的邮递费用值，如“10.00”（已处理为2位小数）
	 * express与express2字段详细含义如下: 1) 如果是免运费：express为"免运费"，express2无意义
	 * 2) 如果express和express2都有价格数据：express表示平邮价格 express2表示快递价格
	 * 3) 如果express有价格数据，express2为空串，则express表示快递价格
	 */
	private String express;

	/**
	 * 邮递费用2.请参见express
	 */
	private String express2;
	
	private String mailPrice;
	
	private String expressPrice;
	
	private String emsPrice;

	/**
	 * 商品价格，以元为单位，如“22.00”（已处理为2位小数）
	 */
	private String price;
	
	private String marketPrice;
	
	private String activePrice;
	
	/**
	 * 店铺所在地
	 */
	private String addr;

	/**
	 * 已售件数
	 */
	private long saleNum;

	/**
	 * 人气
	 */
	private long favNum;
	
	private String degree;
	
	private String saleType;
	
	private String officialStatus;
	
	/**
	 * 商家认证状态
	 */
	private String userValidStatus;

	/**
	 * 先行赔付状态
	 */
	private String legend1Status;

	/**
	 * 7天退货状态
	 */
	private String legend2Status;

	/**
	 * 诚保代充状态
	 */
	private String legend3Status;

	/**
	 * 假一赔三状态
	 */
	private String legend4Status;
	
	private String legend5Status;

	/**
	 * 货到付款状态
	 */
	private String cashDeliveryStatus;

	private String vouchersStatus;

	/**
	 * 自动发货状态
	 */
	private String autoShipStatus;

	/**
	 * 裳品廊状态
	 */
	private String validClothStatus;
	
	/**
	 * 商品属性串，如"6e20:b|6eba:800|6e17:6|6ec0:200000"
	 */
	private String propertyIds;

	/**
	 * QQ会员价格,以元为单位，已处理为两位小数的格式，如“12.00”
	 */
	private String qqVipPrice;
	
	private String spromoteStoreStatus;
	
	private String spromoteBeginTime;
	
	private String spromoteEndTime;
	
	private String isImportComm;

	/**
	 * 搜索优化新增商品活动标识
	 */
	private long activePriceType;

	/**
	 * 搜索优化新增拍便宜商品开团链接
	 */
	private String takeCheapLink;

	/**
	 * 搜索优化新增已浏览和已下单个性化数据
	 * 0、普通商品，1、已购买过的商品，2、已浏览过的商品
	 * @return
	 */
	private int personalizedTypeTag;

	/**
	 * 拍便宜商品标识
	 */
	private int istakeCheap = 0;

	/**
	 * 闪购商品标识
	 */
	private int isFlashSale = 0;

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getImg160() {
		return img160;
	}

	public void setImg160(String img160) {
		this.img160 = img160;
	}

	public String getImgL() {
		return imgL;
	}

	public void setImgL(String imgL) {
		this.imgL = imgL;
	}

	public String getImgLL() {
		return imgLL;
	}

	public void setImgLL(String imgLL) {
		this.imgLL = imgLL;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleH() {
		return titleH;
	}

	public void setTitleH(String titleH) {
		this.titleH = titleH;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getCommId() {
		return commId;
	}

	public void setCommId(String commId) {
		this.commId = commId;
	}

	public String getLeafClassId() {
		return leafClassId;
	}

	public void setLeafClassId(String leafClassId) {
		this.leafClassId = leafClassId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getSaleInfo() {
		return saleInfo;
	}

	public void setSaleInfo(String saleInfo) {
		this.saleInfo = saleInfo;
	}

	public String getSalePromotionInfo() {
		return salePromotionInfo;
	}

	public void setSalePromotionInfo(String salePromotionInfo) {
		this.salePromotionInfo = salePromotionInfo;
	}

	public String getSpuId() {
		return spuId;
	}

	public void setSpuId(String spuId) {
		this.spuId = spuId;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getQqStatus() {
		return qqStatus;
	}

	public void setQqStatus(String qqStatus) {
		this.qqStatus = qqStatus;
	}

	public long getCredit() {
		return credit;
	}

	public void setCredit(long credit) {
		this.credit = credit;
	}

	public String getStar() {
		return star;
	}

	public void setStar(String star) {
		this.star = star;
	}

	public String getCommProperty() {
		return commProperty;
	}

	public void setCommProperty(String commProperty) {
		this.commProperty = commProperty;
	}

	public String getExpress() {
		return express;
	}

	public void setExpress(String express) {
		this.express = express;
	}

	public String getExpress2() {
		return express2;
	}

	public void setExpress2(String express2) {
		this.express2 = express2;
	}

	public String getMailPrice() {
		return mailPrice;
	}

	public void setMailPrice(String mailPrice) {
		this.mailPrice = mailPrice;
	}

	public String getExpressPrice() {
		return expressPrice;
	}

	public void setExpressPrice(String expressPrice) {
		this.expressPrice = expressPrice;
	}

	public String getEmsPrice() {
		return emsPrice;
	}

	public void setEmsPrice(String emsPrice) {
		this.emsPrice = emsPrice;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(String marketPrice) {
		this.marketPrice = marketPrice;
	}

	public String getActivePrice() {
		return activePrice;
	}

	public void setActivePrice(String activePrice) {
		this.activePrice = activePrice;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public long getSaleNum() {
		return saleNum;
	}

	public void setSaleNum(long saleNum) {
		this.saleNum = saleNum;
	}

	public long getFavNum() {
		return favNum;
	}

	public void setFavNum(long favNum) {
		this.favNum = favNum;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String getSaleType() {
		return saleType;
	}

	public void setSaleType(String saleType) {
		this.saleType = saleType;
	}

	public String getOfficialStatus() {
		return officialStatus;
	}

	public void setOfficialStatus(String officialStatus) {
		this.officialStatus = officialStatus;
	}

	public String getUserValidStatus() {
		return userValidStatus;
	}

	public void setUserValidStatus(String userValidStatus) {
		this.userValidStatus = userValidStatus;
	}

	public String getLegend1Status() {
		return legend1Status;
	}

	public void setLegend1Status(String legend1Status) {
		this.legend1Status = legend1Status;
	}

	public String getLegend2Status() {
		return legend2Status;
	}

	public void setLegend2Status(String legend2Status) {
		this.legend2Status = legend2Status;
	}

	public String getLegend3Status() {
		return legend3Status;
	}

	public void setLegend3Status(String legend3Status) {
		this.legend3Status = legend3Status;
	}

	public String getLegend4Status() {
		return legend4Status;
	}

	public void setLegend4Status(String legend4Status) {
		this.legend4Status = legend4Status;
	}

	public String getLegend5Status() {
		return legend5Status;
	}

	public void setLegend5Status(String legend5Status) {
		this.legend5Status = legend5Status;
	}

	public String getCashDeliveryStatus() {
		return cashDeliveryStatus;
	}

	public void setCashDeliveryStatus(String cashDeliveryStatus) {
		this.cashDeliveryStatus = cashDeliveryStatus;
	}

	public String getVouchersStatus() {
		return vouchersStatus;
	}

	public void setVouchersStatus(String vouchersStatus) {
		this.vouchersStatus = vouchersStatus;
	}

	public String getAutoShipStatus() {
		return autoShipStatus;
	}

	public void setAutoShipStatus(String autoShipStatus) {
		this.autoShipStatus = autoShipStatus;
	}

	public String getValidClothStatus() {
		return validClothStatus;
	}

	public void setValidClothStatus(String validClothStatus) {
		this.validClothStatus = validClothStatus;
	}

	public String getPropertyIds() {
		return propertyIds;
	}

	public void setPropertyIds(String propertyIds) {
		this.propertyIds = propertyIds;
	}

	public String getQqVipPrice() {
		return qqVipPrice;
	}

	public void setQqVipPrice(String qqVipPrice) {
		this.qqVipPrice = qqVipPrice;
	}

	public String getSpromoteStoreStatus() {
		return spromoteStoreStatus;
	}

	public void setSpromoteStoreStatus(String spromoteStoreStatus) {
		this.spromoteStoreStatus = spromoteStoreStatus;
	}

	public String getSpromoteBeginTime() {
		return spromoteBeginTime;
	}

	public void setSpromoteBeginTime(String spromoteBeginTime) {
		this.spromoteBeginTime = spromoteBeginTime;
	}

	public String getSpromoteEndTime() {
		return spromoteEndTime;
	}

	public void setSpromoteEndTime(String spromoteEndTime) {
		this.spromoteEndTime = spromoteEndTime;
	}

	public String getIsImportComm() {
		return isImportComm;
	}

	public void setIsImportComm(String isImportComm) {
		this.isImportComm = isImportComm;
	}

	public String getTakeCheapLink() {
		return takeCheapLink;
	}

	public void setTakeCheapLink(String takeCheapLink) {
		this.takeCheapLink = takeCheapLink;
	}

	public long getActivePriceType() {
		return activePriceType;
	}

	public void setActivePriceType(long activePriceType) {
		this.activePriceType = activePriceType;
	}

	public int getPersonalizedTypeTag() {
		return personalizedTypeTag;
	}

	public void setPersonalizedTypeTag(int personalizedTypeTag) {
		this.personalizedTypeTag = personalizedTypeTag;
	}

	public int getIstakeCheap() {
		return istakeCheap;
	}

	public void setIstakeCheap(int istakeCheap) {
		this.istakeCheap = istakeCheap;
	}

	public int getIsFlashSale() {
		return isFlashSale;
	}

	public void setIsFlashSale(int isFlashSale) {
		this.isFlashSale = isFlashSale;
	}
}
