package com.qq.qqbuy.thirdparty.http.search.po;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import com.qq.qqbuy.common.util.CoderUtil;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.search.util.SearchChecker;

/**
 * 
 * @author winsonwu
 * 
 */
public class SearchBizParams {

	/**
	 * 搜索关键字
	 */
	private String keyword;

	/**
	 * 搜索路径
	 */
	private String path;

	/**
	 * 当前页码
	 */
	private int pageNum = 1;

	/**
	 * 分页大小
	 */
	private int pageSize = 5;

	/**
	 * 当前价低价
	 */
	private float beginPrice;

	/**
	 * 当前价高价
	 */
	private float endPrice;

	/**
	 * 排序方式
	 */
	private int orderStyle;
	/**
	 * 商品属性过滤条件
	 */
	private long Property;
	 /**
     * AC标识. 此值为空或者为0，代表此链接为系统默认生成链接，无用户点击行为，
     * 此时页面展示的逻辑某些可能会通过读取后台默认配置来展示，比如说女装类目
     * 下默认是大图片展示的，这个默认值是后台配置的，只有此位为0时才生效，
     * 如果此位值为1，则默认配置不生效
     */
	private int ac;
	/**
	 * 1款到发货, 2货到付款, 4支持财富通【有此参数， 则以此参数为准；没有则用下面三个的和】
	 */
	private int Paytype;
	/**
	 * 是否需要em标签，标红参数 为0,则不对标题标红, 其他进行标红
	 */
	private int earmark;
	/**
	 * 卖家所在地
	 */
	private String  addr;

	/**
	 * 新旧程度，1全新,2二手, 4陈列品
	 */
	private int degree;

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public float getBeginPrice() {
		return beginPrice;
	}

	public void setBeginPrice(float beginPrice) {
		this.beginPrice = beginPrice;
	}

	public float getEndPrice() {
		return endPrice;
	}

	public void setEndPrice(float endPrice) {
		this.endPrice = endPrice;
	}

	public int getOrderStyle() {
		return orderStyle;
	}

	public void setOrderStyle(int orderStyle) {
		this.orderStyle = orderStyle;
	}

	public Map<String, String> validate() {
		Map<String, String> result = new HashMap<String, String>();
		if (StringUtil.isBlank(keyword) && StringUtil.isBlank(path)) {
			result.put("required", "keyword, path参数至少提供一个");
		}
		return result;
	}

	public Map<String, String> toQueryMap() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("ac", ""+ac);//有此参数才支持排序
		map.put("earmark", ""+earmark);
		// 搜索后台为gbk,需要转码
		if (!StringUtil.isEmpty(keyword)) {
			map.put("KeyWord", escapeAndEncodeKeyword(keyword));
		}
		if (!StringUtil.isEmpty(path)) {
			map.put("Path", path);
		}
		if (pageNum >= 1) {
			map.put("PageNum", "" + pageNum);
		}
		if (pageSize > 0) {
			map.put("PageSize", "" + pageSize);
		}
		if (SearchChecker.checkOrderStyle(orderStyle)) {
			map.put("OrderStyle", "" + orderStyle);
		}
		// appendParam(sb, "sClassid", this.curClassId);
		// appendParam(sb, "SaleType", this.sellMode);
		// appendParam(sb, "EndTime", this.endTime);
		if (beginPrice > 0) {
			map.put("BeginPrice", "" + beginPrice);
		}
		if (endPrice > 0) {
			map.put("EndPrice", "" + endPrice);
		}
		if (endPrice > 0) {
			map.put("EndPrice", "" + endPrice);
		}
		if(Property>0){
			map.put("Property", "" + Property);
		}
		if(Paytype>0){
			map.put("Paytype", "" + Paytype);
		}
		if(StringUtil.isNotEmpty(addr)){
			try {
				map.put("Address", URLEncoder.encode(addr, "gbk"));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(degree>0){
			map.put("degree", "" + degree);
		}
		// 处理PayType
		// if (this.payType >= 0) {
		// if ((this.payType & CommoditySearchConst.PAY_TYPE_DOC) ==
		// CommoditySearchConst.PAY_TYPE_DOC) {
		// appendParam(sb, "PayType1", 1);
		// }
		//            
		// if ((this.payType & CommoditySearchConst.PAY_TYPE_COD) ==
		// CommoditySearchConst.PAY_TYPE_COD) {
		// appendParam(sb, "PayType2", 1);
		// }
		//            
		// if ((this.payType & CommoditySearchConst.PAY_TYPE_CFT) ==
		// CommoditySearchConst.PAY_TYPE_CFT) {
		// appendParam(sb, "PayType3", 1);
		// }
		// }

		// 处理新旧程度
		// if (this.newOrOld >= 0) {
		// if ((this.newOrOld & CommoditySearchConst.NEWOLD_NEW) ==
		// CommoditySearchConst.NEWOLD_NEW) {
		// appendParam(sb, "Degree1", 1);
		// }
		//            
		// if ((this.newOrOld & CommoditySearchConst.NEWOLD_OLD) ==
		// CommoditySearchConst.NEWOLD_OLD) {
		// appendParam(sb, "Degree2", 1);
		// }
		// }

		return map;
	}
	 public void addProperty(long property) {
	    	if(this.Property > 0) {
	    		this.Property = this.Property | property;
	    	} else {
	    		this.Property = property;
	    	}    	
	    }
	public long getProperty() {
		return Property;
	}

	public void setProperty(long property) {
		Property = property;
	}

	public int getAc() {
		return ac;
	}

	public void setAc(int ac) {
		this.ac = ac;
	}

	public int getPaytype() {
		return Paytype;
	}

	public void setPaytype(int paytype) {
		Paytype = paytype;
	}

	public int getEarmark() {
		return earmark;
	}

	public void setEarmark(int earmark) {
		this.earmark = earmark;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}
	
	private String escapeAndEncodeKeyword(String keyword) {
		if (StringUtil.isBlank(keyword)) {
			return keyword;
		}
		keyword = StringUtil.escapeForSearch(keyword);
		try {
			return URLEncoder.encode(keyword, "gbk").replaceAll("\\+", "%20");
		} catch (UnsupportedEncodingException e) {
			// ignore
		}
		return keyword;
	}

	public int getDegree() {
		return degree;
	}

	public void setDegree(int degree) {
		this.degree = degree;
	}
}
