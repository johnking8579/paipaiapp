package com.qq.qqbuy.thirdparty.http.search.po;

import java.io.Serializable;

/**
 * 结果路径信息
 * @author winsonwu
 * @Created 2012-3-13
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class SearchResultPath implements Serializable {

	private static final long serialVersionUID = 4149160944583230780L;

	/**
     * 属性 ID
     */
    private String attrId;

    /**
     * 属性名称
     */
    private String attrName;

    /**
     * 路径 ID
     */
    private String itemId;

    /**
     * 路径名称
     */
    private String itemName;

    /**
     * 路径地址
     */
    private String pathUrl;

	public String getAttrId() {
		return attrId;
	}

	public void setAttrId(String attrId) {
		this.attrId = attrId;
	}

	public String getAttrName() {
		return attrName;
	}

	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getPathUrl() {
		return pathUrl;
	}

	public void setPathUrl(String pathUrl) {
		this.pathUrl = pathUrl;
	}
    
}
