package com.qq.qqbuy.thirdparty.http.search;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import com.google.gson.*;
import com.qq.qqbuy.common.util.HttpUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.thirdparty.http.search.po.SearchResult;
import com.qq.qqbuy.thirdparty.http.search.po.SearchResultClass;
import com.qq.qqbuy.thirdparty.http.search.po.SearchResultCluster;
import com.qq.qqbuy.thirdparty.http.search.po.SearchResultItem;
import com.qq.qqbuy.thirdparty.http.search.po.SearchResultPath;
import com.qq.qqbuy.thirdparty.http.search.po.SearchResultPriceRange;

/**
 * 搜索结果的序列化和反序列化
 * @author winsonwu
 * @Created 2012-3-13
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class SearchResultSerializer   {
	
	// 结果集字段：热门分类
    private static final String RESULT_FIELD_HOTCLASS = "hotclass";
    // 结果集字段：分类
    private static final String RESULT_FIELD_CLASS = "class";
    // 结果集字段：路径
    private static final String RESULT_FIELD_PATH = "Path";
    // 结果集字段：商品属性
    private static final String RESULT_FIELD_PROPERTY = "Property";
    // 结果集字段：价格区间
    private static final String RESULT_FIELD_PRICERANGE = "PriceRange";
    // 结果集字段：商品列表
    private static final String RESULT_FIELD_LIST = "list";
	// 结果集字段：优质店铺id
	private static final String FLAG_SHIP_SHOPID = "FlagshipShopId";

	/**
	 * 拍便宜H5开团链接
	 */
	private static final String TAKECHEAP_TUAN_DETAIL_H5_URL = "www.paipai.com/m2/ttj/tuan_detail.html";

	/**
	 * 拍便宜APP开团链接
	 */
	private static final String TAKECHEAP_TUAN_DETAIL_APP_URL = "app.paipai.com/takeCheap/tuan_detail.html";

	/**
	 * 拍便宜APP开团新链接
	 */
	private static final String TAKECHEAP_TUAN_NEW_APP_URL = "app.paipai.com/takeCheapTest/tuan_detail.html";

	/**
     * 根据搜索返回的字符串构造一个 SearchResult，当含有 callback 时该方法依赖于搜索返回字符串的格式。
     * JSON CONTENT前面字符应该是 "callback({", 结尾紧接着字符 "{)"（空白字符除外）,如下：
     * try{
     * callback({
     * JSON CONTENT
     * });
     * }catch(e){}
     *
     * @param searchReturnStr 搜索返回的字符串
     * @param callback        callback名称，JSONP 返回类型含有 callback，如果没有则为 null
     * @param fields          需要解析的字段，如果需要所有字段，则传递 null
     * @return 构造的 SearchResult
     */
	public static SearchResult parseResult(String searchReturnStr, String callback, String[] fields,int versionCode) throws BusinessException {
        int end;
        if (StringUtil.isNotBlank(callback)) {
            int start;
            if ((start = searchReturnStr.indexOf(callback)) > 0) {
                searchReturnStr = searchReturnStr.substring(start + callback.length());
            }

            if ((start = searchReturnStr.indexOf("{")) > 0) {
                searchReturnStr = searchReturnStr.substring(start);
            }

            end = searchReturnStr.lastIndexOf(");");
        } else {
            end = searchReturnStr.lastIndexOf("}") + 1;
        }
        searchReturnStr = searchReturnStr.substring(0, end);

        try {
        	JSONObject jobj = JSONObject.fromObject(searchReturnStr.trim());
            return parseResult(jobj, fields,versionCode);
        } catch (BusinessException e) {
        	throw e;
    	} catch (Exception e) {
        	Log.run.error("SearchResultSerializer#parseResult fail with translate search result to json object!", e);
            throw BusinessException.createInstance(ErrConstant.ERRCODE_SEARCH_PARSE_RESULT_FAILED, "搜索结果转化成Json对象时候出错");
        }
    }

    /**
     * 根据一个 JSON 对象构造 SearchResult
     *
     * @param json   JSON 对象
     * @param fields 需要解析的字段，如果需要所有字段
     * @return 构造的 SearchResult
     */
    protected static SearchResult parseResult(JSONObject json, String[] fields,int versionCode) throws BusinessException {
        SearchResult result = new SearchResult();
        result.setRetCode(SearchResult.ERR_RET_CODE);
        try {
            // 解析请求状态信息
            int retCode = getInt(json, "retCode");
            result.setRetCode(retCode);
            result.setMsg(getString(json, "msg"));
            result.setDtag(getString(json, "dtag"));
			//获取优质店铺json信息
			String shopList = parseShipShop(getString(json,FLAG_SHIP_SHOPID));
			result.setShopList(shopList);
            if (retCode != SearchResult.SUCCESS_RET_CODE) {
                return result;
            }
            JSONObject data = getJSONObject(json, "data");
            if (data != null) {
            	result.setTotalNum(getInt(data, "totalNum"));

	            Set<String> fieldNames = new HashSet<String>();
	            if (null == fields) {
	                fieldNames.add(RESULT_FIELD_HOTCLASS);
	                fieldNames.add(RESULT_FIELD_CLASS);
	                fieldNames.add(RESULT_FIELD_PATH);
	                fieldNames.add(RESULT_FIELD_PROPERTY);
	                fieldNames.add(RESULT_FIELD_PRICERANGE);
	                fieldNames.add(RESULT_FIELD_LIST);
	            } else {
	                Collections.addAll(fieldNames, fields);
	            }
				// 解析 hot class 信息
				LinkedHashMap<String, List<SearchResultCluster>> hotClasses = parseHotClass(data, fieldNames);
				result.setHotClasses(hotClasses);

				// 解析 CLASS 信息
				List<SearchResultClass> classes = parseClass(data, fieldNames);
				result.setClasses(classes);

				// 解析 PATH
				List<SearchResultPath> paths = parsePath(data, fieldNames);
				result.setResultPaths(paths);

				// 解析 属性
				LinkedHashMap<String, List<SearchResultCluster>> propertyClusters = parseProperty(data, fieldNames);
				result.setPropertyClusters(propertyClusters);

				// 解析 PRICE RANGE
				List<SearchResultPriceRange> priceRanges = parsePriceRange(data, fieldNames);
				result.setResultPriceRanges(priceRanges);

				// 解析 ITEM
				List<SearchResultItem> items = parseItem(data, fieldNames,versionCode);
				result.setResultItems(items);
            }

            return result;
        } catch (Exception e) {
        	Log.run.error("SearchResultSerializer#parseResult fail in parsing from json!", e);
            throw BusinessException.createInstance(ErrConstant.ERRCODE_SEARCH_PARSE_RESULT_FAILED, "从搜索结果的Json数据中解析失败");
        }
    }

	/**
	 * 根据店铺ID搜索优质店铺信息
	 * @param shopId
	 * @return
	 */
	private static String parseShipShop(String shopId) {
		String shopList = "[]";
		if (StringUtil.isEmpty(shopId)){
			return shopList;
		}
		StringBuffer url = new StringBuffer();
		try {
			url.append("http://ss.paipai.com/json/search?")
					.append("KeyWord=").append(URLDecoder.decode(shopId, "utf-8"));
		} catch (UnsupportedEncodingException e) {
			Log.run.error("parseShipShop error :"+e.getMessage());
			return shopList;
		}
		String result = "";
		try {
			result = HttpUtil.getWithProxy(url.toString(), null, 10000, 15000, "gbk", true, false, null, "10.6.223.129", 80);
//			result = HttpUtil.get(url.toString(), 10000, 15000, "gbk", false);
		} catch (Exception e) {
			Log.run.error("parseShipShop interface error:"+e.getMessage());
			return shopList;
		}
		if (StringUtil.isEmpty(result)){
			return shopList;
		}
		try {
			JsonParser jsonParser = new JsonParser();
			String replaceAll = result.replaceAll("\"", "\\\"").replaceAll("\n", "").replaceAll("\r", "").replaceAll("<[^>]+>", "");
			JsonElement jsonElement = jsonParser.parse(replaceAll);
			JsonElement data = jsonElement.getAsJsonObject().get("data");
			JsonArray jsonArray = data.getAsJsonObject().get("shopList").getAsJsonArray();
			Gson gson = new Gson();
			shopList = gson.toJson(jsonArray);
		} catch (JsonSyntaxException e) {
			Log.run.error("parseShipShop json error:"+e.getMessage());
			return shopList;
		}
		return shopList;
	}

	private static List<SearchResultItem> parseItem(JSONObject dataObj, Set<String> fieldNames,int versionCode) {
    	List<SearchResultItem> ret = new ArrayList<SearchResultItem>();
    	if (dataObj != null && fieldNames.contains(RESULT_FIELD_LIST) && dataObj.has(RESULT_FIELD_LIST)) {
            JSONArray itemListObj = getJSONArray(dataObj, RESULT_FIELD_LIST);
            if (itemListObj != null && itemListObj.size() > 0) {
	            for (int i = 0; i < itemListObj.size(); i++) {
	            	SearchResultItem item = new SearchResultItem();
	                JSONObject itemObj = itemListObj.getJSONObject(i);
	                
	                item.setImg(getString(itemObj, "img"));
	                item.setImg160(getString(itemObj, "img160"));
	                item.setImgL(getString(itemObj, "imgL"));
	                item.setImgLL(getString(itemObj, "imgLL"));
	                item.setTitle(getString(itemObj, "title"));
	                item.setTitleH(getString(itemObj, "titleH"));
	                item.setLink(getString(itemObj, "link"));
	                item.setCommId(getString(itemObj, "commId"));
	                item.setLeafClassId(getString(itemObj, "LeafClassId"));
	                item.setNickName(getString(itemObj, "nickName"));
	                item.setSaleInfo(getString(itemObj, "saleinfo"));
	                item.setSalePromotionInfo(getString(itemObj, "salePromotionInfo"));
	                item.setSpuId(getString(itemObj, "Spuid"));
	                item.setQq(getString(itemObj, "qq"));
	                item.setQqStatus(getString(itemObj, "QQStatus"));
	                item.setCredit(getLong(itemObj, "credit"));
	                item.setStar(getString(itemObj, "star"));
	                item.setCommProperty(getString(itemObj, "CommProperty"));
	                item.setExpress(getString(itemObj, "express"));
	                item.setExpress2(getString(itemObj, "express2"));
	                item.setMailPrice(getString(itemObj, "Mail_Price"));
	                item.setExpressPrice(getString(itemObj, "Express_Price"));
	                item.setEmsPrice(getString(itemObj, "Ems_Price"));
	                item.setPrice(getString(itemObj, "price"));
	                item.setMarketPrice(getString(itemObj, "MarketPrice"));
	                item.setActivePrice(getString(itemObj, "Active_Price"));
	                item.setAddr(getString(itemObj, "addr"));
	                item.setSaleNum(getLong(itemObj, "saleNum"));
	                item.setFavNum(getLong(itemObj, "favNum"));
	                item.setDegree(getString(itemObj, "degree"));
	                item.setSaleType(getString(itemObj, "SaleType"));
	                item.setOfficialStatus(getString(itemObj, "OfficialStatus"));
	                item.setUserValidStatus(getString(itemObj, "UserValidStatus"));
	                item.setLegend1Status(getString(itemObj, "LegendOneStatus"));
	                item.setLegend2Status(getString(itemObj, "LegendTwoStatus"));
	                item.setLegend3Status(getString(itemObj, "LegendThreeStatus"));
	                item.setLegend4Status(getString(itemObj, "LegendFourStatus"));
	                item.setLegend5Status(getString(itemObj, "LegendFiveStatus"));
	                item.setCashDeliveryStatus(getString(itemObj, "CashDeliveryStatus"));
	                item.setVouchersStatus(getString(itemObj, "VouchersStatus"));
	                item.setAutoShipStatus(getString(itemObj, "AutoShipStatus"));
	                item.setValidClothStatus(getString(itemObj, "ValidClothStatus"));
	                item.setPropertyIds(getString(itemObj, "PropertyIDs"));
	                item.setQqVipPrice(getString(itemObj, "QQVipPrice"));
	                item.setSpromoteStoreStatus(getString(itemObj, "SpromoteStoreStatus"));
	                item.setSpromoteBeginTime(getString(itemObj, "SpromoteBeginTime"));
	                item.setSpromoteEndTime(getString(itemObj, "SpromoteEndTime"));
	                item.setIsImportComm(getString(itemObj, "IsImportComm"));
					/**
					 * 搜索优化新增商品活动标识
					 */
					long price_type = getLong(itemObj, "Active_Price_Type");
					if(price_type == 43 || price_type == 44){
						price_type = 0;
					}else if(price_type == 1){//拍便宜
						price_type = 44;
						item.setIstakeCheap(1);
					}else if(price_type == 3){//闪购
						price_type = 43;
						item.setIsFlashSale(1);
					}
					item.setActivePriceType(price_type);
					/**
					 * 搜索优化新增拍便宜商品开团链接
					 */
					String takeCheapLink = "";
					String paiPianYiLink = getString(itemObj, "paiPianYiLink");
					Log.run.info("paiPianYiLink is:"+paiPianYiLink);
					if (StringUtil.isNotBlank(paiPianYiLink) && paiPianYiLink.contains(TAKECHEAP_TUAN_DETAIL_H5_URL)){
						if(versionCode < 317){
							takeCheapLink = paiPianYiLink.replace(TAKECHEAP_TUAN_DETAIL_H5_URL, TAKECHEAP_TUAN_DETAIL_APP_URL);
						} else {
							takeCheapLink = paiPianYiLink.replace(TAKECHEAP_TUAN_DETAIL_H5_URL, TAKECHEAP_TUAN_NEW_APP_URL);
						}
					}
					Log.run.info("takeCheapLink is:"+takeCheapLink);
					item.setTakeCheapLink(takeCheapLink);
					/**
					 * 搜索优化新增已浏览和已下单个性化数据
					 */
					item.setPersonalizedTypeTag(getInt(itemObj,"PersonalizedTypeTag"));
					ret.add(item);
	            }
            }
        }
    	return ret;
    }
    
    private static List<SearchResultPriceRange> parsePriceRange(JSONObject dataObj, Set<String> fieldNames) {
    	List<SearchResultPriceRange> ret = new ArrayList<SearchResultPriceRange>();
    	if (dataObj != null && fieldNames.contains(RESULT_FIELD_PRICERANGE) && dataObj.has(RESULT_FIELD_PRICERANGE)) {
            JSONArray priceRangeListObj = getJSONArray(dataObj, RESULT_FIELD_PRICERANGE);
            if (priceRangeListObj != null && priceRangeListObj.size() > 0) {
	            for (int i = 0; i < priceRangeListObj.size(); i++) {
	            	SearchResultPriceRange priceRange = new SearchResultPriceRange();
	                JSONObject priceRangeObj = priceRangeListObj.getJSONObject(i);
	                priceRange.setHighLimit(getInt(priceRangeObj, "HighLimit"));
	                priceRange.setLowLimit(getInt(priceRangeObj, "LowLimit"));
	                priceRange.setRatio(getInt(priceRangeObj, "Ratio"));
	
	                ret.add(priceRange);
	            }
            }
        }
    	return ret;
    }
    
    /**
     * 解析hotClass部分的json内容
     * @param dataObj
     * @param fieldNames
     * @return
     */
    private static LinkedHashMap<String, List<SearchResultCluster>> parseHotClass(JSONObject dataObj, Set<String> fieldNames) {
    	if (dataObj != null && fieldNames.contains(RESULT_FIELD_HOTCLASS) && dataObj.has(RESULT_FIELD_HOTCLASS)) {
            JSONArray hotClassListObj = getJSONArray(dataObj, RESULT_FIELD_HOTCLASS);
            return parseCluster(hotClassListObj);
        }
    	return new LinkedHashMap<String, List<SearchResultCluster>>();
    }
    
    private static LinkedHashMap<String, List<SearchResultCluster>> parseProperty(JSONObject dataObj, Set<String> fieldNames) {
    	if (dataObj != null && fieldNames.contains(RESULT_FIELD_PROPERTY) && dataObj.has(RESULT_FIELD_PROPERTY)) {
            JSONArray propertyListObj = getJSONArray(dataObj, RESULT_FIELD_PROPERTY);
            return parseCluster(propertyListObj);
        }
    	return new LinkedHashMap<String, List<SearchResultCluster>>();
    }
    
    /**
     * hotClass/property部分的内容格式：
     * "hotclass": [    //"Property": [
     * [
     *   "cluster",
     *   [
     *      {
     *      "attrId": "",
     *      "clusterId": "",
     *      "clusterName": "",
     *      "clusterUrl": "",
     *      "clusterCount": ""
     *      }
     *   ]
     * ],
     * [
     *   "XX", 
     *   [
     *      {
     *      ....
     *      }
     *   ]
     * ]
     * ]
     * @param clusterListObj
     * @return
     */
    private static LinkedHashMap<String, List<SearchResultCluster>> parseCluster(JSONArray clusterListObj) {
    	LinkedHashMap<String, List<SearchResultCluster>> ret = new LinkedHashMap<String, List<SearchResultCluster>>();
    	if (clusterListObj != null && clusterListObj.size() > 0) {
            // 当前节点名称
            String clusterKey = "";
            for (int i = 0; i < clusterListObj.size(); i++) {
                JSONArray clusterObj = clusterListObj.getJSONArray(i);
                if (clusterObj != null && clusterObj.size() > 0) {
	                for (int j = 0; j < clusterObj.size(); j++) {
	                    Object clusterKeyObj = clusterObj.get(j);
	                    if (clusterKeyObj instanceof String) {
	                        clusterKey = clusterKeyObj.toString();
	                        ret.put(clusterKey, new ArrayList<SearchResultCluster>());
	                        continue;
	                    }
	                    JSONArray clusterValueListObj = clusterObj.getJSONArray(j);
	                    if (clusterValueListObj != null && clusterValueListObj.size() > 0) {
	                    	if (!ret.containsKey(clusterKey)) {
	                            ret.put(clusterKey, new ArrayList<SearchResultCluster>());
	                        }
	                    	List<SearchResultCluster> values = ret.get(clusterKey);
	                    	
		                    for (int k = 0; k < clusterValueListObj.size(); k++) {
		                        JSONObject clusterValueObj = clusterValueListObj.getJSONObject(k);
		                        
		                        if (clusterValueObj != null) {
			                    	SearchResultCluster cluster = new SearchResultCluster();
			                        cluster.setAttrId(getString(clusterValueObj, "attrId"));
			                        cluster.setClusterId(getString(clusterValueObj, "clusterId"));
			                        cluster.setClusterName(escapeSpecialChars(getString(clusterValueObj, "clusterName")));
			                        cluster.setClusterUrl(getString(clusterValueObj, "clusterUrl"));
			                        cluster.setClusterCount(getString(clusterValueObj, "clusterCount"));

			                        values.add(cluster);
		                        }
		                    }
		                }
	                }
                }
            }
        }
    	return ret;
    }
    
    private static List<SearchResultClass> parseClass(JSONObject dataObj, Set<String> fieldNames) {
    	List<SearchResultClass> ret = new ArrayList<SearchResultClass>();
    	if (dataObj != null && fieldNames.contains(RESULT_FIELD_CLASS) && dataObj.has(RESULT_FIELD_CLASS)) {
            JSONArray classListObj = getJSONArray(dataObj, RESULT_FIELD_CLASS);
            if (classListObj != null && classListObj.size() > 0) {
	            for (int i = 0; i < classListObj.size(); i++) {
	            	SearchResultClass cls = new SearchResultClass();
	                JSONObject classObj = classListObj.getJSONObject(i);
	                cls.setClassId(getString(classObj, "classId"));
	                cls.setCount(getString(classObj, "count"));
	
	                ret.add(cls);
	            }
            }
        }
    	return ret;
    }
    
    private static List<SearchResultPath> parsePath(JSONObject dataObj, Set<String> fieldNames) {
    	List<SearchResultPath> ret = new ArrayList<SearchResultPath>();
    	if (dataObj != null && fieldNames.contains(RESULT_FIELD_PATH) && dataObj.has(RESULT_FIELD_PATH)) {
            JSONArray pathListObj = getJSONArray(dataObj, RESULT_FIELD_PATH);
            for (int i = 0; i < pathListObj.size(); i++) {
            	SearchResultPath path = new SearchResultPath();
                JSONObject pathObj = pathListObj.getJSONObject(i);
                path.setAttrId(getString(pathObj, "AttrId"));
                path.setAttrName(getString(pathObj, "AttrName"));
                path.setItemId(getString(pathObj, "ItemId"));
                path.setItemName(getString(pathObj, "ItemName"));
                path.setPathUrl(getString(pathObj, "pathUrl"));

                ret.add(path);
            }
        }
    	return ret;
    }

    /**
     * 转换 json 中的特殊字符
     * @param original 原字符串
     * @return 转换后的字符
     */
    private static String escapeSpecialChars(String original) {
        if(StringUtil.isBlank(original)) {
            return original;
        }
        return original.replaceAll("&quot;", "=").replaceAll("&lt;", "<").replaceAll("&gt;", ">")
                .replaceAll("&amp;", "&").replaceAll("&#38;", "&");
    }
    
    protected static double getDouble(JSONObject jobj, String key) {
		if (jobj != null) {
			try {
				return jobj.getDouble(key);
			} catch (Exception e) {
				// ignore
			}
		}
		return 0;
	}

	protected static long getLong(JSONObject jobj, String key) {
		if (jobj != null) {
			try {
				return jobj.getLong(key);
			} catch (Exception e) {
				// ignore;
			}
		}
		return 0;
	}
	
	protected static int getInt(JSONObject jobj, String key) {
		if (jobj != null) {
			try {
				return jobj.getInt(key);
			} catch (Exception e) {
				// ignore;
			}
		}
		return 0;
	}
	
	protected static String getString(JSONObject jobj, String key) {
		if (jobj != null) {
			try {
				return jobj.getString(key);
			} catch (Exception e) {
				// ignore
			}
		}
		return "";
	}
	
	protected static JSONObject getJSONObject(JSONObject jobj, String key) {
		if (jobj != null) {
			try {
				return jobj.getJSONObject(key);
			} catch (Exception e) {
				// ignore
			}
		}
		return null;
	}
	
	protected static JSONArray getJSONArray(JSONObject jobj, String key) {
		if (jobj != null) {
			try {
				return jobj.getJSONArray(key);
			} catch (Exception e) {
				// ignore
			}
		}
		return null;
	}

	public static void main(String[] args) {
		SearchResultSerializer.parseShipShop("2920947974");
	}
}
