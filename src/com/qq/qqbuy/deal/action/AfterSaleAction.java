package com.qq.qqbuy.deal.action;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.mapper.ActionMapping;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.qq.qqbuy.common.JsonOutput;
import com.qq.qqbuy.common.action.AuthRequiredAction;
import com.qq.qqbuy.common.exception.NotLoginException;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.deal.biz.DealBiz;
import com.qq.qqbuy.deal.biz.DrawIdGenerator;
import com.qq.qqbuy.deal.biz.QueryDealBiz;
import com.qq.qqbuy.deal.po.ApplyRefundForm;
import com.qq.qqbuy.deal.po.DealDetailInfo;
import com.qq.qqbuy.deal.po.MakeEvalListPo;
import com.qq.qqbuy.deal.util.DealUtil;
import com.qq.qqbuy.tenpay.TenpayUtil;
import com.qq.qqbuy.thirdparty.idl.deal.DealClient;
import com.qq.qqbuy.thirdparty.idl.dealpc.InfoType;
import com.qq.qqbuy.thirdparty.idl.dealpc.PcDealQueryClient;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CDealInfo;
import com.qq.qqbuy.thirdparty.idl.refund.RefundClient;
import com.qq.qqbuy.thirdparty.idl.sms.SmsCodeClient;
import com.qq.qqbuy.thirdparty.idl.userinfo.ApiUserClient;

/**
 * 支付完毕后,用户的一系列行为,例如收货,评价,退款,维权等
 * @author JingYing
 * @date 2014年11月23日
 */
public class AfterSaleAction extends AuthRequiredAction	{

	private static final long serialVersionUID = -3106397663483685378L;

	static Logger log = LogManager.getLogger();
	
	DealBiz dealBiz;
	QueryDealBiz queryDealBiz;
	DealClient dealClient = new DealClient();
	SmsCodeClient smsCodeClient = new SmsCodeClient();
	RefundClient refundClient = new RefundClient(); 
	Gson gson = new GsonBuilder().serializeNulls().create();
	JsonOutput out = new JsonOutput();
	DrawIdGenerator drawIdGenerator = new DrawIdGenerator(); 
	
	String dealId, tradeIds, pt, token, lskey;	//财付通交易号, 大单id. 小单id(逗号隔开). 支付方式. QQ登录态(@开头). 
	String mobileNo, biz;						//手机号, 验证码业务场景(2_1代表确认收货, 2_2代表申请退款)
	String dsr;									//评分(依次为商品和描述相符度,卖家的服务态度,卖家的发货速度,逗号分隔)
	
	/**
	 * [
		{"t":"111", "l":"1", "c":"很差", "r":["3","1"]},
		{"t":"222", "l":"3", "c":"不错!"}
		]
	 */
	String info; //评价详情.注意C参数的XSS
	
	int isReq, isRecv, reason, toBuyerAmount, event, tradeId; //注意reason参数的xss
	String msg;
	
	/**
	 * 财付通的确认收货页面地址
	 * @return
	 */
	public String cftRecvUrl()	{
		long wid = getWidFromToken(getAppToken());
		ActionMapping mapping = ServletActionContext.getActionMapping();
		String returnUrl = getBasePath() + mapping.getNamespace() + "/fake." + mapping.getExtension();	//地址无意义,手机端会拦截对该地址的访问
		
		long[] tradeIdArr = Util.toLongArray(tradeIds.split(","));
		DealDetailInfo deal = queryDealBiz.dealDetail(wid, dealId, false, 0, getMk());
		String url = TenpayUtil.genConfirmRecvUrl(wid, deal.getTenpayCode(), dealId, 
												tradeIdArr, deal.getDealPayFeeTotal(), returnUrl, lskey);	//lskey手机有的传, 有的没传,没传的话不要拼入url

		JsonObject json = new JsonObject();
		json.addProperty("url", url);
		return doPrint(out.setData(json));
	}
	
	/**
	 * 发送手机验证码
	 * @return
	 */
	public String sendSmsCode()	{
		assertLogin(true);
		if(Util.isNotEmpty(dealId))	{	//dealId有值时,用收货人手机号
			CDealInfo deal = PcDealQueryClient.sysGetDealInfo(dealId, DealUtil.isHistory(dealId), InfoType.DEAL, getMk());
			if(deal == null)	{		//避免dealUtil.isHistory()有问题
				deal = PcDealQueryClient.sysGetDealInfo(dealId, !DealUtil.isHistory(dealId), InfoType.DEAL, getMk());
			}
			if(deal == null)					throw new IllegalArgumentException("dealId=" + dealId);
			if(deal.getBuyerUin() != getQq())	throw new IllegalArgumentException("非该用户的订单号");
			mobileNo = deal.getStrReceiveMobile();
		} else if("".equals(dealId)){	//dealId=""时,用登录人手机号
			mobileNo = ApiUserClient.getUserSimpleInfo(getQq()).getMobile();
		}								//dealId为null时,用mobileNo
		smsCodeClient.send(getQq(), mobileNo, biz, getMk(), getSk());
		return doPrint(out);
	}
	
	/**
		2.财付通不需要token. 3.微信不输入验证码
	 */
	public String recvItem()	{
		assertLogin(false);
		String[] tradeIdArr = tradeIds.split(",");
		CDealInfo deal = PcDealQueryClient.sysGetDealInfo(dealId, false, InfoType.DEAL, getMk());
		String drawId = drawIdGenerator.genRecvDrawId(deal.getDealCftPayid(), Util.toLongArray(tradeIdArr));
		dealClient.confirmRecvDeal(getQq(), dealId, tradeIdArr, "", drawId);
		return doPrint(out);
	}
	
	/**
	 * 提交评价
	 * @return
	 */
	public String makeEval()	{
		assertLogin(true);
		MakeEvalListPo po;
		try {
			int[] dsrs = Util.toIntArray(dsr.split(","));
			long seller = Long.parseLong(dealId.split("-")[0]);
			po = new MakeEvalListPo(dealId, getQq(), seller, dsrs[0], dsrs[1], dsrs[2]);
			po.setMakeEvalPoList(MakeEvalListPo.parseFromJson(dealId, info));
		} catch (Exception e) {
			throw new IllegalArgumentException(e.getMessage(), e);
		}
		dealBiz.makeEvalToSeller(getQq(), dealId, getSk(), getMk(), po);
		return doPrint(out);
	}
	
	/**
	 * 提交退款协议
	 * @return
	 */
	public String refund()	{
		assertLogin(true);
		String[] dealArr = dealId.split("-");
		if(dealArr.length != 3)
			throw new IllegalArgumentException(dealId);
		ApplyRefundForm form = new ApplyRefundForm();
		form.setBuyer(getQq());
		form.setDealId(dealId);
		form.setRecItem(isRecv);
		form.setRefundApplyMsg(msg);
		form.setRefundReasonType(reason);
		form.setRefundToBuyerAmount(toBuyerAmount); 
		form.setReqItem(isReq);
		form.setSeller(Long.parseLong(dealArr[0]));
		if(tradeId != 0)	{
			form.setTradeId(dealArr[0] + "-" + dealArr[1] + "-" + tradeId);
		}
		refundClient.applyRefund(form, getHttpHeadIp(), getMk(), getSk(), event);
		return doPrint(out);
	}
	
	/**
	 * 兼容明文手机号的token解析方式
	 * @param token
	 * @param dealId
	 * @return
	 */
	private String fetchMobileByToken(String token, String dealId)	{
		String[] arr = token.split("-");
		if("1".equals(arr[0]))	{			//格式:1-验证码  使用登录人的手机号
			return ApiUserClient.getUserSimpleInfo(getQq()).getMobile();
		} else if("2".equals(arr[0]))	{	//格式:2-验证码  使用收货人的手机号
			CDealInfo deal = PcDealQueryClient.sysGetDealInfo(dealId, DealUtil.isHistory(dealId), InfoType.DEAL, getMk());
			if(deal == null)	{
				deal = PcDealQueryClient.sysGetDealInfo(dealId, !DealUtil.isHistory(dealId), InfoType.DEAL, getMk());
			}
			if(deal == null)	throw new IllegalArgumentException("dealId=" + dealId);
			return deal.getStrReceiveMobile();
		} else	{							//格式:手机号-验证码
			return arr[0];
		}
	}
	
	private void assertLogin(boolean getSkey)	{
		boolean isLogin = getSkey ? checkLoginAndGetSkey() : checkLogin();
		if(!isLogin){
			throw new NotLoginException();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public String getDealId() {
		return dealId;
	}

	public void setDealId(String dealId) {
		this.dealId = dealId;
	}

	public String getTradeIds() {
		return tradeIds;
	}

	public void setTradeIds(String tradeIds) {
		this.tradeIds = tradeIds;
	}

	public String getPt() {
		return pt;
	}

	public void setPt(String pt) {
		this.pt = pt;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getBiz() {
		return biz;
	}

	public void setBiz(String biz) {
		this.biz = biz;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getDsr() {
		return dsr;
	}

	public void setDsr(String dsr) {
		this.dsr = dsr;
	}

	public String getLskey() {
		return lskey;
	}

	public void setLskey(String lskey) {
		this.lskey = lskey;
	}

	public int getIsReq() {
		return isReq;
	}

	public void setIsReq(int isReq) {
		this.isReq = isReq;
	}

	public int getIsRecv() {
		return isRecv;
	}

	public void setIsRecv(int isRecv) {
		this.isRecv = isRecv;
	}

	public int getReason() {
		return reason;
	}

	public void setReason(int reason) {
		this.reason = reason;
	}

	public int getToBuyerAmount() {
		return toBuyerAmount;
	}

	public void setToBuyerAmount(int toBuyerAmount) {
		this.toBuyerAmount = toBuyerAmount;
	}

	public int getEvent() {
		return event;
	}

	public void setEvent(int event) {
		this.event = event;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public void setQueryDealBiz(QueryDealBiz queryDealBiz) {
		this.queryDealBiz = queryDealBiz;
	}

	public void setDealBiz(DealBiz dealBiz) {
		this.dealBiz = dealBiz;
	}

	public int getTradeId() {
		return tradeId;
	}

	public void setTradeId(int tradeId) {
		this.tradeId = tradeId;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
}
