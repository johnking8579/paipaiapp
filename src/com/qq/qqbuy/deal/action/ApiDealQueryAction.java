package com.qq.qqbuy.deal.action;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.qq.qqbuy.common.JsonOutput;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.action.PaipaiApiBaseAction;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.AntiXssHelper;
import com.qq.qqbuy.common.util.HttpUtil.ProxyManager;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.deal.biz.QueryDealBiz;
import com.qq.qqbuy.deal.po.DealCancelInfo;
import com.qq.qqbuy.deal.po.DealCmdyInfo;
import com.qq.qqbuy.deal.po.DealDetailInfo;
import com.qq.qqbuy.deal.po.DealListInfo;
import com.qq.qqbuy.deal.po.DealListItemInfo;
import com.qq.qqbuy.deal.po.LogisticInfoPo;
import com.qq.qqbuy.deal.po.LogisticLogPo;
import com.qq.qqbuy.deal.util.DealConstant;
import com.qq.qqbuy.dealopr.po.DealState;
import com.qq.qqbuy.shop.biz.ShopBiz;
import com.qq.qqbuy.shop.po.ShopInfo;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.EmployeeInfoPo;
import com.qq.qqbuy.thirdparty.idl.userinfo.ApiUserClient;
import com.qq.qqbuy.wuliu.biz.PPWuliuBiz;
import com.qq.qqbuy.wuliu.po.LogisticLogInfo;
import com.qq.qqbuy.wuliu.po.TraceInfo;

public class ApiDealQueryAction extends PaipaiApiBaseAction
{
    private static final long serialVersionUID = 4720962769613850217L;
    
    /**
     * 订单查询服务
     */
    private QueryDealBiz queryDealBiz;

    /**
     * 物流查询服务
     */
    private PPWuliuBiz wuliuBiz;
    ShopBiz shopBiz;
    Gson gson = new GsonBuilder().serializeNulls().create();
    JsonOutput out = new JsonOutput();
    
    /**
     * 查询各状态的订单数量 statesList 的版本号
     * stateVer >=1 返回待评价订单、易完成订单数量
     */
    private int statVer = 0;

    /*************************** START查询订单列表的请求参数 **********************/
    /**
     * 起始页，从1开始，默认为1，也就是第一页
     */
    private int pageNo = 1;

    /**
     * 每一页的个数，默认为5
     */
    private int pageSize = 5;

    /**
     * 待查询的订单类型 空字符串代表所有 DS_WAIT_BUYER_PAY：待在线付款或在货到付款流程中
     * DS_WAIT_SELLER_DELIVERY： 待发货(已在线付款) DS_WAIT_BUYER_RECEIVE：待确认收货(已在线付款)
     * DS_DEAL_END_NORMAL： 交易成功(在线支付) DS_DEAL_SUCCESS_ALL：交易成功(在线支付+货到付款)
     * COD_WAIT_SHIP：货到付款待发货   COD_SHIP_OK：货到付款已发货   COD_SIGN：货到付款已签收
     * COD_SUCCESS：货到付款成功(已打款)
     */
    private String dealType = "";
    
    /**
     * 订单评价状态：100需买家评价，101需卖家评价，102买家已评价，103卖家已评价
     */
    private int rateState = 0;
    
    /**
     * short historyFlag : 是否查询历史数据    1查询历史数据（三个月前的）,其他都为三个月内的 
     */
	private short historyFlag=0;
    
    /*************************** END 查询订单列表的请求参数 **********************/

    /*************************** START查询订单详情的请求参数 **********************/
    /**
     * 卖家qq号码
     */
    private long sellerUin = 0;

    /**
     * 订单id
     */
    private String dealCode = "";

    /**
     * 是否列出商品 0（默认）：不列出订单相关商品 非0：列出订单相关商品（速度比较慢）
     */
    private long listItem = 0;

    /*************************** END 查询订单详情的请求参数 **********************/

    /*************************** START关闭订单的请求参数 **********************/
    /**
     * closeReason：关闭原因 8：无法联系上卖家 9：卖家无诚意完成交易 10：卖家缺货无法交易 11：因为网银无法完成付款
     * 12：因为误拍我不想要了
     */
    private int closeReason = 12;
    
    /*************************** END关闭订单的请求参数 **********************/
    
    /**
     * 我的订单列表
     * @return
     */
    public String dealList()    {
        if (!checkLogin())	{
        	out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL).setMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
        } else	{
	        DealListInfo deals = queryDealBiz.queryDealList(getWid(), adaptDealState(dealType), rateState, pageNo, pageSize, historyFlag, getMk());
	        if(parseVersionCode() <= 313)	{	//TODO 等3.1.3(包含)之前的版本全部升级后,再加密或去掉手机号
	        	deals.setBuyerMobile(ApiUserClient.getUserSimpleInfo(getWid()).getMobile());	
	        }
	        out.setData(gson.toJsonTree(deals));
        }
        return doPrint(out);
    }
    
    /**
     * @deprecated 遗留下的"订单状态转换"方法, 等手机端修改接口,再统一使用DealConstant.STATE_*代替
     * @param dealStateStr
     * @return
     */
	private int adaptDealState(String dealStateStr) {
		if ("DS_WAIT_BUYER_PAY".equals(dealStateStr)) {
			return (int)DealState.DS_WAIT_BUYER_PAY.getCode();
		} else if ("DS_COMBO_WAIT_SHIP".equals(dealStateStr)) {
			return DealConstant.STATE_COMBO_WAIT_SHIP;
		} else if ("DS_COMBO_WAIT_RECV".equals(dealStateStr)) {
			return DealConstant.STATE_COMBO_WAIT_RECV;
		} else	{
			try {
				return Integer.parseInt(dealStateStr);
			} catch (NumberFormatException e1) {
				return (int)DealState.DS_UNKNOWN.getCode();
			}
		}
	}
	
    private int parseVersionCode()	{
    	try {
			return Integer.parseInt(getVersionCode());
		} catch (Exception e) {
			return 0;
		}
    }

    
    /**
     * 查询订单详情
     * @return
     */
    public String dealInfo()    {
        if (!checkLogin()) {
        	out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL).setMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
        } else        {
        	boolean isHistory = historyFlag==0 ? false : true;
        	DealDetailInfo deal = queryDealBiz.dealDetail(getWid(), dealCode, isHistory, listItem, getMk());
        	
        	String MobileNo = deal.getReceiverMobile();
			if(StringUtil.isNotBlank(MobileNo) && MobileNo.trim().length() == 11){
				deal.setReceiverMobile(StringUtil.substring(MobileNo, 0, 3) + "****" + StringUtil.substring(MobileNo, 7, 11));
			}
			
        	JsonElement dealj = gson.toJsonTree(deal);
        	if(deal != null)	{
	        	ShopInfo shop = shopBiz.getShopInfo(deal.getSellerUin());
	        	dealj.getAsJsonObject().add("shop", gson.toJsonTree(shop));
        	}
        	out.setData(dealj);
        }
        return doPrint(out);
    }
    
    /**
     * 取消订单
     * @return
     */
    public String cancle()
    {
        jsonStr = "{}";
        if (checkLoginAndGetSkey())        {
            if (checkCancelDeal())
            {
                JsonConfig jsoncfg = new JsonConfig();
                String[] excludes ={ "size", "empty" };
                jsoncfg.setExcludes(excludes);
                dealCode = AntiXssHelper.htmlAttributeEncode(dealCode);
                DealCancelInfo ret = queryDealBiz.cancelDeal(getQq(), getSk(), getMk(),
                        dealCode, closeReason,getSid(),getLk());
                
                if (ret != null && ret.errCode == 0)
                    jsonStr = JSONSerializer.toJSON(ret, jsoncfg).toString();
                else if (ret != null)
                    setErrCodeAndMsg((int) ret.errCode, ret.msg);
            } else
            {
                setErrCodeAndMsg(ErrConstant.ERRCODE_INVALID_PARAMETER,"参数校验不合法");
            }
        } else
        {
            setErrCodeAndMsg(ErrConstant.ERRCODE_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
        }
        return SUCCESS;
    }
    
    private boolean checkCancelDeal()
    {
        boolean ret = false;
        if (getQq() < 10000 || StringUtils.isEmpty(dealCode))
            return ret;
        return true;
    }
    
	/**
	 * 从qq速递获得物流信息
	 * */
	public String dealSuDi() {
		jsonStr = "{}";
		if (checkLogin()) {
			if (!StringUtil.isBlank(dealCode)) {
				dealCode = AntiXssHelper.htmlAttributeEncode(dealCode);
               LogisticInfoPo infoPo=new LogisticInfoPo();
				LogisticLogInfo logisticLogInfo = null;
				try {
					logisticLogInfo = wuliuBiz
							.getWuliuLogEx(getWid(), dealCode,historyFlag);
				} catch (BusinessException e) {
					Log.run.error(e.getMessage(), e);
				}
				infoPo.retCode=logisticLogInfo.getRetCode();
				infoPo.setCompanyId(logisticLogInfo.getData().getCompanyId());
				infoPo.setCompanyName(logisticLogInfo.getData().getCompanyName());
				infoPo.setCompanyPhone(logisticLogInfo.getData().getCompanyPhone());
				infoPo.setCompanyUrl(logisticLogInfo.getData().getCompanyUrl());
				infoPo.setDeliverId(logisticLogInfo.getData().getDeliverId());
				TraceInfo[] traceInfoArr=logisticLogInfo.getData().getTraceInfos();
				if(traceInfoArr!=null){
					for(TraceInfo trace:traceInfoArr){
						LogisticLogPo po=new LogisticLogPo();
						po.setDeliveryTime(trace.getTime());
						po.setTraceInfo(trace.getDesc());
						infoPo.setLogisticLogList(po);						
					}
				}
				jsonStr = JSONSerializer.toJSON(infoPo).toString();	
			}
		}
		return SUCCESS;
	}
    
	
	public String tes() throws IOException{
        long start = System.currentTimeMillis();
                jsonStr = "{}";
                JsonConfig jsoncfg = new JsonConfig();
                String[] excludes ={ "size", "empty" };
                jsoncfg.setExcludes(excludes);
                DealDetailInfo ret = new DealDetailInfo();
                if (checkLogin()) {
                	try {
                		 //获取卖家用户列表
                        Vector<EmployeeInfoPo> employeeList = queryDealBiz.getEmployeeList(sellerUin,getMk(),getSk(),getQq(),getCp());
                        //对员工列表进行排序。判断哪一个QQ号在线。如果在线前在线QQ放在第一位如果都不在线。将卖家Qq放在第一位
                        System.out.println("employeeList:"+employeeList+"employeeList.size:"+employeeList.size());
                       if(employeeList!=null&&employeeList.size()>0){
                    	   //将卖家自己也加入到员工列表中
                    	    EmployeeInfoPo ep = new EmployeeInfoPo();
                    	    ep.setStatus(getQQOnlinepic(sellerUin,true));
                      		ep.setSellerUin(sellerUin);
                    	    ep.setEmployeeUin(sellerUin);
                        //重新排序
                        getNewVector(employeeList,ep);
                       }else if(employeeList!=null&&employeeList.size()==0){
						//如果为空则将卖家设置为员工
                    	   EmployeeInfoPo ep = new EmployeeInfoPo();
                    	   ep.setStatus(getQQOnlinepic(sellerUin,true));
                    	   ep.setSellerUin(sellerUin);
                    	   ep.setEmployeeUin(sellerUin);
                    	   employeeList.add(ep);
                       	}else {
                       		employeeList = new Vector<EmployeeInfoPo>();
                       		EmployeeInfoPo ep = new EmployeeInfoPo();
                       	    ep.setStatus(getQQOnlinepic(sellerUin,true));
                       		ep.setSellerUin(sellerUin);
                     	    ep.setEmployeeUin(sellerUin);
                     	    employeeList.add(ep);
						}
                        ret.setEmployeeList(employeeList);
                        if (ret != null )
                            jsonStr = JSONSerializer.toJSON(ret, jsoncfg).toString();
					} catch (Exception e) {
							setErrCodeAndMsg(0, e.getMessage());
							Log.run.info("employeeListerror"+e.getMessage());
					}
                }
        return SUCCESS;
    
	}

	/**
	 * 查询用户订单商品列表
	 * @return
	 */
	public String dealItems()    {
		if (!checkLogin())	{
			out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL).setMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
		} else	{
			JsonArray items = new JsonArray();
			Set<String> set = new HashSet<String>();
			DealListInfo deals = queryDealBiz.queryDealList(getWid(), adaptDealState(dealType), rateState, pageNo, pageSize, historyFlag, getMk());
			List<DealListItemInfo> dealList = deals.getDealList();
			if(dealList != null && !dealList.isEmpty()){
				for(DealListItemInfo deal : dealList){
					List<DealCmdyInfo> itemList = deal.getItemList();
					for(DealCmdyInfo item : itemList){
						String itemCode = item.getItemCode();
						if(!set.contains(itemCode)){
							set.add(itemCode);
							JsonObject itemObj = new JsonObject();
							itemObj.addProperty("itemCode",itemCode);
							itemObj.addProperty("itemPrice",item.getItemDealPrice());
							itemObj.addProperty("itemTitle",item.getItemName());
							itemObj.addProperty("itemPic",item.getItemPic80());
							items.add(itemObj);
						}
					}
				}
			}
			JsonObject json = new JsonObject();
			json.add("items", items);
			out.setData(json);
		}
		return doPrint(out);
	}
	/**
	 * @param ep 
	 * 
	
	 * @Title: getNewVector 
	
	 * @Description: 对特定
	
	 * @param @param empVecter
	 * @param @return
	 * @param @throws IOException    设定文件 
	
	 * @return Vector<EmployeeInfoPo>    返回类型 
	
	 * @throws
	 */
	public static Vector<EmployeeInfoPo> getNewVector(Vector<EmployeeInfoPo> empVecter, EmployeeInfoPo ep) throws IOException{
		if(empVecter!=null){
			//是否将卖家设置到第一位
		boolean issetuin=true;
		 Iterator iterator  = empVecter.iterator();
		 while (iterator.hasNext()) {
			EmployeeInfoPo type = (EmployeeInfoPo) iterator.next();
			if(type.getEmployeeUin()!=0&&!"".equals(type.getEmployeeUin())){
				int pic=getQQOnlinepic(type.getEmployeeUin(),true);
				type.setStatus(pic);
			}
			if(type.getStatus()!=0){
				//如果卖家员工里有了在线的则不再设置卖家
				issetuin=false;
			}
		}
		 //对verter进行排序。0在上
		 Collections.sort(empVecter, new Comparator<EmployeeInfoPo>()
				 {
					@Override
					public int compare(EmployeeInfoPo o1,
							EmployeeInfoPo o2) {
						return o2.getStatus()-o1.getStatus();
					}
			 
				 });
		 if(issetuin){
			 empVecter.add(0, ep);
		 }
		}
		return empVecter;
	}
	

	
	/**
	 * 
	
	 * @Title: SendGET 
	
	 * @Description: 用于判断是否在线。根据请求返回的图片
	
	 * @param @param url
	 * @param @param param
	 * @param @param isproxy
	 * @param @return
	 * @param @throws IOException    设定文件 
	
	 * @return String    返回类型 
	
	 * @throws
	 */
	public static int getQQOnlinepic(long qq, boolean isproxy)
			throws IOException {
		String url = "http://wpa.paipai.com/pa?p=1:"+qq+":17";
		System.out.println("访问地址:" + url);
		URL serverUrl = new URL(url);
		HttpURLConnection conn = null;
		if (isproxy) {
			conn = (HttpURLConnection) serverUrl.openConnection(ProxyManager
					.getProxy());
		} else {
			conn = (HttpURLConnection) serverUrl.openConnection();
		}
		conn.setRequestMethod("GET");
		// 必须设置false，否则会自动redirect到Location的地址
		conn.setInstanceFollowRedirects(false);

		conn.addRequestProperty("Accept-Charset", "UTF-8;");
		conn.addRequestProperty("User-Agent",
				"Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.2.8) Firefox/3.6.8");
		conn.addRequestProperty("Referer", "http://app.paipai.com/");
		conn.connect();
		String location = conn.getHeaderField("Location");
		System.out.println("跳转地址:" + location);
		 if(location.contains("state_2_1.gif")){
				return 2;
			}else if(location.contains("state_2_2.gif")){
				return 1;
			}else {
				return 0;
			}
		
	}
	
	
	public static void main(String[] args) throws IOException {
		// 请求的cgi地址57625398
				 Vector<EmployeeInfoPo> employeeList = new Vector<EmployeeInfoPo>();
				 EmployeeInfoPo p1= new EmployeeInfoPo();
				 p1.setEmployeeUin(345433386);
				 EmployeeInfoPo p2= new EmployeeInfoPo();
				 p2.setEmployeeUin(1275000334);
				 EmployeeInfoPo p3= new EmployeeInfoPo();
				 p3.setEmployeeUin(234549442);
				 employeeList.add(p1);
				 employeeList.add(p2);
				 employeeList.add(p3);
				 EmployeeInfoPo ep= new EmployeeInfoPo();
				 ep.setEmployeeUin(852026881);
				 ep.setStatus(getQQOnlinepic(852026881, false));
				 getNewVector(employeeList,ep);
				 Iterator iterator  = employeeList.iterator();
				 
				 iterator  = employeeList.iterator();
				 while (iterator.hasNext()) {
						EmployeeInfoPo type = (EmployeeInfoPo) iterator.next();
						System.out.println(type.getEmployeeUin()+""+type.getStatus());
					}
	}
	
	public PPWuliuBiz getWuliuBiz() {
		return wuliuBiz;
	}

	public void setWuliuBiz(PPWuliuBiz wuliuBiz) {
		this.wuliuBiz = wuliuBiz;
	}
	
    public int getPageNo()
    {
        return pageNo;
    }

    public int getCloseReason()
    {
        return closeReason;
    }

    public void setCloseReason(int closeReason)
    {
        this.closeReason = closeReason;
    }

    public long getSellerUin()
    {
        return sellerUin;
    }

    public void setSellerUin(long sellerUin)
    {
        this.sellerUin = sellerUin;
    }

    public String getDealCode()
    {
        return dealCode;
    }

    public void setDealCode(String dealCode)
    {
        this.dealCode = dealCode;
    }

    public long getListItem()
    {
        return listItem;
    }

    public void setListItem(long listItem)
    {
        this.listItem = listItem;
    }

    public void setPageNo(int pageNo)
    {
        this.pageNo = pageNo;
    }

    public int getPageSize()
    {
        return pageSize;
    }

    public void setPageSize(int pageSize)
    {
        this.pageSize = pageSize;
    }

    public String getDealType()
    {
        return dealType;
    }

    public void setDealType(String dealType)
    {
        this.dealType = dealType;
    }
    
    public int getRateState() 
    {
		return rateState;
	}

	public void setRateState(int rateState) 
	{
		this.rateState = rateState;
	}

	private String jsonStr = "";

    public QueryDealBiz getQueryDealBiz()
    {
        return queryDealBiz;
    }

    public void setQueryDealBiz(QueryDealBiz queryDealBiz)
    {
        this.queryDealBiz = queryDealBiz;
    }

    public String getJsonStr()
    {
        return jsonStr;
    }

    public void setJsonStr(String jsonStr)
    {
        this.jsonStr = jsonStr;
    }
    
    public int getStatVer() 
    {
		return statVer;
	}

	public void setStatVer(int statVer) 
	{
		this.statVer = statVer;
	}
	
    public short getHistoryFlag() {
		return historyFlag;
	}

	public void setHistoryFlag(short historyFlag) {
		this.historyFlag = historyFlag;
	}

	public void setShopBiz(ShopBiz shopBiz) {
		this.shopBiz = shopBiz;
	}

}
