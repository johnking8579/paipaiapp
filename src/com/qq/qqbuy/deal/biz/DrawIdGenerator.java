package com.qq.qqbuy.deal.biz;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * drawid: 和第三方支付系统交互时使用的交易号. 和CDealInfo.dealCftPayid相关
 * @author JingYing
 * @date 2015年4月14日
 */
public class DrawIdGenerator {
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
	
	/**
	 * 生成退款drawId
	 * @param dealCftPayid
	 * @param tradeIds
	 * @return
	 */
	public String genRefundDrawId(String dealCftPayid, long[] tradeIds)	{
		return gen(112, dealCftPayid, tradeIds);
	}
	
	/**
	 * 生成确认收货drawid
	 * @param dealCftPayid
	 * @param tradeIds
	 * @return
	 */
	public String genRecvDrawId(String dealCftPayid, long[] tradeIds)	{
		return gen(113, dealCftPayid, tradeIds);
	}
	
	
	/**
	 * 生成drawid ：前缀112(申请退款)/113(确认收货)+spid(10位)+日期（YYMMDD)+序列号(9位)
	 *               112 2000000101 150414 000000017）
	 *               
	 * @param drawHead 退款和确认收货的drawhead各不同
	 * @param tenpayTransId
	 * @param tradeId
	 * @return
	 */
	private String gen(int drawHead, String dealCftPayid, long[] tradeIds) {
		Arrays.sort(tradeIds);		//取最小的tradeId
		String spid;
		if(dealCftPayid.length() == 28)
			spid = dealCftPayid.substring(0, 10);
		else
			spid = "1000000001";
		return new StringBuilder()
			.append(drawHead)
			.append(spid)
			.append(sdf.format(new Date()))
			.append(String.format("%09d", tradeIds[0] % 1000000000))	
			.toString();
	}
}
