package com.qq.qqbuy.deal.biz;

import java.util.Map;
import java.util.Vector;

import com.qq.qqbuy.deal.po.DealCancelInfo;
import com.qq.qqbuy.deal.po.DealDetailInfo;
import com.qq.qqbuy.deal.po.DealListInfo;
import com.qq.qqbuy.deal.po.DealStateList;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.EmployeeInfoPo;

/**
 * 
 * @ClassName: QueryDealBiz
 * 
 * @Description: 查询订单相关的biz
 * 
 * @author wendyhu
 * @date 2012-12-13 下午02:55:03
 */
public interface QueryDealBiz
{
	
    /**
     * 
     * @Title: queryDealStatesList
     * 
     * @Description: 查询订单状态列表
     * @param uin
     *            ： 用户qq号码
     * @param skey
     *            ：IDL登陆验证key
     * @param mk
     *            :机器码
     * @return 设定文件
     * @return DealStateList 返回类型
     * @throws
     */
    public DealStateList queryDealStatesList(long uin, int ver);

    /**
     * @Description: 根据订单状态查询订单列表
     * @param dealState 见DealConstant.STATE_*
     * @param pageNo	页面起始页，从1开始
     * @param pageSize     *            每页的大小
     * @param short historyFlag : 是否查询历史数据    1查询历史数据（三个月前的）,其他都为三个月内的 
     * @throws
     */
    public DealListInfo queryDealList(long uin, int dealState, int rateState, 
    		int pageNo, int pageSize, short historyFlag, String mk);

    /**
     * 
     * @Title: cancelDeal 
     * 
     * @Description: 取消订单
     *  
     * @param buyerUin：买家qq号码
     * @param skey：登陆key
     * @param mk: 机器码
     * @param dealCode：订单id
     * @param closeReason：关闭原因 8：无法联系上卖家
     *                             9：卖家无诚意完成交易
     *                             10：卖家缺货无法交易
     *                             11：因为网银无法完成付款
     *                             12：因为误拍我不想要了。
     * @return    设定文件 
     * @return DealCancelInfo    返回类型 
     * @throws
     */
    public DealCancelInfo cancelDeal(long buyerUin,String skey, String mk, String dealCode, long closeReason,String sid,String lskey);  
    
    /**
     * 
     * @Title: dealDetail
     * @Description: 查询商品详情
     * @param listItem
     *            :是否列出商品 0（默认）：不列出订单相关商品 非0：列出订单相关商品（速度比较慢）
     * @throws
     */
	public DealDetailInfo dealDetail(long wid, String dealCode, boolean isHistory, long listItem, String mk);
	
	/**
	 * 
	
	 * @Title: getEmployeeList 
	
	 * @Description: 根据卖家Qq号查出该Qq下所有的员工列表
	
	 * @param @param sellerUin
	 * @param @param mk
	 * @param @return    设定文件 
	
	 * @return List<EmployeeInfoPo>    返回类型 
	
	 * @throws
	 */
	public Vector<EmployeeInfoPo> getEmployeeList(long sellerUin, String mk,String sk,long qq,String clientip);

	/**
	 * 统计订单个数
	 * @param uin
	 * @return
	 */
	Map<String, Integer> getDealStatInfoByUin(long uin, String mk);

//	/**
//	 * 根据小订单号, 查询对应的最新一条退款协议. 注:不适合批量查询
//	 * @param dealId 大订单
//	 * @param tradeId 子订单
//	 * @param buyerUin
//	 * @param mk 机器码
//	 * @return
//	 */
//	CRefundInfo findRefundInfo(String dealId, int tradeId, long buyerUin, String mk);
}
