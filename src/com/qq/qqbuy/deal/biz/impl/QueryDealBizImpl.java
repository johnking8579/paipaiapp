package com.qq.qqbuy.deal.biz.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import com.paipai.lang.uint32_t;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.Pager;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.deal.biz.QueryDealBiz;
import com.qq.qqbuy.deal.po.DealCancelInfo;
import com.qq.qqbuy.deal.po.DealDetailInfo;
import com.qq.qqbuy.deal.po.DealListInfo;
import com.qq.qqbuy.deal.po.DealStateList;
import com.qq.qqbuy.deal.util.DealConstant;
import com.qq.qqbuy.deal.util.DealConverter;
import com.qq.qqbuy.deal.util.DealUtil;
import com.qq.qqbuy.thirdparty.idl.IDLResult;
import com.qq.qqbuy.thirdparty.idl.deal.DealClient;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.CloseDealReq;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.CloseDealResp;
import com.qq.qqbuy.thirdparty.idl.dealpc.InfoType;
import com.qq.qqbuy.thirdparty.idl.dealpc.PcDealQueryClient;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CDealInfo;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CTradoInfo;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.EmployeeInfoPo;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.GetDealInfoList2Resp;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.GetEmployeeListReq;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.GetEmployeeListResp;
import com.qq.qqbuy.thirdparty.idl.stat.StatClient;

public class QueryDealBizImpl implements QueryDealBiz {
	
	StatClient statClient = new StatClient();
	
	/**
	 * 注意:查询条件dealState 不等于 订单详情中的dealState
	 */
	@Override
	public DealListInfo queryDealList(long buyerUin, int dealState, int rateState, 
			int pageNo, int pageSize, short historyFlag, String mk) {
		boolean isHistory = historyFlag==0?false:true;
		int infoType = InfoType.or(InfoType.DEAL,InfoType.TRADE_ALL);
		Pager<CDealInfo> pager = PcDealQueryClient.getDealList(buyerUin,
				dealState, rateState, pageNo, pageSize, infoType, historyFlag);
		
		DealListInfo ret = new DealListInfo();
		ret.setCountTotal(pager.getTotalCount());
		ret.setPageIndex(pageNo);
		ret.setPageTotal(DealUtil.getPageNum(pager.getTotalCount(), pageSize));
		
		if(dealState == DealConstant.STATE_PAY_REFUND)	{	//查询退款订单时, 要关联查找refund
			List<CDealInfo> refundings = new ArrayList<CDealInfo>();
			infoType = InfoType.or(InfoType.DEAL, InfoType.TRADE_ALL, InfoType.REFUND);
			for(CDealInfo d : pager.getElements())	{
				CDealInfo dealWithRefund = PcDealQueryClient.sysGetDealInfo(d.getDisDealId(), isHistory, infoType, mk);
				refundings.add(dealWithRefund == null ? d : dealWithRefund);
			}
			DealConverter.convertDealList(refundings, ret, false);
		} else	{
			DealConverter.convertDealList(pager.getElements(), ret, false);
		}
		return ret;
	}
	
	/**
	 * 统计订单个数
	 * @param uin
	 * @return key=统计口径值, value=订单个数
	 */
	@Override
	public Map<String,Integer> getDealStatInfoByUin(long uin, String mk)	{
		Map<uint32_t, Integer> statmap = statClient.getDealStatInfoByUin(uin);
		Map<String,Integer> ret = new HashMap<String,Integer>();
		for(Entry<uint32_t, Integer> e : statmap.entrySet())	{
			ret.put(e.getKey().intValue()+"", e.getValue());
		}
		
		//查询待评价个数
		try {
			Pager<CDealInfo> eval = PcDealQueryClient.getDealList(uin, DealConstant.STATE_SUCCESS_ALL, DealConstant.RATE_STATE_NEED_BUYER, 1, 1, InfoType.DEAL, 0);
			ret.put(DealConstant.RATE_STATE_NEED_BUYER+"", new Long(eval.getTotalCount()).intValue());	//由于statClient.getDealStatInfoByUin()没有待评价数, 所以自定义一个值
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//查询退款中的子订单数量
		try {
			ret.put("1000", calcRefundingTrades(uin, mk));
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return ret;
	}
	
	/**
	 * 统计该用户所有的退款中的子订单数量.如果该订单是全额退款,按一个订单算
	 * @param buyerUin
	 * @param mk
	 * @return
	 */
	private int calcRefundingTrades(long buyerUin, String mk)	{
		int ps = 20;
		Pager<CDealInfo> pager = this.queryRefundingDeals(buyerUin, 1, ps, 0, mk);
		long maxPageNo = pager.getTotalCount()/ps;
		if(pager.getTotalCount()%ps > 0)
			maxPageNo++;
		List<CDealInfo> all = pager.getElements();
		for(int pageNo=2; pageNo<=maxPageNo; pageNo++)	{
			all.addAll(this.queryRefundingDeals(buyerUin, pageNo, ps, 0, mk).getElements());
		}
		
		int count = 0;
		for(CDealInfo d : all){
			//当查询条件中dealState=8时, 返回的dateState=各种值. 当订单是全额退且未发货,++
			if(d.getDealState() == DealConstant.STATE_PAY_OK && d.getDealRefundState() == 1)	{	
				count++;
			} else	{
				for(CTradoInfo t : d.getTradeList())	{
					if(t.getTradeRefundState() >= DealConstant.STATE_REFUND_START 
							&& t.getTradeRefundState()<=DealConstant.STATE_REFUND_WAIT_AGREE)
						count++;
				}
			}
		}
		return count;
	}
	
	
	/**
	 * 查询退款中的订单, 要关联查找出子订单的退款信息
	 * @param buyerUin
	 * @param dealState
	 * @param pageNo
	 * @param pageSize
	 * @param historyFlag
	 * @param mk
	 * @return
	 */
	public Pager<CDealInfo> queryRefundingDeals(long buyerUin, int pageNo, int pageSize, int historyFlag, String mk) {
		int infoType = InfoType.or(InfoType.DEAL, InfoType.TRADE_ALL);	//即使加上InfoType.Refund,在列表中也查不到退款信息,深研没有回应
		Pager<CDealInfo> pager = PcDealQueryClient.getDealList(buyerUin, DealConstant.STATE_PAY_REFUND, 0, pageNo, pageSize, infoType, historyFlag);
		infoType = InfoType.or(InfoType.DEAL, InfoType.TRADE_ALL, InfoType.REFUND);
		boolean isHistory = historyFlag==0?false:true;
		
		List<CDealInfo> refundings = new ArrayList<CDealInfo>();
		for(CDealInfo d : pager.getElements())	{
			CDealInfo dealWithRefund = PcDealQueryClient.sysGetDealInfo(d.getDisDealId(), isHistory, infoType, mk);
			if(dealWithRefund != null)
				refundings.add(dealWithRefund);
			else
				refundings.add(d);
		}
		pager.setElements(refundings);
		return pager;
	}
	
	
	@Override
	public DealCancelInfo cancelDeal(long buyerUin, String skey, String mk,
			String dealCode, long closeReason, String sid, String lskey) {
		StringBuilder buf = new StringBuilder();
		buf.append(" buyerUin:").append(buyerUin).append(" dealCode:").append(
				dealCode).append(" closeReason：").append(closeReason);
		DealCancelInfo ret = new DealCancelInfo();
		DealClient dealClient = new DealClient();
		CloseDealReq req = new CloseDealReq();
		req.setSource("mqgo");
		req.setDealId(dealCode);
		req.setCloseReason(closeReason);
		req.setSceneId(3);//系统关闭，不对用户是否设置交易密码做校验
		try {
			IDLResult<CloseDealResp> result = dealClient.closeDeal(req, buyerUin,skey);

			if (result != null && result.isSucceed()) {
				ret.setCloseReasonDesc(DealUtil.getCloseReasonDesc((int)closeReason));
				ret.setDealCode(dealCode);
			} else if (result != null) {
				ret.errCode = result.getBizErrCode();
				ret.msg = "取消失败, " + DealUtil.getDealErrMsg((int) ret.errCode)
						+ ". 请重新操作!";
			}
		} catch (BusinessException e) {
			ret.errCode = e.getErrCode();
			ret.msg = e.getErrMsg();
		}
		return ret;
	}
	
	/**
	 * 获取订单详情
	 */
	@Override
	public DealDetailInfo dealDetail(long buyerUin, String dealId, boolean isHistory, long listItem, String mk) {
		int infoType = InfoType.DEAL;
		if(listItem == 1)	infoType = InfoType.or(InfoType.DEAL, InfoType.TRADE_ALL, InfoType.TRADE_EXT, InfoType.REFUND);	//把退款协议&扩展信息也查出来
		CDealInfo deal = PcDealQueryClient.sysGetDealInfo(dealId, isHistory, infoType, mk);
		DealDetailInfo ret = new DealDetailInfo();
		DealConverter.convertDealDetail(deal, ret, false);
		return ret;
	}
	
	/**
	 * 获取员工列表
	 */
	@Override
	public Vector<EmployeeInfoPo> getEmployeeList(long sellerUin, String mk,String sk,long qq,String clientip) {
		GetEmployeeListReq req = new GetEmployeeListReq();
		req.setSellerUin(sellerUin);
		req.setMachineKey(mk);
		req.setSource("paipaiApp");
		// 1、调用idl
		GetEmployeeListResp res = new GetEmployeeListResp();
		res= PcDealQueryClient.queryEmployee(req,sk,qq,clientip);
		if(res!=null){
			Log.run.info("QueryDealBizImpl:getEmployeeList-->res:"+res.getEmployeeInfoPo());
			return res.getEmployeeInfoPo();
		}else {
			Log.run.info("QueryDealBizImpl:getEmployeeList-->res:空");
			return null;
		}
		
	}
	
	@Override
	public DealStateList queryDealStatesList(long uin, int ver) {
		DealStateList ret = new DealStateList();
		// BuyerSearchDealListRequest req = new
		// BuyerSearchDealListRequest();
		// req.setUin(uin);
		// req.setListItem(0);
		// req.setBuyerUin(uin);
		// req.setPageIndex(1);
		// req.setPageSize(1);
		// req.setSid(sid);
		// req.setLskey(lskey);
		//
		// // 1、获取订单总数
		// OpenApiProxy proxy = null;
		// proxy = OpenApi.getProxy();
		// BuyerSearchDealListResponse res = proxy.buyerSearchDealList(req);
		// if (res != null && res.isSucceed())
		// {
		// ret.setTotalCount(res.countTotal);
		// }

		// 1、获取订单总数
		GetDealInfoList2Resp list2Resp = PcDealQueryClient.getDealStateNum(
				uin, DealConstant.STATE_START, 0);
		if (list2Resp != null && list2Resp.getResult() == 0) {
			ret.setTotalCount(list2Resp.getTotalNum());
		}

		// 2、版本大于等于1时，返回待评价订单和交易完成订单数量
		if (ver >= 1) {
			// 2.1、待评价订单
			list2Resp = PcDealQueryClient.getDealStateNum(uin,
					DealConstant.STATE_SUCCESS_ALL,
					DealConstant.RATE_STATE_NEED_BUYER);
			if (list2Resp != null && list2Resp.getResult() == 0) {
				ret.setWaitBuyerEvalCount(list2Resp.getTotalNum());
			}
			// 2.2、交易完成订单
			list2Resp = PcDealQueryClient.getDealStateNum(uin,
					DealConstant.STATE_SUCCESS_ALL, 0);
			if (list2Resp != null && list2Resp.getResult() == 0) {
				ret.setEndNormalCount(list2Resp.getTotalNum());
			}
		}
		
		// 3、版本大于等于2时，返回货到付款待发货、待收货订单数量
		if (ver >= 2) {
			// 3.1、货到付款待发货
			list2Resp = PcDealQueryClient.getDealStateNum(uin, 
					DealConstant.STATE_COD_WAIT_SHIP, 0);
			if (list2Resp != null && list2Resp.getResult() == 0) {
				ret.setCodWaitSellerDeliveryCount(list2Resp.getTotalNum());
			}
			// 3.2、货到付款待收货
			list2Resp = PcDealQueryClient.getDealStateNum(uin, 
					DealConstant.STATE_COD_SHIP_OK, 0);
			if (list2Resp != null && list2Resp.getResult() == 0) {
				ret.setCodWaitBuyerReceiveCount(list2Resp.getTotalNum());
			}
		}
		
		// 4.1在线支付待付款
		list2Resp = PcDealQueryClient.getDealStateNum(uin, 
				DealConstant.STATE_WAIT_PAY, 0);
		if (list2Resp != null && list2Resp.getResult() == 0) {
			ret.setWaitBuyerPayCount(list2Resp.getTotalNum());
		}
		
		// 4.2 在线支付待发货
		list2Resp = PcDealQueryClient.getDealStateNum(uin, 
				DealConstant.STATE_PAY_OK, 0);
		if (list2Resp != null && list2Resp.getResult() == 0) {
			ret.setWaitSellerDeliveryCount(list2Resp.getTotalNum());
		}
		
		// 4.3在线支付待收货
		list2Resp = PcDealQueryClient.getDealStateNum(uin, 
				DealConstant.STATE_SHIPPING_OK, 0);
		if (list2Resp != null && list2Resp.getResult() == 0) {
			ret.setWaitBuyerReceiveCount(list2Resp.getTotalNum());
		}
		
//			// 4、获取其它状态的数量
//			GetAllStateDealCountByUinReq allStateReq = new GetAllStateDealCountByUinReq();
//			allStateReq.setUin(uin);
//			allStateReq.setDealType(1);
//			GetAllStateDealCountByUinResp resp = DealClient
//					.getDealStateNum(allStateReq);
//			// 设置返回值
//			if (resp != null && resp.getErrCode() == 0) {
//				Map<Integer, Integer> mp = resp.getPaipaiDealCount();
//				if (mp != null) {
//					if (mp.get(3) != null) {
//						ret.setWaitSellerDeliveryCount(mp.get(3));
//					}
//					if (mp.get(4) != null) {
//						ret.setWaitBuyerPayCount(mp.get(4));
//					}
//					if (mp.get(5) != null) {
//						ret.setWaitBuyerReceiveCount(mp.get(5));
//					}
//				}
//			} else if (resp != null) {
//				ret.errCode = resp.result;
//				ret.msg = resp.getErrMsg();
//			}
		return ret;
	}
	
}
