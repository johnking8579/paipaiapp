package com.qq.qqbuy.deal.util;


import com.qq.qqbuy.common.constant.C2CDefineConstant;
import com.qq.qqbuy.deal.po.NewOrderInfo;
import com.qq.qqbuy.deal.po.OrderInfo;
import com.qq.qqbuy.item.util.VipDiscount;
import com.qq.qqbuy.thirdparty.idl.userinfo.UserInfoClient;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.FindBuyerVIPAccountResp;

/* @FileName: BizUtil.java    
 * @Description: TODO 
 * @author: homerwu   
 * @date:2012-12-11 下午02:55:22    
 */
public class BizUtil {
	
	 public static final int ORDER_PRICE_TYPE_RECOMMAND = 15; //促销价

	public static int getPriceTypeByUserInfo(OrderInfo order, long uin) {
		UserInfoClient client = new UserInfoClient();
		//GetFlagLevel4SingleResp superQQLevel = client.getSuperQQLevel(uin);
		FindBuyerVIPAccountResp buyerVipLevel = client.findBuyerVIPAccount(uin);
		//if (superQQLevel != null && superQQLevel.getErrCode() == 0 && buyerVipLevel != null && buyerVipLevel.getErrCode() == 0) {
		if (buyerVipLevel != null && buyerVipLevel.getErrCode() == 0) {
			order.setDiamondLevel(buyerVipLevel.getBuyerVIPAccountInfo().getDwLevel());
			//order.setSuperQQLevel(superQQLevel.getFlagLevel());
			return VipDiscount.getPriceType(buyerVipLevel.getBuyerVIPAccountInfo().getDwLevel(), 0,uin);
		}
		return 0;
	}
	
	public static int getDiscountTypeByUserInfo(OrderInfo order,long uin) {
		UserInfoClient client = new UserInfoClient();
		//GetFlagLevel4SingleResp superQQLevel = client.getSuperQQLevel(uin);
		FindBuyerVIPAccountResp buyerVipLevel = client.findBuyerVIPAccount(uin);
		if ( buyerVipLevel != null) {
			order.setDiamondLevel(buyerVipLevel.getBuyerVIPAccountInfo().getDwLevel());
			//order.setSuperQQLevel(superQQLevel.getFlagLevel());
			return VipDiscount.getLevel(buyerVipLevel.getBuyerVIPAccountInfo().getDwLevel(), 0);
		}
		return 0;
	}
	
	public static int getDiscountTypeByUserInfo(NewOrderInfo order,long uin) {
		UserInfoClient client = new UserInfoClient();
		//GetFlagLevel4SingleResp superQQLevel = client.getSuperQQLevel(uin);
		FindBuyerVIPAccountResp buyerVipLevel = client.findBuyerVIPAccount(uin);
		if (buyerVipLevel != null) {
			order.setDiamondLevel(buyerVipLevel.getBuyerVIPAccountInfo().getDwLevel());
			//order.setSuperQQLevel(superQQLevel.getFlagLevel());
			return VipDiscount.getLevel(buyerVipLevel.getBuyerVIPAccountInfo().getDwLevel(), 0);
		}
		return 0;
	}
	
	public static int  getMtFromProperty(long property) {
		if ((property & 0x01)> 0) {
			return C2CDefineConstant.POST_COD_ZJS;
		} else if ((property & 0x02) > 0){
			return C2CDefineConstant.POST_COD_SF;
		}  else if ((property & 0x04) > 0){
			return C2CDefineConstant.POST_COD_YS;
		}else if ((property & 0x08) > 0){
			return C2CDefineConstant.POST_COD_YT;
		}
		return 0;
	}
	
}
