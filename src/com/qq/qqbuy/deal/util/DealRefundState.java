package com.qq.qqbuy.deal.util;


/**
 * enum DealRefundStateFlag_E
{
     DEAL_BIZ_FLAG_NORMAL = 0, //没有退款
     DEAL_BIZ_FLAG_FULL_REFUND = 1, //全额退款
     DEAL_BIZ_FLAG_REFUND = 2, //退款流程
     DEAL_BIZ_FLAG_COD = 3, //货到付款流程
     DEAL_BIZ_FLAG_B2C = 4, //b2c订单流程
    
     DEAL_BIZ_FLAG_ALL_REFUND = 100, //查询用，退款流程订单(1和2的组合状态)
};
只是CDealInfo.dealRefundState和CTradoInfo.dealRefundState取这些值.
CTradoInfo.tradeRefundState的值不是这个, 还是
 */
public enum DealRefundState {
	没有退款(0),
	全额退款(1),
	退款流程(2),
	货到付款流程(3),
	b2c订单流程(4),
	查询用退款流程订单(100);
	
	public static DealRefundState create(long code)	{
		for (DealRefundState ds : values()) {
			if (ds.code == code)
				return ds;
		}
		return null;
	}
	
	
	private long code;
	
	private DealRefundState(int code)	{
		this.code = code;
	}
	
	public long getCode()	{
		return code;
	}
	
}
