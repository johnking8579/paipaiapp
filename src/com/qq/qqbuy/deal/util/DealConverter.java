package com.qq.qqbuy.deal.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import com.qq.qqbuy.common.util.ImageUrlUtil;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.TssHelper;
import com.qq.qqbuy.deal.po.DealCmdyInfo;
import com.qq.qqbuy.deal.po.DealDetailInfo;
import com.qq.qqbuy.deal.po.DealListInfo;
import com.qq.qqbuy.deal.po.DealListItemInfo;
import com.qq.qqbuy.dealopr.po.DealPayType;
import com.qq.qqbuy.dealopr.po.DealRateState;
import com.qq.qqbuy.dealopr.po.DealState;
import com.qq.qqbuy.dealopr.po.DealType;
import com.qq.qqbuy.item.constant.TransportType;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CDealInfo;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CRefundInfo;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CTradoInfo;

/**
 * 
 * @ClassName： DealConverter
 * 
 * @Description： 将IDLresponse映射为接口返回字段
 * @author wamiwen
 * @date 2013-7-19 下午01:25:45
 * 
 */
public class DealConverter {

	/**
	 * 
	 * @Title: convertDealDetail
	 * 
	 * @Description: 将IDL返回映射为订单详情接口返回对象
	 * @param @param dealSrc
	 * @param @param dealRet
	 * @param @param isNeedChangeImageUrl 设定文件
	 * @return void 返回类型
	 * @throws
	 */
	public static void convertDealDetail(CDealInfo dealSrc,
			DealDetailInfo dealRet, boolean isNeedChangeImageUrl) {
		if (dealSrc == null || dealRet == null) {
			return;
		}
		dealRet.setBuyerName(StringUtil.isEmpty(dealSrc.getBuyerName()) ? dealSrc.getBuyerNick(): dealSrc.getBuyerName());
		dealRet.setBuyerRecvRefund(dealSrc.getBuyerRecvRefund());
		dealRet.setBuyerRemark(dealSrc.getBuyerBuyRemark());
		dealRet.setBuyerUin(dealSrc.getBuyerUin());
		dealRet.setCouponFee(dealSrc.getCouponFee());
		dealRet.setCreateTime(DealUtil.parseDateToString(dealSrc.getDealCreateTime()));
		dealRet.setDealCode(dealSrc.getDisDealId());
		dealRet.setDealDesc(dealSrc.getDealDesc());
		dealRet.setDealEndTime(DealUtil.parseDateToString(dealSrc.getDealEndTime()));
		dealRet.setDealNote(dealSrc.getDealNote());
		//dealRet.setSellerNote(dealSrc.getDealNote());
		dealRet.setDealPayFeePoint(dealSrc.getDealPayFeeScore());
		dealRet.setDealPayFeeTicket(dealSrc.getDealPayFeeTicket());
		dealRet.setDealPayFeeTotal(dealSrc.getDealPayFeeTotal());
		dealRet.setDealPayType(DealPayType.getEnumByCode(dealSrc.getDealPayType()).name());
		dealRet.setDealPayTypeDesc(DealPayType.getEnumByCode(dealSrc.getDealPayType()).getShowMeg());
		dealRet.setDealRateState(dealSrc.getDealRateState()+"");
		dealRet.setDealRateStateDesc(DealRateState.getEnumByCode((int) dealSrc.getDealRateState()).getShowMeg());
		
		
		//交易成功，但是实际支付金额为0的订单，将其作为交易关闭处理
		if(0 == dealSrc.getSellerRecvRefund() && DealState.DS_DEAL_END_NORMAL.getCode() == dealSrc.getDealState()){
			//dealRet.setDealState("CGI_DEALDETAIL_STATE_CLOSE");
			//注释原因为：发现CGI_DEALDETAIL_STATE_CLOSE为接口返回值，而武汉值并没有在页面返回值中定义，DS_CLOSED中已定义
			dealRet.setDealState("DS_CLOSED");
			dealRet.setDealStateDesc("交易已关闭");
		}else{
			dealRet.setDealState(DealState.getDealStateByCode(dealSrc.getDealState()).name());
			String dealStateDesc = DealState.getDealStateByCode(dealSrc.getDealState()).getDesc();
			dealRet.setDealStateDesc(DealUtil.filterDealStateDesc(dealStateDesc));
		}
				
				
//		dealRet.setDealState(DealState.getDealStateByCode(
//				dealSrc.getDealState()).name());
//		if ((DealState.DS_DEAL_END_NORMAL.getCode() == dealSrc.getDealState())
//				&& (dealSrc.getSellerRecvRefund() == 0L)) {
//			dealRet.setDealState(DealState.DS_CLOSED.name());
//		}
		
		
		String dealStateDesc = DealState.getDealStateByCode(
				dealSrc.getDealState()).getDesc();
		dealRet.setDealStateDesc(DealUtil.filterDealStateDesc(dealStateDesc));
		
		
		dealRet.setPropertyMask(dealSrc.getPropertymask() + "");
		dealRet.setDealType(DealType.getEnumByCode(dealSrc.getDealType())
				.name());
		dealRet.setDealTypeDesc(DealType.getEnumByCode(dealSrc.getDealType())
				.getShowMeg());
		dealRet.setFreight(dealSrc.getDealPayFeeShipping());
		dealRet.setHasInvoice(StringUtil.isNotEmpty(dealSrc
				.getDealInvoiceTitle()) ? 1 : 0);
		dealRet.setInvoiceContent(dealSrc.getDealInvoiceContent());
		dealRet.setInvoiceTitle(dealSrc.getDealInvoiceTitle());
		dealRet.setLastUpdateTime(DealUtil.parseDateToString(dealSrc
				.getLastUpdateTime()));
		dealRet.setPayReturnTime(DealUtil.parseDateToString(dealSrc
				.getPayReturnTime()));
		dealRet.setPayTime(DealUtil.parseDateToString(dealSrc.getPayTime()));
		dealRet.setReceiverAddress(dealSrc.getReceiveAddr());
		dealRet.setReceiverMobile(dealSrc.getStrReceiveMobile());
		dealRet.setReceiverName(dealSrc.getReceiveName());
		dealRet.setReceiverPhone(dealSrc.getReceiveTel());
		dealRet.setReceiverPostcode(dealSrc.getReceivePostcode());
		dealRet.setRecvfeeReturnTime(DealUtil.parseDateToString(dealSrc
				.getRecvfeeReturnTime()));
		dealRet.setRecvfeeTime(DealUtil.parseDateToString(dealSrc
				.getRecvfeeTime()));
		dealRet.setSellerConsignmentTime(DealUtil.parseDateToString(dealSrc
				.getSellerConsignmentTime()));
		dealRet.setSellerCrm(dealSrc.getSellerCrm() + "");
		dealRet.setSellerName(dealSrc.getSellerName());
		dealRet.setSellerRecvRefund(dealSrc.getSellerRecvRefund());
		dealRet.setSellerUin(dealSrc.getSellerUin());
		if (StringUtil.isNumeric(dealSrc.getShippingfeeCalc())) {
			dealRet.setShippingfeeCalc(Long.parseLong(dealSrc.getShippingfeeCalc()));
		}
		dealRet.setTenpayCode(dealSrc.getDealCftPayid());
		dealRet.setTotalCash(dealSrc.getDealPayFeeCash());
		// add by wanghao start
		String mailType = TransportType.valueOf(dealSrc.getMailType()).name();
		String mailTypeMeg = TransportType.valueOf(dealSrc.getMailType()).getShowMeg();
		int mailTypeCode = TransportType.valueOf(dealSrc.getMailType()).getTypeCode();
		int payTypeCode = DealPayType.getEnumByCode(dealSrc.getDealPayType()).getCode();
		//当发货方式未知时，如果付款方式为货到付款，则发货方式为 货到付款
		if(mailTypeCode == TransportType.TRANSPORT_UNKNOWN.getTypeCode() && payTypeCode == DealPayType.OFF_LINE.getCode()){
			mailType = TransportType.TRANSPORT_OFFLINE.name();
			mailTypeMeg = TransportType.TRANSPORT_OFFLINE.getShowMeg();
		}
		//end
		dealRet.setTransportType(mailType);
		dealRet.setTransportTypeDesc(mailTypeMeg);
		dealRet.setWhoPayShippingfee(dealSrc.getWhoPayShippingfee());
		dealRet.setWuliuId(dealSrc.getWuliuId() + "");

		long totalCmdyFee = 0; //商品总价
		long totalCmdyDiscount = 0; //商品总折扣
//		long voucherDiscount = 0; // 代金券优惠
		Vector<CTradoInfo> tradeList = dealSrc.getTradeList();
		RefundCalculator calculator = new RefundCalculator();
		if (tradeList != null && tradeList.size() > 0) {
			List<DealCmdyInfo> itemList = dealRet.getItemList();
			for (CTradoInfo cTradoInfo : tradeList) {
				if (cTradoInfo == null) {
					continue;
				}
				DealCmdyInfo dealCmdyInfo = new DealCmdyInfo();
				convertDealCmdy(cTradoInfo, dealCmdyInfo, isNeedChangeImageUrl);
				totalCmdyFee += dealCmdyInfo.getItemDealPrice()
						* dealCmdyInfo.getItemDealCount();
				totalCmdyDiscount += dealCmdyInfo.getItemAdjustPrice()- dealCmdyInfo.getItemDiscountFee();
				
				//计算子订单的最大退款值和最小退款值
				dealCmdyInfo.setMinRefundAmount(calculator.calcMinRefundToBuyer(dealSrc, cTradoInfo));
				dealCmdyInfo.setMaxRefundAmount(calculator.calcMaxRefundToBuyer(dealSrc, cTradoInfo));
				
				itemList.add(dealCmdyInfo);
				
//				if (voucherDiscount <= 0) {  //candeladiao，只需从一个Trade中取出即可，多个Trade的信息是一样的，不适合叠加数据
//					voucherDiscount = extractVoucherAmount(cTradoInfo.getExtInfo());
//				}
			}
		}
		long totalCouponFee = 0;
		if (dealSrc.getDealPayType() == DealConstant.DEAL_PAY_TYPE_CODE_TENPAY) {
			totalCouponFee = dealRet.getDealPayFeeTotal() - dealRet.getFreight() - totalCmdyFee;
		} else if (dealSrc.getDealPayType() == DealConstant.DEAL_PAY_TYPE_CODE_OFF_LINE) {
			totalCouponFee = totalCmdyDiscount + dealSrc.getCouponFee();// + voucherDiscount;
			dealRet.setFreight(dealRet.getDealPayFeeTotal() - totalCmdyFee - totalCouponFee);
		}
		dealRet.setTotalCouponFee(totalCouponFee);

	}

	/**
	 * 
	 * @Title: convertDealList
	 * 
	 * @Description: 将IDL返回映射为订单列表接口返回对象
	 * @param @param vecDealInfo
	 * @param @param ret
	 * @param @param isNeedChangeImageUrl 设定文件
	 * @return void 返回类型
	 * @throws
	 */
	public static void convertDealList(List<CDealInfo> vecDealInfo, DealListInfo ret, boolean isNeedChangeImageUrl) {
		if (vecDealInfo == null || vecDealInfo.size() <= 0 || ret == null) {
			return;
		}
		List<DealListItemInfo> dealList = ret.getDealList();
		for (CDealInfo cDealInfo : vecDealInfo) {
			if (cDealInfo == null) {
				continue;
			}
			DealListItemInfo dealListItem = new DealListItemInfo();
			dealListItem.setCreateTime(DealUtil.parseDateToString(cDealInfo.getDealCreateTime()));
			dealListItem.setDealCode(cDealInfo.getDisDealId());
			//add by wanghao start
			dealListItem.setDealType(DealType.getEnumByCode(cDealInfo.getDealType()).name());
			dealListItem.setDealTypeDesc(DealType.getEnumByCode(cDealInfo.getDealType()).getShowMeg());
			//add by wanghao end
			 //交易成功，但是实际支付金额为0的订单，将其作为交易关闭处理
			if(0 == cDealInfo.getSellerRecvRefund() && DealState.DS_DEAL_END_NORMAL.getCode() == cDealInfo.getDealState()){
				dealListItem.setDealState("CGI_DEALDETAIL_STATE_CLOSE");
				dealListItem.setDealStateDesc("交易已关闭");
			}else{
				dealListItem.setDealState(DealState.getDealStateByCode(cDealInfo.getDealState()).name());
				String dealStateDesc = DealState.getDealStateByCode(cDealInfo.getDealState()).getDesc();
				dealListItem.setDealStateDesc(DealUtil.filterDealStateDesc(dealStateDesc));
			}
			
			int dealRateState = (int) cDealInfo.getDealRateState();
			dealListItem.setDealRateState(dealRateState+"");
			dealListItem.setDealRateStateDesc(DealUtil.getDealRateStateDesc(dealRateState));
			dealListItem.setFreight(cDealInfo.getDealPayFeeShipping());
			dealListItem.setLastUpdateTime(DealUtil.parseDateToString(cDealInfo.getLastUpdateTime()));
			dealListItem.setSellerName(cDealInfo.getSellerName());
			dealListItem.setSellerUin(cDealInfo.getSellerUin());
			dealListItem.setTotalCash(cDealInfo.getDealPayFeeCash());
			dealListItem.setTotalFee(cDealInfo.getDealPayFeeTotal());
			dealListItem.setTransportType(TransportType.valueOf(cDealInfo.getMailType()).name());
			dealListItem.setTransportTypeDesc(TransportType.valueOf(cDealInfo.getMailType()).getShowMeg());
			dealListItem.setDealPayType(DealPayType.getEnumByCode(cDealInfo.getDealPayType()).name());
			dealListItem.setDealPayTypeDesc(DealPayType.getEnumByCode(cDealInfo.getDealPayType()).getShowMeg());
			dealListItem.setDealRefundState(cDealInfo.getDealRefundState());
			dealListItem.setStrReceiveMobile(cDealInfo.getStrReceiveMobile());

			Vector<CTradoInfo> tradeList = cDealInfo.getTradeList();
			if (tradeList != null && tradeList.size() > 0) {
				List<DealCmdyInfo> itemList = dealListItem.getItemList();
				for (CTradoInfo cTradoInfo : tradeList) {
					if (cTradoInfo == null) {
						continue;
					}
					DealCmdyInfo dealCmdyInfo = new DealCmdyInfo();
					convertDealCmdy(cTradoInfo, dealCmdyInfo, isNeedChangeImageUrl);
					itemList.add(dealCmdyInfo);
				}
			}

			dealList.add(dealListItem);
		}
	}

	/**
	 * 
	 * @Title: convertDealCmdy
	 * 
	 * @Description: IDL返回的订单商品对象映射为接口订单商品对象
	 * @param @param cmdySrc
	 * @param @param cmdyRet
	 * @param @param isNeedChangeImageUrl 设定文件
	 * @return void 返回类型
	 * @throws
	 */
	public static void convertDealCmdy(CTradoInfo cmdySrc, DealCmdyInfo cmdyRet, boolean isNeedChangeImageUrl) {
		if (cmdySrc == null || cmdyRet == null) {
			return;
		}
		
		Vector<CRefundInfo> refunds = cmdySrc.getRefundList();
		if(refunds != null && refunds.size()>1)	{
			Collections.sort(refunds, new Comparator<CRefundInfo>() {
				@Override
				public int compare(CRefundInfo o1, CRefundInfo o2) {
					return (int)(o2.getRefundReqTime() - o1.getRefundReqTime());
				}
			});
		}
		if(refunds != null && !refunds.isEmpty()){
			cmdyRet.setLatestRefundInfo(refunds.get(0));
		}
		 
		cmdyRet.setItemAdjustPrice(cmdySrc.getItemAdjustPrice());
		if (cmdySrc.getDisItemId() != null  && cmdySrc.getDisItemId().length() > 18) {
			cmdyRet.setItemCode(new StringBuffer(cmdySrc.getDisItemId()).replace(14, 16, "00").toString());
		}
		cmdyRet.setItemCodeHistory(cmdySrc.getDisItemId());
		cmdyRet.setItemDealCount(cmdySrc.getDealItemCount());
		cmdyRet.setItemDealPrice(cmdySrc.getItemPrice());
		// cmdyRet.setItemFlag(itemFlag);
		cmdyRet.setItemName(cmdySrc.getItemName());
		String itemPic80 = TssHelper.getImgUrl(cmdySrc.getItemLogo(), "", false);
		if (isNeedChangeImageUrl) {
			cmdyRet.setItemPic80(ImageUrlUtil.genImageUrl(itemPic80,ImageUrlUtil.DEFAULTFormat80));
		} else {
			cmdyRet.setItemPic80(ImageUrlUtil.genImageUrl(itemPic80,ImageUrlUtil.DEFAULTFormat160));
		}
		cmdyRet.setItemRetailPrice(cmdySrc.getItemOriginalPrice());
		if (cmdySrc.getTradeRefundId() > 0L) {
			cmdyRet.setRefundState(cmdySrc.getTradeRefundState());
		}
		cmdyRet.setRefundStateDesc(DealState.getDealStateByCode(cmdySrc.getTradeRefundState()).getDesc());
		cmdyRet.setStockAttr(cmdySrc.getItemAttrOptionValue());
		cmdyRet.setStockLocalCode(cmdySrc.getStockLocalCode());
		
		// 2013-10-16 红包抵扣
		cmdyRet.setItemDiscountFee(cmdySrc.getDiscountFee());
		// end
		cmdyRet.setDealSubCode(cmdySrc.getTradeId() + "");
		cmdyRet.setItemDealState(DealState.getDealStateByCode(cmdySrc.getTradeState()).name());
		String dealStateDesc = DealState.getDealStateByCode(cmdySrc.getTradeState()).getDesc();
		cmdyRet.setItemDealStateDesc(DealUtil.filterDealStateDesc(dealStateDesc));
		
		cmdyRet.setBuyerFee(cmdySrc.getBuyerFee());
		cmdyRet.setSellerFee(cmdySrc.getSellerFee());
	}
}
