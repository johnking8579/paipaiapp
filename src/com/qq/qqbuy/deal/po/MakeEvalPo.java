package com.qq.qqbuy.deal.po;

import java.util.LinkedList;
import java.util.List;

import com.paipai.util.string.StringUtil;

public class MakeEvalPo {
	/**
	 * 子订单id
	 */
	private String tradeId;
	
	/**
	 * 评价内容
	 */
	private String content;
	
	/**
	 * 评价等级
	 */
	private int level;
	
	/**
	 * 差评理由id
	 */
	private List<Integer> reasons = new LinkedList<Integer>();

	public String getTradeId() {
		return tradeId;
	}
	
	/**
	 * 
	 * @Title: createTradeId
	 *
	 * @Description: 根据dealCode和dealSubCode生成tradeId
	 * @param @param dealCode
	 * @param @param dealSubCode
	 * @param @return  设定文件
	 * @return String  返回类型
	 * @throws
	 */
	public void createTradeId(String dealCode, String dealSubCode) {
		if (StringUtil.isEmpty(dealCode)
				|| StringUtil.isEmpty(dealSubCode)) {
			return;
		}
		int index = dealCode.lastIndexOf("-");
		if (index < 1) {
			return;
		}
		this.tradeId = dealCode.substring(0, index + 1) + dealSubCode;
	}

	public void setTradeId(String tradeId) {
		this.tradeId = tradeId;
	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public List<Integer> getReasons() {
		return reasons;
	}

	public void setReasons(List<Integer> reasons) {
		this.reasons = reasons;
	}
	
	/**
	 * 
	 * @Title: createReasons
	 *
	 * @Description: 根据reasonStr生成reason List
	 * @param @param reasonStr  设定文件
	 * @return void  返回类型
	 * @throws
	 */
	public void createReasons(String reasonStr) {
		if (StringUtil.isEmpty(reasonStr)) {
			return;
		}
		String[] list = reasonStr.split("-");
		if (list == null || list.length < 1) {
			return;
		}
		List<Integer> reasonList = new LinkedList<Integer>();
		if (list.length > 0) {
			reasonList.add(Integer.valueOf(list[0]));
		}
		if (list.length > 1) {
			reasonList.add(Integer.valueOf(list[1]));
		}
		
		this.reasons = reasonList;
	}

	@Override
	public String toString() {
		return "MakeEvalPo [content=" + content + ", level=" + level
				+ ", reasons=" + reasons + ", tradeId=" + tradeId + "]";
	}

}
