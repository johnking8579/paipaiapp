package com.qq.qqbuy.deal.po;

import com.qq.qqbuy.deal.util.DealConstant;
import com.qq.qqbuy.item.util.DispFormater;

/**
 * 根据运费模版计算的运送费用
 * @author winsonwu
 * @Created 2012-12-28
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class ShipCalcVo implements DealConstant {

	// 运送方式： 0=免运费, 1=快递, 2=平邮, 3=EMS, 4=货到付款
	private int type;
	
	// 邮递类型：下单接口使用
	private int mailType;
	
	// 支付类型：下单接口使用
	private int payType;
	//同城
	public static String RULE_TYPE_ONE_CITY = "同城" ;
	//默认
	public static String RULE_TYPE_DEFAULT = "默认" ;

	
	// 预送方式名称
	private String name;
	
	// 运费，单位为分
	private long fee;
	
	// 分单返回原始费用
	private long mailFee;
	
	private static ShipCalcVo FREE = new ShipCalcVo(SHIPCALC_TYPE_FREE, "免运费", 0, MAIL_TYPE_NONE, PAYTYPE_OLP);
	
	public static ShipCalcVo getShipCalcInfo(int type, long fee) {
		String name;
		switch (type) {
		case SHIPCALC_TYPE_FREE:
			return FREE;
		case SHIPCALC_TYPE_EXPRESS: 
			name = "快递: ￥" + DispFormater.priceDispFormater(fee) + "元";
			return new ShipCalcVo(SHIPCALC_TYPE_EXPRESS, name, fee, MAIL_TYPE_EXPRESS, PAYTYPE_OLP);
		case SHIPCALC_TYPE_NORMAL:
			name = "平邮: ￥" + DispFormater.priceDispFormater(fee) + "元";
			return new ShipCalcVo(SHIPCALC_TYPE_NORMAL, name, fee, MAIL_TYPE_NORMAL, PAYTYPE_OLP);
		case SHIPCALC_TYPE_EMS:
			name = "EMS: ￥" + DispFormater.priceDispFormater(fee) + "元";
			return new ShipCalcVo(SHIPCALC_TYPE_EMS, name, fee, MAIL_TYPE_EMS, PAYTYPE_OLP);
		case SHIPCALC_TYPE_COD_ZJS:
			name = "货到付款: ￥" + DispFormater.priceDispFormater(fee) + "元";
			return new ShipCalcVo(SHIPCALC_TYPE_COD_ZJS, name, fee, MAIL_TYPE_COD_ZJS, PAYTYPE_COD);
		case SHIPCALC_TYPE_COD_SF:
			name = "货到付款: ￥" + DispFormater.priceDispFormater(fee) + "元";
			return new ShipCalcVo(SHIPCALC_TYPE_COD_SF, name, fee, MAIL_TYPE_COD_SF, PAYTYPE_COD);
		case SHIPCALC_TYPE_COD_YS:
			name = "货到付款: ￥" + DispFormater.priceDispFormater(fee) + "元";
			return new ShipCalcVo(SHIPCALC_TYPE_COD_YS, name, fee, MAIL_TYPE_COD_YS, PAYTYPE_COD);
		case SHIPCALC_TYPE_COD_YT:
			name = "货到付款: ￥" + DispFormater.priceDispFormater(fee) + "元";
			return new ShipCalcVo(SHIPCALC_TYPE_COD_YT, name, fee, MAIL_TYPE_COD_YT, PAYTYPE_COD);
		}
		return null;
	}

	private ShipCalcVo(int type, String name, long fee, int mailType, int payType) {
		this.type = type;
		this.name = name;
		this.fee = fee;
		this.mailFee = fee;
		this.mailType = mailType;
		this.payType = payType;
	}
	
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getFee() {
		return fee;
	}

	public void setFee(long fee) {
		this.fee = fee;
	}

	public long getMailFee() {
		return mailFee;
	}

	public void setMailFee(long mailFee) {
		this.mailFee = mailFee;
	}

	public int getMailType() {
		return mailType;
	}

	public void setMailType(int mailType) {
		this.mailType = mailType;
	}

	public int getPayType() {
		return payType;
	}

	public void setPayType(int payType) {
		this.payType = payType;
	} 
	
}
