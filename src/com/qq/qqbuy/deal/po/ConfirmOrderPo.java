package com.qq.qqbuy.deal.po;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author winsonwu
 * @Created 2012-12-28 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class ConfirmOrderPo  {

	// 卖家QQ号
	private long sellerUin;

	// 买家QQ号
	private long buyerUin;

	// 当前选中的收货地址id
	private int adid;

	/************************************* 购物车购买商品 **************************************/
	// 支付方式：货到付款、在线支付
	private int payType;

	// 商品购买列表：itemCode-itemAttr-buyNum~itemCode1-itemAttr1-buyNum1~ ......格式进行组装
	private String itemList;

	/************************************* 单个购买商品 ****************************************/
	// 商品ID
	private String itemCode;

	// 购买商品库存属性
	private String itemAttr = "";

	// 购买数量
	private long buyNum;
	
	private int ver = 1;
	
	private String sk;
	
	private String mk;
	/**
	 * 是否使用红包进行批价，0：不使用，1：使用
	 */
	private int useRedPackage = 0;

	public ConfirmOrderPo(long buyerUin, String itemCode, String itemAttr,
			long buyNum, int adid) {
		this.buyerUin = buyerUin;
		this.itemCode = itemCode;
		this.itemAttr = itemAttr;
		this.buyNum = buyNum;
		this.adid = adid;
	}

	public ConfirmOrderPo(long buyerUin, int payType, String itemList, int adid) {
		this.buyerUin = buyerUin;
		this.payType = payType;
		this.itemList = itemList;
		this.adid = adid;
	}

	public ConfirmOrderPo(long buyerUin, int payType, String itemList, int adid,int useRedPackage) {
		this.buyerUin = buyerUin;
		this.payType = payType;
		this.itemList = itemList;
		this.adid = adid;
		this.useRedPackage = useRedPackage;
	}

	public Map<String, String> validate() {
		// TODO winsonwu
		return new HashMap<String, String>();
	}

	public long getBuyerUin() {
		return buyerUin;
	}

	public void setBuyerUin(long buyerUin) {
		this.buyerUin = buyerUin;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemAttr() {
		return itemAttr;
	}

	public void setItemAttr(String itemAttr) {
		this.itemAttr = itemAttr;
	}

	public long getBuyNum() {
		return buyNum;
	}

	public void setBuyNum(long buyNum) {
		this.buyNum = buyNum;
	}

	public int getAdid() {
		return adid;
	}

	public void setAdid(int adid) {
		this.adid = adid;
	}

	public int getPayType() {
		return payType;
	}

	public void setPayType(int payType) {
		this.payType = payType;
	}

	public String getItemList() {
		return itemList;
	}

	public void setItemList(String itemList) {
		this.itemList = itemList;
	}

	public long getSellerUin() {
		return sellerUin;
	}

	public void setSellerUin(long sellerUin) {
		this.sellerUin = sellerUin;
	}

	public String getSk() {
		return sk;
	}

	public void setSk(String sk) {
		this.sk = sk;
	}

    public int getVer() {
        return ver;
    }

    public void setVer(int ver) {
        this.ver = ver;
    }

	public String getMk() {
		return mk;
	}

	public void setMk(String mk) {
		this.mk = mk;
	}


	public int getUseRedPackage() {
		return useRedPackage;
	}

	public void setUseRedPackage(int useRedPackage) {
		this.useRedPackage = useRedPackage;
	}
}
