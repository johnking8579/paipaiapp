package com.qq.qqbuy.deal.po;

/**
 * 申请退款表单
 * 
 * @author JingYing
 * @date 2014年11月27日
 */
public class ApplyRefundForm {

	private long buyer, seller;
	private String dealId, tradeId;
	private int isRecItem, isReqItem;	//是否收到货, 是否退货. 0否1是
	private String refundApplyMsg;

	/**
	 * 5 与卖家协商一致退款 6 卖家虚假标记发货 7 商品缺货无法发货 8 物流公司运送问题 0 其他退款原因
	 */
	private int refundReasonType;
	private long refundToBuyerAmount; // 退给买家多少钱, 单位分

	public long getBuyer() {
		return buyer;
	}

	public void setBuyer(long buyer) {
		this.buyer = buyer;
	}

	public long getSeller() {
		return seller;
	}

	public void setSeller(long seller) {
		this.seller = seller;
	}

	public String getDealId() {
		return dealId;
	}

	public void setDealId(String dealId) {
		this.dealId = dealId;
	}

	public String getTradeId() {
		return tradeId;
	}

	public void setTradeId(String tradeId) {
		this.tradeId = tradeId;
	}

	public int getIsRecItem() {
		return isRecItem;
	}

	public void setRecItem(int isRecItem) {
		this.isRecItem = isRecItem;
	}

	public int getIsReqItem() {
		return isReqItem;
	}

	public void setReqItem(int isReqItem) {
		this.isReqItem = isReqItem;
	}

	public String getRefundApplyMsg() {
		return refundApplyMsg;
	}

	public void setRefundApplyMsg(String refundApplyMsg) {
		this.refundApplyMsg = refundApplyMsg;
	}

	public int getRefundReasonType() {
		return refundReasonType;
	}

	public void setRefundReasonType(int refundReasonType) {
		this.refundReasonType = refundReasonType;
	}

	public long getRefundToBuyerAmount() {
		return refundToBuyerAmount;
	}

	public void setRefundToBuyerAmount(long refundToBuyerAmount) {
		this.refundToBuyerAmount = refundToBuyerAmount;
	}

}
