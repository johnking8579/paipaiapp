package com.qq.qqbuy.deal.po;

import com.qq.qqbuy.common.po.CommonPo;

/**
 * 
 * @ClassName: DealStateList
 * 
 * @Description:
 * @author wendyhu
 * @date 2012-12-13 下午03:06:15
 */
public class DealStateList extends CommonPo
{
    /**
     * 订单总数
     */
    private long totalCount = 0;

    /**
     * 待发货数
     */
    private long waitSellerDeliveryCount = 0;
    
    /**
     * 货到付款待发货数
     */
    private long codWaitSellerDeliveryCount = 0;

    /**
     * 待付款
     */
    private long waitBuyerPayCount = 0;

    /**
     * 待确认收货数
     */
    private long waitBuyerReceiveCount = 0;
    
    /**
     * 货到付款待收货数
     */
    private long codWaitBuyerReceiveCount = 0;
    
    /**
     * 待评价数
     */
    private long waitBuyerEvalCount = 0;
    
    /**
     * 交易成功数
     */
    private long endNormalCount = 0;
    
    public long getTotalCount()
    {
        return totalCount;
    }

    public void setTotalCount(long totalCount)
    {
        this.totalCount = totalCount;
    }

    public long getWaitSellerDeliveryCount()
    {
        return waitSellerDeliveryCount;
    }

    public void setWaitSellerDeliveryCount(long waitSellerDeliveryCount)
    {
        this.waitSellerDeliveryCount = waitSellerDeliveryCount;
    }

    public long getWaitBuyerPayCount()
    {
        return waitBuyerPayCount;
    }

    public void setWaitBuyerPayCount(long waitBuyerPayCount)
    {
        this.waitBuyerPayCount = waitBuyerPayCount;
    }

    public long getWaitBuyerReceiveCount()
    {
        return waitBuyerReceiveCount;
    }

    public void setWaitBuyerReceiveCount(long waitBuyerReceiveCount)
    {
        this.waitBuyerReceiveCount = waitBuyerReceiveCount;
    }
    
    public long getWaitBuyerEvalCount() {
		return waitBuyerEvalCount;
	}

	public void setWaitBuyerEvalCount(long waitBuyerEvalCount) {
		this.waitBuyerEvalCount = waitBuyerEvalCount;
	}

	public long getEndNormalCount() {
		return endNormalCount;
	}

	public void setEndNormalCount(long endNormalCount) {
		this.endNormalCount = endNormalCount;
	}
	
	public long getCodWaitSellerDeliveryCount() {
		return codWaitSellerDeliveryCount;
	}

	public void setCodWaitSellerDeliveryCount(long codWaitSellerDeliveryCount) {
		this.codWaitSellerDeliveryCount = codWaitSellerDeliveryCount;
	}

	public long getCodWaitBuyerReceiveCount() {
		return codWaitBuyerReceiveCount;
	}

	public void setCodWaitBuyerReceiveCount(long codWaitBuyerReceiveCount) {
		this.codWaitBuyerReceiveCount = codWaitBuyerReceiveCount;
	}
}
