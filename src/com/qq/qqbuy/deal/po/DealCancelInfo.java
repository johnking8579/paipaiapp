package com.qq.qqbuy.deal.po;


import com.qq.qqbuy.common.po.CommonPo;

/**
 * 
 * @ClassName: DealListInfo
 * 
 * @Description: 关闭订单
 * @author wendyhu
 * @date 2012-12-13 下午07:21:44
 */
public class DealCancelInfo  extends CommonPo
{
    /**
     * 订单Code
     */
    private  String dealCode = "";
    
    /**
     * 关闭订单原因
     */
    private String closeReasonDesc = "";

    public String getDealCode()
    {
        return dealCode;
    }

    public void setDealCode(String dealCode)
    {
        this.dealCode = dealCode;
    }

    public String getCloseReasonDesc()
    {
        return closeReasonDesc;
    }

    public void setCloseReasonDesc(String closeReasonDesc)
    {
        this.closeReasonDesc = closeReasonDesc;
    }

    @Override
    public String toString()
    {
        return "DealCancelInfo [closeReasonDesc=" + closeReasonDesc
                + ", dealCode=" + dealCode + ", errCode=" + errCode + ", msg="
                + msg + ", retCode=" + retCode + "]";
    }
}
