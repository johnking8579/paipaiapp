package com.qq.qqbuy.deal.po;

import com.qq.qqbuy.thirdparty.openapi.po.GetDealDetailResponse;

/* @FileName: OrderResp.java    
 * @Description: TODO 
 * @author: homerwu   
 * @date:2013-8-14 下午02:25:17    
 */
public class OrderResp {

	private GetDealDetailResponse dealDetail = new GetDealDetailResponse();

	public GetDealDetailResponse getDealDetail() {
		return dealDetail;
	}

	public void setDealDetail(GetDealDetailResponse dealDetail) {
		this.dealDetail = dealDetail;
	}
	
	
}
