package com.qq.qqbuy.deal.po;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.EmployeeInfoPo;

public class DealDetailInfo  
{

    /**
     * buyerName:买家名称
     */
    private String buyerName = null;

    /**
     * buyerUin:买家QQ号
     */
    private long buyerUin;

    /**
     * buyerRemark:买家下订单时的备注
     */
    private String buyerRemark = null;

    /**
     * dealCode:订单编码
     */
    private String dealCode = null;

    /**
     * dealDesc:订单序列号
     */
    private String dealDesc = null;

    /**
     * dealNoteType:订单备注类型
     * RED:红色
     * YELLOW:黄色
     * GREEN:绿色
     * BLUE:蓝色
     * PINK:粉红色
     * UN_LABEL:未标注
     */
    private String dealNoteType = null;

    /**
     * dealNote:订单的备注内容
     */
    private String dealNote = null;

    /**
     * dealState:订单状态
     */
    private String dealState = null;

    /**
     * dealStateDesc:订单状态说明
     */
    private String dealStateDesc = null;

    /**
     * dealType:订单类型
     */
    private String dealType = null;

    /**
     * dealTypeDesc:订单类型描述
     */
    private String dealTypeDesc = null;

    /**
     * dealPayType:订单的支付方式
     */
    private String dealPayType = null;

    /**
     * dealPayTypeDesc:订单支付方式说明
     */
    private String dealPayTypeDesc = null;

    /**
     * dealRateState:订单的评价状态
     */
    private String dealRateState = null;

    /**
     * dealRateStateDesc:订单的评价状态说明
     */
    private String dealRateStateDesc = null;

    /**
     * createTime:订单的创建时间
     */
    private String createTime = null;

    /**
     * dealEndTime:订单结束时间
     */
    private String dealEndTime = null;

    /**
     * lastUpdateTime:订单的最后更新时间
     */
    private String lastUpdateTime = null;

    /**
     * payTime:买家的付款时间
     */
    private String payTime = null;

    /**
     * payReturnTime:打款返回时间
     */
    private String payReturnTime = null;

    /**
     * recvfeeReturnTime:退款返回时间
     */
    private String recvfeeReturnTime = null;

    /**
     * recvfeeTime:退款时间
     */
    private String recvfeeTime = null;

    /**
     * sellerConsignmentTime:   卖家发货时间
     */
    private String sellerConsignmentTime = null;

    /**
     * hasInvoice:是否提供发票:0否,1是
     */
    private long hasInvoice;

    /**
     * invoiceContent:发票内容
     */
    private String invoiceContent = null;

    /**
     * invoiceTitle:发票标题
     */
    private String invoiceTitle = null;

    /**
     * tenpayCode:财付通付款单号
     */
    private String tenpayCode = null;

    /**
     * transportType:运送类型
     */
    private String transportType = null;

    /**
     * transportTypeDesc:运费类型说明
     */
    private String transportTypeDesc = null;

    /**
     * whoPayShippingfee:   承担运费方式
     */
    private long whoPayShippingfee;

    /**
     * wuliuId:物流id
     */
    private String wuliuId = null;

    /**
     * receiverAddress:收货人详细地址
     */
    private String receiverAddress = null;

    /**
     * receiverMobile:收货人手机号码
     */
    private String receiverMobile = null;

    /**
     * receiverName:收货人姓名
     */
    private String receiverName = null;

    /**
     * receiverPhone:收货人电话号码
     */
    private String receiverPhone = null;

    /**
     * receiverPostcode:收货人邮编,为空时返回0
     */
    private String receiverPostcode = null;

    /**
     * sellerRecvRefund:退款:卖家实收金额
     */
    private long sellerRecvRefund;

    /**
     * buyerRecvRefund:退款:买家收到的退款金额
     */
    private long buyerRecvRefund;

    /**
     * couponFee:折扣优惠金额
     */
    private long couponFee;

    /**
     * dealPayFeePoint:实际积分支付金额
     */
    private long dealPayFeePoint;

    /**
     * dealPayFeeTicket:财付券支付金额
     */
    private long dealPayFeeTicket;

    /**
     * dealPayFeeTotal:费用合计
     */
    private long dealPayFeeTotal;

    /**
     * freight: 支付的运费(单位:分)
     */
    private long freight;

    /**
     * shippingfeeCalc:运费合计说明
     */
    private long shippingfeeCalc;

    /**
     * totalCash:买家支付现金总额
     */
    private long totalCash;

    /**
     * sellerCrm:客服CRM
     */
    private String sellerCrm = null;

    /**
     * sellerName:卖家名称
     */
    private String sellerName = null;
    
    /**
     * 卖家评论
     */
    private String sellerNote = "";

    /**
     * sellerUin:卖家QQ号
     */
    private long sellerUin;
    
    /**
     * 订单属性
     */
    private String propertyMask = "";
    
    /** itemList:订单的商品列表 */
    private List<DealCmdyInfo> itemList = new ArrayList<DealCmdyInfo>();
    
    
    /**
     * 总的活动优惠金额
     */
    private long totalCouponFee = 0;
    
    /**
     * 卖家的员工列表
     */
    private Vector<EmployeeInfoPo> employeeList = new Vector<EmployeeInfoPo>();
    
    public String getPropertyMask()
    {
        return propertyMask;
    }

    public void setPropertyMask(String propertyMask)
    {
        this.propertyMask = propertyMask;
    }

    public String getSellerNote()
    {
        return sellerNote;
    }

    public void setSellerNote(String sellerNote)
    {
        this.sellerNote = sellerNote;
    }

    public long getTotalCouponFee()
    {
        return totalCouponFee;
    }

    public void setTotalCouponFee(long totalCouponFee)
    {
        this.totalCouponFee = totalCouponFee;
    }

    public String getBuyerName()
    {
        return buyerName;
    }

    public void setBuyerName(String buyerName)
    {
        this.buyerName = buyerName;
    }

    public long getBuyerUin()
    {
        return buyerUin;
    }

    public void setBuyerUin(long buyerUin)
    {
        this.buyerUin = buyerUin;
    }

    public String getBuyerRemark()
    {
        return buyerRemark;
    }

    public void setBuyerRemark(String buyerRemark)
    {
        this.buyerRemark = buyerRemark;
    }

    public String getDealCode()
    {
        return dealCode;
    }

    public void setDealCode(String dealCode)
    {
        this.dealCode = dealCode;
    }

    public String getDealDesc()
    {
        return dealDesc;
    }

    public void setDealDesc(String dealDesc)
    {
        this.dealDesc = dealDesc;
    }

    public String getDealNoteType()
    {
        return dealNoteType;
    }

    public void setDealNoteType(String dealNoteType)
    {
        this.dealNoteType = dealNoteType;
    }

    public String getDealNote()
    {
        return dealNote;
    }

    public void setDealNote(String dealNote)
    {
        this.dealNote = dealNote;
    }

    public String getDealState()
    {
        return dealState;
    }

    public void setDealState(String dealState)
    {
        this.dealState = dealState;
    }

    public String getDealStateDesc()
    {
        return dealStateDesc;
    }

    public void setDealStateDesc(String dealStateDesc)
    {
        this.dealStateDesc = dealStateDesc;
    }

    public String getDealType()
    {
        return dealType;
    }

    public void setDealType(String dealType)
    {
        this.dealType = dealType;
    }

    public String getDealTypeDesc()
    {
        return dealTypeDesc;
    }

    public void setDealTypeDesc(String dealTypeDesc)
    {
        this.dealTypeDesc = dealTypeDesc;
    }

    public String getDealPayType()
    {
        return dealPayType;
    }

    public void setDealPayType(String dealPayType)
    {
        this.dealPayType = dealPayType;
    }

    public String getDealPayTypeDesc()
    {
        return dealPayTypeDesc;
    }

    public void setDealPayTypeDesc(String dealPayTypeDesc)
    {
        this.dealPayTypeDesc = dealPayTypeDesc;
    }

    public String getDealRateState()
    {
        return dealRateState;
    }

    public void setDealRateState(String dealRateState)
    {
        this.dealRateState = dealRateState;
    }

    public String getDealRateStateDesc()
    {
        return dealRateStateDesc;
    }

    public void setDealRateStateDesc(String dealRateStateDesc)
    {
        this.dealRateStateDesc = dealRateStateDesc;
    }

    public String getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(String createTime)
    {
        this.createTime = createTime;
    }

    public String getDealEndTime()
    {
        return dealEndTime;
    }

    public void setDealEndTime(String dealEndTime)
    {
        this.dealEndTime = dealEndTime;
    }

    public String getLastUpdateTime()
    {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(String lastUpdateTime)
    {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getPayTime()
    {
        return payTime;
    }

    public void setPayTime(String payTime)
    {
        this.payTime = payTime;
    }

    public String getPayReturnTime()
    {
        return payReturnTime;
    }

    public void setPayReturnTime(String payReturnTime)
    {
        this.payReturnTime = payReturnTime;
    }

    public String getRecvfeeReturnTime()
    {
        return recvfeeReturnTime;
    }

    public void setRecvfeeReturnTime(String recvfeeReturnTime)
    {
        this.recvfeeReturnTime = recvfeeReturnTime;
    }

    public String getRecvfeeTime()
    {
        return recvfeeTime;
    }

    public void setRecvfeeTime(String recvfeeTime)
    {
        this.recvfeeTime = recvfeeTime;
    }

    public String getSellerConsignmentTime()
    {
        return sellerConsignmentTime;
    }

    public void setSellerConsignmentTime(String sellerConsignmentTime)
    {
        this.sellerConsignmentTime = sellerConsignmentTime;
    }

    public long getHasInvoice()
    {
        return hasInvoice;
    }

    public void setHasInvoice(long hasInvoice)
    {
        this.hasInvoice = hasInvoice;
    }

    public String getInvoiceContent()
    {
        return invoiceContent;
    }

    public void setInvoiceContent(String invoiceContent)
    {
        this.invoiceContent = invoiceContent;
    }

    public String getInvoiceTitle()
    {
        return invoiceTitle;
    }

    public void setInvoiceTitle(String invoiceTitle)
    {
        this.invoiceTitle = invoiceTitle;
    }

    public String getTenpayCode()
    {
        return tenpayCode;
    }

    public void setTenpayCode(String tenpayCode)
    {
        this.tenpayCode = tenpayCode;
    }

    public String getTransportType()
    {
        return transportType;
    }

    public void setTransportType(String transportType)
    {
        this.transportType = transportType;
    }

    public String getTransportTypeDesc()
    {
        return transportTypeDesc;
    }

    public void setTransportTypeDesc(String transportTypeDesc)
    {
        this.transportTypeDesc = transportTypeDesc;
    }

    public long getWhoPayShippingfee()
    {
        return whoPayShippingfee;
    }

    public void setWhoPayShippingfee(long whoPayShippingfee)
    {
        this.whoPayShippingfee = whoPayShippingfee;
    }

    public String getWuliuId()
    {
        return wuliuId;
    }

    public void setWuliuId(String wuliuId)
    {
        this.wuliuId = wuliuId;
    }

    public String getReceiverAddress()
    {
        return receiverAddress;
    }

    public void setReceiverAddress(String receiverAddress)
    {
        this.receiverAddress = receiverAddress;
    }

    public String getReceiverMobile()
    {
        return receiverMobile;
    }

    public void setReceiverMobile(String receiverMobile)
    {
        this.receiverMobile = receiverMobile;
    }

    public String getReceiverName()
    {
        return receiverName;
    }

    public void setReceiverName(String receiverName)
    {
        this.receiverName = receiverName;
    }

    public String getReceiverPhone()
    {
        return receiverPhone;
    }

    public void setReceiverPhone(String receiverPhone)
    {
        this.receiverPhone = receiverPhone;
    }

    public String getReceiverPostcode()
    {
        return receiverPostcode;
    }

    public void setReceiverPostcode(String receiverPostcode)
    {
        this.receiverPostcode = receiverPostcode;
    }

    public long getSellerRecvRefund()
    {
        return sellerRecvRefund;
    }

    public void setSellerRecvRefund(long sellerRecvRefund)
    {
        this.sellerRecvRefund = sellerRecvRefund;
    }

    public long getBuyerRecvRefund()
    {
        return buyerRecvRefund;
    }

    public void setBuyerRecvRefund(long buyerRecvRefund)
    {
        this.buyerRecvRefund = buyerRecvRefund;
    }

    public long getCouponFee()
    {
        return couponFee;
    }

    public void setCouponFee(long couponFee)
    {
        this.couponFee = couponFee;
    }

    public long getDealPayFeePoint()
    {
        return dealPayFeePoint;
    }

    public void setDealPayFeePoint(long dealPayFeePoint)
    {
        this.dealPayFeePoint = dealPayFeePoint;
    }

    public long getDealPayFeeTicket()
    {
        return dealPayFeeTicket;
    }

    public void setDealPayFeeTicket(long dealPayFeeTicket)
    {
        this.dealPayFeeTicket = dealPayFeeTicket;
    }

    public long getDealPayFeeTotal()
    {
        return dealPayFeeTotal;
    }

    public void setDealPayFeeTotal(long dealPayFeeTotal)
    {
        this.dealPayFeeTotal = dealPayFeeTotal;
    }

    public long getFreight()
    {
        return freight;
    }

    public void setFreight(long freight)
    {
        this.freight = freight;
    }

    public long getShippingfeeCalc()
    {
        return shippingfeeCalc;
    }

    public void setShippingfeeCalc(long shippingfeeCalc)
    {
        this.shippingfeeCalc = shippingfeeCalc;
    }

    public long getTotalCash()
    {
        return totalCash;
    }

    public void setTotalCash(long totalCash)
    {
        this.totalCash = totalCash;
    }

    public String getSellerCrm()
    {
        return sellerCrm;
    }

    public void setSellerCrm(String sellerCrm)
    {
        this.sellerCrm = sellerCrm;
    }

    public String getSellerName()
    {
        return sellerName;
    }

    public void setSellerName(String sellerName)
    {
        this.sellerName = sellerName;
    }

    public long getSellerUin()
    {
        return sellerUin;
    }

    public void setSellerUin(long sellerUin)
    {
        this.sellerUin = sellerUin;
    }

    public List<DealCmdyInfo> getItemList()
    {
        return itemList;
    }

    public void setItemList(List<DealCmdyInfo> itemList)
    {
        this.itemList = itemList;
    }

    public Vector<EmployeeInfoPo> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(Vector<EmployeeInfoPo> employeeList) {
		this.employeeList = employeeList;
	}

}
