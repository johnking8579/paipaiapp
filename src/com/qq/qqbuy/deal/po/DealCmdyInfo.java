package com.qq.qqbuy.deal.po;

import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CRefundInfo;

/**
 * 
 * @ClassName: DealItem
 * 
 * @Description: 订单里面的商品信息
 * 
 * @author wendyhu
 * @date 2012-12-13 下午07:15:40
 */
public class DealCmdyInfo
{

    /** itemCode:商品编码 */
    private String itemCode = "";

    /** itemCodeHistory:订单的商品快照编码 */
    private String itemCodeHistory = "";

    /** stockLocalCode:商品库存编码 */
    private String stockLocalCode = "";

    /** stockAttr:买家下单时选择的库存属性 */
    private String stockAttr = "";

    /** itemName:商品名称 */
    private String itemName = "";

    /** itemPic80:商品图片的url */
    private String itemPic80 = "";

    /** itemRetailPrice:商品的市场价格 */
    private long itemRetailPrice;

    /** itemDealPrice:买家下单时的商品价格 */
    private long itemDealPrice;

    /** itemAdjustPrice:订单的调整价格:正数为订单加价,负数为订单减价 */
    private long itemAdjustPrice;

    /** itemDealCount:购买的数量 */
    private long itemDealCount;

    /** itemFlag */
    private String itemFlag = "";

    /** refundState:退款状态，有退款时才有值 */
    private long refundState;

    /** refundStateDesc:退款状态描述，有退款时才有值 */
    private String refundStateDesc = "";

    /** availableAction */
    public String availableAction = null;

    /** itemDiscountFee:购买商品的红包值、折扣优惠价。。。 */
    public long itemDiscountFee;
    /** dealSubCode:子订单id。。。 */
    public String dealSubCode = null;

    /** itemDealState:子订单状态 **/
    public String itemDealState = "";

    /** itemDealStateDesc:子订单状态 描述 **/
    public String itemDealStateDesc = "";
    
    private CRefundInfo latestRefundInfo;		//子订单对应的最新一条退款单
    
    private long minRefundAmount, maxRefundAmount;	//子订单的最少退款额, 最大退款额
    private long buyerFee, sellerFee;

    public String getAvailableAction()
    {
        return availableAction;
    }

    public void setAvailableAction(String availableAction)
    {
        this.availableAction = availableAction;
    }

    public long getItemDiscountFee()
    {
        return itemDiscountFee;
    }

    public void setItemDiscountFee(long itemDiscountFee)
    {
        this.itemDiscountFee = itemDiscountFee;
    }

    public String getDealSubCode()
    {
        return dealSubCode;
    }

    public void setDealSubCode(String dealSubCode)
    {
        this.dealSubCode = dealSubCode;
    }

    public String getItemDealState()
    {
        return itemDealState;
    }

    public void setItemDealState(String itemDealState)
    {
        this.itemDealState = itemDealState;
    }

    public String getItemDealStateDesc()
    {
        return itemDealStateDesc;
    }

    public void setItemDealStateDesc(String itemDealStateDesc)
    {
        this.itemDealStateDesc = itemDealStateDesc;
    }

    public String getItemCode()
    {
        return itemCode;
    }

    public void setItemCode(String itemCode)
    {
        this.itemCode = itemCode;
    }

    public String getItemCodeHistory()
    {
        return itemCodeHistory;
    }

    public void setItemCodeHistory(String itemCodeHistory)
    {
        this.itemCodeHistory = itemCodeHistory;
    }

    public String getStockLocalCode()
    {
        return stockLocalCode;
    }

    public void setStockLocalCode(String stockLocalCode)
    {
        this.stockLocalCode = stockLocalCode;
    }

    public String getStockAttr()
    {
        return stockAttr;
    }

    public void setStockAttr(String stockAttr)
    {
        this.stockAttr = stockAttr;
    }

    public String getItemName()
    {
        return itemName;
    }

    public void setItemName(String itemName)
    {
        this.itemName = itemName;
    }

    public String getItemPic80()
    {
        return itemPic80;
    }

    public void setItemPic80(String itemPic80)
    {
        this.itemPic80 = itemPic80;
    }

    public long getItemRetailPrice()
    {
        return itemRetailPrice;
    }

    public void setItemRetailPrice(long itemRetailPrice)
    {
        this.itemRetailPrice = itemRetailPrice;
    }

    public long getItemDealPrice()
    {
        return itemDealPrice;
    }

    public void setItemDealPrice(long itemDealPrice)
    {
        this.itemDealPrice = itemDealPrice;
    }

    public long getItemAdjustPrice()
    {
        return itemAdjustPrice;
    }

    public void setItemAdjustPrice(long itemAdjustPrice)
    {
        this.itemAdjustPrice = itemAdjustPrice;
    }

    public long getItemDealCount()
    {
        return itemDealCount;
    }

    public void setItemDealCount(long itemDealCount)
    {
        this.itemDealCount = itemDealCount;
    }

    public String getItemFlag()
    {
        return itemFlag;
    }

    public void setItemFlag(String itemFlag)
    {
        this.itemFlag = itemFlag;
    }

    public long getRefundState()
    {
        return refundState;
    }

    public void setRefundState(long refundState)
    {
        this.refundState = refundState;
    }

    public String getRefundStateDesc()
    {
        return refundStateDesc;
    }

    public void setRefundStateDesc(String refundStateDesc)
    {
        this.refundStateDesc = refundStateDesc;
    }

	public CRefundInfo getLatestRefundInfo() {
		return latestRefundInfo;
	}

	public void setLatestRefundInfo(CRefundInfo latestRefundInfo) {
		this.latestRefundInfo = latestRefundInfo;
	}

	public long getMinRefundAmount() {
		return minRefundAmount;
	}

	public void setMinRefundAmount(long minRefundAmount) {
		this.minRefundAmount = minRefundAmount;
	}

	public long getMaxRefundAmount() {
		return maxRefundAmount;
	}

	public void setMaxRefundAmount(long maxRefundAmount) {
		this.maxRefundAmount = maxRefundAmount;
	}

	public long getBuyerFee() {
		return buyerFee;
	}

	public void setBuyerFee(long buyerFee) {
		this.buyerFee = buyerFee;
	}

	public long getSellerFee() {
		return sellerFee;
	}

	public void setSellerFee(long sellerFee) {
		this.sellerFee = sellerFee;
	}
	
}
