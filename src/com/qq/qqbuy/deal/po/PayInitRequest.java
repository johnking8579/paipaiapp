package com.qq.qqbuy.deal.po;

public class PayInitRequest implements java.io.Serializable {
	public static final String ver = "2.0";
	public int charset = 1;
	public int bank_type = 0;
	public String desc;
	public String purchaser_id;
	public String bargainor_id;
	public String sp_billno;
	/**
	 * 总金额,以分为单位
	 */
	public int total_fee;
	/**
	 * 现金支付币种,目前只支持人民币,默认值是1-人民币
	 */
	public int fee_type = 1;
	public String notify_url;
	public String callback_url;
	public String attach;
	/**
	 * 订单生成时间，格式为yyyymmddhhmmss，如2009年12月25日9点10分10秒表示为20091225091010。时区为GMT+8 beijing。该时间取自商户服务器 可选
	 */
	public String time_start;
	/**
	 * 订单失效时间，格式为yyyymmddhhmmss，如2009年12月27日9点10分10秒表示为20091227091010。时区为GMT+8 beijing。该时间取自商户服务器 可选
	 */
	public String time_expire;
	/**
	 * 可选
	 */
	public String transaction_id;
	public static final String sign_sp_id = "1000000101";

	/**
	 * 拍拍时间戳 linux时间戳
	 */
	public String sp_time_stamp;
	/**
	 * 产品价格，以分为单位
	 */
	public int price;
	/**
	 * 物流费用，以分为单位
	 */
	public int transport_fee;
	/**
	 * 现金支付金额，以分为单位
	 */
	public int fee1 = 0;
	/**
	 * 代金券金额
	 */
	public int fee2 = 0;
	/**
	 * 其它费用
	 */
	public int fee3 = 0;
	// -----------------------------
	// 以下为可选
	public String ticket_id;
	public String gwq_listid;
	public String mac;
	public String medi_sign;
	public String comm1;
	public String comm2;
	public String goods_tag;
	
	@Override
	public String toString() {
		return "PayInitRequest [attach=" + attach + ", bank_type=" + bank_type
				+ ", bargainor_id=" + bargainor_id + ", callback_url="
				+ callback_url + ", charset=" + charset + ", comm1=" + comm1
				+ ", comm2=" + comm2 + ", desc=" + desc + ", fee1=" + fee1
				+ ", fee2=" + fee2 + ", fee3=" + fee3 + ", fee_type="
				+ fee_type + ", goods_tag=" + goods_tag + ", gwq_listid="
				+ gwq_listid + ", mac=" + mac + ", medi_sign=" + medi_sign
				+ ", notify_url=" + notify_url + ", price=" + price
				+ ", purchaser_id=" + purchaser_id + ", sp_billno=" + sp_billno
				+ ", sp_time_stamp=" + sp_time_stamp + ", ticket_id="
				+ ticket_id + ", time_expire=" + time_expire + ", time_start="
				+ time_start + ", total_fee=" + total_fee + ", transaction_id="
				+ transaction_id + ", transport_fee=" + transport_fee + "]";
	}
}
