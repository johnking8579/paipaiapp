package com.qq.qqbuy.deal.po;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.qq.qqbuy.cmdy.po.CmdyItemVo;
import com.qq.qqbuy.cmdy.po.PromotionRuleVo;
import com.qq.qqbuy.item.util.DispFormater;

/**
 * @author winsonwu
 * @Created 2013-2-20 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class ConfirmPackageVo {

	// dealId
	private long dealId;
	// 卖家
	private long sellerUin;

	// 包裹总价
	private long totalPrice;

	// 包裹中总价展示
	private String disTotalPrice;

	// 运送方式选择列表
	private Vector<ShipCalcVo> shipCalcInfos = new Vector<ShipCalcVo>();

	// 包裹中商品列表
	private Vector<CmdyItemVo> items = new Vector<CmdyItemVo>();
	private String shopName;
	private int shopType; // 0:拍拍，1:商城
	
	private int promotion;// 0:不参加促销，1:参加促销

	private List<PromotionRuleVo> promotionRules = new ArrayList<PromotionRuleVo>();

	public long lastCalculateTotalPrice() {
		long total = 0;
		if (items != null && items.size() > 0) {
			for (CmdyItemVo item : items) {
				total += item.lastCalculateTotalPrice();
			}
		}
		totalPrice = total;
		disTotalPrice = DispFormater.priceDispFormater(total);

		return total;
	}

	public List<PromotionRuleVo> getPromotionRules() {
		return promotionRules;
	}

	public void setPromotionRules(List<PromotionRuleVo> promotionRules) {
		this.promotionRules = promotionRules;
	}

	public long getDealId() {
		return dealId;
	}

	public void setDealId(long dealId) {
		this.dealId = dealId;
	}

	public long getSellerUin() {
		return sellerUin;
	}

	public void setSellerUin(long sellerUin) {
		this.sellerUin = sellerUin;
	}

	public long getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(long totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getDisTotalPrice() {
		return disTotalPrice;
	}

	public void setDisTotalPrice(String disTotalPrice) {
		this.disTotalPrice = disTotalPrice;
	}

	public Vector<ShipCalcVo> getShipCalcInfos() {
		return shipCalcInfos;
	}

	public void setShipCalcInfos(Vector<ShipCalcVo> shipCalcInfos) {
		this.shipCalcInfos = shipCalcInfos;
	}

	public Vector<CmdyItemVo> getItems() {
		return items;
	}

	public void setItems(Vector<CmdyItemVo> items) {
		this.items = items;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public int getShopType() {
		return shopType;
	}

	public void setShopType(int shopType) {
		this.shopType = shopType;
	}

	public int getPromotion() {
		return promotion;
	}

	public void setPromotion(int promotion) {
		this.promotion = promotion;
	}

}
