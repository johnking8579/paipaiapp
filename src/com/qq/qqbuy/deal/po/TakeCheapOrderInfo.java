package com.qq.qqbuy.deal.po;

import com.qq.qqbuy.thirdparty.openapi.po.GetDealDetailResponse;

/**
 * Created by wanghao5 on 2015/1/6.
 */
public class TakeCheapOrderInfo {

    /**
     * 卖家qq
     */
    private long sellerUin;
    /**
     * 订单数据串，格式为：0,2,,,,,,,||0EF6FE4B00000000000000000BEC3F38,,1,0,65536,,140737488512428||1275000334,112
     *
     */
    private String orderData;
    /**
     * 地址信息串，格式为：狗一样，13254884884，河南省南阳市唐河县，就5888448
     * 收货人,手机号码,省市县,详细地址
     */
    private String addressStr;
    /**
     * 支付方式，0：在线支付 1：货到付款
     */
    private int payType;
    /**
     * 在线支付方式，货到付款时为0，在线支付时：0：财付通 2：微信支付 4：手q支付
     */
    private int paychannel;
    /**
     * 是否检查重复订单，1：不检查 "":检查
     */
    private String nocheckrepeat;
    /**
     * 收货地址id
     */
    private long addressid;
    /**
     * 时间戳
     */
    private long seq;
    /**
     * 地址类型，默认为0，使用手q地址时为2
     */
    private int addrType = 0;
    private int anonymous = 1;
    private int otherpay = 0;
    /**
     * 城市编码
     */
    private long regionid;
    /**
     * 详细地址
     */
    private String recvaddr;
    /**
     * 收货人
     */
    private String recvname;
    /**
     * 微信绑定手机号
     */
    private long wxpay_mobile;
    /**
     * 手机号
     */
    private long moblie;

    //团团降使用参数 start  tuan == 1 && groupnum > 0
    private int tuan = 1;
    /**
     * 满图案人数
     */
    private int groupnum;
    /**
     * 活动数量
     */
    private int activenum;
    /**
     * 团购ID
     */
    private long gid;
    //团团降使用参数 end
    /**
     * 吆喝信息
     */
    private String tuanMsg;
    /**
     * 当isJBT!=""时，为1
     */
    private int isJBT;
    /**
     * 大V团，isDav=1 且 promoteKey!="" 时isDav=2
     */
    private int isDav;
    /**
     * 大V团批价返回key
     */
    private String promoteKey;

    // pp侧下单后有值
    private GetTakeCheapDealDetailResponse dealDetail = new GetTakeCheapDealDetailResponse();

    public TakeCheapOrderInfo(){}

    public TakeCheapOrderInfo(long sellerUin,String orderData,String addressStr,int payType
                              ,int paychannel,long addressid,int addrType
                              ,int anonymous,int otherpay,long regionid
                              ,String recvaddr,String recvname,long wxpay_mobile
                              ,long moblie,int tuan,int groupnum,int activenum,long gid
                              ,String tuanMsg){
        this.sellerUin = sellerUin;
        this.orderData = orderData;
        this.addressStr = addressStr;
        this.payType = payType;
        this.paychannel = paychannel;
        this.addressid = addressid;
        this.addrType = addrType;
        this.anonymous = anonymous;
        this.otherpay = otherpay;
        this.regionid = regionid;
        this.recvaddr = recvaddr;
        this.recvname = recvname;
        this.wxpay_mobile = wxpay_mobile;
        this.moblie = moblie;
        this.tuan = tuan;
        this.groupnum = groupnum;
        this.activenum = activenum;
        this.gid = gid;
        this.tuanMsg = tuanMsg;
    }

    public String getOrderData() {
        return orderData;
    }

    public void setOrderData(String orderData) {
        this.orderData = orderData;
    }

    public String getAddressStr() {
        return addressStr;
    }

    public void setAddressStr(String addressStr) {
        this.addressStr = addressStr;
    }

    public int getPayType() {
        return payType;
    }

    public void setPayType(int payType) {
        this.payType = payType;
    }

    public int getPaychannel() {
        return paychannel;
    }

    public void setPaychannel(int paychannel) {
        this.paychannel = paychannel;
    }

    public String getNocheckrepeat() {
        return nocheckrepeat;
    }

    public void setNocheckrepeat(String nocheckrepeat) {
        this.nocheckrepeat = nocheckrepeat;
    }

    public long getAddressid() {
        return addressid;
    }

    public void setAddressid(long addressid) {
        this.addressid = addressid;
    }

    public long getSeq() {
        return seq;
    }

    public void setSeq(long seq) {
        this.seq = seq;
    }

    public int getAddrType() {
        return addrType;
    }

    public void setAddrType(int addrType) {
        this.addrType = addrType;
    }

    public int getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(int anonymous) {
        this.anonymous = anonymous;
    }

    public int getOtherpay() {
        return otherpay;
    }

    public void setOtherpay(int otherpay) {
        this.otherpay = otherpay;
    }

    public long getRegionid() {
        return regionid;
    }

    public void setRegionid(long regionid) {
        this.regionid = regionid;
    }

    public String getRecvaddr() {
        return recvaddr;
    }

    public void setRecvaddr(String recvaddr) {
        this.recvaddr = recvaddr;
    }

    public String getRecvname() {
        return recvname;
    }

    public void setRecvname(String recvname) {
        this.recvname = recvname;
    }

    public long getWxpay_mobile() {
        return wxpay_mobile;
    }

    public void setWxpay_mobile(long wxpay_mobile) {
        this.wxpay_mobile = wxpay_mobile;
    }

    public long getMoblie() {
        return moblie;
    }

    public void setMoblie(long moblie) {
        this.moblie = moblie;
    }

    public int getTuan() {
        return tuan;
    }

    public void setTuan(int tuan) {
        this.tuan = tuan;
    }

    public int getGroupnum() {
        return groupnum;
    }

    public void setGroupnum(int groupnum) {
        this.groupnum = groupnum;
    }

    public int getActivenum() {
        return activenum;
    }

    public void setActivenum(int activenum) {
        this.activenum = activenum;
    }

    public long getGid() {
        return gid;
    }

    public void setGid(long gid) {
        this.gid = gid;
    }

    public String getTuanMsg() {
        return tuanMsg;
    }

    public void setTuanMsg(String tuanMsg) {
        this.tuanMsg = tuanMsg;
    }

    public int getIsJBT() {
        return isJBT;
    }

    public void setIsJBT(int isJBT) {
        this.isJBT = isJBT;
    }

    public int getIsDav() {
        return isDav;
    }

    public void setIsDav(int isDav) {
        this.isDav = isDav;
    }

    public String getPromoteKey() {
        return promoteKey;
    }

    public void setPromoteKey(String promoteKey) {
        this.promoteKey = promoteKey;
    }

    public long getSellerUin() {
        return sellerUin;
    }

    public void setSellerUin(long sellerUin) {
        this.sellerUin = sellerUin;
    }

    public GetTakeCheapDealDetailResponse getDealDetail() {
        return dealDetail;
    }

    public void setDealDetail(GetTakeCheapDealDetailResponse dealDetail) {
        this.dealDetail = dealDetail;
    }
}
