package com.qq.qqbuy.deal.po;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.qq.qqbuy.common.util.Util;

public class MakeEvalListPo {
	/**
	 * 大订单ID
	 */
	private String dealId;
	
	/**
	 * 买家QQ
	 */
	private long buyerUin;
	
	/**
	 * 卖家QQ
	 */
	private long sellerUin;
	
	/**
	 * 商品和描述相符
	 */
	private long dsr1;
	
	/**
	 * 卖家的服务态度
	 */
	private long dsr2;
	
	/**
	 * 卖家的发货速度
	 */
	private long dsr3;
	
	/**
	 * 商品评价列表
	 */
	private List<MakeEvalPo> makeEvalPoList = new LinkedList<MakeEvalPo>();
	
	public static List<MakeEvalPo> parseFromJson(String dealId, String json)	{
		JsonArray arr = new JsonParser().parse(json).getAsJsonArray();
		List<MakeEvalPo> list = new ArrayList<MakeEvalPo>();
		String[] dealIdSplit = dealId.split("-");
		for(JsonElement e : arr)	{
			JsonObject j = e.getAsJsonObject();
			MakeEvalPo m = new MakeEvalPo();
			m.setContent(Util.htmlEscape(j.get("c").getAsString()));
			m.setTradeId(dealIdSplit[0] + "-" + dealIdSplit[1] + "-" + j.get("t").getAsString());
			m.setLevel(j.get("l").getAsInt());

			JsonElement r = j.get("r");
			if(r != null)	{
				List<Integer> ints = new ArrayList<Integer>();
				for(JsonElement je : r.getAsJsonArray())	{
					ints.add(je.getAsInt());
				}
				m.setReasons(ints);
			}

			list.add(m);
		}
		return list;
	}
	

	public MakeEvalListPo(String dealId, long buyerUin, long sellerUin,
			long dsr1, long dsr2, long dsr3) {
		this.dealId = dealId;
		this.buyerUin = buyerUin;
		this.sellerUin = sellerUin;
		this.dsr1 = dsr1;
		this.dsr2 = dsr2;
		this.dsr3 = dsr3;
	}
	
	
	
	public String getDealId() {
		return dealId;
	}

	public void setDealId(String dealId) {
		this.dealId = dealId;
	}

	public long getBuyerUin() {
		return buyerUin;
	}

	public void setBuyerUin(long buyerUin) {
		this.buyerUin = buyerUin;
	}

	public long getSellerUin() {
		return sellerUin;
	}

	public void setSellerUin(long sellerUin) {
		this.sellerUin = sellerUin;
	}

	public long getDsr1() {
		return dsr1;
	}

	public void setDsr1(long dsr1) {
		this.dsr1 = dsr1;
	}

	public long getDsr2() {
		return dsr2;
	}

	public void setDsr2(long dsr2) {
		this.dsr2 = dsr2;
	}

	public long getDsr3() {
		return dsr3;
	}

	public void setDsr3(long dsr3) {
		this.dsr3 = dsr3;
	}

	public List<MakeEvalPo> getMakeEvalPoList() {
		return makeEvalPoList;
	}

	public void setMakeEvalPoList(List<MakeEvalPo> makeEvalPoList) {
		this.makeEvalPoList = makeEvalPoList;
	}
	
}
