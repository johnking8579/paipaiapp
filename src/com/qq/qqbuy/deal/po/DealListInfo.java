package com.qq.qqbuy.deal.po;

import java.util.ArrayList;
import java.util.List;

import com.qq.qqbuy.common.po.CommonPo;

/**
 * 
 * @ClassName: DealListInfo
 * 
 * @Description: 订单列表
 * @author wendyhu
 * @date 2012-12-13 下午07:21:44
 */
public class DealListInfo  extends CommonPo
{

    /** pageIndex:当前为第几页 */
    private long pageIndex;

    /** pageTotal:总页数 */
    private long pageTotal;

    /** countTotal:满足查询条件的记录总数 */
    private long countTotal;

    /** dealList:订单列表 */
    private List<DealListItemInfo> dealList = new ArrayList<DealListItemInfo>();
    
    private String buyerMobile;

    public long getPageIndex()
    {
        return pageIndex;
    }

    public void setPageIndex(long pageIndex)
    {
        this.pageIndex = pageIndex;
    }

    public long getPageTotal()
    {
        return pageTotal;
    }

    public void setPageTotal(long pageTotal)
    {
        this.pageTotal = pageTotal;
    }

    public long getCountTotal()
    {
        return countTotal;
    }

    public void setCountTotal(long countTotal)
    {
        this.countTotal = countTotal;
    }

    public List<DealListItemInfo> getDealList()
    {
        return dealList;
    }

    public void setDealList(List<DealListItemInfo> dealList)
    {
        this.dealList = dealList;
    }
    
    public String getBuyerMobile() {
		return buyerMobile;
	}

	public void setBuyerMobile(String buyerMobile) {
		this.buyerMobile = buyerMobile;
	}

	@Override
    public String toString()
    {
        return "DealListInfo [countTotal=" + countTotal + ", dealList="
                + dealList + ", pageIndex=" + pageIndex + ", pageTotal="
                + pageTotal + ", errCode=" + errCode + ", msg=" + msg
                + ", retCode=" + retCode + "]";
    }


}
