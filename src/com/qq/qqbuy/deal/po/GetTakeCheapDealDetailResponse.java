package com.qq.qqbuy.deal.po;

/**
 * Created by wanghao5 on 2015/1/12.
 */
public class GetTakeCheapDealDetailResponse{

    /**
     * 拍便宜团id
     */
    private long gid;
    /**
     * 卖家qq
     */
    private long sellerUin;
    /**
     * 订单串
     */
    private String dealCode;

    public long getGid() {
        return gid;
    }

    public void setGid(long gid) {
        this.gid = gid;
    }

    public long getSellerUin() {
        return sellerUin;
    }

    public void setSellerUin(long sellerUin) {
        this.sellerUin = sellerUin;
    }

    public String getDealCode() {
        return dealCode;
    }

    public void setDealCode(String dealCode) {
        this.dealCode = dealCode;
    }
}
