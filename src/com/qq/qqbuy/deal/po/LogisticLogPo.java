package com.qq.qqbuy.deal.po;


/**
 *物流日志
 */
public class LogisticLogPo {

	private String deliveryTime ;
	 
	private String traceInfo ;

	public String getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public String getTraceInfo() {
		return traceInfo;
	}

	public void setTraceInfo(String traceInfo) {
		this.traceInfo = traceInfo;
	}

	

	
}
