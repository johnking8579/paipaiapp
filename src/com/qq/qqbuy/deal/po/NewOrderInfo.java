package com.qq.qqbuy.deal.po;

import com.qq.qqbuy.common.SpringHelper;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.item.biz.ItemBiz;
import com.qq.qqbuy.item.po.Mstock4J;
import com.qq.qqbuy.item.po.wap2.ItemResponse;
import com.qq.qqbuy.thirdparty.openapi.po.GetDealDetailResponse;




/**
 * 下单po
 * @author homerwu
 *
 */
public class NewOrderInfo {
	
	/************************************请求参数*****************************/
	private String sid;
	private String itemCode;
	private int payType;
	private int addressId;
	private long uin;
	private long itemCount;
	private int redPackageId;
	private String stockAttr;
	private int stockId;
	private String buyerNote ="";
	private String expressType;
	private String priceType;
	
	private String reserved; //主要用于pps参数
	/************************************vo*****************************/
	private ItemResponse item = new ItemResponse();//新商品vo
	
	// pp侧下单后有值
	private GetDealDetailResponse dealDetail = new GetDealDetailResponse();
	private long dealTotalFee;
	private long superQQDiscount;
	private int discountType;
	private int superQQLevel;
	private int diamondLevel;
	
	/************************************construction method*****************************/
	
	/**
	 * for wap submit
	 */
	public NewOrderInfo(String sid, String itemCode, int payType, int addressId,
			long uin, long itemCount, int redPackageId,
			int stockId, String buyerNote, String reserved,String sa) {
		this.sid = sid;
		this.itemCode = itemCode;
		this.payType = payType;
		this.addressId = addressId;
		this.uin = uin;
		this.itemCount = itemCount;
		this.redPackageId = redPackageId;
		this.stockId = stockId;
		this.buyerNote = buyerNote;
		this.reserved = reserved;
		this.stockAttr = sa;
	}
	
	/**
	 * for api submit
	 *
	 */
	public NewOrderInfo(String sid, String itemCode, int payType, int addressId,
			long uin, long itemCount, int redPackageId,
			String stockAttr, String buyerNote,
			String reserved, String expressType, String priceType) {
		this.sid = sid;
		this.itemCode = itemCode;
		this.payType = payType;
		this.addressId = addressId;
		this.uin = uin;
		this.itemCount = itemCount;
		this.redPackageId = redPackageId;
		this.stockAttr = stockAttr;
		this.buyerNote = buyerNote;
		this.reserved = reserved;
		this.expressType = expressType;
		this.priceType = priceType;
	}

	public NewOrderInfo(String itemCode,int stockId,long itemCount,String sa) {
		this.itemCode = itemCode;
		this.stockId = stockId;
		this.itemCount = itemCount;
		this.stockAttr = sa;
	}
	
	/************************************biz util*****************************/
	public void initItemInfo(boolean needParseAttr,boolean needDetailInfo, boolean needExtendInfo,boolean needQgoItemInfo)throws BusinessException{
		ItemBiz biz = (ItemBiz)SpringHelper.getBean("itemBiz");
		item = biz.getItemFromPaipai2(itemCode, needParseAttr, needDetailInfo, needExtendInfo, needQgoItemInfo);
	}
	
	public long caculateMppTotalDealFee() throws BusinessException{
		if (this.getItem().getMmItemInfo().isSupportCOD() && this.getItem().getMmItemInfo().isFree4Freight()) {
			return this.getDealDetail().getDealPayFeeTotal() + this.getItem().getMmItemInfo().getAdditionalFee() * itemCount;
		} else {
			return this.getDealDetail().getDealPayFeeTotal();
		}
	}
	
	public Mstock4J getCurrentStock() throws BusinessException{
		Mstock4J stock = this.getItem().getCurrentStock(stockAttr);
		return stock;
//		
//		if (this.item.getPpItemInfo().isMobileItem()) {
//			if (StringUtil.isEmpty(this.stockAttr)) {
//				Mstock4J stock = new Mstock4J();
//				stock.setStockPrice(this.item.getMmItemInfo().getItemPrice());
//				stock.setDiscountPrice(this.item.getMmItemInfo().getDiscountPrice());
//				return stock;
//			} else {
//				for (Mstock4J stock : this.item.getMmItemInfo().getStockList()) {
//					if (this.stockAttr.equals(stock.getStockAttr())){
//						return stock;
//					}
//				}
//			}
//		} else {
//			Mstock4J stock = new Mstock4J();
//			if (StringUtil.isEmpty(this.stockAttr)) {
//				stock.setStockPrice(this.getItem().getPpItemInfo().getItemPrice());
//				stock.setDiscountPrice(this.getItem().getPpItemInfo().discountPrice);
//				return stock;
//			}
//			for (Stock respStock : this.item.getPpItemInfo().stockList) {
//				if (this.stockAttr.equals(stock.getStockAttr())){
//					stock.setStockId(respStock.stockId);
//					stock.setStockAttr(respStock.stockAttr);
//					stock.setStockPrice(respStock.stockPrice);
//					stock.setDiscountPrice(respStock.discountPrice);
//					return stock;
//				}
//			}
//		}
//		throw BusinessException.createInstance(BusinessErrorType.INVALID_STOCKATTR);
	}
	
	public void caculateDiscount()  throws BusinessException{
		long discount = this.getCurrentStock().getStockDiscountPrice(discountType);//取折扣价格
		this.superQQDiscount = discount > 0 ? discount : 0;
	}
	
	/************************************setter getter*****************************/
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public int getPayType() {
		return payType;
	}
	public void setPayType(int payType) {
		this.payType = payType;
	}
	public int getAddressId() {
		return addressId;
	}
	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}
	public long getUin() {
		return uin;
	}
	public void setUin(long uin) {
		this.uin = uin;
	}
	public long getItemCount() {
		return itemCount;
	}
	public void setItemCount(long itemCount) {
		this.itemCount = itemCount;
	}
	public GetDealDetailResponse getDealDetail() {
		return dealDetail;
	}
	public void setDealDetail(GetDealDetailResponse dealDetail) {
		this.dealDetail = dealDetail;
	}
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public String getReserved() {
		return reserved;
	}
	public void setReserved(String reserved) {
		this.reserved = reserved;
	}

	public int getRedPackageId() {
		return redPackageId;
	}

	public void setRedPackageId(int redPackageId) {
		this.redPackageId = redPackageId;
	}

	public String getBuyerNote() {
		return buyerNote;
	}

	public void setBuyerNote(String buyerNote) {
		this.buyerNote = buyerNote;
	}

	public String getStockAttr() {
		return stockAttr;
	}

	public void setStockAttr(String stockAttr) {
		this.stockAttr = stockAttr;
	}

	public String getExpressType() {
		return expressType;
	}

	public void setExpressType(String expressType) {
		this.expressType = expressType;
	}

	public String getPriceType() {
		return priceType;
	}

	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}

	public long getDealTotalFee() {
		return dealTotalFee;
	}

	public void setDealTotalFee(long dealTotalFee) {
		this.dealTotalFee = dealTotalFee;
	}

	public int getStockId() {
		return stockId;
	}

	public void setStockId(int stockId) {
		this.stockId = stockId;
	}

	public long getSuperQQDiscount() {
		return superQQDiscount;
	}

	public void setSuperQQDiscount(long superQQDiscount) {
		this.superQQDiscount = superQQDiscount;
	}

	public int getDiscountType() {
		return discountType;
	}

	public void setDiscountType(int discountType) {
		this.discountType = discountType;
	}

	public int getSuperQQLevel() {
		return superQQLevel;
	}

	public void setSuperQQLevel(int superQQLevel) {
		this.superQQLevel = superQQLevel;
	}

	public int getDiamondLevel() {
		return diamondLevel;
	}

	public void setDiamondLevel(int diamondLevel) {
		this.diamondLevel = diamondLevel;
	}

	public ItemResponse getItem() {
		return item;
	}

	public void setItem(ItemResponse item) {
		this.item = item;
	}

	
}
