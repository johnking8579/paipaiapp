package com.qq.qqbuy.deal.po;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @ClassName: DealInfo
 * @Description: 订单列表信息
 * @author wendyhu
 * @date 2012-12-13 下午07:18:22
 */
public class DealListItemInfo
{
    /** dealCode:订单编码 */
    private String dealCode = "";

    /** sellerName:卖家名称（店铺名称） */
    private String sellerName = "";

    /** sellerUin:卖家QQ号 */
    private long sellerUin;

    /** freight:运费 */
    private long freight = 0;

    /** transportType:运费类型 */
    private String transportType = "";
    
    /** transportTypeDesc:运费类型说明*/
    private String transportTypeDesc = "";
    
    /**
     * 总价
     */
    private long totalFee = 0;

    /** totalCash:总支付现金 */
    private long totalCash = 0;

    /** dealState:订单状态 */
    private String dealState = "";

    /** dealStateDesc:订单状态说明 */
    private String dealStateDesc = "";
    
    /** dealRateState:订单的评价状态 */
    private String dealRateState = null;

    /** dealRateStateDesc:订单的评价状态说明 */
    private String dealRateStateDesc = null;

    /** createTime:下单时间 */
    private String createTime = "";

    /** lastUpdateTime:最后修改时间 */
    private String lastUpdateTime = "";

    /** dealFlag:订单标志 */
    private String dealFlag = "";

    /** availableAction:订单在当前状态下可做的操作 */
    private String availableAction = "";
    
    /**
     * dealPayType:订单的支付方式
     */
    private String dealPayType = null;

    /**
     * dealPayTypeDesc:订单支付方式说明
     */
    private String dealPayTypeDesc = null;
    
    private long dealRefundState;	//退款状态
    private String strReceiveMobile;	//收货人手机号
    /**
     * 订单类型,
     * SELL_TYPE_TAKECHAEAP 拍便宜订单,
     * SELL_TYPE_ALL(0, "所有类型"),
     * SELL_TYPE_BIN(1, "一口价"),
     * SELL_TYPE_AUCTION_SINGLE(2, "单件拍卖"),
     * SELL_TYPE_B2C(3, "b2c订单"),
     * SELL_TYPE_TAKECHAEAP(10,"拍便宜订单"),
     * UNKNOW(-1, "未知类型");
     */
    private String dealType;
    /**
     * 订单类型描述
     */
    private String dealTypeDesc;

    /** itemList:订单的商品列表 */
    private List<DealCmdyInfo> itemList = new ArrayList<DealCmdyInfo>();
    
    public long getTotalFee()
    {
        return totalFee;
    }

    public void setTotalFee(long totalFee)
    {
        this.totalFee = totalFee;
    }

    public String getDealCode()
    {
        return dealCode;
    }

    public void setDealCode(String dealCode)
    {
        this.dealCode = dealCode;
    }

    public String getSellerName()
    {
        return sellerName;
    }

    public void setSellerName(String sellerName)
    {
        this.sellerName = sellerName;
    }

    public long getSellerUin()
    {
        return sellerUin;
    }

    public void setSellerUin(long sellerUin)
    {
        this.sellerUin = sellerUin;
    }

    public long getFreight()
    {
        return freight;
    }

    public void setFreight(long freight)
    {
        this.freight = freight;
    }

    public String getTransportType()
    {
        return transportType;
    }

    public void setTransportType(String transportType)
    {
        this.transportType = transportType;
    }
    
    public String getTransportTypeDesc() 
    {
		return transportTypeDesc;
	}

	public void setTransportTypeDesc(String transportTypeDesc) 
	{
		this.transportTypeDesc = transportTypeDesc;
	}

	public long getTotalCash()
    {
        return totalCash;
    }

    public void setTotalCash(long totalCash)
    {
        this.totalCash = totalCash;
    }

    public String getDealState()
    {
        return dealState;
    }

    public void setDealState(String dealState)
    {
        this.dealState = dealState;
    }

    public String getDealStateDesc()
    {
        return dealStateDesc;
    }

    public void setDealStateDesc(String dealStateDesc)
    {
        this.dealStateDesc = dealStateDesc;
    }
    
    public String getDealRateState() 
    {
		return dealRateState;
	}

	public void setDealRateState(String dealRateState) 
	{
		this.dealRateState = dealRateState;
	}

	public String getDealRateStateDesc() {
		return dealRateStateDesc;
	}

	public void setDealRateStateDesc(String dealRateStateDesc) {
		this.dealRateStateDesc = dealRateStateDesc;
	}

	public String getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(String createTime)
    {
        this.createTime = createTime;
    }

    public String getLastUpdateTime()
    {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(String lastUpdateTime)
    {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getDealFlag()
    {
        return dealFlag;
    }

    public void setDealFlag(String dealFlag)
    {
        this.dealFlag = dealFlag;
    }

    public String getAvailableAction()
    {
        return availableAction;
    }

    public void setAvailableAction(String availableAction)
    {
        this.availableAction = availableAction;
    }

    public List<DealCmdyInfo> getItemList()
    {
        return itemList;
    }

    public void setItemList(List<DealCmdyInfo> itemList)
    {
        this.itemList = itemList;
    }

    public String getDealPayType() {
		return dealPayType;
	}

	public void setDealPayType(String dealPayType) {
		this.dealPayType = dealPayType;
	}

	public String getDealPayTypeDesc() {
		return dealPayTypeDesc;
	}

	public void setDealPayTypeDesc(String dealPayTypeDesc) {
		this.dealPayTypeDesc = dealPayTypeDesc;
	}

	public long getDealRefundState() {
		return dealRefundState;
	}

	public void setDealRefundState(long dealRefundState) {
		this.dealRefundState = dealRefundState;
	}

	public String getStrReceiveMobile() {
		return strReceiveMobile;
	}

	public void setStrReceiveMobile(String strReceiveMobile) {
		this.strReceiveMobile = strReceiveMobile;
	}

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public String getDealTypeDesc() {
        return dealTypeDesc;
    }

    public void setDealTypeDesc(String dealTypeDesc) {
        this.dealTypeDesc = dealTypeDesc;
    }
}
