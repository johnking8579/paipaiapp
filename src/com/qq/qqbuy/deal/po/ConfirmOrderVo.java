package com.qq.qqbuy.deal.po;

import java.util.Vector;

import com.qq.qqbuy.cmdy.po.RedPackageVo;
import com.qq.qqbuy.item.po.ItemBO;
import com.qq.qqbuy.item.util.DispFormater;
import com.qq.qqbuy.recvaddr.po.ReceiverAddress;
import com.qq.qqbuy.recvaddr.po.RecvAddr;


/**
 * 用于封装确认订单返回的信息
 * 
 * @author winsonwu
 * @Created 2012-12-26 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class ConfirmOrderVo {

	private int version = 1;
	
	// Decode后的商品库存属性, 迁移到CmdyItemVo中
	@Deprecated
	private String decodeAttr;

	// 购买的商品信息, 使用回自定义的Vo对象，这样容易控制jsp使用的变量。
	@Deprecated
	private Vector<ItemBO> items = new Vector<ItemBO>();

	// 运送方式选择列表
	@Deprecated
	private Vector<ShipCalcVo> shipCalcInfos = new Vector<ShipCalcVo>();

	// 基准总价(itemPrice * buyNum); 购物车的价格复杂一点
	@Deprecated
	private long sdPrice;

	// 卖家Uin
	private long sellerUin;

	// 分单结果总价
	private long totalPrice;

	// 分单结果展示总价
	private String disTotalPrice;

	// 收货地址列表
	private RecvAddr recv;

	// 默认收货地址
	private ReceiverAddress defaultAddr;

	// 分单后的正常包裹列表
	private Vector<ConfirmPackageVo> normalPackages = new Vector<ConfirmPackageVo>();

	// 分单后的问题包裹列表
	private Vector<ConfirmPackageVo> problemPackages = new Vector<ConfirmPackageVo>();

	private Vector<RedPackageVo> redPackages = new Vector<RedPackageVo>();


	public long lastCalculateTotalPrice() {
		long total = 0;
		if (normalPackages != null && normalPackages.size() > 0) {
			for (ConfirmPackageVo normalPackage : normalPackages) {
				total += normalPackage.lastCalculateTotalPrice();
			}
		}

		totalPrice = total;
		disTotalPrice = DispFormater.priceDispFormater(total);

		return total;
	}
	
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public RecvAddr getRecv() {
		return recv;
	}

	public void setRecv(RecvAddr recv) {
		this.recv = recv;
	}

	public Vector<ShipCalcVo> getShipCalcInfos() {
		return shipCalcInfos;
	}

	public void setShipCalcInfos(Vector<ShipCalcVo> shipCalcInfos) {
		this.shipCalcInfos = shipCalcInfos;
	}

	public Vector<ItemBO> getItems() {
		return items;
	}

	public void setItems(Vector<ItemBO> items) {
		this.items = items;
	}

	public ReceiverAddress getDefaultAddr() {
		return defaultAddr;
	}

	public void setDefaultAddr(ReceiverAddress defaultAddr) {
		this.defaultAddr = defaultAddr;
	}

	public long getSdPrice() {
		return sdPrice;
	}

	public void setSdPrice(long sdPrice) {
		this.sdPrice = sdPrice;
	}

	public String getDecodeAttr() {
		return decodeAttr;
	}

	public void setDecodeAttr(String decodeAttr) {
		this.decodeAttr = decodeAttr;
	}

	public Vector<ConfirmPackageVo> getNormalPackages() {
		return normalPackages;
	}

	public void setNormalPackages(Vector<ConfirmPackageVo> normalPackages) {
		this.normalPackages = normalPackages;
	}

	public Vector<ConfirmPackageVo> getProblemPackages() {
		return problemPackages;
	}

	public void setProblemPackages(Vector<ConfirmPackageVo> problemPackages) {
		this.problemPackages = problemPackages;
	}

	public long getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(long totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getDisTotalPrice() {
		return disTotalPrice;
	}

	public void setDisTotalPrice(String disTotalPrice) {
		this.disTotalPrice = disTotalPrice;
	}

	public long getSellerUin() {
		return sellerUin;
	}

	public void setSellerUin(long sellerUin) {
		this.sellerUin = sellerUin;
	}

	public Vector<RedPackageVo> getRedPackages() {
		return redPackages;
	}

	public void setRedPackages(Vector<RedPackageVo> redPackages) {
		this.redPackages = redPackages;
	}


}
