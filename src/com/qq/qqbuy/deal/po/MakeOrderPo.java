package com.qq.qqbuy.deal.po;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import com.qq.qqbuy.common.BasicResult;
import com.qq.qqbuy.common.constant.ErrConstant;

/**
 * 封装Biz层的下单请求参数，应对后续OpenApi和IDL接口的变更
 * 
 * @author winsonwu
 * @Created 2012-12-26 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class MakeOrderPo  {

	private String cftAttach;

	private String wk;
	private String skey;

	// 买家号
	private long buyerUin;

	// 卖家号
	private long sellerUin;

	// 地址id
	private int addressid;

	/****************************************** 购物车下单 *********************************************/
	// 下单信息的分单后包裹列表
	private String[] orderStrList;

	/****************************************** 单件商品下单 *******************************************/
	// 购买数量
	private int buyNum;

	// 邮递类型
	private int mailType;

	// 促销规则id
	private int promotionId;

	// 商品id
	private String itemCode;

	// 收货地址
	private String recvAddr;

	// 收货姓名
	private String recvName;

	// 收货手机
	private String recvMobile;

	// 收货电话
	private String recvPhone;

	// 收货邮编
	private String recvPost;

	// 商品类型
	private int cmdyType;

	// 商品属性
	private String itemAttr;

	// 是否匿名购买
	private int anonymous;

	// 下单来源
	private int orderFrom;

	// 是否保存虚拟地址
	private int saveAddr;

	// 发票名称
	private String innoviceTitle;

	// 价格类型
	private int priceType;

	// 支付类型
	private int payType;

	// 拍拍设置的cookie(_p),请设置
	private String refer;

	// 买家留言
	private String buyerNote;

	// 红包id
	private int redPacketId;

	// 店铺代金券id
	private int shopcouponId;

	// 区域id
	private int regionId;

	// 游戏名
	private String gameName;

	// 所在区服
	private String gameArea;

	// 充值类型
	private int gameClass;

	// 游戏账号
	private String gameId;

	// 手机号码
	private String mobileNum;

	// qq号码
	private int qqNum;

	// 使用的积分数
	private int paySocre;

	// 颜色
	private String cmdyColor;

	// 尺码
	private String cmdySize;

	// 扩展信息
	private Map<String, String> extInfoMap;
	
	// 版本
	private int ver;
	
	private String mk ;

	public MakeOrderPo() {
	}

	// pc下单常用字段构造器
	public MakeOrderPo(long buyerUin, int addressid,
			int buyNum, int mailType, int promotionId, String itemCode,
			String itemAttr, int priceType, int payType,
			String buyerNote, int redPacketId,String mk) {
		super();
		this.buyerUin = buyerUin;
		this.addressid = addressid;
		this.buyNum = buyNum;
		this.mailType = mailType;
		this.promotionId = promotionId;
		this.itemCode = itemCode;
		this.itemAttr = itemAttr;
		this.priceType = priceType;
		this.payType = payType;
		this.buyerNote = buyerNote;
		this.redPacketId = redPacketId;
		this.mk = mk ;
	}


	public static MakeOrderPo createInstance4Cmdy(long buyerUin, String skey,
			String orderStrList, int adid, String cftAttach, String mk) {
		String[] list = null;
		MakeOrderPo po = new MakeOrderPo();
		po.setBuyerUin(buyerUin);
		list = orderStrList.split(",");

//		for (int i = 0; i < list.length; i++) {
//			// str += "android";
//			list[i] += " ";
//		}
		po.setSkey(skey);
		po.setOrderStrList(list);
		po.setAddressid(adid);
		po.setCftAttach(cftAttach);
		po.setMk(mk);
		return po;
	}

	public static MakeOrderPo createInstance4Direct(long buyerUin,
			String ic, String attr, int bc, int mt, int pt,
			int adid) {
		MakeOrderPo po = new MakeOrderPo();
		po.setBuyerUin(buyerUin);
		po.setItemCode(ic);
		po.setItemAttr(attr);
		po.setBuyNum(bc);
		po.setMailType(mt);
		po.setPayType(pt);
		po.setAddressid(adid);
		return po;
	}

	public Map<String, String> validate() {
		Map<String, String> map = new HashMap<String, String>();
		if (!checkQQUin(null, sellerUin+"")) {
			map.put("sellerUin", "sellerUin参数不合法");
		}
		return map;
	}
	
	private static final String pattern_qquin = "^[1-9]\\d{4,9}$";
	
	public static boolean checkQQUin(BasicResult result, String uin) {
		if (null == uin || "" == uin) {
			result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
			result.setMsg("QQ号码为空");
			return false;
		}

		if (result != null) {
			boolean isSuccess = Pattern.matches(pattern_qquin, "" + uin);
			if (!isSuccess) {
				result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
				result.setMsg("QQ号码不合法");
				return false;
			}
		}
		return true;
	}

//	public OrderDealRequest toOpenApiOrderDealReq() {
//		OrderDealRequest openApiReq = new OrderDealRequest();
//
//		if (buyNum > 1) {
//			openApiReq.setBuyCount(this.getBuyNum()); // 购买数量，默认为1
//		}
//		if (DealChecker.checkMailType(mailType)) {
//			openApiReq.setExpressType("" + mailType);
//		}
//		if (DealChecker.checkPriceType(priceType)) {
//			openApiReq.setPriceType("" + priceType);
//		}
//		if (DealChecker.checkPayType(payType)) {
//			openApiReq.setPayType("" + payType);
//		}
//		if (redPacketId > 0) {
//			openApiReq.setRedPacketId(redPacketId);
//		}
//		if (promotionId > 0) {
//			openApiReq.setPromotionId(promotionId);
//		}
//
//		openApiReq.setVisitkey(buyerUin);
//		openApiReq.setUin(buyerUin);
//		openApiReq.setSellerUin(sellerUin);
//		openApiReq.setItemCode(itemCode);
//		// openApiReq.setRegionId(regionId);
//		openApiReq.setRegionId(addressid); // TODO winson， openApi的定义存在问题。
//
//		if (!StringUtil.isEmpty(recvAddr)) {
//			openApiReq.setReceiveAddress(recvAddr);
//		}
//		if (!StringUtil.isEmpty(recvMobile)) {
//			openApiReq.setReceiveMobile(recvMobile);
//		}
//		if (!StringUtil.isEmpty(recvName)) {
//			openApiReq.setReceiveName(recvName);
//		}
//		if (!StringUtil.isEmpty(recvPhone)) {
//			openApiReq.setReceivePhone(recvPhone);
//		}
//		if (!StringUtil.isEmpty(recvPost)) {
//			openApiReq.setReceivePost(recvPost);
//		}
//
//		if (!StringUtil.isEmpty(itemAttr)) {
//			openApiReq.setItemStockAttr(itemAttr);
//		}
//		if (!StringUtil.isEmpty(buyerNote)) {
//			openApiReq.setBuyerNote(buyerNote);
//		}
//		if (!StringUtil.isEmpty(innoviceTitle)) {
//			openApiReq.setInnoviceTitle(innoviceTitle);
//		}
//
//		return openApiReq;
//	}

	public String getCftAttach() {
		return cftAttach;
	}

	public void setCftAttach(String cftAttach) {
		this.cftAttach = cftAttach;
	}

	public long getBuyerUin() {
		return buyerUin;
	}

	public void setBuyerUin(long buyerUin) {
		this.buyerUin = buyerUin;
	}

	public int getBuyNum() {
		return buyNum;
	}

	public void setBuyNum(int buyNum) {
		this.buyNum = buyNum;
	}

	public int getMailType() {
		return mailType;
	}

	public void setMailType(int mailType) {
		this.mailType = mailType;
	}

	public int getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getRecvAddr() {
		return recvAddr;
	}

	public void setRecvAddr(String recvAddr) {
		this.recvAddr = recvAddr;
	}

	public String getRecvName() {
		return recvName;
	}

	public void setRecvName(String recvName) {
		this.recvName = recvName;
	}

	public String getRecvMobile() {
		return recvMobile;
	}

	public void setRecvMobile(String recvMobile) {
		this.recvMobile = recvMobile;
	}

	public String getRecvPhone() {
		return recvPhone;
	}

	public void setRecvPhone(String recvPhone) {
		this.recvPhone = recvPhone;
	}

	public String getRecvPost() {
		return recvPost;
	}

	public void setRecvPost(String recvPost) {
		this.recvPost = recvPost;
	}

	public int getCmdyType() {
		return cmdyType;
	}

	public void setCmdyType(int cmdyType) {
		this.cmdyType = cmdyType;
	}

	public String getItemAttr() {
		return itemAttr;
	}

	public void setItemAttr(String itemAttr) {
		this.itemAttr = itemAttr;
	}

	public int getAnonymous() {
		return anonymous;
	}

	public void setAnonymous(int anonymous) {
		this.anonymous = anonymous;
	}

	public int getOrderFrom() {
		return orderFrom;
	}

	public void setOrderFrom(int orderFrom) {
		this.orderFrom = orderFrom;
	}

	public int getSaveAddr() {
		return saveAddr;
	}

	public void setSaveAddr(int saveAddr) {
		this.saveAddr = saveAddr;
	}

	public String getInnoviceTitle() {
		return innoviceTitle;
	}

	public void setInnoviceTitle(String innoviceTitle) {
		this.innoviceTitle = innoviceTitle;
	}

	public int getPriceType() {
		return priceType;
	}

	public void setPriceType(int priceType) {
		this.priceType = priceType;
	}

	public int getPayType() {
		return payType;
	}

	public void setPayType(int payType) {
		this.payType = payType;
	}

	public String getRefer() {
		return refer;
	}

	public void setRefer(String refer) {
		this.refer = refer;
	}

	public String getBuyerNote() {
		return buyerNote;
	}

	public void setBuyerNote(String buyerNote) {
		this.buyerNote = buyerNote;
	}

	public int getRedPacketId() {
		return redPacketId;
	}

	public void setRedPacketId(int redPacketId) {
		this.redPacketId = redPacketId;
	}

	public int getShopcouponId() {
		return shopcouponId;
	}

	public void setShopcouponId(int shopcouponId) {
		this.shopcouponId = shopcouponId;
	}

	public int getAddressid() {
		return addressid;
	}

	public void setAddressid(int addressid) {
		this.addressid = addressid;
	}

	public int getRegionId() {
		return regionId;
	}

	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public String getGameArea() {
		return gameArea;
	}

	public void setGameArea(String gameArea) {
		this.gameArea = gameArea;
	}

	public int getGameClass() {
		return gameClass;
	}

	public void setGameClass(int gameClass) {
		this.gameClass = gameClass;
	}

	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	public String getMobileNum() {
		return mobileNum;
	}

	public void setMobileNum(String mobileNum) {
		this.mobileNum = mobileNum;
	}

	public int getQqNum() {
		return qqNum;
	}

	public void setQqNum(int qqNum) {
		this.qqNum = qqNum;
	}

	public int getPaySocre() {
		return paySocre;
	}

	public void setPaySocre(int paySocre) {
		this.paySocre = paySocre;
	}

	public String getCmdyColor() {
		return cmdyColor;
	}

	public void setCmdyColor(String cmdyColor) {
		this.cmdyColor = cmdyColor;
	}

	public String getCmdySize() {
		return cmdySize;
	}

	public void setCmdySize(String cmdySize) {
		this.cmdySize = cmdySize;
	}

	public Map<String, String> getExtInfoMap() {
		return extInfoMap;
	}

	public void setExtInfoMap(Map<String, String> extInfoMap) {
		this.extInfoMap = extInfoMap;
	}

	public long getSellerUin() {
		return sellerUin;
	}

	public void setSellerUin(long sellerUin) {
		this.sellerUin = sellerUin;
	}

	public String getWk() {
		return wk;
	}

	public void setWk(String wk) {
		this.wk = wk;
	}

	public String[] getOrderStrList() {
		return orderStrList;
	}

	public void setOrderStrList(String[] orderStrList) {
		this.orderStrList = orderStrList;
	}

	public String getSkey() {
		return skey;
	}

	public void setSkey(String skey) {
		this.skey = skey;
	}

    public int getVer() {
        return ver;
    }

    public void setVer(int ver) {
        this.ver = ver;
    }

	public String getMk() {
		return mk;
	}

	public void setMk(String mk) {
		this.mk = mk;
	}
	

}
