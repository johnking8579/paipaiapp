package com.qq.qqbuy.deal.po;

/**
 * 封装下单后的信息
 * 
 * @author winsonwu
 * @Created 2012-12-26 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class MakeOrderVo {

	// dealCode:订单编码
	private String dealCode;

	// 支付类型：DealConstant.PAYTYPE_OLP、DealConstant.PAYTYPE_COD
	private int payType;

	// 该订单的卖家QQ号
	private long sellerUin;

	// 总金额
	private long totalFee;

	// 跳财付通支付的Url
	private String tenpayUrl;

	public String getDealCode() {
		return dealCode;
	}

	public void setDealCode(String dealCode) {
		this.dealCode = dealCode;
	}

	public int getPayType() {
		return payType;
	}

	public void setPayType(int payType) {
		this.payType = payType;
	}

	public long getSellerUin() {
		return sellerUin;
	}

	public void setSellerUin(long sellerUin) {
		this.sellerUin = sellerUin;
	}

	public String getTenpayUrl() {
		return tenpayUrl;
	}

	public void setTenpayUrl(String tenpayUrl) {
		this.tenpayUrl = tenpayUrl;
	}

	public long getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(long totalFee) {
		this.totalFee = totalFee;
	}

}
