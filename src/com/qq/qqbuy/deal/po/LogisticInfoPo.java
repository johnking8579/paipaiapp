package com.qq.qqbuy.deal.po;

import java.util.ArrayList;
import java.util.List;

import com.qq.qqbuy.common.po.CommonPo;

public class LogisticInfoPo extends CommonPo {

	private String deliverId;

	private String companyId;

	private String companyName;

	private String companyUrl;
	
	private String companyPhone;
	
	private List<LogisticLogPo> logisticLogList = new ArrayList<LogisticLogPo>();

	public List<LogisticLogPo> getLogisticLogList() {
		return logisticLogList;
	}

	public void setLogisticLogList(LogisticLogPo po) {
		this.logisticLogList.add(po);
	}

	public String getDeliverId() {
		return deliverId;
	}

	public void setDeliverId(String deliverId) {
		this.deliverId = deliverId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyUrl() {
		return companyUrl;
	}

	public void setCompanyUrl(String companyUrl) {
		this.companyUrl = companyUrl;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public void setLogisticLogList(List<LogisticLogPo> logisticLogList) {
		this.logisticLogList = logisticLogList;
	}

}
