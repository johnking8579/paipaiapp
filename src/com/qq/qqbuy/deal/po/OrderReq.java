package com.qq.qqbuy.deal.po;


/* @FileName: OrderReq.java    
 * @Description: TODO 
 * @author: homerwu   
 * @date:2013-8-14 下午02:25:52    
 */
public class OrderReq {
	
	private String skey;

	// 买家号
	private long buyerUin;

	// 卖家号
	private long sellerUin;

	// 地址id
	private int addressid;

	// 购买数量
	private int buyNum;

	// 邮递类型
	private int mailType;

	// 促销规则id
	private int promotionId;

	// 商品id
	private String itemCode;

	// 商品类型
	private int cmdyType;

	// 商品属性
	private String itemAttr;

	// 是否匿名购买
	private int anonymous;

	// 下单来源
	private int orderFrom;

	// 是否保存虚拟地址
	private int saveAddr;

	// 发票名称
	private String innoviceTitle;

	// 价格类型
	private int priceType;

	// 支付类型
	private int payType;

	// 买家留言
	private String buyerNote;

	// 红包id
	private int redPacketId;

	// 店铺代金券id
	private int shopcouponId;

	// 区域id
	private int regionId;
	
	// 网购代金卷
	private long onlineShoppingCoupons;

	//红包积分
	private long redBagScore;

	//场景id
	private long sceneId;

	public String getSkey() {
		return skey;
	}

	public void setSkey(String skey) {
		this.skey = skey;
	}

	public long getBuyerUin() {
		return buyerUin;
	}

	public void setBuyerUin(long buyerUin) {
		this.buyerUin = buyerUin;
	}

	public long getSellerUin() {
		return sellerUin;
	}

	public void setSellerUin(long sellerUin) {
		this.sellerUin = sellerUin;
	}

	public int getAddressid() {
		return addressid;
	}

	public void setAddressid(int addressid) {
		this.addressid = addressid;
	}

	public int getBuyNum() {
		return buyNum;
	}

	public void setBuyNum(int buyNum) {
		this.buyNum = buyNum;
	}

	public int getMailType() {
		return mailType;
	}

	public void setMailType(int mailType) {
		this.mailType = mailType;
	}

	public int getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public int getCmdyType() {
		return cmdyType;
	}

	public void setCmdyType(int cmdyType) {
		this.cmdyType = cmdyType;
	}

	public String getItemAttr() {
		return itemAttr;
	}

	public void setItemAttr(String itemAttr) {
		this.itemAttr = itemAttr;
	}

	public int getAnonymous() {
		return anonymous;
	}

	public void setAnonymous(int anonymous) {
		this.anonymous = anonymous;
	}

	public int getOrderFrom() {
		return orderFrom;
	}

	public void setOrderFrom(int orderFrom) {
		this.orderFrom = orderFrom;
	}

	public int getSaveAddr() {
		return saveAddr;
	}

	public void setSaveAddr(int saveAddr) {
		this.saveAddr = saveAddr;
	}

	public String getInnoviceTitle() {
		return innoviceTitle;
	}

	public void setInnoviceTitle(String innoviceTitle) {
		this.innoviceTitle = innoviceTitle;
	}

	public int getPriceType() {
		return priceType;
	}

	public void setPriceType(int priceType) {
		this.priceType = priceType;
	}

	public int getPayType() {
		return payType;
	}

	public void setPayType(int payType) {
		this.payType = payType;
	}

	public String getBuyerNote() {
		return buyerNote;
	}

	public void setBuyerNote(String buyerNote) {
		this.buyerNote = buyerNote;
	}

	public int getRedPacketId() {
		return redPacketId;
	}

	public void setRedPacketId(int redPacketId) {
		this.redPacketId = redPacketId;
	}

	public int getShopcouponId() {
		return shopcouponId;
	}

	public void setShopcouponId(int shopcouponId) {
		this.shopcouponId = shopcouponId;
	}

	public int getRegionId() {
		return regionId;
	}

	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}

	public long getOnlineShoppingCoupons() {
		return onlineShoppingCoupons;
	}

	public void setOnlineShoppingCoupons(long onlineShoppingCoupons) {
		this.onlineShoppingCoupons = onlineShoppingCoupons;
	}

	public long getRedBagScore() {
		return redBagScore;
	}

	public void setRedBagScore(long redBagScore) {
		this.redBagScore = redBagScore;
	}

	public long getSceneId() {
		return sceneId;
	}

	public void setSceneId(long sceneId) {
		this.sceneId = sceneId;
	}
}
