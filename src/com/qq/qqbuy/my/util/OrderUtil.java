package com.qq.qqbuy.my.util;

import java.util.Map;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.my.po.CodDealSummary;
import com.qq.qqbuy.thirdparty.idl.deal.DealQueryClient;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.GetAllStateDealCountByUinReq;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.GetAllStateDealCountByUinResp;



public class OrderUtil{
    /**
     * 货到付款订单的订单子状态
     * @author millionchen
     *
     */
    private static enum CodDealOrderStatus{
        /** 待买家确认*/
        WAIT_FOR_BUYER_CONFIRM("WAIT_FOR_BUYER_CONFIRM", 1),

        /** 待卖家确认*/
        WAIT_FOR_SELLER_CONFIRM("WAIT_FOR_SELLER_CONFIRM", 2),

        /** 待卖家发货*/
        WAIT_FOR_SELLER_SEND("WAIT_FOR_SELLER_SEND", 3),

        /** 待卖家确认收货*/
        WAIT_FOR_SELLER_CONFIRM_RECEIVE("WAIT_FOR_SELLER_CONFIRM_RECEIVE", 6),

        /** 订单交易成功*/
        DEAL_SUCCESS("DEAL_SUCCESS", 7),

        /** 订单被拒收*/
        DEAL_DENIED("DEAL_DENIED", 8),

        /** 订单被关闭*/
        DEAL_CLOSED("DEAL_CLOSED", 9),

        /** 订单状态不确定*/
        UNCERTAIN("UNCERTAIN", -1),

        /** 所有状态*/
        ALL("ALL", 0);

        private final String substateName;

        private final int    substateValue;

        private CodDealOrderStatus(String substateName, int substateValue)
        {
            this.substateName = substateName;
            this.substateValue = substateValue;
        }

        public int getSubstateValue()
        {
            return substateValue;
        }
    }

    /**
     * 买家的所有订单
     * @param uin
     * @return
     * @throws BusinessException 
     */
//    public static AllDealSummary getMyOrderSummary(long uin) throws BusinessException
//    {
//    	AllDealSummary os = new AllDealSummary();
//        os.setUin(uin);
//        
//        OpenApiProxy proxy = null;
//        BuyerSearchDealListRequest req = new BuyerSearchDealListRequest();
//        try 
//        {        
//	        req.setUin(uin);
//	        req.setListItem(0);
//	        req.setBuyerUin(uin);
//	        req.setPageIndex(1);
//	        req.setPageSize(1);
//	        
//	        proxy = OpenApi.getProxy();
//	        BuyerSearchDealListResponse res = proxy.buyerSearchDealList(req);
//	        if (res.isSucceed())
//	        {
//	            os.setTotalCount((int) res.countTotal);
//	        }
//        } 
//        catch (Exception e)
//        {
//        	Log.openapiAccess.error("AllDealSummary==>getMyOrderSummary error:" + req);
//            throw BusinessException.createInstance(BusinessErrorType.CALL_OPENAPI_BIZ_FAILED,e);
//        }
//        
//        GetAllStateDealCountByUinReq allStateReq = new GetAllStateDealCountByUinReq();
//        try 
//        {  
//	    	allStateReq.setUin(uin);
//	    	allStateReq.setDealType(1);
//	    	
//	    	GetAllStateDealCountByUinResp resp = DealQueryClient.getAllStateDealCountByUin(allStateReq);
//	    	if ( resp != null && resp.getErrCode() == 0 ) 
//	    	{
//	    		Map<Integer, Integer> mp = resp.getPaipaiDealCount();
//	    		if ( mp != null ) {
//	    			if ( mp.get(3) != null ) {
//	    				os.setWaitSellerDeliveryCount(mp.get(3));
//	    			}
//	    			if ( mp.get(4) != null ) {
//	    				os.setWaitBuyerPayCount(mp.get(4));
//	    			}
//					if ( mp.get(5) != null ) {
//						os.setWaitBuyerReceiveCount(mp.get(5));
//					}
//	    		}
//	    	}
//        }
//        catch (Exception e)
//        {
//        	Log.idlRun.error("AllDealSummary==>getMyOrderSummary error:" + allStateReq);
//            throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL,e);
//        }
//        return os;
//    }

    /**
     * 买家货到付款类订单
     * @param uin
     * @return
     */
    public static CodDealSummary getMyCodOrderSummary(long uin) throws BusinessException
    {
    	CodDealSummary cos = new CodDealSummary();
        cos.setUin(uin);
        
    	GetAllStateDealCountByUinReq allStateReq = new GetAllStateDealCountByUinReq();
        try 
        { 
	    	allStateReq.setUin(uin);
			allStateReq.setDealType(22);
	
			GetAllStateDealCountByUinResp resp = DealQueryClient.getAllStateDealCountByUin(allStateReq);
			
			if ( resp != null && resp.getErrCode() == 0 ) 
			{
	    		Map<Integer, Integer> mp = resp.getCodDealCount();
	    		if ( mp != null ) 
	    		{
	    			try
	    			{
		    			cos.setTotalCount(mp.get(100));
		    			cos.setWaitBuyerConfirmCount(mp.get(101));
		    			cos.setWaitSellerConfirmCount(mp.get(102));
		    			cos.setWaitSellerDeliveryCount(mp.get(103));
		    			cos.setWaitBuyerReceiveCount(mp.get(106));
		    			cos.setSucessedCount(mp.get(107));
		    			cos.setBuyerRefusedCount(mp.get(108));
		    			cos.setClosedCount(mp.get(109));
	    			}
	    			catch(Exception e)
	    			{
	    	        	throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL,e);
	    			}
	    		}
			}
        }
        catch (Exception e)
        {
        	throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL,e);
        }
 
        return cos;
    }
}
