package com.qq.qqbuy.my.biz.impl;


import java.util.Calendar;

import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.my.biz.MyBiz;
import com.qq.qqbuy.my.po.CodDealSummary;
import com.qq.qqbuy.my.po.DealSummaryPo;
import com.qq.qqbuy.my.po.UserInfoPo;
import com.qq.qqbuy.my.util.OrderUtil;
import com.qq.qqbuy.thirdparty.idl.dealpc.PcDealQueryClient;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.GetDealInfoList2Resp;
import com.qq.qqbuy.user.biz.UserBiz;
import com.qq.qqbuy.user.po.ColorDiamondInfo;

/**
 * @author winsonwu
 * @Created 2012-8-23
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class MyBizImpl implements MyBiz {


	private UserBiz userBiz ;
	
	public UserBiz getUserBiz() {
		return userBiz;
	}

	public void setUserBiz(UserBiz userBiz) {
		this.userBiz = userBiz;
	}
		
	@Override
	public DealSummaryPo queryUserDealSummary(long uin) throws BusinessException 
	{
  
		DealSummaryPo dealSummaryPo = new DealSummaryPo();
	
		//获取货到付款订单数量信息：显示待短信确认订单数量
		CodDealSummary codDealSummary = OrderUtil.getMyCodOrderSummary(uin);
		dealSummaryPo.setCodDealSummary(codDealSummary);

		//未完成订单（进行中订单数量）
		GetDealInfoList2Resp res = PcDealQueryClient.getDealList(uin, 700, 1, 1);		
		dealSummaryPo.setNotCompleteCount((int)res.getTotalNum());
		

		//暂时没有使用到所有订单相关信息（我的订单显示数字已经去除）
//		try 
//		{
//			AllDealSummary allDealSummary = OrderUtil.getMyOrderSummary(uin);
//			dealSummaryPo.setAllDealSummary(allDealSummary);
//		}
//		catch (BusinessException e) {
//	
//			throw e;
//		}
		
		return dealSummaryPo;
	}

	@Override
	public UserInfoPo queryUserInfo(long uin) throws BusinessException 
   {
		UserInfoPo po = new UserInfoPo();
		
		//设置QQ号码
		po.setUin(uin);
		
		//设置图像： 查询用户空间图片,判断用户是否设置了头像
//		boolean havePhoto = HttpUtil.getUserQzoneImage(String.valueOf(uin), true);	
//		if(havePhoto)
//		{
		po.setPicURL("http://qlogo3.store.qq.com/qzone/"+ uin + "/" + uin +"/50");
//		}
		
		//问候语：设置提示语查询当前的时间：24小时制,当前时间对应的小时，范围：0-23
		Calendar cal= Calendar.getInstance();
		int hour=cal.get(Calendar.HOUR_OF_DAY);
		po.setHour(hour);
		
		//设置用户彩钻等级
//		int level = userBiz.getColorDiamondLevel(uin);
		ColorDiamondInfo colorDiamondInfo = userBiz.getColorDiamondLevel(uin);
		if (colorDiamondInfo != null) {
			po.setLevel(colorDiamondInfo.getColorDiamondLevel());
		}
				
		return po;
	}
	

	
}
