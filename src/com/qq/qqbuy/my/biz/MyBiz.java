package com.qq.qqbuy.my.biz;

import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.my.po.DealSummaryPo;
import com.qq.qqbuy.my.po.UserInfoPo;


/**
 * @author winsonwu
 * @Created 2012-8-23
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public interface MyBiz 
{	
	public UserInfoPo queryUserInfo(long uin) throws BusinessException;
	
	public DealSummaryPo queryUserDealSummary(long uin) throws BusinessException;
}
