package com.qq.qqbuy.my.action;

import java.util.Map;

import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.Page;
import com.qq.qqbuy.common.action.PaipaiApiBaseAction;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.deal.biz.QueryDealBiz;
import com.qq.qqbuy.deal.po.DealStateList;
import com.qq.qqbuy.deal.util.DealConstant;
import com.qq.qqbuy.favorite.biz.FavoriteBiz;
import com.qq.qqbuy.favorite.po.FavoriteItemsPo;
import com.qq.qqbuy.favorite.po.FavoriteShopDetails;
import com.qq.qqbuy.my.po.MainMySummary;
import com.qq.qqbuy.redpacket.biz.RedPacketBiz;
import com.qq.qqbuy.redpacket.po.RedPacketPo;
import com.qq.qqbuy.thirdparty.idl.bounspackage.BonusPackageClient;
import com.qq.qqbuy.thirdparty.idl.bounspackage.protocol.SearchBonusPackagePo;
import com.qq.qqbuy.thirdparty.idl.bounspackage.protocol.SearchBonusPackagersp;
import com.qq.qqbuy.thirdparty.idl.stat.StatConst;
import com.qq.qqbuy.user.biz.UserBiz;
import com.qq.qqbuy.user.po.GetUserTagsRespPo;

public class ApiMyAction extends PaipaiApiBaseAction {
	private QueryDealBiz queryDealBiz;
	private RedPacketBiz redPacketBiz;
	private FavoriteBiz favBiz;
	private UserBiz userBiz;
	private String jsonStr;
	private Gson gson = new GsonBuilder().serializeNulls().create();
	
	/**
	 * 商品收藏最多显示多少条数据
	 */
    private int itemCount = 10;
	/**
	 * 店铺收藏最多显示多少条数据
	 */
	private int shopCount = 10;
	/**
	 * 新增红包信息
	 */
	private String redDesc;
	/**
	 * 页码
	 */
	private int pageIndex = 1;
	/**
	 * 每页数据数
	 */
	private int pageSize = 20;
	/**
	 * 红包状态
	 */
	private int state = 1;
	/**
	 *
	 */
	private long transactionId;
	/**
	 * 店铺ID
	 */
	private long shopId;

	/**
	 * 个人中心首页相关数据
	 * @return
	 */
	public String summary() {
		if (!checkLoginAndGetSkey()) {
			setErrCodeAndMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL,ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
			return RST_FAILURE;
		}
		
		MainMySummary summary = new MainMySummary();
		
		//订单各状态数量
		try {
			Map<String,Integer> dealCount = queryDealBiz.getDealStatInfoByUin(getQq(), getMk());	//net-sf-json.jar不支持key=int,所以转换
			summary.setDealCount(dealCount);
			adaptDealStat(summary, dealCount);
		} catch (Exception e) {
			Log.run.error(e.getMessage(), e);
		}

		// 优惠券列表
		try {
			Page<RedPacketPo> coupon = redPacketBiz.query(100, getQq(),getMk(), 2, getSk());
			summary.setCoupon(coupon);
		} catch (Exception e) {
			Log.run.error(e.getMessage(), e);
		}
		
		// 代金券列表
		try {
			Page<RedPacketPo> cashCoupon = redPacketBiz.query(100, getQq(), getMk(), 4, getPPSkey());
			summary.setCashCoupon(cashCoupon);
		} catch (Exception e) {
			Log.run.error(e.getMessage(), e);
		}
		
		// 收藏商品列表
		try {				
			FavoriteItemsPo favoriteItemsPo = favBiz.getFavItemList(getQq(), getMk(), getSk(), 0, itemCount);
			summary.setFavoriteItemsPo(favoriteItemsPo);				
		} catch (Exception e) {
			Log.run.error(e.getMessage(), e);
		}
		
		// 收藏店铺列表
		try {
			FavoriteShopDetails favoriteShopDetails = favBiz.getFavShopList(getQq(), getMk(), getSk(), 0, shopCount);
			summary.setFavoriteShopDetails(favoriteShopDetails);
		} catch (Exception e) {
			Log.run.error(e.getMessage(), e);
		}
		
		// 标签列表
		try {
			GetUserTagsRespPo getUserTagsRespPo = userBiz.getUserTag(getWid(), getMk());
			summary.setGetUserTagsRespPo(getUserTagsRespPo);
		} catch (Exception e) {
			Log.run.error(e.getMessage(), e);
		}
		
		jsonStr = gson.toJson(summary);
		
		//新增红包信息
		redDesc = "{}";
		try {
			redDesc = BonusPackageClient.getHbDesc(getWid(), getSk());
		}catch(Exception e){
			Log.run.error(e.getMessage(), e);
		}
		return RST_SUCCESS;
	}
	
	/**
	 * 把queryDealBiz.getDealStatInfoByUin()返回的订单数量, 适配成旧版APP使用的WaitBuyerPayCount, WaitBuyerReceiveCount等数据
	 * @param summary
	 * @param dealCount
	 */
	private void adaptDealStat(MainMySummary summary, Map<String,Integer> dealCount)	{
		int all = toInt(dealCount.get(StatConst.UIN_DEAL_BUYER_PAY_3M+""));
		int waitPay =  toInt(dealCount.get(StatConst.UIN_DEAL_BUYER_WAIT4PAY+""));
		int waitRecv = toInt(dealCount.get(StatConst.UIN_DEAL_BUYER_WAIT4PAYCONFIRM+""));
		int waitEval = toInt(dealCount.get(DealConstant.RATE_STATE_NEED_BUYER+""));
		int waitDelivery = toInt(dealCount.get(StatConst.UIN_DEAL_BUYER_WAIT4CONSIGNMENT+""));
		
		summary.setAllDealCount(all);
		summary.setWaitBuyerPayCount(waitPay);
		summary.setWaitBuyerReceiveCount(waitRecv);
		summary.setWaitBuyerEvalCount(waitEval);
		summary.setPolWaitRecvCount(waitRecv);	
		
		DealStateList list = new DealStateList();
		list.setCodWaitBuyerReceiveCount(0);
		list.setCodWaitSellerDeliveryCount(0);
		list.setEndNormalCount(all);
		list.setTotalCount(all);
		list.setWaitBuyerEvalCount(waitEval);
		list.setWaitBuyerPayCount(waitPay);
		list.setWaitBuyerReceiveCount(waitRecv);
		list.setWaitSellerDeliveryCount(waitDelivery);
		summary.setDealStateList(list);
	}
	
	private int toInt(Integer obj)	{
		return obj == null ? 0 : obj.intValue();
	}
	
	/**
	 * 最早的代码
	private void adaptDealStat2(MainMySummary summary, Map<String,Integer> dealCount, int ver)	{
		DealStateList dealStateList = queryDealBiz.queryDealStatesList(getQq(), 2);
		if (dealStateList != null && dealStateList.errCode == 0) {
			summary.setWaitBuyerPayCount(dealStateList.getWaitBuyerPayCount());
			summary.setWaitBuyerReceiveCount(dealStateList.getWaitBuyerReceiveCount() + dealStateList.getCodWaitBuyerReceiveCount());
			// 如果版本大于等于3，待收货返回在线支付待发货、货到付款待发货、在线支付待收货、货到付款待收货
			if (ver >= 4) {
				summary.setWaitBuyerReceiveCount(summary.getWaitBuyerReceiveCount()
						+ dealStateList.getWaitSellerDeliveryCount()
						+ dealStateList.getCodWaitSellerDeliveryCount());
			}
			
			//返回各种数量,待收货、待发货、待评价等
			summary.setDealStateList(dealStateList);
			
			summary.setPolWaitRecvCount(dealStateList.getWaitBuyerReceiveCount());
			summary.setWaitBuyerEvalCount(dealStateList.getWaitBuyerEvalCount());
			summary.setAllDealCount(dealStateList.getTotalCount());
		} else if (dealStateList != null) {
			setErrCodeAndMsg((int) dealStateList.errCode, dealStateList.msg);
		}
	}
	 */
	
	/**
	 * 获取用户领的红包列表
	 * @return
	 */
	public String redPackageList(){

		if (!checkLoginAndGetSkey()) {
			setErrCodeAndMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL,ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
			return RST_FAILURE;
		}
		try{
			jsonStr = BonusPackageClient.getBonusDetail(getWid(),getSk(),pageIndex,pageSize);
			return RST_SUCCESS;
		}catch (BusinessException be){
			setErrCodeAndMsg(be.getErrCode(),be.getErrMsg());
			return RST_API_FAILURE;
		}catch (Exception e) {
			setErrCodeAndMsg(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP, "捕获到未知异常：" + e.getClass().getSimpleName());
			return RST_API_FAILURE;
		}
	}

	/**
	 * 获取用户发的红包列表
	 * @return
	 */
	public String bonusPackage(){
		jsonStr = "{}";
		JsonConfig jsoncfg = new JsonConfig();
		String[] excludes = { "size", "empty" };
		jsoncfg.setExcludes(excludes);
		try {
			if (!checkLoginAndGetSkey()) {
                setErrCodeAndMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL,ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
                return RST_FAILURE;
            }
			SearchBonusPackagePo packagePo = new SearchBonusPackagePo();
			packagePo.setIPageIndex(pageIndex);
			/**
			 * pageSize=-1表示不分页
			 */
			packagePo.setIPageSize(pageSize);
			packagePo.setDdwUin(getWid());
			packagePo.setDdwShopUin(shopId);
			packagePo.setDwUpdateTime(System.currentTimeMillis());
			/**
			 * state=-1表示不分状态
			 */
			packagePo.setIState(state);
			packagePo.setDdwTransactionId(transactionId);
			SearchBonusPackagersp bonusPackage = BonusPackageClient.bonusPackageList(getSk(), packagePo);
			if(bonusPackage != null){
				jsonStr = JSONSerializer.toJSON(bonusPackage, jsoncfg).toString();
				Log.run.info("bonusPackage:" + jsonStr);
				return RST_SUCCESS;
			}else{
				setErrCodeAndMsg((int) result.getErrCode(), result.getMsg());
				return RST_API_FAILURE;
			}
		} catch (Exception e) {
			setErrCodeAndMsg(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP, "捕获到未知异常：" + e.getClass().getSimpleName());
			return RST_API_FAILURE;
		}
	}

	public QueryDealBiz getQueryDealBiz() {
		return queryDealBiz;
	}

	public void setQueryDealBiz(QueryDealBiz queryDealBiz) {
		this.queryDealBiz = queryDealBiz;
	}

	public RedPacketBiz getRedPacketBiz() {
		return redPacketBiz;
	}

	public void setRedPacketBiz(RedPacketBiz redPacketBiz) {
		this.redPacketBiz = redPacketBiz;
	}

	public int getItemCount() {
		return itemCount;
	}

	public void setItemCount(int itemCount) {
		this.itemCount = itemCount;
	}

	public int getShopCount() {
		return shopCount;
	}

	public void setShopCount(int shopCount) {
		this.shopCount = shopCount;
	}

	public FavoriteBiz getFavBiz() {
		return favBiz;
	}

	public void setFavBiz(FavoriteBiz favBiz) {
		this.favBiz = favBiz;
	}
	
	public void setUserBiz(UserBiz userBiz) {
		this.userBiz = userBiz;
	}

	public String getJsonStr() {
		return jsonStr;
	}

	public void setJsonStr(String jsonStr) {
		this.jsonStr = jsonStr;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public long getShopId() {
		return shopId;
	}

	public void setShopId(long shopId) {
		this.shopId = shopId;
	}

	public String getRedDesc() {
		return redDesc;
	}

	public void setRedDesc(String redDesc) {
		this.redDesc = redDesc;
	}
}
