package com.qq.qqbuy.my.po;

/**
 * 用于封装我的拍拍的输出数据
 * @author tylerliu
 * @Created 2012-12-07
 */
public class DealSummaryPo 
{
	//所以订单信息：货到付款 与 在线支付
	private AllDealSummary allDealSummary ;
	
	//货到付款所有订单
	private CodDealSummary codDealSummary ;
	
	//待短信确认订单 
	int waitBuyerConfirmCount = 0;

	//待付款订单 
	int waitBuyerPayCount = 0;
	
	//待收货订单
	int waitBuyerReceiveCount = 0;
		
	//未完成订单
	int notCompleteCount = 0;

	//所有订单
	int totalCount  = 0;

	public AllDealSummary getAllDealSummary() {
		return allDealSummary;
	}

	public void setAllDealSummary(AllDealSummary allDealSummary) {
		this.allDealSummary = allDealSummary;
	}

	public CodDealSummary getCodDealSummary() {
		return codDealSummary;
	}

	public void setCodDealSummary(CodDealSummary codDealSummary) {
		this.codDealSummary = codDealSummary;
	}
	
	//待短信确认订单 
	public int getWaitBuyerConfirmCount()
	{
		if(codDealSummary != null)
		{
			this.waitBuyerPayCount = codDealSummary.getWaitBuyerConfirmCount();
		}
		return waitBuyerPayCount;
	}
	//待付款订单 
	public int getWaitBuyerPayCount()
	{
		if(allDealSummary != null)
		{
			this.waitBuyerConfirmCount = allDealSummary.getWaitBuyerPayCount();	
		}
		return waitBuyerConfirmCount;
	}
	
	//待收货订单
	public int getWaitBuyerReceiveCount()
	{
		if(allDealSummary != null)
		{
			this.waitBuyerReceiveCount = allDealSummary.getWaitBuyerReceiveCount();
		}
		return waitBuyerReceiveCount;
	}
	
	//未完成订单
	public int getNotCompleteCount()
	{
		return notCompleteCount;
	}
	
	//所有订单
	public int getTotalCount()
	{
		if(allDealSummary != null)
		{
			this.totalCount = allDealSummary.getTotalCount();
		}
	
		return totalCount;
	}

	public void setNotCompleteCount(int notCompleteCount) {
		this.notCompleteCount = notCompleteCount;
	}
	
}
