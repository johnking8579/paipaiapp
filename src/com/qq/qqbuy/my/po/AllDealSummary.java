package com.qq.qqbuy.my.po;

public class AllDealSummary {
    private long uin;
    /**
     * 所有定单
     */
    private int totalCount;

    /**
     * 等待买家付款
     * DS_WAIT_BUYER_PAY
     */
    private int waitBuyerPayCount;
    /**
     * 等待卖家发货
     * DS_WAIT_SELLER_DELIVERY
     */
    private int waitSellerDeliveryCount;
    /**
     * 卖家已发货，等待买家收货
     * DS_WAIT_BUYER_RECEIVE
     */
    private int waitBuyerReceiveCount;

    public long getUin() {
        return uin;
    }

    public void setUin(long uin) {
        this.uin = uin;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getWaitBuyerPayCount() {
        return waitBuyerPayCount;
    }

    public void setWaitBuyerPayCount(int waitBuyerPayCount) {
        this.waitBuyerPayCount = waitBuyerPayCount;
    }

    public int getWaitSellerDeliveryCount() {
        return waitSellerDeliveryCount;
    }

    public void setWaitSellerDeliveryCount(int waitSellerDeliveryCount) {
        this.waitSellerDeliveryCount = waitSellerDeliveryCount;
    }

    public int getWaitBuyerReceiveCount() {
        return waitBuyerReceiveCount;
    }

    public void setWaitBuyerReceiveCount(int waitBuyerReceiveCount) {
        this.waitBuyerReceiveCount = waitBuyerReceiveCount;
    }

    @Override
    public String toString() {
        return "OrderSummary [uin=" + uin + ", totalCount=" + totalCount + ", waitBuyerPayCount=" + waitBuyerPayCount
                        + ", waitSellerDeliveryCount=" + waitSellerDeliveryCount + ", waitBuyerReceiveCount="
                        + waitBuyerReceiveCount + "]";
    }

}
