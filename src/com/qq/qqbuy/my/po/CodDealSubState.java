package com.qq.qqbuy.my.po;



/**
 * 货到付款订单子状态
 * 
 */
public class CodDealSubState
{
    // 包括７种：待买家短信确认、待供货商确认、待供货商发货、待确认收货、交易成功、交易取消、已拒收
    // 待买家确认
    private final static String WAIT_FOR_BUYER_CONFIRM          = "待买家短信确认";

    // 待卖家确认
    private final static String WAIT_FOR_SELLER_CONFIRM         = "待供货商确认";

    // 待卖家发货
    private final static String WAIT_FOR_SELLER_SEND            = "待供货商发货";

    // 待确认收货
    private final static String WAIT_FOR_SELLER_CONFIRM_RECEIVE = "待确认收货";

    // 交易成功
    private final static String DEAL_SUCCESS                    = "交易成功";

    //* 买家拒收
    private final static String DEAL_DENIED                     = "已拒收";

    // 交易关闭
    private final static String DEAL_CLOSED                     = "交易取消";

    public static String getDealStateDesc(int codDealSubState)
    {
        if (codDealSubState == 1)
        {
            return WAIT_FOR_BUYER_CONFIRM;
        }

        if (codDealSubState == 2)
        {
            return WAIT_FOR_SELLER_CONFIRM;
        }

        if (codDealSubState == 3)
        {
            return WAIT_FOR_SELLER_SEND;
        }

        if (codDealSubState == 6)
        {
            return WAIT_FOR_SELLER_CONFIRM_RECEIVE;
        }

        if (codDealSubState == 7)
        {
            return DEAL_SUCCESS;
        }

        if (codDealSubState == 8)
        {
            return DEAL_DENIED;
        }

        if (codDealSubState == 9)
        {
            return DEAL_CLOSED;
        }

        return "";
    }
}
