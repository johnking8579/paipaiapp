package com.qq.qqbuy.my.po;



/**
 * 货到付款类订单
 */
public class CodDealSummary
{
    private long uin;

    /**
     * 所有货到付款订单数量
     */
    private int  totalCount;

    /**
     * 待买家确认订单数量
     */
    private int  waitBuyerConfirmCount;

    /**
     * 待卖家确认订单数量
     */
    private int  waitSellerConfirmCount;

    /**
     * 待卖家发货订单数量
     */
    private int  waitSellerDeliveryCount;

    /**
     * 待确认收货订单数量
     */
    private int  waitBuyerReceiveCount;

    /**
     * 交易成功订单数量
     */
    private int  sucessedCount;

    /**
     * 买家拒收订单数量
     */
    private int  buyerRefusedCount;

    /**
     * 交易关闭
     */
    private int  closedCount;

    public long getUin()
    {
        return uin;
    }

    public void setUin(long uin)
    {
        this.uin = uin;
    }

    public int getTotalCount()
    {
        return totalCount;
    }

    public void setTotalCount(int totalCount)
    {
        this.totalCount = totalCount;
    }

    public int getWaitBuyerConfirmCount()
    {
        return waitBuyerConfirmCount;
    }

    public void setWaitBuyerConfirmCount(int waitBuyerConfirmCount)
    {
        this.waitBuyerConfirmCount = waitBuyerConfirmCount;
    }

    public int getWaitSellerConfirmCount()
    {
        return waitSellerConfirmCount;
    }

    public void setWaitSellerConfirmCount(int waitSellerConfirmCount)
    {
        this.waitSellerConfirmCount = waitSellerConfirmCount;
    }

    public int getWaitSellerDeliveryCount()
    {
        return waitSellerDeliveryCount;
    }

    public void setWaitSellerDeliveryCount(int waitSellerDeliveryCount)
    {
        this.waitSellerDeliveryCount = waitSellerDeliveryCount;
    }

    public int getWaitBuyerReceiveCount()
    {
        return waitBuyerReceiveCount;
    }

    public void setWaitBuyerReceiveCount(int waitBuyerReceiveCount)
    {
        this.waitBuyerReceiveCount = waitBuyerReceiveCount;
    }

    public int getSucessedCount()
    {
        return sucessedCount;
    }

    public void setSucessedCount(int sucessedCount)
    {
        this.sucessedCount = sucessedCount;
    }

    public int getBuyerRefusedCount()
    {
        return buyerRefusedCount;
    }

    public void setBuyerRefusedCount(int buyerRefusedCount)
    {
        this.buyerRefusedCount = buyerRefusedCount;
    }

    public int getClosedCount()
    {
        return closedCount;
    }

    public void setClosedCount(int closedCount)
    {
        this.closedCount = closedCount;
    }

    @Override
    public String toString()
    {
        return "CodOrderSummary [uin=" + uin + ", totalCount=" + totalCount + ", waitBuyerConfirmCount="
                + waitBuyerConfirmCount + ", waitSellerConfirmCount=" + waitSellerConfirmCount
                + ", waitSellerDeliveryCount=" + waitSellerDeliveryCount + ", waitBuyerReceiveCount="
                + waitBuyerReceiveCount + ", sucessedCount=" + sucessedCount + ", buyerRefusedCount="
                + buyerRefusedCount + ", closedCount=" + closedCount + "]";
    }

}
