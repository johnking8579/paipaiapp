package com.qq.qqbuy.my.po;

/**
 * 用于封装我的QQ网购的输出数据
 * @author tylerliu
 * @Created 2012-12-07
 */
public class UserInfoPo {
	
	private long uin;
	
	//截取后的QQ昵称
	private String nickNameStrs=""; 
	
	//用户等级
	private long level;
	
	private String picURL = "http://qlogo3.store.qq.com/qzone/0/0/50";
	
	private String levelPicURL;
	
	private String levelName ;
	
	private String greetingWord = "您好.";
	
	private int hour ;

	public long getUin() {
		return uin;
	}

	public void setUin(long uin) {
		this.uin = uin;
	}

	public String getNickNameStrs() {
		return nickNameStrs;
	}

	public void setNickNameStrs(String nickNameStrs) {
		this.nickNameStrs = nickNameStrs;
	}

	public long getLevel() {
		return level;
	}

	public void setLevel(long level) {
		this.level = level;
	}

	public String getPicURL() 
	{
		//this.picURL = "http://qlogo3.store.qq.com/qzone/"+ uin + "/"+ uin + "/50";
		
		return picURL;
	}

	public void setPicURL(String picURL) 
	{
		this.picURL = picURL;
	}

	public String getLevelPicURL() 
	{
		if(this.level > 0)
		{
			this.levelPicURL = "http://3gimg.qq.com/mobilelife/color_lv" + this.level + ".gif";
		}
		return levelPicURL;
	}

	public void setLevelPicURL(String levelPicURL) 
	{
		this.levelPicURL = levelPicURL;
	}

	public String getLevelName() 
	{
		if(this.level > 0)
		{
			this.levelName = "彩钻" + this.level +"级";
		}
		return levelName;
	}

	public void setLevelName(String levelName) 
	{		
		this.levelName = levelName;
	}

	public String getGreetingWord() 
	{
		if(this.hour < 12)
		{
			this.greetingWord = "上午好";
		}
		else if(this.hour >= 12 && this.hour < 14)
		{
			this.greetingWord = "中午好";
		}
		else if(this.hour >= 14 && this.hour < 18)
		{
			this.greetingWord = "下午好";
		}
		else  if(this.hour >= 18 )
		{
			this.greetingWord = "晚上好";
		}
		
		return greetingWord;
	}

	public void setGreetingWord(String greetingWord) {
		this.greetingWord = greetingWord;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}
}
