package com.qq.qqbuy.my.po;

import java.util.Map;

import com.qq.qqbuy.common.Page;
import com.qq.qqbuy.deal.po.DealStateList;
import com.qq.qqbuy.favorite.po.FavoriteItemsPo;
import com.qq.qqbuy.favorite.po.FavoriteShopDetails;
import com.qq.qqbuy.redpacket.po.RedPacketPo;
import com.qq.qqbuy.user.po.GetUserTagsRespPo;

public class MainMySummary {

	/**
     * 等待买家付款
     * DS_WAIT_BUYER_PAY
     */
    private long waitBuyerPayCount = 0;
    
    /**
     * 卖家已发货，等待买家收货
     * DS_WAIT_BUYER_RECEIVE
     */
    private long waitBuyerReceiveCount = 0;
    
    /**
     * 在线支付，待收货
     */
    private long polWaitRecvCount = 0;
    
    /**
     * 总订单数
     */
    private long allDealCount = 0;
    
    /**
     * 待评价数
     */
    private long waitBuyerEvalCount = 0;
    
    /**
     * 收藏商品列表
     */
    private FavoriteItemsPo favoriteItemsPo;
    
    /**
     * 收藏店铺列表
     */
    private FavoriteShopDetails favoriteShopDetails;
    
    /**
     *  标签列表
     */
    private GetUserTagsRespPo getUserTagsRespPo;
    
    /**
     * 优惠券列表
     */
    private Page<RedPacketPo> coupon;
    
    /**
     * 代金券列表
     */
    private Page<RedPacketPo> cashCoupon;
    
    /**
     * 返回各种数量,待收货、待发货、待评价等
     */
    private DealStateList dealStateList;
    
    private Map<String,Integer> dealCount;

	public long getWaitBuyerPayCount() {
		return waitBuyerPayCount;
	}

	public void setWaitBuyerPayCount(long waitBuyerPayCount) {
		this.waitBuyerPayCount = waitBuyerPayCount;
	}

	public long getWaitBuyerReceiveCount() {
		return waitBuyerReceiveCount;
	}

	public void setWaitBuyerReceiveCount(long waitBuyerReceiveCount) {
		this.waitBuyerReceiveCount = waitBuyerReceiveCount;
	}

	public long getWaitBuyerEvalCount() {
		return waitBuyerEvalCount;
	}

	public void setWaitBuyerEvalCount(long waitBuyerEvalCount) {
		this.waitBuyerEvalCount = waitBuyerEvalCount;
	}

	public long getPolWaitRecvCount() {
		return polWaitRecvCount;
	}

	public void setPolWaitRecvCount(long polWaitRecvCount) {
		this.polWaitRecvCount = polWaitRecvCount;
	}

	public long getAllDealCount() {
		return allDealCount;
	}

	public void setAllDealCount(long allDealCount) {
		this.allDealCount = allDealCount;
	}

	@Override
	public String toString() {
		return "MainMySummary [waitBuyerEvalCount=" + waitBuyerEvalCount
				+ ", waitBuyerPayCount=" + waitBuyerPayCount
				+ ", waitBuyerReceiveCount=" + waitBuyerReceiveCount
				+ ", getUserTagsRespPo=" + getUserTagsRespPo
				+ ", favoriteItemsPo=" + favoriteItemsPo
				+ ", favoriteShopDetails=" + favoriteShopDetails
				+ ", coupon=" + coupon
				+ ", cashCoupon=" + cashCoupon
				+ "]";
	}

	public FavoriteItemsPo getFavoriteItemsPo() {
		return favoriteItemsPo;
	}

	public void setFavoriteItemsPo(FavoriteItemsPo favoriteItemsPo) {
		this.favoriteItemsPo = favoriteItemsPo;
	}

	public FavoriteShopDetails getFavoriteShopDetails() {
		return favoriteShopDetails;
	}

	public void setFavoriteShopDetails(FavoriteShopDetails favoriteShopDetails) {
		this.favoriteShopDetails = favoriteShopDetails;
	}

	public GetUserTagsRespPo getGetUserTagsRespPo() {
		return getUserTagsRespPo;
	}

	public void setGetUserTagsRespPo(GetUserTagsRespPo getUserTagsRespPo) {
		this.getUserTagsRespPo = getUserTagsRespPo;
	}

	public Page<RedPacketPo> getCoupon() {
		return coupon;
	}

	public void setCoupon(Page<RedPacketPo> coupon) {
		this.coupon = coupon;
	}

	public Page<RedPacketPo> getCashCoupon() {
		return cashCoupon;
	}

	public void setCashCoupon(Page<RedPacketPo> cashCoupon) {
		this.cashCoupon = cashCoupon;
	}

	public DealStateList getDealStateList() {
		return dealStateList;
	}

	public void setDealStateList(DealStateList dealStateList) {
		this.dealStateList = dealStateList;
	}

	public Map<String, Integer> getDealCount() {
		return dealCount;
	}

	public void setDealCount(Map<String, Integer> dealCount) {
		this.dealCount = dealCount;
	}
}
