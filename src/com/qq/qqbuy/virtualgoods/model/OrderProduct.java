package com.qq.qqbuy.virtualgoods.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 订单详情实体
 * 
 * @author lhn
 * 
 */
public class OrderProduct implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8251858623718767207L;
	/**
	 * 表自增id
	 */
	private Long id;
	/**
	 * 订单编号
	 */
	private Long fdealId;
	/**
	 * 商品编号
	 */
	private String fitemId;
	/**
	 * 商家本地商品编码
	 */
	private String fitemLocalCode;
	/**
	 * 商品系统库存编号
	 */
	private Long fitemStockId;
	/**
	 * 商家本地库存编码
	 */
	private String fstockLocalCode;
	/**
	 * 商品名称
	 */
	private String fitemName;
	/**
	 * 产品编码
	 */
	private String fproductCode;
	/**
	 * 商品价格
	 */
	private Integer fitemPrice;
	/**
	 * 商品销售属性选项组合值
	 */
	private String fitemAttrOptionValue;
	/**
	 * 商品促销说明
	 */
	private String fitemPromotionDesc;
	/**
	 * 订单商品状态
	 */
	private Byte fdealItemState;
	/**
	 * 实际销售数目
	 */
	private Integer fdealItemNum;
	/**
	 * 订单状态
	 */
	private Byte fdealState;
	/**
	 * 支付方式
	 */
	private Byte fdealPayType;
	/**
	 * 是否自动发货商品
	 */
	private Byte fisAutoSendItem;
	/**
	 * 积分
	 */
	private Integer fdealItemScore;
	/**
	 * 购买数量
	 */
	private Integer fdealItemCount;
	/**
	 * 最后更新时间
	 */
	private Date flastUpdateTime;
	/**
	 * 商品标配说明
	 */
	private String fitemAccessoryDesc;
	/**
	 * 商家商品备注信息
	 */
	private String fsellerRemark;
	/**
	 * 买家商品备注信息
	 */
	private String fbuyerRemark;
	/**
	 * 商家编号
	 */
	private Long fsellerUin;
	/**
	 * 商家名称
	 */
	private String fsellerName;
	/**
	 * 买家编号
	 */
	private Long fbuyerUin;
	/**
	 * 买家名称
	 */
	private String fbuyerName;
	/**
	 * 送送所需天数
	 */
	private String freceiveDays;
	/**
	 * 运费模板编号
	 */
	private Long fshippingfeeTemplateId;
	/**
	 * 运费模板说明
	 */
	private String fshippingfeeDesc;
	/**
	 * 商品运费
	 */
	private Integer fitemShippingFee;
	/**
	 * 商品类型
	 */
	private Byte fitemType;
	/**
	 * 订单生成时间
	 */
	private Date fdealCreateTime;
	/**
	 * 子订单编号
	 */
	private Long ftradeId;
	/**
	 * 子订单状态
	 */
	private Integer ftradeState;
	/**
	 * 子订单属性
	 */
	private Integer ftradePropertymask;
	/**
	 * 子订单类型
	 */
	private Integer ftradeType;
	/**
	 * 商品快照版本
	 */
	private Byte fitemSnapversion;
	/**
	 * 商品重置时间
	 */
	private Date fitemResetTime;
	/**
	 * 商品类目
	 */
	private Integer fitemClass;
	/**
	 * 商品图片主图
	 */
	private String fitemLogo;
	/**
	 * 商品成本价
	 */
	private Integer fitemCostPrice;
	/**
	 * 商品价格调整优惠
	 */
	private Integer fitemAdjustPrice;
	/**
	 * 退款协议ID
	 */
	private Long ftradeRefundId;
	/**
	 * 退款状态
	 */
	private Integer ftradeRefundState;
	/**
	 * 关闭原因
	 */
	private Integer fcloseReason;
	/**
	 * 关闭原因描述
	 */
	private String fcloseReasonDesc;
	/**
	 * 套餐信息
	 */
	private String fpackageInfo;
	/**
	 * 卖家延迟收货时间
	 */
	private Integer fdelayRecvtime;
	/**
	 * 卖家标记缺货时间
	 */
	private Date fmarkNostockTime;
	/**
	 * 支付完成时间
	 */
	private Date fpayTime;
	/**
	 * 卖家发货时间
	 */
	private Date fsellerConsignmentTime;
	/**
	 * 退款状态
	 */
	private Integer fdealRefundState;
	/**
	 * 商品原价
	 */
	private Integer fitemOriginalPrice;
	/**
	 * 交易取消时间
	 */
	private Date fcloseTime;
	/**
	 * 超时商品标识
	 */
	private Integer ftimeoutItemFlag;
	/**
	 * 折扣（红包）金额
	 */
	private Integer fdiscountFee;
	/**
	 * 打款完成时间
	 */
	private Date frecvfeeTime;
	/**
	 * 打款返回时间
	 */
	private Date frecvfeeReturnTime;
	/**
	 * 打款:买家打款金额
	 */
	private Integer fbuyerFee;
	/**
	 * 打款:卖家实收金额
	 */
	private Integer fsellerFee;
	/**
	 * 打款单号
	 */
	private String fdrawId;
	/**
	 * 打款验证token
	 */
	private String ftoken;
	/**
	 * 子订单属性
	 */
	private Integer ftradePropertymask2;
	/**
	 * 网购商品单号
	 */
	private String fb2b2cTdealCode;
	
	private String fitemSkuId;

	
	public String getFitemSkuId() {
		return fitemSkuId;
	}

	public void setFitemSkuId(String fitemSkuId) {
		this.fitemSkuId = fitemSkuId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFdealId() {
		return fdealId;
	}

	public void setFdealId(Long fdealId) {
		this.fdealId = fdealId;
	}

	public String getFitemId() {
		return fitemId;
	}

	public void setFitemId(String fitemId) {
		this.fitemId = fitemId;
	}

	public String getFitemLocalCode() {
		return fitemLocalCode;
	}

	public void setFitemLocalCode(String fitemLocalCode) {
		this.fitemLocalCode = fitemLocalCode;
	}

	public Long getFitemStockId() {
		return fitemStockId;
	}

	public void setFitemStockId(Long fitemStockId) {
		this.fitemStockId = fitemStockId;
	}

	public String getFstockLocalCode() {
		return fstockLocalCode;
	}

	public void setFstockLocalCode(String fstockLocalCode) {
		this.fstockLocalCode = fstockLocalCode;
	}

	public String getFitemName() {
		return fitemName;
	}

	public void setFitemName(String fitemName) {
		this.fitemName = fitemName;
	}

	public String getFproductCode() {
		return fproductCode;
	}

	public void setFproductCode(String fproductCode) {
		this.fproductCode = fproductCode;
	}

	public Integer getFitemPrice() {
		return fitemPrice;
	}

	public void setFitemPrice(Integer fitemPrice) {
		this.fitemPrice = fitemPrice;
	}

	public String getFitemPromotionDesc() {
		return fitemPromotionDesc;
	}

	public void setFitemPromotionDesc(String fitemPromotionDesc) {
		this.fitemPromotionDesc = fitemPromotionDesc;
	}

	public Byte getFdealItemState() {
		return fdealItemState;
	}

	public void setFdealItemState(Byte fdealItemState) {
		this.fdealItemState = fdealItemState;
	}

	public Integer getFdealItemNum() {
		return fdealItemNum;
	}

	public void setFdealItemNum(Integer fdealItemNum) {
		this.fdealItemNum = fdealItemNum;
	}

	public Byte getFdealState() {
		return fdealState;
	}

	public void setFdealState(Byte fdealState) {
		this.fdealState = fdealState;
	}

	public Byte getFdealPayType() {
		return fdealPayType;
	}

	public void setFdealPayType(Byte fdealPayType) {
		this.fdealPayType = fdealPayType;
	}

	public Byte getFisAutoSendItem() {
		return fisAutoSendItem;
	}

	public void setFisAutoSendItem(Byte fisAutoSendItem) {
		this.fisAutoSendItem = fisAutoSendItem;
	}

	public Integer getFdealItemScore() {
		return fdealItemScore;
	}

	public void setFdealItemScore(Integer fdealItemScore) {
		this.fdealItemScore = fdealItemScore;
	}

	public Integer getFdealItemCount() {
		return fdealItemCount;
	}

	public void setFdealItemCount(Integer fdealItemCount) {
		this.fdealItemCount = fdealItemCount;
	}

	public Date getFlastUpdateTime() {
		return flastUpdateTime;
	}

	public void setFlastUpdateTime(Date flastUpdateTime) {
		this.flastUpdateTime = flastUpdateTime;
	}

	public String getFitemAccessoryDesc() {
		return fitemAccessoryDesc;
	}

	public void setFitemAccessoryDesc(String fitemAccessoryDesc) {
		this.fitemAccessoryDesc = fitemAccessoryDesc;
	}

	public String getFsellerRemark() {
		return fsellerRemark;
	}

	public void setFsellerRemark(String fsellerRemark) {
		this.fsellerRemark = fsellerRemark;
	}

	public String getFbuyerRemark() {
		return fbuyerRemark;
	}

	public void setFbuyerRemark(String fbuyerRemark) {
		this.fbuyerRemark = fbuyerRemark;
	}

	public Long getFsellerUin() {
		return fsellerUin;
	}

	public void setFsellerUin(Long fsellerUin) {
		this.fsellerUin = fsellerUin;
	}

	public String getFsellerName() {
		return fsellerName;
	}

	public void setFsellerName(String fsellerName) {
		this.fsellerName = fsellerName;
	}

	public Long getFbuyerUin() {
		return fbuyerUin;
	}

	public void setFbuyerUin(Long fbuyerUin) {
		this.fbuyerUin = fbuyerUin;
	}

	public String getFbuyerName() {
		return fbuyerName;
	}

	public void setFbuyerName(String fbuyerName) {
		this.fbuyerName = fbuyerName;
	}

	public String getFreceiveDays() {
		return freceiveDays;
	}

	public void setFreceiveDays(String freceiveDays) {
		this.freceiveDays = freceiveDays;
	}

	public Long getFshippingfeeTemplateId() {
		return fshippingfeeTemplateId;
	}

	public void setFshippingfeeTemplateId(Long fshippingfeeTemplateId) {
		this.fshippingfeeTemplateId = fshippingfeeTemplateId;
	}

	public String getFshippingfeeDesc() {
		return fshippingfeeDesc;
	}

	public void setFshippingfeeDesc(String fshippingfeeDesc) {
		this.fshippingfeeDesc = fshippingfeeDesc;
	}

	public Integer getFitemShippingFee() {
		return fitemShippingFee;
	}

	public void setFitemShippingFee(Integer fitemShippingFee) {
		this.fitemShippingFee = fitemShippingFee;
	}

	public Byte getFitemType() {
		return fitemType;
	}

	public void setFitemType(Byte fitemType) {
		this.fitemType = fitemType;
	}

	public Date getFdealCreateTime() {
		return fdealCreateTime;
	}

	public void setFdealCreateTime(Date fdealCreateTime) {
		this.fdealCreateTime = fdealCreateTime;
	}

	public Long getFtradeId() {
		return ftradeId;
	}

	public void setFtradeId(Long ftradeId) {
		this.ftradeId = ftradeId;
	}

	public Integer getFtradeState() {
		return ftradeState;
	}

	public void setFtradeState(Integer ftradeState) {
		this.ftradeState = ftradeState;
	}

	public Integer getFtradePropertymask() {
		return ftradePropertymask;
	}

	public void setFtradePropertymask(Integer ftradePropertymask) {
		this.ftradePropertymask = ftradePropertymask;
	}

	public Integer getFtradeType() {
		return ftradeType;
	}

	public void setFtradeType(Integer ftradeType) {
		this.ftradeType = ftradeType;
	}

	public Byte getFitemSnapversion() {
		return fitemSnapversion;
	}

	public void setFitemSnapversion(Byte fitemSnapversion) {
		this.fitemSnapversion = fitemSnapversion;
	}

	public Date getFitemResetTime() {
		return fitemResetTime;
	}

	public void setFitemResetTime(Date fitemResetTime) {
		this.fitemResetTime = fitemResetTime;
	}

	public Integer getFitemClass() {
		return fitemClass;
	}

	public void setFitemClass(Integer fitemClass) {
		this.fitemClass = fitemClass;
	}

	public String getFitemLogo() {
		return fitemLogo;
	}

	public void setFitemLogo(String fitemLogo) {
		this.fitemLogo = fitemLogo;
	}

	public Integer getFitemCostPrice() {
		return fitemCostPrice;
	}

	public void setFitemCostPrice(Integer fitemCostPrice) {
		this.fitemCostPrice = fitemCostPrice;
	}

	public Integer getFitemAdjustPrice() {
		return fitemAdjustPrice;
	}

	public void setFitemAdjustPrice(Integer fitemAdjustPrice) {
		this.fitemAdjustPrice = fitemAdjustPrice;
	}

	public Long getFtradeRefundId() {
		return ftradeRefundId;
	}

	public void setFtradeRefundId(Long ftradeRefundId) {
		this.ftradeRefundId = ftradeRefundId;
	}

	public Integer getFtradeRefundState() {
		return ftradeRefundState;
	}

	public void setFtradeRefundState(Integer ftradeRefundState) {
		this.ftradeRefundState = ftradeRefundState;
	}

	public Integer getFcloseReason() {
		return fcloseReason;
	}

	public void setFcloseReason(Integer fcloseReason) {
		this.fcloseReason = fcloseReason;
	}

	public String getFcloseReasonDesc() {
		return fcloseReasonDesc;
	}

	public void setFcloseReasonDesc(String fcloseReasonDesc) {
		this.fcloseReasonDesc = fcloseReasonDesc;
	}

	public String getFpackageInfo() {
		return fpackageInfo;
	}

	public void setFpackageInfo(String fpackageInfo) {
		this.fpackageInfo = fpackageInfo;
	}

	public Integer getFdelayRecvtime() {
		return fdelayRecvtime;
	}

	public void setFdelayRecvtime(Integer fdelayRecvtime) {
		this.fdelayRecvtime = fdelayRecvtime;
	}

	public Date getFmarkNostockTime() {
		return fmarkNostockTime;
	}

	public void setFmarkNostockTime(Date fmarkNostockTime) {
		this.fmarkNostockTime = fmarkNostockTime;
	}

	public Date getFpayTime() {
		return fpayTime;
	}

	public void setFpayTime(Date fpayTime) {
		this.fpayTime = fpayTime;
	}

	public Date getFsellerConsignmentTime() {
		return fsellerConsignmentTime;
	}

	public void setFsellerConsignmentTime(Date fsellerConsignmentTime) {
		this.fsellerConsignmentTime = fsellerConsignmentTime;
	}

	public Integer getFdealRefundState() {
		return fdealRefundState;
	}

	public void setFdealRefundState(Integer fdealRefundState) {
		this.fdealRefundState = fdealRefundState;
	}

	public Integer getFitemOriginalPrice() {
		return fitemOriginalPrice;
	}

	public void setFitemOriginalPrice(Integer fitemOriginalPrice) {
		this.fitemOriginalPrice = fitemOriginalPrice;
	}

	public Date getFcloseTime() {
		return fcloseTime;
	}

	public void setFcloseTime(Date fcloseTime) {
		this.fcloseTime = fcloseTime;
	}

	public Integer getFtimeoutItemFlag() {
		return ftimeoutItemFlag;
	}

	public void setFtimeoutItemFlag(Integer ftimeoutItemFlag) {
		this.ftimeoutItemFlag = ftimeoutItemFlag;
	}

	public Integer getFdiscountFee() {
		return fdiscountFee;
	}

	public void setFdiscountFee(Integer fdiscountFee) {
		this.fdiscountFee = fdiscountFee;
	}

	public Date getFrecvfeeTime() {
		return frecvfeeTime;
	}

	public void setFrecvfeeTime(Date frecvfeeTime) {
		this.frecvfeeTime = frecvfeeTime;
	}

	public Date getFrecvfeeReturnTime() {
		return frecvfeeReturnTime;
	}

	public void setFrecvfeeReturnTime(Date frecvfeeReturnTime) {
		this.frecvfeeReturnTime = frecvfeeReturnTime;
	}

	public Integer getFbuyerFee() {
		return fbuyerFee;
	}

	public void setFbuyerFee(Integer fbuyerFee) {
		this.fbuyerFee = fbuyerFee;
	}

	public Integer getFsellerFee() {
		return fsellerFee;
	}

	public void setFsellerFee(Integer fsellerFee) {
		this.fsellerFee = fsellerFee;
	}

	public String getFdrawId() {
		return fdrawId;
	}

	public void setFdrawId(String fdrawId) {
		this.fdrawId = fdrawId;
	}

	public String getFtoken() {
		return ftoken;
	}

	public void setFtoken(String ftoken) {
		this.ftoken = ftoken;
	}

	public Integer getFtradePropertymask2() {
		return ftradePropertymask2;
	}

	public void setFtradePropertymask2(Integer ftradePropertymask2) {
		this.ftradePropertymask2 = ftradePropertymask2;
	}

	public String getFb2b2cTdealCode() {
		return fb2b2cTdealCode;
	}

	public void setFb2b2cTdealCode(String fb2b2cTdealCode) {
		this.fb2b2cTdealCode = fb2b2cTdealCode;
	}

	public String getFitemAttrOptionValue() {
		return fitemAttrOptionValue;
	}

	public void setFitemAttrOptionValue(String fitemAttrOptionValue) {
		this.fitemAttrOptionValue = fitemAttrOptionValue;
	}

}
