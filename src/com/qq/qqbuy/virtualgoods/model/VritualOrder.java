package com.qq.qqbuy.virtualgoods.model;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;
/**
 * 

 * @ClassName: VritualOrder 

 * @Description: 用于给前端显示的内容
 
 * @author lhn

 * @date 2014-12-26 上午10:04:52 

 *
 */
public class VritualOrder implements Serializable {
	//订单编号
	private Integer fdealId;
	//下单时间
	private String fdealCreateTime = new String();
	//商品编号
	private String fitemId = new String();
	/**
	 * 商品名称
	 */
	private String fitemName;
	/**
	 * 商品价格
	 */
	private int fitemPrice;
	/**
	 * 购买数量
	 */
	private int fdealItemCount;
	/**
	 * 商品图片主图
	 */
	private String fitemLogo;
	/**
	 * 费用合计
	 */
	private int fdealPayFeeTotal;
	/**
	 * 买家编号
	 */
	private long fbuyerUin;
	/**
	 * 商家编号
	 */
	private long fsellerUin;
	/**
	 * 虚拟订单类型，5：Q币 6：话费
	 */
	private int fdealType;
	/**
	 * 订单状态
	 */
	private int fdealState;
	/**
	 * 支付方式
	 */
	private int fdealPayType;
	
	/**
	 * 店家名称
	 */
	private String fsellerName;
	
	/**
	 * 充值账号
	 */
	private String frechargeAccount;
	
	
	
	public String getFrechargeAccount() {
		return frechargeAccount;
	}
	public void setFrechargeAccount(String frechargeAccount) {
		this.frechargeAccount = frechargeAccount;
	}
	private String fitemSkuId;
	
	
	public String getFitemSkuId() {
		return fitemSkuId;
	}
	public void setFitemSkuId(String fitemSkuId) {
		this.fitemSkuId = fitemSkuId;
	}
	public Integer getFdealId() {
		return fdealId;
	}
	public void setFdealId(Integer fdealId) {
		this.fdealId = fdealId;
	}
	public String getFdealCreateTime() {
		return fdealCreateTime;
	}
	public void setFdealCreateTime(String fdealCreateTime) {
		this.fdealCreateTime = fdealCreateTime;
	}
	public String getFitemId() {
		return fitemId;
	}
	public void setFitemId(String fitemId) {
		this.fitemId = fitemId;
	}
	public String getFitemName() {
		return fitemName;
	}
	public void setFitemName(String fitemName) {
		this.fitemName = fitemName;
	}
	public int getFitemPrice() {
		return fitemPrice;
	}
	public void setFitemPrice(int fitemPrice) {
		this.fitemPrice = fitemPrice;
	}
	public int getFdealItemCount() {
		return fdealItemCount;
	}
	public void setFdealItemCount(int fdealItemCount) {
		this.fdealItemCount = fdealItemCount;
	}
	public String getFitemLogo() {
		return fitemLogo;
	}
	public void setFitemLogo(String fitemLogo) {
		this.fitemLogo = fitemLogo;
	}
	public int getFdealPayFeeTotal() {
		return fdealPayFeeTotal;
	}
	public void setFdealPayFeeTotal(int fdealPayFeeTotal) {
		this.fdealPayFeeTotal = fdealPayFeeTotal;
	}
	public long getFbuyerUin() {
		return fbuyerUin;
	}
	public void setFbuyerUin(long fbuyerUin) {
		this.fbuyerUin = fbuyerUin;
	}
	public long getFsellerUin() {
		return fsellerUin;
	}
	public void setFsellerUin(long fsellerUin) {
		this.fsellerUin = fsellerUin;
	}
	public int getFdealState() {
		return fdealState;
	}
	public void setFdealState(int fdealState) {
		this.fdealState = fdealState;
	}
	public int getFdealPayType() {
		return fdealPayType;
	}
	public void setFdealPayType(int fdealPayType) {
		this.fdealPayType = fdealPayType;
	}
	public String getFsellerName() {
		return fsellerName;
	}
	public void setFsellerName(String fsellerName) {
		this.fsellerName = fsellerName;
	}

	public int getFdealType() {
		return fdealType;
	}

	public void setFdealType(int fdealType) {
		this.fdealType = fdealType;
	}
}
