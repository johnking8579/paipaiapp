package com.qq.qqbuy.virtualgoods.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 订单实体类
 * 
 * @author lhn
 * 
 */
public class Order implements Cloneable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1541316894974941296L;
	/**
	 * 表自增id
	 */
	private Long id;
	/**
	 * 订单编号
	 */
	private Long fdealId;
	/**
	 * 商家编号
	 */
	private Long fsellerUin;
	/**
	 * 商家名称
	 */
	private String fsellerName;
	/**
	 * 买家编号
	 */
	private Long fbuyerUin;
	/**
	 * 买家名称
	 */
	private String fbuyerName;
	/**
	 * 订单描述
	 */
	private String fdealDesc;
	/**
	 * 订单类型
	 */
	private Byte fdealType;
	/**
	 * 收货地址_姓名
	 */
	private String freceiveName;
	/**
	 * 收货地址_地址
	 */
	private String freceiveAddr;
	/**
	 * 收货地址_邮编
	 */
	private String freceivePostcode;
	/**
	 * 收货地息_电话
	 */
	private String freceiveTel;
	/**
	 * 收货地址_手机
	 */
	private String freceiveMobile;
	/**
	 * 支付方式
	 */
	private Byte fdealPayType;
	/**
	 * 现金支付金额
	 */
	private Integer fdealPayFeeCash;
	/**
	 * 财付券支付金额
	 */
	private Integer fdealPayFeeTicket;
	/**
	 * 积分支付金额
	 */
	private Integer fdealPayFeeScore;
	/**
	 * 运费合计
	 */
	private Integer fdealPayFeeShipping;
	/**
	 * 费用合计
	 */
	private Integer fdealPayFeeTotal;
	/**
	 * 获得积分总额
	 */
	private Integer fdealRecvScore;
	/**
	 * 订单状态
	 */
	private Byte fdealState;
	/**
	 * 订单支付ID
	 */
	private String fdealCftPayid;
	/**
	 * 订单生成时间
	 */
	private Date fdealCreateTime;
	/**
	 * 交易结束时间
	 */
	private Date fdealEndTime;
	/**
	 * 是否有发票
	 */
	private Long fhasInvoice;
	/**
	 * 发票抬头
	 */
	private String fdealInvoiceTitle;
	/**
	 * 发票内容
	 */
	private String fdealInvoiceContent;
	/**
	 * 订单最后更新时间
	 */
	private Date flastUpdateTime;
	/**
	 * 客服CRM
	 */
	private Integer fsellerCrm;
	/**
	 * 运费合计说明
	 */
	private String fshippingfeeCalc;
	/**
	 * 承担运费方式
	 */
	private Byte fwhoPayShippingfee;
	/**
	 * 备注类型
	 */
	private Byte fdealNoteType;
	/**
	 * 备注说明
	 */
	private String fdealNote;
	/**
	 * 订单评价状态
	 */
	private Integer fdealRateState;
	/**
	 * 退款投诉状态
	 */
	private Byte fdealRefundState;
	/**
	 * 商品标题列表
	 */
	private String fitemTitleList;
	/**
	 * 邮递类型
	 */
	private Byte fmailType;
	/**
	 * 促销信息
	 */
	private String fcomboInfo;
	/**
	 * 折扣优惠金额
	 */
	private Integer fcouponFee;
	/**
	 * 订单属性
	 */
	private Integer fpropertymask;
	/**
	 * 物流单ID
	 */
	private Long fwuliuId;
	/**
	 * 退款:买家退款金额
	 */
	private Integer fbuyerRecvRefund;
	/**
	 * 退款:卖家实收金额
	 */
	private Integer fsellerRecvRefund;
	/**
	 * 支付完成时间
	 */
	private Date fpayTime;
	/**
	 * 支付返回时间
	 */
	private Date fpayReturnTime;
	/**
	 * 打款完成时间
	 */
	private Date frecvfeeTime;
	/**
	 * 打款返回时间
	 */
	private Date frecvfeeReturnTime;
	/**
	 * 收货地址_地址编码
	 */
	private String freceiveAddrCode;
	/**
	 * 卖家发货时间
	 */
	private Date fsellerConsignmentTime;
	/**
	 * 商品ID签名
	 */
	private Long fitemlistMd5;
	/**
	 * 订单序列
	 */
	private Long fgenTimestamp;
	/**
	 * 订单来源信息
	 */
	private String freferer;
	/**
	 * 卖家昵称
	 */
	private String fsellerNick;
	/**
	 * 买家昵称
	 */
	private String fbuyerNick;
	/**
	 * 支付单编号
	 */
	private Long fpayId;
	/**
	 * 买家商品备注信息
	 */
	private String fbuyerBuyRemark;
	/**
	 * 数据版本号
	 */
	private Integer fdataVersion;
	/**
	 * 货到付款手续费
	 */
	private Integer fdealPayFeeCommission;
	/**
	 * 隐藏标识
	 */
	private Integer fhideFlag;
	/**
	 * 网购备货单ID
	 */
	private Long fb2b2cWdealId;
	/**
	 * 网购备货单号
	 */
	private String fb2b2cWdealCode;

	/**
	 * 访问者Ip
	 */
	private String fvisitorIp;
	/**
	 * 虚拟充值账户
	 */
	private String frechargeAccount;
	/**
	 * 商品详细信息
	 */
	private String orderDetailText;
	/**
	 * 卖家手机
	 */
	private String fsellerMobile;
	/**
	 * 卖家邮箱
	 */
	private String FsellerMail;

	private List<OrderProduct> orderProductList;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFdealId() {
		return fdealId;
	}

	public void setFdealId(Long fdealId) {
		this.fdealId = fdealId;
	}

	public Long getFsellerUin() {
		return fsellerUin;
	}

	public void setFsellerUin(Long fsellerUin) {
		this.fsellerUin = fsellerUin;
	}

	public String getFsellerName() {
		return fsellerName;
	}

	public void setFsellerName(String fsellerName) {
		this.fsellerName = fsellerName;
	}

	public Long getFbuyerUin() {
		return fbuyerUin;
	}

	public void setFbuyerUin(Long fbuyerUin) {
		this.fbuyerUin = fbuyerUin;
	}

	public String getFbuyerName() {
		return fbuyerName;
	}

	public void setFbuyerName(String fbuyerName) {
		this.fbuyerName = fbuyerName;
	}

	public Byte getFdealType() {
		return fdealType;
	}

	public void setFdealType(Byte fdealType) {
		this.fdealType = fdealType;
	}

	public String getFreceiveName() {
		return freceiveName;
	}

	public void setFreceiveName(String freceiveName) {
		this.freceiveName = freceiveName;
	}

	public String getFreceiveAddr() {
		return freceiveAddr;
	}

	public void setFreceiveAddr(String freceiveAddr) {
		this.freceiveAddr = freceiveAddr;
	}

	public String getFreceivePostcode() {
		return freceivePostcode;
	}

	public void setFreceivePostcode(String freceivePostcode) {
		this.freceivePostcode = freceivePostcode;
	}

	public String getFreceiveTel() {
		return freceiveTel;
	}

	public void setFreceiveTel(String freceiveTel) {
		this.freceiveTel = freceiveTel;
	}

	public String getFreceiveMobile() {
		return freceiveMobile;
	}

	public void setFreceiveMobile(String freceiveMobile) {
		this.freceiveMobile = freceiveMobile;
	}

	public Byte getFdealPayType() {
		return fdealPayType;
	}

	public void setFdealPayType(Byte fdealPayType) {
		this.fdealPayType = fdealPayType;
	}

	public Integer getFdealPayFeeCash() {
		return fdealPayFeeCash;
	}

	public void setFdealPayFeeCash(Integer fdealPayFeeCash) {
		this.fdealPayFeeCash = fdealPayFeeCash;
	}

	public Integer getFdealPayFeeTicket() {
		return fdealPayFeeTicket;
	}

	public void setFdealPayFeeTicket(Integer fdealPayFeeTicket) {
		this.fdealPayFeeTicket = fdealPayFeeTicket;
	}

	public Integer getFdealPayFeeScore() {
		return fdealPayFeeScore;
	}

	public void setFdealPayFeeScore(Integer fdealPayFeeScore) {
		this.fdealPayFeeScore = fdealPayFeeScore;
	}

	public Integer getFdealPayFeeShipping() {
		return fdealPayFeeShipping;
	}

	public void setFdealPayFeeShipping(Integer fdealPayFeeShipping) {
		this.fdealPayFeeShipping = fdealPayFeeShipping;
	}

	public Integer getFdealPayFeeTotal() {
		return fdealPayFeeTotal;
	}

	public void setFdealPayFeeTotal(Integer fdealPayFeeTotal) {
		this.fdealPayFeeTotal = fdealPayFeeTotal;
	}

	public Integer getFdealRecvScore() {
		return fdealRecvScore;
	}

	public void setFdealRecvScore(Integer fdealRecvScore) {
		this.fdealRecvScore = fdealRecvScore;
	}

	public Byte getFdealState() {
		return fdealState;
	}

	public void setFdealState(Byte fdealState) {
		this.fdealState = fdealState;
	}

	public String getFdealCftPayid() {
		return fdealCftPayid;
	}

	public void setFdealCftPayid(String fdealCftPayid) {
		this.fdealCftPayid = fdealCftPayid;
	}

	public Date getFdealCreateTime() {
		return fdealCreateTime;
	}

	public void setFdealCreateTime(Date fdealCreateTime) {
		this.fdealCreateTime = fdealCreateTime;
	}

	public Date getFdealEndTime() {
		return fdealEndTime;
	}

	public void setFdealEndTime(Date fdealEndTime) {
		this.fdealEndTime = fdealEndTime;
	}

	public Long getFhasInvoice() {
		return fhasInvoice;
	}

	public void setFhasInvoice(Long fhasInvoice) {
		this.fhasInvoice = fhasInvoice;
	}

	public String getFdealInvoiceTitle() {
		return fdealInvoiceTitle;
	}

	public void setFdealInvoiceTitle(String fdealInvoiceTitle) {
		this.fdealInvoiceTitle = fdealInvoiceTitle;
	}

	public String getFdealInvoiceContent() {
		return fdealInvoiceContent;
	}

	public void setFdealInvoiceContent(String fdealInvoiceContent) {
		this.fdealInvoiceContent = fdealInvoiceContent;
	}

	public Date getFlastUpdateTime() {
		return flastUpdateTime;
	}

	public void setFlastUpdateTime(Date flastUpdateTime) {
		this.flastUpdateTime = flastUpdateTime;
	}

	public Integer getFsellerCrm() {
		return fsellerCrm;
	}

	public void setFsellerCrm(Integer fsellerCrm) {
		this.fsellerCrm = fsellerCrm;
	}

	public Byte getFwhoPayShippingfee() {
		return fwhoPayShippingfee;
	}

	public void setFwhoPayShippingfee(Byte fwhoPayShippingfee) {
		this.fwhoPayShippingfee = fwhoPayShippingfee;
	}

	public Byte getFdealNoteType() {
		return fdealNoteType;
	}

	public void setFdealNoteType(Byte fdealNoteType) {
		this.fdealNoteType = fdealNoteType;
	}

	public String getFdealNote() {
		return fdealNote;
	}

	public void setFdealNote(String fdealNote) {
		this.fdealNote = fdealNote;
	}

	public Integer getFdealRateState() {
		return fdealRateState;
	}

	public void setFdealRateState(Integer fdealRateState) {
		this.fdealRateState = fdealRateState;
	}

	public Byte getFdealRefundState() {
		return fdealRefundState;
	}

	public void setFdealRefundState(Byte fdealRefundState) {
		this.fdealRefundState = fdealRefundState;
	}

	public String getFitemTitleList() {
		return fitemTitleList;
	}

	public void setFitemTitleList(String fitemTitleList) {
		this.fitemTitleList = fitemTitleList;
	}

	public Byte getFmailType() {
		return fmailType;
	}

	public void setFmailType(Byte fmailType) {
		this.fmailType = fmailType;
	}

	public String getFcomboInfo() {
		return fcomboInfo;
	}

	public void setFcomboInfo(String fcomboInfo) {
		this.fcomboInfo = fcomboInfo;
	}

	public Integer getFcouponFee() {
		return fcouponFee;
	}

	public void setFcouponFee(Integer fcouponFee) {
		this.fcouponFee = fcouponFee;
	}

	public Integer getFpropertymask() {
		return fpropertymask;
	}

	public void setFpropertymask(Integer fpropertymask) {
		this.fpropertymask = fpropertymask;
	}

	public Long getFwuliuId() {
		return fwuliuId;
	}

	public void setFwuliuId(Long fwuliuId) {
		this.fwuliuId = fwuliuId;
	}

	public Integer getFbuyerRecvRefund() {
		return fbuyerRecvRefund;
	}

	public void setFbuyerRecvRefund(Integer fbuyerRecvRefund) {
		this.fbuyerRecvRefund = fbuyerRecvRefund;
	}

	public Integer getFsellerRecvRefund() {
		return fsellerRecvRefund;
	}

	public void setFsellerRecvRefund(Integer fsellerRecvRefund) {
		this.fsellerRecvRefund = fsellerRecvRefund;
	}

	public Date getFpayTime() {
		return fpayTime;
	}

	public void setFpayTime(Date fpayTime) {
		this.fpayTime = fpayTime;
	}

	public Date getFpayReturnTime() {
		return fpayReturnTime;
	}

	public void setFpayReturnTime(Date fpayReturnTime) {
		this.fpayReturnTime = fpayReturnTime;
	}

	public Date getFrecvfeeTime() {
		return frecvfeeTime;
	}

	public void setFrecvfeeTime(Date frecvfeeTime) {
		this.frecvfeeTime = frecvfeeTime;
	}

	public Date getFrecvfeeReturnTime() {
		return frecvfeeReturnTime;
	}

	public void setFrecvfeeReturnTime(Date frecvfeeReturnTime) {
		this.frecvfeeReturnTime = frecvfeeReturnTime;
	}

	public String getFreceiveAddrCode() {
		return freceiveAddrCode;
	}

	public void setFreceiveAddrCode(String freceiveAddrCode) {
		this.freceiveAddrCode = freceiveAddrCode;
	}

	public Date getFsellerConsignmentTime() {
		return fsellerConsignmentTime;
	}

	public void setFsellerConsignmentTime(Date fsellerConsignmentTime) {
		this.fsellerConsignmentTime = fsellerConsignmentTime;
	}

	public Long getFitemlistMd5() {
		return fitemlistMd5;
	}

	public void setFitemlistMd5(Long fitemlistMd5) {
		this.fitemlistMd5 = fitemlistMd5;
	}

	public Long getFgenTimestamp() {
		return fgenTimestamp;
	}

	public void setFgenTimestamp(Long fgenTimestamp) {
		this.fgenTimestamp = fgenTimestamp;
	}

	public String getFsellerNick() {
		return fsellerNick;
	}

	public void setFsellerNick(String fsellerNick) {
		this.fsellerNick = fsellerNick;
	}

	public String getFbuyerNick() {
		return fbuyerNick;
	}

	public void setFbuyerNick(String fbuyerNick) {
		this.fbuyerNick = fbuyerNick;
	}

	public Long getFpayId() {
		return fpayId;
	}

	public void setFpayId(Long fpayId) {
		this.fpayId = fpayId;
	}

	public String getFbuyerBuyRemark() {
		return fbuyerBuyRemark;
	}

	public void setFbuyerBuyRemark(String fbuyerBuyRemark) {
		this.fbuyerBuyRemark = fbuyerBuyRemark;
	}

	public Integer getFdataVersion() {
		return fdataVersion;
	}

	public void setFdataVersion(Integer fdataVersion) {
		this.fdataVersion = fdataVersion;
	}

	public Integer getFdealPayFeeCommission() {
		return fdealPayFeeCommission;
	}

	public void setFdealPayFeeCommission(Integer fdealPayFeeCommission) {
		this.fdealPayFeeCommission = fdealPayFeeCommission;
	}

	public Integer getFhideFlag() {
		return fhideFlag;
	}

	public void setFhideFlag(Integer fhideFlag) {
		this.fhideFlag = fhideFlag;
	}

	public Long getFb2b2cWdealId() {
		return fb2b2cWdealId;
	}

	public void setFb2b2cWdealId(Long fb2b2cWdealId) {
		this.fb2b2cWdealId = fb2b2cWdealId;
	}

	public String getFb2b2cWdealCode() {
		return fb2b2cWdealCode;
	}

	public void setFb2b2cWdealCode(String fb2b2cWdealCode) {
		this.fb2b2cWdealCode = fb2b2cWdealCode;
	}

	public String getFdealDesc() {
		return fdealDesc;
	}

	public void setFdealDesc(String fdealDesc) {
		this.fdealDesc = fdealDesc;
	}

	public String getFshippingfeeCalc() {
		return fshippingfeeCalc;
	}

	public void setFshippingfeeCalc(String fshippingfeeCalc) {
		this.fshippingfeeCalc = fshippingfeeCalc;
	}

	public String getFreferer() {
		return freferer;
	}

	public void setFreferer(String freferer) {
		this.freferer = freferer;
	}

	public String getOrderDetailText() {
		return orderDetailText;
	}

	public void setOrderDetailText(String orderDetailText) {
		this.orderDetailText = orderDetailText;
	}

	public String getFvisitorIp() {
		return fvisitorIp;
	}

	public void setFvisitorIp(String fvisitorIp) {
		this.fvisitorIp = fvisitorIp;
	}

	public String getFrechargeAccount() {
		return frechargeAccount;
	}

	public void setFrechargeAccount(String frechargeAccount) {
		this.frechargeAccount = frechargeAccount;
	}

	public String getFsellerMobile() {
		return fsellerMobile;
	}

	public void setFsellerMobile(String fsellerMobile) {
		this.fsellerMobile = fsellerMobile;
	}

	public String getFsellerMail() {
		return FsellerMail;
	}

	public void setFsellerMail(String fsellerMail) {
		FsellerMail = fsellerMail;
	}

	public List<OrderProduct> getOrderProductList() {
		return orderProductList;
	}

	public void setOrderProductList(List<OrderProduct> orderProductList) {
		this.orderProductList = orderProductList;
	}

}
