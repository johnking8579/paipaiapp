package com.qq.qqbuy.virtualgoods.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Pagination<T> implements Serializable {

	private static final long serialVersionUID = 5370172249739508256L;
	private int pageSize;
	private int pageNo;
	private int totalItem;
	private int pageCount;
	private List<T> dataList;

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getTotalItem() {
		return totalItem;
	}

	public void setTotalItem(int totalItem) {
		this.totalItem = totalItem;
	}

	public List<T> getDataList() {
		return dataList;
	}

	public void setDataList(List<T> dataList) {
		this.dataList = dataList;
	}

	public int getPageCount() {
		if (totalItem == 0 || pageSize == 0) {
			return 0;
		}
		if (totalItem % pageSize == 0) {
			pageCount = totalItem / pageSize;
		} else {
			pageCount = totalItem / pageSize + 1;
		}
		return pageCount;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Pagination [dataList=").append(dataList == null ? "[]" : dataList).append(", pageNo=").append(pageNo).append(", pageSize=").append(pageSize).append(", totalItem=").append(totalItem).append("]");
		return builder.toString();
	}
	
	/**
	 * 返回空Pagination
	 * @param <T>
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public static <T> Pagination<T> emptyPagination(Integer pageNo, Integer pageSize) {
		Pagination<T> pagin = new Pagination<T>();
		pagin.setDataList(new ArrayList<T>());
		pagin.setTotalItem(0);
		pagin.setPageSize(pageSize);
		pagin.setPageNo(pageNo);
		return pagin;
	}
}
