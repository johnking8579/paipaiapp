package com.qq.qqbuy.virtualgoods.model;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;

public class PagVritual implements Serializable{
	private int pageSize;
	private int pageNo;
	private int totalItem;
	private int pageCount;
	private List<VritualOrder> virtualVc;
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getTotalItem() {
		return totalItem;
	}
	public void setTotalItem(int totalItem) {
		this.totalItem = totalItem;
	}
	public int getPageCount() {
		return pageCount;
	}
	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}
	public List<VritualOrder> getVirtualVc() {
		return virtualVc;
	}
	public void setVirtualVc(List<VritualOrder> virtualVc) {
		this.virtualVc = virtualVc;
	}
	
	
}
