package com.qq.qqbuy.virtualgoods.action;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.qq.qqbuy.common.JsonOutput;
import com.qq.qqbuy.common.action.AuthRequiredAction;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.virtualgoods.biz.VirtualBiz;
import com.qq.qqbuy.virtualgoods.model.Order;
import com.qq.qqbuy.virtualgoods.model.OrderProduct;
import com.qq.qqbuy.virtualgoods.model.PagVritual;
import com.qq.qqbuy.virtualgoods.model.VritualOrder;
import com.qq.qqbuy.virtualgoods.protocol.SubmitVirtualOrderInfo;
import com.qq.qqbuy.virtualgoods.protocol.SubmitVirtualOrderResp;

/**
 * 

 * @ClassName: VirtualAction 

 * @Description: 该类用于虚拟商品的相关对外接口
 
 * @author lhn

 * @date 2014-12-18 下午12:01:17 

 *
 */
public class VirtualAction  extends AuthRequiredAction {
	
	private long buyerUin;//买家ID
	private short dealType=5;//订单类型5位虚拟商品
	private short dealSource=2;//订单来源默认为2   1:网站2：App
	private short dealPayType;//支付方式1-财付通5-微信支付 
	private String rechargeAccount;//充值账号
	private String itemId;//商品ID
	private int isAutoSendItem;//是否是自动发货商品1-是 2-否，必填项
	private long dealItemCount;// 购买数量
	private long itemOriginalPrice;//面值
	private long skuId; //非必写
	private VirtualBiz virtualBiz;
	private String fdealType="" ;//空：查询所有订单（qq币，话费）  "5"：Q币   "6"：话费
	//输出信息
	JsonOutput out = new JsonOutput();
	
	
	//以下虚拟商品相关
	private String fdealId;
	private int pageNo =1 ;
	private int pageSize=10;
	private long fbuyerUin;
	private boolean isnochecke=false;
	/**
	 * 
	
	 * @Title: VirtualSubmitOrder 
	
	 * @Description: 提交虚拟商品的订单
	
	 * @param @return    设定文件 
	
	 * @return String    返回类型 
	
	 * @throws
	 */
	public String  submitVirtualOrder() {
		//先判断是否登录
		if(checkLogin()){
			//构造订单
			SubmitVirtualOrderInfo virtualorder = virtualBiz.getVirtualOrder(buyerUin,getCp(),dealType,dealSource,dealPayType,rechargeAccount,itemId,isAutoSendItem,itemOriginalPrice,skuId,dealItemCount);
			//将构造好的订单提交
			SubmitVirtualOrderResp resp = virtualBiz.submitVirtual(virtualorder);
			
			JsonParser jsonParser = new JsonParser();
			Gson gson = new Gson();
			String  repgson = gson.toJson(resp);
			JsonElement jsonElement = jsonParser.parse(repgson);
			out.setData(jsonElement);
			
		}else {
			out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL+"");
			out.setMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
		}
		return doPrint(out.toJsonStr());
	}
	
	
/*	*//**
	 * @throws BizOrderServiceException 
	 * 
	
	 * @Title: getOrderList 
	
	 * @Description: 获取买家的订单列表SAF接口由于网络问题不使用
	
	 * @param @return    设定文件 
	
	 * @return String    返回类型 
	
	 * @throws
	 *//*
	public  String getOrderList(){
		  ClassPathXmlApplicationContext appContext = new ClassPathXmlApplicationContext("conf-spring\\biz-context-virtualsaf.xml");   
	       BuyerOrderService buyerOrderService = (BuyerOrderService)appContext.getBean("buyerOrderService");
		 System.out.println("VirtualgetOrder:---开始");
	       OrderSearch orderSearch = new OrderSearch();
	       orderSearch.setFbuyerUin(888888);
	       System.out.println("OrderSearch   set end");
	       Pagination<Order> fenyeList = null;
	       List<Order> orderlist= new ArrayList<Order>();
	       Gson gson = new Gson();
		try {
			fenyeList = buyerOrderService. selectOrderList( orderSearch, 1,
					3);
			   System.out.println("fenyeList   set end");
			   orderlist= fenyeList.getDataList();
			   System.out.println("orderlist   get end");
			   System.out.println("orderlist json   get begin");
			   System.out.println("virtualgson:"+gson.toJson(orderlist));
		} catch (Exception e) {
			 System.out.println("BizOrderServiceException"+e.getMessage());
			e.printStackTrace();
		}
	    
		return doPrint(gson.toJson(orderlist));
	}*/
	/**
	 * 
	
	 * @Title: viewVirtualOrderDetail 
	
	 * @Description: 订单详情数据 
	
	 * @param @return    设定文件 
	
	 * @return String    返回类型 
	
	 * @throws
	 */
	public String viewVirtualOrderDetail() {
		if(checkLogin()||isnochecke){
			//查询该订单
			if(fdealId!=null&&!"".equals(fdealId)){
				try {
					Order order= this.virtualBiz.getVirtualOrder(fdealId);
					if(order!=null){
						String fsellerMobile = order.getFsellerMobile();
						if(StringUtil.isNotBlank(fsellerMobile) && fsellerMobile.trim().length() == 11){
							order.setFsellerMobile(StringUtil.substring(fsellerMobile, 0, 3) + "****" + StringUtil.substring(fsellerMobile, 7, 11));
						}
						
						String frechargeAccount = order.getFrechargeAccount();
						if(StringUtil.isNotBlank(frechargeAccount) && frechargeAccount.trim().length() == 11){
							order.setFrechargeAccount(StringUtil.substring(frechargeAccount, 0, 3) + "****" + StringUtil.substring(frechargeAccount, 7, 11));
						}
						
						String date= JsonUtils.writeValue(order);
						JsonParser jp = new JsonParser();
						out.setData(jp.parse(date));
					}else {
						out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP+"");
						out.setMsg("订单处理异常");
					}
				} catch (Exception e) {
					out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP+"");
					out.setMsg("订单处理异常");
				}
		
			}else {
				out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER+"");
				out.setMsg("订单号不能为空");
			}
		}else {
			out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL + "");
	    	out.setMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
		}
		return doPrint(out.toJsonStr());
	}
	
	/**
	 * 
	
	 * @Title: getVirtualOrderList 
	
	 * @Description: 获取买家列表
	
	 * @param @return
	 * @param @throws UnsupportedEncodingException    设定文件 
	
	 * @return String    返回类型 
	
	 * @throws
	 */
	public String getVirtualOrderList() throws UnsupportedEncodingException{
		if(checkLogin()||isnochecke){
				//如果需要核对登录状态则
				if(!isnochecke){
					fbuyerUin=getWid();
				}
				if(StringUtils.isEmpty(fdealType) || "5".equals(fdealType) || "6".equals(fdealType)) {
					PagVritual vritualOrder = this.virtualBiz.getVirtualOrderList(fbuyerUin,pageNo,pageSize,fdealType);
					String date= JsonUtils.writeValue(vritualOrder);
					JsonParser jp = new JsonParser();
					out.setData(jp.parse(date));	
				} else {
					out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER + "");
			    	out.setMsg("参数校验不合法");
				}				
		}else {
			out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL + "");
	    	out.setMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
		}
		return doPrint(out.toJsonStr());
	}
	/**
	 * 拆分接口返回订单数据
	 * @param resultList
	 * @return
	 * @throws Exception
	 */
	private static List<VritualOrder> splitResut(List<Map> resultList) {
		List<VritualOrder> list = new ArrayList<VritualOrder>();
		for (Map map : resultList) {
			VritualOrder virtualOrderTemp = new VritualOrder();
			// 订单编号
			virtualOrderTemp.setFdealId((Integer) map.get("fdealId"));
			// 下单时间
			virtualOrderTemp.setFdealCreateTime((String)map.get("fdealCreateTime"));
			String text = (String)map.get("orderDetailText");
			if(text!=null && !"".equals(text)){
				OrderProduct orderProduct = JsonUtils.readValue(text.substring(1, text.length()-1), OrderProduct.class);
				virtualOrderTemp.setFitemId(orderProduct.getFitemId());
				virtualOrderTemp.setFitemName(orderProduct.getFitemName());
				if(orderProduct.getFitemPrice()!=null){
				/*	BigDecimal itemPrice = new BigDecimal(orderProduct.getFitemPrice());
					virtualOrderTemp.setFitemPrice(itemPrice.divide(new BigDecimal("100")));*/
					virtualOrderTemp.setFitemPrice(orderProduct.getFitemPrice());
				}
				virtualOrderTemp.setFdealItemCount(orderProduct.getFdealItemCount());
				virtualOrderTemp.setFitemLogo(orderProduct.getFitemLogo());
			}
			Integer amount = (Integer)map.get("fdealPayFeeTotal");
			//virtualOrderTemp.setAmount(new BigDecimal(amount).divide(new BigDecimal("100")));
			virtualOrderTemp.setFdealPayFeeTotal(amount);
			virtualOrderTemp.setFbuyerUin((Integer)map.get("fbuyerUin"));
			virtualOrderTemp.setFsellerUin((Integer)map.get("fsellerUin"));
			virtualOrderTemp.setFdealState((Integer)map.get("fdealState"));
			// 支付方式 0未定义 1财付通 2货到付款 3分期付款 4移动积分
			virtualOrderTemp.setFdealPayType((Integer)map.get("fdealPayType"));
			list.add(virtualOrderTemp);
		}
		return list;
	}
	
	
	/**
	 * 
	
	 * @Title: updateOrderCancel 
	
	 * @Description: 取消订单
	
	 * @param @return    设定文件 
	
	 * @return String    返回类型 
	
	 * @throws
	 */
	public String updateOrderCancel(){
		if(checkLogin()){
			//查询该订单
			if(fdealId!=null&&!"".equals(fdealId)){
				try {
					//去消订单
					String result = this.virtualBiz.updateCancel(fdealId);
					if(result!=null&&!"0".equals(result)){
						Map<String, Object> map = JsonUtils.readValue(result);
						out.setErrCode((String)map.get("code"));
						out.setMsg((String)map.get("msg"));
						out.setData(new Gson().toJsonTree(result));
					}else {
						out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP+"");
						out.setMsg("订单处理异常");
					}
				} catch (Exception e) {
					out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP+"");
					out.setMsg("订单处理异常");
				}
		
			}else {
				out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER+"");
				out.setMsg("订单号不能为空");
			}
		}else {
			out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL + "");
	    	out.setMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
		}
		return doPrint(out.toJsonStr());
		
	}
	
	
	public long getBuyerUin() {
		return buyerUin;
	}

	public void setBuyerUin(long buyerUin) {
		this.buyerUin = buyerUin;
	}

	public short getDealType() {
		return dealType;
	}

	public void setDealType(short dealType) {
		this.dealType = dealType;
	}

	public short getDealSource() {
		return dealSource;
	}

	public void setDealSource(short dealSource) {
		this.dealSource = dealSource;
	}

	public short getDealPayType() {
		return dealPayType;
	}

	public void setDealPayType(short dealPayType) {
		this.dealPayType = dealPayType;
	}

	public String getRechargeAccount() {
		return rechargeAccount;
	}

	public void setRechargeAccount(String rechargeAccount) {
		this.rechargeAccount = rechargeAccount;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public int getIsAutoSendItem() {
		return isAutoSendItem;
	}

	public void setIsAutoSendItem(int isAutoSendItem) {
		this.isAutoSendItem = isAutoSendItem;
	}

	public long getDealItemCount() {
		return dealItemCount;
	}

	public void setDealItemCount(long dealItemCount) {
		this.dealItemCount = dealItemCount;
	}

	public long getItemOriginalPrice() {
		return itemOriginalPrice;
	}

	public void setItemOriginalPrice(long itemOriginalPrice) {
		this.itemOriginalPrice = itemOriginalPrice;
	}

	public long getSkuId() {
		return skuId;
	}

	public void setSkuId(long skuId) {
		this.skuId = skuId;
	}

	public JsonOutput getOut() {
		return out;
	}

	public void setOut(JsonOutput out) {
		this.out = out;
	}

	public VirtualBiz getVirtualBiz() {
		return virtualBiz;
	}

	public void setVirtualBiz(VirtualBiz virtualBiz) {
		this.virtualBiz = virtualBiz;
	}


	public String getFdealId() {
		return fdealId;
	}


	public void setFdealId(String fdealId) {
		this.fdealId = fdealId;
	}


	public int getPageNo() {
		return pageNo;
	}


	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}


	public int getPageSize() {
		return pageSize;
	}


	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}


	public long getFbuyerUin() {
		return fbuyerUin;
	}


	public void setFbuyerUin(long fbuyerUin) {
		this.fbuyerUin = fbuyerUin;
	}


	public boolean isIsnochecke() {
		return isnochecke;
	}


	public void setIsnochecke(boolean isnochecke) {
		this.isnochecke = isnochecke;
	}


	public String getFdealType() {
		return fdealType;
	}


	public void setFdealType(String fdealType) {
		this.fdealType = fdealType;
	}




}
