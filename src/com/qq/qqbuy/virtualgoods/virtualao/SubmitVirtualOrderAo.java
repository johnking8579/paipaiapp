package com.qq.qqbuy.virtualgoods.virtualao;

import com.paipai.lang.uint16_t;
import com.paipai.lang.uint32_t;
import com.paipai.lang.uint64_t;
import com.paipai.lang.uint8_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;


/**
 * 虚拟定的那Ao。
 * @author 于建明
 *
 */
@HeadApiProtocol(cPlusNamespace = "c2cent::ao::virtualorder", needInit = true)
public class SubmitVirtualOrderAo {
	@Member(desc = "订单po", cPlusNamespace = "c2cent::po::virtualorder", isNeedUFlag = true)
	public class SubmitVirtualOrderInfo {
		
		@Field(desc = "订单po版本", defaultValue = "20141213")
		uint8_t version;

		@Field(desc = "买家编号，必填项")
		uint32_t buyerUin;

		@Field(desc = "订单类型：5-虚拟直充订单，必填项")
		uint8_t dealType;
		
		@Field(desc = "订单来源	1：网站，2：APP")
		uint8_t dealSource;
		
		@Field(desc = "支付方式：1-财付通5-微信支付，必填项")
		uint8_t dealPayType;
		
		@Field(desc = "访问者ip，必填项")
		String ip;

		@Field(desc = "充值账户，必填项")
		String rechargeAccount;
		
		@Field(desc = "费用合计，非必填")
		uint32_t dealPayFeeTotal;

		@Field(desc = "订单中的商品，目前只支持一单一品，必填项")
		SubmitVirtualProductInfo submitVirtualProductInfo;
		
		
		uint8_t version_u;

		uint8_t buyerUin_u;

		uint8_t dealType_u;
		
		uint8_t dealSource_u;
		
		uint8_t dealPayType_u;
		
		uint8_t ip_u;

		uint8_t rechargeAccount_u;
		
		uint8_t dealPayFeeTotal_u;

		uint8_t submitVirtualProductInfo_u;
		
	}
	
	
	@Member(desc = "虚拟商品po", cPlusNamespace = "c2cent::po::virtualorder", isNeedUFlag = true)
	public class SubmitVirtualProductInfo {
		
		@Field(desc = "订单商品po版本", defaultValue = "20141213")
		uint8_t version;

		@Field(desc = "商品编号，必填项")
		String itemId;

		@Field(desc = "是否自动发货商品 ：1-是 2-否，必填项")
		uint16_t isAutoSendItem;
		
		@Field(desc = "购买数量，必填项")
		uint32_t dealItemCount;
		
		@Field(desc = "面值，非必填")
		uint32_t itemOriginalPrice;

		@Field(desc = "商品skuId，非必填")
		uint64_t skuId;

		uint8_t version_u;

		uint8_t itemId_u;

		uint8_t isAutoSendItem_u;
		
		uint8_t dealItemCount_u;
		
		uint8_t itemOriginalPrice_u;

		uint8_t skuId_u;
		
	}
	
	@ApiProtocol(cmdid = "0x55221801L", desc = "提交虚拟直充订单接口")
	class SubmitVirtualOrder {
		@ApiProtocol(cmdid = "0x55221801L", desc = "提交虚拟订单请求类")
		class Req {
			@Field(desc = "要添加的订单PO")
			SubmitVirtualOrderInfo submitVirtualOrderInfo;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0x55228801L", desc = "提交虚拟订单返回类")
		class Resp {
			@Field(desc = "调用状态,0表示成功;1表示服务异常;2表示参数错误;")
			uint8_t state;
			
			@Field(desc = "错误信息")
			String errMsg;
			
			@Field(desc = "订单号")
			uint64_t orderId;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

}
