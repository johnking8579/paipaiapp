package com.qq.qqbuy.virtualgoods.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *提交虚拟订单返回类
 *
 *@date 2014-12-17 04:53:39
 *
 *@since version:0
*/
public class  SubmitVirtualOrderResp extends NetMessage
{
	/**
	 * 调用状态,0表示成功;1表示服务异常;2表示参数错误;
	 *
	 * 版本 >= 0
	 */
	 private short state;

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 订单号
	 *
	 * 版本 >= 0
	 */
	 private long orderId;

	/**
	 * 输出保留字
	 *
	 * 版本 >= 0
	 */
	 private String outReserve = new String();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushUByte(state);
		bs.pushString(errMsg);
		bs.pushLong(orderId);
		bs.pushString(outReserve);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		state = bs.popUByte();
		errMsg = bs.popString();
		orderId = bs.popLong();
		outReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x55228801L;
	}


	/**
	 * 获取调用状态,0表示成功;1表示服务异常;2表示参数错误;
	 * 
	 * 此字段的版本 >= 0
	 * @return state value 类型为:short
	 * 
	 */
	public short getState()
	{
		return state;
	}


	/**
	 * 设置调用状态,0表示成功;1表示服务异常;2表示参数错误;
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setState(short value)
	{
		this.state = value;
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	/**
	 * 获取订单号
	 * 
	 * 此字段的版本 >= 0
	 * @return orderId value 类型为:long
	 * 
	 */
	public long getOrderId()
	{
		return orderId;
	}


	/**
	 * 设置订单号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOrderId(long value)
	{
		this.orderId = value;
	}


	/**
	 * 获取输出保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return outReserve;
	}


	/**
	 * 设置输出保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		this.outReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SubmitVirtualOrderResp)
				length += 1;  //计算字段state的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += 17;  //计算字段orderId的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(outReserve);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
