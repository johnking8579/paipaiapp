package com.qq.qqbuy.virtualgoods.protocol;


import com.paipai.util.annotation.Protocol;
import com.paipai.netframework.kernal.NetAction;


import com.paipai.lang.GenericWrapper;/**
 *
 *NetAction类
 *
 */
public class SubmitVirtualOrderAo  extends NetAction
{
	@Override
	 public int getSnowslideThresold(){
		// 这里设置防雪崩时间，也就是超时时间值
		  return 2;
	 }




	@Protocol(cmdid = 0x55221801L, desc = "提交虚拟直充订单接口", export = true)
	 public SubmitVirtualOrderResp SubmitVirtualOrder(SubmitVirtualOrderReq req){
		SubmitVirtualOrderResp resp = new  SubmitVirtualOrderResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }



}
