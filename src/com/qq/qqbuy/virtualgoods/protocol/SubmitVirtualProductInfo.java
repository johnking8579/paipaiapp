package com.qq.qqbuy.virtualgoods.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *虚拟商品po
 *
 *@date 2014-12-17 04:53:39
 *
 *@since version:0
*/
public class SubmitVirtualProductInfo  implements ICanSerializeObject
{
	/**
	 * 订单商品po版本
	 *
	 * 版本 >= 0
	 */
	 private short version = 0;

	/**
	 * 商品编号，必填项
	 *
	 * 版本 >= 0
	 */
	 private String itemId = new String();

	/**
	 * 是否自动发货商品 ：1-是 2-否，必填项
	 *
	 * 版本 >= 0
	 */
	 private int isAutoSendItem;

	/**
	 * 购买数量，必填项
	 *
	 * 版本 >= 0
	 */
	 private long dealItemCount;

	/**
	 * 面值，非必填
	 *
	 * 版本 >= 0
	 */
	 private long itemOriginalPrice;

	/**
	 * 商品skuId，非必填
	 *
	 * 版本 >= 0
	 */
	 private long skuId;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 版本 >= 0
	 */
	 private short itemId_u;

	/**
	 * 版本 >= 0
	 */
	 private short isAutoSendItem_u;

	/**
	 * 版本 >= 0
	 */
	 private short dealItemCount_u;

	/**
	 * 版本 >= 0
	 */
	 private short itemOriginalPrice_u;

	/**
	 * 版本 >= 0
	 */
	 private short skuId_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUByte(version);
		bs.pushString(itemId);
		bs.pushUShort(isAutoSendItem);
		bs.pushUInt(dealItemCount);
		bs.pushUInt(itemOriginalPrice);
		bs.pushLong(skuId);
		bs.pushUByte(version_u);
		bs.pushUByte(itemId_u);
		bs.pushUByte(isAutoSendItem_u);
		bs.pushUByte(dealItemCount_u);
		bs.pushUByte(itemOriginalPrice_u);
		bs.pushUByte(skuId_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUByte();
		itemId = bs.popString();
		isAutoSendItem = bs.popUShort();
		dealItemCount = bs.popUInt();
		itemOriginalPrice = bs.popUInt();
		skuId = bs.popLong();
		version_u = bs.popUByte();
		itemId_u = bs.popUByte();
		isAutoSendItem_u = bs.popUByte();
		dealItemCount_u = bs.popUByte();
		itemOriginalPrice_u = bs.popUByte();
		skuId_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取订单商品po版本
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:short
	 * 
	 */
	public short getVersion()
	{
		return version;
	}


	/**
	 * 设置订单商品po版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion(short value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取商品编号，必填项
	 * 
	 * 此字段的版本 >= 0
	 * @return itemId value 类型为:String
	 * 
	 */
	public String getItemId()
	{
		return itemId;
	}


	/**
	 * 设置商品编号，必填项
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemId(String value)
	{
		this.itemId = value;
		this.itemId_u = 1;
	}


	/**
	 * 获取是否自动发货商品 ：1-是 2-否，必填项
	 * 
	 * 此字段的版本 >= 0
	 * @return isAutoSendItem value 类型为:int
	 * 
	 */
	public int getIsAutoSendItem()
	{
		return isAutoSendItem;
	}


	/**
	 * 设置是否自动发货商品 ：1-是 2-否，必填项
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setIsAutoSendItem(int value)
	{
		this.isAutoSendItem = value;
		this.isAutoSendItem_u = 1;
	}


	/**
	 * 获取购买数量，必填项
	 * 
	 * 此字段的版本 >= 0
	 * @return dealItemCount value 类型为:long
	 * 
	 */
	public long getDealItemCount()
	{
		return dealItemCount;
	}


	/**
	 * 设置购买数量，必填项
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealItemCount(long value)
	{
		this.dealItemCount = value;
		this.dealItemCount_u = 1;
	}


	/**
	 * 获取面值，非必填
	 * 
	 * 此字段的版本 >= 0
	 * @return itemOriginalPrice value 类型为:long
	 * 
	 */
	public long getItemOriginalPrice()
	{
		return itemOriginalPrice;
	}


	/**
	 * 设置面值，非必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setItemOriginalPrice(long value)
	{
		this.itemOriginalPrice = value;
		this.itemOriginalPrice_u = 1;
	}


	/**
	 * 获取商品skuId，非必填
	 * 
	 * 此字段的版本 >= 0
	 * @return skuId value 类型为:long
	 * 
	 */
	public long getSkuId()
	{
		return skuId;
	}


	/**
	 * 设置商品skuId，非必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSkuId(long value)
	{
		this.skuId = value;
		this.skuId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return itemId_u value 类型为:short
	 * 
	 */
	public short getItemId_u()
	{
		return itemId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemId_u(short value)
	{
		this.itemId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return isAutoSendItem_u value 类型为:short
	 * 
	 */
	public short getIsAutoSendItem_u()
	{
		return isAutoSendItem_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIsAutoSendItem_u(short value)
	{
		this.isAutoSendItem_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dealItemCount_u value 类型为:short
	 * 
	 */
	public short getDealItemCount_u()
	{
		return dealItemCount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealItemCount_u(short value)
	{
		this.dealItemCount_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return itemOriginalPrice_u value 类型为:short
	 * 
	 */
	public short getItemOriginalPrice_u()
	{
		return itemOriginalPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemOriginalPrice_u(short value)
	{
		this.itemOriginalPrice_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return skuId_u value 类型为:short
	 * 
	 */
	public short getSkuId_u()
	{
		return skuId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSkuId_u(short value)
	{
		this.skuId_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SubmitVirtualProductInfo)
				length += 1;  //计算字段version的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(itemId);  //计算字段itemId的长度 size_of(String)
				length += 2;  //计算字段isAutoSendItem的长度 size_of(uint16_t)
				length += 4;  //计算字段dealItemCount的长度 size_of(uint32_t)
				length += 4;  //计算字段itemOriginalPrice的长度 size_of(uint32_t)
				length += 17;  //计算字段skuId的长度 size_of(uint64_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段itemId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段isAutoSendItem_u的长度 size_of(uint8_t)
				length += 1;  //计算字段dealItemCount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段itemOriginalPrice_u的长度 size_of(uint8_t)
				length += 1;  //计算字段skuId_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
