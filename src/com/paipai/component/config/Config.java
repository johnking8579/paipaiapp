package com.paipai.component.config;

import java.util.HashMap;
import java.util.ResourceBundle;
/**
 * 反编译自com.paipai.component.jar, 在工程中加入该类后,根据tomcat的类加载顺序,该类会比com.paipai.component.jar中的Config.class优先加载.
 * 如果要使用系统变量+服务器上的so,直接把com.paipai.component.*.java删除即可
 * 
 * 原文件逻辑:如果是windows环境,则会强行终止
 * 修改后: 去掉以上代码
 */
public class Config {
	public static boolean isWindow = System.getProperty("os.name").toLowerCase().indexOf("window") > -1;
	
	private static HashMap<String, Config> configs = new HashMap();

	private ResourceBundle res = null;

	public static Config con = getInstance("configcenter");

	public static Config getInstance(String resName) {
		Config ret = null;

		if (configs.containsKey(resName))
			ret = (Config) configs.get(resName);
		else {
			synchronized (configs) {
				if (configs.containsKey(resName)) {
					ret = (Config) configs.get(resName);
				} else {
					ret = new Config(resName);
					configs.put(resName, ret);
				}
			}
		}

		return ret;
	}

	private Config(String resName) {
	}

	public String getString(String key, String defaultValue) {
		try {
			String v = this.res.getString(key);

			if ((v == null) || (v.trim().length() == 0)) {
				return defaultValue;
			}
			return new String(v.trim().getBytes("iso-8859-1"), "gbk").trim();
		} catch (Exception e) {
		}
		return defaultValue;
	}

	public String getString(String key) {
		return getString(key, "");
	}

	public String getStringx(String key) {
		String ret = "";
		try {
			ret = this.res.getString(key);
			if ((ret == null) || (ret.trim().length() == 0))
				ret = "";
		} catch (Exception e) {
		}

		return ret;
	}
}
