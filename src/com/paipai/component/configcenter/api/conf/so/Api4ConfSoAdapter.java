 package com.paipai.component.configcenter.api.conf.so;
 
 import com.paipai.component.configcenter.api.conf.res.ConfFile;
import com.paipai.component.configcenter.api.conf.res.ConfItem;
import com.paipai.component.configcenter.api.conf.res.DbConf;
import com.paipai.component.configcenter.api.conf.res.MsgQ;
import com.paipai.component.configcenter.api.conf.res.Shm;
import com.paipai.component.configcenter.api.conf.res.Socket;
import com.paipai.component.configcenter.api.conf.res.UnixSocket;
 
 public class Api4ConfSoAdapter
 {
   private static boolean ready = false;
 
   public static native long open(String paramString);
 
   public static native void close();
 
   public static native String getErrMsg();
 
   public static native long getShmCount();
 
   public static native long getShmByName(String paramString, Shm paramShm);
 
   public static native long getShmByIndex(long paramLong, Shm paramShm);
 
   public static native long getMsgQCount();
 
   public static native long getMsgQByName(String paramString, MsgQ paramMsgQ);
 
   public static native long getMsgQByIndex(long paramLong, MsgQ paramMsgQ);
 
   public static native long getSocketCount();
 
   public static native long getSocketByName(String paramString, Socket paramSocket);
 
   public static native long getSocketByIndex(long paramLong, Socket paramSocket);
 
   public static native long getUnixSocketCount();
 
   public static native long getUnixSocketByName(String paramString, UnixSocket paramUnixSocket);
 
   public static native long getUnixSocketByIndex(long paramLong, UnixSocket paramUnixSocket);
 
   public static native long getDbConfCount();
 
   public static native long getDbConfByName(String paramString, DbConf paramDbConf);
 
   public static native long getDbConfByIndex(long paramLong, DbConf paramDbConf);
 
   public static native long getConfFileCount();
 
   public static native long getConfFileByName(String paramString, ConfFile paramConfFile);
 
   public static native long getConfFileByIndex(long paramLong, ConfFile paramConfFile);
 
   public static native long getConfItemByName(String paramString, ConfItem paramConfItem);
 
   public static boolean isReady() { return ready; }
 
 
   static	{
	if (System.getProperty("os.name").toLowerCase().indexOf("window") < 0)	{
		String sopath = "so/confg4conf_api_x86_64-suse-linux.so";
		System.load(Thread.currentThread().getContextClassLoader().getResource(sopath).getPath());
		ready = true;
	}
}
 }

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.component.configcenter.api.conf.so.Api4ConfSoAdapter
 * JD-Core Version:    0.6.2
 */