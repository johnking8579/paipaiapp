package com.paipai.component.configcenter.api.app.so;


public class Api4AppSoAdapter {
	public static Object lock = new Object();

	private static boolean ready = false;

	public static synchronized native boolean init();

	public static synchronized native String getServiceAddress(
			String paramString, long paramLong);

	public static synchronized native int getServiceAddressCount(
			String paramString);

	public static synchronized native String getCmdAddress(long paramLong1,
			long paramLong2);

	public static synchronized native int getCmdAddressCount(long paramLong);

	public static boolean isReady() {
		return ready;
	}

	static {
		if (System.getProperty("os.name").toLowerCase().indexOf("window") < 0)	{
			String sopath = "so/confg4app_api_x86_64-suse-linux.so";
			System.load(Thread.currentThread().getContextClassLoader().getResource(sopath).getPath());
			ready = init();
		}
	}
}
