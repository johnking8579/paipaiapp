//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.deal.ShippingFee.java

package com.paipai.deal.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *运费模板
 *
 *@date 2012-12-18 01:05:41
 *
 *@since version:0
*/
public class TemplateSimpleInfo  implements ICanSerializeObject
{
	/**
	 * 版本 >= 0
	 */
	 private long dwId;

	/**
	 * 版本 >= 0
	 */
	 private long dwUin;

	/**
	 * 版本 >= 0
	 */
	 private short cInnerId;

	/**
	 * 版本 >= 0
	 */
	 private long dwProperty;

	/**
	 * 版本 >= 0
	 */
	 private String sName = "";

	/**
	 * 版本 >= 0
	 */
	 private String sDesc = "";

	/**
	 * 版本 >= 0
	 */
	 private long dwCreateTime;

	/**
	 * 版本 >= 0
	 */
	 private long dwLastModifyTime;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(dwId);
		bs.pushUInt(dwUin);
		bs.pushUByte(cInnerId);
		bs.pushUInt(dwProperty);
		bs.pushString(sName);
		bs.pushString(sDesc);
		bs.pushUInt(dwCreateTime);
		bs.pushUInt(dwLastModifyTime);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		dwId = bs.popUInt();
		dwUin = bs.popUInt();
		cInnerId = bs.popUByte();
		dwProperty = bs.popUInt();
		sName = bs.popString();
		sDesc = bs.popString();
		dwCreateTime = bs.popUInt();
		dwLastModifyTime = bs.popUInt();
		return bs.getReadLength();
	} 


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwId value 类型为:long
	 * 
	 */
	public long getDwId()
	{
		return dwId;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwId(long value)
	{
		this.dwId = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwUin value 类型为:long
	 * 
	 */
	public long getDwUin()
	{
		return dwUin;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwUin(long value)
	{
		this.dwUin = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cInnerId value 类型为:short
	 * 
	 */
	public short getCInnerId()
	{
		return cInnerId;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCInnerId(short value)
	{
		this.cInnerId = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwProperty value 类型为:long
	 * 
	 */
	public long getDwProperty()
	{
		return dwProperty;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwProperty(long value)
	{
		this.dwProperty = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sName value 类型为:String
	 * 
	 */
	public String getSName()
	{
		return sName;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSName(String value)
	{
		this.sName = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sDesc value 类型为:String
	 * 
	 */
	public String getSDesc()
	{
		return sDesc;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSDesc(String value)
	{
		this.sDesc = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwCreateTime value 类型为:long
	 * 
	 */
	public long getDwCreateTime()
	{
		return dwCreateTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwCreateTime(long value)
	{
		this.dwCreateTime = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwLastModifyTime value 类型为:long
	 * 
	 */
	public long getDwLastModifyTime()
	{
		return dwLastModifyTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwLastModifyTime(long value)
	{
		this.dwLastModifyTime = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(TemplateSimpleInfo)
				length += 4;  //计算字段dwId的长度 size_of(uint32_t)
				length += 4;  //计算字段dwUin的长度 size_of(uint32_t)
				length += 1;  //计算字段cInnerId的长度 size_of(uint8_t)
				length += 4;  //计算字段dwProperty的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sName);  //计算字段sName的长度 size_of(String)
				length += ByteStream.getObjectSize(sDesc);  //计算字段sDesc的长度 size_of(String)
				length += 4;  //计算字段dwCreateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段dwLastModifyTime的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this,
                ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
