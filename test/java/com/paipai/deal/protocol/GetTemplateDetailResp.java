 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.deal.ShippingFee.java

package com.paipai.deal.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *
 *
 *@date 2012-12-18 02:25:25
 *
 *@since version:1
*/
public class  GetTemplateDetailResp implements IServiceObject
{
	public long result;
	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private TemplateDetailInfo detail = new TemplateDetailInfo();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(detail);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		detail = (TemplateDetailInfo) bs.popObject(TemplateDetailInfo.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x22108803L;
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return detail value 类型为:TemplateDetailInfo
	 * 
	 */
	public TemplateDetailInfo getDetail()
	{
		return detail;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:TemplateDetailInfo
	 * 
	 */
	public void setDetail(TemplateDetailInfo value)
	{
		if (value != null) {
				this.detail = value;
		}else{
				this.detail = new TemplateDetailInfo();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetTemplateDetailResp)
				length += ByteStream.getObjectSize(detail);  //计算字段detail的长度 size_of(TemplateDetailInfo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
