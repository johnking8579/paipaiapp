//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.deal.ShippingFee.java

package com.paipai.deal.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import java.util.Vector;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 *运费模板
 *
 *@date 2012-12-18 01:05:41
 *
 *@since version:0
*/
public class TemplateDetailInfo  implements ICanSerializeObject
{
	/**
	 * 运费信息
	 *
	 * 版本 >= 0
	 */
	 private TemplateSimpleInfo oSimple = new TemplateSimpleInfo();

	/**
	 * 规则信息
	 *
	 * 版本 >= 0
	 */
	 private Vector<Rule> rule_vector = new Vector<Rule>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(oSimple);
		bs.pushObject(rule_vector);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		oSimple = (TemplateSimpleInfo) bs.popObject(TemplateSimpleInfo.class);
		rule_vector = (Vector<Rule>)bs.popVector(Rule.class);
		return bs.getReadLength();
	} 


	/**
	 * 获取运费信息
	 * 
	 * 此字段的版本 >= 0
	 * @return oSimple value 类型为:TemplateSimpleInfo
	 * 
	 */
	public TemplateSimpleInfo getOSimple()
	{
		return oSimple;
	}


	/**
	 * 设置运费信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:TemplateSimpleInfo
	 * 
	 */
	public void setOSimple(TemplateSimpleInfo value)
	{
		if (value != null) {
				this.oSimple = value;
		}else{
				this.oSimple = new TemplateSimpleInfo();
		}
	}


	/**
	 * 获取规则信息
	 * 
	 * 此字段的版本 >= 0
	 * @return rule_vector value 类型为:Vector<Rule>
	 * 
	 */
	public Vector<Rule> getRule_vector()
	{
		return rule_vector;
	}


	/**
	 * 设置规则信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<Rule>
	 * 
	 */
	public void setRule_vector(Vector<Rule> value)
	{
		if (value != null) {
				this.rule_vector = value;
		}else{
				this.rule_vector = new Vector<Rule>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(TemplateDetailInfo)
				length += ByteStream.getObjectSize(oSimple);  //计算字段oSimple的长度 size_of(TemplateSimpleInfo)
				length += ByteStream.getObjectSize(rule_vector);  //计算字段rule_vector的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
