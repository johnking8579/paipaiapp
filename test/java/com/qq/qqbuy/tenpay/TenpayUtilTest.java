package com.qq.qqbuy.tenpay;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Test;

import com.qq.qqbuy.common.util.MD5Coding;
import com.qq.qqbuy.common.util.StringUtil;

public class TenpayUtilTest {
	
	@Test
	public void test1()	{
			//https://www.tenpay.com/template/paipai/auth_once.shtml?uin=2097792742&trans_num=1&trans_id_0=1000000101201411031337000610&draw_id_0=1131000000101141120319630123&deal_id_0=795019790-20141103-1319630123&buyer_fee_0=0&seller_fee_0=1&channel=1&remark=&return_url=http://pay.paipai.com/cgi-bin/trade_recv_confirm/return&timestamp=1416470628&sign=f53cfb3077031f6416070dc0068d16b7&skey=@7oYodwTe9&magic_key=&tag=0.5082797049544752
			Map m = new HashMap();
			m.put("uin", "525218150");
			m.put("trans_num", "1");
			m.put("trans_id_0", "1000000101201412101358476055");
			m.put("draw_id_0", "1131000000101141210331922995");
			m.put("deal_id_0", "795019790-20141210-1331922995");
			m.put("buyer_fee_0", "0");
			m.put("seller_fee_0", "1");
			m.put("channel", "1");
			m.put("remark", "");
			m.put("return_url", "http://pay.paipai.com/cgi-bin/trade_recv_confirm/return");
			m.put("timestamp", "1418211559");
			m.put("lskey", "@2m6iEiSV9");
			m.put("magic_key", "");
			m.put("tag", "0.8757159721571952");
			System.out.println(TenpayUtil.确认收货页面_手机用 + TenpayUtil.signParam(m));
	}
	
	/**
	 * https://www.tenpay.com/app/mpay/mobile_auth.cgi?uin=88908775&trans_num=1&
	 * trans_id_0=1000000101201409091314450811&draw_id_0=1131000000101140916306156043&
	 * deal_id_0=1275000330-20140909-1306156043&buyer_fee_0=0&seller_fee_0=1&channel=1&
	 * remark=&return_url=http://pay.paipai.com/cgi-bin/trade_recv_confirm/return&
	 * timestamp=1410833093&sign=073c4fc3e9d7b090c6361693b68d67dd&lskey=@8knliMAI6&magic_key=&tag=
	 */
	@Test
	public void test2()	{
		Map m = new HashMap();
		m.put("uin", "88908775");
		m.put("trans_num", "1");
		m.put("trans_id_0", "1000000101201409091314450811");
		m.put("draw_id_0", "1131000000101140916306156043");
		m.put("deal_id_0", "1275000330-20140909-1306156043");
		m.put("buyer_fee_0", "0");
		m.put("seller_fee_0", "1");
		m.put("channel", "1");
		m.put("remark", "");
		m.put("return_url", "http://pay.paipai.com/cgi-bin/trade_recv_confirm/return");
		m.put("timestamp", "1410833093");
		m.put("lskey", "@8knliMAI6");
		m.put("magic_key", "");
		m.put("tag", "");
		System.out.println("073c4fc3e9d7b090c6361693b68d67dd");
//		System.out.println(new PayBizImpl().getCfmRcvToken(uin, tenpayCode, dealCode, minSubDealCode, sellerFee, callbackUrl, lskey));
	}
	@Test
	public void test3()	{
		Map<String, String> params = new LinkedHashMap<String, String>();
		params.put("uin", "88908775");
		params.put("trans_num", "1");
		params.put("trans_id_0", "1000000101201409091314450811");
		params.put("draw_id_0", "1131000000101140916306156043");
		params.put("deal_id_0", "1275000330-20140909-1306156043");
		params.put("buyer_fee_0", "0");
		params.put("seller_fee_0", "1");
		params.put("channel", "1");
		params.put("remark", "");
		params.put("return_url", "http://pay.paipai.com/cgi-bin/trade_recv_confirm/return");
		params.put("timestamp", "1410833093");
		String sign = map2StringV2(params);
		sign += "tencent*20090925@cft_paipai";
		sign = MD5Coding.encode2HexStr(sign.getBytes());
		params.put("sign", sign);
		params.put("lskey", "@8knliMAI6");
		params.put("magic_key", "");
		params.put("tag", "");
		
		System.out.println(sign);
	}
	
	/**
	 * https://www.tenpay.com/template/paipai/auth_once.shtml?
	 * uin=525218150&trans_num=1&trans_id_0=1000000101201412101358592369&draw_id_0=1131000000101141210331875613&deal_id_0=795019790-20141210-1331875613&
	 * buyer_fee_0=0&seller_fee_0=1&channel=1&remark=&return_url=http://pay.paipai.com/cgi-bin/trade_recv_confirm/return&timestamp=1418207974&sign=53885925302343c4c66ed2d90851d55c&skey=@TrzL0m2rW&magic_key=&tag=0.5027525944169611
	 * 
	 * https://www.tenpay.com/app/mpay/mobile_auth.cgi?
	 * uin=525218150&trans_num=1&trans_id_0=1000000101201412101358592369&draw_id_0=1131000000101141210331875613&deal_id_0=795019790-20141210-1331875613&
	 * buyer_fee_0=&seller_fee_0=&channel=1&remark=&return_url=http://www.baidu.com&timestamp=1418208413&sign=9bc58f9c8f4fbab8893e2bb365d9b205&magic_key=&tag=
	 * 
	 * token=jMuQ6mrJOcW8KxfkuA2rBdnbVwyV3dOQohXRLia-HWsu-BC22xJgmWI9WChwu3bBOVb-VwCi3b_2zc3rMapATp-A8gCu0Gm8Wm3wM_E31snQ-a_vJNW5DmgRdunqu2lqtneoZb06tZtza6lajgs-FKLc3mxa3crqnfC0AlSMnl-v1rPXLZU9B7jGVxJ3bWxHFejLDMXwxD-72Hb43k7zaSgP6INGm5EC5J6rnz19ZTtZ2lweWAbOBr5-dHWW7y_Y5VJFii7rIr-sXVEQJEPn6rEaZTsZ5jDyBtmohhPIYNe_pRlE12GFcAOZofdfcQVsdVILbCX9W344xEOy6BmIf36sEVxD4grrTwy_3AM9la1ZEHpuJP_AnAeObYj_IQGWbwKw-wQkJjzYm1ksVOurl8pu5CDK2gP--ABxpidahTq8Gc9u-TyYsw==
	 */
	@Test
	public void test4()	{
		Map<String, String> params = new LinkedHashMap<String, String>();
		params.put("uin", "525218150");
		params.put("trans_num", "1");
		params.put("trans_id_0", "1000000101201412101358760569");
		params.put("draw_id_0", "1131000000101141210332032324");
		params.put("deal_id_0", "795019790-20141210-1332032324");
		params.put("buyer_fee_0", "0");
		params.put("seller_fee_0", "3");
		params.put("channel", "1");
		params.put("remark", "");
		params.put("return_url", "http://www.baidu.com");
		params.put("timestamp", "1418212096");
		String sign = map2StringV2(params);
		sign += "tencent*20090925@cft_paipai";
		sign = MD5Coding.encode2HexStr(sign.getBytes());
		params.put("sign", sign);
		params.put("lskey", "@h755iDZn4");
		params.put("magic_key", "");
		params.put("tag", "");
		
		String urlParams = map2StringV2(params);
		System.out.println("https://www.tenpay.com/app/mpay/mobile_auth.cgi?" + urlParams);
	}
	
	@Test
	public void genMyCftUrl()	{
		String s = TenpayUtil.genConfirmRecvUrl(525218150, "1000000101201412101358592369", 
				"795019790-20141210-1331875613", new long[]{1331875613}, 1, "http://www.baidu.com", null);
		System.out.println(s);
	}
	
	
	@Test
	public void decryptToken()	{
//		String token = "jMuQ6mrJOcW8KxfkuA2rBdnbVwyV3dOQohXRLia-HWsu-BC22xJgmWI9WChwu3bBOVb-VwCi3b_2zc3rMapATp-A8gCu0Gm8Wm3wM_E31snQ-a_vJNW5DmgRdunqu2lqtneoZb06tZtza6lajgs-FKLc3mxa3crqnfC0AlSMnl-v1rPXLZU9B7jGVxJ3bWxHFejLDMXwxD-72Hb43k7zaSgP6INGm5EC5J6rnz19ZTtZ2lweWAbOBr5-dHWW7y_Y5VJFii7rIr-sXVEQJEPn6rEaZTsZ5jDyBtmohhPIYNe_pRlE12GFcAOZofdfcQVsdVILbCX9W344xEOy6BmIf36sEVxD4grrTwy_3AM9la1ZEHpuJP_AnAeObYj_IQGWbwKw-wQkJjzYm1ksVOurl8pu5CDK2gP--ABxpidahTq8Gc9u-TyYsw==";
//		String token = "fehbGClwoTaTtPOCDbLOAyJDtWh7U4a-HFIbWwaW8A34NW__eA1Lx33SKku6gziTn7vfIofYp1hqNikIZ2Bg7rsePssybdhayQwwiukJbx3Nr6FT4H_gbgDV8zrWo-9HFUqxGw1Ksvx4JESWp_YnBsGCmfpwXFO6mwqMCi6SAdIy5PxoXToeY1eb8g46mEfGwRRyVkDwYJBPWueGsachP1WBlT-0Bf6A8QS1U8UiA9LxVa29i0fIxsS6yI9YBDw15qg34BAtbHrKbyoFQVltgw78JYWISNk2cVM_v2rs-bpsUrvmkBc_OZg4ODIe026sMnuO1VRNVZAyzAEG98ySgaxLv-68s7vrVcPD5Bpa24Bywh8G1JFFww9EFDfeI6WlAbMlrcf-tDm92SsYIQU16f8v3RyuHRX5XAGx793xAa20osiW931vUQ==";
//		String token = "jMuQ6mrJOcW8KxfkuA2rBdnbVwyV3dOQohXRLia-HWsu-BC22xJgmWI9WChwu3bBOVb-VwCi3b8ZP7kaxPOoW18gxN-VmYUR9ethuJiS42BH2Vzs3fIVYi3OncVJdEoi9AYcyWZPvMTR9YhGfqYBM9i5pwP8_wYpXSQJFzmr7kSacHN5A2qXbRBL9FCpebNHxl69cdurRH2c9A69GgVRKNzPJu_V8Ja-ox23NtzijxUmVPGrltS1OHnRkwz5IAXFl0e2Q1nqRPRd-sD2s7aEqjEMFi6nT0OxzDN1jw0-q74uRAcy3ntg4iZF5nvyoblboSOwyXfq0HDTLcm9tXYJNwpmzocwXjS2di04XQSBpZoTLDVmxHmuJrpeEEP90S04naPEybgVUnH5IW5IZItcMpBw1UY-RS73vaSYuUI5azUjPW7MimMkZg==";
		String token = "jMuQ6mrJOcW8KxfkuA2rBdnbVwyV3dOQohXRLia-HWsu-BC22xJgmWI9WChwu3bBOVb-VwCi3b9mGeZeCjx0o5GiO_GzxIVcJPiyboUikjFS2Kb-0sPm0LTQPcMKb0QGSr2jyVoBrHRsUnwqG96qJik171_mdRBPoevEAvDjFlrH2IINsQSIWkdqVlGKaEFYL0zPClqm4ZUmp2bOhzQEP_5ZIFaIE2KirOyNHgHDTRk01fok739ji-GqSDQjT1oev3Mi9SR6sBS_pVkQzBNcr5I0XmnR3SErPLkmdX42A28hW5ZRrsAUze1T5HEfd0-bUtrJZmpdhKyz95v-A9O7uQ3cRMdfGI4HGPqUiTqsylBy9ZlKmo_XdGjjFdpHkjDURoOT8aOu2Q2uID1FaerGcmhqmmLPEX4h3GUAwz_UcT95wBM9vWEfnA==";
		System.out.println(TenpayUtil.decrypt(token));
		//uin=525218150&trans_num=1&trans_id_0=1000000101201412101358592369&draw_id_0=1131000000101141210331875613&deal_id_0=795019790-20141210-1331875613&token_0=15d82f0283bee4713989a63eda78c70a&buyer_fee_0=0&seller_fee_0=1&remark=&timestamp=1418208514&passcheck=1&usertype=2&certcheck=1&channel=1&sign=62e3133c5d8cdc7a50fc696c6fa8bdc0
		//59d47827b34b6e359b93de0b9ee781e8
	}
	
	protected String map2StringV2(Map<String, String> params) {
		if (params != null && params.size() > 0) {
			StringBuffer sbf = new StringBuffer();
			for (Entry<String, String> entry : params.entrySet()) {
				if (entry != null && !StringUtil.isEmpty(entry.getKey()))
						//&& !StringUtil.isEmpty(entry.getValue()))
				{
					sbf.append("&").append(entry.getKey()).append("=").append(
							entry.getValue());
				}
			}
			return (sbf.length() > 1) ? sbf.substring(1) : sbf.toString();
		}
		return "";
	}

}
