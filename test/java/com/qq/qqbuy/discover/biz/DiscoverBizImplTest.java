package com.qq.qqbuy.discover.biz;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Map.Entry;

import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.qq.qqbuy.BaseTest;
import com.qq.qqbuy.discover.biz.impl.DiscoverBizImpl;
import com.qq.qqbuy.discover.biz.impl.EcDataPolicyProtocV1.CProPolicyCmd;
import com.qq.qqbuy.discover.biz.impl.STPolicyMsg;
import com.qq.qqbuy.discover.biz.impl.STPolicySysHead;

public class DiscoverBizImplTest extends BaseTest   {
	
	DiscoverBizImpl discoverBiz = new DiscoverBizImpl();
	
	@BeforeClass
	public static void beforeClass()	{
		System.setProperty("catalina.base", "c:");
	}
	
	
	private byte[] parseResponse() throws IOException	{
		InputStream is = this.getClass().getResourceAsStream("biResponse.bin");
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		int len;
		while((len=is.read(buf)) != -1)	{
			bos.write(buf, 0, len);
		}
		is.close();
		return bos.toByteArray();
	}
	
	@Test
	public void testParseCmd() throws IOException	{
		byte[] bs = parseResponse();
		byte[] dest = new byte[bs.length-1-1-172];
		System.arraycopy(bs, 173, dest, 0, dest.length);
		
		CProPolicyCmd cmd = CProPolicyCmd.parseFrom(dest);
	}
	
	@Test
	public void testParseSysHead() throws IOException	{
		byte[] bs = parseResponse();
		ByteBuffer buf = ByteBuffer.wrap(bs);
		System.out.println(buf.get());
//		System.out.println(buf.getLong());
	}
	
//	@Test
	public void test() throws UnsupportedEncodingException	{
		ByteBuffer buf = ByteBuffer.allocate(100);
		buf.putInt(1);
		buf.putLong(2);
		buf.put("啊哦".getBytes("utf-8"));
		System.out.println("啊哦".getBytes("utf-8").length);
		
		byte[] bs = buf.array();
		ByteBuffer b = ByteBuffer.wrap(bs);
		System.out.println(b.getInt());
		System.out.println(b.getLong());
		byte[] sb = new byte[6];
		b.get(sb);
		System.out.println(new String(sb, "utf-8"));
	}
	
//	@Test
	public void test1() throws IOException	{
		byte[] bs = parseResponse();
		byte[] headbs = STPolicyMsg.parseSysheadFromResp(bs);
		STPolicySysHead head = STPolicySysHead.deserialize(headbs);
		System.out.println(head.getMsgId());
	}
	
	@Test
	public void testParseEntireMsg() throws Exception	{
		FileInputStream is = new FileInputStream("d:/users/jing/desktop/test.bin");
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		int len;
		while((len=is.read(buf)) != -1)	{
			bos.write(buf, 0, len);
		}
		is.close();
	}
	
	@Test
	public void testBanner() throws IOException	{
		System.out.println(discoverBiz.discoverBanner());
		
		JsonObject j1 = discoverBiz.discoverBanner().getAsJsonObject();
		JsonObject j2 = discoverBiz.discoverSets().getAsJsonObject();
		for(Entry<String,JsonElement> e : j2.entrySet())	{
			j1.add(e.getKey(), e.getValue());
		}
		System.out.println(j1);
	}
	
	@Test
	public void testAct() throws IOException	{
		System.out.println(discoverBiz.discoverAct());
	}
}
