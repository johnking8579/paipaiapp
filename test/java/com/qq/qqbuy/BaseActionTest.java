package com.qq.qqbuy;

import org.apache.struts2.StrutsSpringTestCase;

public class BaseActionTest extends StrutsSpringTestCase{
	
	@Override
	protected String[] getContextLocations() {
		return new String[]{"classpath:conf-spring/*.xml"};
	}

}
