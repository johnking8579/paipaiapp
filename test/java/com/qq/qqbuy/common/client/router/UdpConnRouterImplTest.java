package com.qq.qqbuy.common.client.router;

import java.io.IOException;
import java.net.DatagramPacket;

import org.junit.Test;

import com.qq.qqbuy.common.client.router.Router;
import com.qq.qqbuy.common.client.router.udp.UdpConnRouterImpl;

public class UdpConnRouterImplTest {
	
	@Test
	public void test1() throws IOException	{
		new UdpConnRouterImpl(Router.PP_GAMMA).send("1.2.3.4",8080, "ssddddddddddddddddddddddsss".getBytes());;
	}
	
	@Test
	public void test2() throws IOException	{
		DatagramPacket packet = new UdpConnRouterImpl(Router.PP_GAMMA).sendAndReceive("1.2.3.4", 8080, "sssss".getBytes());
		System.out.println(packet.getLength());
	}

}
