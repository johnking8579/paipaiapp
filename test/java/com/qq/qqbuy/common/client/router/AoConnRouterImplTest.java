package com.qq.qqbuy.common.client.router;

import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.paipai.component.configagent.Configs;
import com.qq.qqbuy.common.client.ao.AoClient;
import com.qq.qqbuy.common.client.router.Router;
import com.qq.qqbuy.common.client.router.ao.AoConnRouterImpl;
import com.qq.qqbuy.common.client.router.ao.UinSkey;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.thirdparty.idl.dealpc.InfoType;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.SysGetDealInfoReq;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.SysGetDealInfoResp;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.AddFavItemListReq;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.AddFavItemListResp;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.GetUserLinkInfoReq;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.GetUserLinkInfoResp;

public class AoConnRouterImplTest {
	
	/**
	 * 解析appToken
	 */
	@Test
	public void testParseToken()	{
		String token = "wtOwLs3k5d2DSWLK4K%2FPexBO4wDt6UWytFYIAUYXM7Q4RH%2FMoR4UDKgk%2Fd26YRB6cfhN2LWN3KBULkx8kLCY9nejMByNwLYc51h%2BFWoVsOzpYFyDWMQvEIUYj%2BUbUmHpRkgwKDmq%2FgSymT2Irhvi3HocDHAD1tcK64lP17Og6Jvi6I8sFDfbqCygGqgDZx1aUBod5ebPkKQGNdravgXM5YAYYXUNwh21jdcTl5HIDVkmLS9WQ3tEaTzcIwG33KjDWHLByzt3wt%2BWeqtLc%2FrRrCZeoer8ZXUJ2WS0ADLvK6p14%3D";
		String mk = "ad93b1fb05fe7d414ac8b59d3eef669ff9d4f03e";
		UinSkey u = new AoConnRouterImpl(Router.PP_GAMMA).parseToken(token, mk);
		System.out.println(u.uin);
		System.out.println(u.skey);
		System.out.println(u.isLogin);
	}
	
	private AsynWebStub newStub()	{
		AsynWebStub stub = new AsynWebStub();
		stub.setTimeout(3000, 3000);
		stub.setUin(System.currentTimeMillis()); // 应为setUin被服务端用来做负载均衡，故需要设置。
		stub.setConfigType(Configs.PAIPAI_CONFIG_TYPE);
		stub.setClientIP(Util.iP2Long("127.0.0.1"));
		stub.setStringEncodecharset("gbk");
		stub.setStringDecodecharset("gbk");
		return stub;
	}
	
	@Test
	public void test1() {
		AsynWebStub stub = new AsynWebStub();
		stub.setTimeout(3000, 3000);
		stub.setUin(System.currentTimeMillis()); // 应为setUin被服务端用来做负载均衡，故需要设置。
		stub.setConfigType(Configs.PAIPAI_CONFIG_TYPE);
		stub.setClientIP(Util.iP2Long("127.0.0.1"));
		stub.setStringEncodecharset("gbk");
		stub.setStringDecodecharset("gbk");
		AddFavItemListReq req = new AddFavItemListReq();
		req.setAddItemIdList(null);
		req.setInReserve("dddddddd");
		req.setMechineKey("mmmmmaaa");
		req.setUin(1L);
		AddFavItemListResp resp = new AddFavItemListResp();
		int result = new AoClient(Router.PP_GAMMA).connect(stub, req, resp);
		if(result == 0)	{
			System.out.println(new Gson().toJson(resp));
		} else	{
			System.out.println("result=" + result);
		}
	}
	
	@Test
	public void test2() throws Exception	{
		SysGetDealInfoReq req = new SysGetDealInfoReq();
		SysGetDealInfoResp resp = new SysGetDealInfoResp();
		req.setDealId("795019790-20150409-1354958520");
//		req.setDealId("805303388-20150408-1354843846");
		req.setSource("WebPPDealQueryClient");
		req.setInfoType(InfoType.or(InfoType.DEAL, InfoType.TRADE_ALL, InfoType.TRADE_EXT, InfoType.REFUND));
		req.setIsHistory(false);
		
		AsynWebStub stub = new AsynWebStub();
		stub.setTimeout(3000, 3000);
		stub.setUin(System.currentTimeMillis()); // 应为setUin被服务端用来做负载均衡，故需要设置。
		stub.setConfigType(Configs.PAIPAI_CONFIG_TYPE);
		stub.setClientIP(Util.iP2Long("127.0.0.1"));
		stub.setStringEncodecharset("gbk");
		stub.setStringDecodecharset("gbk");
		
		int result = new AoClient(Router.PP_IDC).connect(stub, req, resp);
		System.out.println("result=" + result);
		System.out.println(new GsonBuilder().setPrettyPrinting().create().toJson(resp));
	}
	
	/**
	 * ，请求时候添加了reqnum字段；命令字：0x8611，机器：10.198.7.47：53101
		测试账号：389163364，157676424
	 */
	@Test
	public void test3()	{
		GetUserLinkInfoReq req = new GetUserLinkInfoReq();
		GetUserLinkInfoResp resp = new GetUserLinkInfoResp();
//		req.setUin(389163364);
		req.setUin(157676424);
		req.setReqnum(3);
		
//		AsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		AsynWebStub stub = newStub();
//		stub.setIpAndPort("10.198.7.47", 53101);
		
		int result = new AoClient(Router.PP_IDC).connect(stub, req, resp);
		System.out.println("result=" + result);
		System.out.println(new GsonBuilder().setPrettyPrinting().create().toJson(resp));
	}

}
