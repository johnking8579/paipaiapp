package com.qq.qqbuy.bi.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.junit.Test;
import org.springframework.aop.framework.ProxyFactory;

import com.opensymphony.xwork2.ActionProxy;
import com.paipai.component.c2cplatform.AsynWebStubException;
import com.qq.qqbuy.BaseActionTest;
import com.qq.qqbuy.common.ConfigHelper;
import com.qq.qqbuy.common.client.http.HttpClient;
import com.qq.qqbuy.common.client.http.HttpConn;
import com.qq.qqbuy.common.client.http.HttpConnJdkImpl;
import com.qq.qqbuy.common.client.http.HttpResp;
import com.qq.qqbuy.common.client.log.HttpConnLogAdvice;
import com.qq.qqbuy.common.client.log.OnlyTimeLogParser;
import com.qq.qqbuy.common.client.log.PacketLogParser;

public class BiActionTest extends BaseActionTest{
	
	@Test
	public void test2() throws IOException	{
		String url = "http://localhost:8080/paipaiapp/api/bi/upPv.xhtml";
		HttpURLConnection c = (HttpURLConnection)new URL(url).openConnection();
		c.setDoOutput(true);
		c.setRequestMethod("POST");
		PrintWriter pw = new PrintWriter(c.getOutputStream());
		//pw.print("networkType=wifi&versionCode=300&longitude=0&mk=0e45052d69448b124b10be66377e41d0bbe0981c&appToken=111&osVersion=4.2.2&mt=android&imei=359209026488746&&appVersion=V3.0.0&networkServer=&latitude=0&channel=%7B%22gdt_vid%22%3A%22%22%2C%22jd_pop%22%3A%22%22%2C%22market%22%3A%22f_yingyonghui%22%2C%22pprd_p%22%3A%22%22%2C%22pps%22%3A%22%22%2C%22qz_express%22%3A%22%22%2C%22qz_gdt%22%3A%22%22%2C%22wid%22%3A%22286637966%22%7D&appID=1&content=dddddd");
		pw.print("appToken=&networkType=wifi&osVersion=4.4.2&mt=android&imei=&versionCode=214002&content=1424937245611|||1424937245611|||welcome|||0||||||||||||||||||e4eb9ff121f35e874741ae1b52109b27c8bed8e8|1424937245611|9|||&pinid=&appVersion=2.1.4&networkServer=ChinaMobile&longitude=0&latitude=0&mk=e4eb9ff121f35e874741ae1b52109b27c8bed8e8&channel={\"market\":\"yima003\"}&appID=2");
		pw.flush();
		pw.close();
		
		c.getInputStream();
		
//		BeanGenerator beanGenerator = new BeanGenerator();
//		  beanGenerator.addProperty("value", String.class);
//		  Object myBean = beanGenerator.create();

		
	}
	
	@Test
	public void testExpoItem() throws Exception	{
		request.setParameter("daps", "48wcZnCaWeK:1fxoK7hq5ac:48:AE46456D000000000401000039BF1155,48wcZnCaWeK:1fxoK7hq5ai:48:05584AA200000000040100003B8494EA,48wcZnCaWeK:1fxoK7hq5ai:48:46232D05000000000401000039549AC6,48wcZnCaWeK:1fxoK7hq5ai:48:B02B4C8F0000000004010000370307AB");
		ActionProxy proxy = getActionProxy("/api/bi/expoItem.xhtml");
		proxy.execute();
	}
	
	@Test
	public void testUppv() throws Exception	{
		//String withoutContent = "networkType=wifi&versionCode=317&longitude=120.545217&mk=ae708ce56d2aac1e427198397a350402d54883d6&osVersion=4.4.2&mk2=864036020841069-0c96bfe463d7&mt=android&imei=864036020841069&pinid=&appVersion=3.1.7&networkServer=%E4%B8%AD%E5%9B%BD%E8%81%94%E9%80%9A&latitude=31.315427&channel=%7B%22gdt_vid%22%3A%22%22%2C%22jd_pop%22%3A%22%22%2C%22market%22%3A%22f_GDTYYB%22%2C%22pprd_p%22%3A%22%22%2C%22pps%22%3A%22%22%2C%22qz_express%22%3A%22%22%2C%22qz_gdt%22%3A%22%22%2C%22wid%22%3A%22%22%7D&appID=1";
//		for(String pair : withoutContent.split("&"))	{
//			String[] arr = pair.split("=", 2);
//			request.setParameter(arr[0], arr[1]);
//		}
		request.setParameter("content", readTxt("content.txt"));
		ActionProxy proxy = getActionProxy("/api/bi/upPv.xhtml");
		proxy.execute();
	}
	
	private String readTxt(String txt) throws IOException	{
		InputStream is = this.getClass().getResourceAsStream(txt);
		BufferedReader br = new BufferedReader(new InputStreamReader(is, "utf-8"));
		StringBuilder sb = new StringBuilder();
		String s;
		while((s=br.readLine())!= null)	{
			sb.append(s);
		}
		is.close();
		return sb.toString();
	}
	
	@Test
	public void testProxy() throws Exception	{
		HttpResp resp = new HttpClient().get("http://www.baidu.com", null, new OnlyTimeLogParser());
		System.out.println(new String(resp.getResponse()));
	}
}
