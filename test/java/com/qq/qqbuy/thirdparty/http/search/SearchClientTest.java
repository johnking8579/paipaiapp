package com.qq.qqbuy.thirdparty.http.search;

import org.junit.Test;

import com.google.gson.Gson;
import com.qq.qqbuy.common.Pager;
import com.qq.qqbuy.search.util.SearchConstant;
import com.qq.qqbuy.thirdparty.http.search.po.SearchBizParams;
import com.qq.qqbuy.thirdparty.http.search.po.SearchCondition;
import com.qq.qqbuy.thirdparty.http.search.po.SearchResult;
import com.qq.qqbuy.thirdparty.http.search.po.SearchSysParams;

public class SearchClientTest {
	
	SearchClient c = new SearchClient();
	@Test
	public void test1()	{
		ShopItemSearchParam par = new ShopItemSearchParam();
		par.setShopId("2952249745");
		Pager p = c.searchShopItem(par);
		System.out.println(new Gson().toJson(p.getElements()));
	}
	
	@Test
	public void test2()	{
		SearchCondition condition = new SearchCondition();
		
		SearchSysParams sysParams = condition.getSysParam();
		sysParams.setCharset("gbk");
		sysParams.setDtag("");
		sysParams.setDtype("json");
		sysParams.setShowPath(true);
		sysParams.setCluster(true);

		SearchBizParams bizParams = condition.getBizParam();
		bizParams.setKeyword("衬衫");
		bizParams.setPageSize(5);
		bizParams.setPageNum(1);
//		bizParams.setOrderStyle(1);
//		bizParams.setPath("2");
		bizParams.setBeginPrice(0);
		bizParams.setEndPrice(0);
		bizParams.setAc(1);
		bizParams.setEarmark(0);
		bizParams.addProperty(SearchConstant.PROP_PAIPAI_AND_WANGGOU);
		
		SearchClient searchClient = new SearchClient();
		SearchResult res = searchClient.search(condition, 525218150,317);
	}

}
