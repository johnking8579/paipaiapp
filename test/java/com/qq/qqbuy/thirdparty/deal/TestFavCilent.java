package com.qq.qqbuy.thirdparty.deal;

import java.util.Vector;

import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

import com.qq.qqbuy.common.po.CommonPo;
import com.qq.qqbuy.favorite.biz.FavoriteBiz;
import com.qq.qqbuy.favorite.biz.impl.FavoriteBizImpl;
import com.qq.qqbuy.favorite.po.FavoriteShopDetailPo;
import com.qq.qqbuy.favorite.po.FavoriteShopDetails;

public class TestFavCilent
{

    /**
     * @Title: main
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param args
     *            设定文件
     * @return void 返回类型
     * @throws
     */
    public static void main(String[] args)
    {
        String jsonStr = "";
        // TestFavCilent client = new TestFavCilent();
        JsonConfig jsoncfg = new JsonConfig();
        String[] excludes =
        { "size", "empty" };
        jsoncfg.setExcludes(excludes);
        FavoriteBiz favBiz = new FavoriteBizImpl();

        // //1、查询列表
        // FavoriteItemsPo items = favBiz.getFavItemList(50888394,
        // "49410572129930375", "@yUKV7BClK", 0, 5);
        // jsonStr = JSONSerializer.toJSON(items, jsoncfg).toString();
        // System.out.println("getFavItemList: " + jsonStr);
        //
        // //2、增加商品
        // Vector<String> addItemIdList = new Vector<String>();
        // addItemIdList.add("CEA06711000000000000000000000E0A");
        // FavoriteUpdatePo addPo = favBiz.addFavItemList(50888394,
        // "49410572129930375", "@yUKV7BClK", addItemIdList);
        //
        // jsonStr = JSONSerializer.toJSON(addPo, jsoncfg).toString();
        // System.out.println("addFavItemList: " + jsonStr);
        //
        // //3、查询列表
        // items = favBiz.getFavItemList(50888394, "49410572129930375",
        // "@yUKV7BClK", 0, 5);
        // jsonStr = JSONSerializer.toJSON(items, jsoncfg).toString();
        // System.out.println("getFavItemList: " + jsonStr);
        //
        // //4、删除商品
        // CommonPo po = favBiz.delFavItemList(50888394, "49410572129930375",
        // "@yUKV7BClK", addItemIdList);
        //
        // jsonStr = JSONSerializer.toJSON(po, jsoncfg).toString();
        // System.out.println("delFavItemList: " + jsonStr);

        // 1、查询列表
        FavoriteShopDetails items = favBiz.getFavShopList(50888394,
                "49410572129930375", "@yUKV7BClK", 0, 5);
        jsonStr = JSONSerializer.toJSON(items, jsoncfg).toString();
        System.out.println("getFavShopList: " + jsonStr);

        // 2、增加店铺
        Vector<Long> addItemIdList = new Vector<Long>();
        addItemIdList.add(313946562L);
        CommonPo addPo = favBiz.addShop2Fav(50888394L,
                "49410572129930375", "@yUKV7BClK", 313946562L, "");

        jsonStr = JSONSerializer.toJSON(addPo, jsoncfg).toString();
        System.out.println("addShop2Fav: " + jsonStr);

        // 3、查询列表
        items = favBiz.getFavShopList(50888394, "49410572129930375",
                "@yUKV7BClK", 0, 5);
        jsonStr = JSONSerializer.toJSON(items, jsoncfg).toString();
        System.out.println("getFavShopList: " + jsonStr);
        
        // 4、查询单个信息
        FavoriteShopDetailPo item = favBiz.getShopIdFavInfo(50888394, "49410572129930375",
                "@yUKV7BClK", 313946562L);
        jsonStr = JSONSerializer.toJSON(item, jsoncfg).toString();
        System.out.println("getShopIdFavInfo: " + jsonStr);

        // 5、删除商品
        CommonPo po = favBiz.delShopItemList(50888394, "49410572129930375",
                "@yUKV7BClK", addItemIdList);

        jsonStr = JSONSerializer.toJSON(po, jsoncfg).toString();
        System.out.println("delShopItemList: " + jsonStr);
    }

}
