//package com.qq.qqbuy.thirdparty.deal;
//
//import net.sf.json.JSONSerializer;
//import net.sf.json.JsonConfig;
//
//import com.qq.qqbuy.snapup.biz.SnapUpBiz;
//import com.qq.qqbuy.snapup.biz.impl.SnapUpBizImpl;
//import com.qq.qqbuy.snapup.filter.SnapupItemInfoFilter;
//
//public class SnapupTest
//{
//
//    /**
//     * @Title: main
//     * @Description: TODO(这里用一句话描述这个方法的作用)
//     * @param args
//     *            设定文件
//     * @return void 返回类型
//     * @throws
//     */
//    public static void main(String[] args)
//    {
//      getSnapupStock("2564F63200000000040100002414C38F");
//       //testSnapUp();
//        
//        
//        //testGetSnapupDealCode(313946562,37640947);
//    }
//
//    
//    /**
//     * 
//     * @Title: getSnapupStock
//     *  
//     * @Description: 查询抢购信息 
//     * @param itemCode    设定文件 
//     * @return void    返回类型 
//     * @throws
//     */
//    public static void getSnapupStock(String itemCode)
//    {
//        SnapUpBiz biz = new SnapUpBizImpl();
//        JsonConfig jsoncfg = new JsonConfig();
//        String[] excludes =
//        { "size", "empty" };
//        jsoncfg.setExcludes(excludes);
//        String datastr = JSONSerializer.toJSON(
//                biz.getSnapupCmdysStockInfo(itemCode), jsoncfg).toString();
//        System.out.println("itemCode:" + itemCode + " datastr:" + datastr);
//
//    }
//    
//    
//    public static void testSnapUp()
//    {
//        SnapUpBiz biz = new SnapUpBizImpl();
//        JsonConfig jsoncfg = new JsonConfig();
//        SnapupItemInfoFilter filter = new SnapupItemInfoFilter();
//        
////        filter.setBuyerUin(getQq());
////        filter.setBuyerName(buyerName);
////        filter.setSellerUin(sellerUin);
////        filter.setItemCode(itemCode);
////        filter.setBuyNum(buyNum);
////        filter.setMaxBuyNum(maxBuyNum);
////        filter.setPrice(price);
////        // req.setProperty(stockAttr.getStockAttrStr().length() == 0 ?
////        // "0" :
////        // "1");
////        filter.setProperty(property);
////        filter.setActiveType(activeType);
////        filter.setPostCode(postCode);
////        filter.setRegion(region);
////        filter.setAddress(address);
////        filter.setAddressId(addressId);
////        filter.setPhone(phone);
////        filter.setMobile(mobile);
////        filter.setBuyerRemark(buyerRemark);
////        filter.setStockAttr(stockAttr);
////        filter.setUploadTime(uploadTime);
//        
//        
//        filter.setBuyerUin(313946562);
//        filter.setBuyerName("胡文斌");
//        filter.setSellerUin(85505239L);
//        filter.setItemCode("D7B4180500000000040100001BAF5216");
//        filter.setBuyNum(1);
//        filter.setMaxBuyNum(1);
//        filter.setPrice(7900);
//        filter.setProperty("1");
//        filter.setActiveType(18);
//        filter.setPostCode("518000");
//        filter.setMobile("15889785202");
//        filter.setAddress("广东深圳市南山区南新路东方美地苑2栋四单元201室");
//        filter.setAddressId(3);
//        filter.setUploadTime("2012-12-27 09:00:39");
//        filter.setStockAttr("尺码:42|颜色分类:303黑红");
//        
//        String[] excludes =
//        { "size", "empty" };
//        jsoncfg.setExcludes(excludes);
//        String datastr = JSONSerializer.toJSON(
//                biz.snapUp(filter), jsoncfg).toString();
//        System.out.println("filter:" + filter + "\r\n" + datastr);
//    }
//    
//    
//    public static void testGetSnapupDealCode(long uin,long queueId)
//    {
//        SnapUpBiz biz = new SnapUpBizImpl();
//        JsonConfig jsoncfg = new JsonConfig();
//        String[] excludes =
//        { "size", "empty" };
//        jsoncfg.setExcludes(excludes);
//        String datastr = JSONSerializer.toJSON(
//                biz.getSnapupDealCode(uin, queueId), jsoncfg).toString();
//        System.out.println("queueId:" + queueId + "\r\n" + datastr);
//    }
//
//}
