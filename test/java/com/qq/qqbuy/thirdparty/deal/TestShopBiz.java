//package com.qq.qqbuy.thirdparty.deal;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import net.sf.json.JSONSerializer;
//import net.sf.json.JsonConfig;
//
//import com.paipai.component.c2cplatform.AsynWebStubException;
//import com.paipai.component.c2cplatform.IAsynWebStub;
//import com.paipai.component.c2cplatform.impl.AsynWebStub;
//import com.qq.qqbuy.bargain.bean.BargainPrice;
//import com.qq.qqbuy.bargain.biz.BargainBiz;
//import com.qq.qqbuy.bargain.biz.impl.BargainBizImpl;
//import com.qq.qqbuy.common.util.StringUtil;
//import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiGetShopComdyListReq;
//import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiGetShopComdyListResp;
//import com.qq.qqbuy.thirdparty.idl.shop.protocol.ApiItemFilter;
//import com.qq.qqbuy.thirdparty.openapi.OpenApi;
//import com.qq.qqbuy.thirdparty.openapi.OpenApiProxy;
//import com.qq.qqbuy.thirdparty.openapi.po.GetShopInfoRequest;
//import com.qq.qqbuy.thirdparty.openapi.po.GetShopInfoResponse;
//
//public class TestShopBiz
//{
//    
//    public static String getCategoryFromBit(String categoryId)
//    {
//        String ret = "";
//        char map[] = {'1','2','4','8'};
//        long bitValue = StringUtil.toLong(categoryId,0);
//        if(bitValue > 0 && bitValue <= 256)
//        {
//            String tmp = "0000000000000000000000000000000000000000000000000000000000000000";
//            int pos = (int)(64 - (bitValue - 1)/4 -1 );
//            ret = tmp.substring(0,pos) + map[(int)(bitValue - 1)%4] + tmp.substring(pos + 1);
//        }else
//        {
//            ret = "0000000000";
//        }
//        System.out.println(ret);
//        return ret;
//    }
//
//    /**
//     * 
//     * @Title: testGetShopInfo 
//     * @Description: 获取店铺信息
//     * @param sellerUin    设定文件 
//     * @return void    返回类型 
//     * @throws
//     */
//    public void testGetShopInfo(long sellerUin)
//    {
//        GetShopInfoRequest req = new GetShopInfoRequest();
//        req.setSellerUin(sellerUin);
//        req.setUin(sellerUin);
//        // 1、获取订单总数
//        OpenApiProxy proxy = null;
//        proxy = OpenApi.getProxy();
//        GetShopInfoResponse res = proxy.getShopInfo(req);
//        if (res != null && res.isSucceed())
//        {
//            System.out.println(res);
//        }
//    }
//
//    public void doShopComdyList() throws Exception {
//        try {
//            //
//            // 1、设置请求参数
//            ApiGetShopComdyListReq req = new ApiGetShopComdyListReq();
//            ApiGetShopComdyListResp resp = new ApiGetShopComdyListResp();
//            req.setMachineKey("mobileLife");
//            req.setSource("mobileLife");
//            req.setShopID(855008480);
//            ApiItemFilter filter = new ApiItemFilter();
//            req.setApiItemFilter(filter);
//            filter.setPageSize(100);
//            filter.setStartIndex(0);
//            //filter.setOrderType(18);
//            Map<String,String> filterMap = new HashMap<String,String>();
//            filterMap.put("shopcategory", "63");
//            filter.setSellerUin(855008480);
//            filter.setFilterMap(filterMap);
//            
//            // 2、调用协议
//            IAsynWebStub webStubCntl = new AsynWebStub();
//            webStubCntl.setOperator(1922434773);
//            int ret = webStubCntl.invoke(req, resp);
//
//            JsonConfig jsoncfg = new JsonConfig();
//            String[] excludes = { "size", "empty" };
//            jsoncfg.setExcludes(excludes);
//            String jsonStr = JSONSerializer.toJSON(resp, jsoncfg).toString();
//            // 3、输出结果
//            System.out.println("client req:" + req + " ret:" + ret + " resp:"
//                    + resp);
//            System.out.println(jsonStr);
//        } catch (AsynWebStubException e) {
//            e.printStackTrace();
//        }
//    }
//    
//    /**
//     * @Title: main
//     * @Description: TODO(这里用一句话描述这个方法的作用)
//     * @param args
//     *            设定文件
//     * @return void 返回类型
//     * @throws
//     */
//    public static void main(String[] args)
//    {
//        BargainBiz bargainBiz = new BargainBizImpl();
//        List<BargainPrice> ret = bargainBiz.getVoiceDatas(0);
//        JsonConfig jsoncfg = new JsonConfig();
//        String[] excludes =
//        { "size", "empty", "saleType", "date" };
//        jsoncfg.setExcludes(excludes);
//        String retJson = "{\"items\":"
//                + JSONSerializer.toJSON(ret, jsoncfg).toString() + "}";
//        System.out.println(retJson);
////        ShopBiz client = new ShopBizImpl();
////        JsonConfig jsoncfg = new JsonConfig();
////        String[] excludes = { "size", "empty" };
////        jsoncfg.setExcludes(excludes);
////        String jsonStr = null;
////        // 1、获取店铺信息
////        ShopInfo shopInfo = client.getShopInfo(855008480, true, true);
////        jsonStr = JSONSerializer.toJSON(shopInfo, jsoncfg).toString();
////        System.out.println("[getShopInfo]: " + jsonStr);
////        
////        //2、获取推荐
////        List<ShopSimpleItem> simpleItems = client.getRecommedComdyList(855000281, "mobileLife");
////        jsonStr = "{\"items\":"
////            + JSONSerializer.toJSON(simpleItems, jsoncfg).toString() + "}";
////        System.out.println("[getRecommedComdyList]: " + jsonStr);
////        
////        //3、获取商品分类
////        List<ShopCategoryItem> items = client.getShopCategoryListByUin(855008480, "mobileLife");
////        jsonStr = "{\"items\":"
////            + JSONSerializer.toJSON(items, jsoncfg).toString() + "}";
////        System.out.println("[getShopCategoryListByUin]: " +  jsonStr);
////        
////        //4、获取商品列表
////        Map<String,String> filterMap = new HashMap<String,String>();
////        //filterMap.put("shopcategory", "0");
////        filterMap.put("state", "1110");
////        ShopItems ret = client.getShopComdyList(855008480, "mobileLife",0,18,15,filterMap);
////        jsonStr = "{\"items\":"
////            + JSONSerializer.toJSON(ret, jsoncfg).toString() + "}";
////        System.out.println("[getShopComdyList]: " + jsonStr);
//    }
//
//}
