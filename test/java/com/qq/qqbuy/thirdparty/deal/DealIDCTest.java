//package com.qq.qqbuy.thirdparty.deal;
//
//
//import java.net.URLDecoder;
//import java.net.URLEncoder;
//
//import net.sf.json.JSONSerializer;
//import net.sf.json.JsonConfig;
//
//import com.paipai.component.c2cplatform.impl.AsynWebStub;
//import com.paipai.component.configagent.Configs;
//import com.paipai.deal.protocol.CloseDealReq;
//import com.paipai.deal.protocol.CloseDealResp;
//import com.paipai.deal.protocol.GetTemplateDetailReq;
//import com.paipai.deal.protocol.GetTemplateDetailResp;
//import com.qq.qqbuy.common.po.QQBuyServiceComm;
//import com.qq.qqbuy.common.util.MD5Coding;
//import com.qq.qqbuy.deal.biz.QueryDealBiz;
//import com.qq.qqbuy.deal.biz.impl.QueryDealBizImpl;
//import com.qq.qqbuy.thirdparty.idl.verifycode.protocol.CheckLoginReq;
//import com.qq.qqbuy.thirdparty.idl.verifycode.protocol.CheckLoginResp;
//import com.qq.qqbuy.thirdparty.openapi.OpenApi;
//import com.qq.qqbuy.thirdparty.openapi.OpenApiProxy;
//import com.qq.qqbuy.thirdparty.openapi.po.BuyerCancelDealRequest;
//import com.qq.qqbuy.thirdparty.openapi.po.BuyerCancelDealResponse;
//
//public class DealIDCTest
//{
//    public static void main(String[] args) throws Exception
//    {
////        getMyOrderSummary(313946562);
////        
////        getDealList(313946562,"DS_WAIT_BUYER_PAY",1,5);
//////        
////        getDealInfo(1799541383,"1799541383-20121213-978005008",1);
////        System.out.println(URLEncoder.encode("oloche+rXW2+LxjrcQIlz9767LTLt8uk15c3d3d90201&#61;&#61;".replace("&#61;", "="),"utf-8"));
////        System.out.println(URLDecoder.decode("oloche+rXW2+LxjrcQIlz9767LTLt8uk15c3d3d90201==","utf-8"));
//////        checkLogin("oloche+rXW2+LxjrcQIlz9767LTLt8uk15c3d3d90201==","170.2.23.32");
//        String mk = "11345678912345678";
//        String sk = "@"
//            + MD5Coding.encode2HexStr(("518800364" + mk).getBytes())
//            .toLowerCase().substring(0, 8) + "@";
//        getShippingFee(518800364L,(short)10,sk,mk);
//        
//        
//        //测试关闭订单
////        cancelDeal(313946562L,"1799541383-20121213-978005008",12);
//    }
//    
//
//    public static void cancelDeal(long buyerUin,String dealCode, long closeReason)
//    {
//        CloseDealReq req = new CloseDealReq();
//        req.setCloseReason(closeReason);
//        req.setDealId(dealCode);
//        req.setSource("moblie");
//        CloseDealResp resp = new CloseDealResp();
//        resp.setResult(-100);
//        AsynWebStub webStub = new AsynWebStub();
//        webStub.setUin(buyerUin); // 应为setUin被服务端用来做负载均衡，故需要设置。
//        webStub.setOperator(buyerUin);
//        webStub.setConfigType(Configs.PAIPAI_CONFIG_TYPE);
//        String mk = "11345678912345678";
//        String sk = "@"
//          + MD5Coding.encode2HexStr(("" + buyerUin + mk).getBytes())
//          .toLowerCase().substring(0, 8) + "@";
//        webStub.setSkey(sk.getBytes());
//        webStub.setMachineKey(mk.getBytes());
//        try {
//            int ret = webStub.invoke(req, resp);
//        } catch (Exception e) {
//            System.out.println("resp:" + resp);
//        }
//        
//        System.out.println(resp);
//    }
//    
//    
//    /**
//     * 
//     * @Title: getShippingFee 
//     * 
//     * @Description: 测试运费模板的 
//     * @param uin
//     * @param innerId
//     * @param skey
//     * @param mk    设定文件 
//     * @return void    返回类型 
//     * @throws
//     */
//    public static void getShippingFee(long uin,short innerId, String skey,String mk)
//    {
//        GetTemplateDetailReq req = new GetTemplateDetailReq();
//        req.setUin(uin);
//        req.setCInnerId(innerId);
//        GetTemplateDetailResp resp = new GetTemplateDetailResp();
//        resp.setResult(-7);
//        
//        AsynWebStub webStub = new AsynWebStub();
//        webStub.setUin(50888394); // 应为setUin被服务端用来做负载均衡，故需要设置。
//        webStub.setOperator(50888394);
//        webStub.setConfigType(Configs.PAIPAI_CONFIG_TYPE);
//        webStub.setSkey("@LuYxIIAgi".getBytes());
//        webStub.setMachineKey(mk.getBytes());
//        try {
//            int ret = webStub.invoke(req, resp);
//            System.out.println("uin:" + uin + " innerId:" + innerId + " skey:" + skey + " mk:" + mk 
//                    + "\r\nresp:" + resp);
//        } catch (Exception e) {
//            System.out.println("resp:" + resp);
//        }
//    }
//    
//   
//
//}