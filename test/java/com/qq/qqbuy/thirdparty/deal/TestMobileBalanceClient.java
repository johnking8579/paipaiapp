package com.qq.qqbuy.thirdparty.deal;

import com.qq.qqbuy.thirdparty.idl.mobileBalance.MobileBalanceClient;
import com.qq.qqbuy.thirdparty.idl.mobileBalance.protocol.QueryBalanceResp;

public class TestMobileBalanceClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String mobileNo = "15889785201";
		long uin = 525711678;
		QueryBalanceResp resp = MobileBalanceClient.queryMobileBalance(mobileNo, uin);
		System.out.println(resp.getQueryMobilebalanceRespone().getBalance());
	}

}
