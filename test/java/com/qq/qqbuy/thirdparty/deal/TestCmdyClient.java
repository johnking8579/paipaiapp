//package com.qq.qqbuy.thirdparty.deal;
//
//import java.io.UnsupportedEncodingException;
//import java.net.URLDecoder;
//import java.util.Vector;
//
//import com.qq.qqbuy.cmdy.po.CmdyItem;
//import com.qq.qqbuy.cmdy.po.CmdyItemListPo;
//import com.qq.qqbuy.cmdy.po.CmdyMakeOrderPo;
//import com.qq.qqbuy.cmdy.po.CmdyOrder;
//import com.qq.qqbuy.cmdy.util.CmdyConverter;
//import com.qq.qqbuy.cmdy.util.CmdyUtil;
//import com.qq.qqbuy.cmdy.util.CmdyValidate;
//import com.qq.qqbuy.common.constant.ErrConstant;
//import com.qq.qqbuy.common.exception.BusinessException;
//import com.qq.qqbuy.common.util.JSONUtil;
//import com.qq.qqbuy.common.util.StringUtil;
//import com.qq.qqbuy.deal.po.ConfirmOrderPo;
//import com.qq.qqbuy.deal.po.ConfirmOrderVo;
//import com.qq.qqbuy.deal.po.ConfirmPackageVo;
//import com.qq.qqbuy.deal.po.MakeOrderPo;
//import com.qq.qqbuy.deal.util.DealUtil;
//import com.qq.qqbuy.recvaddr.biz.impl.RecvAddrBizImpl;
//import com.qq.qqbuy.recvaddr.po.RecvAddr;
//import com.qq.qqbuy.thirdparty.idl.cmdy.CmdyClient;
//import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.AddCmdy2CartResp;
//import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.CommodityView;
//import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.ConfirmOrderResp;
//import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.DealViewData;
//import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.GetCmdyKindsResp;
//import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.MakeOrderResp;
//import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.ModifyCmdyNumInCartResp;
//import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.MultiPrice;
//import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.PromotionRule;
//import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.RmvCmdyFromCartResp;
//import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.ShopView;
//import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.ViewCartResp;
//import com.qq.qqbuy.thirdparty.openapi.po.GetReceiverAddressListResponse.ReceiverAddress;
//
//public class TestCmdyClient {
//
//	/**
//	 * @param args
//	 * @throws UnsupportedEncodingException
//	 */
//	public static void main(String[] args) throws UnsupportedEncodingException {
//
//		System.getProperties().put("qgo.cfg.env", "local");
//		// System.out
//		// .println(URLDecoder
//		// .decode("0%7E%7E611968687%7E0%7EAFE6792400000000002F3A65071AF5B5%24%E7%89%88%E6%9C%AC%3A%E6%B8%AF%E8%A1%8C%E5%B8%A6%E5%8F%91%E7%A5%A8%7C%E9%A2%9C%E8%89%B2%3A%E9%BB%91%E7%BB%BF%7C%E9%85%8D%E7%BD%AE%3A%E8%B1%AA%E5%8D%8E%E9%85%8D%E7%BD%AE%E5%8C%85%E9%82%AE%242%240%24%7E%2C0%7E%7E1278298472%7E0%7E6849314C000000000401000015BE68FC%24%E4%B8%BB%E8%A6%81%E9%A2%9C%E8%89%B2%3A%E9%BB%91%E8%89%B2%7C%E4%B8%8A%E8%A3%85%E5%B0%BA%E7%A0%81%3AM%241%240%24%7E%2C",
//		// "utf-8"));
//		// 增加
//		// buyerUin itemCode itemAttr buyNum addForce
//		// args = new String[] {"0", "381936231",
//		// "A7B7C4040000000004010000000153E8", "", "1", "0"};
//		// args = new String[] {"0", "381936231",
//		// "A7B7C404000000000000000000000A98", "", "1", "0"};
//		// args = new String[] {"0", "381936231",
//		// "A7B7C40400000000000000000000136D", "", "1", "0"};
//
//		// args = new String[] {"0", "381936231",
//		// "FD0F084400000000007D3A7D0001C4DA", "", "1", "0"};
//		// args = new String[] {"0", "381936231",
//		// "E622493E0000000000663B0E0001C7E2", "", "1", "0"};
//
//		// args = new String[] {"0", "381936231",
//		// "EEAC6E370000000004010000000090C2", "", "1", "0"};
//		// args = new String[] {"0", "381936231",
//		// "EEAC6E370000000004010000000090C9",
//		// "dadada:dadfdf|sss:ddddddddddddddd|ere:ss", "1", "0"};
//
//		// 新增到购物车：测试促销
//		// args = new String[] {"0", "313946562",
//		// "AFE6792400000000002F3A65071AF5B5", "版本:港行带发票|颜色:黑绿|配置:豪华配置包邮", "2",
//		// "0"};
//
//		// 查询
//		// buyerUin opForce
//		// args = new String[] {"3", "151738366", "1"};
//
//		// 修改商品数量
//		// buyerUin itemCode itemAttr buyNum
//		// args = new String[] {"2", "313946562",
//		// "AFE6792400000000002F3A65071AF5B5", "版本:港行带发票|颜色:黑绿|配置:豪华配置包邮", "3"};
//
//		// 删除商品
//		// buyerUin payTyp itemCode-itemAttr-buyNum itemCode1-itemAttr1-buyNum1
//		// ......
//		// args = new String[] {"1", "313946562", "1",
//		// "AFE6792400000000002F3A65071AF5B5-版本:港行带发票|颜色:黑绿|配置:豪华配置包邮-1"};
//
//		// 分单
//		// buyerUin regionId payType itemCode-itemAttr-buyNum
//		// itemCode1-itemAttr1-buyNum1 ......
//		// args = new String[] {"4", "50888394", "40004", "0",
//		// "67607313000000000401000000004273--1"};
//		// args = new String[] {"4", "381936231", "40004", "0",
//		// "FD0F084400000000007D3A7D0001C4DA--3"};
//		// itemCode$itemAttr$buyNum~itemCode1$itemAttr1$buyNum1~
//		args = new String[] { "5", "151738366", "40004", "0",
//				"0~9747187~1341636272~0~B0BEF74F00000000040100001326DFB7%24%241%2415%240~~0%2C" };
//
//		// adit=4&ver=2&itemList=D4704C0B00000000040100001C84B88D$主要颜色:颜色以实物为准%7C男裤尺码:30%20（2尺3）$1~&dtag=ios&payType=0&
//		// args = new String[] { "4", "151738366", "40004", "0",
//		// "D4704C0B00000000040100001C84B88D$主要颜色:颜色以实物为准|男裤尺码:30 （2尺3）$1" };
//
//		// 分单，测试促销
//		// args = new String[] {"4", "50888394", "40004", "0",
//		// "D77C6B67000000000401000010EE5A44--1"};
//
//		// 下单
//		// buyerUin recvAddrId
//		// comboId~dealNote~dealType~invoiceTitle~invoiceType~promotionRuleId~sellerProperty~sellerUin~specialFee~specialFeeType~transportType
//		// itemCode-itemAttr-buyNum-priceType-redPacketId
//		// itemCode1-itemAttr1-buyNum1-priceType1-redPacketId1 ......
//		// args = new String[] {"5", "313946562", "1",
//		// "~TestClient测试dealNote--wendyhu~0~TestClient测试invoiceTitle--wendyhu~~0~1~1735097559~~~1",
//		// "D77C6B67000000000401000010EE5A44--2-0-"};
//		// args = new String[] {"5", "381936231", "3",
//		// "~TestClient测试dealNote--winson~0~TestClient测试invoiceTitle--winson~~0~1~1141379069~~~1",
//		// "FD0F084400000000007D3A7D0001C4DA--3-0-"};
//		// args = new String[] {"5", "381936231", "3",
//		// "~TestClient测试dealNote--winson~0~TestClient测试invoiceTitle--winson~~0~0~1044980454~~~1",
//		// "E622493E0000000000663B0E0001C7E2--1-0-"};
//		// args = new String[] {"5", "381936231", "3",
//		// "~TestClient测试dealNote--winson~0~TestClient测试invoiceTitle--winson~~0~1~1141379069~~~1",
//		// "FD0F084400000000007D3A7D0001C4DA--1-0-"};
//
//		// 下单，测试货到付款支付方式
//		// args = new String[] {"5", "381936231", "4",
//		// "~TestClient测试dealNote--winson~1~TestClient测试invoiceTitle--winson~~0~1~1183635322~~~11",
//		// "7AD78C4600000000007A3B100000FCCD--1-0-"};
//
//		// 下单，测试促销
//		// args = new String[] {"5", "381936231", "4",
//		// "~TestClient测试dealNote--winson~0~TestClient测试invoiceTitle--winson~~6000604~1~930000110~~~1",
//		// "EEAC6E3700000000040100000000A809--1-0-"};
//
//		// 查询购物车属性
//		// args = new String[] {"6", "381936231"};
//
//		int type = StringUtil.toInt(args[0], 1);
//		switch (type) {
//		case 0:
//			// 新增
//			testAdd(args);
//			break;
//		case 1:
//			// 删除
//			testRvm(args);
//			break;
//		case 2:
//			// 修改商品数量
//			testModifyNum(args);
//			break;
//		case 3:
//			// 查询购物车
//			testQueryCmdy(args);
//			break;
//		case 4:
//			// 分单
//			testConfirmOrder(args);
//			break;
//		case 5:
//			// 下单
//			testMakeOrder(args);
//			break;
//		case 6:
//			// 查询购物车属性
//			testQueryCmdyKinds(args);
//			break;
//		}
//
//	}
//
//	protected static void testAdd(String[] args) {
//		// buyerUin itemCode itemAttr buyNum addForce
//		long buyerUin = StringUtil.toLong(args[1], -1);
//		String itemCode = args[2];
//		String itemAttr = args[3];
//		int buyNum = StringUtil.toInt(args[4], 1);
//		boolean addForce = "1".equals(args[5]) ? true : false;
//		AddCmdy2CartResp resp = CmdyClient.addCmdy2Cart(buyerUin, itemCode,
//				itemAttr, buyNum, addForce);
//		System.out.println(JSONUtil.getJsonStr(resp));
//	}
//
//	protected static void testRvm(String[] args) {
//		// buyerUin payTyp itemCode-itemAttr-buyNum itemCode1-itemAttr1-buyNum1
//		// ......
//		long buyerUin = StringUtil.toLong(args[1], -1);
//		int payType = StringUtil.toInt(args[2], 0);
//		CmdyItemListPo po = new CmdyItemListPo();
//		po.setPayType(payType);
//		for (int i = 3; i < args.length; i++) {
//			String str = args[i];
//			String[] strs = str.split("-");
//			CmdyItem item = new CmdyItem();
//			item.setItemCode(strs[0]);
//			item.setItemAttr(strs[1]);
//			item.setBuyNum(StringUtil.toInt(strs[2], 1));
//			po.getCmdyItems().add(item);
//		}
//
//		RmvCmdyFromCartResp resp = CmdyClient.rmvCmdy4Cart(buyerUin, po);
//		System.out.println(JSONUtil.getJsonStr(resp));
//	}
//
//	protected static void testModifyNum(String[] args) {
//		// buyerUin itemCode itemAttr buyNum
//		long buyerUin = StringUtil.toLong(args[1], -1);
//		String itemCode = args[2];
//		String itemAttr = args[3];
//		int buyNum = StringUtil.toInt(args[4], 1);
//
//		ModifyCmdyNumInCartResp resp = CmdyClient.modifyCmdyNum(buyerUin,
//				itemCode, itemAttr, buyNum);
//		System.out.println(JSONUtil.getJsonStr(resp));
//	}
//
//	protected static void testQueryCmdy(String[] args) {
//		// buyerUin opForce
//		long buyerUin = StringUtil.toLong(args[1], -1);
//		boolean opForce = "1".equals(args[2]) ? true : false;
//
//		ViewCartResp resp = CmdyClient.queryCmdy(buyerUin, opForce);
//		System.out.println(JSONUtil.getJsonStr(resp));
//	}
//
//	protected static void testConfirmOrder(String[] args) {
//		// buyerUin regionId payType itemCode-itemAttr-buyNum
//		// itemCode1-itemAttr1-buyNum1 ......
//		long buyerUin = StringUtil.toLong(args[1], -1);
//
//		long addrId = 0;
//		long regionId = StringUtil.toLong(args[2], 0);
//		// 获取用户收货地址信息
//		RecvAddrBizImpl recvBiz = new RecvAddrBizImpl();
//		// try {
//		// // RecvAddr recv = recvBiz.listRecvAddr(buyerUin, false);
//		// // ReceiverAddress address = RecvAddrUtil.getFirstRecv(recv);
//		// // if (address != null) {
//		// // regionId = address.getRegionId();
//		// // addrId = address.getAddressId();
//		// // }
//		// } catch (BusinessException e) {
//		// }
//
//		int payType = StringUtil.toInt(args[3], 0);
//
//		CmdyItemListPo po = CmdyItemListPo.disSerialize(args[4]);
//		po.setPayType(payType);
//
//		ConfirmOrderResp resp = CmdyClient.cmdyConfirmOrder(151738366,
//				"@9nyiWgAhJ", po, 40004);
//		System.out.print(resp);
//	}
//
//	protected static void testMakeOrder(String[] args) {
//		// buyerUin recvAddrId
//		// comboId~dealNote~dealType~invoiceTitle~invoiceType~promotionRuleId~sellerProperty~sellerUin~specialFee~specialFeeType~transportType
//		// itemCode-itemAttr-buyNum-priceType-redPacketId
//		// itemCode1-itemAttr1-buyNum1-priceType1-redPacketId1 ......
//		MakeOrderPo req = MakeOrderPo
//				.createInstance4Cmdy(
//						151738366,
//						"@9nyiWgAhJ",
//						"1~9747187~1341636272~11~B0BEF74F00000000040100001326DFB7$$1$15$0~~0",
//						8, 151738366 + "from:" + 1341636272);
//		req.setVer(2);
//		CmdyMakeOrderPo po = CmdyMakeOrderPo.deSerialize(req);
//		po.setRecvAddrId(req.getAddressid());
//		MakeOrderResp cmdyMakeOrderRsp = CmdyClient.cmdyMakeOrder(
//				req.getBuyerUin(), req.getSkey(), po);
//		System.out.println(JSONUtil.getJsonStr(cmdyMakeOrderRsp));
//	}
//
//	protected static void testQueryCmdyKinds(String[] args) {
//		// buyerUin
//		long buyerUin = StringUtil.toLong(args[1], -1);
//		GetCmdyKindsResp resp = CmdyClient.queryCmdyKinds(buyerUin);
//		System.out.println(JSONUtil.getJsonStr(resp));
//	}
//
//}
