package com.qq.qqbuy.thirdparty.deal;


import java.util.BitSet;

import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

import com.qq.qqbuy.common.BitSetJsonBeanProcessor;
import com.qq.qqbuy.item.biz.impl.PPItemBizImpl;
import com.qq.qqbuy.item.po.ItemBO;

public class TestItemBiz
{
   
    
    /**
     * @Title: main
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param args
     *            设定文件
     * @return void 返回类型
     * @throws
     */
    public static void main(String[] args)
    {
        JsonConfig jsoncfg = new JsonConfig();
        String[] excludes = { "size", "empty" };
        jsoncfg.setExcludes(excludes);
        String jsonStr = null;
        PPItemBizImpl biz = new PPItemBizImpl();
//        ItemBO item = biz.fetchItemInfo("7A0B6D1C000000000401000018B8A522",true);
        ItemBO resp = biz.fetchItemInfo("2D829C3E00000000040100000A9E2DD3",true);
        jsonStr = JSONSerializer.toJSON(resp, jsoncfg).toString();
        System.out.println(jsonStr);
    }

}
